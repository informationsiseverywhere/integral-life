/*
 * File: P6674at.java
 * Date: 30 August 2009 0:50:51
 * Author: Quipoz Limited
 *
 * Class transformed from P6674AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
//ILIFE-7857 start
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
//ILIFE-7857 end
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.procedures.LincCSService;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcolf1TableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO; //ILIFE-7857
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf; //ILIFE-7857
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.reassurance.procedures.Actvres;
import com.csc.life.reassurance.procedures.Csncalc;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.reassurance.recordstructures.Csncalcrec;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.RertpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Rertpf;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.LaptTableDAM;
import com.csc.life.terminationclaims.dataaccess.LaptlapTableDAM;
import com.csc.life.terminationclaims.dataaccess.model.Laptpf;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.life.newbusiness.procedures.Nlgcalc;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;
/**
* <pre>
*AUTHOR.
*REMARKS.
*
*  This program will be run under AT and will perform the
*  general functions for the finalisation of Lapse
*  Processing.
*
*  The program will carry out the following functions:
*
*  1.  Increment the TRANNO field on the Contract Header.
*
*  2.  Call the Breakout routine if required.
*
*  3.  Validflag '2' the old COVRMJA record.
*
*  4.  Create a new COVRMJA record.
*
*
*  Processing:
*  -----------
*
*  Contract Header
*  ---------------
*  The ATMOD-PRIMARY-KEY will contain the Company and Contract
*  number of the contract being processed.  Use these to
*  perform a READH on CHDRMJA. Increment the TRANNO field by 1.
*
*
*
*
*
*  Breakout Processing
*  -------------------
*  Read all the LAPT for the contract.
*  If the Plan Suffix on the first record equals zeros, no
*  physical breakout is required
*  else
*  if the lowest Plan Suffix on LAPT is less than or equal to
*  CHDRMJA-POLSUM, a breakout is required.  Set the
*  OLD-SUMMARY to the value of CHDRMJA-POLSUM and the
*  NEW-SUMMARY to the value of the lowest LAPT-PLNSFX, then
*  call 'BRKOUT'.
*
*  Read T5679 (Component Status) for the transaction,
*  accessing the table with the transaction name as the item
*  key.
*
*  BEGNH and NEXTR all the LAPT records again.  For each LAPT
*  record read, update the COVRMJA record(s) and INCR records
*  with processing dependant on field values as below:-
*
*  If LAPT-RIDER is non-zero, update the corresponding COVRMJA
*  record as follows:
*
*       READH the COVRMJA file, with the key set as follows:
*                    Company - LAPT-CHDRCOY
*                    Contract Number - LAPT-CHDRNUM
*                    Life - LAPT-LIFE
*                    Coverage - LAPT-COVERAGE
*                    Rider - LAPT-RIDER
*                    Plan Suffix - LAPT-PLNSFX.
*        Perform the UPDAT-COVR section.
*  u001au001au001au001au001au001a
*  else
*
*  the LAPT-RIDER is zero, so update the corresponding COVRMJA
*  records as follows (ie. Lapse the coverage and the
*  associated riders):
*
*       READH the COVRMJA file, with the key set as follows:
*                    Company - LAPT-CHDRCOY
*                    Contract Number - LAPT-CHDRNUM
*                    Life - LAPT-LIFE
*                    Coverage - LAPT-COVERAGE
*                    Plan Suffix - LAPT-PLNSFX.
*       Perform the UPDAT-COVR section.
*
*       For each Rider attached to the Coverage being processed,
*       check its status against T5679 and then if status is
*       valid, attempt to lapse as above. Perform this for ALL
*       riders attached to the coverage. Use the new logical
*       LAPTLAP to check that the rider about to be processed
*       has not already been selected for Lapse via the online
*       transaction.
*
*  u001au001au001au001au001au001a
*  READH the INCR file, with the key set as follows:
*                    Company - LAPT-CHDRCOY
*                    Contract Number - LAPT-CHDRNUM
*                    Life - LAPT-LIFE
*                    Coverage - LAPT-COVERAGE
*                    Rider - LAPT-RIDER
*                    Plan Suffix - LAPT-PLNSFX.
*  UPDATE the INCR record with Validflag '2' and P/Statcode.
*  u001au001au001au001au001au001a
*  When all processing has been performed for the LAPT record,
*  delete the LAPT record.
*
*  On a change of contract header number or end of file:
*    If a breakout was performed, update the POLSUM field
*    with the new number of summarised policies.
*    Check for the contract being Lapsed:-
*
*       Read all the COVRMJA records for the contract,
*       checking the coverage premium status (PSTATCODE)
*       against the appropriate T5679-SET-COV-PREM-STAT, and
*       coverage risk status (STATCODE) against the appropriate
*       T5679-SET-COV-RISK-STAT.  If the statii do not match,
*       rewrite the CHDRMJA record and exit; else if the
*       statii match for all COVRMJA records, subtract 1 from
*       CHDRMJA-TRANNO, rewrite the CHDRMJA record with a
*       validflag of '2', and write a new CHDRMJA record
*       setting CHDRMJA-TRANNO to CHDRMJA-TRANNO + 1,
*       PSTATCODE to T5679-SET-CN-PREM-STAT and STATCODE to
*       T5679-SET-CN-RISK-STAT.
*
*
*  UPDAT-COVR Processing
*  ---------------------
*       Call T6597-PREMSUBR-04  Using OVRD-OVRDUE-REC
*       if statuz is okay
*          update COVRMJA-PSTATCODE with T6597-CPSTAT-04
*          update COVRMJA-STATCODE  with T6597-CRSTAT-04
*       else
*          use existing coverage statii.
*
*    i.e update the COVERAGE-PSTATCODE And COVERAGE
*        STATCODE With a statuz of LAPSE.
*
*  Rewrite the existing COVRMJA with the validflag set to '2'.
*  Create a new COVRMJA record with fields set as follows:
*
*       TRANNO - CHDRMJA-TRANNO
*       INSTPREM - zeros
*       PREM-CESS-DATE - CHDRMJA-BTDATE
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through it's life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as it's item name, the transaction code and
* the contract status of a contract, eg. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, eg AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to Covrsta records, and
* current categories refer to Covrsts records. Agcmsts
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the Agcmsts, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
*
*
*****************************************************
* </pre>
*/
public class P6674at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P6674AT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOkeys = new FixedLengthStringData(28);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaOkeys, 2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaOkeys, 6);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(17);
	protected PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	protected PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	protected PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	protected FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 16);
	protected PackedDecimalData wsaaBusinessDate = new PackedDecimalData(8, 0);
	protected PackedDecimalData wsaaTotCovrpuInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsaaEndOfLapt = new FixedLengthStringData(1).init("N");
	private Validator endOfLapt = new Validator(wsaaEndOfLapt, "Y");

	protected FixedLengthStringData wsaaEndOfCovr = new FixedLengthStringData(1).init("N");
	private Validator endOfCovr = new Validator(wsaaEndOfCovr, "Y");

	protected FixedLengthStringData wsaaChdrLapsed = new FixedLengthStringData(1).init("N");
	private Validator chdrLapsed = new Validator(wsaaChdrLapsed, "Y");

	protected FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).init("N");
	protected Validator validStatuz = new Validator(wsaaValidStatuz, "Y");

	protected FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1).init("N");
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");
	private PackedDecimalData wsaaOccdate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaIndex2 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	protected FixedLengthStringData wsaaProcessRider = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaRiderStatus = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1).init(SPACES);
	protected Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");

		/*                                                         <D9604>*/
	private FixedLengthStringData wsaaFlexTots = new FixedLengthStringData(30);
	private PackedDecimalData wsaaTotBilled = new PackedDecimalData(18, 2).isAPartOf(wsaaFlexTots, 0);
	private PackedDecimalData wsaaTotPremRecd = new PackedDecimalData(18, 2).isAPartOf(wsaaFlexTots, 10);
	private PackedDecimalData wsaaTotOvrdMin = new PackedDecimalData(18, 2).isAPartOf(wsaaFlexTots, 20);

	private FixedLengthStringData wsaaFpcoUpdate = new FixedLengthStringData(1).init(SPACES);
	private Validator fpcoUpdated = new Validator(wsaaFpcoUpdate, "Y");

		/*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/* TABLES */
	private static final String t5679 = "T5679";
	protected static final String t5687 = "T5687";
	protected static final String t6597 = "T6597";
	private static final String t5729 = "T5729";
	private static final String t7508 = "T7508";
	private static final String tr384 = "TR384";
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	//private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM(); //ILIFE-7857
	private DescTableDAM descIO = new DescTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private Fpcolf1TableDAM fpcolf1IO = new Fpcolf1TableDAM();
	private FprmTableDAM fprmIO = new FprmTableDAM();
	private IncrTableDAM incrIO = new IncrTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected LaptTableDAM laptIO = new LaptTableDAM();
	private LaptlapTableDAM laptlapIO = new LaptlapTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Crtundwrec crtundwrec = new Crtundwrec();
	protected Batckey wsaaBatckey = new Batckey();
	protected Syserrrec syserrrec = new Syserrrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	protected Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	protected Ovrduerec ovrduerec = new Ovrduerec();
	protected Letrqstrec letrqstrec = new Letrqstrec();
	private T5679rec t5679rec = new T5679rec();
	protected T5687rec t5687rec = new T5687rec();
	protected T6597rec t6597rec = new T6597rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T7508rec t7508rec = new T7508rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	protected Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();//NLG
	NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);
	Nlgtpf nlgtpf = new Nlgtpf();  
	//ILIFE-7857 start
	protected Covrpf covrpf = getCovrpf();
	private List<Covrpf> covrpfList = null;
	protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrUpdateRecord;
	private List<Covrpf> covrInsertRecord;
	private List<Covrpf> itemList = null;
	private Iterator<Covrpf> covrpfIter;
	private Covrpf covrpfRec = new Covrpf();
	//ILIFE-7857 end
	Covrpf covr = new Covrpf();
	boolean covrlapse=false;// ILIFE-8601
	private ZonedDecimalData mainRider = new ZonedDecimalData(1, 0).setUnsigned();
	private static final String compStatus = "IF";
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO",ItempfDAO.class); 
	private boolean lincFlag = false;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	private static final String tjl47 = "TJL47";
	private static final String lincFeature = "NBPRP116";
	private boolean adjuReasrFlag;
	private static final String ADJU_REASR_FEATURE = "CSLRI008";
	private Csncalcrec csncalcrec = new Csncalcrec();
	private Laptpf laptpf;
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8).init(SPACES);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private RacdpfDAO racdDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private static final String LAPSE_LETTER_FEATURE = "CSLRI004";
	private boolean lapseLetterFlag = false;//IJS-386
	private List<Rertpf> rertpfUpdateList = new ArrayList<>();
	private List<Rertpf> rertpfList = new ArrayList<>();
	private RertpfDAO rertpfDAO = getApplicationContext().getBean("rertpfDAO", RertpfDAO.class);
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		checkBrkoutReqd1150,
		exit1199,
		nextFpco2470,
		exit3290,
		writeNewContractHeader3420,
		writeRecord3430,
		exit3809
	}

	public P6674at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		mainline0001();
		exit0009();
	}

protected void mainline0001()
	{
		initialise1000();
		if (endOfLapt.isTrue()) {
			batchHeader3500();
			releaseSoftlock3600();
			return ;
		}
		wsaaTotCovrpuInstprem.set(ZERO);
		startLapt1200();
		while ( !(endOfLapt.isTrue())) {
			processLapt2000();
		}
		
		if(!rertpfUpdateList.isEmpty()) {
			rertpfDAO.updateRertList(rertpfUpdateList);
			rertpfUpdateList.clear();
		}
		finalise3000();
		if (isEQ(chdrmjaIO.getNlgflg().trim(),'Y')){
			nlgtpf = nlgtpfDAO.getNlgtRecord(chdrmjaIO.chdrcoy.toString(),chdrmjaIO.chdrnum.toString());
			ReadNlgt();
		}
		if(lincFlag)
			performLincProcessing();

		a000Statistics();
	}

protected Covrpf getCovrpf() {

	return new Covrpf();
}

private void ReadNlgt() {
	// TODO Auto-generated method stub
	initialize(nlgcalcrec.nlgcalcRec);
	nlgcalcrec.function.set(SPACES);
	nlgcalcrec.tranno.set(ptrnIO.getTranno());
	nlgcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
	nlgcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
	if(lapseLetterFlag) {//IJS-386
	  nlgcalcrec.effdate.set(chdrmjaIO.getPtdate());		
	}else {
	  nlgcalcrec.effdate.set(datcon1rec.intDate);
	}
	nlgcalcrec.frmdate.set(chdrmjaIO.getOccdate()); 
	nlgcalcrec.ptdate.set(chdrmjaIO.getPtdate());
	nlgcalcrec.btdate.set(chdrmjaIO.getBtdate());
	nlgcalcrec.billfreq.set(chdrmjaIO.getBillfreq());
	nlgcalcrec.batcactmn.set(nlgtpf.getBatcactmn());
	nlgcalcrec.cnttype.set(chdrmjaIO.getCnttype());
	nlgcalcrec.batcactyr.set(nlgtpf.getBatcactyr());
	nlgcalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	nlgcalcrec.language.set(atmodrec.language);
	nlgcalcrec.billchnl.set(chdrmjaIO.getBillchnl());
	nlgcalcrec.Cntcurr.set(chdrmjaIO.getCntcurr());
	nlgcalcrec.inputAmt.set(ZERO);
	nlgcalcrec.todate.set(varcom.maxdate);
	nlgcalcrec.nlgBalance.set(nlgtpf.getNlgbal());
	nlgcalcrec.totTopup.set(nlgtpf.getAmnt01());
	nlgcalcrec.totWdrAmt.set(nlgtpf.getAmnt02());
	nlgcalcrec.ovduePrem.set(nlgtpf.getAmnt03());
	nlgcalcrec.unpaidPrem.set(nlgtpf.getAmnt04());
	nlgcalcrec.yrsInf.set(nlgtpf.getYrsinf());
	nlgcalcrec.nlgFlag.set(nlgtpf.getNlgflag());
	nlgcalcrec.transMode.set("L");
	nlgcalcrec.status.set(Varcom.oK);
callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec);
if (isNE(nlgcalcrec.status,varcom.oK)) {
syserrrec.statuz.set(nlgcalcrec.status);
syserrrec.params.set(nlgcalcrec.nlgcalcRec);
fatalError9000();
}
}

protected void exit0009()
	{
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
		start1010();
	}

protected void start1010()
	{
		/*    Retrieve the contract header and store the transaction number*/
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		/*    Get today's date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError9000();
		}
		wsaaBusinessDate.set(datcon1rec.intDate);
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFunction(varcom.readr);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		wsaaItemCnttype.set(chdrmjaIO.getCnttype());
		/*    Read the Component Status Table (T5679) for the transaction.*/
		readT56794000();
		/*  MOVE CHDRMJA-CURRFROM       TO WSAA-CURRFROM.                */
		wsaaEndOfLapt.set("N");
		wsaaPlanSuffix.set(9999);
		checkIfBreakoutReqd1100();
		readT57291300();
		lincFlag = FeaConfg.isFeatureExist(atmodrec.company.toString(), lincFeature, appVars, "IT");
		adjuReasrFlag = FeaConfg.isFeatureExist(atmodrec.company.toString(), ADJU_REASR_FEATURE, appVars, "IT");
		lapseLetterFlag = FeaConfg.isFeatureExist(atmodrec.company.toString(), LAPSE_LETTER_FEATURE, appVars, "IT");
	}

	/**
	* <pre>
	* Call the BRKOUT subroutine to breakout COVR and AGCM        *
	* INCR (Increase Pending) records are similarly broken out.   *
	* </pre>
	*/
protected void checkIfBreakoutReqd1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1110();
					nextr1130();
				case checkBrkoutReqd1150:
					checkBrkoutReqd1150();
					performBrkout1170();
				case exit1199:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1110()
	{
		/*    Read all the LAPT for the contract.*/
		/*    If the Plan Suffix on the first record equals zeros, no*/
		/*    physical breakout is required*/
		/*    else*/
		/*    if the lowest Plan Suffix on LAPT is less than or equal to*/
		/*    CHDRMJA-POLSUM, a breakout is required.*/
		/*    Set the OLD-SUMMARY to the value of CHDRMJA-POLSUM*/
		/*    and the NEW-SUMMARY to the value of the lowest LAPT-PLAN-SUFF*/
		/*    then call 'BRKOUT'.*/
		laptIO.setDataArea(SPACES);
		laptIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		laptIO.setChdrnum(chdrmjaIO.getChdrnum());
		laptIO.setPlanSuffix(ZERO);
		laptIO.setFunction(varcom.begn);


		//performance improvement --  atiwari23
		laptIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		laptIO.setFitKeysSearch("CHDRNUM");

		laptIO.setFormat(formatsInner.laptrec);
		SmartFileCode.execute(appVars, laptIO);
		if (isNE(laptIO.getStatuz(), varcom.oK)
		&& isNE(laptIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(laptIO.getParams());
			syserrrec.statuz.set(laptIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(laptIO.getStatuz(), varcom.endp)
		|| isNE(chdrmjaIO.getChdrnum(), laptIO.getChdrnum())) {
			wsaaEndOfLapt.set("Y");
			goTo(GotoLabel.exit1199);
		}
		if (isEQ(laptIO.getPlanSuffix(), ZERO)) {
			wsaaPlanSuffix.set(ZERO);
			goTo(GotoLabel.exit1199);
		}
		if (isEQ(chdrmjaIO.getPolsum(), 1)) {
			wsaaPlanSuffix.set(laptIO.getPlanSuffix());
			goTo(GotoLabel.exit1199);
		}
		laptIO.setFunction(varcom.nextr);
	}

protected void nextr1130()
	{
		if ((setPrecision(chdrmjaIO.getPolsum(), 0)
		&& isGT(chdrmjaIO.getPolsum(), sub(laptIO.getPlanSuffix(), 1)))) {
			if (isLT(laptIO.getPlanSuffix(), wsaaPlanSuffix)) {
				wsaaPlanSuffix.set(laptIO.getPlanSuffix());
			}
		}
		SmartFileCode.execute(appVars, laptIO);
		if (isNE(laptIO.getStatuz(), varcom.oK)
		&& isNE(laptIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(laptIO.getParams());
			syserrrec.statuz.set(laptIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(laptIO.getStatuz(), varcom.endp)
		|| isNE(chdrmjaIO.getChdrnum(), laptIO.getChdrnum())) {
			goTo(GotoLabel.checkBrkoutReqd1150);
		}
		nextr1130();
		return ;
	}

protected void checkBrkoutReqd1150()
	{
		if ((setPrecision(chdrmjaIO.getPolsum(), 0)
		&& isGT(chdrmjaIO.getPolsum(), sub(wsaaPlanSuffix, 1)))) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.exit1199);
		}
	}

protected void performBrkout1170()
	{
		brkoutrec.brkOldSummary.set(chdrmjaIO.getPolsum());
		compute(brkoutrec.brkNewSummary, 0).set(sub(wsaaPlanSuffix, 1));
		brkoutrec.brkChdrnum.set(chdrmjaIO.getChdrnum());
		brkoutrec.brkChdrcoy.set(chdrmjaIO.getChdrcoy());
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		brkoutrec.brkStatuz.set(varcom.oK);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz, varcom.oK)) {
			syserrrec.params.set(brkoutrec.outRec);
			syserrrec.statuz.set(brkoutrec.brkStatuz);
			fatalError9000();
		}
	}

protected void startLapt1200()
	{
		start1210();
	}

protected void start1210()
	{
		/*    Begin the LAPT file again, to enable record processing*/
		laptIO.setDataArea(SPACES);
		laptIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		laptIO.setChdrnum(chdrmjaIO.getChdrnum());
		laptIO.setPlanSuffix(ZERO);
		laptIO.setFunction(varcom.begnh);
		laptIO.setFormat(formatsInner.laptrec);
		SmartFileCode.execute(appVars, laptIO);
		if (isNE(laptIO.getStatuz(), varcom.oK)
		&& isNE(laptIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(laptIO.getParams());
			syserrrec.statuz.set(laptIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(laptIO.getStatuz(), varcom.endp)
		|| isNE(chdrmjaIO.getChdrnum(), laptIO.getChdrnum())) {
			batchHeader3500();
			releaseSoftlock3600();
			exit0009();
		}
		if (isEQ(laptIO.getStatuz(), varcom.oK)
		&& isEQ(laptIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		&& isEQ(laptIO.getChdrnum(), chdrmjaIO.getChdrnum())
		&& isEQ(wsaaLife, SPACES)) {
			wsaaLife.set(laptIO.getLife());
			wsaaCoverage.set(laptIO.getCoverage());
			if(laptpf==null) {
				laptpf = new Laptpf();
				laptpf.setLife(laptIO.getLife().toString());
				laptpf.setCoverage(laptIO.getCoverage().toString());
				laptpf.setRider(laptIO.getRider().toString());
				laptpf.setPlnsfx(laptIO.getPlanSuffix().toInt());
			}
		}
	}

	/**
	* <pre>
	**********************************************************<D9604>
	* </pre>
	*/
protected void readT57291300()
	{
		start1310();
	}

	/**
	* <pre>
	**********************************************************<D9604>
	* </pre>
	*/
protected void start1310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5729);
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			initialize(wsaaFlexTots);
			wsaaFlexiblePremium.set("Y");
		}
	}

protected void processLapt2000()
	{
		start2010();
	}

protected void start2010()
	{
		incrCheck5000();
		/*    If LAPT-RIDER is zeros, Lapse the coverage and lapse all*/
		/*    the associated riders  otherwise*/
		/*    The rider is non-zero, so only the Rider selected is to*/
		/*    be made lapse*/
		//ILIFE-7857 start
		covrpf.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		covrpf.setChdrnum(chdrmjaIO.getChdrnum().toString());
		covrpf.setPlanSuffix(ZERO.intValue());
		covrpfList = covrpfDAO.getCovrmjaByComAndNum(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
		if(covrpfList.isEmpty()){
			fatalError9000();
		}
		rertpfList = rertpfDAO.getRertpfList(chdrmjaIO.getChdrcoy().toString(),chdrmjaIO.getChdrnum().toString(),"1",0);
		
		int j = 0;
		for(int i = 0; i < covrpfList.size(); i++) {
			if("00".equals(covrpfList.get(i).getRider()) && "1".equals(covrpfList.get(i).getValidflag())  && compStatus.equals(covrpfList.get(i).getStatcode())) {
				j++;
			}
		}
		
		mainRider.set(j);
		if (isEQ(laptIO.getRider(), "00")) {
			/*    MOVE ZEROS                  TO WSAA-RIDER                 */
			/*covrmjaIO.setParams(SPACES);
			covrmjaIO.setStatuz(varcom.oK);
			covrmjaIO.setFunction(varcom.begn);
			covrmjaIO.setFormat(formatsInner.covrmjarec);
			covrmjaIO.setChdrcoy(laptIO.getChdrcoy());
			covrmjaIO.setChdrnum(laptIO.getChdrnum());
			covrmjaIO.setLife(laptIO.getLife());
			covrmjaIO.setCoverage(laptIO.getCoverage());
			covrmjaIO.setRider(laptIO.getRider());
			covrmjaIO.setPlanSuffix(laptIO.getPlanSuffix());*/
			
			mainRider.subtract(1);
		
			
			covrpf.setChdrcoy(laptIO.getChdrcoy().toString());
			covrpf.setChdrnum(laptIO.getChdrnum().toString());
			covrpf.setLife(laptIO.getLife().toString());
			covrpf.setPlanSuffix(laptIO.getPlanSuffix().toInt());
			//covrpfList = covrpfDAO.selectCovrRecord(covrpf);
			covrpf.setValidflag("1");
				covrpfList = covrpfDAO.selectCovrRecordBegin(covrpf);
			
			if(covrpfList==null || (covrpfList!=null && covrpfList.size()==0))
			{
				return;
			}
		else {
			for(Covrpf covrpf : covrpfList){
				//ILIFE-8601 start
				if("1".equals(covrpf.getValidflag()) && covrpf.getCoverage().equals(laptIO.getCoverage().toString())) {
						processCovrs2100(covrpf);
					}
				
			}
			}
			//performance improvement --  atiwari23
			/*covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE");*/
			//getCovr2050();
			/*    PERFORM 2100-PROCESS-COVRS  UNTIL COVRMJA-STATUZ = MRNF   */
		/*	while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
				processCovrs2100();
			}
*/
			laptIO.setRider("00");
			deleteLapt2500();
			a300BegnLifeio();
			a100DelPolUnderwritting();
		}
		else {
			//covrmjaIO.setParams(SPACES);
			covrInsertRecord=new LinkedList<Covrpf>();//added for ILIFE-7945
			covrpf.setChdrcoy(laptIO.getChdrcoy().toString());
			covrpf.setChdrnum(laptIO.getChdrnum().toString());
			covrpf.setLife(laptIO.getLife().toString());
			covrpf.setCoverage(laptIO.getCoverage().toString());
			covrpf.setRider(laptIO.getRider().toString());
			covrpf.setPlanSuffix(laptIO.getPlanSuffix().toInt());
			covrpf.setValidflag("1");//added for ILIFE-7945
		  /*readhCovr2200(covrpf);
			updateCovr2600(covrpf);
			a300BegnLifeio();
			a200DelCovUnderwritting(covrpf);commented for ILIFE-7945*/
			
			//added for ILIFE-7945
			readhCovr2200();
			updateCovr2600(covr);
			a300BegnLifeio();
			a200DelCovUnderwritting(covr);
		
			//end
			/*****    PERFORM 2600-UPDATE-COVR.                                 */
		}
	}

protected void getCovr2050()
	{
		/*START*/
		//ILIFE-7857 start
	/*	SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError9000();
		}
		if (isNE(covrmjaIO.getChdrcoy(), laptIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), laptIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), laptIO.getLife())
		|| isNE(covrmjaIO.getCoverage(), laptIO.getCoverage())
		|| isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
		}*/
		/*EXIT*/
		//ILIFE-7857 end
	}
//ILIFE-7857 start
protected void processCovrs2100(Covrpf covrpf)
	{
		//start2110();
		 covrUpdateRecord=new LinkedList<Covrpf>();
		 covrInsertRecord=new LinkedList<Covrpf>();
		
		if (isNE(covrpf.getPlanSuffix(), laptIO.getPlanSuffix())) {
			return ;
		}
		wsaaProcessRider.set("Y");
		if (isNE(covrpf.getRider(), "00")) {
			readT56794000();
			checkRiderStatus2700(covrpf);//ILIFE-8157
		}
		if (isEQ(wsaaProcessRider, "Y")) {
			/*readhCovr2200(covrpf);
			updateCovr2300(covrpf);commented for ILIFE-7945*/
			//readhCovr2200();
			updateCovr2300(covrpf);//ILIFE-8601 
		
			
			/* if(!covrUpdateRecord.isEmpty()){
				 covrpfDAO.updateCovrBulk(covrUpdateRecord);
			 }
			 
			 if(!covrInsertRecord.isEmpty()){
				 covrpfDAO.insertCovrBulk(covrInsertRecord);
			 }*/
		}
		
	//	nextCovrRecord2110();
	//ILIFE-7857 end
	}

protected void start2110()
	{
		/* We check the plan-suffix here, since we could be processing a   */
		/*  multi-policy plan with multiple riders and we are only Lapsing */
		/*  one of the policies within the plan - ie lapse all riders, but */
		/*  only if they have the specified plan-suffix. We need to ignore */
		/*  all the other rider records with different plan-suffix values  */
		//ILIFE-7857 start
		/*if (isNE(covrmjaIO.getPlanSuffix(), laptIO.getPlanSuffix())) {
			return ;
		}*/
		//ILIFE-7857 end
		/* PERFORM 2200-READH-COVR.                                     */
		/* IF COVRMJA-STATUZ           NOT = O-K                        */
		/* AND COVRMJA-STATUZ          NOT = MRNF                       */
		/*    MOVE COVRMJA-PARAMS      TO SYSR-PARAMS                   */
		/*    MOVE COVRMJA-STATUZ      TO SYSR-STATUZ                   */
		/*    PERFORM 9000-FATAL-ERROR                                  */
		/* END-IF.                                                      */
		/* IF COVRMJA-STATUZ           = MRNF                           */
		/*    NEXT SENTENCE                                             */
		/* ELSE                                                         */
		/*    PERFORM 2300-UPDATE-COVR                                  */
		/*    ADD 1                    TO WSAA-RIDER                    */
		/*    MOVE WSAA-RIDER          TO LAPT-RIDER                    */
		/* END-IF.                                                      */
		/* IF COVRMJA-STATUZ           = MRNF                      <009>*/
		/*    GO TO 2119-EXIT                                      <009>*/
		/* END-IF.                                                 <009>*/
		/* If we are processing a rider attatched to a coverage check      */
		/* whether it has already been lapsed . If so we don't want        */
		/* to perform section 2300.                                        */
		/* The following code (<009>), currently checks against to see     */
		/*   if the Rider we are processing has already been LApsed. IF    */
		/*   not, we then try and LApse the rider, regardless of the       */
		/*   status of the Rider....                                       */
		/* We Need to change this processing so that only COVS/RIDs with   */
		/*   a status on the LEFT side of T5679 get processed - just       */
		/*   because a rider is, say, SU (surrender), why should we then   */
		/*   try and LAPSE it ???                                          */
		/*  We also need to ensure that there is no LAPT record for        */
		/*  the rider we are processing - this means it has already been   */
		/*  selected for processing at the online stage so we shouldn't    */
		/*  try and do it here.                                            */
		//ILIFE-7857 start
		/*wsaaProcessRider.set("Y");
		if (isNE(covrmjaIO.getRider(), "00")) {
			readT56794000();
			checkRiderStatus2700();
		}*/
		//ILIFE-7857 end 
		/* IF COVRMJA-RIDER            NOT = '00'                  <009>*/
		/*    AND COVRMJA-STATCODE     = T5679-SET-RID-RISK-STAT   <009>*/
		/*    AND COVRMJA-PSTATCODE    = T5679-SET-RID-PREM-STAT   <009>*/
		/*     NEXT SENTENCE                                       <009>*/
		/* ELSE                                                    <009>*/
		/*    PERFORM 2300-UPDATE-COVR                             <009>*/
		/* END-IF.                                                 <009>*/
		/* Now check whether we really want to process the rider or not    */
		//ILIFE-7857 start
		/*if (isEQ(wsaaProcessRider, "Y")) {
			readhCovr2200();
			updateCovr2300();
		}*/
		//ILIFE-7857 end
	}

	/**
	* <pre>
	**** ADD 1                       TO WSAA-RIDER.              <009>
	**** MOVE WSAA-RIDER             TO LAPT-RIDER.              <009>
	* </pre>
	*/
	//ILIFE-7857 start
/*protected void nextCovrRecord2110()
	{
		 Now get the next COVR record ( if we have just processed a      
		  coverage, we will now be looking for a rider, if we have just  
		  processed a rider, we will be looking for the next rider etc.) 
		covrmjaIO.setFunction(varcom.nextr);
		getCovr2050();
		EXIT
	}*/

//protected void readhCovr2200(Covrpf covrpf) commented for ILIFE-7945
protected void readhCovr2200()
	{
		/*START*/
		/* MOVE SPACES                 TO COVRMJA-DATA-AREA.            */
		/* MOVE LAPT-CHDRCOY           TO COVRMJA-CHDRCOY.              */
		/* MOVE LAPT-CHDRNUM           TO COVRMJA-CHDRNUM.              */
		/* MOVE LAPT-LIFE              TO COVRMJA-LIFE.                 */
		/* MOVE LAPT-COVERAGE          TO COVRMJA-COVERAGE.             */
		/* MOVE LAPT-RIDER             TO COVRMJA-RIDER.                */
		/* MOVE LAPT-PLAN-SUFFIX       TO COVRMJA-PLAN-SUFFIX.          */
	//ILIFE-7945
	//covrpfList = covrpfDAO.selectCovrRecord(covrpf);
	covrpf = covrpfDAO.getCovrRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlanSuffix(), covrpf.getValidflag());
	//if(covrpfList==null){
	if(covrpf==null){
		fatalError9000();
	}
	covr = new Covrpf(covrpf);
	//end
		/*covrmjaIO.setFunction(varcom.readh);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			 AND COVRMJA-STATUZ          NOT = MRNF                       
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}*/
		/*EXIT*/
	}
/*
protected void updateCovr2300(Covrpf covrpf)commented for ILIFE-7945*/
protected void updateCovr2300(Covrpf covrpf) // ILIFE-8157
{
		/*start2310();
		writeCovr2340();*/
	
	covrpf.setValidflag("2");
	covrpf.setCurrto(chdrmjaIO.getBtdate().toInt());
/*	covrpfDAO.updateCovrBulk(covrUpdateRecord);
	covrUpdateRecord.add(covrpf);*/
//	covrpfDAO.updateCovrmjaRecord(covrpfList);
	covrpfDAO.updateCovrValidAndCurrtoFlag(covrpf);
	/*    Create a new COVRMJA record with new fields*/  	
	compute(wsaaTotCovrpuInstprem, 3).setRounded((add(wsaaTotCovrpuInstprem, covrpf.getInstprem())));
	//wsaaTotCovrpuInstprem.add(covrpf.getInstprem());
	covrpf.setTransactionDate(wsaaTransactionDate.toInt());
	covrpf.setTransactionTime(wsaaTransactionTime.toInt());
	covrpf.setUser(wsaaUser.toInt());
	covrpf.setTermid(wsaaTermid.toString());
	covrpf.setTranno(chdrmjaIO.getTranno().toInt());
	covrpf.setValidflag("1");
	covrpf.setCurrfrom(chdrmjaIO.getBtdate().toInt());
	covrpf.setCurrto(varcom.vrcmMaxDate.toInt());
	covrpf.setInstprem(BigDecimal.ZERO); //IBPLIFE-4293
	lapseMethod4000(covrpf);
	checkExistingRertRecord(covrpf);
	covrInsertRecord.clear();
	covrInsertRecord.add(covrpf);
	covrpfDAO.insertCovrRecord(covrInsertRecord);
	
	if (flexiblePremium.isTrue()
			&& isEQ(covrpf.getRider(), "00")) {
				processFpco2400();
			}
	/*    MOVE CHDRMJA-BTDATE         TO COVRMJA-PREM-CESS-DATE.       */
	//ILIFE-7857 end
	}

protected void checkExistingRertRecord(Covrpf covrpf) {
	if(!rertpfList.isEmpty()) {
		for(Rertpf rert : rertpfList) {
			if(covrpf.getLife().equals(rert.getLife()) && covrpf.getCoverage().equals(rert.getCoverage())
					&& covrpf.getRider().equals(rert.getRider()) && covrpf.getPlanSuffix()==rert.getPlnsfx()) {
				Rertpf r = new Rertpf(rert);
				r.setValidflag("2");
				rertpfUpdateList.add(r);
			}
		}
	}
}

protected void start2310()
	{
		/*    Rewrite the existing COVRMJA with the validflag set to '2'*/
	//ILIFE-7857 start
		/*covrpf.setValidflag("2");
		covrpf.setCurrto(chdrmjaIO.getBtdate());
		covrpfList = covrpfDAO.updateCovrBulk("2",chdrmjaIO.getBtdate().toString());
		covrUpdateRecord.add(covrpf);*/
		/*covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(chdrmjaIO.getBtdate());
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError9000();
		}*/
		/*    Create a new COVRMJA record with new fields*/   
		/*wsaaTotCovrpuInstprem.add(covrpf.getInstprem());
		covrpf.setTransactionDate(wsaaTransactionDate);
		covrpf.setTransactionTime(wsaaTransactionTime);
		covrpf.setUser(wsaaUser);
		covrpf.setTermid(wsaaTermid);
		covrpf.setTranno(chdrmjaIO.getTranno());
		covrpf.setValidflag("1");
		covrInsertRecord.add(covrpf);
		if (flexiblePremium.isTrue()
				&& isEQ(covrmjaIO.getRider(), "00")) {
					processFpco2400();*/
				}
		/*    MOVE CHDRMJA-BTDATE         TO COVRMJA-PREM-CESS-DATE.       */
	//	lapseMethod4000();
	/*}*/

protected void writeCovr2340()
	{
		/*covrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError9000();
		}
		if (flexiblePremium.isTrue()
		&& isEQ(covrmjaIO.getRider(), "00")) {
			processFpco2400();
		}*/
		/*EXIT*/
		//ILIFE-7857 end
	}

protected void processFpco2400()
	{
		start2410();
	}

	/**
	* <pre>
	**************************                                <D9604>
	* </pre>
	*/
protected void start2410()
	{
		/*                                                         <D9604>*/
		/*    Sum the total billed, recd and ovrdue min to subtract<D9604>*/
		/*    from the FPRM.                                       <D9604>*/
		/*    Then vf2 the FPCO and write a new vf1.               <D9604>*/
		/*                                                         <D9604>*/
		wsaaFpcoUpdate.set("Y");
		//ILIFE-7857 start
		fpcoIO.setChdrcoy(covrpf.getChdrcoy());
		fpcoIO.setChdrnum(covrpf.getChdrnum());
		fpcoIO.setLife(covrpf.getLife());
		fpcoIO.setCoverage(covrpf.getCoverage());
		fpcoIO.setRider(covrpf.getRider());
		fpcoIO.setPlanSuffix(covrpf.getPlanSuffix());
		fpcoIO.setTargfrom(0);
		fpcoIO.setFunction(varcom.begnh);
		fpcoIO.setFormat(formatsInner.fpcorec);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fpcoIO.getStatuz());
			syserrrec.params.set(fpcoIO.getParams());
			fatalError9000();
		}
		if (isNE(fpcoIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(fpcoIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(fpcoIO.getLife(), covrpf.getLife())
		|| isNE(fpcoIO.getCoverage(), covrpf.getCoverage())
		|| isNE(fpcoIO.getRider(), covrpf.getRider())
		|| isNE(fpcoIO.getPlanSuffix(), covrpf.getPlanSuffix())) {
			syserrrec.params.set(fpcoIO.getParams());
			fatalError9000();
		}
		while ( !(isEQ(fpcoIO.getStatuz(), varcom.endp))) {
			updateFpco2450();
		}
//ILIFE-7857 end
	}

	/**
	* <pre>
	*                                                         <D9604>
	* </pre>
	*/
protected void updateFpco2450()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2450();
					updateFpco2460();
					writeVf1Fpco2465();
				case nextFpco2470:
					nextFpco2470();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	**************************                                <D9604>
	* </pre>
	*/
protected void start2450()
	{
		/*                                                         <D9604>*/
		/*    Sum the total billed, recd and ovrdue min to subtract<D9604>*/
		/*    from the FPRM.                                       <D9604>*/
		/*    Then vf2 the FPCO and write a new vf1.               <D9604>*/
		/*                                                         <D9604>*/
		/*    dont update the record just written, rewrt to release<D9604>*/
		/*                                                         <D9604>*/
		if (isEQ(fpcoIO.getTranno(), chdrmjaIO.getTranno())) {
			fpcoIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, fpcoIO);
			if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(fpcoIO.getStatuz());
				syserrrec.params.set(fpcoIO.getParams());
				fatalError9000();
			}
			goTo(GotoLabel.nextFpco2470);
		}
		/*                                                         <D9604>*/
		wsaaTotBilled.add(fpcoIO.getBilledInPeriod());
		wsaaTotPremRecd.add(fpcoIO.getPremRecPer());
		wsaaTotOvrdMin.add(fpcoIO.getOverdueMin());
		fpcoIO.setValidflag("2");
		fpcoIO.setCurrto(wsaaBusinessDate);
	}

protected void updateFpco2460()
	{
		fpcoIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fpcoIO.getStatuz());
			syserrrec.params.set(fpcoIO.getParams());
			fatalError9000();
		}
	}

protected void writeVf1Fpco2465()
	{
		fpcolf1IO.setParams(SPACES);
		fpcolf1IO.setChdrcoy(fpcoIO.getChdrcoy());
		fpcolf1IO.setChdrnum(fpcoIO.getChdrnum());
		fpcolf1IO.setValidflag("1");
		fpcolf1IO.setCurrto(varcom.vrcmMaxDate);
		fpcolf1IO.setCurrfrom(fpcoIO.getCurrto());
		fpcolf1IO.setEffdate(fpcoIO.getCurrto());
		fpcolf1IO.setTargfrom(fpcoIO.getTargfrom());
		fpcolf1IO.setTargto(fpcoIO.getTargto());
		fpcolf1IO.setLife(fpcoIO.getLife());
		fpcolf1IO.setCoverage(fpcoIO.getCoverage());
		fpcolf1IO.setRider(fpcoIO.getRider());
		fpcolf1IO.setPlanSuffix(fpcoIO.getPlanSuffix());
		fpcolf1IO.setActiveInd("N");
		fpcolf1IO.setAnnProcessInd("N");
		fpcolf1IO.setTargetPremium(fpcoIO.getTargetPremium());
		fpcolf1IO.setPremRecPer(ZERO);
		fpcolf1IO.setBilledInPeriod(ZERO);
		fpcolf1IO.setOverdueMin(ZERO);
		fpcolf1IO.setMinOverduePer(ZERO);
		fpcolf1IO.setTranno(chdrmjaIO.getTranno());
		fpcolf1IO.setAnnivProcDate(fpcoIO.getAnnivProcDate());
		fpcolf1IO.setFunction(varcom.writr);
		fpcolf1IO.setFormat(formatsInner.fpcolf1rec);
		SmartFileCode.execute(appVars, fpcolf1IO);
		if (isNE(fpcolf1IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcolf1IO.getParams());
			syserrrec.statuz.set(fpcolf1IO.getStatuz());
			fatalError9000();
		}
	}

protected void nextFpco2470()
	{
		fpcoIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)
		&& isNE(fpcoIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(fpcoIO.getStatuz());
			syserrrec.params.set(fpcoIO.getParams());
			fatalError9000();
		}
		//ILIFE-7857 start
		if (isNE(fpcoIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(fpcoIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(fpcoIO.getLife(), covrpf.getLife())
		|| isNE(fpcoIO.getCoverage(), covrpf.getCoverage())
		|| isNE(fpcoIO.getRider(), covrpf.getRider())
		|| isNE(fpcoIO.getPlanSuffix(), covrpf.getPlanSuffix())) {
			fpcoIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, fpcoIO);
			if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(fpcoIO.getStatuz());
				syserrrec.params.set(fpcoIO.getParams());
				fatalError9000();
			}
			fpcoIO.setStatuz(varcom.endp);
			//ILIFE-7857 end
		}
	}

protected void deleteLapt2500()
	{
		start2510();
	}

protected void start2510()
	{
		/*    The temporary LAPT record has been processed, so delete the*/
		/*    record.*/
		laptIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, laptIO);
		if (isNE(laptIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(laptIO.getParams());
			syserrrec.statuz.set(laptIO.getStatuz());
			fatalError9000();
		}
		laptIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, laptIO);
		if ((isNE(laptIO.getStatuz(), varcom.oK))
		&& (isNE(laptIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(laptIO.getParams());
			syserrrec.statuz.set(laptIO.getStatuz());
			fatalError9000();
		}
		/*    Read the next LAPT record*/
		laptIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, laptIO);
		if (isNE(laptIO.getStatuz(), varcom.oK)
		&& isNE(laptIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(laptIO.getParams());
			syserrrec.statuz.set(laptIO.getStatuz());
			fatalError9000();
		}
		/*2085-CONTINUE.                                                   */
		if (isEQ(laptIO.getStatuz(), varcom.endp)
		|| isNE(chdrmjaIO.getChdrcoy(), laptIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), laptIO.getChdrnum())) {
			wsaaEndOfLapt.set("Y");
		}
	}
//ILIFE-7857 start
//protected void updateCovr2600(Covrpf covrpf)ILIFE-7945
protected void updateCovr2600(Covrpf covrpf) //ILIFE-8157
	{
		/*CONT*/
		/* IF COVRMJA-STATUZ           = MRNF                           */
		/*    MOVE COVRMJA-PARAMS      TO SYSR-PARAMS                   */
		/*    MOVE COVRMJA-STATUZ      TO SYSR-STATUZ                   */
		/*    PERFORM 9000-FATAL-ERROR                                  */
		/* END-IF.                                                      */
		//updateCovr2300(covrpf);ILIFE-7945
		updateCovr2300(covrpf); //ILIFE-8157
		deleteLapt2500();
		/*EXIT*/
	}
//ILIFE-7857 end
protected void checkRiderStatus2700(Covrpf covrpf) //ILIFE-8157
	{
		/*START*/
		/* Check that the Rider we are processing has valid risk &         */
		/*  premium statii as defined by T5679.                            */
		wsaaRiderStatus.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaRiderStatus, "Y")); wsaaIndex.add(1)){
			checkRiderStatii2800(covrpf);  //ILIFE-8157
		}
		if (isNE(wsaaRiderStatus, "Y")) {
			wsaaProcessRider.set("N");
			return ;
		}
		/* Check if this Rider has already been selected for Lapsins.      */
		checkRiderLapt2900(covrpf); //ILIFE-8157 
		/*EXIT*/
	}

protected void checkRiderStatii2800(Covrpf covrpf) //ILIFE-8157
	{
		/*START*/
		//ILIFE-7857 start
		if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrpf.getStatcode())) {
			for (wsaaIndex2.set(1); !(isGT(wsaaIndex2, 12)
			|| isEQ(wsaaRiderStatus, "Y")); wsaaIndex2.add(1)){
				if (isEQ(t5679rec.ridPremStat[wsaaIndex2.toInt()], covrpf.getPstatcode())) {
					wsaaRiderStatus.set("Y");
				}
				//ILIFE-7857 end
			}
		}
		/*EXIT*/
	}

protected void checkRiderLapt2900(Covrpf covrpf)
	{
		start2900(covrpf); //ILIFE-8157
	}

protected void start2900(Covrpf covrpf)
	{
		/* Need to check if a LAPT record exists for this Rider. If there  */
		/*  is, a manual Lapse has already been requested for this rider   */
		/*  so we don't want to process it now simply because we are       */
		/*  lapsing its parent Coverage.                                   */
		/*  So, just do a straight read using the LAPTLAP logical just     */
		/*   to see if a record exists.                                    */
		laptlapIO.setParams(SPACES);
		laptlapIO.setFunction(varcom.readr);
		laptlapIO.setFormat(formatsInner.laptlaprec);
		//ILIFE-7857 start
		laptlapIO.setChdrcoy(covrpf.getChdrcoy());
		laptlapIO.setChdrnum(covrpf.getChdrnum());
		laptlapIO.setLife(covrpf.getLife());
		laptlapIO.setCoverage(covrpf.getCoverage());
		laptlapIO.setRider(covrpf.getRider());
		laptlapIO.setPlanSuffix(covrpf.getPlanSuffix());
		//ILIFE-7857 end
		SmartFileCode.execute(appVars, laptlapIO);
		if (isNE(laptlapIO.getStatuz(), varcom.oK)
		&& isNE(laptlapIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(laptlapIO.getParams());
			syserrrec.statuz.set(laptlapIO.getStatuz());
			fatalError9000();
		}
		/*  If there is no LAPT record found, then it is OK to continue    */
		/*    processing this rider, so set status accordingly.            */
		if (isEQ(laptlapIO.getStatuz(), varcom.mrnf)) {
			wsaaProcessRider.set("Y");
		}
		else {
			wsaaProcessRider.set("N");
		}
	}

protected void finalise3000()
	{
		start3010();
	}

protected void start3010()
	{
		writePtrn3100();
		wsaaEndOfCovr.set("N");
		wsaaValidStatuz.set("N");
		wsaaChdrLapsed.set("N");
		wsaaValidPstatcode.set("Y");
		//ILIFE-7857 start
		covrpf.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		covrpf.setChdrnum(chdrmjaIO.getChdrnum().toString());
		covrpf.setPlanSuffix(ZERO.intValue());
		covrpfList = covrpfDAO.getCovrmjaByComAndNum(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
		if(covrpfList.isEmpty()){
			fatalError9000();
		}
		int count = 0; 		
		while (covrpfList.size() > count) {
		Covrpf	covrpf= new Covrpf();
			covrpf = covrpfList.get(count);
			checkChdrPaidup3200(covrpf);
			count++;
		}
		if (covrpfList.size() == count){
			wsaaEndOfCovr.set("Y");
		}
		/*while ( !(endOfCovr.isTrue())) {
			checkChdrPaidup3200();
		}*/
//ILIFE-7857 end 
		if (validStatuz.isTrue()) {
			int j = 0;
				for(int i = 0; i < covrpfList.size(); i++) {
					if("LA".equals(covrpfList.get(i).getPstatcode()) && "1".equals(covrpfList.get(i).getValidflag())) {
						j++;
					}
				}
				if(covrpfList.size() == j) {
					wsaaChdrLapsed.set("Y");
				}
				
				else if(isEQ(wsaaValidStatuz,"Y"))
						{
					wsaaChdrLapsed.set("Y");
						}			
				else {
					wsaaChdrLapsed.set("N");
				}
		
		}
		updateChdr3400();
		if(adjuReasrFlag && laptpf!=null) {
			adjustReasurance();
		}
		writeLetter3800();
		sendLapseSMSNotification();
		if (flexiblePremium.isTrue()) {
			updateFprm3700();
		}
		/*                                                         <D9604>*/
		batchHeader3500();
		dryProcessing6000();
		releaseSoftlock3600();
	}

protected void adjustReasurance() {
	
	List<Covrpf> covrpfList1 = covrpfDAO.getCovrmjaByComAndNum(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
	for(Covrpf c:covrpfList1) {
		if(isGT(c.getCoverage(),laptpf.getCoverage()) && c.getStatcode().equals("IF") && isGT(c.getSumins(), 0)) {
			Racdpf r = new Racdpf();
			r.setChdrcoy(c.getChdrcoy());
			r.setChdrnum(c.getChdrnum());
			r.setLife(c.getLife());
			r.setCoverage(c.getCoverage());
			r.setRider(c.getRider());
			r.setPlanSuffix(c.getPlanSuffix());
			r.setRetype("F");
			int cnt = racdDAO.getRacdCntByType(r);
			if(cnt==0) {
				continue;
			}
			csncalcrec.csncalcRec.set(SPACES);
			csncalcrec.function.set("ADJU");
			csncalcrec.chdrcoy.set(atmodrec.company);
			csncalcrec.chdrnum.set(wsaaPrimaryChdrnum);
			csncalcrec.cnttype.set(chdrmjaIO.getCnttype());
			csncalcrec.currency.set(chdrmjaIO.getCntcurr());
			csncalcrec.fsuco.set(wsaaFsuco);
			csncalcrec.language.set(atmodrec.language);
			csncalcrec.incrAmt.set(ZERO);
			csncalcrec.effdate.set(chdrmjaIO.getPtdate());
			csncalcrec.tranno.set(chdrmjaIO.getTranno());
			csncalcrec.planSuffix.set(ZERO);
			csncalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
			csncalcrec.life.set(c.getLife());
			csncalcrec.coverage.set(c.getCoverage());
			csncalcrec.rider.set(c.getRider());
			csncalcrec.planSuffix.set(c.getPlanSuffix());
			callProgram(Csncalc.class, csncalcrec.csncalcRec);
			if (isNE(csncalcrec.statuz, Varcom.oK)
			&& isNE(csncalcrec.statuz, "FACL")) {
				syserrrec.statuz.set(csncalcrec.statuz);
				syserrrec.params.set(csncalcrec.csncalcRec);
				fatalError9000();
			}
			
			activateReassurer(c);
			updateCovr(c); //PINNACLE-3048
		}
	}
	
}

//PINNACLE-3048
protected void updateCovr(Covrpf covrpf) {
	covrpf.setValidflag("2");
	covrpf.setCurrto(chdrmjaIO.getBtdate().toInt()-1);
	covrpfDAO.updateCovrValidAndCurrtoFlag(covrpf);
	List<Covrpf> covrInsertList = new LinkedList<Covrpf>();
	covrpf.setTranno(chdrmjaIO.getTranno().toInt());
	covrpf.setValidflag("1");
	covrpf.setCurrfrom(chdrmjaIO.getBtdate().toInt());
	covrpf.setCurrto(varcom.vrcmMaxDate.toInt());
	covrInsertList.add(covrpf);
	covrpfDAO.insertCovrRecord(covrInsertList);
}

protected void activateReassurer(Covrpf c) {
	Actvresrec actvresrec = new Actvresrec();
		
		readLife4100(c);
		actvresrec.actvresRec.set(SPACES);
		actvresrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		actvresrec.chdrnum.set(chdrmjaIO.getChdrnum());
		actvresrec.cnttype.set(chdrmjaIO.getCnttype());
		actvresrec.currency.set(chdrmjaIO.getCntcurr());
		actvresrec.tranno.set(chdrmjaIO.getTranno());
		actvresrec.life.set(c.getLife());
		actvresrec.coverage.set(c.getCoverage());
		actvresrec.rider.set(c.getRider());
		actvresrec.planSuffix.set(ZERO);
		actvresrec.crtable.set(c.getCrtable());
		actvresrec.effdate.set(csncalcrec.effdate);
		actvresrec.clntcoy.set(csncalcrec.fsuco);
		actvresrec.l1Clntnum.set(wsaaL1Clntnum);
		actvresrec.jlife.set(c.getJlife());
		if (isNE(wsaaL2Clntnum,SPACES)) {
			actvresrec.l2Clntnum.set(wsaaL2Clntnum);
		}
		actvresrec.oldSumins.set(ZERO);
		actvresrec.newSumins.set(c.getSumins());
		actvresrec.crrcd.set(c.getCrrcd());
		actvresrec.language.set(csncalcrec.language);
		actvresrec.function.set("ADJU");
		actvresrec.batctrcde.set(csncalcrec.batctrcde);
		callProgram(Actvres.class, actvresrec.actvresRec);
		if (isNE(actvresrec.statuz,Varcom.oK)) {
			syserrrec.statuz.set(actvresrec.statuz);
			syserrrec.params.set(actvresrec.actvresRec);
			fatalError9000();
		}
	
}

protected void readLife4100(Covrpf c) {
	Lifepf lifepf = new Lifepf();
	lifepf.setChdrcoy(c.getChdrcoy());
	lifepf.setChdrnum(c.getChdrnum());
	lifepf.setLife(c.getLife());
	lifepf.setJlife("00");
	lifepf = lifepfDAO.getLifepf(lifepf);
	if(lifepf==null){
		syserrrec.params.set(c.getChdrnum());
		fatalError9000();
	}
	wsaaL1Clntnum.set(lifepf.getLifcnum());
	List<Lifepf> lifepfList = lifepfDAO.getLifeData(c.getChdrcoy(),c.getChdrnum(), c.getLife(), "01");
	if (!(lifepfList.isEmpty())){
		wsaaL2Clntnum.set(lifepfList.get(0).getLifcnum());
	} else {
		wsaaL2Clntnum.set(SPACES);
	}
}

protected void writePtrn3100()
	{
		start3110();
	}

protected void start3110()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		if(lapseLetterFlag) {//IJS-386
		 ptrnIO.setPtrneff(chdrmjaIO.getPtdate());		
		}else {
		 ptrnIO.setPtrneff(wsaaBusinessDate);
		}
		ptrnIO.setDatesub(wsaaBusinessDate);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setValidflag("1");
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			fatalError9000();
		}
	}

protected void checkChdrPaidup3200(Covrpf covrpf)
	{
		/*try {
			start3210();
	//		checkStatuz3250();//ILIFE-7857
		}
		catch (GOTOException e){
			 Expected exception for control flow purposes. 
		}*/
	
	try {
		if (isNE(covrpf.getRider(), "00")) {
			checkRiderStatuz3350(covrpf);
		}
		else {
			checkCoverageStatuz3300(covrpf);
		}	
		
		if (validStatuz.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaEndOfCovr.set("Y");
			return ;
		}			

	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
	}
	}
//ILIFE-7857 start 
protected void start3210()
	{
	
	/*	covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		covrmjaIO.setFormat(formatsInner.covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.endp)
		|| isNE(chdrmjaIO.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), covrmjaIO.getChdrnum())) {
			wsaaEndOfCovr.set("Y");
			goTo(GotoLabel.exit3290);
		}*/
		
	}
//ILIFE-7857 end
protected void checkStatuz3250()
//ILIFE-7857 start
	{/*
		if (isNE(covrmjaIO.getRider(), "00")) {
			checkRiderStatuz3350();
		}
		else {
			checkCoverageStatuz3300();
		}
		if (validStatuz.isTrue()) {
			NEXT_SENTENCE
		}
		else {
			wsaaEndOfCovr.set("Y");
			return ;
		}
		
		covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.endp)
		|| isNE(chdrmjaIO.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(), covrmjaIO.getChdrnum())) {
			wsaaEndOfCovr.set("Y");
		}
		else {
			checkStatuz3250();
			return ;
		}
	*/}

protected void checkCoverageStatuz3300(Covrpf covrpf)
	{
		/*START*/
		/*if(isEQ(covrpf.getValidflag(),2)){
			wsaaEndOfCovr.set("Y");
			return;
		}
		else*/
		if ((isNE(t5679rec.setCovRiskStat, SPACES))
		&& (isEQ(t5679rec.setCovRiskStat, covrpf.getStatcode()))) {
			if ((isNE(t5679rec.setCovPremStat, SPACES))
			&& (isEQ(t5679rec.setCovPremStat, covrpf.getPstatcode()))) {
				wsaaValidStatuz.set("Y");
				covrlapse=true; // //ILIFE-8601 
			//	return;
			}
		}
		else {
					wsaaValidStatuz.set("N");
		}
		}
		/*EXIT*/
/*	}*/

protected void checkRiderStatuz3350(Covrpf covrpf)
	{
		/*START*/
		if ((isNE(t5679rec.setRidRiskStat, SPACES))
		&& (isEQ(t5679rec.setRidRiskStat, covrpf.getStatcode()))) {
			if ((isNE(t5679rec.setRidPremStat, SPACES))
			&& (isEQ(t5679rec.setRidPremStat, covrpf.getPstatcode()))) {
				wsaaValidStatuz.set("Y");
			}
		}
		else if(covrlapse){
			wsaaValidStatuz.set("Y");	//ILIFE-8601 
		}				
		else {
			wsaaValidStatuz.set("N");
		}
		/*EXIT*/
		//ILIFE-7857 end
	}

protected void updateChdr3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3410();
					updatePayr3415();
				case writeNewContractHeader3420:
					writeNewContractHeader3420();
				case writeRecord3430:
					writeRecord3430();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3410()
	{
		/*  Since a READH of the CHDR is done at this point the TRANNO*/
		/*  at this point will be correct and will not be advanced by*/
		/*  1.*/
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFunction(varcom.readh);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
		/*  Rewrite the original contract header for historical purposes.*/
		chdrmjaIO.setValidflag("2");
		/* Write the CURRTO and CURRFROM dates for valid flag '2'*/
		/* records on the CHDR File*/
		/*  MOVE WSAA-BUSINESS-DATE     TO CHDRMJA-CURRTO.               */
		/*  IF WSAA-OCCDATE             > WSAA-CURRFROM                  */
		/*     MOVE WSAA-OCCDATE        TO CHDRMJA-CURRFROM              */
		/*  ELSE                                                         */
		/*     MOVE WSAA-CURRFROM       TO CHDRMJA-CURRFROM.             */
		chdrmjaIO.setCurrto(chdrmjaIO.getBtdate());
		chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
	}

	/**
	* <pre>
	**   IF CHDRMJA-BILLFREQ = '00' OR '  '
	**      GO TO 3420-WRITE-NEW-CONTRACT-HEADER.
	* Update current PAYR record with valid flag = '2' and write a
	* new PAYR record with valid flag = '1' and effective date is
	* the old BTDATE.
	* </pre>
	*/
protected void updatePayr3415()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError9000();
		}
		/* Rewrite the Current PAYR record as Valid flag 2.*/
		payrIO.setValidflag("2");
		/* MOVE CHDRMJA-CURRFROM       TO PAYR-EFFDATE.         <D9703> */
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError9000();
		}
		
		/* IBPLIFE-4293  Starts */
		setPrecision(chdrmjaIO.getSinstamt01(), 2);
		chdrmjaIO.setSinstamt01(sub(chdrmjaIO.getSinstamt01(), wsaaTotCovrpuInstprem));
		setPrecision(payrIO.getSinstamt01(), 2);
		payrIO.setSinstamt01(sub(payrIO.getSinstamt01(), wsaaTotCovrpuInstprem));
		setPrecision(chdrmjaIO.getSinstamt06(), 2);
		chdrmjaIO.setSinstamt06(add(add(add(add(chdrmjaIO.getSinstamt01(), chdrmjaIO.getSinstamt02()), chdrmjaIO.getSinstamt03()), chdrmjaIO.getSinstamt04()), chdrmjaIO.getSinstamt05()));
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()), payrIO.getSinstamt04()), payrIO.getSinstamt05()));
		
		/* IBPLIFE-4293  Ends */
	
		
		if (chdrLapsed.isTrue()) {
			/*       MOVE ZERO                 TO CHDRMJA-SINSTAMT01           */
			/*                                    CHDRMJA-SINSTAMT02           */
			/*                                    CHDRMJA-SINSTAMT03           */
			/*                                    CHDRMJA-SINSTAMT04           */
			/*                                    CHDRMJA-SINSTAMT05           */
			/*                                    CHDRMJA-SINSTAMT06           */
			/*       MOVE ZEROS                TO PAYR-SINSTAMT01      <LA5030>*/
			/*                                    PAYR-SINSTAMT02      <LA5030>*/
			/*                                    PAYR-SINSTAMT03      <LA5030>*/
			/*                                    PAYR-SINSTAMT04      <LA5030>*/
			/*                                    PAYR-SINSTAMT05      <LA5030>*/
			/*                                    PAYR-SINSTAMT06      <LA5030>*/
			goTo(GotoLabel.writeNewContractHeader3420);
		}
	}

	/**
	* <pre>
	***  MOVE 0                      TO CHDRMJA-INSTTOT01
	***                                 CHDRMJA-INSTTOT02
	***                                 CHDRMJA-INSTTOT03
	***                                 CHDRMJA-INSTTOT04
	***                                 CHDRMJA-INSTTOT05
	***                                 CHDRMJA-INSTTOT06.
	* </pre>
	*/
protected void writeNewContractHeader3420() 
	{
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		chdrmjaIO.setValidflag("1");
		/* Write the CURRTO and CURRFROM dates for valid flag '1'*/
		/* records on the CHDR File*/
		/*  MOVE 99999999               TO CHDRMJA-CURRTO.               */
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		/*  MOVE 1                      TO DTC2-FREQ-FACTOR.             */
		/*  MOVE 'DY'                   TO DTC2-FREQUENCY.               */
		/*  MOVE WSAA-BUSINESS-DATE     TO DTC2-INT-DATE-1.              */
		/*  CALL 'DATCON2'              USING DTC2-DATCON2-REC.          */
		/*  IF DTC2-STATUZ               NOT = O-K                       */
		/*     MOVE DTC2-DATCON2-REC     TO SYSR-PARAMS                  */
		/*     MOVE DTC2-STATUZ          TO SYSR-STATUZ                  */
		/*     PERFORM 9000-FATAL-ERROR.                                 */
		/*  MOVE DTC2-INT-DATE-2        TO CHDRMJA-CURRFROM.             */
		chdrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		payrIO.setTranno(chdrmjaIO.getTranno());
		if(lapseLetterFlag) {
			payrIO.setEffdate(chdrmjaIO.getPtdate());
		}else {
		    payrIO.setEffdate(chdrmjaIO.getCurrfrom());
		}
		if(isEQ(mainRider,0) && wsaaChdrLapsed.toString().equals("Y")) {// ILIFE-7863
		//ILIFE-7769
		if (isNE(t5679rec.setCnPremStat, SPACES)) {
			chdrmjaIO.setPstatcode(t5679rec.setCnPremStat);
		}
		if (isNE(t5679rec.setCnRiskStat, SPACES)) {
			chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		}
		//end
		}
		if (chdrLapsed.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.writeRecord3430);
		}
		/*if (isNE(t5679rec.setCnPremStat, SPACES)) {
			chdrmjaIO.setPstatcode(t5679rec.setCnPremStat);
		}
		if (isNE(t5679rec.setCnRiskStat, SPACES)) {
			chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		}commented for ILIFE-7769*/
	}

protected void writeRecord3430()
	{
		chdrmjaIO.setFunction(varcom.writr);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
		/* Write a new PAYR record with valid flag = '1'.                  */
		payrIO.setValidflag("1");
		payrIO.setPstatcode(chdrmjaIO.getPstatcode());
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError9000();
		}
	}

protected void batchHeader3500()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void releaseSoftlock3600()
	{
		start3610();
	}

protected void start3610()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError9000();
		}
	}

protected void updateFprm3700()
	{
		start3710();
	}

	/**
	* <pre>
	***************************                               <D9604>
	* </pre>
	*/
protected void start3710()
	{
		/*                                                         <D9604> */
		/* Only update the FPRM if any of the FPCO's have been rewr<D9604> */
		/* ie if only a benefit billed rider lapsed, the FPRM will <D9604> */
		/* updated                                                 <D9604>*/
		/*                                                         <D9604>*/
		if (!fpcoUpdated.isTrue()) {
			return ;
		}
		fprmIO.setParams(SPACES);
		fprmIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		fprmIO.setChdrnum(chdrmjaIO.getChdrnum());
		fprmIO.setPayrseqno(1);
		fprmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			fatalError9000();
		}
		fprmIO.setValidflag("2");
		fprmIO.setCurrto(wsaaBusinessDate);
		fprmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			fatalError9000();
		}
		fprmIO.setCurrfrom(wsaaBusinessDate);
		if (chdrLapsed.isTrue()) {
			fprmIO.setCurrto(wsaaBusinessDate);
		}
		else {
			fprmIO.setCurrto(varcom.vrcmMaxDate);
		}
		fprmIO.setValidflag("1");
		setPrecision(fprmIO.getTotalRecd(), 2);
		fprmIO.setTotalRecd(sub(fprmIO.getTotalRecd(), wsaaTotPremRecd));
		setPrecision(fprmIO.getTotalBilled(), 2);
		fprmIO.setTotalBilled(sub(fprmIO.getTotalBilled(), wsaaTotBilled));
		setPrecision(fprmIO.getMinPrmReqd(), 2);
		fprmIO.setMinPrmReqd(sub(fprmIO.getMinPrmReqd(), wsaaTotOvrdMin));
		fprmIO.setTranno(chdrmjaIO.getTranno());
		fprmIO.setFunction(varcom.writr);
		fprmIO.setFormat(formatsInner.fprmrec);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			fatalError9000();
		}
	}

protected void writeLetter3800()
	{
		try {
			readT66343801();
			setUpParm3803();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void sendLapseSMSNotification() {

	}

protected void readT66343801()
	{
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		/* MOVE WSAA-ITEM-T6634             TO ITEM-ITEMITEM.   <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			readT6634Again3810();
		}
		else {
			/*     MOVE ITEM-GENAREA            TO T6634-T6634-REC  <PCPPRT>*/
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
		/* IF   T6634-LETTER-TYPE          = SPACE              <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			goTo(GotoLabel.exit3809);
		}
	}

protected void setUpParm3803()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaBusinessDate);
		/* MOVE 'EN'                   TO LETRQST-RDOCPFX.      <PCPPRT>*/
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(atmodrec.company);
		/* MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.      <PCPPRT>*/
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		letrqstrec.otherKeys.set(SPACES);
		/* MOVE ATMD-LANGUAGE          TO LETRQST-OTHER-KEYS.   <PCPPRT>*/
		letrqstrec.otherKeys.set(wsaaOkeys);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.function.set("ADD");
		/* CALL 'HLETRQS' USING LETRQST-PARAMS.                 <PCPPRT>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError9000();
		}
	}

protected void readT6634Again3810()
	{
		start3811();
	}

protected void start3811()
	{
		wsaaItemCnttype.set("***");
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		/* MOVE WSAA-ITEM-T6634             TO ITEM-ITEMITEM.   <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
	}

protected void readT56794000()
	{
		start4010();
	}

protected void start4010()
	{
		/*    Read the Component Status table*/
	
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}
//ILIFE-7857 start
protected void lapseMethod4000(Covrpf covrpf)
	{
		para4010(covrpf);
	}

protected void para4010(Covrpf covrpf)
	{
		/* Read T5687 for non-forfieture method.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItmfrm(covrpf.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(covrpf.getCrtable(), itdmIO.getItemitem())
		|| isNE(atmodrec.company, itdmIO.getItemcoy())) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	
		
		/* Read T6597 for non-forfieture subroutine.*/
		if (isEQ(t5687rec.nonForfeitMethod, SPACES)) {
			return ;
		}
		
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t6597);
		itdmIO.setItemitem(t5687rec.nonForfeitMethod);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItmfrm(covrpf.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemtabl(), t6597)
		|| isNE(t5687rec.nonForfeitMethod, itdmIO.getItemitem())
		|| isNE(atmodrec.company, itdmIO.getItemcoy())) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		t6597rec.t6597Rec.set(itdmIO.getGenarea());
		if (isEQ(t6597rec.premsubr04, SPACES)) {
			return ;
		}
		ovrduerec.ovrdueRec.set(SPACES);
		/* MOVE ATMD-COMPANY           TO OVRD-COMPANY.                 */
		ovrduerec.language.set(atmodrec.language);
		ovrduerec.trancode.set(wsaaBatckey.batcBatctrcde);
		ovrduerec.acctyear.set(wsaaBatckey.batcBatcactyr);
		ovrduerec.acctmonth.set(wsaaBatckey.batcBatcactmn);
		ovrduerec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ovrduerec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		ovrduerec.user.set(wsaaUser);
		ovrduerec.tranDate.set(wsaaTransactionDate);
		ovrduerec.tranTime.set(wsaaTransactionTime);
		ovrduerec.chdrnum.set(chdrmjaIO.getChdrnum());
		ovrduerec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		ovrduerec.company.set(chdrmjaIO.getCowncoy());
		ovrduerec.cntcurr.set(chdrmjaIO.getCntcurr());
		ovrduerec.effdate.set(chdrmjaIO.getCcdate());
		ovrduerec.outstamt.set(chdrmjaIO.getOutstamt());
		ovrduerec.btdate.set(chdrmjaIO.getBtdate());
		ovrduerec.ptdate.set(chdrmjaIO.getPtdate());
		ovrduerec.agntnum.set(chdrmjaIO.getAgntnum());
		ovrduerec.cownnum.set(chdrmjaIO.getCownnum());
		ovrduerec.tranno.set(chdrmjaIO.getTranno());
		ovrduerec.ovrdueDays.set(ZERO);
		ovrduerec.life.set(covrpf.getLife());
		ovrduerec.coverage.set(covrpf.getCoverage());
		ovrduerec.rider.set(covrpf.getRider());
		ovrduerec.planSuffix.set(covrpf.getPlanSuffix());
		ovrduerec.crtable.set(covrpf.getCrtable());
		ovrduerec.pumeth.set(SPACES);
		ovrduerec.billfreq.set(chdrmjaIO.getBillfreq());
		ovrduerec.instprem.set(covrpf.getInstprem());
		ovrduerec.sumins.set(covrpf.getSumins());
		ovrduerec.crrcd.set(covrpf.getCrrcd());
		ovrduerec.premCessDate.set(covrpf.getPremCessDate());
		ovrduerec.pupfee.set(ZERO);
		ovrduerec.cnttype.set(chdrmjaIO.getCnttype());
		ovrduerec.occdate.set(chdrmjaIO.getOccdate());
		ovrduerec.aloind.set(SPACES);
		ovrduerec.efdcode.set(SPACES);
		ovrduerec.procSeqNo.set(ZERO);
		ovrduerec.description.set(SPACES);
		ovrduerec.polsum.set(ZERO);
		ovrduerec.actualVal.set(ZERO);
		ovrduerec.newRiskCessDate.set(varcom.vrcmMaxDate);
		ovrduerec.newPremCessDate.set(varcom.vrcmMaxDate);
		ovrduerec.newRerateDate.set(varcom.vrcmMaxDate);
		ovrduerec.runDate.set(wsaaBusinessDate);
		callProgram(t6597rec.premsubr04, ovrduerec.ovrdueRec);
		if (isEQ(ovrduerec.statuz, varcom.oK)) {
			if (isNE(ovrduerec.newRiskCessDate, varcom.vrcmMaxDate)
			&& isNE(ovrduerec.newRiskCessDate, ZERO)) {
				covrpf.setRiskCessDate(ovrduerec.newRiskCessDate.toInt());
			}
			if (isNE(ovrduerec.newPremCessDate, varcom.vrcmMaxDate)
			&& isNE(ovrduerec.newPremCessDate, ZERO)) {
				covrpf.setPremCessDate(ovrduerec.newPremCessDate.toInt());
			}
			if (isNE(t6597rec.cpstat04, SPACES)) {
				if (isNE(t6597rec.crstat04, SPACES)) {
					covrpf.setPstatcode(t6597rec.cpstat04.toString());
					covrpf.setStatcode(t6597rec.crstat04.toString());
				}
				//ILIFE-7857 end
			}
		}
		else {
			syserrrec.statuz.set(ovrduerec.statuz);
			syserrrec.params.set(ovrduerec.ovrdueRec);
			fatalError9000();
		}
	}

protected void incrCheck5000()
	{
		para5010();
	}

protected void para5010()
	{
		/* Instead of updating the INCR record in situ, write a new     */
		/* INCR record for this transaction to allow reversals to       */
		/* reinstate the previous record.                               */
		incrIO.setDataArea(SPACES);
		incrIO.setChdrcoy(laptIO.getChdrcoy());
		incrIO.setChdrnum(laptIO.getChdrnum());
		incrIO.setLife(laptIO.getLife());
		incrIO.setCoverage(laptIO.getCoverage());
		incrIO.setRider(laptIO.getRider());
		incrIO.setPlanSuffix(laptIO.getPlanSuffix());
		/* MOVE READH                  TO INCR-FUNCTION.        <A06275>*/
		incrIO.setFunction(varcom.readr);
		incrIO.setFormat(formatsInner.incrrec);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), varcom.oK)
		&& isNE(incrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(incrIO.getParams());
			fatalError9000();
		}
		if (isEQ(incrIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		incrIO.setValidflag("2");
		if (isNE(t5679rec.setCovPremStat, SPACES)) {
			incrIO.setPstatcode(t5679rec.setCovPremStat);
		}
		if (isNE(t5679rec.setCovRiskStat, SPACES)) {
			incrIO.setStatcode(t5679rec.setCovRiskStat);
		}
		/* MOVE REWRT                  TO INCR-FUNCTION.        <A06275>*/
		incrIO.setTransactionDate(wsaaTransactionDate);
		incrIO.setTransactionTime(wsaaTransactionTime);
		incrIO.setUser(wsaaUser);
		incrIO.setTermid(wsaaTermid);
		incrIO.setTranno(chdrmjaIO.getTranno());
		incrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(incrIO.getParams());
			fatalError9000();
		}
	}

protected void dryProcessing6000()
	{
		start6010();
	}

protected void start6010()
	{
		/* This section will determine if the DIARY system is active       */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75086100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75086100();
		}
		/* If item not found no Batch Diary Processing is Required.        */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		if(lapseLetterFlag) {//IJS-386
		  drypDryprcRecInner.drypEffectiveDate.set(chdrmjaIO.getPtdate());		
		}else {
		  drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		}
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(covrpf.getCpiDate()); //ILIFE-7857
		drypDryprcRecInner.drypBbldate.set(covrpf.getBenBillDate()); //ILIFE-7857
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrmjaIO.getStatementDate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
	//	noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);//modified for ILIFE-7945
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError9000();
		}
	}

protected void readT75086100()
	{
		start6110();
	}

protected void start6110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void fatalError9000()
	{
		start9010();
		errorProg9050();
	}

protected void start9010()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg9050()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(chdrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError9000();
		}
	}

protected void a100DelPolUnderwritting()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrmjaIO.getChdrcoy());
		crtundwrec.currcode.set(chdrmjaIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrmjaIO.getChdrnum());
		crtundwrec.cnttyp.set(chdrmjaIO.getCnttype());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError9000();
		}
	}
//ILIFE-7857 start
//protected void a200DelCovUnderwritting(Covrpf covrpf) commented for ILIFE-7945
protected void a200DelCovUnderwritting(Covrpf covrpf) //ILIFE-8157
	{
		//a200Ctrl();
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrmjaIO.getChdrcoy());
		crtundwrec.currcode.set(chdrmjaIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrmjaIO.getChdrnum());
		crtundwrec.cnttyp.set(chdrmjaIO.getCnttype());
		crtundwrec.crtable.set(covrpf.getCrtable());
		crtundwrec.life.set(covrpf.getLife());
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError9000();
		}
	}


protected void a200Ctrl()                               
	{
		/*initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrmjaIO.getChdrcoy());
		crtundwrec.currcode.set(chdrmjaIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrmjaIO.getChdrnum());
		crtundwrec.cnttyp.set(chdrmjaIO.getCnttype());
		crtundwrec.crtable.set(covrpf.getCrtable());
		crtundwrec.life.set(covrpf.getLife());
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError9000();
		}*/
		//ILIFE-7857 end
	}

protected void a300BegnLifeio()
	{
		a300Ctrl();
	}

protected void a300Ctrl()
	{
		lifeIO.setRecKeyData(SPACES);
		lifeIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifeIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifeIO.setLife(laptIO.getLife());
		/* MOVE                        TO LIFE-JLIFE                    */
		/* MOVE                        TO LIFE-CURRFROM                 */
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (!(isEQ(lifeIO.getStatuz(), varcom.oK)
		&& isEQ(chdrmjaIO.getChdrcoy(), lifeIO.getChdrcoy())
		&& isEQ(chdrmjaIO.getChdrnum(), lifeIO.getChdrnum())
		&& isEQ(laptIO.getLife(), lifeIO.getLife()))) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError9000();
		}
	}

protected void performLincProcessing() {
	Tjl47rec tjl47rec = new Tjl47rec(); 
	Itempf itempf = null;
	itempf = itempfDAO.readItemItem("IT", atmodrec.company.toString(), tjl47, wsaaBatckey.batcBatctrcde.toString());
	if(itempf != null) {
		tjl47rec.tjl47Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		LincCSService lincService = (LincCSService) getApplicationContext().getBean(tjl47rec.lincsubr.toString().toLowerCase());
		lincService.generalProcessing(setLincpfHelper(), tjl47rec);
	}
}

protected LincpfHelper setLincpfHelper() {
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	LincpfHelper lincpfHelper = new LincpfHelper();
	lincpfHelper.setChdrcoy(atmodrec.company.toString());
	lincpfHelper.setChdrnum(wsaaPrimaryChdrnum.toString());
	lincpfHelper.setOwncoy(chdrmjaIO.getCowncoy().toString());
	lincpfHelper.setClntnum(chdrmjaIO.getCownnum().toString());
	lincpfHelper.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
	lincpfHelper.setCownnum(chdrmjaIO.getCownnum().toString());
	lincpfHelper.setCnttype(chdrmjaIO.getCnttype().toString());
	lincpfHelper.setOccdate(chdrmjaIO.getOccdate().toInt());
	lincpfHelper.setTransactionDate(datcon1rec.intDate.toInt());
	lincpfHelper.setTranscd(wsaaBatckey.batcBatctrcde.toString());
	lincpfHelper.setLanguage(atmodrec.language.toString());
	lincpfHelper.setTranno(chdrmjaIO.getTranno().toInt());
	if(lapseLetterFlag) {
	 lincpfHelper.setPtrnEff(chdrmjaIO.getPtdate().toInt());	
	}else {
	 lincpfHelper.setPtrnEff(datcon1rec.intDate.toInt());
	}
	lincpfHelper.setTermId(varcom.vrcmTermid.toString());
	lincpfHelper.setTrdt(varcom.vrcmDate.toInt());
	lincpfHelper.setTrtm(varcom.vrcmTime.toInt());
	lincpfHelper.setUsrprf(varcom.vrcmUser.toString());
	lincpfHelper.setJobnm(wsaaUser.toString());
	lincpfHelper = setClientDetails(lincpfHelper);
	lincpfHelper = getLifeDetails(lincpfHelper);
	return lincpfHelper;
}

protected LincpfHelper getLifeDetails(LincpfHelper lincpfHelper) {
	Clntpf clntpf = new Clntpf();
	clntpf.setClntpfx("CN");
	clntpf.setClntcoy(lincpfHelper.getOwncoy());
	clntpf.setClntnum(lifeIO.getLifcnum().toString());
	clntpf = clntpfDAO.selectClient(clntpf);
	lincpfHelper.setLifcnum(lifeIO.getLifcnum().toString());
	lincpfHelper.setLcltdob(lifeIO.getCltdob().toInt());
	lincpfHelper.setLcltsex(lifeIO.getCltsex().toString());
	lincpfHelper.setLzkanasnm(clntpf.getZkanasnm());
	lincpfHelper.setLzkanagnm(clntpf.getZkanagnm());
	lincpfHelper.setLzkanaddr01(clntpf.getZkanaddr01());
	lincpfHelper.setLzkanaddr02(clntpf.getZkanaddr02());
	lincpfHelper.setLzkanaddr03(clntpf.getZkanaddr03());
	lincpfHelper.setLzkanaddr04(clntpf.getZkanaddr04());
	lincpfHelper.setLzkanaddr05(clntpf.getZkanaddr05());
	lincpfHelper.setLgivname(clntpf.getGivname());
	lincpfHelper.setLsurname(clntpf.getSurname());
	return lincpfHelper;
}

protected LincpfHelper setClientDetails(LincpfHelper lincpfHelper) {
	Clntpf clntpf = new Clntpf();
	clntpf.setClntpfx("CN");
	clntpf.setClntcoy(lincpfHelper.getOwncoy());
	clntpf.setClntnum(lincpfHelper.getClntnum());
	clntpf = clntpfDAO.selectClient(clntpf);
	lincpfHelper.setZkanasnm(clntpf.getZkanasnm());
	lincpfHelper.setZkanagnm(clntpf.getZkanagnm());
	lincpfHelper.setZkanaddr01(clntpf.getZkanaddr01());
	lincpfHelper.setZkanaddr02(clntpf.getZkanaddr02());
	lincpfHelper.setZkanaddr03(clntpf.getZkanaddr03());
	lincpfHelper.setZkanaddr04(clntpf.getZkanaddr04());
	lincpfHelper.setZkanaddr05(clntpf.getZkanaddr05());
	lincpfHelper.setGivname(clntpf.getGivname());
	lincpfHelper.setSurname(clntpf.getSurname());
	lincpfHelper.setCltsex(clntpf.getCltsex());
	lincpfHelper.setCltdob(clntpf.getCltdob());
	lincpfHelper.setClttype(clntpf.getClttype());
	return lincpfHelper;
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData laptrec = new FixedLengthStringData(10).init("LAPTREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData incrrec = new FixedLengthStringData(10).init("INCRREC");
	private FixedLengthStringData laptlaprec = new FixedLengthStringData(10).init("LAPTLAPREC");
	private FixedLengthStringData fpcorec = new FixedLengthStringData(10).init("FPCOREC");
	private FixedLengthStringData fpcolf1rec = new FixedLengthStringData(10).init("FPCOLF1REC");
	private FixedLengthStringData fprmrec = new FixedLengthStringData(10).init("FPRMREC");
	private FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
	
}
}
