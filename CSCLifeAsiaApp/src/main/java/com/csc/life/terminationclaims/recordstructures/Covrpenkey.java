package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:24
 * Description:
 * Copybook name: COVRPENKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrpenkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrpenFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrpenKey = new FixedLengthStringData(64).isAPartOf(covrpenFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrpenChdrcoy = new FixedLengthStringData(1).isAPartOf(covrpenKey, 0);
  	public FixedLengthStringData covrpenChdrnum = new FixedLengthStringData(8).isAPartOf(covrpenKey, 1);
  	public FixedLengthStringData covrpenLife = new FixedLengthStringData(2).isAPartOf(covrpenKey, 9);
  	public FixedLengthStringData covrpenCoverage = new FixedLengthStringData(2).isAPartOf(covrpenKey, 11);
  	public FixedLengthStringData covrpenRider = new FixedLengthStringData(2).isAPartOf(covrpenKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(covrpenKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrpenFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrpenFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}