package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh580screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 17, 22, 5, 18, 23, 24, 15, 16, 1, 2, 3, 21, 20}; 
	public static int maxRecords = 6;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {14}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 16, 5, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh580ScreenVars sv = (Sh580ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sh580screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sh580screensfl, 
			sv.Sh580screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sh580ScreenVars sv = (Sh580ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sh580screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sh580ScreenVars sv = (Sh580ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sh580screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sh580screensflWritten.gt(0))
		{
			sv.sh580screensfl.setCurrentIndex(0);
			sv.Sh580screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sh580ScreenVars sv = (Sh580ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sh580screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sh580ScreenVars screenVars = (Sh580ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.hemv.setFieldName("hemv");
				screenVars.hactval.setFieldName("hactval");
				screenVars.fieldType.setFieldName("fieldType");
				screenVars.descrip.setFieldName("descrip");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.actvalue.setFieldName("actvalue");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.estMatValue.set(dm.getField("estMatValue"));
			screenVars.hemv.set(dm.getField("hemv"));
			screenVars.hactval.set(dm.getField("hactval"));
			screenVars.fieldType.set(dm.getField("fieldType"));
			screenVars.descrip.set(dm.getField("descrip"));
			screenVars.cnstcur.set(dm.getField("cnstcur"));
			screenVars.actvalue.set(dm.getField("actvalue"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sh580ScreenVars screenVars = (Sh580ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.hemv.setFieldName("hemv");
				screenVars.hactval.setFieldName("hactval");
				screenVars.fieldType.setFieldName("fieldType");
				screenVars.descrip.setFieldName("descrip");
				screenVars.cnstcur.setFieldName("cnstcur");
				screenVars.actvalue.setFieldName("actvalue");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("estMatValue").set(screenVars.estMatValue);
			dm.getField("hemv").set(screenVars.hemv);
			dm.getField("hactval").set(screenVars.hactval);
			dm.getField("fieldType").set(screenVars.fieldType);
			dm.getField("descrip").set(screenVars.descrip);
			dm.getField("cnstcur").set(screenVars.cnstcur);
			dm.getField("actvalue").set(screenVars.actvalue);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sh580screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sh580ScreenVars screenVars = (Sh580ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.estMatValue.clearFormatting();
		screenVars.hemv.clearFormatting();
		screenVars.hactval.clearFormatting();
		screenVars.fieldType.clearFormatting();
		screenVars.descrip.clearFormatting();
		screenVars.cnstcur.clearFormatting();
		screenVars.actvalue.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sh580ScreenVars screenVars = (Sh580ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.estMatValue.setClassString("");
		screenVars.hemv.setClassString("");
		screenVars.hactval.setClassString("");
		screenVars.fieldType.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.cnstcur.setClassString("");
		screenVars.actvalue.setClassString("");
	}

/**
 * Clear all the variables in Sh580screensfl
 */
	public static void clear(VarModel pv) {
		Sh580ScreenVars screenVars = (Sh580ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.estMatValue.clear();
		screenVars.hemv.clear();
		screenVars.hactval.clear();
		screenVars.fieldType.clear();
		screenVars.descrip.clear();
		screenVars.cnstcur.clear();
		screenVars.actvalue.clear();
	}
}
