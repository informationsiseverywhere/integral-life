/*
 * File: Unlmata.java
 * Date: 30 August 2009 2:50:37
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLMATA.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.life.interestbearing.dataaccess.HitrclmTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrtrgTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.MatdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.MatdtrgTableDAM;
import com.csc.life.terminationclaims.dataaccess.MathclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrntrgTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* UNLMATA - Unit Linked Maturity Trigger Module.
* ------------------------------------------------
*
* This program is an item entry on T6598, the claim subroutine
* method  table. This method is used in order to update the
* relevant sub-accounts. The trigger module and key were passed  with
* other details with the UTRN record.
*
* PROCESSING.
* ----------
*
* Read the UTRN record using the Trigger Module Key of the UTRN to set
* up the READR.
*
* If no UTRN is found perform the normal error handling.
*
* For the UTRN record found find the relevant MATD record using the
* MATDTRC logical view. Again normal error handling will be performed
* if no MATD record found.
*
* For the MATDTRG found if the estimated value is zero it updates the
* the MATD actual value with the UTRN fund amount and sets the
* Estimated Value to zero.
*
* This program is invoked from Unit Dealing and as such should not
* perform any postings until all UTRN's have been dealt. To ascertain
* this we need another 2 checks:-
*
* 1. The UTRN Feedback Indicator must be 'Y', denoting it has been
*    processed.
*
* 2. All MATD records have a non-zero Estimated Value.
*
* Firstly then, read the UTRN and if FDBKIND not = 'Y' set a flag
* to stop any posting.
*
* Secondly, use a new logical view MATDCLM to loop round the MATD
* records and if all Estimated Values are zero then processing can
* continue. During this time accumulate a total actual value.
*
* Once it has been decided that the Maturity can now fully process
* you must firstly decide if this contract has component level
* accounting or not.
*
* If Component Level Accounting answer on T5688 is 'Y' then another
* loop round the MATD's will be necessary posting the MATDCLM-ACTVALUE
* to an ACMV record for each MATD found. GL Information to come from
* T5645 Line 01.
*
* If NOT Component Level accounting no loop is required as the total
* Actual Value has already been accumulated. This value is to be
* posted as one entry on an ACMV. GL Information to come from T5645
* Line 02.
*
* If an adjustment was entered by the user at the time of the online
* transaction then it should be posted now. This amount is stored on
* the MATHCLM-OTHERADJST field. GL Information to come from T5645
* Line 03.
*
* The Net value is to be posted to a Pending Maturity Account. This
* requires a ACMV record to be written. GL Information to come from
* T5645 Line 04.
*
* LIFACMV linkage area is as follows:-
*
*            Function               - PSTW
*            Batch key              - AT linkage
*            Document number        - contract number
*            Transaction number     - transaction no.
*            Journal sequence no.   - Start From 1 and increment by 1
*            Sub-account code       - from applicable T5645 entry
*            Sub-account type       - ditto
*            Sub-account GL map     - ditto
*            Sub-account GL sign    - ditto
*            S/acct control total   - ditto
*            Cmpy code (sub ledger) - batch company
*            Cmpy code (GL)         - batch company
*            Subsidiary ledger      - contract number
*            Original currency code - currency payable in
*            Original currency amt  - claim amount
*            Accounting curr code   - blank (handled by subroutine)
*            Accounting curr amount - zero (handled by subroutine)
*            Exchange rate          - zero (handled by subroutine)
*            Trans reference        - contract transaction number
*            Trans description      - from transaction code description
*            Posting month and year - defaulted
*            Effective date         - Maturity Claim effective-date
*            Reconciliation amount  - zero
*            Reconciliation date    - Max date
*            Transaction Id         - AT linkage
*            Substitution code 1    - contract type
*
* The LIFRTRN linkage is the same as the LIFACMV linkage.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Include processing for interest bearing funds.                      *
*                                                                     *
*****************************************************************
* </pre>
*/
public class Unlmata extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLMATA";
		/* ERRORS */
	private static final String e308 = "E308";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String utrnrec = "UTRNREC";
	private static final String utrntrgrec = "UTRNTRGREC";
	private static final String matdclmrec = "MATDCLMREC";
	private static final String matdtrgrec = "MATDTRGREC";
	private static final String mathclmrec = "MATHCLMREC";
	private static final String chdrmatrec = "CHDRMATREC";
	private PackedDecimalData wsaaActualVal = new PackedDecimalData(18, 3).init(0);
	private PackedDecimalData wsaaNetVal = new PackedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaTodayDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaTodayDateR = new FixedLengthStringData(8).isAPartOf(wsaaTodayDate, 0, REDEFINE);
	private ZonedDecimalData wsaaTodayYymmdd = new ZonedDecimalData(6, 0).isAPartOf(wsaaTodayDateR, 2).setUnsigned();
	private PackedDecimalData wsaaTransDate = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);

		/* WSAA-GENERAL-FIELDS */
	private FixedLengthStringData wsaaEndTrans = new FixedLengthStringData(1);
	private Validator endOfTransaction = new Validator(wsaaEndTrans, "Y");

	private FixedLengthStringData wsaaNotReady = new FixedLengthStringData(1);
	private Validator noNeedToPost = new Validator(wsaaNotReady, "Y");

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(16);
	private PackedDecimalData wsaaDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTranid, 0);
	private PackedDecimalData wsaaTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTranid, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTranid, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 12);
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrclmTableDAM hitrclmIO = new HitrclmTableDAM();
	private HitrtrgTableDAM hitrtrgIO = new HitrtrgTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private MatdclmTableDAM matdclmIO = new MatdclmTableDAM();
	private MatdtrgTableDAM matdtrgIO = new MatdtrgTableDAM();
	private MathclmTableDAM mathclmIO = new MathclmTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrntrgTableDAM utrntrgIO = new UtrntrgTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Batcuprec batcuprec = new Batcuprec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Udtrigrec udtrigrec = new Udtrigrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		seExit9090, 
		dbExit9190
	}

	public Unlmata() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		udtrigrec.udtrigrecRec = convertAndSetParam(udtrigrec.udtrigrecRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		initialize1000();
		processMatdtrg2000();
		wsaaEndTrans.set(SPACES);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			a200CheckFeedbackIndHitr();
		}
		else {
			checkFeedbackIndUtrn2100();
		}
		/* IF There are still outstanding funds to be dealt, no need to*/
		/* process*/
		if (noNeedToPost.isTrue()) {
			return ;
		}
		wsaaEndTrans.set(SPACES);
		startMatdclm2400();
		while ( !(endOfTransaction.isTrue()
		|| noNeedToPost.isTrue())) {
			processMatdclm2500();
		}
		
		/* There are still outstanding funds to be dealt, so no need to*/
		/* process*/
		if (noNeedToPost.isTrue()) {
			return ;
		}
		openBatch4200();
		begnhMathclm2700();
		while ( !(isEQ(mathclmIO.getStatuz(), varcom.endp))) {
			nextrMathclm2800();
		}
		
		closeBatch4300();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize1000()
	{
		go1010();
	}

protected void go1010()
	{
		syserrrec.subrname.set(wsaaSubr);
		wsaaEndTrans.set(SPACES);
		wsaaNotReady.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTodayDate.set(datcon1rec.intDate);
		wsaaTransDate.set(wsaaTodayYymmdd);
		wsaaTransTime.set(getCobolTime());
		readMathclm1100();
		readChdrmat1200();
		readTabT56451300();
		readTabT56881400();
	}

protected void readMathclm1100()
	{
		start1100();
	}

protected void start1100()
	{
		/*  Set up Header rec. key.*/
		mathclmIO.setParams(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO MATHCLM-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO MATHCLM-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-TRANNO    TO MATHCLM-TRANNO.               */
		mathclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		mathclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		mathclmIO.setTranno(udtrigrec.tk2Tranno);
		mathclmIO.setPlanSuffix(ZERO);
		mathclmIO.setFormat(mathclmrec);
		mathclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		matdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		matdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(), varcom.oK)
		&& isNE(mathclmIO.getStatuz(), varcom.endp)) {
			mathclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
			mathclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
			mathclmIO.setTranno(udtrigrec.tk2Tranno);
			mathclmIO.setPlanSuffix(ZERO);
			syserrrec.params.set(mathclmIO.getParams());
			dbError9100();
		}
		/* IF WSAA-TRIGGER-CHDRCOY NOT = MATHCLM-CHDRCOY                */
		/* OR WSAA-TRIGGER-CHDRNUM NOT = MATHCLM-CHDRNUM                */
		/* OR WSAA-TRIGGER-TRANNO  NOT = MATHCLM-TRANNO                 */
		if (isNE(udtrigrec.tk2Chdrcoy, mathclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum, mathclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno, mathclmIO.getTranno())) {
			mathclmIO.setStatuz(varcom.endp);
			mathclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
			mathclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
			mathclmIO.setTranno(udtrigrec.tk2Tranno);
			mathclmIO.setPlanSuffix(ZERO);
			syserrrec.params.set(mathclmIO.getParams());
			dbError9100();
		}
	}

protected void readChdrmat1200()
	{
		/*START*/
		chdrmatIO.setDataArea(SPACES);
		chdrmatIO.setChdrcoy(mathclmIO.getChdrcoy());
		chdrmatIO.setChdrnum(mathclmIO.getChdrnum());
		chdrmatIO.setFormat(chdrmatrec);
		chdrmatIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmatIO);
		if (isNE(chdrmatIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmatIO.getParams());
			dbError9100();
		}
		/*EXIT*/
	}

protected void readTabT56451300()
	{
		read1310();
	}

protected void read1310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(mathclmIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(mathclmIO.getChdrcoy());
		/* MOVE 'E'                    TO DESC-LANGUAGE.                */
		descIO.setLanguage(udtrigrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			dbError9100();
		}
	}

protected void readTabT56881400()
	{
		read1410();
	}

protected void read1410()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(mathclmIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(mathclmIO.getCnttype());
		itdmIO.setItmfrm(mathclmIO.getEffdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(), mathclmIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), mathclmIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(mathclmIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			systemError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void processMatdtrg2000()
	{
		para2010();
	}

protected void para2010()
	{
		readUtrn2600();
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			a100ReadHitr();
			return ;
		}
		wsaaBatckey.batcBatccoy.set(utrnIO.getBatccoy());
		wsaaBatckey.batcBatcbrn.set(utrnIO.getBatcbrn());
		wsaaBatckey.batcBatcactyr.set(utrnIO.getBatcactyr());
		wsaaBatckey.batcBatcactmn.set(utrnIO.getBatcactmn());
		wsaaBatckey.batcBatctrcde.set(utrnIO.getBatctrcde());
		wsaaBatckey.batcBatcbatch.set(utrnIO.getBatcbatch());
		/*  Start detail rec.*/
		matdtrgIO.setParams(SPACES);
		matdtrgIO.setChdrcoy(utrnIO.getChdrcoy());
		matdtrgIO.setChdrnum(utrnIO.getChdrnum());
		matdtrgIO.setTranno(utrnIO.getTranno());
		matdtrgIO.setPlanSuffix(utrnIO.getPlanSuffix());
		matdtrgIO.setCoverage(utrnIO.getCoverage());
		matdtrgIO.setRider(utrnIO.getRider());
		matdtrgIO.setVirtualFund(utrnIO.getUnitVirtualFund());
		matdtrgIO.setFieldType(utrnIO.getUnitType());
		matdtrgIO.setFormat(matdtrgrec);
		/* MOVE BEGNH                  TO MATDTRG-FUNCTION.             */
		matdtrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		matdtrgIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		
		SmartFileCode.execute(appVars, matdtrgIO);
		if (isNE(matdtrgIO.getStatuz(), varcom.oK)
		&& isNE(matdtrgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdtrgIO.getParams());
			dbError9100();
		}
		if (isNE(utrnIO.getChdrcoy(), matdtrgIO.getChdrcoy())
		|| isNE(utrnIO.getChdrnum(), matdtrgIO.getChdrnum())
		|| isNE(utrnIO.getTranno(), matdtrgIO.getTranno())
		|| isNE(utrnIO.getPlanSuffix(), matdtrgIO.getPlanSuffix())
		|| isNE(utrnIO.getCoverage(), matdtrgIO.getCoverage())
		|| isNE(utrnIO.getRider(), matdtrgIO.getRider())
		|| isNE(utrnIO.getUnitVirtualFund(), matdtrgIO.getVirtualFund())
		|| isNE(utrnIO.getUnitType(), matdtrgIO.getFieldType())
		|| isEQ(matdtrgIO.getStatuz(), varcom.endp)) {
			checkPlanSuffix2200();
		}
		/*   Update detail record*/
		if (isEQ(utrnIO.getContractAmount(), ZERO)) {
			return ;
		}
		matdtrgIO.setEstMatValue(ZERO);
		if (isLTE(utrnIO.getContractAmount(), ZERO)) {
			setPrecision(utrnIO.getContractAmount(), 2);
			utrnIO.setContractAmount(mult(utrnIO.getContractAmount(), -1));
		}
		setPrecision(matdtrgIO.getActvalue(), 2);
		matdtrgIO.setActvalue(add(matdtrgIO.getActvalue(), utrnIO.getContractAmount()));
		matdtrgIO.setFormat(matdtrgrec);
		/* MOVE REWRT                  TO MATDTRG-FUNCTION.             */
		matdtrgIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, matdtrgIO);
		if (isNE(matdtrgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(matdtrgIO.getParams());
			dbError9100();
		}
	}

protected void checkFeedbackIndUtrn2100()
	{
		check2110();
		continue2120();
	}

protected void check2110()
	{
		utrntrgIO.setDataArea(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO UTRNTRG-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO UTRNTRG-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-TRANNO    TO UTRNTRG-TRANNO.               */
		utrntrgIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		utrntrgIO.setChdrnum(udtrigrec.tk2Chdrnum);
		utrntrgIO.setTranno(udtrigrec.tk2Tranno);
		utrntrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		utrntrgIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void continue2120()
	{
		utrntrgIO.setFormat(utrntrgrec);
		SmartFileCode.execute(appVars, utrntrgIO);
		if (isNE(utrntrgIO.getStatuz(), varcom.oK)
		&& isNE(utrntrgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrntrgIO.getParams());
			dbError9100();
		}
		/* IF (WSAA-TRIGGER-CHDRNUM    NOT = UTRNTRG-CHDRNUM            */
		/* OR WSAA-TRIGGER-CHDRCOY     NOT = UTRNTRG-CHDRCOY            */
		/* OR WSAA-TRIGGER-TRANNO      NOT = UTRNTRG-TRANNO )           */
		if (isNE(udtrigrec.tk2Chdrcoy, utrntrgIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum, utrntrgIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno, utrntrgIO.getTranno())
		|| isEQ(utrntrgIO.getStatuz(), varcom.endp)) {
			utrntrgIO.setStatuz(varcom.endp);
			wsaaNotReady.set(" ");
			return ;
		}
		if (isNE(utrntrgIO.getFeedbackInd(), "Y")) {
			wsaaNotReady.set("Y");
		}
		else {
			wsaaNotReady.set(" ");
			utrntrgIO.setFunction(varcom.nextr);
			continue2120();
			return ;
		}
	}

protected void checkPlanSuffix2200()
	{
		para2210();
	}

protected void para2210()
	{
		matdtrgIO.setParams(SPACES);
		matdtrgIO.setChdrcoy(utrnIO.getChdrcoy());
		matdtrgIO.setChdrnum(utrnIO.getChdrnum());
		matdtrgIO.setTranno(utrnIO.getTranno());
		matdtrgIO.setPlanSuffix(ZERO);
		matdtrgIO.setCoverage(utrnIO.getCoverage());
		matdtrgIO.setRider(utrnIO.getRider());
		matdtrgIO.setVirtualFund(utrnIO.getUnitVirtualFund());
		matdtrgIO.setFieldType(utrnIO.getUnitType());
		matdtrgIO.setFormat(matdtrgrec);
		/* MOVE BEGNH                  TO MATDTRG-FUNCTION.             */
		matdtrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		matdtrgIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, matdtrgIO);
		if (isNE(matdtrgIO.getStatuz(), varcom.oK)
		&& isNE(matdtrgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdtrgIO.getParams());
			dbError9100();
		}
		if (isNE(utrnIO.getChdrcoy(), matdtrgIO.getChdrcoy())
		|| isNE(utrnIO.getChdrnum(), matdtrgIO.getChdrnum())
		|| isNE(utrnIO.getTranno(), matdtrgIO.getTranno())
		|| isNE(utrnIO.getCoverage(), matdtrgIO.getCoverage())
		|| isNE(utrnIO.getRider(), matdtrgIO.getRider())
		|| isNE(matdtrgIO.getPlanSuffix(), ZERO)
		|| isNE(utrnIO.getUnitType(), matdtrgIO.getFieldType())
		|| isNE(utrnIO.getUnitVirtualFund(), matdtrgIO.getVirtualFund())) {
			/*     MOVE MRNF               TO SYSR-STATUZ                   */
			matdtrgIO.setParams(SPACES);
			matdtrgIO.setStatuz(varcom.endp);
			matdtrgIO.setChdrcoy(utrnIO.getChdrcoy());
			matdtrgIO.setChdrnum(utrnIO.getChdrnum());
			matdtrgIO.setTranno(utrnIO.getTranno());
			matdtrgIO.setPlanSuffix(ZERO);
			matdtrgIO.setCoverage(utrnIO.getCoverage());
			matdtrgIO.setRider(utrnIO.getRider());
			matdtrgIO.setVirtualFund(utrnIO.getUnitVirtualFund());
			matdtrgIO.setFieldType(utrnIO.getUnitType());
			syserrrec.params.set(matdtrgIO.getParams());
			dbError9100();
		}
	}

protected void checkPlanSuffix22300()
	{
		para2310();
	}

protected void para2310()
	{
		matdclmIO.setParams(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO MATDCLM-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO MATDCLM-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-TRANNO    TO MATDCLM-TRANNO.               */
		matdclmIO.setPlanSuffix(ZERO);
		/* MOVE WSAA-TRIGGER-COVERAGE  TO MATDCLM-COVERAGE.             */
		/* MOVE WSAA-TRIGGER-RIDER     TO MATDCLM-RIDER.                */
		matdclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		matdclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		matdclmIO.setTranno(udtrigrec.tk2Tranno);
		matdclmIO.setCoverage(udtrigrec.tk2Coverage);
		matdclmIO.setRider(udtrigrec.tk2Rider);
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		matdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		matdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO","COVERAGE","RIDER","PLNSFX");
		
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(), varcom.oK)
		&& isNE(matdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			dbError9100();
		}
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = MATDCLM-CHDRCOY            */
		/* OR WSAA-TRIGGER-CHDRNUM     NOT = MATDCLM-CHDRNUM            */
		/* OR WSAA-TRIGGER-TRANNO      NOT = MATDCLM-TRANNO             */
		/* OR WSAA-TRIGGER-COVERAGE    NOT = MATDCLM-COVERAGE           */
		/* OR WSAA-TRIGGER-RIDER       NOT = MATDCLM-RIDER              */
		if (isNE(udtrigrec.tk2Chdrcoy, matdclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum, matdclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno, matdclmIO.getTranno())
		|| isNE(udtrigrec.tk2Coverage, matdclmIO.getCoverage())
		|| isNE(udtrigrec.tk2Rider, matdclmIO.getRider())
		|| isNE(matdclmIO.getPlanSuffix(), ZERO)) {
			/*     MOVE MRNF               TO SYSR-STATUZ                   */
			matdclmIO.setStatuz(varcom.endp);
			matdclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
			matdclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
			matdclmIO.setTranno(udtrigrec.tk2Tranno);
			matdclmIO.setCoverage(udtrigrec.tk2Coverage);
			matdclmIO.setRider(udtrigrec.tk2Rider);
			matdclmIO.setPlanSuffix(ZERO);
			syserrrec.params.set(matdclmIO.getParams());
			dbError9100();
		}
	}

protected void startMatdclm2400()
	{
		start2410();
	}

protected void start2410()
	{
		/*  Start detail rec.*/
		matdclmIO.setParams(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO MATDCLM-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO MATDCLM-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-TRANNO    TO MATDCLM-TRANNO.               */
		/* MOVE WSAA-TRIGGER-SUFFIX    TO MATDCLM-PLAN-SUFFIX.          */
		/* MOVE WSAA-TRIGGER-COVERAGE  TO MATDCLM-COVERAGE.             */
		/* MOVE WSAA-TRIGGER-RIDER     TO MATDCLM-RIDER.                */
		matdclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		matdclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		matdclmIO.setTranno(udtrigrec.tk2Tranno);
		matdclmIO.setPlanSuffix(udtrigrec.tk2Suffix);
		matdclmIO.setLife(udtrigrec.life);
		matdclmIO.setCoverage(udtrigrec.tk2Coverage);
		matdclmIO.setRider(udtrigrec.tk2Rider);
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		matdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		matdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO","COVERAGE","RIDER","PLNSFX");
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(), varcom.oK)
		&& isNE(matdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			dbError9100();
		}
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = MATDCLM-CHDRCOY            */
		/* OR WSAA-TRIGGER-CHDRNUM     NOT = MATDCLM-CHDRNUM            */
		/* OR WSAA-TRIGGER-TRANNO      NOT = MATDCLM-TRANNO             */
		/* OR WSAA-TRIGGER-SUFFIX      NOT = MATDCLM-PLAN-SUFFIX        */
		/* OR WSAA-TRIGGER-COVERAGE    NOT = MATDCLM-COVERAGE           */
		/* OR WSAA-TRIGGER-RIDER       NOT = MATDCLM-RIDER              */
		if (isNE(udtrigrec.tk2Chdrcoy, matdclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum, matdclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno, matdclmIO.getTranno())
		|| isNE(udtrigrec.tk2Suffix, matdclmIO.getPlanSuffix())
		|| isNE(udtrigrec.tk2Coverage, matdclmIO.getCoverage())
		|| isNE(udtrigrec.tk2Rider, matdclmIO.getRider())
		|| isEQ(matdclmIO.getStatuz(), varcom.endp)) {
			checkPlanSuffix22300();
		}
	}

protected void processMatdclm2500()
	{
		para2510();
	}

protected void para2510()
	{
		/*  READ all MATD recs for this transaction*/
		if (isNE(matdclmIO.getChdrcoy(), mathclmIO.getChdrcoy())
		|| isNE(matdclmIO.getChdrnum(), mathclmIO.getChdrnum())
		|| isNE(matdclmIO.getTranno(), mathclmIO.getTranno())
		|| isEQ(matdclmIO.getStatuz(), varcom.endp)) {
			wsaaEndTrans.set("Y");
			wsaaNotReady.set(" ");
			return ;
		}
		if (isNE(matdclmIO.getEstMatValue(), ZERO)) {
			wsaaNotReady.set("Y");
			return ;
		}
		/*   Read next detail record*/
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(), varcom.oK)
		&& isNE(matdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			dbError9100();
		}
	}

protected void readUtrn2600()
	{
		read2610();
	}

protected void read2610()
	{
		utrnIO.setDataArea(SPACES);
		/* MOVE UTRN-LINKAGE           TO UTRN-DATA-KEY.                */
		utrnIO.setChdrcoy(udtrigrec.chdrcoy);
		utrnIO.setChdrnum(udtrigrec.chdrnum);
		utrnIO.setLife(udtrigrec.life);
		utrnIO.setCoverage(udtrigrec.coverage);
		utrnIO.setRider(udtrigrec.rider);
		utrnIO.setPlanSuffix(udtrigrec.planSuffix);
		utrnIO.setUnitVirtualFund(udtrigrec.unitVirtualFund);
		utrnIO.setUnitType(udtrigrec.unitType);
		utrnIO.setTranno(udtrigrec.tranno);
		utrnIO.setFormat(utrnrec);
		utrnIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)
		&& isNE(utrnIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(utrnIO.getParams());
			dbError9100();
		}
	}

protected void begnhMathclm2700()
	{
		start2700();
	}

protected void start2700()
	{
		/*  Set up Header rec. key.*/
		mathclmIO.setParams(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO MATHCLM-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO MATHCLM-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-TRANNO    TO MATHCLM-TRANNO.               */
		mathclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		mathclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
		mathclmIO.setTranno(udtrigrec.tk2Tranno);
		mathclmIO.setPlanSuffix(ZERO);
		mathclmIO.setFormat(mathclmrec);
		/* MOVE BEGNH                  TO MATHCLM-FUNCTION.             */
		mathclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		matdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		matdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(), varcom.oK)
		&& isNE(mathclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(mathclmIO.getParams());
			dbError9100();
		}
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = MATHCLM-CHDRCOY            */
		/* OR WSAA-TRIGGER-CHDRNUM     NOT = MATHCLM-CHDRNUM            */
		/* OR WSAA-TRIGGER-TRANNO      NOT = MATHCLM-TRANNO             */
		if (isNE(udtrigrec.tk2Chdrcoy, mathclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum, mathclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno, mathclmIO.getTranno())) {
			mathclmIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
			mathclmIO.setChdrnum(udtrigrec.tk2Chdrnum);
			mathclmIO.setTranno(udtrigrec.tk2Tranno);
			mathclmIO.setPlanSuffix(ZERO);
			syserrrec.params.set(mathclmIO.getParams());
			dbError9100();
		}
	}

protected void nextrMathclm2800()
	{
		processMatdclm2800();
		nextrMathclm2805();
	}

protected void processMatdclm2800()
	{
		wsaaActualVal.set(ZERO);
		startMatdclm2960();
		while ( !(isEQ(matdclmIO.getStatuz(), varcom.endp))) {
			processMatdclm2950();
		}
		
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			startMatdclm2960();
			while ( !(isEQ(matdclmIO.getStatuz(), varcom.endp))) {
				postActualComp3000();
			}
			
		}
		else {
			postActualTotal3100();
		}
		compute(wsaaNetVal, 3).set(add(wsaaActualVal, mathclmIO.getOtheradjst()));
		postAdj3200();
		postNetAmount3300();
		updateMathclm4000();
	}

protected void nextrMathclm2805()
	{
		mathclmIO.setFormat(mathclmrec);
		mathclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(), varcom.oK)
		&& isNE(mathclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(mathclmIO.getParams());
			dbError9100();
		}
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = MATHCLM-CHDRCOY            */
		/* OR WSAA-TRIGGER-CHDRNUM     NOT = MATHCLM-CHDRNUM            */
		/* OR WSAA-TRIGGER-TRANNO      NOT = MATHCLM-TRANNO             */
		if (isNE(udtrigrec.tk2Chdrcoy, mathclmIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Chdrnum, mathclmIO.getChdrnum())
		|| isNE(udtrigrec.tk2Tranno, mathclmIO.getTranno())
		|| isEQ(mathclmIO.getStatuz(), varcom.endp)) {
			mathclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*  Sum all MATD recs for this transaction with the same policy
	*  number as the MATHCLM.
	* </pre>
	*/
protected void processMatdclm2950()
	{
		/*ADD-ACTUAL-VALUE*/
		/*   Accumulate Total Actual amount*/
		wsaaActualVal.add(matdclmIO.getActvalue());
		/*   Read next detail record*/
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(), varcom.oK)
		&& isNE(matdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			dbError9100();
		}
		if (isNE(matdclmIO.getChdrcoy(), mathclmIO.getChdrcoy())
		|| isNE(matdclmIO.getChdrnum(), mathclmIO.getChdrnum())
		|| isNE(matdclmIO.getTranno(), mathclmIO.getTranno())
		|| isNE(matdclmIO.getPlanSuffix(), mathclmIO.getPlanSuffix())
		|| isEQ(matdclmIO.getStatuz(), varcom.endp)) {
			matdclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void startMatdclm2960()
	{
		start2960();
	}

protected void start2960()
	{
		/*  Start detail rec.*/
		matdclmIO.setParams(SPACES);
		matdclmIO.setChdrcoy(mathclmIO.getChdrcoy());
		matdclmIO.setChdrnum(mathclmIO.getChdrnum());
		matdclmIO.setTranno(mathclmIO.getTranno());
		matdclmIO.setPlanSuffix(mathclmIO.getPlanSuffix());
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		matdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		matdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO","PLNSFX");
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(), varcom.oK)
		&& isNE(matdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			dbError9100();
		}
		if (isNE(matdclmIO.getChdrcoy(), mathclmIO.getChdrcoy())
		|| isNE(matdclmIO.getChdrnum(), mathclmIO.getChdrnum())
		|| isNE(matdclmIO.getTranno(), mathclmIO.getTranno())
		|| isNE(matdclmIO.getPlanSuffix(), mathclmIO.getPlanSuffix())
		|| isEQ(matdclmIO.getStatuz(), varcom.endp)) {
			matdclmIO.setStatuz(varcom.endp);
		}
	}

protected void postActualComp3000()
	{
		para3010();
		nextMatd3080();
	}

protected void para3010()
	{
		/*  READ all MATD recs for this transaction*/
		/*   Post individual Components*/
		if (isEQ(matdclmIO.getActvalue(), ZERO)) {
			return ;
		}
		/*     GO TO                   3090-EXIT.                       */
		lifacmvSetup3500();
		lifacmvrec1.origamt.set(matdclmIO.getActvalue());
		lifacmvrec1.sacscode.set(t5645rec.sacscode02);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec1.glcode.set(t5645rec.glmap02);
		lifacmvrec1.glsign.set(t5645rec.sign02);
		lifacmvrec1.contot.set(t5645rec.cnttot02);
		wsaaRldgChdrnum.set(mathclmIO.getChdrnum());
		wsaaPlan.set(matdclmIO.getPlanSuffix());
		wsaaRldgLife.set(matdclmIO.getLife());
		wsaaRldgCoverage.set(matdclmIO.getCoverage());
		wsaaRldgRider.set(matdclmIO.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.substituteCode[6].set(matdclmIO.getCrtable());
		lifacmvWrite3600();
	}

protected void nextMatd3080()
	{
		/*   Read next detail record*/
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(), varcom.oK)
		&& isNE(matdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			dbError9100();
		}
		if (isNE(matdclmIO.getChdrcoy(), mathclmIO.getChdrcoy())
		|| isNE(matdclmIO.getChdrnum(), mathclmIO.getChdrnum())
		|| isNE(matdclmIO.getTranno(), mathclmIO.getTranno())
		|| isNE(matdclmIO.getPlanSuffix(), mathclmIO.getPlanSuffix())
		|| isEQ(matdclmIO.getStatuz(), varcom.endp)) {
			matdclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void postActualTotal3100()
	{
		para3110();
	}

protected void para3110()
	{
		if (isEQ(wsaaActualVal, ZERO)) {
			return ;
		}
		lifacmvSetup3500();
		lifacmvrec1.origamt.set(wsaaActualVal);
		lifacmvrec1.sacscode.set(t5645rec.sacscode01);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec1.glcode.set(t5645rec.glmap01);
		lifacmvrec1.glsign.set(t5645rec.sign01);
		lifacmvrec1.contot.set(t5645rec.cnttot01);
		lifacmvrec1.rldgacct.set(mathclmIO.getChdrnum());
		lifacmvWrite3600();
	}

protected void postAdj3200()
	{
		start3200();
	}

protected void start3200()
	{
		if (isEQ(mathclmIO.getOtheradjst(), ZERO)) {
			return ;
		}
		lifacmvSetup3500();
		lifacmvrec1.origamt.set(mathclmIO.getOtheradjst());
		lifacmvrec1.sacscode.set(t5645rec.sacscode03);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec1.glcode.set(t5645rec.glmap03);
		lifacmvrec1.glsign.set(t5645rec.sign03);
		lifacmvrec1.contot.set(t5645rec.cnttot03);
		lifacmvrec1.rldgacct.set(mathclmIO.getChdrnum());
		lifacmvWrite3600();
	}

protected void postNetAmount3300()
	{
		start3300();
	}

protected void start3300()
	{
		if (isEQ(wsaaNetVal, ZERO)) {
			return ;
		}
		lifacmvSetup3500();
		lifacmvrec1.origamt.set(wsaaNetVal);
		lifacmvrec1.sacscode.set(t5645rec.sacscode04);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec1.glcode.set(t5645rec.glmap04);
		lifacmvrec1.glsign.set(t5645rec.sign04);
		lifacmvrec1.contot.set(t5645rec.cnttot04);
		lifacmvrec1.rldgacct.set(mathclmIO.getChdrnum());
		lifacmvWrite3600();
	}

protected void lifacmvSetup3500()
	{
		start3510();
	}

protected void start3510()
	{
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.rdocnum.set(mathclmIO.getChdrnum());
		lifacmvrec1.tranno.set(mathclmIO.getTranno());
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.rldgcoy.set(mathclmIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(mathclmIO.getChdrcoy());
		lifacmvrec1.origcurr.set(mathclmIO.getCurrcd());
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.tranref.set(mathclmIO.getTranno());
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.effdate.set(mathclmIO.getEffdate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(mathclmIO.getCnttype());
		lifacmvrec1.substituteCode[6].set(SPACES);
		/* MOVE PARM-TRANID            TO VRCM-TRANID.                  */
		/* MOVE VRCM-TERMID            TO LIFA-TERMID.                  */
		/* MOVE VRCM-USER              TO LIFA-USER.                    */
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.transactionTime.set(getCobolTime());
		lifacmvrec1.transactionDate.set(getCobolDate());
		/*    The Transaction Date and Time are set up in LIFACMV.*/
		/*    MOVE WSAA-TODAY-DATE        TO LIFA-TRANSACTION-TIME.        */
		/*    MOVE WSAA-TRANS-TIME        TO LIFA-TRANSACTION-DATE.        */
		lifacmvrec1.transactionDate.set(wsaaTransDate);
		lifacmvrec1.transactionTime.set(wsaaTransTime);
	}

protected void lifacmvWrite3600()
	{
		/*WRITE*/
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			dbError9100();
		}
		/*EXIT*/
	}

protected void updateMathclm4000()
	{
		/*REWRITE-MATHCLM*/
		mathclmIO.setClamamt(wsaaActualVal);
		mathclmIO.setFormat(mathclmrec);
		/* MOVE REWRT                  TO MATHCLM-FUNCTION.             */
		mathclmIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(), varcom.oK)
		&& isNE(mathclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(mathclmIO.getParams());
			dbError9100();
		}
		/*EXIT*/
	}

protected void openBatch4200()
	{
		open4210();
	}

protected void open4210()
	{
		/*    The Originating Batch Key is required in the Trigger module*/
		/*    that is why the UTRN is used to set up the key. By calling*/
		/*    BATCDOR with an 'AUTO' function it will only open a new batch*/
		/*    if the online batch has since been closed. Otherwise it opens*/
		/*    a new one and it is what is moved in to the LIFA/R BATCKEY's.*/
		batcdorrec.batcdorRec.set(SPACES);
		batcdorrec.function.set("AUTO");
		/* MOVE PARM-TRANID            TO BATD-TRANID.                  */
		wsaaTime.set(getCobolTime());
		wsaaDate.set(getCobolDate());
		wsaaTermid.set(SPACES);
		wsaaUser.set(ZERO);
		batcdorrec.tranid.set(wsaaTranid);
		/* MOVE UTRN-BATCCOY           TO LIFA-BATCCOY.                 */
		/* MOVE UTRN-BATCBRN           TO LIFA-BATCBRN.                 */
		/* MOVE UTRN-BATCACTYR         TO LIFA-BATCACTYR.               */
		/* MOVE UTRN-BATCACTMN         TO LIFA-BATCACTMN.               */
		/* MOVE UTRN-BATCTRCDE         TO LIFA-BATCTRCDE.               */
		/* MOVE UTRN-BATCBATCH         TO LIFA-BATCBATCH.               */
		/*   MOVE WSAA-BATCKEY           TO LIFA-BATCKEY.         <LA4524>*/
		lifacmvrec1.batckey.set(udtrigrec.batchkey);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcdorrec.batchkey.set(lifacmvrec1.batckey);
		batcdorrec.prefix.set("BA");
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			dbError9100();
		}
		lifacmvrec1.batckey.set(batcdorrec.batchkey);
	}

protected void closeBatch4300()
	{
		close4310();
	}

protected void close4310()
	{
		batcuprec.function.set(varcom.writs);
		batcuprec.batchkey.set(batcdorrec.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			dbError9100();
		}
		batcdorrec.function.set("CLOSE");
		/* MOVE PARM-TRANID            TO BATD-TRANID.                  */
		wsaaTime.set(getCobolTime());
		wsaaDate.set(getCobolDate());
		wsaaTermid.set(SPACES);
		wsaaUser.set(ZERO);
		batcdorrec.tranid.set(wsaaTranid);
		batcdorrec.batchkey.set(batcdorrec.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			dbError9100();
		}
	}

protected void a100ReadHitr()
	{
		a110Read();
	}

protected void a110Read()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrcoy(udtrigrec.chdrcoy);
		hitrclmIO.setChdrnum(udtrigrec.chdrnum);
		hitrclmIO.setLife(udtrigrec.life);
		hitrclmIO.setCoverage(udtrigrec.coverage);
		hitrclmIO.setRider(udtrigrec.rider);
		hitrclmIO.setPlanSuffix(udtrigrec.planSuffix);
		hitrclmIO.setZintbfnd(udtrigrec.unitVirtualFund);
		hitrclmIO.setTranno(udtrigrec.tranno);
		hitrclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			dbError9100();
		}
		wsaaBatckey.batcBatccoy.set(hitrclmIO.getBatccoy());
		wsaaBatckey.batcBatcbrn.set(hitrclmIO.getBatcbrn());
		wsaaBatckey.batcBatcactyr.set(hitrclmIO.getBatcactyr());
		wsaaBatckey.batcBatcactmn.set(hitrclmIO.getBatcactmn());
		wsaaBatckey.batcBatctrcde.set(hitrclmIO.getBatctrcde());
		wsaaBatckey.batcBatcbatch.set(hitrclmIO.getBatcbatch());
		matdtrgIO.setParams(SPACES);
		/* MOVE BEGNH                  TO MATDTRG-FUNCTION.     <LA3993>*/
		matdtrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		matdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		matdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO","PLNSFX","LIFE","COVERAGE","RIDER");
          
		matdtrgIO.setChdrcoy(hitrclmIO.getChdrcoy());
		matdtrgIO.setChdrnum(hitrclmIO.getChdrnum());
		matdtrgIO.setTranno(hitrclmIO.getTranno());
		matdtrgIO.setPlanSuffix(hitrclmIO.getPlanSuffix());
		matdtrgIO.setLife(hitrclmIO.getLife());
		matdtrgIO.setCoverage(hitrclmIO.getCoverage());
		matdtrgIO.setRider(hitrclmIO.getRider());
		matdtrgIO.setVirtualFund(hitrclmIO.getZintbfnd());
		matdtrgIO.setFormat(matdtrgrec);
		SmartFileCode.execute(appVars, matdtrgIO);
		if (isNE(matdtrgIO.getStatuz(), varcom.oK)
		&& isNE(matdtrgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdtrgIO.getParams());
			dbError9100();
		}
		if (isNE(hitrclmIO.getChdrcoy(), matdtrgIO.getChdrcoy())
		|| isNE(hitrclmIO.getChdrnum(), matdtrgIO.getChdrnum())
		|| isNE(hitrclmIO.getTranno(), matdtrgIO.getTranno())
		|| isNE(hitrclmIO.getPlanSuffix(), matdtrgIO.getPlanSuffix())
		|| isNE(hitrclmIO.getLife(), matdtrgIO.getLife())
		|| isNE(hitrclmIO.getCoverage(), matdtrgIO.getCoverage())
		|| isNE(hitrclmIO.getRider(), matdtrgIO.getRider())
		|| isNE(hitrclmIO.getZintbfnd(), matdtrgIO.getVirtualFund())
		|| isEQ(matdtrgIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isEQ(hitrclmIO.getContractAmount(), ZERO)) {
			return ;
		}
		matdtrgIO.setEstMatValue(ZERO);
		if (isLTE(hitrclmIO.getContractAmount(), ZERO)) {
			setPrecision(hitrclmIO.getContractAmount(), 2);
			hitrclmIO.setContractAmount(mult(hitrclmIO.getContractAmount(), -1));
		}
		setPrecision(matdtrgIO.getActvalue(), 2);
		matdtrgIO.setActvalue(add(matdtrgIO.getActvalue(), hitrclmIO.getContractAmount()));
		/* MOVE REWRT                  TO MATDTRG-FUNCTION.     <LA3993>*/
		matdtrgIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, matdtrgIO);
		if (isNE(matdtrgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(matdtrgIO.getParams());
			dbError9100();
		}
	}

protected void a200CheckFeedbackIndHitr()
	{
		a210Check();
		a220Continue();
	}

protected void a210Check()
	{
		hitrtrgIO.setParams(SPACES);
		hitrtrgIO.setChdrnum(udtrigrec.tk2Chdrnum);
		hitrtrgIO.setChdrcoy(udtrigrec.tk2Chdrcoy);
		hitrtrgIO.setTranno(udtrigrec.tk2Tranno);
		hitrtrgIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		hitrtrgIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrtrgIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
	}

protected void a220Continue()
	{
		SmartFileCode.execute(appVars, hitrtrgIO);
		if (isNE(hitrtrgIO.getStatuz(), varcom.oK)
		&& isNE(hitrtrgIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitrtrgIO.getParams());
			dbError9100();
		}
		if (isNE(udtrigrec.tk2Chdrnum, hitrtrgIO.getChdrnum())
		|| isNE(udtrigrec.tk2Chdrcoy, hitrtrgIO.getChdrcoy())
		|| isNE(udtrigrec.tk2Tranno, hitrtrgIO.getTranno())
		|| isEQ(hitrtrgIO.getStatuz(), varcom.endp)) {
			wsaaNotReady.set(" ");
			hitrtrgIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(hitrtrgIO.getFeedbackInd(), "Y")) {
			wsaaNotReady.set("Y");
		}
		else {
			wsaaNotReady.set(" ");
			hitrtrgIO.setFunction(varcom.nextr);
			a220Continue();
			return ;
		}
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
				case seExit9090: 
					seExit9090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		/* MOVE BOMB                   TO PARM-STATUZ.                  */
		udtrigrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9100();
				case dbExit9190: 
					dbExit9190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		/* MOVE BOMB                   TO PARM-STATUZ.                  */
		udtrigrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
