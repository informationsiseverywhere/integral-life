/*
 * File: Dppan4.java
 * Date: 29 August 2009 22:46:41
 * Author: Quipoz Limited
 * 
 * Class transformed from DPPAN4.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Dthcpy;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
*REMARKS.
*
*  DPPAN4 - Death Claim Processing Subroutine No. 4
*  -------------------------------------------------
*
*  The linkage in to this subroutine is DTHCPY.
*
*  It is called with every CLMD record written from P5256 (driven
*  by what is passed from DCCAN4).
*
*  For each CLMD read T5688 to determine whether Component  Level
*  Accounting is applicable or not.
*
*  Read  T5645  with  the  key WSAA-SUBR ie. DPPAN4. Based on the
*  Component Level Accounting answer post ACMV's  as  appropriate
*  calling  LIFACMV  to  do  this. If the DTHP-ACTUAL-VAL is zero
*  then NO ACMV's are necessary.
*
*
*  Rewrite the original REGP with valid flag '2'.
*
*  If CLMDCLM-ANNYPIND = D, this means continue payments.
*  If CLMDCLM-ANNYPIND = E, this means terminate payments.
*
*  Check the field CLMDCLM-ANNYPIND and if it is 'E', alter the
*  status as appropriate from the T6693 check (this terminates
*  the REGP) - insert this new record with a valid flag '1'.
*
*  If it is 'D' alter the FINAL-PAYDATE and insert this new
*  record with a valid flag '1'
*
*
*****************************************************************
* </pre>
*/
public class Dppan4 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "DPPAN4";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaElement = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(2, 0).isAPartOf(wsaaElement, 0).setUnsigned();

	private FixedLengthStringData wsaaComlvlacc = new FixedLengthStringData(1);
	private Validator comlvlaccRequired = new Validator(wsaaComlvlacc, "Y");
	private Validator comlvlaccNotrequired = new Validator(wsaaComlvlacc, "N");
		/* WSAA-STORAGE-AREAS-AND-SUBS */
	private FixedLengthStringData wsaaNewRegpStatus = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaTableStatusFlag = new FixedLengthStringData(1);
	private Validator statusOk = new Validator(wsaaTableStatusFlag, "Y");
	private Validator statusNotOk = new Validator(wsaaTableStatusFlag, "N");
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String h144 = "H144";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t6693 = "T6693";
		/* FORMATS */
	private static final String clmdclmrec = "CLMDCLMREC";
	private static final String descrec = "DESCREC";
	private static final String itdmrec = "ITEMREC";
	private static final String itemrec = "ITEMREC";
	private static final String regprec = "REGPREC";
	private static final String regpclmrec = "REGPCLMREC";
	private ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private RegpclmTableDAM regpclmIO = new RegpclmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T6693rec t6693rec = new T6693rec();
	private Varcom varcom = new Varcom();
	private Dthcpy dthcpy = new Dthcpy();

	public Dppan4() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dthcpy.deathRec = convertAndSetParam(dthcpy.deathRec, parmArray, 0);
		try {
			startSubr0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr0000()
	{
		para0010();
		exit0090();
	}

protected void para0010()
	{
		/*    Initialise all working storage fields.*/
		dthcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaBatckey.set(dthcpy.batckey);
		comlvlaccRequired.setTrue();
		statusNotOk.setTrue();
		wsaaItemitem.set(SPACES);
		/*    In order to use the processing indicator CLMDCLM-ANNYPIND*/
		/*    the CLMDCLM must be read.*/
		readrClmdclm1000();
		/*    For each CLMD read T5688 to determine whether component*/
		/*    level accounting is applicable or not.*/
		readTabT56882000();
		// MIBT-266 STARTS
		//		if (isEQ(t5688rec.comlvlacc, "N")) {
		//			wsaaComlvlacc.set("N");
		//		}
		//MIBT-266 ENDS
		if (isEQ(t5688rec.comlvlacc, "N") || isEQ(t5688rec.comlvlacc,SPACES))//ILIFE-4885 
		{
			wsaaComlvlacc.set("N");
		}
		/*    Read  T5645  with  the  key WSAA-SUBR ie. DPPAN4.*/
		/*    Based on the Component Level Accounting answer post ACMV's*/
		/*    as appropriate calling  LIFACMV  to  do  this. If the*/
		/*    DTHP-ACTUAL-VAL is zero then NO ACMV's are necessary.*/
		if (isNE(dthcpy.actualVal, ZERO)) {
			readTabT56453000();
			wsaaJrnseq.set(ZERO);
			if (isEQ(dthcpy.fieldType, "S")) {
				componentPosting4000();
			}
		}
		begnhRegp5000();
		/*    The status of REGPCLM is checked if present on T6693.  Then*/
		/*    if a match is found the REGP is updated as appropriate.*/
		/*    This is done for each REGP until ENDP.*/
		if (isNE(regpclmIO.getStatuz(), varcom.endp)) {
			while ( !(isEQ(regpclmIO.getStatuz(), varcom.endp))) {
				updateRegp6000();
			}
			
		}
	}

protected void exit0090()
	{
		exitProgram();
	}

protected void updateRegp6000()
	{
		/*READ*/
		/*    The status of REGPCLM is checked if present on T6693.*/
		/*    This is done for each REGP until ENDP.*/
		readTabT66937000();
		/*    Rewrite the original REGP with valid flag '2'.*/
		/*    Check the field CLMDCLM-ANNYPIND, if it is 'E' alter the*/
		/*    status as appropriate from the T6693 check (this terminates*/
		/*    the REGP) - insert this new record with a valid flag '1'.*/
		/*    If it is 'D', alter the FINAL-PAYDATE and insert this new*/
		/*    record with a valid flag '1'*/
		processRegp8000();
		/*EXIT*/
	}

protected void readrClmdclm1000()
	{
		read1010();
	}

protected void read1010()
	{
		/*    Read (READR) CLMDCLM.*/
		clmdclmIO.setChdrcoy(dthcpy.chdrChdrcoy);
		clmdclmIO.setChdrnum(dthcpy.chdrChdrnum);
		clmdclmIO.setCoverage(dthcpy.covrCoverage);
		clmdclmIO.setRider(dthcpy.covrRider);
		clmdclmIO.setCrtable(dthcpy.crtable);
		clmdclmIO.setFunction(varcom.readr);
		clmdclmIO.setFormat(clmdclmrec);
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		wsaaElement.set(dthcpy.element);
	}

protected void readTabT56882000()
	{
		read2010();
	}

protected void read2010()
	{
		/*    For each CLMD read T5688 to determine whether component*/
		/*    level accounting is applicable or not.*/
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(dthcpy.cnttype);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), dthcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), dthcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemitem(dthcpy.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readTabT56453000()
	{
		read3010();
	}

protected void read3010()
	{
		/*    Read T5645 with the key WSAA-SUBR (ie. DPPAN4).*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dthcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*    Find the description.*/
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(dthcpy.chdrChdrcoy);
		/* MOVE 'E'                    TO DESC-LANGUAGE.                */
		descIO.setLanguage(dthcpy.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void componentPosting4000()
	{
		para4010();
	}

protected void para4010()
	{
		/*    Based on the Component Level Accounting answer post ACMV's*/
		/*    as appropriate calling LIFACMV to do this.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		wsaaRldgacct.set(ZERO);
		lifacmvrec.substituteCode[6].set(dthcpy.crtable);
		wsaaRldgChdrnum.set(dthcpy.chdrChdrnum);
		if (comlvlaccRequired.isTrue()) {
			wsaaRldgLife.set(dthcpy.lifeLife);
			wsaaRldgCoverage.set(dthcpy.covrCoverage);
			wsaaRldgRider.set(dthcpy.covrRider);
			wsaaRldgPlanSuffix.set(ZERO);
		}
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		//ILIFE-4885
		if (comlvlaccNotrequired.isTrue() ) {
			lifacmvrec.rldgacct.set(dthcpy.chdrChdrnum);
		}
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(dthcpy.chdrChdrnum);
		lifacmvrec.tranno.set(dthcpy.tranno);
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(dthcpy.effdate);
		lifacmvrec.origcurr.set(dthcpy.currcode);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.termid.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[1]);
		lifacmvrec.glcode.set(t5645rec.glmap[1]);
		lifacmvrec.glsign.set(t5645rec.sign[1]);
		lifacmvrec.contot.set(t5645rec.cnttot[1]);
		lifacmvrec.rldgcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(dthcpy.chdrChdrnum);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(dthcpy.cnttype);
		lifacmvrec.origamt.set(dthcpy.actualVal);
		lifacmvrec.termid.set(dthcpy.termid);
		lifacmvrec.user.set(dthcpy.user);
		lifacmvrec.transactionTime.set(dthcpy.time);
		lifacmvrec.transactionDate.set(dthcpy.date_var);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		if (isNE(lifacmvrec.origamt, 0)) {
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				fatalError600();
			}
		}
		else {
			return ;
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode[2]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[2]);
		lifacmvrec.glcode.set(t5645rec.glmap[2]);
		lifacmvrec.glsign.set(t5645rec.sign[2]);
		lifacmvrec.contot.set(t5645rec.cnttot[2]);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
	}

protected void begnhRegp5000()
	{
		read5010();
	}

protected void read5010()
	{
		/*    Read (BEGNH) the Regular Payments REGPCLM.*/
		regpclmIO.setChdrcoy(dthcpy.chdrChdrcoy);
		regpclmIO.setChdrnum(dthcpy.chdrChdrnum);
		regpclmIO.setLife(dthcpy.lifeLife);
		regpclmIO.setCoverage(dthcpy.covrCoverage);
		regpclmIO.setRider(dthcpy.covrRider);
		regpclmIO.setPlanSuffix(wsaaPlnsfx);
		regpclmIO.setRgpynum(ZERO);
		regpclmIO.setFormat(regpclmrec);
		/* MOVE BEGNH                  TO REGPCLM-FUNCTION.             */
		regpclmIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		regpclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		SmartFileCode.execute(appVars, regpclmIO);
		if (isNE(regpclmIO.getStatuz(), varcom.oK)
		&& isNE(regpclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(regpclmIO.getChdrcoy(), dthcpy.chdrChdrcoy)
		|| isNE(regpclmIO.getChdrnum(), dthcpy.chdrChdrnum)
		|| isNE(regpclmIO.getLife(), dthcpy.lifeLife)
		|| isNE(regpclmIO.getCoverage(), dthcpy.covrCoverage)
		|| isNE(regpclmIO.getRider(), dthcpy.covrRider)
		|| isNE(regpclmIO.getPlanSuffix(), wsaaPlnsfx))) {
			/*     MOVE REWRT              TO REGPCLM-FUNCTION              */
			/*     CALL 'REGPCLMIO'        USING REGPCLM-PARAMS             */
			/*     IF REGPCLM-STATUZ    NOT = O-K                           */
			/*        MOVE REGPCLM-PARAMS  TO SYSR-PARAMS                   */
			/*        PERFORM 600-FATAL-ERROR                               */
			/*     END-IF                                                   */
			regpclmIO.setStatuz(varcom.endp);
		}
	}

protected void readTabT66937000()
	{
		para7000();
	}

protected void para7000()
	{
		/*    The status of REGPCLM is checked if present on T6693.  This i*/
		/*    done twice. The first time with RGPYSTAT & CRTABLE as the*/
		/*    item key, and the second (if not found) with **** as the*/
		/*    item key. If, after the second check, the item has not been*/
		/*    found, then set the error code and perform fatal error.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(regpclmIO.getRgpystat(), SPACES);
		stringVariable1.addExpression(regpclmIO.getCrtable(), SPACES);
		stringVariable1.setStringInto(wsaaItemitem);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(regpclmIO.getChdrcoy());
		itdmIO.setItemtabl(t6693);
		itdmIO.setItmfrm(regpclmIO.getCertdate());
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), regpclmIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t6693)
		|| isNE(itdmIO.getItemitem(), wsaaItemitem)
		|| isEQ(itdmIO.getStatuz(), varcom.endp))) {
			nextStatusCheck7100();
		}
		else {
			t6693rec.t6693Rec.set(itdmIO.getGenarea());
			t6693Search7110();
		}
	}

protected void nextStatusCheck7100()
	{
		para7100();
	}

protected void para7100()
	{
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(regpclmIO.getRgpystat(), SPACES);
		stringVariable1.addExpression("****", SPACES);
		stringVariable1.setStringInto(wsaaItemitem);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(regpclmIO.getChdrcoy());
		itdmIO.setItemtabl(t6693);
		itdmIO.setItmfrm(regpclmIO.getCertdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), regpclmIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t6693)
		|| isNE(itdmIO.getItemitem(), wsaaItemitem)
		|| isEQ(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h144);
			fatalError600();
		}
		else {
			t6693rec.t6693Rec.set(itdmIO.getGenarea());
			t6693Search7110();
		}
	}

protected void t6693Search7110()
	{
		/*PARA*/
		for (wsaaIndex.set(1); !(statusOk.isTrue()
		|| isGT(wsaaIndex, 12)); wsaaIndex.add(1)){
			if (isEQ(wsaaBatckey.batcBatctrcde, t6693rec.trcode[wsaaIndex.toInt()])) {
				wsaaNewRegpStatus.set(t6693rec.rgpystat[wsaaIndex.toInt()]);
				statusOk.setTrue();
			}
		}
		if (statusNotOk.isTrue()) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h144);
			fatalError600();
		}
		/*EXIT*/
	}

protected void processRegp8000()
	{
		starts8000();
	}

protected void starts8000()
	{
		/*    Rewrite the original REGP with valid flag '2'.*/
		/*    If CLMDCLM-ANNYPIND = D, this means continue payments.*/
		/*    If CLMDCLM-ANNYPIND = E, this means terminate payments.*/
		/*    Check the field CLMDCLM-ANNYPIND and if it is 'E', alter the*/
		/*    status as appropriate from the T6693 check (this terminates*/
		/*    the REGP) - insert this new record with a valid flag '1'.*/
		/*    If it is 'D' alter the FINAL-PAYDATE and insert this new*/
		/*    record with a valid flag '1'*/
		regpclmIO.setValidflag("2");
		regpclmIO.setFormat(regpclmrec);
		/* MOVE REWRT                  TO REGPCLM-FUNCTION              */
		regpclmIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, regpclmIO);
		if (isNE(regpclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpclmIO.getParams());
			fatalError600();
		}
		/*    Set up the fields for new REGP record to be written, using a*/
		/*    different logical view of REGP.*/
		setupFields1000();
		regpIO.setValidflag("1");
		regpIO.setRecvdDate(varcom.vrcmMaxDate);
		regpIO.setIncurdt(varcom.vrcmMaxDate);
		regpIO.setFormat(regprec);
		regpIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/*    Get the next REGP.*/
		regpclmIO.setFunction(varcom.nextr);
		//performance improvement --  atiwari23 
		regpclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		regpclmIO.setFormat(regpclmrec);
		SmartFileCode.execute(appVars, regpclmIO);
		if (isNE(regpclmIO.getStatuz(), varcom.oK)
		&& isNE(regpclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(regpclmIO.getChdrcoy(), dthcpy.chdrChdrcoy)
		|| isNE(regpclmIO.getChdrnum(), dthcpy.chdrChdrnum)
		|| isNE(regpclmIO.getLife(), dthcpy.lifeLife)
		|| isNE(regpclmIO.getCoverage(), dthcpy.covrCoverage)
		|| isNE(regpclmIO.getRider(), dthcpy.covrRider)
		|| isNE(regpclmIO.getPlanSuffix(), wsaaPlnsfx))) {
			/*     MOVE REWRT              TO REGPCLM-FUNCTION              */
			/*     CALL 'REGPCLMIO'        USING REGPCLM-PARAMS             */
			/*     IF REGPCLM-STATUZ    NOT = O-K                           */
			/*        MOVE REGPCLM-PARAMS  TO SYSR-PARAMS                   */
			/*        PERFORM 600-FATAL-ERROR                               */
			/*     END-IF                                                   */
			regpclmIO.setStatuz(varcom.endp);
		}
	}

protected void setupFields1000()
	{
		para1010();
	}

protected void para1010()
	{
		/*    Set up the REGP logical file's fields from the REGPCLM*/
		/*    fields ready to be inserted as a new record.*/
		if (isEQ(clmdclmIO.getAnnypind(), "D")) {
			regpIO.setFinalPaydate(regpclmIO.getRevdte());
		}
		if (isEQ(clmdclmIO.getAnnypind(), "E")) {
			regpIO.setFinalPaydate(regpclmIO.getFinalPaydate());
			regpIO.setFinalPaydate(regpclmIO.getLastPaydate());//ILIFE-4885
		}
		regpIO.setRgpystat(wsaaNewRegpStatus);
		regpIO.setChdrcoy(regpclmIO.getChdrcoy());
		regpIO.setChdrnum(regpclmIO.getChdrnum());
		regpIO.setLife(regpclmIO.getLife());
		regpIO.setCoverage(regpclmIO.getCoverage());
		regpIO.setRider(regpclmIO.getRider());
		regpIO.setRgpynum(regpclmIO.getRgpynum());
		regpIO.setPlanSuffix(regpclmIO.getPlanSuffix());
		regpIO.setValidflag(regpclmIO.getValidflag());
		regpIO.setSacscode(regpclmIO.getSacscode());
		regpIO.setSacstype(regpclmIO.getSacstype());
		regpIO.setGlact(regpclmIO.getGlact());
		regpIO.setDebcred(regpclmIO.getDebcred());
		regpIO.setDestkey(regpclmIO.getDestkey());
		regpIO.setPaycoy(regpclmIO.getPaycoy());
		regpIO.setPayclt(regpclmIO.getPayclt());
		regpIO.setRgpymop(regpclmIO.getRgpymop());
		regpIO.setRegpayfreq(regpclmIO.getRegpayfreq());
		regpIO.setCurrcd(regpclmIO.getCurrcd());
		regpIO.setPymt(regpclmIO.getPymt());
		regpIO.setPrcnt(regpclmIO.getPrcnt());
		regpIO.setTotamnt(regpclmIO.getTotamnt());
		regpIO.setPayreason(regpclmIO.getPayreason());
		regpIO.setClaimevd(regpclmIO.getClaimevd());
		regpIO.setBankkey(regpclmIO.getBankkey());
		regpIO.setBankacckey(regpclmIO.getBankacckey());
		regpIO.setCrtdate(regpclmIO.getCrtdate());
		regpIO.setAprvdate(regpclmIO.getAprvdate());
		regpIO.setFirstPaydate(regpclmIO.getFirstPaydate());
		regpIO.setNextPaydate(regpclmIO.getNextPaydate());
		regpIO.setRevdte(regpclmIO.getRevdte());
		regpIO.setLastPaydate(regpclmIO.getLastPaydate());
		regpIO.setAnvdate(regpclmIO.getAnvdate());
		regpIO.setCancelDate(regpclmIO.getCancelDate());
		regpIO.setRgpytype(regpclmIO.getRgpytype());
		regpIO.setCrtable(regpclmIO.getCrtable());
		regpIO.setCertdate(regpclmIO.getCertdate());
		regpIO.setTermid(regpclmIO.getTermid());
		regpIO.setTranno(dthcpy.tranno);
		regpIO.setUser(dthcpy.user);
		regpIO.setTransactionDate(dthcpy.date_var);
		regpIO.setTransactionTime(dthcpy.time);
	}

protected void fatalError600()
	{
		error610();
		exit620();
	}

protected void error610()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit620()
	{
		dthcpy.status.set("BOMB");
		/*EXIT*/
	}
}
