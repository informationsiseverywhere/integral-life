package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:28
 * Description:
 * Copybook name: HCLAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hclakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hclaFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hclaKey = new FixedLengthStringData(256).isAPartOf(hclaFileKey, 0, REDEFINE);
  	public FixedLengthStringData hclaChdrcoy = new FixedLengthStringData(1).isAPartOf(hclaKey, 0);
  	public FixedLengthStringData hclaChdrnum = new FixedLengthStringData(8).isAPartOf(hclaKey, 1);
  	public FixedLengthStringData hclaLife = new FixedLengthStringData(2).isAPartOf(hclaKey, 9);
  	public FixedLengthStringData hclaCoverage = new FixedLengthStringData(2).isAPartOf(hclaKey, 11);
  	public FixedLengthStringData hclaRider = new FixedLengthStringData(2).isAPartOf(hclaKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(241).isAPartOf(hclaKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hclaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hclaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}