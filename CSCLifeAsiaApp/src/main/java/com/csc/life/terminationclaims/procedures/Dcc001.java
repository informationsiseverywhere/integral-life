package com.csc.life.terminationclaims.procedures;
/* File: Dcc001.java
 * Date: 29 August 2009 22:45:11
 * Author: Quipoz Limited
 *
 * Class transformed from DCC001.CBL
 * Copyright (2007) CSC Asia, all rights reserved.
 */

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import java.math.BigDecimal;
import java.util.List;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.dataaccess.CovrclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  DCC001 - Death Claim Calculation Subroutine No. 1
*  -------------------------------------------------
*
*  This program is an item entry on T6598, the death claim
*  subroutine method table. This is the calculation process for Sum
*  Insured.
*
*  The following linkage information is passed to this
*  subroutine:-
*             - DEATHREC
*
*  Processing
*  ----------
*
*  Set CDTH-STATUS to O.K.
*
*  If it is the first time through this routine:-
*      - Begn on  the COVR records for this 'dead' life and
*        return the sum assured
*      - access T5687 for the Calculation method
*      - access T6640 for the Bonus Method
*      - access T6639 for the Bonus Programs
*      - set the first time flag to false.
*
*  If the Calculation method returned from T5687 is not blank:-
*      - set CDTH-ACTUAL-VAL to the value accumulated in sum
*        assured
*      - set CDTH-FIELD-TYPE to 'S'
*  else
*  If the Bonus calculation method returned from T6640 is not blank
*  and the Reversionary Bonus Program on T6639 is not blank:-
*      - set CDTH-FIELD-TYPE to 'B'
*      - set CDTH-ACTUAL-VAL to the ACBL balance for the T5645
*        Sacscode and Sacstype.
*  else
*  If the Bonus calculation method returned from T6640 is not blank
*  and the Terminal Bonus Program on T6639 is not blank:-
*      - set CDTH-ACTUAL-VAL to the value calculated in the called
*        program
*      - set CDTH-FIELD-TYPE to 'T'
*  else
*  If the Bonus calculation method returned from T6640 is not blank
*  and the Additional Bonus Program on T6639 is not blank:-
*      - set CDTH-ACTUAL-VAL to the value calculated in the called
*        program
*      - set CDTH-FIELD-TYPE to 'X'
*  else
*  If the Bonus calculation method returned from T6640 is not blank
*  and the Interim Bonus Program on T6639 is not blank:-
*      - set CDTH-ACTUAL-VAL to the value calculated in the called
*        program
*      - set CDTH-FIELD-TYPE to 'M'
*  else
*  If the Bonus calculation method returned from T6640 is not blank
*  and  the Low  Cost  Rider field and subroutine on T6640 are  not
*  blank then call the Low Cost Rider subroutine:-
*      - set CDTH-ACTUAL-VAL to the value calculated in the called
*        program
*      - set CDTH-FIELD-TYPE to 'L'
*  else
*      get the Next COVR record.
*
*  Next COVR Record Section.
*      If end of file is reached:-
*         - set first time through to true again
*         - set status = ENDP.
*
*      - return the sum assured
*      - access T5687 for the Calculation method
*      - access T6640 for the Bonus Method
*      - access T6639 for the Bonus Programs
*      - perform processing for methods as for first coverage.
*
*****************************************************************
* </pre>
*/
public class Dcc001 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "DCC001";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private String wsaaValidStatuz = "";
	private String g408 = "G408";
	private String e652 = "E652";
		/* TABLES */
	private String t5687 = "T5687";
	private String t5679 = "T5679";
	private String t5645 = "T5645";
	private String t6639 = "T6639";
	private String t6640 = "T6640";
	private String t6644 = "T6644";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String acblrec = "ACBLREC";
	private String chdrlifrec = "CHDRLIFREC";
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaRvBonusBon = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaIntBon = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTrmBon = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaAddBon = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaBonusSum = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaT6639Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6639Dcmeth = new FixedLengthStringData(4).isAPartOf(wsaaT6639Key, 0);
	private FixedLengthStringData wsaaPremStat = new FixedLengthStringData(2).isAPartOf(wsaaT6639Key, 4);

	private FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaSwitch, "Y");
	private Validator nextPol = new Validator(wsaaSwitch, "X");
	private String wsaaProcessedRvBon = "N";
	private FixedLengthStringData wsaaElement = new FixedLengthStringData(4);
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
	private Bonusrec bonusrec = new Bonusrec();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Claim Coverage Record*/
	private CovrclmTableDAM covrclmIO = new CovrclmTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	protected Deathrec deathrec = getDeathrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6639rec t6639rec = new T6639rec();
	private T6640rec t6640rec = new T6640rec();
	protected Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	private ExternalisedRules er = new ExternalisedRules();
	private ItemDAO itempfDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;

	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpfList;
	protected Covrpf covrpf = null;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr101,
		exit105,
		para111,
		nextr112,
		exit115,
		exit125,
		covr272,
		exit9020
	}

	public Dcc001() {
		super();
	}

	public Deathrec getDeathrec() {
		return new Deathrec();
	}
public void mainline(Object... parmArray)
	{
		deathrec.deathRec = convertAndSetParam(deathrec.deathRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		deathrec.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		if (firstTime.isTrue()) {
			wsaaSumins.set(ZERO);
			wsaaIntBon.set(ZERO);
			wsaaTrmBon.set(ZERO);
			wsaaAddBon.set(ZERO);
			wsaaBonusSum.set(ZERO);
			wsaaElement.set(SPACES);
			readComponentsAndTables100();
		}
		if (isEQ(deathrec.status,varcom.oK)) {
			checkDeathMethod210(covrpf);
		}
		/*EXIT*/
		exitProgram();
	}

/*protected void readComponentsAndTables100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para101();
				}
				case nextr101: {
					nextr101();
					readT6640102();
					readT6639103();
				}
				case exit105: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
*/
protected void para101()
	{
		if (!firstTime.isTrue()) {
			covrIO.setFunction(varcom.nextr);
			goTo(GotoLabel.nextr101);
		}
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(deathrec.chdrChdrcoy);
		covrIO.setChdrnum(deathrec.chdrChdrnum);
		covrIO.setLife(deathrec.lifeLife);
		covrIO.setCoverage(deathrec.covrCoverage);
		covrIO.setRider(deathrec.covrRider);
		covrIO.setPlanSuffix(0);
		covrIO.setFunction(varcom.begn);
	}

protected void readComponentsAndTables100()
{
	if(firstTime.isTrue())
	{
		covrpf = new Covrpf();
		covrpf.setChdrcoy(deathrec.chdrChdrcoy.toString());
		covrpf.setChdrnum(deathrec.chdrChdrnum.toString());
		covrpf.setLife(deathrec.lifeLife.toString());
		covrpf.setCoverage(deathrec.covrCoverage.toString());
		covrpf.setRider(deathrec.covrRider.toString());//ILIFE-4595
		covrpf.setPlanSuffix(0);
		covrpf.setValidflag("1");
		covrpfList = covrpfDAO.selectCovrRecord(covrpf);
		if(covrpfList != null && covrpfList.size() > 0)
		{
			for(Covrpf covr : covrpfList)
			{
				deathrec.actualVal.set(ZERO);
				accumSumins110(covr);
				wsaaPlan.set(covr.getPlanSuffix());
				deathrec.element.set(wsaaPlansuff);
				wsaaElement.set(wsaaPlansuff);
				deathrec.actualVal.set(wsaaSumins);
				if (isEQ(deathrec.status,varcom.oK)) {
					readT5687120();
				}
				wsaaSwitch.set("N");
				readT6640102();
				readT6639103(covr);
				covrpf = covr;

			}

		}
		else
		{
			deathrec.status.set(varcom.endp);
			wsaaSwitch.set("Y");
			wsaaProcessedRvBon = "N";
		}
	}
	else
	{
		deathrec.status.set(varcom.endp);
		wsaaSwitch.set("Y");
		wsaaProcessedRvBon = "N";
	}

}


/*protected void nextr101()
	{
		deathrec.actualVal.set(ZERO);
		accumSumins110();
		if (isEQ(covrIO.getStatuz(),varcom.endp)) {
			deathrec.status.set(varcom.endp);
			wsaaSwitch.set("Y");
			wsaaProcessedRvBon = "N";
			goTo(GotoLabel.exit105);
		}
		wsaaPlan.set(covrIO.getPlanSuffix());
		deathrec.element.set(wsaaPlansuff);
		wsaaElement.set(wsaaPlansuff);
		deathrec.actualVal.set(wsaaSumins);
		if (isEQ(deathrec.status,varcom.oK)) {
			readT5687120();
		}
		wsaaSwitch.set("N");
	}*/

protected void readT6640102()
	{
	    itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(deathrec.chdrChdrcoy.toString());
		itempf.setItemitem(deathrec.crtable.toString());
		itempf.setItemtabl(t6640);
		itempf.setItmfrm(deathrec.effdate.getbigdata());
		itempf.setItmto(deathrec.effdate.getbigdata());

		List<Itempf> itempfList6640 = itempfDAO.findByItemDates(itempf);

		if(itempfList6640.size() > 0) {
			for (Itempf it : itempfList6640) {
				t6640rec.t6640Rec.set(StringUtil.rawToString(it.getGenarea()));

			}
		}

	}

protected void readT6639103(Covrpf covr)
	{
		if (isEQ(t6640rec.revBonusMeth,SPACES)) {
			t6639rec.t6639Rec.set(SPACES);
			return;
		}
		wsaaT6639Dcmeth.set(t5687rec.dcmeth);
		wsaaPremStat.set(covr.getPstatcode());
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(deathrec.chdrChdrcoy.toString());
		itempf.setItemitem(wsaaT6639Key.toString());
		itempf.setItemtabl(t6639);
		itempf.setItmfrm(deathrec.effdate.getbigdata());
		itempf.setItmto(deathrec.effdate.getbigdata());

		List<Itempf> itempfList6639 = itempfDAO.findByItemDates(itempf);
		if(itempfList6639.size() > 0) {
			for (Itempf it : itempfList6639) {
				t6639rec.t6639Rec.set(StringUtil.rawToString(it.getGenarea()));

			}
		}

	}

protected void accumSumins110(Covrpf covr)
	{
		/*GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case para111: {
					para111();
				}
				case nextr112: {
					nextr112();
				}
				case exit115: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}

		}*/
	checkT5679300(covr);
	if (isEQ(wsaaValidStatuz,"Y")) {
		/*NEXT_SENTENCE*/
	}
	else {
		return;
	}
	if (isEQ(covr.getValidflag(),"1")) {
		wsaaSumins.set(covr.getSumins());
		return;
	}

	}

/*protected void para111()
	{

	//performance improvement --  atiwari23
	/*covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		if (isNE(covrIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(covrIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(covrIO.getLife(),deathrec.lifeLife)
		|| isNE(covrIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(covrIO.getRider(),deathrec.covrRider)
		|| isEQ(covrIO.getStatuz(),varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit115);
		}

		checkT5679300();
		if (isEQ(wsaaValidStatuz,"Y")) {
			/*NEXT_SENTENCE
		}
		else {
			goTo(GotoLabel.nextr112);
		}
		if (isEQ(covrIO.getValidflag(),"1")) {
			wsaaSumins.set(covrIO.getSumins());
			goTo(GotoLabel.exit115);
		}
	}*/

protected void nextr112()
	{
		covrIO.setFunction(varcom.nextr);
		goTo(GotoLabel.para111);
	}

protected void readT5687120()
	{
		try {
			read121();
		}
		catch (GOTOException e){
		}
	}

protected void read121()
	{

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(deathrec.chdrChdrcoy.toString());
		itempf.setItemitem(deathrec.crtable.toString());
		itempf.setItemtabl(t5687);
		itempf.setItmfrm(varcom.vrcmMaxDate.getbigdata());
		itempf.setItmto(varcom.vrcmMaxDate.getbigdata());
		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {
				t5687rec.t5687Rec.set(StringUtil.rawToString(it.getGenarea()));

			}
		}
		descIO.setDescitem(deathrec.crtable);
		descIO.setDesctabl(t5687);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(deathrec.chdrChdrcoy);
		descIO.setLanguage("E");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			deathrec.description.set(descIO.getShortdesc());
		}
		else {
			deathrec.description.fill("?");
		}
	}

/*protected void checkMethods200()
	{
		checkDeathMethod210();

	}*/

protected void checkDeathMethod210(Covrpf covr)
	{
		deathrec.element.set(wsaaElement);
		if (isNE(t5687rec.dcmeth,SPACES)) {
			deathrec.actualVal.set(wsaaSumins);
			deathrec.fieldType.set("S");
			t5687rec.dcmeth.set(SPACES);
		}
		else {
			if (isNE(t6640rec.revBonusMeth,SPACES)
			&& isEQ(wsaaProcessedRvBon,"N")) {
				readT5645250();
				deathrec.fieldType.set("B");
				descIO.setDescitem("REV");
				readT6644600();
				wsaaProcessedRvBon = "Y";
			}
			else {
				if (isNE(t6640rec.revBonusMeth,SPACES)
				&& isNE(t6639rec.intBonusProg,SPACES)) {
					deathrec.fieldType.set("M");
					callBonusSubroutine260(covr);
					deathrec.actualVal.set(bonusrec.intBonusSa);
					deathrec.actualVal.add(bonusrec.intBonusBon);
					wsaaIntBon.set(deathrec.actualVal);
					descIO.setDescitem("INT");
					readT6644600();
					t6639rec.intBonusProg.set(SPACES);
				}
				else {
					if (isNE(t6640rec.revBonusMeth,SPACES)
					&& isNE(t6639rec.trmBonusProg,SPACES)) {
						deathrec.fieldType.set("T");
						callBonusSubroutine260(covr);
						deathrec.actualVal.set(bonusrec.trmBonusSa);
						deathrec.actualVal.add(bonusrec.trmBonusBon);
						wsaaTrmBon.set(deathrec.actualVal);
						descIO.setDescitem("TRM");
						readT6644600();
						t6639rec.trmBonusProg.set(SPACES);
					}
					else {
						if (isNE(t6640rec.revBonusMeth,SPACES)
						&& isNE(t6639rec.addBonusProg,SPACES)) {
							deathrec.fieldType.set("X");
							callBonusSubroutine260(covr);
							deathrec.actualVal.set(bonusrec.extBonusSa);
							deathrec.actualVal.add(bonusrec.extBonusBon);
							wsaaAddBon.set(deathrec.actualVal);
							descIO.setDescitem("EXT");
							readT6644600();
							t6639rec.addBonusProg.set(SPACES);
						}
						else {
							if (isNE(t6640rec.revBonusMeth,SPACES)
							&& isNE(t6640rec.lcsubr,SPACES)
							&& isNE(t6640rec.lcrider,SPACES)) {
								descIO.setDescitem("LOW");
								readT6644600();
								processCovrRider270(covr);
							}
							else {
								wsaaSwitch.set("X");
								readComponentsAndTables100();
							}
						}
					}
				}
			}
		}
	}

protected void readT5645250()
	{
		go251();
	}

protected void go251()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(deathrec.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		acblIO.setParams(SPACES);
		wsaaRldgChdrnum.set(deathrec.chdrChdrnum);
		wsaaRldgLife.set(deathrec.lifeLife);
		wsaaRldgCoverage.set(deathrec.covrCoverage);
		wsaaRldgRider.set(deathrec.covrRider);
		wsaaPlan.set(covrIO.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaRldgacct);
		acblIO.setRldgcoy(deathrec.chdrChdrcoy);
		getContractInfo280();
		acblIO.setOrigcurr(chdrlifIO.getCntcurr());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(),varcom.oK))
		&& (isNE(acblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			deathrec.actualVal.set(ZERO);
		}
		else {
			deathrec.actualVal.set(acblIO.getSacscurbal());
		}
	}

protected void callBonusSubroutine260(Covrpf covr)
	{

		bonusrec.bonusRec.set(SPACES);
		bonusrec.rvBonusSa.set(ZERO);
		bonusrec.trmBonusSa.set(ZERO);
		bonusrec.intBonusSa.set(ZERO);
		bonusrec.extBonusSa.set(ZERO);
		bonusrec.rvBonusBon.set(ZERO);
		bonusrec.trmBonusBon.set(ZERO);
		bonusrec.intBonusBon.set(ZERO);
		bonusrec.rvBalLy.set(ZERO);
		bonusrec.extBonusBon.set(ZERO);
		bonusrec.batckey.set(deathrec.batckey);
		bonusrec.effectiveDate.set(deathrec.effdate);
		if (isEQ(covr.getUnitStatementDate(),varcom.vrcmMaxDate)) {
			bonusrec.unitStmtDate.set(covr.getCrrcd());
		}
		else {
			bonusrec.unitStmtDate.set(covr.getUnitStatementDate());
		}
		bonusrec.language.set(deathrec.language);
		bonusrec.tranno.set(covr.getTranno());
		getContractInfo280();
		bonusrec.cnttype.set(chdrlifIO.getCnttype());
		bonusrec.cntcurr.set(chdrlifIO.getCntcurr());
		bonusrec.chdrChdrcoy.set(deathrec.chdrChdrcoy);
		bonusrec.chdrChdrnum.set(deathrec.chdrChdrnum);
		bonusrec.lifeLife.set(deathrec.lifeLife);
		bonusrec.covrCoverage.set(deathrec.covrCoverage);
		bonusrec.covrRider.set(deathrec.covrRider);
		bonusrec.plnsfx.set(covr.getPlanSuffix());
		bonusrec.premStatus.set(covr.getPstatcode());
		bonusrec.crtable.set(covr.getCrtable());
		bonusrec.sumin.set(covr.getSumins());
		bonusrec.allocMethod.set("C");
		bonusrec.bonusCalcMeth.set(t6640rec.revBonusMeth);
		bonusrec.user.set(covr.getUser());
		bonusrec.bonusPeriod.set(ZERO);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covr.getCrrcd());
		datcon3rec.intDate2.set(deathrec.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		bonusrec.termInForce.set(datcon3rec.freqFactor);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covr.getCrrcd());
		datcon3rec.intDate2.set(covr.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError9000();
		}
		bonusrec.duration.set(datcon3rec.freqFactor);
		if (isEQ(deathrec.fieldType,"M")) {
			callProgram(t6639rec.intBonusProg, bonusrec.bonusRec);
			checkStatus290();
		}
		else {
			if (isEQ(deathrec.fieldType,"T")) {
				callProgram(t6639rec.trmBonusProg, bonusrec.bonusRec);
				checkStatus290();
			}
			else {
				if (isEQ(deathrec.fieldType,"X")) {
					callProgram(t6639rec.addBonusProg, bonusrec.bonusRec);
					checkStatus290();
				}
				else {
					syserrrec.params.set(bonusrec.bonusRec);
					fatalError9000();
				}
			}
		}
	}

protected void processCovrRider270(Covrpf covr)
	{
		/*GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					go271();
				}
				case covr272: {
					covr272();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go271()
	{*/
		compute(wsaaBonusSum, 3).setRounded((add(add(wsaaTrmBon,wsaaIntBon),wsaaAddBon)));
		/*covrclmIO.setParams(SPACES);
		covrclmIO.setChdrcoy(covrIO.getChdrcoy());
		covrclmIO.setChdrnum(covrIO.getChdrnum());
		covrclmIO.setLife(covrIO.getLife());
		covrclmIO.setCoverage(covrIO.getCoverage());
		covrclmIO.setRider(ZERO);
		covrclmIO.setPlanSuffix(ZERO);
		covrclmIO.setFunction(varcom.begn);*/
		covrpf = new Covrpf();
		covrpf.setChdrcoy(covr.getChdrcoy());/* IJTI-1523 */
		covrpf.setChdrnum(covr.getChdrnum());/* IJTI-1523 */
		covrpf.setLife(covr.getLife());/* IJTI-1523 */
		covrpf.setCoverage(covr.getCoverage());/* IJTI-1523 */
		covrpf.setRider("0");
		covrpf.setPlanSuffix(0);

		List<Covrpf> covrpfList1 = covrpfDAO.selectCovrRecord(covrpf);
		if(covrpfList1!= null && covrpfList1.size() > 0)
		{
			for(Covrpf covr1 : covrpfList1)
			{
				if(!AppVars.getInstance().getAppConfig().isVpmsEnable()) {
					if (isGT(covr1.getSumins(),wsaaBonusSum)) {
						compute(wsaaBonusSum, 3).setRounded((sub(covr1.getSumins(),wsaaBonusSum)));
					}
					if (isLT(covr1.getSumins(),wsaaBonusSum)) {
						wsaaBonusSum.set(ZERO);
					}
					deathrec.actualVal.set(wsaaBonusSum);
				}
				else{
					bonusrec.sumin.set(covr1.getSumins());
					bonusrec.trmBonusSa.set(wsaaTrmBon);
					bonusrec.intBonusSa.set(wsaaIntBon);
					bonusrec.extBonusSa.set(wsaaAddBon);
					callProgram("VPMDCC001", deathrec, bonusrec);
					deathrec.status.set(varcom.endp);
				}
				/*Ticket #ILIFE-2005 - End*/
				t6640rec.lcsubr.set(SPACES);
				t6640rec.lcrider.set(SPACES);
				deathrec.fieldType.set("L");
			}
		}

	}

protected void covr272()
	{
	//performance improvement --  atiwari23
	covrclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	covrclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE");
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(),varcom.oK)
		&& isNE(covrclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrclmIO.getParams());
			syserrrec.statuz.set(covrclmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(covrclmIO.getChdrcoy(),covrIO.getChdrcoy())
		|| isNE(covrclmIO.getChdrnum(),covrIO.getChdrnum())
		|| isNE(covrclmIO.getLife(),covrIO.getLife())
		|| isNE(covrclmIO.getCoverage(),covrIO.getCoverage())
		|| isEQ(covrclmIO.getStatuz(),varcom.endp)) {
			covrclmIO.setChdrcoy(covrIO.getChdrcoy());
			covrclmIO.setChdrnum(covrIO.getChdrnum());
			covrclmIO.setLife(covrIO.getLife());
			covrclmIO.setCoverage(covrIO.getCoverage());
			syserrrec.params.set(covrclmIO.getParams());
			syserrrec.statuz.set(covrclmIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(covrclmIO.getCrtable(),t6640rec.lcrider)
		&& isEQ(covrclmIO.getPlanSuffix(),covrIO.getPlanSuffix())) {
			/*NEXT_SENTENCE*/
		}
		else {
			covrclmIO.setFunction(varcom.nextr);
			goTo(GotoLabel.covr272);
		}
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization
		changes related to TRM calculation] Start
		*/
		//ILIFE-3389 SUM Screen bomb (Updated 1 line)
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable()  && er.isCallExternal("VPMDCC001") && er.isExternalized(bonusrec.cnttype.toString(), bonusrec.crtable.toString())))
		{
			if (isGT(covrclmIO.getSumins(),wsaaBonusSum)) {
				compute(wsaaBonusSum, 3).setRounded((sub(covrclmIO.getSumins(),wsaaBonusSum)));
			}
			if (isLT(covrclmIO.getSumins(),wsaaBonusSum)) {
				wsaaBonusSum.set(ZERO);
			}
			deathrec.actualVal.set(wsaaBonusSum);
		}
		else{
			bonusrec.sumin.set(covrclmIO.getSumins());
			bonusrec.trmBonusSa.set(wsaaTrmBon);
			bonusrec.intBonusSa.set(wsaaIntBon);
			bonusrec.extBonusSa.set(wsaaAddBon);
			callProgram("VPMDCC001", deathrec, bonusrec);
			deathrec.status.set(varcom.endp);
		}
		/*Ticket #ILIFE-2005 - End*/
		t6640rec.lcsubr.set(SPACES);
		t6640rec.lcrider.set(SPACES);
		deathrec.fieldType.set("L");
	}

protected void getContractInfo280()
	{
		/*PARA*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(deathrec.chdrChdrcoy);
		chdrlifIO.setChdrnum(deathrec.chdrChdrnum);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void checkStatus290()
	{
		/*BONUS*/
		if (isNE(bonusrec.statuz,varcom.oK)) {
			syserrrec.params.set(bonusrec.bonusRec);
			syserrrec.statuz.set(bonusrec.statuz);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void checkT5679300(Covrpf covr)
	{
		readT5679310();
		doCheck340(covr);
	}

protected void readT5679310()
	{
		wsaaBatckey.set(deathrec.batckey);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(deathrec.chdrChdrcoy);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void doCheck340(Covrpf covr)
	{
		wsaaValidStatuz = "N";
		if (isEQ(covr.getRider(),"00")) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub.add(1)){
				checkCoverage400(covr);
			}
		}
		else {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub.add(1)){
				checkRider500(covr);
			}
		}
		/*EXIT*/
	}

protected void checkCoverage400(Covrpf covr)
	{
		/*COVERAGE*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()],covr.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()],covr.getPstatcode())) {
					wsaaValidStatuz = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void checkRider500(Covrpf covr)
	{
		/*COVERAGE*/
		if (isEQ(t5679rec.ridRiskStat[wsaaSub.toInt()],covr.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub.add(1)){
				if (isEQ(t5679rec.ridPremStat[wsaaSub.toInt()],covr.getPstatcode())) {
					wsaaValidStatuz = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void readT6644600()
	{
		read601();
	}

protected void read601()
	{
		descIO.setDesctabl(t6644);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(deathrec.chdrChdrcoy);
		descIO.setLanguage(deathrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			deathrec.description.set(descIO.getShortdesc());
		}
		else {
			deathrec.description.fill("?");
		}
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		deathrec.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
