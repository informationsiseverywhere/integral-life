package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5318
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class S5318ScreenVars extends SmartVarModel { 
	//ILIFE-1137
	//added for death claim flexibility BRSO
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	//public FixedLengthStringData dataArea = new FixedLengthStringData(963);
	//added for death claim flexibility BRSO
	//public FixedLengthStringData dataFields = new FixedLengthStringData(451).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData causeofdth = DD.causeofdth.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData clamamt = DD.clamamt.copyToZonedDecimal().isAPartOf(dataFields,4);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,21);
	public ZonedDecimalData dtofdeath = DD.dtofdeath.copyToZonedDecimal().isAPartOf(dataFields,24);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,32);
	public ZonedDecimalData estimateTotalValue = DD.estimtotal.copyToZonedDecimal().isAPartOf(dataFields,40);
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,57);
	public ZonedDecimalData interest = DD.interest.copyToZonedDecimal().isAPartOf(dataFields,58);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,71);
	public ZonedDecimalData ofcharge = DD.ofcharge.copyToZonedDecimal().isAPartOf(dataFields,101);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,114);
	public ZonedDecimalData policyloan = DD.policyloan.copyToZonedDecimal().isAPartOf(dataFields,131);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,148);
	public ZonedDecimalData tdbtamt = DD.tdbtamt.copyToZonedDecimal().isAPartOf(dataFields,152);
	public ZonedDecimalData totclaim = DD.totclaim.copyToZonedDecimal().isAPartOf(dataFields,169);
	public ZonedDecimalData zrcshamt = DD.zrcshamt.copyToZonedDecimal().isAPartOf(dataFields,186);
	public FixedLengthStringData asterisk = DD.asterisk.copy().isAPartOf(dataFields,199);
	public FixedLengthStringData astrsk = DD.astrsk.copy().isAPartOf(dataFields,200);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,201);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,209);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,217);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,220);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,228);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,258);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,266);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,313);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,321);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,368);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,376);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,423);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,433);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,441);
	//ILIFE-1137
	//added for death claim flexibility BRSO
	public ZonedDecimalData susamt = DD.susamt.copyToZonedDecimal().isAPartOf(dataFields,451);
	public ZonedDecimalData nextinsamt = DD.nextinsamt.copyToZonedDecimal().isAPartOf(dataFields,468);
	
	public ZonedDecimalData zhldclmv = DD.zhldclmv.copyToZonedDecimal().isAPartOf(dataFields,485);//BRD-34
	public ZonedDecimalData zhldclma = DD.zhldclma.copyToZonedDecimal().isAPartOf(dataFields,502);//BRD-34
	public FixedLengthStringData bnfying = DD.bnfying.copy().isAPartOf(dataFields,519);
	public ZonedDecimalData interestrate = DD.intrat.copyToZonedDecimal().isAPartOf(dataFields,520);  
	public FixedLengthStringData claimnumber = DD.claimnumber.copy().isAPartOf(dataFields,528);
	public FixedLengthStringData aacct = DD.aacct.copy().isAPartOf(dataFields,537);
	public FixedLengthStringData claimnotes = DD.claimnotes.copy().isAPartOf(dataFields,551);
	 public FixedLengthStringData investres = DD.investres.copy().isAPartOf(dataFields,552);
	public FixedLengthStringData claimTyp = DD.clamtyp.copy().isAPartOf(dataFields,553);
	public FixedLengthStringData claimStat = DD.clamstat.copy().isAPartOf(dataFields,555);
	public PackedDecimalData contactDate = DD.contactDate.copy().isAPartOf(dataFields,557);
	////added for death claim flexibility BRSO
	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(128).isAPartOf(dataArea, 451);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData causeofdthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clamamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData dtofdeathErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData estimtotalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData interestErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ofchargeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData policyloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData tdbtamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData totclaimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData zrcshamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData asteriskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData astrskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	//ILIFE-1137
	//added for death claim flexibility BRSO
	public FixedLengthStringData susamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData nextinsamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	
	public FixedLengthStringData zhldclmvErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);//BRD-34
	public FixedLengthStringData zhldclmaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);//BRD-34
	public FixedLengthStringData bnfyingErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData interestrateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData claimnumberErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	 public FixedLengthStringData aacctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
    public FixedLengthStringData claimnotesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
    public FixedLengthStringData investresErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
    public FixedLengthStringData claimTypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData claimStatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData contactDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	//added for death claim
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(384).isAPartOf(dataArea, 579);
	public FixedLengthStringData[] causeofdthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clamamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] dtofdeathOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] estimtotalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] interestOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ofchargeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] policyloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] tdbtamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] totclaimOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] zrcshamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] asteriskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] astrskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	//ILIFE-1137
	public FixedLengthStringData[] susamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] nextinsamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	
	public FixedLengthStringData[] zhldclmvOut = FLSArrayPartOfStructure(12, 1, outputIndicators,408);//BRD-34
	public FixedLengthStringData[] zhldclmaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);//BRD-34
	public FixedLengthStringData[] bnfyingOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] interestrateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] claimnumberOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	 public FixedLengthStringData[] aacctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] claimnotesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	 public FixedLengthStringData[] investresOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 494);
	public FixedLengthStringData[] claimTypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 506);
	public FixedLengthStringData[] claimStatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 518);
	public FixedLengthStringData[] contactDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 530);
	public FixedLengthStringData subfileArea = new FixedLengthStringData(getSubfileAreaSize());
	public FixedLengthStringData subfileFields = new FixedLengthStringData(getSubfileFieldsSize()).isAPartOf(subfileArea, 0);
	public ZonedDecimalData actvalue = DD.actvalue.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData estMatValue = DD.emv.copyToZonedDecimal().isAPartOf(subfileFields,22);
	public ZonedDecimalData hactual = DD.hactual.copyToZonedDecimal().isAPartOf(subfileFields,39);
	public FixedLengthStringData hcnstcur = DD.hcnstcur.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData hcover = DD.hcover.copy().isAPartOf(subfileFields,59);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,61);
	public ZonedDecimalData hemv = DD.hemv.copyToZonedDecimal().isAPartOf(subfileFields,65);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(subfileFields,82);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,84);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(subfileFields,86);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(subfileFields,96);
	public FixedLengthStringData vfund = DD.vfund.copy().isAPartOf(subfileFields,97);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(getErrorSubfileSize()).isAPartOf(subfileArea, getSubfileFieldsSize());
	public FixedLengthStringData actvalueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData emvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hactualErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hcnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hcoverErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData hemvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData shortdsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData typeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData vfundErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(getOutputSubfileSize()).isAPartOf(subfileArea, getErrorSubfileSize()+getSubfileFieldsSize());
	public FixedLengthStringData[] actvalueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] emvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hactualOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hcnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hcoverOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] hemvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] typeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] vfundOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, getSubfileFieldsSize()+getErrorSubfileSize()+getOutputSubfileSize());
	//public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData dtofdeathDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData contactDateDisp = new FixedLengthStringData(10);

	public LongData S5318screensflWritten = new LongData(0);
	public LongData S5318screenctlWritten = new LongData(0);
	public LongData S5318screenWritten = new LongData(0);
	public LongData S5318protectWritten = new LongData(0);
	public GeneralTable s5318screensfl = new GeneralTable(AppVars.getInstance());
	
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21};
	public static int[] affectedInds = new int[] {};
	public FixedLengthStringData defintflag = new FixedLengthStringData(1); 

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5318screensfl;
	}

	public S5318ScreenVars() {
		super();
		initialiseScreenVars();
	}

	{
		screenIndicArea = DD.indicarea.copy();
	}
	
	protected void initialiseScreenVars() {
		fieldIndMap.put(dtofdeathOut,new String[] {"01","09","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"02","10","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(causeofdthOut,new String[] {"03","09","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(otheradjstOut,new String[] {"07","09","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupflgOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"05","09","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(longdescOut,new String[] {"06","09","-06",null, null, null, null, null, null, null, null, null});
        fieldIndMap.put(bnfyingOut,new String[] {"21",null, "-21","22", null, null, null, null, null, null, null, null});
        fieldIndMap.put(claimnumberOut,new String[] {"23",null, "-23","24", null, null, null, null, null, null, null, null});
		fieldIndMap.put(aacctOut,new String[] {"25",null, "-25","26", null, null, null, null, null, null, null, null});
	    fieldIndMap.put(claimnotesOut,new String[] {"27",null, "-27","28", null, null, null, null, null, null, null, null});
	    fieldIndMap.put(investresOut,new String[] {"29",null, "-29","30", null, null, null, null, null, null, null, null});
	    fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
	    fieldIndMap.put(contactDateOut,new String[] {"83","84","-83","82", null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimStatOut,new String[] {"78","79","-78","80",null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimTypOut,new String[] {"85", "86", "-85","81",null, null, null, null, null, null, null, null});
	   
	    
		screenSflDateFields =getscreenSflDateFields();
		screenSflDateErrFields = getscreenSflDateErrFields();
		screenSflDateDispFields = getscreenSflDateDispFields();
		
		screenSflFields = getscreenLSflFields();
		screenSflOutFields = getscreenSflOutFields();
		screenSflErrFields = getscreenSflErrFields();
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5318screen.class;
		screenSflRecord = S5318screensfl.class;
		screenCtlRecord = S5318screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5318protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5318screenctl.lrec.pageSubfile);
	}
	
	public int getDataAreaSize()
	{
		return 1287;
	}
	
	public int getDataFieldsSize()
	{
		return 565;
	}
	public int getErrorIndicatorSize()
	{
		return 180;
	}
	public int getOutputFieldSize()
	{
		return 542;
	}
	
	public BaseData[] getscreenFields()
	{
		
		return new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, asterisk, astrsk, dtofdeath, policyloan, effdate, causeofdth, otheradjst, fupflg, estimateTotalValue, clamamt, reasoncd, longdesc, currcd, interest, ofcharge, totclaim, zrcshamt, tdbtamt, susamt, nextinsamt, zhldclmv, zhldclma, bnfying,interestrate,claimnumber,aacct,claimnotes,investres,claimStat,claimTyp,contactDate};
		
	}
	
	public BaseData[][] getscreenOutFields()
	{
		
		return new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, asteriskOut, astrskOut, dtofdeathOut, policyloanOut, effdateOut, causeofdthOut, otheradjstOut, fupflgOut, estimtotalOut, clamamtOut, reasoncdOut, longdescOut, currcdOut, interestOut, ofchargeOut, totclaimOut, zrcshamtOut, tdbtamtOut, susamtOut, nextinsamtOut, zhldclmvOut, zhldclmaOut, bnfyingOut,interestrateOut,claimnumberOut,aacctOut,claimnotesOut,investresOut,claimStatOut,claimTypOut,contactDateOut};
		
	}
	
	public BaseData[] getscreenErrFields()
	{
		
		return new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, asteriskErr, astrskErr, dtofdeathErr, policyloanErr, effdateErr, causeofdthErr, otheradjstErr, fupflgErr, estimtotalErr, clamamtErr, reasoncdErr, longdescErr, currcdErr, interestErr, ofchargeErr, totclaimErr, zrcshamtErr, tdbtamtErr, susamtErr, nextinsamtErr , zhldclmvErr,zhldclmaErr, bnfyingErr,interestrateErr,claimnumberErr,aacctErr,claimnotesErr,investresErr,claimStatErr,claimTypErr,contactDateErr};
		
	}	
	
	public int getSubfileAreaSize()
	{
		return 327;
	}
	
	public int getSubfileFieldsSize()
	{
		return 101;
	}

	public int getErrorSubfileSize()
	{
		return 56;
	}

	public int getOutputSubfileSize()
	{
		return 168;
	}

	public BaseData[] getscreenLSflFields()
	{
		return new BaseData[] {hemv, hcover, hcrtable, hcnstcur, hactual, coverage, rider, liencd, actvalue, estMatValue, shortds, vfund, fieldType, cnstcur};
	}
	
	public BaseData[][] getscreenSflOutFields()
	{
		return new BaseData[][] {hemvOut, hcoverOut, hcrtableOut, hcnstcurOut, hactualOut, coverageOut, riderOut, liencdOut, actvalueOut, emvOut, shortdsOut, vfundOut, typeOut, cnstcurOut};
	}
	
	
	public BaseData[] getscreenSflErrFields()
	{
		return new BaseData[] {hemvErr, hcoverErr, hcrtableErr, hcnstcurErr, hactualErr, coverageErr, riderErr, liencdErr, actvalueErr, emvErr, shortdsErr, vfundErr, typeErr, cnstcurErr};
	}
	

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {occdate, ptdate, btdate, dtofdeath, effdate, contactDate};
	}
	
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, dtofdeathDisp, effdateDisp, contactDateDisp};
	}
	

	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {occdateErr, ptdateErr, btdateErr, dtofdeathErr, effdateErr, contactDateErr};
	}
	
	public BaseData[] getscreenSflDateFields()
	{
		return new BaseData[] {};
	}
	
	public BaseData[] getscreenSflDateDispFields()
	{
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenSflDateErrFields()
	{
		return new BaseData[] {};
	}


	public static int[] getScreenSflPfInds()
	{
		return pfInds;
	}

	public static int[] getScreenSflAffectedInds()
	{
		return affectedInds;
	}
}
