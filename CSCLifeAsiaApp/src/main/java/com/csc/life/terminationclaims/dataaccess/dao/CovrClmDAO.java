package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.CovrClm;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface 	CovrClmDAO extends BaseDAO<CovrClm> {
	public List<CovrClm> selectCovrClmData(CovrClm covrClmData );
    //ILIFE-4406
    public List<CovrClm> searchCovrclmRecord(String chdrcoy, String chdrnum);
    public List<CovrClm> selectCovrClmCoverageData(CovrClm covrClmData);
}

