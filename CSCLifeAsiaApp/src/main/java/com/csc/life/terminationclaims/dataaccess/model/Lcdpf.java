package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;
import java.io.Serializable;

public class Lcdpf implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private long uniqueNumber;
	private String chdrnum;
	private String cnttype;
	private String statcode;
	//covrpf's risk state code
	private String pstatcode;
	private String longdesc;
	private String notifistatus;
	private int occdate;
	private int rcesdte;
	// USRPRF
	private String usrprf;
	// JOBNM
	private String jobnm;
	// DATIME
	private String datime;
	private String life;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public String getLongdesc() {
		return longdesc;
	}
	public void setLongdesc(String longdesc) {
		this.longdesc = longdesc;
	}
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	public int getRcesdte() {
		return rcesdte;
	}
	public void setRcesdte(int rcesdte) {
		this.rcesdte = rcesdte;
	}
	public String getNotifistatus() {
		return notifistatus;
	}
	public void setNotifistatus(String notifistatus) {
		this.notifistatus = notifistatus;
	}
	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}
	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}
}
