/*
 * File: Pr690.java
 * Date: 30 August 2009 1:56:46
 * Author: Quipoz Limited
 * 
 * Class transformed from PR690.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclaanlTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclabanTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclabltTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclaltlTableDAM;
import com.csc.life.terminationclaims.screens.Sr690ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Tr687rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Itdmkey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
***********************************************************************
* </pre>
*/
public class Pr690 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR690");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected ZonedDecimalData wsaaCompYear = new ZonedDecimalData(3, 0).setUnsigned();

	protected FixedLengthStringData wsaaTr687Itemitem = new FixedLengthStringData(7);
	protected FixedLengthStringData wsaaTr687Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTr687Itemitem, 0);
	protected FixedLengthStringData wsaaTr687Benpln = new FixedLengthStringData(2).isAPartOf(wsaaTr687Itemitem, 4);
		/* WSAA-VARIABLES */
	protected ZonedDecimalData ix = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaBenDesc = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBenDesclong = new FixedLengthStringData(29);

	private FixedLengthStringData wsaaLadc = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaL = new FixedLengthStringData(1).isAPartOf(wsaaLadc, 0);
	private FixedLengthStringData wsaaA = new FixedLengthStringData(1).isAPartOf(wsaaLadc, 1);
	private FixedLengthStringData wsaaD = new FixedLengthStringData(1).isAPartOf(wsaaLadc, 2);
	private FixedLengthStringData wsaaC = new FixedLengthStringData(1).isAPartOf(wsaaLadc, 3);
		/* TABLES */
	protected String tr687 = "TR687";
	private String tr50a = "TR50A";
		/* FORMATS */
	protected String itdmrec = "ITEMREC   ";
	private String descrec = "DESCREC   ";
	protected String hclaltlrec = "HCLALTLREC";
	protected String hclaanlrec = "HCLAANLREC";
	protected String hclabltrec = "HCLABLTREC";
	protected String hclabanrec = "HCLABANREC";
	private String covrrec = "COVRREC   ";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Component (Coverage/Rider) Record*/
	protected CovrTableDAM covrIO = new CovrTableDAM();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Hospital Benefit File Logical*/
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
		/*HB Claim Accum - Plan Annual Limits*/
	protected HclaanlTableDAM hclaanlIO = new HclaanlTableDAM();
		/*HB Claim Accum - Benefit Annual limits*/
	protected HclabanTableDAM hclabanIO = new HclabanTableDAM();
		/*HB Claim Accum - Benefit Life Time Limit*/
	protected HclabltTableDAM hclabltIO = new HclabltTableDAM();
		/*HB Claim Accum - Plan Life Time Limits*/
	protected HclaltlTableDAM hclaltlIO = new HclaltlTableDAM();
		/*Table items, date - maintenance view*/
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected Tr687rec tr687rec = new Tr687rec();
	protected Itdmkey wsaaItdmkey = new Itdmkey();
	protected Sr690ScreenVars sv = ScreenProgram.getScreenVars( Sr690ScreenVars.class);

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1290, 
		exit1390, 
		preExit
	}

	public Pr690() {
		super();
		screenVars = sv;
		new ScreenModel("Sr690", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			loadSubfile1050();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR690", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.taccamt1.set(ZERO);
		sv.taccamt2.set(ZERO);
		sv.tdeduct.set(ZERO);
		sv.zunit.set(ZERO);
		sv.amtlife.set(ZERO);
		sv.amtyear.set(ZERO);
		sv.tactexp.set(ZERO);
		sv.desc.set(SPACES);
		sv.hosben.set(SPACES);
		sv.limitc.set(SPACES);
		sv.crtable.set(SPACES);
		sv.benpln.set(SPACES);
		sv.chdrnum.set(SPACES);
		hbnfIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		covrIO.setChdrcoy(hbnfIO.getChdrcoy());
		covrIO.setChdrnum(hbnfIO.getChdrnum());
		covrIO.setLife(hbnfIO.getLife());
		covrIO.setCoverage(hbnfIO.getCoverage());
		covrIO.setRider(hbnfIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setStatuz(varcom.oK);
		covrIO.setFunction(varcom.readr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,"****")) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		datcon3rec.intDate1.set(covrIO.getCrrcd());
		datcon3rec.intDate2.set(datcon1rec.intDate);
		datcon3rec.frequency.set("01");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		compute(wsaaCompYear, 5).set(add(0.99999,datcon3rec.freqFactor));
		wsaaItdmkey.itdmItemcoy.set(hbnfIO.getChdrcoy());
		wsaaTr687Crtable.set(hbnfIO.getCrtable());
		wsaaTr687Benpln.set(hbnfIO.getBenpln());
		wsaaItdmkey.itdmItemitem.set(wsaaTr687Itemitem);
	}

protected void loadSubfile1050()
	{
		wsaaItdmkey.itdmItemtabl.set(tr687);
		itdmIO.setDataArea(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsaaItdmkey.itdmItemcoy);
		itdmIO.setItemtabl(wsaaItdmkey.itdmItemtabl);
		itdmIO.setItemitem(wsaaItdmkey.itdmItemitem);
		itdmIO.setItmfrm(hbnfIO.getEffdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsaaItdmkey.itdmItemcoy)
		|| isNE(itdmIO.getItemtabl(),tr687)
		|| isNE(itdmIO.getItemitem(),wsaaItdmkey.itdmItemitem)) {
			itdmIO.setStatuz(varcom.endp);
			blankSubfile1400();
			goTo(GotoLabel.exit1090);
		}
		tr687rec.tr687Rec.set(itdmIO.getGenarea());
		loadSubfile1800();
	}

protected void loadSubfile1800()
	{
		/*BEGN*/
		setUpNonSubfile1100();
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			loadSubfile1200();
		}
		
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

protected void setUpNonSubfile1100()
	{
		begin1110();
	}

protected void begin1110()
	{
		sv.chdrnum.set(hbnfIO.getChdrnum());
		sv.crtable.set(hbnfIO.getCrtable());
		sv.benpln.set(hbnfIO.getBenpln());
		sv.zunit.set(hbnfIO.getZunit());
		if (isEQ(tr687rec.hosben[1],SPACES)) {
			if (isNE(tr687rec.amtlife[1],ZERO)) {
				compute(sv.taccamt1, 2).set(mult(tr687rec.amtlife[1],sv.zunit));
			}
			if (isNE(tr687rec.amtyear[1],ZERO)) {
				compute(sv.taccamt2, 2).set(mult(tr687rec.amtyear[1],sv.zunit));
			}
			sv.tdeduct.set(tr687rec.aad);
		}
		hclaanlIO.setChdrcoy(hbnfIO.getChdrcoy());
		hclaanlIO.setChdrnum(hbnfIO.getChdrnum());
		hclaanlIO.setLife(hbnfIO.getLife());
		hclaanlIO.setCoverage(hbnfIO.getCoverage());
		hclaanlIO.setRider(hbnfIO.getRider());
		hclaanlIO.setAcumactyr(wsaaCompYear);
		hclaanlIO.setFunction(varcom.readr);
		hclaanlIO.setFormat(hclaanlrec);
		SmartFileCode.execute(appVars, hclaanlIO);
		if (isNE(hclaanlIO.getStatuz(),varcom.oK)
		&& isNE(hclaanlIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hclaanlIO.getStatuz());
			syserrrec.params.set(hclaanlIO.getParams());
			fatalError600();
		}
		if (isEQ(hclaanlIO.getStatuz(),varcom.oK)
		&& isNE(hclaanlIO.getClmpaid(),ZERO)) {
			compute(sv.taccamt1, 2).set(sub(sv.taccamt1,hclaanlIO.getClmpaid()));
		}
		hclaltlIO.setChdrcoy(hbnfIO.getChdrcoy());
		hclaltlIO.setChdrnum(hbnfIO.getChdrnum());
		hclaltlIO.setLife(hbnfIO.getLife());
		hclaltlIO.setCoverage(hbnfIO.getCoverage());
		hclaltlIO.setRider(hbnfIO.getRider());
		hclaltlIO.setFunction(varcom.readr);
		hclaltlIO.setFormat(hclaltlrec);
		SmartFileCode.execute(appVars, hclaltlIO);
		if (isNE(hclaltlIO.getStatuz(),varcom.oK)
		&& isNE(hclaltlIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hclaltlIO.getStatuz());
			syserrrec.params.set(hclaltlIO.getParams());
			fatalError600();
		}
		if (isEQ(hclaltlIO.getStatuz(),varcom.oK)
		&& isNE(hclaltlIO.getClmpaid(),ZERO)) {
			compute(sv.taccamt2, 2).set(sub(sv.taccamt2,hclaltlIO.getClmpaid()));
		}
	}

protected void loadSubfile1200()
	{
		try {
			begn1210();
			contitem1230();
		}
		catch (GOTOException e){
		}
	}

protected void begn1210()
	{
		for (ix.set(1); !(isGT(ix,6)); ix.add(1)){
			if (isNE(tr687rec.hosben[ix.toInt()],SPACES)) {
				sv.hosben.set(tr687rec.hosben[ix.toInt()]);
				benefitDesc1300();
				sv.desc.set(wsaaBenDesclong);
				ladcCode1500();
				sv.limitc.set(wsaaLadc);
				compute(sv.amtlife, 0).set(mult(tr687rec.amtlife[ix.toInt()],sv.zunit));
				compute(sv.amtyear, 0).set(mult(tr687rec.amtyear[ix.toInt()],sv.zunit));
				if (isNE(sv.amtlife,ZERO)) {
					readHclablt1600();
				}
				if (isNE(sv.amtyear,ZERO)) {
					readHclaban1700();
				}
				scrnparams.function.set(varcom.sadd);
				processScreen("SR690", sv);
				if (isNE(scrnparams.statuz,varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
		}
	}

protected void contitem1230()
	{
		if (isEQ(tr687rec.contitem,SPACES)) {
			itdmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsaaItdmkey.itdmItemcoy);
		itdmIO.setItemtabl(wsaaItdmkey.itdmItemtabl);
		itdmIO.setItemitem(tr687rec.contitem);
		itdmIO.setItmfrm(hbnfIO.getEffdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsaaItdmkey.itdmItemcoy)
		|| isNE(itdmIO.getItemtabl(),tr687)
		|| isNE(itdmIO.getItemitem(),tr687rec.contitem)) {
			itdmIO.setStatuz(varcom.endp);
		}
		tr687rec.tr687Rec.set(itdmIO.getGenarea());
	}

protected void blankSubfile1400()
	{
		begin1410();
	}

protected void begin1410()
	{
		sv.chdrnum.set(hbnfIO.getChdrnum());
		sv.crtable.set(hbnfIO.getCrtable());
		sv.benpln.set(hbnfIO.getBenpln());
		sv.zunit.set(hbnfIO.getZunit());
		sv.taccamt1.set(ZERO);
		sv.taccamt2.set(ZERO);
		sv.tdeduct.set(ZERO);
		sv.amtlife.set(ZERO);
		sv.amtyear.set(ZERO);
		sv.tactexp.set(ZERO);
		sv.desc.set(SPACES);
		sv.hosben.set(SPACES);
		sv.limitc.set(SPACES);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR690", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void benefitDesc1300()
	{
		try {
			begn1310();
		}
		catch (GOTOException e){
		}
	}

protected void begn1310()
	{
		if (isEQ(tr687rec.hosben[ix.toInt()],SPACES)) {
			wsaaBenDesclong.fill(SPACES);
			goTo(GotoLabel.exit1390);
		}
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tr50a);
		descIO.setDescitem(tr687rec.hosben[ix.toInt()]);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaBenDesclong.set(descIO.getLongdesc());
		}
		else {
			wsaaBenDesclong.fill("?");
		}
	}

protected void ladcCode1500()
	{
		begn1510();
	}

protected void begn1510()
	{
		wsaaLadc.set(SPACES);
		if (isNE(tr687rec.amtlife[ix.toInt()],ZERO)) {
			wsaaL.set("Y");
		}
		else {
			wsaaL.set("N");
		}
		if (isNE(tr687rec.amtyear[ix.toInt()],ZERO)) {
			wsaaA.set("Y");
		}
		else {
			wsaaA.set("N");
		}
		if (isNE(tr687rec.gdeduct[ix.toInt()],ZERO)) {
			wsaaD.set("Y");
		}
		else {
			wsaaD.set("N");
		}
		if (isNE(tr687rec.copay[ix.toInt()],ZERO)) {
			wsaaC.set("Y");
		}
		else {
			wsaaC.set("N");
		}
	}

protected void readHclablt1600()
	{
		begn1610();
	}

protected void begn1610()
	{
		hclabltIO.setChdrcoy(hbnfIO.getChdrcoy());
		hclabltIO.setChdrnum(hbnfIO.getChdrnum());
		hclabltIO.setLife(hbnfIO.getLife());
		hclabltIO.setCoverage(hbnfIO.getCoverage());
		hclabltIO.setRider(hbnfIO.getRider());
		hclabltIO.setHosben(tr687rec.hosben[ix.toInt()]);
		hclabltIO.setFunction(varcom.readr);
		hclabltIO.setFormat(hclabltrec);
		SmartFileCode.execute(appVars, hclabltIO);
		if (isNE(hclabltIO.getStatuz(),varcom.oK)
		&& isNE(hclabltIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hclabltIO.getStatuz());
			syserrrec.params.set(hclabltIO.getParams());
			fatalError600();
		}
		if (isEQ(hclabltIO.getStatuz(),varcom.oK)) {
			compute(sv.amtlife, 2).set(sub(sv.amtlife,hclabltIO.getClmpaid()));
		}
	}

protected void readHclaban1700()
	{
		begn1710();
	}

protected void begn1710()
	{
		hclabanIO.setChdrcoy(hbnfIO.getChdrcoy());
		hclabanIO.setChdrnum(hbnfIO.getChdrnum());
		hclabanIO.setLife(hbnfIO.getLife());
		hclabanIO.setCoverage(hbnfIO.getCoverage());
		hclabanIO.setRider(hbnfIO.getRider());
		hclabanIO.setHosben(tr687rec.hosben[ix.toInt()]);
		hclabanIO.setAcumactyr(wsaaCompYear);
		hclabanIO.setFunction(varcom.readr);
		hclabanIO.setFormat(hclabanrec);
		SmartFileCode.execute(appVars, hclabanIO);
		if (isNE(hclabanIO.getStatuz(),varcom.oK)
		&& isNE(hclabanIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hclabanIO.getStatuz());
			syserrrec.params.set(hclabanIO.getParams());
			fatalError600();
		}
		if (isEQ(hclabanIO.getStatuz(),varcom.oK)) {
			compute(sv.amtyear, 2).set(sub(sv.amtyear,hclabanIO.getClmpaid()));
			sv.tactexp.set(hclabanIO.getClmpaid());
		}
		else {
			sv.tactexp.set(ZERO);
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*VALIDATE-SCREEN*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SR690", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR690", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR690", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
