package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AcldTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:08
 * Class transformed from ACLD.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AcldTableDAM extends AcldpfTableDAM {

	public AcldTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ACLD");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", ACDBEN"
		             + ", RGPYNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "CRTABLE, " +
		            "RGPYNUM, " +
		            "ACDBEN, " +
		            "DATEFRM, " +
		            "DATETO, " +
		            "GCDBLIND, " +
		            "BENFREQ, " +
		            "ACTEXP, " +
		            "ACBENUNT, " +
		            "CVBENUNT, " +
		            "GCNETPY, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "ACDBEN ASC, " +
		            "RGPYNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "ACDBEN DESC, " +
		            "RGPYNUM DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               crtable,
                               rgpynum,
                               acdben,
                               datefrm,
                               dateto,
                               gcdblind,
                               benfreq,
                               actexp,
                               acbenunt,
                               cvbenunt,
                               gcnetpy,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(233);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getAcdben().toInternal()
					+ getRgpynum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, acdben);
			what = ExternalData.chop(what, rgpynum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller70.setInternal(rgpynum.toInternal());
	nonKeyFiller80.setInternal(acdben.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(104);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getCrtable().toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ getDatefrm().toInternal()
					+ getDateto().toInternal()
					+ getGcdblind().toInternal()
					+ getBenfreq().toInternal()
					+ getActexp().toInternal()
					+ getAcbenunt().toInternal()
					+ getCvbenunt().toInternal()
					+ getGcnetpy().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, datefrm);
			what = ExternalData.chop(what, dateto);
			what = ExternalData.chop(what, gcdblind);
			what = ExternalData.chop(what, benfreq);
			what = ExternalData.chop(what, actexp);
			what = ExternalData.chop(what, acbenunt);
			what = ExternalData.chop(what, cvbenunt);
			what = ExternalData.chop(what, gcnetpy);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public FixedLengthStringData getAcdben() {
		return acdben;
	}
	public void setAcdben(Object what) {
		acdben.set(what);
	}
	public PackedDecimalData getRgpynum() {
		return rgpynum;
	}
	public void setRgpynum(Object what) {
		setRgpynum(what, false);
	}
	public void setRgpynum(Object what, boolean rounded) {
		if (rounded)
			rgpynum.setRounded(what);
		else
			rgpynum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getDatefrm() {
		return datefrm;
	}
	public void setDatefrm(Object what) {
		setDatefrm(what, false);
	}
	public void setDatefrm(Object what, boolean rounded) {
		if (rounded)
			datefrm.setRounded(what);
		else
			datefrm.set(what);
	}	
	public PackedDecimalData getDateto() {
		return dateto;
	}
	public void setDateto(Object what) {
		setDateto(what, false);
	}
	public void setDateto(Object what, boolean rounded) {
		if (rounded)
			dateto.setRounded(what);
		else
			dateto.set(what);
	}	
	public FixedLengthStringData getGcdblind() {
		return gcdblind;
	}
	public void setGcdblind(Object what) {
		gcdblind.set(what);
	}	
	public FixedLengthStringData getBenfreq() {
		return benfreq;
	}
	public void setBenfreq(Object what) {
		benfreq.set(what);
	}	
	public PackedDecimalData getActexp() {
		return actexp;
	}
	public void setActexp(Object what) {
		setActexp(what, false);
	}
	public void setActexp(Object what, boolean rounded) {
		if (rounded)
			actexp.setRounded(what);
		else
			actexp.set(what);
	}	
	public PackedDecimalData getAcbenunt() {
		return acbenunt;
	}
	public void setAcbenunt(Object what) {
		setAcbenunt(what, false);
	}
	public void setAcbenunt(Object what, boolean rounded) {
		if (rounded)
			acbenunt.setRounded(what);
		else
			acbenunt.set(what);
	}	
	public PackedDecimalData getCvbenunt() {
		return cvbenunt;
	}
	public void setCvbenunt(Object what) {
		setCvbenunt(what, false);
	}
	public void setCvbenunt(Object what, boolean rounded) {
		if (rounded)
			cvbenunt.setRounded(what);
		else
			cvbenunt.set(what);
	}	
	public PackedDecimalData getGcnetpy() {
		return gcnetpy;
	}
	public void setGcnetpy(Object what) {
		setGcnetpy(what, false);
	}
	public void setGcnetpy(Object what, boolean rounded) {
		if (rounded)
			gcnetpy.setRounded(what);
		else
			gcnetpy.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		acdben.clear();
		rgpynum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		crtable.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		datefrm.clear();
		dateto.clear();
		gcdblind.clear();
		benfreq.clear();
		actexp.clear();
		acbenunt.clear();
		cvbenunt.clear();
		gcnetpy.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}