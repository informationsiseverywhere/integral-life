package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:56
 * Description:
 * Copybook name: SURDCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Surdclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData surdclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData surdclmKey = new FixedLengthStringData(64).isAPartOf(surdclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData surdclmChdrcoy = new FixedLengthStringData(1).isAPartOf(surdclmKey, 0);
  	public FixedLengthStringData surdclmChdrnum = new FixedLengthStringData(8).isAPartOf(surdclmKey, 1);
  	public PackedDecimalData surdclmTranno = new PackedDecimalData(5, 0).isAPartOf(surdclmKey, 9);
  	public PackedDecimalData surdclmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(surdclmKey, 12);
  	public FixedLengthStringData surdclmLife = new FixedLengthStringData(2).isAPartOf(surdclmKey, 15);
  	public FixedLengthStringData surdclmCoverage = new FixedLengthStringData(2).isAPartOf(surdclmKey, 17);
  	public FixedLengthStringData surdclmRider = new FixedLengthStringData(2).isAPartOf(surdclmKey, 19);
  	public FixedLengthStringData surdclmCrtable = new FixedLengthStringData(4).isAPartOf(surdclmKey, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(39).isAPartOf(surdclmKey, 25, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(surdclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		surdclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}