/*
 * File: P5186.java
 * Date: 30 August 2009 0:17:50
 * Author: Quipoz Limited
 * 
 * Class transformed from P5186.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import com.csc.smart.recordstructures.Subprogrec;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringUtils;

import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.LifeAssurdListDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Lcdpf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.life.terminationclaims.screens.Sa507ScreenVars;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;

import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*             P5186 - REGULAR PAYMENTS SELECTION
*             u001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001a
*Initialise
*----------
*
*  Skip  this  section  if  returning from an optional selection
*  (current stack position action flag = '*').
*
*  Clear the subfile ready for loading.
*
*  The  details  of  the  contract  being  enquired upon will be
*  stored  in  the  CHDRRGP I/O module. Retrieve the details and
*  set up the header portion of the screen. Some fields are not
*  present and the CHDRMJA I/O module will need to be read to
*  obtain these fields.
*
*  Look up the following descriptions and names:
*
*       Contract Type, (CNTTYPE) - long description from T5688,
*
*       Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*
*       Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*
*       The owner's client (CLTS) details.
*
*       The joint  owner's  client (CLTS) details if they exist.
*
*  Load the subfile as follows:
*
*  Retrieve the COVRRGP file (RETRV), if the selected plan has a
*  plan suffix  which  is  contained  within the summarised plan
*  record:  less than  or  equal  to  the  number  of summarised
*  policies (CHDR-POLSUM):  then the policy must be 'broken out'
*  with each life,  coverage  and rider for an associated policy
*  written to the  subfile  along  with  the  description of the
*  cover/rider.
*
*       Initial Screen Load.
*       --------------------
*
*       After the contract we read all of the components and
*       write one line to the  subfile  for  each.  The  normal
*       indentation  should  be  employed in the display of the
*       Life, Coverage and Rider  sequence  numbers.  For  each
*       life  read  the LIFE file and obtain the Life Assured's
*       Client Number. Place this on the screen and  read  CLTS
*       to  obtain  the client's name. Format this for display.
*       The selection field should be protected  for  the  line
*       displaying the life details.
*
*       On  the  coverage  and rider lines display the coverage
*       code, (CRTABLE) and  read  T5687  to  obtain  the  long
*       description for display.
*
*       The  component  lines  should  be interspersed with the
*       details of any existing  payment  detail.  So  for  any
*       component  read the Regular Payment Details file, REGP,
*       and if corresponding  records  are  found  display  the
*       payment details as follows:
*
*            .  In  the  same  place  as  the client number and
*            coverage  code  are  displayed  show   the   short
*            description from T6691, keyed on the Payment Type.
*
*            . Display the payment status.
*
*            .  Display  the  First  Payment Date and the Final
*            Payment Date if it is not Max Date.
*
*            .  If  the  amount  is   non-zero   display   this
*            otherwise display the percentage.
*
*            . Display the currency code.
*
*       These  details  are all displayed on one long field and
*       so will require formatting within the program.
*
*       Also store the Regular Payment  Sequence  Number  in  a
*       hidden  field  on  the  subfile for later processing if
*       the line is selected for further action.
*
*       For all component and  Regular  Payment  subfile  lines
*       set  the  Life,  Coverage  and  Rider  numbers  in  the
*       appropriate fields but non-display where  necessary  to
*       provide  the  indentation  and  also  the values of the
*       fields for later processing. Also store the CRTABLE  in
*       a  hidden  field  on  each  subfile  record  for  later
*       processing.
*
*  Load all pages  required  in  the subfile and set the subfile
*  more indicator to no.
*
*  Page  up  and  Page  down  should  never  be received as the
*  subfile load will load all of the  components  and  existing
*  payment  details  regardless  of  how  many there are. There
*  should never be so many that it impairs system response.
*
*
*Validation
*----------
*
*  . Selections to Process
*       Two passes will  be  made  over  the  subfile,  one  to
*       validate   the   selections   and   a  second,  if  the
*       selections are all valid, to process them.  The  second
*       pass will be carried out in the 4000 section.
*
*       For  the  first  pass perform a loop using Subfile Read
*       Next Changed to pick up all of the selected lines.  For
*       each  one  check  that it is valid by reading T6693 and
*       ensuring that the selected action is a valid  entry  on
*       the Extra Data Screen.
*
*       If  the selected line is itself a Coverage or Rider use
*       a key of  '**'  concatenated  with  the  Coverage/Rider
*       Code.  If  no  entry  is  found  then  Regular  Payment
*       details may not be set up against  this  component.  If
*       an  entry  is  found  then  check  the Transaction Code
*       obtained from T1690 against the Allowable  Transactions
*       on  the  Extra  Data  Screen. If no match is found then
*       the action is invalid against that component.
*
*       If the selected line is a  Payment  Details  line  then
*       read  T6693  with  a  key of the Regular Payment Status
*       concatenated with the associated Component Code. If  no
*       entry  is found then read again using four asterisks in
*       place of the Component  Code.  If  there  is  still  no
*       entry  then there is no possible action for the status.
*       If  an  entry  is  found  then  check  the  transaction
*       against  the  Allowable  Transactions on the Extra Data
*       Screen.
*
*       If no selections were made re-set the subfile RRN to  1
*       and re-display the screen.
*
*
*Updating
*--------
*
*  There is no validation in this program.
*
*
*Next Program
*------------
*
*  If "KILL" was requested move spaces to  the  current  program
*  position and action field, add 1 to the  program  pointer and
*  exit.
*
*  At this point the program will be either  searching  for  the
*  FIRST  selected  record  in  order  to pass  control  to  the
*  appropriate  generic  enquiry  program   for   the   selected
*  component  or it will be returning from one  of  the  Enquiry
*  screens after displaying some details and  searching  for the
*  NEXT selected record.
*
*  It will be able  to determine which of these two states it is
*  in by examining the Stack Action Flag.
*
*  If not returning  from  a  component (stack action is blank),
*  save the next four  programs currently on the stack. Read the
*  first record from the subfile. If this is not  selected, read
*  the next one and so on, until a selected  record is found, or
*  the end of the subfile is reached.
*
*  If a subfile  record  has been selected, look up the programs
*  required to  be  processed  from  the coverage/rider programs
*  table (T5671  -  accessed  by transaction number concatenated
*  with coverage/rider code from the subfile record). Move these
*  four programs into  the  program  stack  and  set the current
*  stack action to '*'  (so  that the system will return to this
*  program to process the next one).
*
*  If  the  selection  has  been made against a regular payment
*  line then store the REGP record with a READS.
*
*  If the selection is a Create  then  set  up  an  initialised
*  REGP record with the following:
*
*       CHDRCOY   -    Signon Company
*       CHDRNUM   -    Contract Number
*       PLNSFX    -    Zeroes
*       LIFE      -    Life Sequence Number
*       COVERAGE  -    Coverage Sequence Number
*       RIDER     -    Rider Sequence Number
*       CRTABLE   -    Coverage/Rider Code
*
*  and  the  remaining  fields  spaces and zeroes and perform a
*  KEEPS on REGP.
*
*  In Register Mode, Default  Follow  Ups will be created  if a
*  default  follow  up  method  exists on  T5688. T5677 is read
*  using the  follow up method  as  the  key and  up to 16 FLUP
*  records will be created.
*
*  Add one to  the  program  pointer  and  exit  to  process the
*  required generic component.
*
*  If nothing was selected or there are no  more  selections  to
*  process,  continue  by  moving blanks to  the  current  Stack
*  Action  field  reload  the  saved  four programs and exit.
*
*Notes.
*------
*
*  Tables Used:
*
*  . T5661 - Follow Up Codes
*            Key: T5677-Follow up Code
*
*  . T5671 - Generic program Switching
*            Key: Transaction Code || CRTABLE
*
*  . T5677 - Default Follow Ups
*            Key: Transaction Code || Default Follow Up Method
*
*  . T5687 - Coverage / Rider Details
*            Key: CRTABLE
*
*  . T5688 - Contract Definition
*            Key: Contract Type
*
*  . T1690 - Submenu Switching
*            Key: WSAA-PROG and Submenu Action
*
*  . T6691 - Regular Payment Type
*            Key: Regular Payment Type
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be '****' and for selections against
*            a Component set Payment Status to '**'.
*
*
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Pa507 extends ScreenProgCS {
private static final Logger LOGGER = LoggerFactory.getLogger(Pa507.class);
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pa507");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-MISCELLANEOUS-FLAGS */
	private ZonedDecimalData wsaaSelected = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSelectFlagLife = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSelectedlife = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");

	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");
		/* WSAA-PROGRAM-SAVE */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);	//ICIL-1286
	private static final String g931 = "G931";
	private static final String rrnh = "RRNH";
		/* TABLES */
	
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	
	private RegpTableDAM regpIO = new RegpTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5671rec t5671rec = new T5671rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sa507ScreenVars sv = getLScreenVars();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	
	private FixedLengthStringData wsaaCoverageStatuzCheck = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaCovrValidStatuz = new FixedLengthStringData(1).isAPartOf(wsaaCoverageStatuzCheck, 0);
	//ILIFE-1138 STARTS
	private FixedLengthStringData wsaaContractStatuzCheck = new FixedLengthStringData(5);	
	private FixedLengthStringData wsaaContStatcode = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 1);
	private FixedLengthStringData wsaaContPstcde = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 3);
	private static final String rrh8 = "RRH8";
	private String t5688="T5688";
	private Clntpf clntpf;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	boolean rpuMetNotFund;
	boolean rpuConfig = false;
	int subFileCount;
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	private LifeAssurdListDAO lifeAssurdListDAO = getApplicationContext().getBean("lifeAssurdListDAO", LifeAssurdListDAO.class);
	private Itempf itempf=null;
	private Subprogrec subprogrec = new Subprogrec();
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private T5679rec t5679rec = new T5679rec();
	String contractStatcode="";
	String componentStacode="";
	private String cltype1="Open";
	private String cltype2="Claim Registered";
	private String cltype="";
	
	
	
	List<String> chdrnumList=new ArrayList<String>();
	//ICIL-1286 Starts
	private Gensswrec gensswrec = new Gensswrec();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private PackedDecimalData wsaaX = new PackedDecimalData(1, 0);
	private PackedDecimalData wsaaY = new PackedDecimalData(1, 0);
	private boolean isPolSelected = false;
	private String[] wsaaSecProgm = new String[8];
	//ICIL-1286 End
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaIndex1 = new PackedDecimalData(3, 0);
	private List<Covrpf> covrpfList = new ArrayList<Covrpf>();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	
	public Pa507() {
		super();
		screenVars = sv;
		new ScreenModel("Sa507", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

protected void plainname() {
	/* PLAIN-100 */
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(), "C")) {
		corpname();

	} else if (isNE(clntpf.getGivname(), SPACES)) {
		String firstName = clntpf.getGivname();
		String lastName = clntpf.getSurname();
		String delimiter = ",";

		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);

	} else {
		wsspcomn.longconfname.set(clntpf.getSurname());
	}
	/* PLAIN-EXIT */
}
protected void corpname() {
	/* PAYEE-1001 */
	wsspcomn.longconfname.set(SPACES);
	/* STRING CLTS-SURNAME DELIMITED SIZE */
	/* CLTS-GIVNAME DELIMITED ' ' */
	String firstName = clntpf.getLgivname();
	String lastName = clntpf.getLsurname();
	String delimiter = "";

	// this way we can override StringUtil behaviour in formatName()
	String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
	wsspcomn.longconfname.set(fullName);
	/* CORP-EXIT */
}
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1010();
			
			
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
LOGGER.info(e.getMessage());
		}
	}

protected void initialise1010()
	{
		/* If returning from a multiple selection then go to exit.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		/* Clear the WORKING STORAGE.*/
		wsaaImmexitFlag.set(SPACES);
		wsaaSelectFlag.set(SPACES);
		wsaaSelected.set(ZERO);
		//ILIFE-1138 STARTS
		wsaaCovrValidStatuz.set("N");
		//ILIFE-1138 ENDS
		wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
		/* Set up basic data. Get todays date.*/
		wsaaBatckey.set(wsspcomn.batchkey);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaMiscellaneousInner.wsaaEffdate.set(datcon1rec.intDate);
		/* Clear the Subfile.*/
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaMiscellaneousInner.wsaaPlansuff.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("Sa507", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
		wsaaSelectFlagLife.set(SPACES);
		scrnparams.subfileRrn.set(1);
		headerDetail1030();
		displayLifeAssurdList1060();
	
	}

protected void headerDetail1030()
	{
		/* Move Contract details to the screen header.*/
		//ILIFE-1138 STARTS
		wsaaContractStatuzCheck.set(SPACES);
		//ILIFE-1138 ENDS
		sv.chdrnum.set(chdrrgpIO.getChdrnum());
		sv.cnttype.set(chdrrgpIO.getCnttype());
		//ILIFE-1138 STARTS
		wsaaContStatcode.set(chdrrgpIO.getStatcode());
		wsaaContPstcde.set(chdrrgpIO.getPstatcode());
		//ILIFE-1138 ENDS
	}

	protected void displayLifeAssurdList1060() {
		/* display life assurd list */
		//get Cliamt Name and Life number
		sv.lifcnum.set(wsspcomn.chdrCownnum);
		clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), sv.lifcnum.toString());
		if(clntpf!=null){
			sv.lifename.set(clntpf.getSurname().trim()+","+clntpf.getGivname().trim());
		}
	//get contract risk status and component risk status
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		 FixedLengthStringData[] cnRiskStat = t5679rec.cnRiskStat;
		 
		 FixedLengthStringData[] covRiskStat = t5679rec.covRiskStat;
		
		// Get whole list Data
			List<Lcdpf> lifeAssurdList = lifeAssurdListDAO.getLifeAssurdList(wsspcomn.chdrCownnum.getData(), t5688,wsspcomn.company.toString() ,cnRiskStat,covRiskStat,"CH", wsspcomn.language.toString(),"IT");
			if (!lifeAssurdList.isEmpty()) {
				for (Lcdpf lcdpf : lifeAssurdList) {
					/* Add record to subfile*/
					sv.subfileArea.set(SPACES);
					sv.subfileFields.set(SPACES);
					sv.chdrnum.set(lcdpf.getChdrnum());
					sv.cnttype.set(lcdpf.getCnttype());
					sv.statcode.set(lcdpf.getStatcode());
					sv.pstatcode.set(lcdpf.getPstatcode());
					sv.longdesc.set(lcdpf.getLongdesc());
					//get notification status by chdrnum and notifnum
					Notipf notipf =notipfDAO.getStatusByNumAndCnum(sv.lifcnum.toString(),lcdpf.getChdrnum());
					if(notipf!=null){
						sv.cltype.set(notipf.getNotifistatus());
					}
					
					sv.riskCommDate.set(lcdpf.getOccdate());
					sv.riskCessDate.set(lcdpf.getRcesdte());
					wsaaValidStatus.set("N");
					validateChdr3000(lcdpf);
					if(isEQ(wsaaValidStatus,"Y")){				
						validateCovr3000(lcdpf);
						if(isEQ(wsaaValidStatus,"Y"))
							addToSubfile1500();
					}
					
				}
			}
		
	}
	protected void validateChdr3000(Lcdpf lcdpf){
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12) || isEQ(wsaaValidStatus,"Y")) ; wsaaIndex.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],lcdpf.getStatcode())) {
				wsaaIndex1.set(13);
				wsaaValidStatus.set("Y");	
		     }
	    }
	}
	
	protected void validateCovr3000(Lcdpf lcdpf){
		covrpfList = covrpfDAO.getCovrByComAndNum(wsspcomn.company.toString(), lcdpf.getChdrnum());/* IJTI-1523 */
		if(covrpfList == null || covrpfList.isEmpty()){
			return;
		}
		for(Covrpf covrpf :covrpfList){
			if(isNE(covrpf.getLife(),lcdpf.getLife()))
				continue;
		 wsaaValidStatus.set("N");
		  if ((isNE(covrpf.getCoverage(),SPACES)) && ((isEQ(covrpf.getRider(),SPACES)) || (isEQ(covrpf.getRider(),"00")))) {
	 		  
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12) || isEQ(wsaaValidStatus,"Y")) ; wsaaIndex.add(1)){
				if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrpf.getStatcode())) {
					wsaaIndex1.set(13);
					wsaaValidStatus.set("Y");
			     }
		    }
		  }
		  if ((isNE(covrpf.getCoverage(),SPACES)) && ((isNE(covrpf.getRider(),SPACES)) && (isNE(covrpf.getRider(),"00")))) {
			  
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12) || isEQ(wsaaValidStatus,"Y")) ; wsaaIndex.add(1)){
				if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrpf.getStatcode())){
					wsaaIndex1.set(13);
					wsaaValidStatus.set("Y");
			    }
			}
		}
		  if(isEQ(wsaaValidStatus,"N")){
			  break;
		  }
		}
	}
protected void readSubfile3100()
	{
		/*READ-NEXT-RECORD*/
		/* CLMD data can not be updated from P5318 screen, so no           */
		/* reason to rewrite CLMD rec with new transaction number.         */
		/*    PERFORM 3300-ADJUST-CLMD-DETAIL.                             */
		scrnparams.function.set(varcom.srdn);
		processScreen("S5318", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
protected void subfileStart()	
{
	/*VALIDATE-SUBFILE*/
	scrnparams.function.set(varcom.sstrt);
	processScreen("Sa507", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}

protected void subfileIo4400() {

	processScreen("Sa507", sv);
	if (isNE(scrnparams.statuz, varcom.oK) && isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}




protected void addToSubfile1500()
	{
		/*ADD-LINE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("Sa507", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
		syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		scrnparams.subfileRrn.set(1);
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			checkSelection();	//ICIL-1286
		
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
LOGGER.info(e.getMessage());
		}
	}

//ICIL-1286 Starts
protected void checkSelection() {
	subfileStart();
	while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
		if(isEQ(sv.slt,5)) {
			isPolSelected = true;
			sv.slt.set(SPACES);
			scrnparams.function.set(varcom.supd);
			subfileIo4400();
			return;
		}
		scrnparams.function.set(varcom.srdn);
		subfileIo4400();
	}
validateSubfile2030();
}
//ICIL-1286 End

protected void screenIo2010()
	{
		/*    CALL 'SIO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S-DATA-AREA                         */
		/*                         S-SUBFILE-AREA.                     */
		/* If user has abandoned the transaction, then prevent*/
		/* program from continuing.*/
		if (isEQ(scrnparams.statuz, "SUBM ")) {
			wsspcomn.flag.set("K");
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If termination of processing then go to exit. P6350 will*/
		/*  release the soft lock.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.edterror.set(varcom.oK);
			return;
		}
		scrnparams.subfileRrn.set(1);
	
	
	}

protected void validateSubfile2030()
	{
	subfileStart();	
	if(isNE(wsaaSelectFlagLife,"Y")){
		wsaaSelectedlife.set(ZERO);
	}
	subFileCount=0;
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
		validateSubfile2600();
		if(rpuMetNotFund) break;
		scrnparams.function.set(varcom.supd);
		subfileIo4400();
		scrnparams.function.set(varcom.srdn);
		subfileIo4400();
	} 		
		if(!rpuMetNotFund){
		if (isEQ(wsaaMiscellaneousInner.wsaaSelected,0) && isEQ(rpuConfig,false)) {
			scrnparams.errorCode.set(g931);
			wsspcomn.edterror.set("Y");
		}			
		else if(isEQ(rpuConfig,true) && isLT(wsaaSelectedlife,subFileCount)){
			scrnparams.errorCode.set(rrh8);
			wsspcomn.edterror.set("Y");
		}else if(!isEQ(cltype,SPACE) && !cltype.isEmpty()){
			cltype="";
			wsaaMiscellaneousInner.wsaaSelected.set(SPACE);
			scrnparams.errorCode.set(rrnh);
			wsspcomn.edterror.set("Y");
		}
	
		}
		
		String chdrnumstr = StringUtils.join(chdrnumList,",");
		wsspcomn.chdrNumList.set(chdrnumstr);
		
		
		
	
	}

protected void validateSubfile2600()
	{
	
					readNextRecord2610();					
					updateErrorIndicators2650();
			
	}

protected void readNextRecord2610()
	{
	
		/* If record has space in the select AND it has already*/
		/* been processed then count it as a valid selection then*/
		/* exit. This is to force the user to only have a single*/
		/* selection given subsequent program limitations.*/
		
	if (isNE(sv.select,SPACES)) {
				
				wsaaMiscellaneousInner.wsaaSelected.add(1);	
				if(isEQ(sv.cltype.trim(),cltype1) || isEQ(sv.cltype.trim(),cltype2)){
					cltype=sv.cltype.trim();
					sv.select.set(SPACES);
				}else{
					chdrnumList.add(sv.chdrnum.toString());
				}
				
			}
	}


protected void updateErrorIndicators2650()
	{
	if (isNE(sv.errorSubfile,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	else {
		wsspcomn.edterror.set(varcom.oK);
		return;
	}
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		scrnparams.function.set(varcom.supd);
		processScreen("Sa507", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}


protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/* Update database files as required*/
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		
		
		
	
				processSubfile();	//ICIL-1286
				
	}
//ICIL-1286 Starts
protected void processSubfile() {
	if(isPolSelected) {
		isPolSelected = false;
		wsspcomn.nextprog.set(wsaaProg);
		for(int i=1;i<8;i++) {
			wsaaSecProgm[i]=wsspcomn.secProg[i].toString();
		}
		genSwitch4200();
		sv.slt.set(SPACES);
		Chdrpf chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString(), sv.chdrnum.toString().trim());
		if (chdrpf.getChdrnum()!=null) {
			chdrpfDAO.setCacheObject(chdrpf);
		}
	return;
	} else if(wsaaSecProgm[1]!=null){
		for(int i=1;i<8;i++) {
			wsspcomn.secProg[i].set(wsaaSecProgm[i]);
		}
	}
nextProgram4010();
}	

protected void genSwitch4200(){
	gensswrec.company.set(wsspcomn.company);
	gensswrec.progIn.set(wsaaProg.toString().toUpperCase());
	gensswrec.transact.set("TAPL");
	gensswrec.function.set("A");
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	wsaaX.set(1);
	compute(wsaaY, 0).set(add(1, wsspcomn.programPtr));
	for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
		wsspcomn.secProg[wsaaY.toInt()].set(gensswrec.progOut[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
	scrnparams.function.set("HIDEW");
	processScreen("Sa507", sv);
}
//ICIL-1286 End

protected void nextProgram4010()
	{
		
	if (isEQ(scrnparams.statuz,"KILL")) {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		//releaseSftlck4400();
		wsaaMiscellaneousInner.wsaaSub.set(1);
		wsspcomn.secActn[wsaaMiscellaneousInner.wsaaSub.toInt()].set(SPACES);
		return;
	}
	if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
		initSelection4100();
	}
	else {
		scrnparams.function.set(varcom.srdn);
	}
	wsaaSelectFlag.set("N");
	wsaaImmexitFlag.set("N");
	while ( !(wsaaSelection.isTrue()
	|| wsaaImmExit.isTrue())) {
		next4200();
	}
	
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
		if (wsaaImmExit.isTrue()) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		return;
		}
	}

		
		if(isEQ(wsaaSelectFlag,"N")){
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set("PA506");
			return;
		}else{
			if (wsaaImmExit.isTrue()) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				wsspcomn.programPtr.add(1);
				scrnparams.subfileRrn.set(1);
				wsspcomn.nextprog.set(wsaaProg);
			return;
			}
		}
		
		continue4080();
	}

protected void continue4080()
	{
		/* Set up for execution of the component specific programs to be*/
		/* executed. Note that this includes a set of follow up records*/
		/* for each payment created according to table settings.*/
		setUpForComponents4300();
		/* Read the  Program  table T5671 for  the components programs*/
		/* to be executed next.*/
		programTables4400();
		/*UPDATE-SCREEN*/
		screenUpdate4600();
	}

protected void initSelection4100()
	{
		try {
			saveNextProgs4110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
	LOGGER.info(e.getMessage());
		}
	}

protected void saveNextProgs4110()
	{
		/* Save the  next  four  programs  after  P  into  Working*/
		/* Storage for re-instatement at the end of the Subfile.*/
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 4); loopVar1 += 1){
			loop14120();
		}
		/* First Read of the Subfile for a Selection.*/
		scrnparams.function.set(varcom.sstrt);
		/* Go to the first  record  in  the Subfile to  find the First*/
		/* Selection.*/
		return;
	}

protected void loop14120()
	{
		/* This loop will load the next four  programs from WSSP Stack*/
		/* into a Working Storage Save area.*/
		wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()].set(wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void next4200()
	{
		try {
			nextRec4210();
			ifSubfileEndp4220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
	LOGGER.info(e.getMessage());
		}
	}

protected void nextRec4210()
	{
		/* Read next subfile record sequentially.*/
		processScreen("Sa507", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void ifSubfileEndp4220()
	{
		/* Check for the end of the Subfile.*/
		/* If end of Subfile re-load the Saved four programs to Return*/
		/*   to initial stack order.*/
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			reloadProgsWssp4230();
		return;
		}
		/* Exit if a selection has been found. NOTE that we space*/
		/* and protect the selection as the subsequent transactions*/
		/* cannot handle multiple entry.*/
		if (isNE(sv.select, SPACES)) {
			sv.select.set(SPACES);
			wsaaSelectFlagLife.set("Y");
			wsaaSelectFlag.set("Y");
		}
		scrnparams.function.set(varcom.srdn);
	return;
	}

protected void reloadProgsWssp4230()
	{
		/* Set Flag for immediate exit as the next action is to go to the*/
		/* next program in the stack.*/
		/* Set Subscripts and call Loop.*/
		wsaaImmexitFlag.set("Y");
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			loop34240();
		}
	return;
	}

protected void loop34240()
	{
		/* Re-load the Saved four programs from Working Storage to the*/
		/* WSSP Program Stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()].set(wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void setUpForComponents4300()
	{
		setupRegp4310();
	}

protected void setupRegp4310()
	{
		/* Do a KEEPS on the key of a REGP record if one does not already e*/
		/*  otherwise do a READS.*/
		regpIO.setNonKey(SPACES);
		regpIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		regpIO.setChdrnum(chdrrgpIO.getChdrnum());	
		regpIO.setRgpynum(ZERO);
		regpIO.setFormat(formatsInner.regprec);
		SmartFileCode.execute(appVars, regpIO);
	}

protected void programTables4400()
	{
		
					readProgramTable4410();
			
	}

protected void readProgramTable4410()
	{
		/* Read the  Program  table T5671 for  the components programs to*/
		/* be executed next.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.addExpression(wsaaMiscellaneousInner.wsaaCrtable);
		
		/* Reset the Action to '*' signifying action desired to the next*/
		/* program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		wsspcomn.nextprog.set(wsaaProg);
	}



protected void loop24430()
	{
		/* This loop will load four programs from table T5671 to the WSSP*/
		/* stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub2.toInt()].set(t5671rec.pgm[wsaaMiscellaneousInner.wsaaSub1.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void planReload4500()
	{
		try {
			reloadProgsWssp4510();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
LOGGER.info(e.getMessage());
		}
	}

protected void reloadProgsWssp4510()
	{
		/* Set Flag for immediate exit as the next action is to go to the*/
		/* next program in the stack.*/
		/* Set Subscripts and call Loop.*/
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 4); loopVar4 += 1){
			loop34520();
		}
		/* Set action flag to ' ' in order that the program will resume*/
		/* initial stack order and go to the next program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		return;
	}

protected void loop34520()
	{
		/* Re-load the Saved four programs from Working Storage to the*/
		/* WSSP Program Stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()].set(wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void screenUpdate4600()
	{
		/*PARA*/
		scrnparams.function.set(varcom.supd);
		processScreen("Sa507", sv);		
	}

protected void releaseSftlck4800()
	{
		unlockContract4810();
	}

protected void unlockContract4810()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrrgpIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);	
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaSelected = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
}


/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData regprec = new FixedLengthStringData(10).init("REGPREC");
}

protected Sa507ScreenVars getLScreenVars() {
	return ScreenProgram.getScreenVars(Sa507ScreenVars.class);
}

public StringUtil getStringUtil() {
	return stringUtil;
}
}
