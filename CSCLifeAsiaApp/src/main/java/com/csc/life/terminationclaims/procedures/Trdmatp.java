/*
 * File: Trdmatp.java
 * Date: 30 August 2009 2:45:20
 * Author: Quipoz Limited
 * 
 * Class transformed from TRDMATP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.contractservicing.procedures.Totloan;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.MatdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.MathclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Matpcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*   This program is an item entry on T6598, the Claim Subroutine
*   method table.
*
*   READR the MATHCLM record for the contract, this contains the
*   contract level details for the maturity including the
*   adjustment amount entered at the time of maturity.
*
*   Do a BEGN read on the maturity Claim Details records
*   (MATDCLM) for the passed key. Accumulate value 'S' field
*   type, 'B' field type, 'M' field type, 'T' field type and
*   'X' field type MATDCLM records in the working storage.
*   (One total will be sufficient for all).
*
*   Do a NEXTR on MATDCLM.
*
*   For each MATDCLM record found do a component posting, and
*   when MATDCLM = ENDP then do the balancing postings.
*
*   Each of the postings will be as follows:-
*
*   If the amounts are not zero, then call LIFACMV to post to
*   the correct maturity accounts. The posting required is
*   defined in the appropriate line number on T5645 table entry
*   for this subroutine. Set up and pass the linkage areas
*   noting some of the values below.
*
*   When all MATD records have been processed for this MATH, then
*   process any loans which may be outstanding.
*   This is done by first calling TOTLOAN to bring
*   the interest on loans for this contract up to date.
*   ACMVs are then required for paying off loans. For each of
*   relevant entries on T5645, call LOANPYMT with the outstanding
*   maturity amount until all surrender is 'allocated' or
*   all entries have been processed and some of the maturity
*   value remains.
*   Then write an ACMV record to suspense for the residual of
*   the value to be matured after the above.
*
*
*****************************************************************
* </pre>
*/
public class Trdmatp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected String wsaaSubr = "TRDMATP";
	private PackedDecimalData wsaaTodayDate = new PackedDecimalData(8, 0);
	protected PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);
	protected PackedDecimalData wsaaAdjustmentAmt = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaContractAmt = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	protected ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	protected ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaAcctLevel = new FixedLengthStringData(1);
	private Validator componentLevelAccounting = new Validator(wsaaAcctLevel, "Y");

	private FixedLengthStringData wsaaT5645 = new FixedLengthStringData(126);
	private FixedLengthStringData[] wsaaStoredT5645 = FLSArrayPartOfStructure(6, 21, wsaaT5645, 0);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaPaidOffFlag = new FixedLengthStringData(1).init("N");
	private Validator noDebts = new Validator(wsaaPaidOffFlag, "Y");
	private Validator inDebt = new Validator(wsaaPaidOffFlag, "N");

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	protected FixedLengthStringData wsaaTranTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);
	protected ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	protected ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	protected ZonedDecimalData wsaaTranUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
	/*ILIFE-3336 Starts*/
	private FixedLengthStringData wsaaPrevCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevLiencd = new FixedLengthStringData(2);
	private PackedDecimalData wsaaPrevTranno = new PackedDecimalData(5);
	private PackedDecimalData wsaaMathPlnsfx = new PackedDecimalData(4);
	private FixedLengthStringData wsaaMathLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaMathChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaMathCoy = new FixedLengthStringData(2);
	private PackedDecimalData wsaaMathTranno = new PackedDecimalData(5);
	/*ILIFE-3336 Ends */
	
	
	
		/* ERRORS */
	private String e044 = "E044";
	private String e308 = "E308";
	private String mathclmrec = "MATHCLMREC";
	private String matdclmrec = "MATDCLMREC";
		/* TABLES */
	private String t1688 = "T1688";
	private String t5645 = "T5645";
	private String t5688 = "T5688";
	private Cashedrec cashedrec = new Cashedrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
		/*Maturity Detail Record*/
	private MatdclmTableDAM matdclmIO = new MatdclmTableDAM();
		/*Maturity Header Claim File*/
	protected MathclmTableDAM mathclmIO = new MathclmTableDAM();
	protected Matpcpy matpcpy = new Matpcpy();
	protected Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Totloanrec totloanrec = new Totloanrec();
	protected Varcom varcom = new Varcom();
	protected Batckey wsaaBatckey = new Batckey();
	private boolean annuRegFlag = false;//ILJ-187

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit0009, 
		exit3109, 
		dbExit9005, 
		seExit9105
	}

	public Trdmatp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		matpcpy.maturityRec = convertAndSetParam(matpcpy.maturityRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			mainlineRoutine0000();
		}
		catch (GOTOException e){
		}
		finally{
			exit0009();
		}
	}

protected void mainlineRoutine0000()
	{
		syserrrec.subrname.set(wsaaSubr);
		wsaaTranTermid.set(SPACES);
		wsaaTranDate.set(ZERO);
		wsaaTranTime.set(ZERO);
		wsaaTranUser.set(ZERO);
		readFirstMathclm1000();
		if (isEQ(mathclmIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit0009);
		}
		wsaaSequenceNo.set(ZERO);
		wsaaAdjustmentAmt.set(ZERO);
		wsaaContractAmt.set(ZERO);
		wsaaTranno.set(mathclmIO.getTranno());
		wsaaTransTime.set(getCobolTime());
		wsaaBatckey.set(matpcpy.batckey);
		annuRegFlag = FeaConfg.isFeatureExist(matpcpy.chdrcoy.toString(), "SUOTR008", appVars, "IT");//ILJ-186
		readTableT56451100();
		readTableT56881200();
		readTableT16881250();
		getTodayDate1300();
		callTotloan1400();
		while ( !(isEQ(mathclmIO.getStatuz(),varcom.endp)
		|| isNE(mathclmIO.getChdrcoy(),matpcpy.chdrcoy)
		|| isNE(mathclmIO.getChdrnum(),matpcpy.chdrnum)
		|| isNE(mathclmIO.getTranno(),matpcpy.tranno))) {
			processMathclm2000();
		}
		
	}

protected void exit0009()
	{
		exitProgram();
	}

protected void readFirstMathclm1000()
	{
		doBegnMathclm1000();
	}

protected void doBegnMathclm1000()
	{
		mathclmIO.setDataArea(SPACES);
		mathclmIO.setChdrcoy(matpcpy.chdrcoy);
		mathclmIO.setChdrnum(matpcpy.chdrnum);
		mathclmIO.setTranno(matpcpy.tranno);
		mathclmIO.setPlanSuffix(matpcpy.planSuffix);
		mathclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		matdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		matdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		mathclmIO.setFormat(mathclmrec);
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(),varcom.oK)
		&& isNE(mathclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(mathclmIO.getParams());
			syserrrec.statuz.set(mathclmIO.getStatuz());
			dbError9000();
		}
		if (isNE(mathclmIO.getChdrcoy(),matpcpy.chdrcoy)
		|| isNE(mathclmIO.getChdrnum(),matpcpy.chdrnum)
		|| isNE(mathclmIO.getTranno(),matpcpy.tranno)
		|| isEQ(mathclmIO.getStatuz(),varcom.endp)) {
			mathclmIO.setStatuz(varcom.endp);
		}
		/*ILIFE-3336 Starts*/
		wsaaMathPlnsfx.set(mathclmIO.getPlanSuffix());
		wsaaMathLife.set(mathclmIO.getLife());
		wsaaMathChdrnum.set(mathclmIO.getChdrnum());
		wsaaMathCoy.set(mathclmIO.getChdrcoy());
		wsaaMathTranno.set(mathclmIO.getTranno());
		/*ILIFE-3336 Ends*/
	}

protected void readTableT56451100()
	{
		readT56451100();
	}

protected void readT56451100()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(matpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq("01");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,2)); wsaaSub1.add(1)){
			compute(wsaaSub2, 0).set(add(2,wsaaSub1));
			wsaaT5645Cnttot[wsaaSub2.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
			wsaaT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsaaT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsaaT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsaaT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
		wsaaT5645Cnttot[5].set(t5645rec.cnttot[3]);
		wsaaT5645Glmap[5].set(t5645rec.glmap[3]);
		wsaaT5645Sacscode[5].set(t5645rec.sacscode[3]);
		wsaaT5645Sacstype[5].set(t5645rec.sacstype[3]);
		wsaaT5645Sign[5].set(t5645rec.sign[3]);
		if(annuRegFlag){//ILJ-187
			wsaaT5645Cnttot[6].set(t5645rec.cnttot[4]);
			wsaaT5645Glmap[6].set(t5645rec.glmap[4]);
			wsaaT5645Sacscode[6].set(t5645rec.sacscode[4]);
			wsaaT5645Sacstype[6].set(t5645rec.sacstype[4]);
			wsaaT5645Sign[6].set(t5645rec.sign[4]);
		}
		itemIO.setItemcoy(matpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSub1.set(14); !(isGT(wsaaSub1,15)); wsaaSub1.add(1)){
			compute(wsaaSub2, 0).set(sub(wsaaSub1,13));
			wsaaT5645Cnttot[wsaaSub2.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
			wsaaT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsaaT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsaaT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsaaT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
	}

protected void readTableT56881200()
	{
		readT56881200();
	}

protected void readT56881200()
	{
		itdmIO.setItemcoy(matpcpy.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(matpcpy.cnttype);
		itdmIO.setItmfrm(matpcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9000();
		}
		if (isNE(itdmIO.getItemcoy(),matpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),matpcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(matpcpy.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			dbError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readTableT16881250()
	{
		readT16881250();
	}

protected void readT16881250()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(matpcpy.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setLanguage(matpcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(wsaaBatckey.batcBatctrcde);
			syserrrec.statuz.set(e044);
			dbError9000();
		}
		wsaaLongdesc.set(descIO.getLongdesc());
	}

protected void getTodayDate1300()
	{
		/*CALL-DATCON1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTodayDate.set(datcon1rec.intDate);
		/*EXIT*/
	}

protected void callTotloan1400()
	{
		start1400();
	}

protected void start1400()
	{
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(matpcpy.chdrcoy);
		totloanrec.chdrnum.set(matpcpy.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(matpcpy.effdate);
		totloanrec.tranno.set(matpcpy.tranno);
		totloanrec.batchkey.set(matpcpy.batckey);
		totloanrec.tranTerm.set(matpcpy.termid);
		totloanrec.tranDate.set(matpcpy.date_var);
		totloanrec.tranTime.set(matpcpy.time);
		totloanrec.tranUser.set(matpcpy.user);
		totloanrec.language.set(matpcpy.language);
		totloanrec.function.set("POST");
		callProgram(Totloan.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError9100();
		}
	}

protected void processMathclm2000()
	{
		processMatdclm2000();
		readNextrMathclm2004();
	}

protected void processMatdclm2000()
	{
		wsaaAdjustmentAmt.set(mathclmIO.getOtheradjst());
		wsaaContractAmt.set(0);
		readFirstMatdclm2100();
		while ( !(isEQ(matdclmIO.getStatuz(),varcom.endp))) {
			processMatdclm3000();
		}
		
		if (inDebt.isTrue()) {
			cashedrec.docamt.set(wsaaContractAmt);
			allocateToLoans8000();
		}
		postAdjustmentAmt5000();
		postPoldebt5500();
		postContractAmt6000();
	}

protected void readNextrMathclm2004()
	{
		mathclmIO.setFormat(mathclmrec);
		mathclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(),varcom.oK)
		&& isNE(mathclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(mathclmIO.getParams());
			syserrrec.statuz.set(mathclmIO.getStatuz());
			dbError9000();
		}
		
		/*ILIFE-3336 Starts*/
		if( (isEQ(wsaaMathChdrnum,mathclmIO.getChdrnum())
				&& isEQ(wsaaMathCoy,mathclmIO.getChdrcoy())
				&& isEQ(wsaaMathLife,mathclmIO.getLife())
				&& isEQ(wsaaMathPlnsfx,mathclmIO.getPlanSuffix())
				&& isEQ(wsaaMathTranno,mathclmIO.getTranno())
				&& isNE(mathclmIO.getStatuz(),varcom.endp))){
			        readNextrMathclm2004();
          }
		/*ILIFE-3336 Ends*/
		/*EXIT*/	
	}

protected void readFirstMatdclm2100()
	{
		doBegnMatdclm2100();
	}

protected void doBegnMatdclm2100()
	{
		matdclmIO.setParams(SPACES);
		matdclmIO.setChdrcoy(mathclmIO.getChdrcoy());
		matdclmIO.setChdrnum(mathclmIO.getChdrnum());
		matdclmIO.setTranno(mathclmIO.getTranno());
		matdclmIO.setPlanSuffix(mathclmIO.getPlanSuffix());
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		matdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		matdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO","PLNSFX");
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(),varcom.oK)
		&& isNE(matdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			syserrrec.statuz.set(matdclmIO.getStatuz());
			dbError9000();
		}
		if (isNE(matdclmIO.getChdrcoy(),mathclmIO.getChdrcoy())
		|| isNE(matdclmIO.getChdrnum(),mathclmIO.getChdrnum())
		|| isNE(matdclmIO.getTranno(),mathclmIO.getTranno())
		|| isNE(matdclmIO.getPlanSuffix(),mathclmIO.getPlanSuffix())
		|| isEQ(matdclmIO.getStatuz(),varcom.endp)) {
			matdclmIO.setStatuz(varcom.endp);
		}
	}

protected void processMatdclm3000()
	{
		postMatdclmRecs3000();
		readNextrMatdclm3006();
	}

protected void postMatdclmRecs3000()
	{
		wsaaContractAmt.add(matdclmIO.getActvalue());
		postMatdclmRecs3100();
	}

protected void readNextrMatdclm3006()
	{
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(),varcom.oK)
		&& isNE(matdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			syserrrec.statuz.set(matdclmIO.getStatuz());
			dbError9000();
		}
		if (isNE(matdclmIO.getChdrcoy(),mathclmIO.getChdrcoy())
		|| isNE(matdclmIO.getChdrnum(),mathclmIO.getChdrnum())
		|| isNE(matdclmIO.getTranno(),mathclmIO.getTranno())
		|| isNE(matdclmIO.getPlanSuffix(),mathclmIO.getPlanSuffix())
		|| isEQ(matdclmIO.getStatuz(),varcom.endp)) {
			matdclmIO.setStatuz(varcom.endp);
			return;
		}
		/*ILIFE-3336 Starts*/
		if( (isEQ(wsaaPrevRider,matdclmIO.getRider())
				&& isEQ(wsaaPrevCoverage,matdclmIO.getCoverage())
				&& isEQ(wsaaPrevLife,matdclmIO.getLife())
				&& isEQ(wsaaPrevLiencd,matdclmIO.getLiencd())
				&& isEQ(wsaaPrevTranno,matdclmIO.getTranno())
				&& isNE(matdclmIO.getStatuz(),varcom.endp))){
			readNextrMatdclm3006();
}
		/*ILIFE-3336 Ends*/
		/*EXIT*/
	}

protected void postMatdclmRecs3100()
	{
	/*ILIFE-3336 Starts*/
	wsaaPrevCoverage.set(matdclmIO.getCoverage());
	wsaaPrevRider.set(matdclmIO.getRider());
	wsaaPrevLife.set(matdclmIO.getLife());
	wsaaPrevLiencd.set(matdclmIO.getLiencd());
	wsaaPrevTranno.set(matdclmIO.getTranno());
	/*ILIFE-3336 Ends*/
	
		try {
			setupLifacmv3100();
		}
		catch (GOTOException e){
		}
	}

protected void setupLifacmv3100()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batccoy.set(matpcpy.chdrcoy);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec.rdocnum.set(matpcpy.chdrnum);
		lifacmvrec.tranno.set(wsaaTranno);
		lifacmvrec.rldgcoy.set(matpcpy.chdrcoy);
		lifacmvrec.origcurr.set(matpcpy.cntcurr);
		lifacmvrec.tranref.set(matpcpy.chdrnum);
		lifacmvrec.trandesc.set(wsaaLongdesc);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.genlcoy.set(matpcpy.chdrcoy);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.effdate.set(matpcpy.effdate);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(wsaaTodayDate);
		lifacmvrec.transactionTime.set(wsaaTransTime);
		lifacmvrec.user.set(matpcpy.user);
		lifacmvrec.termid.set(matpcpy.termid);
		lifacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.substituteCode[1].set(matpcpy.cnttype);
		lifacmvrec.substituteCode[6].set(matdclmIO.getCrtable());
		lifacmvrec.origamt.set(matdclmIO.getActvalue());
		wsaaAcctLevel.set(t5688rec.comlvlacc);
		if (componentLevelAccounting.isTrue()) {
			if (isEQ(matdclmIO.getFieldType(),"S")) {
				wsaaIndex.set(8);
			}
			else {
				if (isEQ(matdclmIO.getFieldType(),"B")) {
					wsaaIndex.set(9);
				}
				else {
					if (isEQ(matdclmIO.getFieldType(),"M")) {
						wsaaIndex.set(10);
					}
					else {
						if (isEQ(matdclmIO.getFieldType(),"T")) {
							wsaaIndex.set(11);
						}
						else {
							if (isEQ(matdclmIO.getFieldType(),"X")) {
								wsaaIndex.set(12);
							}
							else {
								/*NEXT_SENTENCE*/
							}
						}
					}
				}
			}
		}
		else {
			if (isEQ(matdclmIO.getFieldType(),"S")) {
				wsaaIndex.set(2);
			}
			else {
				if (isEQ(matdclmIO.getFieldType(),"B")) {
					wsaaIndex.set(3);
				}
				else {
					if (isEQ(matdclmIO.getFieldType(),"M")) {
						wsaaIndex.set(4);
					}
					else {
						if (isEQ(matdclmIO.getFieldType(),"T")) {
							wsaaIndex.set(5);
						}
						else {
							if (isEQ(matdclmIO.getFieldType(),"X")) {
								wsaaIndex.set(6);
							}
						}
					}
				}
			}
		}
		wsaaRldgChdrnum.set(matpcpy.chdrnum);
		wsaaPlan.set(matdclmIO.getPlanSuffix());
		wsaaRldgLife.set(matdclmIO.getLife());
		wsaaRldgCoverage.set(matdclmIO.getCoverage());
		wsaaRldgRider.set(matdclmIO.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		
		if (isNE(wsaaIndex,ZERO))
		{
			lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaIndex.toInt()]);		 
		 
			lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaIndex.toInt()]);		 
		 
			lifacmvrec.glcode.set(t5645rec.glmap[wsaaIndex.toInt()]);		 
		 
			lifacmvrec.glsign.set(t5645rec.sign[wsaaIndex.toInt()]);		 
		 
			lifacmvrec.contot.set(t5645rec.cnttot[wsaaIndex.toInt()]);
		}
		
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			goTo(GotoLabel.exit3109);
		}
		postAcmvRecord7000();
/*IJS-38*//*		if (isEQ(matdclmIO.getFieldType(),"B")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode[1]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[1]);
			lifacmvrec.glcode.set(t5645rec.glmap[1]);
			lifacmvrec.glsign.set(t5645rec.sign[1]);
			lifacmvrec.contot.set(t5645rec.cnttot[1]);
			postAcmvRecord7000();
		}*/
/*IJS-38*/
	}

protected void postAdjustmentAmt5000()
	{
		start5000();
	}

protected void start5000()
	{
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(matpcpy.chdrnum);
		lifacmvrec.sacscode.set(t5645rec.sacscode07);
		lifacmvrec.sacstyp.set(t5645rec.sacstype07);
		lifacmvrec.glcode.set(t5645rec.glmap07);
		lifacmvrec.glsign.set(t5645rec.sign07);
		lifacmvrec.contot.set(t5645rec.cnttot07);
		lifacmvrec.origamt.set(wsaaAdjustmentAmt);
		lifacmvrec.acctamt.set(ZERO);
		if (isNE(lifacmvrec.origamt,ZERO)) {
			postAcmvRecord7000();
		}
	}

protected void postPoldebt5500()
	{
		/*START*/
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(matpcpy.chdrnum);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[5]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[5]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[5]);
		lifacmvrec.glsign.set(wsaaT5645Sign[5]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[5]);
		lifacmvrec.origamt.set(mathclmIO.getTdbtamt());
		if (isNE(lifacmvrec.origamt,ZERO)) {
			postAcmvRecord7000();
		}
		/*EXIT*/
	}

protected void postContractAmt6000()
	{
		postAmounts6000();
	}

protected void postAmounts6000()
{
	if(annuRegFlag){
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[6]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[6]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[6]);
		lifacmvrec.glsign.set(wsaaT5645Sign[6]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[6]);
	}else{
		lifacmvrec.sacscode.set(t5645rec.sacscode13);
		lifacmvrec.sacstyp.set(t5645rec.sacstype13);
		lifacmvrec.glcode.set(t5645rec.glmap13);
		lifacmvrec.glsign.set(t5645rec.sign13);
		lifacmvrec.contot.set(t5645rec.cnttot13);
	}	
	lifacmvrec.origamt.set(wsaaContractAmt);
	lifacmvrec.acctamt.set(ZERO);
	lifacmvrec.origamt.add(wsaaAdjustmentAmt);
	if (isNE(lifacmvrec.origamt,ZERO)) {
		postAcmvRecord7000();
	}
	matpcpy.status.set(varcom.endp);
}

protected void postAcmvRecord7000()
	{
		/*START*/
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			systemError9100();
		}
		/*EXIT*/
	}

protected void allocateToLoans8000()
	{
		start8000();
	}

protected void start8000()
	{
		wsaaGlCompany.set(lifacmvrec.genlcoy);
		wsaaGlCurrency.set(lifacmvrec.genlcur);
		wsaaTranTermid.set(matpcpy.termid);
		wsaaTranUser.set(matpcpy.user);
		wsaaTranTime.set(matpcpy.time);
		wsaaTranDate.set(matpcpy.date_var);
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(matpcpy.chdrcoy);
		wsaaTranEntity.set(matpcpy.chdrnum);
		cashedrec.cashedRec.set(SPACES);
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(wsaaBatckey);
		cashedrec.doctNumber.set(lifacmvrec.rdocnum);
		cashedrec.doctCompany.set(lifacmvrec.batccoy);
		cashedrec.trandate.set(lifacmvrec.effdate);
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		cashedrec.transeq.set(wsaaSequenceNo);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(lifacmvrec.origcurr);
		cashedrec.dissrate.set(lifacmvrec.crate);
		cashedrec.trandesc.set(lifacmvrec.trandesc);
		cashedrec.chdrcoy.set(matpcpy.chdrcoy);
		cashedrec.chdrnum.set(matpcpy.chdrnum);
		cashedrec.tranno.set(lifacmvrec.tranno);
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.trankey.set(wsaaTrankey);
		cashedrec.language.set(matpcpy.language);
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,4)
		|| isEQ(wsaaContractAmt,0)); wsaaSub1.add(1)){
			loans8200();
		}
		wsaaSequenceNo.set(cashedrec.transeq);
		if (isGT(wsaaContractAmt,0)) {
			wsaaPaidOffFlag.set("Y");
		}
	}

protected void loans8200()
	{
		start8200();
	}

protected void start8200()
	{
		wsaaGlMap.set(wsaaT5645Glmap[wsaaSub1.toInt()]);
		cashedrec.sign.set(wsaaT5645Sign[wsaaSub1.toInt()]);
		cashedrec.sacscode.set(wsaaT5645Sacscode[wsaaSub1.toInt()]);
		cashedrec.sacstyp.set(wsaaT5645Sacstype[wsaaSub1.toInt()]);
		cashedrec.genlkey.set(wsaaGenlkey);
		cashedrec.origamt.set(wsaaContractAmt);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			systemError9100();
		}
		wsaaContractAmt.set(cashedrec.docamt);
	}

protected void dbError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					chkError9000();
				}
				case dbExit9005: {
					dbExit9005();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void chkError9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9005);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9005()
	{
		matpcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void systemError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					chkError9100();
				}
				case seExit9105: {
					seExit9105();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void chkError9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9105);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9105()
	{
		matpcpy.status.set(varcom.bomb);
		exitProgram();
	}
}
