package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sh5c2screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sh5c2screensfl";
		lrec.subfileClass = Sh5c2screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 27;
		lrec.pageSubfile = 16;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 10, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh5c2ScreenVars sv = (Sh5c2ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh5c2screenctlWritten, sv.Sh5c2screensflWritten, av, sv.sh5c2screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh5c2ScreenVars screenVars = (Sh5c2ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.riskcommdteDisp.setClassString("");	
		screenVars.claimnumber.setClassString("");
		screenVars.claimTyp.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.rsncde.setClassString("");
		screenVars.totalPrem.setClassString("");
		screenVars.tCashval.setClassString("");
		screenVars.tPolicyReserve.setClassString("");
		screenVars.tFundVal.setClassString("");
		screenVars.apl.setClassString("");
		screenVars.aplInterest.setClassString("");
		screenVars.policyloan.setClassString("");
		screenVars.policyloanInterest.setClassString("");
		screenVars.susamt.setClassString("");
		screenVars.unexpiredprm.setClassString("");
		screenVars.total.setClassString("");
		screenVars.resndesc.setClassString("");
		screenVars.refundOption.setClassString("");
		screenVars.advPrm.setClassString("");
	}

/**
 * Clear all the variables in Sh5c2screenctl
 */
	public static void clear(VarModel pv) {
		Sh5c2ScreenVars screenVars = (Sh5c2ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.riskcommdteDisp.clear();	
		screenVars.riskcommdte.clear();	
		screenVars.claimnumber.clear();
		screenVars.claimTyp.clear();
		screenVars.effdateDisp.clear();
		screenVars.rsncde.clear();
		screenVars.totalPrem.clear();
		screenVars.tCashval.clear();
		screenVars.tPolicyReserve.clear();
		screenVars.tFundVal.clear();
		screenVars.apl.clear();
		screenVars.aplInterest.clear();
		screenVars.policyloan.clear();
		screenVars.policyloanInterest.clear();
		screenVars.susamt.clear();
		screenVars.unexpiredprm.clear();
		screenVars.total.clear();	
		screenVars.effdate.clear();
		screenVars.resndesc.clear();
		screenVars.refundOption.clear();
		screenVars.advPrm.clear();
	}
}
