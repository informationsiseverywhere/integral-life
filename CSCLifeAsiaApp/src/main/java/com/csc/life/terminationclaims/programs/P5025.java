/*
 * File: P5025.java
 * Date: 29 August 2009 23:57:05
 * Author: Quipoz Limited
 * 
 * Class transformed from P5025.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.dao.RplnpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rplnpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.screens.S5025ScreenVars;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  This is a Sub Menu program for Maturity/Expiry.
*
*  This program will read CHDRMAT for the contract number entered
*  and loop around the COVRMAT records for the contract checking
*  that Maturity or Expiry is an allowable transaction. It firstly
*  checks the contract status on table T5679, secondly reads all
*  the COVRMAT records and check the component statii on table
*  T5679.  The effective date cannot be greater than three months
*  prior to the risk cessation date (ie. risk cessation date -
*  effective <= 3 months). Thirdly it reads table T5687 and if the
*  Maturity Calculation Method is not spaces for any of the
*  COVRMAT's then it can either mature or vest.  To test if a
*  COVRMAT vests, table T6625 is read and if a valid status is
*  returned, the record vests and so cannot mature.  If the
*  Maturity Calculation Method is spaces for ALL the COVRMAT's
*  then only the expiry transaction is allowable.
*
*  The transactions code are:-
*
*       Maturity      T542
*       Expiry        T539
*
*
* Validation
* ----------
*
*  Key 1 - Contract number (CHDRMAT)
*
*       Y = mandatory, must exist on file.
*            - CHDRMAT  -  Life  new  business  contract  header
*                 logical view.
*            - must be correct status for transaction (T5679).
*            - MUST BE CORRECT BRANCH.
*
*       N = IRREVELANT.
*
*       Blank = irrelevant.
*
*  Key 2 - Effective date
*
*       Y = must be a valid date and must not be less than the
*           Paid-to-date (PTDATE) and must be greater than the
*           Risk Commencement Date (OCCDATE) of contract header.
*
*       N = default to today date and must not be less than the
*           Paid-to-date (PTDATE) and must be greater than the
*           Risk Commencement Date (OCCDATE) of contract header.
*
*
* Updating
* --------
*
*  Set up WSSP-FLAG:
*       - always "M".
*
*  For maintenance transactions  (WSSP-FLAG  set to  'M' above),
*  soft lock the contract header (call SFTLOCK). If the contract
*  is already locked, display an error message.
*
*****************************************************************
* </pre>
*/
public class P5025 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5025");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaEligibleDate = new ZonedDecimalData(8, 0).setUnsigned();
	private String wsaaEligible = "";
	private String wsaaValidStatus = "";

	protected FixedLengthStringData wsaaMatureExpiry = new FixedLengthStringData(1).init("E");
	private Validator maturityTrans = new Validator(wsaaMatureExpiry, "M");
	private Validator expiryTrans = new Validator(wsaaMatureExpiry, "E");
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t6625 = "T6625";
		/* Dummy user wssp. Replace with required version.
		    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Batckey wsaaBatchkey = new Batckey();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	protected Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	protected Sdasancrec sdasancrec = new Sdasancrec();
	protected S5025ScreenVars sv = getLScreenVars();
	protected ErrorsInner errorsInner = new ErrorsInner();
	private List<Covrpf> covrpfList =new ArrayList<Covrpf>(); 
	protected Covrpf covrpf;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private boolean wsaaEof;
	Iterator<Covrpf> iterator ; 
	protected ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	protected Itempf itempf = null;
	protected Chdrpf chdrpf;
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	//ICIL-14 Start
	boolean isLoanConfig = false;
	private RplnpfDAO rplnpfDAO = getApplicationContext().getBean("rplnpfDAO", RplnpfDAO.class);
	private Rplnpf rplnpf = new Rplnpf();
	//ICIL-14 End

	protected S5025ScreenVars getLScreenVars(){
		return ScreenProgram.getScreenVars( S5025ScreenVars.class);
	}
/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit2219, 
		loopCovrmat2320, 
		exit2390, 
		initialPrem2405, 
		exit2409, 
		exit2519, 
		exit12090, 
		keeps3070, 
		batching3080, 
		exit3090
	}

	public P5025() {
		super();
		screenVars = sv;
		new ScreenModel("S5025", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		sv.efdate.set(varcom.vrcmMaxDate);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		if (isEQ(wsaaToday, ZERO)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		
		isLoanConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSLND002", appVars, "IT"); //ICIL-14
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S5025IO' USING        SCRN-SCREEN-PARAMS               */
		/*                                S5025-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		sv.errorIndicators.set(SPACES);
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateFields2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		
		
		//ICIL-14 Start
		if(isEQ(isLoanConfig,true)){
			rplnpf = rplnpfDAO.getRploanRecord(sv.chdrsel.toString().trim(),"P");
			if(rplnpf!=null) {
				if(isEQ(sv.action, "A")){
					sv.actionErr.set(errorsInner.rrfh);	
				}			
			}
		}
		//ICIL-14 End
		
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateFields2200()
	{
		try {
			validateChdrsel2210();
			maturityOrExpiry2215();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateChdrsel2210()
	{
		if (isEQ(sv.chdrsel, SPACES)) {
			sv.chdrselErr.set(errorsInner.e186);
			goTo(GotoLabel.exit2219);
		}

		chdrpf = new Chdrpf();

		chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsspcomn.company.toString(), sv.chdrsel.toString().trim());

		if(null == chdrpf)
			sv.chdrselErr.set(errorsInner.e544);

		if (isEQ(sv.chdrselErr, SPACES)
				&& isNE(wsspcomn.branch, chdrpf.getCntbranch())) {
			sv.chdrselErr.set(errorsInner.e455);
		}
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				goTo(GotoLabel.exit2219);
			}
		}
		if (isEQ(sv.action, "I")) {
			goTo(GotoLabel.exit2219);
		}
		if (isNE(sv.chdrselErr, SPACES)) {
			goTo(GotoLabel.exit2219);
		}
		checkStatus2500();
	}

protected void maturityOrExpiry2215()
	{
		validateEffdate2220();
		if (isNE(sv.errorIndicators, SPACES)) {
			return ;
		}
		chkMaturityExpiry2300();
		if (isEQ(wsaaValidStatus, "N")) {
			sv.chdrselErr.set(errorsInner.e767);
			return ;
		}
		if (isEQ(sv.action, "A")) {
			if (!maturityTrans.isTrue()) {
				sv.chdrselErr.set(errorsInner.t012);
				sv.chdrselOut[varcom.ri.toInt()].set("Y");
				return ;
			}
		}
		else {
			if (isEQ(sv.action, "B")) {
				if (!expiryTrans.isTrue()) {
					sv.chdrselErr.set(errorsInner.t017);
					sv.chdrselOut[varcom.ri.toInt()].set("Y");
					return ;
				}
			}
		}
		if (isEQ(wsaaEligible, "N")) {
			sv.chdrselErr.set(errorsInner.t013);
			sv.chdrselOut[varcom.ri.toInt()].set("Y");
		}
	}

	/**
	* <pre>
	*  Effective date is optional, if entered it must be a valid
	*  date else take the default date which is today date. It must
	*  be greater than the business date(Today date) and can be a
	*  pass date but not prior to the contract commencement date.
	* </pre>
	*/
protected void validateEffdate2220()
	{
		if (isEQ(sv.efdate, varcom.vrcmMaxDate)) {
			sv.efdate.set(wsaaToday);
		}
		else {
			datcon1rec.function.set("CONV");
			datcon1rec.intDate.set(sv.efdate);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				sv.efdateErr.set(errorsInner.e032);
				sv.efdateOut[varcom.ri.toInt()].set("Y");
				return ;
			}
		}
		if (isGT(sv.efdate, wsaaToday)) {
			sv.efdateErr.set(errorsInner.f530);
		}
		else {
			if (isLT(sv.efdate, chdrpf.getOccdate())) {
				sv.efdateErr.set(errorsInner.f616);
			}
		}
	}

protected void chkMaturityExpiry2300()
	{
		/* Initial to Expiry allowable first.*/
		wsaaMatureExpiry.set("E");
		wsaaEligible = "N";
		wsaaValidStatus = "N";
		datcon2rec.intDatex1.set(sv.efdate);
		datcon2rec.frequency.set("12");
		datcon2rec.freqFactor.set(3);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaEligibleDate.set(datcon2rec.intDatex2);
		readCovrmat2310();
		return;
	}

protected void readCovrmat2310()
	{
		wsaaEof = false;
		covrpfList = covrpfDAO.getCovrsurByComAndNum(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());
		iterator =   covrpfList.iterator();
		if (iterator.hasNext()) 
			covrpf = iterator.next();		
		else

			wsaaEof = true;
		loopCovrmat2320();
		return;
	}

protected void loopCovrmat2320()
	{



		if(wsaaEof == true)
			//goTo(GotoLabel.exit2390);
			return;

		/* COVR statcode and pstatcode must not be matured or expiry*/
		/* status.*/
		if (isNE(wsaaValidStatus, "Y")) {
			chkComponentsStatii2400();
		}
		/* COVR risk cessation date has to be within 3 months of effective*/
		/* date for it to mature or expire. i.e. Risk Cessation Date -*/
		/* S5025-EFDATE <= 3.*/
		if ((isGT(wsaaEligibleDate, covrpf.getRiskCessDate())
				|| isEQ(wsaaEligibleDate, covrpf.getRiskCessDate()))
		&& isEQ(wsaaValidStatus, "Y")) {
			wsaaEligible = "Y";
		}
		readT56872320();
		return;
	}

protected void readT56872320()
	{

		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),t5687 ,covrpf.getCrtable(),sv.efdate.toInt());/* IJTI-1523 */

		if (itempfList.size() >= 1 )

			t5687rec.t5687Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));

		else {
			fatalError600();
		}

		if (isNE(t5687rec.maturityCalcMeth, SPACES)) {
			/*     MOVE 'M'                TO  WSAA-MATURE-EXPIRY.          */
			checkVesting2600();
		}
		if (isEQ(wsaaEligible, "Y")
		&& maturityTrans.isTrue()) {
			//goTo(GotoLabel.exit2390);
			return;
		}
		nextrCovrmat2340();
		return;
	}

protected void nextrCovrmat2340()
	{
		if (iterator.hasNext()) {
			covrpf = iterator.next();		
		}
		else
			wsaaEof = true;
		//goTo(GotoLabel.loopCovrmat2320);
		loopCovrmat2320();
		return;
	}

protected void chkComponentsStatii2400()
	{
		wsaaSub.set(ZERO);
		loopRisk2402();
		return;
	}

protected void loopRisk2402()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			//goTo(GotoLabel.exit2409);
			return;
		}
		if (isEQ(covrpf.getStatcode(), t5679rec.covRiskStat[wsaaSub.toInt()])) {
			//	goTo(GotoLabel.initialPrem2405);
			initialPrem2405();
		return ;
		}
	}

protected void initialPrem2405()
	{
		wsaaSub.set(ZERO);
		loopPrem2407();
		return;
	}

protected void loopPrem2407()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			return ;
		}
		if (isEQ(covrpf.getPstatcode(), t5679rec.covPremStat[wsaaSub.toInt()])) {
			wsaaValidStatus = "Y";
			return ;
		}
		loopPrem2407();
		return ;
	}

protected void checkStatus2500()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5679);
		itempf.setItemitem(subprogrec.transcd.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		lookForStat2510();
	}

protected void lookForStat2510()
	{
			try {
					initialRisk2510();
					loopRisk2512();
			initialPrem2515();
					loopPrem2517();
			}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialRisk2510()
	{
		wsaaSub.set(ZERO);
	}

protected void loopRisk2512()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			sv.chdrselErr.set(errorsInner.e767);
			goTo(GotoLabel.exit2519);
		}
		if (isNE(chdrpf.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
			loopRisk2512();
			return ;
		}
		}

protected void initialPrem2515()
	{
		wsaaSub.set(ZERO);
	}

protected void loopPrem2517()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			sv.chdrselErr.set(errorsInner.e767);
			return ;
		}
		if (isNE(chdrpf.getPstcde(), t5679rec.cnPremStat[wsaaSub.toInt()])) {
			loopPrem2517();
			return ;
		}
	}

protected void checkVesting2600()
	{
		/* Read T6625 table to check if component can vest.             */
		/* If the component is not present on T6625, it is not able to  */
		/* vest and can therefore mature, so set maturity flag.         */


		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),t6625,covrpf.getCrtable(),sv.efdate.toInt());/* IJTI-1523 */

		if(null == itempfList || itempfList.size() < 1 )
			wsaaMatureExpiry.set("M");
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(errorsInner.e073);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		wsspcomn.currfrom.set(sv.efdate);
		if (isEQ(scrnparams.statuz, "BACH")) {
			//goTo(GotoLabel.exit3090);
			return;
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details*/
		if (isEQ(scrnparams.action, "I")) {
			wsspcomn.flag.set("I");
			//
			chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsspcomn.company.toString(),sv.chdrsel.toString());
			//goTo(GotoLabel.keeps3070);
			keeps3070();
			return;
		}
		else {
			wsspcomn.flag.set("M");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			//goTo(GotoLabel.batching3080);
			batching3080();
			return;
		}
		/*    Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		/* MOVE S5025-CHDRSEL          TO SFTL-ENTITY*/
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			//goTo(GotoLabel.exit3090);
			return;
		}
		keeps3070();
		return;
	}

protected void keeps3070()
	{
		/*   Store the contract header for use by the transaction programs*/
        if(chdrpf != null) {
		chdrpfDAO.setCacheObject(chdrpf);
        }
		batching3080();
		return;
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
public static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e032 = new FixedLengthStringData(4).init("E032");
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	public FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	public FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	public FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData f530 = new FixedLengthStringData(4).init("F530");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData t012 = new FixedLengthStringData(4).init("T012");
	private FixedLengthStringData t013 = new FixedLengthStringData(4).init("T013");
	private FixedLengthStringData t017 = new FixedLengthStringData(4).init("T017");
	private FixedLengthStringData rrfh = new FixedLengthStringData(4).init("RRFH");/*ICIL-14*/
	private FixedLengthStringData e944 = new FixedLengthStringData(4).init("E944");/*ICIL-14*/
}
}
