package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5024
 * @version 1.0 generated on 30/08/09 06:31
 * @author Quipoz
 */
public class S5024ScreenVars extends SmartVarModel { 

	/*bug #ILIFE-983 start*/
	public FixedLengthStringData dataArea = new FixedLengthStringData(293+48);
	/*bug #ILIFE-983 END*/
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(101).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData bbranch = DD.bbranch.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData bcompany = DD.bcompany.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(dataFields,9);
	public ZonedDecimalData scheduleNumber = DD.bschednum.copyToZonedDecimal().isAPartOf(dataFields,19);
	public FixedLengthStringData chdrnum = DD.chdrnumfrm.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData chdrnum1 = DD.chdrnumto.copy().isAPartOf(dataFields,35);
	public FixedLengthStringData datecfrom = DD.datecfrom.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData datecto = DD.datecto.copy().isAPartOf(dataFields,53);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,63);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData runid = DD.runid.copy().isAPartOf(dataFields,81);
	public FixedLengthStringData runlib = DD.runlib.copy().isAPartOf(dataFields,83);
	public FixedLengthStringData jobname = DD.jobname.copy().isAPartOf(dataFields, 93);
	
	/*bug #ILIFE-983 start*/
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48+12).isAPartOf(dataArea, 101);
	/*bug #ILIFE-983 END*/
	
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bcompanyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bschednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bschednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrnumfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData chdrnumtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData datecfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData datectoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	
	/*bug #ILIFE-983 start*/
	public FixedLengthStringData runidErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData runlibErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jobnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144+36).isAPartOf(dataArea, 149+12);
	/*bug #ILIFE-983 END*/
	
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bcompanyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bschednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bschednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrnumfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] chdrnumtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] datecfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] datectoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	
	/*bug #ILIFE-983 start*/
	public FixedLengthStringData[] runidOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] runlibOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jobnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	/*bug #ILIFE-983 END*/
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S5024screenWritten = new LongData(0);
	public LongData S5024protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5024ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(datecfromOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datectoOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumfrmOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumtoOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {effdate, acctmonth, jobq,runid,runlib,jobname, acctyear, scheduleName, scheduleNumber, bbranch, bcompany, datecfrom, datecto, chdrnum, chdrnum1};
		screenOutFields = new BaseData[][] {effdateOut, acctmonthOut, jobqOut,runidOut,runlibOut,jobnameOut, acctyearOut, bschednamOut, bschednumOut, bbranchOut, bcompanyOut, datecfromOut, datectoOut, chdrnumfrmOut, chdrnumtoOut};
		screenErrFields = new BaseData[] {effdateErr, acctmonthErr, jobqErr, runidErr,runlibErr,jobnameErr, acctyearErr, bschednamErr, bschednumErr, bbranchErr, bcompanyErr, datecfromErr, datectoErr, chdrnumfrmErr, chdrnumtoErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5024screen.class;
		protectRecord = S5024protect.class;
	}

}
