package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR679
 * @version 1.0 generated on 30/08/09 07:23
 * @author Quipoz
 */
public class Sr679ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1863);//BRD-306 STARTS
	public FixedLengthStringData dataFields = new FixedLengthStringData(679).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,3);
	public ZonedDecimalData benBillDate = DD.bbldat.copyToZonedDecimal().isAPartOf(dataFields,7);
	public FixedLengthStringData benpln = DD.benpln.copy().isAPartOf(dataFields,15);
	public ZonedDecimalData annivProcDate = DD.cbanpr.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,33);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,49);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,51);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,89);
	public FixedLengthStringData frqdesc = DD.frqdesc.copy().isAPartOf(dataFields,119);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,129);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,146);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,193);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,201);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,203);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,205);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,252);
	public FixedLengthStringData livesno = DD.livesno.copy().isAPartOf(dataFields,260);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,261);
	public FixedLengthStringData optdscs = new FixedLengthStringData(120).isAPartOf(dataFields, 265);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(8, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData optdsc04 = DD.optdsc.copy().isAPartOf(filler,45);
	public FixedLengthStringData optdsc05 = DD.optdsc.copy().isAPartOf(filler,60);
	public FixedLengthStringData optdsc06 = DD.optdsc.copy().isAPartOf(filler,75);
	public FixedLengthStringData optdsc07 = DD.optdsc.copy().isAPartOf(filler,90);
	public FixedLengthStringData optdsc08 = DD.optdsc.copy().isAPartOf(filler,105);
	public FixedLengthStringData optinds = new FixedLengthStringData(8).isAPartOf(dataFields, 385);
	public FixedLengthStringData[] optind = FLSArrayPartOfStructure(8, 1, optinds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(optinds, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01 = DD.optind.copy().isAPartOf(filler1,0);
	public FixedLengthStringData optind02 = DD.optind.copy().isAPartOf(filler1,1);
	public FixedLengthStringData optind03 = DD.optind.copy().isAPartOf(filler1,2);
	public FixedLengthStringData optind04 = DD.optind.copy().isAPartOf(filler1,3);
	public FixedLengthStringData optind05 = DD.optind.copy().isAPartOf(filler1,4);
	public FixedLengthStringData optind06 = DD.optind.copy().isAPartOf(filler1,5);
	public FixedLengthStringData optind07 = DD.optind.copy().isAPartOf(filler1,6);
	public FixedLengthStringData optind08 = DD.optind.copy().isAPartOf(filler1,7);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,393);
	public ZonedDecimalData premcess = DD.premcess.copyToZonedDecimal().isAPartOf(dataFields,397);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,405);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,415);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,423);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,426);
	public ZonedDecimalData rerateDate = DD.rrtdat.copyToZonedDecimal().isAPartOf(dataFields,428);
	public ZonedDecimalData rerateFromDate = DD.rrtfrm.copyToZonedDecimal().isAPartOf(dataFields,436);
	public ZonedDecimalData singlePremium = DD.singprm.copyToZonedDecimal().isAPartOf(dataFields,444);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,461);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,462);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,464);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,468);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,483);
	public FixedLengthStringData waiverprem = DD.waiverprem.copy().isAPartOf(dataFields,500);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,501);
	public FixedLengthStringData zdesc = DD.zdesc.copy().isAPartOf(dataFields,514);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,534);
	/*BRD-306 STARTS*/
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 551);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 568);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 585);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 602);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 619);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,636);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,637);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,640);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,643);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,646);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,649);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,666);
	public ZonedDecimalData zunit = DD.zunit.copyToZonedDecimal().isAPartOf(dataFields,667);
	/*BRD-306 END*/
	//ILIFE-3421-STARTS
	public FixedLengthStringData waitperiod = DD.waitperiod.copy().isAPartOf(dataFields,669);
	public FixedLengthStringData bentrm = DD.bentrm.copy().isAPartOf(dataFields,672);
	public FixedLengthStringData poltyp = DD.zpoltyp.copy().isAPartOf(dataFields,674);
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,675);
	//ILIFE-3421-ENDS
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,676);//BRD-NBP-011
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(296).isAPartOf(dataArea, 679);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bbldatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData benplnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cbanprErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData frqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData livesnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(8, 4, optdscsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(32).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData optdsc04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData optdsc05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData optdsc06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData optdsc07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData optdsc08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData optindsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData[] optindErr = FLSArrayPartOfStructure(8, 4, optindsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(32).isAPartOf(optindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData optind02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData optind03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData optind04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData optind05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData optind06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData optind07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData optind08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData premcessErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData rrtdatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData rrtfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData singprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData waiverpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData zdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	/*BRD-306 STARTS*/
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 252);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 256);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 260);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 264);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 268);
	public FixedLengthStringData zunitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 272);
	/*BRD-306 END*/
	////ILIFE-3421-STARTS
	public FixedLengthStringData waitperiodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 276);
	public FixedLengthStringData bentrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 280);
	public FixedLengthStringData poltypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 284);
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 288);
	////ILIFE-3421-ENDS
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 292);//BRD-NBP-011
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(888).isAPartOf(dataArea, 975);
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bbldatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] benplnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cbanprOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] frqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] livesnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 276);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(8, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(96).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] optdsc04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] optdsc05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] optdsc06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] optdsc07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] optdsc08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData optindsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 372);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(8, 12, optindsOut, 0);
	public FixedLengthStringData[][] optindO = FLSDArrayPartOfArrayStructure(12, 1, optindOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(96).isAPartOf(optindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optind01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] optind02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] optind03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] optind04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] optind05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] optind06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] optind07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] optind08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] premcessOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] rrtdatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] rrtfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] singprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[] waiverpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] zdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	/*BRD-306 STARTS*/
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 756);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 768);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 780);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 792);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 804);
	public FixedLengthStringData[] zunitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 816);
	/*BRD-306 END*/
	////ILIFE-3421-STARTS
	public FixedLengthStringData[]waitperiodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 828);
	public FixedLengthStringData[]bentrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 840);
	public FixedLengthStringData[]poltypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 852);
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 864);
	////ILIFE-3421-ENDS
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 876);//BRD-NBP-011
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData benBillDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData annivProcDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premcessDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rerateFromDateDisp = new FixedLengthStringData(10);

	public LongData Sr679screenWritten = new LongData(0);
	public LongData Sr679protectWritten = new LongData(0);	
	public FixedLengthStringData contDtCalcScreenflag = new FixedLengthStringData(1); 
	public FixedLengthStringData cntEnqScreenflag = new FixedLengthStringData(1); //ILJ-387
	
	public boolean hasSubfile() {
		return false;
	}


	public Sr679ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "33",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {null, null, "44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bbldatOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc03Out,new String[] {null, null, null, "04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind03Out,new String[] {"06","04","-06","04",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc01Out,new String[] {null, null, null, "02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind01Out,new String[] {"05","02","-05","02",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc02Out,new String[] {null, null, null, "14",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind02Out,new String[] {"15","14","-04","14",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bappmethOut,new String[] {null, null, null, "07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdescOut,new String[] {null, null, "01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc05Out,new String[] {null, null, null, "13",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind05Out,new String[] {"12","13","-10","13",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc04Out,new String[] {null, null, null, "09",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind04Out,new String[] {"08","09","-08","09",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc06Out,new String[] {null, null, null, "11",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind06Out,new String[] {"11","16","-03","16",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc07Out,new String[] {null, null, null, "42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind07Out,new String[] {"41","42","-41","42",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zunitOut,new String[] {"14","02","-14",null, null, null, null, null, null, null, null, null});
		//ILIFE-3399-STARTS
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "43", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "44", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "45", null, null, null, null, null, null, null, null});
		//ILIFE-3399-ENDS
		//ILIFE-3421-STARTS
		fieldIndMap.put(waitperiodOut,new String[] {null, null, null, "46", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bentrmOut,new String[] {null, null , null, "47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(poltypOut,new String[] {null, null, null, "48", null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmbasisOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		//ILIFE-3421-ENDS
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(optdsc08Out,new String[] {null, null, null, "73",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind08Out,new String[] {null,null,null,"74",null, null, null, null, null, null, null, null});		
		//fieldIndMap.put(rcessageOut,new String[] {null, null, null,"78",null, null, null, null, null, null, null, null});//ILJ-45
		fieldIndMap.put(rcesdteOut,new String[] {null, null, null,"79",null, null, null, null, null, null, null, null});//ILJ-45
		
		screenFields = new BaseData[] {crtabdesc, chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, numpols, planSuffix, lifenum, lifename, jlife, jlifename, life, coverage, rider, liencd, zagelit, anbAtCcd, statFund, statSect, statSubsect, sumin, frqdesc, singlePremium, premcess, instPrem, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate, benpln, optdsc03, optind03, optdsc01, optind01, optdsc02, optind02, bappmeth, zdesc, zlinstprem, livesno, waiverprem, optdsc05, optind05, optdsc04, optind04, optdsc06, optind06, taxamt, optdsc07, optind07,loadper, rateadj, fltmort, premadj,adjustageamt,zbinstprem, riskCessAge, riskCessTerm, premCessAge, premCessTerm,mortcls,zunit,waitperiod ,bentrm,poltyp ,prmbasis,dialdownoption, optdsc08, optind08};
		screenOutFields = new BaseData[][] {crtabdescOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, numpolsOut, plnsfxOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, liencdOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, suminOut, frqdescOut, singprmOut, premcessOut, instprmOut, crrcdOut, rcesdteOut, cbanprOut, rrtdatOut, rrtfrmOut, bbldatOut, benplnOut, optdsc03Out, optind03Out, optdsc01Out, optind01Out, optdsc02Out, optind02Out, bappmethOut, zdescOut, zlinstpremOut, livesnoOut, waiverpremOut, optdsc05Out, optind05Out, optdsc04Out, optind04Out, optdsc06Out, optind06Out, taxamtOut, optdsc07Out, optind07Out,loadperOut, rateadjOut, rateadjOut, premadjOut,adjustageamtOut,zbinstpremOut,rcessageOut, rcesstrmOut, pcessageOut, pcesstrmOut,mortclsOut,zunitOut,waitperiodOut ,bentrmOut ,poltypOut ,prmbasisOut,dialdownoptionOut, optdsc08Out, optind08Out};
		screenErrFields = new BaseData[] {crtabdescErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, numpolsErr, plnsfxErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, liencdErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, suminErr, frqdescErr, singprmErr, premcessErr, instprmErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr, benplnErr, optdsc03Err, optind03Err, optdsc01Err, optind01Err, optdsc02Err, optind02Err, bappmethErr, zdescErr, zlinstpremErr, livesnoErr, waiverpremErr, optdsc05Err, optind05Err, optdsc04Err, optind04Err, optdsc06Err, optind06Err, taxamtErr, optdsc07Err, optind07Err,loadperErr, rateadjErr, fltmortErr, premadjErr,adjustageamtErr,zbinstpremErr,rcessageErr, rcesstrmErr, pcessageErr, pcesstrmErr,mortclsErr,zunitErr,waitperiodErr , bentrmErr ,poltypErr ,prmbasisErr,dialdownoptionErr, optdsc08Err, optind08Err};
		screenDateFields = new BaseData[] {premcess, crrcd, riskCessDate, annivProcDate, rerateDate, rerateFromDate, benBillDate};
		screenDateErrFields = new BaseData[] {premcessErr, crrcdErr, rcesdteErr, cbanprErr, rrtdatErr, rrtfrmErr, bbldatErr};
		screenDateDispFields = new BaseData[] {premcessDisp, crrcdDisp, riskCessDateDisp, annivProcDateDisp, rerateDateDisp, rerateFromDateDisp, benBillDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr679screen.class;
		protectRecord = Sr679protect.class;
	}

}
