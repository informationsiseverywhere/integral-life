package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:18
 * Description:
 * Copybook name: LAPTLAPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Laptlapkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData laptlapFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData laptlapKey = new FixedLengthStringData(64).isAPartOf(laptlapFileKey, 0, REDEFINE);
  	public FixedLengthStringData laptlapChdrcoy = new FixedLengthStringData(1).isAPartOf(laptlapKey, 0);
  	public FixedLengthStringData laptlapChdrnum = new FixedLengthStringData(8).isAPartOf(laptlapKey, 1);
  	public FixedLengthStringData laptlapLife = new FixedLengthStringData(2).isAPartOf(laptlapKey, 9);
  	public FixedLengthStringData laptlapCoverage = new FixedLengthStringData(2).isAPartOf(laptlapKey, 11);
  	public FixedLengthStringData laptlapRider = new FixedLengthStringData(2).isAPartOf(laptlapKey, 13);
  	public PackedDecimalData laptlapPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(laptlapKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(laptlapKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(laptlapFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		laptlapFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}