package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.RegrpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Regrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RegrpfDAOImpl extends BaseDAOImpl<Regrpf> implements RegrpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(RegrpfDAOImpl.class);

	public List<Regrpf> searchRegrpfRecordByCoy(String coy) {

		String sqlSelect = "SELECT CHDRNUM,UNIQUE_NUMBER FROM Regrpf ORDER BY CHDRCOY, CHDRNUM, UNIQUE_NUMBER DESC";
		PreparedStatement psSelect = getPrepareStatement(sqlSelect);
		ResultSet sqlprs = null;
		List<Regrpf> regrpfList = new ArrayList<>();
		try {
			sqlprs = executeQuery(psSelect);
			while (sqlprs.next()) {
				Regrpf r = new Regrpf();
				r.setChdrnum(sqlprs.getString("CHDRNUM"));
				r.setUniqueNumber(sqlprs.getLong("UNIQUE_NUMBER"));
				regrpfList.add(r);
			}

		} catch (SQLException e) {
			LOGGER.error("searchCovrByChdrnum()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, sqlprs);
		}
		return regrpfList;

	}

	public int deleteRegrpfRecord(List<Long> uniqueNumberList) {
		if (uniqueNumberList == null || uniqueNumberList.isEmpty()) {
			return 0;
		}
		String stmt = "DELETE FROM REGRPF WHERE UNIQUE_NUMBER=?";
		PreparedStatement ps = getPrepareStatement(stmt);
		int i = 0;
		try {
			for (Long l : uniqueNumberList) {
				ps.setLong(1, l);
				i++;
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("deleteRacdRcds()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		return i;
	}

	public void insertRegrpfRecord(List<Regrpf> insertRegrpfList) {
		if (insertRegrpfList != null && !insertRegrpfList.isEmpty()) {
			StringBuilder insertSql = new StringBuilder();
			insertSql
					.append(" INSERT INTO REGRPF (CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,RGPYNUM,PYMT,CURRCD,PRCNT,RGPYTYPE,PAYREASON,RGPYSTAT,REVDTE,FPAYDATE,PROGNAME,EXCODE,EXREPORT,CRTABLE,TRANNO,LPAYDATE,USRPRF,JOBNM,DATIME)");
			insertSql.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement ps = getPrepareStatement(insertSql.toString());
			try {
				for (Regrpf r : insertRegrpfList) {
					ps.setString(1, r.getChdrcoy());
					ps.setString(2, r.getChdrnum());
					ps.setString(3, r.getLife());
					ps.setString(4, r.getCoverage());
					ps.setString(5, r.getRider());
					ps.setInt(6, r.getRgpynum());
					ps.setBigDecimal(7, r.getPymt());
					ps.setString(8, r.getCurrcd());
					ps.setBigDecimal(9, r.getPrcnt());
					ps.setString(10, r.getRgpytype());
					ps.setString(11, r.getPayreason());
					ps.setString(12, r.getRgpystat());
					ps.setInt(13, r.getRevdte());
					ps.setInt(14, r.getFirstPaydate());
					ps.setString(15, r.getProgname());
					ps.setString(16, r.getExcode());
					ps.setString(17, r.getExreport());
					ps.setString(18, r.getCrtable());
					ps.setInt(19, r.getTranno());
					ps.setInt(20, r.getLastPaydate());
					ps.setString(21, getUsrprf());
					ps.setString(22, getJobnm());
					ps.setTimestamp(23, getDatime());
					ps.addBatch();
				}
				ps.executeBatch();

			} catch (SQLException e) {
				LOGGER.error("insertRegrpgfRecord", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		}
	}
}
