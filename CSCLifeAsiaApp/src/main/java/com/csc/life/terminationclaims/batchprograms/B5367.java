/*
 * File: B5367.java
 * Date: 29 August 2009 21:14:18
 * Author: Quipoz Limited
 *
 * Class transformed from B5367.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.RegxpfTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.RegrDAO;
import com.csc.life.terminationclaims.dataaccess.dao.RegxpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Regrpf;
import com.csc.life.terminationclaims.dataaccess.model.Regxpf;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.terminationclaims.tablestructures.T6762rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*                   REGULAR PAYMENTS IN REVIEW
*                   --------------------------
* Overview
* ________
*
*   This program is part of the new 'Multi-Threading Batch
* Performance' suite. It runs directly after B5366, which 'splits'
* the REGPPF according to the number of regular payment programs
* to run.All references to the REGP are via REGXPF - a temporary
* file holding all the REGP records for this program to process.
*
*
* Regular payments processes only those REGP records which have
* the REVDTE <= effective date
*
* The following control totals are maintained within the program:
*
*       .    Number of payments extracted
*       .    Total processed
*       .    Total rejected
*       .    Number of locked contracts
*       .    Payments with ineligible status
*       .    Contracts with ineligible status
*       .    Components with ineligible status
*
* 1000-INITIALISE SECTION
* _______________________
*
*  -  Frequently-referenced tables are stored in working storage
*     arrays to minimize disk IO. These tables include
*  . T5679 - Transaction Codes For Status
*            Key: Transaction Code.
*
*  . T6689 - Regular Payment Exceptions
*            Key: Exclusion Reason Code
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be set to '****'.
*
*  . T6762 - Regular payments Exception codes
*            Key: Program name
*
*  -  Issue an override to read the correct REGXPF for this run.
*     The CRTTMPF process has already created a file specific to
*     the run using the  REGXPF fields and is identified  by
*     concatenating the following:-
*
*     'REGX'
*      BPRD-SYSTEM-PARAM04
*      BSSC-SCHEDULE-NUMBER
*
*      eg REGX2B0001,  for the first run
*         REGX2B0002,  for the second etc.
*
*     The number of threads would have been created by the
*     CRTTMPF process given the parameters of the process
*     definition of the CRTTMPF.
*     To get the correct member of the above physical for B5365
*     to read from, concatenate :-
*
*     'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
*  -  Initialise the static values of the LIFACMV copybook.
*
* 2000-READ SECTION
* _________________
*
* -  Read the REGX records sequentially incrementing the control
*    total.
*
* -  If end of file move ENDP to WSSP-EDTERROR.
*
* 2500-EDIT SECTION
* _________________
*
* -  Move OK to WSSP-EDTERROR.
*
*
*  Check that Regular Payment is in a fit state to be processed
*  by reading T6693 and matching  the current transaction code,
*  (obtained from BSPRREC ), against those on  the  extra  data
*  screen.  If  the  transaction  code cannot be found then the
*  Regular Payment may not be processed so go on  to  the  next
*  record.
*
*  If  it  may  be  processed read the associated CHDR and COVR
*  records. Read CHDR with a READR. For the read  of  COVR  use
*  the  Plan  Suffix  from REGP. If this is zero then perform a
*  BEGN on COVR otherwise use READR for a direct read.
*
*  Check that the contract and the component are both  able  to
*  be  processed  by  checking  their  Premium  Status and Risk
*  Status codes against those on T5679 for the transaction.
*                                    If the statuses are invalid
*  add 1 to control totals 5 or 6 and move SPACES to
*  WSSP-EDTERROR.
*
*
* - 'Soft lock' the contract, if it is to be processed.
*    If the contract is already 'locked' increment control
*    total number 4 and  move SPACES to WSSP-EDTERROR.
*
*  3000-UPDATE SECTION
*  ___________________
*
*  Read CHDR, PAYR and REGP with READH. Set the Validflag on CHDR
*  to '2' and perform a REWRT. Increment the TRANNO by 1, set the
*  Validflag back to '1' and write the new CHDR record.
*  Set the Validflag on PAYR to '2' and perform a REWRT.
*  Set the TRANNO the CHDR-TRANNO from above, set the validflag
*  back to '1' and WRITR the new PAYR.
*
*  Write a PTRN record with the new incremented TRANNO.
*
*  Set the Validflag on REGP to '2' and perform a REWRT.
*
*  Set  the  Validflag  back to '1' and place the new TRANNO on
*  REGP. Replace the Regular Payment Status Code on  REGP  with
*  the corresponding Status Code from T6693.
*
*  Perform a WRITR on REGP.
*
*  Remove the softlock on the contract.
*
*  Read the next REGP record.
*
*
*
* 4000-CLOSE SECTION
* __________________
*
*  - Close Files.
*
*  - Delete the Override function for the REGXPF file
*
*   Error Processing:
*
*     Perform the 600-FATAL-ERROR section. The
*     SYSR-SYSERR-TYPE flag does not need to be set in this
*     program, because MAINB takes care of a system errors.
*
*          (BATD processing is handled in MAINB)
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5367 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private RegxpfTableDAM regxpf = new RegxpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5367");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaFoundItem = new FixedLengthStringData(1).init("N");
	private Validator foundItem = new Validator(wsaaFoundItem, "Y");

	private FixedLengthStringData wsaaValidTrcode = new FixedLengthStringData(1).init("N");
	private Validator validTrcode = new Validator(wsaaValidTrcode, "Y");

	private FixedLengthStringData wsaaRegxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaRegxFn, 0, FILLER).init("REGX");
	private FixedLengthStringData wsaaRegxRunid = new FixedLengthStringData(2).isAPartOf(wsaaRegxFn, 4);
	private ZonedDecimalData wsaaRegxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaRegxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaT6693Key1 = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat1 = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key1, 0);
	private FixedLengthStringData wsaaT6693Crtable1 = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key1, 2);
	//private int wsaaT6693Size = 80;
	//ILIFE-2628 fixed--Array size increased
	private int wsaaT6693Size = 1000;
	//ILIFE-2643 by slakkala fixed--Array size increased STARTS
	private FixedLengthStringData wsaaT6693Array = new FixedLengthStringData(78000);
	//ENDS
	private FixedLengthStringData[] wsaaT6693Rec = FLSArrayPartOfStructure(1000, 78, wsaaT6693Array, 0);
	private FixedLengthStringData[] wsaaT6693Key = FLSDArrayPartOfArrayStructure(6, wsaaT6693Rec, 0);
	private FixedLengthStringData[] wsaaT6693Paystat = FLSDArrayPartOfArrayStructure(2, wsaaT6693Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT6693Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT6693Key, 2, HIVALUES);
	private FixedLengthStringData[][] wsaaT6693Data = FLSDArrayPartOfArrayStructure(12, 6, wsaaT6693Rec, 6);
	private FixedLengthStringData[][] wsaaT6693Rgpystat = FLSDArrayPartOfArrayStructure(2, wsaaT6693Data, 0);
	private FixedLengthStringData[][] wsaaT6693Trcode = FLSDArrayPartOfArrayStructure(4, wsaaT6693Data, 2);
	private String itemrec = "ITEMREC";
	private String chdrrgprec = "CHDRRGPREC";
	private String covrrec = "COVRREC";
	private String regrrec = "REGRREC";
	private String ptrnrec = "PTRNREC";
	private String payrrec = "PAYRREC";
	private String chdrpayrec = "CHDRPAYREC";
		/* ERRORS */
	private String h791 = "H791";
	private String ivrm = "IVRM";
	private String t5679 = "T5679";
	private String t6693 = "T6693";
	private String t6762 = "T6762";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;

	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
	private FixedLengthStringData wsysPayrkey = new FixedLengthStringData(20).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysPayrkey, 0);
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(77).isAPartOf(wsysSystemErrorParams, 20);
		/* WSAA-MISCELLANEOUS */
	private String wsaaValidStatus = "";
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrnumStore = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaNextPaystat = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaT6693Ix = new IntegerData();

		/*Component (Coverage/Rider) Record*/
//	private CovrTableDAM covrIO = new CovrTableDAM();

	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Payor Details Logical File*/
//	private PayrTableDAM payrIO = new PayrTableDAM();


	private T5679rec t5679rec = new T5679rec();
	private T6693rec t6693rec = new T6693rec();
	private T6762rec t6762rec = new T6762rec();
	
	private Regxpf regxpfRec = new Regxpf();
	private Payrpf payrIO = new Payrpf();
	private Regppf regpIO = new Regppf();
	private Regrpf regrIO = new Regrpf();
	private Covrpf covrIO = new Covrpf();
	private Chdrpf chdrpayIO = new Chdrpf();
	private Chdrpf chdrrgpIO = new Chdrpf();
	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private RegxpfDAO regxpfDAO = getApplicationContext().getBean("regxpfDAO", RegxpfDAO.class);
	private RegrDAO regrpfDAO = getApplicationContext().getBean("RegrDAO", RegrDAO.class);
	private RegppfDAO regppfDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
    private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
    private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    
	private Map<String, List<Chdrpf>> chdrpfMap = new HashMap<>();
	private Map<String, List<Covrpf>> covrpfMap = new HashMap<>();
	private Map<String, List<Regppf>> regppfMap = new HashMap<>();
	private Map<String, List<Payrpf>> payrpfMap = new HashMap<>();
	
	private List<Regrpf> regrpfListInst = new ArrayList<>();
	private List<Chdrpf> chdrpfListUpdt = new ArrayList<>();
	private List<Chdrpf> chdrpfListInst = new ArrayList<>();
	private List<Payrpf> payrpfListUpdt = new ArrayList<>();
	private List<Payrpf> payrpfListInst = new ArrayList<>();
	private List<Regppf> regppfListUpdt = new ArrayList<>();
	private List<Regppf> regppfListInst = new ArrayList<>();
	private List<Ptrnpf> ptrnpfList = new ArrayList<>();
	
	private Iterator<Regxpf> iter;
    private int batchID, batchExtractSize;
	private int ct01Val, ct02Val, ct05Val, ct06Val, ct07Val, ct04Val;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1599,
		exit2090,
		exit2579,
		exit2790
	}

	public B5367() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		wsspEdterror.set(varcom.oK);
		wsaaRegxRunid.set(bprdIO.getSystemParam04());
		wsaaRegxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
//		StringBuilder stringVariable1 = new StringBuilder();
//		stringVariable1.append("OVRDBF FILE(REGXPF) TOFILE(");
//		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
//		stringVariable1.append("/");
//		stringVariable1.append(wsaaRegxFn.toString());
//		stringVariable1.append(") ");
//		stringVariable1.append("MBR(");
//		stringVariable1.append(wsaaThreadMember.toString());
//		stringVariable1.append(")");
//		stringVariable1.append(" SEQONLY(*YES 1000)");
//		wsaaQcmdexc.setLeft(stringVariable1.toString());
//		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
//		regxpf.openInput();
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
        readChunkRecord();
        if(wsspEdterror.equals(varcom.endp)){
        	return;
        }
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		this.loadTables();
	}

	private void readChunkRecord() {
		List<Regxpf> regxpfList = regxpfDAO.findResult(wsaaRegxFn.toString(), wsaaThreadMember.toString(), batchExtractSize, batchID);
		if(regxpfList.isEmpty()){
			wsspEdterror.set(varcom.endp);
			return;
		}
		this.iter= regxpfList.iterator();
		List<String> chdrnumList = new ArrayList<>();
		for (Regxpf p : regxpfList) {
			chdrnumList.add(p.getChdrnum());
		}
		String coy = bsprIO.getCompany().toString();
		regppfMap = regppfDAO.searchRecords(chdrnumList);
		chdrpfMap = chdrpfDAO.searchChdrpf(coy, chdrnumList);
		covrpfMap = covrpfDAO.searchCovrMap(coy, chdrnumList);
		payrpfMap = payrpfDAO.getPayrLifMap(coy, chdrnumList);
		
	}

private void loadTables(){
//	itemIO.setStatuz(varcom.oK);
//	itemIO.setItempfx("IT");
//	itemIO.setItemcoy(bsprIO.getCompany());
//	itemIO.setItemtabl(t5679);
//	itemIO.setItemitem(bprdIO.getAuthCode());
//	itemIO.setFunction(varcom.readr);
//	SmartFileCode.execute(appVars, itemIO);
//	if (isNE(itemIO.getStatuz(),varcom.oK)
//	&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
//		syserrrec.params.set(itemIO.getParams());
//		syserrrec.statuz.set(itemIO.getStatuz());
//		fatalError600();
//	}
//	t5679rec.t5679Rec.set(itemIO.getGenarea());
	
	String coy = bsprIO.getCompany().toString();
	
	List<Itempf> items = this.itemDAO.getAllItemitem("IT", coy, t5679, bprdIO.getAuthCode().toString());
	if(items.isEmpty()) {
		syserrrec.params.set(t5679);
		fatalError600();
	}
	t5679rec.t5679Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
	
//	itemIO.setItempfx("IT");
//	itemIO.setItemcoy(bsprIO.getCompany());
//	itemIO.setItemtabl(t6762);
//	itemIO.setItemitem(bprdIO.getBatchProgram());
//	itemIO.setFunction(varcom.readr);
//	SmartFileCode.execute(appVars, itemIO);
//	if (isNE(itemIO.getStatuz(),varcom.oK)) {
//		syserrrec.params.set(itemIO.getParams());
//		syserrrec.statuz.set(itemIO.getStatuz());
//		fatalError600();
//	}
//	t6762rec.t6762Rec.set(itemIO.getGenarea());
	
	items = this.itemDAO.getAllItemitem("IT", coy, t6762, bprdIO.getBatchProgram().toString());
	if(items.isEmpty()) {
		syserrrec.params.set(t6762);
		fatalError600();
	}
	t6762rec.t6762Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
	
//	itdmIO.setStatuz(varcom.oK);
//	itdmIO.setParams(SPACES);
//	itdmIO.setItemcoy(bsprIO.getCompany());
//	itdmIO.setItemtabl(t6693);
//	itdmIO.setItemitem(SPACES);
//	itdmIO.setItmfrm(varcom.vrcmMaxDate);
//	itdmIO.setFormat(itemrec);
//	itdmIO.setFunction(varcom.begn);
//	wsaaT6693Ix.set(1);
//	while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
		//performance improvement --  atiwari23
//		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		//ILife3168 Paymentreview report --nnaveenkumar
//		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL");
//		loadT66931500();
//	}
	
	
	wsaaT6693Ix.set(1);
	Map<String,List<Itempf>> itemMap = itemDAO.loadSmartTable("IT", coy, t6693);
	for(String itemkey:itemMap.keySet()){
		for(Itempf i:itemMap.get(itemkey)){
			if(i.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata())<=0){
				loadT66931500(i);
			}
		}
	}
}

	protected void loadT66931500(Itempf itdmIO) {
		// SmartFileCode.execute(appVars, itdmIO);
		// if (isNE(itdmIO.getStatuz(),varcom.oK)
		// && isNE(itdmIO.getStatuz(),varcom.endp)) {
		// syserrrec.params.set(itdmIO.getParams());
		// syserrrec.statuz.set(itdmIO.getStatuz());
		// fatalError600();
		// }
		// if (isEQ(itdmIO.getStatuz(),varcom.endp)
		// || isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		// || isNE(itdmIO.getItemtabl(),t6693)) {
		// itdmIO.setStatuz(varcom.endp);
		// goTo(GotoLabel.exit1599);
		// }
		if (isGT(wsaaT6693Ix, wsaaT6693Size)) {
			syserrrec.params.set(t6693);
			syserrrec.statuz.set(h791);
			fatalError600();
		}
		if (isLT(bsscIO.getEffectiveDate(), itdmIO.getItmfrm())
				|| isGT(bsscIO.getEffectiveDate(), itdmIO.getItmto())) {
			//itdmIO.setFunction(varcom.nextr);
			//goTo(GotoLabel.exit1599);
			return;
		}
		t6693rec.t6693Rec.set(StringUtil.rawToString(itdmIO.getGenarea()));
		wsaaT6693Key[wsaaT6693Ix.toInt()].set(itdmIO.getItemitem());
		for (wsaaSub1.set(1); !(isGT(wsaaSub1, 12)); wsaaSub1.add(1)) {
			wsaaT6693Trcode[wsaaT6693Ix.toInt()][wsaaSub1.toInt()]
					.set(t6693rec.trcode[wsaaSub1.toInt()]);
			wsaaT6693Rgpystat[wsaaT6693Ix.toInt()][wsaaSub1.toInt()]
					.set(t6693rec.rgpystat[wsaaSub1.toInt()]);
		}
		//itdmIO.setFunction(varcom.nextr);
		wsaaT6693Ix.add(1);
	}


protected void readFile2000()
	{
		try {
			readFile2010();
		}
		catch (GOTOException e){
		}
	}

protected void readFile2010()
	{
//	regxpf.read(regxpfRec);
//	//MIBT-102
//	if (regxpf.isAtEnd()||isEQ(regxpfRec.getrevdte, ZERO)) {
//			wsspEdterror.set(varcom.endp);
//			goTo(GotoLabel.exit2090);
//		}
		if(!iter.hasNext()){
			batchID++;
			readChunkRecord();
			if(wsspEdterror.equals(varcom.endp)){
				return;
			}
		}
		regxpfRec = iter.next();
		if (regxpfRec.getRevdte() == 0) {
			wsspEdterror.set(varcom.endp);
			return;
		}
//		contotrec.totno.set(ct01);
//		contotrec.totval.set(1);
//		callContot001();
		this.ct01Val++;
		readRegp2100();
		wsysChdrnum.set(regxpfRec.getChdrnum());
	}

protected void readRegp2100()
	{
		regpRead2110();
	}

protected void regpRead2110()
	{
//		regpIO.setDataArea(SPACES);
//		regpIO.setChdrcoy(regxpfRec.getchdrcoy);
//		regpIO.setChdrnum(regxpfRec.getchdrnum);
//		regpIO.setLife(regxpfRec.getlife);
//		regpIO.setCoverage(regxpfRec.getcoverage);
//		regpIO.setRider(regxpfRec.getrider);
//		regpIO.setRgpynum(regxpfRec.getrgpynum);
//		regpIO.setFunction(varcom.readh);
//		regpIO.setFormat("REGPREC");
//		SmartFileCode.execute(appVars, regpIO);
//		if (isNE(regpIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(regpIO.getParams());
//			syserrrec.statuz.set(regpIO.getStatuz());
//			fatalError600();
//		}
		
		regpIO = null;
		
		if (regppfMap != null && regppfMap.containsKey(regxpfRec.getChdrnum())) {
			for (Regppf pf : regppfMap.get(regxpfRec.getChdrnum())) {
				if (pf.getChdrcoy().equals(regxpfRec.getChdrcoy())
						&& pf.getLife().equals(regxpfRec.getLife())
						&& pf.getCoverage().equals(regxpfRec.getCoverage())
						&& pf.getRider().equals(regxpfRec.getRider())
						&& pf.getRgpynum() == regxpfRec.getRgpynum()
						&& "1".equals(pf.getValidflag())) {
					regpIO = pf;
				}
			}
		}
		
		if (regpIO == null) {
			syserrrec.params.set(regxpfRec.getChdrnum());
			fatalError600();
		}
	}

protected void edit2500()
	{
		try {
			read2510();
		}
		catch (GOTOException e){
		}
	}

protected void read2510()
	{
		wsspEdterror.set(varcom.oK);
		checkT66932600();
		if (isEQ(wsspEdterror,SPACES)) {
			goTo(GotoLabel.exit2579);
		}
		readChdrCovr2700();
		if (isEQ(wsspEdterror,SPACES)) {
			goTo(GotoLabel.exit2579);
		}
		softlock2580();
		if (isEQ(sftlockrec.statuz,"LOCK")) {
//			contotrec.totno.set(ct04);
//			contotrec.totval.set(1);
//			callContot001();
			this.ct04Val++;
			wsspEdterror.set(SPACES);
			regrIO.setExcode(t6762rec.regpayExcpCsfl.toString());
			regrIO.setExreport("Y");
			reportProcess2900();
		}
	}

protected void softlock2580()
	{
		start2580();
	}

protected void start2580()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(regxpfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(regxpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void checkT66932600()
	{
		start2610();
		foundItem2650();
	}

protected void start2610()
	{
		wsaaFoundItem.set("N");
		wsaaValidTrcode.set("N");
		wsaaT6693Ix.set(1);
		wsaaT6693Paystat1.set(regxpfRec.getRgpystat());
		wsaaT6693Crtable1.set(regxpfRec.getCrtable());
		 searchlabel1:
		{
			for (; isLT(wsaaT6693Ix,wsaaT6693Rec.length); wsaaT6693Ix.add(1)){
				if (isEQ(wsaaT6693Key[wsaaT6693Ix.toInt()],wsaaT6693Key1)) {
					foundItem.setTrue();
					break searchlabel1;
				}
			}
			wsaaFoundItem.set("N");
		}
		if (isEQ(wsaaFoundItem,"N")) {
			wsaaT6693Ix.set(1);
			wsaaT6693Crtable1.set("****");
			wsaaT6693Paystat1.set(regxpfRec.getRgpystat());
			 searchlabel2:
			{
				for (; isLT(wsaaT6693Ix,wsaaT6693Rec.length); wsaaT6693Ix.add(1)){
					if (isEQ(wsaaT6693Key[wsaaT6693Ix.toInt()],wsaaT6693Key1)) {
						foundItem.setTrue();
						break searchlabel2;
					}
				}
			}
		}
	}

protected void foundItem2650()
	{
		if (foundItem.isTrue()) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)); wsaaSub1.add(1)){
				if (isEQ(bprdIO.getAuthCode(),wsaaT6693Trcode[wsaaT6693Ix.toInt()][wsaaSub1.toInt()])) {
					wsaaNextPaystat.set(wsaaT6693Rgpystat[wsaaT6693Ix.toInt()][wsaaSub1.toInt()]);
					validTrcode.setTrue();
				}
			}
		}
		if (isEQ(wsaaValidTrcode,"N")) {
			wsspEdterror.set(SPACES);
//			contotrec.totno.set(ct05);
//			contotrec.totval.set(1);
//			callContot001();
			this.ct05Val++;
			regrIO.setExcode(t6762rec.regpayExcpIpst.toString());
			regrIO.setExreport("Y");
			reportProcess2900();
		}
	}

protected void readChdrCovr2700()
	{
		try {
			readChdrCovrPara2700();
		}
		catch (GOTOException e){
		}
	}

protected void readChdrCovrPara2700()
	{
//		chdrrgpIO.setChdrpfx("CH");
//		chdrrgpIO.setChdrcoy(bsprIO.getCompany());
//		chdrrgpIO.setChdrnum(regxpfRec.getChdrnum());
//		chdrrgpIO.setFunction(varcom.readr);
//		chdrrgpIO.setFormat(chdrrgprec);
//		SmartFileCode.execute(appVars, chdrrgpIO);
//		if ((isNE(chdrrgpIO.getStatuz(),varcom.oK))) {
//			syserrrec.params.set(chdrrgpIO.getParams());
//			syserrrec.statuz.set(chdrrgpIO.getStatuz());
//			fatalError600();
//		}
		chdrrgpIO = null;
		if (chdrpfMap != null && chdrpfMap.containsKey(regxpfRec.getChdrnum())) {
			for (Chdrpf c : chdrpfMap.get(regxpfRec.getChdrnum())) {
				if ("CH".equals(c.getChdrpfx())) {
					chdrrgpIO = c;
				}
			}
		}
		if (chdrrgpIO == null) {
			syserrrec.params.set(regxpfRec.getChdrnum());
			fatalError600();
		}
		wsaaValidStatus = "N";
		wsaaStatcode.set(chdrrgpIO.getStatcode());
		wsaaPstcde.set(chdrrgpIO.getPstcde());
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)); wsaaSub1.add(1)){
			validateChdrStatus2850();
		}
		if (isEQ(wsaaValidStatus,"N")) {
			wsspEdterror.set(SPACES);
//			contotrec.totno.set(ct06);
//			contotrec.totval.set(1);
//			callContot001();
			this.ct06Val++;
			regrIO.setExcode(t6762rec.regpayExcpIchs.toString());
			regrIO.setExreport("Y");
			reportProcess2900();
			goTo(GotoLabel.exit2790);
		}
//		covrIO.setChdrcoy(bsprIO.getCompany());
//		covrIO.setChdrnum(regxpfRec.getChdrnum());
//		covrIO.setLife(regxpfRec.getLife());
//		covrIO.setCoverage(regxpfRec.getCoverage());
//		covrIO.setRider(regxpfRec.getRider());
//		covrIO.setPlanSuffix(regxpfRec.getPlnsfx());
//		covrIO.setFunction(varcom.readr);
//		covrIO.setFormat(covrrec);
//		SmartFileCode.execute(appVars, covrIO);
//		if ((isNE(covrIO.getStatuz(),varcom.oK))) {
//			syserrrec.params.set(covrIO.getParams());
//			syserrrec.statuz.set(covrIO.getStatuz());
//			fatalError600();
//		}
		covrIO = null;
		if (covrpfMap != null && covrpfMap.containsKey(regxpfRec.getChdrnum())) {
			for (Covrpf c : covrpfMap.get(regxpfRec.getChdrnum())) {
				if (c.getLife().equals(regxpfRec.getLife())
						&& c.getCoverage().equals(regxpfRec.getCoverage())
						&& c.getRider().equals(regxpfRec.getRider()) 
						&& c.getPlanSuffix() == regxpfRec.getPlanSuffix()) {
					covrIO = c;
				}
			}
		}
		if (covrIO == null) {
			syserrrec.params.set(regxpfRec.getChdrnum());
			fatalError600();
		}
		wsaaValidStatus = "N";
		wsaaStatcode.set(covrIO.getStatcode());
		wsaaPstcde.set(covrIO.getPstatcode());
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)); wsaaSub1.add(1)){
			validateCovrStatus2860();
		}
		if (isEQ(wsaaValidStatus,"N")) {
			wsspEdterror.set(SPACES);
//			contotrec.totno.set(ct07);
//			contotrec.totval.set(1);
//			callContot001();
			this.ct07Val++;
			regrIO.setExcode(t6762rec.regpayExcpIcos.toString());
			regrIO.setExreport("Y");
			reportProcess2900();
		}
	}

protected void validateChdrStatus2850()
	{
		/*PARA*/
		if (isEQ(t5679rec.cnRiskStat[wsaaSub1.toInt()],wsaaStatcode)) {
			for (wsaaSub2.set(1); !(isGT(wsaaSub2,12)); wsaaSub2.add(1)){
				if (isEQ(t5679rec.cnPremStat[wsaaSub2.toInt()],wsaaPstcde)) {
					wsaaSub2.set(13);
					wsaaValidStatus = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void validateCovrStatus2860()
	{
		/*PARA*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub1.toInt()],wsaaStatcode)) {
			for (wsaaSub2.set(1); !(isGT(wsaaSub2,12)); wsaaSub2.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaSub2.toInt()],wsaaPstcde)) {
					wsaaSub2.set(13);
					wsaaValidStatus = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void reportProcess2900()
	{
		start2910();
	}

protected void start2910()
	{
		regrIO.setChdrcoy(regxpfRec.getChdrcoy());
		regrIO.setChdrnum(regxpfRec.getChdrnum());
		regrIO.setLife(regxpfRec.getLife());
		regrIO.setCoverage(regxpfRec.getCoverage());
		regrIO.setRider(regxpfRec.getRider());
		regrIO.setRgpynum(regxpfRec.getRgpynum());
		regrIO.setRgpytype(regxpfRec.getRgpytype());
		regrIO.setRgpystat(regxpfRec.getRgpystat());
		regrIO.setCrtable(regxpfRec.getCrtable());
		regrIO.setTranno(regxpfRec.getTranno());
		regrIO.setPymt(regpIO.getPymt());
		regrIO.setCurrcd(regpIO.getCurrcd());
		regrIO.setPrcnt(regpIO.getPrcnt());
		regrIO.setPayreason(regpIO.getPayreason());
		regrIO.setRevdte(regpIO.getRevdte());
		regrIO.setFirstPaydate(regpIO.getFirstPaydate());
		regrIO.setLastPaydate(regpIO.getLastPaydate());
		regrIO.setProgname(bprdIO.getBatchProgram().toString());
		this.regrpfListInst.add(regrIO);
//		regrIO.setFunction("WRITR");
//		regrIO.setFormat(regrrec);
//		SmartFileCode.execute(appVars, regrIO);
//		if (isNE(regrIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(regrIO.getParams());
//			syserrrec.statuz.set(regrIO.getStatuz());
//			fatalError600();
//		}
	}

protected void update3000()
	{
		/*UPDATE*/
		rewriteChdr3200();
		rewritePayr3300();
		writePtrn3400();
		rewriteRegp3500();
		releaseSoftlock4000();
		/*EXIT*/
	}

protected void rewriteChdr3200()
	{
		start3210();
	}

protected void start3210()
	{
//		chdrpayIO.setParams(SPACES);
//		chdrpayIO.setChdrcoy(chdrrgpIO.getChdrcoy());
//		chdrpayIO.setChdrnum(chdrrgpIO.getChdrnum());
//		chdrpayIO.setFunction(varcom.begnh);
//		chdrpayIO.setFormat(chdrpayrec);
//		SmartFileCode.execute(appVars, chdrpayIO);
//		if ((isNE(chdrpayIO.getStatuz(),varcom.oK))
//		&& (isNE(chdrpayIO.getStatuz(),varcom.endp))) {
//			syserrrec.params.set(chdrpayIO.getParams());
//			syserrrec.statuz.set(chdrpayIO.getStatuz());
//			fatalError600();
//		}
//		if ((isNE(chdrpayIO.getChdrcoy(),chdrrgpIO.getChdrcoy()))
//		|| (isNE(chdrpayIO.getChdrnum(),chdrrgpIO.getChdrnum()))
//		|| (isNE(chdrpayIO.getValidflag(),chdrrgpIO.getValidflag()))
//		|| (isEQ(chdrpayIO.getStatuz(),varcom.endp))) {
//			chdrpayIO.setStatuz(varcom.endp);
//			syserrrec.params.set(chdrpayIO.getParams());
//			syserrrec.statuz.set(chdrpayIO.getStatuz());
//			fatalError600();
//		}
		chdrpayIO = null;
		if (chdrpfMap != null && chdrpfMap.containsKey(chdrrgpIO.getChdrnum())) {
			for (Chdrpf c : chdrpfMap.get(chdrrgpIO.getChdrnum())) {
				chdrpayIO = c;
				Chdrpf pf = new Chdrpf(chdrpayIO);
				pf.setUniqueNumber(chdrpayIO.getUniqueNumber());
				pf.setValidflag('2');
				pf.setCurrto(bsscIO.getEffectiveDate().toInt());
				this.chdrpfListUpdt.add(pf);
				setPrecision(chdrpayIO.getTranno(), 0);
				chdrpayIO.setTranno(add(chdrpayIO.getTranno(),1).toInt());
				chdrpayIO.setValidflag('1');
				chdrpayIO.setCurrfrom(bsscIO.getEffectiveDate().toInt());
				chdrpayIO.setCurrto(varcom.vrcmMaxDate.toInt());
				this.chdrpfListInst.add(chdrpayIO);
				break;
			}
		}
		if (chdrpayIO == null) {
			syserrrec.params.set(chdrrgpIO.getChdrnum());
			fatalError600();
		}
		
//		chdrpayIO.setValidflag("2");
//		chdrpayIO.setCurrto(bsscIO.getEffectiveDate());
//		chdrpayIO.setFunction(varcom.rewrt);
//		chdrpayIO.setFormat(chdrpayrec);
//		SmartFileCode.execute(appVars, chdrpayIO);
//		if ((isNE(chdrpayIO.getStatuz(),varcom.oK))) {
//			syserrrec.params.set(chdrpayIO.getParams());
//			syserrrec.statuz.set(chdrpayIO.getStatuz());
//			fatalError600();
//		}
//		setPrecision(chdrpayIO.getTranno(), 0);
//		chdrpayIO.setTranno(add(chdrpayIO.getTranno(),1));
//		chdrpayIO.setValidflag("1");
//		chdrpayIO.setCurrfrom(bsscIO.getEffectiveDate());
//		chdrpayIO.setCurrto(varcom.vrcmMaxDate);
//		chdrpayIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, chdrpayIO);
//		if (isNE(chdrpayIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(chdrpayIO.getParams());
//			syserrrec.statuz.set(chdrpayIO.getStatuz());
//			fatalError600();
//		}
	}

protected void rewritePayr3300()
	{
		start3310();
	}

protected void start3310()
	{
//		payrIO.setParams(SPACES);
//		payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
//		payrIO.setChdrnum(chdrrgpIO.getChdrnum());
//		payrIO.setPayrseqno(ZERO);
//		payrIO.setFunction(varcom.begnh);
//		payrIO.setFormat(payrrec);
//		SmartFileCode.execute(appVars, payrIO);
//		if (isNE(payrIO.getStatuz(),varcom.oK)
//		&& isNE(payrIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(payrIO.getParams());
//			syserrrec.statuz.set(payrIO.getStatuz());
//			fatalError600();
//		}
//		if (isNE(payrIO.getChdrnum(),chdrrgpIO.getChdrnum())
//		|| isNE(payrIO.getChdrcoy(),chdrrgpIO.getChdrcoy())
//		|| isNE(payrIO.getValidflag(),"1")
//		|| isEQ(payrIO.getStatuz(),varcom.endp)) {
//			syserrrec.params.set(payrIO.getParams());
//			syserrrec.statuz.set(payrIO.getStatuz());
//			fatalError600();
//		}
		payrIO = null;
		if (payrpfMap != null && payrpfMap.containsKey(chdrrgpIO.getChdrnum())) {
			for (Payrpf c : payrpfMap.get(chdrrgpIO.getChdrnum())) {
				if (c.getPayrseqno() == 1 && "1".equals(c.getValidflag())) {	//ILIFE-6771 changing 0 to 1 since in payrpf no record with seq no. 0 exists.
					payrIO = c;
					Payrpf pf = new Payrpf(payrIO);
					pf.setValidflag("2");
					this.payrpfListUpdt.add(pf);
					payrIO.setTranno(chdrpayIO.getTranno());
					payrIO.setValidflag("1");
					this.payrpfListInst.add(payrIO);
					break;
				}
			}
		}
		if (payrIO == null) {
			syserrrec.params.set(chdrrgpIO.getChdrnum());
			fatalError600();
		}
//		payrIO.setValidflag("2");
//		payrIO.setFunction(varcom.rewrt);
//		payrIO.setFormat(payrrec);
//		SmartFileCode.execute(appVars, payrIO);
//		if (isNE(payrIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(payrIO.getParams());
//			syserrrec.statuz.set(payrIO.getStatuz());
//			fatalError600();
//		}
//		payrIO.setTranno(chdrpayIO.getTranno());
//		payrIO.setValidflag("1");
//		payrIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, payrIO);
//		if (isNE(payrIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(payrIO.getParams());
//			syserrrec.params.set(payrIO.getStatuz());
//			fatalError600();
//		}
	}

protected void writePtrn3400()
	{
		start3410();
	}

protected void start3410()
	{
	    Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setTermid(varcom.vrcmTermid.toString());
		ptrnIO.setTrdt(datcon1rec.intDate.toInt());
		ptrnIO.setTrtm(Integer.parseInt(getCobolTime()));
		ptrnIO.setUserT(999999);
		ptrnIO.setBatcpfx(batcdorrec.prefix.toString());
		ptrnIO.setBatccoy(bsprIO.getCompany().toString());
		ptrnIO.setBatcbrn(bsscIO.getInitBranch().toString());
		ptrnIO.setBatcactyr(bsscIO.getAcctYear().toInt());
		ptrnIO.setBatctrcde(bprdIO.getAuthCode().toString());
		ptrnIO.setBatcactmn(bsscIO.getAcctMonth().toInt());
		ptrnIO.setBatcbatch(batcdorrec.batch.toString());
		ptrnIO.setTranno(chdrpayIO.getTranno());
		ptrnIO.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrpayIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(regxpfRec.getChdrnum());/* IJTI-1523 */
		ptrnIO.setValidflag("1");
		this.ptrnpfList.add(ptrnIO);
//		ptrnIO.setFormat(ptrnrec);
//		ptrnIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, ptrnIO);
//		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(ptrnIO.getParams());
//			syserrrec.statuz.set(ptrnIO.getStatuz());
//			fatalError600();
//		}
	}

protected void rewriteRegp3500()
	{
		start3501();
	}

protected void start3501()
	{
//		regpIO.setUser(999999);
//		regpIO.setTransactionDate(datcon1rec.intDate);
//		regpIO.setTransactionTime(getCobolTime());
//		regpIO.setValidflag("2");
//		regpIO.setFunction(varcom.rewrt);
//		SmartFileCode.execute(appVars, regpIO);
//		if (isNE(regpIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(regpIO.getParams());
//			syserrrec.statuz.set(regpIO.getStatuz());
//			fatalError600();
//		}
//		regpIO.setValidflag("1");
//		regpIO.setTranno(chdrpayIO.getTranno());
//		regpIO.setAprvdate(varcom.vrcmMaxDate);
//		if(isNE(regpIO.getRevdte(),varcom.vrcmMaxDate)){
//			regpIO.setRgpystat(wsaaNextPaystat);
//		}
//		regpIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, regpIO);
//		if (isNE(regpIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(regpIO.getParams());
//			syserrrec.statuz.set(regpIO.getStatuz());
//			fatalError600();
//		}
		Regppf pf = new Regppf(regpIO);
		pf.setUser(999999);
		pf.setTransactionDate(datcon1rec.intDate.toInt());
		pf.setTransactionTime(Integer.parseInt(getCobolTime()));
		pf.setValidflag("2");
		this.regppfListUpdt.add(pf);
		regpIO.setValidflag("1");
		regpIO.setTranno(chdrpayIO.getTranno());
		regpIO.setAprvdate(varcom.vrcmMaxDate.toInt());
		if(isNE(regpIO.getRevdte(),varcom.vrcmMaxDate)){
			regpIO.setRgpystat(wsaaNextPaystat.toString());
		}
		this.regppfListInst.add(regpIO);
		regrIO.setExcode("");
		regrIO.setExreport("");
		reportProcess2900();
//		contotrec.totno.set(ct02);
//		contotrec.totval.set(1);
//		callContot001();
		this.ct02Val++;
	}

protected void commit3500() {
	commitControlTotals();
	
	regrpfDAO.insertRecords(regrpfListInst);
	regrpfListInst.clear();
	
	chdrpfDAO.updateInvalidChdrRecord(chdrpfListUpdt);
	chdrpfListUpdt.clear();
	chdrpfDAO.insertChdrValidRecord(chdrpfListInst);
	chdrpfListInst.clear();
	
	payrpfDAO.updatePayrRecord(payrpfListUpdt);
	payrpfListUpdt.clear();
	payrpfDAO.insertPayrpfList(payrpfListInst);
	payrpfListInst.clear();
	
	regppfDAO.updateValidflag(regppfListUpdt);
	regppfListUpdt.clear();
	regppfDAO.insertRecords(regppfListInst);
	regppfListInst.clear();
	
	ptrnpfDAO.insertPtrnPF(ptrnpfList);
	ptrnpfList.clear();
	
}

private void commitControlTotals() {
	contotrec.totno.set(ct01);
	contotrec.totval.set(ct01Val);
	callContot001();
	ct01Val = 0;
	
	contotrec.totno.set(ct02);
	contotrec.totval.set(ct02Val);
	callContot001();
	ct02Val=0;
	
	contotrec.totno.set(ct04);
	contotrec.totval.set(ct04Val);
	callContot001();
	ct04Val=0;
	
	contotrec.totno.set(ct05);
	contotrec.totval.set(ct05Val);
	callContot001();
	ct05Val=0;
	
	contotrec.totno.set(ct06);
	contotrec.totval.set(ct06Val);
	callContot001();
	ct06Val=0;
	
	contotrec.totno.set(ct07);
	contotrec.totval.set(ct07Val);
	callContot001();
	ct07Val=0;
}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void releaseSoftlock4000()
	{
		start4001();
	}

protected void start4001()
	{
		sftlockrec.company.set(batcdorrec.company);
		sftlockrec.entity.set(regxpfRec.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(999999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
	}

protected void close4000()
	{
	chdrpfMap.clear();
	covrpfMap.clear();
	regppfMap.clear();
	payrpfMap.clear();
	chdrpfMap = null;
	covrpfMap =null;
	regppfMap =null;
	payrpfMap=null;
	iter = null;
	
	regrpfListInst.clear();
	chdrpfListUpdt.clear();
	chdrpfListInst.clear();
	payrpfListUpdt.clear();
	payrpfListInst.clear();
	regppfListUpdt.clear();
	regppfListInst.clear();
	ptrnpfList.clear();
	
	regrpfListInst=null;
	chdrpfListUpdt=null;
	chdrpfListInst=null;
	payrpfListUpdt=null;
	payrpfListInst=null;
	regppfListUpdt=null;
	regppfListInst=null;
	ptrnpfList=null;
	
	lsaaStatuz.set(varcom.oK);
	}
}
