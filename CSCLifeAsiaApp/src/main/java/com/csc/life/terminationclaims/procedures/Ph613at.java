/*
 * File: Ph613at.java
 * Date: 30 August 2009 1:11:27
 * Author: Quipoz Limited
 * 
 * Class transformed from PH613AT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;    //ILIFE-9101
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.OverpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Overpf;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.contractservicing.procedures.Revgenat;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.newbusiness.procedures.Nlgcalc;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*    Lapse Reinstatement AT Module
*
*    This module is the controlling program for Lapse Re-
*    instatement. It is normally requested from program PH613
*    (Lapse Reinstatement). A Debit Accounting entry will be
*    post to the Suspense Account of the policy owner while
*    credit entry for Lapse Reinstatement fee will be posted.
*    Also a letter request for Lapse Reinstatement will be
*    issued.
*
*****************************************************************
* </pre>
*/
public class Ph613at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PH613AT";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	protected FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(32);  //MLIL-680
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
		/*01  WSAA-OTHER-KEYS.                                             */
	//private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();//FSD026 Lapse Reinstatement modify visibility 
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaFeeTax = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/* FORMATS */
	protected static final String chdrmjarec = "CHDRMJAREC";       //MLIL-680
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	protected static final String tr384 = "TR384";
	private static final String t7508 = "T7508";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
		/* ERRORS */
	protected static final String g437 = "G437";
	private static final String h134 = "H134";
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	protected Datcon1rec datcon1rec = new Datcon1rec();  //MLIL-680
	private Datcon2rec datcon2rec = new Datcon2rec();
	protected Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	protected Sftlockrec sftlockrec = new Sftlockrec();        //MLIL-680
	protected Syserrrec syserrrec = new Syserrrec();
	protected Varcom varcom = new Varcom();
	protected Letrqstrec letrqstrec = new Letrqstrec();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec2 = new Lifrtrnrec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private T5645rec t5645rec = new T5645rec();
	protected Tr384rec tr384rec = new Tr384rec();//FSD026 Lapse Reinstatement modify visibility
	private T7508rec t7508rec = new T7508rec();
	protected Letcokcpy letcokcpy = new Letcokcpy();//FSD026 Lapse Reinstatement modify visibility
	protected Atmodrec atmodrec = new Atmodrec();   //MLIL-680
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	//private WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();	//FSD026 Lapse Reinstatement modify visibility
	protected WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();
	
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();	//ILIFE-3997
	PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private TaxdpfDAO taxdpfDAO = getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	Ptrnpf ptrnpf = new Ptrnpf();
    private Itempf itemIO = null;
    private Descpf descIO = null;
    private boolean BTPRO028Permission  = false;
	 private OverpfDAO overpfDAO =  getApplicationContext().getBean("overpfDAO", OverpfDAO.class);
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		gotTr52e1500, 
		exit1500
	}

	public Ph613at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		process1000();
		/*EXIT*/
		exitProgram();
	}

protected void process1000()
	{
		start1010();
	}

protected void start1010()
	{
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaTransAreaInner.wsaaTransArea.set(atmodrec.transArea);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(wsaaBatckey.batcBatccoy);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			xxxxFatalError();
		}
		
		//validateCovr1000();     
		callProgram(Revgenat.class, atmodrec.atmodRec);
		
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.function.set("LOCK");
		sftlockrec.enttyp.set(chdrmjaIO.getChdrpfx());
		sftlockrec.company.set(chdrmjaIO.getChdrcoy());
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(wsaaTransAreaInner.wsaaUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
		//ILIFE-3997 Starts
		if(isEQ(chdrmjaIO.getNlgflg(),"Y")) {
			nlgflg3020();
		}
		//ILIFE-3997 End
		if (isGT(wsaaTransAreaInner.wsaaTotalfee, 0)) {
			calcFeeTax1500();
			postFee2000();
			writeLetc3000();
		}
		dryProcessing8000();
		releaseSoftlock9000();
		
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", "BTPRO028", appVars, "IT");
		if(BTPRO028Permission)
		{
			Overpf overpf = overpfDAO.getOverpfRecord(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString(), "1");
			if(null!= overpf && isEQ(overpf.getOverdueflag(),"O") && isEQ(overpf.getEnddate(),varcom.vrcmMaxDate ))
			{
				overpfDAO.updateOverpfRecord(wsaaToday.toInt(),overpf.getUnique_number());
			}
		}	
	}
//ILIFE-8010
/*protected void validateCovr1000()
{
	List<String> chdrnumList = new ArrayList<>();
	chdrnumList.add(wsaaPrimaryChdrnum.toString());
	covrpfMap = covrpfDAO.searchCovrrnlByChdrnum(chdrnumList);
	
	if (covrpfMap != null && covrpfMap.containsKey(wsaaPrimaryChdrnum.toString())) 
	{
		for (Covrpf c : covrpfMap.get(wsaaPrimaryChdrnum.toString()))
		{
			if(c.getValidflag().equals("1" ) &&  !(c.getStatcode().equals(chdrmjaIO.getStatcode().toString())) &&  !(c.getPstatcode().equals(chdrmjaIO.getPstatcode().toString())))
			{
				covrpfInsertList.add(c);
			}
		}
	}  
	covrpfDAO.updateCovrValidFlag(covrpfInsertList);
}*/

//ILIFE-3997 Starts
protected void nlgflg3020() {
	ptrnpf= ptrnpfDAO.getPtrnData(chdrmjaIO.getChdrcoy().toString(),chdrmjaIO.getChdrnum().toString(),wsaaBatckey.batcBatctrcde.toString());
	if(ptrnpf != null)
		nlgcalcrec.tranno.set(ptrnpf.getTranno());
	nlgcalcrec.fsuco.set(wsaaTransAreaInner.wsaaFsuCoy);
	nlgcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
	nlgcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
	nlgcalcrec.effdate.set(wsaaToday);
	nlgcalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	nlgcalcrec.frmdate.set(chdrmjaIO.getOccdate());
	nlgcalcrec.todate.set(varcom.maxdate);
	nlgcalcrec.cnttype.set(chdrmjaIO.getCnttype());
	nlgcalcrec.language.set(atmodrec.language);
	nlgcalcrec.Cntcurr.set(chdrmjaIO.getCntcurr());
	nlgcalcrec.billchnl.set(chdrmjaIO.getBillchnl());
	nlgcalcrec.billfreq.set(chdrmjaIO.getBillfreq());
	nlgcalcrec.occdate.set(chdrmjaIO.getOccdate());
	nlgcalcrec.function.set("LINST");
	nlgcalcrec.status.set(Varcom.oK);
	callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec);
	if(isNE(nlgcalcrec.status,varcom.oK)) {
		syserrrec.params.set(nlgcalcrec.nlgcalcRec);
		xxxxFatalError();
	}
}
//ILIFE-3997 End
protected void calcFeeTax1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1500();
				case gotTr52e1500: 
					gotTr52e1500();
				case exit1500: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1500()
	{
		wsaaFeeTax.set(ZERO);
		if (isEQ(wsaaTransAreaInner.wsaaTotalfee, ZERO)) {
			goTo(GotoLabel.exit1500);
		}
		/* Read TR52D.                                                     */
		itemIO = new Itempf();
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy().toString());
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrmjaIO.getRegister().toString());
		itemIO = itemDAO.getItempfRecord(itemIO);
	
		if (itemIO == null) {
			itemIO = new Itempf();
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrmjaIO.getChdrcoy().toString());
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO = itemDAO.getItempfRecord(itemIO);

			if (itemIO == null) {
				syserrrec.statuz.set("MRNF");
				syserrrec.params.set(chdrmjaIO.getChdrcoy().toString().concat(tr52d));
				xxxxFatalError();
			}
		}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itemIO.getGenarea()));
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit1500);
		}
		itemIO = null; 
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		List<Itempf> list = itemDAO.getItdmByFrmdate(chdrmjaIO.getChdrcoy().toString(), tr52e, wsaaTr52eKey.toString(), wsaaTransAreaInner.wsaaReinstDate.toInt());
		if(list.isEmpty()) {
			itemIO = new Itempf();
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrmjaIO.getChdrcoy().toString());
			itemIO.setItemtabl(tr52e);
			itemIO.setItemitem(wsaaTr52eKey.toString());
			itemIO = itemDAO.getItempfRecord(itemIO);
		}else {
			itemIO = list.get(0);
		}
		
		if (itemIO == null) {
			goTo(GotoLabel.gotTr52e1500);
		}
		itemIO = null; 
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		list = itemDAO.getItdmByFrmdate(chdrmjaIO.getChdrcoy().toString(), tr52e, wsaaTr52eKey.toString(), wsaaTransAreaInner.wsaaReinstDate.toInt());
		if(list.isEmpty()) {
			itemIO = new Itempf();
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrmjaIO.getChdrcoy().toString());
			itemIO.setItemtabl(tr52e);
			itemIO.setItemitem(wsaaTr52eKey.toString());
			itemIO = itemDAO.getItempfRecord(itemIO);
		}else {
			itemIO = list.get(0);
		}
		if(itemIO == null) {
			syserrrec.params.set(chdrmjaIO.getChdrcoy().toString().concat(tr52e).concat(wsaaTr52eKey.toString()));
			syserrrec.statuz.set("MRNF");
			xxxxFatalError();
		}
		
	}

protected void gotTr52e1500()
	{
	    if(itemIO == null) {
	    	tr52erec.tr52eRec.set(SPACES);
	    }else {
		tr52erec.tr52eRec.set(StringUtil.rawToString(itemIO.getGenarea()));
	    }
	    
		if (isNE(tr52erec.taxind12, "Y")) {
			return ;
		}
		/* Call TR52D tax subroutine                                       */
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.planSuffix.set(0);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.cnttype.set(chdrmjaIO.getCnttype());
		txcalcrec.register.set(chdrmjaIO.getRegister());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.transType.set("RSTF");
		txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
		txcalcrec.effdate.set(wsaaTransAreaInner.wsaaReinstDate);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCntcurr());
		stringVariable1.addExpression(tr52erec.txitem);
		stringVariable1.setStringInto(txcalcrec.rateItem);
		txcalcrec.amountIn.set(wsaaTransAreaInner.wsaaTotalfee);
		txcalcrec.tranno.set(add(chdrmjaIO.getTranno(),1));     //ILIFE-9334
		txcalcrec.batckey.set(atmodrec.batchKey);
		txcalcrec.cntTaxInd.set("Y");
		txcalcrec.jrnseq.set(1);
		txcalcrec.language.set(atmodrec.language);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			xxxxFatalError();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[2]);
			}
		}
		
		Taxdpf taxdIO = new Taxdpf();
		taxdIO.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		taxdIO.setChdrnum(chdrmjaIO.getChdrnum().toString());
		taxdIO.setTrantype(txcalcrec.transType.toString());
		taxdIO.setLife("  ");
		taxdIO.setCoverage("  ");
		taxdIO.setRider("  ");
		taxdIO.setPlansfx(0);
		taxdIO.setEffdate(txcalcrec.effdate.toInt());
		taxdIO.setInstfrom(varcom.vrcmMaxDate.toInt());
		taxdIO.setBillcd(varcom.vrcmMaxDate.toInt());
		taxdIO.setInstto(varcom.vrcmMaxDate.toInt());
		taxdIO.setTranno(chdrmjaIO.getTranno().toInt());
		taxdIO.setBaseamt(txcalcrec.amountIn.getbigdata());
		taxdIO.setTranref("  ");
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(txcalcrec.taxrule);
		stringVariable2.addExpression(txcalcrec.rateItem);
		taxdIO.setTranref(stringVariable2.toString());
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1].getbigdata());
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2].getbigdata());
		taxdIO.setTaxamt03(BigDecimal.ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1].toString());
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2].toString());
		taxdIO.setTxabsind03(" ");
		taxdIO.setTxtype01(txcalcrec.taxType[1].toString());
		taxdIO.setTxtype02(txcalcrec.taxType[2].toString());
		taxdIO.setTxtype03("  ");
		taxdIO.setPostflg("P");
		List<Taxdpf> list = new ArrayList<Taxdpf>();
		list.add(taxdIO);
		taxdpfDAO.insertTaxdPF(list);
	
	}

protected void postFee2000()
	{
		postFee2010();
	}

protected void postFee2010()
	{
	    itemIO = itemDAO.findItemBySeq(smtpfxcpy.item.toString(), chdrmjaIO.getChdrcoy().toString(), t5645, wsaaSubr, "1", "  ");
	
		if (itemIO == null) {
			syserrrec.params.set(chdrmjaIO.getChdrcoy().toString().concat(t5645).concat(wsaaSubr));
			syserrrec.statuz.set(h134);
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itemIO.getGenarea()));
		descIO = new Descpf();
		descIO.setDescpfx(smtpfxcpy.item.toString());
		descIO.setDesccoy(chdrmjaIO.getChdrcoy().toString());
		descIO.setDesctabl(t1688);
		descIO.setDescitem(StringUtil.fillSpace(wsaaBatckey.batcBatctrcde.toString(),8));
		descIO.setLanguage(atmodrec.language.toString());
		descIO.setItemseq("  ");
		descIO = descDAO.getDescRow(descIO);

		if (descIO == null) {
			syserrrec.params.set(chdrmjaIO.getChdrcoy().toString().concat(t1688).concat(wsaaBatckey.batcBatctrcde.toString()));
			syserrrec.statuz.set("MRNF");
			xxxxFatalError();
		}
		lifrtrnrec2.batckey.set(atmodrec.batchKey);
		lifrtrnrec2.rdocnum.set(chdrmjaIO.getChdrnum());
		lifrtrnrec2.rldgacct.set(chdrmjaIO.getChdrnum());
		//lifrtrnrec2.tranno.set(chdrmjaIO.getTranno()); 
		lifrtrnrec2.tranno.set(add(chdrmjaIO.getTranno(),1)); //ILIFE-9101
	   	lifrtrnrec2.jrnseq.set(0);
		lifrtrnrec2.crate.set(0);
		lifrtrnrec2.acctamt.set(0);
		lifrtrnrec2.rcamt.set(0);
		lifrtrnrec2.rldgcoy.set(chdrmjaIO.getChdrcoy());
		lifrtrnrec2.origcurr.set(chdrmjaIO.getBillcurr());
		lifrtrnrec2.origamt.set(wsaaTransAreaInner.wsaaTotalfee);
		lifrtrnrec2.origamt.add(wsaaFeeTax);
		lifrtrnrec2.trandesc.set(descIO.getLongdesc());
		lifrtrnrec2.user.set(wsaaTransAreaInner.wsaaUser);
		lifrtrnrec2.termid.set(wsaaTransAreaInner.wsaaTermid);
		lifrtrnrec2.transactionDate.set(wsaaTransAreaInner.wsaaTranDate);
		lifrtrnrec2.transactionTime.set(wsaaTransAreaInner.wsaaTranTime);
		lifrtrnrec2.genlcur.set(SPACES);
		lifrtrnrec2.genlcoy.set(chdrmjaIO.getChdrcoy());
		lifrtrnrec2.sacscode.set(t5645rec.sacscode01);
		lifrtrnrec2.sacstyp.set(t5645rec.sacstype01);
		lifrtrnrec2.glcode.set(t5645rec.glmap01);
		lifrtrnrec2.glsign.set(t5645rec.sign01);
		lifrtrnrec2.contot.set(t5645rec.cnttot01);
		lifrtrnrec2.postyear.set(SPACES);
		lifrtrnrec2.postmonth.set(SPACES);
		lifrtrnrec2.effdate.set(wsaaTransAreaInner.wsaaReinstDate);
		lifrtrnrec2.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec2.substituteCode[1].set(chdrmjaIO.getCnttype());
		lifrtrnrec2.function.set("PSTW");
		callProgram(Lifrtrn.class, lifrtrnrec2.lifrtrnRec);
		if (isNE(lifrtrnrec2.statuz,varcom.oK)) {
			syserrrec.params.set(lifrtrnrec2.lifrtrnRec);
			xxxxFatalError();
		}
		lifacmvrec1.batckey.set(atmodrec.batchKey);
		lifacmvrec1.rldgcoy.set(chdrmjaIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(chdrmjaIO.getChdrcoy());
		lifacmvrec1.rdocnum.set(chdrmjaIO.getChdrnum());
		lifacmvrec1.rldgacct.set(chdrmjaIO.getChdrnum());
		lifacmvrec1.tranref.set(chdrmjaIO.getChdrnum());
		//lifacmvrec1.tranno.set(chdrmjaIO.getTranno());
		lifacmvrec1.tranno.set(add(chdrmjaIO.getTranno(),1));   //ILIFE-9101
		lifacmvrec1.jrnseq.set(1);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.origcurr.set(chdrmjaIO.getBillcurr());
		lifacmvrec1.origamt.set(wsaaTransAreaInner.wsaaTotalfee);
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.user.set(wsaaTransAreaInner.wsaaUser);
		lifacmvrec1.termid.set(wsaaTransAreaInner.wsaaTermid);
		lifacmvrec1.transactionDate.set(wsaaTransAreaInner.wsaaTranDate);
		lifacmvrec1.transactionTime.set(wsaaTransAreaInner.wsaaTranTime);
		lifacmvrec1.genlcur.set(SPACES);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			xxxxFatalError();
		}
		if (isLT(wsaaTransAreaInner.wsaaReinstDate, datcon2rec.intDate2)) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode02);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec1.glcode.set(t5645rec.glmap02);
			lifacmvrec1.glsign.set(t5645rec.sign02);
			lifacmvrec1.contot.set(t5645rec.cnttot02);
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode03);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec1.glcode.set(t5645rec.glmap03);
			lifacmvrec1.glsign.set(t5645rec.sign03);
			lifacmvrec1.contot.set(t5645rec.cnttot03);
		}
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.effdate.set(wsaaTransAreaInner.wsaaReinstDate);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(chdrmjaIO.getCnttype());
		lifacmvrec1.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void writeLetc3000()
	{
		letc3010();
	}

protected void letc3010()
	{
		itemIO = new Itempf();
		itemIO.setItempfx(smtpfxcpy.item.toString());
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy().toString());
		itemIO.setItemtabl(tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype());
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		itemIO.setItemitem(stringVariable1.toString());
		itemIO = itemDAO.getItempfRecord(itemIO);
		if (itemIO==null) {
			syserrrec.params.set(chdrmjaIO.getChdrcoy().toString().concat(tr384).concat(stringVariable1.toString()));
			syserrrec.statuz.set(g437);
			xxxxFatalError();
		}
		tr384rec.tr384Rec.set(StringUtil.rawToString(itemIO.getGenarea()));
		letrqstrec.statuz.set(SPACES);
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrmjaIO.getChdrpfx());
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdoccoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.despnum.set(chdrmjaIO.getDespnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		letcokcpy.recCode.set("LD");
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letcokcpy.ldDate.set(wsaaTransAreaInner.wsaaReinstDate);
		letrqstrec.otherKeys.set(letcokcpy.saveOtherKeys);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(letrqstrec.statuz);
			syserrrec.params.set(letrqstrec.params);
			xxxxFatalError();
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (itemIO==null) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (itemIO==null) {
			return ;
		}
		t7508rec.t7508Rec.set(StringUtil.rawToString(itemIO.getGenarea()));
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaTransAreaInner.wsaaFsuCoy);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypAplsupto.set(varcom.vrcmMaxDate);
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void readT75088100()
	{
	    itemIO = new Itempf();
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company.toString());
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key.toString());
		itemIO = itemDAO.getItempfRecord(itemIO);
	}

protected void releaseSoftlock9000()
	{
		/*START*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.enttyp.set(chdrmjaIO.getChdrpfx());
		sftlockrec.company.set(chdrmjaIO.getChdrcoy());
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxxxErrorBomb();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorBomb()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure WSAA-TRANS-AREA--INNER
 */
//private static final class WsaaTransAreaInner { //FSD026 Lapse Reinstatement modify visibility
protected static final class WsaaTransAreaInner { 

	public FixedLengthStringData wsaaTransArea = new FixedLengthStringData(95);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	public ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned(); //MLIL-680
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
	/*private FixedLengthStringData wsaaReinstDate = new FixedLengthStringData(8).isAPartOf(wsaaTransArea, 70);*/ //FSD026 Lapse Reinstatement modify visibility
	public FixedLengthStringData wsaaReinstDate = new FixedLengthStringData(8).isAPartOf(wsaaTransArea, 70);
	public ZonedDecimalData wsaaTotalfee = new ZonedDecimalData(17, 2).isAPartOf(wsaaTransArea, 78); //MLIL-680
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}

}
