/*
 * File: Unlmatc.java
 * Date: 30 August 2009 2:50:49
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLMATC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.terminationclaims.dataaccess.UtrsmatTableDAM;
import com.csc.life.terminationclaims.recordstructures.Matccpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* UNLMATC - Maturity Calculation Method #1.
* -----------------------------------------------
*
* This  program  is  an  item  entry  on  T6598,  the  maturity  claim
* subroutine   method  table.  This  is  the  calculation  process  for
* obtaining the Bid value of the investments, the  amount  is  returned
* in the currency of the fund.
*
*       The  following  is  the  linkage  information  passed  to  this
* subroutine:-
*
*            - company
*            - contract header number
*            - suffix
*            - life number
*            - joint-life number
*            - coverage
*            - rider
*            - crtable
*            - effective date
*            - language
*            - estimated value
*            - actual value
*            - currency
*            - element code
*            - description
*            - type code
*            - status
*
* PROCESSING.
* ----------
*
* If it is the first time through this routine:
*
* NB. - accumulate all records/amounts for the same fund, including
*       initial and accumulation.
*
*     - set-up the key and BEGN option  and read fund UTRS
*         keyed:  Coy, Chdr, Life, Jlife, Coverage, Rider
*
*     - for each fund on component get the now bid price
*         read the VPRC (prices file), keyed on:
*         - Virtual fund, Unit type and Effective-date.
*
* If "INITIAL" units
*
*     the no. of initial deemed units are multiplied by
*     the bid price.
*     This result is the maturity value amount of the initial
*     units.
*
* If "ACCUMULATION" units
*
*     the no. of accumulation deemed units are multiplied by
*     the bid price.
*     This result is the maturity value amount of the accumulation
*     units.
*
*     Return one line for each fund, currency conversion will
*     occur in the calling program.
*
*     Return Estimate Amount
*            Virtual Fund
*            Fund Type 'I' or 'A'
*            Fund Description
*            Currency Code
*            Actual Amount = zero
*
*     else
*
*            set status = ENDP.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*   Include value of interest bearing funds when calculating          *
*   the surrender value. Note that a new field, MATC-ENDF has         *
*   been introduced; it will be set to 'I' when unit linked           *
*   funds have been processed, to indicate that interest bearing      *
*   funds should be processed the next time through. Once             *
*   interest bearing funds have been processed, MATC-ENDF             *
*   will be set to 'Y' to indicate that fund processing               *
*   is complete.                                                      *
*                                                                     *
*****************************************************************
* </pre>
*/
public class Unlmatc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "UNLMATC";
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);

	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator policy1 = new Validator(wsaaPlnsfx, 1);

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, 1);
	private Validator partPlan = new Validator(wsaaPlanSwitch, 2);
	private Validator summaryPart = new Validator(wsaaPlanSwitch, 3);
	private PackedDecimalData wsaaInitialAmount = new PackedDecimalData(18, 5).init(0);
	private PackedDecimalData wsaaAccumAmount = new PackedDecimalData(18, 5).init(0);
	private PackedDecimalData wsaaInitialBidPrice = new PackedDecimalData(9, 5).init(0);
	private PackedDecimalData wsaaAccumBidPrice = new PackedDecimalData(9, 5).init(0);
	private FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaNoMore = new FixedLengthStringData(1);
	private Validator endOfFund = new Validator(wsaaNoMore, "Y");

	private FixedLengthStringData wsaaUtype = new FixedLengthStringData(1);
	private Validator initialUnits = new Validator(wsaaUtype, "I");

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaFirstHits = new FixedLengthStringData(1).init("Y");
	private Validator firstHits = new Validator(wsaaFirstHits, "Y");
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
		/* ERRORS */
	private static final String g117 = "G117";
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t5687 = "T5687";
	private static final String utrsclmrec = "UTRSCLMREC";
	private static final String utrsmatrec = "UTRSMATREC";
	private static final String hitsrec = "HITSREC";
	private DescTableDAM descIO = new DescTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();
	private UtrsmatTableDAM utrsmatIO = new UtrsmatTableDAM();
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5515rec t5515rec = new T5515rec();
	private Varcom varcom = new Varcom();
	private Matccpy matccpy = new Matccpy();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readAccum5100, 
		seExit9090, 
		dbExit9190
	}

	public Unlmatc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		matccpy.maturityRec = convertAndSetParam(matccpy.maturityRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(matccpy.status, varcom.endp)) {
			wsaaFirstTime.set("Y");
			return ;
		}
		if (firstTime.isTrue()) {
			initialization020();
		}
		if (isEQ(matccpy.endf, "I")) {
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			matccpy.estimatedVal.set(ZERO);
			matccpy.actualVal.set(ZERO);
			if (firstHits.isTrue()) {
				a100CallBegnHits();
			}
		}
		if (isEQ(matccpy.status, varcom.oK)) {
			if (isEQ(matccpy.endf, "I")) {
				a200InterestBearing();
			}
			else {
				if (wholePlan.isTrue()) {
					processWholePlan3000();
				}
				else {
					processComponent4000();
				}
			}
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialization020()
	{
		init020();
		processChoices023();
	}

protected void init020()
	{
		wsaaChdrcoy.set(matccpy.chdrChdrcoy);
		wsaaChdrnum.set(matccpy.chdrChdrnum);
		wsaaCoverage.set(matccpy.covrCoverage);
		wsaaRider.set(matccpy.covrRider);
		wsaaLife.set(matccpy.lifeLife);
		wsaaPlanSuffix.set(matccpy.planSuffix);
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		wsaaVirtualFund.set(SPACES);
		wsaaUnitType.set(SPACES);
		matccpy.description.set(SPACES);
		matccpy.status.set(varcom.oK);
		wsaaFirstTime.set("N");
		/* Initialize plan suffix to zeroes if part of a summary.*/
		wsaaPlanSwitch.set(matccpy.planSwitch);
		if (summaryPart.isTrue()) {
			wsaaPlanSuffix.set(ZERO);
			matccpy.planSuffix.set(ZERO);
		}
	}

protected void processChoices023()
	{
		if (wholePlan.isTrue()) {
			callBegnUtrsmat2000();
		}
		else {
			callBegnUtrsclm2100();
		}
		if (isEQ(matccpy.status, varcom.endp)) {
			getCrtableDesc2200();
		}
		/*EXIT*/
	}

protected void callBegnUtrsmat2000()
	{
		begnUtrsmat2000();
	}

protected void begnUtrsmat2000()
	{
		utrsmatIO.setParams(SPACES);
		utrsmatIO.setChdrcoy(wsaaChdrcoy);
		utrsmatIO.setChdrnum(wsaaChdrnum);
		utrsmatIO.setLife(wsaaLife);
		utrsmatIO.setCoverage(wsaaCoverage);
		utrsmatIO.setRider(wsaaRider);
		utrsmatIO.setPlanSuffix(ZERO);
		utrsmatIO.setUnitVirtualFund(wsaaVirtualFund);
		utrsmatIO.setUnitType(wsaaUnitType);
		utrsmatIO.setFormat(utrsmatrec);
		utrsmatIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrsmatIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsmatIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, utrsmatIO);
		if (isNE(utrsmatIO.getStatuz(), varcom.oK)
		&& isNE(utrsmatIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrsmatIO.getParams());
			dbError9100();
		}
		if (isNE(utrsmatIO.getChdrcoy(), wsaaChdrcoy)
		|| isNE(utrsmatIO.getChdrnum(), wsaaChdrnum)
		|| isNE(utrsmatIO.getLife(), wsaaLife)
		|| isNE(utrsmatIO.getCoverage(), wsaaCoverage)
		|| isNE(utrsmatIO.getRider(), wsaaRider)
		|| isEQ(utrsmatIO.getStatuz(), varcom.endp)) {
			/*     MOVE 'Y'                TO WSAA-FIRST-TIME               */
			utrsmatIO.setStatuz(varcom.endp);
		}
	}

protected void callBegnUtrsclm2100()
	{
		begnUtrsclm2100();
	}

protected void begnUtrsclm2100()
	{
		utrsclmIO.setParams(SPACES);
		utrsclmIO.setChdrcoy(wsaaChdrcoy);
		utrsclmIO.setChdrnum(wsaaChdrnum);
		utrsclmIO.setLife(wsaaLife);
		utrsclmIO.setCoverage(wsaaCoverage);
		utrsclmIO.setRider(wsaaRider);
		utrsclmIO.setPlanSuffix(wsaaPlanSuffix);
		utrsclmIO.setUnitVirtualFund(wsaaVirtualFund);
		utrsclmIO.setUnitType(wsaaUnitType);
		utrsclmIO.setFormat(utrsclmrec);
		utrsclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrsclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		SmartFileCode.execute(appVars, utrsclmIO);
		if (isNE(utrsclmIO.getStatuz(), varcom.oK)
		&& isNE(utrsclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrsclmIO.getParams());
			dbError9100();
		}
		if (isNE(utrsclmIO.getChdrcoy(), wsaaChdrcoy)
		|| isNE(utrsclmIO.getChdrnum(), wsaaChdrnum)
		|| isNE(utrsclmIO.getLife(), wsaaLife)
		|| isNE(utrsclmIO.getCoverage(), wsaaCoverage)
		|| isNE(utrsclmIO.getRider(), wsaaRider)
		|| isNE(utrsclmIO.getPlanSuffix(), wsaaPlanSuffix)
		|| isEQ(utrsclmIO.getStatuz(), varcom.endp)) {
			/*     MOVE 'Y'                TO WSAA-FIRST-TIME               */
			utrsclmIO.setStatuz(varcom.endp);
		}
	}

protected void getCrtableDesc2200()
	{
		callDescio2200();
	}

protected void callDescio2200()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(matccpy.chdrChdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(matccpy.crtable);
		descIO.setLanguage("E");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			dbError9100();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			matccpy.description.set(descIO.getLongdesc());
		}
		else {
			matccpy.description.set(SPACES);
		}
		matccpy.type.set("A");
	}

protected void processWholePlan3000()
	{
		getFirstUtrsmat3000();
	}

protected void getFirstUtrsmat3000()
	{
		if (isEQ(utrsmatIO.getStatuz(), varcom.endp)) {
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(matccpy.chdrChdrnum);
			wsaaCoverage.set(matccpy.covrCoverage);
			wsaaRider.set(matccpy.covrRider);
			wsaaLife.set(matccpy.lifeLife);
			matccpy.endf.set("I");
			return ;
		}
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		wsaaVirtualFund.set(utrsmatIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsmatIO.getUnitType());
		while ( !(isEQ(utrsmatIO.getStatuz(), varcom.endp)
		|| isNE(wsaaVirtualFund, utrsmatIO.getUnitVirtualFund())
		|| isNE(wsaaUnitType, utrsmatIO.getUnitType()))) {
			accumAmtWholePlan3100();
		}
		
		readVprnunl5000();
		checkEof6000();
		if (isEQ(matccpy.endf, "I")) {
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(matccpy.chdrChdrnum);
			wsaaCoverage.set(matccpy.covrCoverage);
			wsaaRider.set(matccpy.covrRider);
			wsaaLife.set(matccpy.lifeLife);
		}
	}

protected void accumAmtWholePlan3100()
	{
		addAmount3100();
		readNextUtrsmat3110();
	}

protected void addAmount3100()
	{
		if (isEQ(utrsmatIO.getUnitType(), "I")) {
			wsaaInitialAmount.add(utrsmatIO.getCurrentUnitBal());
			wsaaUtype.set("I");
		}
		else {
			wsaaAccumAmount.add(utrsmatIO.getCurrentUnitBal());
			wsaaUtype.set("A");
		}
	}

protected void readNextUtrsmat3110()
	{
		utrsmatIO.setFormat(utrsmatrec);
		utrsmatIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsmatIO);
		if (isNE(utrsmatIO.getStatuz(), varcom.oK)
		&& isNE(utrsmatIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrsmatIO.getParams());
			dbError9100();
		}
		if (isNE(utrsmatIO.getChdrcoy(), matccpy.chdrChdrcoy)
		|| isNE(utrsmatIO.getChdrnum(), matccpy.chdrChdrnum)
		|| isNE(utrsmatIO.getLife(), matccpy.lifeLife)
		|| isNE(utrsmatIO.getCoverage(), matccpy.covrCoverage)
		|| isNE(utrsmatIO.getRider(), matccpy.covrRider)
		|| isEQ(utrsmatIO.getStatuz(), varcom.endp)) {
			matccpy.endf.set("I");
			/*     MOVE 'Y'                TO WSAA-FIRST-TIME               */
			utrsmatIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processComponent4000()
	{
		begnUtrsclm4000();
	}

protected void begnUtrsclm4000()
	{
		if (isEQ(utrsclmIO.getStatuz(), varcom.endp)) {
			if (isEQ(utrsclmIO.getStatuz(), varcom.endp)) {
				matccpy.endf.set("I");
				wsaaVirtualFund.set(SPACES);
				wsaaChdrnum.set(matccpy.chdrChdrnum);
				wsaaCoverage.set(matccpy.covrCoverage);
				wsaaRider.set(matccpy.covrRider);
				wsaaLife.set(matccpy.lifeLife);
				return ;
			}
		}
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		wsaaVirtualFund.set(utrsclmIO.getUnitVirtualFund());
		wsaaUnitType.set(utrsclmIO.getUnitType());
		while ( !(isEQ(utrsclmIO.getStatuz(), varcom.endp)
		|| isNE(wsaaVirtualFund, utrsclmIO.getUnitVirtualFund())
		|| isNE(wsaaUnitType, utrsclmIO.getUnitType()))) {
			accumAmtPartPlan4100();
		}
		
		readVprnunl5000();
		checkEof6000();
		if (isEQ(matccpy.endf, "I")) {
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(matccpy.chdrChdrnum);
			wsaaCoverage.set(matccpy.covrCoverage);
			wsaaRider.set(matccpy.covrRider);
			wsaaLife.set(matccpy.lifeLife);
		}
	}

protected void accumAmtPartPlan4100()
	{
		addAmount4100();
		readNextUtrsclm4104();
	}

protected void addAmount4100()
	{
		if (isEQ(utrsclmIO.getUnitType(), "I")) {
			wsaaInitialAmount.add(utrsclmIO.getCurrentUnitBal());
			wsaaUtype.set("I");
		}
		else {
			wsaaAccumAmount.add(utrsclmIO.getCurrentUnitBal());
			wsaaUtype.set("A");
		}
	}

protected void readNextUtrsclm4104()
	{
		utrsclmIO.setFormat(utrsclmrec);
		utrsclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsclmIO);
		if (isNE(utrsclmIO.getStatuz(), varcom.oK)
		&& isNE(utrsclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrsclmIO.getParams());
			dbError9100();
		}
		if (isNE(utrsclmIO.getChdrcoy(), matccpy.chdrChdrcoy)
		|| isNE(utrsclmIO.getChdrnum(), matccpy.chdrChdrnum)
		|| isNE(utrsclmIO.getLife(), matccpy.lifeLife)
		|| isNE(utrsclmIO.getCoverage(), matccpy.covrCoverage)
		|| isNE(utrsclmIO.getRider(), matccpy.covrRider)
		|| isNE(utrsclmIO.getPlanSuffix(), matccpy.planSuffix)
		|| isEQ(utrsclmIO.getStatuz(), varcom.endp)) {
			/*     MOVE 'Y'                TO WSAA-FIRST-TIME               */
			matccpy.endf.set("I");
			utrsclmIO.setStatuz(varcom.endp);
			wsaaVirtualFund.set(SPACES);
			wsaaChdrnum.set(matccpy.chdrChdrnum);
			wsaaCoverage.set(matccpy.covrCoverage);
			wsaaRider.set(matccpy.covrRider);
			wsaaLife.set(matccpy.lifeLife);
		}
	}

protected void readVprnunl5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read5000();
				case readAccum5100: 
					readAccum5100();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read5000()
	{
		/*  read the VPRC file for each fund type*/
		if (isEQ(wsaaInitialAmount, ZERO)) {
			goTo(GotoLabel.readAccum5100);
		}
		vprnudlIO.setDataArea(SPACES);
		vprnudlIO.setCompany(matccpy.chdrChdrcoy);
		vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
		vprnudlIO.setUnitType("I");
		vprnudlIO.setEffdate(matccpy.effdate);
		vprnudlIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
        
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(), varcom.oK)
		&& isNE(vprnudlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			dbError9100();
		}
		if (isNE(wsaaVirtualFund, vprnudlIO.getUnitVirtualFund())
		|| isNE(vprnudlIO.getUnitType(), "I")
		|| isNE(matccpy.chdrChdrcoy, vprnudlIO.getCompany())) {
			syserrrec.params.set(wsaaVirtualFund);
			syserrrec.statuz.set(g117);
			systemError9000();
		}
		wsaaInitialBidPrice.set(vprnudlIO.getUnitBidPrice());
	}

protected void readAccum5100()
	{
		if (isEQ(wsaaAccumAmount, ZERO)) {
			return ;
		}
		vprnudlIO.setDataArea(SPACES);
		vprnudlIO.setCompany(matccpy.chdrChdrcoy);
		vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
		vprnudlIO.setUnitType("A");
		vprnudlIO.setEffdate(matccpy.effdate);
		vprnudlIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(), varcom.oK)
		&& isNE(vprnudlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			dbError9100();
		}
		if (isNE(wsaaVirtualFund, vprnudlIO.getUnitVirtualFund())
		|| isNE(vprnudlIO.getUnitType(), "A")
		|| isNE(matccpy.chdrChdrcoy, vprnudlIO.getCompany())) {
			syserrrec.params.set(wsaaVirtualFund);
			syserrrec.statuz.set(g117);
			systemError9000();
		}
		wsaaAccumBidPrice.set(vprnudlIO.getUnitBidPrice());
	}

protected void checkEof6000()
	{
		eof6000();
	}

protected void eof6000()
	{
		if (initialUnits.isTrue()) {
			compute(wsaaInitialAmount, 6).setRounded(mult(wsaaInitialAmount, wsaaInitialBidPrice));
			matccpy.estimatedVal.set(wsaaInitialAmount);
			matccpy.type.set("I");
		}
		else {
			compute(wsaaAccumAmount, 6).setRounded(mult(wsaaAccumAmount, wsaaAccumBidPrice));
			matccpy.estimatedVal.set(wsaaAccumAmount);
			matccpy.type.set("A");
		}
		if (summaryPart.isTrue()) {
			compute(matccpy.estimatedVal, 3).setRounded(div(matccpy.estimatedVal, matccpy.polsum));
		}
		matccpy.fund.set(wsaaVirtualFund);
		readT55157000();
	}

protected void readT55157000()
	{
		go7000();
	}

protected void go7000()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescitem(wsaaVirtualFund);
		descIO.setDesctabl(t5515);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(matccpy.chdrChdrcoy);
		descIO.setLanguage("E");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			dbError9100();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			matccpy.description.set(descIO.getLongdesc());
		}
		else {
			matccpy.description.set(SPACES);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(matccpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(wsaaVirtualFund);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(wsaaVirtualFund, itdmIO.getItemitem())
		|| isNE(matccpy.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5515)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			matccpy.currcode.set(SPACES);
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
			matccpy.currcode.set(t5515rec.currcode);
			matccpy.element.set(wsaaVirtualFund);
		}
	}

protected void a100CallBegnHits()
	{
		a110BegnHits();
	}

protected void a110BegnHits()
	{
		wsaaFirstHits.set("N");
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(wsaaChdrcoy);
		hitsIO.setChdrnum(wsaaChdrnum);
		hitsIO.setLife(wsaaLife);
		hitsIO.setCoverage(wsaaCoverage);
		hitsIO.setRider(wsaaRider);
		hitsIO.setPlanSuffix(ZERO);
		hitsIO.setZintbfnd(wsaaVirtualFund);
		hitsIO.setFormat(hitsrec);
		hitsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			dbError9100();
		}
		if (isNE(hitsIO.getChdrcoy(), wsaaChdrcoy)
		|| isNE(hitsIO.getChdrnum(), wsaaChdrnum)
		|| isNE(hitsIO.getLife(), wsaaLife)
		|| isNE(hitsIO.getCoverage(), wsaaCoverage)
		|| isNE(hitsIO.getRider(), wsaaRider)
		|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
			hitsIO.setStatuz(varcom.endp);
			matccpy.status.set(varcom.endp);
			wsaaFirstTime.set("Y");
		}
	}

protected void a200InterestBearing()
	{
		a210Start();
	}

protected void a210Start()
	{
		if (isEQ(hitsIO.getStatuz(), varcom.endp)) {
			matccpy.endf.set("Y");
			matccpy.status.set(varcom.endp);
			wsaaFirstTime.set("Y");
			return ;
		}
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		wsaaVirtualFund.set(hitsIO.getZintbfnd());
		while ( !(isEQ(hitsIO.getStatuz(), varcom.endp)
		|| isNE(wsaaVirtualFund, hitsIO.getZintbfnd()))) {
			a300AccumAmount();
		}
		
		matccpy.estimatedVal.set(wsaaAccumAmount);
		matccpy.fund.set(wsaaVirtualFund);
		matccpy.type.set("D");
		readT55157000();
	}

protected void a300AccumAmount()
	{
		a310AddAmount();
	}

protected void a310AddAmount()
	{
		wsaaAccumAmount.add(hitsIO.getZcurprmbal());
		hitsIO.setFormat(hitsrec);
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			dbError9100();
		}
		if (isNE(hitsIO.getChdrcoy(), matccpy.chdrChdrcoy)
		|| isNE(hitsIO.getChdrnum(), matccpy.chdrChdrnum)
		|| isNE(hitsIO.getLife(), matccpy.lifeLife)
		|| isNE(hitsIO.getCoverage(), matccpy.covrCoverage)
		|| isNE(hitsIO.getRider(), matccpy.covrRider)
		|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
			matccpy.endf.set("Y");
			hitsIO.setStatuz(varcom.endp);
			matccpy.status.set(varcom.endp);
			wsaaFirstTime.set("Y");
			wsaaFirstHits.set("Y"); //MIBT-377
		}
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
				case seExit9090: 
					seExit9090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		matccpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9100();
				case dbExit9190: 
					dbExit9190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		matccpy.status.set(varcom.bomb);
		exitProgram();
	}
}
