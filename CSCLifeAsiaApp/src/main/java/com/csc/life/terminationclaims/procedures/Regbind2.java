
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.recordstructures.Regpsubrec;
import com.csc.life.terminationclaims.tablestructures.T6695rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Regbind2 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REGBIND2  ";

	private FixedLengthStringData wsaaTerm = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaNumericYears = new ZonedDecimalData(6, 0).isAPartOf(wsaaTerm, 0).setUnsigned();
	private FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(wsaaNumericYears, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaLeadingZeroes = new FixedLengthStringData(3).isAPartOf(filler, 0);
	private FixedLengthStringData wsaaNumYears = new FixedLengthStringData(3).isAPartOf(filler, 3);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaNewAmtToPay = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAmount = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaYears = new ZonedDecimalData(6, 0);
		/* ERRORS */
	private static final String h967 = "H967";
		/* TABLES */
	private static final String t6695 = "T6695";
	private static final String t555 = "T555";
	private static final String ta83 = "TA83";
		/* FORMATS */
	private static final String itdmrec = "ITEMREC";
	private static final String regprec = "REGPREC";

	private FixedLengthStringData wsaaT6695Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6695Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6695Key, 0);
	private FixedLengthStringData wsaaT6695Cltype = new FixedLengthStringData(2).isAPartOf(wsaaT6695Key, 4);
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T6695rec t6695rec = new T6695rec();
	private Regpsubrec regpsubrec = new Regpsubrec();
	private boolean regpayFlag = false;
	private PackedDecimalData wsaaKeepSumins = new PackedDecimalData(17, 2);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);

	public Regbind2() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		regpsubrec.batcsubRec = convertAndSetParam(regpsubrec.batcsubRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		regpayFlag = FeaConfg.isFeatureExist(regpsubrec.subCompany.toString(), "CSCOM006", appVars, "IT");
		syserrrec.subrname.set(wsaaSubr);
		regpsubrec.subSubrname.set(wsaaSubr);
		wsaaNewAmtToPay.set(ZERO);
		wsaaNumYears.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaT6695Key.set(SPACES);
		regpsubrec.subStatuz.set(varcom.oK);
		retrvRegp100();
		if (isEQ(regpsubrec.subStatuz, varcom.oK)) {
			readT6695200();
		}
		if (isEQ(regpsubrec.subStatuz, varcom.oK)) {
			if(isEQ(regpsubrec.subBatctrcde,t555) || isEQ(regpsubrec.subBatctrcde,ta83))
				doProcessing300New();
			else
				doProcessing300();
		}
		if (isEQ(regpsubrec.subStatuz, varcom.oK)) {
				keepsRegp400New();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void retrvRegp100()
	{
		/*GO*/
		regpIO.setFormat(regprec);
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			regpsubrec.subStatuz.set(regpIO.getStatuz());
			return ;
		}
		wsaaT6695Crtable.set(regpIO.getCrtable());
		wsaaT6695Cltype.set(regpIO.getPayreason());
		/*EXIT*/
	}

protected void readT6695200()
	{
		go201();
	}

protected void go201()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(regpsubrec.subCompany);
		itdmIO.setItempfx("IT");
		itdmIO.setItemtabl(t6695);
		itdmIO.setItemitem(wsaaT6695Key);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(itdmIO.getStatuz());
			return ;
			/****     GO TO 190-EXIT                                            */
		}
		if ((isEQ(itdmIO.getStatuz(), varcom.endp))
		|| (isNE(itdmIO.getItemcoy(), regpsubrec.subCompany))
		|| (isNE(itdmIO.getItempfx(), "IT"))
		|| (isNE(itdmIO.getItemtabl(), t6695))
		|| (isNE(itdmIO.getItemitem(), wsaaT6695Key))) {
			itdmIO.setGenarea(SPACES);
			regpsubrec.subStatuz.set(h967);
		}
		else {
			t6695rec.t6695Rec.set(itdmIO.getGenarea());
		}
	}

protected void doProcessing300()
	{
		/*GO*/
		callDatcon3500();
		getSuminsforPymt();
		if (isNE(regpsubrec.subStatuz, varcom.oK)) {
			return ;
		}
		wsaaSub.set(1);
		checkT6695600();
		
		/*EXIT*/
	}

protected void keepsRegp400()
	{
		/*GO*/
		setPrecision(regpIO.getPymt(), 2);
		regpIO.setPymt(add(regpIO.getPymt(), wsaaNewAmtToPay));
		regpIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			regpsubrec.subStatuz.set(regpIO.getStatuz());
		}
		/*EXIT*/
	}

protected void keepsRegp400New()
{
	/*GO*/
	regpIO.setPymt(wsaaNewAmtToPay);
	setPrecision(regpIO.getPymt(), 2);
	regpIO.setFunction(varcom.keeps);
	SmartFileCode.execute(appVars, regpIO);
	if (isNE(regpIO.getStatuz(), varcom.oK)) {
		regpsubrec.subStatuz.set(regpIO.getStatuz());
	}
	/*EXIT*/
}

protected void callDatcon3500()
	{
		/*GO*/
		datcon3rec.frequency.set("01");
		datcon3rec.intDate2.set(regpIO.getAnvdate());
		datcon3rec.intDate1.set(regpIO.getFirstPaydate());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			regpsubrec.subStatuz.set(datcon3rec.statuz);
			return ;
		}
		else {
			wsaaTerm.set(datcon3rec.freqFactor);
		}
		/*EXIT*/
	}


protected void checkT6695600()
{
	/*GO*/
	int years = 0;
	years = wsaaNumYears.toInt();
		/*wsaaNewAmtToPay.set(regpIO.getPymt());*/
			while ( !(isGT(wsaaSub, 12))) {
					if (isLT(years, t6695rec.tupyr[wsaaSub.toInt()].toInt())
							|| isEQ(years, t6695rec.tupyr[wsaaSub.toInt()].toInt())) {
							compute(wsaaAmount, 2).set((div((mult(t6695rec.pctinc[wsaaSub.toInt()], wsaaNewAmtToPay)), 100)));
							wsaaNewAmtToPay.add(wsaaAmount);
							zrdecplrec.amountIn.set(wsaaNewAmtToPay);
							a000CallRounding();
							wsaaNewAmtToPay.set(zrdecplrec.amountOut);
							wsaaSub.set(13);
							return;
					}
					else{
						wsaaSub.add(1);
						if(regpayFlag){
							if(isEQ(wsaaSub,13)){
									handleContItem();
							}
						}
				}
			}
	/*EXIT*/
}

protected void doProcessing300New(){
	callDatcon9000();
	if (isNE(regpsubrec.subStatuz, varcom.oK)) {
		return ;
	}
	wsaaSub.set(1);
	checkT6695600New();
	/*EXIT*/
}

protected void callDatcon9000()
{
	/*GO*/
	datcon3rec.frequency.set("01");
	datcon3rec.intDate2.set(regpIO.getNextPaydate());
	datcon3rec.intDate1.set(regpIO.getFirstPaydate());
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		regpsubrec.subStatuz.set(datcon3rec.statuz);
		return ;
	}
	else {
		wsaaTerm.set(datcon3rec.freqFactor);
	}
	/*EXIT*/
}
protected void checkT6695600New()
	{
		/*GO*/
		int years = 0;
		years = wsaaNumYears.toInt();
			wsaaNewAmtToPay.set(regpIO.getPymt());
				while ( !(isGT(wsaaSub, 12))) {
						if (isLT(years, t6695rec.tupyr[wsaaSub.toInt()].toInt())
								|| isEQ(years, t6695rec.tupyr[wsaaSub.toInt()].toInt())) {
								compute(wsaaAmount, 2).set((div((mult(t6695rec.pctinc[wsaaSub.toInt()], wsaaNewAmtToPay)), 100)));
								wsaaNewAmtToPay.add(wsaaAmount);
								zrdecplrec.amountIn.set(wsaaNewAmtToPay);
								a000CallRounding();
								wsaaNewAmtToPay.set(zrdecplrec.amountOut);
								wsaaSub.set(13);
								return;
						}
						else{
							wsaaSub.add(1);
							if(regpayFlag){
								if(isEQ(wsaaSub,13)){
										handleContItem();
								}
							}
						}
				}
				
		/*EXIT*/
	}
protected void handleContItem(){
	if(t6695rec.contitem.trim().length() > 0 && t6695rec.contitem != null){
		wsaaT6695Key=t6695rec.contitem;
		if (isEQ(regpsubrec.subStatuz, varcom.oK)) {
			readT6695200();
		}
		if (isEQ(regpsubrec.subStatuz, varcom.oK)) {
			wsaaSub.set(1);
		}
	}
}
protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(regpsubrec.subCompany);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(regpIO.getCurrcd());
		zrdecplrec.batctrcde.set(regpsubrec.subBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			regpsubrec.subStatuz.set(zrdecplrec.statuz);
		}
		/*A000-EXIT*/
	}

protected void  getSuminsforPymt(){
	
	Covrpf covrpf = covrpfDAO.getCovrpfData(regpIO.getChdrcoy().toString(), regpIO.getChdrnum().toString(), regpIO.getLife().toString(), regpIO.getCoverage().toString(), regpIO.getCrtable().toString());
	if(covrpf != null){
			compute(wsaaKeepSumins,2).set(div(mult(covrpf.getSumins(),regpIO.getPrcnt()),100));
			ChangeAcctoFreq();
			wsaaNewAmtToPay.set(wsaaKeepSumins);
	}
}


protected void ChangeAcctoFreq(){
	
	if(isEQ(regpIO.regpayfreq,"12")){
		compute(wsaaKeepSumins,2).set(div(wsaaKeepSumins,12));
	}
	if(isEQ(regpIO.regpayfreq,"02")){
		compute(wsaaKeepSumins,2).set(div(wsaaKeepSumins,2));
	}
	if(isEQ(regpIO.regpayfreq,"04")){
		compute(wsaaKeepSumins,2).set(div(wsaaKeepSumins,4));
	}
	if(isEQ(regpIO.regpayfreq,"26")){
		compute(wsaaKeepSumins,2).set(div(wsaaKeepSumins,26));
	}
}


}
