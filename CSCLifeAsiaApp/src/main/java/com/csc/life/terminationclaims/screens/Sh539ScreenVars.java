package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SH539
 * @version 1.0 generated on 30/08/09 07:03
 * @author Quipoz
 */
public class Sh539ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(569);
	public FixedLengthStringData dataFields = new FixedLengthStringData(265).isAPartOf(dataArea, 0);
	public FixedLengthStringData trandsc = DD.trandsc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bilfrqdesc = DD.bilfrqdesc.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,60);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,62);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,88);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,91);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,94);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData flag = DD.flag.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,133);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,180);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,235);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,243);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,244);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,254);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,262);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 265);
	public FixedLengthStringData trandscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bilfrqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData flagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 341);
	public FixedLengthStringData[] trandscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bilfrqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] flagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(273);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(95).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData etidays = DD.etidays.copyToZonedDecimal().isAPartOf(subfileFields,2);
	public ZonedDecimalData etiyear = DD.etiyear.copyToZonedDecimal().isAPartOf(subfileFields,5);
	public ZonedDecimalData hetinsi = DD.hetinsi.copyToZonedDecimal().isAPartOf(subfileFields,7);
	public ZonedDecimalData hetiosi = DD.hetiosi.copyToZonedDecimal().isAPartOf(subfileFields,24);
	public ZonedDecimalData hrefval = DD.hrefval.copyToZonedDecimal().isAPartOf(subfileFields,41);
	public ZonedDecimalData hsurval = DD.hsurval.copyToZonedDecimal().isAPartOf(subfileFields,58);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,75);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(subfileFields,77);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,85);
	public ZonedDecimalData zrdate = DD.zrdate.copyToZonedDecimal().isAPartOf(subfileFields,87);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(44).isAPartOf(subfileArea, 95);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData etidaysErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData etiyearErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hetinsiErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hetiosiErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hrefvalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hsurvalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData zrdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea, 139);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] etidaysOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] etiyearOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hetinsiOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hetiosiOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hrefvalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hsurvalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] zrdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 271);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData zrdateDisp = new FixedLengthStringData(10);

	public LongData Sh539screensflWritten = new LongData(0);
	public LongData Sh539screenctlWritten = new LongData(0);
	public LongData Sh539screenWritten = new LongData(0);
	public LongData Sh539protectWritten = new LongData(0);
	public GeneralTable sh539screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sh539screensfl;
	}

	public Sh539ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(flagOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {riskCessDate, life, coverage, rider, hetiosi, hsurval, hetinsi, hrefval, etiyear, etidays, zrdate};
		screenSflOutFields = new BaseData[][] {rcesdteOut, lifeOut, coverageOut, riderOut, hetiosiOut, hsurvalOut, hetinsiOut, hrefvalOut, etiyearOut, etidaysOut, zrdateOut};
		screenSflErrFields = new BaseData[] {rcesdteErr, lifeErr, coverageErr, riderErr, hetiosiErr, hsurvalErr, hetinsiErr, hrefvalErr, etiyearErr, etidaysErr, zrdateErr};
		screenSflDateFields = new BaseData[] {riskCessDate, zrdate};
		screenSflDateErrFields = new BaseData[] {rcesdteErr, zrdateErr};
		screenSflDateDispFields = new BaseData[] {riskCessDateDisp, zrdateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, ptdate, btdate, billfreq, bilfrqdesc, crrcd, mop, flag, trandsc};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, ptdateOut, btdateOut, billfreqOut, bilfrqdescOut, crrcdOut, mopOut, flagOut, trandscOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, ptdateErr, btdateErr, billfreqErr, bilfrqdescErr, crrcdErr, mopErr, flagErr, trandscErr};
		screenDateFields = new BaseData[] {ptdate, btdate, crrcd};
		screenDateErrFields = new BaseData[] {ptdateErr, btdateErr, crrcdErr};
		screenDateDispFields = new BaseData[] {ptdateDisp, btdateDisp, crrcdDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sh539screen.class;
		screenSflRecord = Sh539screensfl.class;
		screenCtlRecord = Sh539screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sh539protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sh539screenctl.lrec.pageSubfile);
	}
}
