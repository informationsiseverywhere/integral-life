package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5019screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	//public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {19, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5019ScreenVars sv = (S5019ScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.S5019screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5019ScreenVars screenVars = (S5019ScreenVars)pv;
		ScreenRecord.setClassStringFormatting(pv);
	}

/**
 * Clear all the variables in S5019screen
 */
	public static void clear(VarModel pv) {
		S5019ScreenVars screenVars = (S5019ScreenVars) pv;
		ScreenRecord.clear(pv);
	}
}
