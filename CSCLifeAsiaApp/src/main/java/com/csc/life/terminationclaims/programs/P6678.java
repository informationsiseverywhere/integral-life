/*
 * File: P6678.java
 * Date: 30 August 2009 0:52:05
 * Author: Quipoz Limited
 * 
 * Class transformed from P6678.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.contractservicing.dataaccess.FupeTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FlupaltTableDAM;
import com.csc.life.newbusiness.dataaccess.FlupenqTableDAM;
import com.csc.life.newbusiness.dataaccess.HxclTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.terminationclaims.dataaccess.FluprgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.screens.S6678ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
*
*  P6678 - Regular Payments Follow Ups.
*  ------------------------------------
*
*  Overview.
*  ---------
*
*  This program will provide a facility to register Follow  Ups
*  against a claim on a Regular Benefit component.
*
*  Any  number  of  Follow  Ups  are  allowed  against a claim.
*  Duplicates are allowed although these are unlikely.
*
*  Once a Follow up is added to the database the user  may  not
*  remove  it.  Only  modifications are allowed. A Follow Up is
*  considered complete when  its  status  matches  one  of  the
*  'Complete'  status  codes  on  T5661. If the user blanks out
*  all the fields on a subfile record that has been added  from
*  the  database  then  the program will re-set them with their
*  original values from the FLUPRGP file.
*
*  Initialise.
*  -----------
*
*  Perform a RETRV  on  REGP  to  obtain  the  Regular  Payment
*  details.
*
*  Clear  the  subfile  and  load  any  existing  records  from
*  FLUPRGP up to a maximum of one page. The key to use for  the
*  BEGNH is as follows:
*
*       . Company           -    REGP-CHDRCOY
*       . Contract Number   -    REGP-CHDRNUM
*       . Claim Number      -    REGP-RGPYNUM, (this will
*                                have to be unpacked),
*       . Follow Up Number  -    zero.
*
*  For  each  record added to the subfile use a hidden field to
*  store the Follow Up Sequence Number. Also set a  flag  in  a
*  hidden  field  to  indicate that this record was loaded from
*  the database.
*
*  Store the key of the first FLUPRGP record written.
*
*  If the first page is not full add the  necessary  number  of
*  blank  records  to  fill  up  the page. Set the first 'Next'
*  Follow Up Sequence number in Working Storage so that it  may
*  be used when adding the new FLUPRGP records.
*
*  Set the Subfile More indicator to 'Y'.
*
*  Obtain the current date from DATCON1.
*
*  Set a flag to indicate that this is the first page.
*
*  Display and Validation. (2000 Section).
*  ---------------------------------------
*
*  If  in  Enquiry  mode, (WSSP-FLAG = 'I'), protect the entire
*  screen prior to output.
*
*  Converse the screen with the I/O module.
*
*  If 'KILL' has been requested or if in enquiry mode skip  all
*  the validation.
*
*  If  Previous  Page, ('ROLD'), has been requested and this is
*  the first page give an error message.
*
*  Otherwise if 'ROLD' or  'ROLU'  have  been  requested  store
*  this fact for later use.
*
*  Perform  a  loop  with  Subfile Read Next Changed to pick up
*  all of the changed records. Validate them as follows:
*
*       Invalid deletion of records: If the subfile record  was
*       added  from  the  database  and  all  fields  have been
*       blanked out then read the corresponding FLUPRGP  record
*       and  move all the fields back to the screen and provide
*       an error message on the Follow Up Type Field.
*
*       Follow Up Code: The Follow Up Code is mandatory.  Check
*       that  it  is  non-blank.  If it is blank and the record
*       was  added  from  the  database  then  give  an   error
*       message.  If  it  is blank and the record was not added
*       from the database then check that all other fields  are
*       blank.  If all the other fields are blank then the line
*       can be ignored. If, however, any of  the  other  fields
*       are  non  blank  then  give an error message indicating
*       that the field is mandatory. Read T5661 with  the  code
*       in order to obtain the associated status codes.
*
*       Status:  The  Status  field  is optional. If it is left
*       blank then use the default initial status  from  T5661.
*       If  it  is  entered  it  will  be  validated by the I/O
*       module as it is a window field.
*
*       Reminder Date: This field is optional. If  it  is  zero
*       or max date set it to today's date.
*
*       Remarks:  The  Remarks  field  is optional. If it blank
*       then set it to the description from T5661.
*
*       Set the hidden Update Required field to either 'M'  for
*       Modify  if  the  record was originally written from the
*       database, or 'C' for Create if it is a new  record  and
*       rewrite the subfile record.
*
*  Updating.
*  ---------
*
*  The  subfile  is  re-read  sequentially and each record that
*  has been marked for update is processed.
*
*       For  Modification  read   the   corresponding   FLUPRGP
*       record, move the fields in and rewrite it.
*
*       For  Creation  set up the fields on the record from the
*       screen and REGP.  Calculate  the  sequence  number  for
*       each  new record from the first 'Next' number stored in
*       Working Storage when the screen  was  first  assembled.
*       Increment  this number as the records are added so that
*       it way be used for each new FLUPRGP record and so  that
*       it  may  be  used  if  another  screen of data is added
*       subsequently. Add the record.
*
*  Where Next.
*  -----------
*
*  If the next page has been  selected,  (Rollup),  proceed  as
*  follows:
*
*       Set  off  the  flag  indicating  that this is the first
*       page.
*
*       Clear the subfile.
*
*       Load the next page writing out blank lines  when  there
*       are no more Follow Up records to display.
*
*  If  the previous page has been selected, (Rolldown), proceed
*  as follows:
*
*       Clear the subfile.
*
*       Perform a BEGNH on the first  Follow  Up  record  whose
*       details  were  displayed  using the key stored when the
*       subfile was originally loaded.
*
*       Using NEXTP load the screen in reverse.
*
*  If either Rollup or Rolldown have been selected set off  the
*  indicator  showing  which  of  them  was  selected  and  set
*  WSAA-PROG to SCRN-SCRNAME to force  the  program  to  resuem
*  fom  the  2000  section,  otherwise if neither of these keys
*  has been selected add 1 to the program pointer and exit.
*
*  Notes.
*  ------
*
*  Tables Used.
*  ------------
*
*  . T5660 - Follow Up Status Codes
*            Key: Follow Up Status Codes
*
*  . T5661 - Follow Up Types
*            Key: Follow up Type Code
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*  Add Validation to  search for a follow up letter type for
*  printing of either standard follow up letter or reminder.
*
*****************************************************************
* </pre>
*/
public class P6678 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6678");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaFirstPage = "";
	private String wsaaNoMoreFlups = "";
	private PackedDecimalData wsaaFupno = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaRrn = new PackedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaRollFupno = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSflsiz = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private String wsaaPageUp = "";
	private String wsaaPageDown = "";
	private ZonedDecimalData wsaaLine1Fupno = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

		/* WSBB-STACK-ARRAY */
	private FixedLengthStringData wsbbFlupkeyArray = new FixedLengthStringData(24);
	private PackedDecimalData[] wsbbFlupkey = PDArrayPartOfStructure(12, 2, 0, wsbbFlupkeyArray, 0);

	private FixedLengthStringData wsbbChangeArray = new FixedLengthStringData(12);
	private FixedLengthStringData[] wsbbChange = FLSArrayPartOfStructure(12, 1, wsbbChangeArray, 0);

	private FixedLengthStringData wsbbClamnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbFiller = new FixedLengthStringData(3).isAPartOf(wsbbClamnum, 0).init("000");
	private FixedLengthStringData wsccClamnum = new FixedLengthStringData(5).isAPartOf(wsbbClamnum, 3);

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

		/* first subfile record.*/
	private FixedLengthStringData wsaaFirstFlupKey = new FixedLengthStringData(72);
	private FixedLengthStringData wsaaFlupKeyChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaFirstFlupKey, 0);
	private FixedLengthStringData wsaaFlupKeyChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstFlupKey, 1);
	private FixedLengthStringData wsaaFlupKeyClamnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstFlupKey, 9);
	private PackedDecimalData wsaaFlupKeyFupno = new PackedDecimalData(2, 0).isAPartOf(wsaaFirstFlupKey, 17);
	private FixedLengthStringData filler = new FixedLengthStringData(53).isAPartOf(wsaaFirstFlupKey, 19, FILLER).init(SPACES);

	private FixedLengthStringData wsaaCheckBox = new FixedLengthStringData(1);
	private Validator checkBox = new Validator(wsaaCheckBox, "Y");
	private PackedDecimalData ix = new PackedDecimalData(2, 0);
	private ZonedDecimalData iy = new ZonedDecimalData(2, 0);
	private PackedDecimalData wsaaDocseq = new PackedDecimalData(2, 0);
		/* TABLES */
	private static final String t5661 = "T5661";
	private static final String t5660 = "T5660";
	private static final String flupaltrec = "FLUPALTREC";
	private static final String flupenqrec = "FLUPENQREC";
	private static final String fluprgprec = "FLUPRGPREC";
	private static final String fuperec = "FUPEREC";
	private static final String hxclrec = "HXCLREC";
	private static final String itemrec = "ITEMREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FlupaltTableDAM flupaltIO = new FlupaltTableDAM();
	private FlupenqTableDAM flupenqIO = new FlupenqTableDAM();
	private FluprgpTableDAM fluprgpIO = new FluprgpTableDAM();
	private FupeTableDAM fupeIO = new FupeTableDAM();
	private HxclTableDAM hxclIO = new HxclTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private T5661rec t5661rec = new T5661rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Optswchrec optswchrec = new Optswchrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6678ScreenVars sv = ScreenProgram.getScreenVars( S6678ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		continue2620, 
		updateErrorIndicators2670, 
		switch4170, 
		addSubfileRecord7290, 
		updateSubfile4670
	}

	public P6678() {
		super();
		screenVars = sv;
		new ScreenModel("S6678", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		sv.crtdate.set(varcom.vrcmMaxDate);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6678", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Retrieve Regular Payment Details*/
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/*    Release hold on the regular payment details*/
		regpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/*    Breakout the claim number i.e from s(9) to X*/
		wsccClamnum.set(regpIO.getRgpynum());
		/*    Perform section to load first page of subfile*/
		fluprgpIO.setParams(SPACES);
		fluprgpIO.setChdrcoy(regpIO.getChdrcoy());
		fluprgpIO.setChdrnum(regpIO.getChdrnum());
		fluprgpIO.setClamnum(wsbbClamnum);
		fluprgpIO.setFupno(ZERO);
		fluprgpIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		fluprgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluprgpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","CLAMNUM");

		SmartFileCode.execute(appVars, fluprgpIO);
		if ((isNE(fluprgpIO.getStatuz(), varcom.oK))
		&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
		/* Store the first subfile record key for later use if a*/
		/* ROLD is pressed.*/
		wsaaFirstFlupKey.set(fluprgpIO.getRecKeyData());
		wsaaX.set(ZERO);
		/*    PERFORM 1100-LOAD-SUBFILE   06 TIMES.                        */
		/*  PERFORM 1100-LOAD-SUBFILE   03 TIMES.                        */
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaSflsiz);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			loadSubfile1100();
		}
		wsaaNoMoreFlups = "N";
		scrnparams.subfileMore.set("Y");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaFirstPage = "Y";
		initOptswch1200();
	}

	/**
	* <pre>
	*     Load subfile
	* </pre>
	*/
protected void loadSubfile1100()
	{
		loadSubfile1110();
		addSubfileRecord1120();
	}

protected void loadSubfile1110()
	{
		wsaaX.add(1);
		/*    MOVE SPACES                 TO S6678-FUPCDE-OUT(PR).         */
		sv.fupcdesOut[varcom.pr.toInt()].set(SPACES);
		if ((isNE(fluprgpIO.getChdrcoy(), regpIO.getChdrcoy()))
		|| (isNE(fluprgpIO.getChdrnum(), regpIO.getChdrnum()))
		|| (isNE(fluprgpIO.getClamnum(), wsbbClamnum))
		|| (isEQ(fluprgpIO.getStatuz(), varcom.endp))) {
			wsaaNoMoreFlups = "Y";
			sv.subfileFields.set(SPACES);
			sv.fupremdt.set(99999999);
			wsaaFupno.add(1);
			sv.hseqno.set(wsaaFupno);
			sv.zitem.set(SPACES);
			sv.sfflg.set(SPACES);
			sv.indic.set(SPACES);
			sv.updteflag.set(SPACES);
			sv.crtuser.set(wsspcomn.userid);
			sv.language.set(wsspcomn.language);
			sv.crtdate.set(varcom.vrcmMaxDate);
			sv.fuprcvd.set(99999999);
			sv.exprdate.set(99999999);
			sv.fuptype.set("C");
			sv.fuptypOut[varcom.nd.toInt()].set("Y");
			return ;
		}
		/*    MOVE FLUPRGP-FUPCODE        TO S6678-FUPCODE.                */
		sv.fupcdes.set(fluprgpIO.getFupcode());
		/*    MOVE 'Y'                    TO S6678-FUPCDE-OUT(PR)          */
		sv.fupcdesOut[varcom.pr.toInt()].set("Y");
		sv.fupremdt.set(fluprgpIO.getFupremdt());
		sv.hseqno.set(fluprgpIO.getFupno());
		wsaaFupno.set(fluprgpIO.getFupno());
		sv.fupremk.set(fluprgpIO.getFupremk());
		sv.fupstat.set(fluprgpIO.getFupstat());
		sv.updteflag.set("N");
		sv.zitem.set(fluprgpIO.getFupcode());
		sv.fuptype.set(fluprgpIO.getFuptype());
		sv.fuprcvd.set(fluprgpIO.getFuprcvd());
		if (isEQ(sv.fuprcvd, ZERO)) {
			sv.fuprcvd.set(varcom.vrcmMaxDate);
		}
		sv.exprdate.set(fluprgpIO.getExprdate());
		if (isEQ(sv.exprdate, ZERO)) {
			sv.exprdate.set(varcom.vrcmMaxDate);
		}
		readFupe1600();
		if (isEQ(fupeIO.getStatuz(), varcom.oK)) {
			sv.sfflg.set("+");
			sv.indic.set("+");
		}
		else {
			sv.sfflg.set(SPACES);
			sv.indic.set(SPACES);
		}
		checkHxcl1500();
		sv.crtuser.set(fluprgpIO.getCrtuser());
		sv.language.set(wsspcomn.language);
		if (isNE(fluprgpIO.getCrtdate(), NUMERIC)) {
			sv.crtdate.set(varcom.vrcmMaxDate);
		}
		else {
			sv.crtdate.set(fluprgpIO.getCrtdate());
		}
		fluprgpIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, fluprgpIO);
		if ((isNE(fluprgpIO.getStatuz(), varcom.oK))
		&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
	}

protected void addSubfileRecord1120()
	{
		sv.fuprmkOut[varcom.pr.toInt()].set("Y");
		if (isEQ(wsspcomn.flag, "I")) {
			sv.fupcdesOut[varcom.pr.toInt()].set("Y");
			sv.fupstsOut[varcom.pr.toInt()].set("Y");
			sv.fupdtOut[varcom.pr.toInt()].set("Y");
			sv.fuprcvdOut[varcom.pr.toInt()].set("Y");
			sv.exprdateOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S6678", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void initOptswch1200()
	{
		start1210();
	}

protected void start1210()
	{
		optswchrec.optsFunction.set(varcom.init);
		optswchrec.optsLanguage.set(wsspcomn.language);
		optswchrec.optsCompany.set(wsspcomn.company);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(datcon1rec.intDate);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			syserrrec.params.set(optswchrec.rec);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			fatalError600();
		}
	}

protected void checkHxcl1500()
	{
		hxcl1510();
	}

protected void hxcl1510()
	{
		hxclIO.setParams(SPACES);
		/*  MOVE FLUPRGP-CHDRCOY       TO HXCL-CHDRCOY.         <LA5056>*/
		/*  MOVE FLUPRGP-CHDRNUM       TO HXCL-CHDRNUM.         <LA5056>*/
		/*  MOVE FLUPRGP-FUPNO         TO HXCL-FUPNO.           <LA5056>*/
		hxclIO.setChdrcoy(regpIO.getChdrcoy());
		hxclIO.setChdrnum(regpIO.getChdrnum());
		hxclIO.setFupno(fupeIO.getFupno());
		hxclIO.setHxclseqno(ZERO);
		hxclIO.setFormat(hxclrec);
		hxclIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		hxclIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hxclIO.setFitKeysSearch("CHDRCOY","CHDRNUM","FUPNO");
		
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(), varcom.oK)
		&& isNE(hxclIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		if (isEQ(hxclIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/*  IF HXCL-CHDRCOY        NOT = FLUPALT-CHDRCOY        <LA5056>*/
		/*   OR HXCL-CHDRNUM       NOT = FLUPALT-CHDRNUM        <LA5056>*/
		/*   OR HXCL-FUPNO         NOT = FLUPALT-FUPNO          <LA5056>*/
		/*      GO TO 1590-EXIT.                                <LA5056>*/
		if (isNE(hxclIO.getChdrcoy(), regpIO.getChdrcoy())
		|| isNE(hxclIO.getChdrnum(), regpIO.getChdrnum())
		|| isNE(hxclIO.getFupno(), fupeIO.getFupno())) {
			return ;
		}
		sv.sfflg.set("+");
		sv.indic.set("+");
	}

protected void readFupe1600()
	{
		readFupe1610();
	}

protected void readFupe1610()
	{
		fupeIO.setParams(SPACES);
		fupeIO.setChdrcoy(fluprgpIO.getChdrcoy());
		fupeIO.setChdrnum(fluprgpIO.getChdrnum());
		fupeIO.setFupno(fluprgpIO.getFupno());
		fupeIO.setClamnum(fluprgpIO.getClamnum());
		fupeIO.setTranno(fluprgpIO.getTranno());
		fupeIO.setDocseq(ZERO);
		fupeIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		fupeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fupeIO.setFitKeysSearch("CHDRCOY","CHDRNUM","FUPNO","TRANNO");
		fupeIO.setFormat(fuperec);
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(), varcom.oK)
		&& isNE(fupeIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(fupeIO.getStatuz());
			syserrrec.params.set(fupeIO.getParams());
			fatalError600();
		}
		if (isNE(fupeIO.getChdrcoy(), fluprgpIO.getChdrcoy())
		|| isNE(fupeIO.getChdrnum(), fluprgpIO.getChdrnum())
		|| isNE(fupeIO.getFupno(), fluprgpIO.getFupno())
		|| isNE(fupeIO.getClamnum(), fluprgpIO.getClamnum())
		|| isNE(fupeIO.getTranno(), fluprgpIO.getTranno())) {
			fupeIO.setStatuz(varcom.endp);
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    IF WSSP-FLAG                 = 'I'                   <V72L11>*/
		/*       MOVE PROT                TO SCRN-FUNCTION         <V72L11>*/
		/*    END-IF.                                              <V72L11>*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
			validateSubfile2060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S6678IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                S6678-DATA-AREA                  */
		/*                                S6678-SUBFILE-AREA.              */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if ((isNE(scrnparams.statuz, varcom.kill))
		&& (isNE(scrnparams.statuz, varcom.calc))
		&& (isNE(scrnparams.statuz, varcom.rold))
		&& (isNE(scrnparams.statuz, varcom.rolu))
		&& (isNE(scrnparams.statuz, varcom.oK))) {
			scrnparams.errorCode.set(errorsInner.curs);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if ((isEQ(scrnparams.statuz, varcom.rold))
		&& (isEQ(wsaaFirstPage, "Y")
		|| isEQ(wsaaX, wsaaSflsiz))) {
			/*     (WSAA-FIRST-PAGE         = 'Y')                          */
			/*      MOVE F498               TO S6678-UPDTEFLAG               */
			scrnparams.errorCode.set(errorsInner.f498);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rold)) {
			wsaaPageUp = "Y";
		}
		else {
			wsaaPageUp = "N";
		}
		/* Page down not allow if no more record when enquiry           */
		if ((isEQ(wsspcomn.flag, "I"))
		&& (isEQ(scrnparams.statuz, varcom.rolu))
		&& (isEQ(wsaaNoMoreFlups, "Y"))) {
			scrnparams.errorCode.set(errorsInner.f499);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaPageDown = "Y";
		}
		else {
			wsaaPageDown = "N";
		}
	}

	/**
	* <pre>
	*    Validate fields
	* </pre>
	*/
protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* Update any error message already log during validate          */
		if (isNE(sv.errorIndicators, SPACES)) {
			scrnparams.function.set(varcom.supd);
			processScreen("S6678", sv);
			if ((isNE(scrnparams.statuz, varcom.oK))
			&& (isNE(scrnparams.statuz, varcom.endp))) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
	}

	/**
	* <pre>
	** Still validate it is not blank line before option switch       
	**   IF WSSP-FLAG                 = 'I'                           
	**      GO TO 2090-EXIT                                           
	**   END-IF.                                                      
	* </pre>
	*/
protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6678", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case continue2620: 
					continue2620();
				case updateErrorIndicators2670: 
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		/* Do not allow a follow up record to be deleted, if this*/
		/* record previously existed on the follow up file.*/
		/*    IF (S6678-FUPCODE            = SPACES)  AND                  */
		/* Move this validation down, as it is not require for enquiry    */
		/* IF (S6678-FUPCDES            = SPACES)  AND          <LA4592>*/
		/*    (S6678-FUPREMK            = SPACES)  AND                  */
		/*    (S6678-FUPSTAT            = SPACE)   AND                  */
		/*    (S6678-UPDTEFLAG          = 'N')     AND                  */
		/*    (S6678-FUPREMDT           = ZERO     OR                   */
		/*                              = 99999999 OR                   */
		/*                              = SPACES)                       */
		/*    PERFORM 2700-RE-READ-FLUP                                 */
		/*       MOVE H961                TO S6678-FUPCDE-ERR              */
		/*    MOVE H961                TO S6678-FUPCDES-ERR     <LA4592>*/
		/*    GO TO 2670-UPDATE-ERROR-INDICATORS                        */
		/* END-IF.                                                      */
		/* Invalid action for in blank line                                */
		if ((isEQ(sv.fupcdes, SPACES))
		&& (isEQ(sv.fupremk, SPACES))
		&& (isEQ(sv.fupstat, SPACES))
		&& (isEQ(sv.fupremdt, ZERO)
		|| isEQ(sv.fupremdt, 99999999)
		|| isEQ(sv.fupremdt, SPACES))
		&& (isEQ(sv.updteflag, " ")
		|| isEQ(sv.updteflag, "C"))
		&& isNE(sv.sfflg, SPACES)) {
			sv.sfflgErr.set(errorsInner.e005);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Only validate it is not blank line before option switch        */
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Do not allow a follow up record to be deleted, if this          */
		/* record previously existed on the follow up file.                */
		if ((isEQ(sv.fupcdes, SPACES))
		&& (isEQ(sv.fupremk, SPACES))
		&& (isEQ(sv.fupstat, SPACES))
		&& (isEQ(sv.updteflag, "N"))
		&& (isEQ(sv.fupremdt, ZERO)
		|| isEQ(sv.fupremdt, 99999999)
		|| isEQ(sv.fupremdt, SPACES))) {
			reReadFlup2700();
			sv.fupcdesErr.set(errorsInner.h961);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Ignore this subfile record if all the field have been*/
		/* blanked out, i.e the record has been created during this*/
		/* work session, but this record is no longer required.*/
		/*    IF (S6678-FUPCODE            = SPACES) AND                   */
		if ((isEQ(sv.fupcdes, SPACES))
		&& (isEQ(sv.fupremk, SPACES))
		&& (isEQ(sv.fupstat, SPACES))
		&& (isEQ(sv.fupremdt, ZERO)
		|| isEQ(sv.fupremdt, 99999999)
		|| isEQ(sv.fupremdt, SPACES))
		&& (isEQ(sv.updteflag, " ")
		|| isEQ(sv.updteflag, "C"))) {
			sv.updteflag.set("X");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/*    IF S6678-FUPCODE             = SPACES                        */
		if (isEQ(sv.fupcdes, SPACES)) {
			if (isEQ(sv.updteflag, "N")) {
				/*          MOVE H963             TO S6678-FUPCDE-ERR              */
				sv.fupcdesErr.set(errorsInner.h963);
				goTo(GotoLabel.updateErrorIndicators2670);
			}
			else {
				if ((isEQ(sv.fupremk, SPACES))
				&& (isEQ(sv.fupstat, SPACES))
				&& (isEQ(sv.fupremdt, ZERO)
				|| isEQ(sv.fupremdt, 99999999)
				|| isEQ(sv.fupremdt, SPACES))) {
					goTo(GotoLabel.continue2620);
				}
				else {
					/*              MOVE H963         TO S6678-FUPCDE-ERR              */
					sv.fupcdesErr.set(errorsInner.h963);
					goTo(GotoLabel.updateErrorIndicators2670);
				}
			}
		}
		/* Check follow up code.*/
		/* This is a mandatory field and it must be valid on T5661*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5661);
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itemIO.setItemitem(wsaaT5661Key);
		/*    MOVE S6678-FUPCODE          TO ITEM-ITEMITEM.                */
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			/*     MOVE U000                TO S6678-FUPCDE-ERR              */
			/*       MOVE E557                TO S6678-FUPCDE-ERR      <V72L11>*/
			sv.fupcdesErr.set(errorsInner.e557);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		else {
			t5661rec.t5661Rec.set(itemIO.getGenarea());
		}
		if (isEQ(sv.fupstat, SPACES)) {
			sv.fupstat.set(t5661rec.fupstat);
		}
		/* Check presence of letter type in case of printing of either     */
		/* standard follow up letter or reminder.                          */
		if (isEQ(sv.fupstat, "L")) {
			if (isEQ(t5661rec.zlettype, SPACES)) {
				/*             MOVE HL50          TO S6678-FUPCDE-ERR      <V72L11>*/
				sv.fupcdesErr.set(errorsInner.hl50);
			}
		}
		if (isEQ(sv.fupstat, "M")) {
			if (isEQ(t5661rec.zlettype, SPACES)) {
				/*             MOVE HL50          TO S6678-FUPCDE-ERR      <V72L11>*/
				sv.fupcdesErr.set(errorsInner.hl50);
			}
		}
		/* Validate the follow up statii on T5660*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5660);
		itemIO.setItemitem(sv.fupstat);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.fupstsErr.set(errorsInner.e558);
		}
		if (isNE(sv.zitem, SPACES)
		&& isNE(sv.fupcdes, SPACES)
		&& isNE(sv.fupcdes, sv.zitem)) {
			sv.fupcdesErr.set(errorsInner.w431);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* If the remark is blank then default to desc from T5661.*/
		/* As remark is always protect, so it should same as code select   */
		if (isEQ(sv.updteflag, "N")) {
			if (isNE(sv.fupremk, SPACES)) {
				goTo(GotoLabel.continue2620);
			}
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5661);
		descIO.setLanguage(wsspcomn.language);
		/*    MOVE S6678-FUPCODE          TO DESC-DESCITEM.                */
		/*    MOVE S6678-FUPCDES          TO DESC-DESCITEM.        <LA4314>*/
		descIO.setDescitem(wsaaT5661Key);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/*     MOVE U000                TO S6678-FUPRMK-ERR              */
			sv.fuprmkErr.set(errorsInner.e557);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		else {
			sv.fupremk.set(descIO.getLongdesc());
		}
	}

protected void continue2620()
	{
		/* If the date is blank the use 'todays' date.*/
		if (isEQ(sv.fupremdt, ZERO)
		|| isEQ(sv.fupremdt, SPACES)
		|| isEQ(sv.fupremdt, 99999999)) {
			if (isEQ(sv.fupstat, "L")) {
				setReminderDate6000();
			}
			else {
				sv.fupremdt.set(datcon1rec.intDate);
			}
		}
		/* Check Received Date and Expiry Date                             */
		if (isNE(sv.fuprcvd, varcom.vrcmMaxDate)
		&& isGT(sv.fuprcvd, datcon1rec.intDate)) {
			sv.fuprcvdErr.set(errorsInner.f073);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isNE(sv.exprdate, varcom.vrcmMaxDate)
		&& isLT(sv.exprdate, datcon1rec.intDate)) {
			sv.exprdateErr.set(errorsInner.g914);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.fuprcvd, varcom.vrcmMaxDate)) {
			for (ix.set(1); !(isGT(ix, 10)
			|| isEQ(t5661rec.fuposs[ix.toInt()], SPACES)); ix.add(1)){
				if (isEQ(t5661rec.fuposs[ix.toInt()], sv.fupstat)) {
					sv.fuprcvdErr.set(errorsInner.e186);
				}
			}
		}
		/* Check option switching for follow-up extended text screen       */
		if (isNE(sv.sfflgErr, SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Cannot switch to Follow-up Extended Text if no follow-up code   */
		if (isNE(sv.sfflg, SPACES)
		&& isNE(sv.sfflg, "+")
		&& isEQ(sv.fupcdes, SPACES)) {
			sv.sfflgErr.set(errorsInner.rf00);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.zitem, SPACES)) {
			if (isEQ(sv.fupcdes, SPACES)) {
				sv.sfflg.set(SPACES);
				sv.indic.set(SPACES);
			}
			else {
				if (isNE(t5661rec.messages, SPACES)) {
					sv.indic.set("+");
				}
			}
		}
		if (isEQ(sv.sfflg, SPACES)
		&& isNE(sv.indic, SPACES)) {
			sv.sfflg.set(sv.indic);
		}
		/* The subfile record has been changed so update the hidden*/
		/* flag.*/
		if (isEQ(sv.updteflag, "N")) {
			sv.updteflag.set("M");
		}
		if (isEQ(sv.updteflag, SPACES)) {
			sv.crtdate.set(datcon1rec.intDate);
			sv.fuptype.set("C");
			sv.fuptypOut[varcom.nd.toInt()].set("N");
			sv.updteflag.set("C");
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isEQ(sv.sfflg, SPACES)
		|| isEQ(sv.sfflg, "+")) {
			/*NEXT_SENTENCE*/
		}
		else {
			optswchrec.optsInd[1].set(SPACES);
			optswchrec.optsSelCode.set(SPACES);
			optswchrec.optsFunction.set("CHCK");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsItemCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelOptno.set(sv.sfflg);
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz, varcom.oK)) {
				sv.sfflgErr.set(optswchrec.optsStatuz);
			}
		}
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6678", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6678", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	* </pre>
	*/
protected void reReadFlup2700()
	{
		go2710();
	}

protected void go2710()
	{
		fluprgpIO.setParams(SPACES);
		fluprgpIO.setChdrcoy(regpIO.getChdrcoy());
		fluprgpIO.setChdrnum(regpIO.getChdrnum());
		fluprgpIO.setClamnum(wsbbClamnum);
		fluprgpIO.setFupno(sv.hseqno);
		/*     MOVE READH                 TO FLUPRGP-FUNCTION.             */
		fluprgpIO.setFormat(fluprgprec);
		fluprgpIO.setFunction(varcom.readr);
		flupFile3200();
		/*     MOVE FLUPRGP-FUPCODE       TO S6678-FUPCODE.                */
		sv.fupcdes.set(fluprgpIO.getFupcode());
		sv.fupstat.set(fluprgpIO.getFupstat());
		sv.fupremdt.set(fluprgpIO.getFupremdt());
		sv.fupremk.set(fluprgpIO.getFupremk());
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if ((isEQ(scrnparams.statuz, varcom.kill))
		|| (isEQ(wsspcomn.flag, "I"))) {
			return ;
		}
		for (wsaaRrn.set(1); !(isGT(wsaaRrn, 100)
		|| isEQ(sv.fupcdes, SPACES)); wsaaRrn.add(1)){
			updateFollowUp3100();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DO THE FOLLOW UP FILE UPDATING
	* </pre>
	*/
protected void updateFollowUp3100()
	{
		updateFollowUp3110();
	}

protected void updateFollowUp3110()
	{
		/* Get the subfile record using the relative record number.*/
		scrnparams.subfileRrn.set(wsaaRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S6678", sv);
		if (isEQ(scrnparams.statuz, varcom.endp)
		|| isEQ(scrnparams.statuz, varcom.mrnf)) {
			return ;
		}
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* The subfile record has not been changed.*/
		if (isEQ(sv.updteflag, SPACES)
		|| isEQ(sv.updteflag, "N")) {
			return ;
		}
		/* Set up key.*/
		fluprgpIO.setParams(SPACES);
		fluprgpIO.setChdrcoy(regpIO.getChdrcoy());
		fluprgpIO.setChdrnum(regpIO.getChdrnum());
		fluprgpIO.setClamnum(wsbbClamnum);
		fluprgpIO.setFupno(sv.hseqno);
		fluprgpIO.setFormat(fluprgprec);
		/* If the update is an addition.*/
		if (isEQ(sv.updteflag, "C")) {
			fluprgpIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, fluprgpIO);
			if (isNE(fluprgpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(fluprgpIO.getParams());
				fatalError600();
			}
			fluprgpIO.setFuptype("C");
			fluprgpIO.setTranno(regpIO.getTranno());
			/*       MOVE S6678-FUPCODE       TO FLUPRGP-FUPCODE               */
			fluprgpIO.setFupcode(sv.fupcdes);
			fluprgpIO.setFupstat(sv.fupstat);
			fluprgpIO.setFupremdt(sv.fupremdt);
			fluprgpIO.setFupremk(sv.fupremk);
			fluprgpIO.setLife(regpIO.getLife());
			fluprgpIO.setJlife("00");
			/*MOVE VRCM-TERM           TO FLUPRGP-TERMID                */
			fluprgpIO.setTermid(varcom.vrcmTermid);
			fluprgpIO.setUser(varcom.vrcmUser);
			fluprgpIO.setTransactionDate(varcom.vrcmDate);
			fluprgpIO.setTransactionTime(varcom.vrcmTime);
			fluprgpIO.setFuprcvd(sv.fuprcvd);
			fluprgpIO.setExprdate(sv.exprdate);
			fluprgpIO.setEffdate(99999999);
			fluprgpIO.setCrtuser(sv.crtuser);
			fluprgpIO.setCrtdate(sv.crtdate);
			fluprgpIO.setClamnum(wsbbClamnum);
			fluprgpIO.setZlstupdt(varcom.vrcmMaxDate);
			fluprgpIO.setFormat(fluprgprec);
			writeFupe3500();
			fluprgpIO.setFunction(varcom.writr);
			flupFile3200();
		}
		/* If the update is an update.*/
		if (isEQ(sv.updteflag, "M")) {
			/*    Release hold on the regular payment details                  */
			fluprgpIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, fluprgpIO);
			if (isNE(fluprgpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(fluprgpIO.getParams());
				fatalError600();
			}
			fluprgpIO.setFunction(varcom.readh);
			flupFile3200();
			/*       MOVE S6678-FUPCODE       TO FLUPRGP-FUPCODE               */
			fluprgpIO.setFupcode(sv.fupcdes);
			fluprgpIO.setFupstat(sv.fupstat);
			fluprgpIO.setFupremdt(sv.fupremdt);
			fluprgpIO.setFupremk(sv.fupremk);
			/*MOVE VRCM-TERM           TO FLUPRGP-TERMID                */
			fluprgpIO.setTermid(varcom.vrcmTermid);
			fluprgpIO.setUser(varcom.vrcmUser);
			fluprgpIO.setTransactionDate(varcom.vrcmDate);
			fluprgpIO.setTransactionTime(varcom.vrcmTime);
			fluprgpIO.setFuprcvd(sv.fuprcvd);
			fluprgpIO.setExprdate(sv.exprdate);
			fluprgpIO.setLstupuser(wsspcomn.userid);
			fluprgpIO.setZlstupdt(datcon1rec.intDate);
			fluprgpIO.setFormat(fluprgprec);
			fluprgpIO.setFunction(varcom.rewrt);
			flupFile3200();
		}
	}

	/**
	* <pre>
	*     ACCESS THE FLUPRGP FILE
	* </pre>
	*/
protected void flupFile3200()
	{
		/*FLUP-FILE*/
		SmartFileCode.execute(appVars, fluprgpIO);
		if (isNE(fluprgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void writeFupe3500()
	{
		start3510();
		nextExtendedText3530();
	}

protected void start3510()
	{
		wsaaDocseq.set(ZERO);
		itemIO.setParams(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5661);
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		iy.set(ZERO);
		for (ix.set(5); !(isEQ(ix, ZERO)
		|| isNE(iy, ZERO)); ix.add(-1)){
			if (isNE(t5661rec.message[ix.toInt()], SPACES)) {
				iy.set(ix);
			}
		}
		ix.set(ZERO);
	}

protected void nextExtendedText3530()
	{
		ix.add(1);
		if (isGT(ix, 5)
		|| isGT(ix, iy)) {
			return ;
		}
		wsaaDocseq.add(1);
		fupeIO.setParams(SPACES);
		fupeIO.setStatuz(varcom.oK);
		fupeIO.setChdrcoy(fluprgpIO.getChdrcoy());
		fupeIO.setChdrnum(fluprgpIO.getChdrnum());
		fupeIO.setFupno(sv.hseqno);
		fupeIO.setClamnum(fluprgpIO.getClamnum());
		fupeIO.setTranno(fluprgpIO.getTranno());
		fupeIO.setDocseq(wsaaDocseq);
		fupeIO.setMessage(t5661rec.message[ix.toInt()]);
		fupeIO.setFunction(varcom.writr);
		fupeIO.setFormat(fuperec);
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fupeIO.getParams());
			syserrrec.statuz.set(fupeIO.getStatuz());
			fatalError600();
		}
		nextExtendedText3530();
		return ;
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.function.set("HIDEW");
			return ;
		}
		if (isEQ(wsaaPageDown, "Y")) {
			wsaaFirstPage = "N";
			fluprgpIO.setDataKey(wsaaFirstFlupKey);
			compute(wsaaRollFupno, 0).set(add(wsaaX, 1));
			fluprgpIO.setFupno(wsaaRollFupno);
			fluprgpIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, fluprgpIO);
			/*     IF (FLUPRGP-CHDRCOY       = REGP-CHDRCOY) OR              */
			/*        (FLUPRGP-CHDRNUM       = REGP-CHDRNUM) OR              */
			/*        (FLUPRGP-CLAMNUM       = WSBB-CLAMNUM) OR              */
			/*        (FLUPRGP-STATUZ    NOT = ENDP)                         */
			if ((isEQ(fluprgpIO.getChdrcoy(), regpIO.getChdrcoy()))
			&& (isEQ(fluprgpIO.getChdrnum(), regpIO.getChdrnum()))
			&& (isEQ(fluprgpIO.getClamnum(), wsbbClamnum))
			&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
				clearSubfile5000();
				/*          PERFORM 4100-ROLU-SUBFILE-LOAD 06 TIMES                */
				/*       PERFORM 4100-ROLU-SUBFILE-LOAD 03 TIMES        <LA4592>*/
				wsaaLine1Fupno.set(fluprgpIO.getFupno());
				PackedDecimalData loopEndVar2 = new PackedDecimalData(7, 0);
				loopEndVar2.set(wsaaSflsiz);
				for (int loopVar2 = 0; !(isEQ(loopVar2, loopEndVar2.toInt())); loopVar2 += 1){
					subfileNextLoad7200();
				}
				compute(wsaaX, 0).set(add(wsaaX, wsaaSflsiz));
			}
			else {
				PackedDecimalData loopEndVar3 = new PackedDecimalData(7, 0);
				loopEndVar3.set(wsaaSflsiz);
				for (int loopVar3 = 0; !(isEQ(loopVar3, loopEndVar3.toInt())); loopVar3 += 1){
					blankSubfile7300();
				}
			}
		}
		if (isEQ(wsaaPageUp, "Y")) {
			fluprgpIO.setParams(SPACES);
			fluprgpIO.setDataKey(wsaaFirstFlupKey);
			fluprgpIO.setFupno(wsaaLine1Fupno);
			/*    MOVE BEGN                TO FLUPRGP-FUNCTION              */
			fluprgpIO.setFunction(varcom.endr);
			//performance improvement --  Niharika Modi 
			fluprgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			fluprgpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","CLAMNUM");

			SmartFileCode.execute(appVars, fluprgpIO);
			if ((isNE(fluprgpIO.getStatuz(), varcom.oK))
			&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(fluprgpIO.getParams());
				fatalError600();
			}
			else {
				/*       PERFORM 5000-CLEAR-SUBFILE                             */
				/*          MOVE 06               TO WSAA-RRN                      */
				wsaaRrn.set(3);
				scrnparams.subfileRrn.set(3);
				/*       PERFORM 4200-ROLD-SUBFILE-LOAD UNTIL                   */
				/*         (FLUPRGP-STATUZ      = ENDP)         OR              */
				/*         (FLUPRGP-CHDRCOY NOT = REGP-CHDRCOY) OR              */
				/*         (FLUPRGP-CHDRNUM NOT = REGP-CHDRNUM) OR              */
				/*         (FLUPRGP-CLAMNUM NOT = WSBB-CLAMNUM) OR              */
				/*         (WSAA-RRN            = 0)                            */
				PackedDecimalData loopEndVar4 = new PackedDecimalData(7, 0);
				loopEndVar4.set(wsaaSflsiz);
				for (int loopVar4 = 0; !(isEQ(loopVar4, loopEndVar4.toInt())); loopVar4 += 1){
					a4200GetPrevPage();
				}
				clearSubfile5000();
				fluprgpIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, fluprgpIO);
				wsaaLine1Fupno.set(fluprgpIO.getFupno());
				PackedDecimalData loopEndVar5 = new PackedDecimalData(7, 0);
				loopEndVar5.set(wsaaSflsiz);
				for (int loopVar5 = 0; !(isEQ(loopVar5, loopEndVar5.toInt())); loopVar5 += 1){
					subfileNextLoad7200();
				}
				compute(wsaaX, 0).set(sub(wsaaX, wsaaSflsiz));
			}
		}
		if ((isEQ(wsaaPageUp, "Y"))
		|| (isEQ(wsaaPageDown, "Y"))) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			/*       MOVE SPACES                 TO WSAA-CHECK-BOX             */
			scrnparams.function.set(varcom.sstrt);
			a100CallS6678io();
			optswchCall4100();
		}
	}

protected void optswchCall4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call4110();
					lineSwitching4120();
				case switch4170: 
					switch4170();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call4110()
	{
		wsspcomn.nextprog.set(wsaaProg);
	}

	/**
	* <pre>
	* Check for Line Switching                                        
	* </pre>
	*/
protected void lineSwitching4120()
	{
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			scrnparams.subfileRrn.set(ZERO);
		}
		else {
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6678", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*    PERFORM UNTIL S6678-SELECT  NOT = SPACES OR                  */
		/*    PERFORM UNTIL S6678-SFFLG   NOT = SPACES OR                  */
		while ( !((isNE(sv.sfflg, SPACES)
		&& isNE(sv.sfflg, "+")
		&& isNE(sv.sfflg, "?"))
		|| isEQ(scrnparams.statuz, varcom.endp))) {
			if (isNE(sv.hseqno, SPACES)
			&& isNE(sv.fupcdes, SPACES)) {
				fupeIO.setParams(SPACES);
				/*       MOVE FLUPRGP-CHDRCOY       TO FUPE-CHDRCOY     <LA5056>*/
				/*       MOVE FLUPRGP-CHDRNUM       TO FUPE-CHDRNUM     <LA5056>*/
				fupeIO.setChdrcoy(regpIO.getChdrcoy());
				fupeIO.setChdrnum(regpIO.getChdrnum());
				fupeIO.setFupno(sv.hseqno);
				fupeIO.setClamnum(SPACES);
				fupeIO.setTranno(ZERO);
				fupeIO.setDocseq(ZERO);
				fupeIO.setFunction(varcom.begn);
				//performance improvement --  Niharika Modi 
				fupeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				fupeIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
				fupeIO.setFormat(fuperec);
				SmartFileCode.execute(appVars, fupeIO);
				if (isNE(fupeIO.getStatuz(), varcom.oK)
				&& isNE(fupeIO.getStatuz(), varcom.endp)) {
					syserrrec.statuz.set(fupeIO.getStatuz());
					syserrrec.params.set(fupeIO.getParams());
					fatalError600();
				}
				/*       IF FUPE-CHDRCOY            NOT = FLUPRGP-CHDRCOY       */
				/*       OR FUPE-CHDRNUM            NOT = FLUPRGP-CHDRNUM       */
				if (isNE(fupeIO.getChdrcoy(), regpIO.getChdrcoy())
				|| isNE(fupeIO.getChdrnum(), regpIO.getChdrnum())
				|| isNE(fupeIO.getFupno(), sv.hseqno)) {
					fupeIO.setStatuz(varcom.endp);
				}
				if (isEQ(fupeIO.getStatuz(), varcom.endp)) {
					sv.sfflg.set(SPACES);
					sv.indic.set(SPACES);
				}
				else {
					sv.sfflg.set("+");
					sv.indic.set("+");
				}
				checkHxcl1500();
			}
			sv.zitem.set(sv.fupcdes);
			scrnparams.function.set(varcom.supd);
			processScreen("S6678", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("S6678", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.switch4170);
		}
		/* Re-read FLUPGRP to get correct record for KEEPS                 */
		fluprgpIO.setParams(SPACES);
		fluprgpIO.setChdrcoy(regpIO.getChdrcoy());
		fluprgpIO.setChdrnum(regpIO.getChdrnum());
		fluprgpIO.setClamnum(wsbbClamnum);
		fluprgpIO.setFupno(sv.hseqno);
		fluprgpIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, fluprgpIO);
		if (isNE(fluprgpIO.getStatuz(), varcom.oK)
		&& isNE(fluprgpIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(fluprgpIO.getStatuz());
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
		/* ...if found, "select" the subfile line to switch to PopUp       */
		/* program.                                                        */
		/* READR and KEEPS the FLUP record for the next program            */
		flupenqIO.setParams(SPACES);
		flupenqIO.setChdrcoy(wsspcomn.company);
		flupenqIO.setChdrnum(fluprgpIO.getChdrnum());
		flupenqIO.setFuptype(fluprgpIO.getFuptype());
		flupenqIO.setTransactionDate(fluprgpIO.getTransactionDate());
		flupenqIO.setTranno(fluprgpIO.getTranno());
		flupenqIO.setFupno(fluprgpIO.getFupno());
		flupenqIO.setFormat(flupenqrec);
		flupenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, flupenqIO);
		if (isNE(flupenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupenqIO.getParams());
			fatalError600();
		}
		flupenqIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, flupenqIO);
		if (isNE(flupenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupenqIO.getParams());
			fatalError600();
		}
		/* READR and KEEPS the FLUP record for the next program            */
		flupaltIO.setParams(SPACES);
		flupaltIO.setChdrcoy(wsspcomn.company);
		flupaltIO.setChdrnum(fluprgpIO.getChdrnum());
		flupaltIO.setFupno(sv.hseqno);
		flupaltIO.setFormat(flupaltrec);
		flupaltIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, flupaltIO);
		if (isNE(flupaltIO.getStatuz(), varcom.oK)
		&& isNE(flupaltIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(flupaltIO.getParams());
			fatalError600();
		}
		if (isEQ(flupaltIO.getStatuz(), varcom.mrnf)) {
			if (isEQ(sv.fupcdes, SPACES)
			&& isEQ(sv.fupremk, SPACES)
			&& isEQ(sv.fupstat, SPACES)
			&& (isEQ(sv.fupremdt, ZERO)
			|| isEQ(sv.fupremdt, 99999999)
			|| isEQ(sv.fupremdt, SPACES))
			&& isEQ(fluprgpIO.getJlife(), SPACES)) {
				goTo(GotoLabel.switch4170);
			}
		}
		flupaltIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, flupaltIO);
		if (isNE(flupaltIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupaltIO.getParams());
			fatalError600();
		}
		keepsFluprgp4700();
		wsaaRrn.set(scrnparams.subfileRrn);
		optswchrec.optsSelType.set("L");
		/*    MOVE S6678-SFFLG            TO OPTS-SEL-OPTNO.               */
		if (isEQ(sv.sfflg, "1")
		|| isEQ(sv.sfflg, "2")) {
			optswchrec.optsSelOptno.set(sv.sfflg);
		}
		optswchrec.optsSelCode.set(SPACES);
		/* Update the subfile line.                                        */
		sv.sfflg.set(SPACES);
		sv.zitem.set(sv.fupcdes);
		sv.updteflag.set("N");
		scrnparams.function.set(varcom.supd);
		processScreen("S6678", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void switch4170()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)
		&& isNE(optswchrec.optsStatuz, varcom.endp)) {
			syserrrec.params.set(optswchrec.rec);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			fatalError600();
		}
		/*    MOVE OPTS-IND(1)            TO S6678-ZSAIND.                 */
		if (isEQ(optswchrec.optsStatuz, varcom.oK)) {
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

	/**
	* <pre>
	**** Duplciate 4100 section, rename as 7200 section               
	*4100-ROLU-SUBFILE-LOAD SECTION.                                  
	* </pre>
	*/
protected void subfileNextLoad7200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					startSubfileReload7210();
					fillSubfileRecord7250();
				case addSubfileRecord7290: 
					addSubfileRecord7290();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*4110-ROLU-SUBFILE-LOAD.                                          
	* </pre>
	*/
protected void startSubfileReload7210()
	{
		wsaaNoMoreFlups = "N";
		if ((isNE(fluprgpIO.getChdrcoy(), regpIO.getChdrcoy()))
		|| (isNE(fluprgpIO.getChdrnum(), regpIO.getChdrnum()))
		|| (isNE(fluprgpIO.getClamnum(), wsbbClamnum))
		|| (isEQ(fluprgpIO.getStatuz(), varcom.endp))) {
			wsaaNoMoreFlups = "Y";
			sv.subfileFields.set(SPACES);
			sv.fupremdt.set(99999999);
			wsaaFupno.add(1);
			sv.hseqno.set(wsaaFupno);
			sv.zitem.set(SPACES);
			sv.sfflg.set(SPACES);
			sv.indic.set(SPACES);
			sv.updteflag.set(SPACES);
			sv.crtuser.set(wsspcomn.userid);
			sv.language.set(wsspcomn.language);
			sv.crtdate.set(varcom.vrcmMaxDate);
			sv.exprdate.set(varcom.vrcmMaxDate);
			sv.fuprcvd.set(varcom.vrcmMaxDate);
			sv.fuptype.set("C");
			sv.fuptypOut[varcom.nd.toInt()].set("Y");
			sv.fupcdesOut[varcom.pr.toInt()].set("N");
			/*    GO TO 4120-ADD-SUBFILE-RECORD                             */
			goTo(GotoLabel.addSubfileRecord7290);
		}
	}

	/**
	* <pre>
	*4115-ROLU.                                                       
	* </pre>
	*/
protected void fillSubfileRecord7250()
	{
		/*    MOVE FLUPRGP-FUPCODE        TO S6678-FUPCODE.                */
		sv.fupcdes.set(fluprgpIO.getFupcode());
		sv.fupremdt.set(fluprgpIO.getFupremdt());
		sv.hseqno.set(fluprgpIO.getFupno());
		wsaaFupno.set(fluprgpIO.getFupno());
		sv.fupremk.set(fluprgpIO.getFupremk());
		sv.fupstat.set(fluprgpIO.getFupstat());
		sv.updteflag.set("N");
		sv.zitem.set(fluprgpIO.getFupcode());
		readFupe1600();
		if (isEQ(fupeIO.getStatuz(), varcom.oK)) {
			sv.sfflg.set("+");
			sv.indic.set("+");
		}
		else {
			sv.sfflg.set(SPACES);
			sv.indic.set(SPACES);
		}
		sv.crtuser.set(fluprgpIO.getCrtuser());
		sv.language.set(wsspcomn.language);
		if (isNE(fluprgpIO.getCrtdate(), NUMERIC)) {
			sv.crtdate.set(varcom.vrcmMaxDate);
		}
		else {
			sv.crtdate.set(fluprgpIO.getCrtdate());
		}
		if (isNE(fluprgpIO.getExprdate(), NUMERIC)) {
			sv.exprdate.set(varcom.vrcmMaxDate);
		}
		else {
			sv.exprdate.set(fluprgpIO.getExprdate());
		}
		if (isNE(fluprgpIO.getFuprcvd(), NUMERIC)) {
			sv.fuprcvd.set(varcom.vrcmMaxDate);
		}
		else {
			sv.fuprcvd.set(fluprgpIO.getFuprcvd());
		}
		sv.fupcdesOut[varcom.pr.toInt()].set("Y");
		fluprgpIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, fluprgpIO);
		if ((isNE(fluprgpIO.getStatuz(), varcom.oK))
		&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*4120-ADD-SUBFILE-RECORD.                                         
	* </pre>
	*/
protected void addSubfileRecord7290()
	{
		sv.fuprmkOut[varcom.pr.toInt()].set("Y");
		if (isEQ(wsspcomn.flag, "I")) {
			sv.fupcdesOut[varcom.pr.toInt()].set("Y");
			sv.fupstsOut[varcom.pr.toInt()].set("Y");
			sv.fupdtOut[varcom.pr.toInt()].set("Y");
			sv.fuprcvdOut[varcom.pr.toInt()].set("Y");
			sv.exprdateOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S6678", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
		/*4190-EXIT.                                                       */
	}

protected void blankSubfile7300()
	{
		start7310();
	}

protected void start7310()
	{
		wsaaNoMoreFlups = "Y";
		sv.subfileFields.set(SPACES);
		sv.fupremdt.set(99999999);
		wsaaFupno.add(1);
		sv.hseqno.set(wsaaFupno);
		sv.zitem.set(SPACES);
		sv.sfflg.set(SPACES);
		sv.indic.set(SPACES);
		sv.updteflag.set(SPACES);
		sv.crtuser.set(wsspcomn.userid);
		sv.language.set(wsspcomn.language);
		sv.crtdate.set(varcom.vrcmMaxDate);
		sv.exprdate.set(varcom.vrcmMaxDate);
		sv.fuprcvd.set(varcom.vrcmMaxDate);
		sv.fuptype.set("C");
		sv.fuptypOut[varcom.nd.toInt()].set("Y");
		sv.fupcdesOut[varcom.pr.toInt()].set("N");
		sv.fuprmkOut[varcom.pr.toInt()].set("Y");
		scrnparams.function.set(varcom.sadd);
		processScreen("S6678", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*4200-ROLD-SUBFILE-LOAD SECTION.                                  
	*4210-ROLD-SUBFILE-LOAD.                                          
	**** MOVE NEXTP                  TO FLUPRGP-FUNCTION.             
	**** CALL 'FLUPRGPIO'            USING FLUPRGP-PARAMS.            
	**** IF (FLUPRGP-STATUZ       NOT = O-K) AND                      
	****    (FLUPRGP-STATUZ       NOT = ENDP)                         
	****    MOVE FLUPRGP-PARAMS   TO SYSR-PARAMS                      
	****    PERFORM 600-FATAL-ERROR                                   
	**** END-IF.                                                      
	**** IF (FLUPRGP-CHDRCOY          = REGP-CHDRCOY) OR              
	****    (FLUPRGP-CHDRNUM          = REGP-CHDRNUM) OR              
	****    (FLUPRGP-CLAMNUM          = WSBB-CLAMNUM) OR              
	****    (FLUPRGP-STATUZ           = ENDP)                         
	****    MOVE SPACES              TO FLUPRGP-PARAMS                
	****    PERFORM 5000-CLEAR-SUBFILE                                
	****    MOVE WSAA-FIRST-FLUP-KEY TO FLUPRGP-DATA-KEY              
	****    MOVE BEGN             TO FLUPRGP-FUNCTION                 
	****    CALL 'FLUPRGPIO'      USING FLUPRGP-PARAMS                
	****    IF (FLUPRGP-STATUZ    NOT = O-K) AND                      
	****       (FLUPRGP-STATUZ    NOT = ENDP)                         
	****        MOVE FLUPRGP-PARAMS  TO SYSR-PARAMS                   
	****        PERFORM 600-FATAL-ERROR                               
	****    ELSE                                                      
	****        MOVE 'Y'             TO WSAA-FIRST-PAGE               
	****        PERFORM 4100-ROLU-SUBFILE-LOAD  06  TIMES     <LA4700>
	****        PERFORM 4100-ROLU-SUBFILE-LOAD  03  TIMES     <LA4700>
	****        PERFORM 7200-SUBFILE-NEXT-LOAD  WSAA-SFLSIZ TIMES     
	****        MOVE ZEROS           TO WSAA-RRN                      
	****        GO TO 4290-EXIT                                       
	****    END-IF                                                    
	**** END-IF.                                                      
	**** MOVE FLUPRGP-FUPCODE        TO S6678-FUPCODE.        <LA4700>
	**** MOVE FLUPRGP-FUPCODE        TO S6678-FUPCDES.        <LA4700>
	**** MOVE FLUPRGP-FUPREMDT       TO S6678-FUPREMDT.               
	**** MOVE FLUPRGP-FUPNO          TO S6678-HSEQNO                  
	****                                WSAA-FUPNO.                   
	**** MOVE FLUPRGP-FUPREMK        TO S6678-FUPREMK.                
	**** MOVE FLUPRGP-FUPSTAT        TO S6678-FUPSTAT.                
	**** MOVE 'N'                    TO S6678-UPDTEFLAG.              
	**** MOVE FLUPRGP-FUPCODE        TO S6678-ZITEM.          <LA4700>
	**** PERFORM 1600-READ-FUPE.                              <LA4700>
	**** IF FUPE-STATUZ              = O-K                    <LA4700>
	****    MOVE '+'                 TO S6678-SFFLG           <LA4700>
	****                                S6678-INDIC           <LA4700>
	**** ELSE                                                 <LA4700>
	****    MOVE SPACES              TO S6678-SFFLG           <LA4700>
	****                                S6678-INDIC           <LA4700>
	**** END-IF.                                              <LA4700>
	**** MOVE FLUPRGP-CRTUSER        TO S6678-CRTUSER.        <LA4700>
	**** MOVE WSSP-LANGUAGE          TO S6678-LANGUAGE.       <LA4700>
	**** IF FLUPRGP-CRTDATE  NOT  NUMERIC                     <LA4700>
	****    MOVE VRCM-MAX-DATE       TO S6678-CRTDATE         <LA4700>
	**** ELSE                                                 <LA4700>
	****    MOVE FLUPRGP-CRTDATE     TO S6678-CRTDATE         <LA4700>
	**** END-IF.                                              <LA4700>
	*4220-ADD-SUBFILE-RECORD.                                         
	**** MOVE 'Y'                    TO S6678-FUPRMK-OUT(PR). <LA4700>
	****                                                      <LA4700>
	**** IF  WSSP-FLAG                = 'I'                   <LA4700>
	****    MOVE 'Y'                 TO S6678-FUPCDES-OUT(PR) <LA4700>
	****                                S6678-FUPSTS-OUT(PR)  <LA4700>
	****                                S6678-FUPDT-OUT(PR)   <LA4700>
	****                                S6678-FUPRCVD-OUT (PR)<LA4700>
	****                                S6678-EXPRDATE-OUT(PR)<LA4700>
	**** END-IF.                                              <LA4700>
	****                                                      <LA4700>
	**** MOVE SADD                   TO SCRN-FUNCTION.                
	**** CALL 'S6678IO'              USING SCRN-SCREEN-PARAMS         
	****                             S6678-DATA-AREA                  
	****                             S6678-SUBFILE-AREA.              
	**** IF SCRN-STATUZ           NOT = O-K                           
	****    MOVE SCRN-STATUZ         TO SYSR-STATUZ                   
	****    PERFORM 600-FATAL-ERROR                                   
	**** END-IF.                                                      
	**** MOVE FLUPRGP-DATA-KEY       TO WSAA-FIRST-FLUP-KEY.          
	**** SUBTRACT 1                  FROM WSAA-RRN.                   
	**** MOVE WSAA-RRN               TO SCRN-SUBFILE-RRN.             
	*4290-EXIT.                                                       
	*    EXIT.                                                        
	* </pre>
	*/
protected void a4200GetPrevPage()
	{
		/*A4210-START*/
		fluprgpIO.setFunction(varcom.nextp);
		SmartFileCode.execute(appVars, fluprgpIO);
		if ((isNE(fluprgpIO.getStatuz(), varcom.oK))
		&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
		/*A4290-EXIT*/
	}

	/**
	* <pre>
	* Option switching                                                
	* </pre>
	*/
protected void optswchCall4600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start4610();
				case updateSubfile4670: 
					updateSubfile4670();
					readNextModifiedRecord4680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start4610()
	{
		if (isEQ(sv.sfflg, SPACES)
		|| isEQ(sv.sfflg, "+")
		|| isEQ(sv.sfflg, "?")) {
			goTo(GotoLabel.updateSubfile4670);
		}
		optswchrec.optsInd[1].set(sv.sfflg);
		wsaaCheckBox.set("Y");
		keepsFluprgp4700();
	}

protected void updateSubfile4670()
	{
		if (isEQ(sv.sfflg, "?")) {
			readFupe1600();
			if (isEQ(fupeIO.getStatuz(), varcom.endp)) {
				sv.sfflg.set(SPACES);
				sv.indic.set(SPACES);
			}
			else {
				sv.sfflg.set("+");
				sv.indic.set("+");
			}
		}
		if (isNE(sv.sfflg, SPACES)
		&& isNE(sv.sfflg, "+")
		&& isNE(sv.sfflg, "?")
		&& isNE(sv.sfflg, "1")
		&& isNE(sv.sfflg, "2")) {
			sv.sfflg.set("?");
		}
		sv.zitem.set(sv.fupcdes);
		if (isNE(sv.updteflag, " ")
		&& isNE(sv.fupcdes, " ")) {
			sv.updteflag.set("N");
		}
		scrnparams.function.set(varcom.supd);
		a100CallS6678io();
	}

protected void readNextModifiedRecord4680()
	{
		scrnparams.function.set(varcom.srdn);
		a100CallS6678io();
		/*EXIT*/
	}

protected void keepsFluprgp4700()
	{
		/*START*/
		fluprgpIO.setParams(SPACES);
		fluprgpIO.setStatuz(varcom.oK);
		fluprgpIO.setChdrcoy(regpIO.getChdrcoy());
		fluprgpIO.setChdrnum(regpIO.getChdrnum());
		fluprgpIO.setClamnum(wsbbClamnum);
		fluprgpIO.setFupno(sv.hseqno);
		fluprgpIO.setFormat(fluprgprec);
		fluprgpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, fluprgpIO);
		if (isNE(fluprgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
		fluprgpIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, fluprgpIO);
		if (isNE(fluprgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
	}

protected void a100CallS6678io()
	{
		/*A110-START*/
		processScreen("S6678", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*A190-EXIT*/
	}

protected void clearSubfile5000()
	{
		/*CLEAR-SUBFILE*/
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6678", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

protected void setReminderDate6000()
	{
		setRemdt6010();
	}

protected void setRemdt6010()
	{
		/*  Calculate the date of issuing reminder letter                  */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5661);
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itemIO.setItemitem(wsaaT5661Key);
		/*    MOVE S6678-FUPCODE          TO ITEM-ITEMITEM.        <V72L11>*/
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(datcon1rec.intDate);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(t5661rec.zelpdays);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		sv.fupremdt.set(datcon2rec.intDate2);
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e005 = new FixedLengthStringData(4).init("E005");
	private FixedLengthStringData f498 = new FixedLengthStringData(4).init("F498");
	private FixedLengthStringData e558 = new FixedLengthStringData(4).init("E558");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	private FixedLengthStringData h961 = new FixedLengthStringData(4).init("H961");
	private FixedLengthStringData h963 = new FixedLengthStringData(4).init("H963");
	private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
	private FixedLengthStringData hl50 = new FixedLengthStringData(4).init("HL50");
	private FixedLengthStringData rf00 = new FixedLengthStringData(4).init("RF00");
	private FixedLengthStringData w431 = new FixedLengthStringData(4).init("W431");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData g914 = new FixedLengthStringData(4).init("G914");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData f499 = new FixedLengthStringData(4).init("F499");
}
}
