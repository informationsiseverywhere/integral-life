package com.csc.life.terminationclaims.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Clnnpf implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long uniqueNumber;
	//INVSCOY
	private String clnncoy;
	//NOTIFINUM
	private String notifinum;
	//LIFENUM
	private String lifenum;
	//CLAIMANT
	private String claimant;
	//RELATIONSHIP
	private String relationship;
	//NOTIFICATION NOTE
	private String NotificationNote;
	// USRPRF
	private String usrprf;
	// JOBNM
	private String jobnm;
	// DATIME
	private String datime;
	private String claimno;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getClnncoy() {
		return clnncoy;
	}
	public void setClnncoy(String clnncoy) {
		this.clnncoy = clnncoy;
	}
	public String getNotifinum() {
		return notifinum;
	}
	public void setNotifinum(String notifinum) {
		this.notifinum = notifinum;
	}
	public String getLifenum() {
		return lifenum;
	}
	public void setLifenum(String lifenum) {
		this.lifenum = lifenum;
	}
	public String getClaimant() {
		return claimant;
	}
	public void setClaimant(String claimant) {
		this.claimant = claimant;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getNotificationNote() {
		return NotificationNote;
	}
	public void setNotificationNote(String notificationNote) {
		NotificationNote = notificationNote;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getClaimno() {
		return claimno;
	}
	public void setClaimno(String claimno) {
		this.claimno = claimno;
	}
	
}
