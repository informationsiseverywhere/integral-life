package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CattpfDAOImpl extends BaseDAOImpl<Cattpf> implements CattpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(CattpfDAOImpl.class);

	public boolean isExists(String chdrcoy,String chdrnum){
		StringBuilder sb = new StringBuilder("");

		sb.append("SELECT COUNT(*) ");
		sb.append("FROM CATTPF WHERE CHDRCOY=? AND CHDRNUM=? AND CLAMSTAT = 'PR' AND CLAMTYP = 'DC' AND VALIDFLAG = '1' ");

		int count = 0;
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet sql1rs = null;
		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);

			sql1rs = executeQuery(ps);

			while (sql1rs.next()) {  
				count = sql1rs.getInt(1);  
			}

		}

		catch (SQLException e) {
			LOGGER.error("selectCattpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, sql1rs);
		}
		if (count > 0){ 
			return true;
		}
		return false;
	}
	public void insertCattpfRecord(Cattpf cattpf){
		StringBuilder sql = new StringBuilder("INSERT INTO CATTPF (CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,CLAIM,CLAMAMT,CLAMTYP,CLAMSTAT,TRANNO,TRCODE,DTOFDEATH");
		sql.append(",CAUSEOFDTH,EFFDATE,LIFCNUM,SEQENUM,USRPRF,JOBNM,DATIME,DOCEFFDATE,FLAG,KPAYDATE,INTBASEDT,FULFILLTRM,KARIFLAG,INTCALFLG,VALIDFLAG,CONDTE)"
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sql.toString());

			ps.setString(1, cattpf.getChdrcoy());
			ps.setString(2, cattpf.getChdrnum());
			ps.setString(3, cattpf.getLife());
			ps.setString(4,cattpf.getJlife() );
			ps.setString(5, cattpf.getCoverage());
			ps.setString(6, cattpf.getRider());
			ps.setString(7, cattpf.getClaim());
			ps.setBigDecimal(8, cattpf.getClamamt());
			ps.setString(9, cattpf.getClamtyp());
			ps.setString(10, cattpf.getClamstat());
			ps.setInt(11, cattpf.getTranno());
			ps.setString(12, cattpf.getTrcode());
			ps.setInt(13, cattpf.getDtofdeath());
			ps.setString(14, cattpf.getCauseofdth());
			ps.setInt(15, cattpf.getEffdate());
			ps.setString(16, cattpf.getLifcnum());
			ps.setString(17, cattpf.getSeqenum());
			ps.setString(18, getUsrprf());
			ps.setString(19, getJobnm());
			ps.setTimestamp(20, new Timestamp(System.currentTimeMillis()));
			ps.setInt(21, cattpf.getDoceffdate());
			ps.setString(22, cattpf.getFlag());
			ps.setInt(23, cattpf.getKpaydate());
			ps.setInt(24, cattpf.getIntbasedt());
			ps.setString(25, cattpf.getFulfilltrm());
			ps.setString(26, cattpf.getKariflag());
			ps.setString(27, cattpf.getIntcalflag());
			ps.setString(28, cattpf.getValidflag());
			ps.setInt(29, cattpf.getCondte());//ILJ-487

			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("insertCattpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	public Cattpf selectRecords(String chdrcoy,String chdrnum){

		StringBuilder sb = new StringBuilder("");

		sb.append("SELECT CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, CLAIM, CLAMTYP, CLAMSTAT, TRANNO, TRCODE, ");
		sb.append("CLAMAMT, DTOFDEATH, CONDTE, CAUSEOFDTH, EFFDATE, LIFCNUM, SEQENUM, DOCEFFDATE, ");
		sb.append("FLAG, KPAYDATE, INTBASEDT, FULFILLTRM, KARIFLAG, INTCALFLG FROM CATTPF WHERE ");
		sb.append("CHDRCOY=? AND CHDRNUM=? AND VALIDFLAG = '1' ORDER BY UNIQUE_NUMBER ASC");

		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet sql1rs = null;
		Cattpf cattpf = new Cattpf();
		try {

			ps.setString(1,chdrcoy );
			ps.setString(2, chdrnum);

			sql1rs = executeQuery(ps);

			while (sql1rs.next()) {
				cattpf.setChdrcoy(sql1rs.getString("CHDRCOY"));
				cattpf.setChdrnum(sql1rs.getString("CHDRNUM"));
				cattpf.setLife(sql1rs.getString("LIFE"));
				cattpf.setJlife(sql1rs.getString("JLIFE"));
				cattpf.setCoverage(sql1rs.getString("COVERAGE"));
				cattpf.setRider(sql1rs.getString("RIDER"));
				cattpf.setClaim(sql1rs.getString("CLAIM"));
				cattpf.setClamtyp(sql1rs.getString("CLAMTYP"));
				cattpf.setClamstat(sql1rs.getString("CLAMSTAT"));
				cattpf.setTranno(sql1rs.getInt("TRANNO"));
				cattpf.setTrcode(sql1rs.getString("TRCODE"));
				cattpf.setClamamt(sql1rs.getBigDecimal("CLAMAMT"));
				cattpf.setDtofdeath(sql1rs.getInt("DTOFDEATH"));
				cattpf.setCondte(sql1rs.getInt("CONDTE"));
				cattpf.setCauseofdth(sql1rs.getString("CAUSEOFDTH"));
				cattpf.setEffdate(sql1rs.getInt("EFFDATE"));
				cattpf.setLifcnum(sql1rs.getString("LIFCNUM"));
				cattpf.setSeqenum(sql1rs.getString("SEQENUM"));
				cattpf.setDoceffdate(sql1rs.getInt("DOCEFFDATE"));
				cattpf.setFlag(sql1rs.getString("FLAG"));
				cattpf.setKpaydate(sql1rs.getInt("KPAYDATE"));
				cattpf.setIntbasedt(sql1rs.getInt("INTBASEDT"));
				cattpf.setFulfilltrm(sql1rs.getString("FULFILLTRM"));
				cattpf.setKariflag(sql1rs.getString("KARIFLAG"));
				cattpf.setIntcalflag(sql1rs.getString("INTCALFLG"));
			}
		} 

		catch (SQLException e) {
			LOGGER.error("selectRecords()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, sql1rs);
		}
		return cattpf;
	}

	public void deleteRcdByChdrnum(String chdrcoy,String chdrnum) {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM CATTPF");
		sb.append(" WHERE CHDRCOY=?  AND CHDRNUM=? AND VALIDFLAG = '1' ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);

			ps.executeUpdate();				


		}catch (SQLException e) {
			LOGGER.error("deleteRcdByChdrnum()",e);	
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}				
	}		

	public int updateCattpf(String chdrcoy,String chdrnum) {

		String SQL_CATT_UPDATE = "UPDATE CATTPF SET VALIDFLAG=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE CHDRCOY=? AND CHDRNUM = ? ";

		PreparedStatement psCattUpdate = getPrepareStatement(SQL_CATT_UPDATE);
		int count;
		try {
			psCattUpdate.setString(1, "2");
			psCattUpdate.setString(2, getJobnm());
			psCattUpdate.setString(3, getUsrprf());
			psCattUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			psCattUpdate.setString(5, chdrcoy);
			psCattUpdate.setString(6, chdrnum);	

			count = psCattUpdate.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateCattRecord()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psCattUpdate, null);
		}
		return count;
	}
	public int updateClaimStatus(String chdrcoy,String chdrnum,String clamstat) {

		String SQL_CATT_UPDATE = "UPDATE CATTPF SET CLAMSTAT=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE CHDRCOY=? AND CHDRNUM = ? AND VALIDFLAG = '1'";

		PreparedStatement psCattUpdate = getPrepareStatement(SQL_CATT_UPDATE);
		int count;
		try {
			psCattUpdate.setString(1, clamstat);
			psCattUpdate.setString(2, getJobnm());
			psCattUpdate.setString(3, getUsrprf());
			psCattUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			psCattUpdate.setString(5, chdrcoy);
			psCattUpdate.setString(6, chdrnum);	

			count = psCattUpdate.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateClaimStatus()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psCattUpdate, null);
		}
		return count;
	}

	public int updateCattpfSetValidflag1(String chdrcoy,String chdrnum) {

		String SQL_CATT_UPDATE = "UPDATE CATTPF SET VALIDFLAG=?,CLAMSTAT=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE CHDRCOY=? AND CHDRNUM = ? ";

		PreparedStatement psCattUpdate = getPrepareStatement(SQL_CATT_UPDATE);
		int count;
		try {
			psCattUpdate.setString(1, "1");
			psCattUpdate.setString(2, "PR");
			psCattUpdate.setString(3, getJobnm());
			psCattUpdate.setString(4, getUsrprf());
			psCattUpdate.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			psCattUpdate.setString(6, chdrcoy);
			psCattUpdate.setString(7, chdrnum);	

			count = psCattUpdate.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateCattRecord()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psCattUpdate, null);
		}
		return count;
	}
	
	public List<Cattpf> approvedRecords(){

		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT CATTPF.CLAIM,CATTPF.CHDRNUM,CATTPF.CONDTE,CATTPF.CLAMTYP,CATTPF.CLAMSTAT,CATTPF.LIFCNUM  ");
		sb.append("FROM CATTPF,CHDRPF WHERE CATTPF.CHDRNUM=CHDRPF.CHDRNUM AND ((CHDRPF.STATCODE='DH' AND CATTPF.CLAMSTAT='AP') OR (CATTPF.CLAMSTAT='RG' AND CHDRPF.STATCODE='CC')) ");//IBPLIFE-3582
		sb.append("and CATTPF.CLAIM!='' and CATTPF.VALIDFLAG='1' ");
		sb.append("ORDER BY CATTPF.CLAIM ASC ");
		List<Cattpf> list = new ArrayList<>();
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet sql1rs = null;
		Cattpf cattpf = null;
		try {

			sql1rs = executeQuery(ps);

			while (sql1rs.next()) {  
				cattpf = new Cattpf();
				cattpf.setClaim(sql1rs.getString(1));
				cattpf.setChdrnum(sql1rs.getString(2));      	
				cattpf.setCondte(sql1rs.getInt(3));    
				cattpf.setClamtyp(sql1rs.getString(4));
				cattpf.setClamstat(sql1rs.getString(5));
				cattpf.setLifcnum(sql1rs.getString(6));
				list.add(cattpf);
			}
		} 

		catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, sql1rs);
		}
		return list;
	}
	
	public Cattpf approvedClaimRecord(String claimNum){

		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT CLAIM, CHDRNUM, CONDTE, CLAMTYP, CLAMSTAT, LIFCNUM ");
		sb.append("FROM CATTPF WHERE CLAIM =? AND VALIDFLAG ='1'");

		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet sql1rs = null;
		
		Cattpf cattpf = null;
		try {
			ps.setString(1, claimNum);
			sql1rs = executeQuery(ps);

			while (sql1rs.next()) {  
				cattpf = new Cattpf();
				cattpf.setClaim(sql1rs.getString(1));
				cattpf.setChdrnum(sql1rs.getString(2));      	
				cattpf.setCondte(sql1rs.getInt(3));    
				cattpf.setClamtyp(sql1rs.getString(4));
				cattpf.setClamstat(sql1rs.getString(5));
				cattpf.setLifcnum(sql1rs.getString(6));
			}
		} 

		catch (SQLException e) {
			LOGGER.error("selectRecords()",e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, sql1rs);
		}
		return cattpf;
	} 

}
