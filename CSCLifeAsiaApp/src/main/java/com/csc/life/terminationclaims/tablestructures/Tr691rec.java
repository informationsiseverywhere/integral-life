package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:23
 * Description:
 * Copybook name: TR691REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr691rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr691Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData flatrate = new ZonedDecimalData(17, 2).isAPartOf(tr691Rec, 0);
  	public ZonedDecimalData pcnt = new ZonedDecimalData(4, 2).isAPartOf(tr691Rec, 17);
  	public ZonedDecimalData tyearno = new ZonedDecimalData(2, 0).isAPartOf(tr691Rec, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(477).isAPartOf(tr691Rec, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr691Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr691Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}