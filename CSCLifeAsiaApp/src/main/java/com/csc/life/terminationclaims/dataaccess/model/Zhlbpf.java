package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name = "ZHLBPF", schema = "VM1DTA")
public class Zhlbpf implements java.io.Serializable {
	@Id
	private String chdrcoy;
	private String chdrnum;
	private String bnyclt;
	private BigDecimal bnypc;
	private String validflag;
	private int tranno;
	private int trdt;
	private int trtm;
	private int usr;
	private BigDecimal actvalue;
	private BigDecimal zclmadjst;
	private BigDecimal zhldclmv;
	private BigDecimal zhldclma;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	

	public Zhlbpf(){
	}
	
	public Zhlbpf(String chdrcoy, String chdrnum, String bnyclt, BigDecimal bnypc, String validflag, int tranno, int trdt, int trtm,
			int usr, BigDecimal actvalue, BigDecimal zclmadjst, BigDecimal zhldclmv, BigDecimal zhldclma, String usrprf, String jobnm, Timestamp datime){
	
	this.chdrcoy = chdrcoy;
	this.chdrnum = chdrnum;
	this.bnyclt = bnyclt;
	this.bnypc = bnypc;
	this.validflag = validflag;
	this.tranno = tranno;
	this.trdt = trdt;
	this.trtm = trtm;
	this.usr = usr;
	this.actvalue = actvalue;
	this.zclmadjst = zclmadjst;
	this.zhldclmv = zhldclmv;
	this.zhldclma = zhldclma;
	this.usrprf = usrprf;
	this.jobnm = jobnm;
	this.datime = new Timestamp(datime.getTime());//IJTI-314
	}
	
	
	@Column(name = "CHDRCOY", length = 2)
	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	@Column(name = "CHDRNUM", length = 8)
	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	
	@Column(name = "BNYCLT", length = 8)
	public String getBnyclt(){
		return bnyclt;
	}
	public void setBnyclt(String bnyclt){
		this.bnyclt = bnyclt;
	}
	@Column(name = "BNYPC", precision = 5, scale = 2)
	public BigDecimal getBnypc() {
		return bnypc;
	}

	public void setBnypc(BigDecimal bnypc) {
		this.bnypc = bnypc;
	}
	
	@Column(name = "VALIDFLAG", length = 1)
	public String getValidflag(){
		return validflag;
	}
	public void setValidflag(String validflag){
		this.validflag = validflag;
	}
	@Column(name = "TRANNO", precision = 5, scale = 0)
	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	
	@Column(name = "TRDT", precision = 6, scale = 0)
	public int getTrdt() {
		return trdt;
	}

	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}
	
	@Column(name = "TRTM", precision = 6, scale = 0)
	public int getTrtm() {
		return trtm;
	}

	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}
	
	@Column(name = "USR", precision = 6, scale = 0)
	public int getUsr() {
		return usr;
	}

	public void setUsr(int usr) {
		this.usr = usr;
	}
	
	@Column(name = "ACTVALUE", precision = 17, scale = 2)
	public BigDecimal getActvalue() {
		return actvalue;
	}

	public void setActvalue(BigDecimal actvalue) {
		this.actvalue = actvalue;
	}
	
	@Column(name = "ZCLMADJST", precision = 17, scale = 2)
	public BigDecimal getZclmadjst() {
		return zclmadjst;
	}

	public void setZclmadjst(BigDecimal zclmadjst) {
		this.zclmadjst = zclmadjst;
	}
	
	@Column(name = "ZHLDCLMV", precision = 17, scale = 2)
	public BigDecimal getZhldclmv() {
		return zhldclmv;
	}

	public void setZhldclmv(BigDecimal zhldclmv) {
		this.zhldclmv = zhldclmv;
	}
	
	@Column(name = "ZHLDCLMA", precision = 17, scale = 2)
	public BigDecimal getZhldclma() {
		return zhldclma;
	}

	public void setZhldclma(BigDecimal zhldclma) {
		this.zhldclma = zhldclma;
	}
	
	@Column(name = "USRPRF", length = 10)
	public String getUsrprf() {
		return this.usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	
	@Column(name = "JOBNM", length = 10)
	public String getJobnm() {
		return this.jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	
	
	@Column(name = "DATIME", length = 26)
	public Timestamp getDatime() {
		return new Timestamp(this.datime.getTime());//IJTI-316
	}

	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}

	
	public String toString() {

		return "Zhlbpf info::: " + 
		chdrcoy+":"+
		chdrnum+":"+
		bnyclt+":"+
		bnypc+":"+
        validflag+":"+
        tranno+":"+
        trdt+":"+
        trtm+":"+
        usr+":"+
        actvalue+":"+
        zclmadjst+":"+
        zhldclmv+":"+
        zhldclma+":"+
        usrprf+":"+
        jobnm+":"+
        datime;

	}
	
	
	
}
