package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl56ScreenVars extends SmartVarModel{
	
	public FixedLengthStringData dataArea = new FixedLengthStringData(63);  
	public FixedLengthStringData dataFields = new FixedLengthStringData(31).isAPartOf(dataArea, 0);
	public FixedLengthStringData scrndesc = DD.scrndesc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData confirmation = DD.confirm.copy().isAPartOf(dataFields,30);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 31);
	public FixedLengthStringData confirmationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData scrndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 39);
	public FixedLengthStringData[] confirmationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] scrndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	
	/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sjl56screenWritten = new LongData(0);
	public LongData Sjl56protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}
	
	public Sjl56ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		fieldIndMap.put(confirmationOut,new String[] {"01", "02", "-01", null, null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {scrndesc, confirmation };
		screenOutFields = new BaseData[][] {scrndescOut, confirmationOut};
		screenErrFields = new BaseData[] {scrndescErr, confirmationErr};
		
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {}; 
		
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		screenRecord = Sjl56screen.class;
		protectRecord = Sjl56protect.class;
	}
}

