package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.KjohpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Kjohpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class KjohpfDAOImpl extends BaseDAOImpl<Kjohpf> implements KjohpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(KjohpfDAOImpl.class);

	@Override
	public void insertKjohpf(Kjohpf pf) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO KJOHPF(CHDRCOY,CHDRNUM,LIFE,CNTTYPE,EFFDATE,CURRCD,CLAMTYP,POLICYLOAN,REASONCD,RESNDESC,TRTM,PLINT01,APLAMT01,APLINT01,PREMSUSP,PAYMMETH,SURRVAL01,SURRVAL02,SURRVAL03,CLAMSTAT,REFUNDOPTION,VALIDFLAG,USER_T,WUEPAMT,PREMPAID,TOTAL,CLAIM,TRANNO,USRPRF,JOBNM,DATIME) ");
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = null;
		try {
				ps = getPrepareStatement(sb.toString());
			
				int i = 1;
				ps.setString(i++, pf.getChdrcoy());
				ps.setString(i++, pf.getChdrnum());
				ps.setString(i++, pf.getLife());
				ps.setString(i++, pf.getCnttype());
				ps.setInt(i++, pf.getEffdate());
				ps.setString(i++, pf.getCurrcd());
				ps.setString(i++, pf.getClaimType());
				ps.setBigDecimal(i++, pf.getPolicyloan());
				ps.setString(i++, pf.getReasoncd());
				ps.setString(i++, pf.getResndesc());
				ps.setInt(i++, pf.getTrtm());
				ps.setBigDecimal(i++, pf.getPlint01());
				ps.setBigDecimal(i++, pf.getAplamt01());
				ps.setBigDecimal(i++, pf.getAplint01());
				ps.setBigDecimal(i++, pf.getPremsusp());
				ps.setString(i++, pf.getPaymmeth());
				ps.setBigDecimal(i++, pf.getSurrval01());
				ps.setBigDecimal(i++, pf.getSurrval02());
				ps.setBigDecimal(i++, pf.getSurrval03());
				ps.setString(i++, pf.getClamstat());
				ps.setString(i++, pf.getRefundoption());
				ps.setString(i++, pf.getValidflag());
				ps.setInt(i++, pf.getUserid());
				ps.setBigDecimal(i++, pf.getWuepamt());
				ps.setBigDecimal(i++, pf.getPrempaid());
				ps.setBigDecimal(i++, pf.getTotal());
				ps.setString(i++, pf.getClaim());
				ps.setInt(i++, pf.getTranno());
				ps.setString(i++, this.getUsrprf());
				ps.setString(i++, this.getJobnm());
				ps.setTimestamp(i++, this.getDatime());
				ps.executeUpdate();	
				
		} catch (SQLException e) {
			LOGGER.error("insertKjohpf()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
			
	}
	
	@Override
	public Kjohpf getKjohpfRecord(String chdrnum, String claimNum) {
	
      	
      	StringBuilder sql = new StringBuilder("SELECT * FROM KJOHPF");
      	sql.append(" WHERE CHDRNUM=? or CLAIM=? and VALIDFLAG='1' ");
      	PreparedStatement ps=null;
      	ResultSet rs=null;
      	
      	Kjohpf kjohpf= null;
      	
      	try {
      		ps=getPrepareStatement(sql.toString());
      		ps.setString(1, chdrnum);
      		ps.setString(2, claimNum);
      		rs=ps.executeQuery();
        while (rs.next()) {
        	kjohpf= new Kjohpf();
        	
        	kjohpf.setChdrnum(rs.getString("CHDRNUM"));
        	kjohpf.setChdrcoy(rs.getString("CHDRCOY"));
        	kjohpf.setCnttype(rs.getString("CNTTYPE"));
        	kjohpf.setReasoncd(rs.getString("REASONCD"));
        	kjohpf.setResndesc(rs.getString("RESNDESC"));
        	kjohpf.setRefundoption(rs.getString("REFUNDOPTION"));
        	kjohpf.setPrempaid(rs.getBigDecimal("PREMPAID"));
        	kjohpf.setTotal(rs.getBigDecimal("TOTAL"));
        	kjohpf.setAplamt01(rs.getBigDecimal("APLAMT01"));
        	kjohpf.setAplint01(rs.getBigDecimal("APLINT01"));
        	kjohpf.setPolicyloan(rs.getBigDecimal("POLICYLOAN"));
        	kjohpf.setPlint01(rs.getBigDecimal("PLINT01"));
        	kjohpf.setWuepamt(rs.getBigDecimal("WUEPAMT"));
        	kjohpf.setSurrval01(rs.getBigDecimal("SURRVAL01"));
        	kjohpf.setSurrval02(rs.getBigDecimal("SURRVAL02"));
        	kjohpf.setSurrval03(rs.getBigDecimal("SURRVAL03"));
        	kjohpf.setPremsusp(rs.getBigDecimal("PREMSUSP"));
        	kjohpf.setClamstat(rs.getString("CLAMSTAT"));
        	kjohpf.setClaim(rs.getString("CLAIM"));
        	kjohpf.setClaimType(rs.getString("CLAMTYP"));
        	kjohpf.setEffdate(rs.getInt("EFFDATE"));
        	
            }
        } catch (SQLException e) {
        LOGGER.error("getKjohpfRecord()", e);
        throw new SQLRuntimeException(e);
    } finally {
        close(ps, rs);
    }
    return kjohpf;
	}
	

}
