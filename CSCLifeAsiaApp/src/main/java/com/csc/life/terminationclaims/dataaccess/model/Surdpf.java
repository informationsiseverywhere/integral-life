package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;

public class Surdpf {

	private String chdrnum;
	private String chdrcoy;
	private int plnsfx;
	private int tranno;
	private String termid;
	private int trdt;
	private int trtm;
	private int userT;
	private int effdate;
	private String currcd;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private String crtable;
	private String shortds;
	private String liencd;
	private String typeT;
	private BigDecimal actvalue;
	private BigDecimal emv;
	private String vrtfund;	
	private String usrprf;
	private String jobnm;
	private String datime;
	private long uniqueNumber;
	private BigDecimal otheradjst;
	private BigDecimal commclaw;

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public int getPlnsfx() {
		return plnsfx;
	}

	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public int getTrdt() {
		return trdt;
	}

	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}

	public int getTrtm() {
		return trtm;
	}

	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}

	public int getUserT() {
		return userT;
	}

	public void setUserT(int userT) {
		this.userT = userT;
	}

	public int getEffdate() {
		return effdate;
	}

	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	public String getCurrcd() {
		return currcd;
	}

	public void setCurrcd(String currcd) {
		this.currcd = currcd;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public String getShortds() {
		return shortds;
	}

	public void setShortds(String shortds) {
		this.shortds = shortds;
	}

	public String getLiencd() {
		return liencd;
	}

	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}

	public String getTypeT() {
		return typeT;
	}

	public void setTypeT(String typeT) {
		this.typeT = typeT;
	}

	public BigDecimal getActvalue() {
		return actvalue;
	}

	public void setActvalue(BigDecimal actvalue) {
		this.actvalue = actvalue;
	}

	public BigDecimal getEmv() {
		return emv;
	}

	public void setEmv(BigDecimal emv) {
		this.emv = emv;
	}

	public String getVrtfund() {
		return vrtfund;
	}

	public void setVrtfund(String vrtfund) {
		this.vrtfund = vrtfund;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public BigDecimal getOtheradjst() {
		return otheradjst;
	}

	public void setOtheradjst(BigDecimal otheradjst) {
		this.otheradjst = otheradjst;
	}

	public BigDecimal getCommclaw() {
		return commclaw;
	}

	public void setCommclaw(BigDecimal commclaw) {
		this.commclaw = commclaw;
	}

}
