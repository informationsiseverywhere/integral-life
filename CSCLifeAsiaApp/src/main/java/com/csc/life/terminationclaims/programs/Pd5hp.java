package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.screens.Sd5hpScreenVars;
import com.csc.life.terminationclaims.tablestructures.Td5hprec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pd5hp extends ScreenProgCS {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5HP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Td5hprec td5hprec = new Td5hprec();
	private String wsaaUpdateFlag = "";
	private static final int wsaaSubfileSize = 30;
	private Sd5hpScreenVars sv = ScreenProgram.getScreenVars( Sd5hpScreenVars.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);	
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Itempf itemkey = new Itempf();
	private Descpf descpf = new Descpf();
	private Itempf itempf = null;
	
	
	public Pd5hp() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5hp", AppVars.getInstance(), sv);
	}
	
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}
	
	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray){
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000(){
	sv.dataArea.set(SPACES);
	readRecord1010();
	moveToScreen1020();
 }

protected void readRecord1010() {
		
	itemkey.setItempfx(wsspsmart.itemkey.substring(0, 2));
	itemkey.setItemcoy(wsspsmart.itemkey.substring(2, 3));
	itemkey.setItemtabl(wsspsmart.itemkey.substring(3, 8));
	itemkey.setItemitem(wsspsmart.itemkey.substring(8, 16));
	
	itempf = itemDAO.getItemRecordByItemkey(itemkey);
	
	if(itempf == null) {
		syserrrec.params.set(itemkey.toString());
		fatalError600();
	}
	
	itemkey.setItemseq(itempf.getItemseq());
	
	td5hprec.td5hpRec.set(StringUtil.rawToString(itempf.getGenarea()));
	
	descpf = descDAO.getdescData(itemkey.getItempfx(), itemkey.getItemtabl(), itemkey.getItemitem(), itemkey.getItemcoy(), wsspcomn.language.toString());
	
	if(descpf == null) {
		syserrrec.params.set(itemkey.toString());
		fatalError600();
	}
	sv.longdesc.set(descpf.getLongdesc());
}

protected void moveToScreen1020() {
	
   sv.company.set(itemkey.getItemcoy());
   sv.item.set(itemkey.getItemitem());
   sv.tabl.set(itemkey.getItemtabl());
   if(isNE(td5hprec.td5hpRec, SPACES)) {
	   sv.remadays.set(td5hprec.remadays);
	   sv.rates.set(td5hprec.rates);
   }else {
   td5hprec.remaday01.set(ZERO);
   td5hprec.remaday02.set(ZERO);
   td5hprec.remaday03.set(ZERO);
   td5hprec.remaday04.set(ZERO);
   td5hprec.remaday05.set(ZERO);
   td5hprec.remaday06.set(ZERO);
   td5hprec.remaday07.set(ZERO);
   td5hprec.remaday08.set(ZERO);
   td5hprec.remaday09.set(ZERO);
   td5hprec.remaday10.set(ZERO);
   td5hprec.remaday11.set(ZERO);
   td5hprec.remaday12.set(ZERO);
   td5hprec.remaday13.set(ZERO);
   td5hprec.remaday14.set(ZERO);
   td5hprec.remaday15.set(ZERO);
   td5hprec.remaday16.set(ZERO);
   td5hprec.remaday17.set(ZERO);
   td5hprec.remaday18.set(ZERO);
   td5hprec.remaday19.set(ZERO);
   td5hprec.remaday20.set(ZERO);
   td5hprec.rate01.set(ZERO);
   td5hprec.rate02.set(ZERO);
   td5hprec.rate03.set(ZERO);
   td5hprec.rate04.set(ZERO);
   td5hprec.rate05.set(ZERO);
   td5hprec.rate06.set(ZERO);
   td5hprec.rate07.set(ZERO);
   td5hprec.rate08.set(ZERO);
   td5hprec.rate09.set(ZERO);
   td5hprec.rate10.set(ZERO);
   td5hprec.rate11.set(ZERO);
   td5hprec.rate12.set(ZERO);
   td5hprec.rate13.set(ZERO);
   td5hprec.rate14.set(ZERO);
   td5hprec.rate15.set(ZERO);
   td5hprec.rate16.set(ZERO);
   td5hprec.rate17.set(ZERO);
   td5hprec.rate18.set(ZERO);
   td5hprec.rate19.set(ZERO);
   td5hprec.rate20.set(ZERO);
   }
}




protected void preScreenEdit(){
	/*PRE-START*/
	/*    This section will handle any action required on the screen **/
	/*    before the screen is painted.                              **/
	if (isEQ(wsspcomn.flag,"I")) {
		scrnparams.function.set(varcom.prot);
	}
	return ;
	/*PRE-EXIT*/
}
/**
* <pre>
*     RETRIEVE SCREEN FIELDS AND EDIT
* </pre>
*/
protected void screenEdit2000(){
			screenIo2010();
			exit2090();
		}

protected void screenIo2010(){
	wsspcomn.edterror.set(varcom.oK);
	/*VALIDATE*/
	if (isEQ(wsspcomn.flag,"I")) {
		return ;
	}
	/*OTHER*/
}

protected void exit2090(){
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}


protected void checkChanges3100()
{
	/*CHECK*/
    //IFSU-2957
	if (isNE(sv.remadays,td5hprec.remadays) || isNE(sv.rates, td5hprec.rates)) {
		td5hprec.remadays.set(sv.remadays);
		td5hprec.rates.set(sv.rates);
		wsaaUpdateFlag = "Y";
	}
	/*EXIT*/
}

protected void update3000(){
	
	if (isEQ(wsspcomn.flag,"I")) {
		return ;
	}
	/*CHECK-CHANGES*/
	wsaaUpdateFlag = "N";
	if (isEQ(wsspcomn.flag,"C")) {
		wsaaUpdateFlag = "Y";
	}
	checkChanges3100();
	
	if(wsaaUpdateFlag.equals("Y")) {
		itemkey.setGenarea(td5hprec.td5hpRec.toString().getBytes());
		itemDAO.updateSmartTableItem(itemkey);
	}
	
}

protected void whereNext4000(){
	/*NEXT-PROGRAM*/
	wsspcomn.programPtr.add(1);
	/*EXIT*/
	
}

}
