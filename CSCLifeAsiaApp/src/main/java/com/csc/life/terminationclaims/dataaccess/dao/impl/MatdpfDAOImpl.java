package com.csc.life.terminationclaims.dataaccess.dao.impl;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.MatdpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Matdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MatdpfDAOImpl extends BaseDAOImpl<Matdpf> implements MatdpfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MatdpfDAOImpl.class);
																																																																																																				
	@Override
	public boolean insertMatdpf(List<Matdpf> matdpfList) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO MATDPF (CHDRNUM, CHDRCOY, PLNSFX, TRANNO, TERMID, TRDT, TRTM, USER_T, EFFDATE, CURRCD, LIFE, JLIFE, COVERAGE, RIDER, CRTABLE, SHORTDS, LIENCD, TYPE_T, ACTVALUE, EMV, VRTFUND, USRPRF, JOBNM ,DATIME) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Matdpf matd : matdpfList) {
				ps.setString(1, matd.getChdrnum());
				ps.setString(2, matd.getChdrcoy());
				ps.setInt(3, matd.getPlnsfx());
				ps.setInt(4, matd.getTranno());
				ps.setString(5, matd.getTermid());
				ps.setInt(6, matd.getTrdt());
				ps.setInt(7, matd.getTrtm());
				ps.setInt(8, matd.getUser_t());
				ps.setInt(9, matd.getEffdate());
				ps.setString(10, matd.getCurrcd());
				ps.setString(11, matd.getLife());
				ps.setString(12, matd.getJlife());
				ps.setString(13, matd.getCoverage());
				ps.setString(14, matd.getRider());
				ps.setString(15, matd.getCrtable());
				ps.setString(16, matd.getShortds());
				ps.setString(17, matd.getLiencd());
				ps.setString(18, matd.getType_t());
				ps.setBigDecimal(19, matd.getActvalue());
				ps.setBigDecimal(20, matd.getEmv());
				ps.setString(21, matd.getVrtfund());
				ps.setString(22, this.getUsrprf());
				ps.setString(23, this.getJobnm());
				ps.setTimestamp(24, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertMatdpf()", e);//IJTI-1561
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return isInsertSuccessful;
	}
	public List<Matdpf> getMatdRecord(Integer chdrcoy,String chdrnum, Integer tranno){
		StringBuilder sqlMatdSelect1 = new StringBuilder(
                "SELECT CHDRNUM,CHDRCOY,PLNSFX,TRANNO,TERMID,TRDT,TRTM,USER_T,EFFDATE,CURRCD,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,SHORTDS,LIENCD,TYPE_T,ACTVALUE,EMV,VRTFUND ");
        sqlMatdSelect1.append(" FROM Matdpf WHERE CHDRCOY = ? AND CHDRNUM = ? AND TRANNO = ?");
    
        sqlMatdSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,UNIQUE_NUMBER DESC ");
     
   	
        PreparedStatement ps = getPrepareStatement(sqlMatdSelect1.toString());
   	 	ResultSet sqlmatdpf1rs = null;
        List<Matdpf> matdlist = null;
    	 try {
 			 ps.setInt(1,chdrcoy);
 			 ps.setString(2, chdrnum);
 			 ps.setInt(3, tranno);
 			 
 			
 			 sqlmatdpf1rs = executeQuery(ps);
    		 matdlist = new ArrayList<Matdpf>();
			 while(sqlmatdpf1rs.next()){
                Matdpf matdpf = new Matdpf();
                matdpf.setChdrnum(sqlmatdpf1rs.getString(1));
                matdpf.setChdrcoy(sqlmatdpf1rs.getString(2));
                matdpf.setPlnsfx(sqlmatdpf1rs.getInt(3));
                matdpf.setTranno(sqlmatdpf1rs.getInt(4));
                matdpf.setTermid(sqlmatdpf1rs.getString(5));
                matdpf.setTrdt(sqlmatdpf1rs.getInt(6));
                matdpf.setTrtm(sqlmatdpf1rs.getInt(7));
                matdpf.setUser_t(sqlmatdpf1rs.getInt(8));
                matdpf.setEffdate(sqlmatdpf1rs.getInt(9));
                matdpf.setCurrcd(sqlmatdpf1rs.getString(10));
                matdpf.setLife(sqlmatdpf1rs.getString(11));
                matdpf.setJlife(sqlmatdpf1rs.getString(12));
                matdpf.setCoverage(sqlmatdpf1rs.getString(13));
                matdpf.setRider(sqlmatdpf1rs.getString(14));
                matdpf.setCrtable(sqlmatdpf1rs.getString(15));
                matdpf.setShortds(sqlmatdpf1rs.getString(16));
                matdpf.setLiencd(sqlmatdpf1rs.getString(17));
                matdpf.setType_t(sqlmatdpf1rs.getString(18));
                matdpf.setActvalue(sqlmatdpf1rs.getBigDecimal(19));
                matdpf.setEmv(sqlmatdpf1rs.getBigDecimal(20));
                matdpf.setVrtfund(sqlmatdpf1rs.getString(21));
                matdlist.add(matdpf);
				}
			} catch (SQLException e) {
	            LOGGER.error("getMatdRecord()", e);//IJTI-1561
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, null);
	        }
	    	 return matdlist;
	    	 }

	 public List<Matdpf> getmatdclmListRecord(String chdrcoy, String chdrnum, int tranno, int plansuffix) {
		StringBuilder sqlMatdSelect1 = new StringBuilder(
                "SELECT CHDRNUM,CHDRCOY,PLNSFX,TRANNO,TERMID,TRDT,TRTM,USER_T,EFFDATE,CURRCD,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,SHORTDS,LIENCD,TYPE_T,ACTVALUE,EMV,VRTFUND ");
        sqlMatdSelect1.append(" FROM Matdpf WHERE CHDRCOY = ? AND CHDRNUM = ? ");
        sqlMatdSelect1.append("  AND TRANNO = ? AND PLNSFX = ? ");
    
        sqlMatdSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,UNIQUE_NUMBER DESC ");
     
   	

        PreparedStatement ps = getPrepareStatement(sqlMatdSelect1.toString());
   	 	ResultSet sqlmatdpf1rs = null;
        List<Matdpf> matdclmList = null;
    	 try {
 			 ps.setInt(1,Integer.parseInt(chdrcoy));
 			 ps.setString(2, chdrnum);
 			 ps.setInt(3, tranno);
 			ps.setInt(4, plansuffix);
 			
 			 sqlmatdpf1rs = executeQuery(ps);
 			matdclmList = new ArrayList<Matdpf>();
			 while(sqlmatdpf1rs.next()){
                Matdpf matdpf = new Matdpf();
                matdpf.setChdrnum(sqlmatdpf1rs.getString(1));
                matdpf.setChdrcoy(sqlmatdpf1rs.getString(2));
                matdpf.setPlnsfx(sqlmatdpf1rs.getInt(3));
                matdpf.setTranno(sqlmatdpf1rs.getInt(4));
                matdpf.setTermid(sqlmatdpf1rs.getString(5));
                matdpf.setTrdt(sqlmatdpf1rs.getInt(6));
                matdpf.setTrtm(sqlmatdpf1rs.getInt(7));
                matdpf.setUser_t(sqlmatdpf1rs.getInt(8));
                matdpf.setEffdate(sqlmatdpf1rs.getInt(9));
                matdpf.setCurrcd(sqlmatdpf1rs.getString(10));
                matdpf.setLife(sqlmatdpf1rs.getString(11));
                matdpf.setJlife(sqlmatdpf1rs.getString(12));
                matdpf.setCoverage(sqlmatdpf1rs.getString(13));
                matdpf.setRider(sqlmatdpf1rs.getString(14));
                matdpf.setCrtable(sqlmatdpf1rs.getString(15));
                matdpf.setShortds(sqlmatdpf1rs.getString(16));
                matdpf.setLiencd(sqlmatdpf1rs.getString(17));
                matdpf.setType_t(sqlmatdpf1rs.getString(18));
                matdpf.setActvalue(sqlmatdpf1rs.getBigDecimal(19));
                matdpf.setEmv(sqlmatdpf1rs.getBigDecimal(20));
                matdpf.setVrtfund(sqlmatdpf1rs.getString(21));
                matdclmList.add(matdpf);
				}
			} catch (SQLException e) {
	            LOGGER.error("getmatdclmListRecord()", e);//IJTI-1561
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, null);
	        }
	    	 return matdclmList;
	    	 }

	public List<Matdpf> getmatdClmRecord(String chdrcoy, String chdrnum) {
		StringBuilder sqlMatdSelect1 = new StringBuilder(
				"SELECT CHDRNUM,CHDRCOY,PLNSFX,TRANNO,TERMID,TRDT,TRTM,USER_T,EFFDATE,CURRCD,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,SHORTDS,LIENCD,TYPE_T,ACTVALUE,EMV,VRTFUND ");
		sqlMatdSelect1.append(" FROM MATDCLM WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlMatdSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,UNIQUE_NUMBER DESC ");
		PreparedStatement ps = getPrepareStatement(sqlMatdSelect1.toString());
		ResultSet sqlmatdpf1rs = null;
		List<Matdpf> matdclmList = null;
		try {
			ps.setInt(1,Integer.parseInt(chdrcoy));
			ps.setString(2, chdrnum);
			sqlmatdpf1rs = executeQuery(ps);
			matdclmList = new ArrayList<Matdpf>();
			while(sqlmatdpf1rs.next()){
				Matdpf matdpf = new Matdpf();
				matdpf.setChdrnum(sqlmatdpf1rs.getString(1));
				matdpf.setChdrcoy(sqlmatdpf1rs.getString(2));
				matdpf.setPlnsfx(sqlmatdpf1rs.getInt(3));
				matdpf.setTranno(sqlmatdpf1rs.getInt(4));
				matdpf.setTermid(sqlmatdpf1rs.getString(5));
				matdpf.setTrdt(sqlmatdpf1rs.getInt(6));
				matdpf.setTrtm(sqlmatdpf1rs.getInt(7));
				matdpf.setUser_t(sqlmatdpf1rs.getInt(8));
				matdpf.setEffdate(sqlmatdpf1rs.getInt(9));
				matdpf.setCurrcd(sqlmatdpf1rs.getString(10));
				matdpf.setLife(sqlmatdpf1rs.getString(11));
				matdpf.setJlife(sqlmatdpf1rs.getString(12));
				matdpf.setCoverage(sqlmatdpf1rs.getString(13));
				matdpf.setRider(sqlmatdpf1rs.getString(14));
				matdpf.setCrtable(sqlmatdpf1rs.getString(15));
				matdpf.setShortds(sqlmatdpf1rs.getString(16));
				matdpf.setLiencd(sqlmatdpf1rs.getString(17));
				matdpf.setType_t(sqlmatdpf1rs.getString(18));
				matdpf.setActvalue(sqlmatdpf1rs.getBigDecimal(19));
				matdpf.setEmv(sqlmatdpf1rs.getBigDecimal(20));
				matdpf.setVrtfund(sqlmatdpf1rs.getString(21));
				matdclmList.add(matdpf);
			}
		} catch (SQLException e) {
			LOGGER.error("getmatdclmListRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		return matdclmList;
	}
	public boolean insertMatdpfRecord(Matdpf matdpf) {
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO MATDPF (CHDRNUM, CHDRCOY, PLNSFX, TRANNO, TERMID, TRDT, TRTM, USER_T, EFFDATE, CURRCD, LIFE, JLIFE, COVERAGE, RIDER, CRTABLE, SHORTDS, LIENCD, TYPE_T, ACTVALUE, EMV, VRTFUND, USRPRF, JOBNM ,DATIME) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, matdpf.getChdrnum());
			ps.setString(2, matdpf.getChdrcoy());
			ps.setInt(3, matdpf.getPlnsfx());
			ps.setInt(4, matdpf.getTranno());
			ps.setString(5, matdpf.getTermid());
			ps.setInt(6, matdpf.getTrdt());
			ps.setInt(7, matdpf.getTrtm());
			ps.setInt(8, matdpf.getUser_t());
			ps.setInt(9, matdpf.getEffdate());
			ps.setString(10, matdpf.getCurrcd());
			ps.setString(11, matdpf.getLife());
			ps.setString(12, matdpf.getJlife());
			ps.setString(13, matdpf.getCoverage());
			ps.setString(14, matdpf.getRider());
			ps.setString(15, matdpf.getCrtable());
			ps.setString(16, matdpf.getShortds());
			ps.setString(17, matdpf.getLiencd());
			ps.setString(18, matdpf.getType_t());
			ps.setBigDecimal(19, matdpf.getActvalue());
			ps.setBigDecimal(20, matdpf.getEmv());
			ps.setString(21, matdpf.getVrtfund());
			ps.setString(22, this.getUsrprf());
			ps.setString(23, this.getJobnm());
			ps.setTimestamp(24, new Timestamp(System.currentTimeMillis()));
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("insertMatdpf()", e);//IJTI-1561
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return isInsertSuccessful;
	}
}
