package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.csc.life.terminationclaims.dataaccess.dao.PtsdpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Ptsdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class PtsdpfDAOImpl extends BaseDAOImpl<Ptsdpf> implements PtsdpfDAO {

	@Override
	public List<Ptsdpf> serachPtsdRecord(Ptsdpf ptsdObj) {
		StringBuilder sqlPtsdSelect1 = new StringBuilder(
                "SELECT CHDRCOY, CHDRNUM, TRANNO, LIFE, JLIFE, EFFDATE, CURRCD, PLNSFX, COVERAGE, RIDER, CRTABLE, VRTFUND, EMV, ACTVALUE, TYPE_T, PERCREQD");
        sqlPtsdSelect1
                .append(" FROM VM1DTA.PTSDPF WHERE CHDRCOY= "+ptsdObj.getChdrcoy()+" AND CHDRNUM = ?");
        sqlPtsdSelect1
        .append(" AND TRANNO >= "+ptsdObj.getTranno()+" ");
        // Start: ILIFE-4349
        if(ptsdObj.getPlanSuffix() != null){
        	sqlPtsdSelect1.append(" AND PLNSFX= " + ptsdObj.getPlanSuffix());
        }
        if(ptsdObj.getLife() != null){
        	sqlPtsdSelect1.append(" AND LIFE= " + ptsdObj.getLife());
        }
        if(ptsdObj.getCoverage() != null){
        	sqlPtsdSelect1.append(" AND COVERAGE= " + ptsdObj.getCoverage());
        }
        if(ptsdObj.getRider() != null){
        	sqlPtsdSelect1.append(" AND RIDER= " + ptsdObj.getRider());
        }
        // Ends: ILIFE-4349
        sqlPtsdSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, TRANNO ASC, UNIQUE_NUMBER DESC ");
        PreparedStatement psPtsdSelect = getPrepareStatement(sqlPtsdSelect1.toString());
        ResultSet sqlPtsdpf1rs = null;
        List<Ptsdpf> ptsdpfList = new ArrayList<Ptsdpf>();
        try {
        	psPtsdSelect.setString(1, ptsdObj.getChdrnum());
        	sqlPtsdpf1rs = executeQuery(psPtsdSelect);
             while (sqlPtsdpf1rs.next()) {
            	 Ptsdpf ptsdpf=new Ptsdpf();
            	 ptsdpf.setChdrcoy(sqlPtsdpf1rs.getString(1));
            	 ptsdpf.setChdrnum(sqlPtsdpf1rs.getString(2));
            	 ptsdpf.setTranno(sqlPtsdpf1rs.getInt(3));
            	 ptsdpf.setLife(sqlPtsdpf1rs.getString(4));
            	 ptsdpf.setJlife(sqlPtsdpf1rs.getString(5));
            	 ptsdpf.setEffdate(sqlPtsdpf1rs.getLong(6));
            	 ptsdpf.setCurrcd(sqlPtsdpf1rs.getString(7));
            	 ptsdpf.setPlanSuffix(sqlPtsdpf1rs.getBigDecimal(8));
            	 ptsdpf.setCoverage(sqlPtsdpf1rs.getString(9));
            	 ptsdpf.setRider(sqlPtsdpf1rs.getString(10));
            	 ptsdpf.setCrtable(sqlPtsdpf1rs.getString(11));
            	 ptsdpf.setVirtualFund(sqlPtsdpf1rs.getString(12));
            	 ptsdpf.setEstMatValue(sqlPtsdpf1rs.getBigDecimal(13));
            	 ptsdpf.setActvalue(sqlPtsdpf1rs.getBigDecimal(14));
            	 ptsdpf.setFieldType(sqlPtsdpf1rs.getString(15));
            	 ptsdpf.setPercreqd(sqlPtsdpf1rs.getBigDecimal(16));
            	 ptsdpfList.add(ptsdpf);
             }
        }catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(psPtsdSelect, sqlPtsdpf1rs);
        }
        return ptsdpfList;
	}
	
public void insertPtsdpfRecord(List<Ptsdpf> ptsdpflist) {
		
		StringBuilder sb = new StringBuilder("insert into vm1dta.ptsdpf(CHDRCOY,CHDRNUM,TRANNO,LIFE,JLIFE,CURRCD,ACTVALUE,PERCREQD,PLNSFX,TYPE_T,COVERAGE,RIDER,CRTABLE,SHORTDS,LIENCD,EMV,VRTFUND,USRPRF,JOBNM,DATIME)");
		sb.append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());
			for(Ptsdpf ptsdpf : ptsdpflist)	{			
			ps.setString(1, ptsdpf.getChdrcoy());
			ps.setString(2, ptsdpf.getChdrnum());
			ps.setInt(3, ptsdpf.getTranno());
			ps.setString(4, ptsdpf.getLife());
			ps.setString(5, ptsdpf.getJlife());
			ps.setString(6, ptsdpf.getCurrcd());
			ps.setBigDecimal(7, ptsdpf.getActvalue());
			ps.setBigDecimal(8, ptsdpf.getPercreqd());
			ps.setBigDecimal(9, ptsdpf.getPlanSuffix());
			ps.setString(10, ptsdpf.getFieldType());
			ps.setString(11, ptsdpf.getCoverage());
			ps.setString(12, ptsdpf.getRider());
			ps.setString(13, ptsdpf.getCrtable());
			ps.setString(14, ptsdpf.getShortds());
			ps.setString(15, ptsdpf.getLiencd());
			
			if(ptsdpf.getEstMatValue()!=null) {//ILIFE-7712 
				ps.setBigDecimal(16,ptsdpf.getEstMatValue().setScale(2, BigDecimal.ROUND_HALF_UP));//ILIFE-7712
				} 
			else{
				ptsdpf.setEstMatValue(BigDecimal.ZERO);
				ps.setBigDecimal(16,ptsdpf.getEstMatValue());				
			}
			ps.setString(17, ptsdpf.getVirtualFund());
			ps.setString(18, getUsrprf());
			ps.setString(19, getJobnm());
			ps.setTimestamp(20, new Timestamp(System.currentTimeMillis()));
			ps.addBatch();
			}	
            ps.executeBatch();
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}
public Ptsdpf getPtsdpfByKey(Ptsdpf ptsdObj) {
	StringBuilder sb = new StringBuilder("select CHDRCOY,CHDRNUM,TRANNO,LIFE,JLIFE,EFFDATE,CURRCD,ACTVALUE,PERCREQD,PLNSFX,TYPE_T,COVERAGE,RIDER,CRTABLE,SHORTDS,LIENCD,EMV,VRTFUND FROM VM1DTA.PTSDPF");
	sb.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=? AND PLNSFX=? AND LIFE=? AND COVERAGE=? AND RIDER=?");
	
	PreparedStatement ps = null;
	ResultSet rs = null;
	Ptsdpf ptsdpf = null;
	
	if(ptsdObj == null) {
		return ptsdpf;
	}
	
	try{
		ps = getPrepareStatement(sb.toString());
		ps.setString(1, ptsdObj.getChdrcoy());
		ps.setString(2, ptsdObj.getChdrnum());
		ps.setInt(3, ptsdObj.getTranno());
		ps.setBigDecimal(4, ptsdObj.getPlanSuffix());
		ps.setString(5, ptsdObj.getLife());
		ps.setString(6, ptsdObj.getCoverage());
		ps.setString(7, ptsdObj.getRider());
		rs = ps.executeQuery();
		if(rs.next()) {
			ptsdpf = new Ptsdpf();
			ptsdpf.setChdrcoy(rs.getString("CHDRCOY"));
        	 ptsdpf.setChdrnum(rs.getString("CHDRNUM"));
        	 ptsdpf.setTranno(rs.getInt("TRANNO"));
        	 ptsdpf.setLife(rs.getString("LIFE"));
        	 ptsdpf.setJlife(rs.getString("JLIFE"));
        	 ptsdpf.setEffdate(rs.getLong("EFFDATE"));
        	 ptsdpf.setCurrcd(rs.getString("CURRCD"));
        	 ptsdpf.setActvalue(rs.getBigDecimal("ACTVALUE"));
        	 ptsdpf.setPercreqd(rs.getBigDecimal("PERCREQD"));
        	 ptsdpf.setPlanSuffix(rs.getBigDecimal("PLNSFX"));
        	 ptsdpf.setFieldType(rs.getString("TYPE_T"));
        	 ptsdpf.setCoverage(rs.getString("COVERAGE"));
        	 ptsdpf.setRider(rs.getString("RIDER"));
        	 ptsdpf.setCrtable(rs.getString("CRTABLE"));
        	 ptsdpf.setShortds(rs.getString("SHORTDS"));
        	 ptsdpf.setLiencd(rs.getString("LIENCD"));
        	 ptsdpf.setEstMatValue(rs.getBigDecimal("EMV"));
        	 ptsdpf.setVirtualFund(rs.getString("VRTFUND"));
       	
		}
	}catch (SQLException e) {
		throw new SQLRuntimeException(e);
	} finally {
		close(ps, rs);			
	}
	
	return ptsdpf;
}

public void updatePtsdpfForEMTByNo(long uniqueNumber, BigDecimal emv) {
	
	StringBuilder sb = new StringBuilder("update VM1DTA.PTSDPF set EMV=? WHERE UNIQUE_NUMBER=?");
	PreparedStatement ps = null;
	 try {
		 ps = getPrepareStatement(sb.toString());
		 ps.setBigDecimal(1, emv);
		 ps.setLong(2, uniqueNumber);
		 ps.executeUpdate();
	 }catch (SQLException e) {
			throw new SQLRuntimeException(e);
	} finally {
			close(ps, null);			
	}
		
	
}

}
