package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH595
 * @version 1.0 generated on 30/08/09 07:05
 * @author Quipoz
 */
public class Sh595ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(475);
	public FixedLengthStringData dataFields = new FixedLengthStringData(251).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,8);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,38);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData ownersel = DD.ownersel.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData premStatDesc = DD.pstatdsc.copy().isAPartOf(dataFields,103);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,113);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,121);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData statdsc = DD.statdsc.copy().isAPartOf(dataFields,175);
	public FixedLengthStringData znfoptds = new FixedLengthStringData(60).isAPartOf(dataFields, 185);
	public FixedLengthStringData[] znfoptd = FLSArrayPartOfStructure(2, 30, znfoptds, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(znfoptds, 0, FILLER_REDEFINE);
	public FixedLengthStringData znfoptd01 = DD.znfoptd.copy().isAPartOf(filler,0);
	public FixedLengthStringData znfoptd02 = DD.znfoptd.copy().isAPartOf(filler,30);
	public FixedLengthStringData znfopts = new FixedLengthStringData(6).isAPartOf(dataFields, 245);
	public FixedLengthStringData[] znfopt = FLSArrayPartOfStructure(2, 3, znfopts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(znfopts, 0, FILLER_REDEFINE);
	public FixedLengthStringData znfopt01 = DD.znfopt.copy().isAPartOf(filler1,0);
	public FixedLengthStringData znfopt02 = DD.znfopt.copy().isAPartOf(filler1,3);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 251);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ownerselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData pstatdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData statdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData znfoptdsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] znfoptdErr = FLSArrayPartOfStructure(2, 4, znfoptdsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(znfoptdsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData znfoptd01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData znfoptd02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData znfoptsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData[] znfoptErr = FLSArrayPartOfStructure(2, 4, znfoptsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(znfoptsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData znfopt01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData znfopt02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 307);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ownerselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] pstatdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] statdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData znfoptdsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] znfoptdOut = FLSArrayPartOfStructure(2, 12, znfoptdsOut, 0);
	public FixedLengthStringData[][] znfoptdO = FLSDArrayPartOfArrayStructure(12, 1, znfoptdOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(znfoptdsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] znfoptd01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] znfoptd02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData znfoptsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 144);
	public FixedLengthStringData[] znfoptOut = FLSArrayPartOfStructure(2, 12, znfoptsOut, 0);
	public FixedLengthStringData[][] znfoptO = FLSDArrayPartOfArrayStructure(12, 1, znfoptOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(znfoptsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] znfopt01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] znfopt02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sh595screenWritten = new LongData(0);
	public LongData Sh595protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh595ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(znfopt02Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(resndescOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = new BaseData[] {chdrnum, ctypedes, statdsc, premStatDesc, ownersel, ownername, occdate, ptdate, znfopt01, znfoptd01, znfopt02, znfoptd02, reasoncd, resndesc};
		screenOutFields = new BaseData[][] {chdrnumOut, ctypedesOut, statdscOut, pstatdscOut, ownerselOut, ownernameOut, occdateOut, ptdateOut, znfopt01Out, znfoptd01Out, znfopt02Out, znfoptd02Out, reasoncdOut, resndescOut};
		screenErrFields = new BaseData[] {chdrnumErr, ctypedesErr, statdscErr, pstatdscErr, ownerselErr, ownernameErr, occdateErr, ptdateErr, znfopt01Err, znfoptd01Err, znfopt02Err, znfoptd02Err, reasoncdErr, resndescErr};
		screenDateFields = new BaseData[] {occdate, ptdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh595screen.class;
		protectRecord = Sh595protect.class;
	}

}
