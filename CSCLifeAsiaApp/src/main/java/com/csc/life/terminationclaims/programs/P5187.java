/*
 * File: P5187.java
 * Date: 30 August 2009 0:18:05
 * Author: Quipoz Limited
 *
 * Class transformed from P5187.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.life.terminationclaims.dataaccess.dao.ClnnpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.life.terminationclaims.dataaccess.dao.InvspfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.tablestructures.T6617rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.FluprgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpenqTableDAM;
import com.csc.life.terminationclaims.screens.S5187ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.life.terminationclaims.tablestructures.T6690rec;
import com.csc.life.terminationclaims.tablestructures.T6692rec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.life.terminationclaims.tablestructures.T6696rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
*
*  P5187 - Regular Benefit Claims.
*  -------------------------------
*
*  Overview.
*  ---------
*
*  This program will perform some of the maintenance  functions
*  for  Regular Benefit Claims. It will be invoked initially by
*  the Regular Payments sub-menu and may also  be  called  from
*  within Contract Enquiry.
*
*  The following functions will be catered for:
*
*       . Register
*       . Adjust
*       . Enquire
*
*  The  Register  function  corresponds  to  Create and will be
*  carried out when WSSP-FLAG is 'C'.
*
*  The Adjust  function  corresponds  to  Modify  and  will  be
*  performed when WSSP-FLAG is 'M'.
*
*  The  Enquire  function will be carried out when WSSP-FLAG is
*  'I'.
*
*  The actual  payments  themselves  are  created  by  a  batch
*  payments  job  which will only operate on claims that are in
*  an 'Approved' state and  are  due  for  payment.  The  Batch
*  Payments  job  will effect any currency conversions that are
*  necessary at the time the payments are made. Claims will  be
*  registered  in  the  currency  of the contract to which they
*  attach but a separate Payment Currency may be  specified  at
*  the time the claim is approved.
*
*  The  Sub-menu will control which actions are allowed against
*  the claim details.
*
*  The Registration and Adjust functions may allow the user  to
*  change  the  frequency  of  the  claim  as  defined  on  the
*  component record.  This  will  be  dependant  on  the  Claim
*  Detail  Rules  on  table  T6696. If the frequency is altered
*  then this will of course affect the  amount  of  the  claim.
*  The   program  will  take  this  into  account  when  it  is
*  comparing  the  requested  amount  of  claim   against   the
*  originally  defined  amount  on  COVR  and  also against the
*  total so far of all existing claims.
*
*  The  Total  Sum  Assured  will  be  displayed  in   contract
*  currency  and  the  claim  details  will be captured in this
*  currency.
*
*  Initialise.
*  -----------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  Initialise  all  relevant  variables  and prepare the screen
*  for display.
*
*  Set a 'First Time' flag for use in the update section.
*
*  Set the variable heading of the screen to the long description
*  of the Transaction Code by reading table T1688.
*
*  Perform a RETRV on the CHDRRGP and REGP files.
*
*  Use CHDRRGP to display the heading  details  on  the  screen
*  using  T5688  for  the  contract  type  description  and the
*  Client  file  for  the  relevant  client  names.  The  short
*  descriptions  for  the  contracts  risk  and  premium status
*  codes should be  obtained  from  T3623,  (Risk  Status)  and
*  T3588, (Premium Status).
*
*  Set  the  Contract Currency in the heading from the Contract
*  Header.
*
*  For Registration set the Payment Currency  to  the  Contract
*  Currency.
*
*  Read  all  the  COVR  records for this component on the policy
*  accumulating all of the Sum  Assured  amounts.  All  of  the
*  COVR  records across the plan will share the same frequency.
*  Display this amount in the Total Sum Assured field.
*
*  Display the REGP details on the screen  looking  up  all  of
*  the descriptions from DESC where appropriate.
*
*  For  Adjustments  or  Enquiries use the claim status to read
*  table T5400 to obtain the short description.  Also  use  the
*  Payment  Type  to  read  table  T6691  to obtain the Regular
*  Payment Type short description. For  Registration  where  no
*  claim  status  or payment type has yet been established read
*  T6690 with the Component Code as key, pick  up  the  Default
*  Regular  Payment  Type  and use this to access T6691 for the
*  short description. Remember to replace  this  later  if  the
*  entered Reason Code has a different Payment Type.
*
*  Read   table   T6693   with   the   current  Payment  Status
*  concatenated with the CRTABLE of the  associated  component.
*  Locate  the  Transaction  Code  of the transaction currently
*  being processed and select the  corresponding  Next  Payment
*  Status.  For  Registration the payment status will be set to
*  '**' for the read of the table. Also  for  Registration  use
*  the  Next Payment Status immediately to access T5400 for the
*  short description.
*
*  If there are Follow Ups currently awaiting completion set  a
*  '+'   in  the  Follow  Ups  indicator  field.  This  can  be
*  determined  by  reading  FLUPRGP  with  a  key  of  CHDRCOY,
*  CHDRNUM,  CLAMNUM a FUPNO of zero and a function of BEGN. If
*  a record is found that matches on Company,  Contract  Number
*  and Claim Number then there are Follow Ups for the payment.
*  If in Create mode, a '+' in the Follow Ups indicator field
*  indicates that default Follow Ups exist.
*
*  If Annuity Details are found, set a '+' in the Annuity Details
*  indicator field.
*
*  If  the  bank details on REGP are non-blank set a '+' in the
*  Bank Details indicator field.
*
*  For Register, the Regular Payment Sequence Number,  RGPYNUM,
*  should  be  set  to  1  greater  than  the  previous highest
*  sequence number for this contract. So read  REGPENQ  with  a
*  function  of  BEGN  and  a Sequence Number set to all 9's to
*  find the most recently  added  Regular  Payment  record  and
*  increment  that Sequence Number by 1 to use on this file. If
*  End of File  is  received  or  the  returned  record  has  a
*  different  Company  or Contract number then set the Sequence
*  Number to 1. Display the value on the screen.
*
*  Read table T5671  with  the  key  of  Transaction  Code  and
*  Component  Type.  Match the current program id. (WSAA-PROG),
*  against the programs and pick  up  the  matching  Validation
*  Item  Key.  Concatenate this with the Contract Currency Code
*  from CHDRRGP and read T5606.
*
*  For Modify and Enquire display the frequency from  the  REGP
*  file.
*
*  For  Register display the default Frequency from T5606. Also
*  default the Claim Currency to  the  Contract  Currency  Code
*  from CHDRRGP.
*  The calculation of the Sum Assured should allow a frequency of
*  00 (for one-off payments) therefore a 1 should be used for the
*  computation.
*
*  Display and Validation. (2000 Section).
*  ---------------------------------------
*
*  If   returning  from  a  program  further  down  the  stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  If WSSP-FLAG = 'I' protect the screen  apart  from  the  two
*  screen switching indicators.
*
*  If  there  is  a  cancellation date display it on the screen
*  and set off the indicators so that the date and its  literal
*  appear on the screen.
*
*  Converse with the screen using the I/O module.
*
*  If 'KILL' has been pressed skip this section.
*
*  If 'CALC' has been pressed set WSSP-EDTERROR to 'Y'.
*
*  .  Reason  Code.  This  is mandatory, check that it has been
*  entered. Read T6692 to obtain the Associated  Payment  Type.
*  If  entered  it  will  be validated by the screen I/O module
*  but the long description will only be present  if  the  user
*  has  employed windowing to select the entry. Therefore if it
*  is blank you should read the DESC file to  obtain  the  long
*  description.  If  the associated payment type from the Extra
*  Data screen differs from  that  previously  used  then  also
*  read   T6691  to  obtain  the  Regular  Payment  Type  short
*  description for display.
*
*  . Evidence. This field is free format and optional.
*
*  . Payment Method. This is mandatory, check that it has  been
*  entered.  If  entered it will be validated by the screen I/O
*  module but the short description will  only  be  present  if
*  the  user  has  employed  windowing  to  make the selection.
*  Therefore if it is blank you should read the  DESC  file  to
*  obtain the short description.
*
*  .  Payment Currency. This is validated by the I/O module but
*  the  program  must  read  the  DESC  file  to   obtain   the
*  decription.  If the user leaves it blank then re-set it from
*  the Contract Currency.
*
*  . Frequency Code. The user may only override  the  Frequency
*  if  the rules on T6696 allow. If Override is not allowed and
*  the user has changed it then set it back to the  value  from
*  T5606  and  give  a message indicating what has happened. If
*  Override is allowed then check that it has been entered.  If
*  entered  it  will  be validated by the screen I/O module but
*  the short description will only be present if the  user  has
*  employed  windowing  to make the selection. Therefore always
*  read the DESC file to obtain the short description.
*
*  If the  Frequency  Code  has  changed  then  the  Total  Sum
*  Assured  field  must  also be re-calculated to bring it into
*  line with the new frequency.
*  The calculation of the Sum Assured should allow a frequency of
*  00 (for one-off payments) therefore a 1 should be used for the
*  computation.
*
*  .  Payee  Client.  Read  table  T6694  with  the  Method  of
*  Payment.  The  'TO'  Sub Account is obtained from here along
*  with rules determining  what  extra  details  are  required.
*  Check   the   Bank,  Payee  and  Contract  details  required
*  indicators. These are not mutually exclusive so all must  be
*  checked.  If  the  'Payee  Details'  is  'Y'  then the Payee
*  client field is mandatory otherwise it must not be  entered.
*  If  it  is  mandatory check that the entry is a valid client
*  number. If found format the name for display.
*
*  . Percentage.  Obtain  the  default  percentage  from  table
*  T6696.  If  the user has entered nothing in the amount field
*  then  display  the  default   percentage   here.   Otherwise
*  calculate  this  percentage  value  as  the  Amount  To  Pay
*  expressed as a percentage of the Sum Assured on the screen.
*
*  .  Destination  Key.  If  the  'Contract  Details  Required'
*  indicator  on T6694 is 'Y' then a valid contract number must
*  be entered here. Otherwise the field must be blank.
*
*  . Amount To Pay. This entry may be left as zero by the  user
*  in  which case the program will calculate the value from the
*  default  percentage  from  T6696   and   the   Sum   Assured
*  calculated  by  the  program  earlier.  If an entry has been
*  made then it must be validated against the  rules  on  T6696
*  and  the  Total  Sum  Assured.  First  calculate the maximum
*  amount to pay  and  the  minimum  amount  to  pay  from  the
*  maximum  and  minimum values on T6696. If the entered amount
*  falls outside this range then give an error message.
*
*
*  . Registration Date. This is the  Effective  Date  of  Claim
*  and  is  mandatory.  It  may not be greater than the current
*  system date although a date in the past will be accepted.
*
*  . First Payment Date. This is  optional.  If  no  entry  has
*  been made then proceed as follows:
*
*       Check  for  a Default Deferment Period on T6696. If one
*       is there use this to calculate a default First  Payment
*       Date  and  set this in the field. The Default Deferment
*       Period is held as an  integer  number  of  frequencies.
*       The  frequency  itself  is  held against the period. If
*       there is no Default Deferment Period on T6696 then  use
*       the  Deferment  Period  on T5606 to calculate the First
*       Payment Date.
*
*       If there is no entry here  either  then  calculate  the
*       minimum  Deferment  Date  from  the  Minimum  Deferment
*       period on  T6696  and  set  this  value  in  the  First
*       Payment  Date.  The  Minimum  Deferment  period is also
*       held as a number of frequencies.
*
*       If this also is blank then default  the  First  Payment
*       Date to the Registration Date.
*
*  If  an  entry has been made then check that it does not fall
*  within the Minimum Deferment Period as defined on T6696.
*
*  . Review Date. This is optional.  Use  the  Review  Term  on
*  T6696  to  check  that  it is at least  as far in advance of
*  the Registration Date as this  value  indicates.  It  should
*  not  be less than the First Payment Date. The Review Term is
*  held as an integer number  of  frequencies.  If  it  is  not
*  entered it should be set to Max Date.
*
*  .  Next  Payment  Date.  For  Registration  default the Next
*  Payment Date to the First Payment Date.
*
*  . Final Payment Date. This is optional  and  if  left  blank
*  will  be  set  to  Max  Date. If entered it must not be less
*  than the Next Payment Date.
*
*  . Anniversary Date. This may be entered by the user  but  if
*  not  then  calculate  it  from the Registration Date and the
*  Indexation Frequency from T6696. The default is Max Date.
*
*  A check must also be made that the total amount of claim  at
*  any  one time does not exceed 100% of the Total Sum Assured.
*  In order to do this the following check must be carried out:
*
*       Read all of the REGPENQ records that match on  the  key
*       apart from Payment Number.
*
*       Check  the  First  Payment  Date, (FPAYDATE), and Final
*       Payment Date, (EPAYDATE) on  the  REGPENQ  record,  and
*       the  Cancellation Date. If there is any overlap between
*       those and the same dates on the screen for the  current
*       claim   then   include   the  REGPENQ  record  in  your
*       calculations.
*
*       Convert the Claim Amount to the same frequency  as  the
*       current claim, if necessary, and accumulate it.
*
*       Finally  take  the total accumulated amount from all of
*       the selected REGPENQ records and add it to the  entered
*       Claim  Amount. If the new total is now greater than the
*       calculated Sum Assured then indicate to the  user  that
*       the  total  has  been exceeded and display the total in
*       the field at the foot of the screen  which  should  now
*       be revealed.
*           NOTE. This validation is against all such payments
*                 on the contract (ie. all policies). If such
*                 validation should be needed against the policy
*                 then the records should be screened using the
*                 plan suffix.
*
*
*  .  The  Follow  Ups indicator, the Annuity Details indicator
*  and the Bank details indicator may only be 'X', '+' or space.
*
*  Updating.
*  ---------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  If 'KILL' has been pressed skip this section.
*
*  If WSSP-FLAG = 'I' then skip this section.
*
*  If  this is the first time through the 3000 section, ('First
*  Time Flag' is set to 'Y'), then carry  out  the  Valid  Flag
*  '2'  updating on the Contract Header and, if in Modify mode,
*  the Regular Payment File as follows:
*
*       Perform a READH on  CHDR,  set  Validflag  to  '2'  and
*       re-write the record.
*
*       Increment the TRANNO by 1, update the POLSUM and POLINC
*       fields.
*
*       Write  a  new  CHDR with a Validflag of '1' and the new
*       TRANNO.
*
*       If the program is in Modify mode,  (WSSP-FLAG  =  'M'),
*       then  the  current  REGP  record  must  be  made into a
*       history record by setting the  REGP  Validflag  to  '2'
*       and performing an UPDAT on REGP.
*
*  Move  all the screen fields to REGP and place the new TRANNO
*  on the record.
*
*  If either the Follow  Ups  indicator  or  the  Bank  Details
*  indicator are 'X' then perform a KEEPS on the REGP file.
*
*  Otherwise  the update proper is to be performed so write the
*  new REGP record and write a PTRN record.
*
*  If it is required  to  break  out  the  Plan,  transfer  the
*  softlock  to  AT  and  call  the  AT  module  otherwise call
*  'SFTLOCK' to unlock the contract.
*
*  Where Next.
*  -----------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN  =  '*',  then  re-load the next 8 programs in
*  the stack from Working Storage.
*
*  If returning  from  the  Follow  Ups  path  the  Follow  Ups
*  indicator  will  be  '?'.  Check if Follow Ups were actually
*  added by reading the FLUPRGP file for this  Payment  Details
*  record  using  a  key  of  CHDRCOY,  CHDRNUM,  a  CLAMNUM of
*  '00000000' and a function of BEGN.  If  a  record  is  found
*  that  matches on Company and Contract Number then Follow Ups
*  have been set up. If a record has been set up by the  Follow
*  Ups  processing  indicate  this  to  the user by setting the
*  Follow Ups indicator field to '+'. If  there  is  no  record
*  then move space to the Follow Ups indicator field.
*
*  If  returning  from  the  Bank Details path the Bank Details
*  indicator will be '?'. Check if Bank Details  were  actually
*  added  by  performing  a RETRV on the REGP file. If the Bank
*  details on the REGP file are non blank indicate this to  the
*  user  by setting the Bank Details indicator field to '+'. If
*  there  are  no  details  move  space  to  the  Bank  Details
*  indicator field.
*
*  If  the  Follow  Ups  indicator  is  'X'  then switch to the
*  Follow Ups path as follows:
*
*       Store the next 8  programs  in  the  stack  in  Working
*       Storage.
*
*       Call GENSSW with an action of 'A'.
*
*       If  there  is an error code returned from GENSSW use it
*       as an error code on the  Follow  Ups  indicator  field,
*       set  WSSP-EDTERROR  to  'Y'  and  set  WSSP-NEXTPROG to
*       SCRN-SCRNAME and go to exit.
*
*       Load the 8 programs returned  from  GENSSW  in  to  the
*       next 8 positions in the program stack.
*
*       Set the Follow Ups indicator to '?'.
*
*       Set WSSP-SEC-ACTN to '*'.
*
*       Add 1 to the program pointer and go to exit.
*
*
*  If  the  Bank  Details  indicator  is 'X' then switch to the
*  Bank Details path as follows:
*
*       Store the next 8  programs  in  the  stack  in  Working
*       Storage.
*
*       Call GENSSW with an action of 'B'.
*
*       If  there  is an error code returned from GENSSW use it
*       as an error code on the Bank Details  indicator  field,
*       set  WSSP-EDTERROR  to  'Y'  and  set  WSSP-NEXTPROG to
*       SCRN-SCRNAME and go to exit.
*
*       Load the 8 programs returned  from  GENSSW  in  to  the
*       next 8 positions in the program stack.
*
*       Set the Bank Details indicator to '?'.
*
*       Set WSSP-SEC-ACTN to '*'.
*
*       Add 1 to the program pointer and go to exit.
*
*  If   returning  from  a  program  further  down  the  stack,
*  WSSP-SEC-ACTN = '*', then move space  to  WSSP-SEC-ACTN  and
*  cause  the  program  to  redisplay  from the 2000 section by
*  setting WSSP-NEXTPROG to SCRN-SCRNAME and go to exit.
*
*  Add 1 to the program pointer and exit.
*
*  Notes.
*  ------
*
*  Tables Used.
*  ------------
*
*  . T1688 - Transactions Codes
*            Key: Transaction Code
*
*  . T3629 - Currency Code Details
*            Key: CURRCD
*
*  . T3588 - Contract Premium Status Codes
*            Key: PSTCDE from CHDRRGP
*
*  . T3623 - Contract Risk Status Codes
*            Key: STATCODE from CHDRRGP
*
*  . T5606 - Regular Benefit Component Edit Rules
*            Key: Validation Item Key (from T5671) || Currency
*                                                     Code
*  . T5661 - Follow-up Codes
*            Key: Follow-up Code
*
*  . T5671 - Generic Program Switching
*            Key: Transaction Code || CRTABLE
*
*  . T5677 - Default Follow-up Codes
*            Key: Transaction Code and Follow-up Method
*
*  . T5688 - Contract Definition
*            Key: Contract Type
*
*  . T5400 - Regular Payment Status
*            Key: Regular Payment Status Code
*
*  . T6625 - General Annuity Details
*            Key: CRTABLE
*
*  . T6692 - Regular Payment Reason Codes
*            Key: Regular Payment Reason Code
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be '****' and for selections against
*            a Component set Payment Status to '**'.
*
*  . T6694 - Regular Payment Method of Payment
*            Key: Regular Payment MOP
*
*  . T6696 - Regular Claim Detail Rules
*            Key: CRTABLE || Reason Code
*
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P5187 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(P5187.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5187");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(19);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaBrkoutPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 16);

	private ZonedDecimalData wsaaSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaSwitch, 1);
	private Validator partPlan = new Validator(wsaaSwitch, 2);
	private Validator summaryPartPlan = new Validator(wsaaSwitch, 3);

	private FixedLengthStringData wsaaT5606Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5606Edtitm = new FixedLengthStringData(5).isAPartOf(wsaaT5606Key, 0);
	private FixedLengthStringData wsaaT5606Currcd = new FixedLengthStringData(3).isAPartOf(wsaaT5606Key, 5);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);

	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Rgpystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);

	private FixedLengthStringData wsaaT6696Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6696Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6696Key, 0);
	private FixedLengthStringData wsaaT6696Cltype = new FixedLengthStringData(2).isAPartOf(wsaaT6696Key, 4);

	private FixedLengthStringData wsaaClamnum2 = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaClamnumFill = new ZonedDecimalData(3, 0).isAPartOf(wsaaClamnum2, 0).setUnsigned();
	private ZonedDecimalData wsaaRgpynum = new ZonedDecimalData(5, 0).isAPartOf(wsaaClamnum2, 3).setUnsigned();
	private FixedLengthStringData wsaaRgpymop = new FixedLengthStringData(1);
	private String wsaaAnnuity = "";
	private PackedDecimalData wsaaIntermed = new PackedDecimalData(7, 5);
	private ZonedDecimalData wsaaNextRgpy = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaPayclt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRgpystat = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaEdtitm = new FixedLengthStringData(5);
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLastFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaFreq2 = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStatuz = "";
	private String wsaaStopProcess = "";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPayrseqno = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaFreq3 = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPremCurrency = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private String wsaaOverlap = "";
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaSumins2 = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaSumins3 = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaLastSumins3 = new PackedDecimalData(17, 2).setUnsigned();
	private FixedLengthStringData wsaaErrorT3590 = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaCurrcd = new FixedLengthStringData(3);
	private PackedDecimalData wsaaPrcnt = new PackedDecimalData(7, 4).setUnsigned();
	private PackedDecimalData wsaaPymt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaPymt2 = new PackedDecimalData(16, 2).setUnsigned();
	private PackedDecimalData wsaaTotPymt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData index1 = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private String firstTime = "";
	private PackedDecimalData wsaaMaxPrcntValue = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaPolinc = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaPolsum = new PackedDecimalData(4, 0);
	private static final String wsaaClaimCompMess = "  Total claim for component, this term :";
	private static final String wsaaClaimReasMess = "Total claim for Reason Code, this term :";
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluprgpTableDAM fluprgpIO = new FluprgpTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private RegpenqTableDAM regpenqIO = new RegpenqTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Atreqrec atreqrec = new Atreqrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private T5606rec t5606rec = new T5606rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T6625rec t6625rec = new T6625rec();
	private T6690rec t6690rec = new T6690rec();
	private T6692rec t6692rec = new T6692rec();
	private T6693rec t6693rec = new T6693rec();
	private T6694rec t6694rec = new T6694rec();
	private T6696rec t6696rec = new T6696rec();
	private T6617rec t6617rec = new T6617rec();
	private static final String t6617 = "T6617";
	private static final String t517 = "T517";
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private S5187ScreenVars sv = ScreenProgram.getScreenVars( S5187ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private ItemDAO itempfDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	private static final String  registerRegularClaim_FEATURE_ID="CMRPY011";//ILIFE-8024
	private boolean registerRegularClaimflag = false; //ILIFE-8024 

	
	//CMRPY005Permission
	boolean CMRPY005Permission  = false;
	
	boolean CMOTH002Permission = false;
	private static final String clm004_FEATURE_ID = "CMOTH002";
	
	//CLM008 & CLM023
	private static final String FEAID_CMOTH003_PERMISSION = "CMOTH003"; 
	private boolean cmoth003permission = false;
	boolean entryFlag = false;
	private Alocnorec alocnorec = new Alocnorec();
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	private Notipf notipf=null;
	private Clnnpf clnnpf = null;
	private static final String RP = "RP";
	public static final String CLMPREFIX = "CLMNTF";
	private List<Clnnpf> clnnpfList1 = new ArrayList<Clnnpf>();
	private ClnnpfDAO clnnpfDAO= getApplicationContext().getBean("clnnpfDAO",ClnnpfDAO.class);
	private List<Invspf> invspfList1 = new ArrayList<Invspf>();
	private InvspfDAO invspfDAO= getApplicationContext().getBean("invspfDAO",InvspfDAO.class);
	//ILJ-48 Starts
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private String itemPFX = "IT";
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-48 End
	private boolean CMRPY008Permission = false;
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf;
	protected Chdrpf chdrpf = null;
	private Mandpf mandlnb = null;
	private static final String MANDATE ="Mandlnb";
	private static final String CLIENT ="CN";
	private static final String CHEQUE ="C";
	private static final String BANKCREDIT ="B";
	//IBPLIFE-1702
	private String expiredFlag="2";
	private String effectiveFlag="1";
	private String claimRegi="Claim Registered";
	private int noTransnoCount = 1;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		create1060,
		exit1090,
		preExit,
		validatePayee2045,
		checkPercentage2050,
		checkDestination2060,
		noReviewTerm2076,
		checkPayDate2077,
		validateFollow2079,
		checkForErrors2080,
		exit2090,
		exit3090,
		popUp4050,
		gensww4010,
		nextProgram4020,
		exit4090,
		investResult,
		claimNotes
	}

	public P5187() {
		super();
		screenVars = sv;
		new ScreenModel("S5187", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case create1060: {
					create1060();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		// CML-009
		CMRPY005Permission  = FeaConfg.isFeatureExist("2", "CMRPY005", appVars, "IT");
		CMOTH002Permission  = FeaConfg.isFeatureExist("2", clm004_FEATURE_ID, appVars, "IT"); //CLM004
		cmoth003permission = FeaConfg.isFeatureExist("2", FEAID_CMOTH003_PERMISSION, appVars, "IT");//ICIL-1045
		CMRPY008Permission = FeaConfg.isFeatureExist("2", "CMRPY008", appVars,"IT");	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaRgpymop.set(SPACES);
		wsaaAnnuity = "N";
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(!cntDteFlag) {
					sv.occdateOut[varcom.nd.toInt()].set("Y");
					}
//ILJ-49 End
		firstTime = "Y";
		wsaaStopProcess = "N";
		sv.anvdate.set(varcom.vrcmMaxDate);
		sv.aprvdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.cancelDate.set(varcom.vrcmMaxDate);
		sv.crtdate.set(varcom.vrcmMaxDate);
		sv.finalPaydate.set(varcom.vrcmMaxDate);
		sv.firstPaydate.set(varcom.vrcmMaxDate);
		sv.lastPaydate.set(varcom.vrcmMaxDate);
		sv.nextPaydate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.revdte.set(varcom.vrcmMaxDate);
		if(cntDteFlag)	{
			sv.riskcommdte.set(varcom.vrcmMaxDate);	//ILJ-48
		}
		wsaaSumins.set(ZERO);
		wsaaSumins2.set(ZERO);
		wsaaSumins3.set(ZERO);
		wsaaLastSumins3.set(ZERO);
		wsaaPayrseqno.set(ZERO);
		wsaaPymt.set(ZERO);
		wsaaTotPymt.set(ZERO);
		wsaaLastFreq.set(ZERO);
		wsaaIntermed.set(ZERO);
		sv.pymt.set(ZERO);
		sv.prcnt.set(ZERO);
		sv.sumin.set(ZERO);
		sv.totalamt.set(ZERO);
		sv.totamnt.set(ZERO);
		
		if(CMRPY005Permission){
			// CML-009
			sv.adjustamt.set(ZERO);
			sv.reasoncd.set(SPACES);
			sv.netclaimamt.set(ZERO);
			sv.resndesc.set(SPACES);
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,"****")) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		descIO.setDataKey(SPACES);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setDesctabl(tablesInner.t1688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.descrip.set(descIO.getLongdesc());
		}
		else {
			sv.descrip.fill("?");
		}
			if (isEQ(wsspcomn.flag, "I")) {
			try {
				if (isNE(chdrrgpIO.getClusterWsaaInUse(),"Y")){
					chdrrgpIO.setClusterWsaaInUse("Y");
					chdrrgpIO.setFunction(varcom.rlse);
					SmartFileCode.execute(appVars, chdrrgpIO);
					if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
						syserrrec.params.set(chdrrgpIO.getParams());
						syserrrec.statuz.set(chdrrgpIO.getStatuz());
						fatalError600();
					}
				}
			} catch (SQLException e) {
				LOGGER.error("Exception occured ",e);
			}
		}
		chdrrgpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		//ILJ-48 Starts
		if(cntDteFlag) {
			Hpadpf hpadpf = hpadpfDAO.getHpadData(chdrrgpIO.getChdrcoy().toString(), chdrrgpIO.getChdrnum().toString());
			if(hpadpf!=null) {
				sv.riskcommdte.set(hpadpf.getRskcommdate());
			}
		}
		//ILJ-48 End
		
		compute(wsspcomn.tranno, 0).set(add(chdrrgpIO.getTranno(), 1));
		/* Store POLINC and POLSUM in WSAA fields to update CHDRRGP*/
		/* after writing CHDR.*/
		wsaaPolsum.set(chdrrgpIO.getPolsum());
		wsaaPolinc.set(chdrrgpIO.getPolinc());
		chdrrgpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		descIO.setDescitem(chdrrgpIO.getCnttype());
		descIO.setDesctabl(tablesInner.t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		descIO.setDescitem(chdrrgpIO.getCntcurr());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.currds.set(descIO.getShortdesc());
		}
		else {
			sv.currds.fill("?");
		}
		descIO.setDescitem(chdrrgpIO.getPstatcode());
		descIO.setDesctabl(tablesInner.t3588);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getLongdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		descIO.setDescitem(chdrrgpIO.getStatcode());
		descIO.setDesctabl(tablesInner.t3623);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getLongdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		cltsIO.setClntnum(chdrrgpIO.getCownnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),1))) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.chdrnum.set(chdrrgpIO.getChdrnum());
		sv.cnttype.set(chdrrgpIO.getCnttype());
		sv.cownnum.set(chdrrgpIO.getCownnum());
		sv.occdate.set(chdrrgpIO.getOccdate());
		sv.ptdate.set(chdrrgpIO.getPtdate());
		sv.btdate.set(chdrrgpIO.getBtdate());
		sv.currcd.set(chdrrgpIO.getCntcurr());
		sv.polinc.set(chdrrgpIO.getPolinc());
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		regpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		sv.crtable.set(regpIO.getCrtable());
		sv.plansfx.set(regpIO.getPlanSuffix());
		if (isEQ(regpIO.getPlanSuffix(),ZERO)) {
			sv.plansfx.set(chdrrgpIO.getPolinc());
			sv.plansfxOut[varcom.hi.toInt()].set("Y");
		}
		if (isEQ(regpIO.getPlanSuffix(),ZERO)) {
			wsaaSwitch.set(1);
		}
		else {
			if (isGT(regpIO.getPlanSuffix(),chdrrgpIO.getPolsum())
			|| isEQ(chdrrgpIO.getPolsum(),1)) {
				wsaaSwitch.set(2);
			}
			else {
				wsaaSwitch.set(3);
			}
		}
		covrmjaIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrrgpIO.getChdrnum());
		covrmjaIO.setLife(regpIO.getLife());
		covrmjaIO.setCoverage(regpIO.getCoverage());
		covrmjaIO.setRider(regpIO.getRider());
		if (isEQ(regpIO.getPlanSuffix(),ZERO)) {
			covrmjaIO.setPlanSuffix(9999);
		}
		else {
			covrmjaIO.setPlanSuffix(regpIO.getPlanSuffix());
		}
		covrmjaIO.setStatuz(varcom.oK);
		covrmjaIO.setFunction(varcom.begn);
		while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
			accumCovrmja1600();
		}

		if (isEQ(wsaaPayrseqno,0)) {
			wsaaPayrseqno.set(1);
		}
		/* Accumulate the total sum insured within the plan.*/
		covrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		covrIO.setChdrnum(chdrrgpIO.getChdrnum());
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setStatuz(varcom.oK);
		covrIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) {
			accumCovr1800();
		}

		wsaaLastSumins3.set(wsaaSumins3);
		readT56711500();
		wsaaT5606Edtitm.set(wsaaEdtitm);
		wsaaT5606Currcd.set(sv.currcd);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5606);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT5606Key);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t5606, itdmIO.getItemtabl()))
		|| (isNE(wsaaT5606Key,itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setGenarea(SPACES);
			sv.currcdErr.set(errorsInner.g528);
			wsspcomn.edterror.set("Y");
		}
		t5606rec.t5606Rec.set(itdmIO.getGenarea());
		if (isNE(wsspcomn.flag,"C")) {
			if ((isNE(t5606rec.benfreq,regpIO.getRegpayfreq()))
			&& (isNE(t5606rec.benfreq,SPACES))) {
				wsaaFreq.set(t5606rec.benfreq);
				wsaaFreq2.set(regpIO.getRegpayfreq());
				if (isEQ(wsaaFreq,0)) {
					wsaaFreq.set(1);
				}
				if (isEQ(wsaaFreq2,0)) {
					wsaaFreq2.set(1);
				}
				compute(wsaaSumins2, 2).set((div((mult(wsaaSumins,wsaaFreq)),wsaaFreq2)));
				wsaaSumins.set(wsaaSumins2);
				compute(wsaaSumins3, 2).set((div((mult(wsaaSumins3,wsaaFreq)),wsaaFreq2)));
			}
			else {
				wsaaSumins2.set(wsaaSumins);
			}
		}
		else {
			wsaaSumins2.set(wsaaSumins);
		}
		sv.sumin.set(wsaaSumins2);
		covrmjaIO.setLife(wsaaLife);
		covrmjaIO.setJlife(wsaaJlife);
		covrmjaIO.setPremCurrency(wsaaPremCurrency);
		covrmjaIO.setCrtable(wsaaCrtable);
		lifeenqIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrrgpIO.getChdrnum());
		lifeenqIO.setLife(regpIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		lifeenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");

		SmartFileCode.execute(appVars, lifeenqIO);
		if ((isNE(lifeenqIO.getStatuz(),varcom.oK))
		&& (isNE(lifeenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrrgpIO.getChdrcoy(),lifeenqIO.getChdrcoy()))
		|| (isNE(chdrrgpIO.getChdrnum(),lifeenqIO.getChdrnum()))
		|| (isNE(regpIO.getLife(),lifeenqIO.getLife()))
		|| (isEQ(lifeenqIO.getStatuz(),varcom.endp))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
			goTo(GotoLabel.create1060);
		}
		sv.canceldateOut[varcom.nd.toInt()].set("Y");
		sv.totalamtOut[varcom.nd.toInt()].set("Y");
		sv.lifcnum.set(lifeenqIO.getLifcnum());

		// CML009
		if (CMRPY005Permission) {
			sv.adjustamtOut[varcom.nd.toInt()].set("N");
			sv.netclaimamtOut[varcom.nd.toInt()].set("N");
			sv.reasoncdOut[varcom.nd.toInt()].set("N");
			sv.resndescOut[varcom.nd.toInt()].set("N");
			sv.pymtAdjOut[varcom.nd.toInt()].set("N");
		} else {
			sv.adjustamtOut[varcom.nd.toInt()].set("Y");
			sv.netclaimamtOut[varcom.nd.toInt()].set("Y");
			sv.reasoncdOut[varcom.nd.toInt()].set("Y");
			sv.resndescOut[varcom.nd.toInt()].set("Y");
			sv.pymtAdjOut[varcom.nd.toInt()].set("Y");
		}
		
		
	    if (CMOTH002Permission) {   // ICIL-1069
				sv.itstdaysOut[varcom.nd.toInt()].set("N");
				sv.itstrateOut[varcom.nd.toInt()].set("N");
				sv.itstamtOut[varcom.nd.toInt()].set("N");
				
				if(isEQ(regpIO.getInterestrate(), ZERO))
	            {
	        		readT6617();
	            	sv.itstrate.set(t6617rec.intRate);	 
	            }
	            else
	            {	
	            	  sv.itstrate.set(regpIO.getInterestrate());
	            }
	            
	            sv.itstdays.set(regpIO.getInterestdays());
	            sv.itstamt.set(regpIO.getInterestamt());
	            
	        } else {
				sv.itstdaysOut[varcom.nd.toInt()].set("Y");
				sv.itstrateOut[varcom.nd.toInt()].set("Y");
				sv.itstamtOut[varcom.nd.toInt()].set("Y");
	        }
	 
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),1))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		wsspcomn.chdrCownnum.set(chdrrgpIO.getCownnum());
		// agoel51
		if (cmoth003permission) {
			if (isEQ(wsspcomn.flag, "C")) {
				allocateNumber();
				sv.incurdt.set(varcom.vrcmMaxDate);
			} else {
				sv.claimno.set(regpIO.getClaimno());
				sv.incurdt.set(regpIO.getIncurdt());
			}
			if(isEQ(regpIO.getClaimnotifino(),SPACES)){
				sv.notifino.set(SPACES);
			}
			else{
				sv.notifino.set(CLMPREFIX+regpIO.getClaimnotifino());
			}
			wsspcomn.chdrCownnum.set(chdrrgpIO.getCownnum().toString());

			wsspcomn.chdrClntcoy.set(wsspcomn.fsuco);
		}
	}

private void getChdrDetails()
{
	chdrpf = chdrpfDAO.getChdrpf(chdrrgpIO.getChdrcoy().toString(), chdrrgpIO.getChdrnum().toString());
	if(null == chdrpf){
		syserrrec.params.set(chdrrgpIO.getChdrcoy().concat(chdrrgpIO));
		fatalError600();
	}
	if(isEQ(sv.rgpymop,SPACES))
		defaultPayment();
}

private void defaultPayment()
{
	if(isEQ(chdrpf.getReqntype(),"4")){
		sv.rgpymop.set(BANKCREDIT);
		mandlnb = mandpfDAO.searchMandpfRecord(wsspcomn.fsuco.toString().trim(), chdrpf.getPayclt().trim(),
				chdrpf.getZmandref().trim(),MANDATE);
		if (mandlnb != null && isEQ(mandlnb.getCrcind(), SPACES)) {
			sv.ddind.set("X");
			sv.payclt.set(mandlnb.getPayrnum());
			clntpf = clntpfDAO.searchClntRecord(CLIENT, wsspcomn.fsuco.toString(), mandlnb.getPayrnum());
			if(null == clntpf){
				syserrrec.params.set(CLIENT.concat( wsspcomn.fsuco.toString()).concat(mandlnb.getPayrnum()));
				fatalError600();
			}
			if(isEQ(clntpf.getValidflag(),"1")){
				plainname100();
				sv.payenme.set(wsspcomn.longconfname);
			}
		
			wsspcomn.chdrCownnum.set(sv.payclt);
		}
	}
	else if(isEQ(chdrpf.getReqntype(),"B") || isEQ(chdrpf.getReqntype(),"1") || isEQ(chdrpf.getReqntype(),"2")){
		sv.rgpymop.set(CHEQUE);
	}
}

protected void plainname100()
{
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(),"C")) {
		corpname100();
		return ;
	}
	if (isNE(clntpf.getGivname(),SPACES)) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getSurname(), "  ");
		stringVariable1.addExpression(", ");
		stringVariable1.addExpression(clntpf.getGivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
	}
	else {
		wsspcomn.longconfname.set(clntpf.getSurname());
	}
}

protected void corpname100()
{
	wsspcomn.longconfname.set(SPACES);
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(clntpf.getLsurname(), "  ");
	stringVariable1.addExpression(" ");
	stringVariable1.addExpression(clntpf.getLgivname(), "  ");
	stringVariable1.setStringInto(wsspcomn.longconfname);
}

/**
 * @author agoel51 
 *  
 */
private void allocateNumber()
{
	alocnorec.function.set("NEXT");
	alocnorec.prefix.set("CM");
	alocnorec.company.set(wsspcomn.company);
	/*wsaaBranch.set(wsspcomn.branch);*/
	alocnorec.genkey.set(SPACES);
	callProgram(Alocno.class, alocnorec.alocnoRec);
	if (isEQ(alocnorec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(alocnorec.statuz);
		fatalError600();
	}
	if (isNE(alocnorec.statuz, varcom.oK)) {
		sv.claimnoErr.set(alocnorec.statuz);
	} else {
		sv.claimno.set(alocnorec.alocNo);
	}
}

protected void readT6617(){
	
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemitem(t517);
	itempf.setItemtabl(t6617);
	itempf.setItemseq("  ");
	itempf = itempfDAO.getItemRecordByItemkey(itempf);
	
	if (itempf == null) {
		syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T6617").concat(t517));
		fatalError600();
	}
	t6617rec.t6617Rec.set(StringUtil.rawToString(itempf.getGenarea()));
}
	
protected void create1060()
	{
		if (isEQ(wsspcomn.flag, "C")) {
			readT66935200();
			createRegp1700();
		}
		if (isNE(wsspcomn.flag,"C")) {
			readRegpDetails1300();
		}
		checkForAnnuity1900();
		if (isEQ(wsaaAnnuity,"Y")
		&& isEQ(wsspcomn.flag,"M")) {
			if (isEQ(t6694rec.contreq,"N")) {
				sv.destkeyOut[varcom.pr.toInt()].set("Y");
			}
			else {
				sv.destkeyOut[varcom.pr.toInt()].set(SPACES);
			}
			wsaaRgpymop.set(sv.rgpymop);
		}
		checkFollowUp5000();
		checkAnnyDetails6000();	
		if(CMRPY008Permission){
			getChdrDetails();
		}
	}

protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))
		&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readRegpDetails1300()
	{
					read1310();
					bypassBankDetails1380();
				}

protected void read1310()
	{
		/* Read Payment Type Details.*/
		descIO.setDescitem(regpIO.getRgpytype());
		descIO.setDesctabl(tablesInner.t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		}
		else {
			sv.rgpytypesd.fill("?");
		}
		/* Read Payee Details*/
		if (isNE(regpIO.getPayclt(),SPACES)) {
			cltsIO.setClntnum(regpIO.getPayclt());
			getClientDetails1200();
			if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
			|| (isNE(cltsIO.getValidflag(),1))) {
				sv.payenmeErr.set(errorsInner.e335);
				sv.payenme.set(SPACES);
			}
			else {
				plainname();
				sv.payenme.set(wsspcomn.longconfname);
			}
		}
		/* Read the Claim Status Description.*/
		descIO.setDescitem(regpIO.getRgpystat());
		descIO.setDesctabl(tablesInner.t5400);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.statdsc.set(descIO.getShortdesc());
		}
		else {
			sv.statdsc.fill("?");
		}
		/* Read the Reason Code Description.*/
		descIO.setDescitem(regpIO.getPayreason());
		descIO.setDesctabl(tablesInner.t6692);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.clmdesc.set(descIO.getLongdesc());
		}
		else {
			sv.clmdesc.fill("?");
		}
		/* Read the Payment Method Description.*/
		descIO.setDescitem(regpIO.getRgpymop());
		descIO.setDesctabl(tablesInner.t6694);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rgpyshort.set(descIO.getShortdesc());
		}
		else {
			sv.rgpyshort.fill("?");
		}
		descIO.setDescitem(regpIO.getRegpayfreq());
		descIO.setDesctabl(tablesInner.t3590);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.frqdesc.set(descIO.getShortdesc());
		}
		else {
			sv.frqdesc.fill("?");
		}
		descIO.setDescitem(regpIO.getCurrcd());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		}
		else {
			sv.clmcurdsc.fill("?");
		}
		sv.payclt.set(regpIO.getPayclt());
		sv.rgpynum.set(regpIO.getRgpynum());
		sv.rgpystat.set(regpIO.getRgpystat());
		sv.cltype.set(regpIO.getPayreason());
		sv.claimevd.set(regpIO.getClaimevd());
		sv.rgpymop.set(regpIO.getRgpymop());
		sv.regpayfreq.set(regpIO.getRegpayfreq());
		wsaaFreq.set(regpIO.getRegpayfreq());
		wsaaFreq3.set(regpIO.getRegpayfreq());
		wsaaLastFreq.set(regpIO.getRegpayfreq());
		sv.prcnt.set(regpIO.getPrcnt());
		if (isNE(regpIO.getDestkey(),SPACES)) {
			sv.destkey.set(regpIO.getDestkey());
		}
		sv.pymt.set(regpIO.getPymt());
		sv.totamnt.set(regpIO.getTotamnt());
		if(CMRPY005Permission){
			//CML009
			sv.adjustamt.set(regpIO.getAdjamt());
			sv.reasoncd.set(regpIO.getReasoncd());
			sv.netclaimamt.set(regpIO.getNetamt());
			sv.resndesc.set(regpIO.getReason());
		}
		sv.claimcur.set(regpIO.getCurrcd());
		if (isEQ(wsspcomn.flag,"I")) {
			sv.aprvdate.set(regpIO.getAprvdate());
		}
		else {
			sv.aprvdate.set(varcom.vrcmMaxDate);
		}
		sv.crtdate.set(regpIO.getCrtdate());
		sv.revdte.set(regpIO.getRevdte());
		sv.firstPaydate.set(regpIO.getFirstPaydate());
		sv.lastPaydate.set(regpIO.getLastPaydate());
		sv.nextPaydate.set(regpIO.getNextPaydate());
		sv.anvdate.set(regpIO.getAnvdate());
		sv.finalPaydate.set(regpIO.getFinalPaydate());
		if (isEQ(wsspcomn.flag,"I")) {
			sv.cancelDate.set(regpIO.getCancelDate());
			sv.canceldateOut[varcom.nd.toInt()].set(" ");
		}
		else {
			sv.canceldateOut[varcom.nd.toInt()].set("Y");
		}
		if ((isEQ(wsspcomn.flag,"M"))
		&& (isNE(regpIO.getPayclt(),SPACES))) {
			wsaaPayclt.set(regpIO.getPayclt());
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(regpIO.getRgpymop());
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
		|| (isNE(regpIO.getRgpymop(),itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.rgpymopErr.set(errorsInner.g529);
			return ;
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		if (isEQ(t6694rec.bankreq,"Y")) {
			if ((isEQ(regpIO.getBankkey(),SPACES))
			|| (isEQ(regpIO.getBankacckey(),SPACES))) {
				sv.ddind.set("X");
			}
			else {
				sv.ddind.set("+");
			}
		}
		else {
			if ((isEQ(regpIO.getBankkey(),SPACES))
			&& (isEQ(regpIO.getBankacckey(),SPACES))) {
				sv.ddind.set(SPACES);
			}
			else {
				sv.ddind.set("+");
			}
		}
	}

protected void bypassBankDetails1380()
	{
		sv.crtdateOut[varcom.pr.toInt()].set("Y");
		if (isLT(regpIO.getFirstPaydate(),datcon1rec.intDate)) {
			sv.fpaydateOut[varcom.pr.toInt()].set("Y");
		}
		/*EXIT*/
	}

protected void findNextRegp1400()
	{
			find1410();
		}

protected void find1410()
	{
		SmartFileCode.execute(appVars, regpenqIO);
		if ((isNE(regpenqIO.getStatuz(),varcom.oK))
		&& (isNE(regpenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regpenqIO.getParams());
			fatalError600();
		}
		if (isEQ(regpenqIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if ((isEQ(regpIO.getChdrcoy(),regpenqIO.getChdrcoy()))
		&& (isEQ(regpIO.getChdrnum(),regpenqIO.getChdrnum()))) {
			if (isGT(regpenqIO.getRgpynum(),wsaaNextRgpy)) {
				wsaaNextRgpy.set(regpenqIO.getRgpynum());
			}
		}
		else {
			regpenqIO.setStatuz(varcom.endp);
		}
		regpenqIO.setFunction(varcom.nextr);
	}

protected void readT56711500()
	{
			read1510();
		}

protected void read1510()
	{
		wsaaT5671Trancd.set(wsaaBatckey.batcBatctrcde);
		wsaaT5671Crtable.set(regpIO.getCrtable());
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			scrnparams.errorCode.set(errorsInner.g520);
			wsspcomn.edterror.set("Y");
			itemIO.setGenarea(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		index1.set(1);
		while ( !((isGT(index1,4))
		|| (isEQ(wsaaProg,t5671rec.pgm[index1.toInt()])))) {
			if (isNE(t5671rec.pgm[index1.toInt()],wsaaProg)) {
				index1.add(1);
			}
		}

		if (isEQ(t5671rec.pgm[index1.toInt()],wsaaProg)) {
			wsaaEdtitm.set(t5671rec.edtitm[index1.toInt()]);
		}
	}

protected void accumCovrmja1600()
	{
			accum1610();
		}

protected void accum1610()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if ((isNE(covrmjaIO.getStatuz(),varcom.oK))
		&& (isNE(covrmjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrrgpIO.getChdrcoy(),covrmjaIO.getChdrcoy()))
		|| (isNE(chdrrgpIO.getChdrnum(),covrmjaIO.getChdrnum()))
		|| (isNE(regpIO.getLife(),covrmjaIO.getLife()))
		|| (isNE(regpIO.getCoverage(),covrmjaIO.getCoverage()))
		|| (isNE(regpIO.getRider(),covrmjaIO.getRider()))
		|| (isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
			covrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		if (partPlan.isTrue()) {
			if (isNE(regpIO.getPlanSuffix(),covrmjaIO.getPlanSuffix())) {
				covrmjaIO.setStatuz(varcom.endp);
				return ;
			}
		}
		/* Check coverage statii against T5679*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatuz = "N";
		wsaaStatcode.set(covrmjaIO.getStatcode());
		wsaaPstcde.set(covrmjaIO.getPstatcode());
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)
		|| isEQ(wsaaValidStatuz,"Y")); wsaaSub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()],wsaaStatcode)) {
				for (wsaaSub.set(1); !(isGT(wsaaSub,12)
				|| isEQ(wsaaValidStatuz,"Y")); wsaaSub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()],wsaaPstcde)) {
						wsaaValidStatuz = "Y";
					}
				}
			}
		}
		if (isEQ(wsaaValidStatuz,"N")) {
			wsaaStopProcess = "Y";
		}
		if (isEQ(regpIO.getCoverage(),covrmjaIO.getCoverage())) {
			if (isGT(covrmjaIO.getPayrseqno(),0)) {
				wsaaPayrseqno.set(covrmjaIO.getPayrseqno());
			}
			else {
				wsaaPayrseqno.set(1);
			}
		}
		wsaaLife.set(covrmjaIO.getLife());
		wsaaJlife.set(covrmjaIO.getJlife());
		wsaaPremCurrency.set(covrmjaIO.getPremCurrency());
		wsaaCrtable.set(covrmjaIO.getCrtable());
		if (summaryPartPlan.isTrue()) {
			if (isNE(covrmjaIO.getSumins(),ZERO)) {
				if (isEQ(sv.plansfx,1)) {
					compute(wsaaSumins, 2).set((sub(covrmjaIO.getSumins(),(div(mult(covrmjaIO.getSumins(),(sub(chdrrgpIO.getPolsum(),1))),chdrrgpIO.getPolsum())))));
				}
				else {
					compute(wsaaSumins, 2).set((div(covrmjaIO.getSumins(),chdrrgpIO.getPolsum())));
				}
			}
		}
		else {
			compute(wsaaSumins, 2).set((add(wsaaSumins,covrmjaIO.getSumins())));
		}
		if (wholePlan.isTrue()) {
			covrmjaIO.setFunction(varcom.nextr);
		}
		else {
			covrmjaIO.setStatuz(varcom.endp);
		}
	}

protected void createRegp1700()
	{
			create1710();
		}

protected void create1710()
	{
		wsaaNextRgpy.set(ZERO);
		regpenqIO.setStatuz(varcom.oK);
		regpenqIO.setChdrcoy(regpIO.getChdrcoy());
		regpenqIO.setChdrnum(regpIO.getChdrnum());
		if(CMRPY005Permission){
			regpenqIO.setAdjamt(regpIO.getAdjamt());
			regpenqIO.setNetamt(regpIO.getNetamt());
			regpenqIO.setReasoncd(regpIO.getReasoncd());
			regpenqIO.setReason(regpIO.getReason());
		}
		regpenqIO.setLife(ZERO);
		regpenqIO.setCoverage(ZERO);
		regpenqIO.setRider(ZERO);
		regpenqIO.setRgpynum(99999);
		regpenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		regpenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		while ( !(isEQ(regpenqIO.getStatuz(),varcom.endp))) {
			findNextRegp1400();
		}

		setPrecision(regpIO.getRgpynum(), 0);
		regpIO.setRgpynum(add(wsaaNextRgpy,1));
		if (isNE(t5606rec.benfreq,SPACES)) {
			sv.regpayfreq.set(t5606rec.benfreq);
			wsaaFreq.set(t5606rec.benfreq);
			wsaaFreq3.set(t5606rec.benfreq);
			wsaaLastFreq.set(t5606rec.benfreq);
			descIO.setDescitem(t5606rec.benfreq);
			descIO.setDesctabl(tablesInner.t3590);
			findDesc1100();
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				sv.frqdesc.set(descIO.getShortdesc());
			}
			else {
				sv.frqdesc.fill("?");
			}
		}
		else {
			sv.regpayfreqErr.set(errorsInner.g528);
			wsspcomn.edterror.set("Y");
		}
		sv.crtdate.set(datcon1rec.intDate);
		sv.claimcur.set(chdrrgpIO.getCntcurr());
		sv.rgpynum.set(regpIO.getRgpynum());
		if (isNE(covrmjaIO.getPremCurrency(),SPACES)) {
			descIO.setDescitem(covrmjaIO.getPremCurrency());
			descIO.setDesctabl(tablesInner.t3629);
			findDesc1100();
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				sv.currds.set(descIO.getShortdesc());
			}
			else {
				sv.currds.fill("?");
			}
		}
		/* Read the Claim Currency Description.*/
		descIO.setDescitem(chdrrgpIO.getCntcurr());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		}
		else {
			sv.clmcurdsc.fill("?");
		}
		/* Read the Claim Status Description.*/
		sv.rgpystat.set(wsaaRgpystat);
		descIO.setDescitem(wsaaRgpystat);
		descIO.setDesctabl(tablesInner.t5400);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.statdsc.set(descIO.getShortdesc());
		}
		else {
			sv.statdsc.fill("?");
		}
		/* Read the Defualt Claim Type Description.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t6690);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(covrmjaIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
			return ;
		}
		t6690rec.t6690Rec.set(itemIO.getGenarea());
		descIO.setDescitem(t6690rec.rgpytype);
		descIO.setDesctabl(tablesInner.t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		}
		else {
			sv.rgpytypesd.fill("?");
		}
	}

protected void accumCovr1800()
	{
			accum1810();
		}

protected void accum1810()
	{
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK))
		&& (isNE(covrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrrgpIO.getChdrcoy(),covrIO.getChdrcoy()))
		|| (isNE(chdrrgpIO.getChdrnum(),covrIO.getChdrnum()))
		|| (isNE(regpIO.getLife(),covrIO.getLife()))
		|| (isNE(regpIO.getCoverage(),covrIO.getCoverage()))
		|| (isNE(regpIO.getRider(),covrIO.getRider()))
		|| (isEQ(covrIO.getStatuz(),varcom.endp))) {
			covrIO.setStatuz(varcom.endp);
			return ;
		}
		covrIO.setFunction(varcom.nextr);
		/* Add SUMINS only if record has valid flag of 1                   */
		if (isNE(covrIO.getValidflag(),1)) {
			covrIO.setFunction(varcom.nextr);
			return ;
		}
		/* Check coverage statii against T5679*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatuz = "N";
		wsaaStatcode.set(covrIO.getStatcode());
		wsaaPstcde.set(covrIO.getPstatcode());
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)
		|| isEQ(wsaaValidStatuz,"Y")); wsaaSub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()],wsaaStatcode)) {
				for (wsaaSub.set(1); !(isGT(wsaaSub,12)
				|| isEQ(wsaaValidStatuz,"Y")); wsaaSub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()],wsaaPstcde)) {
						wsaaValidStatuz = "Y";
					}
				}
			}
		}
		if (isNE(wsaaValidStatuz,"N")) {
			wsaaSumins3.add(covrIO.getSumins());
		}
	}

protected void checkForAnnuity1900()
	{
		starts1900();
	}

protected void starts1900()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6625);
		itdmIO.setItemitem(sv.crtable);
		itdmIO.setItmfrm(sv.crtdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6625)
		|| isNE(itdmIO.getItemitem(),sv.crtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.anntind.set(" ");
			sv.anntindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			wsaaAnnuity = "Y";
			t6625rec.t6625Rec.set(itdmIO.getGenarea());
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
			screenio2015();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			protectScr2400();
		}
		//fwang3
		if (cmoth003permission) {
			sv.showFlag.set("Y");
		} else {
			sv.showFlag.set("N");
		}	
		if (isEQ(sv.lastPaydate,varcom.vrcmMaxDate)) {
			sv.npaydateOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(wsaaAnnuity,"N")) {
			sv.destkeyOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(wsaaStopProcess,"Y")) {
			scrnparams.errorCode.set(errorsInner.h136);
			scrnparams.function.set(varcom.prot);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag,"C")) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaT6696Crtable.set(covrmjaIO.getCrtable());
			wsaaT6696Cltype.set(regpIO.getPayreason());
			itdmIO.setDataKey(SPACES);
			itdmIO.setItemcoy(wsspcomn.company);
			itdmIO.setFormat(formatsInner.itdmrec);
			itdmIO.setItemtabl(tablesInner.t6696);
			itdmIO.setItempfx("IT");
			itdmIO.setItemitem(wsaaT6696Key);
			itdmIO.setItmfrm(chdrrgpIO.getOccdate());
			itdmIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if ((isNE(itdmIO.getStatuz(),varcom.oK))
			&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
			|| (isNE(tablesInner.t6696, itdmIO.getItemtabl()))
			|| (isNE(wsaaT6696Key,itdmIO.getItemitem()))
			|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
				itdmIO.setGenarea(SPACES);
				sv.cltypeErr.set(errorsInner.g522);
				wsspcomn.edterror.set("Y");
			}
			else {
				t6696rec.t6696Rec.set(itdmIO.getGenarea());
				if (isEQ(t6696rec.frqoride,"N")) {
					sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
				}
			}
		}
		
		
		//ILIFE-5084 starts
		if (isEQ(sv.ddind, SPACES)) {
			sv.ddindOut[varcom.pr.toInt()].set("Y");
			return;
		}  		//ILIFE-5084 ends
		
		
	}

protected void screenio2015()
	{
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
					checkPayment2030();
					checkCurrency2035();
					checkFreqency2040();
					checkReasonDesc2045();
				}
				case validatePayee2045: {
					validatePayee2045();
				}
				case checkPercentage2050: {
					checkPercentage2050();
					if (CMRPY005Permission) {
						checkAdjustment2055();
					}
				}
				case checkDestination2060: {
					checkDestination2060();
					checkNextDate2069();
					checkReviewDate2075();
				}
				case noReviewTerm2076: {
					noReviewTerm2076();
				}
				case checkPayDate2077: {
					checkPayDate2077();
				}
				case validateFollow2079: {
					validateFollow2079();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(wsaaStopProcess,"Y")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaAnnuity,"Y")
		&& isNE(wsaaRgpymop,sv.rgpymop)) {
			checkT66942600();
		}
		if ((isNE(scrnparams.statuz,varcom.calc))
		&& (isNE(scrnparams.statuz,varcom.oK))) {
			scrnparams.errorCode.set(errorsInner.curs);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag,"I")) {
			if ((isNE(sv.ddind,"+")
			&& isNE(sv.ddind,"X")
			&& isNE(sv.ddind,SPACES))) {
				sv.ddindErr.set(errorsInner.h118);
			}	
			if ((isEQ(sv.ddind,"X")
			&& isEQ(regpIO.getBankkey(),SPACES))) {
				sv.ddindErr.set(errorsInner.e493);
			}
			goTo(GotoLabel.checkForErrors2080);
		}
		
		if (cmoth003permission) {	
			if(isNE(sv.notifino,SPACES) && isEQ(wsspcomn.flag, "C")){
				notipf = notipfDAO.getNotiReByNotifin(sv.notifino.toString().replace(CLMPREFIX, ""),wsspcomn.company.toString());
				if(notipf != null && isEQ(notipf.getIncidentt().trim(),RP)){
					if(isNE(notipf.getIncurdate(),SPACES) && isNE(notipf.getRclaimreason(),SPACES) && !entryFlag){
							entryFlag = true;
							sv.incurdt.set(notipf.getIncurdate());
							sv.cltype.set(notipf.getRclaimreason());
					}
				}
				
				else{
				sv.notifinoErr.set(errorsInner.rrsn);
				}
			}
		}
		if (isEQ(sv.cltype,SPACES)) {
			sv.cltypeErr.set(errorsInner.e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.reasoncd,SPACES) && isEQ(wsspcomn.flag, "M")) {
			sv.adjustamtErr.set(errorsInner.e186);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t6692);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(sv.cltype);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.cltypeErr.set(errorsInner.g521);
			itemIO.setGenarea(SPACES);
		}
		t6692rec.t6692Rec.set(itemIO.getGenarea());
		wsaaT6696Crtable.set(covrmjaIO.getCrtable());
		wsaaT6696Cltype.set(sv.cltype);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6696);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6696Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t6696, itdmIO.getItemtabl()))
		|| (isNE(wsaaT6696Key,itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.cltypeErr.set(errorsInner.g522);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		t6696rec.t6696Rec.set(itdmIO.getGenarea());
		descIO.setDescitem(sv.cltype);
		descIO.setDesctabl(tablesInner.t6692);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.clmdesc.set(descIO.getLongdesc());
		}
		else {
			sv.clmdesc.fill("?");
		}
		/* Read the Claim Type Details.*/
		descIO.setDescitem(t6692rec.rgpytype);
		descIO.setDesctabl(tablesInner.t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		}
		else {
			sv.rgpytypesd.fill("?");
		}
	}

protected void checkPayment2030()
	{
		if (isEQ(sv.rgpymop,SPACES)) {
			sv.rgpymopErr.set(errorsInner.e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.rgpymop);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
		|| (isNE(sv.rgpymop,itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.rgpymopErr.set(errorsInner.g529);
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		/*  Get Method of Payment Description*/
		descIO.setDescitem(sv.rgpymop);
		descIO.setDesctabl(tablesInner.t6694);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rgpyshort.set(descIO.getShortdesc());
		}
		else {
			sv.rgpyshort.fill("?");
		}
	}

protected void checkCurrency2035()
	{
		if (isEQ(sv.claimcur,SPACES)) {
			sv.claimcur.set(chdrrgpIO.getCntcurr());
		}
		descIO.setDescitem(sv.claimcur);
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		}
		else {
			sv.clmcurdsc.fill("?");
		}
	}

protected void checkFreqency2040()
	{
		if (isEQ(sv.regpayfreq,SPACES)) {
			sv.regpayfreqErr.set(errorsInner.e186);
			goTo(GotoLabel.validatePayee2045);
		}
		if (isNE(wsaaLastFreq,sv.regpayfreq)) {
			chdrenqIO.setChdrnum(sv.chdrnum);
			checkContractNo2100();
			if (isEQ(sv.cltype,SPACES)) {
				goTo(GotoLabel.validatePayee2045);
			}
			if (isEQ(sv.regpayfreq,"00")
			&& isEQ(t6694rec.contreq,"Y")) {
				sv.regpayfreqErr.set(errorsInner.e925);
				wsspcomn.edterror.set("Y");
			}
			if ((isEQ(t6696rec.frqoride,"N"))) {
				if ((isEQ(t6694rec.contreq,"Y"))
				&& (isNE(chdrenqIO.getBillfreq(),sv.regpayfreq))) {
					sv.regpayfreq.set(chdrenqIO.getBillfreq());
					sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
					wsaaFreq2.set(sv.regpayfreq);
					wsaaFreq.set(sv.regpayfreq);
					wsaaSumins2.set(ZERO);
				}
				else {
					if ((isNE(t5606rec.benfreq,sv.regpayfreq))) {
						sv.regpayfreq.set(t5606rec.benfreq);
						sv.regpayfreqErr.set(errorsInner.g514);
						wsspcomn.edterror.set("Y");
					}
				}
			}
			if (isEQ(t6696rec.frqoride,"Y")) {
				wsaaFreq2.set(sv.regpayfreq);
				wsaaFreq.set(sv.regpayfreq);
				wsaaLastFreq.set(sv.regpayfreq);
				wsaaSumins2.set(ZERO);
			}
			if (isEQ(wsaaFreq3,0)) {
				wsaaFreq3.set(1);
			}
			if (isEQ(wsaaFreq2,0)) {
				wsaaFreq2.set(1);
			}
			compute(wsaaSumins2, 2).set((div((mult(wsaaSumins,wsaaFreq3)),wsaaFreq2)));
			sv.sumin.set(wsaaSumins2);
		}
		else {
			chdrenqIO.setChdrnum(sv.chdrnum);
			checkContractNo2100();
			if (isEQ(sv.regpayfreq,"00")
			&& isEQ(t6694rec.contreq,"Y")) {
				sv.regpayfreqErr.set(errorsInner.e925);
				wsspcomn.edterror.set("Y");
			}
			if (isEQ(t6696rec.frqoride,"N")) {
				if ((isNE(chdrenqIO.getBillfreq(),sv.regpayfreq)
				&& isEQ(wsspcomn.flag,"C"))) {
					sv.regpayfreq.set(chdrenqIO.getBillfreq());
				}
				wsaaFreq2.set(sv.regpayfreq);
				wsaaFreq.set(sv.regpayfreq);
				wsaaSumins2.set(ZERO);
				if (isEQ(wsaaFreq3,0)) {
					wsaaFreq3.set(1);
				}
				if (isEQ(wsaaFreq2,0)) {
					wsaaFreq2.set(1);
				}
				compute(wsaaSumins2, 2).set((div((mult(wsaaSumins,wsaaFreq3)),wsaaFreq2)));
				sv.sumin.set(wsaaSumins2);
			}
			else {
				wsaaFreq.set(sv.regpayfreq);
			}
		}
		descIO.setDescitem(sv.regpayfreq);
		descIO.setDesctabl(tablesInner.t3590);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.frqdesc.set(descIO.getShortdesc());
		}
		else {
			sv.frqdesc.fill("?");
		}
	}

private void checkReasonDesc2045() {
	/* Read the Premium Status Description. */
	if (isNE(sv.reasoncd, SPACES)) {
		if (isEQ(sv.resndesc, SPACES)) {
			descIO.setDescitem(sv.reasoncd);
			descIO.setDesctabl(tablesInner.t5500);
			findDesc1100();
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.resndesc.set(descIO.getLongdesc());
			} else {
				sv.resndesc.fill("?");
			}
		}
	}
}
protected void validatePayee2045()
	{
		if (isEQ(sv.rgpymop,SPACES)) {
			goTo(GotoLabel.checkPercentage2050);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.rgpymop);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
		|| (isNE(sv.rgpymop,itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.rgpymopErr.set(errorsInner.g529);
			goTo(GotoLabel.checkPercentage2050);
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		if (isEQ(t6694rec.payeereq,"Y")) {
			if (isEQ(sv.payclt,SPACES)) {
				sv.payclt.set(sv.cownnum);
			}
		}
		else {
			if (isNE(sv.payclt,SPACES)) {
				sv.paycltErr.set(errorsInner.e492);
			}
		}
		if (isEQ(sv.payclt,SPACES)) {
			sv.payenme.set(SPACES);
		}
		if (isEQ(t6694rec.payeereq,"Y")) {
			if (isEQ(sv.paycltErr,SPACES)) {
				cltsIO.setClntnum(sv.payclt);
				getClientDetails1200();
				if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
				|| isNE(cltsIO.getValidflag(),1)) {
					sv.paycltErr.set(errorsInner.e335);
					sv.payenme.set(SPACES);
				}
				else {
					plainname();
					sv.payenme.set(wsspcomn.longconfname);
				}
			}
		}
	}

protected void checkPercentage2050()
	{
		if (isEQ(wsaaSumins,ZERO)) {
			sv.pymt.set(ZERO);
			sv.prcnt.set(ZERO);
			scrnparams.errorCode.set(errorsInner.g516);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkDestination2060);
		}
		if (isNE(sv.cltypeErr,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkDestination2060);
		}
		if (isNE(sv.pymt, ZERO)) {
			zrdecplrec.amountIn.set(sv.pymt);
			callRounding8000();
			if (isNE(zrdecplrec.amountOut, sv.pymt)) {
				sv.pymtErr.set(errorsInner.rfik);
			}
		}
		if (isEQ(sv.pymt,ZERO)) {
			sv.prcnt.set(t6696rec.dfclmpct);
		}
		else {
			compute(wsaaIntermed, 6).setRounded(div(sv.pymt,wsaaSumins2));
			compute(wsaaPrcnt, 6).setRounded(mult(wsaaIntermed,100));
			sv.prcnt.set(wsaaPrcnt);
		}
		if (isGT(sv.prcnt,t6696rec.mxovrpct)) {
			if (isEQ(sv.pymt,ZERO)) {
				sv.prcntErr.set(errorsInner.g515);
			}
			else {
				sv.pymtErr.set(errorsInner.g515);
			}
		}
		if (isLT(sv.prcnt,t6696rec.mnovrpct)) {
			if (isEQ(sv.pymt,ZERO)) {
				sv.prcntErr.set(errorsInner.g517);
			}
			else {
				sv.pymtErr.set(errorsInner.g517);
			}
		}
		if ((isEQ(sv.pymt,ZERO))
		&& (isEQ(sv.pymtErr,SPACES))
		&& (isEQ(sv.prcntErr,SPACES))) {
			compute(sv.pymt, 2).set((div((mult(wsaaSumins2,sv.prcnt)),100)));
		}
	}

	// CML-009
	protected void checkAdjustment2055() {
		double temp = sv.pymt.getbigdata().doubleValue()
				+ sv.adjustamt.getbigdata().doubleValue();
		sv.netclaimamt.set(temp);
		if (temp < 0) {
			// if the net claim amount is not followed regular rule,
			// then display error messages.
			sv.netclaimamtOut[varcom.pr.toInt()].set("Y");
			sv.netClaimErr.set(errorsInner.rrkv);
		}
	}

protected void checkDestination2060()
	{
		if (isEQ(t6694rec.contreq,"Y")) {
			if (isEQ(sv.destkey,SPACES)) {
				sv.destkey.set(sv.chdrnum);
			}
		}
		else {
			sv.destkey.set(SPACES);
		}
		if (isEQ(t6694rec.contreq,"Y")) {
			if (isEQ(sv.destkey,SPACES)) {
				sv.destkeyErr.set(errorsInner.e186);
			}
		}
		else {
			if (isNE(sv.destkey,SPACES)) {
				sv.destkeyErr.set(errorsInner.e492);
			}
		}
		if ((isEQ(t6694rec.contreq,"Y"))
		&& (isEQ(sv.destkeyErr,SPACES))) {
			chdrenqIO.setChdrnum(sv.destkey);
			checkContractNo2100();
		}
		if ((isEQ(sv.crtdate,varcom.vrcmMaxDate))
		|| (isEQ(sv.crtdate,SPACES))) {
			sv.crtdateErr.set(errorsInner.e186);
			goTo(GotoLabel.validateFollow2079);
		}
		else {
			if (isGT(sv.crtdate,datcon1rec.intDate)) {
				sv.crtdateErr.set(errorsInner.f073);
			}
			else {
				if (isLT(sv.crtdate,sv.occdate)) {
					sv.crtdateErr.set(errorsInner.f616);
				}
			}
		}
		datcon2rec.freqFactor.set(ZERO);
		if (isEQ(sv.firstPaydate,varcom.vrcmMaxDate)) {
			if (isNE(sv.cltype,SPACES)) {
				if (isNE(t6696rec.dfdefprd,ZERO)) {
					datcon2rec.freqFactor.set(t6696rec.dfdefprd);
					datcon2rec.frequency.set(t6696rec.freqcy01);
				}
				else {
					if (isNE(t5606rec.dfrprd,ZERO)) {
						datcon2rec.freqFactor.set(t5606rec.dfrprd);
						datcon2rec.frequency.set("12");
					}
				}
				if (isNE(datcon2rec.freqFactor,ZERO)) {
					datcon2rec.intDate2.set(ZERO);
					datcon2rec.intDate1.set(sv.crtdate);
					callProgram(Datcon2.class, datcon2rec.datcon2Rec);
					if (isNE(datcon2rec.statuz,varcom.oK)) {
						syserrrec.params.set(datcon2rec.datcon2Rec);
						syserrrec.statuz.set(datcon2rec.statuz);
						fatalError600();
					}
					sv.firstPaydate.set(datcon2rec.intDate2);
				}
				else {
					sv.firstPaydate.set(sv.crtdate);
				}
			}
		}
		else {
			if (isNE(sv.cltype,SPACES)) {
				if ((isNE(t6696rec.mndefprd,ZERO))
				&& (isNE(t6696rec.freqcy02,SPACES))) {
					datcon2rec.freqFactor.set(t6696rec.mndefprd);
					datcon2rec.frequency.set(t6696rec.freqcy02);
					datcon2rec.intDate2.set(ZERO);
					datcon2rec.intDate1.set(sv.crtdate);
					callProgram(Datcon2.class, datcon2rec.datcon2Rec);
					if (isNE(datcon2rec.statuz,varcom.oK)) {
						syserrrec.params.set(datcon2rec.datcon2Rec);
						syserrrec.statuz.set(datcon2rec.statuz);
						fatalError600();
					}
					if (isGT(datcon2rec.intDate2,sv.firstPaydate)) {
						sv.fpaydateErr.set(errorsInner.g518);
					}
				}
				else {
					if (isLT(sv.firstPaydate,sv.crtdate)) {
						sv.fpaydateErr.set(errorsInner.h132);
					}
				}
			}
			else {
				if (isLT(sv.firstPaydate,sv.crtdate)) {
					sv.fpaydateErr.set(errorsInner.h132);
				}
			}
		}
	}

protected void checkNextDate2069()
	{
		if (isNE(sv.lastPaydate,varcom.vrcmMaxDate)) {
			if ((isEQ(sv.nextPaydate,varcom.vrcmMaxDate))
			|| (isEQ(sv.nextPaydate,ZERO))) {
				sv.npaydateErr.set(errorsInner.g540);
			}
			if (isLTE(sv.nextPaydate,sv.lastPaydate)) {
				sv.npaydateErr.set(errorsInner.g537);
			}
			if (isGT(sv.nextPaydate,sv.revdte)) {
				sv.npaydateErr.set(errorsInner.g538);
			}
			if (isGT(sv.nextPaydate,sv.finalPaydate)) {
				sv.npaydateErr.set(errorsInner.g539);
			}
		}
		else {
			sv.nextPaydate.set(sv.firstPaydate);
		}
	}

protected void checkReviewDate2075()
	{
		if ((isEQ(sv.revdte,varcom.vrcmMaxDate))
		|| (isEQ(sv.revdte,ZERO))) {
			sv.revdte.set(varcom.vrcmMaxDate);
		}
		if (isNE(sv.cltype,SPACES)) {
			if (isEQ(t6696rec.revitrm,ZERO)) {
				goTo(GotoLabel.noReviewTerm2076);
			}
		}
		else {
			goTo(GotoLabel.checkPayDate2077);
		}
		if (isEQ(sv.revdte,varcom.vrcmMaxDate)) {
			datcon2rec.freqFactor.set(t6696rec.revitrm);
			datcon2rec.frequency.set(t6696rec.freqcy03);
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(sv.crtdate);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			sv.revdte.set(datcon2rec.intDate2);
		}
		datcon2rec.freqFactor.set(t6696rec.revitrm);
		datcon2rec.frequency.set(t6696rec.freqcy03);
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.intDate1.set(sv.crtdate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		if (isGT(datcon2rec.intDate2,sv.revdte)) {
			sv.revdteErr.set(errorsInner.g518);
			goTo(GotoLabel.checkPayDate2077);
		}
	}

protected void noReviewTerm2076()
	{
		if (isNE(sv.revdte,varcom.vrcmMaxDate)) {
			if (isLT(sv.revdte,sv.firstPaydate)) {
				sv.revdteErr.set(errorsInner.g530);
			}
		}
	}

protected void checkPayDate2077()
	{
		if (isNE(sv.finalPaydate,varcom.vrcmMaxDate)) {
			if (isLT(sv.finalPaydate,sv.nextPaydate)) {
				sv.epaydateErr.set(errorsInner.g519);
			}
		}
		if ((isNE(sv.anvdate,varcom.vrcmMaxDate))
		&& (isNE(sv.anvdate,ZERO))) {
			if (isLT(sv.anvdate,sv.crtdate)) {
				sv.anvdateErr.set(errorsInner.g534);
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		if ((isEQ(sv.anvdate,varcom.vrcmMaxDate))
		|| (isEQ(sv.anvdate,ZERO))) {
			if (isNE(t6696rec.inxfrq,SPACES)) {
				datcon2rec.freqFactor.set(1);
				datcon2rec.frequency.set(t6696rec.inxfrq);
				datcon2rec.intDate2.set(ZERO);
				datcon2rec.intDate1.set(sv.crtdate);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					syserrrec.statuz.set(datcon2rec.statuz);
					fatalError600();
				}
				sv.anvdate.set(datcon2rec.intDate2);
			}
			else {
				sv.anvdate.set(varcom.vrcmMaxDate);
			}
		}
	}

protected void validateFollow2079()
	{
		if ((isNE(sv.fupflg,"+"))
		&& (isNE(sv.fupflg,"X"))
		&& (isNE(sv.fupflg,SPACES))) {
			sv.fupflgErr.set(errorsInner.h118);
		}
		if ((isNE(sv.anntind,"+"))
		&& (isNE(sv.anntind,"X"))
		&& (isNE(sv.anntind,SPACES))) {
			sv.anntindErr.set(errorsInner.h118);
		}
		if ((isNE(sv.ddind,"+"))
		&& (isNE(sv.ddind,"X"))
		&& (isNE(sv.ddind,SPACES))) {
			sv.ddindErr.set(errorsInner.h118);
		}
		if ((isNE(wsaaPayclt,SPACES))
		&& (isNE(sv.payclt,SPACES))) {
			if (isNE(wsaaPayclt,sv.payclt)) {
				if (isEQ(t6694rec.bankreq,"Y")) {
					regpIO.setBankkey(SPACES);
					regpIO.setBankacckey(SPACES);
					sv.ddind.set("X");
				}
				else {
					if (isEQ(t6694rec.bankreq,"N")
					|| isEQ(t6694rec.bankreq, " ")) {
						regpIO.setBankkey(SPACES);
						regpIO.setBankacckey(SPACES);
						sv.ddind.set(SPACES);
					}
				}
			}
		}
		if ((isEQ(t6694rec.bankreq,"Y"))
		&& (isEQ(sv.ddind,SPACES))) {
			sv.ddind.set("X");
		}
		if ((isGT(sv.rgpynum,1))
		&& (isNE(wsspcomn.flag,"I"))) {
			wsaaPymt.set(ZERO);
			wsaaPymt2.set(ZERO);
			regpenqIO.setChdrcoy(regpIO.getChdrcoy());
			regpenqIO.setChdrnum(regpIO.getChdrnum());
			regpenqIO.setLife(regpIO.getLife());
			regpenqIO.setCoverage(regpIO.getCoverage());
			regpenqIO.setRider(regpIO.getRider());
			regpenqIO.setRgpynum(99999);
			regpenqIO.setStatuz(varcom.oK);
			regpenqIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi
			regpenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			regpenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
			wsaaTotPymt.set(ZERO);
			while ( !(isEQ(regpenqIO.getStatuz(),varcom.endp))) {
				accumRegp2500();
			}

			if (isNE(sv.regpayfreq,"00")) {
				compute(wsaaPymt, 2).set(add(wsaaPymt,sv.pymt));
				compute(wsaaTotPymt, 2).set(add(wsaaTotPymt,sv.pymt));
			}
			else {
				wsaaPymt.set(sv.pymt);
				wsaaTotPymt.set(sv.pymt);
			}
		}
		else {
			if (isNE(sv.regpayfreq,"00")) {
				wsaaPymt.set(sv.pymt);
			}
			else {
				wsaaPymt.set(sv.pymt);
				wsaaTotPymt.set(sv.pymt);
			}
		}
		wsaaMaxPrcntValue.set(ZERO);
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaSumins3.set(wsaaLastSumins3);
			checkFreqIntableT35907000();
			if (isEQ(wsaaErrorT3590,"Y")) {
				goTo(GotoLabel.checkForErrors2080);
			}
			if (isEQ(sv.regpayfreq,"00")) {
				wsaaFreq2.set(1);
			}
			else {
				wsaaFreq2.set(sv.regpayfreq);
			}
			wsaaFreq.set(t5606rec.benfreq);
			//ILIFE-8024
			registerRegularClaimflag = FeaConfg.isFeatureExist(regpIO.getChdrcoy().toString().trim(), registerRegularClaim_FEATURE_ID, appVars, "IT");
			if (registerRegularClaimflag && isEQ(wsaaFreq,0)) {
				wsaaFreq.set(1);
				registerRegularClaimflag= false;
			}
			//ILIFE-8024
			compute(wsaaSumins3, 2).set((div((mult(wsaaSumins3,wsaaFreq)),wsaaFreq2)));
		}
		compute(wsaaMaxPrcntValue, 2).set((div(mult(wsaaSumins3,t6696rec.mxovrpct),100)));
		if (isGT(wsaaPymt,wsaaMaxPrcntValue)) {
			sv.totalamt.set(wsaaPymt);
			sv.totalamtErr.set(errorsInner.g515);
			sv.msgclaim.set(wsaaClaimReasMess);
			sv.msgclaimOut[varcom.nd.toInt()].set(SPACES);
			sv.totalamtOut[varcom.nd.toInt()].set(SPACES);
			goTo(GotoLabel.checkForErrors2080);
		}
		else {
			sv.totalamtOut[varcom.nd.toInt()].set("Y");
			sv.msgclaimOut[varcom.nd.toInt()].set("Y");
		}
		if (isGT(wsaaTotPymt,wsaaSumins3)) {
			sv.totalamt.set(wsaaTotPymt);
			sv.totalamtErr.set(errorsInner.g527);
			sv.msgclaim.set(wsaaClaimCompMess);
			sv.msgclaimOut[varcom.nd.toInt()].set(SPACES);
			sv.totalamtOut[varcom.nd.toInt()].set(SPACES);
		}
		else {
			sv.totalamtOut[varcom.nd.toInt()].set("Y");
			sv.msgclaimOut[varcom.nd.toInt()].set("Y");
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkContractNo2100()
	{
		/*CHECK*/
		chdrenqIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if ((isNE(chdrenqIO.getStatuz(),varcom.oK))
		&& (isNE(chdrenqIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.mrnf)) {
			sv.destkeyErr.set(errorsInner.g126);
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void protectScr2400()
	{
		/*PROTECT*/
		sv.paycltOut[varcom.pr.toInt()].set("Y");
		sv.cltypeOut[varcom.pr.toInt()].set("Y");
		sv.claimevdOut[varcom.pr.toInt()].set("Y");
		sv.rgpymopOut[varcom.pr.toInt()].set("Y");
		sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
		sv.destkeyOut[varcom.pr.toInt()].set("Y");
		sv.pymtOut[varcom.pr.toInt()].set("Y");
		sv.claimcurOut[varcom.pr.toInt()].set("Y");
		sv.crtdateOut[varcom.pr.toInt()].set("Y");
		sv.revdteOut[varcom.pr.toInt()].set("Y");
		sv.fpaydateOut[varcom.pr.toInt()].set("Y");
		sv.npaydateOut[varcom.pr.toInt()].set("Y");
		sv.anvdateOut[varcom.pr.toInt()].set("Y");
		sv.canceldateOut[varcom.pr.toInt()].set("Y");
		sv.epaydateOut[varcom.pr.toInt()].set("Y");
		// CML-009
		sv.adjustamtOut[varcom.pr.toInt()].set("Y");
		sv.netclaimamtOut[varcom.pr.toInt()].set("Y");
		sv.reasoncdOut[varcom.pr.toInt()].set("Y");
		sv.resndescOut[varcom.pr.toInt()].set("Y");
		//fwang3
		sv.notifinoOut[varcom.pr.toInt()].set("Y");
		sv.incurdtOut[varcom.pr.toInt()].set("Y");
		/*EXIT*/
	}

protected void accumRegp2500()
	{
			accum2510();
		}

protected void accum2510()
	{
		SmartFileCode.execute(appVars, regpenqIO);
		if ((isNE(regpenqIO.getStatuz(),varcom.oK))
		&& (isNE(regpenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regpenqIO.getParams());
			fatalError600();
		}
		if (isEQ(regpenqIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if ((isEQ(regpIO.getChdrcoy(),regpenqIO.getChdrcoy()))
		&& (isEQ(regpIO.getChdrnum(),regpenqIO.getChdrnum()))
		&& (isEQ(regpIO.getLife(),regpenqIO.getLife()))
		&& (isEQ(regpIO.getCoverage(),regpenqIO.getCoverage()))
		&& (isEQ(regpIO.getRider(),regpenqIO.getRider()))) {
			if (isEQ(sv.cltype,regpenqIO.getPayreason())) {
				if (isNE(regpIO.getRgpynum(),regpenqIO.getRgpynum())) {
					wsaaOverlap = "N";
					checkOverlap2510();
					if (isEQ(wsaaOverlap,"Y")) {
						wsaaOverlap = "N";
						wsaaFreq2.set(regpenqIO.getRegpayfreq());
						wsaaPymt2.set(regpenqIO.getPymt());
						if (isNE(wsaaFreq2,ZERO)) {
							compute(wsaaPymt2, 2).set((div((mult(wsaaPymt2,wsaaFreq2)),wsaaFreq)));
							compute(wsaaPymt, 2).set(add(wsaaPymt,wsaaPymt2));
						}
					}
				}
			}
		}
		else {
			regpenqIO.setStatuz(varcom.endp);
		}
		if ((isEQ(regpIO.getChdrcoy(),regpenqIO.getChdrcoy()))
		&& (isEQ(regpIO.getChdrnum(),regpenqIO.getChdrnum()))
		&& (isEQ(regpIO.getLife(),regpenqIO.getLife()))
		&& (isEQ(regpIO.getCoverage(),regpenqIO.getCoverage()))
		&& (isEQ(regpIO.getRider(),regpenqIO.getRider()))) {
			if (isNE(regpIO.getRgpynum(),regpenqIO.getRgpynum())) {
				wsaaOverlap = "N";
				checkOverlap2510();
				if (isEQ(wsaaOverlap,"Y")) {
					wsaaOverlap = "N";
					wsaaFreq2.set(regpenqIO.getRegpayfreq());
					wsaaPymt2.set(regpenqIO.getPymt());
					if (isNE(wsaaFreq2,ZERO)) {
						compute(wsaaPymt2, 2).set((div((mult(wsaaPymt2,wsaaFreq2)),wsaaFreq)));
						wsaaTotPymt.add(wsaaPymt2);
					}
				}
			}
		}
		regpenqIO.setFunction(varcom.nextr);
	}

protected void checkOverlap2510()
	{
		/*OVERLAP*/
		if (isEQ(regpenqIO.getCancelDate(),varcom.vrcmMaxDate)) {
			if ((isGT(sv.firstPaydate,regpenqIO.getFinalPaydate()))
			|| (isLT(sv.finalPaydate,regpenqIO.getFirstPaydate()))) {
				wsaaOverlap = "N";
			}
			else {
				wsaaOverlap = "Y";
			}
		}
		else {
			wsaaOverlap = "N";
		}
		/*EXIT*/
	}

protected void checkT66942600()
	{
			starts2600();
		}

protected void starts2600()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.rgpymop);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
		|| (isNE(sv.rgpymop,itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			sv.destkeyOut[varcom.pr.toInt()].set("Y");
			return ;
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		wsaaRgpymop.set(sv.rgpymop);
		if (isEQ(t6694rec.contreq,"N")) {
			sv.destkeyOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.destkeyOut[varcom.pr.toInt()].set(SPACES);
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
			softLock3080();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if ((isEQ(firstTime,"Y"))
		&& (isNE(wsspcomn.flag,"I"))) {
			chdrlifIO.setChdrpfx(chdrrgpIO.getChdrpfx());
			chdrlifIO.setChdrcoy(chdrrgpIO.getChdrcoy());
			chdrlifIO.setChdrnum(chdrrgpIO.getChdrnum());
			chdrlifIO.setValidflag(chdrrgpIO.getValidflag());
			chdrlifIO.setCurrfrom(chdrrgpIO.getCurrfrom());
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			chdrlifIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			chdrlifIO.setCurrto(datcon1rec.intDate);
			chdrlifIO.setValidflag("2");
			chdrlifIO.setFunction(varcom.writd);
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			if ((isNE(regpIO.getDestkey(),SPACES))
			&& (isNE(regpIO.getAprvdate(),varcom.vrcmMaxDate))) {
				setPrecision(chdrlifIO.getSinstamt05(), 2);
				chdrlifIO.setSinstamt05((mult(regpIO.getPymt(),-1)));
				setPrecision(chdrlifIO.getSinstamt06(), 2);
				chdrlifIO.setSinstamt06((add(add(add(add(chdrlifIO.getSinstamt01(),chdrlifIO.getSinstamt02()),chdrlifIO.getSinstamt03()),chdrlifIO.getSinstamt04()),chdrlifIO.getSinstamt05())));
			}
			chdrlifIO.setCurrfrom(datcon1rec.intDate);
			chdrlifIO.setCurrto(varcom.vrcmMaxDate);
			chdrlifIO.setValidflag("1");
			setPrecision(chdrlifIO.getTranno(), 0);
			chdrlifIO.setTranno(add(chdrlifIO.getTranno(),1));
			chdrlifIO.setFunction(varcom.writr);
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			payrIO.setDataArea(SPACES);
			payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
			payrIO.setChdrnum(chdrrgpIO.getChdrnum());
			payrIO.setValidflag("1");
			payrIO.setPayrseqno(wsaaPayrseqno);
			payrIO.setFormat(formatsInner.payrrec);
			payrIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(payrIO.getStatuz());
				fatalError600();
			}
			payrIO.setValidflag("2");
			payrIO.setFunction(varcom.rewrt);
			payrIO.setFormat(formatsInner.payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
			if ((isNE(regpIO.getDestkey(),SPACES))
			&& (isNE(regpIO.getAprvdate(),varcom.vrcmMaxDate))) {
				setPrecision(payrIO.getSinstamt05(), 2);
				payrIO.setSinstamt05((mult(regpIO.getPymt(),-1)));
				setPrecision(payrIO.getSinstamt06(), 2);
				payrIO.setSinstamt06((add(add(add(add(payrIO.getSinstamt01(),payrIO.getSinstamt02()),payrIO.getSinstamt03()),payrIO.getSinstamt04()),payrIO.getSinstamt05())));
			}
			payrIO.setValidflag("1");
			payrIO.setTranno(chdrlifIO.getTranno());
			payrIO.setFunction(varcom.writr);
			payrIO.setFormat(formatsInner.payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
			callChdrrgpio3600();
		}
		if ((isEQ(firstTime,"Y"))
		&& (isEQ(wsspcomn.flag,"I"))) {
			chdrlifIO.setChdrpfx(chdrrgpIO.getChdrpfx());
			chdrlifIO.setChdrcoy(chdrrgpIO.getChdrcoy());
			chdrlifIO.setChdrnum(chdrrgpIO.getChdrnum());
			chdrlifIO.setValidflag(chdrrgpIO.getValidflag());
			chdrlifIO.setCurrfrom(chdrrgpIO.getCurrfrom());
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			chdrlifIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			setPrecision(chdrlifIO.getTranno(), 0);
			chdrlifIO.setTranno(add(chdrlifIO.getTranno(),1));
		}
		if (isEQ(firstTime,"Y")) {
			firstTime = "N";
			if (isEQ(wsspcomn.flag,"M")) {
				regpIO.setValidflag("2");
				regpIO.setFormat(formatsInner.regprec);
				regpIO.setFunction(varcom.updat);
				SmartFileCode.execute(appVars, regpIO);
				if (isNE(regpIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(regpIO.getParams());
					fatalError600();
				}
				regpIO.setValidflag("1");
			}
		}
		if (isEQ(sv.fupflg,"X")) {
			moveToRegp3400();
			regpIO.setFunction(varcom.keeps);
			regpIO.setFormat(formatsInner.regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			chdrenqIO.setChdrcoy(wsspcomn.company);
			chdrenqIO.setChdrnum(sv.chdrnum);
			chdrenqIO.setFormat(formatsInner.chdrenqrec);
			chdrenqIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
			chdrenqIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(sv.ddind,"X")) {
			moveToRegp3400();
			regpIO.setFunction(varcom.keeps);
			regpIO.setFormat(formatsInner.regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		//ICIL-1546 Starts
		if(cmoth003permission) {
			if (isEQ(sv.claimnotes, "X")) {
				goTo(GotoLabel.claimNotes);
			}
			if (isEQ(sv.investres, "X")) {
				goTo(GotoLabel.investResult);
			}
		}
		//ICIL-1546 End
		readT66935200();
		if(cmoth003permission){
			updateClaimNoAndNotifiNo();//fwang3
			if(isNE(sv.notifino,SPACES) && isNE(wsspcomn.flag,"I")){
				clnnpfDAO.updateClnnpfClaimno(sv.claimno.toString(),sv.notifino.toString().replace(CLMPREFIX, ""));
			}
			if(isNE(sv.notifino,SPACES) && isNE(wsspcomn.flag,"I")){
				invspfDAO.updateInvspfClaimno(sv.claimno.toString(),sv.notifino.toString().replace(CLMPREFIX, ""));
			}
			
		}
		if (isEQ(wsspcomn.flag,"C")) {
			createRegp3100();
			//IBPLIFE-1702
			setupNotipf();
			if(notipf != null ) {
				notipfDAO.updateNotipfValidFlag(expiredFlag,notipf.getNotifinum());
				//insert new Notipf record with validFlag 1;
				notipf.setValidFlag(effectiveFlag);
				notipf.setNotifistatus(claimRegi);
				addTransnoToNotipf(wsspcomn.wsaanotificationNum.toString().trim().replace(CLMPREFIX, ""));
				notipfDAO.insertNotipf(notipf);
			}
			//end
		}
		if (isEQ(wsspcomn.flag,"M")) {
			updateRegp3200();
		}
		if ((isEQ(wsspcomn.flag,"C"))
		&& (isNE(regpIO.getPlanSuffix(),ZERO))) {
			if ((isLTE(regpIO.getPlanSuffix(),chdrrgpIO.getPolsum()))
			&& (isGT(chdrrgpIO.getPolsum(),1))) {
				callAtrqForBrkout3700();
				goTo(GotoLabel.exit3090);
			}
		}
	}
//IBPLIFE-1702
private void addTransnoToNotipf(String notifinum) {
	int newTransno;
	String maxTransnum = notipfDAO.getMaxTransno(notifinum);
	if("".equals(maxTransnum) || maxTransnum==null) {
		notipf.setTransno(String.valueOf(noTransnoCount));
	}else {
		newTransno = Integer.valueOf(maxTransnum.trim())+noTransnoCount;	//IBPLIFE-1071
		notipf.setTransno(String.valueOf(newTransno));
	}
}

/**
 * @author fwang3 
 * Update the fields
 */
private void updateClaimNoAndNotifiNo() {
	if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "M")) {
		regpIO.setClaimno(sv.claimno);
		regpIO.setClaimnotifino(sv.notifino.toString().replace(CLMPREFIX, ""));
		regpIO.setIncurdt(sv.incurdt);
	}
}


protected void setupNotipf(){
	

	wsspcomn.chdrCownnum.set(sv.lifcnum.toString());	
	wsspcomn.wsaaclaimno.set(sv.claimno.toString().trim());

	if(isEQ(sv.notifino,SPACES)){
		wsspcomn.wsaarelationship.set(SPACES);
		wsspcomn.wsaaclaimant.set(sv.lifcnum.toString());
		wsspcomn.wsaanotificationNum.set(SPACES);
	}
	else{
		notipf = notipfDAO.getNotiReByNotifin(sv.notifino.toString().replace(CLMPREFIX, ""),wsspcomn.company.toString());
		wsspcomn.wsaarelationship.set(notipf.getRelationcnum());
		wsspcomn.wsaaclaimant.set(notipf.getClaimant());
		wsspcomn.wsaanotificationNum.set(sv.notifino.toString());
	}

} 

protected void updateClaimnoInAll(){
	
	clnnpfList1 =  clnnpfDAO.getClnnpfList(wsspcomn.company.toString(), sv.notifino.toString().replace(CLMPREFIX, "").trim(),sv.claimno.toString().trim());
	if(clnnpfList1!=null && !clnnpfList1.isEmpty()){
		sv.claimnotes.set("+");
	}
	else{
		sv.claimnotes.set(SPACES);
	}
}
protected void checkInvspf(){
	
	invspfList1 =  invspfDAO.getInvspfList(wsspcomn.company.toString(), sv.notifino.toString().replace(CLMPREFIX, "").trim(),sv.claimno.toString().trim());
	if(invspfList1!=null && !invspfList1.isEmpty()){
		sv.investres.set("+");
	}
	else{
		sv.investres.set(SPACES);
	}
}

protected void softLock3080()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrrgpIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void createRegp3100()
	{
		create3110();
	}

protected void create3110()
	{
		if (isEQ(t6694rec.contreq,"Y")) {
			regpIO.setDestkey(sv.destkey);
			regpIO.setGlact(t6694rec.glact);
		}
		else {
			regpIO.setDestkey(SPACES);
			regpIO.setGlact(t6694rec.glact);
		}
		regpIO.setSacscode(t6694rec.sacscode);
		regpIO.setSacstype(t6694rec.sacstype);
		regpIO.setDebcred(t6694rec.debcred);
		regpIO.setRgpytype(t6692rec.rgpytype);
		regpIO.setTranno(chdrlifIO.getTranno());
		regpIO.setValidflag("1");
		regpIO.setRgpystat(wsaaRgpystat);
		regpIO.setRgpynum(sv.rgpynum);
		regpIO.setPayclt(sv.payclt);
		regpIO.setPayreason(sv.cltype);
		regpIO.setClaimevd(sv.claimevd);
		regpIO.setRgpymop(sv.rgpymop);
		regpIO.setRegpayfreq(sv.regpayfreq);
		regpIO.setPrcnt(sv.prcnt);
		regpIO.setPymt(sv.pymt);
		if(CMRPY005Permission){
			regpIO.setAdjamt(sv.adjustamt);
			regpIO.setNetamt(sv.netclaimamt);
			regpIO.setReasoncd(sv.reasoncd);
			regpIO.setReason(sv.resndesc);
		}
		regpIO.setCurrcd(sv.claimcur);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setCrtdate(sv.crtdate);
		regpIO.setRevdte(sv.revdte);
		regpIO.setFirstPaydate(sv.firstPaydate);
		regpIO.setAnvdate(sv.anvdate);
		regpIO.setFinalPaydate(sv.finalPaydate);
		regpIO.setCancelDate(varcom.vrcmMaxDate);
		regpIO.setAprvdate(varcom.vrcmMaxDate);
		regpIO.setNextPaydate(sv.nextPaydate);
		regpIO.setLastPaydate(varcom.vrcmMaxDate);
		regpIO.setCertdate(varcom.vrcmMaxDate);
		regpIO.setRecvdDate(varcom.vrcmMaxDate);
		// regpIO.setIncurdt(varcom.vrcmMaxDate);
		regpIO.setIncurdt(sv.incurdt);  /* IBPLIFE-9014*/
		calculateDate3500();
		regpIO.setCrtable(wsaaCrtable);
		regpIO.setPaycoy(wsspcomn.fsuco);
		regpIO.setFunction(varcom.writr);
		regpIO.setFormat(formatsInner.regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		updatPtrn3300();
	}

protected void updateRegp3200()
	{
		update3210();
	}

protected void update3210()
	{
		if (isEQ(t6694rec.contreq,"Y")) {
			regpIO.setDestkey(sv.destkey);
			regpIO.setGlact(t6694rec.glact);
		}
		else {
			regpIO.setDestkey(SPACES);
			regpIO.setGlact(t6694rec.glact);
		}
		regpIO.setSacscode(t6694rec.sacscode);
		regpIO.setSacstype(t6694rec.sacstype);
		regpIO.setDebcred(t6694rec.debcred);
		regpIO.setRgpytype(t6692rec.rgpytype);
		regpIO.setTranno(chdrlifIO.getTranno());
		if (isNE(wsaaRgpystat,SPACES)) {
			regpIO.setRgpystat(wsaaRgpystat);
		}
		regpIO.setRgpynum(sv.rgpynum);
		regpIO.setPayclt(sv.payclt);
		regpIO.setPayreason(sv.cltype);
		regpIO.setClaimevd(sv.claimevd);
		regpIO.setRgpymop(sv.rgpymop);
		regpIO.setRegpayfreq(sv.regpayfreq);
		regpIO.setPrcnt(sv.prcnt);
		regpIO.setPymt(sv.pymt);
		if(CMRPY005Permission){
			regpIO.setAdjamt(sv.adjustamt);
			regpIO.setNetamt(sv.netclaimamt);
			regpIO.setReasoncd(sv.reasoncd);
			regpIO.setReason(sv.resndesc);
		}
		regpIO.setCurrcd(sv.claimcur);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setCrtdate(sv.crtdate);
		regpIO.setRevdte(sv.revdte);
		regpIO.setFirstPaydate(sv.firstPaydate);
		regpIO.setNextPaydate(sv.nextPaydate);
		regpIO.setAnvdate(sv.anvdate);
		regpIO.setFinalPaydate(sv.finalPaydate);
		regpIO.setCancelDate(varcom.vrcmMaxDate);
		regpIO.setAprvdate(varcom.vrcmMaxDate);
		regpIO.setRecvdDate(varcom.vrcmMaxDate);
		regpIO.setIncurdt(varcom.vrcmMaxDate);
		regpIO.setFunction(varcom.writr);
		regpIO.setFormat(formatsInner.regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		updatPtrn3300();
	}

protected void updatPtrn3300()
	{
		updat3310();
	}

protected void updat3310()
	{
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrrgpIO.getChdrnum());
		ptrnIO.setRecode(chdrrgpIO.getRecode());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTerm);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setBatccoy(wsspcomn.company);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setPrtflg(SPACES);
		ptrnIO.setValidflag("1");
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void moveToRegp3400()
	{
		moveToRegp3410();
	}

protected void moveToRegp3410()
	{
		regpIO.setDestkey(sv.destkey);
		regpIO.setRgpynum(sv.rgpynum);
		regpIO.setPayclt(sv.payclt);
		regpIO.setPayreason(sv.cltype);
		regpIO.setClaimevd(sv.claimevd);
		regpIO.setRgpymop(sv.rgpymop);
		regpIO.setRegpayfreq(sv.regpayfreq);
		regpIO.setPrcnt(sv.prcnt);
		regpIO.setPymt(sv.pymt);
		if(CMRPY005Permission){
			regpIO.setAdjamt(sv.adjustamt);
			regpIO.setNetamt(sv.netclaimamt);
			regpIO.setReasoncd(sv.reasoncd);
			regpIO.setReason(sv.resndesc);
		}
		regpIO.setCurrcd(sv.claimcur);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setCrtdate(sv.crtdate);
		regpIO.setRevdte(sv.revdte);
		regpIO.setFirstPaydate(sv.firstPaydate);
		regpIO.setAnvdate(sv.anvdate);
		regpIO.setFinalPaydate(sv.finalPaydate);
		regpIO.setCancelDate(sv.cancelDate);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setNextPaydate(sv.nextPaydate);
		regpIO.setLastPaydate(sv.lastPaydate);
		regpIO.setCrtable(wsaaCrtable);
		regpIO.setTranno(chdrlifIO.getTranno());
	}

protected void calculateDate3500()
	{
		/*START*/
		datcon2rec.intDate1.set(sv.crtdate);
		if (isNE(t6625rec.frequency,SPACES)) {
			datcon2rec.freqFactor.set(1);
			datcon2rec.frequency.set(t6625rec.frequency);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			regpIO.setCertdate(datcon2rec.intDate2);
		}
		/*EXIT*/
	}

protected void callChdrrgpio3600()
	{
		start3601();
	}

protected void start3601()
	{
		chdrrgpIO.setFormat(formatsInner.chdrrgprec);
		chdrrgpIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if ((isNE(chdrrgpIO.getStatuz(),varcom.oK))
		&& (isNE(chdrrgpIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		chdrrgpIO.setPolsum(wsaaPolsum);
		chdrrgpIO.setPolinc(wsaaPolinc);
		chdrrgpIO.setFormat(formatsInner.chdrrgprec);
		chdrrgpIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if ((isNE(chdrrgpIO.getStatuz(),varcom.oK))
		&& (isNE(chdrrgpIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
	}

protected void callAtrqForBrkout3700()
	{
		start3800();
	}

protected void start3800()
	{
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrrgpIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if ((isNE(sftlockrec.statuz,varcom.oK))
		&& (isNE(sftlockrec.statuz,"LOCK"))) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5187AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTerm);
		atreqrec.reqDate.set(datcon1rec.intDate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrrgpIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTerm);
		wsaaBrkoutPlanSuffix.set(regpIO.getPlanSuffix());
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case popUp4050: {
					popUp4050();
				}
				case investResult:
					investResult4060();
				case claimNotes:
					claimNotes4070();
				case gensww4010: {
					gensww4010();
				}
				case nextProgram4020: {
					nextProgram4020();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}
		if (isEQ(sv.fupflg,"?")) {
			if (isEQ(sv.ddind,"X")) {
				regpIO.setFunction(varcom.keeps);
				regpIO.setFormat(formatsInner.regprec);
				SmartFileCode.execute(appVars, regpIO);
				if (isNE(regpIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(regpIO.getParams());
					fatalError600();
				}
				goTo(GotoLabel.popUp4050);
			}
			else {
				checkFollowUp5000();
			}
		}
		if (isEQ(sv.ddind,"?")) {
			checkBankDetails4400();
		}
		if (isEQ(sv.anntind,"?")) {
			checkAnnyDetails6000();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
	}


protected void investResult4060()
{
	
	if ((isEQ(sv.investres, "X")))  {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			saveProgramStack4200();
		}
	}
	gensswrec.function.set(SPACES);
	
	 if (isEQ(sv.investres, "?")) {
		 checkInvspf();
		}
	
	if (isEQ(sv.investres, "X")) {
		sv.investres.set("?");
		setupNotipf();
		gensswrec.function.set("E");
		goTo(GotoLabel.gensww4010);
	}

	
	
}


protected void claimNotes4070()
{
	
	if ((isEQ(sv.claimnotes, "X")))  {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			saveProgramStack4200();
		}
	}
	gensswrec.function.set(SPACES);
	
	if (isEQ(sv.claimnotes, "?")) {
		updateClaimnoInAll();
	}
	
	if (isEQ(sv.claimnotes, "X")) {
		sv.claimnotes.set("?");
		setupNotipf();
		gensswrec.function.set("F");
		goTo(GotoLabel.gensww4010);
	}
	
	
}


protected void popUp4050()
	{
		if ((isEQ(sv.ddind,"X"))
		|| (isEQ(sv.fupflg,"X"))
		|| (isEQ(sv.anntind,"X"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				saveProgramStack4200();
			}
		}
		gensswrec.function.set(SPACES);
		if (isEQ(sv.fupflg,"X")) {
			sv.fupflg.set("?");
			gensswrec.function.set("B");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.ddind,"X")) {
			sv.ddind.set("?");
			gensswrec.function.set("A");
			goTo(GotoLabel.gensww4010);
		}
		if (isEQ(sv.anntind,"X")) {
			sv.anntind.set("?");
			gensswrec.function.set("C");
			chdrmjaIO.setChdrcoy(wsspcomn.company);
			chdrmjaIO.setChdrnum(sv.chdrnum);
			chdrmjaIO.setFormat(formatsInner.chdrmjarec);
			chdrmjaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			chdrmjaIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			covrmjaIO.setChdrcoy(regpIO.getChdrcoy());
			covrmjaIO.setChdrnum(regpIO.getChdrnum());
			covrmjaIO.setLife(regpIO.getLife());
			covrmjaIO.setCoverage(regpIO.getCoverage());
			covrmjaIO.setRider(regpIO.getRider());
			covrmjaIO.setPlanSuffix(regpIO.getPlanSuffix());
			covrmjaIO.setFunction(varcom.readr);
			covrmjaIO.setFormat(formatsInner.covrmjarec);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			covrmjaIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.gensww4010);
		}
	}

protected void gensww4010()
	{
		if (isEQ(gensswrec.function,SPACES)) {
			goTo(GotoLabel.nextProgram4020);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz,varcom.oK))
		&& (isNE(gensswrec.statuz,varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
		}
	}

protected void nextProgram4020()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void checkBankDetails4400()
	{
		bank4400();
	}

protected void bank4400()
	{
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		regpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		if ((isEQ(regpIO.getBankkey(),SPACES))
		&& (isEQ(regpIO.getBankacckey(),SPACES))) {
			sv.ddind.set(SPACES);
		}
		else {
			sv.ddind.set("+");
			wsaaPayclt.set(sv.payclt);
		}
	}

protected void checkFollowUp5000()
	{
		follow5010();
	}

protected void follow5010()
	{
		fluprgpIO.setChdrcoy(wsspcomn.company);
		fluprgpIO.setChdrnum(chdrrgpIO.getChdrnum());
		wsaaRgpynum.set(regpIO.getRgpynum());
		wsaaClamnumFill.set(ZERO);
		fluprgpIO.setClamnum(wsaaClamnum2);
		fluprgpIO.setFupno(0);
		fluprgpIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		fluprgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluprgpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","CLAMNUM");
		SmartFileCode.execute(appVars, fluprgpIO);
		if ((isNE(fluprgpIO.getStatuz(),varcom.oK))
		&& (isNE(fluprgpIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
		if ((isEQ(wsspcomn.company,fluprgpIO.getChdrcoy()))
		&& (isEQ(chdrrgpIO.getChdrnum(),fluprgpIO.getChdrnum()))
		&& (isEQ(fluprgpIO.getClamnum(),wsaaClamnum2))
		&& (isNE(fluprgpIO.getStatuz(),varcom.endp))) {
			sv.fupflg.set("+");
		}
		else {
			sv.fupflg.set(SPACES);
		}
	}

protected void readT56065100()
	{
		read5110();
	}

protected void read5110()
	{
		wsaaT5606Edtitm.set(wsaaEdtitm);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t5606);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT5606Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t5606, itdmIO.getItemtabl()))
		|| (isNE(wsaaT5606Key,itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setGenarea(SPACES);
		}
		t5606rec.t5606Rec.set(itdmIO.getGenarea());
	}

protected void readT66935200()
	{
			readT66935210();
		}

protected void readT66935210()
	{
		if (isEQ(wsspcomn.flag,"M")) {
			wsaaT6693Rgpystat.set(regpIO.getRgpystat());
		}
		else {
			wsaaT6693Rgpystat.set("**");
		}
		wsaaT6693Crtable.set(covrmjaIO.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6693);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6693Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t6693, itdmIO.getItemtabl()))
		|| (isNE(wsaaT6693Key,itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			wsaaT6693Crtable.set("****");
			readT66935220();
			if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
			|| (isNE(tablesInner.t6693, itdmIO.getItemtabl()))
			|| (isNE(wsaaT6693Key,itdmIO.getItemitem()))
			|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
				itdmIO.setStatuz(varcom.endp);
				itdmIO.setGenarea(SPACES);
				return ;
			}
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaRgpystat.set(SPACES);
		index1.set(1);
		while ( !(isGT(index1,12))) {
			findStatus5210();
		}

	}

protected void findStatus5210()
	{
		/*FIND-STATUS*/
		if (isEQ(t6693rec.trcode[index1.toInt()],wsaaBatckey.batcBatctrcde)) {
			wsaaRgpystat.set(t6693rec.rgpystat[index1.toInt()]);
			index1.set(15);
		}
		index1.add(1);
		/*EXIT*/
	}

protected void readT66935220()
	{
		readAgain5225();
	}

protected void readAgain5225()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6693);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6693Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
	}

protected void checkAnnyDetails6000()
	{
		readAnny6010();
	}

protected void readAnny6010()
	{
		annyIO.setChdrcoy(regpIO.getChdrcoy());
		annyIO.setChdrnum(regpIO.getChdrnum());
		annyIO.setLife(regpIO.getLife());
		annyIO.setCoverage(regpIO.getCoverage());
		annyIO.setRider(regpIO.getRider());
		annyIO.setPlanSuffix(regpIO.getPlanSuffix());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(),varcom.oK)) {
			sv.anntind.set("+");
		}
		else {
			sv.anntind.set(" ");
			sv.anntindOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkFreqIntableT35907000()
	{
			start7010();
		}

protected void start7010()
	{
		wsaaErrorT3590.set("N");
		descIO.setDescitem(sv.regpayfreq);
		descIO.setDesctabl(tablesInner.t3590);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isEQ(descIO.getStatuz(),varcom.mrnf))) {
			sv.regpayfreqErr.set(errorsInner.h216);
			wsspcomn.edterror.set("Y");
			wsaaErrorT3590.set("Y");
			return ;
		}
		if ((isNE(descIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.claimcur);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData e492 = new FixedLengthStringData(4).init("E492");
	private FixedLengthStringData e925 = new FixedLengthStringData(4).init("E925");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData g126 = new FixedLengthStringData(4).init("G126");
	private FixedLengthStringData g514 = new FixedLengthStringData(4).init("G514");
	private FixedLengthStringData g515 = new FixedLengthStringData(4).init("G515");
	private FixedLengthStringData g516 = new FixedLengthStringData(4).init("G516");
	private FixedLengthStringData g517 = new FixedLengthStringData(4).init("G517");
	private FixedLengthStringData g518 = new FixedLengthStringData(4).init("G518");
	private FixedLengthStringData g519 = new FixedLengthStringData(4).init("G519");
	private FixedLengthStringData g520 = new FixedLengthStringData(4).init("G520");
	private FixedLengthStringData g521 = new FixedLengthStringData(4).init("G521");
	private FixedLengthStringData g522 = new FixedLengthStringData(4).init("G522");
	private FixedLengthStringData g527 = new FixedLengthStringData(4).init("G527");
	private FixedLengthStringData g528 = new FixedLengthStringData(4).init("G528");
	private FixedLengthStringData g529 = new FixedLengthStringData(4).init("G529");
	private FixedLengthStringData g530 = new FixedLengthStringData(4).init("G530");
	private FixedLengthStringData g534 = new FixedLengthStringData(4).init("G534");
	private FixedLengthStringData g537 = new FixedLengthStringData(4).init("G537");
	private FixedLengthStringData g538 = new FixedLengthStringData(4).init("G538");
	private FixedLengthStringData g539 = new FixedLengthStringData(4).init("G539");
	private FixedLengthStringData g540 = new FixedLengthStringData(4).init("G540");
	private FixedLengthStringData h118 = new FixedLengthStringData(4).init("H118");
	private FixedLengthStringData h132 = new FixedLengthStringData(4).init("H132");
	private FixedLengthStringData h136 = new FixedLengthStringData(4).init("H136");
	private FixedLengthStringData h216 = new FixedLengthStringData(4).init("H216");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
	private FixedLengthStringData e493 = new FixedLengthStringData(4).init("E493");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData rrkv = new FixedLengthStringData(4).init("RRKV");
	private FixedLengthStringData rrsn = new FixedLengthStringData(4).init("RRSN");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3629 = new FixedLengthStringData(5).init("T3629");
	private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5400 = new FixedLengthStringData(5).init("T5400");
	private FixedLengthStringData t6625 = new FixedLengthStringData(5).init("T6625");
	private FixedLengthStringData t6690 = new FixedLengthStringData(5).init("T6690");
	private FixedLengthStringData t6691 = new FixedLengthStringData(5).init("T6691");
	private FixedLengthStringData t6692 = new FixedLengthStringData(5).init("T6692");
	private FixedLengthStringData t6693 = new FixedLengthStringData(5).init("T6693");
	private FixedLengthStringData t6694 = new FixedLengthStringData(5).init("T6694");
	private FixedLengthStringData t6696 = new FixedLengthStringData(5).init("T6696");
	private FixedLengthStringData t5500 = new FixedLengthStringData(5).init("T5500");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData chdrrgprec = new FixedLengthStringData(10).init("CHDRRGPREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC   ");
	private FixedLengthStringData regprec = new FixedLengthStringData(10).init("REGPREC   ");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
}
}
