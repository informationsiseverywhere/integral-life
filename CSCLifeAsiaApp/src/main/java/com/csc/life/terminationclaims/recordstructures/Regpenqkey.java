package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:43
 * Description:
 * Copybook name: REGPENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regpenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regpenqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData regpenqKey = new FixedLengthStringData(256).isAPartOf(regpenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData regpenqChdrcoy = new FixedLengthStringData(1).isAPartOf(regpenqKey, 0);
  	public FixedLengthStringData regpenqChdrnum = new FixedLengthStringData(8).isAPartOf(regpenqKey, 1);
  	public FixedLengthStringData regpenqLife = new FixedLengthStringData(2).isAPartOf(regpenqKey, 9);
  	public FixedLengthStringData regpenqCoverage = new FixedLengthStringData(2).isAPartOf(regpenqKey, 11);
  	public FixedLengthStringData regpenqRider = new FixedLengthStringData(2).isAPartOf(regpenqKey, 13);
  	public PackedDecimalData regpenqRgpynum = new PackedDecimalData(5, 0).isAPartOf(regpenqKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(regpenqKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regpenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regpenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}