package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.general.tablestructures.T5611rec;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.KjodpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.KjohpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Kjodpf;
import com.csc.life.terminationclaims.dataaccess.model.Kjohpf;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.recordstructures.UnexpiredPrmrec;
import com.csc.life.terminationclaims.screens.Sh5c2ScreenVars;
import com.csc.life.terminationclaims.screens.Sh5c3ScreenVars;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UlnkpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ulnkpf;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BaseScreenData;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Ph5c2 extends ScreenProgCS{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected BinaryData wsaaSubfchg = new BinaryData(3, 0);
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Ph5c2");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("03");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(1000);
	protected Batckey wsaaBatckey = new Batckey();
	
	private Sh5c2ScreenVars sv = ScreenProgram.getScreenVars( Sh5c2ScreenVars.class);
	private Sh5c3ScreenVars sv1 = ScreenProgram.getScreenVars( Sh5c3ScreenVars.class);
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	protected Chdrpf chdrpf = new Chdrpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	protected Descpf descpf = null;
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Payrpf payrpf = null;
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private UlnkpfDAO ulnkpfDAO = getApplicationContext().getBean("ulnkpfDAO", UlnkpfDAO.class);
	private Ulnkpf ulnkpf=new Ulnkpf();
	protected Iterator<Lifepf> lifeIterator;
	private boolean CMOTH003Permission  = false;
	private static final String feaConfigClmNotify= "CMOTH003";
	protected LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	protected Lifepf lifepf = new Lifepf();	
	protected ClntpfDAO clntpfDao = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	protected Clntpf cltspf = new Clntpf();
	private static final String SPLIT_SIGN = "_";
	protected Deathrec deathrec = getDeathrec();
	private Alocnorec alocnorec = new Alocnorec();
	private StringUtil stringUtil = new StringUtil();
	private static final String e304 = "E304";
	boolean CMDTH010Permission = false;
	private static final String feaConfigPreRegistartion= "CMDTH010";
	protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	protected Cattpf cattpf = new Cattpf();	
	protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected List<Covrpf> covrpfList;
	protected Covrpf covrpf = null;
	protected ItemDAO itempfDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected List<Itempf> itempfList;
	protected Itempf itempf = null;
	protected boolean loopflag=false;
	protected boolean subflag=false;
	protected T5687rec t5687rec = new T5687rec();
	private T3695rec t3695rec = new T3695rec();
	private T5645rec t5645rec = new T5645rec();
	private T6598rec t6598rec = new T6598rec();
	private T5611rec t5611rec = new T5611rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected List<Lifepf> lifeList;
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private Acblpf acblpf = null;
	private LoanTableDAM loanIO = new LoanTableDAM();
	private Totloanrec totloanrec = new Totloanrec();
	private static final String loanrec = "LOANREC";
	private PackedDecimalData wsaaT5645Idx = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaBaseIdx = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaSumFlag = new FixedLengthStringData(1).init(SPACES);
	private ExternalisedRules er = new ExternalisedRules();
	private ErrorsInner errorsInner = new ErrorsInner();
	static String rncd=" ";
	static String rndesc=" ";
	static String rfOption=" ";
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private Kjohpf kjohpf=new Kjohpf();
	private Kjodpf kjodpf=new Kjodpf();
	private KjohpfDAO kjohpfDAO = getApplicationContext().getBean("kjohpfDAO", KjohpfDAO.class);
	private KjodpfDAO kjodpfDAO = getApplicationContext().getBean("kjodpfDAO", KjodpfDAO.class);
	private List<Kjodpf> listKjodpf = new ArrayList<>();
	private List<Cattpf> listcattpf = new ArrayList<>();
	private Map<String, List<Itempf>> t5540Map;
	
	private ZonedDecimalData wsaaSwitch2 = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator firstTime = new Validator(wsaaSwitch2, "1");
	private ItdmTableDAM itdmIO = new ItdmTableDAM();

	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private FixedLengthStringData wsaaT5611Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5611Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5611Item, 0);
	private FixedLengthStringData wsaaT5611Pstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 4);
	BigDecimal totalPrem=BigDecimal.ZERO;
	BigDecimal totalCash=BigDecimal.ZERO;
	BigDecimal totalPolReserve=BigDecimal.ZERO;
	BigDecimal totalFund=BigDecimal.ZERO;
	BigDecimal total=BigDecimal.ZERO;
	private List<Covrpf> covrpfUpdateList = new ArrayList<>();
	private List<Covrpf> covrpfInsertList = new ArrayList<>();
	protected BsscTableDAM bsscIO = new BsscTableDAM(); 
	protected Sftlockrec sftlockrec = new Sftlockrec();
	//private Batckey wsaaBatchkey = new Batckey();
	int warningFlag=0;
	BigDecimal tsusamt=BigDecimal.ZERO;
	BigDecimal tunExpPrm=BigDecimal.ZERO; 
	BigDecimal tapl=BigDecimal.ZERO; 
	BigDecimal taplInt=BigDecimal.ZERO; 
	BigDecimal tpl=BigDecimal.ZERO; 
	BigDecimal tplInt=BigDecimal.ZERO; 
	
	protected WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	
	protected static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	public Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");

	public FixedLengthStringData wsaaCovrComponent = new FixedLengthStringData(8);
	public FixedLengthStringData wsaaCoverage = new FixedLengthStringData(4).isAPartOf(wsaaCovrComponent, 2);

	private FixedLengthStringData wsaaRidrComponent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(4).isAPartOf(wsaaRidrComponent, 4);
	public FixedLengthStringData lifeType = new FixedLengthStringData(1).init("L");
	private FixedLengthStringData jlifeType = new FixedLengthStringData(1).init("J");
	public FixedLengthStringData coverageType = new FixedLengthStringData(1).init("C");
	private FixedLengthStringData riderType = new FixedLengthStringData(1).init("R");
	private FixedLengthStringData wsaaStoreLifcnum = new FixedLengthStringData(8);
	public FixedLengthStringData wsaaStoreLifeno = new FixedLengthStringData(2);
	private PackedDecimalData wsaaStoreSuffix = new PackedDecimalData(5, 0);
	public FixedLengthStringData wsaaStoreCoverage = new FixedLengthStringData(2);
	
	
}

	
	public Deathrec getDeathrec() {
		return new Deathrec();
	}
	
	public Ph5c2() {
		super();
		screenVars = sv;
		new ScreenModel("Sh5c2", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}

	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) 
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	
	private enum GotoLabel implements GOTOInterface {
		exit1090, setLoanInfo, skipLoanRead, DEFAULT
	}
	
	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
	
		initialise1010();
		
	}

protected void initialise1010()
	{
		rncd=sv.rsncde.toString();
		rndesc=sv.resndesc.toString();
		CMOTH003Permission  = FeaConfg.isFeatureExist("2", feaConfigClmNotify, appVars, "IT");
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", feaConfigPreRegistartion, appVars, "IT");
		wsaaBatckey.set(wsspcomn.batchkey);
		
		sv.dataArea.set(SPACES);
	
		sv.subfileArea.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if(isEQ(sv1.efdate, SPACES)){
			sv1.efdate.set(datcon1rec.intDate);
		}
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/* Dummy subfile initalisation for prototype - replace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("Sh5c2", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Dummy field initilisation for prototype version.*/
		
		if (isEQ(wsspcomn.flag, "I")) {
			kjohpf=kjohpfDAO.getKjohpfRecord(" ", sv1.claimnmber.toString());
			if(null!=kjohpf){
				sv.chdrnum.set(kjohpf.getChdrnum());
				sv.effdate.set(kjohpf.getEffdate().toString());
			}
		}else{
			sv.chdrnum.set(sv1.chdrsel);
		}
		sv.cashvalare.set(BigDecimal.ZERO);
		sv.cashvalare.setBlankWhenZero();
		sv.fundVal.set(BigDecimal.ZERO);
		sv.fundVal.setBlankWhenZero();
		chdrpf = chdrpfDAO.getChdrpf("2", sv.chdrnum.toString());
		if(null==chdrpf) {
			fatalError600();
		}
		
		sv.cnttype.set(chdrpf.getCnttype());
		sv.occdate.set(chdrpf.getOccdate());
		sv.cownnum.set(chdrpf.getCownnum().toString());
		sv.btdate.set(chdrpf.getBtdate());
		sv.ptdate.set(chdrpf.getPtdate());
		if (isNE(wsspcomn.flag, "I")) {
			if(isEQ(sv1.efdate, SPACES)){
				sv.effdate.set(chdrpf.getCurrfrom());
			}else{
				sv.effdate.set(sv1.efdate);
			}
		}
		sv.riskcommdte.set(varcom.vrcmMaxDate);
		
		
		
		ulnkpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ulnkpf.setChdrnum(chdrpf.getChdrnum());

		int count=ulnkpfDAO.checkUlnkpf(ulnkpf);
		if(count==0){
			deathrec.effdate.set(wsspcomn.currfrom);
		}
		if(CMOTH003Permission){
			allocateNumber2600();
		}

		
		descpf = this.getDescData("T5688", chdrpf.getCnttype().trim());
		if (descpf != null) {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		
		descpf = this.getDescData("T3623", chdrpf.getStatcode().trim());
		if (descpf != null) {
			sv.rstate.set(descpf.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		
		descpf = this.getDescData("T3588", chdrpf.getPstcde().trim());
		if (descpf != null) {
			sv.pstate.set(descpf.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		
		Hpadpf hpadpf = hpadpfDAO.getHpadData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if(hpadpf!=null) {
			sv.riskcommdte.set(hpadpf.getRskcommdate());
		}
		
		lifepf = lifepfDAO.getLifeRecord(wsspcomn.company.toString().trim(), sv.chdrnum.toString().trim(), "01", "00");
		if (lifepf== null ) {
			syserrrec.params.set(lifepf);
			fatalError600();
		}
		
		List<String> clntnumList = new ArrayList();
		clntnumList.add(chdrpf.getCownnum());
		List<Lifepf> lifepfList = lifepfDAO.selectLifeDetails(lifepf);

		/* Move the last record read to the screen and get the*/
		/* description.*/
		if(lifepfList==null){
			fatalError600();
		}
		sv.lifcnum.set(lifepfList.get(0).getLifcnum());
		for(Lifepf lifepf:lifepfList){
			clntnumList.add(lifepf.getLifcnum());
			loopflag = true;
		}
		
		Map<String,Clntpf> cltspfMap = clntpfDao.searchClntRecord("CN", wsspcomn.fsuco.toString(), clntnumList);
		if(cltspfMap.containsKey(wsspcomn.fsuco.toString()+ getSplitSign() +chdrpf.getCownnum())){/* IJTI-1523 */
			cltspf = cltspfMap.get(wsspcomn.fsuco.toString()+ getSplitSign() +chdrpf.getCownnum());/* IJTI-1523 */
			plainname();
			sv.ownername.set(wsspcomn.longconfname);}
		else{
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}

		if(cltspfMap.containsKey(wsspcomn.fsuco.toString()+ getSplitSign() +lifepfList.get(0).getLifcnum())){
			cltspf = cltspfMap.get(wsspcomn.fsuco.toString()+ getSplitSign() +lifepfList.get(0).getLifcnum());
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		else{
			sv.linsnameErr.set(e304);//ILB-459
			sv.linsname.set(SPACES);
		}
		
		if(CMDTH010Permission){
			checkPreRegistration();
		}		

		lifeList = lifepfDAO.getLifeRecords(wsspcomn.company.toString(), chdrpf.getChdrnum(), "1");
		if((null!=lifeList) && (!lifeList.isEmpty())){
			lifeIterator = lifeList.iterator();
			if(lifeIterator.hasNext()) {
				lifepf = lifeIterator.next();
				loopflag = true;
			}
		}
		srcalcpy.status.set(SPACES);
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5645");
		itempf.setItemitem(wsaaProg.toString());
	    itempf.setItemseq("  ");
		itempf = itempfDAO.getItemRecordByItemkey(itempf);
		
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5645").concat(wsaaProg.toString()));
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T3695");
		itempf.setItemitem(t5645rec.sacstype05.toString().trim());
		itempf = itempfDAO.getItemRecordByItemkey(itempf);
		
		if (itempf==null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T3695").concat(t5645rec.sacstype01.toString()));
			fatalError600();
		}
		t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		
		while (isEQ(loopflag, true)) {
			processLife1100();
		}
		
		scrnparams.subfileRrn.set(1);
		wsaaSub.set(1);
		sv.totalPrem.set(totalPrem);
		sv.tFundVal.set(totalFund);
		sv.tCashval.set(totalCash);
		sv.tPolicyReserve.set(totalPolReserve);
		sv.refundOption.set(rfOption);

		sv.resndesc.setEnabled(BaseScreenData.ENABLED);
		sv.rsncde.setEnabled(BaseScreenData.ENABLED);
		sv.refundOption.setEnabled(BaseScreenData.ENABLED);
		sv.tPolicyReserve.setEnabled(BaseScreenData.ENABLED);
		
		/* Check the presence of existing loans and the values of*/
		/* those loans*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy());
		totloanrec.chdrnum.set(sv.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.function.set("LOAN");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		
		/* Read T5645 for finanical accounting rules.*/
		itempf = new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(chdrpf.getChdrcoy().toString());
		itempf.setItemtabl("T5645");
		itempf.setItemitem("HRTOTLON");
	    itempf = itempfDAO.getItempfRecord(itempf);
		if (itempf == null) {
			syserrrec.statuz.set("H134");
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				
		loanIO.setParams(SPACES);
		loanIO.setChdrcoy(chdrpf.getChdrcoy());
		loanIO.setChdrnum(chdrpf.getChdrnum());
		loanIO.setLoanNumber(0);
		loanIO.setFormat(loanrec);
		loanIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		loanIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		if (isGT(totloanrec.loanCount, 0)) {
			readLoan1500();
			processloanInfo1600();
		}
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5645");
		itempf.setItemitem(wsaaProg.toString());
	    itempf.setItemseq("  ");
		itempf = itempfDAO.getItemRecordByItemkey(itempf);
		
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5645").concat(wsaaProg.toString()));
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		    itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl("T3695");
			itempf.setItemitem(t5645rec.sacstype05.toString().trim());
			itempf = itempfDAO.getItemRecordByItemkey(itempf);
			
			if (itempf==null) {
				syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T3695").concat(t5645rec.sacstype01.toString()));
				fatalError600();
			}
			t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			
		
		getContractSuspense();
		getUnexpiredPremium();		
		if(isEQ(wsspcomn.flag, "I"))
		{
			kjohpf = kjohpfDAO.getKjohpfRecord(chdrpf.getChdrnum(), " ");
			sv.refundOption.set(kjohpf.getRefundoption());
			sv.policyloan.set(kjohpf.getPolicyloan());
			sv.policyloanInterest.set(kjohpf.getPlint01());
			sv.apl.set(kjohpf.getAplamt01());
			sv.aplInterest.set(kjohpf.getAplint01());
			sv.totalPrem.set(kjohpf.getPrempaid());
			sv.tCashval.set(kjohpf.getSurrval01());
			sv.tPolicyReserve.set(kjohpf.getSurrval02());
			sv.tFundVal.set(kjohpf.getSurrval03());
			sv.susamt.set(kjohpf.getPremsusp());
			sv.unexpiredprm.set(kjohpf.getWuepamt());
			sv.total.set(kjohpf.getTotal());
			sv.rsncde.set(kjohpf.getReasoncd());
			sv.resndesc.set(kjohpf.getResndesc());
			sv.claimnumber.set(kjohpf.getClaim());
			sv.claimTyp.set(kjohpf.getClaimType());
			sv.effdate.set(kjohpf.getEffdate());
			
		}
		
	}
protected void readT5687()
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemitem(covrpf.getCrtable());/* IJTI-1386 */
	itempf.setItemtabl("T5687");
	itempf.setItmfrm(new BigDecimal(covrpf.getCrrcd()));
	itempf.setItmto(new BigDecimal(covrpf.getCrrcd()));

	itempfList = itempfDAO.findByItemDates(itempf);
	if(itempfList.size() > 0) {
		for (Itempf it : itempfList) {				
			t5687rec.t5687Rec.set(StringUtil.rawToString(it.getGenarea()));
			
		}
	} 
	
	itempf = new Itempf();
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl("T6598");
	itempf.setItempfx("IT");
	itempf.setItemitem(t5687rec.svMethod.toString());
	itempf = itempfDAO.getItempfRecord(itempf);
	
	if(null!=itempf){
		t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}else{
		t6598rec.t6598Rec.set(SPACES);
	}
	
}

protected void getValues()
{
	readT5687();
		
		chdrsurIO.setChdrcoy(wsspcomn.company);
		chdrsurIO.setChdrnum(sv.chdrnum);
		chdrsurIO.setFunction(varcom.readr);
		if (isNE(sv.chdrnum, SPACES)) {
			SmartFileCode.execute(appVars, chdrsurIO);
		}
		else {
			chdrsurIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)
		&& isNE(chdrsurIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
	srcalcpy.chdrCurr.set(chdrsurIO.getCntcurr());
	srcalcpy.effdate.set(sv.effdate);
	srcalcpy.ptdate.set(chdrsurIO.getPtdate());
	srcalcpy.type.set("F");
	/* IF COVRCLM-INSTPREM         > ZERO                           */
	srcalcpy.singp.set(covrpf.getInstprem());
	/* ELSE                                                         */
	/*    MOVE COVRCLM-SINGP       TO SURC-SINGP.                   */
	/* if no surrender method found print zeros as surrender value*/
	if (isEQ(t6598rec.calcprog, SPACES)){
		
		sv.cashvalare.set(ZERO);
		sv.fundVal.set(ZERO);
		
		srcalcpy.status.set(varcom.endp);
		/*     GO TO 1630-ADD-TO-SUBFILE.                               */
		goTo(GotoLabel.exit1090);
	}
	
	/*IVE-797 RUL Product - Full Surrender Calculation started*/
	//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
	/*ILIFE-7548 start*/
	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())&& er.isExternalized(chdrsurIO.getCnttype().toString(), srcalcpy.crtable.toString())))  
	{
		srcalcpy.surrCalcMeth.set(t5687rec.svMethod);
		srcalcpy.cnttype.set(chdrsurIO.getCnttype());
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		callProgramX(t6598rec.calcprog, srcalcpy.surrenderRec);
	}
	/*ILIFE-7548 end*/
	else
	{
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
		Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

		vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
		vpxsurcrec.function.set("INIT");
		callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
		srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
		vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
		vpmfmtrec.initialize();
		vpmfmtrec.amount02.set(wsaaEstimateTot);			

		callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrsurIO);//VPMS call
		
		if(isEQ(srcalcpy.type,"L"))
		{
			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
			callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			srcalcpy.status.set(varcom.endp);
		}
		else if(isEQ(srcalcpy.type,"C"))
		{
			srcalcpy.status.set(varcom.endp);
		}
		else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
		{
			srcalcpy.endf.set("Y");
		}
		else
		{
			srcalcpy.status.set(varcom.oK);
		}
		/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(vpxsurcrec.statuz.equals(varcom.endp))
			srcalcpy.status.set(varcom.endp);
		/* ILIFE-3142 End*/
	}
	
	/*IVE-797 RUL Product - Full Surrender Calculation end*/
	if (isEQ(srcalcpy.status, varcom.bomb)) {
		syserrrec.statuz.set(srcalcpy.status);
		fatalError600();
	}
	if (isNE(srcalcpy.status, varcom.oK)
	&& isNE(srcalcpy.status, varcom.endp)
	&& isNE(srcalcpy.status, "NOPR") && isNE(srcalcpy.status, varcom.mrnf)) {
		syserrrec.statuz.set(srcalcpy.status);
		fatalError600();
	}
	
	if (isEQ(srcalcpy.status, varcom.oK)
	|| isEQ(srcalcpy.status, varcom.endp)) {
		if (isEQ(srcalcpy.currcode, SPACES)) {
			srcalcpy.currcode.set(chdrsurIO.getCntcurr());
		}
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		zrdecplrec.currency.set(srcalcpy.currcode);
		callRounding6000();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
		zrdecplrec.currency.set(srcalcpy.currcode);
		callRounding6000();
		srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
	}
	if (isEQ(srcalcpy.type, "C")) {
		compute(srcalcpy.actualVal, 2).set(mult(srcalcpy.actualVal, -1));
	}
	/* Check the amount returned for being negative. In the case of    */
	/* SUM products this is possible and so set these values to zero.  */
	/* Note SUM products do not have to have the same PT & BT dates.   */
	checkT56112700();
	if (isNE(srcalcpy.type, "E")) {
		if (isNE(wsaaSumFlag, SPACES)) {
			if (isNE(sv.ptdate, sv.btdate)) {
				sv.ptdateErr.set(SPACES);
				sv.btdateErr.set(SPACES);
			}
			if (isLT(srcalcpy.actualVal, ZERO)) {
				srcalcpy.actualVal.set(ZERO);
			}
		}
	}
	/*    IF SURC-ENDF  = 'Y'                                          */
	/*       GO TO 1640-EXIT.                                          */
	if (isEQ(srcalcpy.estimatedVal, ZERO)
	&& isEQ(srcalcpy.actualVal, ZERO)) {
		goTo(GotoLabel.exit1090);
	}
	
	
	sv.fundVal.set(srcalcpy.estimatedVal);
	sv.cashvalare.set(srcalcpy.actualVal);
	
	sv.effdate.set(srcalcpy.effdate);
	/*  IF S5026-SHORTDS         NOT = COVRCLM-CRTABLE       <A06843>*/
	/*      MOVE SPACES             TO S5026-RIIND           <A06843>*/
	/*  END-IF.                                              <A06843>*/
	/*  If the description is 'PENALTY', as set up in subroutine       */
	/*  UNLSURC then subract this amount to give a true actual value.  */
	/* IF FIRST-TIME                                                */
	/*    MOVE 0                   TO WSAA-SWITCH2                  */
	/*    MOVE S5026-CNSTCUR       TO WSAA-STORED-CURRENCY          */
	/*    ADD SURC-ESTIMATED-VAL   TO  WSAA-ESTIMATE-TOT            */
	/*    ADD SURC-ACTUAL-VAL      TO  WSAA-ACTUAL-TOT              */
	/* ELSE                                                         */
	/*    IF SURC-CURRCODE            = WSAA-STORED-CURRENCY           */
	/* IF S5026-CNSTCUR            = WSAA-STORED-CURRENCY      <010>*/
	/*    ADD SURC-ESTIMATED-VAL   TO  WSAA-ESTIMATE-TOT            */
	/*    ADD SURC-ACTUAL-VAL      TO  WSAA-ACTUAL-TOT              */
	/* ELSE                                                         */
	/*    MOVE ZEROES              TO WSAA-ESTIMATE-TOT             */
	/*                                WSAA-ACTUAL-TOT               */
	/*      MOVE 1                 TO WSAA-CURRENCY-SWITCH.         */
	if (firstTime.isTrue()) {
		wsaaSwitch2.set(0);
		wsaaStoredCurrency.set(covrpf.getPremCurrency());
		wsaaEstimateTot.add(srcalcpy.estimatedVal);
		
	}
	else {
		if (isEQ(covrpf.getPremCurrency(), wsaaStoredCurrency)) {
			wsaaEstimateTot.add(srcalcpy.estimatedVal);
		
		}
		else {
			wsaaEstimateTot.set(ZERO);
		}
	}
	
}

protected void checkT56112700()
{
	start2710();
}

protected void start2710()
{
	/* Read the Coverage Surrender/Paid-Up/Bonus parameter table.      */
	itdmIO.setDataArea(SPACES);
	itdmIO.setItemtabl("T5611");
	itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
	wsaaT5611Crtable.set(srcalcpy.crtable);
	wsaaT5611Pstatcode.set(srcalcpy.pstatcode);
	itdmIO.setItemitem(wsaaT5611Item);
	itdmIO.setItmfrm(srcalcpy.effdate);
	itdmIO.setFunction(varcom.begn);
	//performance improvement --  Niharika Modi 
	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(itdmIO.getStatuz());
		fatalError600();
	}
	else {
		if (isNE(itdmIO.getStatuz(), varcom.endp)
		&& isEQ(itdmIO.getItemtabl(), "T5611")
		&& isEQ(itdmIO.getItemcoy(), srcalcpy.chdrChdrcoy)
		&& isEQ(itdmIO.getItemitem(), wsaaT5611Item)) {
			t5611rec.t5611Rec.set(itdmIO.getGenarea());
			wsaaSumFlag.set("Y");
		}
		else {
			wsaaSumFlag.set(SPACES);
		}
	}
}


protected void callRounding6000()
{
	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(wsspcomn.company);
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		syserrrec.params.set(zrdecplrec.zrdecplRec);
		fatalError600();
	}
	/*EXIT*/
}

private void getUnexpiredPremium() {
	
	if(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("UNEXPIREDPRM"))
	{
		UnexpiredPrmrec unexpiredPrmRec = new UnexpiredPrmrec();
		unexpiredPrmRec.cnttype.set(sv.cnttype);
		String isPrmCollected;
		if(isGT(payrpf.getPtdate(), sv.effdate) && isEQ(payrpf.getPtdate() , payrpf.getBtdate())) {
			isPrmCollected = "Y";
		}else {
			isPrmCollected = "N";
		}
		unexpiredPrmRec.prmCollectedFlag.set(isPrmCollected);
		
		ZonedDecimalData calcPremium = new ZonedDecimalData(17,2);
		if(isGT(payrpf.getEffdate(),sv.effdate)) {
			List<Payrpf> payrpflist = payrpfDAO.getPayrListByCoyAndNumAndSeq(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum().toString(), payrpf.getPayrseqno(), "2");
			for(Payrpf payrpf1: payrpflist) {
				if(isLT(payrpf1.getEffdate(),sv.effdate)) {
					calcPremium.set(payrpf1.getSinstamt06());
					unexpiredPrmRec.frequency.set(payrpf1.getBillfreq());
				}
			}
		}else {
			calcPremium.set(payrpf.getSinstamt06());
			unexpiredPrmRec.frequency.set(payrpf.getBillfreq());
		}

		unexpiredPrmRec.calcPremium.set(calcPremium);
		unexpiredPrmRec.sccDate.set(sv.occdate);
		unexpiredPrmRec.sEffDate.set(sv.effdate);
		unexpiredPrmRec.sptDate.set(sv.ptdate);
		callProgram("UNEXPIREDPRM",unexpiredPrmRec.unexpiredPrmRec);
		sv.unexpiredprm.set(unexpiredPrmRec.unexpiredPrmOut);
		tunExpPrm=sv.unexpiredprm.getbigdata();
	}
}

protected void getContractSuspense()
{
	BigDecimal suspenseLPS=BigDecimal.ZERO;
	BigDecimal suspenseLPSk=BigDecimal.ZERO;
	acblpf = acblDao.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode05.toString(), chdrpf.getChdrnum(), payrpf.getBillcurr(), t5645rec.sacstype05.toString());
	
	if (acblpf == null) {
		sv.susamt.set(ZERO);
	}
	else {
		if (isEQ(t3695rec.sign,"-")) {
			compute(sv.susamt, 2).set(mult(acblpf.getSacscurbal(),-1));
		}
		else {
			sv.susamt.set(acblpf.getSacscurbal());
		}
	}
	suspenseLPS=sv.susamt.getbigdata();
	sv.susamt.set(BigDecimal.ZERO);
	
	acblpf = acblDao.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode06.toString(), chdrpf.getChdrnum(), payrpf.getBillcurr(), t5645rec.sacstype06.toString());
	if (null!=acblpf) {
		if (isEQ(t3695rec.sign,"-")) {
			compute(sv.susamt, 2).set(mult(acblpf.getSacscurbal(),-1));
		}
		else {
			sv.susamt.set(acblpf.getSacscurbal());
		}
	}
	suspenseLPSk=sv.susamt.getbigdata();
	sv.susamt.set(suspenseLPS.add(suspenseLPSk));
	tsusamt=sv.susamt.getbigdata();
}

protected void processloanInfo1600()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: 
			case setLoanInfo: 
				setLoanInfo();
			case skipLoanRead: 
				skipLoanRead();
			case exit1090: 
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}
protected void setLoanInfo(){
	/* Work out Current Interest Balance.*/
	/* Initialise index to read the Accounting Rules in Table*/
	/* T5645. Currently the Accounting Rules for this process*/
	/* are stored in the following order:*/
	/*    1st 4 - Policy Loan*/
	/*    2nd 4 - APL*/
	/*    3rd 4 - Cash Deposit (Anticipated Endowment)*/
	/* Thus, we initialise the index with 0, 4, and 8 for Policy*/
	/* Loan, APL and Cash Deposit respectively depending on the*/
	/* Loan Type.*/
	/* The Loan Types are 'P for Policy Loan, 'A' for APL and 'E'*/
	/* for Cash Deposit.*/
	if (isNE(loanIO.getChdrcoy(), chdrpf.getChdrcoy())
			|| isNE(loanIO.getChdrnum(), chdrpf.getChdrnum())
			|| isNE(loanIO.getValidflag(), "1")
			|| isEQ(loanIO.getStatuz(), varcom.endp)) {
				scrnparams.subfileMore.set(SPACES);
				goTo(GotoLabel.exit1090);
	}
	if (isEQ(loanIO.getLoanType(), "P")){
		wsaaBaseIdx.set(0);
	}
	else if (isEQ(loanIO.getLoanType(), "A")){
		wsaaBaseIdx.set(4);
	}
	/*else if (isEQ(loanIO.getLoanType(), "E")){
		wsaaBaseIdx.set(8);
	}*/
	else{
		goTo(GotoLabel.skipLoanRead);
	}
	
	getInterestAcbl();
	getLoanAcbl();
}

protected void skipLoanRead()
{
	//initialize(sv.subfileFields);
	loanIO.setFunction(varcom.nextr);
	SmartFileCode.execute(appVars, loanIO);
	if (isNE(loanIO.getStatuz(), varcom.oK)
	&& isNE(loanIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(loanIO.getParams());
		fatalError600();
	}
	goTo(GotoLabel.setLoanInfo);
}

protected void getInterestAcbl(){
	
	/* Use entry 2 on the T5645 record read to get Loan interest*/
	/* ACBL for this loan number.*/
	/* COMPUTE WSAA-T5645-IDX      =  WSAA-BASE-IDX + 2.*/
	/* COMPUTE WSAA-T5645-IDX      =  WSAA-BASE-IDX + 4.            */
	compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 2));
	wsaaSacscode.set(t5645rec.sacscode[wsaaT5645Idx.toInt()]);
	wsaaSacstype.set(t5645rec.sacstype[wsaaT5645Idx.toInt()]);
	
	wsaaRldgacct.set(SPACES);
	wsaaChdrnum.set(loanIO.getChdrnum());
	wsaaLoanNumber.set(loanIO.getLoanNumber());
	
	acblpf = acblDao.loadDataByBatch(loanIO.getChdrcoy().toString(),wsaaSacscode.toString(),
			wsaaRldgacct.toString(),loanIO.getLoanCurrency().toString(),wsaaSacstype.toString());
	if (acblpf == null) {
		acblpf = new Acblpf();
		acblpf.setSacscurbal(BigDecimal.ZERO);
	}
	/*    GO TO 6090-EXIT.*/
	if (isEQ(loanIO.getLoanType(), "P"))
		sv.policyloanInterest.set(acblpf.getSacscurbal().add(sv.policyloanInterest.getbigdata()));
	else if(isEQ(loanIO.getLoanType(), "A")){
		sv.aplInterest.set(acblpf.getSacscurbal());
	}
	tplInt=sv.policyloanInterest.getbigdata();
	taplInt=sv.aplInterest.getbigdata();
}

protected void getLoanAcbl(){
		/* Use entry 1 on the T5645 record read to get Loan*/
		/* ACBL for this loan number.*/
		compute(wsaaT5645Idx, 0).set(add(wsaaBaseIdx, 1));
		wsaaSacscode.set(t5645rec.sacscode[wsaaT5645Idx.toInt()]);
		wsaaSacstype.set(t5645rec.sacstype[wsaaT5645Idx.toInt()]);
		
		wsaaRldgacct.set(SPACES);
		wsaaChdrnum.set(loanIO.getChdrnum());
		wsaaLoanNumber.set(loanIO.getLoanNumber());
		
		acblpf = acblDao.loadDataByBatch(loanIO.getChdrcoy().toString(),wsaaSacscode.toString(),
				wsaaRldgacct.toString(),loanIO.getLoanCurrency().toString(),wsaaSacstype.toString());

		if (acblpf == null) {
			fatalError600();
		}
		
		if (isEQ(loanIO.getLoanType(), "P"))
			sv.policyloan.set(acblpf.getSacscurbal().add(sv.policyloan.getbigdata()));
		else if(isEQ(loanIO.getLoanType(), "A")){
			sv.apl.set(acblpf.getSacscurbal());
		}
		
		tapl=sv.apl.getbigdata();
		tpl=sv.policyloan.getbigdata();
	}


protected void readLoan1500()
{
	/*START*/
	SmartFileCode.execute(appVars, loanIO);
	if (isNE(loanIO.getStatuz(), varcom.oK)
	&& isNE(loanIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(loanIO.getParams());
		fatalError600();
	}
	if (isNE(loanIO.getChdrcoy(), chdrpf.getChdrcoy())
	|| isNE(loanIO.getChdrnum(), chdrpf.getChdrnum())
	|| isEQ(loanIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(loanIO.getParams());
		syserrrec.statuz.set(loanIO.getStatuz());
		fatalError600();
	}
	/*EXIT*/
}

protected void processLife1100()
{
			/*    Set up the LIFE details from the previously read LIFEENQ*/
	/*    record.*/
	String tempRldgAcct;
	sv.subfileFields.set(SPACES);
	if (isEQ(lifepf.getJlife(),ZERO)) {
	
	cltspf.setClntnum(lifepf.getLifcnum());
	
	cltspf.setClntcoy(wsspcomn.fsuco.toString());
	cltspf.setClntpfx("CN");
	cltspf = clntpfDao.selectClient(cltspf);
	if (cltspf == null) {
		fatalError600();
	}
	covrpf = new Covrpf();
	covrpf.setChdrcoy(lifepf.getChdrcoy());
	covrpf.setChdrnum(lifepf.getChdrnum());
	covrpf.setLife(lifepf.getLife()); 
	covrpf.setPlanSuffix(0);
	covrpf.setValidflag("1");
	covrpfList = covrpfDAO.getcovrmatRecordsForMature(covrpf.getChdrcoy(),covrpf.getChdrnum());
	if(covrpfList != null && covrpfList.size() > 0)
	{
	for(Covrpf covr:covrpfList) {//ILIFE-7791 starts
		if(isNE(covr.getLife(),lifepf.getLife())) {
			continue;
		}
			covrpf = covr;
			
			payrpf = payrpfDAO.getpayrRecord(wsspcomn.company.toString(),chdrpf.getChdrnum(),1);
				
			if(isEQ(covr.getRider(),"00")) {
				sv.coverage.set(covrpf.getCoverage());
			}
			else {
				sv.rider.set(covrpf.getRider());
			}
				
			
			sv.component.set(wsaaMiscellaneousInner.wsaaCovrComponent);
			
			if (isNE(covr.getRider(), "00")
					&& isNE(covr.getRider(), "  ")) {
						sv.coverage.set(SPACES);
			}
			
			if (isEQ(covrpf.getJlife().trim(), "")){
				tempRldgAcct=chdrpf.getChdrnum()+covrpf.getLife()+covrpf.getCoverage()+covrpf.getRider()+"00";
			}
			else{
				tempRldgAcct=chdrpf.getChdrnum()+covrpf.getLife()+covrpf.getCoverage()+covrpf.getRider()+covrpf.getJlife();
			}
			List<Acblpf> acblpfList = acblDao.loadDataBulk(wsspcomn.company.toString(), chdrpf.getChdrnum(), "","", payrpf.getBillcurr(), t5645rec.sacstype03.toString());
			
			if (null != acblpfList && !acblpfList.isEmpty()) {
			for (Acblpf acbl : acblpfList) {
				acblpf = acbl;
				if (isEQ(tempRldgAcct, acblpf.getRldgacct())) {
					if (isEQ(t3695rec.sign,"-")) {
						compute(sv.instPrem, 2).set(mult(acblpf.getSacscurbal(),-1));
					}
					else {
						sv.instPrem.set(acblpf.getSacscurbal());
					}
					
					break;
				}
			}
			}
			if(sv.component.toString().matches("[0-9]+") && sv.component.toString().length() > 0) {
				sv.instPrem.set(BigDecimal.ZERO);
				sv.instPrem.setBlankWhenZero();
			}
			
			sv.component.set(covr.getCrtable());
			descpf=descDAO.getdescData("IT", "T5687", covrpf.getCrtable(), wsspcomn.company.toString(), wsspcomn.language.toString());
			if (descpf == null) {
				sv.compdesc.fill("?");
			}
			else {
				sv.compdesc.set(descpf.getLongdesc());
			}
			/*    IF   POLICY-LEVEL-ENQUIRY                                    */
//		covrStatusDescs1600();
			/*    Store the COVRENQ key for later use.*/
			srcalcpy.currcode.set(covrpf.getPremCurrency());
			srcalcpy.endf.set(SPACES);
			srcalcpy.tsvtot.set(ZERO);
			srcalcpy.tsv1tot.set(ZERO);
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
			srcalcpy.chdrChdrcoy.set(covrpf.getChdrcoy());
			srcalcpy.chdrChdrnum.set(covrpf.getChdrnum());
			srcalcpy.lifeLife.set(covrpf.getLife());
			srcalcpy.lifeJlife.set(covrpf.getJlife());
			srcalcpy.covrCoverage.set(covrpf.getCoverage());
			srcalcpy.covrRider.set(covrpf.getRider());
			srcalcpy.crtable.set(covrpf.getCrtable());
			srcalcpy.crrcd.set(covrpf.getCrrcd());
			srcalcpy.convUnits.set(covrpf.getConvertInitialUnits());
			srcalcpy.pstatcode.set(covrpf.getPstatcode());
			srcalcpy.status.set(SPACES);
			srcalcpy.polsum.set(chdrsurIO.getPolsum());
			srcalcpy.language.set(wsspcomn.language);
			srcalcpy.planSuffix.set(covrpf.getPlanSuffix());
			
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				srcalcpy.billfreq.set("00");
			}
			else {
				srcalcpy.billfreq.set(payrpf.getBillfreq());
			}
			
			while (isNE(srcalcpy.status, varcom.mrnf) && !(isEQ(srcalcpy.status, varcom.endp))) {
				try {	
						
						getValues();
						
					
				}
				catch (GOTOException e){
					/* Expected exception for control flow purposes. */
				}
			}
			setKjodpf(covrpf);
			updateCovr(covrpf);
			
			if(!(sv.instPrem.toString().trim().equals(""))){
				totalPrem=totalPrem.add(sv.instPrem.getbigdata());
				
			} 
			if(!(sv.cashvalare.toString().trim().equals(""))){
				totalCash=totalCash.add(sv.cashvalare.getbigdata());
			
			} 
			if(!(sv.polReserve.toString().trim().equals(""))){
				totalPolReserve=totalPolReserve.add(sv.polReserve.getbigdata());
			
			} 
			if(!(sv.fundVal.toString().trim().equals(""))){
				totalFund=totalFund.add(sv.fundVal.getbigdata());
			
			} 
			sv.totalPrem.set(totalPrem);
			sv.tFundVal.set(totalFund);
			sv.tCashval.set(totalCash);
			sv.tPolicyReserve.set(totalPolReserve);
			
			scrnparams.function.set(varcom.sadd);
			processScreen("Sh52c", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
	setCattpf();
	listcattpf.add(cattpf);
	}
}

		
		if(isNE(lifepf.getJlife(),ZERO)) {
			
			sv.component.set(lifepf.getLifcnum());
			
			cltspf.setClntpfx("CN");
			cltspf.setClntcoy(wsspcomn.fsuco.toString());
			cltspf = clntpfDao.selectClient(cltspf);
			if (cltspf == null) {
				fatalError600();
			}
			
			plainname();
			sv.compdesc.set(wsspcomn.longconfname);
			scrnparams.function.set(varcom.sadd);
			processScreen("Sh5c2", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			
		}
		
	if(lifeIterator.hasNext()) {
		lifepf = lifeIterator.next();
	}
	else {
		loopflag=false;
	}
	
	
	
}

private void checkPreRegistration() {
	if(cattpfDAO.isExists(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum())){
		cattpf = cattpfDAO.selectRecords(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		sv.claimnumber.set(cattpf.getClaim());
		sv.claimTyp.set(cattpf.getClamtyp());
		
	}
	else{
	allocateNumber2600();	
	sv.claimTyp.set("KJ");
	}
}

public StringUtil getStringUtil() {
	return stringUtil;
}
protected void plainname()
{
	/*PLAIN-100*/
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(cltspf.getClttype(), "C")) {
		corpname();

	} else if (isNE(cltspf.getGivname(), SPACES)) {
		String firstName = cltspf.getGivname();/* IJTI-1386 */
		String lastName = cltspf.getSurname();/* IJTI-1386 */
		String delimiter = ",";
		
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		
	} else {
		wsspcomn.longconfname.set(cltspf.getSurname());
	}
	/*PLAIN-EXIT*/
}
protected void corpname()
{
	/* PAYEE-1001 */
	wsspcomn.longconfname.set(SPACES);
	/* STRING CLTS-SURNAME DELIMITED SIZE */
	/* CLTS-GIVNAME DELIMITED ' ' */
	String firstName = cltspf.getLgivname();/* IJTI-1386 */
	String lastName = cltspf.getLsurname();/* IJTI-1386 */
	String delimiter = "";
	
	// this way we can override StringUtil behaviour in formatName()
	String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
	wsspcomn.longconfname.set(fullName);
	/*CORP-EXIT*/
}

public String getSplitSign() {
	return SPLIT_SIGN;
}

protected Descpf getDescData(String desctable, String descitem){
	Descpf descpf=descDAO.getdescData("IT", desctable, descitem, wsspcomn.company.toString().trim(), wsspcomn.language.toString().trim());
	return descpf;
	}

protected void preScreenEdit()
	{
		/*PRE-START*/

	scrnparams.subfileRrn.set(1);
	if (isEQ(wsspcomn.flag, "I")) {
		scrnparams.function.set(varcom.prot);
	}
	
	return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
			checkForErrors2080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'Sjl36IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          Sjl36-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void mapReasonCode()
{
	if (isNE(sv.rsncde, SPACES)) {
		if (isEQ(sv.resndesc, SPACES)) {
			scrnparams.statuz.set("CALC");
			descpf=descDAO.getdescData("IT", "TH5C0", sv.rsncde.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
			if (descpf == null) {
				sv.resndesc.fill("?");
			}
			else {
				sv.resndesc.set(descpf.getLongdesc());
			}
		}
	}
	
}

protected void validate2020()
	{	
		if (isEQ(wsspcomn.flag, "I")) { 
			return;
		}
		sv.totalPrem.set(totalPrem);
		sv.tFundVal.set(totalFund);
		sv.tCashval.set(totalCash);
		
		sv.susamt.set(tsusamt);
		sv.unexpiredprm.set(tunExpPrm);
		sv.apl.set(tapl);
		sv.aplInterest.set(taplInt);
		sv.policyloan.set(tpl);
		sv.policyloanInterest.set(tplInt);

		if(isNE(sv.tPolicyReserve.getFormDataRaw(),"")){
			totalPolReserve=BigDecimal.valueOf(Double.parseDouble(sv.tPolicyReserve.getFormDataRaw().replaceAll(",", "")));
		}
		sv.tPolicyReserve.set(totalPolReserve);
		sv.rsncde.set(sv.rsncde.getFormData());
		
		mapReasonCode();
		if (isEQ(sv.rsncde, SPACES)) {
			sv.rsncdeErr.set(errorsInner.rlag);
		}
		
		sv.refundOption.set(sv.refundOption.getFormData());
	//Resetting fields based on Refund Option
		if (isEQ(sv.refundOption, SPACES)) {
			sv.refundOptionErr.set(errorsInner.rumq);
		}else{
			
			if(sv.refundOption.equals("01")){
								
				sv.totalPrem.set(BigDecimal.ZERO);
				sv.totalPrem.setBlankWhenZero();
				sv.tFundVal.set(BigDecimal.ZERO);
				sv.tFundVal.setBlankWhenZero();
				sv.tPolicyReserve.set(BigDecimal.ZERO);
				sv.tPolicyReserve.setBlankWhenZero();
			}
			else if(sv.refundOption.equals("02")){
				
				sv.totalPrem.set(BigDecimal.ZERO);
				sv.totalPrem.setBlankWhenZero();
				sv.tFundVal.set(BigDecimal.ZERO);
				sv.tFundVal.setBlankWhenZero();
				sv.tCashval.set(BigDecimal.ZERO);
				sv.tCashval.setBlankWhenZero();
			}
			else if(sv.refundOption.equals("03")){
				
				sv.tPolicyReserve.set(BigDecimal.ZERO);
				sv.tPolicyReserve.setBlankWhenZero();
				sv.tFundVal.set(BigDecimal.ZERO);
				sv.tFundVal.setBlankWhenZero();
				sv.tCashval.set(BigDecimal.ZERO);
				sv.tCashval.setBlankWhenZero();
			}
			else if(sv.refundOption.equals("06")){
				readT5540();
				
				sv.tPolicyReserve.set(BigDecimal.ZERO);
				sv.tPolicyReserve.setBlankWhenZero();
				sv.totalPrem.set(BigDecimal.ZERO);
				sv.totalPrem.setBlankWhenZero();
				sv.tCashval.set(BigDecimal.ZERO);
				sv.tCashval.setBlankWhenZero();
			}
			else if(sv.refundOption.equals("04")){
				
				sv.tPolicyReserve.set(BigDecimal.ZERO);
				sv.tPolicyReserve.setBlankWhenZero();
				sv.totalPrem.set(BigDecimal.ZERO);
				sv.totalPrem.setBlankWhenZero();
				sv.tCashval.set(BigDecimal.ZERO);
				sv.tCashval.setBlankWhenZero();
				sv.tFundVal.set(BigDecimal.ZERO);
				sv.tFundVal.setBlankWhenZero();
				
			}
			else if(sv.refundOption.equals("05")){
				
				sv.tPolicyReserve.set(BigDecimal.ZERO);
				sv.tPolicyReserve.setBlankWhenZero();
				sv.totalPrem.set(BigDecimal.ZERO);
				sv.totalPrem.setBlankWhenZero();
				sv.tCashval.set(BigDecimal.ZERO);
				sv.tCashval.setBlankWhenZero();
				sv.tFundVal.set(BigDecimal.ZERO);
				sv.tFundVal.setBlankWhenZero();
				
				sv.susamt.set(BigDecimal.ZERO);
				sv.susamt.setBlankWhenZero();
				sv.apl.set(BigDecimal.ZERO);
				sv.apl.setBlankWhenZero();
				sv.aplInterest.set(BigDecimal.ZERO);
				sv.aplInterest.setBlankWhenZero();
				sv.policyloan.set(BigDecimal.ZERO);
				sv.policyloan.setBlankWhenZero();
				sv.policyloanInterest.set(BigDecimal.ZERO);
				sv.policyloanInterest.setBlankWhenZero();
				sv.advPrm.set(BigDecimal.ZERO);
				sv.advPrm.setBlankWhenZero();
				sv.unexpiredprm.set(BigDecimal.ZERO);
				sv.unexpiredprm.setBlankWhenZero();
				
			}
			
		
			total= (sv.totalPrem.getbigdata().add(sv.tCashval.getbigdata()).add(sv.tFundVal.getbigdata()).add(sv.tPolicyReserve.getbigdata())).subtract(sv.apl.getbigdata().add(sv.aplInterest.getbigdata()).add(sv.policyloan.getbigdata()).add(sv.policyloanInterest.getbigdata())).add(sv.susamt.getbigdata()).add(sv.advPrm.getbigdata()).add(sv.unexpiredprm.getbigdata());
			sv.total.set(total);
			if(total.compareTo(BigDecimal.ZERO)== -1){
				sv.totalErr.set(errorsInner.rums);
			}
			else if(total.compareTo(BigDecimal.ZERO)== 0){
				if(warningFlag==0){
					sv.totalErr.set(errorsInner.rumt);
					warningFlag=1;
				}
			}
			
		}
		
		
	}

protected void checkForErrors2080()
	{
		  //  Redisplay screen if CALC is pressed                          
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}

	}



protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required / WSSP*/
	if (isEQ(wsspcomn.flag, "A")) {
		setKjohpf();
		kjohpfDAO.insertKjohpf(kjohpf);
		kjodpfDAO.insertKjodpfList(listKjodpf);	
		for(Cattpf c : listcattpf){
			cattpfDAO.insertCattpfRecord(c);
		}
		writeChdrpf();
		writeCovrpf();
		writePtrn();
	}
	if (isNE(wsspcomn.flag, "I")) {
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(chdrpf.getChdrcoy());
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set(chdrpf.getChdrpfx());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}
		
	}

protected void writeCovrpf() {
	if(covrpfUpdateList != null && !covrpfUpdateList.isEmpty()) {
		covrpfDAO.updateCovrRPStat(covrpfUpdateList);
		covrpfUpdateList.clear();
	}
	if(covrpfInsertList != null && !covrpfInsertList.isEmpty()) {
		covrpfDAO.insertCovrpfList(covrpfInsertList);
		covrpfInsertList.clear();
	}
}

protected void writeChdrpf(){
	Chdrpf chdrpfIO = new Chdrpf(chdrpf);
	List<Chdrpf> chdrList =   new ArrayList<Chdrpf>();
	chdrList.add(chdrpf);
	
	chdrpfDAO.updateInvalidChdrRecord(chdrList);
	chdrList.clear();
	
	chdrpfIO.setChdrcoy(chdrpf.getChdrcoy());
	chdrpfIO.setChdrnum(chdrpf.getChdrnum());	
	chdrpfIO.setTranno(chdrpf.getTranno()+1);
	chdrpfIO.setStatcode("CC");
	chdrpfIO.setPstcde("CC");
	chdrList.add(chdrpfIO);
	chdrpfDAO.insertChdrAmt(chdrList);
	chdrList.clear();
}

protected void writePtrn(){
	Ptrnpf ptrnpf = new Ptrnpf();
	ptrnpf.setChdrpfx(chdrpf.getChdrpfx().toString());
	ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	ptrnpf.setChdrnum(chdrpf.getChdrnum());
	ptrnpf.setValidflag("1");
	ptrnpf.setTranno(chdrpf.getTranno()+1);		
	ptrnpf.setTrdt(varcom.vrcmDate.toInt());
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	ptrnpf.setTermid(varcom.vrcmTermid.toString());
	ptrnpf.setUserT(varcom.vrcmUser.toInt());
	ptrnpf.setPtrneff(datcon1rec.intDate.toInt());
	ptrnpf.setDatesub(datcon1rec.intDate.toInt());
	ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
	ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
	ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
	ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
	ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
	ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
	ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	ptrnpfDAO.insertPtrnPF(ptrnpf);
}

protected void updateCovr(Covrpf covr) {
	Covrpf covrpf = new Covrpf(covr);
	covrpf.setCurrto(bsscIO.getEffectiveDate().toInt());
	covrpf.setValidflag("2");
	covrpfUpdateList.add(covrpf);

	covr.setTranno(chdrpf.getTranno()+1);
	covr.setStatcode("CC");
	covr.setPstatcode("CC");
	covr.setValidflag("1");
	covr.setCurrfrom(bsscIO.getEffectiveDate().toInt());
	covrpfInsertList.add(covr);

}

protected void setKjodpf(Covrpf covrpf){
	kjodpf=new Kjodpf();
	kjodpf.setChdrnum(sv.chdrnum.toString());
	kjodpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	kjodpf.setLife(covrpf.getLife());
	kjodpf.setCoverage(covrpf.getCoverage());
	kjodpf.setRider(covrpf.getRider());
	kjodpf.setTranno(chdrpf.getTranno()+1);
	kjodpf.setEffdate(sv.effdate.toInt());
	kjodpf.setCurrcd("JPY");
	kjodpf.setCrtable(covrpf.getCrtable());
	kjodpf.setActvalue(sv.cashvalare.getbigdata());
	kjodpf.setClaim(sv.claimnumber.toString());
	kjodpf.setTermid(covrpf.getTermid());
	kjodpf.setTrdt(datcon1rec.intDate.toInt());
	kjodpf.setTrtm(Integer.parseInt(getCobolTime()));
	kjodpf.setUser(varcom.vrcmUser.toInt());
	kjodpf.setUsrprf(wsspcomn.userid.toString());
	listKjodpf.add(kjodpf);
}

protected void setKjohpf(){
		kjohpf.setChdrnum(sv.chdrnum.toString());
		kjohpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		kjohpf.setLife(covrpfList.get(0).getLife());
		kjohpf.setCnttype(sv.cnttype.toString());
		kjohpf.setEffdate(sv.effdate.toInt());
		kjohpf.setCurrcd("JPY");
		kjohpf.setClaimType("KJ");
		kjohpf.setPolicyloan(sv.policyloan.getbigdata());
		kjohpf.setReasoncd(sv.rsncde.toString());
		kjohpf.setResndesc(sv.resndesc.toString());
		kjohpf.setTrtm(Integer.parseInt(getCobolTime()));
		kjohpf.setPlint01(sv.policyloanInterest.getbigdata());
		kjohpf.setAplamt01(sv.apl.getbigdata());
		kjohpf.setAplint01(sv.aplInterest.getbigdata());
		kjohpf.setPremsusp(sv.susamt.getbigdata());
		kjohpf.setPaymmeth(chdrpf.getBillchnl());
		kjohpf.setSurrval01(sv.tCashval.getbigdata());
		kjohpf.setSurrval02(sv.tPolicyReserve.getbigdata());
		kjohpf.setSurrval03(sv.tFundVal.getbigdata());
		
		kjohpf.setClamstat("RG");
		
		kjohpf.setRefundoption(sv.refundOption.toString());
		kjohpf.setValidflag("1");
		kjohpf.setUserid(varcom.vrcmUser.toInt());
		kjohpf.setTranno(chdrpf.getTranno()+1);
		kjohpf.setWuepamt(sv.unexpiredprm.getbigdata());
		kjohpf.setPrempaid(sv.totalPrem.getbigdata());
		kjohpf.setUsrprf(wsspcomn.userid.toString());
		kjohpf.setTotal(sv.total.getbigdata());
		kjohpf.setClaim(sv.claimnumber.toString());
	
}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.nextprog.set(wsaaProg); 
		wsspcomn.programPtr.add(2);
		/*EXIT*/
	}

protected void readT5540() {
	t5540Map = itempfDAO.loadSmartTable("IT", wsspcomn.company.toString(), "T5540");
	Iterator<Covrpf> iterator = covrpfList.iterator();
	Covrpf covrSur=new Covrpf();
	while (iterator.hasNext()){
		covrSur=iterator.next();
		if(isEQ(covrSur.getRider(),"00")) {	
			String keyItemitem = covrSur.getCrtable().trim();
			if(!t5540Map.containsKey(keyItemitem) && isEQ(sv.chdrnumErr,SPACES)){
				sv.refundOptionErr.set(errorsInner.g709);
				break;
			}
			
		}
	}
}
 
protected void allocateNumber2600()
{
	alocnorec.function.set("NEXT");
	alocnorec.prefix.set("CM");
	alocnorec.company.set(wsspcomn.company);
	alocnorec.genkey.set(SPACES);
	callProgram(Alocno.class, alocnorec.alocnoRec);
	if (isEQ(alocnorec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(alocnorec.statuz);
		fatalError600();
	}
	if (isNE(alocnorec.statuz, varcom.oK)) {
		sv.claimnumberErr.set(alocnorec.statuz);
	}
	else {
		sv.claimnumber.set(alocnorec.alocNo);
	}
}

protected void setCattpf(){
	cattpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	cattpf.setChdrnum(chdrpf.getChdrnum());
	cattpf.setClaim(sv.claimnumber.toString());
	cattpf.setClamamt(BigDecimal.ZERO);
	cattpf.setClamstat("RG");
	cattpf.setClamtyp(sv.claimTyp.toString());
	cattpf.setCoverage(covrpfList.get(0).getCoverage());
	cattpf.setCauseofdth(" ");
	cattpf.setDoceffdate(99999999);
	cattpf.setDtofdeath(0);
	cattpf.setEffdate(sv.effdate.toInt());
	cattpf.setFlag(" ");
	cattpf.setFulfilltrm(" ");
	cattpf.setIntbasedt(99999999);
	cattpf.setIntcalflag(" ");
	cattpf.setJlife(" ");
	cattpf.setKariflag(" ");
	cattpf.setKpaydate(99999999);
	cattpf.setLifcnum(sv.lifcnum.toString());	
	cattpf.setLife(lifepf.getLife());
	cattpf.setRider(" ");
	cattpf.setSeqenum("000");
	cattpf.setTranno(chdrpf.getTranno()+1);
	cattpf.setTrcode(wsaaBatckey.batcBatctrcde.toString());
	cattpf.setValidflag("1");
	cattpf.setCondte(varcom.vrcmMaxDate.toInt());
}

private static final class ErrorsInner { 
	private FixedLengthStringData rumq = new FixedLengthStringData(4).init("RUMQ");
	private FixedLengthStringData rums = new FixedLengthStringData(4).init("RUMS");
	private FixedLengthStringData rlag = new FixedLengthStringData(4).init("RLAG");
	private FixedLengthStringData rumt = new FixedLengthStringData(4).init("RUMT");
	private FixedLengthStringData g709 = new FixedLengthStringData(4).init("G709");
}

}
