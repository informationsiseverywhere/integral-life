package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:27
 * Description:
 * Copybook name: HCLACAPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hclacapkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hclacapFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hclacapKey = new FixedLengthStringData(256).isAPartOf(hclacapFileKey, 0, REDEFINE);
  	public FixedLengthStringData hclacapChdrcoy = new FixedLengthStringData(1).isAPartOf(hclacapKey, 0);
  	public FixedLengthStringData hclacapChdrnum = new FixedLengthStringData(8).isAPartOf(hclacapKey, 1);
  	public FixedLengthStringData hclacapLife = new FixedLengthStringData(2).isAPartOf(hclacapKey, 9);
  	public FixedLengthStringData hclacapCoverage = new FixedLengthStringData(2).isAPartOf(hclacapKey, 11);
  	public FixedLengthStringData hclacapRider = new FixedLengthStringData(2).isAPartOf(hclacapKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(241).isAPartOf(hclacapKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hclacapFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hclacapFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}