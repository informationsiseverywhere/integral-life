/*
 * File: T6696pt.java
 * Date: 30 August 2009 2:30:52
 * Author: Quipoz Limited
 * 
 * Class transformed from T6696PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.terminationclaims.tablestructures.T6696rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6696.
*
*
*****************************************************************
* </pre>
*/
public class T6696pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Regular Claim Detail Rules                 S6696");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(55);
	private FixedLengthStringData filler9 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine004, 15, FILLER).init("Default Claim %  . . . . . . . .");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine004, 49).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(55);
	private FixedLengthStringData filler11 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine005, 15, FILLER).init("Maximum Override %   . . . . . .");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine005, 49).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(55);
	private FixedLengthStringData filler13 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine006, 15, FILLER).init("Minimum Override %   . . . . . .");
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 49).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(71);
	private FixedLengthStringData filler15 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine007, 15, FILLER).init("Default Deferment Period   . . .");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 49).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine007, 52, FILLER).init("  Frequency . .");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 69);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(71);
	private FixedLengthStringData filler18 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler19 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine008, 15, FILLER).init("Minimum Deferment Period   . . .");
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 49).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine008, 52, FILLER).init("  Frequency . .");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 69);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(71);
	private FixedLengthStringData filler21 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine009, 15, FILLER).init("Review Term  . . . . . . . . . .");
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 49).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine009, 52, FILLER).init("  Frequency . .");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 69);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(59);
	private FixedLengthStringData filler24 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler25 = new FixedLengthStringData(36).isAPartOf(wsaaPrtLine010, 15, FILLER).init("Override of Frequency Allowed. .");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 51);
	private FixedLengthStringData filler26 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 52, FILLER).init("  (Y/N)");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(51);
	private FixedLengthStringData filler27 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler28 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine011, 15, FILLER).init("Indexation Frequency . . . . . .");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 49);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(59);
	private FixedLengthStringData filler29 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine012, 15, FILLER).init("Indexation Subroutine. . . . . .");
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 49);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(54);
	private FixedLengthStringData filler31 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler32 = new FixedLengthStringData(47).isAPartOf(wsaaPrtLine013, 7, FILLER).init("Enter the Sub Account that will fund the claim:");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(51);
	private FixedLengthStringData filler33 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler34 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine014, 15, FILLER).init("Sub Account Code . . . . . . . .");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 49);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(51);
	private FixedLengthStringData filler35 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler36 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine015, 15, FILLER).init("Sub Account Type . . . . . . . .");
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 49);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(63);
	private FixedLengthStringData filler37 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler38 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine016, 15, FILLER).init("G/L Account Key  . . . . . . . .");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine016, 49);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(50);
	private FixedLengthStringData filler39 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler40 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine017, 15, FILLER).init("DB/CR (+/-)  . . . . . . . . . .");
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 49);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(53);
	private FixedLengthStringData filler41 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler42 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine018, 15, FILLER).init("Dissection Method. . . . . . . .");
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine018, 49);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6696rec t6696rec = new T6696rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6696pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6696rec.t6696Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t6696rec.dfclmpct);
		fieldNo008.set(t6696rec.mxovrpct);
		fieldNo009.set(t6696rec.mnovrpct);
		fieldNo010.set(t6696rec.dfdefprd);
		fieldNo011.set(t6696rec.freqcy01);
		fieldNo012.set(t6696rec.mndefprd);
		fieldNo013.set(t6696rec.freqcy02);
		fieldNo014.set(t6696rec.revitrm);
		fieldNo015.set(t6696rec.freqcy03);
		fieldNo016.set(t6696rec.frqoride);
		fieldNo017.set(t6696rec.inxfrq);
		fieldNo018.set(t6696rec.inxsbm);
		fieldNo019.set(t6696rec.sacscode);
		fieldNo020.set(t6696rec.sacstype);
		fieldNo021.set(t6696rec.glact);
		fieldNo022.set(t6696rec.sign);
		fieldNo023.set(t6696rec.dissmeth);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
