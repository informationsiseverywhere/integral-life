/*
 * File: P5008.java
 * Date: 29 August 2009 23:54:13
 * Author: Quipoz Limited
 * 
 * Class transformed from P5008.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.procedures.Trcdechk;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.contractservicing.tablestructures.Tranchkrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifematTableDAM;
import com.csc.life.terminationclaims.dataaccess.MathclmTableDAM;
import com.csc.life.terminationclaims.screens.S5008ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P5008 - Maturity Reversal Selection
* -----------------------------------
*
* Initialise
* ----------
*
* Clear the subfile ready for loading.
*
* The  details of the contract being worked with will have been
* stored  in  the  CHDRMAT I/O module. Retrieve the details and
* set up the header portion of the screen.
*
* Look up the following descriptions and names:
*
*   -  Contract Type, (CNTTYPE) - long description from T5688,
*   -  Contract  Status,  (STATCODE)  -  short description from
*      T3623,
*   -  Premium  Status,  (PSTATCODE)  -  short description from
*      T3588,
*   -  The owner's client (CLTS) details.
*   -  The joint  owner's  client (CLTS) details if they exist.
*
* Ensure that the last active transaction is a Maturity/Expiry
* transaction by sequentially reading PTRNREV and ignoring
* non-significant transactions. Non-significant transactions
* are defined as those transaction having an entry in T6661
* with no subroutine specified and the reversal flag set to N.
*
* If the last active transaction is not a Maturity/Expiry then
* no reversal request can be entertained, otherwise display
* the TRANNO and a  description of  the  transaction  to  be
* reversed.
*
* Load the subfile as follows:
*
*    Read  the  first  COVRMAT  record on the contract, using
*    zero  in  Plan  Suffix,  '01'  in  the  Life field and a
*    function of BEGN.
*
*    If the Plan Suffix from the returned record is zero then
*    add 1 to it before display. Obtain the long descriptions
*    for  the  coverage risk status, STATCODE, from T5682 and
*    the  premium  status  code,  PSTATCODE,  from  T5681 and
*    display them.
*
*    Check the transaction number TRANNO of the COVR record.
*    If it is the same as the TRANNO of the PTRNREV record,
*    then automatically select this COVR for reversal.
*    Otherwise, protect this COVR from selection.
*
*    Repeat  the  above  line  for as many times as there are
*    summarised policies, incrementing the Plan Suffix by one
*    for each subfile record written.
*
*    Store  the  life number and increment the Plan Suffix by
*    one.  Perform  a BEGN to obtain the first COVR record
*    for  the  next  Plan  Suffix.  Write  out its details as
*    before  and repeat the process until either the Company,
*    Contract Header Number or Life Number changes.
*
*    Load all pages  required  in  the subfile and set the
*    subfile more indicator to no.
*
* Updating
* --------
*
*      No Updating is required.
*
*
* Next Program
* ------------
*
* If there is only one policy in the plan, KEEPS the COVR
* record, call GENSSW with a function of 'A' and exit.
*
* Read through the subfile to find which record has been
* selected. Save the plan-suffix and perform a KEEPS on the
* COVR record. Call GENSSW with a function of 'A' and exit.
*
*****************************************************************
* </pre>
*/
public class P5008 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5008");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaNoCompMatured = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaMiscellaneous = new FixedLengthStringData(20);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaMiscellaneous, 0).setUnsigned();
	private ZonedDecimalData wsaaActualWritten = new ZonedDecimalData(3, 0).isAPartOf(wsaaMiscellaneous, 4);
	private PackedDecimalData wsaaCovrTimes = new PackedDecimalData(5, 0).isAPartOf(wsaaMiscellaneous, 7).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).isAPartOf(wsaaMiscellaneous, 10).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).isAPartOf(wsaaMiscellaneous, 12).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).isAPartOf(wsaaMiscellaneous, 14).setUnsigned();
	private ZonedDecimalData wsaaSavePlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(wsaaMiscellaneous, 16).setUnsigned();

	private FixedLengthStringData wsaaTransactionMatch = new FixedLengthStringData(1).init(SPACES);
	private Validator transactionMatch = new Validator(wsaaTransactionMatch, "Y");
	private FixedLengthStringData wsaaTransMatc = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaTransAutoMatc = new FixedLengthStringData(4);
	private String wsaaTransErr = "";

	private FixedLengthStringData wsaaProcessingMsg = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaMsgProcess = new FixedLengthStringData(19).isAPartOf(wsaaProcessingMsg, 0).init(SPACES);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaProcessingMsg, 19, FILLER).init(SPACES);
	private ZonedDecimalData wsaaMsgTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaProcessingMsg, 20).setUnsigned();
	private FixedLengthStringData filler1 = new FixedLengthStringData(3).isAPartOf(wsaaProcessingMsg, 25, FILLER).init(" - ");
	private FixedLengthStringData wsaaMsgTrantype = new FixedLengthStringData(4).isAPartOf(wsaaProcessingMsg, 28).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaProcessingMsg, 32, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsgTrandesc = new FixedLengthStringData(27).isAPartOf(wsaaProcessingMsg, 33);

	private FixedLengthStringData wsaaConfirmMsg = new FixedLengthStringData(60);
	private FixedLengthStringData filler3 = new FixedLengthStringData(19).isAPartOf(wsaaConfirmMsg, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsg = new FixedLengthStringData(22).isAPartOf(wsaaConfirmMsg, 19).init(SPACES);
	private FixedLengthStringData filler4 = new FixedLengthStringData(19).isAPartOf(wsaaConfirmMsg, 41, FILLER).init(SPACES);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
		/* ERRORS */
	private String e350 = "E350";
	private String h143 = "H143";
	private String h093 = "H093";
	private String h205 = "H205";
	private String t014 = "T014";
	private String t015 = "T015";
		/* TABLES */
	private String t1688 = "T1688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5679 = "T5679";
	private String t5681 = "T5681";
	private String t5682 = "T5682";
	private String t5688 = "T5688";
	private String t6661 = "T6661";
	private String tr386 = "TR386";
	private String covrmatrec = "COVRMATREC";
	private String itemrec = "ITEMREC";
	private String ptrnrevrec = "PTRNREVREC";
	private String mathclmrec = "MATHCLMREC";
		/*Contract Header Maturities*/
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Maturity/Expiry logical over COVR*/
	private CovrmatTableDAM covrmatIO = new CovrmatTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Logical File on LIFEPF-Maturity/Expiry*/
	private LifematTableDAM lifematIO = new LifematTableDAM();
		/*Maturity Header Claim File*/
	private MathclmTableDAM mathclmIO = new MathclmTableDAM();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private T6661rec t6661rec = new T6661rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Tranchkrec tranchkrec = new Tranchkrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5008ScreenVars sv = ScreenProgram.getScreenVars( S5008ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1009, 
		readPtrn1012, 
		badTrans1016, 
		exit1019, 
		readJlife1630, 
		exit1690, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit4090, 
		exit4110
	}

	public P5008() {
		super();
		screenVars = sv;
		new ScreenModel("S5008", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1009);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaTransErr = "N";
		wsaaMiscellaneous.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5008", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		chdrmatIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmatIO);
		if (isNE(chdrmatIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmatIO.getParams());
			fatalError600();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		wsaaTransMatc.set(t6661rec.trcode);
		wsaaTransAutoMatc.set("BRFS");
		checkLastTrans1010();
		readT56791050();
		sv.selectOut[varcom.ri.toInt()].set("N");
		if (isEQ(chdrmatIO.getPolsum(),chdrmatIO.getPolinc())
		&& isEQ(wsaaTransErr,"N")) {
			sv.select.set("X");
			sv.selectOut[varcom.ri.toInt()].set("Y");
		}
		else {
			sv.select.set(SPACES);
		}
		sv.planSuffix.set(0);
		sv.rstatdesc.set("Select Whole Plan");
		scrnparams.function.set(varcom.sadd);
		processScreen("S5008", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		compute(wsaaCovrTimes, 0).set(sub(chdrmatIO.getPolinc(),chdrmatIO.getPolsum()));
		if (isEQ(chdrmatIO.getPolinc(),1)) {
			wsaaPlanSuffix.set(ZERO);
		}
		else {
			wsaaPlanSuffix.set(chdrmatIO.getPolinc());
		}
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaCovrTimes);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			loadCovrSubfile1100();
		}
		if (isNE(chdrmatIO.getPolsum(),ZERO)) {
			processSummary1200();
		}
		fillScreen1600();
	}

protected void checkLastTrans1010()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readPtrnrev1010();
				}
				case readPtrn1012: {
					readPtrn1012();
				}
				case badTrans1016: {
					badTrans1016();
				}
				case exit1019: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readPtrnrev1010()
	{
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(chdrmatIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrmatIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		ptrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnrevIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	}

protected void readPtrn1012()
	{
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrmatIO.getChdrnum(),ptrnrevIO.getChdrnum()))
		|| (isNE(chdrmatIO.getChdrcoy(),ptrnrevIO.getChdrcoy()))) {
			ptrnrevIO.setStatuz(varcom.endp);
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		if ((isEQ(ptrnrevIO.getBatctrcde(),wsaaTransMatc)  ||  (isEQ(ptrnrevIO.getBatctrcde(),wsaaTransAutoMatc)))) {
			wsaaMsgTranno.set(ptrnrevIO.getTranno());
			wsaaMsgTrantype.set(ptrnrevIO.getBatctrcde());
			descIO.setDescitem(ptrnrevIO.getBatctrcde());
			descIO.setDesctabl(t1688);
			findDesc1300();
			wsaaMsgTrandesc.set(descIO.getLongdesc());
			itemIO.setFormat(itemrec);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr386);
			wsaaTr386Lang.set(wsspcomn.language);
			wsaaTr386Pgm.set(wsaaProg);
			wsaaTr386Id.set(SPACES);
			itemIO.setItemitem(wsaaTr386Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			tr386rec.tr386Rec.set(itemIO.getGenarea());
			wsaaMsgProcess.set(tr386rec.progdesc01);
			sv.sfcdesc.set(wsaaProcessingMsg);
			wsaaMsg.set(tr386rec.progdesc02);
			sv.confirm.set(wsaaConfirmMsg);
			sv.sfcdescOut[varcom.nd.toInt()].set(SPACES);
			sv.sfcdesdOut[varcom.nd.toInt()].set(SPACES);
			keepPtrnrev1020();
			goTo(GotoLabel.exit1019);
		}
		tranchkrec.codeCheckRec.set(SPACES);
		tranchkrec.tcdeStatuz.set(varcom.oK);
		tranchkrec.tcdeTranCode.set(ptrnrevIO.getBatctrcde());
		tranchkrec.tcdeCompany.set(wsspcomn.company);
		callProgram(Trcdechk.class, tranchkrec.codeCheckRec);
		if ((isNE(tranchkrec.tcdeStatuz,varcom.oK))
		&& (isNE(tranchkrec.tcdeStatuz,varcom.mrnf))) {
			syserrrec.params.set(tranchkrec.codeCheckRec);
			fatalError600();
		}
		if (isEQ(tranchkrec.tcdeStatuz,varcom.mrnf)) {
			goTo(GotoLabel.badTrans1016);
		}
		if (isEQ(tranchkrec.tcdeStatuz,varcom.oK)) {
			ptrnrevIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readPtrn1012);
		}
	}

protected void badTrans1016()
	{
		sv.sfcdescOut[varcom.nd.toInt()].set("Y");
		sv.sfcdesdOut[varcom.nd.toInt()].set("Y");
		wsspcomn.edterror.set("Y");
		wsaaTransErr = "Y";
		if (isEQ(wsaaBatckey.batcBatctrcde,"T540")) {
			sv.chdrnumErr.set(t015);
		}
		else {
			sv.chdrnumErr.set(t014);
		}
	}

protected void keepPtrnrev1020()
	{
		/*KEEP*/
		ptrnrevIO.setFunction(varcom.keeps);
		ptrnrevIO.setFormat(ptrnrevrec);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readT56791050()
	{
		readStatusTable1050();
	}

protected void readStatusTable1050()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmatIO.getChdrcoy());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void loadCovrSubfile1100()
	{
		begnCovrmat1100();
	}

protected void begnCovrmat1100()
	{
		covrmatIO.setDataArea(SPACES);
		covrmatIO.setChdrcoy(chdrmatIO.getChdrcoy());
		covrmatIO.setChdrnum(chdrmatIO.getChdrnum());
		covrmatIO.setPlanSuffix(wsaaPlanSuffix);
		covrmatIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrmatIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmatIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, covrmatIO);
		if ((isNE(covrmatIO.getStatuz(),varcom.oK))
		&& (isNE(covrmatIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrmatIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrmatIO.getChdrcoy(),covrmatIO.getChdrcoy()))
		|| (isNE(chdrmatIO.getChdrnum(),covrmatIO.getChdrnum()))
		|| (isEQ(covrmatIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrmatIO.getParams());
			fatalError600();
		}
		sv.planSuffix.set(wsaaPlanSuffix);
		wsaaTransactionMatch.set("N");
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)
		|| isEQ(wsaaTransactionMatch,"Y")); wsaaSub.add(1)){
			checkStatusCoverage1400();
		}
		if (transactionMatch.isTrue()) {
			checkCurrentTrans1120();
		}
		descIO.setDescitem(covrmatIO.getStatcode());
		descIO.setDesctabl(t5682);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.rstatdesc.fill("?");
		}
		descIO.setDescitem(covrmatIO.getPstatcode());
		descIO.setDesctabl(t5681);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.pstatdesc.fill("?");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5008", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isGT(wsaaPlanSuffix,ZERO)) {
			wsaaPlanSuffix.subtract(1);
		}
	}

protected void checkCurrentTrans1120()
	{
		readMathclm1120();
	}

protected void readMathclm1120()
	{
		mathclmIO.setParams(SPACES);
		mathclmIO.setChdrcoy(chdrmatIO.getChdrcoy());
		mathclmIO.setChdrnum(chdrmatIO.getChdrnum());
		mathclmIO.setTranno(ptrnrevIO.getTranno());
		mathclmIO.setPlanSuffix(covrmatIO.getPlanSuffix());
		mathclmIO.setFunction(varcom.readr);
		mathclmIO.setFormat(mathclmrec);
		SmartFileCode.execute(appVars, mathclmIO);
		if ((isNE(mathclmIO.getStatuz(),varcom.oK))
		&& (isNE(mathclmIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(mathclmIO.getParams());
			fatalError600();
		}
		if (isEQ(mathclmIO.getStatuz(),varcom.mrnf)) {
			sv.selectOut[varcom.ri.toInt()].set("N");
			sv.select.set(SPACES);
		}
		else {
			sv.selectOut[varcom.ri.toInt()].set("Y");
			sv.select.set("X");
		}
	}

protected void processSummary1200()
	{
		readCovrmat1200();
	}

protected void readCovrmat1200()
	{
		covrmatIO.setDataArea(SPACES);
		covrmatIO.setChdrcoy(chdrmatIO.getChdrcoy());
		covrmatIO.setChdrnum(chdrmatIO.getChdrnum());
		covrmatIO.setPlanSuffix(ZERO);
		covrmatIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrmatIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmatIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, covrmatIO);
		if ((isNE(covrmatIO.getStatuz(),varcom.oK))
		&& (isNE(covrmatIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrmatIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrmatIO.getChdrcoy(),covrmatIO.getChdrcoy()))
		|| (isNE(chdrmatIO.getChdrnum(),covrmatIO.getChdrnum()))
		|| (isEQ(covrmatIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrmatIO.getParams());
			fatalError600();
		}
		sv.rstatcode.set(covrmatIO.getStatcode());
		sv.pstatcode.set(covrmatIO.getPstatcode());
		descIO.setDescitem(covrmatIO.getStatcode());
		descIO.setDesctabl(t5682);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.rstatdesc.fill("?");
		}
		descIO.setDescitem(covrmatIO.getPstatcode());
		descIO.setDesctabl(t5681);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstatdesc.set(descIO.getLongdesc());
		}
		else {
			sv.pstatdesc.fill("?");
		}
		sv.selectOut[varcom.ri.toInt()].set("N");
		sv.select.set(SPACES);
		PackedDecimalData loopEndVar2 = new PackedDecimalData(7, 0);
		loopEndVar2.set(chdrmatIO.getPolsum());
		for (int loopVar2 = 0; !(isEQ(loopVar2,loopEndVar2.toInt())); loopVar2 += 1){
			loadSummarySubfile1250();
		}
	}

protected void loadSummarySubfile1250()
	{
		/*FILL-SUBFILE-RECORD*/
		sv.planSuffix.set(wsaaPlanSuffix);
		scrnparams.function.set(varcom.sadd);
		processScreen("S5008", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isGT(wsaaPlanSuffix,ZERO)) {
			wsaaPlanSuffix.subtract(1);
		}
		/*EXIT*/
	}

protected void findDesc1300()
	{
		/*READ-DESC*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmatIO.getChdrcoy());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkStatusCoverage1400()
	{
		/*SEARCH*/
		if (isEQ(covrmatIO.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)
			|| isEQ(wsaaTransactionMatch,"Y")); wsaaSub.add(1)){
				checkStatusPremium1500();
			}
		}
		/*EXIT*/
	}

protected void checkStatusPremium1500()
	{
		/*START*/
		if (isNE(covrmatIO.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
			wsaaTransactionMatch.set("Y");
		}
		/*EXIT*/
	}

protected void fillScreen1600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					headings1600();
				}
				case readJlife1630: {
					readJlife1630();
				}
				case exit1690: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void headings1600()
	{
		sv.cntcurr.set(chdrmatIO.getCntcurr());
		sv.chdrnum.set(chdrmatIO.getChdrnum());
		sv.cnttype.set(chdrmatIO.getCnttype());
		descIO.setDescitem(chdrmatIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmatIO.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		else {
			sv.chdrstatus.fill("?");
		}
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmatIO.getPstatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.premstatus.set(descIO.getShortdesc());
		}
		else {
			sv.premstatus.fill("?");
		}
		lifematIO.setDataArea(SPACES);
		lifematIO.setChdrnum(chdrmatIO.getChdrnum());
		lifematIO.setChdrcoy(chdrmatIO.getChdrcoy());
		lifematIO.setLife(covrmatIO.getLife());
		lifematIO.setJlife("00");
		lifematIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifematIO);
		if ((isNE(lifematIO.getStatuz(),varcom.oK))
		&& (isNE(lifematIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(lifematIO.getParams());
			fatalError600();
		}
		if (isEQ(lifematIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.readJlife1630);
		}
		sv.lifenum.set(lifematIO.getLifcnum());
		cltsIO.setClntnum(lifematIO.getLifcnum());
		getClientDetails1700();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),1))) {
			sv.lifenameErr.set(h143);
			sv.lifename.set(SPACES);
		}
		else {
			plainname();
			sv.lifename.set(wsspcomn.longconfname);
		}
	}

protected void readJlife1630()
	{
		lifematIO.setJlife("01");
		lifematIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifematIO);
		if ((isNE(lifematIO.getStatuz(),varcom.oK))
		&& (isNE(lifematIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(lifematIO.getParams());
			fatalError600();
		}
		if (isEQ(lifematIO.getStatuz(),varcom.mrnf)) {
			sv.jlife.set("NONE");
			sv.jlifename.set(SPACES);
			goTo(GotoLabel.exit1690);
		}
		sv.jlife.set(lifematIO.getLifcnum());
		cltsIO.setClntnum(lifematIO.getLifcnum());
		getClientDetails1700();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),"1"))) {
			sv.jlifenameErr.set(e350);
			sv.jlifename.set(SPACES);
		}
		else {
			plainname();
			sv.jlifename.set(wsspcomn.longconfname);
		}
	}

protected void getClientDetails1700()
	{
		/*READ-CLTS*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))
		&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateScreen2010();
		checkForErrors2050();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.subfileRrn.set(1);
	}

protected void validateScreen2010()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5008", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsaaTransErr,"Y")) {
			if (isEQ(wsaaBatckey.batcBatctrcde,"T540")) {
				sv.chdrnumErr.set(t015);
			}
			else {
				sv.chdrnumErr.set(t014);
			}
		}
		if (isEQ(wsaaTransactionMatch,"N")) {
			sv.premstatusErr.set(h205);
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		try {
			nextProgram4010();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			releaseSoftlock4500();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(chdrmatIO.getPolinc(),1)) {
			keepsCovr4400();
			gensswrec.function.set("A");
			callGenssw4100();
			goTo(GotoLabel.exit4090);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5008", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
		while ( !((isNE(sv.select,SPACES))
		|| (isEQ(scrnparams.statuz,varcom.endp)))) {
			processScreen("S5008", sv);
			if ((isNE(scrnparams.statuz,varcom.oK))
			&& (isNE(scrnparams.statuz,varcom.endp))) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		if (isNE(sv.select,SPACES)) {
			covrmatIO.setPlanSuffix(sv.planSuffix);
			keepsCovr4400();
			gensswrec.function.set("A");
			callGenssw4100();
		}
	}

protected void callGenssw4100()
	{
		try {
			callGenssw4110();
		}
		catch (GOTOException e){
		}
	}

protected void callGenssw4110()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz,varcom.oK))
		&& (isNE(gensswrec.statuz,varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4110);
		}
		compute(wsaaSub1, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			gensToWsspProgs4300();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.programPtr.add(1);
	}

protected void gensToWsspProgs4300()
	{
		/*GENS-TO-WSSP-PROGS*/
		wsspcomn.secProg[wsaaSub1.toInt()].set(gensswrec.progOut[wsaaSub2.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
		/*EXIT*/
	}

protected void keepsCovr4400()
	{
		/*KEEPS*/
		covrmatIO.setFunction(varcom.keeps);
		covrmatIO.setPlanSuffix(sv.planSuffix);
		covrmatIO.setFormat(covrmatrec);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmatIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void releaseSoftlock4500()
	{
		releaseContract4500();
	}

protected void releaseContract4500()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmatIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}
}
