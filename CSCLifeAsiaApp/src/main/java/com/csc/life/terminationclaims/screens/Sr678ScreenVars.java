package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR678
 * @version 1.0 generated on 30/08/09 07:23
 * @author Quipoz
 */
public class Sr678ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(203);
	public FixedLengthStringData dataFields = new FixedLengthStringData(59).isAPartOf(dataArea, 0);
	public ZonedDecimalData aad = DD.aad.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData factor = DD.factor.copyToZonedDecimal().isAPartOf(dataFields,10);
	public ZonedDecimalData premunit = DD.premunit.copyToZonedDecimal().isAPartOf(dataFields,12);
	public ZonedDecimalData zssi = DD.zssi.copyToZonedDecimal().isAPartOf(dataFields,18);
	public FixedLengthStringData benpln = DD.benpln.copy().isAPartOf(dataFields,35);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,37);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,41);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,49);
	public ZonedDecimalData zunit = DD.zunit.copyToZonedDecimal().isAPartOf(dataFields,57);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 59);
	public FixedLengthStringData aadErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData factorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData premunitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData zssiErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData benplnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData zunitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 95);
	public FixedLengthStringData[] aadOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] factorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] premunitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] zssiOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] benplnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] zunitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(225);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(79).isAPartOf(subfileArea, 0);
	public ZonedDecimalData amtlife = DD.amtlife.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData amtyear = DD.amtyear.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public FixedLengthStringData benefits = DD.benefits.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData benfamt = DD.benfamt.copyToZonedDecimal().isAPartOf(subfileFields,49);
	public ZonedDecimalData copay = DD.copay.copyToZonedDecimal().isAPartOf(subfileFields,56);
	public ZonedDecimalData gdeduct = DD.gdeduct.copyToZonedDecimal().isAPartOf(subfileFields,59);
	public FixedLengthStringData hosben = DD.hosben.copy().isAPartOf(subfileFields,69);
	public ZonedDecimalData nofday = DD.nofday.copyToZonedDecimal().isAPartOf(subfileFields,74);
	public ZonedDecimalData seqno = DD.seqno.copyToZonedDecimal().isAPartOf(subfileFields,77);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 79);
	public FixedLengthStringData amtlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData amtyearErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData benefitsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData benfamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData copayErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData gdeductErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hosbenErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData nofdayErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData seqnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 115);
	public FixedLengthStringData[] amtlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] amtyearOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] benefitsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] benfamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] copayOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] gdeductOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hosbenOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] nofdayOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] seqnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 223);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr678screensflWritten = new LongData(0);
	public LongData Sr678screenctlWritten = new LongData(0);
	public LongData Sr678screenWritten = new LongData(0);
	public LongData Sr678protectWritten = new LongData(0);
	public GeneralTable sr678screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr678screensfl;
	}

	public Sr678ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {benefits, nofday, hosben, copay, benfamt, seqno, gdeduct, amtlife, amtyear};
		screenSflOutFields = new BaseData[][] {benefitsOut, nofdayOut, hosbenOut, copayOut, benfamtOut, seqnoOut, gdeductOut, amtlifeOut, amtyearOut};
		screenSflErrFields = new BaseData[] {benefitsErr, nofdayErr, hosbenErr, copayErr, benfamtErr, seqnoErr, gdeductErr, amtlifeErr, amtyearErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {crtable, itmfrm, itmto, zunit, benpln, premunit, zssi, factor, aad};
		screenOutFields = new BaseData[][] {crtableOut, itmfrmOut, itmtoOut, zunitOut, benplnOut, premunitOut, zssiOut, factorOut, aadOut};
		screenErrFields = new BaseData[] {crtableErr, itmfrmErr, itmtoErr, zunitErr, benplnErr, premunitErr, zssiErr, factorErr, aadErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr678screen.class;
		screenSflRecord = Sr678screensfl.class;
		screenCtlRecord = Sr678screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr678protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr678screenctl.lrec.pageSubfile);
	}
}
