package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl53screen extends ScreenRecord{
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 10, 13, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {18, 19, 2, 72}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl53ScreenVars sv = (Sjl53ScreenVars) pv;
		//clearInds(av, pfInds);
		//write(lrec, sv.Sjl53screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl53ScreenVars screenVars = (Sjl53ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.paymentFrom.setClassString("");
		screenVars.paymentDateDisp.setClassString("");
		screenVars.payee.setClassString("");
		screenVars.amountFrom.setClassString("");
		screenVars.amountTo.setClassString("");
		screenVars.paymentMethod.setClassString("");
		screenVars.bankcode.setClassString("");
		screenVars.paymentStatus.setClassString("");
	}

	/**
	 * Clear all the variables in Sjl53screen
	 */
	public static void clear(VarModel pv) {
		Sjl53ScreenVars screenVars = (Sjl53ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.paymentFrom.clear();
		screenVars.paymentDateDisp.clear();
		screenVars.payee.clear();
		screenVars.amountFrom.clear();
		screenVars.amountTo.clear();
		screenVars.paymentMethod.clear();
		screenVars.bankcode.clear();
		screenVars.paymentStatus.clear();
	}

}
