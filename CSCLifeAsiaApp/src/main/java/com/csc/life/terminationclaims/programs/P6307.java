/*
 * File: P6307.java
 * Date: 30 August 2009 0:42:35
 * Author: Quipoz Limited
 * 
 * Class transformed from P6307.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.fop.datatypes.Space;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.life.newbusiness.procedures.Nlgcalc;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrptsTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeptsTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.PtsdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.PtshpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.PyoupfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Ptsdpf;
import com.csc.life.terminationclaims.dataaccess.model.Ptshpf;
import com.csc.life.terminationclaims.dataaccess.model.Pyoupf;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.screens.S6307ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Td5h6rec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UlnkpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZutrpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ulnkpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;
import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.life.unitlinkedprocessing.tablestructures.Th608rec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsuframework.core.FeaConfg;//ILIFE-7956
import com.csc.life.contractservicing.recordstructures.PartSurrenderRec;//ILIFE-7956
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  P6307 - Part Surrender Details.
*  -------------------------------
*
*
*  The Part Surrender transaction  is  selected from  the  surrender
*  sub-menu  S6306/P6306.  Funds  are  displayed which relate to the
*  component being processed and from this  display  the  funds  for
*  surrendering  are  selected.  Note: where surrender is mentioned,
*  part surrender is implied.
*
*  Initialise
*  ----------
*
*  Read the  Contract  header  (function  RETRV)  and    read    the
*  relevant  data  necessary  for  obtaining the status description,
*  the Owner,  the  Life-assured,    Joint-life,  CCD,  Paid-to-date
*  and the Bill-to-date.
*
*  RETRV  the  "Plan"  or   the  policy  to  be  worked  on from the
*  COVRPTS  I/O  module,  it was selected  from  program  P5015  (or
*  defaulted  if  only  one   policy present). If the Plan suffix in
*  the key "kept" is zero, then funds  over  the entire Plan may  be
*  surrendered,  otherwise   funds  over  a    single  policy may be
*  surrendered.
*
*  RETRV  the  life to be worked on from  the  LIFECLM  I/O  module,
*  it    was  selected  (or defaulted if only one life present) from
*  program  P5088.  For  the  life  selected  output  a  highlighted
*  'asterisk' to the corresponding life on the screen.
*
*  LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To  obtain  the  life  assured and joint-life details (if any) do
*  the following:
*
*       - READR the  life  details  using    LIFEPTS  (for  this
*       contract  number,  joint  life  number '00'). Format the
*       name accordingly.
*
*       - READR the joint-life details using LIFEPTS  (for  this
*       contract  number,  joint  life  number '01'). Format the
*       name accordingly.
*
*
*  Obtain the  necessary  descriptions  by    calling  'DESCIO'  and
*  output the descriptions of the statuses to the screen.
*
*  Format Name:
*
*       Read   the  client  details  record  and  use  the  relevant
*       copybook in order to format the required names.
*
*  Build the Coverage/Rider review details:
*
*       If the Part surrender is to  occur  over  the  entire  Plan,
*       then  read  all  the  coverage/riders for each policy within
*       the Plan. The literal "Policy no."  and  the  policy  number
*       field are non-display when the entire Plan is selected.
*
*       For  each  policy within a Plan being surrendered, read only
*       the relevant coverage/rider details.
*
*       NB. The currency is decided by the surrender subroutine  and
*       passed back.
*
*       If  all  the  returned  details  are  of  the  same currency
*       throughout, then default the currency field  at  the  bottom
*       of the screen to the same currency.
*
*  For each coverage/rider attached to the policy:
*
*       -  access  table  T5542 (keyed on unit withdrawal method and
*       currency). The frequency is used to  access  an  array  from
*       which  the  required  limits  are obtained. Ensure that this
*       coverage/rider is greater then the  minimum  permitted  time
*       period  from  the Contract Commencement Date (in months). If
*       it does not satisfy this condition then protect  the  %  and
*       actual value fields from data entry (selection).
*
*       -  call  the  surrender calculation subroutine as defined on
*       T5687; this method is used to access T6598,  which  contains
*       the  subroutines  necessary  for  the  surrender calculation
*       processing.
*
*  Linkage area passed to the surrender calculation subroutine:-
*
*             - company
*             - contract header number
*             - suffix
*             - life number
*             - joint-life number
*             - coverage
*             - rider
*             - crtable
*             - language
*             - estimated value
*             - actual value
*             - currency
*             - element code
*             - description
*             - type code
*             - status
*
*           DOWHILE
*              surrender calculation subroutine status not = ENDP
*                 - load the subfile record
*                 - accumulate single currency if applicable
*                 - call surrender calculation subroutine
*           ENDDO
*
*  Surrender claim subroutines may work at  Plan  or  policy  level,
*  therefore  the  DOWHILE  process may be processed for end of COVR
*  for a Plan or a Policy, determine the  type  of  surrender  being
*  processed.
*
*  Total Estimated Value:
*
*       this  amount  is  the total of the estimated values from the
*       coverage/rider subfile review. This amount  is  the  sum  of
*       the  individual  amounts returned by the subroutine  (if all
*       the amounts returned are in the same currency).
*
*  Total Actual Value:
*
*       this amount is the total  of  the  actual  values  from  the
*       coverage/rider  subfile review that were entered by the user
*       less any outstanding policy loans. At  initialisation,  this
*       amount is zero.
*
*  The  above  details  are returned  to  the  user  and if the user
*  wishes to continue with the Part Surrender, the required data  is
*  entered. If no data is entered,  defaults  apply,  otherwise  the
*  user opts for KILL and exits from this transaction.
*
*  Validation
*  ----------
*
*  If KILL was entered, then skip the remainder  of  the  validation
*  and exit from the program.
*
*  Effective Date (optional)
*
*       Check  that  the Effective Date entered on the screen is not
*       less than the contract commencement date (CCD) and  that  it
*       is not greater than today's date. Default is today's date.
*
*  Currency (optional)
*
*  Validated by the I/O module.
*
*       A  blank  entry  defaults to the contract currency or to the
*       fund currency if the fund currencies are all the same.
*
*  Amount required from this component
*
*       A value may or may not be entered. If a  value  is  entered,
*       then  access  table T5542 and compare this value against the
*       minimum  withdrawal  amount  permitted.  The  actual   value
*       entered  subtracted  from  the  estimated value must leave a
*       minimum amount (T5542).
*
*  Percentage required from this component
*
*       A value may or may not be entered. If  a  value  exists,  it
*       must  not  be greater than 100%. If a percentage is entered,
*       the monetary amount must satisfy/leave the required  minimum
*       amounts in the fund.
*
*  Total amount required for part surrender
*
*       A  value  may  or may not be entered. If a value is entered,
*       the amount must not be greater than the estimated total.  It
*       also  must  be spread pro rata across the funds and compared
*       against the minimum amounts remaining  for  each  individual
*       fund (access T5542).
*
*  Total percentage required for part surrender
*
*       A  value  may  or  may not be entered. If a value exists, it
*       must not be greater than 100%. This percentage  amount  must
*       be  spread  pro  rata  across all the permitted funds. Again
*       refer to T5542 for the permitted minimum encashment  amounts
*       and remaining amounts.
*
*       At  the  subfile  level  an  amount  or  a percentage may be
*       entered but not both. An  amount  for  one  coverage  and  a
*       percentage for another may be entered.
*
*       If  entries  are  made  at  the subfile level then the total
*       amount or percentage field at the bottom of the  screen  may
*       not be entered.
*
*  If CALC  is  pressed,  then  the  screen is re-displayed and if a
*  currency  was entered, then the total  estimated  value  and  the
*  total   actual   value   are  adjusted  for  this  new  currency.
*  Re-calculate/convert all of the amounts held in the subfile.
*
*
*
*  Updating
*  --------
*
*  If the KILL function key was pressed, skip the updating and  exit
*  from the program.
*
*  CREATE PART SURRENDER CLAIMS HEADER RECORD.
*
*  Create  a  surrender  claim  header  record  (PTSHCLM)  for  this
*  contract and for this policy (i.e. a "PTSH" record is created for
*  each  policy) and output the details entered on the screen. Keyed
*  by "Company, Contract number, Suffix".
*
*                - company
*                - contract number
*                - suffix
*                - transaction
*                - life number
*                - joint-life number
*                - effective date
*                - currency
*                - total amount for surrender
*                - total percentage
*
*  CREATE PART SURRENDER CLAIMS DETAILS RECORD.
*
*  DOWHILE there are records in the subfile
*
*  - create a PTSDCLM record  for  each  subfile  entry  output  the
*  following   fields,  Keyed  by:  Company,  Contract  no,  Suffix,
*  Coverage, Rider, Element type
*
*                - company
*                - contract number
*                - suffix
*                - transaction
*                - life number
*                - joint-life number
*                - coverage
*                - rider
*                - element type (4 character code)
*                - description
*                - re-insurance indicator
*                - currency
*                - percentage of coverage/rider
*                - actual value of coverage/rider
*                - estimated value
*                - type (1 character code) returned by
*                  surrender calc.
*
*
*  ENDDO
*
*
*  Next Program
*  ------------
*
*  For KILL or 'Enter', add 1 to the program pointer and exit.
*
*
*  Modifications
*  -------------
*
*  Include the following logical views (accessing  only  the  fields
*  required).
*
*            - CHDRPTS
*            - LIFEPTS
*            - COVRPTS
*            - PTSHCLM
*            - PTSDCLM
*****************************************************************
* </pre>
*/
public class P6307 extends ScreenProgCS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(P6307.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6307");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private ZonedDecimalData wsaaSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaSwitch, "1");
	private Validator partPlan = new Validator(wsaaSwitch, "2");
	private Validator summaryPartPlan = new Validator(wsaaSwitch, "3");
	private ZonedDecimalData wsaaCovrSuffix = new ZonedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData storeCovtable = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData storeCrrcd = new PackedDecimalData(8, 0).init(ZERO);
	private String penaltyOnly = "N";
	protected FixedLengthStringData wsaaStoredLife = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);

	private ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator detailsSameCurrency = new Validator(wsaaCurrencySwitch, "0");
	private Validator detailsDifferent = new Validator(wsaaCurrencySwitch, "1");
	private Validator totalCurrDifferent = new Validator(wsaaCurrencySwitch, "2");

	private FixedLengthStringData wsaaGotSflEntry = new FixedLengthStringData(1).init("N");
	private Validator detailRecordEntered = new Validator(wsaaGotSflEntry, "Y");

	private FixedLengthStringData wsaaGotPenEntry = new FixedLengthStringData(1).init("N");
	private Validator penaltyRecordEntered = new Validator(wsaaGotPenEntry, "Y");

	private FixedLengthStringData wsaaNoSurrmeth = new FixedLengthStringData(1).init("N");
	private Validator noSurrenderMethod = new Validator(wsaaNoSurrmeth, "Y");
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(0);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData feeValue = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaFee = new ZonedDecimalData(17, 2);
	private PackedDecimalData runningTotalSurr = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData runningTotalFee = new PackedDecimalData(18, 3).init(0);
	private PackedDecimalData runningTotalEst = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaComponentEst = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaEstTotal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotalamtLeft = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaEstMatValue = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOrigTotEst = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaOrigCurr = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsaaCtrctAmount = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotalSurrCharge = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompActvalue = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaCompEstvalue = new PackedDecimalData(18, 5);
	
	/*ILIFE-6709*/
	private PackedDecimalData actCumulativeVal = new PackedDecimalData(22, 2);
	/*ILIFE-6709*/
	
	private FixedLengthStringData wsaaCurrcd = new FixedLengthStringData(3).init(SPACES);
	protected FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private String wsaaCalc = "";
	private FixedLengthStringData wsaaReturn = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaa1stTime = new FixedLengthStringData(1);
	private Validator firstTimeThru = new Validator(wsaa1stTime, "Y");
	private ZonedDecimalData index1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTotFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaUnproc = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaConvertEstTot = new ZonedDecimalData(17, 2).setUnsigned();
	private BinaryData wsaaRrn = new BinaryData(9, 0);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaIsT5542Ok = new FixedLengthStringData(1);
	private Validator t5542NotOk = new Validator(wsaaIsT5542Ok, "N");
	private Validator t5542IsOk = new Validator(wsaaIsT5542Ok, "Y");

	private FixedLengthStringData wsaaT5542Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5542Meth = new FixedLengthStringData(4).isAPartOf(wsaaT5542Key, 0);
	private FixedLengthStringData wsaaT5542Curr = new FixedLengthStringData(3).isAPartOf(wsaaT5542Key, 4);

	private FixedLengthStringData wsaaEffDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaEffYy = new ZonedDecimalData(4, 0).isAPartOf(wsaaEffDate, 0).setUnsigned();
	private ZonedDecimalData wsaaEffMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffDate, 4).setUnsigned();
	private ZonedDecimalData wsaaEffDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffDate, 6).setUnsigned();

	private FixedLengthStringData wsaaTranDates = new FixedLengthStringData(372);
	private FixedLengthStringData[] wsaaTranDay = FLSArrayPartOfStructure(372, 1, wsaaTranDates, 0);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	protected ChdrptsTableDAM chdrptsIO = new ChdrptsTableDAM();
	protected CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
		/*Life and Joint Life - Part Surr*/
	private LifeptsTableDAM lifeptsIO = new LifeptsTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();

	private Batckey wsaaBatckey = new Batckey();
	protected Srcalcpy srcalcpy = new Srcalcpy();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected T5687rec t5687rec = new T5687rec();
	protected T6598rec t6598rec = new T6598rec();
	private T5542rec t5542rec = new T5542rec();
	private Th506rec th506rec = new Th506rec();
	private Th608rec th608rec = new Th608rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6307ScreenVars sv = ScreenProgram.getScreenVars( S6307ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private ErrorsInner errorsInner = new ErrorsInner();
	private ExternalisedRules er = new ExternalisedRules();
	private List<Itempf> itempfList;
	protected CovrpfDAO covrClmdao =getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	protected Covrpf covrClm=null;
	protected List<Covrpf> covrClmList=null;
	protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Map<String, List<Itempf>> t5687Map;
	private Map<String, List<Itempf>> t5542Map;
	private Covrpf covrSur=null;
	private List<Covrpf> covrSurList=null;
	private UtrnpfDAO utrnDao =getApplicationContext().getBean("utrnpfDAO",UtrnpfDAO.class);
	private Utrnpf utrnObj=null;
	List<Utrnpf> utrnList=null;
	boolean checkPtsdFlag=false;
	private PtshpfDAO ptshDao =getApplicationContext().getBean("ptshpfDAO",PtshpfDAO.class);
	private Ptshpf ptshObj=null;
	List<Ptshpf> ptshList=null;
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();//NLG
	NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);
	Nlgtpf nlgtpf = new Nlgtpf();
	PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	Ptrnpf ptrnpf = new Ptrnpf();
    private static final String t6640 = "T6640";
	//ILIEF-5140 
	UlnkpfDAO ulnkpfDAO = getApplicationContext().getBean("ulnkpfDAO",UlnkpfDAO.class);
	Map<String,BigDecimal> mapOfUlnkpf =null;
	private PackedDecimalData runningTotalFeeExt = new PackedDecimalData(18, 3).init(0);
	//ILIEF-5140 		  
	private Zutrpf zutrpf = new Zutrpf();//ILIFE-5459
	private ZutrpfDAO zutrpfDAO = getApplicationContext().getBean("zutrpfDAO", ZutrpfDAO.class);//ILIFE-5459
	private PackedDecimalData wsaaBusinessDate = new PackedDecimalData(8, 0);//ILIFE-5459
	//ILIFE-6594
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private PtsdpfDAO ptsdDao =getApplicationContext().getBean("ptsdpfDAO",PtsdpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Clntpf clts = null;
	private Map<String, List<Itempf>> tr52dListMap;
	private Map<String, List<Itempf>> tr52eListMap;
	private Map<String,Descpf> t5688Map;
	private Map<String,Descpf> t3588Map;
	private List<Ptsdpf> ptsdtrgList=null;
	protected Payrpf payrpf = null;
	private PackedDecimalData wsaaDuration = new PackedDecimalData(7, 2);		
	private PackedDecimalData wsaaDoBDate = new PackedDecimalData(8, 0);
	private boolean isAusPreserv=false;
	/*ICIL-254*/
	boolean susur002Permission = false;	
	private Td5h6rec td5h6rec = new Td5h6rec();
	private BabrpfDAO babrpfDao = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class);
	private PyoupfDAO pyoupfDAO = getApplicationContext().getBean("pyoupfDAO", PyoupfDAO.class);
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	
	private Chdrpf chdrenq = null;
	private Ptshpf ptshpf = null;
	private Ptsdpf ptsdpf = null;
	protected Itempf itempf = null;
	private Descpf descpf = null;
	private List<Ptsdpf> ptsdpflist = null;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class); //ILB-462
	private PtsdpfDAO ptsdpfDAO = getApplicationContext().getBean("ptsdpfDAO",PtsdpfDAO.class); //ILB-462
	private Ptsdpf wsaaPtsdpf = null;
	
	private static final String td5h6 = "TD5H6";
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();

	private int len = 0;
	private StringBuffer banktemp = null;
	private Pyoupf pyoupf = new Pyoupf();
	private BabrTableDAM babrIO = new BabrTableDAM();
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private Clbapf clbapf;
	private Babrpf babrpf = null;
	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaBankdesc = new FixedLengthStringData(30).isAPartOf(wsaaBankkey, 0);
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private final String wsaaLargeName = "LGNMS";
	private ChdrpfDAO chdrDAO = getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private List<Chdrpf> chdrpflist = null;
	private Chdrpf chdrobj = null;
	boolean maxwithConfig = false;//ILIFE-7956
	private ZonedDecimalData wsaaTotal = new ZonedDecimalData(17,2);//ILIFE-7956
	private PartSurrenderRec partSurrenderrec = new PartSurrenderRec();   //ILIFE-7956 
	private int count=0;//ILIFE-7956
	private BigDecimal TotalamtLeft;
	private PackedDecimalData wsaaTemp = new PackedDecimalData(17, 2).init(ZERO).setUnsigned();//ILIFE-7956    //IBPLIFE-2063
	private int addFund=1;//ILIFE-7956
	public boolean vFlag = false; //ILIFE-7956
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue1030, 
		checkPtsd1050, 
		att1620, 
		att1820, 
		checkForErrors2050, 
		exit2090, 
		updateErrorIndicators2270, 
		penaltyCheck, 
		updSfl2600, 
		exit2690, 
		nextSfl2818, 
		exit2819, 
		validatePenaltyExit, 
		readSubfile3070,
		updateBank3080,
		exit3090, 
		checkTots3320, 
		exit3390, 
		exit5190
	}

	public P6307() {
		super();
		screenVars = sv;
		new ScreenModel("S6307", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case continue1030: 
					continue1030();
				case checkPtsd1050: 
					checkPtsd1050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{	
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaSwitch.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		wsaaCurrencySwitch.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaFee.set(ZERO);
		wsaaCovrSuffix.set(ZERO);
		wsaaCtrctAmount.set(ZERO);
		wsaaOrigTotEst.set(ZERO);
		wsaaStoredLife.set(SPACES);
		wsaaStoredCoverage.set(SPACES);
		wsaaStoredRider.set(SPACES);
		wsaaStoredCurrency.set(SPACES);
		wsaaLife.set(SPACES);
		wsaaJlife.set(SPACES);
		wsaaCrtable.set(SPACES);
		wsaaUnproc.set(SPACES);
		wsaaIsT5542Ok.set("Y");
		wsaa1stTime.set("Y");
		covrsurIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		chdrptsIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrptsIO);
		if (isNE(chdrptsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
		initSmartTable();
		srcalcpy.ptdate.set(chdrptsIO.getPtdate());
		/* decide which part of the plan is being surrended*/
		srcalcpy.planSuffix.set(covrsurIO.getPlanSuffix());
		if (isEQ(covrsurIO.getPlanSuffix(),ZERO)) {
			wsaaSwitch.set(1);
			wsaaCovrSuffix.set(ZERO);
		}
		else {
			if (isGT(covrsurIO.getPlanSuffix(),chdrptsIO.getPolsum())
			|| isEQ(chdrptsIO.getPolsum(),1)) {
				wsaaSwitch.set(2);
				wsaaCovrSuffix.set(covrsurIO.getPlanSuffix());
			}
			else {
				wsaaCovrSuffix.set(ZERO);
				wsaaSwitch.set(3);
			}
		}
		ptshpf = new Ptshpf();
		ptshpf.setPlanSuffix(new BigDecimal(covrsurIO.getPlanSuffix().toInt()));
		/* MOVE CHDRPTS-BILLFREQ       TO SURC-BILLFREQ.                */
		srcalcpy.chdrCurr.set(chdrptsIO.getCntcurr());
		srcalcpy.polsum.set(ZERO);
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		susur002Permission = FeaConfg.isFeatureExist("2", "SUSUR002", appVars, "IT"); //ICIL-254
	    if (!susur002Permission) {
			sv.susur002flag.set("N");
			sv.payeeOut[varcom.nd.toInt()].set("Y");
			sv.payeeOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.susur002flag.set("Y");
		}
		sv.planSuffix.set(ZERO);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.estimateTotalValue.set(ZERO);
		sv.prcnt.set(ZERO);
		sv.percreqd.set(ZERO);
		sv.totalamt.set(ZERO);
		sv.totalfee.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.clamant.set(ZERO);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		
		sv.reserveUnitsDate.set(varcom.vrcmMaxDate);// ILIFE-5459
		sv.reserveUnitsInd.set(SPACES);// ILIFE-5459
		//sv.rsuninOut[varcom.pr.toInt()].set("N"); // ILIFE-5459
		
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		//ILIFE-6594
		
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrptsIO.getChdrcoy().toString());
		itempf.setItemtabl("TH506");
		itempf.setItemitem(chdrptsIO.getCnttype().toString());
		itempf.setItemseq("  ");
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		
		 if (itempf ==null) {
		  fatalError600();
		  }else {
		  th506rec.th506Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
		 }
		
		if (isEQ(th506rec.ind,"Y")) {
			sv.effdate.set(varcom.vrcmMaxDate);
			validateEffdate2700();
		}
		else {
			sv.effdate.set(datcon1rec.intDate);
		}
		srcalcpy.currcode.set(chdrptsIO.getCntcurr());
		sv.occdate.set(chdrptsIO.getOccdate());
		sv.chdrnum.set(chdrptsIO.getChdrnum());
		sv.cnttype.set(chdrptsIO.getCnttype());

		if(t5688Map != null){
			if(t5688Map.get(chdrptsIO.getCnttype().toString()) != null){
				sv.ctypedes.set(t5688Map.get(chdrptsIO.getCnttype().toString()).getLongdesc());
			}else{
				sv.ctypedes.fill("?");
			}
		}else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrptsIO.getCownnum());
		//ILIFE-6594 
		clts = getClientDetails1200(chdrptsIO.getCownnum().toString());
		if (clts == null || isNE(clts.getValidflag(),1)) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
			wsaaDoBDate.set(clts.getCltdob());
		}
		//Ilife-6594 add by liwei
		payrpf = payrpfDAO.getpayrRecord(wsspcomn.company.toString(),chdrptsIO.getChdrnum().toString(),1);
		sv.btdate.set(payrpf.getBtdate());
		sv.ptdate.set(payrpf.getPtdate());
		if (isNE(sv.ptdate,sv.btdate)) {
			sv.ptdateErr.set(errorsInner.g008);
		}
		/*    Retrieve contract status from T3623*/
        descpf = descDAO.getdescData("IT", "T3623", chdrptsIO.getStatcode().toString().trim(), wsspcomn.company.toString().trim(), wsspcomn.language.toString().trim());
		if(descpf != null){
			sv.rstate.set(descpf.getShortdesc());
			
		}else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		if(t3588Map != null){
			if(t3588Map.get(chdrptsIO.getPstatcode().toString()) != null){
				sv.pstate.set(t3588Map.get(chdrptsIO.getPstatcode().toString()).getShortdesc());
			}else{
				sv.pstate.fill("?");
			}
		}else {
			sv.pstate.fill("?");
		}
		lifeptsIO.setDataArea(SPACES);
		lifeptsIO.setChdrcoy(chdrptsIO.getChdrcoy());
		lifeptsIO.setChdrnum(chdrptsIO.getChdrnum());
		lifeptsIO.setLife(covrsurIO.getLife());
		wsaaLife.set(covrsurIO.getLife());
		lifeptsIO.setJlife("00");
		lifeptsIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifeptsIO);
		if (isNE(lifeptsIO.getStatuz(),varcom.oK)
		&& isNE(lifeptsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeptsIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,lifeptsIO.getChdrcoy())
		|| isNE(chdrptsIO.getChdrnum(),lifeptsIO.getChdrnum())
		|| isNE(lifeptsIO.getJlife(),"00")
		&& isNE(lifeptsIO.getJlife(), "  ")
		|| isEQ(lifeptsIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(lifeptsIO.getParams());
			fatalError600();
		}
		/* Move the last record read to the screen and get the*/
		sv.lifcnum.set(lifeptsIO.getLifcnum());

		//ILIFE-6594 
		clts = getClientDetails1200(lifeptsIO.getLifcnum().toString());
		/* Get the confirmation name.*/
		if (clts == null|| isNE(clts.getValidflag(),1)) {
			sv.linsnameErr.set(errorsInner.e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		
		if(susur002Permission) {
			initpayout1020();
		}
			

		/* Get the confirmation name.*/
		if (clts == null|| isNE(clts.getValidflag(),1)) {
				sv.jlinsnameErr.set(errorsInner.e304);
				sv.jlinsname.set(SPACES);
		}else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
		if(susur002Permission) {
				sv.payee.set(chdrptsIO.getCownnum());
				sv.bankacckey.set(SPACES);
				sv.bankkey.set(SPACES);
				sv.reqntype.set(chdrptsIO.getReqntype());
				 
				//ILIFE-2472-START
		     if( null!=chdrptsIO.getPayclt() && isNE(chdrptsIO.getPayclt(), SPACES) ) {
					sv.payee.set(chdrptsIO.getPayclt());
				}
				//ILIFE-2472-END
				a1000GetPayorname();
				sv.payeename.set(namadrsrec.name);
				wsaaClntkey.set(SPACES);
				//ILIFE-2472-START
				if (isNE(chdrptsIO.getCownnum(),SPACES)) {
					wsaaClntkey.set(wsspcomn.clntkey);
					wsspcomn.clntkey.set(SPACES);
					StringUtil stringVariable1 = new StringUtil();
					stringVariable1.addExpression(chdrptsIO.getCownpfx());
					stringVariable1.addExpression(chdrptsIO.getCowncoy());
					stringVariable1.addExpression(chdrptsIO.getCownnum());
					stringVariable1.setStringInto(wsspcomn.clntkey);
				}
				
				if ( isNE(sv.reqntype, '4') || isNE(sv.reqntype, 'C') ) {
					sv.bankacckeyOut[varcom.nd.toInt()].set("Y");
					sv.crdtcrdOut[varcom.nd.toInt()].set("Y");
					sv.bankacckey.set(SPACES);
					sv.crdtcrd.set(SPACES);
					
				}
		}		
		/*    look for joint life.*/
		lifeptsIO.setJlife("01");
		lifeptsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeptsIO);
		if ((isNE(lifeptsIO.getStatuz(),varcom.oK))
		&& (isNE(lifeptsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(lifeptsIO.getParams());
			fatalError600();
		}
		if (isEQ(lifeptsIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.continue1030);
		}
		sv.jlifcnum.set(lifeptsIO.getLifcnum());
		
	}

protected void initpayout1020() {
	
	sv.payee.set(chdrptsIO.getCownnum());
	

}
protected void initSmartTable(){
	String company =  wsspcomn.company.toString().trim();
	t5687Map = itemDAO.loadSmartTable("IT", company, "T5687");
	t5542Map = itemDAO.loadSmartTable("IT", company, "T5542");
	tr52dListMap = itemDAO.loadSmartTable("IT", chdrptsIO.getChdrcoy().toString(), "TR52D");
	tr52eListMap = itemDAO.loadSmartTable("IT", chdrptsIO.getChdrcoy().toString(), "TR52E");
	t5688Map = descDAO.getItems("IT", company, "T5688", wsspcomn.language.toString().trim());
	t3588Map = descDAO.getItems("IT", company, "T3588", wsspcomn.language.toString().trim());
}
protected void continue1030()
	{
		if (isEQ(covrsurIO.getPlanSuffix(),ZERO)) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
		}
		else {
			wsaaPlanSuffix.set(covrsurIO.getPlanSuffix());
			sv.planSuffix.set(covrsurIO.getPlanSuffix());
		}
		if (wholePlan.isTrue()) {
			wholePlan1300();
		}
		else {
			partPlan1700();
		}
		/* Write blank subfile if its empty. This is to initialise the     */
		/* the subfile area.                                               */
		if (isEQ(scrnparams.subfileRrn, 1)) {
			blankSubfile1900x();
		}
		if (summaryPartPlan.isTrue()) {
			compute(wsaaEstimateTot, 3).setRounded(div(wsaaEstimateTot,chdrptsIO.getPolsum()));
			compute(wsaaCtrctAmount, 3).setRounded(div(wsaaCtrctAmount,chdrptsIO.getPolsum()));
		}
		if (detailsSameCurrency.isTrue()) {
			wsaaOrigTotEst.set(wsaaEstimateTot);
			wsaaOrigCurr.set(wsaaStoredCurrency);
		}
		else {
			wsaaOrigCurr.set(chdrptsIO.getCntcurr());
			wsaaOrigTotEst.set(wsaaCtrctAmount);
		}
		if (detailsSameCurrency.isTrue()) {
			sv.estimateTotalValue.set(wsaaEstimateTot);
			if (isNE(wsaaStoredCurrency,chdrptsIO.getCntcurr())) {
				sv.currcd.set(wsaaStoredCurrency);
			}
			else {
				sv.currcd.set(chdrptsIO.getCntcurr());
			}
		}
		wsaaStoredCurrency.set(SPACES);

		utrnObj=new Utrnpf();
		utrnObj.setChdrcoy(chdrptsIO.getChdrcoy().toString().trim());
		utrnObj.setChdrnum(chdrptsIO.getChdrnum().toString().trim());
		utrnList=utrnDao.searchUtrnRecord(utrnObj);
		if(utrnList.isEmpty()){
			checkPtsd1050();
		}
		else{
			for(int intUtrnCounter=0;intUtrnCounter<utrnList.size();intUtrnCounter++){
				utrnObj=utrnList.get(intUtrnCounter);
				if(!utrnObj.getFeedbackInd().equals("Y")){
					scrnparams.errorCode.set(errorsInner.h355);
					wsaaUnproc.set("Y");
					checkPtsd1050();
				}
			}
		}
		checkPtsdFlag=false;
	}


protected void checkPtsd1050()
	{
		if(!checkPtsdFlag){
			Ptsdpf ptsdtrg = new Ptsdpf();
			ptsdtrg.setChdrcoy(chdrptsIO.getChdrcoy().toString().trim());
			ptsdtrg.setChdrnum(chdrptsIO.getChdrnum().toString().trim());
			ptsdtrg.setPlanSuffix(new BigDecimal(0));
			ptsdtrg.setTranno(0);
			ptsdtrgList=ptsdDao.serachPtsdRecord(ptsdtrg);
			checkPtsdFlag=true;
		}
		if(checkPtsdFlag){
			if (ptsdtrgList == null || ptsdtrgList.size() <= 0) {
				return ;
			}
		}
				
	}

protected Clntpf getClientDetails1200(String clntnum){
	Clntpf clts = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString().trim(),clntnum.trim());
	return clts;
}

protected void wholePlan1300(){
	covrClm=new Covrpf();
	covrClm.setChdrcoy(covrsurIO.getChdrcoy().toString().trim());
	covrClm.setChdrnum(covrsurIO.getChdrnum().toString().trim());
	covrClmList=covrClmdao.selectCovrClm(covrClm);

	List<Covrpf> tempList = new ArrayList();
	for(Covrpf covrdata: covrClmList)
	{
		if(covrdata.getRider().equals("00"))
			tempList.add(covrdata);
	}
	
	if(!tempList.isEmpty()){
	
		for(int intCounter=0;intCounter<=tempList.size()-1;intCounter++){
			covrClm=tempList.get(intCounter);
				processComponents1350();
		}

	}
}
protected void processComponents1350()
	{
		read1351();
		nextrec1355();
	}

protected void read1351()
{
	wsaaCrtable.set(covrClm.getCrtable());
	obtainSurrenderCalc1400();
	sv.coverage.set(covrClm.getCoverage());
	sv.hcover.set(covrClm.getCoverage());
	if (isEQ(covrClm.getRider(),"00")) {
		sv.rider.set(SPACES);
		sv.crrcd.set(covrClm.getCrrcd());
	}
	else {
		sv.crrcd.set(covrClm.getCrrcd());
		sv.rider.set(covrClm.getRider());
	}
	if (isNE(covrClm.getRider(),"00")
	&& isNE(covrClm.getRider(), "  ")) {
		sv.coverage.set(SPACES);
	}
	sv.cnstcur.set(covrClm.getPremCurrency());
	sv.hcnstcur.set(covrClm.getPremCurrency());
	srcalcpy.currcode.set(covrClm.getPremCurrency());
	srcalcpy.estimatedVal.set(ZERO);
	srcalcpy.actualVal.set(ZERO);
	srcalcpy.chdrChdrcoy.set(covrClm.getChdrcoy());
	srcalcpy.chdrChdrnum.set(covrClm.getChdrnum());
	srcalcpy.lifeLife.set(covrClm.getLife());
	srcalcpy.lifeJlife.set(covrClm.getJlife());
	srcalcpy.covrCoverage.set(covrClm.getCoverage());
	srcalcpy.covrRider.set(covrClm.getRider());
	srcalcpy.crtable.set(covrClm.getCrtable());
	srcalcpy.crrcd.set(covrClm.getCrrcd());
	srcalcpy.convUnits.set(covrClm.getConvertInitialUnits());
	srcalcpy.pstatcode.set(covrClm.getPstatcode());
	srcalcpy.status.set(SPACES);
	srcalcpy.polsum.set(chdrptsIO.getPolsum());
	srcalcpy.language.set(wsspcomn.language);
	while ( !(isEQ(srcalcpy.status,varcom.endp))) {
		callSurMethodWhole1600();
	}
	
}

protected void nextrec1355()
	{
		wsaaStoredLife.set(covrClm.getLife());
		wsaaStoredCoverage.set(covrClm.getCoverage());
		wsaaStoredRider.set(covrClm.getRider());
	}

protected void obtainSurrenderCalc1400()
	{
	List<Itempf> itempfList = new ArrayList<Itempf>();
	String keyItemitem = wsaaCrtable.toString().trim();
	Iterator<Itempf> itemIterator=null;
	if(t5687Map.containsKey(keyItemitem)){
		itempfList=t5687Map.get(keyItemitem);
		itemIterator = itempfList.iterator();
		while(itemIterator.hasNext()){
			Itempf itempf=itemIterator.next();
			if (itempf.getItmfrm().toString().equals(chdrptsIO.getOccdate().toString().trim())) {
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				break;
			}else{
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		}
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(payrpf.getBillfreq());
		}
	}
	
	itempf = new Itempf();
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl("T6598");
	itempf.setItempfx("IT");
	itempf.setItemitem(t5687rec.partsurr.toString());
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	if(itempf == null) {
		t6598rec.t6598Rec.set(SPACES);	
	}
	t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			
}

protected void callSurMethodWhole1600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read1610();
					writeToSubfile1615();
				case att1620: 
					att1620();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read1610()
	{
		srcalcpy.effdate.set(sv.effdate);
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))//ILIFE-7423 
		{
			srcalcpy.type.set("P");
		}
		else
		{
			if(isNE(srcalcpy.type,"J"))			
				srcalcpy.type.set("P");
		} 

		if (isGT(covrClm.getInstprem(),ZERO)) {
			srcalcpy.singp.set(covrClm.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrClm.getSingp());
		}
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		if (isEQ(t6598rec.calcprog,SPACES)) {
			sv.htype.set(SPACES);
			sv.fieldType.set(SPACES);
			sv.shortds.set(SPACES);
			sv.fund.set(SPACES);
			sv.cnstcur.set(SPACES);
			sv.hcnstcur.set(SPACES);
			sv.estMatValue.set(0);
			sv.hemv.set(0);
			sv.actvalue.set(0);
			sv.hactval.set(0);
			sv.percreqd.set(0);
			srcalcpy.estimatedVal.set(0);
			srcalcpy.actualVal.set(0);
			srcalcpy.status.set(varcom.endp);
			goTo(GotoLabel.att1620);
		}
		sv.percreqd.set(ZERO);
		

		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		/*ILIFE-7548 start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())&& er.isExternalized(srcalcpy.crtable.toString(),txcalcrec.cnttype.toString()))) //ILIFE-7423 
		{
			srcalcpy.cnttype.set(chdrptsIO.getCnttype());  //ILIFE-9394
			callProgramX(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		/*ILIFE-7548 end*/
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrptsIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				srcalcpy.status.set(varcom.endp);
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.endf.set("Y");
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
 		if (isEQ(srcalcpy.status,varcom.bomb)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isNE(srcalcpy.status,varcom.oK)
		&& isNE(srcalcpy.status,varcom.endp)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isEQ(srcalcpy.status, varcom.oK)) {
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			a000CallRounding();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			a000CallRounding();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		/*    IF SURC-ENDF  = 'Y'                                          */
		/*       GO TO 1640-EXIT.                                          */
		/*   IF SURC-NE-UNITS = 'Y'                                       */
		/*      MOVE 'Y'                    TO WSSP-EDTERROR              */
		/*      MOVE H374  TO  SCRN-ERROR-CODE                            */
		/*   END-IF.                                                      */
		/*   IF SURC-TM-UNITS = 'Y'                                       */
		/*      MOVE 'Y'                    TO WSSP-EDTERROR              */
		/*      MOVE H375  TO  SCRN-ERROR-CODE                            */
		/*   END-IF.                                                      */
		/* Ref 4/T004-01 in amendment history.                          */
		/* IF SURC-PS-NOT-ALLWD = 'Y'                                   */
		/*    MOVE 'Y'                    TO WSSP-EDTERROR              */
		/*    MOVE 'Y'                    TO S6307-EFFDATE-OUT(RI) <019>*/
		/*   MOVE H376                   TO SCRN-ERROR-CODE            */
		/*    MOVE H376                   TO S6307-EFFDATE-ERR     <019>*/
		/* END-IF.                                                 <019>*/
		if (isEQ(srcalcpy.psNotAllwd,"T")) {
			wsspcomn.edterror.set("Y");
			scrnparams.errorCode.set(errorsInner.h388);
		}
		if (isEQ(srcalcpy.currcode,SPACES)
		&& isNE(srcalcpy.type,"C")
		&& isNE(srcalcpy.type,"J")) {
			sv.cnstcur.set(chdrptsIO.getCntcurr());
			sv.hcnstcur.set(chdrptsIO.getCntcurr());
		}
		else {
			sv.cnstcur.set(srcalcpy.currcode);
			sv.hcnstcur.set(srcalcpy.currcode);
		}
		sv.htype.set(srcalcpy.type);
		sv.fieldType.set(srcalcpy.type);
		sv.estMatValue.set(srcalcpy.estimatedVal);
		sv.hemv.set(srcalcpy.estimatedVal);
		if (isNE(srcalcpy.type,"C")
		&& isNE(srcalcpy.type,"J")) {
			sv.actvalue.set(srcalcpy.actualVal);
			sv.hactval.set(srcalcpy.actualVal);
		}
		else {
			/*ILIFE-6709*/
			if(isEQ(srcalcpy.type,"C")){
				if(isEQ(srcalcpy.ffamt,0) && isNE(srcalcpy.feepc,0)){
					sv.percreqd.set(srcalcpy.feepc);
				}
				else if(isNE(srcalcpy.ffamt,0)){
					sv.actvalue.set(srcalcpy.ffamt);
				}
			}
			/*ILIFE-6709*/
			else{
				sv.percreqd.set(srcalcpy.actualVal);
				sv.actvalue.set(ZERO);
				sv.hactval.set(ZERO);
			}
		}
		/*sv.hcrtable.set(covrclmIO.getCrtable());
		sv.hlife.set(covrclmIO.getLife());
		sv.hjlife.set(covrclmIO.getJlife());*/
		sv.hcrtable.set(covrClm.getCrtable());
		sv.hlife.set(covrClm.getLife());
		sv.hjlife.set(covrClm.getJlife());
		if ((isEQ(srcalcpy.estimatedVal,ZERO)
		&& isEQ(srcalcpy.actualVal,ZERO))
		|| isEQ(srcalcpy.type,"C")
		|| isEQ(srcalcpy.type,"J")) {
			sv.percreqdOut[varcom.pr.toInt()].set("Y");
			sv.cnstcurOut[varcom.pr.toInt()].set("Y");
			sv.actvalueOut[varcom.pr.toInt()].set("Y");
		}
		sv.shortds.set(srcalcpy.description);
		sv.fund.set(srcalcpy.fund);
		if (firstTimeThru.isTrue()) {
			wsaa1stTime.set("N");
			wsaaStoredCurrency.set(sv.cnstcur);
			wsaaEstimateTot.set(srcalcpy.estimatedVal);
		}
		else {
			if (isEQ(srcalcpy.currcode,wsaaStoredCurrency)) {
				wsaaEstimateTot.add(srcalcpy.estimatedVal);
			}
			else {
				if (isNE(srcalcpy.currcode,SPACES)) {
					wsaaEstimateTot.set(ZERO);
					wsaaCurrencySwitch.set(1);
				}
			}
		}
		if (isNE(srcalcpy.type,"C")
		&& isNE(srcalcpy.type,"J")) {
			if (isNE(srcalcpy.currcode,chdrptsIO.getCntcurr())) {
				conlinkrec.currIn.set(srcalcpy.currcode);
				conlinkrec.amountIn.set(srcalcpy.estimatedVal);
				conlinkrec.currOut.set(chdrptsIO.getCntcurr());
				xcvrtCtrctCurr1900();
				wsaaCtrctAmount.add(conlinkrec.amountOut);
			}
			else {
				wsaaCtrctAmount.add(srcalcpy.estimatedVal);
			}
		}
		//ILIFE-7956 -START
		
				if(sv.fieldType.equals("A") || sv.fieldType.equals("D")) { //ILB-1242
					compute(wsaaTotal, 3).setRounded(add(wsaaTotal,sv.actvalue));
					count++;
				
			}
		//ILIFE-7956 -END	
	}

protected void writeToSubfile1615()
	{
		if ((isEQ(srcalcpy.estimatedVal,ZERO)
		&& isEQ(srcalcpy.actualVal,ZERO))
		|| isEQ(srcalcpy.type,"C")
		|| isEQ(srcalcpy.type,"J")) {
			sv.percreqdOut[varcom.pr.toInt()].set("Y");
			sv.cnstcurOut[varcom.pr.toInt()].set("Y");
			sv.actvalueOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(srcalcpy.estimatedVal,ZERO)
		&& isEQ(srcalcpy.actualVal, ZERO)
		&& isNE(srcalcpy.type, "C")) {
			goTo(GotoLabel.att1620);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void att1620()
	{
		/*   Set off all the attribute indicators.*/
		sv.percreqdOut[varcom.pr.toInt()].set(" ");
		sv.cnstcurOut[varcom.pr.toInt()].set(" ");
		sv.percreqdOut[varcom.hi.toInt()].set(" ");
		sv.cnstcurOut[varcom.hi.toInt()].set(" ");
		sv.actvalueOut[varcom.pr.toInt()].set(" ");
		sv.actvalueOut[varcom.hi.toInt()].set(" ");
		sv.percreqd.set(ZERO);
		/*EXIT*/
	}

protected void partPlan1700()
	{
		/*START*/
		/* Begin on the coverage/rider record*/
		chdrptsIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrptsIO);
		if (isNE(chdrptsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
		wsaaStoredCoverage.set(covrsurIO.getCoverage());
		wsaaStoredRider.set(covrsurIO.getRider());
		wsaaStoredCurrency.set(covrsurIO.getPremCurrency());
		wsaaPlanSuffix.set(covrsurIO.getPlanSuffix());
		covrSur=new Covrpf();
		covrSur.setChdrcoy(covrsurIO.getChdrcoy().toString().trim());
		covrSur.setChdrnum(covrsurIO.getChdrnum().toString().trim());
		covrSur.setLife(covrsurIO.getLife().toString().trim());
		covrSur.setCoverage(covrsurIO.getCoverage().toString().trim());
		covrSur.setRider(covrsurIO.getRider().toString().trim());
		covrSurList=covrClmdao.selectCovrSurData(covrSur);
		for(int intCounter=0;intCounter<=covrSurList.size()-1;intCounter++){
			covrSur=covrSurList.get(intCounter);
			processPartComponents1750();
		}
		/*while ( !(isEQ(covrsurIO.getStatuz(),varcom.endp))) {
			processPartComponents1750();
		}*/
		
		/*EXIT*/
	}

protected void processPartComponents1750()
	{
		read1751();
		nextrec1755();
	}
protected void read1751()
{
	wsaaCrtable.set(covrSur.getCrtable());
	obtainSurrenderCalc1400();
	sv.coverage.set(covrSur.getCoverage());
	sv.hcover.set(covrSur.getCoverage());
	if (isEQ(covrSur.getRider(),"00")) {
		sv.rider.set(SPACES);
		sv.crrcd.set(covrSur.getCrrcd());
	}
	else {
		sv.crrcd.set(covrSur.getCrrcd());
		sv.rider.set(covrSur.getRider());
	}
	if (isNE(covrSur.getRider(),"00")
	&& isNE(covrSur.getRider(), "  ")) {
		sv.coverage.set(SPACES);
	}
	sv.cnstcur.set(covrSur.getPremCurrency());
	sv.hcnstcur.set(covrSur.getPremCurrency());
	srcalcpy.currcode.set(covrSur.getPremCurrency());
	srcalcpy.estimatedVal.set(ZERO);
	srcalcpy.effdate.set(ZERO);
	srcalcpy.actualVal.set(ZERO);
	srcalcpy.chdrChdrcoy.set(covrSur.getChdrcoy());
	srcalcpy.chdrChdrnum.set(covrSur.getChdrnum());
	srcalcpy.lifeLife.set(covrSur.getLife());
	srcalcpy.lifeJlife.set(covrSur.getJlife());
	srcalcpy.covrCoverage.set(covrSur.getCoverage());
	srcalcpy.covrRider.set(covrSur.getRider());
	srcalcpy.crtable.set(covrSur.getCrtable());
	srcalcpy.crrcd.set(covrSur.getCrrcd());
	srcalcpy.convUnits.set(covrSur.getConvertInitialUnits());
	srcalcpy.status.set(SPACES);
	srcalcpy.pstatcode.set(covrSur.getPstatcode());
	sv.shortds.set(srcalcpy.description);
	srcalcpy.polsum.set(chdrptsIO.getPolsum());
	srcalcpy.language.set(wsspcomn.language);
	while ( !(isEQ(srcalcpy.status,varcom.endp))) {
		callSurMethodPart1800();
	}
	
}

/*protected void read1751()	{
		wsaaCrtable.set(covrsurIO.getCrtable());
		obtainSurrenderCalc1400();
		sv.coverage.set(covrsurIO.getCoverage());
		sv.hcover.set(covrsurIO.getCoverage());
		if (isEQ(covrsurIO.getRider(),"00")) {
			sv.rider.set(SPACES);
			sv.crrcd.set(covrsurIO.getCrrcd());
		}
		else {
			sv.crrcd.set(covrsurIO.getCrrcd());
			sv.rider.set(covrsurIO.getRider());
		}
		if (isNE(covrsurIO.getRider(),"00")
		&& isNE(covrsurIO.getRider(), "  ")) {
			sv.coverage.set(SPACES);
		}
		sv.cnstcur.set(covrsurIO.getPremCurrency());
		sv.hcnstcur.set(covrsurIO.getPremCurrency());
		srcalcpy.currcode.set(covrsurIO.getPremCurrency());
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.effdate.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrsurIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrsurIO.getChdrnum());
		srcalcpy.lifeLife.set(covrsurIO.getLife());
		srcalcpy.lifeJlife.set(covrsurIO.getJlife());
		srcalcpy.covrCoverage.set(covrsurIO.getCoverage());
		srcalcpy.covrRider.set(covrsurIO.getRider());
		srcalcpy.crtable.set(covrsurIO.getCrtable());
		srcalcpy.crrcd.set(covrsurIO.getCrrcd());
		srcalcpy.convUnits.set(covrsurIO.getConvertInitialUnits());
		srcalcpy.status.set(SPACES);
		srcalcpy.pstatcode.set(covrsurIO.getPstatcode());
		sv.shortds.set(srcalcpy.description);
		srcalcpy.polsum.set(chdrptsIO.getPolsum());
		srcalcpy.language.set(wsspcomn.language);
		while ( !(isEQ(srcalcpy.status,varcom.endp))) {
			callSurMethodPart1800();
		}
		
	}

	
*/
protected void nextrec1755()
	{
		/* Read the next coverage/rider record.*/
		/*covrsurIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(),varcom.oK)
		&& isNE(covrsurIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if (isNE(covrsurIO.getChdrcoy(),chdrptsIO.getChdrcoy())
		|| isNE(covrsurIO.getChdrnum(),chdrptsIO.getChdrnum())
		|| isNE(covrsurIO.getPlanSuffix(),wsaaCovrSuffix)) {
			covrsurIO.setStatuz(varcom.endp);

		}*/
		/*EXIT*/
	}

protected void callSurMethodPart1800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read1810();
					writeToSubfile1815();
				case att1820: 
					att1820();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read1810()
	{
		srcalcpy.effdate.set(sv.effdate);
		srcalcpy.type.set("P");
		/*if (isGT(covrsurIO.getInstprem(),ZERO)) {
			srcalcpy.singp.set(covrsurIO.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrsurIO.getSingp());
		}*/
		if (isGT(covrSur.getInstprem(),ZERO)) {
			srcalcpy.singp.set(covrSur.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrSur.getSingp());
		}
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		if (isEQ(t6598rec.calcprog,SPACES)) {
			sv.htype.set(SPACES);
			sv.fieldType.set(SPACES);
			sv.shortds.set(SPACES);
			sv.fund.set(SPACES);
			sv.cnstcur.set(SPACES);
			sv.hcnstcur.set(SPACES);
			sv.estMatValue.set(0);
			sv.hemv.set(0);
			sv.actvalue.set(0);
			sv.hactval.set(0);
			sv.percreqd.set(0);
			srcalcpy.estimatedVal.set(0);
			srcalcpy.actualVal.set(0);
			srcalcpy.status.set(varcom.endp);
			goTo(GotoLabel.att1820);
		}
		sv.percreqd.set(ZERO);
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		/*ILIFE-7548 start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()) && er.isExternalized(srcalcpy.crtable.toString(),txcalcrec.cnttype.toString())))//ILIFE-7423 
		{
			srcalcpy.cnttype.set(chdrptsIO.getCnttype()); //ILIFE-9394
			callProgramX(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		/*ILIFE-7548 end*/
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrptsIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				srcalcpy.status.set(varcom.endp);
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.endf.set("Y");
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}
		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isEQ(srcalcpy.status,varcom.bomb)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isNE(srcalcpy.status,varcom.oK)
		&& isNE(srcalcpy.status,varcom.endp)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isEQ(srcalcpy.status, varcom.oK)) {
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			a000CallRounding();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			a000CallRounding();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		/* IF SURC-ENDF  = 'Y'                                          */
		/*    GO TO 1840-EXIT                                           */
		/* END-IF.                                                      */
		sv.percreqdOut[varcom.pr.toInt()].set(SPACES);
		sv.cnstcurOut[varcom.pr.toInt()].set(SPACES);
		sv.percreqdOut[varcom.ri.toInt()].set(SPACES);
		sv.cnstcurOut[varcom.ri.toInt()].set(SPACES);
		sv.actvalueOut[varcom.pr.toInt()].set(SPACES);
		sv.actvalueOut[varcom.ri.toInt()].set(SPACES);
		if (isEQ(srcalcpy.psNotAllwd,"T")) {
			wsspcomn.edterror.set("Y");
			scrnparams.errorCode.set(errorsInner.h388);
		}
		sv.estMatValue.set(srcalcpy.estimatedVal);
		sv.hemv.set(srcalcpy.estimatedVal);
		if (isNE(srcalcpy.type,"C")
		&& isNE(srcalcpy.type,"J")) {
			sv.actvalue.set(srcalcpy.actualVal);
			sv.hactval.set(srcalcpy.actualVal);
		}
		else {
			sv.percreqd.set(srcalcpy.actualVal);
			sv.actvalue.set(ZERO);
			sv.hactval.set(ZERO);
		}
		if (summaryPartPlan.isTrue()) {
			if (isNE(srcalcpy.estimatedVal,ZERO)) {
				compute(wsaaEstMatValue, 3).setRounded((div(srcalcpy.estimatedVal,chdrptsIO.getPolsum())));
				zrdecplrec.amountIn.set(wsaaEstMatValue);
				zrdecplrec.currency.set(srcalcpy.currcode);
				a000CallRounding();
				wsaaEstMatValue.set(zrdecplrec.amountOut);
				sv.estMatValue.set(wsaaEstMatValue);
				sv.hemv.set(wsaaEstMatValue);
			}
		}
		if (summaryPartPlan.isTrue()) {
			if (isNE(srcalcpy.actualVal,ZERO)
			&& isNE(srcalcpy.type,"C")
			&& isNE(srcalcpy.type,"J")) {
				compute(wsaaEstMatValue, 3).setRounded((div(srcalcpy.actualVal,chdrptsIO.getPolsum())));
				zrdecplrec.amountIn.set(wsaaEstMatValue);
				zrdecplrec.currency.set(srcalcpy.currcode);
				a000CallRounding();
				wsaaEstMatValue.set(zrdecplrec.amountOut);
				sv.actvalue.set(wsaaEstMatValue);
				sv.hactval.set(wsaaEstMatValue);
			}
		}
		sv.fund.set(srcalcpy.fund);
		sv.shortds.set(srcalcpy.description);
		/*sv.hcrtable.set(covrsurIO.getCrtable());
		sv.hlife.set(covrsurIO.getLife());
		sv.hjlife.set(covrsurIO.getJlife());*/
		sv.hcrtable.set(covrSur.getCrtable());
		sv.hlife.set(covrSur.getLife());
		sv.hjlife.set(covrSur.getJlife());
		if (isEQ(srcalcpy.currcode,SPACES)
		&& isNE(srcalcpy.type,"C")
		&& isNE(srcalcpy.type,"J")) {
			sv.cnstcur.set(chdrptsIO.getCntcurr());
			sv.hcnstcur.set(chdrptsIO.getCntcurr());
		}
		else {
			sv.cnstcur.set(srcalcpy.currcode);
			sv.hcnstcur.set(srcalcpy.currcode);
		}
		sv.htype.set(srcalcpy.type);
		sv.fieldType.set(srcalcpy.type);
		if ((isEQ(srcalcpy.estimatedVal,ZERO)
		&& isEQ(srcalcpy.actualVal,ZERO))
		|| isEQ(srcalcpy.type,"C")
		|| isEQ(srcalcpy.type,"J")) {
			sv.percreqdOut[varcom.pr.toInt()].set("Y");
			sv.cnstcurOut[varcom.pr.toInt()].set("Y");
			sv.actvalueOut[varcom.pr.toInt()].set("Y");
		}
		sv.fund.set(srcalcpy.fund);
		if (firstTimeThru.isTrue()) {
			wsaa1stTime.set("N");
			wsaaStoredCurrency.set(sv.cnstcur);
			wsaaEstimateTot.set(srcalcpy.estimatedVal);
		}
		else {
			if (isEQ(srcalcpy.currcode,wsaaStoredCurrency)) {
				wsaaEstimateTot.add(srcalcpy.estimatedVal);
			}
			else {
				if (isNE(srcalcpy.currcode,SPACES)) {
					wsaaEstimateTot.set(ZERO);
					wsaaCurrencySwitch.set(1);
				}
			}
		}
		if (isNE(srcalcpy.type,"C")
		&& isNE(srcalcpy.type,"J")) {
			if (isNE(srcalcpy.currcode,chdrptsIO.getCntcurr())) {
				conlinkrec.currIn.set(srcalcpy.currcode);
				conlinkrec.amountIn.set(srcalcpy.estimatedVal);
				conlinkrec.currOut.set(chdrptsIO.getCntcurr());
				xcvrtCtrctCurr1900();
				wsaaCtrctAmount.add(conlinkrec.amountOut);
			}
			else {
				wsaaCtrctAmount.add(srcalcpy.estimatedVal);
			}
		}
	}

protected void writeToSubfile1815()
	{
		if ((isEQ(srcalcpy.estimatedVal,ZERO)
		&& isEQ(srcalcpy.actualVal,ZERO))
		|| isEQ(srcalcpy.type,"C")
		|| isEQ(srcalcpy.type,"J")) {
			sv.percreqdOut[varcom.pr.toInt()].set("Y");
			sv.cnstcurOut[varcom.pr.toInt()].set("Y");
			sv.actvalueOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(srcalcpy.estimatedVal,ZERO)
		&& isEQ(srcalcpy.actualVal,ZERO)) {
			goTo(GotoLabel.att1820);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void att1820()
	{
		/*   Set off all the attribute indicators.*/
		sv.percreqdOut[varcom.pr.toInt()].set(" ");
		sv.cnstcurOut[varcom.pr.toInt()].set(" ");
		sv.percreqdOut[varcom.hi.toInt()].set(" ");
		sv.cnstcurOut[varcom.hi.toInt()].set(" ");
		sv.actvalueOut[varcom.pr.toInt()].set(" ");
		sv.actvalueOut[varcom.hi.toInt()].set(" ");
		/*EXIT*/
	}

protected void xcvrtCtrctCurr1900()
	{
		start1900();
	}

	/**
	* <pre>
	****  This is used to convert the fund line values to the         
	****  contract currency.                                          
	* </pre>
	*/
protected void start1900()
	{
		conlinkrec.function.set("CVRT");
		conlinkrec.statuz.set(SPACES);
		/* MOVE VRCM-MAX-DATE          TO CLNK-CASHDATE.        <LA4958>*/
		conlinkrec.cashdate.set(sv.effdate);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrptsIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,"****")) {
			scrnparams.errorCode.set(conlinkrec.statuz);
			wsspcomn.edterror.set("Y");
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(chdrptsIO.getCntcurr());
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void blankSubfile1900x()
	{
		/*X-START*/
		sv.cnstcur.set(SPACES);
		sv.coverage.set(SPACES);
		sv.hcnstcur.set(SPACES);
		sv.hcover.set(SPACES);
		// MIBT-237 Starts
//		sv.hcrtable.set(SPACES);
		// MIBT-237 Ends
		sv.hjlife.set(SPACES);
		sv.hlife.set(SPACES);
		sv.htype.set(SPACES);
		sv.hupdflg.set(SPACES);
		sv.rider.set(SPACES);
		sv.shortds.set(SPACES);
		sv.fieldType.set(SPACES);
		sv.fund.set(SPACES);
		sv.actvalue.set(ZERO);
		sv.estMatValue.set(ZERO);
		sv.hactval.set(ZERO);
		sv.hemv.set(ZERO);
		sv.percreqd.set(ZERO);
		sv.crrcd.set(varcom.vrcmMaxDate);
		scrnparams.function.set(varcom.sadd);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*X-EXIT*/
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clts.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(clts.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clts.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(clts.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clts.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clts.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clts.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clts.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clts.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
			preStart();
		}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsaaEstimateTot,ZERO)) {
			/*        AND WSAA-ESTIMATE-TOT1 = ZEROS                           */
			wsspcomn.edterror.set("Y");
			/***           MOVE 'Y' TO WSSP-EDTERROR      S6307-CURRCD-OUT(PR)  */
			/***                    S6307-EFFDATE-OUT(PR) S6307-PRCNT-OUT(PR)   */
			/***                    S6307-TOTALAMT-OUT(PR)                      */
			/***           MOVE H377 TO SCRN-ERROR-CODE                         */
			/***           MOVE 'Y' TO WSAA-RETURN                              */
		}
		else {
			if (detailsSameCurrency.isTrue()) {
				sv.estimateTotalValue.set(wsaaEstimateTot);
			}
		}
		if (isEQ(srcalcpy.psNotAllwd,"T")
		|| noSurrenderMethod.isTrue()) {
			wsspcomn.edterror.set("Y");
			scrnparams.errorCode.set(errorsInner.h388);
		}
		/*    IF S6307-TOTALFEE           NOT = ZERO                       */
		/*       PERFORM 5100-CHECK-CALC-TAX                               */
		/*  Change the values back as they has been changed in             */
		/*  5100-CHECK-CALC-TAX sction                                     */
		/*       MOVE 'INIT '             TO SCRN-FUNCTION                 */
		/*       MOVE '****'              TO SCRN-STATUZ                   */
		/*    END-IF.                                                      */
		/* Comment below is invalid. Display the total fee in all cases */
		/*WE WILL ONLY DISPLAY THE TOTAL FEE AMOUNT, IF IT IS     <022>*/
		/*THE SURRENDER OF A WHOLE PLAN.                          <022>*/
		/*    IF WHOLE-PLAN                                           <022>*/
		/*       MOVE ' '     TO S6307-TOTALFEE-OUT(ND)               <022>*/
		/*    ELSE                                                    <022>*/
		/*       MOVE 'Y'     TO S6307-TOTALFEE-OUT(ND).              <022>*/
		wsaa1stTime.set("N");
		scrnparams.subfileRrn.set(1);
		if (isEQ(scrnparams.errorCode, errorsInner.h355)) {
			scrnparams.function.set(varcom.prot);
		}
		
	
		
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateScreen2010();
					validateSelectionFields2070();
				case checkForErrors2050: 
					checkForErrors2050();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	
	}

private void Getwithdrawalamount() {
	
	initialize(nlgcalcrec.nlgcalcRec);
	nlgcalcrec.function.set(SPACES);
	nlgcalcrec.tranno.set(chdrptsIO.getTranno());
	nlgcalcrec.chdrcoy.set(chdrptsIO.getChdrcoy());
	nlgcalcrec.chdrnum.set(chdrptsIO.getChdrnum());
	nlgcalcrec.effdate.set(datcon1rec.intDate);
	nlgcalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	nlgcalcrec.billfreq.set(chdrptsIO.getBillfreq());
	nlgcalcrec.cnttype.set(chdrptsIO.getCnttype());
	nlgcalcrec.language.set(wsspcomn.language);
	nlgcalcrec.frmdate.set(chdrptsIO.getOccdate());
	nlgcalcrec.todate.set(varcom.maxdate);
	nlgcalcrec.occdate.set(chdrptsIO.getOccdate());
	nlgcalcrec.ptdate.set(chdrptsIO.getPtdate());
	nlgcalcrec.billfreq.set(chdrptsIO.getBillfreq());
	nlgcalcrec.inputAmt.set(runningTotalSurr);
	compute(nlgcalcrec.inputAmt, 0).set(mult(-1, nlgcalcrec.inputAmt));
	nlgcalcrec.function.set("PSURR");
	callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec);
	if (isNE(nlgcalcrec.status, varcom.oK)) {
		syserrrec.statuz.set(nlgcalcrec.status);
		syserrrec.params.set(nlgcalcrec.nlgcalcRec);
		fatalError600();
	} 
}

protected void screenIo2010()
	{
		/*    CALL 'S6307IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6307-DATA-AREA                         */
		/*                         S6307-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
	//ILIFE-7956 -START
	 	maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
		
		if(maxwithConfig) 
		{
			partSurrenderrec.getFundPer().clear();
			partSurrenderrec.getFundWithdrawAmt().clear();
			partSurrenderrec.getFundEstValue().clear();
		}
	//ILIFE-7956 -END		
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.checkForErrors2050);
		}
		if (isEQ(wsaaReturn,"Y")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaUnproc,"Y")) {
			scrnparams.errorCode.set(errorsInner.h355);
			goTo(GotoLabel.checkForErrors2050);
		}
		sv.currcdOut[varcom.pr.toInt()].set(" ");
		sv.effdateOut[varcom.pr.toInt()].set(" ");
		sv.prcntOut[varcom.pr.toInt()].set(" ");
		sv.totalamtOut[varcom.pr.toInt()].set(" ");
		sv.payeeOut[varcom.pr.toInt()].set(" ");
		wsspcomn.edterror.set(varcom.oK);
		wsaaGotSflEntry.set("N");
		wsaaGotPenEntry.set("N");
		runningTotalSurr.set(ZERO);
		runningTotalEst.set(ZERO);
		/*ILIFE-6709*/
		runningTotalFeeExt.set(ZERO);
		actCumulativeVal.set(ZERO);
		/*ILIFE-6709*/
		feeValue.set(ZERO);
		runningTotalFee.set(ZERO);
		sv.totalfee.set(ZERO);
		srcalcpy.neUnits.set("N");
		srcalcpy.tmUnits.set("N");
		if (isNE(srcalcpy.psNotAllwd,"T")) {
			srcalcpy.psNotAllwd.set(SPACES);
		}
		if (isEQ(scrnparams.errorCode,"H376")) {
			scrnparams.errorCode.set(SPACES);
		}
		
		if(susur002Permission){
		if(isEQ(sv.payee, SPACES)){
			sv.payee.set(chdrptsIO.getCownnum());
		}
		if (isNE(chdrptsIO.getCownnum(), SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrptsIO.getCownpfx());
			stringVariable1.addExpression(chdrptsIO.getCowncoy());
			stringVariable1.addExpression(sv.payee);
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(sv.payee);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
	
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))
		&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.payeeErr.set(errorsInner.e335);
			wsspcomn.edterror.set("Y");
			sv.payeename.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
		a1000GetPayorname();
		sv.payeename.set(namadrsrec.name);
		
		if (isEQ(sv.reqntype, "4")) {
			sv.crdtcrd.set(SPACES);
			sv.bankacckeyOut[varcom.nd.toInt()].set("N");
			sv.crdtcrdOut[varcom.nd.toInt()].set("Y");
			if (isEQ(sv.bankacckey, SPACES)) {
				sv.bankacckeyErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				checkForErrors2050();
			}
			
		}
		
		if (isEQ(sv.reqntype, "C")) {
			sv.bankacckey.set(SPACES);
			sv.crdtcrdOut[varcom.nd.toInt()].set("N");
			sv.bankacckeyOut[varcom.nd.toInt()].set("Y");
			if (isEQ(sv.crdtcrd, SPACES)) {
				sv.crdtcrdErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				checkForErrors2050();
			}
		}
		
		if (isNE(sv.reqntype, '4') && isNE(sv.reqntype, 'C') ) {
			sv.bankacckeyOut[varcom.nd.toInt()].set("Y");
			sv.crdtcrdOut[varcom.nd.toInt()].set("Y");
			sv.bankacckey.set(SPACES);
			sv.crdtcrd.set(SPACES);
			
		}
		
		if (isEQ(sv.reqntype, SPACES) ) {
			sv.reqntypeErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			checkForErrors2050();
			
		}
		sv.bankkey.set(SPACES);

	}
		
		
}


protected void a1000GetPayorname()	{
	/*A1010-NAMADRS*/
	initialize(namadrsrec.namadrsRec);
	namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
	namadrsrec.clntCompany.set(wsspcomn.fsuco);
	namadrsrec.clntNumber.set(sv.payee);
	namadrsrec.language.set(wsspcomn.language);
	namadrsrec.function.set(wsaaLargeName);
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(namadrsrec.statuz);
		fatalError600();
	}
	/*A1090-EXIT*/
}

protected void validateScreen2010()
	{
	if(susur002Permission){
		if (isNE(sv.bankacckey,SPACES) || isNE(sv.crdtcrd,SPACES)) {
			clbapf = new Clbapf();
			clbapf.setClntpfx("CN");
			clbapf.setClntcoy(wsspcomn.fsuco.toString().trim());
			clbapf.setClntnum(sv.payee.toString().trim());
			List<Clbapf> clbalist = clbapfDAO.searchClbapfDatabyObject(clbapf);
			if (clbalist != null && clbalist.size() > 0) {
				for (Clbapf clbaitem : clbalist) {
					sv.bankkey.set(clbaitem.getBankkey());
		          }
             }
          }

        if (isNE(sv.bankacckey,SPACES)) {
	        bankBranchDesc1120();
         }

        if (isNE(sv.crdtcrd,SPACES)) {
	       bankBranchDesc1120();
         }
	 }
		if ((isEQ(scrnparams.statuz,"CALC")
		&& isNE(scrnparams.deviceInd,"*RMT"))
		|| (isGT(sv.totalamt,ZERO)
		&& isLTE(sv.estimateTotalValue,ZERO))) {
			wsaaCalc = "Y";
		}
		else {
			wsaaCalc = " ";
		}
		wsaaGotSflEntry.set("N");
		wsaaGotPenEntry.set("N");
		penaltyOnly = "N";
		runningTotalSurr.set(ZERO);
		runningTotalEst.set(ZERO);
		feeValue.set(ZERO);
		runningTotalFee.set(ZERO);
		/*  MOVE ZEROS TO WSAA-FLAG-1, WSAA-FLAG-2.                 <019>*/
		if (isGT(sv.totalamt,ZERO)) {
			penaltyOnly = "Y";
		}
		else {
			if (isGT(sv.prcnt,ZERO)) {
				penaltyOnly = "Y";
			}
			/*ILIFE-6709*/
			else if(isGT(sv.percreqd, ZERO)){
				penaltyOnly = "Y";
			}
			else if(isGT(sv.actvalue, ZERO)){
				penaltyOnly = "Y";
			}
			/*ILIFE-6709*/
		}
		/* SET INDEX FOR USE WITH T5542.                                   */
		/* IF CHDRPTS-BILLFREQ  = '12'                             <027>*/
		/*    MOVE 5            TO INDEX1                          <027>*/
		/* ELSE                                                    <027>*/
		/* IF CHDRPTS-BILLFREQ  = '04'                             <027>*/
		/*    MOVE 4            TO INDEX1                          <027>*/
		/* ELSE                                                    <027>*/
		/* IF CHDRPTS-BILLFREQ  = '02'                             <027>*/
		/*    MOVE 3            TO INDEX1                          <027>*/
		/* ELSE                                                    <027>*/
		/* IF CHDRPTS-BILLFREQ  = '01'                             <027>*/
		/*    MOVE 2            TO INDEX1                          <027>*/
		/* ELSE                                                    <027>*/
		/*    MOVE 1            TO INDEX1                          <027>*/
		/* END-IF                                                       */
		/* END-IF                                                       */
		/* END-IF                                                       */
		/* END-IF.                                                      */
		/* Effective date being checked before processing on possible      */
		/* subfile entries, as a valid EFFDATE is required.                */
		if (isEQ(sv.effdate,varcom.vrcmMaxDate)
		&& isNE(th506rec.ind,"Y")) {
			sv.effdateErr.set(errorsInner.e186);
			goTo(GotoLabel.checkForErrors2050);
		}
		else {
			if (isLT(sv.effdate,chdrptsIO.getOccdate())) {
				sv.effdateErr.set(errorsInner.f616);
				goTo(GotoLabel.checkForErrors2050);
			}
		}
		if (isEQ(th506rec.ind,"Y")) {
			validateEffdate2700();
			if (isNE(sv.errorIndicators,SPACES)) {
				goTo(GotoLabel.checkForErrors2050);
			}
		}
		if (isEQ(sv.currcd,SPACES)) {
			sv.currcd.set(chdrptsIO.getCntcurr());
		}
		if (isNE(sv.totalamt, ZERO)) {
			zrdecplrec.amountIn.set(sv.totalamt);
			zrdecplrec.currency.set(sv.currcd);
			a000CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.totalamt)) {
				sv.totalamtErr.set(errorsInner.rfik);
				wsspcomn.edterror.set("Y");
			}
		}
		checkLimit2080();
		if (isNE(sv.totalamt,0)
		&& isNE(sv.currcd,wsaaStoredCurrency)
		&& isNE(sv.currcd,wsaaOrigCurr)) {
			wsaaConvertEstTot.set(0);
			calculateConvertEstTot();
		}
		scrnparams.statuz.set(varcom.oK);
		//ILIFE-5140
		mapOfUlnkpf = getUlnkpfRecord(sv);
		//ILIFE-5140
		vFlag=false;//ILIFE-7956
		while ( !(isEQ(scrnparams.statuz,varcom.endp)
		|| isEQ(scrnparams.statuz,"KILL"))) {
			validateSubfile2500();
		}
		
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		/* IF SCRN-STATUZ              = ENDP AND                  <031>*/
		/*    WSSP-EDTERROR            = 'Y'                       <031>*/
		/*    GO TO 2090-EXIT.                                     <031>*/
		/*  IF  WSAA-FLAG-1         = 1                             <019>*/
		/*     AND WSAA-FLAG-2        = 0                           <019>*/
		/*         MOVE H370 TO SCRN-ERROR-CODE.                    <019>*/
		if (isNE(sv.ptdate,sv.btdate)) {
			sv.ptdateErr.set(errorsInner.g008);
			wsspcomn.edterror.set("Y");
		}
		// ILIFE-5459 Start
				if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("Y")){
					if (isEQ(sv.reserveUnitsDate, 99999999) || isEQ(sv.reserveUnitsDate, SPACES) || isEQ(sv.reserveUnitsDate, 00000000)) { 
						sv.rundteErr.set(errorsInner.e186);
						wsspcomn.edterror.set("Y");
						goTo(GotoLabel.exit2090);
					}

					if (isLT(sv.reserveUnitsDate, chdrptsIO.getCcdate())) {
						sv.rundteErr.set(errorsInner.h359);
						wsspcomn.edterror.set("Y");
						goTo(GotoLabel.exit2090);
					}
					if (isGT(sv.reserveUnitsDate, datcon1rec.intDate)) {
						sv.rundteErr.set(errorsInner.rlcc);
						wsspcomn.edterror.set("Y");
						goTo(GotoLabel.exit2090);
					}
				}
				if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("N") 
						&& !sv.reserveUnitsDate.equals(varcom.vrcmMaxDate)){
					sv.rundteErr.set(errorsInner.g099);
					sv.rsuninErr.set(errorsInner.g099);
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.exit2090);
				}
				
				if (isEQ(sv.reserveUnitsInd, SPACES) && isNE(sv.reserveUnitsDate, varcom.vrcmMaxDate)) {
					sv.rsuninErr.set(errorsInner.e186);
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.exit2090);
				}
			// ILIFE-5459 end
				
		/*  The setting of the screen currency code to the default      */
		/*  contract currency code is now carried out prior to the      */
		/*  2500- section. This value is required in there.             */
		/* IF S6307-CURRCD                 = SPACES                     */
		/*     MOVE CHDRPTS-CNTCURR        TO S6307-CURRCD.             */
		if ((isNE(sv.currcd,wsaaStoredCurrency))
		|| (isEQ(wsaaCalc,"Y"))) {
			wsaaStoredCurrency.set(sv.currcd);
			/*MOVE 3 TO WSAA-CURRENCY-SWITCH*/
			wsaaCurrencySwitch.set(0);
			wsspcomn.edterror.set("Y");
			readjustCurrencies2100();
			/*        MOVE WSAA-ESTIMATE-TOT TO S6307-ESTIMATE-TOTAL-VALUE     */
			/*        MOVE WSAA-ACTUAL-TOT   TO S6307-CLAMANT.                 */
			sv.estimateTotalValue.set(runningTotalEst);
			sv.clamant.set(runningTotalSurr);
		}
		calcSurrCharge2800(); 
		/* This was previously commented out but uncommented to test,      */
		/* by mistake hence date change in code.                           */
		/*    IF S6307-ERROR-SUBFILE          NOT = SPACES                 */
		/*    OR S6307-ERROR-INDICATORS       NOT = SPACES                 */
		/*        MOVE 'Y'                    TO WSSP-EDTERROR             */
		/*    IF WSSP-EDTERROR                = 'Y'                        */
		/*        GO TO 2090-EXIT.                                         */
		/*       effective date must be entered - should be not less than*/
		/*       the ccd and not less than date of death.*/
		if (isLTE(sv.estimateTotalValue,ZERO)) {
			sv.clamantErr.set(errorsInner.e762);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*IF S6307-EFFDATE = VRCM-MAX-DATE                             */
		/*   MOVE E186  TO S6307-EFFDATE-ERR                           */
		/*ELSE                                                         */
		/*   IF S6307-EFFDATE < CHDRPTS-OCCDATE                        */
		/*       MOVE F616 TO S6307-EFFDATE-ERR.                  <009>*/
		/*     ELSE                                                      */
		/*IF T5542-WDL-FREQ(1) NOT = ZERO                   <013>*/
		/*        IF T5542-WDL-FREQ(INDEX1) NOT = ZERO                   */
		/*           PERFORM CHECK-DATES-FROM-RCD                        */
		/*   IF DTC3-FREQ-FACTOR < T5542-WDL-FREQ(1)        <013>*/
		/*           IF DTC3-FREQ-FACTOR < T5542-WDL-FREQ(INDEX1)        */
		/*              MOVE 'Y' TO SURC-PS-NOT-ALLWD                    */
		/*              MOVE H376 TO SCRN-ERROR-CODE                     */
		/*              MOVE 'Y' TO WSSP-EDTERROR.                       */
		if (detailRecordEntered.isTrue()) {
			if (isNE(sv.totalamt,ZERO)) {
				sv.totalamtErr.set(errorsInner.h372);
				sv.totalamtOut[varcom.ri.toInt()].set("Y");
			}
			else {
				if (isNE(sv.prcnt,ZERO)) {
					sv.prcntErr.set(errorsInner.h372);
					sv.prcntOut[varcom.ri.toInt()].set("Y");
				}
			}
		}
		//ILIFE-7956 - START
		maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
		
		if(maxwithConfig) 
		{
			if (!detailRecordEntered.isTrue()
					&& !penaltyRecordEntered.isTrue()
					&& isEQ(sv.totalamt,ZERO)
					&& isEQ(sv.prcnt,ZERO) && !vFlag) {
						sv.totalamtErr.set(errorsInner.h378);
						sv.prcntErr.set(errorsInner.h378);
						sv.totalamtOut[varcom.ri.toInt()].set("Y");
						sv.prcntOut[varcom.ri.toInt()].set("Y");
					}
		}
		//ILIFE-7956 - END
		else {
		if (!detailRecordEntered.isTrue()
		&& !penaltyRecordEntered.isTrue()
		&& isEQ(sv.totalamt,ZERO)
		&& isEQ(sv.prcnt,ZERO)) {
			sv.totalamtErr.set(errorsInner.h378);
			sv.prcntErr.set(errorsInner.h378);
			sv.totalamtOut[varcom.ri.toInt()].set("Y");
			sv.prcntOut[varcom.ri.toInt()].set("Y");
		}
		}
		if (isGT(sv.totalamt,ZERO)
		&& isGT(sv.prcnt,ZERO)) {
			sv.totalamtErr.set(errorsInner.h379);
			sv.prcntErr.set(errorsInner.h379);
			sv.totalamtOut[varcom.ri.toInt()].set("Y");
			sv.prcntOut[varcom.ri.toInt()].set("Y");
		}
		if (!detailRecordEntered.isTrue()
		&& isGT(sv.prcnt,100)) {
			sv.prcntErr.set(errorsInner.h370);
			sv.prcntOut[varcom.ri.toInt()].set("Y");
		}
		if (!detailRecordEntered.isTrue()
		&& isGT(sv.totalamt,sv.estimateTotalValue)) {
			sv.totalamtErr.set(errorsInner.h371);
			sv.totalamtOut[varcom.ri.toInt()].set("Y");
		}
		
		/*  The fee amount should not exceed the total surrender        */
		/*  amount.(Amendment History Ref. 14)                          */
		if (isNE(sv.prcnt,ZERO) )
		{
		compute(runningTotalSurr, 2).setRounded((div((mult(runningTotalEst,sv.prcnt)),100)));
		sv.clamant.set(runningTotalSurr);
		}
		else
		{
		sv.clamant.set(runningTotalSurr);
		}
		/* IF  S6307-CLAMANT               < S6307-TOTALFEE     <V5L012>*/
		if ((setPrecision(sv.clamant, 2)
		&& isLT(sv.clamant,(add(sv.totalfee,wsaaTotalSurrCharge))))) {
			sv.clamantErr.set("H396");
		}
		
		isAusPreserv = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUOTR005", appVars, "IT");
		
		if(isAusPreserv)
		{
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(wsaaDoBDate);
			datcon3rec.intDate2.set(datcon1rec.intDate);		 			
			datcon3rec.frequency.set("12");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				datcon3rec.freqFactor.set(0);
			}
			wsaaDuration.set(div(datcon3rec.freqFactor,12));
			
			if (isLT(wsaaDuration,55))
			{
				sv.clamantErr.set("RRF5");
			}
		}
		
		/*  Subtract the fee from the claim amount.                     */
		if (isEQ(sv.clamantErr,SPACES)) {
			compute(sv.clamant, 2).set(sub(sub(sv.clamant,sv.totalfee),wsaaTotalSurrCharge));
		}
		/* COMPUTE S6307-CLAMANT =                                 <009>*/
		/*   (RUNNING-TOTAL-SURR - FEE-USED).                      <009>*/
		sv.estimateTotalValue.set(runningTotalEst);
		/*IF  T5542-WDREM(1) NOT = ZERO                           <014>*/
		/*   IF  T5542-WDREM(1) >                                <014>*/
		/* IF  T5542-WDREM(INDEX1) NOT = ZERO                      <014>*/
		/*     IF  T5542-WDREM(INDEX1) >                           <014>*/
		/*    (RUNNING-TOTAL-EST - (RUNNING-TOTAL-SURR + FEE-USED))<009>*/
		/*         MOVE 'Y' TO SURC-TM-UNITS                       <009>*/
		/*         MOVE H384 TO SCRN-ERROR-CODE                         */
		/*     ELSE                                                     */
		/*         MOVE 'N' TO SURC-TM-UNITS                            */
		/*     END-IF                                                   */
		/* END-IF.                                                      */
		if (isEQ(srcalcpy.tmUnits,"Y")
		&& isEQ(scrnparams.errorCode,SPACES)) {
			scrnparams.errorCode.set("H375");
		}
		/* IF  T5542-WDL-AMOUNT(1) NOT = ZERO                      <014>*/
		/* IF  T5542-WDL-AMOUNT(INDEX1) NOT = ZERO                 <014>*/
		/*     IF  T5542-WDL-AMOUNT(1) > RUNNING-TOTAL-SURR        <014>*/
		/*     IF  T5542-WDL-AMOUNT(INDEX1) > RUNNING-TOTAL-SURR   <014>*/
		/*         MOVE 'Y' TO SURC-NE-UNITS                       <009>*/
		/*     ELSE                                                     */
		/*         MOVE 'N' TO SURC-NE-UNITS                            */
		/*     END-IF                                                   */
		/* END-IF.                                                      */
		//ILIFE-7956 -START
        maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
		
		if(maxwithConfig) 
		{
			partSurrenderrec.setCurr(wsaaT5542Curr.toString());
			partSurrenderrec.setPartsurrMeth(wsaaT5542Meth.toString());
			partSurrenderrec.setEstTotalValue(sv.estimateTotalValue.getbigdata());
			partSurrenderrec.setTotalWithdrawAmt(sv.totalamt.getbigdata());
			partSurrenderrec.setContractFreq(chdrptsIO.getBillfreq().toString());
			partSurrenderrec.setContractType(chdrptsIO.getCnttype().toString());
			partSurrenderrec.setTotalWithdrawPer(sv.prcnt.getbigdata());
			partSurrenderrec.setnFunds(count);   
			partSurrenderrec.setOutWithdrawalAmt(BigDecimal.ZERO);
			partSurrenderrec.setOutWithdrawalPer(BigDecimal.ZERO);
			partSurrenderrec.setOutFundPerAmt(BigDecimal.ZERO);
			partSurrenderrec.setOutFundWithAmt(BigDecimal.ZERO);
	
			
			if(null != partSurrenderrec.getTotalWithdrawAmt() && partSurrenderrec.getTotalWithdrawAmt().compareTo(BigDecimal.ZERO) > 0) {   
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("OUTWITHDRAWALAMT")
						&& er.isExternalized(chdrptsIO.getCnttype().toString(), covrClm.getCrtable()))) //IBPLIFE-1797
					{
						callProgram("OUTWITHDRAWALAMT",partSurrenderrec);
						if(isNE(partSurrenderrec.statuz, Varcom.oK)) {							
								wsspcomn.edterror.set("Y");
								scrnparams.errorCode.set(partSurrenderrec.statuz);
								goTo(GotoLabel.exit2090);
						}
					}
				}
			
			if(null != partSurrenderrec.getTotalWithdrawPer() && partSurrenderrec.getTotalWithdrawPer().compareTo(BigDecimal.ZERO) > 0) {   
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("OUTWITHDRAWALPER")
						&& er.isExternalized(chdrptsIO.getCnttype().toString(), covrClm.getCrtable()))) //IBPLIFE-1797
				{
					callProgram("OUTWITHDRAWALPER",partSurrenderrec);
					if(isNE(partSurrenderrec.statuz, Varcom.oK)) {
						wsspcomn.edterror.set("Y");
						scrnparams.errorCode.set(partSurrenderrec.statuz);
						goTo(GotoLabel.exit2090);
						}
					}
				}
			if(!(partSurrenderrec.getFundWithdrawAmt().isEmpty())) {  
			    for(int ix=0; ix<partSurrenderrec.getFundWithdrawAmt().size(); ix++) {
			        if(!(partSurrenderrec.getFundWithdrawAmt().get(ix).equals("0.00"))&&
			        		(er.isExternalized(chdrptsIO.getCnttype().toString(), covrClm.getCrtable())))//IBPLIFE-1797
					{
						callProgram("OUTFUNDWITHAMT",partSurrenderrec);
						if(isNE(partSurrenderrec.statuz, Varcom.oK)) {
							wsspcomn.edterror.set("Y");
							scrnparams.errorCode.set(partSurrenderrec.statuz);
							goTo(GotoLabel.exit2090);
						}
					}
				}
			}
			
			if(!(partSurrenderrec.getFundPer().isEmpty())) {  
				for(int ix=0; ix<partSurrenderrec.getFundPer().size(); ix++) {
					if(!(partSurrenderrec.getFundPer().get(ix).equals("0.00")))
					{
						if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("OUTFUNDPER")
								&& er.isExternalized(chdrptsIO.getCnttype().toString(), covrClm.getCrtable())))//IBPLIFE-1797
						{
							callProgram("OUTFUNDPER",partSurrenderrec);
							if(isNE(partSurrenderrec.statuz, Varcom.oK)) {
								wsspcomn.edterror.set("Y");
								scrnparams.errorCode.set(partSurrenderrec.statuz);
								goTo(GotoLabel.exit2090);
							}
						}
					}
				}
			}
}
//ILIFE-7956 -END				
		if (isEQ(srcalcpy.neUnits,"Y")) {
			scrnparams.errorCode.set("H374");
		}
		if (isEQ(wsspcomn.edterror, "Y")) {
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.errorSubfile,SPACES)
		|| isNE(sv.errorIndicators,SPACES)
		|| isNE(scrnparams.errorCode,SPACES)
		|| isEQ(wsaaCalc,"Y")) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
	
   }

protected void bankBranchDesc1120()
{
		babrIO.setDataKey(SPACES);
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
		sv.bankkeyErr.set(errorsInner.f906);
		}
		if (isNE(babrIO.getStatuz(),varcom.oK)
				&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		wsaaBankkey.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsaaBankdesc);
}

protected void validateSelectionFields2070()
	{
		/* Differentiate that this is remote                               */
		/*    IF SCRN-STATUZ                  = 'CALC'                     */
		if ((isEQ(scrnparams.statuz,"CALC")
		&& isNE(scrnparams.deviceInd,"*RMT"))
		|| isEQ(wsaaCalc,"Y")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkLimit2080() {
		if (!susur002Permission)
			return;
		uwlvIO.setDataKey(SPACES);
		uwlvIO.setUserid(wsspcomn.userid);
		uwlvIO.setCompany(wsspcomn.company);
		uwlvIO.setFunction(varcom.readr);
		uwlvIO.setFormat(formatsInner.uwlvrec);
		SmartFileCode.execute(appVars, uwlvIO);
		if (isNE(uwlvIO.getStatuz(), varcom.oK) &&
				isNE(uwlvIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(uwlvIO.getParams());
			syserrrec.statuz.set(uwlvIO.getStatuz());
			fatalError600();
		}
		if (isEQ(uwlvIO.getStatuz(), varcom.mrnf)) {
			sv.clamantErr.set(errorsInner.rf05);
			wsspcomn.edterror.set("Y");
			return;
		} else {
			if((null!=uwlvIO.getPoslevel()) && isEQ(uwlvIO.getPoslevel(),SPACES)){
				sv.clamantErr.set(errorsInner.rf05);
				wsspcomn.edterror.set("Y");
				return;
			}
			readTd5h6();
			itempfList = itemDAO.getAllItemitem("IT", chdrptsIO.getChdrcoy().toString(), t6640,
					covrsurIO.getCrtable().toString());

			if (itempfList.size() >= 1) {

				if (isGT(sv.clamant, td5h6rec.fullsurauthlim)) {
					sv.clamantErr.set(errorsInner.rrfn);
					wsspcomn.edterror.set("Y");
					return;
				}
			} else {
				if (isGT(sv.estimateTotalValue, td5h6rec.fullsurauthlim)) {
					sv.estimtotalErr.set(errorsInner.rrfn);
					wsspcomn.edterror.set("Y");
					return;
				}
			}
		}
}

protected void readTd5h6() 
{
itempf = new Itempf();
itempf.setItempfx(smtpfxcpy.item.toString());
itempf.setItemcoy(wsspcomn.company.toString());
itempf.setItemtabl(td5h6);/* IJTI-1523 */
itempf.setItemitem(uwlvIO.getPoslevel().toString());
itempf = itemDAO.getItempfRecord(itempf);
if (itempf== null ) {
	syserrrec.params.set(itempf);
	fatalError600();
}
else {
	td5h6rec.td5h6Rec.set(StringUtil.rawToString(itempf.getGenarea()));
}
}

protected void checkForErrors2050()
	{
		if ((isNE(sv.errorIndicators,SPACES))
		|| (isNE(scrnparams.errorCode,SPACES))) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
		}
	}

protected void calculateConvertEstTot()
	{
		/*CALCULATE-CONVERT-START*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			calculateReadSubfile();
		}
		
		/*CALCULATE-EXIT*/
	}

protected void calculateReadSubfile()
	{
		/*CALCULATE-READ-START*/
		if (isNE(sv.fieldType,"C")
		&& isNE(sv.fieldType,"J")) {
			if (isNE(sv.hcnstcur,sv.currcd)) {
				calculateConvert();
				wsaaConvertEstTot.add(conlinkrec.amountOut);
			}
			else {
				wsaaConvertEstTot.add(sv.hemv);
			}
		}
		scrnparams.function.set(varcom.srdn);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*CALCULATE-READ-EXIT*/
	}

protected void calculateConvert()
	{
		calculateConvertStart1();
	}

protected void calculateConvertStart1()
	{
		conlinkrec.function.set("CVRT");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(sv.hcnstcur);
		/* MOVE VRCM-MAX-DATE          TO CLNK-CASHDATE.        <LA4958>*/
		conlinkrec.cashdate.set(sv.effdate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(sv.hemv);
		conlinkrec.company.set(chdrptsIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			scrnparams.errorCode.set(conlinkrec.statuz);
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkDatesFromRcd()
	{
		/*STRT*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.function.set("CMDF");
		/* MOVE     'M'              TO DTC3-FREQUENCY.            <009>*/
		datcon3rec.frequency.set("12");
		/* MOVE     S6307-OCCDATE    TO DTC3-INT-DATE-1.           <013>*/
		datcon3rec.intDate1.set(sv.crrcd);
		datcon3rec.intDate2.set(sv.effdate);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		/*RCD-EXIT*/
	}

protected void readjustCurrencies2100()
	{
			g02150();
		}

protected void g02150()
	{
		wsaaEstimateTot.set(ZERO);
		runningTotalSurr.set(ZERO);
		runningTotalEst.set(ZERO);
		runningTotalFee.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			readSubfile2200();
		}
		
		if (isNE(scrnparams.errorCode,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if ((isNE(sv.prcnt,ZERO))
		|| (isNE(sv.totalamt,ZERO))) {
			convertTotalPercent();
			return ;
		}
	}

protected void readSubfile2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go2250();
				case updateErrorIndicators2270: 
					updateErrorIndicators2270();
					readNextRecord2280();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go2250()
	{
		if (isNE(sv.currcd,sv.hcnstcur)
		&& isNE(sv.fieldType,"C")
		&& isNE(sv.fieldType,"J")
		&& isEQ(sv.totalamt,ZERO)
		&& isEQ(sv.prcnt,ZERO)
		&& isEQ(sv.percreqd,ZERO)) {
			runningTotalSurr.add(sv.actvalue);
		}
		if (isEQ(sv.currcd,sv.hcnstcur)
		&& isNE(sv.fieldType,"C")
		&& isNE(sv.fieldType,"J")) {
			sv.cnstcur.set(sv.currcd);
			sv.estMatValue.set(sv.hemv);
			wsaaEstimateTot.add(sv.estMatValue);
			runningTotalEst.add(sv.estMatValue);
			if (isNE(sv.percreqd,ZERO)) {
				compute(sv.actvalue, 3).setRounded((div((mult(sv.estMatValue,sv.percreqd)),100)));
				zrdecplrec.amountIn.set(sv.actvalue);
				zrdecplrec.currency.set(sv.currcd);
				a000CallRounding();
				sv.actvalue.set(zrdecplrec.amountOut);
				runningTotalSurr.add(sv.actvalue);
				runningTotalFee.add(sv.actvalue);
				goTo(GotoLabel.updateErrorIndicators2270);
			}
			else {
				if (isNE(sv.actvalue, ZERO)) {
					zrdecplrec.amountIn.set(sv.actvalue);
					zrdecplrec.currency.set(sv.currcd);
					a000CallRounding();
					if (isNE(zrdecplrec.amountOut, sv.actvalue)) {
						sv.actvalueErr.set(errorsInner.rfik);
						wsspcomn.edterror.set("Y");
					}
				}
				runningTotalSurr.add(sv.actvalue);
				runningTotalFee.add(sv.actvalue);
				goTo(GotoLabel.updateErrorIndicators2270);
			}
		}
		if (isNE(sv.hemv,ZERO)
		&& isNE(sv.fieldType,"C")
		&& isNE(sv.fieldType,"J")) {
			conlinkrec.amountIn.set(sv.hemv);
			callXcvrt2300();
			sv.estMatValue.set(conlinkrec.amountOut);
			wsaaEstimateTot.add(conlinkrec.amountOut);
			runningTotalEst.add(conlinkrec.amountOut);
			if (isNE(sv.percreqd,ZERO)) {
				compute(sv.actvalue, 3).setRounded((div((mult(sv.estMatValue,sv.percreqd)),100)));
				zrdecplrec.amountIn.set(sv.actvalue);
				zrdecplrec.currency.set(sv.currcd);
				a000CallRounding();
				sv.actvalue.set(zrdecplrec.amountOut);
				runningTotalSurr.add(sv.actvalue);
			}
		}
		runningTotalFee.add(sv.actvalue);
		/*convert the actual value if it is non zero.*/
		/*    No currency conversion for actual value*/
		/*    ADD S6307-ACTVALUE TO WSAA-ACTUAL-TOT.                       */
		/*    IF S6307-HACTVAL            NOT = ZERO                       */
		/*         MOVE S6307-HACTVAL     TO CLNK-AMOUNT-IN                */
		/*         PERFORM 2300-CALL-XCVRT                                 */
		/*         MOVE CLNK-AMOUNT-OUT   TO S6307-ACTVALUE                */
		/*         ADD  CLNK-AMOUNT-OUT   TO WSAA-ACTUAL-TOT.              */
		/*    IF S6307-HACTVAL            = ZERO                           */
		/*    AND S6307-ACTVALUE          NOT = ZERO                       */
		/*         MOVE S6307-ACTVALUE    TO CLNK-AMOUNT-IN                */
		/*         PERFORM 2300-CALL-XCVRT                                 */
		/*         MOVE CLNK-AMOUNT-OUT   TO S6307-ACTVALUE                */
		/*         ADD  CLNK-AMOUNT-OUT   TO WSAA-ACTUAL-TOT.              */
		if (isNE(sv.fieldType,SPACES)) {
			sv.cnstcur.set(sv.currcd);
		}
	}

protected void updateErrorIndicators2270()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		sv.percreqdOut[varcom.pr.toInt()].set(" ");
		sv.actvalueOut[varcom.pr.toInt()].set(" ");
		/*  Ref No. 9/T007-01 in amendment history.                     */
		if (isEQ(sv.hemv,ZERO)) {
			sv.percreqdOut[varcom.pr.toInt()].set("Y");
			sv.actvalueOut[varcom.pr.toInt()].set("Y");
		}
		if ((isNE(sv.fieldType,"A"))
		&& (isNE(sv.fieldType,"I"))) {
			sv.percreqdOut[varcom.pr.toInt()].set("Y");
			sv.actvalueOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextRecord2280()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void convertTotalPercent()
	{
		/*CONVERSION-PARA*/
		//ILIFE-5140 
		if (isNE(sv.totalamt,ZERO) && isEQ(runningTotalFeeExt,ZERO) ) {
			compute(runningTotalSurr, 2).set((add(runningTotalSurr,sv.totalamt)));
			compute(runningTotalFee, 3).set((add(runningTotalFee,sv.totalamt)));
			return ;
		}
		if (isNE(sv.prcnt,ZERO) && isEQ(runningTotalFeeExt,ZERO)) {
			compute(runningTotalSurr, 2).set((add(runningTotalSurr,(div((mult(runningTotalEst,sv.prcnt)),100)))));
			compute(runningTotalFee, 3).set((add(runningTotalFee,(div((mult(runningTotalEst,sv.prcnt)),100)))));
		}//ILIFE-5140
	}

protected void callXcvrt2300()
	{
		g02350();
	}

protected void g02350()
	{
		conlinkrec.function.set("CVRT");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(sv.hcnstcur);
		/* MOVE VRCM-MAX-DATE          TO CLNK-CASHDATE.                */
		conlinkrec.cashdate.set(sv.effdate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrptsIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,"****")) {
			scrnparams.errorCode.set(conlinkrec.statuz);
			wsspcomn.edterror.set("Y");
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(sv.currcd);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void validateSubfile2500()
	{
		/*BEGN*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.fieldType,"C")) {
			wsaaFee.set(sv.estMatValue);
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			readSubfile2600();
			addFund++;//ILIFE-7956
		}
		addFund=1; //ILIFE-7956
		
		if (isNE(scrnparams.errorCode,SPACES)
		|| isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void readSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2610();
				case penaltyCheck: 
					penaltyCheck();
				case updSfl2600: 
					updSfl2600();
					readNext2600();
				case exit2690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2610()
	{
		if (isEQ(penaltyOnly,"Y")
		&& isEQ(sv.fieldType,"C")) {
			if (isGT(sv.totalamt,ZERO)) {
				if (wholePlan.isTrue()) {
					runningTotalSurr.set(ZERO);
				}
				/*       COMPUTE RUNNING-TOTAL-SURR = (RUNNING-TOTAL-SURR +<009>*/
				/*                                     S6307-TOTALAMT)     <009>*/
				/*  In the case where a total figure has been entered for the   */
				/*  screen then the running total will come out as the same     */
				/*  figure input. Simply move it in each time.                  */
				/*  Strictly speaking, this accumulation need only be done      */
				/*  once in the process and not for each record. However this   */
				/*  fix does work.                                              */
				runningTotalSurr.set(sv.totalamt);
				/*       COMPUTE RUNNING-TOTAL-FEE = (RUNNING-TOTAL-FEE +  <022>*/
				/*                                     S6307-TOTALAMT)     <022>*/
				/*  The value for the fee in this case must be calculated as a  */
				/*  pro-rata amount based on what proportion of the amount      */
				/*  being surrendered is represented by this component.         */
				/*  If the currency has changed, get a new total figure.        */
				/*  Either the original or a converted figure from the pre-read */
				/*  of the subfile. If there has been no change then use the    */
				/*  current screen total.                                       */
				if (isNE(sv.currcd,wsaaStoredCurrency)) {
					if (isNE(sv.currcd,wsaaOrigCurr)) {
						wsaaEstMatValue.set(wsaaConvertEstTot);
					}
					else {
						wsaaEstMatValue.set(wsaaOrigTotEst);
					}
				}
				else {
					wsaaEstMatValue.set(wsaaEstimateTot);
				}
				/*ILIFE-6709 changed runningTotalFee to runningTotalEst since RTF would be zero*/
				compute(runningTotalFee, 4).setRounded(mult(runningTotalEst,(div(sv.totalamt,wsaaEstMatValue))));
				/*       IF WSAA-CVRT-FLAG2        = 'Y'                   <035>*/
				/*          MOVE WSAA-TOT          TO CLNK-AMOUNT-IN       <035>*/
				/*          PERFORM 2300-CALL-XCVRT                        <035>*/
				/*          MOVE CLNK-AMOUNT-OUT   TO WSAA-TOT             <035>*/
				/*          MOVE SPACES            TO WSAA-CVRT-FLAG1      <035>*/
				/*                                    WSAA-CVRT-FLAG2      <035>*/
				/*       END-IF                                            <035>*/
				/*       COMPUTE WSAA-FEE          = (S6307-TOTALAMT / 100) *<36*/
				/*                                   (WSAA-FEE /           <036>*/
				/*                                   (WSAA-TOT / 100))     <036>*/
				/*       MOVE 'Y' TO WSAA-TOT-FLAG                         <036>*/
				wsaaGotPenEntry.set("Y");
				goTo(GotoLabel.penaltyCheck);
			}
			else {
				if (isGT(sv.prcnt,ZERO)) {
					if (wholePlan.isTrue()) {
						runningTotalSurr.set(ZERO);
					}
					/*  In the case where a total fee has been entered, the running */
					/*  surrender value cannot be calculated from the running total */
					/*  estimate. This in itelf is an accumulation. Therefore       */
					/*  calculate it directly from the figure. Strictly speaking    */
					/*  this accumulation need only actually be done once in the    */
					/*  the process and not for each fee record. However this fix   */
					/*  does work.                                                  */
					compute(runningTotalSurr, 2).setRounded((div((mult(runningTotalEst,sv.prcnt)),100)));
					/*       (RUNNING-TOTAL-SURR + (RUNNING-TOTAL-EST *        <009>*/
					/*                              S6307-PRCNT) / 100)        <009>*/
					/*       COMPUTE RUNNING-TOTAL-FEE =                       <022>*/
					/*       (RUNNING-TOTAL-FEE + (RUNNING-TOTAL-EST *         <022>*/
					/*                              S6307-PRCNT) / 100)        <022>*/
					/*  RUNNING-TOTAL-FEE to be used after all.                     */
					
					/*ILIFE-6709 changed runningTotalFee to runningTotalEst since RTF would be zero and as per comments mentioned above for RTF*/
					compute(runningTotalFee, 3).set(div((mult(runningTotalEst,sv.prcnt)),100));
					/*       IF S6307-CURRCD       NOT = SPACES                <035>*/
					/*       IF S6307-CURRCD       NOT = WSAA-STORED-CURRENCY  <039>*/
					/*             MOVE WSAA-FEE       TO CLNK-AMOUNT-IN       <035>*/
					/*             PERFORM 2300-CALL-XCVRT                     <035>*/
					/*             MOVE CLNK-AMOUNT-OUT TO WSAA-FEE            <035>*/
					/*       END-IF                                            <035>*/
					compute(wsaaFee, 2).set(div((mult(wsaaFee,sv.prcnt)),100));
					/*       ADD WSAA-FEE              TO RUNNING-TOTAL-FEE    <035>*/
					wsaaGotPenEntry.set("Y");
					goTo(GotoLabel.penaltyCheck);
				}
				/*ILIFE-6709*/
				else if (isGT(sv.percreqd,ZERO)) {
					if (wholePlan.isTrue()) {
						runningTotalSurr.set(ZERO);
					}
					if (isNE(sv.currcd,wsaaStoredCurrency)) {
						if (isNE(sv.currcd,wsaaOrigCurr)) {
							wsaaEstMatValue.set(wsaaConvertEstTot);
						}
						else {
							wsaaEstMatValue.set(wsaaOrigTotEst);
						}
					}
					else {
						wsaaEstMatValue.set(wsaaEstimateTot);
					}
					runningTotalSurr.add(actCumulativeVal);
					compute(runningTotalFee, 4).setRounded(mult(runningTotalEst,(div(actCumulativeVal,wsaaEstMatValue))));
					wsaaGotPenEntry.set("Y");
					goTo(GotoLabel.penaltyCheck);
				}
				else if(isNE(sv.actvalue, ZERO)){
					runningTotalFee.set(t5542rec.ffamt[index1.toInt()]);					
				}
				/*ILIFE-6709*/
			}
		}
		/* Ref in amendment history, 6/T003-01, 7/T003-02.              */
		/* Accumulate the estimate amount for this component.           */
		/* If there has been a currency change then this value will     */
		/* need to be converted before adding in.                       */
		if (isNE(sv.fieldType,"C")
		&& isNE(sv.fieldType,"J")) {
			wsaaEstMatValue.set(sv.estMatValue);
			if (isGT(sv.hemv,0)
			&& isNE(sv.currcd,wsaaStoredCurrency)) {
				if (isEQ(sv.hcnstcur,sv.currcd)) {
					wsaaEstMatValue.set(sv.hemv);
				}
				else {
					conlinkrec.amountIn.set(sv.hemv);
					callXcvrt2300();
					wsaaEstMatValue.set(conlinkrec.amountOut);
				}
			}
			wsaaComponentEst.add(wsaaEstMatValue);
		}
		/* <039> Only set flag to indicate Penalty record found if      */
		/*       the component to which it applies has had a            */
		/*       surrender requested ie. the total amount is entered,   */
		/*       the total percentage has been entered or the detail    */
		/*       line has been filled in (either value or %) for one    */
		/*       of the funds oon this component.                       */
		maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
		if(maxwithConfig) {
			if (isGT(sv.percreqd,0)
					|| isGT(sv.actvalue,0)) {
						//ILIFE-9445
						if (isNE(sv.fieldType,"C")
						&& isNE(sv.fieldType,"J")) {
							if(isEQ(sv.totalamt,ZERO) && isEQ(sv.prcnt,ZERO)) {
								wsaaGotSflEntry.set("Y");
							}
							else {
								sv.percreqd.set(ZERO);
								sv.actvalue.set(ZERO);
								runningTotalEst.add(sv.estMatValue);
								sv.hupdflg.set(" ");
							}
						}//end
						else if (isNE(sv.fieldType,"C")
						&& isNE(sv.fieldType,"J") && isNE(sv.totalamt,ZERO) && isNE(sv.actvalue,ZERO)) {
							wsaaGotSflEntry.set("Y");
						}
						else if(isNE(sv.fieldType,"C")
								&& isNE(sv.fieldType,"J") && isNE(sv.totalamt,ZERO) && isNE(sv.percreqd,ZERO))
						{
							wsaaGotSflEntry.set("Y");
						}
						else if(isNE(sv.fieldType,"C")
								&& isNE(sv.fieldType,"J") && isNE(sv.prcnt,ZERO) && isNE(sv.percreqd,ZERO))
						{
							wsaaGotSflEntry.set("Y");
						}
						else if(isNE(sv.fieldType,"C")
								&& isNE(sv.fieldType,"J") && isNE(sv.prcnt,ZERO) && isNE(sv.actvalue,ZERO))
						{
							wsaaGotSflEntry.set("Y");
						}
						else {
							if (isNE(sv.totalamt,0)
							|| isNE(sv.prcnt,0)
							|| isGT(runningTotalFee,0)) {
								wsaaGotPenEntry.set("Y");
							}
						}
						vFlag=true;//ILIFE-7956
					}
					else {
						runningTotalEst.add(sv.estMatValue);
						sv.hupdflg.set(" ");
					}
		}else {
		if (isGT(sv.percreqd,0)
		|| isGT(sv.actvalue,0)) {
			if (isNE(sv.fieldType,"C")
					&& isNE(sv.fieldType,"J")) {
						if(isEQ(sv.totalamt,ZERO) && isEQ(sv.prcnt,ZERO)) {
							wsaaGotSflEntry.set("Y");
						}
						else {
							sv.percreqd.set(ZERO);
							sv.actvalue.set(ZERO);
							runningTotalEst.add(sv.estMatValue);
							sv.hupdflg.set(" ");
						}
			}
			else {
				if (isNE(sv.totalamt,0)
				|| isNE(sv.prcnt,0)
				|| isGT(runningTotalFee,0)) {
					wsaaGotPenEntry.set("Y");
				}
			}
		}
		else {
			runningTotalEst.add(sv.estMatValue);
			sv.hupdflg.set(" ");
		}
		}
		if (isLTE(sv.percreqd,0)
		&& isLTE(sv.actvalue,0)) {
			getSurrenderMeth();
			readT5542();
			if (t5542NotOk.isTrue()) {
				scrnparams.statuz.set(varcom.endp);
				goTo(GotoLabel.exit2690);
			}
			if (isEQ(sv.fieldType,"C")
			|| isEQ(sv.fieldType,"J")) {
				goTo(GotoLabel.penaltyCheck);
			}
			else {
				if (isGT(sv.totalamt,0)
				|| isGT(sv.prcnt,0) || isGT(sv.percreqd, 0)) { /*ILIFE-6709*/
					wsaaEstMatValue.set(sv.estMatValue);
					if (isGT(sv.hemv,0)
					&& isNE(sv.currcd,wsaaStoredCurrency)) {
						if (isEQ(sv.hcnstcur,sv.currcd)) {
							wsaaEstMatValue.set(sv.hemv);
						}
						else {
							conlinkrec.amountIn.set(sv.hemv);
							callXcvrt2300();
							wsaaEstMatValue.set(conlinkrec.amountOut);
						}
					}
				//ILIFE-5140
				
						getCalculatationActualValue(mapOfUlnkpf, sv);
				}
				if(maxwithConfig) {
					if(isEQ(runningTotalFeeExt,ZERO) || addFund <=count) {
						partSurrenderrec.getFundPer().add(sv.percreqd.getbigdata().toString());
						partSurrenderrec.getFundWithdrawAmt().add(sv.actvalue.getbigdata().toString());
						partSurrenderrec.getFundEstValue().add(sv.estMatValue.getbigdata().toString());
					}
					
				}
				if(isEQ(runningTotalFeeExt,ZERO))
					goTo(GotoLabel.updSfl2600);
				else
					goTo(GotoLabel.penaltyCheck);
				//ILIFE-5140			
			}
		}
		if (isGT(sv.actvalue,sv.estMatValue)
		&& isNE(sv.fieldType,"C")
		&& isNE(sv.fieldType,"J")) {
			/*    MOVE H371 TO S6307-ACTVALUE-ERR                    <020> */
			scrnparams.errorCode.set(errorsInner.h371);
		}
		/*    IF  S6307-PERCREQD     > ZEROS      AND                      */
		/*        S6307-ACTVALUE     > ZEROS                               */
		/*        MOVE H373 TO S6307-ACTVALUE-ERR.                         */
		if (isNE(sv.percreqd,ZERO)) {
			if (isNE(sv.fieldType,"C")
			&& isNE(sv.fieldType,"J")) {
				compute(sv.actvalue, 3).setRounded((div((mult(sv.estMatValue,sv.percreqd)),100)));
				compute(sv.hactval, 3).setRounded((div((mult(sv.hemv,sv.percreqd)),100)));
				zrdecplrec.amountIn.set(sv.actvalue);
				zrdecplrec.currency.set(sv.currcd);
				a000CallRounding();
				sv.actvalue.set(zrdecplrec.amountOut);
				zrdecplrec.amountIn.set(sv.hactval);
				zrdecplrec.currency.set(sv.currcd);
				a000CallRounding();
				sv.hactval.set(zrdecplrec.amountOut);
				/* <039> The calculation to get the ACTVALUE here is now as     */
				/*       with the 'second' loop (2200- section) ie. the ACTVAL  */
				/*       is derived from the converted Estimated Value. This is */
				/*       then used to calculate the FEE. By doing this we       */
				/*       ensure that any potential differences due to rounding  */
				/*       are avoided and therefore that the FEE displayed on    */
				/*       the screen is the correct factor from the ACTVALUEs    */
				/*       displayed on the screen.                               */
				/*       IF S6307-CURRCD            NOT = SPACES           <035>*/
				if (isNE(sv.currcd,wsaaStoredCurrency)) {
					if (isEQ(sv.currcd,sv.hcnstcur)) {
						wsaaEstMatValue.set(sv.hemv);
					}
					else {
						conlinkrec.amountIn.set(sv.hemv);
						callXcvrt2300();
						wsaaEstMatValue.set(conlinkrec.amountOut);
					}
					compute(sv.actvalue, 3).setRounded((div((mult(wsaaEstMatValue,sv.percreqd)),100)));
					zrdecplrec.amountIn.set(sv.actvalue);
					zrdecplrec.currency.set(sv.currcd);
					a000CallRounding();
					sv.actvalue.set(zrdecplrec.amountOut);
					/*****          MOVE S6307-ACTVALUE         TO CLNK-AMOUNT-IN  <035>*/
					/*****          PERFORM 2300-CALL-XCVRT                        <035>*/
					/*****          MOVE CLNK-AMOUNT-OUT        TO S6307-ACTVALUE  <035>*/
				}
				/*          MOVE S6307-ACTVALUE TO S6307-HACTVAL                   */
				runningTotalSurr.add(sv.actvalue);
				runningTotalFee.add(sv.actvalue);
				runningTotalEst.add(sv.estMatValue);
				actCumulativeVal.add(sv.actvalue);	/*ILIFE-6709*/
				//ILIFE-7956 -START
				maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
				
				if(maxwithConfig) 
				{
					partSurrenderrec.getFundPer().add(sv.percreqd.getbigdata().toString());
					partSurrenderrec.getFundWithdrawAmt().add(sv.actvalue.getbigdata().toString());
					partSurrenderrec.getFundEstValue().add(sv.estMatValue.getbigdata().toString());
				}
				//ILIFE-7956 -END
			}
		}
		else {
			if (isNE(sv.actvalue,ZERO)) {
				if (isNE(sv.fieldType,"C")
				&& isNE(sv.fieldType,"J")) {
					if (isNE(sv.currcd, wsaaStoredCurrency)) {
						if (isEQ(sv.currcd, sv.hcnstcur)) {
							zrdecplrec.amountIn.set(sv.actvalue);
							zrdecplrec.currency.set(sv.currcd);
							a000CallRounding();
							if (isNE(zrdecplrec.amountOut, sv.actvalue)) {
								sv.actvalueErr.set(errorsInner.rfik);
								wsspcomn.edterror.set("Y");
							}
						}
						else {
							conlinkrec.amountIn.set(sv.actvalue);
							callXcvrt2300();
							sv.actvalue.set(conlinkrec.amountOut);
						}
					}
					else {
						zrdecplrec.amountIn.set(sv.actvalue);
						zrdecplrec.currency.set(sv.currcd);
						a000CallRounding();
						if (isNE(zrdecplrec.amountOut, sv.actvalue)) {
							sv.actvalueErr.set(errorsInner.rfik);
							wsspcomn.edterror.set("Y");
						}
					}
					/*ILIFE-6709*/
					/*runningTotalSurr.add(sv.actvalue);*/
					/*runningTotalFee.add(sv.actvalue);*/
					runningTotalEst.add(sv.estMatValue);
					getCalculatationActualValue(mapOfUlnkpf, sv);
					/*ILIFE-6709*/
					//ILIFE-7956 -START
					maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
					
					if(maxwithConfig) 
					{
						partSurrenderrec.getFundPer().add(sv.percreqd.getbigdata().toString());
						partSurrenderrec.getFundWithdrawAmt().add(sv.actvalue.getbigdata().toString());
						partSurrenderrec.getFundEstValue().add(sv.estMatValue.getbigdata().toString());
					}
					//ILIFE-7956 -END
				}
			}
			else {
				runningTotalEst.add(sv.estMatValue);
			}
		}
	}

protected void penaltyCheck()
	{
		if (isEQ(sv.fieldType,"C")) {
			srcalcpy.neUnits.set("N");
			srcalcpy.tmUnits.set("N");
			validatePenalty();
			if (t5542NotOk.isTrue()) {
				scrnparams.statuz.set(varcom.endp);
				goTo(GotoLabel.exit2690);
			}
			/******      SUBTRACT S6307-ACTVALUE FROM RUNNING-TOTAL-SURR   <018>*/
		}
		else {
			getSurrenderMeth();
			if (isNE(sv.currcd,SPACES)) {
				wsaaT5542Curr.set(sv.currcd);
			}
			else {
				wsaaT5542Curr.set(srcalcpy.chdrCurr);
			}
		}
		if (isNE(sv.fieldType,"C")
		&& isNE(sv.fieldType,"J")) {
			readT5542();
			if (t5542NotOk.isTrue()) {
				scrnparams.statuz.set(varcom.endp);
				goTo(GotoLabel.exit2690);
			}
		}
		/*    Validate the effective-date against Risk Commencement date   */
		/*    of the Cover.                                                */
		/*    IF  S6307-PERCREQD > 0     OR                           <009>*/
		/*        S6307-ACTVALUE > 0                                  <009>*/
		/* Ref 4/T004-01 in amendment history.                          */
		/* IF ( ( S6307-PERCREQD > 0    OR                         <025>*/
		/*        S6307-ACTVALUE > 0 )  AND                        <025>*/
		/*    IF  S6307-FIELD-TYPE          = 'C'                  <LA4772>*/
		if (isNE(sv.fieldType,"C")) {
			if ((isGT(runningTotalFee,0)   
			&& (isNE(srcalcpy.psNotAllwd,"T")))) {
				if (isNE(t5542rec.wdlFreq[index1.toInt()],ZERO)
				&& isEQ(sv.rider,"00")
				|| isEQ(sv.rider, "  ")) {
					checkDatesFromRcd();
					/*       IF DTC3-FREQ-FACTOR < T5542-WDL-FREQ(1)          <013>*/
					if (isLT(datcon3rec.freqFactor,t5542rec.wdlFreq[index1.toInt()])) {
						srcalcpy.psNotAllwd.set("Y");
						sv.effdateErr.set(errorsInner.h376);
						sv.effdateOut[varcom.ri.toInt()].set("Y");
						/*********       MOVE H376 TO SCRN-ERROR-CODE                  <013>*/
						/*********       MOVE 'Y' TO WSSP-EDTERROR                     <020>*/
					}
				}
				/* Ref in amendment history, 6/T003-01, 7/T003-02.              */
				/* Following checks carried out now, per component.             */
				/* After this, zeroise the accumulators as this is the          */
				/* effective end of processing for this component.              */
				
				if (isNE(t5542rec.wdrem[index1.toInt()],ZERO)) {
					//ILIFE-7956 - START
					maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
					
					if(maxwithConfig) {
						compute(wsaaTemp,2).set(sub(sv.estimateTotalValue,sv.clamant));        //IBPLIFE-2063
						if (isNE(t5542rec.wdrem[index1.toInt()],ZERO)) {
							if ((setPrecision(t5542rec.wdrem[index1.toInt()], 3)
									&& isGT(t5542rec.wdrem[index1.toInt()],wsaaTemp)))
							{	
								srcalcpy.tmUnits.set("Y");
								scrnparams.errorCode.set(errorsInner.h384);
							}
							else
							{
								srcalcpy.tmUnits.set("N");
							}
						}
					}	
					//ILIFE-7956 - END					
					else {
						if ((setPrecision(t5542rec.wdrem[index1.toInt()], 3)
								&& isGT(t5542rec.wdrem[index1.toInt()],(sub(wsaaComponentEst,runningTotalFee))))) {
							srcalcpy.tmUnits.set("Y");
							scrnparams.errorCode.set(errorsInner.h384);
						}
						else {
							srcalcpy.tmUnits.set("N");
							}
						}
					}
				
				if (isNE(t5542rec.wdlAmount[index1.toInt()],ZERO)) {
					if (isGT(t5542rec.wdlAmount[index1.toInt()],runningTotalFee)) {
						srcalcpy.neUnits.set("Y");
						scrnparams.errorCode.set(errorsInner.h374);
					}
					else {
						srcalcpy.neUnits.set("N");
					}
				}
			}
			runningTotalFee.set(ZERO);
			wsaaComponentEst.set(ZERO);
		}
		/*    IF SURC-NE-UNITS = 'Y'                                       */
		/*       MOVE 'H374' TO  SCRN-ERROR-CODE.                          */
		/*    IF SURC-TM-UNITS = 'Y'                                       */
		/*       MOVE 'H375' TO  SCRN-ERROR-CODE.                          */
		/* Ref 4/T004-01 in amendment history.                          */
		/* IF SURC-PS-NOT-ALLWD = 'Y'                              <009>*/
		/*    MOVE 'Y'    TO  S6307-EFFDATE-OUT(RI)                <019>*/
		/*    MOVE H376   TO  S6307-EFFDATE-ERR                    <020>*/
		/*  MOVE H376  TO  SCRN-ERROR-CODE                       <020>*/
		/* END-IF.                                                      */
		if (isEQ(srcalcpy.psNotAllwd,"T")) {
			scrnparams.errorCode.set(errorsInner.h388);
		}
		/*    IF  S6307-PERCREQD     > 99                                  */
		/*        MOVE H370 TO S6307-PERCREQD-ERR.                         */
		/* IF  S6307-PERCREQD     > 99                             <013>*/
		/* Not check the field type 'C' Fee and 'J' Penalty             */
		if ((isNE(sv.fieldType, "C"))
		&& (isNE(sv.fieldType, "J"))) {
		if (isGT(sv.percreqd,100)) {
				scrnparams.errorCode.set(errorsInner.h370);
				sv.percreqdOut[varcom.ri.toInt()].set("Y");
				/*****     MOVE 1             TO WSAA-FLAG-1                   <019>*/
				/****  ELSE                                                    <019>*/
				/*****     MOVE 1             TO WSAA-FLAG-2.                  <019>*/
			}
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			/*  MOVE 'Y'                 TO WSSP-EDTERROR                 */
			sv.hupdflg.set(" ");
		}
		else {
			sv.hupdflg.set("Y");
		}
	}

protected void updSfl2600()
	{
		sv.percreqdOut[varcom.pr.toInt()].set(" ");
		sv.actvalueOut[varcom.pr.toInt()].set(" ");
		if (isEQ(sv.hemv,ZERO)) {
			sv.percreqdOut[varcom.pr.toInt()].set("Y");
			sv.actvalueOut[varcom.pr.toInt()].set("Y");
		}
		if ((isNE(sv.fieldType,"A"))
		&& (isNE(sv.fieldType,"I"))) {
			sv.percreqdOut[varcom.pr.toInt()].set("Y");
			sv.actvalueOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.percreqdOut[varcom.ri.toInt()].set(" ");
		sv.actvalueOut[varcom.ri.toInt()].set(" ");
	}

protected void readNext2600()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(sv.hcrtable, "")) {
			scrnparams.statuz.set(varcom.endp);
			goTo(GotoLabel.exit2690);
		}
		if (isNE(sv.fieldType,"C")) {
			wsaaFee.add(sv.estMatValue);
		}
	}

	/**
	* <pre>
	*2590-EXIT.                                                       
	*    EXIT.                                                        
	* </pre>
	*/
protected void validateEffdate2700()
	{
		if (isEQ(sv.effdate,varcom.vrcmMaxDate)) {
			wsaaEffDate.set(datcon1rec.intDate);
		}
		else {
			wsaaEffDate.set(sv.effdate);
		}
		//ILIFE-6594
		String keyItemitem = wsaaEffYy.toString().trim();
		itempf = new Itempf();
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("TH608");
		itempf.setItemitem(wsaaEffYy.toString());
		itempf.setItemseq("  ");
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if(itempf != null) {
		th608rec.th608Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		wsaaTranDates.set(th608rec.th608Rec);
		/* COMPUTE WSAA-SUB = (WSAA-EFF-MM * 31) + WSAA-EFF-DD.         */
		compute(wsaaSub, 0).set(add((mult((sub(wsaaEffMm,1)),31)),wsaaEffDd));
		if (isEQ(sv.effdate,varcom.vrcmMaxDate)) {
			while ( !(isEQ(wsaaTranDay[wsaaSub.toInt()],"T")
			|| isEQ(wsaaSub,372))) {
				wsaaSub.add(1);
			}
			
			compute(wsaaEffMm, 0).setDivide(wsaaSub, (31));
			wsaaEffDd.setRemainder(wsaaEffMm);
			if (isEQ(wsaaEffDd,0)) {
				wsaaEffDd.set(31);
			}
			else {
				wsaaEffMm.add(1);
			}
			sv.effdate.set(wsaaEffDate);
		}
		if (isNE(wsaaTranDay[wsaaSub.toInt()],"T")) {
			sv.effdateErr.set(errorsInner.hl61);
		}
	}

protected void calcSurrCharge2800()
	{
		/*INIT*/
		wsaaTotalSurrCharge.set(ZERO);
		wsaaCompActvalue.set(ZERO);
		wsaaCompEstvalue.set(ZERO);
		sv.taxamt.set(0);
		scrnparams.statuz.set(varcom.oK);
		/*START*/
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			processSubfile2810();
		}
		
		/*EXIT*/
	}

protected void processSubfile2810()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readSfl2811();
					surrCharge2813();
				case nextSfl2818: 
					nextSfl2818();
				case exit2819: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readSfl2811()
	{
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2819);
		}
		/*ACCUM*/
		if (isNE(sv.fieldType,"C")
		&& isNE(sv.fieldType,"J")) {
			//ILIFE-5140
			if(isEQ(runningTotalFeeExt,ZERO))
			wsaaCompActvalue.add(sv.actvalue);
			//ILIFE-5140
			wsaaCompEstvalue.add(sv.estMatValue);
			goTo(GotoLabel.nextSfl2818);
		}
	}

protected void surrCharge2813()
	{
		if (isEQ(sv.fieldType,"J")) {
			/*     COMPUTE S6307-ACTVALUE  = WSAA-COMP-ACTVALUE     <LA4965>*/
			compute(sv.actvalue, 6).setRounded(div(mult(wsaaCompActvalue, sv.percreqd), 100));
			/*     COMPUTE S6307-ACTVALUE  = S6307-ACTVALUE         <LA4965>*/
			compute(sv.actvalue, 6).setRounded(add(sv.actvalue, (mult((div(mult(wsaaCompEstvalue, sv.prcnt), 100)), (div(sv.percreqd, 100))))));
			/*     COMPUTE S6307-ACTVALUE  = S6307-ACTVALUE         <LA4965>*/
			compute(sv.actvalue, 6).setRounded(add(sv.actvalue, (mult((mult(sv.totalamt, (div(wsaaCompEstvalue, sv.estimateTotalValue)))), (div(sv.percreqd, 100))))));
			zrdecplrec.amountIn.set(sv.actvalue);
			zrdecplrec.currency.set(sv.currcd);
			a000CallRounding();
			sv.actvalue.set(zrdecplrec.amountOut);
			wsaaTotalSurrCharge.add(sv.actvalue);
			wsaaCompActvalue.set(ZERO);
			wsaaCompEstvalue.set(ZERO);
			scrnparams.function.set(varcom.supd);
			processScreen("S6307", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			goTo(GotoLabel.nextSfl2818);
		}
	}

protected void nextSfl2818()
	{
		if (isEQ(sv.fieldType, "C")
		|| isEQ(sv.fieldType, "J")) {
			checkCalcTax5100();
		}
		scrnparams.function.set(varcom.srdn);
	}

protected void validatePenalty()
	{
		try {
			penaltyPara();
			haveDetails();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void penaltyPara()
	{
		/* IF SCRN-ERROR-CODE = 'H374' OR 'H375'                   <009>*/
		/*    MOVE SPACES TO SCRN-ERROR-CODE.                      <009>*/
		getSurrenderMeth();
		if (isNE(sv.currcd,SPACES)) {
			wsaaT5542Curr.set(sv.currcd);
		}
		else {
			wsaaT5542Curr.set(srcalcpy.chdrCurr);
		}
		readT5542();
		if (t5542NotOk.isTrue()) {
			goTo(GotoLabel.validatePenaltyExit);
		}
	}

protected void haveDetails()
	{
		if (isEQ(srcalcpy.psNotAllwd,"T")) {
			return ;
		}
		/*IF  T5542-FFAMT(1) NOT = ZERO                           <014>*/
		if (isNE(t5542rec.ffamt[index1.toInt()],ZERO)) {
			if (isNE(runningTotalFee,ZERO)) {
				/*    MOVE T5542-FFAMT(1) TO S6307-ACTVALUE               <014>*/
				sv.actvalue.set(t5542rec.ffamt[index1.toInt()]);
				sv.hactval.set(t5542rec.ffamt[index1.toInt()]);
				sv.totalfee.add(t5542rec.ffamt[index1.toInt()]);
				/*        MOVE ZERO               TO RUNNING-TOTAL-FEE     <022>*/
				return ;
			}
			else {
				sv.actvalue.set(ZERO);
				sv.hactval.set(ZERO);
				return ;
			}
		}
		/* IF T5542-FEEPC(1)  = 0                                  <014>*/
		if (isEQ(t5542rec.feepc[index1.toInt()],0)
		|| isEQ(runningTotalFee,0)) {
			sv.actvalue.set(ZERO);
			sv.hactval.set(ZERO);
			sv.percreqd.set(ZERO);
			return ;
		}
		/*  MOVE ZEROS TO S6307-PERCREQD.                          <009>*/
		/*  COMPUTE FEE-VALUE ROUNDED =                            <009>*/
		/*      (RUNNING-TOTAL-SURR * T5542-FEEPC(1)) / 100.      <014>*/
		/*      (RUNNING-TOTAL-SURR * T5542-FEEPC(INDEX1)) / 100. <022>*/
		/*       (RUNNING-TOTAL-FEE * T5542-FEEPC(INDEX1)) / 100.  <022>*/
		/* IF WSAA-TOT-FLAG           = 'Y'                        <036>*/
		/*    COMPUTE FEE-VALUE ROUNDED =                          <036>*/
		/*       (WSAA-FEE * T5542-FEEPC(INDEX1)) / 100            <036>*/
		/*       MOVE 'N' TO WSAA-TOT-FLAG                         <036>*/
		/* ELSE                                                    <036>*/
		/*    COMPUTE FEE-VALUE ROUNDED =                          <036>*/
		/*       (RUNNING-TOTAL-FEE * T5542-FEEPC(INDEX1)) / 100   <036>*/
		/* END-IF.                                                 <036>*/
		compute(feeValue, 4).setRounded(div((mult(runningTotalFee,t5542rec.feepc[index1.toInt()])),100));
		/*    IF  FEE-VALUE < T5542-FEEMIN(1)                     <014>*/
		if (isLT(feeValue,t5542rec.feemin[index1.toInt()])) {
			/*        MOVE T5542-FEEMIN(1) TO S6307-ACTVALUE          <014>*/
			sv.actvalue.set(t5542rec.feemin[index1.toInt()]);
			sv.hactval.set(t5542rec.feemin[index1.toInt()]);
			sv.totalfee.add(t5542rec.feemin[index1.toInt()]);
			/*                               FEE-USED               <018>*/
			//sv.percreqd.set(ZERO);	/*ILIFE-6709*/
		}
		else {
			/*     IF  FEE-VALUE  > T5542-FEEMAX(1)                <014>*/
			if (isGT(feeValue,t5542rec.feemax[index1.toInt()])
			&& isNE(t5542rec.feemax[index1.toInt()],ZERO)) {
				/*        MOVE T5542-FEEMAX(1) TO S6307-ACTVALUE       <014>*/
				sv.actvalue.set(t5542rec.feemax[index1.toInt()]);
				sv.hactval.set(t5542rec.feemax[index1.toInt()]);
				/*                                 FEE-USED                <018>*/
				sv.totalfee.add(t5542rec.feemax[index1.toInt()]);
				//sv.percreqd.set(ZERO);	/*ILIFE-6709*/
			}
			else {
				sv.percreqd.set(t5542rec.feepc[index1.toInt()]);
				sv.totalfee.add(feeValue);
				sv.actvalue.set(feeValue);
				sv.hactval.set(feeValue);
			}
		}
	}

protected void getSurrenderMeth(){
	List<Itempf> itempfList = new ArrayList<Itempf>();
	String keyItemitem = sv.hcrtable.toString().trim();
	Iterator<Itempf> itemIterator=null;
	if(t5687Map.containsKey(keyItemitem)){
		itempfList=t5687Map.get(keyItemitem);
		itemIterator = itempfList.iterator();
		while(itemIterator.hasNext()){
			Itempf itempf=itemIterator.next();
			if (itempf.getItmfrm().toString().equals(chdrptsIO.getOccdate().toString().trim())) {
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				break;
			}else{
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		}
	}
}

protected void readT5542(){

		if (isEQ(t5687rec.partsurr,SPACES)) {
			srcalcpy.psNotAllwd.set("T");
			return ;
		}
		if (isNE(sv.currcd,SPACES)) {
			wsaaT5542Curr.set(sv.currcd);
		}
		else {
			wsaaT5542Curr.set(srcalcpy.chdrCurr);
		}
		/* If this is a single premium component then move '00' to the     */
		/* WSAA-BILLFREQ, otherwise use the PAYR billing frequency         */
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			wsaaBillfreq.set("00");
		}
		else {
			wsaaBillfreq.set(payrpf.getBillfreq());
		}
		wsaaT5542Meth.set(t5687rec.partsurr);

		List<Itempf> itempfList = new ArrayList<Itempf>();
		String keyItemitem = wsaaT5542Key.toString().trim();
		Iterator<Itempf> itemIterator=null;
		boolean itemFound = false;
		if(t5542Map.containsKey(keyItemitem)){
			itempfList=t5542Map.get(keyItemitem);
			itemIterator = itempfList.iterator();
			while(itemIterator.hasNext()){
				Itempf itempf=itemIterator.next();
				if (itempf.getItmfrm().toString().equals(srcalcpy.crrcd.toString().trim())) {
					t5542rec.t5542Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					srcalcpy.psNotAllwd.set(SPACES);
					break;
				}else{
					t5542rec.t5542Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					srcalcpy.psNotAllwd.set(SPACES);
				}
			}
			itemFound=true;
		}
		if (!itemFound) {
			srcalcpy.psNotAllwd.set("T");
		}
		wsaaSub.set(ZERO);
		/*  PERFORM VARYING WSAA-SUB FROM 1 BY 1 UNTIL              <027>*/
		/*                                WSAA-SUB > 9              <027>*/
		/*                                                          <027>*/
		/*  IF CHDRPTS-BILLFREQ   = T5542-BILLFREQ(WSAA-SUB)        <027>*/
		/*         MOVE WSAA-SUB TO INDEX1                          <027>*/
		/*         MOVE 10 TO WSAA-SUB                              <027>*/
		/*  END-IF                                                  <027>*/
		/*  IF INDEX1   > 9                                         <027>*/
		/*     MOVE 'Y'   TO WSSP-EDTERROR                          <027>*/
		/*     MOVE H388  TO SCRN-ERROR-CODE                        <027>*/
		/*  END-IF                                                  <027>*/
		/*  END-PERFORM.                                            <027>*/
		wsaaIsT5542Ok.set("Y");
		for (wsaaSub.set(1); !(t5542NotOk.isTrue()
		|| isEQ(wsaaBillfreq, t5542rec.billfreq[wsaaSub.toInt()])); wsaaSub.add(1)){
			if (isGT(wsaaSub, 6) || isEQ(wsaaSub, t5542rec.billfreq.length - 1)) {
				wsspcomn.edterror.set("Y");
				scrnparams.errorCode.set(errorsInner.h141);
				wsaaIsT5542Ok.set("N");
			}
		}
		index1.set(wsaaSub);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3010();
					checkPreviousSurrender3030();
					readPtshclm3040();
				case readSubfile3070: 
					readSubfile3070();
				case updateBank3080:
					updateBank3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void checkPreviousSurrender3030()
	{
		/* What we try to do here is to give each set of surrender details */
		/* its own TRANNO. So that surrendering more than one policy       */
		/* within a plan can be handled by the triggering module. If we    */
		/* don't do this, the triggering module will get very confused and */
		/* will only read the first header record(PTSH) and it thinks      */
		/* all the detail records(PTSD) with the same TRANNO are belonging */
		/* to this header record!                                          */
		/* Check if the present TRANNO has been used by the previous       */
		/* surrender. If it has, add 1 to TRANNO and check again until     */
		/* a TRANNO has not been used before.                              */

		ptshObj=new Ptshpf();
		ptshObj.setChdrcoy(wsspcomn.company.toString().trim());
		ptshObj.setChdrnum(chdrptsIO.getChdrnum().toString().trim());
		ptshObj.setTranno(chdrptsIO.getTranno().toInt());
		ptshList=ptshDao.serachPtshRecord(ptshObj);
	}

protected void readPtshclm3040(){
	for(int intptshCounter=0;intptshCounter<ptshList.size();intptshCounter++){
		setPrecision(chdrptsIO.getTranno(), 0);
		chdrptsIO.setTranno(add(chdrptsIO.getTranno(),1));
	}
}

protected void readSubfile3070()
	{
		/*  Read thru subfile to accumulate total estimated value*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaEstTotal.set(0);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			addEstTot5000();
		}
		
		/*   restart the subfile and read each record and update the*/
		/*   claims detail file.*/
		wsaaTotalamtLeft.set(sv.totalamt);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(chdrptsIO.getNlgflg().trim(),'Y')){
			nlgtpf = nlgtpfDAO.getNlgtRecord(chdrptsIO.chdrcoy.toString(),chdrptsIO.chdrnum.toString());
			Getwithdrawalamount();
		}
		wsaaRrn.set(ZERO);
		ptsdpflist = new ArrayList<Ptsdpf>();
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			readSubfile3100();
		}
		
		if(ptsdpflist != null && ptsdpflist.size()>0) {
			ptsdpfDAO.insertPtsdpfRecord(ptsdpflist);
		}
		ptsdpflist = new ArrayList<Ptsdpf>();
		/*  Ref No. 11/T013-01 in amendment history.                    */
		if (isGT(sv.totalamt,0)
		&& isNE(wsaaTotalamtLeft,0)) {
			roundingAdjust3200();
		}
		createSurhHeader3400();
		// ILIFE-5459 Start	
		if(isEQ(sv.reserveUnitsInd, "Y") && isNE(sv.reserveUnitsDate, 0) && 
				isNE(sv.reserveUnitsDate, varcom.maxdate)){
			writeReservePricing();
		}// ILIFE-5459 End
		
	}

protected void updateBank3080() {
	if(!susur002Permission) return ;
	pyoupf = new Pyoupf();
	 if(isNE(sv.bankacckey,SPACE)){
		    pyoupf.setBankacckey(sv.bankacckey.toString());
		    }
		 if(isNE(sv.crdtcrd,SPACE)){
		    pyoupf.setBankacckey(sv.crdtcrd.toString());
		    }
		if(isEQ(sv.bankacckey,SPACE)){
		     pyoupf.setBankacckey(sv.crdtcrd.toString());
		    }
		if(isEQ(sv.crdtcrd,SPACE)){
		     pyoupf.setBankacckey(sv.bankacckey.toString());
		    }
	pyoupf.setBankdesc(sv.bankdesc.toString());
	pyoupf.setBankkey(sv.bankkey.toString());
	pyoupf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	pyoupf.setChdrcoy(wsspcomn.company.toString());
	pyoupf.setChdrnum(sv.chdrnum.toString());
	pyoupf.setChdrpfx("CH");
	pyoupf.setPayrnum(sv.payee.trim());//ICIL-484
	pyoupf.setPaymentflag(" ");
	pyoupf.setReqntype(sv.reqntype.toString());
	pyoupf.setReqnno(" ");
	pyoupf.setTranno(chdrptsIO.getTranno().toInt());
	pyoupfDAO.insertPyoupfRecord(pyoupf);
	
	chdrptsIO.setFunction(varcom.rlse);
	executeChdrptsIO3020();
	 
	 chdrptsIO.setCownnum(sv.payee);
	 chdrptsIO.setReqntype(sv.reqntype);
	 if(isNE(sv.bankacckey,SPACES)){
		 chdrptsIO.setBankacckey(sv.bankacckey.toString());
	 	}
	 if(isNE(sv.crdtcrd,SPACES)){
		 chdrptsIO.setBankacckey(sv.crdtcrd.toString());
		}
	 chdrptsIO.setBankkey(sv.bankkey.toString());
	 chdrptsIO.setZmandref(SPACES);
	 chdrptsIO.setFunction(varcom.updat);
	 executeChdrptsIO3020();
	 
	 chdrptsIO.setFunction(varcom.keeps);
	 executeChdrptsIO3020();

}

protected void executeChdrptsIO3020() {
	 SmartFileCode.execute(appVars, chdrptsIO);
		if (isNE(chdrptsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
}

protected void readSubfile3100()
	{
		/*READ-NEXT-RECORD*/
		createSurdDetail3300();
		scrnparams.function.set(varcom.srdn);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void roundingAdjust3200()
	{
		
		/*  The residual, rounding error needs to be added to the       */
		/*  last processed fund value. The last record read will always */
		/*  be a Fee record. Use the RRN stored in the Create Record    */
		/*  section and update that record.                             */
		wsaaPtsdpf = new Ptsdpf(); 	   
		wsaaPtsdpf.setPlanSuffix(new BigDecimal(wsaaPlanSuffix.toInt()));
		wsaaPtsdpf.setChdrcoy(wsspcomn.company.toString());
		wsaaPtsdpf.setChdrnum(chdrptsIO.getChdrnum().toString());
		wsaaPtsdpf.setLife(sv.hlife.toString());
		wsaaPtsdpf.setCoverage(sv.hcover.toString());
		if (isEQ(sv.rider,SPACES)) {
			wsaaPtsdpf.setRider("00");
		}
		else {
			wsaaPtsdpf.setRider(sv.rider.toString());
		}		
		wsaaPtsdpf.setTranno(chdrptsIO.getTranno().toInt());
		ptsdpf = ptsdpfDAO.getPtsdpfByKey(wsaaPtsdpf);
		if (ptsdpf == null) {
			syserrrec.params.set(ptsdpf.getUniqueNumber());
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
		setPrecision(ptsdpf.getEstMatValue(), 2);
		TotalamtLeft=wsaaTotalamtLeft.getbigdata();
		ptsdpf.setEstMatValue(new BigDecimal(add(ptsdpf.getEstMatValue(), TotalamtLeft).toDouble()));
		//ptsdpf.setEstMatValue(ptsdpf.getEstMatValue().add(new BigDecimal(wsaaTotalamtLeft.toDouble())));
		maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
		
		if(maxwithConfig) 
		{
			ptsdpfDAO.updatePtsdpfForEMTByNo(ptsdpf.getUniqueNumber(), ptsdpf.getEstMatValue().setScale(3, BigDecimal.ROUND_HALF_UP));
		}
		else
		{
			ptsdpfDAO.updatePtsdpfForEMTByNo(ptsdpf.getUniqueNumber(), ptsdpf.getEstMatValue());
		}
	    
	}

protected void createSurdDetail3300()   
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					writeFromSubfile3310();
				case checkTots3320: 
					checkTots3320();
				case exit3390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void writeFromSubfile3310()
	{
	//MIBT308-start	
	if (isEQ(sv.fieldType,"C")) {
		sv.actvalue.set(sv.percreqd);
		sv.percreqd.set(ZERO);
		}
	//MIBT308-end		
		if (isEQ(sv.estMatValue,ZERO)
		&& isEQ(sv.actvalue,ZERO)) {
			goTo(GotoLabel.exit3390);
		}
		if (isEQ(sv.actvalue,ZERO)
		&& isEQ(sv.percreqd,ZERO)
		&& isNE(sv.fieldType,"C")
		&& isNE(sv.fieldType,"J")) {
			goTo(GotoLabel.checkTots3320);
		}
		ptsdpf = new Ptsdpf();
		ptsdpf.setPlanSuffix(new BigDecimal(wsaaPlanSuffix.toInt()));
		ptsdpf.setChdrcoy(wsspcomn.company.toString());
		ptsdpf.setChdrnum(chdrptsIO.getChdrnum().toString());
		/*MOVE COVRSUR-LIFE           TO PTSDCLM-LIFE.                 */
		/*MOVE COVRSUR-JLIFE          TO PTSDCLM-JLIFE.                */
		/*MOVE COVRSUR-CRTABLE        TO PTSDCLM-CRTABLE.              */
		/* MOVE SURC-LIFE-LIFE         TO PTSDCLM-LIFE.            <CAS1*/
		/* MOVE SURC-LIFE-JLIFE        TO PTSDCLM-JLIFE.           <CAS1*/
		ptsdpf.setLife(sv.hlife.toString());
		ptsdpf.setJlife(sv.hjlife.toString());
		ptsdpf.setCrtable(sv.hcrtable.toString());
		ptsdpf.setCoverage(sv.hcover.toString());
		if (isEQ(sv.rider,SPACES)) {
			ptsdpf.setRider("00");
		}
		else {
			ptsdpf.setRider(sv.rider.toString());
		}
		ptsdpf.setShortds(sv.shortds.toString());
		ptsdpf.setTranno(chdrptsIO.getTranno().toInt());
		/*   MOVE S6307-RIIND            TO PTSDCLM-RIIND.                */
		ptsdpf.setCurrcd(sv.cnstcur.toString());
		/* IF S6307-ACTVALUE           > ZERO                           */
		/*    AND S6307-PERCREQD NOT > ZERO                        <009>*/
		/*                                                         <029>*/
		/*    MOVE S6307-ACTVALUE      TO PTSDCLM-EST-MAT-VALUE    <029>*/
		/*    MOVE ZEROS               TO PTSDCLM-EST-MAT-VALUE    <029>*/
		/*                                                         <029>*/
		/* ELSE                                                         */
		/*    MOVE S6307-EST-MAT-VALUE TO PTSDCLM-EST-MAT-VALUE.        */
		if (isGT(sv.actvalue,ZERO)
		&& isLTE(sv.percreqd,ZERO)) {
			 maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
				
				if(maxwithConfig) 
				{
					ptsdpf.setEstMatValue((BigDecimal.valueOf(sv.actvalue.toDouble()).setScale(3, BigDecimal.ROUND_HALF_UP)));
				}
				else
				{
					ptsdpf.setEstMatValue(BigDecimal.valueOf(sv.actvalue.toDouble()));
				}
		}
		else {
			if(maxwithConfig) 
			{
				ptsdpf.setEstMatValue((BigDecimal.valueOf(sv.estMatValue.toDouble()).setScale(3, BigDecimal.ROUND_HALF_UP)));
			}
			else
			{
				ptsdpf.setEstMatValue(BigDecimal.valueOf(sv.estMatValue.toDouble()));
			}
		}
		ptsdpf.setActvalue(BigDecimal.ZERO);
		ptsdpf.setFieldType(sv.htype.toString());
		if (isEQ(ptsdpf.getFieldType(),"C")
		|| isEQ(ptsdpf.getFieldType(),"J")) {
			ptsdpf.setActvalue(BigDecimal.valueOf(sv.actvalue.toDouble()));
			ptsdpf.setEstMatValue(BigDecimal.ZERO);
		}
		ptsdpf.setVirtualFund(sv.fund.toString());
		ptsdpf.setPercreqd(BigDecimal.valueOf(sv.percreqd.toDouble()));
	    ptsdpflist.add(ptsdpf);
	    
		goTo(GotoLabel.exit3390);
	}

protected void checkTots3320()
	{
		/*    If total has been entered at bottom of screen then output*/
		/*    a detail record for each fund (ie line of subfile) on screen*/
		if (isEQ(sv.totalamt,ZERO)
		&& isEQ(sv.prcnt,ZERO)) {
			return ;
		}
		ptsdpf = new Ptsdpf();
		ptsdpf.setPlanSuffix(new BigDecimal(wsaaPlanSuffix.toInt()));
		ptsdpf.setChdrcoy(wsspcomn.company.toString());
		ptsdpf.setChdrnum(chdrptsIO.getChdrnum().toString());
		/*MOVE COVRSUR-LIFE           TO PTSDCLM-LIFE.                 */
		/*MOVE COVRSUR-JLIFE          TO PTSDCLM-JLIFE.                */
		/*MOVE COVRSUR-CRTABLE        TO PTSDCLM-CRTABLE.              */
		/* MOVE SURC-LIFE-LIFE         TO PTSDCLM-LIFE.            <CAS1*/
		/* MOVE SURC-LIFE-JLIFE        TO PTSDCLM-JLIFE.           <CAS1*/
		ptsdpf.setLife(sv.hlife.toString());
		ptsdpf.setJlife(sv.hjlife.toString());
		ptsdpf.setCrtable(sv.hcrtable.toString());
		ptsdpf.setCoverage(sv.hcover.toString());
		if (isEQ(sv.rider,SPACES)) {
			ptsdpf.setRider("00");
		}
		else {
			ptsdpf.setRider(sv.rider.toString());
		}
		ptsdpf.setShortds(sv.shortds.toString());
		ptsdpf.setTranno(chdrptsIO.getTranno().toInt());
		/*  MOVE S6307-RIIND            TO PTSDCLM-RIIND.                */
		ptsdpf.setCurrcd(sv.cnstcur.toString());
		ptsdpf.setActvalue(BigDecimal.ZERO);
		if (isGT(sv.totalamt,ZERO)) {
			maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
			
			if(maxwithConfig) 
			{
				ptsdpf.setEstMatValue((new BigDecimal(div((mult(sv.totalamt,sv.estMatValue)),wsaaEstTotal).toDouble())).setScale(3, BigDecimal.ROUND_HALF_UP));
			}
			else
			{
				ptsdpf.setEstMatValue(new BigDecimal(div((mult(sv.totalamt,sv.estMatValue)),wsaaEstTotal).toDouble()));
			}
			ptsdpf.getEstMatValue().setScale(3, BigDecimal.ROUND_HALF_UP);
			zrdecplrec.amountIn.set(ptsdpf.getEstMatValue());
			zrdecplrec.currency.set(sv.currcd);
			a000CallRounding();
			if(maxwithConfig) 
			{
				ptsdpf.setEstMatValue(new BigDecimal((zrdecplrec.amountOut.toDouble())).setScale(3, BigDecimal.ROUND_HALF_UP));
			}
			else
			{
				ptsdpf.setEstMatValue(new BigDecimal(zrdecplrec.amountOut.toDouble()));
			}
			/* At the beginning, S6307-TOTALAMT = WSAA-TOTALAMT-LEFT.          */
			/* We are assuming that the rounding error is less than 1.         */
			if(maxwithConfig) 
			{
				compute(wsaaTotalamtLeft, 2).set(sub(wsaaTotalamtLeft,ptsdpf.getEstMatValue().setScale(3, BigDecimal.ROUND_HALF_UP)));
			}
			else
			{
				compute(wsaaTotalamtLeft, 2).set(sub(wsaaTotalamtLeft,ptsdpf.getEstMatValue().setScale(3, BigDecimal.ROUND_HALF_UP)));
			}
			/*****  Ref No. 11/T013-01 in amendment history.                    */
			/*****     IF WSAA-TOTALAMT-LEFT < 1                           <004>*/
			/*****         ADD WSAA-TOTALAMT-LEFT TO PTSDCLM-EST-MAT-VALUE <004>*/
			/*****     ELSE                                                <004>*/
			/*****         NEXT SENTENCE                                   <004>*/
		}
		else {
			if(maxwithConfig) 
			{
				ptsdpf.setEstMatValue(new BigDecimal((sv.estMatValue.toDouble())).setScale(3, BigDecimal.ROUND_HALF_UP));
			}
			else
			{
				ptsdpf.setEstMatValue(new BigDecimal(sv.estMatValue.toDouble()));
			}
		}
		ptsdpf.setFieldType(sv.htype.toString());
		ptsdpf.setVirtualFund(sv.fund.toString());
		ptsdpf.setPercreqd(new BigDecimal(sv.prcnt.toDouble()));
		
		ptsdpflist.add(ptsdpf);
		/* Store the relative record number for each non-fee record.    */		
		//wsaaRrn.set(ptsdclmIO.getRrn());
	    wsaaPtsdpf = new Ptsdpf();
	    wsaaPtsdpf.setChdrcoy(ptsdpf.getChdrcoy());
	    wsaaPtsdpf.setChdrnum(ptsdpf.getChdrnum());
	    wsaaPtsdpf.setTranno(ptsdpf.getTranno());
	    wsaaPtsdpf.setPlanSuffix(ptsdpf.getPlanSuffix());
	    wsaaPtsdpf.setLife(ptsdpf.getLife());
	    wsaaPtsdpf.setCoverage(ptsdpf.getCoverage());
	    wsaaPtsdpf.setRider(ptsdpf.getRider());
       
	}

protected void createSurhHeader3400()
{

	    ptshpf = new Ptshpf();
	    ptshpf.setChdrcoy(wsspcomn.company.toString());
	    ptshpf.setChdrnum(chdrptsIO.getChdrnum().toString());
	    ptshpf.setPlanSuffix(new BigDecimal(wsaaPlanSuffix.toInt()));
	    ptshpf.setLife(wsaaLife.toString());
	    ptshpf.setJlife(wsaaJlife.toString());
	    ptshpf.setCurrcd(sv.currcd.toString());
	    ptshpf.setEffdate(sv.effdate.toLong());
	    ptshpf.setPrcnt(new BigDecimal(sv.prcnt.toDouble()));
	    maxwithConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
		
		if(maxwithConfig) 
		{
			ptshpf.setTotalamt(new BigDecimal(sv.totalamt.toDouble()).setScale(3, BigDecimal.ROUND_HALF_UP));
		}
		else
		{
			ptshpf.setTotalamt(new BigDecimal(sv.totalamt.toDouble()));
		}
	    
	    ptshpf.setTranno(chdrptsIO.getTranno().toInt());
	    ptshpf.setCnttype(chdrptsIO.getCnttype().toString());
	    ptshDao.insertPtshRecord(ptshpf);
	}

	/**
	* <pre>
	*3500-CALL-XCVRT  SECTION.                                        
	*3510-SCREEN-IO.                                                  
	*3590-EXIT.                                                       
	*     EXIT.                                                       
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void addEstTot5000()
	{
		/*ADD*/
		/* Forget the penalty, do not accumulate as estimated total. So    */
		/* that for surrender by total amount, it can work out the         */
		/* correct percentages from each fund based on this total.         */
		if (isNE(sv.fieldType,"C")
		&& isNE(sv.fieldType,"J")) {
			wsaaEstTotal.add(sv.estMatValue);
		}
		/*NEXT-SUBFILE*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S6307", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkCalcTax5100()
	{
		try {
			start5100();
			readChdrenq5120();
			readTr52d5130();
			callTaxSubr5140();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start5100()
	{
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
	}

protected void readChdrenq5120()
	{
	
		chdrenq = chdrpfDAO.getChdrpfByConAndNumAndServunit(chdrptsIO.getChdrcoy().toString(), chdrptsIO.getChdrnum().toString());
		if (chdrenq == null) {
			syserrrec.params.set(chdrptsIO.getChdrcoy().toString().concat(chdrptsIO.getChdrnum().toString()));
			syserrrec.params.set("MRNF");
			fatalError600();
		}
	}

protected void readTr52d5130()
	{
		String keyItemitem = chdrenq.getReg().trim();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (tr52dListMap.containsKey(keyItemitem)){	
			itempfList = tr52dListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));							
			}		
		}else{
			keyItemitem = "***";
			if (tr52dListMap.containsKey(keyItemitem)){	
				itempfList = tr52dListMap.get(keyItemitem);
				Iterator<Itempf> iterator = itempfList.iterator();
				while (iterator.hasNext()) {
					Itempf itempf = new Itempf();
					itempf = iterator.next();
					if(itempf.getItemcoy().equals(chdrenq.getChdrcoy().toString().trim())){
						tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));					
					}			
				}		
			}else{
				fatalError600();
			}
		}
		
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
		}
		else {
			goTo(GotoLabel.exit5190);
		}
		/* Read table TR52E                                                */
		//ILIFE-6713
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrenq.getCnttype());
		wsaaTr52eCrtable.set(sv.hcrtable);
		String itemitem = "";
		if (tr52eListMap.containsKey(wsaaTr52eKey.toString().trim())){
			itemitem = wsaaTr52eKey.toString().trim();
		}else {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrenq.getCnttype());
			wsaaTr52eCrtable.set("****");
			if (tr52eListMap.containsKey(wsaaTr52eKey.toString().trim())){
				itemitem = wsaaTr52eKey.toString().trim();
			}else{
				wsaaTr52eKey.set(SPACES);
				wsaaTr52eTxcode.set(tr52drec.txcode);
				wsaaTr52eCnttype.set("***");
				wsaaTr52eCrtable.set("****");
				if (tr52eListMap.containsKey(wsaaTr52eKey.toString().trim())){
					itemitem = wsaaTr52eKey.toString().trim();
				}
			}
		}
		if(!itemitem.equals("")){
			readTr52e5200(itemitem);
		}
		/* IF TR52E-TAXIND-05          NOT = 'Y'                        */
		if (isNE(tr52erec.taxind03, "Y")) {
			goTo(GotoLabel.exit5190);
		}
	}

protected void callTaxSubr5140()
	{
		/* Call TR52D tax subroutine                                       */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrenq.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenq.getChdrnum());
		txcalcrec.life.set(sv.hlife);
		txcalcrec.coverage.set(sv.coverage);
		txcalcrec.rider.set(sv.rider);
		txcalcrec.crtable.set(sv.hcrtable);
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrenq.getCnttype());
		txcalcrec.register.set(chdrenq.getReg());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		if (isEQ(sv.currcd, SPACES)) {
			txcalcrec.ccy.set(chdrenq.getCntcurr());
			wsaaCntCurr.set(chdrenq.getCntcurr());
		}
		else {
			txcalcrec.ccy.set(sv.currcd);
			wsaaCntCurr.set(sv.currcd);
		}
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.effdate.set(sv.effdate);
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.taxrule.set(SPACES);
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.amountIn.set(sv.actvalue);
		txcalcrec.transType.set("SURF");
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				sv.taxamt.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				sv.taxamt.add(txcalcrec.taxAmt[2]);
			}
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
		}
	}

protected void readTr52e5200(String itemitem){
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	itempfList = tr52eListMap.get(itemitem);
	Iterator<Itempf> iterator = itempfList.iterator();
	while (iterator.hasNext()) {
		Itempf itempf = new Itempf();
		itempf = iterator.next();
		if(itempf.getItmfrm().compareTo(new BigDecimal(sv.effdate.toString())) < 1 ){
			tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));		
		}			
	}		
}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/**
 * ILIFE-5459 Start
 */
protected void writeReservePricing(){

	varcom.vrcmTranid.set(wsspcomn.tranid);
	wsaaBatckey.set(wsspcomn.batchkey);
	syserrrec.subrname.set(wsaaProg);
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	wsaaBusinessDate.set(datcon1rec.intDate);
	
	zutrpf.setChdrpfx("CH");
	zutrpf.setChdrcoy(ptshpf.getChdrcoy().trim());
	zutrpf.setChdrnum(sv.chdrnum.toString().trim());
	zutrpf.setLife(ptshpf.getLife().trim());	
	zutrpf.setCoverage(sv.coverage.toString().trim());
	zutrpf.setRider(sv.rider.toString().trim());
	zutrpf.setPlanSuffix(ptshpf.getPlanSuffix().intValue());
	zutrpf.setTranno(ptshpf.getTranno()); // TODO ADD 1 ?
	zutrpf.setReserveUnitsInd(sv.reserveUnitsInd.toString().trim());
	zutrpf.setReserveUnitsDate(sv.reserveUnitsDate.toInt());
	zutrpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());	
	zutrpf.setTransactionDate(varcom.vrcmDate.toInt());
	zutrpf.setTransactionTime(varcom.vrcmTime.toInt());
	zutrpf.setUser(varcom.vrcmUser.toInt());
	zutrpf.setEffdate(sv.effdate.toInt());
	zutrpf.setValidflag("1");
	zutrpf.setDatesub(wsaaBusinessDate.toInt()); 
	zutrpf.setCrtuser(wsspcomn.userid.toString());  
	zutrpf.setUserProfile(wsspcomn.userid.toString()); 
	zutrpf.setJobName(appVars.getLoggedOnUser());
	zutrpf.setDatime("");
	
	try{
		zutrpfDAO.insertZutrpfRecord(zutrpf);
	}catch(Exception e){
	
		LOGGER.error("Exception occured in writeReservePricing()",e);
		fatalError600();
	}
	
}// ILIFE-5459 End

/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e762 = new FixedLengthStringData(4).init("E762");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData g008 = new FixedLengthStringData(4).init("G008");
	private FixedLengthStringData h141 = new FixedLengthStringData(4).init("H141");
	private FixedLengthStringData h355 = new FixedLengthStringData(4).init("H355");
	private FixedLengthStringData h370 = new FixedLengthStringData(4).init("H370");
	private FixedLengthStringData h371 = new FixedLengthStringData(4).init("H371");
	private FixedLengthStringData h372 = new FixedLengthStringData(4).init("H372");
	private FixedLengthStringData h374 = new FixedLengthStringData(4).init("H374");
	private FixedLengthStringData h376 = new FixedLengthStringData(4).init("H376");
	private FixedLengthStringData h378 = new FixedLengthStringData(4).init("H378");
	private FixedLengthStringData h379 = new FixedLengthStringData(4).init("H379");
	private FixedLengthStringData h388 = new FixedLengthStringData(4).init("H388");
	private FixedLengthStringData h384 = new FixedLengthStringData(4).init("H384");
	private FixedLengthStringData hl61 = new FixedLengthStringData(4).init("HL61");
	private FixedLengthStringData hl62 = new FixedLengthStringData(4).init("HL62");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData h359 = new FixedLengthStringData(4).init("H359");
	private FixedLengthStringData rlcc = new FixedLengthStringData(4).init("RLCC");
	private FixedLengthStringData g099 = new FixedLengthStringData(4).init("G099");
	private FixedLengthStringData rrfn = new FixedLengthStringData(4).init("RRFN");
	private FixedLengthStringData f906 = new FixedLengthStringData(4).init("F906");
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
	private FixedLengthStringData rf05 = new FixedLengthStringData(4).init("RF05");
}

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData uwlvrec = new FixedLengthStringData(10).init("UWLVREC");
}

//ILIFE-5140
private Map<String,BigDecimal> getUlnkpfRecord(S6307ScreenVars sv){
	
	Ulnkpf ulnkpf = new Ulnkpf();
	ulnkpf.setChdrnum(String.valueOf(sv.chdrnum));
	ulnkpf.setChdrcoy(String.valueOf(wsspcomn.company));
	List<Ulnkpf> ulnkpfList = ulnkpfDAO.getUlnkpfByChdrnum(ulnkpf);
	Map<String,BigDecimal> map = new HashMap<>();
	if(ulnkpfList!=null && !ulnkpfList.isEmpty()){
		ulnkpf = ulnkpfList.get(0);
		map.put(ulnkpf.getUnitAllocFund01(), ulnkpf.getUnitAllocPercAmt01());
		map.put(ulnkpf.getUnitAllocFund02(), ulnkpf.getUnitAllocPercAmt02());	
		map.put(ulnkpf.getUnitAllocFund03(), ulnkpf.getUnitAllocPercAmt03());	
		map.put(ulnkpf.getUnitAllocFund04(), ulnkpf.getUnitAllocPercAmt04());	
		map.put(ulnkpf.getUnitAllocFund05(), ulnkpf.getUnitAllocPercAmt05());	
		map.put(ulnkpf.getUnitAllocFund06(), ulnkpf.getUnitAllocPercAmt06());	
		map.put(ulnkpf.getUnitAllocFund07(), ulnkpf.getUnitAllocPercAmt07());	
		map.put(ulnkpf.getUnitAllocFund08(), ulnkpf.getUnitAllocPercAmt08());	
		map.put(ulnkpf.getUnitAllocFund09(), ulnkpf.getUnitAllocPercAmt09());	
		map.put(ulnkpf.getUnitAllocFund10(), ulnkpf.getUnitAllocPercAmt10());	
	}
	
	return map;
}
private void getCalculatationActualValue(Map<String,BigDecimal> mapOfUlnkpf, S6307ScreenVars sv){
	if(mapOfUlnkpf!=null && mapOfUlnkpf.size() > 0){
		BigDecimal ualprc = mapOfUlnkpf.get(sv.fund.toString());
		if(ualprc == null) { //ILIFE-8614
			return;
		}
		if (isNE(sv.totalamt,ZERO)) {
			compute(runningTotalFee, 3).setRounded((add(runningTotalFee,(div((mult(sv.totalamt,ualprc)),100)))));
			compute(runningTotalSurr, 3).setRounded((add(runningTotalSurr,(div((mult(sv.totalamt,ualprc)),100)))));	
			runningTotalFeeExt.set(runningTotalFee);
			sv.actvalue.set((div((mult(sv.totalamt,ualprc)),100)));
			sv.hactval.set(sv.actvalue);
			zrdecplrec.amountIn.set(sv.actvalue);
			zrdecplrec.currency.set(sv.currcd);
			a000CallRounding();
			sv.actvalue.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(sv.hactval);
			zrdecplrec.currency.set(sv.currcd);
			a000CallRounding();
			sv.hactval.set(zrdecplrec.amountOut);
		}
		if (isNE(sv.prcnt,ZERO)) {
			compute(runningTotalFeeExt, 3).setRounded((div((mult(sv.estimateTotalValue,sv.prcnt)),100)));
			compute(runningTotalFee, 3).setRounded((add(runningTotalFee,(div((mult(runningTotalFeeExt,ualprc)),100)))));
			compute(runningTotalSurr, 3).setRounded((add(runningTotalSurr,(div((mult(runningTotalFeeExt,ualprc)),100)))));
			sv.actvalue.set((div((mult(runningTotalFeeExt,ualprc)),100)));
			sv.hactval.set(sv.actvalue);
			zrdecplrec.amountIn.set(sv.actvalue);
			zrdecplrec.currency.set(sv.currcd);
			a000CallRounding();
			sv.actvalue.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(sv.hactval);
			zrdecplrec.currency.set(sv.currcd);
			a000CallRounding();
			sv.hactval.set(zrdecplrec.amountOut);
		}
		/*ILIFE-6709*/
		if(isNE(sv.actvalue,ZERO) && isNE(sv.fieldType, "C") && isNE(sv.fieldType, "J") && isEQ(sv.totalamt,ZERO) &&  isEQ(sv.prcnt,ZERO)){ 
			compute(actCumulativeVal,2).setRounded(div(mult(sv.actvalue,100),ualprc));
			compute(runningTotalFee, 3).setRounded((add(runningTotalFee,(div((mult(actCumulativeVal,ualprc)),100)))));
			compute(runningTotalSurr, 3).setRounded((add(runningTotalSurr,(div((mult(actCumulativeVal,ualprc)),100)))));	
		}
		/*ILIFE-6709*/
	}else{
		runningTotalFee.add(wsaaEstMatValue);
	}
}//ILIFE-5140						

}
