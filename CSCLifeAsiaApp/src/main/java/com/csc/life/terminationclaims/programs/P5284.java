/*
 * File: P5284.java
 * Date: 30 August 2009 0:23:21
 * Author: Quipoz Limited
 * 
 * Class transformed from P5284.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.power;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.interestbearing.dataaccess.HitrrnlTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CpbnfypfDAO;
import com.csc.life.newbusiness.dataaccess.model.Cpbnfypf;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.terminationclaims.dataaccess.FlupclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.HdcdTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.ClmdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmhpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClnnpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.InvspfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ZhlbpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ZhlcpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.ZhlbpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.ZhlcpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Clmdpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmhpf;
import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.life.terminationclaims.dataaccess.model.Zhlbpf;
import com.csc.life.terminationclaims.dataaccess.model.Zhlcpf;
import com.csc.life.terminationclaims.recordstructures.Dthclmflxrec;
import com.csc.life.terminationclaims.screens.S5284ScreenVars;
import com.csc.life.terminationclaims.screens.Sd5jlScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6617rec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.smart400framework.utility.DateUtils;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*      Death Claims Approval
*
*  This  transaction, Death Claims Approval, is selected from
*  the Claims sub-menu S6319/P6319.
*
*     Initialise
*     ----------
*
*  Read the  Contract  header  (function  RETRV)  and  read
*  the relevant data necessary for obtaining the status
*  description, the Owner, the CCD, Paid-to-date and the
*  Bill-to-date.
*
*  Read the Claim header record (CLMHCLM).
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To obtain the life assured and joint-life details (if any)
*  do the following;-
*
*  - Obtain  the  life  details  using LIFECLM,(READR) (for
*    this  contract  number, using the life number/joint
*    life  number  from the CLMHCLM record).  Format the
*    name accordingly.
*
*  - Obtain  the  joint-life  details using LIFECLM (READR)
*    (for   this   contract   number,   using  the  life
*     number/joint  life number from the CHLMCLM record).
*     Format the name accordingly.
*
*  - indicate the 'dead' life by a highlighted 'asterisk'.
*
*  Obtain the  necessary  descriptions  by  calling 'DESCIO'
*  and output the descriptions of the statuses to the screen.
*
*  Format Name
*
*  Read the  client  details  record  and  use the relevant
*  copybook in order to format the required names.
*
*  OUTPUT THE CLAIM HEADER DETAILS.
*
*  Output the following details from the claim header:
*
*                - date of death
*                - effective date
*                - cause of death
*                - follow-ups
*                - adjustment reason code
*                - policy loan amount
*                - other adjustments amount
*                - currency
*                - adjustment reason description
*
*  OUTPUT THE TOTAL AMOUNT.
*
*  Read the   claim   detail   records  for  this  contract
*  (CLMDCLM)  and sum all the actual value amounts and
*  output to the screen as the:-
*
*  'Net  Claim  Amount'  and the 'Total Actual Amount'.
*
*  The above details are returned  to  the user and if he
*  wishes to  continue  and  perform  the  Approval  routine
*  he presses enter. If required, he  may also enter (at the
*  bottom of the screen) the  number of days interest to be
*  added to the claim amount  and/or  any  office  charges
*  that  may  have accrued (deduction),  otherwise  opt  for
*  'KILL' to exit from this transaction.
*
*     Validation
*     ----------
*
*  If 'KILL  was entered, then skip the remainder of the
*  validation and exit from the program.
*
*  Office Charges
*
*  - check that the office  charge  is not greater than the
*    'Net Claim Amount'.
*
*  PROCESS INTEREST PAYMENTS
*
*  If a value is entered it  will  be the number of days of
*  interest that  is  applicable  to  the  total claim
*  amount due to late settlement of the claim.
*
*  To obtain  the  interest  rate applicable, access T6617,
*  keyed  by  transaction.  The  net  claim  amount is
*  multiplied by the calculated interest rate below.
*
*  Use the following method to calculate the interest:
*
*  (1 + .i)y  *  (1 +  ( (.i * d)  /  n) ).
*
*  i = interest rate per annum
*  y = to the power of the number of whole years
*  d = the number of residual days
*  n = no.  of days in the year (use 365).
*
*  If 'CALC' is pressed, then the screen is redisplayed and
*  the adjusted amount is included with the final total
*  amount, i.e. if an interest accrued amount exists.
*
*
*     Updating
*     --------
*
*  Skip this section  if  returning  from  an optional
*  selection (current stack position action flag = '*').
*
*  If the KILL function key was pressed, skip the updating
*  and exit from the program.
*
*  UPDATE CLAIMS HEADER.
*
*  Add  the  following  fields  to  the  Claim  header
*  (CLMHCLM) record and update the file:-
*           - interest credit days
*           - interest accrued amounts
*           - office charges
*
*  Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
*  Call the AT  submission module to submit P5284AT, passing
*  the Contract  number  as  the  'primary  key',  this
*  performs the Claims Approval transaction.
*
*  Next Program
*  ------------
*
*  For 'KILL' or 'Enter', add 1 to the program pointer and exit.
*
*
*  Modifications
*  -------------
*
*  Rewrite P5284 with the above processing included.
*
*  Include  the  following logical views (access only the
*  fields required).
*
*            - CHDRCLM
*            - LIFECLM
*            - CLMRCLM
*
*****************************************************************     *
****************** LIFE ASIA V2.0 ENHANCEMENTS ******************     *
*****************************************************************     *
*                                                                     *
* As part of the new Interest Bearing functionality, an error         *
* message is output in cases where there are outstanding              *
* HITR records.                                                       *
*                                                                     *
*****************************************************************
* </pre>
*/
public class P5284 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5284");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaOutstanflu = new FixedLengthStringData(1).init(SPACES);
	protected ZonedDecimalData wsaaNet = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaTotclaim = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaWholeYear = new ZonedDecimalData(5, 0).init(ZERO);
	private ZonedDecimalData wsaaCalc = new ZonedDecimalData(17, 7).init(ZERO);
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(3, 0).init(ZERO);
	private FixedLengthStringData wsaaIncompleteUtrn = new FixedLengthStringData(1).init(SPACES);

	protected FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	protected FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	protected FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	protected PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	protected PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	protected PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	protected FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	protected FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaTransactionRec, 16);
	protected FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 19);

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	private FixedLengthStringData wsaaCurrStore = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaCurrIn = new FixedLengthStringData(3);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	
	private ZonedDecimalData wsaaHdlAdj = new ZonedDecimalData(17, 2).init(ZERO);//BRD-34
	private ZonedDecimalData wsaaHdlAmt = new ZonedDecimalData(17, 2).init(ZERO);//BRD-34
	private static final String acblenqrec = "ACBLREC"; //BRD-34
	
	
		/* ERRORS */
	private static final String g892 = "G892";
	private static final String e304 = "E304";
	private static final String f910 = "F910";
	private static final String h017 = "H017";
	private static final String h074 = "H074";
	private static final String h077 = "H077";
	private static final String h960 = "H960";
	private static final String rfik = "RFIK";
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t3623 = "T3623";
	private static final String t6617 = "T6617";
	private static final String t3588 = "T3588";
	private static final String t5661 = "T5661";
	private static final String clmhclmrec = "CLMHCLMREC";
	private static final String utrnrec = "UTRNREC";
	private static final String hitrrnlrec = "HITRRNLREC";
	private static final String hdcdrec = "HDCDREC";
/**	protected ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM(); */
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HdcdTableDAM hdcdIO = new HdcdTableDAM();
	private HitrrnlTableDAM hitrrnlIO = new HitrrnlTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeclmTableDAM lifeclmIO = new LifeclmTableDAM();
	//private UtrnTableDAM utrnIO = new UtrnTableDAM(); PErformance tuning of Life transactions
	private ZhlcpfDAO zhlcpfDAO = new ZhlcpfDAOImpl(); /*BRD-34*/
	private ZhlbpfDAO zhlbpfDAO = new ZhlbpfDAOImpl();/*BRD-34*/
	protected Batckey wsaaBatckey = new Batckey();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected Atreqrec atreqrec = new Atreqrec();
	private T6617rec t6617rec = new T6617rec();
	private T5661rec t5661rec = new T5661rec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected Sftlockrec sftlockrec = new Sftlockrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5284ScreenVars sv = getLScreenVars();
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	// ILIFE-6592
	protected ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	protected ClmhpfDAO clmhpfDAO = getApplicationContext().getBean("clmhpfDAO", ClmhpfDAO.class);
	protected Clntpf cownClntpf = new Clntpf();
	protected Clntpf lifeClntpf = new Clntpf();
	protected Clmhpf claimHeader = new Clmhpf();
	// ILIFE-6592
	
	//ILIFE-1137
	//added for death claim flexibility
	private T5688rec t5688rec = new T5688rec();
	private Dthclmflxrec dthclmflxRec =  new Dthclmflxrec();
	private static final String e308 = "E308";	
	//end added for death claim flexibility		
	//BRD-34 starts
	private Zhlcpf zhlcpf = new Zhlcpf();
	private Zhlbpf zhlbpf = new Zhlbpf();
  	private List<Zhlcpf> zhlcpfList= null;
  	private List<Zhlbpf> zhlbpfList= null;
  	//BRD-34 ends

	private ClmdpfDAO clmdpfDAO = getApplicationContext().getBean("clmdpfDAO", ClmdpfDAO.class);
	protected List<Clmdpf> clmdpfList;
	private Clmdpf clmdpf = null;
	
	
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private List<Lifepf> LifepfList;
	protected Lifepf lifepf = null;
	
	private ItemDAO itempfDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	
	

	//======CLM015=========
	private ErrorsInner errorsInner = new ErrorsInner();
	private Gensswrec gensswrec = new Gensswrec();
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	boolean CMDTH006Permission  = false;
	private Sd5jlScreenVars sd5jl = ScreenProgram.getScreenVars(Sd5jlScreenVars.class);
	private CpbnfypfDAO cpbnfypfDAO = getApplicationContext().getBean("cpbnfypfDAO", CpbnfypfDAO.class);

	protected Chdrpf chdrclmIO = new Chdrpf();
	protected ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	
	
	//====CLM004========
	boolean CMOTH002Permission  = false;
	private Datcon3rec datcon3rec = new Datcon3rec();
	private PackedDecimalData wsaaDate1 = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaDate2 = new PackedDecimalData(8, 0);
	
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaIntDays = new ZonedDecimalData(5, 0);
	
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO",  FluppfDAO.class);
	private ZonedDecimalData wsaaItstdays = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaItstrate = new ZonedDecimalData(8, 5);
	private static final String clm004_FEATURE_ID = "CMOTH002";

	private boolean CMOTH003Permission  = false;
	private Fluppf fluppf = null ;
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	private Notipf notipf=null;
	private List<Clnnpf> clnnpfList1 = new ArrayList<Clnnpf>();
	private ClnnpfDAO clnnpfDAO= getApplicationContext().getBean("clnnpfDAO",ClnnpfDAO.class);
	private List<Invspf> invspfList1 = new ArrayList<Invspf>();
	private InvspfDAO invspfDAO= getApplicationContext().getBean("invspfDAO",InvspfDAO.class);
	public static final String CLMPREFIX = "CLMNTF";
	private static final String feaConfigClmNotify= "CMOTH003";
	private FixedLengthStringData wsaaNotifinum = new FixedLengthStringData(8);

	protected AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class); // ILIFE-8394
	protected Acblpf acblpf; // ILIFE-8394
	boolean CMDTH010Permission  = false;
	private static final String feaConfigPreRegistartion= "CMDTH010";
	

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		readNext2450, 
		exit2490, 
		exit1090, 
		rewrite3170, 
		validateSelectionFields2070, 
		exit3190, 
		nextUtrn5020, 
		readUtrn5030			
	}

	public P5284() {
		super();
		screenVars = sv;
		new ScreenModel("S5284", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

protected S5284ScreenVars getLScreenVars() {
	return ScreenProgram.getScreenVars(S5284ScreenVars.class);
}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}
private static final class ErrorsInner { 
	private FixedLengthStringData rrli = new FixedLengthStringData(4).init("RRLI");
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
				case exit1090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		CMDTH006Permission  = FeaConfg.isFeatureExist("2", "CMDTH006", appVars, "IT");
		CMOTH002Permission  = FeaConfg.isFeatureExist("2", clm004_FEATURE_ID, appVars, "IT"); //CLM004
		CMOTH003Permission  = FeaConfg.isFeatureExist("2", feaConfigClmNotify, appVars, "IT");
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", feaConfigPreRegistartion, appVars, "IT");
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		/*    Set wsaa-totclaim to Zeros each time this program            */
		/*    is called, also set Claim Detail status to O-K.              */
		wsaaTotclaim.set(ZERO);
		/*    Dummy field initilisation for prototype version.*/
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.dtofdeath.set(varcom.vrcmMaxDate);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.clamamt.set(ZERO);
		//ILIFE-1137
		//added for death claim flexibility
		sv.susamt.set(ZERO);
		sv.nextinsamt.set(ZERO);
		wsaaY.set(ZERO);
		//end		
		sv.intday.set(ZERO);
		sv.interest.set(ZERO);
		sv.net.set(ZERO);
		sv.ofcharge.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.zrcshamt.set(ZERO);
		sv.policyloan.set(ZERO);
		sv.proceeds.set(ZERO);
		sv.totclaim.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		chdrclmIO = chdrpfDAO.getCacheObject(chdrclmIO);
		if (chdrclmIO == null) {
			syserrrec.params.set(chdrclmIO);
			fatalError600();
		}
		sv.occdate.set(chdrclmIO.getOccdate());
		sv.chdrnum.set(chdrclmIO.getChdrnum());
		sv.cnttype.set(chdrclmIO.getCnttype());
		descIO.setDescitem(chdrclmIO.getCnttype());
		sv.paycurr.set(chdrclmIO.getCntcurr());
		wsaaCurrStore.set(sv.paycurr);
		descIO.setDesctabl(t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrclmIO.getCownnum());
		cltsIO.setClntnum(chdrclmIO.getCownnum());
		// ILIFE-6592
		cownClntpf = getClientDetails1200(chdrclmIO.getCownnum());
//		getClientDetails1200();
		// ILIFE-6592
		/* Get the confirmation name.*/
		if (cownClntpf == null
		|| !"1".equals(cownClntpf.getValidflag())) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname(cownClntpf);
			sv.ownername.set(wsspcomn.longconfname);
		}
		// ILIFE-6592
		sv.btdate.set(chdrclmIO.getBtdate());
		sv.ptdate.set(chdrclmIO.getPtdate());
		/*    Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrclmIO.getStatcode());
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		if(!CMOTH003Permission){
			sv.aacctOut[varcom.nd.toInt()].set("Y");
			sv.claimnumberOut[varcom.nd.toInt()].set("Y");
			sv.claimnotesOut[varcom.nd.toInt()].set("Y");
			sv.investresOut[varcom.nd.toInt()].set("Y");
			sv.fupflgOut[varcom.nd.toInt()].set("Y");
		}
		
		if(!CMDTH010Permission){
			sv.claimnumberOut[varcom.nd.toInt()].set("Y");	
		}
		
		
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrclmIO.getPstcde());
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/*  Read the Claims Header record (CLMHCLM)*/ //TODO
		// ILIFE-6592
		claimHeader = clmhpfDAO.getClmhpfH(chdrclmIO.getChdrcoy().toString(), chdrclmIO.getChdrnum());
		findClmHeaderbyValidLif();
		if (claimHeader == null) {
			clmhclmIO.setChdrcoy(chdrclmIO.getChdrcoy());
			clmhclmIO.setChdrnum(chdrclmIO.getChdrnum());
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
		/*clmhclmIO.setDataArea(SPACES);
		clmhclmIO.setChdrcoy(chdrclmIO.getChdrcoy());
		clmhclmIO.setChdrnum(chdrclmIO.getChdrnum());
		clmhclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}*/
		/*   output claim header details*/
		sv.dtofdeath.set(claimHeader.getDtofdeath());
		sv.effdate.set(claimHeader.getEffdate());
		sv.causeofdth.set(claimHeader.getCauseofdth());
		sv.reasoncd.set(claimHeader.getReasoncd());
		sv.policyloan.set(claimHeader.getPolicyloan());
		sv.zrcshamt.set(claimHeader.getZrcshamt());
		sv.otheradjst.set(claimHeader.getOtheradjst());
		sv.currcd.set(claimHeader.getCurrcd());
		sv.resndesc.set(claimHeader.getResndesc());
		
		clmdpf = new Clmdpf();
		clmdpf.setChdrcoy(claimHeader.getChdrcoy());
		clmdpf.setChdrnum(claimHeader.getChdrnum());
		// ILIFE-6592
		clmdpfList = clmdpfDAO.selectClmdpfRecord(clmdpf);
		getClmdpfByValidLifeStat();
		if(clmdpfList != null && clmdpfList.size() > 0)
		{
			if(CMOTH003Permission){
				sv.claimnumber.set(clmdpfList.get(0).getClaimno());
				if(isNE(clmdpfList.get(0).getClaimnotifino(),SPACES))
					sv.aacct.set(CLMPREFIX+clmdpfList.get(0).getClaimnotifino());
			}
			else if(CMDTH010Permission){
				if(sv.claimnumberOut[varcom.nd.toInt()].equals("Y")){
					sv.claimnumberOut[varcom.nd.toInt()].set("N");
				}
			sv.claimnumber.set(clmdpfList.get(0).getClaimno());
			}
			for(Clmdpf clmd : clmdpfList)
			{
				wsaaTotclaim.add(clmd.getActvalue());
			}
		}
		
		//clmdclmIO.setDataArea(SPACES);
		//clmdclmIO.setChdrcoy(chdrclmIO.getChdrcoy());
	//	clmdclmIO.setChdrnum(chdrclmIO.getChdrnum());
		//clmdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		//clmdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//clmdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		//while ( !(isEQ(clmdclmIO.getStatuz(),varcom.endp))) {
			//readClaimDetails1300();
		//}
		
		sv.totclaim.set(wsaaTotclaim);
		zrdecplrec.amountIn.set(sv.totclaim);
		if (isEQ(sv.paycurr, SPACES)) {
			zrdecplrec.currency.set(sv.currcd);
		}
		else {
			zrdecplrec.currency.set(sv.paycurr);
		}
		callRounding7000();
		sv.totclaim.set(zrdecplrec.amountOut);
		/* COMPUTE S5284-CLAMAMT       = S5284-TOTCLAIM +               */
		/*     CLMHCLM-POLICYLOAN      + S5284-OTHERADJST.              */
		/* COMPUTE S5284-CLAMAMT       = S5284-TOTCLAIM -       <LA2108>*/
		//ILIFE-1137
		readTabT5688250();
		if (isNE(t5688rec.dflexmeth,SPACES)){
			dthclmflxRec.chdrChdrcoy.set(chdrclmIO.getChdrcoy());
			dthclmflxRec.chdrChdrnum.set(chdrclmIO.getChdrnum());			
			dthclmflxRec.ptdate.set(sv.ptdate);
			if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
				dthclmflxRec.dtofdeath.set(sv.dtofdeath);
			}else{
				datcon1rec.function.set(varcom.tday);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				wsaaToday.set(datcon1rec.intDate);				
				dthclmflxRec.dtofdeath.set(wsaaToday);
			}			
			dthclmflxRec.btdate.set(sv.btdate);
			dthclmflxRec.cnttype.set(sv.cnttype);
			dthclmflxRec.register.set(chdrclmIO.getReg());
			dthclmflxRec.cntcurr.set(chdrclmIO.getCntcurr());
			dthclmflxRec.effdate.set(chdrclmIO.getCurrfrom());
			callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
			if (isNE(dthclmflxRec.status, varcom.oK)) {
				syserrrec.statuz.set(dthclmflxRec.status);
				syserrrec.params.set(dthclmflxRec.dthclmflxrec);
				fatalError600();
			}	
			sv.susamt.set(dthclmflxRec.susamt);
			sv.nextinsamt.set(dthclmflxRec.nextinsamt);
		}

		///ILIFE-8394 starts
		else{
			   acblpf = acblDao.getAcblpfRecord(chdrclmIO.getChdrcoy().toString(), "LP", chdrclmIO.getChdrnum(), 
	                   "S", null);
	                   if(acblpf!=null){
	                          BigDecimal suspenseamt = acblpf.getSacscurbal();
	                          sv.susamt.set(suspenseamt);             
	                   }
		}
		//ILIFE-8394 ends
		if(CMOTH003Permission){
			checkFollowups1850();
		}

		retrieveSuspenseCustomerSpecifc();
		//modified for death claim flexibility
		compute(sv.clamamt, 2).set(sub(add(add(add(sv.totclaim, claimHeader.getPolicyloan()), sv.otheradjst), claimHeader.getZrcshamt()), sv.susamt,sv.nextinsamt));
		customerSpecificPensionCalc();
	//	CalcCustomerSpecifc();
		sv.susamt.set(mult(sv.susamt,-1));
		//compute(sv.clamamt, 2).set(add(add(add(sv.totclaim, clmhclmIO.getPolicyloan()), sv.otheradjst), clmhclmIO.getZrcshamt()));
		zrdecplrec.amountIn.set(sv.clamamt);
		if (isEQ(sv.paycurr, SPACES)) {
			zrdecplrec.currency.set(sv.currcd);
		}
		else {
			zrdecplrec.currency.set(sv.paycurr);
		}
		callRounding7000();
		sv.clamamt.set(zrdecplrec.amountOut);
		readT6617();
		
		 if (CMOTH002Permission) { // ICIL-1069
				sv.itstrateOut[varcom.nd.toInt()].set("N");
				sv.itstrate.set(t6617rec.intRate);	 
	        } else {
				sv.itstrateOut[varcom.nd.toInt()].set("Y");
	        }
		//BRD-34 starts
		//Get Hold Claim Adjustment
				wsaaHdlAdj.set(ZERO);
				wsaaHdlAmt.set(ZERO);
				CalCovAmt1400();
				CalBeneAmt1500();
				sv.zhldclmv.set(wsaaHdlAmt.toDouble()+wsaaHdlAdj.toDouble());
				sv.zhldclma.set(wsaaHdlAdj);
				compute(wsaaNet,2).set(add(sub(sv.totclaim,wsaaHdlAmt),sub(sv.otheradjst,sv.zhldclma)));		
				sv.net.set(wsaaNet);
					
		//BRD-34 Ends		
	//	sv.net.set(sv.clamamt);
		checkIncompleteUtrn5000();
		if (isEQ(claimHeader.getJlife(), "01")) {
			sv.astrsk.set("*");
		}
		else {
			sv.asterisk.set("*");
		}
		/*  Read the Life file  (LIFECLM)*/
		/*lifeclmIO.setChdrnum(clmhclmIO.getChdrnum());
		lifeclmIO.setChdrcoy(clmhclmIO.getChdrcoy());
		lifeclmIO.setLife(clmhclmIO.getLife());
		lifeclmIO.setJlife("00");
		lifeclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		lifeclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, lifeclmIO);
		if (isNE(lifeclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company, lifeclmIO.getChdrcoy())
		|| isNE(chdrclmIO.getChdrnum(), lifeclmIO.getChdrnum())
		|| isNE(lifeclmIO.getJlife(), "00")
		&& isNE(lifeclmIO.getJlife(), SPACES)
		|| isEQ(lifeclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		/* Move the last record read to the screen and get the*/
		/* description.*/
		lifepf = new Lifepf(); 
		lifepf.setChdrnum(claimHeader.getChdrnum());
		lifepf.setChdrcoy(claimHeader.getChdrcoy());
		lifepf.setLife(claimHeader.getLife());
		//lifepf.setJlife("00");
		lifepf.setValidflag("1");
		
		LifepfList = lifepfDAO.findByKey(lifepf);
		if(LifepfList!= null && LifepfList.size() > 0)
		{	
			for(Lifepf life : LifepfList)
			{
				if(isEQ(life.getJlife(), "00")) 
				{
					sv.lifcnum.set(life.getLifcnum());
				}
				else if(isEQ(life.getJlife(), "01"))
				{
					sv.jlifcnum.set(life.getLifcnum());
					lifeClntpf = getClientDetails1200(life.getLifcnum());
					setJointName(lifeClntpf);
					lifepf = life;
					continue;
				}
//					cltsIO.setClntnum(life.getLifcnum());
					lifeClntpf = getClientDetails1200(life.getLifcnum());
					/* Get the confirmation name.*/
					if (lifeClntpf == null
							|| !"1".equals(lifeClntpf.getValidflag())) {
						sv.linsnameErr.set(e304);
						sv.linsname.set(SPACES);
					}
					else {
						plainname(lifeClntpf);
						sv.linsname.set(wsspcomn.longconfname);
					}
				//}
					lifepf = life;
			}
		}
		
		if(CMOTH002Permission){   // ICIL-1069
			processInterestDays();
		}
	}



protected void setJointName(Clntpf lifeClntpf)
{
	if (lifeClntpf == null
			|| !"1".equals(lifeClntpf.getValidflag())) {
		sv.jlinsnameErr.set(e304);
		sv.jlinsname.set(SPACES);
	}
	else {
		plainname(lifeClntpf);
		sv.jlinsname.set(wsspcomn.longconfname);
	}		
}
//BRD-34 starts
protected void  CalCovAmt1400() {
	/*ILIFE_3135 starts*/
	
	zhlcpf.setChdrcoy(chdrclmIO.getChdrcoy().toString());
	zhlcpf.setChdrnum(chdrclmIO.getChdrnum());
	/*ILIFE-3125 starts*/
	zhlcpf.setValidflag("1");
	zhlcpfList=zhlcpfDAO.readZhlcpfData(zhlcpf);
	 
	
	if (zhlcpfList==null || zhlcpfList.size()<=0) {
		
			
			return ;
		}

	/*Iterator<Zhlcpf> lsIterator =zhlcpfList.iterator();
	while(lsIterator.hasNext())
	{
		wsaaHdlAdj.add(zhlcpf.getZhldclma().doubleValue());
		wsaaHdlAmt.add(zhlcpf.getZhldclmv().doubleValue());
		
		lsIterator.next();
	
	}*/
	for(Zhlcpf zhlcpf: zhlcpfList)
		{
			wsaaHdlAdj.add(zhlcpf.getZhldclma().doubleValue());
			wsaaHdlAmt.add(zhlcpf.getZhldclmv().doubleValue());
		}
	/*ILIFE-3125 ends*/
}

protected void  CalBeneAmt1500() {

	zhlbpf.setChdrcoy(chdrclmIO.getChdrcoy().toString());
	zhlbpf.setChdrnum(chdrclmIO.getChdrnum());
	zhlbpf.setValidflag("1");
	zhlbpfList = zhlbpfDAO.readZhlbpfData(zhlbpf);
	
	if (zhlbpfList==null || zhlbpfList.size()<=0) {
		syserrrec.statuz.set(Varcom.mrnf);
			return ;
		}
	
	for (Zhlbpf zhlbpf:zhlbpfList)
		{
		wsaaHdlAdj.add(zhlbpf.getZhldclma().doubleValue());
		wsaaHdlAmt.add(zhlbpf.getZhldclmv().doubleValue());
		}
	/*Iterator<Zhlbpf> lsIterator =zhlbpfList.iterator();
	while(lsIterator.hasNext())
	{
		wsaaHdlAdj.add(zhlbpf.getZhldclma().doubleValue());
		wsaaHdlAmt.add(zhlbpf.getZhldclmv().doubleValue());
		
		lsIterator.next();
		
	}*/
} /*ILIFE_3135 ends*/

//BRD-34 Ends

protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
//ILIFE-6592
	private Clntpf getClientDetails1200(String clientNum) {
		Clntpf clntpf = new Clntpf();
		clntpf.setClntnum(clientNum);
		clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), clientNum);
		return clntpf;
	}
	
protected void getClientDetails1200()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO); // TODO
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
//ILIFE-6592
/*
protected void readClaimDetails1300()
	{
		/*GO*//*
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.oK)
		&& isNE(clmdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		if (isNE(chdrclmIO.getChdrcoy(), clmdclmIO.getChdrcoy())
		|| isNE(chdrclmIO.getChdrnum(), clmdclmIO.getChdrnum())
		|| isEQ(clmdclmIO.getStatuz(), varcom.endp)) {
			clmdclmIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaTotclaim.add(clmdclmIO.getActvalue());
		clmdclmIO.setFunction(varcom.nextr);
		/*EXIT*//*
	}
/*
protected void largename()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}*/
//ILIFE-6592
protected void plainname(Clntpf clntpf) {
	/* PLAIN-100 */
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(), "C")) {
		corpname(clntpf);

	} else if (isNE(clntpf.getGivname(), SPACES)) {
		String firstName = clntpf.getGivname();
		String lastName = clntpf.getSurname();
		String delimiter = ",";

		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);

	} else {
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}
	/* PLAIN-EXIT */
}
//ILIFE-6592
/*
protected void payeename()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}
	*/

protected void corpname(Clntpf clntpf) {
	/* PAYEE-1001 */
	wsspcomn.longconfname.set(SPACES);
	/* STRING CLTS-SURNAME DELIMITED SIZE */
	/* CLTS-GIVNAME DELIMITED ' ' */
	String firstName = clntpf.getLgivname();
	String lastName = clntpf.getLsurname();
	String delimiter = "";

	// this way we can override StringUtil behaviour in formatName()
	String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
	wsspcomn.longconfname.set(fullName);
	/* CORP-EXIT */
}
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (CMDTH006Permission) {
			sv.bnfyingOut[varcom.nd.toInt()].set("N");
		} else {
			sv.bnfyingOut[varcom.nd.toInt()].set("Y");
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsaaIncompleteUtrn, "Y")) {
			sv.chdrnumErr.set(g892);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			checkForErrors2080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateSelectionFields2070()
{
	if (isEQ(sv.bnfying, "X")) {
		wsspcomn.confirmationKey.set(SPACES);
	}
	if(CMOTH003Permission){
	if (isEQ(sv.fupflg, "X")) {
		wsspcomn.confirmationKey.set(SPACES);
	}
	if (isEQ(sv.investres, "X")) {
		wsspcomn.confirmationKey.set(SPACES);
	}
	if (isEQ(sv.claimnotes, "X")) {
		wsspcomn.confirmationKey.set(SPACES);
	}
	}
	if (isEQ(wsspcomn.confirmationKey, "N")) {
		sv.confirmErr.set(errorsInner.rrli);
	}
}
protected void screenIo2010()
	{
		/*    CALL 'S5284IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5284-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		validateSelectionFields2070();
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if(CMOTH003Permission){
			if(isNE(sv.aacct,SPACES)){
				wsaaNotifinum.set(sv.aacct.toString().replace(CLMPREFIX, ""));
			}
			else{
				wsaaNotifinum.set(SPACES);
			}
			clnnpfList1 =  clnnpfDAO.getClnnpfList(wsspcomn.company.toString(), wsaaNotifinum.toString(),sv.claimnumber.toString().trim());
			invspfList1 =  invspfDAO.getInvspfList(wsspcomn.company.toString(), wsaaNotifinum.toString(),sv.claimnumber.toString().trim());			
		}
		if (isEQ(wsaaIncompleteUtrn, "Y")) {
			sv.chdrnumErr.set(g892);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* If currency is blank, display an error                          */
		if (isEQ(sv.paycurr, SPACES)) {
			sv.paycurrErr.set(h960);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*   check that the office charge is not greater than the net*/
		customerSpecificClmAppr2010(); 									//MLI-1315
		/* If the payment currency has changed then the net amount and     */
		/* office charges will have to be converted into the payment       */
		/* currency                                                        */
		if (isNE(sv.paycurr, wsaaCurrStore)) {
			wsaaCurrIn.set(wsaaCurrStore);
			wsaaCurrStore.set(sv.paycurr);
			wsaaAmount.set(sv.net);
			convertCurrency6000();
			sv.net.set(conlinkrec.amountOut);
			wsaaAmount.set(sv.ofcharge);
			convertCurrency6000();
			sv.ofcharge.set(conlinkrec.amountOut);
		}
		if (isNE(sv.ofcharge, ZERO)) {
			zrdecplrec.amountIn.set(sv.ofcharge);
			zrdecplrec.currency.set(sv.paycurr);
			callRounding7000();
			if (isNE(zrdecplrec.amountOut, sv.ofcharge)) {
				sv.ofchargeErr.set(rfik);
			}
		}
		CalcCustomerSpecifc();
		/* IF S5284-INTDAY > 0                                          */
		processInterest2100();
		compute(sv.proceeds, 2).set(sub(add(sv.net, sv.interest), sv.ofcharge));
		checkFollowups2200();
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void customerSpecificClmAppr2010() {
	if (isGT(sv.ofcharge, sv.net)) {
		/*      MOVE U042       TO S5284-OFCHARGE-ERR                    */
		sv.ofchargeErr.set(h074);
		wsspcomn.edterror.set("Y");
	}
}


protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

public  Integer getCurrentBusinessDate() {
	final Datcon1rec datcon1rec = new Datcon1rec();
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	return datcon1rec.intDate.toInt();
   }


protected void processInterestDays()
{
	int days1 = 0;
	days1 = DateUtils.calDays(claimHeader.getEffdate().toString(),getCurrentBusinessDate().toString());
	
	List<Fluppf> flupList = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(),
			claimHeader.getChdrnum());
	int days2 = 0;
	if(!flupList.isEmpty()) {
		List<String> crtdateList = new ArrayList<String>();
		List<String> rcvdateList = new ArrayList<String>();
		for (Fluppf pf : flupList) {
			if(isNE(pf.getFuprcvd(),99999999) && isNE(pf.getCrtDate(),99999999)){
				crtdateList.add(String.valueOf(pf.getCrtDate()));
				rcvdateList.add(String.valueOf(pf.getFuprcvd()));
			}
			
		}
		days2 = DateUtils.calDays(DateUtils.getMaxOrMinDate(crtdateList, "min"),
				DateUtils.getMaxOrMinDate(rcvdateList, "max"));
	}
	int result = days1 - days2 - t6617rec.defInterestDay.toInt();
	sv.intday.set(result < 0 ? 0 : result);
}

protected void readT6617(){
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
	itempf.setItemtabl(t6617);
	itempf.setItmfrm(varcom.vrcmMaxDate.getbigdata());
	itempf.setItmto(varcom.vrcmMaxDate.getbigdata());

	itempfList = itempfDAO.findByItemDates(itempf);
	if(itempfList.size() > 0) {
	for (Itempf it : itempfList) {				
		t6617rec.t6617Rec.set(StringUtil.rawToString(it.getGenarea()));
		
	}
}
}


protected void processInterest2100()
	{
		readTable2110();
	}

protected void readTable2110()
	{
		/*  read table T6617 to obtain the interest rate*/
		/*	itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6617);
		itdmIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t6617)
		|| isNE(itdmIO.getItemitem(), wsaaBatckey.batcBatctrcde)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*     MOVE U045                TO SCRN-ERROR-CODE               
			scrnparams.errorCode.set(h077);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t6617rec.t6617Rec.set(itdmIO.getGenarea());

		}*/
		
		compute(wsaaWholeYear, 0).setDivide(sv.intday, (365));	
		wsaaRemainder.setRemainder(wsaaWholeYear);
		
		if(CMOTH002Permission ){
			compute(wsaaCalc,8).set(add(1,(div(mult((div(sv.itstrate, 100)), wsaaRemainder), 365))));
			compute(sv.interest, 5).setRounded(sub((mult(mult(power((add(1, (div(sv.itstrate, 100)))), wsaaWholeYear), wsaaCalc), sv.net)), sv.net));
		}
		else{
			compute(sv.interest, 5).set(sub((mult(mult(power((add(1, (div(t6617rec.intRate, 100)))), wsaaWholeYear), (add(1, (div(mult((div(t6617rec.intRate, 100)), wsaaRemainder), 365))))), sv.net)), sv.net));

		}
		zrdecplrec.amountIn.set(sv.interest);
		zrdecplrec.currency.set(sv.paycurr);
		callRounding7000();
		sv.interest.set(zrdecplrec.amountOut);
		/* If the currency has changed then convert any interest calculated*/
		/* into the payment currency                                       */
		/*IF S5284-PAYCURR            NOT = WSAA-CURR-STORE       <007>*/
		/*MOVE S5284-NET           TO WSAA-AMOUNT              <007>*/
		if (isNE(wsaaCurrIn, SPACES)) {
			if (isNE(sv.paycurr, wsaaCurrIn)) {
				wsaaAmount.set(sv.interest);
				convertCurrency6000();
				sv.interest.set(conlinkrec.amountOut);
			}
		}
	}

protected void checkFollowups2200()
	{
		read2205();
	}

protected void read2205()
	{
		wsaaOutstanflu.set(SPACES);
		/**flupclmIO.setDataKey(SPACES);
		flupclmIO.setChdrcoy(claimHeader.getChdrcoy());
		flupclmIO.setChdrnum(claimHeader.getChdrnum());
		flupclmIO.setFupno(ZERO);
		flupclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		flupclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		flupclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
 
		SmartFileCode.execute(appVars, flupclmIO);
		if ((isNE(flupclmIO.getStatuz(), varcom.oK))
		&& (isNE(flupclmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		if ((isNE(flupclmIO.getChdrcoy(), clmhclmIO.getChdrcoy()))
		|| (isNE(flupclmIO.getChdrnum(), clmhclmIO.getChdrnum()))
		|| (isEQ(flupclmIO.getStatuz(), varcom.endp))) {
			return ;
		}*/
		/*  But are any of them outstanding?*/
		
		fluppf = new Fluppf();
		fluppf.setChdrcoy(claimHeader.getChdrcoy().toCharArray()[0]);
		fluppf.setChdrnum(claimHeader.getChdrnum());
		fluppf.setFupTyp('C');
		List<Fluppf> data = fluppfDAO.getFluppfRecordByChdrnumAndTyp(fluppf);
		Iterator<Fluppf> fluppfIT = data.iterator();
		while (fluppfIT.hasNext()) {
			fluppf = fluppfIT.next();
			checkThem2400();
			if(isEQ(wsaaOutstanflu, "Y"))
			{
				break;
			}
		}
		
		if (isEQ(wsaaOutstanflu, "Y")) {
			/*     MOVE U025                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(h017);
			wsspcomn.edterror.set("Y");
		}
	}

protected void readFollowupTab2300()
	{
		go2301();
	}

protected void go2301()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(clmhclmIO.getChdrcoy());
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5661);
		/*    MOVE FLUPCLM-FUPCODE        TO ITEM-ITEMITEM.                */
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(fluppf.getFupCde());
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaOutstanflu.set("N");
		}
		else {
			wsaaOutstanflu.set(" ");
			t5661rec.t5661Rec.set(itemIO.getGenarea());
		}
	}

protected void checkThem2400()
	{
		/*   Check for any follow-ups.*/
		readFollowupTab2300();
		if (isEQ(wsaaOutstanflu, "N")) {
			return;
		}
		/*  Assume the current status will not be found,*/
		/*     therefore, the follow-up is outstanding.*/
		wsaaOutstanflu.set("Y");
		for (wsaaX.set(1); !(isGT(wsaaX, 10)
		|| isEQ(wsaaOutstanflu, "N")); wsaaX.add(1)){
			outstandingCheck2500();
		}
		/*  If a match was not found, this is all we need to know.*/
		if (isEQ(wsaaOutstanflu, "Y")
		|| isGT(wsaaX, 10)) {
			wsaaOutstanflu.set("Y");
		}
	
	}

	
	/**
	* <pre>
	*  If any one of the codes matches, it is not outstanding.
	* </pre>
	*/
protected void outstandingCheck2500()
	{
		/*GO*/
		if (isEQ(fluppf.getFupSts(), t5661rec.fuposs[wsaaX.toInt()])) {
			wsaaOutstanflu.set("N");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/* if returning from an optional selection skip this section.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		
		if (isEQ(sv.bnfying, "X")) {
			return ;
		}
		if(CMOTH003Permission){
			if (isEQ(sv.fupflg, "X")) {
				return ;
			}
			if (isEQ(sv.investres, "X")) {
				return ;
			}
			if (isEQ(sv.claimnotes, "X")) {
				return ;
			}	
		}
		// update client status
		if (lifeClntpf != null) {
			lifeClntpf.setClntpfx("CN");
			lifeClntpf.setClntcoy(wsspcomn.fsuco.toString());
			lifeClntpf.setClntnum(lifepf.getLifcnum());
			lifeClntpf.setCltstat("DC");		
			clntpfDAO.updateCltStateUniqueNumber(lifeClntpf);
		} else {
			cownClntpf.setClntpfx("CN");
			cownClntpf.setClntcoy(wsspcomn.fsuco.toString());
			cownClntpf.setClntnum(lifepf.getLifcnum());
			cownClntpf.setCltstat("DC");
			clntpfDAO.updateCltStateUniqueNumber(cownClntpf);		
		}
		
		/*
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifepf.getLifcnum());
		cltsIO.setCltstat("DC");
		cltsIO.setFunction(varcom.rewrt);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}*/
		
		/*  Update  the Claims Header record (CLMHCLM)*/
		// ILIFE-6592
		/* Firstly, update the existing CLMHCLM rec with a VALIDFLAG='2'.  */
//		clmhclmIO.setValidflag("2");//TODO
		/*     MOVE S5284-INTDAY           TO CLMHCLM-INTDAYS.             */
		/*     MOVE S5284-INTEREST         TO CLMHCLM-INTEREST.            */
		/*     MOVE S5284-OFCHARGE         TO CLMHCLM-OFCHARGE.            */
		/*     MOVE S5284-PAYCURR          TO CLMHCLM-CURRCD.      <LA4544>*/
//		clmhclmIO.setFunction(varcom.updat);
//		clmhclmIO.setFormat(clmhclmrec);
//		SmartFileCode.execute(appVars, clmhclmIO);
//		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
//			syserrrec.params.set(clmhclmIO.getParams());
//			fatalError600();
//		}
		clmhpfDAO.updateClmhpfValidFlag(claimHeader.getUniqueNumber(), "2");
		
		/* Now, we need to create the new VALIDFLAG = '1' record.          */
		claimHeader.setValidflag("1");
		claimHeader.setDtofdeath(sv.dtofdeath.toInt());
		claimHeader.setEffdate(sv.effdate.toInt());
		claimHeader.setTranno(chdrclmIO.getTranno());
		claimHeader.setReasoncd(sv.reasoncd.toString());
		claimHeader.setResndesc(sv.resndesc.toString());
		claimHeader.setPolicyloan(sv.policyloan.getbigdata());
		claimHeader.setCauseofdth(sv.causeofdth.toString());
		claimHeader.setOtheradjst(sv.otheradjst.getbigdata());
		claimHeader.setZrcshamt(sv.zrcshamt.getbigdata());
		claimHeader.setIntdays(sv.intday.toInt());
		claimHeader.setInterest(sv.interest.getbigdata());
		claimHeader.setOfcharge(sv.ofcharge.getbigdata());
		claimHeader.setCurrcd(sv.paycurr.toString());
		claimHeader.setDteappr(wsaaToday.toInt());
		claimHeader.setTrdt(wsaaToday.toInt());
		claimHeader.setTrtm(varcom.vrcmTime.toInt());
		if(CMOTH002Permission)
		{	
		claimHeader.setInterestrate(sv.itstrate.toInt());
		}
		clmhpfDAO.insert(claimHeader);
		updateCustomerSpecifc();
		/*clmhclmIO.setFunction(varcom.writr);
		clmhclmIO.setFormat(clmhclmrec);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}*/
		clmdpf = new Clmdpf();
		clmdpf.setChdrcoy(claimHeader.getChdrcoy());
		clmdpf.setChdrnum(claimHeader.getChdrnum());		
		clmdpfList = clmdpfDAO.selectClmdpfRecord(clmdpf);
		/**
		clmdclmIO.setParams(SPACES);
		clmdclmIO.setChdrcoy(claimHeader.getChdrcoy());
		clmdclmIO.setChdrnum(claimHeader.getChdrnum());
		clmdclmIO.setFunction(varcom.begnh);
		while ( !(isEQ(clmdclmIO.getStatuz(), varcom.endp))) {
			updtClaimDetails3100();
		}*/
		Iterator<Clmdpf> clmdpfIt = clmdpfList.iterator();
		while (clmdpfIt.hasNext()) {
			clmdpf = clmdpfIt.next();
			updtClaimDetails3100();
		}
		// ILIFE-6592
		/*  transfer the contract soft lock to AT*/
		sftlockrec.function.set("TOAT");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrclmIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*  call the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5284AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		/* MOVE VRCM-TERM            TO  ATRT-REQ-TERM.                 */
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(datcon1rec.intDate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrclmIO.getChdrnum());
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		/* MOVE VRCM-TERM            TO  WSAA-TERMID.                   */
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaCntcurr.set(chdrclmIO.getCntcurr());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		atreqrec.transArea.set(wsaaTransactionRec);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
		if(CMOTH003Permission){
			if(isNE(wsaaNotifinum,SPACES) && isNE(wsspcomn.flag,"I")){
				clnnpfDAO.updateClnnpfClaimno(sv.claimnumber.toString(),wsaaNotifinum.toString());
			}
			if(isNE(wsaaNotifinum,SPACES) && isNE(wsspcomn.flag,"I")){
				invspfDAO.updateInvspfClaimno(sv.claimnumber.toString(),wsaaNotifinum.toString());
			}
		}
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	* </pre>
	*/
protected void updtClaimDetails3100()
	{
/**		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: */
					start3100();
/**				case rewrite3170: 
					rewrite3170();
				case exit3190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}*/
	}

protected void start3100()
	{
		/**SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.oK)
		&& isNE(clmdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		if (isNE(claimHeader.getChdrcoy(), clmdclmIO.getChdrcoy())//TODO
		|| isNE(claimHeader.getChdrnum(), clmdclmIO.getChdrnum())
		|| isEQ(clmdclmIO.getStatuz(), varcom.endp)) {
			if (isNE(clmdclmIO.getStatuz(), varcom.endp)) {
				clmdclmIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, clmdclmIO);
				if (isNE(clmdclmIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(clmdclmIO.getParams());
					fatalError600();
				}
			}
			clmdclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3190);
		}*/
		writeHdcd3200();
		if (isEQ(sv.paycurr, chdrclmIO.getCntcurr())) {
			/**goTo(GotoLabel.rewrite3170);*/
			return;
		}
		wsaaCurrIn.set(chdrclmIO.getCntcurr());
		clmdpf.setCnstcur(sv.paycurr.toString());
		wsaaAmount.set(clmdpf.getActvalue());
		convertCurrency6000();
		clmdpf.setActvalue(conlinkrec.amountOut.toDouble());
		clmdpfDAO.updateCnstCurActValue(clmdpf);
	}

protected void writeHdcd3200()
	{
		start3200();
	}

protected void start3200()
	{
		/* For every CLMDCLM written, write a HDCD                         */
		/*                                                         <LA2108>*/
		hdcdIO.setDataArea(SPACES);
		hdcdIO.setChdrcoy(clmdpf.getChdrcoy());
		hdcdIO.setChdrnum(clmdpf.getChdrnum());
		hdcdIO.setLife(clmdpf.getLife());
		hdcdIO.setCoverage(clmdpf.getCoverage());
		hdcdIO.setRider(clmdpf.getRider());
		hdcdIO.setCrtable(clmdpf.getCrtable());
		hdcdIO.setJlife(clmdpf.getJlife());
		hdcdIO.setTranno(clmdpf.getTranno());
		hdcdIO.setHactval(clmdpf.getActvalue());
		hdcdIO.setHemv(clmdpf.getEstMatValue());
		hdcdIO.setHcnstcur(clmdpf.getCnstcur());
		hdcdIO.setFieldType(clmdpf.getFieldType());
		hdcdIO.setFormat(hdcdrec);
		hdcdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdcdIO);
		if (isNE(hdcdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdcdIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
		/*NEXT-PROGRAM
		wsspcomn.programPtr.add(1);
		EXIT*/
	}


protected void nextProgram4010()
{
	gensswrec.company.set(wsspcomn.company);
	gensswrec.progIn.set(wsaaProg);
	gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
	wsspcomn.nextprog.set(wsaaProg);
	/*  If first time into this section (stack action blank)*/
	/*  save next eight programs in stack*/
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			saveProgram4100();
		}
	}

	/*   Check if claim payees selected previously*/
	if (isEQ(sv.bnfying, "?")) {
		checkBeneficiaries3800();
	}

	if (isEQ(sv.bnfying, "X")) {
		sd5jl.totalAmount.set(wsaaNet);
		gensswrec.function.set("A");
		sv.bnfying.set("?");
		callGenssw4300();
		return;
	}
	if(CMOTH003Permission){
		if (isEQ(sv.fupflg, "?")) {
			checkFollowups1850();
		}
		if (isEQ(sv.fupflg, "X")) {
			gensswrec.function.set("B");
			sv.fupflg.set("?");
			callGenssw4300();
			return ;
		}
	    if (isEQ(sv.investres, "?")) {
	    	checkInvspf();
		}
		if (isEQ(sv.investres, "X")) {
			gensswrec.function.set("C");
			sv.investres.set("?");
			setupNotipf();
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.claimnotes, "?")) {
			checkClnnpf();
		}
		if (isEQ(sv.claimnotes, "X")) {
			gensswrec.function.set("D");
			sv.claimnotes.set("?");
			setupNotipf();
			callGenssw4300();
			return ;
		}
		}
	/*   No more selected (or none)*/
	/*      - restore stack form wsaa to wssp*/
	wsaaX.set(wsspcomn.programPtr);
	wsaaY.set(1);
	for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
		restoreProgram4200();
	}
	/*  If current stack action is * then re-display screen*/
	/*     (in this case, some other option(s) were requested)*/
	/*  Otherwise continue as normal.*/
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.nextprog.set(scrnparams.scrname);
	}
	else {
		wsspcomn.programPtr.add(1);
	}
}

protected void checkFollowups1850()
{
	String chdrnum = " ";
	String chdrcoy = " ";
	if(chdrclmIO.getChdrnum()!= null)
	{
		chdrnum = chdrclmIO.getChdrnum();
	}
	if(chdrclmIO.getChdrcoy()!= null)
	{
		chdrcoy = chdrclmIO.getChdrcoy().toString();
	}
	boolean recFound = fluppfDAO.checkFluppfRecordByChdrnum(chdrcoy, chdrnum);
	if (!recFound) {
		//fatalError600();
		sv.fupflg.set(SPACES);
	} else {
		sv.fupflg.set("+");
	}
}

protected void setupNotipf(){
	wsspcomn.chdrCownnum.set(sv.lifcnum.toString());	
	wsspcomn.wsaaclaimno.set(sv.claimnumber.toString().trim());
	if(isEQ(sv.aacct,SPACES)){
		wsspcomn.wsaarelationship.set(SPACES);
		wsspcomn.wsaaclaimant.set(sv.lifcnum.toString());
		wsspcomn.wsaanotificationNum.set(SPACES);
	}
	else{
		notipf = notipfDAO.getNotiReByNotifin(wsaaNotifinum.toString(),wsspcomn.company.toString());
		wsspcomn.wsaarelationship.set(notipf.getRelationcnum());
		wsspcomn.wsaaclaimant.set(notipf.getClaimant());
		wsspcomn.wsaanotificationNum.set(sv.aacct.toString());
	}
}

protected void checkClnnpf(){
	
	if(clnnpfList1!=null && !clnnpfList1.isEmpty()){
		sv.claimnotes.set("+");		
	}
	else{
		sv.claimnotes.set(SPACES);
	}
}

protected void checkInvspf(){
	
	if(invspfList1!=null && !invspfList1.isEmpty()){
		sv.investres.set("+");
	}
	else{
		sv.investres.set(SPACES);
	}
}
protected void restoreProgram4200()
{
	/*RESTORE*/
	wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
}
	protected void checkBeneficiaries3800()
	{
		List<Cpbnfypf> cpbnfypfList = cpbnfypfDAO.searchCpbnfypfRecord(wsspcomn.company.toString(),chdrclmIO.getChdrnum());
		if (cpbnfypfList!=null && !cpbnfypfList.isEmpty()) {
			if ((isNE(cpbnfypfList.get(0).getChdrcoy(), chdrclmIO.getChdrcoy()))
					|| (isNE(cpbnfypfList.get(0).getChdrnum(), chdrclmIO.getChdrnum()))) {
				sv.bnfying.set(SPACES);
			} else {
				sv.bnfying.set("+");
			}
		}
	}
protected void saveProgram4100()
{
	/*SAVE*/
	wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
}

protected void callGenssw4300()
{
	/*CALL-SUBROUTINE*/
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	/*    load from gensw to wssp*/
	compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
	wsaaY.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		loadProgram4400();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
	/*EXIT*/
}

protected void loadProgram4400()
{
	/*RESTORE1*/
	wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT1*/
}
//ILIFe-4559 : remove the costly query on UTRNPF from P5284
//Removing goto and begn nextr
protected void checkIncompleteUtrn5000(){

	/* Make sure there is no unprocessed UTRNs.                        */
	/* If there is, display message and disallow approval.             */
	//get the count of Utrnpf entry with feedback indicator as Y. If count is more than 0, 
	//disallow approval.
	UtrnpfDAO utrnPfDAO =  getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	int unprocessedCnt = utrnPfDAO.getUnprocessedUtrnCount(chdrclmIO.getChdrcoy().toString(),chdrclmIO.getChdrnum());

	wsaaIncompleteUtrn.set("N");
	if(unprocessedCnt > 0){
		wsaaIncompleteUtrn.set("Y");
	}
}


protected void checkIncompleteHitr5100()
	{
		hitr5110();
	}

protected void hitr5110()
	{
		/*    Check that the contract has no unprocessed HITRs pending.    */
		hitrrnlIO.setParams(SPACES);
		hitrrnlIO.setChdrcoy(wsspcomn.company);
		hitrrnlIO.setChdrnum(chdrclmIO.getChdrnum());
		hitrrnlIO.setFormat(hitrrnlrec);
		hitrrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrrnlIO);
		if (isNE(hitrrnlIO.getStatuz(), varcom.oK)
		&& isNE(hitrrnlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hitrrnlIO.getParams());
			fatalError600();
		}
		if (isEQ(hitrrnlIO.getStatuz(), varcom.oK)) {
			wsaaIncompleteUtrn.set("Y");
		}
	}

protected void convertCurrency6000()
	{
		para6010();
	}

protected void para6010()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.function.set("CVRT");
		/*MOVE WSAA-CURR-STORE        TO CLNK-CURR-IN.            <007>*/
		conlinkrec.currIn.set(wsaaCurrIn);
		conlinkrec.currOut.set(sv.paycurr);
		conlinkrec.amountIn.set(wsaaAmount);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(wsaaToday);
		conlinkrec.company.set(wsspcomn.company);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(sv.paycurr);
		callRounding7000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void callRounding7000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
//ILIFE-1137
protected void readTabT5688250()
{
	read251();
}

protected void read251()
{
	itdmIO.setItemcoy(chdrclmIO.getChdrcoy());
	itdmIO.setItemtabl(t5688);
	itdmIO.setItempfx("IT");
	itdmIO.setItemitem(chdrclmIO.getCnttype());
	itdmIO.setItmfrm(sv.effdate);
	itdmIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), Varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(itdmIO.getStatuz());
		fatalError600();
	}
	if (isNE(itdmIO.getItemcoy(), chdrclmIO.getChdrcoy())
	|| isNE(itdmIO.getItemtabl(), t5688)
	|| isNE(itdmIO.getItemitem(), chdrclmIO.getCnttype())
	|| isEQ(itdmIO.getStatuz(), Varcom.endp)) {
		itdmIO.setItemitem(chdrclmIO.getCnttype());
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(e308);
		fatalError600();
	}
	t5688rec.t5688Rec.set(itdmIO.getGenarea());
}

protected void retrieveSuspenseCustomerSpecifc() {
	
}

protected void CalcCustomerSpecifc() {
	
}

public StringUtil getStringUtil() {
	return stringUtil;
}

protected void customerSpecificPensionCalc() {
	
}

protected void updateCustomerSpecifc() {}

protected void getClmdpfByValidLifeStat(){}

protected void findClmHeaderbyValidLif(){}
}

