/*
 * File: T6648pt.java
 * Date: 30 August 2009 2:28:55
 * Author: Quipoz Limited
 * 
 * Class transformed from T6648PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.terminationclaims.tablestructures.T6648rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6648.
*
*
*****************************************************************
* </pre>
*/
public class T6648pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Paid-up Component                          S6648");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(77);
	private FixedLengthStringData filler9 = new FixedLengthStringData(77).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Freq     Minimum       Minimum      Minimum          Minimum         Minimum");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(77);
	private FixedLengthStringData filler10 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(67).isAPartOf(wsaaPrtLine005, 10, FILLER).init("Sum-Assured   Remaining    Months           Paid Up         Paid Up");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(76);
	private FixedLengthStringData filler12 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine006, 24, FILLER).init("Pol. Term    In Force           Sum             Cash");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(77);
	private FixedLengthStringData filler14 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler15 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine007, 24, FILLER).init("(months)                      Assured          Option");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(77);
	private FixedLengthStringData filler16 = new FixedLengthStringData(77).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" ----------------------------------------------------------------------------");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(77);
	private FixedLengthStringData filler17 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  00");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine009, 7).setPattern("ZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine009, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 40).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine009, 47).setPattern("ZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 61, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine009, 63).setPattern("ZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(77);
	private FixedLengthStringData filler22 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 0, FILLER).init("  01");
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine010, 7).setPattern("ZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine010, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 40).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine010, 47).setPattern("ZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 61, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine010, 63).setPattern("ZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(77);
	private FixedLengthStringData filler27 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 0, FILLER).init("  02");
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine011, 7).setPattern("ZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine011, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 40).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine011, 47).setPattern("ZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 61, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine011, 63).setPattern("ZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(77);
	private FixedLengthStringData filler32 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 0, FILLER).init("  04");
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine012, 7).setPattern("ZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine012, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 40).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine012, 47).setPattern("ZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 61, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine012, 63).setPattern("ZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(77);
	private FixedLengthStringData filler37 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 0, FILLER).init("  12");
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine013, 7).setPattern("ZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 21, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrtLine013, 27).setPattern("ZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 40).setPattern("ZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine013, 47).setPattern("ZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 61, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(13, 2).isAPartOf(wsaaPrtLine013, 63).setPattern("ZZZZZZZZZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6648rec t6648rec = new T6648rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6648pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6648rec.t6648Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t6648rec.minisuma01);
		fieldNo008.set(t6648rec.minterm01);
		fieldNo009.set(t6648rec.minmthif01);
		fieldNo010.set(t6648rec.minpusa01);
		fieldNo011.set(t6648rec.minipup01);
		fieldNo012.set(t6648rec.minisuma02);
		fieldNo013.set(t6648rec.minterm02);
		fieldNo014.set(t6648rec.minmthif02);
		fieldNo015.set(t6648rec.minpusa02);
		fieldNo016.set(t6648rec.minipup02);
		fieldNo017.set(t6648rec.minisuma03);
		fieldNo018.set(t6648rec.minterm03);
		fieldNo019.set(t6648rec.minmthif03);
		fieldNo020.set(t6648rec.minpusa03);
		fieldNo021.set(t6648rec.minipup03);
		fieldNo022.set(t6648rec.minisuma04);
		fieldNo023.set(t6648rec.minterm04);
		fieldNo024.set(t6648rec.minmthif04);
		fieldNo025.set(t6648rec.minpusa04);
		fieldNo026.set(t6648rec.minipup04);
		fieldNo027.set(t6648rec.minisuma05);
		fieldNo028.set(t6648rec.minterm05);
		fieldNo029.set(t6648rec.minmthif05);
		fieldNo030.set(t6648rec.minpusa05);
		fieldNo031.set(t6648rec.minipup05);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
