package com.csc.life.terminationclaims.recordstructures;

import java.util.ArrayList;
import java.util.List;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class Cmcl001Rec extends ExternalData {
	// Cmcl001Rec
	private static final long serialVersionUID = 1L;

	// *******************************
	// Attribute Declarations
	// *******************************
	public FixedLengthStringData statuz = new FixedLengthStringData(4);

	private String premMethod;
	private String language;
	private String currcode;
	private String billfreq;
	private String ratingdate;
	private String rstaflag;
	private String life;
	private String tranEffdate;
	private List<List<String>> tpdtype;
	private List<List<String>> crtable;
	private List<List<String>> sumin;
	private List<List<String>> covrCoverage;
	private List<List<String>> covrRiderId;
	private List<List<String>> mortcls;
	private List<String> lsex;
	private List<List<String>> duration;
	private List<List<String>> prmbasis;
	private List<List<String>> linkcov;
	private String chdrnum;
	private String chdrcoy;
	private List<List<String>> riskComDate;
	private List<String> effdate;
	private List<List<String>> dob;
	private List<String> rstate01;
	private String srcebus;
	private String agentClass;
	private List<List<String>> termdate;
	private List<List<String>> icommtot;
	private List<List<String>> totsumin;
	private List<List<String>> payamnt;
	private List<List<String>> covrcd;
	private String crrcd;
	private String cnttype;
	private String covcrtable;
	private String covcovrCoverage;
	private String commclaw = "0";
	private String costfreq;
	
	public String getPremMethod() {
		return premMethod;
	}

	public void setPremMethod(String premMethod) {
		this.premMethod = premMethod;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCurrcode() {
		return currcode;
	}

	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}

	public String getBillfreq() {
		return billfreq;
	}

	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}

	public String getRatingdate() {
		return ratingdate;
	}

	public void setRatingdate(String ratingdate) {
		this.ratingdate = ratingdate;
	}

	public String getRstaflag() {
		return rstaflag;
	}

	public void setRstaflag(String rstaflag) {
		this.rstaflag = rstaflag;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getTranEffdate() {
		return tranEffdate;
	}

	public void setTranEffdate(String tranEffdate) {
		this.tranEffdate = tranEffdate;
	}

	public List<List<String>> getTpdtype() {
		return tpdtype;
	}

	public void setTpdtype(List<List<String>> tpdtype) {
		this.tpdtype = tpdtype;
	}

	public List<List<String>> getCrtable() {
		return crtable;
	}

	public void setCrtable(List<List<String>> crtable) {
		this.crtable = crtable;
	}

	public List<List<String>> getSumin() {
		return sumin;
	}

	public void setSumin(List<List<String>> sumin) {
		this.sumin = sumin;
	}

	public List<List<String>> getCovrCoverage() {
		return covrCoverage;
	}

	public void setCovrCoverage(List<List<String>> covrCoverage) {
		this.covrCoverage = covrCoverage;
	}

	public List<List<String>> getCovrRiderId() {
		return covrRiderId;
	}

	public void setCovrRiderId(List<List<String>> covrRiderId) {
		this.covrRiderId = covrRiderId;
	}

	public List<List<String>> getMortcls() {
		return mortcls;
	}

	public void setMortcls(List<List<String>> mortcls) {
		this.mortcls = mortcls;
	}

	public List<String> getLsex() {
		return lsex;
	}

	public void setLsex(List<String> lsex) {
		this.lsex = lsex;
	}

	public List<List<String>> getDuration() {
		return duration;
	}

	public void setDuration(List<List<String>> duration) {
		this.duration = duration;
	}

	public List<List<String>> getPrmbasis() {
		return prmbasis;
	}

	public void setPrmbasis(List<List<String>> prmbasis) {
		this.prmbasis = prmbasis;
	}

	public List<List<String>> getLinkcov() {
		return linkcov;
	}

	public void setLinkcov(List<List<String>> linkcov) {
		this.linkcov = linkcov;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public List<List<String>> getRiskComDate() {
		return riskComDate;
	}

	public void setRiskComDate(List<List<String>> riskComDate) {
		this.riskComDate = riskComDate;
	}

	public List<String> getEffdate() {
		return effdate;
	}

	public void setEffdate(List<String> effdate) {
		this.effdate = effdate;
	}

	public List<List<String>> getDob() {
		return dob;
	}

	public void setDob(List<List<String>> dob) {
		this.dob = dob;
	}

	public List<String> getRstate01() {
		return rstate01;
	}

	public void setRstate01(List<String> rstate01) {
		this.rstate01 = rstate01;
	}

	public String getSrcebus() {
		return srcebus;
	}

	public void setSrcebus(String srcebus) {
		this.srcebus = srcebus;
	}

	public String getAgentClass() {
		return agentClass;
	}

	public void setAgentClass(String agentClass) {
		this.agentClass = agentClass;
	}

	public List<List<String>> getTermdate() {
		return termdate;
	}

	public void setTermdate(List<List<String>> termdate) {
		this.termdate = termdate;
	}

	public List<List<String>> getIcommtot() {
		return icommtot;
	}

	public void setIcommtot(List<List<String>> icommtot) {
		this.icommtot = icommtot;
	}

	public List<List<String>> getTotsumin() {
		return totsumin;
	}

	public void setTotsumin(List<List<String>> totsumin) {
		this.totsumin = totsumin;
	}

	public List<List<String>> getPayamnt() {
		return payamnt;
	}

	public void setPayamnt(List<List<String>> payamnt) {
		this.payamnt = payamnt;
	}

	public List<List<String>> getCovrcd() {
		return covrcd;
	}

	public void setCovrcd(List<List<String>> covrcd) {
		this.covrcd = covrcd;
	}

	public String getCrrcd() {
		return crrcd;
	}

	public void setCrrcd(String crrcd) {
		this.crrcd = crrcd;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public String getCovcrtable() {
		return covcrtable;
	}

	public void setCovcrtable(String covcrtable) {
		this.covcrtable = covcrtable;
	}

	public String getCovcovrCoverage() {
		return covcovrCoverage;
	}

	public void setCovcovrCoverage(String covcovrCoverage) {
		this.covcovrCoverage = covcovrCoverage;
	}

	public String getCommclaw() {
		return commclaw;
	}

	public void setCommclaw(String commclaw) {
		this.commclaw = commclaw;
	}
	public String getCostfreq() {
		return costfreq;
	}

	public void setCostfreq(String costfreq) {
		this.costfreq = costfreq;
	}
	
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

	public void initialise() {
		payamnt = new ArrayList<>();
		if (totsumin == null) {
			totsumin = new ArrayList<>();
		}
	}

	@Override
	public void initialize() {
		// Auto-generated method stub
	}

}