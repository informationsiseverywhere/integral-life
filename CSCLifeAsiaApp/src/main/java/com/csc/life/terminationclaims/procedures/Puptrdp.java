/*
 * File: Puptrdp.java
 * Date: 30 August 2009 2:02:57
 * Author: Quipoz Limited
 * 
 * Class transformed from PUPTRDP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*                      P U P T R D P
*                   -------------------
*
*  Commission Processing
*  ---------------------
*  Commission details are represented by AGCM records, using
*  the logical view AGCMPUP.
*  Read all the AGCMPUP records for the component and
*  accumulate the earned and paid commision for each agent.
*
*  If the paid amount is greater than the earned amount,
*  create two ACMV records, using LIFACMV.  The claw back
*  amount (earned commission - paid commission) will be used
*  to debit the commission payable and to credit the
*  commission to advance.
*
*  See the Notes section at the end of this specification. The
*  subroutine LIFACMV will be called in order to write these
*  ACMV records.
*
*
*  NOTES
*  -----
*  Tables:
*  -------
*  T5645 - Transaction Accounting Rules       Key: Program Id.
*
*
*  ACMV Record
*  -----------
*  Two ACMV records will be created.
*  The details for these will be held on T5645, accessed by
*  program Id.  Line #1 will give details for the first
*  posting which will be negative, and line #2 will give
*  details for the second posting which will be positive.  The
*  amount written on the ACMV record will be positive in both
*  cases, with the appropriate negative or positive value of
*  the field being determined by the 'sign' entered on T5645.
*
*  Note: the subroutine LIFACMV will be called to add these
*  records, call it with a function of 'PSTW' so that ACBL
*  records are created in the subroutine.
*
*  The key will be the batch transaction key passed in the
*  linkage section. The remaining fields will be set as
*  follows:
*
*  . ACMV-RLDGCOY           -  Contract Company
*  . ACMV-SACSCODE          -  from T5645 - lines 1 and 2
*  . ACMV-RLDGACCT          -  Agent number
*  . ACMV-ORIGCURR          -  Contract Currency
*  . ACMV-SACSTYP           -  from T5645 - lines 1 and 2
*  . ACMV-RDOCNUM           -  Contract Number
*  . ACMV-TRANNO            -  CHDRMJA-TRANNO
*  . ACMV-JRNSEQ            -  zeros
*  . ACMV-ORIGAMT           -  amount of clawback (earned
*                              minus paid) in contract currency
*  . ACMV-TRANREF           -  CHDRMJA-TRANNO
*  . ACMV-TRANDESC          -  transaction description from
*                              T5645
*  . ACMV-CRATE             -  zeros
*  . ACMV-ACCTAMT           -  zeros
*  . ACMV-GENLCOY           -  contract company
*  . ACMV-GENLCUR           -  spaces
*  . ACMV-GLCODE            -  from T5645 - lines 1 and 2
*  . ACMV-GLSIGN            -  from T5645 - lines 1 and 2
*  . ACMV-CONTOT            -  from T5645 - lines 1 and 2
*  . ACMV-POSTYEAR          -  zeros
*  . ACMV-POSTMONTH         -  zeros
*  . ACMV-EFFDATE           -  CHDRMJA-BTDATE
*  . ACMV-RCAMT             -  zeros
*  . ACMV-FRCDATE           -  VRCM-MAX-DATE
*  . ACMV-INTEXTIND         -  spaces
*  . ACMV-TRANSACTION-DATE  -  system date
*  . ACMV-TRANSACTION-TIME  -  system time
*  . ACMV-USER              -  VRCM-USER
*  . ACMV-TERMID            -  VRCM-TERMID
*
*
****************************************************************
* </pre>
*/
public class Puptrdp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PUPTRDP";
	private PackedDecimalData wsaaCommEarned = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCommPaid = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCommClawback = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOvrdCommEarned = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOvrdCommPaid = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOvrdCommClawback = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String th605 = "TH605";
		/* ERRORS */
	private static final String e308 = "E308";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String agcmrec = "AGCMREC";
	private static final String agcmdbcrec = "AGCMREC";
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Trmreasrec trmreasrec = new Trmreasrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Th605rec th605rec = new Th605rec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Ovrduerec ovrduerec = new Ovrduerec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		callAgcmio2020, 
		postOvrdCommission2205, 
		exit2209
	}

	public Puptrdp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		ovrduerec.statuz.set(varcom.oK);
		readTableT56451000();
		/* Read T5688 using contract type from OVRDUEREC*/
		readTableT56881100();
		readTh6051200();
		commissionClawback2000();
		processReassurance3000();
		/*EXIT*/
		exitProgram();
	}

protected void readTableT56451000()
	{
		readT56451000();
		callDescio1005();
	}

protected void readT56451000()
	{
		/*    Read the Transaction Accounting Rules Table*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			databaseError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void callDescio1005()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ovrduerec.company);
		descIO.setDesctabl(t5645);
		descIO.setLanguage(ovrduerec.language);
		descIO.setDescitem(wsaaSubr);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			databaseError9000();
		}
		/*EXIT*/
	}

protected void readTableT56881100()
	{
		readT56881100();
	}

protected void readT56881100()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(ovrduerec.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(ovrduerec.cnttype);
		itdmIO.setItmfrm(ovrduerec.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError9000();
		}
		if (isNE(itdmIO.getItemcoy(), ovrduerec.company)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), ovrduerec.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(ovrduerec.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			databaseError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readTh6051200()
	{
		start1210();
	}

protected void start1210()
	{
		/*  Read TH605 to see whether system is "Override based on Agent   */
		/*  Details"                                                       */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		//itemIO.setItemcoy(ovrduerec.company); //MIBT-133
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(th605);
		//itemIO.setItemitem(ovrduerec.company); //MIBT-133
		itemIO.setItemitem(ovrduerec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError9000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*2000-COMMISSION-CLAWBACK SECTION.                                
	*2000-DO-BEGN-AGCMPUP.                                            
	**** MOVE SPACES                 TO AGCMPUP-DATA-AREA.            
	**** MOVE OVRD-CHDRCOY           TO AGCMPUP-CHDRCOY.              
	**** MOVE OVRD-CHDRNUM           TO AGCMPUP-CHDRNUM.              
	**** MOVE OVRD-LIFE              TO AGCMPUP-LIFE.                 
	**** MOVE OVRD-COVERAGE          TO AGCMPUP-COVERAGE.             
	**** MOVE OVRD-RIDER             TO AGCMPUP-RIDER.                
	**** MOVE OVRD-PLAN-SUFFIX       TO AGCMPUP-PLAN-SUFFIX.          
	**** MOVE BEGNH                  TO AGCMPUP-FUNCTION.             
	**** MOVE BEGN                   TO AGCMPUP-FUNCTION.        <001>
	**** MOVE AGCMPUPREC             TO AGCMPUP-FORMAT.               
	**** CALL 'AGCMPUPIO'            USING AGCMPUP-PARAMS.            
	**** IF AGCMPUP-STATUZ           NOT = O-K                        
	**** AND AGCMPUP-STATUZ          NOT = ENDP                       
	****     MOVE AGCMPUP-PARAMS     TO SYSR-PARAMS                   
	****     PERFORM 9000-DATABASE-ERROR.                             
	**** IF OVRD-CHDRCOY             NOT = AGCMPUP-CHDRCOY            
	**** OR OVRD-CHDRNUM             NOT = AGCMPUP-CHDRNUM            
	**** OR OVRD-LIFE                NOT = AGCMPUP-LIFE               
	**** OR OVRD-COVERAGE            NOT = AGCMPUP-COVERAGE           
	**** OR OVRD-RIDER               NOT = AGCMPUP-RIDER              
	**** OR OVRD-PLAN-SUFFIX         NOT = AGCMPUP-PLAN-SUFFIX        
	**** OR AGCMPUP-STATUZ           = ENDP                           
	****     MOVE ENDP               TO AGCMPUP-STATUZ.               
	**** PERFORM 2100-PROCESS-AGCMPUP                                 
	****     UNTIL AGCMPUP-STATUZ    = ENDP.                          
	*2009-EXIT.                                                       
	**** EXIT.                                                        
	*2100-PROCESS-AGCMPUP SECTION.                                    
	*2100-CHK-FOR-OVRD-COMM.                                          
	**** MOVE ZEROES                 TO WSAA-COMM-EARNED              
	****                                WSAA-COMM-PAID                
	****                                WSAA-OVRD-COMM-EARNED         
	****                                WSAA-OVRD-COMM-PAID.          
	**** IF AGCMPUP-OVRDCAT          = 'O'                            
	****     ADD AGCMPUP-COMERN      TO WSAA-OVRD-COMM-EARNED         
	****     ADD AGCMPUP-COMPAY      TO WSAA-OVRD-COMM-PAID           
	**** ELSE                                                         
	****     ADD AGCMPUP-COMERN      TO WSAA-COMM-EARNED              
	****     ADD AGCMPUP-COMPAY      TO WSAA-COMM-PAID.               
	**** PERFORM 2200-POST-CLAWBACK.                                  
	* Rewrite the AGCM record, to update commission payed
	**** MOVE AGCMPUP-COMERN         TO AGCMPUP-COMPAY.               
	**** MOVE AGCMPUPREC             TO AGCMPUP-FORMAT.               
	**** MOVE REWRT                  TO AGCMPUP-FUNCTION.             
	**** MOVE WRITD                  TO AGCMPUP-FUNCTION.        <001>
	**** CALL 'AGCMPUPIO'            USING AGCMPUP-PARAMS.            
	**** IF AGCMPUP-STATUZ           NOT = O-K                        
	****     MOVE AGCMPUP-PARAMS     TO SYSR-PARAMS                   
	****     MOVE AGCMPUP-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 9000-DATABASE-ERROR.                             
	*2102-READ-NEXT-AGCMPUP.                                          
	**** MOVE AGCMPUPREC             TO AGCMPUP-FORMAT.               
	**** MOVE NEXTR                  TO AGCMPUP-FUNCTION.             
	**** CALL 'AGCMPUPIO'            USING AGCMPUP-PARAMS.            
	**** IF AGCMPUP-STATUZ           NOT = O-K                        
	**** AND AGCMPUP-STATUZ          NOT = ENDP                       
	****     MOVE AGCMPUP-PARAMS     TO SYSR-PARAMS                   
	****     PERFORM 9000-DATABASE-ERROR.                             
	**** IF OVRD-CHDRCOY             NOT = AGCMPUP-CHDRCOY            
	**** OR OVRD-CHDRNUM             NOT = AGCMPUP-CHDRNUM            
	**** OR OVRD-LIFE                NOT = AGCMPUP-LIFE               
	**** OR OVRD-COVERAGE            NOT = AGCMPUP-COVERAGE           
	**** OR OVRD-RIDER               NOT = AGCMPUP-RIDER              
	**** OR OVRD-PLAN-SUFFIX         NOT = AGCMPUP-PLAN-SUFFIX        
	**** OR AGCMPUP-STATUZ           = ENDP                           
	****    MOVE ENDP                TO AGCMPUP-STATUZ.               
	*2109-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void commissionClawback2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2010();
				case callAgcmio2020: 
					callAgcmio2020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2010()
	{
		/* Read the first AGCM record for this contract.                   */
		agcmIO.setParams(SPACES);
		agcmIO.setChdrnum(ovrduerec.chdrnum);
		agcmIO.setChdrcoy(ovrduerec.chdrcoy);
		agcmIO.setPlanSuffix(ZERO);
		agcmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		agcmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		agcmIO.setFormat(agcmrec);
	}

protected void callAgcmio2020()
	{
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), varcom.oK)
		&& isNE(agcmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			databaseError9000();
		}
		/* If the end of the AGCM file has been reached or an AGCM         */
		/* record was read but not for the contract being processed,       */
		/* then exit the section.                                          */
		if (isEQ(agcmIO.getStatuz(), varcom.endp)
		|| isNE(agcmIO.getChdrcoy(), ovrduerec.chdrcoy)
		|| isNE(agcmIO.getChdrnum(), ovrduerec.chdrnum)) {
			return ;
		}
		/* for the AGCM record just read.                                  */
		if (isEQ(agcmIO.getChdrcoy(), ovrduerec.chdrcoy)
		&& isEQ(agcmIO.getChdrnum(), ovrduerec.chdrnum)
		&& isEQ(agcmIO.getLife(), ovrduerec.life)
		&& isEQ(agcmIO.getCoverage(), ovrduerec.coverage)
		&& isEQ(agcmIO.getRider(), ovrduerec.rider)
		&& isEQ(agcmIO.getPlanSuffix(), ovrduerec.planSuffix)
		&& isNE(agcmIO.getCompay(), agcmIO.getComern())) {
			updateAgcmFile2100();
		}
		/* Loop round to see if there any other AGCM records to            */
		/* process for the contract.                                       */
		agcmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callAgcmio2020);
	}

protected void updateAgcmFile2100()
	{
		readh2110();
		rewrt2120();
		post2130();
		writr2140();
	}

	/**
	* <pre>
	* previous processing loop on AGCM is not upset.                  
	* created with commission paid set to earned, valid-flag of "1"   
	* and correct tranno. Also, post account records (ACMV).          
	* </pre>
	*/
protected void readh2110()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmIO.getAgntnum());
		agcmdbcIO.setLife(agcmIO.getLife());
		agcmdbcIO.setCoverage(agcmIO.getCoverage());
		agcmdbcIO.setRider(agcmIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			databaseError9000();
		}
	}

protected void rewrt2120()
	{
		agcmdbcIO.setValidflag("2");
		agcmdbcIO.setCurrto(ovrduerec.btdate);
		agcmdbcIO.setFunction(varcom.rewrt);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			databaseError9000();
		}
	}

protected void post2130()
	{
		if (isEQ(agcmIO.getOvrdcat(), "O")) {
			wsaaOvrdCommEarned.set(agcmIO.getComern());
			wsaaOvrdCommPaid.set(agcmIO.getCompay());
		}
		else {
			wsaaCommEarned.set(agcmIO.getComern());
			wsaaCommPaid.set(agcmIO.getCompay());
		}
		postClawback2200();
	}

protected void writr2140()
	{
		/* correct tranno, and new commission paid.                        */
		agcmdbcIO.setCompay(agcmIO.getComern());
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setTranno(ovrduerec.tranno);
		agcmdbcIO.setCurrfrom(ovrduerec.btdate);
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writr);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			databaseError9000();
		}
	}

protected void postClawback2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					calculateClawback2200();
					postCommissionAgent2201();
					postCommissionComponent2202();
				case postOvrdCommission2205: 
					postOvrdCommission2205();
					postOvrdCommComponent2206();
				case exit2209: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void calculateClawback2200()
	{
		compute(wsaaOvrdCommClawback, 2).set(sub(wsaaOvrdCommEarned, wsaaOvrdCommPaid));
		compute(wsaaCommClawback, 2).set(sub(wsaaCommEarned, wsaaCommPaid));
	}

	/**
	* <pre>
	*  If commission due = 0 then do not write a ACMV record.
	* </pre>
	*/
protected void postCommissionAgent2201()
	{
		if (isEQ(wsaaCommClawback, ZERO)) {
			goTo(GotoLabel.postOvrdCommission2205);
		}
		setupCommonAcmv2300();
		lifacmvrec.origamt.set(wsaaCommClawback);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		wsaaRldgChdrnum.set(ovrduerec.chdrnum);
		wsaaRldgLife.set(ovrduerec.life);
		wsaaRldgCoverage.set(ovrduerec.coverage);
		wsaaRldgRider.set(ovrduerec.rider);
		wsaaPlansuff.set(ovrduerec.planSuffix);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaRldgacct);
		/* First posting is to Life Agent account and should post agent*/
		/* numver as Entity type and post to T5645 entry 01.*/
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(ovrduerec.agntnum);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			databaseError9000();
		}
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmIO.getAnnprem());
			callZorcompy4000();
		}
	}

protected void postCommissionComponent2202()
	{
		lifacmvrec.origamt.set(wsaaCommClawback);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		/*  If component level accounting is required then post to T5645*/
		/*  entry 03 and LIFA-RLDGACCT = Full component key else post to*/
		/*  T5645 entry 02 and LIFA-RLDGACCT = Contract Header Number.*/
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlansuff.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(ovrduerec.chdrnum);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			databaseError9000();
		}
	}

protected void postOvrdCommission2205()
	{
		/*   If overriding commission due = 0 then do not write a*/
		/*   ACMV record.*/
		if (isEQ(wsaaOvrdCommClawback, ZERO)) {
			goTo(GotoLabel.exit2209);
		}
		/* First posting is to Life Agent account and should post agent*/
		/* numver as Entity type and post to T5645 entry 04.*/
		setupCommonAcmv2300();
		lifacmvrec.origamt.set(wsaaOvrdCommClawback);
		/* MOVE AGCMPUP-CEDAGENT       TO LIFA-TRANREF.                 */
		lifacmvrec.tranref.set(agcmIO.getCedagent());
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.contot.set(t5645rec.cnttot04);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(SPACES);
		/* MOVE OVRD-AGNTNUM           TO LIFA-RLDGACCT.                */
		lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			databaseError9000();
		}
	}

protected void postOvrdCommComponent2206()
	{
		lifacmvrec.origamt.set(wsaaOvrdCommClawback);
		/* MOVE AGCMPUP-CEDAGENT       TO LIFA-TRANREF.                 */
		lifacmvrec.tranref.set(agcmIO.getCedagent());
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		/*  If component level accounting is required then post to T5645*/
		/*  entry 06 and LIFA-RLDGACCT = Full component key else post to*/
		/*  T5645 entry 05 and LIFA-RLDGACCT = Contract Header Number.*/
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.contot.set(t5645rec.cnttot06);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlansuff.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			lifacmvrec.rldgacct.set(ovrduerec.chdrnum);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			databaseError9000();
		}
	}

protected void setupCommonAcmv2300()
	{
		moveCommon2300();
	}

protected void moveCommon2300()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batccoy.set(ovrduerec.company);
		lifacmvrec.batcbrn.set(ovrduerec.batcbrn);
		lifacmvrec.batcactyr.set(ovrduerec.acctyear);
		lifacmvrec.batcactmn.set(ovrduerec.acctmonth);
		lifacmvrec.batctrcde.set(ovrduerec.trancode);
		lifacmvrec.batcbatch.set(ovrduerec.batcbatch);
		lifacmvrec.rdocnum.set(ovrduerec.chdrnum);
		lifacmvrec.tranno.set(ovrduerec.tranno);
		lifacmvrec.rldgcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		lifacmvrec.tranref.set(ovrduerec.tranno);
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(ovrduerec.btdate);
		lifacmvrec.termid.set(ovrduerec.termid);
		lifacmvrec.user.set(ovrduerec.user);
		lifacmvrec.transactionTime.set(ovrduerec.tranTime);
		lifacmvrec.transactionDate.set(ovrduerec.tranDate);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(ZERO);
		lifacmvrec.postmonth.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
	}

	/**
	* <pre>
	*                                                         <R96REA>
	***********************************                       <R96REA>
	* </pre>
	*/
protected void processReassurance3000()
	{
		trmreas3001();
	}

	/**
	* <pre>
	***********************************                       <R96REA>
	* </pre>
	*/
protected void trmreas3001()
	{
		wsaaBatckey.batcBatcpfx.set("BA");
		wsaaBatckey.batcBatccoy.set(ovrduerec.chdrcoy);
		wsaaBatckey.batcBatcbrn.set(ovrduerec.batcbrn);
		wsaaBatckey.batcBatcactyr.set(ovrduerec.acctyear);
		wsaaBatckey.batcBatcactmn.set(ovrduerec.acctmonth);
		wsaaBatckey.batcBatctrcde.set(ovrduerec.trancode);
		wsaaBatckey.batcBatcbatch.set(ovrduerec.batcbatch);
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("CHGR");
		trmreasrec.chdrcoy.set(ovrduerec.chdrcoy);
		trmreasrec.chdrnum.set(ovrduerec.chdrnum);
		trmreasrec.life.set(ovrduerec.life);
		trmreasrec.coverage.set(ovrduerec.coverage);
		trmreasrec.rider.set(ovrduerec.rider);
		trmreasrec.planSuffix.set(ovrduerec.planSuffix);
		trmreasrec.cnttype.set(ovrduerec.cnttype);
		trmreasrec.crtable.set(ovrduerec.crtable);
		trmreasrec.polsum.set(ovrduerec.polsum);
		trmreasrec.effdate.set(ovrduerec.ptdate);
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(ovrduerec.tranno);
		trmreasrec.language.set(ovrduerec.language);
		trmreasrec.billfreq.set(ovrduerec.billfreq);
		trmreasrec.ptdate.set(ovrduerec.ptdate);
		trmreasrec.origcurr.set(ovrduerec.cntcurr);
		trmreasrec.acctcurr.set(ovrduerec.cntcurr);
		trmreasrec.crrcd.set(ovrduerec.crrcd);
		trmreasrec.convUnits.set(ZERO);
		trmreasrec.jlife.set(SPACES);
		trmreasrec.singp.set(ovrduerec.instprem);
		trmreasrec.pstatcode.set(ovrduerec.pstatcode);
		trmreasrec.oldSumins.set(ovrduerec.sumins);
		trmreasrec.newSumins.set(ovrduerec.newSumins);
		trmreasrec.clmPercent.set(ZERO);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(trmreasrec.statuz);
			syserrrec.params.set(trmreasrec.trracalRec);
			databaseError9000();
		}
	}

protected void callZorcompy4000()
	{
		start4010();
	}

protected void start4010()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set("Y");
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.effdate.set(agcmIO.getEfdate());
		zorlnkrec.ptdate.set(agcmIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(lifacmvrec.tranno);
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set("Y");
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			databaseError9000();
		}
	}

protected void databaseError9000()
	{
		/*ERRORS*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		ovrduerec.statuz.set(syserrrec.statuz);
		/*EXIT*/
	}
}
