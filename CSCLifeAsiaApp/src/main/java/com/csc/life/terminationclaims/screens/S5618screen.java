package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5618screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5618ScreenVars sv = (S5618ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5618screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5618ScreenVars screenVars = (S5618ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.unit.setClassString("");
		screenVars.insprm01.setClassString("");
		screenVars.insprm02.setClassString("");
		screenVars.insprm03.setClassString("");
		screenVars.insprm04.setClassString("");
		screenVars.insprm05.setClassString("");
		screenVars.insprm06.setClassString("");
		screenVars.insprm07.setClassString("");
		screenVars.insprm08.setClassString("");
		screenVars.insprm09.setClassString("");
		screenVars.insprm10.setClassString("");
		screenVars.insprm11.setClassString("");
		screenVars.insprm12.setClassString("");
		screenVars.insprm13.setClassString("");
		screenVars.insprm14.setClassString("");
		screenVars.insprm15.setClassString("");
		screenVars.insprm16.setClassString("");
		screenVars.insprm17.setClassString("");
		screenVars.insprm18.setClassString("");
		screenVars.insprm19.setClassString("");
		screenVars.insprm20.setClassString("");
		screenVars.insprm21.setClassString("");
		screenVars.insprm22.setClassString("");
		screenVars.insprm23.setClassString("");
		screenVars.insprm24.setClassString("");
		screenVars.insprm25.setClassString("");
		screenVars.insprm26.setClassString("");
		screenVars.insprm27.setClassString("");
		screenVars.insprm28.setClassString("");
		screenVars.insprm29.setClassString("");
		screenVars.insprm30.setClassString("");
		screenVars.insprm31.setClassString("");
		screenVars.insprm32.setClassString("");
		screenVars.insprm33.setClassString("");
		screenVars.insprm34.setClassString("");
		screenVars.insprm35.setClassString("");
		screenVars.insprm36.setClassString("");
		screenVars.insprm37.setClassString("");
		screenVars.insprm38.setClassString("");
		screenVars.insprm39.setClassString("");
		screenVars.insprm40.setClassString("");
		screenVars.insprm41.setClassString("");
		screenVars.insprm42.setClassString("");
		screenVars.insprm43.setClassString("");
		screenVars.insprm44.setClassString("");
		screenVars.insprm45.setClassString("");
		screenVars.insprm46.setClassString("");
		screenVars.insprm47.setClassString("");
		screenVars.insprm48.setClassString("");
		screenVars.insprm49.setClassString("");
		screenVars.insprm50.setClassString("");
	}

/**
 * Clear all the variables in S5618screen
 */
	public static void clear(VarModel pv) {
		S5618ScreenVars screenVars = (S5618ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.unit.clear();
		screenVars.insprm01.clear();
		screenVars.insprm02.clear();
		screenVars.insprm03.clear();
		screenVars.insprm04.clear();
		screenVars.insprm05.clear();
		screenVars.insprm06.clear();
		screenVars.insprm07.clear();
		screenVars.insprm08.clear();
		screenVars.insprm09.clear();
		screenVars.insprm10.clear();
		screenVars.insprm11.clear();
		screenVars.insprm12.clear();
		screenVars.insprm13.clear();
		screenVars.insprm14.clear();
		screenVars.insprm15.clear();
		screenVars.insprm16.clear();
		screenVars.insprm17.clear();
		screenVars.insprm18.clear();
		screenVars.insprm19.clear();
		screenVars.insprm20.clear();
		screenVars.insprm21.clear();
		screenVars.insprm22.clear();
		screenVars.insprm23.clear();
		screenVars.insprm24.clear();
		screenVars.insprm25.clear();
		screenVars.insprm26.clear();
		screenVars.insprm27.clear();
		screenVars.insprm28.clear();
		screenVars.insprm29.clear();
		screenVars.insprm30.clear();
		screenVars.insprm31.clear();
		screenVars.insprm32.clear();
		screenVars.insprm33.clear();
		screenVars.insprm34.clear();
		screenVars.insprm35.clear();
		screenVars.insprm36.clear();
		screenVars.insprm37.clear();
		screenVars.insprm38.clear();
		screenVars.insprm39.clear();
		screenVars.insprm40.clear();
		screenVars.insprm41.clear();
		screenVars.insprm42.clear();
		screenVars.insprm43.clear();
		screenVars.insprm44.clear();
		screenVars.insprm45.clear();
		screenVars.insprm46.clear();
		screenVars.insprm47.clear();
		screenVars.insprm48.clear();
		screenVars.insprm49.clear();
		screenVars.insprm50.clear();
	}
}
