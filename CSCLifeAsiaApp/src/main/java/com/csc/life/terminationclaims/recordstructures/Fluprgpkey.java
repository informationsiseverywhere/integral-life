package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:40
 * Description:
 * Copybook name: FLUPRGPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fluprgpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fluprgpFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData fluprgpKey = new FixedLengthStringData(256).isAPartOf(fluprgpFileKey, 0, REDEFINE);
  	public FixedLengthStringData fluprgpChdrcoy = new FixedLengthStringData(1).isAPartOf(fluprgpKey, 0);
  	public FixedLengthStringData fluprgpChdrnum = new FixedLengthStringData(8).isAPartOf(fluprgpKey, 1);
  	public FixedLengthStringData fluprgpClamnum = new FixedLengthStringData(8).isAPartOf(fluprgpKey, 9);
  	public PackedDecimalData fluprgpFupno = new PackedDecimalData(2, 0).isAPartOf(fluprgpKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(237).isAPartOf(fluprgpKey, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fluprgpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fluprgpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}