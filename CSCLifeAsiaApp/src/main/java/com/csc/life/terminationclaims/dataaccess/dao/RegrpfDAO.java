package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Regrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface RegrpfDAO extends BaseDAO<Regrpf>{
	public List<Regrpf> searchRegrpfRecordByCoy(String coy);
	public int deleteRegrpfRecord(List<Long> uniqueNumberList);
	public void insertRegrpfRecord(List<Regrpf> insertRegrpfList);
}
