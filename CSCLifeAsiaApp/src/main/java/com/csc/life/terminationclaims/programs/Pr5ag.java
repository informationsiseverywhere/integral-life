package com.csc.life.terminationclaims.programs;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import java.util.ArrayList;
import java.util.List;
import com.csc.life.terminationclaims.screens.Sr5agScreenVars;
import com.csc.life.terminationclaims.tablestructures.Tr5agrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.parent.ScreenProgram;


import com.csc.smart400framework.parent.ScreenProgCS;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;

public class Pr5ag extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR5AG");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "";
	private String wsaaFirstTime = "Y";
	private ZonedDecimalData wsaaSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaStoreSeqno = new FixedLengthStringData(2);
		/* ERRORS */
	private static final String e026 = "E026";
	private static final String e027 = "E027";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr5agrec tr5agrec = new Tr5agrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr5agScreenVars sv = ScreenProgram.getScreenVars( Sr5agScreenVars.class);
	
	public List<String> listProgdesc = new ArrayList<String>();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readPrimaryRecord1020, 
		generalArea1045, 
		other1080, 
		exit2090, 
		other3080, 
		exit3090, 
		continue4020, 
		other4080
	}

	public Pr5ag() {
		super();
		screenVars = sv;
		new ScreenModel("Sr5ag", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case readPrimaryRecord1020: 
					readPrimaryRecord1020();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				case other1080: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		
		if (isNE(wsaaFirstTime, "Y")) {
			goTo(GotoLabel.readPrimaryRecord1020);
		}
		if (isEQ(itemIO.getItemseq(), SPACES)) {
			wsaaSeq.set(ZERO);
		}
		else {
			wsaaSeq.set(itemIO.getItemseq());
		}
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		
		listProgdesc.clear();
	}

protected void readPrimaryRecord1020()
	{
		for(int i = 0 ; i <= 2; i++) {
			wsaaSeq.set(i);
			itemIO.setDataKey(wsspsmart.itemkey);
			if (isEQ(wsaaSeq, ZERO)) {
				itemIO.setItemseq(SPACES);
			}
			else {
				itemIO.setItemseq(wsaaSeq);
			}
			/*READ-RECORD*/
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();			
			}
			readRecord1031();
			/*READ-SECONDARY-RECORDS*/
			/******IF WSAA-FIRST-TIME          NOT = 'Y'*/
			/******   GO TO 1040-MOVE-TO-SCREEN*/
			/******END-IF.*/
		}
		StringBuilder strBuilder = new StringBuilder();		
		for (int i=0; i<listProgdesc.size(); i++) {
			strBuilder.append(listProgdesc.get(i));/* IJTI-1523 */
		}
		sv.excltxt.set(strBuilder.toString());
		
		
		if(isEQ(wsspcomn.flag, "I")){
			sv.excltxt.setReadOnly();
		}else{
			sv.excltxt.setEnabled(BaseScreenData.ENABLED);
		}
		
		
	}
protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
//		wsaaFirstTime = "N";
		moveToScreen1040();
	}

protected void moveToScreen1040()
	{
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)
		&& isEQ(itemIO.getItemseq(), SPACES)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		//IPNC-980 : Target#1 Screen SR386 displaying Blank value in Text Ares for Messages  - Start
		/*if (isEQ(itemIO.getStatuz(), varcom.mrnf)
		&& isEQ(wsspcomn.flag, "I")) {
			scrnparams.errorCode.set(e026);
			wsaaSeq.subtract(1);
			goTo(GotoLabel.other1080);
		}*/
		
		//IPNC-980 : Target#1 Screen SR386 displaying Blank value in Text Ares for Messages  - Start
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr5agrec.tr5agRec.set(itemIO.getGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC     **/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.       **/
		if (isNE(itemIO.getGenarea(), SPACES)) {
			generalArea1045();
		}else{
			exit2090();
		}
		
	}

protected void generalArea1045()
	{	
		sv.progdescs.set(tr5agrec.progdescs);
		try {
			listProgdesc.add(sv.progdescs.toString());
		} catch (Exception ex) {
			
		}
		/*CONFIRMATION-FIELDS*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
		/**     RETRIEVE SCREEN FIELDS AND EDIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
				case exit2090: 
					exit2090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(scrnparams.statuz, varcom.rold)
		&& isEQ(itemIO.getItemseq(), SPACES)) {
			scrnparams.errorCode.set(e027);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)
		&& isEQ(itemIO.getItemseq(), "2")) {
			scrnparams.errorCode.set(e026);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/**    Enter screen validation here.                              **/
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
		/**    UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		/*
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq, ZERO)) {
			itemIO.setItemseq(SPACES);
		}
		else {
			itemIO.setItemseq(wsaaSeq);
		}
		SmartFileCode.execute(appVars, itemIO);
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		*/
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);		
		
		String printTemplate = new String(sv.excltxt.toString().trim()); 
		String pageData = "";	
		int numOfLastDataOfPage = 0;
		String messages[] = printTemplate.split(System.lineSeparator());
		StringBuilder strBuilder = new StringBuilder();
		String message;
		for (int i =0; i<messages.length; i++){
			message = messages[i];/* IJTI-1523 */
			strBuilder.append(System.lineSeparator()+message);
		}
		printTemplate = (strBuilder.toString());
		int numOfSequence = (printTemplate.length() / 500) + 1;
		for (int i=0; i< numOfSequence; i++)
		{
			wsaaSeq.set(i);
			itemIO.setFunction(varcom.readh);
			itemIO.setDataKey(wsspsmart.itemkey);
			if (isEQ(wsaaSeq, ZERO)) {
				itemIO.setItemseq(SPACES);
			}
			else {
				itemIO.setItemseq(wsaaSeq);
			}
			SmartFileCode.execute(appVars, itemIO);						
			if (((i+1)*500) > printTemplate.length()) {
				numOfLastDataOfPage = printTemplate.length() - i*500;
				if(numOfSequence <= i){
					pageData = "";	
				} else {
					pageData = printTemplate.substring(i*500, i*500+numOfLastDataOfPage);
				}
			} else {
				pageData = printTemplate.substring(i*500, (i+1) * 500);
			}
			itemIO.setGenarea(pageData);		
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				itemIO.setFunction(varcom.writr);
				itemIO.setFormat(itemrec);
			}
			else {
				itemIO.setFunction(varcom.rewrt);
			}
			SmartFileCode.execute(appVars, itemIO);
			
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			
			if (isNE(itemIO.getItemseq(), SPACES)) {
				checkBlankLastRecord3200();
			}
		}
		
		// Delete redundant data
		
		for (int i=numOfSequence; i<=2; i++) {
			wsaaSeq.set(i);		
			itemIO.setFunction(varcom.delet);
			itemIO.setDataKey(wsspsmart.itemkey);
			itemIO.setItemseq(wsaaSeq);			
			SmartFileCode.execute(appVars, itemIO);
		}
		/*
		itemIO.setGenarea(tr386rec.tr386Rec);
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setFunction(varcom.writr);
			itemIO.setFormat(itemrec);
		}
		else {
			itemIO.setFunction(varcom.rewrt);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getItemseq(), SPACES)) {
			checkBlankLastRecord3200();
		}*/
	}

protected void checkChanges3100()
	{
		/*CHECK*/
//		if (isNE(sv.progdescs, tr386rec.progdescs)) {
//			tr386rec.progdescs.set(sv.progdescs);
			wsaaUpdateFlag = "Y";
//		}
		/*EXIT*/
	}

protected void checkBlankLastRecord3200()
	{
		check3200();
	}

protected void check3200()
	{
		tr5agrec.tr5agRec.set(SPACES);
		if (isNE(itemIO.getGenarea(), tr5agrec.tr5agRec)) {
			return ;
		}
		wsaaStoreSeqno.set(itemIO.getItemseq());
		itemIO.setItemseq("2");
		itemIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getItemseq(), wsaaStoreSeqno)) {
			itemIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation4020();
				case continue4020: 
					continue4020();
				case other4080: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation4020()
	{
		if (isNE(scrnparams.statuz, varcom.rolu)
		&& isNE(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.continue4020);
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaSeq.add(1);
		}
		else {
			wsaaSeq.subtract(1);
		}
		goTo(GotoLabel.other4080);
	}

protected void continue4020()
	{
		wsspcomn.programPtr.add(1);
		wsaaFirstTime = "Y";
		wsaaSeq.set(ZERO);
		itemIO.setItemseq(SPACES);
	}

}
