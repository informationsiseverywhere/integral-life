package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MatdclmTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:23
 * Class transformed from MATDCLM.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MatdclmTableDAM extends MatdpfTableDAM {

	public MatdclmTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("MATDCLM");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO"
		             + ", PLNSFX"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", LIENCD";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "TRANNO, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "LIENCD, " +
		            "CRTABLE, " +
		            "SHORTDS, " +
		            "CURRCD, " +
		            "EMV, " +
		            "ACTVALUE, " +
		            "TYPE_T, " +
		            "VRTFUND, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "EFFDATE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO ASC, " +
		            "PLNSFX ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "LIENCD ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO DESC, " +
		            "PLNSFX DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "LIENCD DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               tranno,
                               life,
                               jlife,
                               coverage,
                               rider,
                               planSuffix,
                               liencd,
                               crtable,
                               shortds,
                               currcd,
                               estMatValue,
                               actvalue,
                               fieldType,
                               virtualFund,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               effdate,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(41);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ getPlanSuffix().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getLiencd().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, liencd);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(tranno.toInternal());
	nonKeyFiller40.setInternal(life.toInternal());
	nonKeyFiller60.setInternal(coverage.toInternal());
	nonKeyFiller70.setInternal(rider.toInternal());
	nonKeyFiller80.setInternal(planSuffix.toInternal());
	nonKeyFiller90.setInternal(liencd.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(132);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getJlife().toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ getCrtable().toInternal()
					+ getShortds().toInternal()
					+ getCurrcd().toInternal()
					+ getEstMatValue().toInternal()
					+ getActvalue().toInternal()
					+ getFieldType().toInternal()
					+ getVirtualFund().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getEffdate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, shortds);
			what = ExternalData.chop(what, currcd);
			what = ExternalData.chop(what, estMatValue);
			what = ExternalData.chop(what, actvalue);
			what = ExternalData.chop(what, fieldType);
			what = ExternalData.chop(what, virtualFund);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public FixedLengthStringData getLiencd() {
		return liencd;
	}
	public void setLiencd(Object what) {
		liencd.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getShortds() {
		return shortds;
	}
	public void setShortds(Object what) {
		shortds.set(what);
	}	
	public FixedLengthStringData getCurrcd() {
		return currcd;
	}
	public void setCurrcd(Object what) {
		currcd.set(what);
	}	
	public PackedDecimalData getEstMatValue() {
		return estMatValue;
	}
	public void setEstMatValue(Object what) {
		setEstMatValue(what, false);
	}
	public void setEstMatValue(Object what, boolean rounded) {
		if (rounded)
			estMatValue.setRounded(what);
		else
			estMatValue.set(what);
	}	
	public PackedDecimalData getActvalue() {
		return actvalue;
	}
	public void setActvalue(Object what) {
		setActvalue(what, false);
	}
	public void setActvalue(Object what, boolean rounded) {
		if (rounded)
			actvalue.setRounded(what);
		else
			actvalue.set(what);
	}	
	public FixedLengthStringData getFieldType() {
		return fieldType;
	}
	public void setFieldType(Object what) {
		fieldType.set(what);
	}	
	public FixedLengthStringData getVirtualFund() {
		return virtualFund;
	}
	public void setVirtualFund(Object what) {
		virtualFund.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		planSuffix.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		liencd.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		jlife.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		crtable.clear();
		shortds.clear();
		currcd.clear();
		estMatValue.clear();
		actvalue.clear();
		fieldType.clear();
		virtualFund.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		effdate.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}