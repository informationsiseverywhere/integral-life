package com.csc.life.terminationclaims.dataaccess.model;
import java.io.Serializable;


/**
 * 
 * @author hxu32 related databaase table NOTIPF
 *
 */
public class Notipf implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// NOTIFICOY
	private String notificoy;
	// NOTIFINUM
	private String notifinum;
	// NOTIFICNUM
	private String notificnum;
	// CLAIMANT
	private String claimant;
	// RELATIONCNUM
	private String relationcnum;
	// NOTIFIDATE
	private int notifidate;
	// INCURDATE
	private int incurdate;
	// LOCATION
	private String location;
	// INCIDENTT
	private String incidentt;
	// DEATHDATE
	private int deathdate;
	// CAUSEDEATH
	private String causedeath;
	// RCLAIMREASON
	private String rclaimreason;
	// CLAIMAT
	private double claimat;
	// DOCTOR
	private String doctor;
	// MEDICALPD
	private String medicalpd;
	// HOSPITALLE
	private String hospitalle;
	// DIAGNOSIS
	private String diagnosis;
	// ADMINTDATE
	private int admintdate;
	// DISCHARGEDATE
	private int dischargedate;
	// ACCIDENTDESC
	private String accidentdesc;
	// VALIDFLAG
	private String validFlag;
	// TRANSACTION NUMBER
	private String Transno;
	// USRPRF
	private String usrprf;
	// NOTIFISTATUS
	private String notifistatus;
	// UNIQUE_NUMBER
	private long uniqueNumber;
	//JOBNM
	private String jobnm;
	//DATIME
	private String datime;
	
	//CHDRNUM
	private String chdrnum;
	

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}


	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	

	public String getNotificoy() {
		return notificoy;
	}

	public void setNotificoy(String notificoy) {
		this.notificoy = notificoy;
	}

	public String getNotifinum() {
		return notifinum;
	}

	public void setNotifinum(String notifinum) {
		this.notifinum = notifinum;
	}

	public String getNotificnum() {
		return notificnum;
	}

	public void setNotificnum(String notificnum) {
		this.notificnum = notificnum;
	}

	public String getClaimant() {
		return claimant;
	}

	public void setClaimant(String claimant) {
		this.claimant = claimant;
	}



	public int getNotifidate() {
		return notifidate;
	}

	public void setNotifidate(int notifidate) {
		this.notifidate = notifidate;
	}

	public int getIncurdate() {
		return incurdate;
	}

	public void setIncurdate(int incurdate) {
		this.incurdate = incurdate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getIncidentt() {
		return incidentt;
	}

	public void setIncidentt(String incidentt) {
		this.incidentt = incidentt;
	}

	public int getDeathdate() {
		return deathdate;
	}

	public void setDeathdate(int deathdate) {
		this.deathdate = deathdate;
	}

	public String getCausedeath() {
		return causedeath;
	}

	public void setCausedeath(String causedeath) {
		this.causedeath = causedeath;
	}

	public String getRclaimreason() {
		return rclaimreason;
	}

	public void setRclaimreason(String rclaimreason) {
		this.rclaimreason = rclaimreason;
	}

	public double getClaimat() {
		return claimat;
	}

	public void setClaimat(double claimat) {
		this.claimat = claimat;
	}

	public String getDoctor() {
		return doctor;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	public String getMedicalpd() {
		return medicalpd;
	}

	public void setMedicalpd(String medicalpd) {
		this.medicalpd = medicalpd;
	}

	public String getHospitalle() {
		return hospitalle;
	}

	public void setHospitalle(String hospitalle) {
		this.hospitalle = hospitalle;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public int getAdmintdate() {
		return admintdate;
	}

	public void setAdmintdate(int admintdate) {
		this.admintdate = admintdate;
	}

	public int getDischargedate() {
		return dischargedate;
	}

	public void setDischargedate(int dischargedate) {
		this.dischargedate = dischargedate;
	}

	public String getAccidentdesc() {
		return accidentdesc;
	}

	public void setAccidentdesc(String accidentdesc) {
		this.accidentdesc = accidentdesc;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getNotifistatus() {
		return notifistatus;
	}

	public void setNotifistatus(String notifistatus) {
		this.notifistatus = notifistatus;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getRelationcnum() {
		return relationcnum;
	}

	public void setRelationcnum(String relationcnum) {
		this.relationcnum = relationcnum;
	}

	public String getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

	public String getTransno() {
		return Transno;
	}

	public void setTransno(String transno) {
		Transno = transno;
	}

}
