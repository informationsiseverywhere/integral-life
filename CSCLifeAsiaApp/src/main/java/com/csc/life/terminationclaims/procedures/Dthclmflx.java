/*
 * File: Dthclmflx.java
 * Date: 24 Feb 2015 00:50:00
 * Author: Dennis Puhawan
 * 
 *
 * 
 * Copyright (2015) CSC Asia, all rights reserved.
 */

package com.csc.life.terminationclaims.procedures;


import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getRemainder;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.life.contractservicing.dataaccess.TaxdbilTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.terminationclaims.recordstructures.Dthclmflxrec;
import com.csc.life.enquiries.dataaccess.AcblenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.quipoz.COBOLFramework.util.StringUtil;

/**
* <pre>
*REMARKS.
*
*
*        DEATH CLAIM FLEXIBILITY BASED ON PRODUCT TYPE.
*        -------------------------------------------
*
*                 PROGRAM NAME DTHCLMFLX
*
* This program is an item entry on T5688, contract processing rules table. 
* This method is used in order to calculate unpaid premium and excess premium in suspense
* update the relevant sub-accounts. This module is being triggered by P5256 (Death Claim Register),
* P5318 (Death Claim Adjustment/Enquiry) and P5284 (Death Claim Approval).
*
*
* PROCESSING.
* ----------
* 
*
*****************************************************************
* </pre>
*/

public class Dthclmflx extends SMARTCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();	
	private final String wsaaSubr = "DTHCLMFLX";
	
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	
	private AcblenqTableDAM acblenqIO = new AcblenqTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private TaxdbilTableDAM taxdbilIO = new TaxdbilTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Tr517rec tr517rec = new Tr517rec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private T6654rec t6654rec = new T6654rec();	
	private Dthclmflxrec dthclmflxrec = new Dthclmflxrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	
	private ZonedDecimalData wsaaTotTax = new ZonedDecimalData(17, 2);
	private FixedLengthStringData wsaaTr517Item = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaTr517Ix = new ZonedDecimalData(2, 0);
	private String wsaaWaiveTax = "";
	private PackedDecimalData wsaaProratePrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAnnPrem = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaTotFreqFactor = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaUpperDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaDaysFactor = new ZonedDecimalData(11, 5);
	private int intTotFreqFactor;

	
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);	
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);	
	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Mop = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1);	
	
	private static final String tr52e = "TR52E";
	private static final String tr52d = "TR52D";	
	private static final String f109 = "F109";	
	
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		addTax1100,
		exit1100,
		exit1110,
		itemCall7023
	}	
	
	public Dthclmflx() {
		super();
	}

	/**
	* The mainline method is the default entry point to the class
	*/
	public void mainline(Object... parmArray)
	{
		dthclmflxrec.dthclmflxrec = convertAndSetParam(dthclmflxrec.dthclmflxrec, parmArray, 0);
		try {
			startCalc010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startCalc010()
	{
	dthclmflxrec.status.set(varcom.oK);	
	/*
	chdrclmIO.setFunction(varcom.retrv);
	SmartFileCode.execute(appVars, chdrclmIO);
	if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(chdrclmIO.getParams());
		fatalError9000();
	}
	*/
		retrieveSuspense();
		readChdr();
		retrieveGracePeriod();
		
		if (isFreqExist()==true){
			datcon3rec.intDate1.set(dthclmflxrec.ptdate);
			datcon3rec.intDate2.set(dthclmflxrec.dtofdeath);
			datcon3rec.frequency.set("DY");			
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				syserrrec.statuz.set(datcon3rec.statuz);
				fatalError9000();
			}
			wsaaDaysFactor.set(datcon3rec.freqFactor);
			if (isLTE(wsaaDaysFactor.toInt(), t6654rec.nonForfietureDays.toInt())
			&& 	(isGTE(wsaaDaysFactor.toInt(),0))){
				if (isNE(chdrIO.billfreq,"00")){
					retrieveUnpaidPrem();
					getTax1100();
				}else{
					dthclmflxrec.nextinsamt.set(ZERO);
				}
			}else{
				dthclmflxrec.nextinsamt.set(ZERO);
			}
		}
		
		exit090();
	}	
	
	protected void retrieveSuspense()
	{
		acblenqIO.setDataArea(SPACES);
		acblenqIO.setRldgcoy(dthclmflxrec.chdrChdrcoy);
		acblenqIO.setRldgacct(dthclmflxrec.chdrChdrnum);
		acblenqIO.setSacscode("LP");
		acblenqIO.setSacstyp("S");
		acblenqIO.setFunction(varcom.begn);
		acblenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acblenqIO.setFitKeysSearch("RLDGCOY", "SACSCODE","SACSTYP");
		SmartFileCode.execute(appVars, acblenqIO);
		if (isNE(acblenqIO.getStatuz(), varcom.oK)
		&& isNE(acblenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acblenqIO.getParams());
			fatalError9000();
		}
		dthclmflxrec.susamt.set(acblenqIO.getSacscurbal());
	}
	
	protected void readChdr(){
		chdrIO.setParams(SPACES);
		chdrIO.setChdrpfx("CH");
		chdrIO.setChdrcoy(dthclmflxrec.chdrChdrcoy);
		chdrIO.setChdrnum(dthclmflxrec.chdrChdrnum);
		chdrIO.setCurrfrom(dthclmflxrec.effdate);
		chdrIO.setValidflag("1");
		chdrIO.setFunction(varcom.readr);
		chdrIO.setFormat(formatsInner.chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if (isEQ(chdrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError9000();
		}		
	}	
	
	protected void retrieveUnpaidPrem()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(dthclmflxrec.chdrChdrcoy);
		payrIO.setChdrnum(dthclmflxrec.chdrChdrnum);		
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError9000();
		}
				
		if (isEQ(chdrIO.getBillfreq(),"01")){
			if (isNE(chdrIO.ptdate,chdrIO.btdate)){
				wsaaAnnPrem.set(payrIO.getSinstamt06());
				dthclmflxrec.nextinsamt.set(wsaaAnnPrem);
			}else{
				dthclmflxrec.nextinsamt.set(ZERO);
			}
		}else{
			wsaaAnnPrem.set(mult(payrIO.getSinstamt06(),chdrIO.getBillfreq()));
			getProratedPrem();
		}
	}
	
	protected void getProratedPrem(){		
		getContAge();
		getContYTD();
		getTotFreqPaid();
		computeProratedPrem();
	}	
	
	protected void getContAge()
	{
		datcon3rec.intDate1.set(chdrIO.getCcdate());
		datcon3rec.intDate2.set(chdrIO.getPtdate());
		datcon3rec.frequency.set(chdrIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		wsaaFreqFactor.set(datcon3rec.freqFactor);
	}	
	
	protected void getContYTD(){
		int intFreqFactor = wsaaFreqFactor.toInt();
		datcon2rec.intDate1.set(chdrIO.getCcdate());
		datcon2rec.frequency.set(chdrIO.getBillfreq());
		datcon2rec.freqFactor.set(intFreqFactor);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError9000();
		}
		wsaaUpperDate.set(datcon2rec.intDate2);
	}
	
	protected void getTotFreqPaid(){		
		datcon3rec.intDate1.set(chdrIO.getCcdate());		
		datcon3rec.intDate2.set(wsaaUpperDate);
		datcon3rec.frequency.set(chdrIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		wsaaTotFreqFactor.set(datcon3rec.freqFactor);		
	}
	
	protected void computeProratedPrem(){
		intTotFreqFactor = getRemainder(wsaaTotFreqFactor.toInt(),chdrIO.getBillfreq().toInt());
		wsaaProratePrem.set(sub(wsaaAnnPrem,mult(div(intTotFreqFactor,chdrIO.getBillfreq().toInt()),wsaaAnnPrem)));
		dthclmflxrec.nextinsamt.set(wsaaProratePrem);
	}	
	
	protected void retrieveGracePeriod()
	{
		String key;
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		if(BTPRO028Permission) {
			key = chdrIO.getBillchnl().toString().trim() + dthclmflxrec.cnttype.toString().trim() + chdrIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key = chdrIO.getBillchnl().toString().trim().concat(dthclmflxrec.cnttype.toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = chdrIO.getBillchnl().toString().trim().concat("*****");
					if(!readT6654(key)) {
						syserrrec.params.set(itemIO.getParams());
						syserrrec.statuz.set(itemIO.getStatuz());
						fatalError9000();
					}
				}
			}
		}
		else {
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(dthclmflxrec.chdrChdrcoy);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6654);
		wsaaT6654Key.set(SPACES);
		wsaaT6654Mop.set(chdrIO.getBillchnl());
		wsaaT6654Cnttype.set(dthclmflxrec.cnttype);
		itemIO.setItemitem(wsaaT6654Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		else {
			wsaaT6654Cnttype.set("***");
			itemIO.setItemitem(wsaaT6654Key);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError9000();
			}
			else {
				t6654rec.t6654Rec.set(itemIO.getGenarea());
			}
		}
		}
		
    }
	protected boolean readT6654(String key) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy("2");
		itempf.setItemtabl(tablesInner.t6654.toString());
		itempf.setItemitem(key);
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null != itempf) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			return true;
		}
		return false;
	}

	protected Boolean isFreqExist(){
	    int count = 1;
	    String strFreq;
	    while (count < t6654rec.zrfreq.length) {
	    	strFreq = t6654rec.zrfreq[count].toString();
	    	if (isEQ(strFreq,chdrIO.getBillfreq())){
	    		return true;
	    	}else{
	    		count++;
	    	}
	    }
	    return false;	
	}

	protected void getTax1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1100();
				case addTax1100: 
					addTax1100();
				case exit1100: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start1100()
	{
		wsaaTotTax.set(ZERO);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dthclmflxrec.chdrChdrcoy);
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(dthclmflxrec.register);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(dthclmflxrec.chdrChdrcoy);
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError9000();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit1100);
		}
		if (isEQ(dthclmflxrec.ptdate, dthclmflxrec.btdate)) {
			calcTax5000();
			goTo(GotoLabel.addTax1100);
		}
		taxdbilIO.setStatuz(varcom.oK);
		taxdbilIO.setChdrcoy(dthclmflxrec.chdrChdrcoy);
		taxdbilIO.setChdrnum(dthclmflxrec.chdrChdrnum);
		taxdbilIO.setInstfrom(dthclmflxrec.ptdate);
		taxdbilIO.setLife(SPACES);
		taxdbilIO.setCoverage(SPACES);
		taxdbilIO.setRider(SPACES);
		taxdbilIO.setTrantype(SPACES);
		taxdbilIO.setPlansfx(ZERO);
		taxdbilIO.setFunction(varcom.begn);
		taxdbilIO.setFormat(formatsInner.taxdbilrec);
		while ( !(isEQ(taxdbilIO.getStatuz(), varcom.endp))) {
			processTax1110();
		}
	}

	protected void addTax1100()
	{		
		dthclmflxrec.nextinsamt.add(wsaaTotTax);
	}

	protected void processTax1110()
	{
		try {
			start1110();
			nextRec1110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void start1110()
	{
		SmartFileCode.execute(appVars, taxdbilIO);
		if (isNE(taxdbilIO.getStatuz(), varcom.oK)
		&& isNE(taxdbilIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(taxdbilIO.getStatuz());
			syserrrec.params.set(taxdbilIO.getParams());
			fatalError9000();
		}
		if (isNE(taxdbilIO.getChdrcoy(), dthclmflxrec.chdrChdrcoy)
		|| isNE(taxdbilIO.getChdrnum(), dthclmflxrec.chdrChdrnum)
		|| isNE(taxdbilIO.getInstfrom(), dthclmflxrec.ptdate)
		|| isEQ(taxdbilIO.getStatuz(), varcom.endp)) {
			taxdbilIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1110);
		}
		if (isEQ(chdrIO.getBillfreq(),"01")){
			if (isNE(chdrIO.ptdate,chdrIO.btdate)){
				if (isNE(taxdbilIO.getTxabsind(1), "Y")) {
					wsaaTotTax.add(taxdbilIO.getTaxamt(1));
				}
				if (isNE(taxdbilIO.getTxabsind(2), "Y")) {
					wsaaTotTax.add(taxdbilIO.getTaxamt(2));
				}
				if (isNE(taxdbilIO.getTxabsind(3), "Y")) {
					wsaaTotTax.add(taxdbilIO.getTaxamt(3));
				}										
			}
		}else{
			if (isNE(taxdbilIO.getTxabsind(1), "Y")) {
				wsaaTotTax.add(mult(taxdbilIO.getTaxamt(1),sub(chdrIO.getBillfreq(),intTotFreqFactor)));
			}
			if (isNE(taxdbilIO.getTxabsind(2), "Y")) {
				wsaaTotTax.add(mult(taxdbilIO.getTaxamt(2),sub(chdrIO.getBillfreq(),intTotFreqFactor)));
			}
			if (isNE(taxdbilIO.getTxabsind(3), "Y")) {
				wsaaTotTax.add(mult(taxdbilIO.getTaxamt(3),sub(chdrIO.getBillfreq(),intTotFreqFactor)));
			}			
			
		}		

	}

	protected void nextRec1110()
	{
		taxdbilIO.setFunction(varcom.nextr);
	}

	protected void calcTax5000()
	{
		start5000();
	}

	protected void start5000()
	{
		wsaaTotTax.set(ZERO);
		/* Read all active coverages for the contract                      */
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrnum(dthclmflxrec.chdrChdrnum);
		covrenqIO.setChdrcoy(dthclmflxrec.chdrChdrcoy);
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		while ( !(isEQ(covrenqIO.getStatuz(), varcom.endp))) {
			processCovrTax5100();
		}
		
		if (isGT(payrIO.getSinstamt02(), 0)) {
			processCtfeeTax5200();
		}
	}

	protected void processCovrTax5100()
	{
		start5100();
	}

	protected void processCtfeeTax5200()
	{
		start5200();
	}

	protected void start5100()
	{
		/* Calculate tax on premiums.                                      */
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			fatalError9000();
		}
		if (isNE(covrenqIO.getChdrcoy(), dthclmflxrec.chdrChdrcoy)
		|| isNE(covrenqIO.getChdrnum(), dthclmflxrec.chdrChdrnum)
		|| isEQ(covrenqIO.getStatuz(), varcom.endp)) {
			covrenqIO.setStatuz(varcom.endp);
			return ;
		}
		/* If installment premium = zero, exit                             */
		if (isEQ(covrenqIO.getInstprem(), ZERO)) {
			covrenqIO.setFunction(varcom.nextr);
			return ;
		}
		
		/*  Read table TR52E.                                              */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(dthclmflxrec.cnttype);
		wsaaTr52eCrtable.set(covrenqIO.getCrtable());
		readTr52e5400();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(dthclmflxrec.cnttype);
			wsaaTr52eCrtable.set("****");
			readTr52e5400();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e5400();
		}
		/*  If TR52E tax indicator not = 'Y', do not calculate tax         */
		if (isNE(tr52erec.taxind01, "Y")) {
			covrenqIO.setFunction(varcom.nextr);
			return ;
		}
		/*  If PAYR-SINSTAMT05 not = zero, check to see if component       */
		/* exists in TR517                                                 */
		if (isNE(payrIO.getSinstamt05(), ZERO)
		&& isEQ(covrenqIO.getCrtable(), wsaaTr517Item)
		&& isEQ(tr517rec.zrwvflg01, "Y")) {
			/*         waiving itself                                        */
			covrenqIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaTr517Ix.set(ZERO);
		if (isNE(payrIO.getSinstamt05(), ZERO)) {
			wsaaWaiveTax = " ";
			for (wsaaTr517Ix.set(1); !(isGT(wsaaTr517Ix, 50)); wsaaTr517Ix.add(1)){
				if (isEQ(tr517rec.ctable[wsaaTr517Ix.toInt()], SPACES)) {
					wsaaTr517Ix.set(51);
				}
				else {
					if (isEQ(tr517rec.ctable[wsaaTr517Ix.toInt()], covrenqIO.getCrtable())) {
						wsaaWaiveTax = "Y";
						wsaaTr517Ix.set(51);
					}
				}
			}
			/*  If component found in TR517, do not calculate tax              */
			/*  as this means the premium of the component is being waived     */
			if (isEQ(wsaaWaiveTax, "Y")) {
				covrenqIO.setFunction(varcom.nextr);
				return ;
			}
		}
		/*  Set values to tax linkage and call tax subroutine              */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.transType.set("PREM");
		txcalcrec.chdrcoy.set(covrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(covrenqIO.getChdrnum());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(dthclmflxrec.cntcurr);
		wsaaCntCurr.set(dthclmflxrec.cntcurr);
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.amountIn.set(ZERO);
		if (isEQ(tr52erec.zbastyp, "Y")) {
			if (isEQ(chdrIO.getBillfreq(),"01")){
				if (isNE(chdrIO.ptdate,chdrIO.btdate)){
					compute(txcalcrec.amountIn, 3).setRounded(sub(covrenqIO.getInstprem(), covrenqIO.getZlinstprem()));
				}
			}else{
				compute(txcalcrec.amountIn, 3).setRounded(mult(sub(covrenqIO.getInstprem(), covrenqIO.getZlinstprem()),sub(chdrIO.getBillfreq(),intTotFreqFactor)));
			}							
		}
		else {
			if (isEQ(chdrIO.getBillfreq(),"01")){
				if (isNE(chdrIO.ptdate,chdrIO.btdate)){
					txcalcrec.amountIn.setRounded(covrenqIO.getInstprem());
				}
			}else{
				txcalcrec.amountIn.setRounded(mult(covrenqIO.getInstprem(),sub(chdrIO.getBillfreq(),intTotFreqFactor)));
			}			
			
		}
		txcalcrec.effdate.set(dthclmflxrec.ptdate);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError9000();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[2]);
			}
		}
		/*  Read next COVRENQ                                              */
		covrenqIO.setFunction(varcom.nextr);
	}

	protected void readTr52e5400()
	{
		start5400();
	}

	protected void start5400()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(dthclmflxrec.chdrChdrcoy);
		itdmIO.setItemtabl(tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(dthclmflxrec.ptdate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (((isNE(itdmIO.getItemcoy(), dthclmflxrec.chdrChdrcoy))
		|| (isNE(itdmIO.getItemtabl(), tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			itdmIO.setItemtabl(tr52e);
			itdmIO.setItemitem(wsaaTr52eKey);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f109);
			fatalError9000();
		}
		if (((isEQ(itdmIO.getItemcoy(), dthclmflxrec.chdrChdrcoy))
		&& (isEQ(itdmIO.getItemtabl(), tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

	protected void start5200()
	{
		/* Calculate tax on contract fee.                                  */
		if (isNE(payrIO.getSinstamt05(), ZERO)
		&& isEQ(tr517rec.zrwvflg03, "Y")) {
			return ;
		}
		/*  Read table TR52E.                                              */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(dthclmflxrec.cnttype);
		wsaaTr52eCrtable.set(covrenqIO.getCrtable());
		readTr52e5400();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(dthclmflxrec.cnttype);
			wsaaTr52eCrtable.set("****");
			readTr52e5400();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e5400();
		}
		/*  If TR52E tax indicator2 not 'Y', do not calculate tax          */
		if (isNE(tr52erec.taxind02, "Y")) {
			covrenqIO.setFunction(varcom.nextr);
			return ;
		}
		/*  Set values to tax linkage and call tax subroutine              */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.transType.set("CNTF");
		txcalcrec.chdrcoy.set(dthclmflxrec.chdrChdrcoy);
		txcalcrec.chdrnum.set(dthclmflxrec.chdrChdrnum);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(dthclmflxrec.cntcurr);
		wsaaCntCurr.set(dthclmflxrec.cntcurr);
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.effdate.set(dthclmflxrec.ptdate);
		txcalcrec.amountIn.set(ZERO);
		if (isEQ(chdrIO.getBillfreq(),"01")){
			if (isNE(chdrIO.ptdate,chdrIO.btdate)){
				txcalcrec.amountIn.set(payrIO.getSinstamt02());
			}
		}else{
			txcalcrec.amountIn.set(mult(payrIO.getSinstamt02(),sub(chdrIO.getBillfreq(),intTotFreqFactor)));
		}					
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError9000();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTotTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}
	
	protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
{
	/*    MOVE BOMB                   TO PARM-STATUZ.          <D60401>*/
	dthclmflxrec.status.set(varcom.bomb);
	/*EXIT*/
	exitProgram();
}
protected void exit090()
{
	exitProgram();
}

	private static final class FormatsInner { 
		private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
		private FixedLengthStringData taxdbilrec = new FixedLengthStringData(10).init("TAXDBILREC");
		private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
		private FixedLengthStringData chdrrec = new FixedLengthStringData(10).init("CHDRREC");
	}

	private static final class TablesInner { 
		/* TABLES */	
	private FixedLengthStringData t6654 = new FixedLengthStringData(5).init("T6654");
	}
	
	
	
}