package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:20
 * Description:
 * Copybook name: CHDRMATKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrmatkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrmatFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrmatKey = new FixedLengthStringData(64).isAPartOf(chdrmatFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrmatChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrmatKey, 0);
  	public FixedLengthStringData chdrmatChdrnum = new FixedLengthStringData(8).isAPartOf(chdrmatKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrmatKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrmatFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrmatFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}