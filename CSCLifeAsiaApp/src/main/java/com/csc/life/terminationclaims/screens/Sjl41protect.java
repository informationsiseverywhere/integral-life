package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for PROTECT
 * @version 1.0 generated on 10/03/20 05:41
 * @author Sparikh8
 */
public class Sjl41protect extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

	/**
	 * Writes a record to the screen.
	 * @param errorInd - will be set on if an error occurs
	 * @param noRecordFoundInd - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
		Sjl41ScreenVars sv = (Sjl41ScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.Sjl41protectWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl41ScreenVars screenVars = (Sjl41ScreenVars)pv;
		ScreenRecord.setClassStringFormatting(pv);
	}

	/**
	 * Clear all the variables in SJL42protect
	 */
	public static void clear(VarModel pv) {
		Sjl41ScreenVars screenVars = (Sjl41ScreenVars) pv;
		ScreenRecord.clear(pv);
	}
}
