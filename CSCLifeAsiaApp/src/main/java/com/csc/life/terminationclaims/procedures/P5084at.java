/*
 * File: P5084at.java
 * Date: 30 August 2009 0:05:17
 * Author: Quipoz Limited
 *
 * Class transformed from P5084AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.List;

import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrmjaTableDAM;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SvltclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;	
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*AUTHOR.
*REMARKS
*
* P5084AT - Full Surrender AT Module.
* -----------------------------------
*
* This AT module is called by the Full Surrender Claims  program   5084
* and  the  Contract number  is passed in the AT parameters area a  the
* "primary key".
*
* NOTE:
*
*          - where "NEW  SUMMARISED"  is  mentioned  it implies th
*               value  contained   in   the  suffix  field  on  th
*               surrender claim header record SURHCLM.
*
*          - where "OLD  SUMMARISED"  is  mentioned  it implies th
*               value  contained   in   the  suffix  field  on  th
*               contract header record CHDRCLM.
*
*
*     AT PROCESSING
*
*     INITIALISATION.
*     --------------
*
*     Read the contract header using the CHDRLIF logical with
*     the key from the AT linkage parameters.
*
*     Breakout
*     --------
*     Read through all SURH records to find the one with the
*     lowest Plan Suffix. If this is a notional policy
*     surrender, call BRKOUT passing the current number of
*     policies summarised as the old summary and the plan
*     suffix established in the loop above as the new number
*     summarised.
*
*     There will in practice be only a single SURH record even
*     for multipolicy surrenders. For such transactions, where
*     more than one policy has been surrendered, the SURH will
*     contain the PLAN SUFFIX of the lowest numbered POLICY
*     to have been surrendered.
*
*     Read the Surrender Header record using the SURHCLM logical
*     with the key from the AT linkage parameters.
*
*     Read T5679 to get valid status codes for this transaction.
*     This table is keyed by transaction code.
*
*     Get today's business date by calling DATCON1.
*
*     Read table T5645 to get the accounting rules for this
*     transaction.
*
*     Read the description file for table T5645 to get the descrip ion
*     for this transaction.
*
*     Read table T5688 to get the contract details for this contra t
*     type.
*
*     Check the return status  codes  for  each  of  the  followin
*     records being updated.
*
*     MAIN PROCESSING.
*     ---------------
*
*     Store the surrender header details for later use ; Plan suff x,
*     effective date, tranno, adjustment, contract type, loan deta ls.
*
*     Store the latest surrender date from the surrender header fo
*     later use.
*
*     Whole Plan Surrender (Plan Suffix = 0).
*     --------------------
*
*       Process Coverages.
*       -----------------
*
*       Loop through the Coverage file using the COVR logical for  ll
*       records on this contract header and with Plan Suffix = 0 a d
*
*       For each coverage record,
*
*         If validflag = '2'
*             bypass processing and go to next record
*
*         Update 'old' record.
*         -------------------
*         Set Validflag = '2'
*         Set Current to date = to latest effective date from SURH
*             stored above
*
*         Use UPDAT function to update the record.
*
*         Write 'new' record.
*         ------------------
*         Read T5687 to get the component details for this coverag
*         type.
*
*         Set Validflag = '1'.
*         Set Current to date = Max Date.
*         Set Current From Date = Surrender Effective date
*         Set Transaction Number to value from Surrender Header Re ord
*
*         Check the component has a valid status by reading T5679
*         details for risk and premium statuses.
*         If invalid skip to next Coverage record.
*
*         Set Coverage risk and premium status codes to the 'set t '
*         codes from T5679.
*
*         Set Date, Time, User and Termid.
*
*         Write the new record using WRITR.
*
*       Process Agent Commission.
*       ------------------------
*
*       Read through all Agent Commission records for this
*       Contract using the AGCM logical.
*
*       For each Agent accumulate values for this agent number,
*       life, coverage, rider, plan suffix combination as follows;
*
*          Override commission earned for this contract
*          Override commission paid for this contract
*          Override commission earned for this component
*          Override commission paid for this component
*          Basic commission earned for this contract
*          Basic commission paid for this contract
*          Basic commission earned for this component
*          Basic commission paid for this component
*
*       Where commission paid exceeds commission earned then updat
*       the agent commission record details with the commission
*       paid = commission earned (basic). Set the validflag on the
*       old record to '2' and write a new validflag '1' record
*       with this information.
*
*       Create postings where appropriate using component level
*       accounting.
*
*         If the accumulator is greater than zero then perform
*         the postings by calling LIFACMV (the accounts to be post d are
*         found on T5645, keyed by program). Set up and pass the
*         linkage area as follows:
*
*              Function               - PSTW
*              Batch key              - from AT linkage
*              Document number        - contract number
*              Transaction number     - transaction no.
*              Journal sequence no.   - 0
*              Sub-account code       - from applicable T5645 entr
*              Sub-account type       - ditto
*              Sub-account GL map     - ditto
*              Sub-account GL sign    - ditto
*              S/acct control total   - ditto
*              Cmpy code (sub ledger) - batch company
*              Cmpy code (GL)         - batch company
*              Subsidiary ledger      - agent number
*              Original currency code - contract currency
*              Original currency amt  - accumulated amount
*              Accounting curr code   - blank (handled by subrouti e)
*              Accounting curr amount - zero (handled by subroutin )
*              Exchange rate          - zero (handled by subroutin )
*              Trans reference        - contract number
*              Trans description      - from transaction code
*                                       description
*              Posting month and year - defaulted
*              Effective date         - Today (Claim Surrender dat )
*              Reconciliation amount  - zero
*              Reconciliation date    - Max date
*              Transaction Id         - from AT linkage
*              Substitution code 1    - contract type
*
*       Process Lives.
*       -------------
*
*       Read through all the Lives on this Contract using the
*       LIFE logical.
*
*       For each Life ;
*
*         Rewrite the current record with valid flag = '2'
*         setting the current To date to the Surrender Effective
*         date.
*
*         Write a new valid flag = '1' record. Increment the
*         Transaction Number and set Current from date to the
*         Surrender Effective Date and Current To date to Max
*         Date.
*
*     Part Plan Surrender (Plan Suffix = passed in from AT).
*     -------------------
*
*       Process Coverages.
*       -----------------
*
*       Loop through the Coverage file using the COVR logical for  ll
*       records on this contract header and with Plan Suffix =
*       Plan Suffix from the Surrender Header.
*
*       Note that for Part Plan Surenders, it is possible to
*       surrender multiple policies within a single transaction.
*       In this case, a single SURH record will be written for
*       multiple SURD records. At this stage it is only the first
*       of such plan suffices to be surrendered. Any further
*       policies surrendered are processed later on in the
*       program during the Letters Processing section. At such
*       time all processing described here (Coverages and
*       Agent Commission records) is repeated for all components
*       associated with those plan suffices. See below.
*       For each coverage record,
*
*         If validflag = '2'
*             bypass processing and go to next record
*
*         Update 'old' record.
*         -------------------
*         Set Validflag = '2'
*         Set Current to date = to latest effective date from SURH
*             stored above
*
*         Use UPDAT function to update the record.
*
*         Write 'new' record.
*         ------------------
*         Read T5687 to get the component details for this coverag
*         type.
*
*         Set Validflag = '1'.
*         Set Current to date = Max Date.
*         Set Current From Date = Surrender Effective date
*         Set Transaction Number to value from Surrender Header Re ord
*
*         Check the component has a valid status by reading T5679
*         details for risk and premium statuses.
*         If invalid skip to next Coverage record.
*
*         Set Coverage risk and premium status codes to the 'set t '
*         codes from T5679.
*
*         Set Date, Time, User and Termid.
*
*         Write the new record using WRITR.
*
*       Process Agent Commission.
*       ------------------------
*
*       Read through all Agent Commission records for this
*       Contract using the AGCM logical.
*
*       For each Agent accumulate values for this agent number,
*       life, coverage, rider, plan suffix combination as follows;
*
*          Override commission earned for this contract
*          Override commission paid for this contract
*          Override commission earned for this component
*          Override commission paid for this component
*          Basic commission earned for this contract
*          Basic commission paid for this contract
*          Basic commission earned for this component
*          Basic commission paid for this component
*
*       Where commission paid exceeds commission earned then updat
*       the agent commission record details with the commission
*       paid = commission earned (basic). Set the validflag on the
*       old record to '2' and write a new validflag '1' record
*       with this information.
*
*       Create postings where appropriate using component level
*       accounting.
*
*         If the accumulator is greater than zero then perform
*         the postings by calling LIFACMV (the accounts to be post d are
*         found on T5645, keyed by program). Set up and pass the
*         linkage area as follows:
*
*              Function               - PSTW
*              Batch key              - from AT linkage
*              Document number        - contract number
*              Transaction number     - transaction no.
*              Journal sequence no.   - 0
*              Sub-account code       - from applicable T5645 entr
*              Sub-account type       - ditto
*              Sub-account GL map     - ditto
*              Sub-account GL sign    - ditto
*              S/acct control total   - ditto
*              Cmpy code (sub ledger) - batch company
*              Cmpy code (GL)         - batch company
*              Subsidiary ledger      - agent number
*              Original currency code - contract currency
*              Original currency amt  - accumulated amount
*              Accounting curr code   - blank (handled by subrouti e)
*              Accounting curr amount - zero (handled by subroutin )
*              Exchange rate          - zero (handled by subroutin )
*              Trans reference        - contract number
*              Trans description      - from transaction code
*                                       description
*              Posting month and year - defaulted
*              Effective date         - Today (Claim Surrender dat )
*              Reconciliation amount  - zero
*              Reconciliation date    - Max date
*              Transaction Id         - from AT linkage
*              Substitution code 1    - contract type
*
*       Check for Whole contract having been processed.
*       ----------------------------------------------
*
*       Having processed all the policies for this plan, check
*       to see whether there are any outstanding components still
*       in force.
*
*       Read through the Coverage file using the COVR logical
*       for all components with this Contract Header number.
*       Check each against the T5679 entry to find whether there a e
*       any components remaining with a valid status for this
*       transaction. If there are not then the Contract status can
*       be set to surrendered. Both Risk and Premium Status are
*       checked.
*
*     Process Surrender Details Records.
*     ---------------------------------
*
*     All the outstanding Surrender Detail records are read using
*     the SURDCLM logical.
*
*     For each Surrender Detail record (SURD) ;
*
*         Read T5687 using the component type to get the full
*         surrender processing method.
*
*         Read T6598 using the surrender processing method
*         from above to get the surrender processing subroutine.
*
*         Check for any Increase records using the INCR logical.
*         Invalidate any which do exist by setting the
*         valid flag to '2' and the status to the 'COVR SET TO'
*         status from T5679 (risk and Premium statuses).
*
*         Load the surrender processing linkage area (prefixed
*         by SURP-) with the approriate values from the SURD
*         record and call the surrender processing routine.
*
*     Batch Header.
*     ------------
*
*     Write a Batch Header with a call to BATCUP using the
*     function WRITS.
*
*     Process subsequent Plan Suffix Surrenders.
*     -----------------------------------------
*
*     This is carried out in the 4550-LETTERS section.
*
*     Read again through the Surrender Detail records using the
*     SURDCLM logical.
*
*     As there is only a single SURH record for each transaction
*     (containing the plan suffix of the lowest policy
*     surrendered for multipolicy surrenders) and as the update
*     of coverages and the clawback of Agent commission depend
*     on the PLAN SUFFIX being processed, there will at this
*     point remain some outstanding, unprocessed COVRS with
*     associated Agent Commission requiring clawback.
*
*     e.g. Surrender Policies 10 and 9 of a 10 Policy Plan.
*
*         SURH record : PLAN SUFFIX = 9, i.e. processing has been
*         done for Policy 9 as part of the SURH processing loop
*         earlier. COVR has been updated and Agent Commission has
*         been clawed back.
*         (Note that this information was needed to establish the
*         number of policies to 'Break Out' - in this case the
*         the old figure of 10 policies summarised on the
*         contract header is updated to 8 in the Break Out
*         subroutine).
*
*         Policy 10 is unprocessed at this point.
*
*     For each Surrender Detail still remaining on this
*     contract where the Plan Suffix is different from the
*     Surrender Header Plan Suffix, carry out the Coverage
*     and Agent Commission processing as described above for
*     Part Plan Surrender Processing. See above.
*
*     For each Surrender Detail record, add the details from
*     the SURD to the Working Storage array used to hold the
*     letter details.
*
*     Write Letters.
*     -------------
*
*     Read T6634 using a concatenated key from the Contract
*     Type and Transaction Code to get the letter type for
*     this transaction.
*
*     Write a Letter Request by calling subroutine LETRQST
*     for this letter type passing the Contract Header Number
*     in the LETOKEYS field. Us the function 'ADD'.
*
*     Check to see whether a letters detail record already
*     exists by reading the Surrender Value Letters file
*     using the SVLTCLM logical and a function of READR.
*     If a record already exists for this Contract Header
*     number for this Company then update this by setting
*     the valid flag to '2' and rewriting the record.
*
*     Load the Surrender Value Letter record with the appropriate
*     details from the current transaction and write a 'new'
*     valid flag '1' record using the SVLTCLM logical with a
*     function of WRITR.
*
*     HOUSEKEEPING.
*     ------------
*
*     Old Contract Header/Payer Records.
*     ---------------------------------
*
*     Read and hold the Contract Header record using the CHDRLIF
*     logical and update by setting the valid flag to '2' and the
*     Current To date to the effective date of the surrender.
*
*     Read and hold the Payer record using the PAYR logical
*     and update by setting the valid flag to '2'.
*
*     New Contract Header.
*     -------------------
*
*     Write a new valid flag '1' record using the CHDRLIF logical
*     with the new transaction number and the current from date
*     set to the effective date of the transaction and the
*     Current To date as Max Date. Set the Statuses accordingly
*     using values from the 'Status Set To' values from T5679
*     for this transaction. Update the instalment premium
*     amount by deducting the total surrendered from the previous
*     premium amount.
*
*     Policy Transaction Record.
*     -------------------------
*
*     Write a Policy Transaction Record for this transcation
*     using the PTRN logical.
*
*               - contract key from contract header
*               - transaction number from contract header
*               - transaction effective date equals todays
*               - batch key information from AT linkage.
*
*     New Payer Header.
*     ----------------
*
*     Write a new valid flag '1' record using the PAYR logical
*     with the new transaction number.
*     Update the instalment premium amount by deducting the
*     total surrendered from the previous premium amount.
*
*     Release Softlock.
*     ----------------
*
*     Release the Softlock on this Contract by calling
*     SFTLOCK with a function of UNLK.
*
*     AGENT/GOVERNMENT statistics.
*     ---------------------------
*
*     Call AGENT/GOVERNMENT Statistics Transaction
*     Subroutine - LIFSTTR.
*
*     LIFSTTR
*     ~~~~~~~
*     This subroutine has two basic functions;
*     a) To reverse Statistical movement records.
*     b) To create  Statistical movement records.
*
*     The STTR file will hold all the relevant details of
*     selected transactions which affect a contract and have
*     to have a statistical record written for it.
*
*     Two new coverage logicals have been created for the
*     Statistical subsystem. These are COVRSTS and COVRSTA.
*     COVRSTS allow only validflag 1 records, and COVRSTA
*     allows only validflag 2 records.
*     Another logical AGCMSTS has been set up for the agent's
*     commission records. This allows only validflag 1 records
*     to be read.
*     The other logical AGCMSTA set up for the old agent's
*     commission records, allows only validflag 2 records,
*     dormant flag 'Y' to be read.
*
*     This subroutine will be included in various AT's
*     which affect contracts/coverages. It is designed to
*     track a policy through its life and report on any
*     changes to it. The statistical records produced form
*     the basis for the Agent Statistical subsystem.
*
*     The three tables used for Statistics are;
*         T6627, T6628, T6629.
*     T6628 has as its item name the transaction code and
*     the contract status of a contract, e.g. T642IF. This
*     table is read first to see if any statistical (STTR)
*     records are required to be written. If not the program
*     will finish without any processing needed. If the table
*     has valid statistical categories, then these are used
*     to access T6629. An item name for T6629 could be 'IF'.
*     On T6629, there are two question fields, which state
*     if agent and/or government details need accumulating.
*
*     The four fields under each question are used to access
*     T6627. These fields refer to Age, Sum insured, Risk term
*     and Premium. T6627 has items which are used to check the
*     value of these fields, e.g. the Age of the policy holder
*     is 35. On T6627, the Age item has several values on
*     To and From fields. These could be 0 - 20, 21 - 40 etc.
*     The Age will be found to be within a certain band, and
*     the band label for that range, e.g. AB, will be moved to
*     the STTR record. The same principle applies for the
*     other T6629 fields.
*
*     T6628 splits the statistical categories into four
*     areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
*     All previous categories refer to COVRSTA records, and
*     current categories refer to COVRSTS records. AGCMSTS
*     records can refer to both. On the coverage logicals,
*     the INCREASE and DECREASE are for the premium, and
*     on the AGCMSTS, these refer to the commission.
*
*     STTR records are written for each valid T6628 category.
*     So for a current record, there could be several current
*     categories, therefore the corresponding number of STTRs
*     will be written. Also, if the premium has increased or
*     decreased, a number of STTRs will be written. This process
*     will be repeated for all the AGCMSTS records attached to
*     that current record.
*
*     When a reversal of STTRs is required, the linkage field
*     LIFS-TRANNOR has the transaction number, which the STTRs
*     have to be reversed back to. The LIFS-TRANNO has the
*     current tranno for a particular coverage. All STTR records
*     are reversed out up to and including the TRANNOR number.
*****************************************************
* </pre>
*/
public class P5084at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5084AT");

	protected FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	protected FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	protected FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	protected PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	protected PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	protected PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	protected FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 16);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	protected ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	protected PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(ZERO);
	protected PackedDecimalData wsaaStartTranno = new PackedDecimalData(5, 0).init(ZERO);
	protected PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaPost01Earned = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaPost02Due = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaAccumulateEarned = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaAccumulatePaid = new PackedDecimalData(17, 2).init(0);
	protected FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaOrigStatcode = new FixedLengthStringData(2).init(SPACES);
	protected ZonedDecimalData wsaaTotCovrsurInstprem = new ZonedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaOvrdAccumEarned = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaOvrdAccumPaid = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPost03Earned = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaPost04Due = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaPost05Due = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaPost06Due = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaCmplvlAccEarned = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaCmplvlAccPaid = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaCmplvlOvrEarned = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaCmplvlOvrPaid = new PackedDecimalData(17, 2).init(0);
	protected FixedLengthStringData wsaaCurrAgntnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	protected FixedLengthStringData wsaaAgcmLife = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaAgcmCoverage = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaAgcmRider = new FixedLengthStringData(2);
	protected PackedDecimalData wsaaAgcmPlanSuffix = new PackedDecimalData(4, 0);
	protected ZonedDecimalData wsaaSvltSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2).isAPartOf(wsaaLetokeys, 1);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2).isAPartOf(wsaaLetokeys, 3);
	private FixedLengthStringData wsaaSurrCurr = new FixedLengthStringData(3).isAPartOf(wsaaLetokeys, 5);
	protected FixedLengthStringData wsaaHoldSurrCurr = new FixedLengthStringData(3);
	protected PackedDecimalData wsaaHoldSuffix = new PackedDecimalData(4, 0);
	protected PackedDecimalData wsaaHoldSurhSuffix = new PackedDecimalData(4, 0);
	protected ZonedDecimalData wsaaSurhEffdate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	protected PackedDecimalData wsaaHoldSurhTranno = new PackedDecimalData(5, 0);
	protected PackedDecimalData wsaaHoldSurhAdj = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaHoldSurhLoan = new PackedDecimalData(17, 2);
	protected FixedLengthStringData wsaaHoldSurhCnttype = new FixedLengthStringData(3);
	protected String wsaaValidStatus = "";
	protected PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).setUnsigned();
		/* WSAA-COVR-KEY */
	protected FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1);
	protected FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2);
	protected PackedDecimalData wsaaCovrPlanSuffix = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/* ERRORS */
	private static final String e308 = "E308";
	protected static final String f294 = "F294";
	private static final String g437 = "G437";
	private static final String h150 = "H150";
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	protected AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	protected ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	protected CovrTableDAM covrIO = new CovrTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	protected IncrTableDAM incrIO = new IncrTableDAM();
	protected IncrmjaTableDAM incrmjaIO = new IncrmjaTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	protected PayrTableDAM payrIO = new PayrTableDAM();
	protected PtrnTableDAM ptrnIO = new PtrnTableDAM();
	protected SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	protected SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	protected SvltclmTableDAM svltclmIO = new SvltclmTableDAM();

	private AcblTableDAM acblIO = new AcblTableDAM();
	protected Batckey wsaaBatckey = new Batckey();

	private Letrqstrec letrqstrec = new Letrqstrec();
	protected Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	protected Brkoutrec brkoutrec = new Brkoutrec();
	private T5645rec t5645rec = new T5645rec();
	protected T5679rec t5679rec = new T5679rec();
	protected T5687rec t5687rec = new T5687rec();
	protected T6598rec t6598rec = new T6598rec();
	protected T5688rec t5688rec = new T5688rec();
	protected Tr384rec tr384rec = new Tr384rec();
	private T1693rec t1693rec = new T1693rec();
	private T7508rec t7508rec = new T7508rec();
	private Th605rec th605rec = new Th605rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	protected Surpcpy surpcpy = new Surpcpy();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	protected Trmreasrec trmreasrec = new Trmreasrec();
	private Crtundwrec crtundwrec = new Crtundwrec();
	protected Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	protected FormatsInner formatsInner = new FormatsInner();
	private SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();
	private TablesInner tablesInner = new TablesInner();
	protected WsaaStoredLetterDetailInner wsaaStoredLetterDetailInner = new WsaaStoredLetterDetailInner();
	boolean susur002Permission = false; 
	private List<Itempf> itdmpfList = null;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpfList = new ArrayList<Covrpf>();
	private Covrpf covrpf = new Covrpf();
	private FixedLengthStringData wsaacrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSurrenderMethod = new FixedLengthStringData(4).init("SC14");
	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaBatchtrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5679Key, 4);


/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT,
		gotLastRecord1124,
		next2455,
		exit2459,
		ctrtLevel2555,
		readNextAgcm2565,
		n2ndPosting2620,
		n2ndPosting2695,
		next3320,
		exit3309,
		readNextAgcm3465,
		exit3659,
		svltRecord5230,
		a200Delet
	}

	public P5084at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		/* Continue processing until contract break.                       */
		/*PERFORM 2000-PROCESS UNTIL SURHCLM-TRANNO NOT = WSAA-TRANNO  */
		/*OR SURHCLM-STATUZ = ENDP.                                */
		processCustomerSpecific();
		
		endOfDrivingFile4000();
		a000Statistics();
		/*EXIT*/
		exitProgram();
	}

	protected void processCustomerSpecific() {
		// TODO Auto-generated method stub
		while (!(isEQ(surhclmIO.getStatuz(), varcom.endp))) {
			process2000();
		}

	}


	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(sysrSyserrRecInner.sysrStatuz, varcom.bomb)) {
			return ;
		}
		sysrSyserrRecInner.sysrSyserrStatuz.set(sysrSyserrRecInner.sysrStatuz);
		if (isNE(sysrSyserrRecInner.sysrSyserrType, "2")) {
			sysrSyserrRecInner.sysrSyserrType.set("1");
		}
		callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1001();
	}

protected void initialise1001()
	{	
		susur002Permission = FeaConfg.isFeatureExist("2", "SUSUR002", appVars, "IT"); 
		wsaaTotCovrsurInstprem.set(0);
		wsaaEffdate.set(0);
		wsaacrtable.set(SPACES);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(tablesInner.t1693);
		itemIO.setItemitem(atmodrec.company);
		itemIO.setFormat(sysrSyserrRecInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		readContractHeader1050();
		/*PERFORM 1100-READ-SURRENDER-HEADER.                          */
		callBreakout1120();
		readSurrenderHeader1100();
		callDatcons1200();
		readStatusCodes1150();
		contractAccounting1500();
		readTableT56881600();
		readTableTh6051800();
		pendingIncrease1300();
		wsaaStoredLetterDetailInner.wsaaStoredLetterDetail.set(SPACES);
		/*  Initialise table numeric items in SVLT rec.                    */
		for (wsaaSvltSub.set(1); !(isGT(wsaaSvltSub, 10)); wsaaSvltSub.add(1)){
			svltclmIO.setEmv(wsaaSvltSub, 0);
			svltclmIO.setActvalue(wsaaSvltSub, 0);
			wsaaStoredLetterDetailInner.wsaaSvltEmv[wsaaSvltSub.toInt()].set(0);
			wsaaStoredLetterDetailInner.wsaaSvltActvalue[wsaaSvltSub.toInt()].set(0);
		}
		wsaaSvltSub.set(ZERO);
	}

protected void readContractHeader1050()
	{
		readContract1051();
	}

protected void readContract1051()
	{
		/*  retrieve the contract header and store the transaction number*/
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction("READR");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		wsaaTranno.set(chdrlifIO.getTranno());
		wsaaStartTranno.set(chdrlifIO.getTranno());
		/*    Increment the transaction numbers.                           */
		wsaaTranno.add(1);
		wsaaStartTranno.add(1);
		/* Read the PAYR file to obtain the Contract Billing frequency     */
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Initial read of the Claim Header.                           *
	* </pre>
	*/
protected void readSurrenderHeader1100()
	{
		readSurrender1101();
	}

protected void readSurrender1101()
	{
		surhclmIO.setDataArea(SPACES);
		surhclmIO.setChdrcoy(atmodrec.company);
		surhclmIO.setChdrnum(wsaaPrimaryChdrnum);
		surhclmIO.setTranno(wsaaTranno);
		surhclmIO.setPlanSuffix(ZERO);
		surhclmIO.setFunction("BEGN");
		surhclmIO.setFormat(formatsInner.surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(surhclmIO.getParams());
			xxxxFatalError();
		}
		wsaaPlanSuffix.set(surhclmIO.getPlanSuffix());
	}

	/**
	* <pre>
	* Call the BRKOUT subroutine to breakout COVR and AGCM        *
	* INCR (Increase Pending) records are similarly broken out.   *
	* </pre>
	*/
protected void callBreakout1120()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					go1121();
					read1122();
				case gotLastRecord1124:
					gotLastRecord1124();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go1121()
	{
		/* We have to find out the smallest plan suffix of the surrenders  */
		/* (if more than one) since the last transaction. Because of the   */
		/* key structure of SURCHCLM, we have to read the whole file       */
		/* until contract break to find out what the smallest plan suffix  */
		/* is.                                                             */
		/* Example: If we have a plan of 10 summarised policies and if     */
		/*          we select policy number 10, 9, 8, and 7 to be          */
		/*          surrenderd. we would have the following records in     */
		/*          this order on the file:-                               */
		/* Company  Cont. No. Tran. No  Suffix                             */
		/*   01     00000123    00001    0010                              */
		/*   01     00000123    00002    0009                              */
		/*   01     00000123    00003    0008                              */
		/*   01     00000123    00004    0007                              */
		/* Now you see why!                                                */
		surhclmIO.setDataArea(SPACES);
		surhclmIO.setChdrcoy(atmodrec.company);
		surhclmIO.setChdrnum(wsaaPrimaryChdrnum);
		surhclmIO.setTranno(wsaaTranno);
		surhclmIO.setPlanSuffix(ZERO);
		surhclmIO.setFunction("BEGN");
		surhclmIO.setFormat(formatsInner.surhclmrec);
	}

protected void read1122()
	{
		SmartFileCode.execute(appVars, surhclmIO);
		if ((isNE(surhclmIO.getStatuz(), varcom.oK))
		&& (isNE(surhclmIO.getStatuz(), varcom.endp))) {
			sysrSyserrRecInner.sysrParams.set(surhclmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(surhclmIO.getChdrcoy(), atmodrec.company)
		|| isNE(surhclmIO.getChdrnum(), wsaaPrimaryChdrnum)
		|| isEQ(surhclmIO.getStatuz(), varcom.endp)) {
			surhclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.gotLastRecord1124);
		}
		wsaaPlanSuffix.set(surhclmIO.getPlanSuffix());
		surhclmIO.setFunction("NEXTR");
		read1122();
		return ;
	}

protected void gotLastRecord1124()
	{
		surhclmIO.setPlanSuffix(wsaaPlanSuffix);
		/*  call the breakout routine in order to breakout the COVR*/
		/*  and the AGCM records - this is done only once for the*/
		/*  first record read from the Surrender header file*/
		if (isGT(surhclmIO.getPlanSuffix(), chdrlifIO.getPolsum())
		|| isEQ(chdrlifIO.getPolsum(), 1)
		|| isEQ(surhclmIO.getPlanSuffix(), 0)) {
			return ;
		}
		brkoutrec.brkOldSummary.set(chdrlifIO.getPolsum());
		compute(brkoutrec.brkNewSummary, 0).set(sub(surhclmIO.getPlanSuffix(), 1));
		brkoutrec.brkChdrnum.set(chdrlifIO.getChdrnum());
		brkoutrec.brkChdrcoy.set(chdrlifIO.getChdrcoy());
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		brkoutrec.brkStatuz.set(SPACES);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz, "****")) {
			sysrSyserrRecInner.sysrParams.set(brkoutrec.brkStatuz);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Read the status codes for the contract type.                *
	* </pre>
	*/
protected void readStatusCodes1150()
	{
		readStatusCodes1151();
	}

protected void readStatusCodes1151()
	{
		covrpf.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		covrpf.setChdrnum(chdrlifIO.getChdrnum().toString());
		covrpf.setLife("01");
		covrpf.setCoverage("01");
		covrpf.setRider("00");
		covrpfList = covrpfDAO.selectCovrClmData(covrpf);
		for(Covrpf c : covrpfList) {
			wsaacrtable.set(c.getCrtable());
		}
		itdmpfList = itemDAO.getItdmByFrmdate(atmodrec.company.toString(), tablesInner.t5687.toString(), wsaacrtable.toString(), wsaaToday.toInt());
		for(Itempf item : itdmpfList) {
			t5687rec.t5687Rec.set(StringUtil.rawToString(item.getGenarea()));
		}
		if(t5687rec.svMethod.equals(wsaaSurrenderMethod)) {
			wsaaBatchtrcde.set(wsaaBatckey.batcBatctrcde);
			wsaaCnttype.set(chdrlifIO.getCnttype());
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(atmodrec.company);
			itemIO.setItemtabl(tablesInner.t5679);
			itemIO.setItemitem(wsaaT5679Key);
			itemIO.setFunction("READR");
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		else {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(atmodrec.company);
			itemIO.setItemtabl(tablesInner.t5679);
			itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
			itemIO.setFunction("READR");
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
	}

	/**
	* <pre>
	* Call DATCON3 subroutine.                                    *
	* </pre>
	*/
protected void callDatcons1200()
	{
		callDatcons1201();
		datcon11205();
	}

protected void callDatcons1201()
	{
		/* Get the frequency Factor from DATCON3 for Regular premiums.*/
		/* IF CHDRLIF-BILLFREQ         = '00'                           */
		if (isEQ(payrIO.getBillfreq(), "00")) {
			datcon3rec.freqFactor.set(1);
			return ;
		}
		datcon3rec.intDate1.set(chdrlifIO.getCcdate());
		datcon3rec.intDate2.set(chdrlifIO.getBillcd());
		datcon3rec.frequency.set(chdrlifIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(datcon3rec.datcon3Rec);
			xxxxFatalError();
		}
	}

protected void datcon11205()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}

protected void pendingIncrease1300()
	{
		/*BEGNH*/
		/* Check for any pending Increase records (Validflag = '1')     */
		/* for the contract and delete them.                            */
		incrmjaIO.setParams(SPACES);
		incrmjaIO.setChdrcoy(atmodrec.company);
		incrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		incrmjaIO.setLife(ZERO);
		incrmjaIO.setCoverage(ZERO);
		incrmjaIO.setRider(ZERO);
		incrmjaIO.setPlanSuffix(ZERO);
		incrmjaIO.setFunction(varcom.begnh);
		incrmjaIO.setFormat(formatsInner.incrmjarec);
		while ( !(isEQ(incrmjaIO.getStatuz(), varcom.endp))) {
			deletePendingIncrs1350();
		}

		/*EXIT*/
	}

protected void deletePendingIncrs1350()
	{
		delete1360();
	}

protected void delete1360()
	{
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)
		&& isNE(incrmjaIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrStatuz.set(incrmjaIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(incrmjaIO.getParams());
			xxxxFatalError();
		}
		/* If the end of the file has been reached, leave the section.  */
		if (isEQ(incrmjaIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* If the record retrieved is not for the correct contract,     */
		/* rewrite the record to release it, move ENDP to the file      */
		/* status and leave the section.                                */
		if (isNE(incrmjaIO.getChdrcoy(), atmodrec.company)
		|| isNE(incrmjaIO.getChdrnum(), wsaaPrimaryChdrnum)) {
			incrmjaIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, incrmjaIO);
			if (isNE(incrmjaIO.getStatuz(), varcom.oK)) {
				sysrSyserrRecInner.sysrStatuz.set(incrmjaIO.getStatuz());
				sysrSyserrRecInner.sysrParams.set(incrmjaIO.getParams());
				xxxxFatalError();
			}
			incrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		/* Update the COVR record for this pending increase, setting    */
		/* the indexation indication to spaces to force Pending         */
		/* Increases to reprocess the component.                        */
		resetCovrIndexation1400();
		/* Delete the record then move NEXTR to the function to look    */
		/* for the next record.                                         */
		incrmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(incrmjaIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(incrmjaIO.getParams());
			xxxxFatalError();
		}
		incrmjaIO.setFunction(varcom.nextr);
	}

protected void resetCovrIndexation1400()
	{
		readh1410();
	}

protected void readh1410()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(incrmjaIO.getChdrcoy());
		covrIO.setChdrnum(incrmjaIO.getChdrnum());
		covrIO.setLife(incrmjaIO.getLife());
		covrIO.setCoverage(incrmjaIO.getCoverage());
		covrIO.setRider(incrmjaIO.getRider());
		covrIO.setPlanSuffix(incrmjaIO.getPlanSuffix());
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			xxxxFatalError();
		}
		covrIO.setIndexationInd(SPACES);
		covrIO.setFunction(varcom.rewrt);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(covrIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			xxxxFatalError();
		}
	}

protected void contractAccounting1500()
	{
		go1501();
	}

protected void go1501()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem("P5084");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrlifIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("P5084");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(descIO.getParams());
			xxxxFatalError();
		}
	}

protected void readTableT56881600()
	{
		read1610();
	}

protected void read1610()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrlifIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");





		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), chdrlifIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrlifIO.getCnttype());
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(e308);
			xxxxFatalError();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTableTh6051800()
	{
		start1810();
	}

protected void start1810()
	{
		/* Read TH605 to see whether system is "Override based on Agent    */
		/* Details"                                                        */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(atmodrec.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	* Do everything.                                              *
	* </pre>
	*/
protected void process2000()
	{
		go2001();
	}

protected void go2001()
	{
		/*  store fields of the surrender header for use later when*/
		/*  calling the Surrender processing sub routine*/
		surpcpy.effdate.set(surhclmIO.getEffdate());
		/*  <020> Store the fields form the the SURH.                      */
		wsaaHoldSurhSuffix.set(surhclmIO.getPlanSuffix());
		wsaaSurhEffdate.set(surhclmIO.getEffdate());
		wsaaHoldSurhTranno.set(surhclmIO.getTranno());
		wsaaHoldSurhAdj.set(surhclmIO.getOtheradjst());
		wsaaHoldSurhCnttype.set(surhclmIO.getCnttype());
		wsaaHoldSurhLoan.set(surhclmIO.getPolicyloan());
		wsaaHoldSurrCurr.set(surhclmIO.getCurrcd());
		/* Get the latest Surrendering date.                               */
		if (isGT(surhclmIO.getEffdate(), wsaaEffdate)) {
			wsaaEffdate.set(surhclmIO.getEffdate());
		}
		surpcpy.cnttype.set(surhclmIO.getCnttype());
		if (isEQ(surhclmIO.getPlanSuffix(), 0)) {
			processWholePlan2050();
		}
		else {
			partPlanSurrender3100();
		}
		/*  Contract Header Update and PTRN write are now done             */
		/*  as part of end processing because at this stage there are      */
		/*  still coverages unprocessed. This does not happen until after  */
		/*  the SURD loop.                                                 */
		/*    PERFORM 4300-WRITE-NEW-CONT-HEADER.                     <020>*/
		/*    PERFORM 4400-PTRN-TRANSACTION.                          <020>*/
		surhclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)
		&& isNE(surhclmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surhclmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(surhclmIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(surhclmIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isEQ(surhclmIO.getStatuz(), varcom.endp)) {
			surhclmIO.setPlanSuffix(wsaaHoldSurhSuffix);
			surhclmIO.setEffdate(wsaaSurhEffdate);
			surhclmIO.setTranno(wsaaHoldSurhTranno);
			surhclmIO.setOtheradjst(wsaaHoldSurhAdj);
			/*    MOVE WSAA-HOLD-SURH-CNTTYPE TO SURHCLM-CNTTYPE.      <021>*/
			/*    MOVE WSAA-HOLD-SURH-LOAN TO SURHCLM-POLICYLOAN.      <021>*/
			surhclmIO.setCnttype(wsaaHoldSurhCnttype);
			surhclmIO.setPolicyloan(wsaaHoldSurhLoan);
			surhclmIO.setCurrcd(wsaaHoldSurrCurr);
			surhclmIO.setStatuz(varcom.endp);
			return ;
		}
	}

protected void processWholePlan2050()
	{
		/*PROCESS*/
		processCoversWhole2400();
		/*    PERFORM 2500-PROCESS-AGCM-WHOLE.                             */
		processAgcmWhole2500();
		processLives2700();
		if(susur002Permission)
		{
		updateAcbl();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Process the related COVERS.                                 *
	* </pre>
	*/
protected void updateAcbl()
{
	acblIO.setRldgcoy(chdrlifIO.getChdrcoy());
	acblIO.setSacscode(t5645rec.sacscode[8]);
	acblIO.setRldgacct(wsaaPrimaryChdrnum);
	acblIO.setOrigcurr(chdrlifIO.getCntcurr());
	acblIO.setSacstyp(t5645rec.sacstype[8]);
	acblIO.setFunction("READH");
	SmartFileCode.execute(appVars, acblIO);
	if (isNE(acblIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(acblIO.getParams());
			xxxxFatalError();
				
	}
	
	lifacmvrec.rcamt.set(ZERO);
	lifacmvrec.contot.set(ZERO);
	lifacmvrec.rcamt.set(ZERO);
	lifacmvrec.frcdate.set(ZERO);
	lifacmvrec.transactionDate.set(ZERO);
	lifacmvrec.transactionTime.set(ZERO);
	lifacmvrec.user.set(ZERO);
	lifacmvrec.function.set("PSTW");
	lifacmvrec.batckey.set(wsaaBatckey);
	lifacmvrec.rdocnum.set(wsaaPrimaryChdrnum);
	lifacmvrec.tranno.set(surhclmIO.getTranno());
	lifacmvrec.sacscode.set(t5645rec.sacscode[8]);
	lifacmvrec.sacstyp.set(t5645rec.sacstype[8]);
	lifacmvrec.glcode.set(t5645rec.glmap[8]);
	lifacmvrec.glsign.set(t5645rec.sign[8]);
	lifacmvrec.contot.set(t5645rec.cnttot[8]);
	lifacmvrec.jrnseq.set(ZERO);
	lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
	lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
	/*MOVE CHDRLIF-CHDRNUM        TO LIFA-RLDGACCT.           <011>*/
	lifacmvrec.rldgacct.set(wsaaPrimaryChdrnum);
	lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
	lifacmvrec.origamt.set(mult(acblIO.getSacscurbal(),-1));
	lifacmvrec.genlcur.set(SPACES);
	lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
	lifacmvrec.acctamt.set(ZERO);
	lifacmvrec.crate.set(ZERO);
	lifacmvrec.postyear.set(SPACES);
	lifacmvrec.postmonth.set(SPACES);
	lifacmvrec.tranref.set(surhclmIO.getTranno());
	lifacmvrec.trandesc.set(descIO.getLongdesc());
	lifacmvrec.effdate.set(wsaaToday);
	lifacmvrec.frcdate.set("99999999");
	lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
	lifacmvrec.substituteCode[6].set(SPACES);
	lifacmvrec.termid.set(wsaaTermid);
	lifacmvrec.user.set(wsaaUser);
	lifacmvrec.transactionTime.set(wsaaTransactionTime);
	lifacmvrec.transactionDate.set(wsaaTransactionDate);
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz, "****")) {
		sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
		xxxxFatalError();
	}
	/* Override Commission                                             */
	if (isEQ(th605rec.indic, "Y")) {
		zorlnkrec.annprem.set(agcmIO.getAnnprem());
		b100CallZorcompy();
	}
}

protected void processCoversWhole2400()
	{
		/*COVERS*/
		/* Read coverages.*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(atmodrec.company);
		covrIO.setChdrnum(chdrlifIO.getChdrnum());
		covrIO.setPlanSuffix(0);
		covrIO.setFunction(varcom.begn);
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {

			//performance improvement --  atiwari23
			covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

			updateComponent2450();
		}

		/*EXIT*/
	}

protected void updateComponent2450()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2450();
				case next2455:
					next2455();
				case exit2459:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2450()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			xxxxFatalError();
		}
		if (isNE(covrIO.getChdrcoy(), atmodrec.company)
		|| isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2459);
		}
		if (isEQ(covrIO.getValidflag(), "2")) {
			covrIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit2459);
		}
		processReassurance2570();
		/* Update the Covr record with a valid flag of 2*/
		covrIO.setValidflag("2");
		covrIO.setCurrto(wsaaEffdate);
		covrIO.setFunction(varcom.updat);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			xxxxFatalError();
		}
		/* Access the table to initialise the COVR fields.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");



		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrlifIO.getCnttype());
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setCurrfrom(surhclmIO.getEffdate());
		/*MOVE WSAA-TRANNO            TO COVR-TRANNO.                  */
		covrIO.setTranno(surhclmIO.getTranno());
		/*  Check to see if component has valid status for update.         */
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			riskStatusCheckWp2920();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				premStatusCheckWp2940();
			}
		}
		if (isEQ(wsaaValidStatus, "N")) {
			goTo(GotoLabel.next2455);
		}
		covrIO.setStatcode(t5679rec.setCovRiskStat);
		if (isEQ(covrIO.getRider(), "00")
		|| isEQ(covrIO.getRider(), SPACES)) {
			covrIO.setStatcode(t5679rec.setCovRiskStat);
		}
		else {
			covrIO.setStatcode(t5679rec.setRidRiskStat);
		}
		/* IF  CHDRLIF-BILLFREQ            NOT = '00'                   */
		if (isNE(payrIO.getBillfreq(), "00")
		&& isNE(t5687rec.singlePremInd, "Y")) {
			if (isEQ(covrIO.getRider(), SPACES)
			|| isEQ(covrIO.getRider(), "00")) {
				covrIO.setPstatcode(t5679rec.setCovPremStat);
			}
			else {
				covrIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		else {
			if (isEQ(covrIO.getRider(), SPACES)
			|| isEQ(covrIO.getRider(), "00")) {
				covrIO.setPstatcode(t5679rec.setSngpCovStat);
			}
			else {
				covrIO.setPstatcode(t5679rec.setSngpRidStat);
			}
		}
	}

	/**
	* <pre>
	****        MOVE T5679-SET-SNGP-COV-STAT TO COVR-PSTATCODE.
	* </pre>
	*/
protected void next2455()
	{
		/* Update the INCR records if necessary.                        */
		wsaaCovrChdrcoy.set(covrIO.getChdrcoy());
		wsaaCovrChdrnum.set(covrIO.getChdrnum());
		wsaaCovrLife.set(covrIO.getLife());
		wsaaCovrCoverage.set(covrIO.getCoverage());
		wsaaCovrRider.set(covrIO.getRider());
		wsaaCovrPlanSuffix.set(covrIO.getPlanSuffix());
		incrCheck6000();
		/*ILIFE-6384 : Start
		 * Update COVR only for valid status
		 * */
		if (isEQ(wsaaValidStatus, "Y")) {
			covrIO.setTransactionDate(wsaaTransactionDate);
			covrIO.setTransactionTime(wsaaTransactionTime);
			covrIO.setUser(wsaaUser);
			covrIO.setTermid(wsaaTermid);
			covrIO.setFunction(varcom.writr);
			covrIO.setFormat(formatsInner.covrrec);
			SmartFileCode.execute(appVars, covrIO);
			if (isNE(covrIO.getStatuz(), varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
				xxxxFatalError();
			}
		}/*ILIFE-6384*/
		covrIO.setFunction(varcom.nextr);
	}

protected void processAgcmWhole2500()
	{
		read2501();
	}

protected void read2501()
	{
		/*  begin on the agcm file and update the agent records*/
		/*  for this coverage*/
		/* Read agent records*/
		agcmIO.setParams(SPACES);
		agcmIO.setChdrcoy(atmodrec.company);
		agcmIO.setChdrnum(chdrlifIO.getChdrnum());
		agcmIO.setPlanSuffix(0);
		/*    MOVE BEGN                   TO AGCM-FUNCTION.                */
		/* MOVE BEGNH                  TO AGCM-FUNCTION.           <012>*/
		agcmIO.setFunction(varcom.begn);



		//performance improvement --  atiwari23
		agcmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");


		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), varcom.oK)
		&& isNE(agcmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(agcmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(agcmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrnum(), agcmIO.getChdrnum())
		|| isNE(atmodrec.company, agcmIO.getChdrcoy())
		|| isNE(agcmIO.getStatuz(), varcom.oK)) {
			/*    AGCM-STATUZ              = ENDP                           */
			/*    MOVE AGCM-PARAMS          TO SYSR-PARAMS                  */
			/*    MOVE AGCM-STATUZ          TO SYSR-STATUZ          <A06092>*/
			/*    PERFORM XXXX-FATAL-ERROR                                  */
			agcmIO.setStatuz(varcom.endp);
		}
		else {
			wsaaAgcmLife.set(agcmIO.getLife());
			wsaaAgcmCoverage.set(agcmIO.getCoverage());
			wsaaAgcmRider.set(agcmIO.getRider());
			wsaaAgcmPlanSuffix.set(agcmIO.getPlanSuffix());
			wsaaAgntnum.set(agcmIO.getAgntnum());
		}
		while ( !(isEQ(agcmIO.getStatuz(), varcom.endp))) {
			readAndAccumAgent2550();
		}

	}

	/**
	* <pre>
	* Read all the agcm records per agent and accumulate earned   *
	* and paid commission                                         *
	* </pre>
	*/
protected void readAndAccumAgent2550()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					read2551();
				case ctrtLevel2555:
					ctrtLevel2555();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read2551()
	{
		while ( !(isNE(agcmIO.getAgntnum(), wsaaAgntnum)
		|| isNE(agcmIO.getLife(), wsaaAgcmLife)
		|| isNE(agcmIO.getCoverage(), wsaaAgcmCoverage)
		|| isNE(agcmIO.getRider(), wsaaAgcmRider)
		|| isNE(agcmIO.getPlanSuffix(), wsaaAgcmPlanSuffix)
		|| isEQ(agcmIO.getStatuz(), varcom.endp))) {
			accum2560();
		}

		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			if ((isGT(wsaaCmplvlAccPaid, wsaaCmplvlAccEarned))) {
				compute(wsaaPost05Due, 2).set(sub(wsaaCmplvlAccPaid, wsaaCmplvlAccEarned));
				wsaaCurrAgntnum.set(agcmIO.getAgntnum());
				postToSubaccounts2600();
			}
			if ((isGT(wsaaCmplvlOvrPaid, wsaaCmplvlOvrEarned))) {
				compute(wsaaPost06Due, 2).set(sub(wsaaCmplvlOvrPaid, wsaaCmplvlOvrEarned));
				wsaaCurrAgntnum.set(agcmIO.getAgntnum());
				postOverrideErnDue2691();
			}
		}
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			goTo(GotoLabel.ctrtLevel2555);
		}
		if (isGT(wsaaAccumulatePaid, wsaaAccumulateEarned)) {
			compute(wsaaPost02Due, 2).set(sub(wsaaAccumulatePaid, wsaaAccumulateEarned));
			wsaaCurrAgntnum.set(agcmIO.getAgntnum());
			postToSubaccounts2600();
		}
		if (isGT(wsaaOvrdAccumPaid, wsaaOvrdAccumEarned)) {
			compute(wsaaPost04Due, 2).set(sub(wsaaOvrdAccumPaid, wsaaOvrdAccumEarned));
			wsaaCurrAgntnum.set(agcmIO.getAgntnum());
			postOverrideErnDue2691();
		}
	}

protected void ctrtLevel2555()
	{
		if (isEQ(agcmIO.getStatuz(), varcom.endp)) {
			return ;
		}
		wsaaAgcmLife.set(agcmIO.getLife());
		wsaaAgcmCoverage.set(agcmIO.getCoverage());
		wsaaAgcmRider.set(agcmIO.getRider());
		wsaaAgcmPlanSuffix.set(agcmIO.getPlanSuffix());
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			wsaaCmplvlAccEarned.set(ZERO);
			wsaaCmplvlAccPaid.set(ZERO);
			wsaaCmplvlOvrEarned.set(ZERO);
			wsaaCmplvlOvrPaid.set(ZERO);
		}
		if ((isNE(agcmIO.getAgntnum(), wsaaAgntnum))) {
			wsaaAccumulateEarned.set(ZERO);
			wsaaAccumulatePaid.set(ZERO);
			wsaaOvrdAccumEarned.set(ZERO);
			wsaaOvrdAccumPaid.set(ZERO);
			wsaaAgntnum.set(agcmIO.getAgntnum());
		}
	}

protected void accum2560()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					read2561();
					rewrt2563();
					writr2564();
				case readNextAgcm2565:
					readNextAgcm2565();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read2561()
	{
		if (isEQ(agcmIO.getOvrdcat(), "O")) {
			wsaaOvrdAccumEarned.add(agcmIO.getComern());
			wsaaOvrdAccumPaid.add(agcmIO.getCompay());
			wsaaCmplvlOvrEarned.add(agcmIO.getComern());
			wsaaCmplvlOvrPaid.add(agcmIO.getCompay());
		}
		else {
			wsaaAccumulateEarned.add(agcmIO.getComern());
			wsaaAccumulatePaid.add(agcmIO.getCompay());
			wsaaCmplvlAccEarned.add(agcmIO.getComern());
			wsaaCmplvlAccPaid.add(agcmIO.getCompay());
		}
		/*    IF COMMISSION EARNED IS = COMMISSION PAYED, WE WILL          */
		/*    NOT NEED TO UPDATE THE AGCM RECORD.                          */
		if (isEQ(agcmIO.getComern(), agcmIO.getCompay())) {
			goTo(GotoLabel.readNextAgcm2565);
		}
		/*    IF THE COMMISSION EARNED IS NOT = THE COMMISSION             */
		/*    PAID, WE WILL SET THE "OLD" RECORD TO VALIDFLAG 2            */
		/*    AND CREATE A NEW RECORD WHERE THE COMMISSION EARNED          */
		/*    IS = THE COMMISSION PAYED.                                   */
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmIO.getAgntnum());
		agcmdbcIO.setLife(agcmIO.getLife());
		agcmdbcIO.setCoverage(agcmIO.getCoverage());
		agcmdbcIO.setRider(agcmIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(agcmdbcIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void rewrt2563()
	{
		agcmdbcIO.setValidflag("2");
		agcmdbcIO.setCurrto(wsaaEffdate);
		agcmdbcIO.setFunction(varcom.rewrt);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(agcmdbcIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void writr2564()
	{
		/* correct tranno, and commission paid equal to earned.            */
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrfrom(wsaaEffdate);
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setCompay(agcmIO.getComern());
		agcmdbcIO.setTranno(wsaaTranno);
		agcmdbcIO.setFunction(varcom.writr);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(agcmdbcIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	****                                                         <012>
	**** MOVE AGCM-COMERN             TO AGCM-COMPAY.            <012>
	**** MOVE REWRT                   TO AGCM-FUNCTION.          <012>
	****                                                         <012>
	**** CALL 'AGCMIO'                USING AGCM-PARAMS.         <012>
	****                                                         <012>
	**** IF AGCM-STATUZ               NOT = O-K                  <012>
	****     MOVE AGCM-PARAMS         TO SYSR-PARAMS             <012>
	****     PERFORM XXXX-FATAL-ERROR.                           <012>
	* </pre>
	*/
protected void readNextAgcm2565()
	{
		agcmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), "****")
		&& isNE(agcmIO.getStatuz(), "ENDP")) {
			sysrSyserrRecInner.sysrParams.set(agcmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(agcmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrnum(), agcmIO.getChdrnum())
		|| isNE(atmodrec.company, agcmIO.getChdrcoy())
		|| isEQ(agcmIO.getStatuz(), varcom.endp)) {
			agcmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processReassurance2570()
	{
		trmreas2571();
	}

protected void trmreas2571()
	{
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("TRMR");
		trmreasrec.chdrcoy.set(covrIO.getChdrcoy());
		trmreasrec.chdrnum.set(covrIO.getChdrnum());
		trmreasrec.life.set(covrIO.getLife());
		trmreasrec.coverage.set(covrIO.getCoverage());
		trmreasrec.rider.set(covrIO.getRider());
		trmreasrec.planSuffix.set(covrIO.getPlanSuffix());
		trmreasrec.crtable.set(covrIO.getCrtable());
		trmreasrec.polsum.set(chdrlifIO.getPolsum());
		trmreasrec.cnttype.set(chdrlifIO.getCnttype());
		trmreasrec.effdate.set(surhclmIO.getEffdate());
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(surhclmIO.getTranno());
		trmreasrec.language.set(atmodrec.language);
		trmreasrec.billfreq.set(chdrlifIO.getBillfreq());
		trmreasrec.ptdate.set(chdrlifIO.getPtdate());
		trmreasrec.origcurr.set(chdrlifIO.getCntcurr());
		trmreasrec.acctcurr.set(covrIO.getPremCurrency());
		trmreasrec.crrcd.set(covrIO.getCrrcd());
		trmreasrec.convUnits.set(covrIO.getConvertInitialUnits());
		trmreasrec.jlife.set(covrIO.getJlife());
		trmreasrec.singp.set(covrIO.getSingp());
		trmreasrec.oldSumins.set(covrIO.getSumins());
		trmreasrec.pstatcode.set(covrIO.getPstatcode());
		trmreasrec.clmPercent.set(ZERO);
		trmreasrec.newSumins.set(ZERO);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(trmreasrec.statuz);
			sysrSyserrRecInner.sysrParams.set(trmreasrec.trracalRec);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Post to SUB ACCOUNT 1 the amount EARNED by the agent        *
	* Post to SUB ACCOUNT 2 the amount DUE (CLAWBACK)             *
	* </pre>
	*/
protected void postToSubaccounts2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					read2601();
				case n2ndPosting2620:
					n2ndPosting2620();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read2601()
	{
		/* If component level accounting is required, and we are           */
		/*  still summing for an agent, don't post this 1st posting        */
		/*  until we have a total (ie Agent num has changed) - just        */
		/*  do the 2nd posting                                             */
		/* If Component Level Accounting is NOT required, we are posting   */
		/*  at agent level anyway so we can go and do postings 1 & 2       */
		if (((isEQ(t5688rec.comlvlacc, "Y"))
		&& (isEQ(wsaaAgntnum, wsaaCurrAgntnum))
		&& (isNE(agcmIO.getStatuz(), varcom.endp)))) {
			goTo(GotoLabel.n2ndPosting2620);
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(wsaaPrimaryChdrnum);
		/*MOVE WSAA-TRANNO            TO LIFA-TRANNO.                  */
		lifacmvrec.tranno.set(surhclmIO.getTranno());
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		/* MOVE CHDRLIF-CHDRNUM        TO LIFA-RLDGACCT.                */
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(chdrlifIO.getAgntnum());
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[1]);
		lifacmvrec.glcode.set(t5645rec.glmap[1]);
		lifacmvrec.glsign.set(t5645rec.sign[1]);
		lifacmvrec.contot.set(t5645rec.cnttot[1]);
		/*    MOVE WSAA-POST-01-EARNED    TO LIFA-ORIGAMT.                 */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaPost05Due);
		}
		else {
			lifacmvrec.origamt.set(wsaaPost02Due);
		}
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		/*MOVE WSAA-TRANNO            TO WSAA-TRANNO.                  */
		/*MOVE WSAA-TRANNO            TO LIFA-TRANREF.                 */
		lifacmvrec.tranref.set(surhclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		/*  MOVE WSAA-TODAY             TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(wsaaEffdate);
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		wsaaRldgChdrnum.set(chdrlifIO.getChdrnum());
		wsaaPlan.set(wsaaAgcmPlanSuffix);
		wsaaRldgLife.set(wsaaAgcmLife);
		wsaaRldgCoverage.set(wsaaAgcmCoverage);
		wsaaRldgRider.set(wsaaAgcmRider);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaRldgacct);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmIO.getAnnprem());
			b100CallZorcompy();
		}
	}

protected void n2ndPosting2620()
	{
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			/*      If we are doing Component Level Accounting then we want to */
			/*       set up Substitution code 6 with the Coverage/Rider table  */
			/*       value....so READR on COVRENQ file.                        */
			covrenqIO.setChdrcoy(chdrlifIO.getChdrcoy());
			covrenqIO.setChdrnum(chdrlifIO.getChdrnum());
			covrenqIO.setLife(wsaaAgcmLife);
			covrenqIO.setCoverage(wsaaAgcmCoverage);
			covrenqIO.setRider(wsaaAgcmRider);
			covrenqIO.setPlanSuffix(wsaaAgcmPlanSuffix);
			covrenqIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, covrenqIO);
			if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(covrenqIO.getParams());
				sysrSyserrRecInner.sysrStatuz.set(covrenqIO.getStatuz());
				xxxxFatalError();
			}
			else {
				lifacmvrec.substituteCode[6].set(covrenqIO.getCrtable());
			}
		}
		else {
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(wsaaPrimaryChdrnum);
		/*MOVE WSAA-TRANNO            TO LIFA-TRANNO.                  */
		lifacmvrec.tranno.set(surhclmIO.getTranno());
		/* MOVE T5645-SACSCODE(2)      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE(2)      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP(2)         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN(2)          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT(2)        TO LIFA-CONTOT.                  */
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcur.set(SPACES);
		/* MOVE CHDRLIF-CHDRNUM        TO LIFA-RLDGACCT.                */
		/*        Check for Component level accounting & act accordingly   */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			wsaaRldgChdrnum.set(chdrlifIO.getChdrnum());
			wsaaPlan.set(wsaaAgcmPlanSuffix);
			wsaaRldgLife.set(wsaaAgcmLife);
			wsaaRldgCoverage.set(wsaaAgcmCoverage);
			wsaaRldgRider.set(wsaaAgcmRider);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode[5]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[5]);
			lifacmvrec.glcode.set(t5645rec.glmap[5]);
			lifacmvrec.glsign.set(t5645rec.sign[5]);
			lifacmvrec.contot.set(t5645rec.cnttot[5]);
			lifacmvrec.origamt.set(wsaaPost05Due);
		}
		else {
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(chdrlifIO.getChdrnum());
			lifacmvrec.sacscode.set(t5645rec.sacscode[2]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[2]);
			lifacmvrec.glcode.set(t5645rec.glmap[2]);
			lifacmvrec.glsign.set(t5645rec.sign[2]);
			lifacmvrec.contot.set(t5645rec.cnttot[2]);
			lifacmvrec.origamt.set(wsaaPost02Due);
		}
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		/* MOVE WSAA-POST-02-DUE       TO LIFA-ORIGAMT.                 */
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		/*MOVE WSAA-TRANNO            TO WSAA-TRANNO.                  */
		/*MOVE WSAA-TRANNO            TO LIFA-TRANREF.                 */
		lifacmvrec.tranref.set(surhclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		/*  MOVE WSAA-TODAY             TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(wsaaEffdate);
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void postOverrideErnDue2691()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2691();
				case n2ndPosting2695:
					n2ndPosting2695();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2691()
	{
		/* If Component level Accounting is required, AND we are           */
		/*  still summing for an agent, don't post this 1st posting        */
		/*  until we have a total (ie Agent num has changed) - just        */
		/*  do the 2nd posting                                             */
		/* If Component Level Accounting is NOT required, we are posting   */
		/*  at agent level anyway so we can go and do postings 1 & 2       */
		if (((isEQ(t5688rec.comlvlacc, "Y"))
		&& (isEQ(wsaaAgntnum, wsaaCurrAgntnum))
		&& (isNE(agcmIO.getStatuz(), varcom.endp)))) {
			goTo(GotoLabel.n2ndPosting2695);
		}
		/* Post to SUB ACCOUNT 3 the amount OVERRIDE EARNED (CLAWBACK)   * */
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(wsaaPrimaryChdrnum);
		lifacmvrec.tranno.set(surhclmIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode[3]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[3]);
		lifacmvrec.glcode.set(t5645rec.glmap[3]);
		lifacmvrec.glsign.set(t5645rec.sign[3]);
		lifacmvrec.contot.set(t5645rec.cnttot[3]);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		/*MOVE CHDRLIF-CHDRNUM        TO LIFA-RLDGACCT.           <011>*/
		lifacmvrec.rldgacct.set(chdrlifIO.getAgntnum());
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		/*    MOVE WSAA-POST-03-EARNED    TO LIFA-ORIGAMT.                 */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.origamt.set(wsaaPost06Due);
		}
		else {
			lifacmvrec.origamt.set(wsaaPost04Due);
		}
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(surhclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmIO.getAnnprem());
			b100CallZorcompy();
		}
	}

protected void n2ndPosting2695()
	{
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			/*      If we are doing Component Level Accounting then we want to */
			/*       set up Substitution code 6 with the Coverage/Rider table  */
			/*       value....so READR on COVRENQ file.                        */
			covrenqIO.setChdrcoy(chdrlifIO.getChdrcoy());
			covrenqIO.setChdrnum(chdrlifIO.getChdrnum());
			covrenqIO.setLife(wsaaAgcmLife);
			covrenqIO.setCoverage(wsaaAgcmCoverage);
			covrenqIO.setRider(wsaaAgcmRider);
			covrenqIO.setPlanSuffix(wsaaAgcmPlanSuffix);
			covrenqIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, covrenqIO);
			if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(covrenqIO.getParams());
				sysrSyserrRecInner.sysrStatuz.set(covrenqIO.getStatuz());
				xxxxFatalError();
			}
			else {
				lifacmvrec.substituteCode[6].set(covrenqIO.getCrtable());
			}
		}
		else {
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		/* IF Component level accounting, post to sub account 6, ELSE      */
		/* Post to SUB ACCOUNT 4 the amount OVERRIDE DUE by the agent*     */
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(wsaaPrimaryChdrnum);
		lifacmvrec.tranno.set(surhclmIO.getTranno());
		/* MOVE T5645-SACSCODE(4)      TO LIFA-SACSCODE.                */
		/* MOVE T5645-SACSTYPE(4)      TO LIFA-SACSTYP.                 */
		/* MOVE T5645-GLMAP(4)         TO LIFA-GLCODE.                  */
		/* MOVE T5645-SIGN(4)          TO LIFA-GLSIGN.                  */
		/* MOVE T5645-CNTTOT(4)        TO LIFA-CONTOT.                  */
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		/*MOVE CHDRLIF-CHDRNUM        TO LIFA-RLDGACCT.           <011>*/
		/* MOVE CHDRLIF-AGNTNUM        TO LIFA-RLDGACCT.           <018>*/
		/*        Check for Component level accounting & act accordingly   */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			wsaaRldgChdrnum.set(chdrlifIO.getChdrnum());
			wsaaPlan.set(wsaaAgcmPlanSuffix);
			wsaaRldgLife.set(wsaaAgcmLife);
			wsaaRldgCoverage.set(wsaaAgcmCoverage);
			wsaaRldgRider.set(wsaaAgcmRider);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode[6]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[6]);
			lifacmvrec.glcode.set(t5645rec.glmap[6]);
			lifacmvrec.glsign.set(t5645rec.sign[6]);
			lifacmvrec.contot.set(t5645rec.cnttot[6]);
			lifacmvrec.origamt.set(wsaaPost06Due);
		}
		else {
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(chdrlifIO.getChdrnum());
			lifacmvrec.sacscode.set(t5645rec.sacscode[4]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[4]);
			lifacmvrec.glcode.set(t5645rec.glmap[4]);
			lifacmvrec.glsign.set(t5645rec.sign[4]);
			lifacmvrec.contot.set(t5645rec.cnttot[4]);
			lifacmvrec.origamt.set(wsaaPost04Due);
		}
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		/* MOVE WSAA-POST-04-DUE       TO LIFA-ORIGAMT.                 */
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(surhclmIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, "****")) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Process the lives.                                          *
	* </pre>
	*/
protected void processLives2700()
	{
		/*LIVES*/
		lifeIO.setParams(SPACES);
		lifeIO.setChdrnum(chdrlifIO.getChdrnum());
		lifeIO.setChdrcoy(atmodrec.company);
		lifeIO.setFormat(formatsInner.liferec);
		lifeIO.setCurrfrom(0);
		lifeIO.setFunction("BEGN");
		while ( !(isEQ(lifeIO.getStatuz(), varcom.endp))) {
			updateLives2800();
		}

		/*EXIT*/
	}

	/**
	* <pre>
	* Update life records.                                        *
	* </pre>
	*/
protected void updateLives2800()
	{
		updateRecs2805();
	}

protected void updateRecs2805()
	{
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)
		&& isNE(lifeIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(lifeIO.getStatuz(), varcom.endp)
		|| isNE(lifeIO.getChdrnum(), surhclmIO.getChdrnum())
		|| isNE(atmodrec.company, lifeIO.getChdrcoy())) {
			lifeIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(lifeIO.getValidflag(), "1")) {
			lifeIO.setFunction(varcom.nextr);
			return ;
		}
		/*    MOVE WSAA-TRANSACTION-DATE  TO LIFE-TRANSACTION-DATE.        */
		/*    MOVE WSAA-TRANSACTION-TIME  TO LIFE-TRANSACTION-TIME.        */
		/*    MOVE WSAA-USER              TO LIFE-USER.                    */
		/*    MOVE WSAA-TERMID            TO LIFE-TERMID.                  */
		lifeIO.setValidflag("2");
		lifeIO.setCurrto(surhclmIO.getEffdate());
		lifeIO.setFunction(varcom.updat);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			xxxxFatalError();
		}
		/*MOVE WSAA-TRANNO            TO LIFE-TRANNO.                  */
		lifeIO.setTransactionDate(wsaaTransactionDate);
		lifeIO.setTransactionTime(wsaaTransactionTime);
		lifeIO.setUser(wsaaUser);
		lifeIO.setTermid(wsaaTermid);
		lifeIO.setTranno(surhclmIO.getTranno());
		lifeIO.setValidflag("1");
		lifeIO.setCurrto(varcom.vrcmMaxDate);
		lifeIO.setCurrfrom(surhclmIO.getEffdate());
		if (isEQ(lifeIO.getJlife(), "00")
		|| isEQ(lifeIO.getJlife(), "  ")) {
			lifeIO.setStatcode(t5679rec.setLifeStat);
		}
		else {
			lifeIO.setStatcode(t5679rec.setJlifeStat);
		}
		lifeIO.setFunction(varcom.writr);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			xxxxFatalError();
		}
		lifeIO.setFunction(varcom.nextr);
	}

protected void updateContractHeader2900()
	{
		read2910();
	}

protected void read2910()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		/*ADD 1                       TO CHDRLIF-TRANNO.               */
		/*MOVE WSAA-TRANNO            TO CHDRLIF-TRANNO.          <006>*/
		wsaaOrigStatcode.set(chdrlifIO.getStatcode());
		/*MOVE T5679-SET-CN-RISK-STAT TO CHDRLIF-STATCODE.             */
		chdrlifIO.setPstattran(chdrlifIO.getTranno());
		varcom.vrcmCompTermid.set(wsaaTermid);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransactionTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrlifIO.setTranid(varcom.vrcmCompTranid);
		/*MOVE SURHCLM-EFFDATE        TO CHDRLIF-CURRTO.               */
		chdrlifIO.setCurrto(wsaaEffdate);
		chdrlifIO.setValidflag("2");
		chdrlifIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
	}

protected void riskStatusCheckWp2920()
	{
		/*START*/
		if (isEQ(covrIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void premStatusCheckWp2940()
	{
		/*START*/
		if (isEQ(covrIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void partPlanSurrender3100()
	{
		/*READ*/
		processCoversPartPlan3200();
		/*    PERFORM 3400-PROCESS-AGCM-PART-PLAN.                         */
		processAgcmPartPlan3400();
		readCoverages3600();
		/*EXIT*/
	}

protected void processCoversPartPlan3200()
	{
		/*READ*/
		/* Read coverages.*/
		covrsurIO.setParams(SPACES);
		covrsurIO.setChdrcoy(atmodrec.company);
		covrsurIO.setChdrnum(chdrlifIO.getChdrnum());
		covrsurIO.setPlanSuffix(surhclmIO.getPlanSuffix());
		covrsurIO.setFunction(varcom.begn);
		while ( !(isEQ(covrsurIO.getStatuz(), varcom.endp))) {
			updateComponent3300();
		}

		/*EXIT*/
	}

protected void updateComponent3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3300();
				case next3320:
					next3320();
				case exit3309:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3300()
	{
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)
		&& isNE(covrsurIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(covrsurIO.getParams());
			xxxxFatalError();
		}
		if (isNE(covrsurIO.getChdrcoy(), atmodrec.company)
		|| isNE(covrsurIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isNE(covrsurIO.getPlanSuffix(), surhclmIO.getPlanSuffix())
		|| isEQ(covrsurIO.getStatuz(), varcom.endp)) {
			covrsurIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3309);
		}
		if (isEQ(covrsurIO.getValidflag(), "2")) {
			covrsurIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit3309);
		}
		processReassurance3470();
		/* Update the Covr record with a valid flag of 2*/
		/* Accumulate the premiums of the policies to be surrendered.      */
		/* So that we can deduct it from the instalment premium on         */
		/* the contract header record in  Section 4300-.                   */
		/*  Do not know at this point whether this component was           */
		/*  actually surrendered by this transaction.                      */
		/*    ADD COVRSUR-INSTPREM        TO WSAA-TOT-COVRSUR-INSTPREM.<020*/
		covrsurIO.setValidflag("2");
		/*MOVE SURHCLM-EFFDATE        TO COVRSUR-CRRCD.                */
		covrsurIO.setCurrto(wsaaEffdate);
		covrsurIO.setFunction(varcom.updat);
		covrsurIO.setFormat(formatsInner.covrsurrec);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrsurIO.getParams());
			xxxxFatalError();
		}
		/* Access the table to initialise the COVR fields.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrsurIO.getCrtable());
		itdmIO.setItmfrm(covrsurIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrsurIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(covrsurIO.getCrtable());
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(f294);
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		covrsurIO.setValidflag("1");
		covrsurIO.setCurrto(varcom.vrcmMaxDate);
		covrsurIO.setCurrfrom(surhclmIO.getEffdate());
		/*MOVE CHDRLIF-TRANNO         TO COVRSUR-TRANNO.               */
		covrsurIO.setTranno(surhclmIO.getTranno());
		/*  Check T5679 to see if component has valid status to update.    */
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			riskStatusCheckPp3500();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				premStatusCheckPp3520();
			}
		}
		if (isEQ(wsaaValidStatus, "N")) {
			goTo(GotoLabel.next3320);
		}
		/*  Only increment Instalment Premium Accumulation and             */
		/*  update the statuses for this component if this is              */
		/*  valid status for the transaction ie. IT HAS                    */
		/*  ACTUALLY BEEN SURRENDERED BY THIS TRANSACTION.                 */
		wsaaTotCovrsurInstprem.add(covrsurIO.getInstprem());
		covrsurIO.setStatcode(t5679rec.setCovRiskStat);
		if (isEQ(covrsurIO.getRider(), "00")
		|| isEQ(covrsurIO.getRider(), SPACES)) {
			covrsurIO.setStatcode(t5679rec.setCovRiskStat);
		}
		else {
			covrsurIO.setStatcode(t5679rec.setRidRiskStat);
		}
		/* IF  CHDRLIF-BILLFREQ            NOT = '00'                   */
		if (isNE(payrIO.getBillfreq(), "00")
		&& isNE(t5687rec.singlePremInd, "Y")) {
			if (isEQ(covrsurIO.getRider(), SPACES)
			|| isEQ(covrsurIO.getRider(), "00")) {
				covrsurIO.setPstatcode(t5679rec.setCovPremStat);
			}
			else {
				covrsurIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		else {
			if (isEQ(covrsurIO.getRider(), SPACES)
			|| isEQ(covrsurIO.getRider(), "00")) {
				/*MOVE T5679-SET-SNGP-COV-STAT TO COVRSUR-PSTATCODE     */
				/*        MOVE T5679-SET-SNGP-RID-STAT TO COVRSUR-PSTATCODE<008>*/
				covrsurIO.setPstatcode(t5679rec.setSngpCovStat);
			}
			else {
				/*        MOVE T5679-SET-SNGP-COV-STAT TO COVRSUR-PSTATCODE.    */
				covrsurIO.setPstatcode(t5679rec.setSngpRidStat);
			}
		}
	}

protected void next3320()
	{
		/* Update the INCR records if necessary.                        */
		wsaaCovrChdrcoy.set(covrsurIO.getChdrcoy());
		wsaaCovrChdrnum.set(covrsurIO.getChdrnum());
		wsaaCovrLife.set(covrsurIO.getLife());
		wsaaCovrCoverage.set(covrsurIO.getCoverage());
		wsaaCovrRider.set(covrsurIO.getRider());
		wsaaCovrPlanSuffix.set(covrsurIO.getPlanSuffix());
		incrCheck6000();
		covrsurIO.setTransactionDate(wsaaTransactionDate);
		covrsurIO.setTransactionTime(wsaaTransactionTime);
		covrsurIO.setUser(wsaaUser);
		covrsurIO.setTermid(wsaaTermid);
		covrsurIO.setPayrseqno("1");
		covrsurIO.setFunction(varcom.writr);
		covrsurIO.setFormat(formatsInner.covrsurrec);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(covrsurIO.getParams());
			xxxxFatalError();
		}
		covrsurIO.setFunction(varcom.nextr);
	}

protected void processAgcmPartPlan3400()
	{
		read3401();
	}

protected void read3401()
	{
		/*  begin on the agcm file and update the agent records*/
		/*  for this coverage*/
		/* Read agent records*/
		/* MOVE SPACES                 TO AGCMSUR-PARAMS.               */
		/* MOVE ATMD-COMPANY           TO AGCMSUR-CHDRCOY.              */
		/* MOVE CHDRLIF-CHDRNUM        TO AGCMSUR-CHDRNUM.              */
		/* MOVE SURHCLM-PLAN-SUFFIX    TO AGCMSUR-PLAN-SUFFIX.          */
		/* MOVE BEGN                   TO AGCMSUR-FUNCTION.             */
		/* CALL 'AGCMSURIO'            USING AGCMSUR-PARAMS.            */
		/* IF CHDRLIF-CHDRNUM          NOT = AGCMSUR-CHDRNUM OR         */
		/*    ATMD-COMPANY             NOT = AGCMSUR-CHDRCOY OR         */
		/*    SURHCLM-PLAN-SUFFIX      NOT = AGCMSUR-PLAN-SUFFIX OR     */
		/*    AGCMSUR-STATUZ           NOT = O-K                   <018>*/
		/*    AGCMSUR-STATUZ           = ENDP                           */
		/*    MOVE AGCMSUR-PARAMS       TO SYSR-PARAMS                  */
		/*    PERFORM XXXX-FATAL-ERROR                                  */
		/* ELSE                                                         */
		/*    MOVE AGCMSUR-LIFE        TO WSAA-AGCM-LIFE           <018>*/
		/*    MOVE AGCMSUR-COVERAGE    TO WSAA-AGCM-COVERAGE       <018>*/
		/*    MOVE AGCMSUR-RIDER       TO WSAA-AGCM-RIDER          <018>*/
		/*    MOVE AGCMSUR-PLAN-SUFFIX TO WSAA-AGCM-PLAN-SUFFIX    <018>*/
		/*    MOVE AGCMSUR-AGNTNUM  TO WSAA-AGNTNUM.                    */
		/* PERFORM 3450-READ-AND-ACCUM-AGENT UNTIL                      */
		/*     AGCMSUR-STATUZ = ENDP.                                   */
		agcmIO.setParams(SPACES);
		agcmIO.setChdrcoy(atmodrec.company);
		agcmIO.setChdrnum(chdrlifIO.getChdrnum());
		agcmIO.setPlanSuffix(ZERO);
		agcmIO.setFunction(varcom.begn);

		//performance improvement --  atiwari23
		agcmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), varcom.oK)
		&& isNE(agcmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(agcmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(agcmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrnum(), agcmIO.getChdrnum())
		|| isNE(atmodrec.company, agcmIO.getChdrcoy())
		|| isNE(agcmIO.getStatuz(), varcom.oK)) {
			/*   MOVE AGCM-PARAMS         TO SYSR-PARAMS           <A06092>*/
			/*   MOVE AGCM-STATUZ         TO SYSR-STATUZ           <A06092>*/
			/*   PERFORM XXXX-FATAL-ERROR                          <A06092>*/
			agcmIO.setStatuz(varcom.endp);
		}
		else {
			wsaaAgcmLife.set(agcmIO.getLife());
			wsaaAgcmCoverage.set(agcmIO.getCoverage());
			wsaaAgcmRider.set(agcmIO.getRider());
			wsaaAgcmPlanSuffix.set(agcmIO.getPlanSuffix());
			wsaaAgntnum.set(agcmIO.getAgntnum());
		}
		while ( !(isEQ(agcmIO.getStatuz(), varcom.endp))) {
			readAndAccumAgent3450();
		}

	}

	/**
	* <pre>
	* Read all the agcm records per agent and accumulate earned   *
	* and paid commission                                         *
	* </pre>
	*/
protected void readAndAccumAgent3450()
	{
		read3451();
		ctrtLevel3455();
	}

protected void read3451()
	{
		/*  The same number of records will be written regardless of       */
		/*  whether Component or Contract level accounting.                */
		wsaaCmplvlAccEarned.set(ZERO);
		wsaaCmplvlAccPaid.set(ZERO);
		wsaaCmplvlOvrEarned.set(ZERO);
		wsaaCmplvlOvrPaid.set(ZERO);
		wsaaAccumulateEarned.set(ZERO);
		wsaaAccumulatePaid.set(ZERO);
		wsaaOvrdAccumEarned.set(ZERO);
		wsaaOvrdAccumPaid.set(ZERO);
		/* PERFORM 3460-ACCUM UNTIL                                     */
		/*     AGCMSUR-AGNTNUM NOT = WSAA-AGNTNUM OR                    */
		/*     AGCMSUR-AGNTNUM     NOT = WSAA-AGNTNUM OR           <018>*/
		/*     AGCMSUR-LIFE        NOT = WSAA-AGCM-LIFE OR         <018>*/
		/*     AGCMSUR-COVERAGE    NOT = WSAA-AGCM-COVERAGE OR     <018>*/
		/*     AGCMSUR-RIDER       NOT = WSAA-AGCM-RIDER OR        <018>*/
		/*     AGCMSUR-PLAN-SUFFIX NOT = WSAA-AGCM-PLAN-SUFFIX OR  <018>*/
		/*     AGCMSUR-STATUZ = ENDP.                              <018>*/
		while ( !(isNE(agcmIO.getAgntnum(), wsaaAgntnum)
		|| isNE(agcmIO.getLife(), wsaaAgcmLife)
		|| isNE(agcmIO.getCoverage(), wsaaAgcmCoverage)
		|| isNE(agcmIO.getRider(), wsaaAgcmRider)
		|| isNE(agcmIO.getPlanSuffix(), wsaaAgcmPlanSuffix)
		|| isEQ(agcmIO.getStatuz(), varcom.endp))) {
			accum3460();
		}

		/*    IF ( ( T5688-COMLVLACC NOT = 'Y'      ) AND             <020>*/
		/*         ( WSAA-AGNTNUM = AGCMSUR-AGNTNUM ) AND             <020>*/
		/*         ( AGCMSUR-STATUZ NOT = ENDP      ) )               <020>*/
		/*        GO TO 3455-CTRT-LEVEL.                              <020>*/
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			if ((isGT(wsaaCmplvlAccPaid, wsaaCmplvlAccEarned))) {
				compute(wsaaPost05Due, 2).set(sub(wsaaCmplvlAccPaid, wsaaCmplvlAccEarned));
				/*         MOVE AGCMSUR-AGNTNUM       TO WSAA-CURR-AGNTNUM <018>*/
				wsaaCurrAgntnum.set(agcmIO.getAgntnum());
				postToSubaccounts2600();
			}
			if ((isGT(wsaaCmplvlOvrPaid, wsaaCmplvlOvrEarned))) {
				compute(wsaaPost06Due, 2).set(sub(wsaaCmplvlOvrPaid, wsaaCmplvlOvrEarned));
				/*         MOVE AGCMSUR-AGNTNUM       TO WSAA-CURR-AGNTNUM <018>*/
				wsaaCurrAgntnum.set(agcmIO.getAgntnum());
				postOverrideErnDue2691();
			}
			return ;
		}
		/*  Contract level.                                                */
		if (isGT(wsaaAccumulatePaid, wsaaAccumulateEarned)) {
			compute(wsaaPost02Due, 2).set(sub(wsaaAccumulatePaid, wsaaAccumulateEarned));
			/*     MOVE AGCMSUR-AGNTNUM        TO WSAA-CURR-AGNTNUM    <018>*/
			wsaaCurrAgntnum.set(agcmIO.getAgntnum());
			postToSubaccounts2600();
		}
		if (isGT(wsaaOvrdAccumPaid, wsaaOvrdAccumEarned)) {
			compute(wsaaPost04Due, 2).set(sub(wsaaOvrdAccumPaid, wsaaOvrdAccumEarned));
			/*     MOVE AGCMSUR-AGNTNUM        TO WSAA-CURR-AGNTNUM    <018>*/
			wsaaCurrAgntnum.set(agcmIO.getAgntnum());
			postOverrideErnDue2691();
		}
	}

protected void ctrtLevel3455()
	{
		/* IF AGCMSUR-STATUZ       = ENDP                          <018>*/
		if (isEQ(agcmIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/*    MOVE AGCM-LIFE              TO WSAA-AGCM-LIFE.          <020>*/
		/*    MOVE AGCM-COVERAGE          TO WSAA-AGCM-COVERAGE.      <020>*/
		/*    MOVE AGCM-RIDER             TO WSAA-AGCM-RIDER.         <020>*/
		/*    MOVE AGCM-PLAN-SUFFIX       TO WSAA-AGCM-PLAN-SUFFIX.   <020>*/
		/* MOVE AGCMSUR-LIFE           TO WSAA-AGCM-LIFE.          <020>*/
		/* MOVE AGCMSUR-COVERAGE       TO WSAA-AGCM-COVERAGE.      <020>*/
		/* MOVE AGCMSUR-RIDER          TO WSAA-AGCM-RIDER.         <020>*/
		/* MOVE AGCMSUR-PLAN-SUFFIX    TO WSAA-AGCM-PLAN-SUFFIX.   <020>*/
		/* MOVE AGCMSUR-AGNTNUM        TO WSAA-AGNTNUM.            <020>*/
		wsaaAgcmLife.set(agcmIO.getLife());
		wsaaAgcmCoverage.set(agcmIO.getCoverage());
		wsaaAgcmRider.set(agcmIO.getRider());
		wsaaAgcmPlanSuffix.set(agcmIO.getPlanSuffix());
		wsaaAgntnum.set(agcmIO.getAgntnum());
		/*EXIT*/
	}

protected void accum3460()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					read3461();
					rewrt3463();
					writr3464();
				case readNextAgcm3465:
					readNextAgcm3465();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read3461()
	{
		/* IF AGCMSUR-OVRDCAT        = 'O'                         <011>*/
		/*    ADD AGCMSUR-COMERN     TO   WSAA-OVRD-ACCUM-EARNED   <011>*/
		/*    ADD AGCMSUR-COMPAY     TO   WSAA-OVRD-ACCUM-PAID     <011>*/
		/*    ADD AGCMSUR-COMERN     TO   WSAA-CMPLVL-OVR-EARNED   <016>*/
		/*    ADD AGCMSUR-COMPAY     TO   WSAA-CMPLVL-OVR-PAID     <016>*/
		/* ELSE                                                    <011>*/
		/*    ADD AGCMSUR-COMERN     TO   WSAA-ACCUMULATE-EARNED   <011>*/
		/*    ADD AGCMSUR-COMPAY     TO   WSAA-ACCUMULATE-PAID     <011>*/
		/*    ADD AGCMSUR-COMERN     TO   WSAA-CMPLVL-ACC-EARNED   <016>*/
		/*    ADD AGCMSUR-COMPAY     TO   WSAA-CMPLVL-ACC-PAID     <016>*/
		/* END-IF.                                                 <011>*/
		if (isEQ(agcmIO.getOvrdcat(), "O")) {
			wsaaOvrdAccumEarned.add(agcmIO.getComern());
			wsaaOvrdAccumPaid.add(agcmIO.getCompay());
			wsaaCmplvlOvrEarned.add(agcmIO.getComern());
			wsaaCmplvlOvrPaid.add(agcmIO.getCompay());
		}
		else {
			wsaaAccumulateEarned.add(agcmIO.getComern());
			wsaaAccumulatePaid.add(agcmIO.getCompay());
			wsaaCmplvlAccEarned.add(agcmIO.getComern());
			wsaaCmplvlAccPaid.add(agcmIO.getCompay());
		}
		/* If commission earned is = commission paid, we will              */
		/* not need to update the AGCM file.                               */
		if (isEQ(agcmIO.getComern(), agcmIO.getCompay())) {
			goTo(GotoLabel.readNextAgcm3465);
		}
		/* If the commission earned is not = the commission                */
		/* paid, we will set the "old" record to validflag 2               */
		/* and create a new record where the commission earned             */
		/* is = the commission paid.                                       */
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmIO.getAgntnum());
		agcmdbcIO.setLife(agcmIO.getLife());
		agcmdbcIO.setCoverage(agcmIO.getCoverage());
		agcmdbcIO.setRider(agcmIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(agcmdbcIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void rewrt3463()
	{
		agcmdbcIO.setValidflag("2");
		agcmdbcIO.setCurrto(wsaaEffdate);
		agcmdbcIO.setFunction(varcom.rewrt);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(agcmdbcIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void writr3464()
	{
		/* correct tranno, and commission paid equal to earned.            */
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrfrom(wsaaEffdate);
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setCompay(agcmIO.getComern());
		agcmdbcIO.setTranno(wsaaTranno);
		agcmdbcIO.setFunction(varcom.writr);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(agcmdbcIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readNextAgcm3465()
	{
		agcmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), varcom.endp)
		&& isNE(agcmIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(agcmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(agcmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrnum(), agcmIO.getChdrnum())
		|| isNE(atmodrec.company, agcmIO.getChdrcoy())
		|| isEQ(agcmIO.getStatuz(), varcom.endp)) {
			agcmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processReassurance3470()
	{
		trmreas3471();
	}

protected void trmreas3471()
	{
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("TRMR");
		trmreasrec.chdrcoy.set(covrsurIO.getChdrcoy());
		trmreasrec.chdrnum.set(covrsurIO.getChdrnum());
		trmreasrec.life.set(covrsurIO.getLife());
		trmreasrec.coverage.set(covrsurIO.getCoverage());
		trmreasrec.rider.set(covrsurIO.getRider());
		trmreasrec.planSuffix.set(covrsurIO.getPlanSuffix());
		trmreasrec.crtable.set(covrsurIO.getCrtable());
		trmreasrec.polsum.set(chdrlifIO.getPolsum());
		trmreasrec.cnttype.set(chdrlifIO.getCnttype());
		trmreasrec.effdate.set(surhclmIO.getEffdate());
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(surhclmIO.getTranno());
		trmreasrec.language.set(atmodrec.language);
		trmreasrec.billfreq.set(chdrlifIO.getBillfreq());
		trmreasrec.ptdate.set(chdrlifIO.getPtdate());
		trmreasrec.origcurr.set(chdrlifIO.getCntcurr());
		trmreasrec.acctcurr.set(covrsurIO.getPremCurrency());
		trmreasrec.crrcd.set(covrsurIO.getCrrcd());
		/*  MOVE COVR-CONVERT-INITIAL-UNITS                      <A06805>*/
		/*                              TO TRRA-CONV-UNITS.      <A06805>*/
		trmreasrec.convUnits.set(covrsurIO.getConvertInitialUnits());
		trmreasrec.jlife.set(covrsurIO.getJlife());
		trmreasrec.singp.set(covrsurIO.getSingp());
		trmreasrec.oldSumins.set(covrsurIO.getSumins());
		trmreasrec.pstatcode.set(covrsurIO.getPstatcode());
		trmreasrec.newSumins.set(ZERO);
		trmreasrec.clmPercent.set(ZERO);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(trmreasrec.statuz);
			sysrSyserrRecInner.sysrParams.set(trmreasrec.trracalRec);
			xxxxFatalError();
		}
	}

protected void riskStatusCheckPp3500()
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrsurIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrsurIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void premStatusCheckPp3520()
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrsurIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrsurIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void readCoverages3600()
	{
		/*START*/
		/*  Read coverages.                                                */
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(atmodrec.company);
		covrIO.setChdrnum(chdrlifIO.getChdrnum());
		covrIO.setPlanSuffix(0);
		covrIO.setFunction(varcom.begn);
		wsaaValidStatus = "N";
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp)
		|| isEQ(wsaaValidStatus, "Y"))) {
			//performance improvement --  atiwari23
			covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
			updateComponent3650();
		}

		if (isEQ(wsaaValidStatus, "N")) {
			wsaaPlanSuffix.set(ZERO);
		}
		/*EXIT*/
	}

protected void updateComponent3650()
	{
		try {
			start3650();
			next3655();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start3650()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(covrIO.getParams());
			xxxxFatalError();
		}
		if (isNE(covrIO.getChdrcoy(), atmodrec.company)
		|| isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3659);
		}
		if (isEQ(covrIO.getValidflag(), "2")) {
			covrIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit3659);
		}
		/* Check to see if component has valid status.                     */
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			riskStatusCheckWp3920();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				premStatusCheckWp3940();
			}
		}
	}

protected void next3655()
	{
		covrIO.setFunction(varcom.nextr);
	}

protected void riskStatusCheckWp3920()
	{
		/*START*/
		/* Check Risk Status.                                              */
		if (isEQ(covrIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void premStatusCheckWp3940()
	{
		/*START*/
		/* Check Premium Status.                                           */
		if (isEQ(covrIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Housekeeping prior to termination.                          *
	* </pre>
	*/
protected void endOfDrivingFile4000()
	{
		/*HOUSEKEEPING-TERMINATE*/
		processSurdclmRecs4100();
		/*PERFORM 4300-WRITE-NEW-CONT-HEADER.                          */
		/*PERFORM 4400-PTRN-TRANSACTION.                               */
		/*PERFORM 4300-WRITE-NEW-CONT-HEADER.                     <006>*/
		batchHeader4500();
		/*    PERFORM 5200-WRITE-LETTERS.                             <020>*/
		letters4550();
		/*  Only now have all the coverages been processed.                */
		/*  Update the Contract Header, write a PTRN and update the        */
		/*  PAYR file.                                                     */
		getSMSnotification();
		writeNewContHeader4300();
		ptrnTransaction4400();
		writeNewPayr4800();
		dryProcessing8000();
		a200DelUnderwritting();
		releaseSoftlock4600();
		/*EXIT*/
	}

	/**
	* <pre>
	* Process related surrender details records                   *
	* </pre>
	*/
protected void processSurdclmRecs4100()
	{
		/*GO*/
		surdclmIO.setParams(SPACES);
		surdclmIO.setChdrcoy(atmodrec.company);
		surdclmIO.setChdrnum(chdrlifIO.getChdrnum());
		/*MOVE WSAA-TRANNO            TO SURDCLM-TRANNO.               */
		surdclmIO.setTranno(wsaaStartTranno);
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setFunction(varcom.begn);
		surdclmIO.setFormat(formatsInner.surdclmrec);
		/* Continue processing until contract break.                       */
		/*PERFORM 4150-UPDATE-SURRENDERS UNTIL SURDCLM-STATUZ = ENDP   */
		/*OR WSAA-TRANNO NOT = SURDCLM-TRANNO.                     */
		while ( !(isEQ(surdclmIO.getStatuz(), varcom.endp))) {
			updateSurrenders4150();
		}

		/*EXIT*/
	}

protected void updateSurrenders4150()
	{
		update4151();
	}

protected void update4151()
	{
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
		&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surdclmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(surdclmIO.getStatuz(), varcom.endp)
		|| isNE(chdrlifIO.getChdrnum(), surdclmIO.getChdrnum())
		|| isNE(atmodrec.company, surdclmIO.getChdrcoy())) {
			surdclmIO.setStatuz(varcom.endp);
			return ;
		}
		wsaaTranno.set(surdclmIO.getTranno());
		//Added for Ad-hoc refund on cancellation
		if(!(surdclmIO.getFieldType().equals("P") && t5687rec.svMethod.equals(wsaaSurrenderMethod))) {
			readTables4200();
		}
		else {
			surdclmIO.setFunction(varcom.nextr);
			return;
		}
		/* The Incr-Check section is now performed per COVR updated and */
		/* not per SURD.                                                */
		/* PERFORM 6000-INCR-CHECK.                             <A06275>*/
		surpcpy.planSuffix.set(surdclmIO.getPlanSuffix());
		surpcpy.crtable.set(surdclmIO.getCrtable());
		surpcpy.chdrcoy.set(surdclmIO.getChdrcoy());
		surpcpy.chdrnum.set(surdclmIO.getChdrnum());
		surpcpy.life.set(surdclmIO.getLife());
		surpcpy.jlife.set(surdclmIO.getJlife());
		surpcpy.coverage.set(surdclmIO.getCoverage());
		surpcpy.rider.set(surdclmIO.getRider());
		surpcpy.fund.set(surdclmIO.getVirtualFund());
		surpcpy.status.set(SPACES);
		surpcpy.tranno.set(surdclmIO.getTranno());
		surpcpy.estimatedVal.set(surdclmIO.getEstMatValue());
		surpcpy.actualVal.set(surdclmIO.getActvalue());
		surpcpy.type.set(surdclmIO.getFieldType());
		/*    MOVE SURDCLM-CURRCD         TO SURP-CURRCODE.                */
		/*    MOVE SURHCLM-CURRCD         TO SURP-CNTCURR.                 */
		if (isEQ(surhclmIO.getCurrcd(), SPACES)) {
			surpcpy.currcode.set(surdclmIO.getCurrcd());
			/*                                       SURP-CNTCURR              */
			surpcpy.cntcurr.set(chdrlifIO.getCntcurr());
		}
		else {
			surpcpy.currcode.set(surhclmIO.getCurrcd());
			surpcpy.cntcurr.set(surdclmIO.getCurrcd());
		}
		surpcpy.batckey.set(wsaaBatckey.batcFileKey);
		surpcpy.termid.set(wsaaTermid);
		surpcpy.date_var.set(wsaaTransactionDate);
		surpcpy.time.set(wsaaTransactionTime);
		surpcpy.user.set(wsaaUser);
		surpcpy.status.set("****");
		surpcpy.language.set(atmodrec.language);
		callSurrenderMethod4250();
//		MIBT-196
		/*  P5084AT loops through SURCLMIO and then calls PRESPRC and loop through SURPCPY.
		 * For Tradtional product SURPCPY status will be endp. But for UL products SURPCPY status will be ok.
		 * So logic has been return if PRESPRC completes and return endp, then we need futher loop in SURCLMIO 
		 * and we set SURCLMIO.statuz as endp
		 * If the PRESPRC returns ok then we need to loop through other records also. SO we set statuz as nextr
		 * */
		 
		if (isEQ(surpcpy.status, varcom.endp)){
			surdclmIO.setStatuz(varcom.endp);			
		}
		else
		{
		surdclmIO.setFunction(varcom.nextr);
	}
}

protected void readTables4200()
	{
		read4201();
	}

protected void read4201()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(surdclmIO.getCrtable());
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), surdclmIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
			xxxxFatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6598);
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
	}

protected void callSurrenderMethod4250()
	{
		/*READ*/
		callProgram(t6598rec.procesprog, surpcpy.surrenderRec);
		if (isEQ(surpcpy.status, varcom.bomb)) {
			sysrSyserrRecInner.sysrStatuz.set(surpcpy.status);
			xxxxFatalError();
		}
		if (isEQ(surpcpy.status, varcom.endp)) {
			return ;
		}
		if (isNE(surpcpy.status, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(surpcpy.status);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void writeNewContHeader4300()
	{
		housekeepingTerminate4301();
		next4370();
	}

protected void housekeepingTerminate4301()
	{
		/*  update the contract header*/
		/*TRANNO has already been updated in P5084!(for the first         */
		/*transaction)                                                    */
		/*  IF WSAA-PLAN-SUFFIX NOT = ZERO    AND                   <008>*/
		/*     WSAA-PLAN-SUFFIX NOT > CHDRLIF-POLSUM                <008>*/
		/*        COMPUTE CHDRLIF-POLSUM = WSAA-PLAN-SUFFIX - 1.    <008>*/
		/*  IF WSAA-PLAN-SUFFIX NOT = ZERO                               */
		/*      ADD 1 TO CHDRLIF-TRANNO                                  */
		/*      MOVE 'UPDAT'                TO CHDRLIF-FUNCTION          */
		/*      MOVE CHDRLIFREC             TO CHDRLIF-FORMAT            */
		/*      CALL 'CHDRLIFIO'            USING CHDRLIF-PARAMS         */
		/*      IF CHDRLIF-STATUZ           NOT = O-K                    */
		/*         MOVE CHDRLIF-PARAMS      TO SYSR-PARAMS               */
		/*         PERFORM XXXX-FATAL-ERROR                              */
		/*      ELSE                                                     */
		/*         GO TO 4390-EXIT.                                      */
		/*  MOVE SURHCLM-EFFDATE        TO CHDRLIF-CURRTO                */
		/*  MOVE '2'                    TO CHDRLIF-VALIDFLAG.            */
		/*  MOVE 'UPDAT'                TO CHDRLIF-FUNCTION.             */
		/*  MOVE CHDRLIFREC             TO CHDRLIF-FORMAT.               */
		/*  CALL 'CHDRLIFIO'            USING CHDRLIF-PARAMS.            */
		/*  IF CHDRLIF-STATUZ           NOT = O-K                        */
		/*     MOVE CHDRLIF-PARAMS      TO SYSR-PARAMS                   */
		/*     PERFORM XXXX-FATAL-ERROR.                                 */
		/*     UPDATE CONTRACT STATUS*/
		/* Update the old contract header with validflag = '2'.            */
		updateContractHeader2900();
		/* Update the old PAYR RECORD with validflag = '2'.                */
		updateOldPayrRecord4700();
		/* Write a new contract header with validflag = '1'.               */
		/*ADD 1                       TO CHDRLIF-TRANNO.               */
		chdrlifIO.setTranno(wsaaTranno);
		chdrlifIO.setValidflag("1");
		/*MOVE T5679-SET-CN-RISK-STAT TO CHDRLIF-STATCODE.             */
		chdrlifIO.setStatcode(t5679rec.setCnRiskStat);
		/*MOVE SURHCLM-EFFDATE        TO CHDRLIF-CURRFROM.             */
		chdrlifIO.setCurrfrom(wsaaEffdate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		/*MOVE SURHCLM-EFFDATE        TO CHDRLIF-STATDATE.             */
		chdrlifIO.setStatdate(wsaaEffdate);
		chdrlifIO.setStattran(chdrlifIO.getTranno());
		/*     UPDATE PREMIUM STATUS*/
		/* Work out new premium details.                                   */
		/* Keep old status codes if part plan surrender.                   */
		/*  <020>  Check against PAYR and not CHDR frequency.              */
		/*         Make sure Premium Status is updated.*/
		if (isNE(wsaaPlanSuffix, ZERO)) {
			chdrlifIO.setStatcode(wsaaOrigStatcode);
		}
		/*        IF CHDRLIF-BILLFREQ = '00' OR '  '                  <020>*/
		if (isEQ(payrIO.getBillfreq(), "00")
		|| isEQ(payrIO.getBillfreq(), "  ")) {
			/*            GO TO 4370-NEXT                                 <020>*/
			/*NEXT_SENTENCE*/
		}
		else {
			setPrecision(chdrlifIO.getSinstamt01(), 2);
			chdrlifIO.setSinstamt01(sub(chdrlifIO.getSinstamt01(), wsaaTotCovrsurInstprem));
			setPrecision(chdrlifIO.getSinstamt06(), 2);
			chdrlifIO.setSinstamt06(sub(chdrlifIO.getSinstamt06(), wsaaTotCovrsurInstprem));
		}
		/*                                    - WSAA-TOT-COVRSUR-INSTPREM  */
		/*            GO TO 4370-NEXT                                 <020>*/
		/*    IF T5679-SET-CN-PREM-STAT = SPACES OR                        */
		if ((isEQ(t5679rec.setCnPremStat, SPACES)
		&& isEQ(t5679rec.setSngpCnStat, SPACES))
		|| isNE(wsaaPlanSuffix, ZERO)) {
			/*T5679-SET-SNGP-CN-STAT = SPACES*/
			/*  single or regular premium present on table*/
			/*NEXT_SENTENCE*/
		}
		else {
			/*       IF CHDRLIF-BILLCHNL = '00' AND                            */
			/*    IF CHDRLIF-BILLFREQ = '00' AND                    <A05691>*/
			if (isEQ(payrIO.getBillfreq(), "00")
			&& isNE(t5679rec.setSngpCnStat, SPACES)) {
				/*   mop on contract  = single payment and single payment on table*/
				chdrlifIO.setPstatcode(t5679rec.setSngpCnStat);
			}
			else {
				if (isNE(t5679rec.setCnPremStat, SPACES)) {
					chdrlifIO.setPstatcode(t5679rec.setCnPremStat);
				}
			}
		}
	}

protected void next4370()
	{
		chdrlifIO.setPstattran(chdrlifIO.getTranno());
		varcom.vrcmCompTermid.set(wsaaTermid);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransactionTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrlifIO.setTranid(varcom.vrcmCompTranid);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Write PTRN transaction.                                     *
	* </pre>
	*/
protected void ptrnTransaction4400()
	{
		ptrnTransaction4401();
	}

protected void ptrnTransaction4401()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTransactionDate(0);
		ptrnIO.setTransactionTime(0);
		ptrnIO.setUser(0);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		/*  MOVE WSAA-TODAY             TO PTRN-PTRNEFF.                 */
		ptrnIO.setPtrneff(wsaaEffdate);
		/* MOVE WSAA-EFFDATE           TO PTRN-DATESUB.         <LA5184>*/
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptrnIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Write batch header transaction.                             *
	* </pre>
	*/
protected void batchHeader4500()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(batcuprec.batcupRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void letters4550()
	{
		start4550();
	}

	/**
	* <pre>
	*  Re-read SURD file for all records for this transaction,
	*  for each, write details out to surrender record.
	* </pre>
	*/
protected void start4550()
	{
		surdclmIO.setParams(SPACES);
		surdclmIO.setChdrcoy(atmodrec.company);
		surdclmIO.setChdrnum(chdrlifIO.getChdrnum());
		surdclmIO.setTranno(wsaaStartTranno);
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setFunction(varcom.begn);
		surdclmIO.setFormat(formatsInner.surdclmrec);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
		&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surdclmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(surdclmIO.getStatuz(), varcom.endp)
		|| isNE(chdrlifIO.getChdrnum(), surdclmIO.getChdrnum())
		|| isNE(atmodrec.company, surdclmIO.getChdrcoy())) {
			surdclmIO.setStatuz(h150);
			xxxxFatalError();
		}
		/*  Initialise the stored plan suffix. This must be zero to        */
		/*  prevent reprocessing of zero plan suffixes which would be      */
		/*  produced from a whole plan surrender and would have been       */
		/*  processed from the SURH. If there are 0 plan suffix records    */
		/*  then there will be no records of different plan suffixes.      */
		wsaaHoldSuffix.set(0);
		/* Continue processing until contract break.                       */
		while ( !(isEQ(surdclmIO.getStatuz(), varcom.endp))) {
			readSurd4560();
		}

		/*  Write Letter.                                                  */
		writeLetters5200();
	}

protected void readSurd4560()
	{
		start4560();
	}

	/**
	* <pre>
	*  Read all SURD records for this contract.
	* </pre>
	*/
protected void start4560()
	{
		/*  We have found a valid SURD to process. Check to see if         */
		/*  this is a different plan suffix to the previous one AND        */
		/*  that it is not the same as that held on the SURH record.       */
		/*  If this is a 'fresh' one call section to update COVRs and      */
		/*  also clawback any commission. Note that the plan suffix to     */
		/*  use must be passed in SURHCLM-PLAN-SUFFIX. Additionally        */
		/*  note that whole plan surrenders where the suffix is zero       */
		/*  will always have been processed earlier via the SURH. This     */
		/*  condition should therefore never be true for zero suffix.      */
		if (isNE(surdclmIO.getPlanSuffix(), wsaaHoldSuffix)
		&& isNE(surdclmIO.getPlanSuffix(), surhclmIO.getPlanSuffix())) {
			wsaaHoldSuffix.set(surhclmIO.getPlanSuffix());
			surhclmIO.setPlanSuffix(surdclmIO.getPlanSuffix());
			partPlanSurrender3100();
			surhclmIO.setPlanSuffix(wsaaHoldSuffix);
			wsaaHoldSuffix.set(surdclmIO.getPlanSuffix());
		}
		surdclmIO.setFunction(varcom.nextr);
		/*  Add letter details to working storage.                         */
		addToSvlt5000();
		/*  Read in next SURD.                                             */
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
		&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(surdclmIO.getStatuz());
			xxxxFatalError();
		}
		/*  Check for contract break.                                      */
		if (isEQ(surdclmIO.getStatuz(), varcom.endp)
		|| isNE(chdrlifIO.getChdrnum(), surdclmIO.getChdrnum())
		|| isNE(atmodrec.company, surdclmIO.getChdrcoy())) {
			surdclmIO.setStatuz(varcom.endp);
		}
	}

	/**
	* <pre>
	* Release Soft locked record.                                 *
	* </pre>
	*/
protected void releaseSoftlock4600()
	{
		releaseSoftlock4601();
	}

protected void releaseSoftlock4601()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void updateOldPayrRecord4700()
	{
		start4700();
	}

	/**
	* <pre>
	*  READH, Update and REWRT the PAYR record as valid flag '2'.
	* </pre>
	*/
protected void start4700()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void writeNewPayr4800()
	{
		start4800();
	}

	/**
	* <pre>
	*  Write a PAYR record with updated premium information.
	* </pre>
	*/
protected void start4800()
	{
		/* Work out new premium details.                                   */
		/* Keep old status codes if part plan surrender.                   */
		if (isNE(payrIO.getBillfreq(), "00")
		|| isNE(payrIO.getBillfreq(), "  ")) {
			setPrecision(payrIO.getSinstamt01(), 2);
			payrIO.setSinstamt01(sub(payrIO.getSinstamt01(), wsaaTotCovrsurInstprem));
			setPrecision(payrIO.getSinstamt06(), 2);
			payrIO.setSinstamt06(sub(payrIO.getSinstamt06(), wsaaTotCovrsurInstprem));
		}
		payrIO.setTranno(wsaaTranno);
		payrIO.setValidflag("1");
		payrIO.setEffdate(wsaaEffdate);
		payrIO.setPstatcode(chdrlifIO.getPstatcode());
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(payrIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(payrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void addToSvlt5000()
	{
		start5000();
	}

	/**
	* <pre>
	*  Add to variable portion of SVLT record.
	* </pre>
	*/
protected void start5000()
	{
		wsaaSvltSub.add(1);
		if (isLTE(wsaaSvltSub, 10)) {
			wsaaStoredLetterDetailInner.wsaaSvltCoverage[wsaaSvltSub.toInt()].set(surdclmIO.getCoverage());
			wsaaStoredLetterDetailInner.wsaaSvltRider[wsaaSvltSub.toInt()].set(surdclmIO.getRider());
			wsaaStoredLetterDetailInner.wsaaSvltShortds[wsaaSvltSub.toInt()].set(surdclmIO.getShortds());
			wsaaStoredLetterDetailInner.wsaaSvltType[wsaaSvltSub.toInt()].set(surdclmIO.getFieldType());
			/*  MOVE SURDCLM-RIIND    TO WSAA-SVLT-RIIND (WSAA-SVLT-SUB)    */
			wsaaStoredLetterDetailInner.wsaaSvltVirtfund[wsaaSvltSub.toInt()].set(surdclmIO.getVirtualFund());
			wsaaStoredLetterDetailInner.wsaaSvltCnstcur[wsaaSvltSub.toInt()].set(surdclmIO.getCurrcd());
			wsaaStoredLetterDetailInner.wsaaSvltEmv[wsaaSvltSub.toInt()].set(surdclmIO.getEstMatValue());
			/*  MOVE SURDCLM-ACTVALUE  TO WSAA-SVLT-ACTVALUE(WSAA-SVLT-SUB) */
			if (isEQ(surdclmIO.getFieldType(), "C")) {
				compute(wsaaStoredLetterDetailInner.wsaaSvltActvalue[wsaaSvltSub.toInt()], 2).set(mult(-1, surdclmIO.getActvalue()));
			}
			else {
				wsaaStoredLetterDetailInner.wsaaSvltActvalue[wsaaSvltSub.toInt()].set(surdclmIO.getActvalue());
			}
		}
	}

protected void writeLetters5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					writeLettersPara5200();
					readT66345210();
					letcRecord5220();
				case svltRecord5230:
					svltRecord5230();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void writeLettersPara5200()
	{
		/*  Skip LETC record writing since it will occur in                */
		/*  additional processing defined.                                 */
		if (isNE(t6598rec.addprocess, SPACES)) {
			goTo(GotoLabel.svltRecord5230);
		}
	}

	/**
	* <pre>
	*  Write LETC via LETRQST and SVLT details record.
	*  First get letter type from T6634.
	*5210-READ-T6634.                                         <PCPPRT>
	* </pre>
	*/
protected void readT66345210()
	{
		/*  Get the Letter type from T6634.                                */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		/* MOVE T6634                      TO ITEM-ITEMTABL.    <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		/*  Build key to T6634 from contract type & transaction code.      */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlifIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(g437);
			xxxxFatalError();
		}
		/* MOVE ITEM-GENAREA               TO T6634-T6634-REC.  <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF  T6634-LETTER-TYPE           = SPACES             <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			/*      GO TO 5299-EXIT.                                 <LA2114>*/
			goTo(GotoLabel.svltRecord5230);
		}
	}

protected void letcRecord5220()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrlifIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		/*  MOVE WSAA-EFFDATE           TO LETRQST-LETTER-REQUEST-DATE.  */
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(wsaaHoldSurhTranno);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.otherKeys.set(SPACES);
		wsaaLetokeys.set(SPACES);
		/*  MOVE CHDRLIF-CHDRNUM        TO WSAA-CHDRNUM.         <LA2114>*/
		wsaaLanguage.set(atmodrec.language);
		wsaaSacscode.set(t5645rec.sacscode07);
		wsaaSacstype.set(t5645rec.sacstype07);
		wsaaSurrCurr.set(wsaaHoldSurrCurr);
		letrqstrec.otherKeys.set(wsaaLetokeys);
		letrqstrec.function.set("ADD");
		/* IF  T6634-LETTER-TYPE       NOT = SPACES             <PCPPRT>*/
		if (isNE(tr384rec.letterType, SPACES)) {
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz, varcom.oK)) {
				sysrSyserrRecInner.sysrParams.set(letrqstrec.params);
				sysrSyserrRecInner.sysrStatuz.set(letrqstrec.statuz);
				xxxxFatalError();
			}
		}
	}

protected void svltRecord5230()
	{
		svltclmIO.setChdrcoy(chdrlifIO.getChdrcoy());
		svltclmIO.setChdrnum(chdrlifIO.getChdrnum());
		svltAlreadyExists5300();
		/*  Move details stored in Working Storage to SVLT Record.         */
		svltclmIO.setCoverages(wsaaStoredLetterDetailInner.wsaaSvltCoverages);
		svltclmIO.setRiders(wsaaStoredLetterDetailInner.wsaaSvltRiders);
		svltclmIO.setShortdss(wsaaStoredLetterDetailInner.wsaaSvltShortdss);
		svltclmIO.setTypes(wsaaStoredLetterDetailInner.wsaaSvltTypes);
		/*  MOVE WSAA-SVLT-RIINDS           TO SVLTCLM-RIINDS.           */
		svltclmIO.setVirtfunds(wsaaStoredLetterDetailInner.wsaaSvltVirtfunds);
		svltclmIO.setCnstcurs(wsaaStoredLetterDetailInner.wsaaSvltCnstcurs);
		svltclmIO.setEmvs(wsaaStoredLetterDetailInner.wsaaSvltEmvs);
		svltclmIO.setActvalues(wsaaStoredLetterDetailInner.wsaaSvltActvalues);
		/*  Store general Contract details in SVLT record area.            */
		svltclmIO.setPlanSuffix(wsaaPlanSuffix);
		svltclmIO.setCnttype(surhclmIO.getCnttype());
		svltclmIO.setCownnum(chdrlifIO.getCownnum());
		svltclmIO.setOccdate(chdrlifIO.getOccdate());
		svltclmIO.setPtdate(chdrlifIO.getPtdate());
		svltclmIO.setTransactionDate(wsaaTransactionDate);
		svltclmIO.setTransactionTime(wsaaTransactionTime);
		svltclmIO.setUser(wsaaUser);
		svltclmIO.setTermid(wsaaTermid);
		svltclmIO.setPolicyloan(surhclmIO.getPolicyloan());
		svltclmIO.setOtheradjst(surhclmIO.getOtheradjst());
		svltclmIO.setValidflag("1");
		svltclmIO.setFunction(varcom.writr);
		svltclmIO.setFormat(formatsInner.svltclmrec);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(svltclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(svltclmIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void svltAlreadyExists5300()
	{
		start5300();
	}

protected void start5300()
	{
		/*  Check to see if SVLT record already exists and if so mark it   */
		/*  as valid flag 2 and rewrite.                                   */
		svltclmIO.setFunction(varcom.readr);
		svltclmIO.setFormat(formatsInner.svltclmrec);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(), varcom.oK)
		&& isNE(svltclmIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(svltclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(svltclmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(svltclmIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		/*  Record already exists, READH, update valid flag and rewrite.   */
		svltclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(svltclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(svltclmIO.getStatuz());
			xxxxFatalError();
		}
		svltclmIO.setValidflag("2");
		svltclmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(svltclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(svltclmIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void incrCheck6000()
	{
		para6010();
	}

protected void para6010()
	{
		/* Instead of updating the INCR record in situ, write a new     */
		/* INCR record for this transaction to allow reversals to       */
		/* reinstate the previous record. Use the COVR details to       */
		/* read the INCR record, not the SURD details.                  */
		incrIO.setDataArea(SPACES);
		/* MOVE SURDCLM-CHDRCOY        TO INCR-CHDRCOY.         <A06275>*/
		/* MOVE SURDCLM-CHDRNUM        TO INCR-CHDRNUM.         <A06275>*/
		/* MOVE SURDCLM-LIFE           TO INCR-LIFE.            <A06275>*/
		/* MOVE SURDCLM-COVERAGE       TO INCR-COVERAGE.        <A06275>*/
		/* MOVE SURDCLM-RIDER          TO INCR-RIDER.           <A06275>*/
		/* MOVE SURDCLM-PLAN-SUFFIX    TO INCR-PLAN-SUFFIX.     <A06275>*/
		/* MOVE READH                  TO INCR-FUNCTION.        <A06275>*/
		incrIO.setChdrcoy(wsaaCovrChdrcoy);
		incrIO.setChdrnum(wsaaCovrChdrnum);
		incrIO.setLife(wsaaCovrLife);
		incrIO.setCoverage(wsaaCovrCoverage);
		incrIO.setRider(wsaaCovrRider);
		incrIO.setPlanSuffix(wsaaCovrPlanSuffix);
		incrIO.setFunction(varcom.readr);
		incrIO.setFormat(formatsInner.incrrec);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), varcom.oK)
		&& isNE(incrIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(incrIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(incrIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		incrIO.setValidflag("2");
		/* IF T5679-SET-COV-PREM-STAT NOT = SPACES              <A06275>*/
		/*    MOVE T5679-SET-COV-PREM-STAT TO INCR-PSTATCODE.   <A06275>*/
		/* IF T5679-SET-COV-RISK-STAT NOT = SPACES              <A06275>*/
		/*    MOVE T5679-SET-COV-RISK-STAT TO INCR-STATCODE.    <A06275>*/
		/* MOVE REWRT                  TO INCR-FUNCTION.        <A06275>*/
		/* Only update the INCR status codes if the COVR status codes   */
		/* have been updated.                                           */
		if (isEQ(wsaaValidStatus, "Y")) {
			if (isNE(t5679rec.setCovPremStat, SPACES)) {
				incrIO.setPstatcode(t5679rec.setCovPremStat);
			}
			if (isNE(t5679rec.setCovRiskStat, SPACES)) {
				incrIO.setStatcode(t5679rec.setCovRiskStat);
			}
		}
		incrIO.setTransactionDate(wsaaTransactionDate);
		incrIO.setTransactionTime(wsaaTransactionTime);
		incrIO.setUser(wsaaUser);
		incrIO.setTermid(wsaaTermid);
		incrIO.setTranno(wsaaTranno);
		incrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(incrIO.getParams());
			xxxxFatalError();
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrlifIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrlifIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(drypDryprcRecInner.drypDryprcRec);
			sysrSyserrRecInner.sysrStatuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(chdrlifIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifsttrrec.lifsttrRec);
			sysrSyserrRecInner.sysrStatuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}

protected void a200DelUnderwritting()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a200Ctrl();
				case a200Delet:
					a200Delet();
					a200Next();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a200Ctrl()
	{
		a300BegnLifeio();
	}

protected void a200Delet()
	{
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrlifIO.getChdrcoy());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrlifIO.getChdrnum());
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(crtundwrec.parmRec);
			sysrSyserrRecInner.sysrStatuz.set(crtundwrec.status);
			sysrSyserrRecInner.sysrIomod.set("CRTUNDWRT");
			xxxxFatalError();
		}
	}

protected void a200Next()
	{
		lifeIO.setFunction(varcom.nextr);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			return ;
		}
		if (isEQ(chdrlifIO.getChdrcoy(), lifeIO.getChdrcoy())
		&& isEQ(chdrlifIO.getChdrnum(), lifeIO.getChdrnum())) {
			goTo(GotoLabel.a200Delet);
		}
		/*A200-EXIT*/
	}

protected void a300BegnLifeio()
	{
		a300Ctrl();
	}

protected void a300Ctrl()
	{
		lifeIO.setRecKeyData(SPACES);
		lifeIO.setChdrcoy(chdrlifIO.getChdrcoy());
		lifeIO.setChdrnum(chdrlifIO.getChdrnum());
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (!(isEQ(lifeIO.getStatuz(), varcom.oK)
		&& isEQ(chdrlifIO.getChdrcoy(), lifeIO.getChdrcoy())
		&& isEQ(chdrlifIO.getChdrnum(), lifeIO.getChdrnum()))) {
			lifeIO.setStatuz(varcom.endp);
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(lifeIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void b100CallZorcompy()
	{
		b110Start();
	}

protected void b110Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		/* MOVE LIFA-SUBSTITUTE-CODE(06)                        <LA4955>*/
		/*                             TO ZORL-CRTABLE.         <LA4955>*/
		zorlnkrec.crtable.set(covrIO.getCrtable());
		zorlnkrec.effdate.set(agcmIO.getEfdate());
		zorlnkrec.ptdate.set(chdrlifIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		/* MOVE CHDRLIF-TRANNO         TO ZORL-TRANNO.          <LA4955>*/
		zorlnkrec.tranno.set(wsaaTranno);
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set(SPACES);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(zorlnkrec.statuz);
			sysrSyserrRecInner.sysrParams.set(zorlnkrec.zorlnkRec);
			xxxxFatalError();
		}
	}
protected void getSMSnotification() {
	
}
/*
 * Class transformed  from Data Structure WSAA-STORED-LETTER-DETAIL--INNER
 */
public static final class WsaaStoredLetterDetailInner {

		/*                                                         <LA2114>*/
	public FixedLengthStringData wsaaStoredLetterDetail = new FixedLengthStringData(400);
	public FixedLengthStringData wsaaSvltCoverages = new FixedLengthStringData(20).isAPartOf(wsaaStoredLetterDetail, 0);
	public FixedLengthStringData[] wsaaSvltCoverage = FLSArrayPartOfStructure(10, 2, wsaaSvltCoverages, 0);
	public FixedLengthStringData wsaaSvltRiders = new FixedLengthStringData(20).isAPartOf(wsaaStoredLetterDetail, 20);
	public FixedLengthStringData[] wsaaSvltRider = FLSArrayPartOfStructure(10, 2, wsaaSvltRiders, 0);
	public FixedLengthStringData wsaaSvltShortdss = new FixedLengthStringData(100).isAPartOf(wsaaStoredLetterDetail, 40);
	public FixedLengthStringData[] wsaaSvltShortds = FLSArrayPartOfStructure(10, 10, wsaaSvltShortdss, 0);
	public FixedLengthStringData wsaaSvltTypes = new FixedLengthStringData(10).isAPartOf(wsaaStoredLetterDetail, 140);
	public FixedLengthStringData[] wsaaSvltType = FLSArrayPartOfStructure(10, 1, wsaaSvltTypes, 0);
		/*  03  WSAA-SVLT-RIINDS.                                   <018>*/
	public FixedLengthStringData wsaaSvltVirtfunds = new FixedLengthStringData(40).isAPartOf(wsaaStoredLetterDetail, 150);
	public FixedLengthStringData[] wsaaSvltVirtfund = FLSArrayPartOfStructure(10, 4, wsaaSvltVirtfunds, 0);
	public FixedLengthStringData wsaaSvltCnstcurs = new FixedLengthStringData(30).isAPartOf(wsaaStoredLetterDetail, 190);
	public FixedLengthStringData[] wsaaSvltCnstcur = FLSArrayPartOfStructure(10, 3, wsaaSvltCnstcurs, 0);
		/* 03  WSAA-SVLT-EMVS.                                  <D96NUM>
		 03  WSAA-SVLT-ACTVALUES.                             <D96NUM>*/
	public FixedLengthStringData wsaaSvltEmvs = new FixedLengthStringData(90).isAPartOf(wsaaStoredLetterDetail, 220);
	public PackedDecimalData[] wsaaSvltEmv = PDArrayPartOfStructure(10, 17, 2, wsaaSvltEmvs, 0);
	public FixedLengthStringData wsaaSvltActvalues = new FixedLengthStringData(90).isAPartOf(wsaaStoredLetterDetail, 310);
	public PackedDecimalData[] wsaaSvltActvalue = PDArrayPartOfStructure(10, 17, 2, wsaaSvltActvalues, 0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t6598 = new FixedLengthStringData(5).init("T6598");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
	private FixedLengthStringData t1693 = new FixedLengthStringData(5).init("T1693");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData th605 = new FixedLengthStringData(5).init("TH605");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
public static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData surhclmrec = new FixedLengthStringData(10).init("SURHCLMREC");
	private FixedLengthStringData covrsurrec = new FixedLengthStringData(10).init("COVRSURREC");
	private FixedLengthStringData surdclmrec = new FixedLengthStringData(10).init("SURDCLMREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData svltclmrec = new FixedLengthStringData(10).init("SVLTCLMREC");
	public FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData incrrec = new FixedLengthStringData(10).init("INCRREC");
	private FixedLengthStringData agcmdbcrec = new FixedLengthStringData(10).init("AGCMREC");
	private FixedLengthStringData incrmjarec = new FixedLengthStringData(10).init("INCRMJAREC");
}
/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
private static final class SysrSyserrRecInner {

		/* System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(528);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	private FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrSyserrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 356);
	private FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
