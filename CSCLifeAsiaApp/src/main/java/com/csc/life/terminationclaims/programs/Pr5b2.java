/*
 * File: Pr5b2.java
 * Date: 20 February 2017 12:46:51
 * Author: Quipoz Limited
 * 
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.screens.Sr5b2ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Tr5b2rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
 *
 *
 *****************************************************************
 * </pre>
 */
public class Pr5b2 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR5B2");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	/* Logical File: Extra data screen */
	private DescTableDAM descIO = new DescTableDAM();
	/* Dated items by from date */
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Tr5b2rec tr5b2rec = new Tr5b2rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr5b2ScreenVars sv = ScreenProgram.getScreenVars(Sr5b2ScreenVars.class);

	private ErrorsInner errorsInner = new ErrorsInner();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		generalArea1045,
		validate2125,
		preExit,
		exit2090,
		exit3090
	}

	public Pr5b2() {
		super();
		screenVars = sv;
		new ScreenModel("Sr5b2", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void initialise1010() {
		/* INITIALISE-SCREEN */
		sv.dataArea.set(SPACES);
		/* READ-PRIMARY-RECORD */
		/* READ-RECORD */
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/* READ-SECONDARY-RECORDS */
	}

	protected void readRecord1031() {
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK) && isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

	protected void moveToScreen1040() {
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr5b2rec.tr5b2Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		tr5b2rec.nlg.set(SPACES);
		tr5b2rec.nlgczag.set(ZERO);
		tr5b2rec.maxnlgyrs.set(ZERO);
	}

	protected void generalArea1045() {
		sv.nlg.set(tr5b2rec.nlg);
		sv.nlgczag.set(tr5b2rec.nlgczag);
		sv.maxnlgyrs.set(tr5b2rec.maxnlgyrs);
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		} else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		} else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		/* CONFIRMATION-FIELDS */
		/* OTHER */
		/* EXIT */
	}

	protected void preScreenEdit() {
		try {
			preStart();
		} catch (GOTOException e) {
		}
	}

	protected void preStart() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

	protected void screenEdit2000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case validate2125:
					validate2125();
				case exit2090: {
					exit2090();
				}
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void screenIo2010() {
		wsspcomn.edterror.set(varcom.oK);
		/* VALIDATE */
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/* OTHER */
	}

	protected void validate2125() {
		if (isEQ(sv.nlg, SPACES)) {
			sv.nlgErr.set(errorsInner.e186);
		} else if (isEQ(sv.nlg, "Y")) {
			if (isEQ(sv.nlgczag, ZERO) && isEQ(sv.maxnlgyrs, ZERO)) {
				sv.nlgczagErr.set(errorsInner.f442);
				sv.maxnlgyrsErr.set(errorsInner.f442);
			} else if (isNE(sv.nlgczag, ZERO) && isNE(sv.maxnlgyrs, ZERO)) {
				sv.nlgczagErr.set(errorsInner.f047);
				sv.maxnlgyrsErr.set(errorsInner.f047);
			}
		} else if (isEQ(sv.nlg, "N")) {
			if (isNE(sv.nlgczag, ZERO)) {
				sv.nlgczagErr.set(errorsInner.e374);
			}
			if (isNE(sv.maxnlgyrs, ZERO)) {
				sv.maxnlgyrsErr.set(errorsInner.e374);
			}
		}
	}

	protected void exit2090() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* EXIT */
	}

	protected void update3000() {
		try {
			preparation3010();
			updatePrimaryRecord3050();
			updateRecord3055();
		} catch (GOTOException e) {
		}
	}

	protected void preparation3010() {
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
	}

	protected void updatePrimaryRecord3050() {
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

	protected void updateRecord3055() {
		checkChanges3100();
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(tr5b2rec.tr5b2Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/* OTHER */
	}

	protected void checkChanges3100() {
		check3100();
	}

	protected void check3100() {
		if (isNE(sv.nlg, tr5b2rec.nlg)) {
			tr5b2rec.nlg.set(sv.nlg);
		}
		if (isNE(sv.nlgczag, tr5b2rec.nlgczag)) {
			tr5b2rec.nlgczag.set(sv.nlgczag);
		}
		if (isNE(sv.maxnlgyrs, tr5b2rec.maxnlgyrs)) {
			tr5b2rec.maxnlgyrs.set(sv.maxnlgyrs);
		}
	}

	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.programPtr.add(1);
		/* EXIT */
	}

	/*
	 * Class transformed from Data Structure ERRORS_INNER
	 */
	private static final class ErrorsInner {
		/* ERRORS */
		private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
		private FixedLengthStringData f047 = new FixedLengthStringData(4).init("F047");
		private FixedLengthStringData f442 = new FixedLengthStringData(4).init("F442");
		private FixedLengthStringData e374 = new FixedLengthStringData(4).init("E374");
	}
}
