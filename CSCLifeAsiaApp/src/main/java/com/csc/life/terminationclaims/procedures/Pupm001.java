/*
 * File: Pupm001.java
 * Date: 30 August 2009 2:02:28
 * Author: Quipoz Limited
 *
 * Class transformed from PUPM001.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.terminationclaims.tablestructures.T6651rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*                      P U P M 0 0 1
*                   -------------------
*  Read T6651 using PUPT-PUMETH||PUPT-CNTCURR as the key, to
*  determine the paid up processing fee rules.
*
*  If T6651-ADJUSTIU = 'Y'
*       all Initial Units for all funds will be converted to
*       Accumulation Units within the same fund, with pro-rata
*       occurring on the total subsequent Accumulation Units
*       for each fund
*  else                                                         ¬
*       pro-rata will be performed only on the Accumulation
*       Units for all funds.  The Initial Units will be
*       by-passed during processing.
*
*  A table will be required in working-storage to hold values
*  for this processing.   The table should be set up as an
*  occurs 40, and if a 41st entry is required, the program
*  should abort with an appropriate message.
*
*  The table should contain:-
*   - Fund
*   - Fund Currency
*   - Payment Amount (in contract currency)
*
*  Initialise the table and total-components-funds field.
*
*  Set up UTRS with a key of:-
*   Company,
*   Contract Header Number,
*   Life,
*   Coverage,
*   Rider,
*   Plan Suffix,
*   and spaces in Fund and Unit Type.
*
*  Perform a BEGN on UTRS.
*  If the current unit balance (UTRS-CURRENT-UNIT-BAL) is
*  zeros, read the next record (NEXTR).
*
*  The first time through or on a change of fund, read Table
*  T5515 (Unit Linked Fund  Table)  to  determine  the  Fund
*  currency AND obtain the bid (if T6651-BIDOFFER = 'B') or
*  offer (if T6651-BIDOFFER = 'O') price from VPRC. The key is
*  Fund, Type (which will always be 'A'), and CHDRMJA-BTDATE.
*
*  If T6651-ADJUSTIU = 'Y', and the Unit Type = 'I', write two
*  UTRN records for each UTRS:
*   1. UNIT-TYPE - 'I'
*      NOF-UNITS - negative real initial units
*                             (UTRS-CURRENT-UNIT-BAL)
*            NOF-DUNITS     - negative deemed initial units
*                             (UTRS-CURRENT-DUNIT-BAL)
*   2. UNIT-TYPE - 'A'
*            NOF-UNITS      - positive real initial units
*                             (UTRS-CURRENT-UNIT-BAL)
*            NOF-DUNITS     - positive real initial units
*                             (UTRS-CURRENT-UNIT-BAL).
*  Use the total number of units to pro-rata against as the
*  Accumulated Real Units plus the Initial Real Units for the
*  fund
*  else
*  If T6651-ADJUSTIU = 'N', ignore any Initial Units and
*  NEXTR; i.e. only pro-rata against the Accumulation Real
*  Units for the fund.
*
*  Value of Units - multiply the total number of real units
*  for the fund (as accumulated above), by the price
*  (VPRC-UNIT-BID/OFFER-PRICE), giving the value of the units
*  in fund currency.
*
*  Perform a NEXTR.
*  If the current unit balance (UTRS-CURRENT-UNIT-BAL) is
*  zeros, read the next record (NEXTR).
*
*  On a change of fund and if the fund currency differs from
*  the payment currency then a currency conversion is
*  necessary.
*
*       Call the subroutine 'XCVRT' with a function of 'REAL',
*       the From Currency as the Fund Currency, the To
*       Currency as the payment currency and the calculated
*       amount in fund currency as the From Amount. The
*       effective date of the transaction is also passed to
*       XCVRT.  This will return the amount in the required
*       currency.
*
*  Write an entry in the table for the fund, the fund currency
*  and the amount in payment currency.
*  Add the amount in the payment currency to the
*  total-components-funds field.
*
*  If the key (apart from fund or type) changes, perform a
*  final currency conversion, and proceed to 'Final Check'
*  section.
*
*  Final Check:
*  Check that the total-components-funds field is the same as
*  the PUPT fund value field; if not, abort the program.
*
*  Write a UTRN record for each entry in the table.
*
*  Commission Processing
*  ---------------------
*  Commission details are represented by AGCM records, using
*  the logical view AGCMPUP.
*  Read all the AGCMPUP records for the component and
*  accumulate the earned and paid commision for each agent.
*
*  If the paid amount is greater than the earned amount,
*  create two ACMV records, using LIFACMV.  The claw back
*  amount (earned commission - paid commission) will be used
*  to debit the commission payable and to credit the
*  commission to advance.
*
*  See the Notes section at the end of this specification. The
*  subroutine LIFACMV will be called in order to write these
*  ACMV records.
*
*
*  NOTES
*  -----
*  Tables:
*  -------
*  T5515 - Unit Linked Fund                   Key: Fund
*  T5645 - Transaction Accounting Rules       Key: Program Id.
*  T5679 - Component Status                   Key: Trancode
*  T6647 - Unit Linked Contract Details       Key:
*                                             Trancode||CNTTYPE
*  T6651 - Paid Up Processing Rules           Key: Paid Up
*                                             Method||Currency
*
*  PTRN Record
*  -----------
*  . PTRN-BATCTRCDE         -  transaction code
*  . PTRN-CHDRCOY           -  CHDRMJA-CHDRCOY
*  . PTRN-CHDRNUM           -  CHDRMJA-CHDRNUM
*  . PTRN-TRANNO            -  CHDRMJA-TRANNO
*  . PTRN-TRANSACTION-DATE  -  system date
*  . PTRN-TRANSACTION-TIME  -  system time
*  . PTRN-PTRNEFF           -  system date
*  . PTRN-TERMID            -  VRCM-TERMID
*  . PTRN-USER              -  VRCM-USER
*
*
*  ACMV Record
*  -----------
*  Two ACMV records will be created.
*  The details for these will be held on T5645, accessed by
*  program Id.  Line #1 will give details for the first
*  posting which will be negative, and line #2 will give
*  details for the second posting which will be positive.  The
*  amount written on the ACMV record will be positive in both
*  cases, with the appropriate negative or positive value of
*  the field being determined by the 'sign' entered on T5645.
*
*  Note: the subroutine LIFACMV will be called to add these
*  records, call it with a function of 'PSTW' so that ACBL
*  records are created in the subroutine.
*
*  The key will be the batch transaction key passed in the
*  linkage section. The remaining fields will be set as
*  follows:
*
*  . ACMV-RLDGCOY           -  Contract Company
*  . ACMV-SACSCODE          -  from T5645 - lines 1 and 2
*  . ACMV-RLDGACCT          -  Agent number
*  . ACMV-ORIGCURR          -  Contract Currency
*  . ACMV-SACSTYP           -  from T5645 - lines 1 and 2
*  . ACMV-RDOCNUM           -  Contract Number
*  . ACMV-TRANNO            -  CHDRMJA-TRANNO
*  . ACMV-JRNSEQ            -  zeros
*  . ACMV-ORIGAMT           -  amount of clawback (earned
*                              minus paid) in contract currency
*  . ACMV-TRANREF           -  CHDRMJA-TRANNO
*  . ACMV-TRANDESC          -  transaction description from
*                              T5645
*  . ACMV-CRATE             -  zeros
*  . ACMV-ACCTAMT           -  zeros
*  . ACMV-GENLCOY           -  contract company
*  . ACMV-GENLCUR           -  spaces
*  . ACMV-GLCODE            -  from T5645 - lines 1 and 2
*  . ACMV-GLSIGN            -  from T5645 - lines 1 and 2
*  . ACMV-CONTOT            -  from T5645 - lines 1 and 2
*  . ACMV-POSTYEAR          -  zeros
*  . ACMV-POSTMONTH         -  zeros
*  . ACMV-EFFDATE           -  CHDRMJA-BTDATE
*  . ACMV-RCAMT             -  zeros
*  . ACMV-FRCDATE           -  VRCM-MAX-DATE
*  . ACMV-INTEXTIND         -  spaces
*  . ACMV-TRANSACTION-DATE  -  system date
*  . ACMV-TRANSACTION-TIME  -  system time
*  . ACMV-USER              -  VRCM-USER
*  . ACMV-TERMID            -  VRCM-TERMID
*
*
*  UTRN Record
*  -----------
*  Write one UTRN for each fund/fund-type requiring a fee.
*  Move spaces to the UTRN record, set the following fields
*  and zeroise all remaining numeric fields:-
*
*  CHDRCOY           |
*  CHDRNUM           |
*  LIFE              |
*  COVERAGE          |-------- Full key using the new tranno
*  RIDER             |
*  PLNSFX            |
*  UNIT-VIRTUAL-FUND | - fund code from the table entry
*  UNIT-TYPE         | - unit type from the table entry
*  TRANNO            | - new CHDRMJA-TRANNO
*  USER                - VRCM-USER
*  TERMID              - VRCM-TERMID
*  UNIT-SUB-ACCOUNT    - if 'I' set to 'INIU',
*                        else if 'A' set to 'ACMU'
*  NOW-DEFER-IND       - from T6647, use the allocation ind
*  BATCOY            |
*  BATCBRN           |
*  BATCACTYR         |------ WSSP information
*  BATCACTMN         |
*  BATCTRCDE         |
*  BATCBATCH         |
*  MONIES-DATE         - from T6647, Effective Date for
*                        Transaction.
*  CRTABLE             - COVRMJA-CRTABLE
*  CNTCURR             - CHDRMJA-CNTCURR
*  CONTRACT-AMOUNT     - fee * (payment currency amount from
*                        table entry / total-components-funds)
*  FUND-CURRENCY       - fund currency from table entry
*  SACSCODE            - From T5645 - line 3
*  SACSTYP             - From T5645 - line 3
*  GENLCDE             - From T5645 - line 3
*  CONTRACT-TYPE       - CHDRMJA-CNTTYPE
*  PROC-SEQ-NO         - from T6647
*  CR-COM-DATE         - COVRMJA-CRRCD
*  TRANSACTION-DATE    - system date
*  TRANSACTION-TIME    - system time
*
*
*  T6651 - Paid Up Processing Rules
*  --------------------------------
*  A new table to hold parameter values used to determine the
*  fees to be charged against a paid up component.
*
*  The table will be keyed on paid up calculation method
*  concatenated with currency.
*
*  The table will be a dated table.
*
*  Parameters are as follows, in a matrix with five
*  occurrences by billing frequency:
*
*       - minimum fee chargeable
*            PUFEEMIN       PIC S9(07)V99
*       - maximum fee chargeable
*            PUFEEMAX       PIC S9(07)V99
*       - percent value to charge
*            FEEPC          PIC S9(03)V99
*       - flat amount to charge
*            PUFFAMT        PIC S9(07)V99
*       - flag to use either bid or offer price
*            BIDOFFER       PIC X
*       - flag to adjust initial units or not
*            ADJUSTIU       PIC X
*
****************************************************************
* </pre>
*/
public class Pupm001 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PUPM001";

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6647Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Item, 4);

	private FixedLengthStringData wsaaGotFreq = new FixedLengthStringData(1);
	private Validator gotFreq = new Validator(wsaaGotFreq, "Y");
	private PackedDecimalData wsaaBusinessDate = new PackedDecimalData(8, 0);
	private String wsaaValidUtrs = "N";
	private String wsaaValidHits = "N";
	private PackedDecimalData wsaaBidofferPrice = new PackedDecimalData(9, 5);
	private PackedDecimalData wsaaFundValue = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaTotalRealFunds = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaTotalComponentsFunds = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaAccumulateEarned = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAccumulatePaid = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCommClawback = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAccumOvrdEarned = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAccumOvrdPaid = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOvrdCommClawback = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaUnitVirtualFund = new FixedLengthStringData(4);
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).init(ZERO);
	private FixedLengthStringData wsaaT5515Fund = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaEndOfPupt = new FixedLengthStringData(1).init("N");
	private Validator endOfPupt = new Validator(wsaaEndOfPupt, "Y");

	private FixedLengthStringData wsaaEndOfFund = new FixedLengthStringData(1).init("N");
	private Validator endOfFund = new Validator(wsaaEndOfFund, "Y");

	private FixedLengthStringData wsaaEndOfCovr = new FixedLengthStringData(1).init("N");
	private Validator endOfCovr = new Validator(wsaaEndOfCovr, "Y");

	private FixedLengthStringData wsaaEndOfAgcmpup = new FixedLengthStringData(1).init("N");
	private Validator endOfAgcmpup = new Validator(wsaaEndOfAgcmpup, "Y");

	private FixedLengthStringData wsaaChdrPaidup = new FixedLengthStringData(1).init("N");
	private Validator chdrPaidup = new Validator(wsaaChdrPaidup, "Y");

	private FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).init("N");
	private Validator validStatuz = new Validator(wsaaValidStatuz, "Y");

	private FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1).init("N");
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");

	private FixedLengthStringData wsaaBenefitBilled = new FixedLengthStringData(1).init("N");
	private Validator benefitBilled = new Validator(wsaaBenefitBilled, "Y");

	private FixedLengthStringData wsaaT6651Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6651Pumeth = new FixedLengthStringData(4).isAPartOf(wsaaT6651Item, 0);
	private FixedLengthStringData wsaaT6651Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT6651Item, 4);

		/* WSAA-PRO-RATA-TABLE */
	private FixedLengthStringData[] wsaaProRataItem = FLSInittedArray (40, 17);
	private FixedLengthStringData[] wsaaFund = FLSDArrayPartOfArrayStructure(4, wsaaProRataItem, 0);
	private FixedLengthStringData[] wsaaFundCurr = FLSDArrayPartOfArrayStructure(3, wsaaProRataItem, 4);
	private PackedDecimalData[] wsaaFundAmount = PDArrayPartOfArrayStructure(18, 5, wsaaProRataItem, 7);

		/* WSAA-IB-PRO-RATA-TABLE */
	private FixedLengthStringData[] wsaaIbProRataItem = FLSInittedArray (40, 17);
	private FixedLengthStringData[] wsaaIbFund = FLSDArrayPartOfArrayStructure(4, wsaaIbProRataItem, 0);
	private FixedLengthStringData[] wsaaIbFundCurr = FLSDArrayPartOfArrayStructure(3, wsaaIbProRataItem, 4);
	private PackedDecimalData[] wsaaIbFundAmount = PDArrayPartOfArrayStructure(18, 5, wsaaIbProRataItem, 7);
		/* WSAA-FREQUENCIES */
	private FixedLengthStringData wsaaFreq1 = new FixedLengthStringData(10).init("0001020412");

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		/* TABLES */
	private static final String t6651 = "T6651";
	private static final String t5645 = "T5645";
	private static final String t5515 = "T5515";
	private static final String t5688 = "T5688";
	private static final String t6647 = "T6647";
	private static final String th605 = "TH605";
		/* ERRORS */
	private static final String h351 = "H351";
	private static final String h114 = "H114";
	private static final String h116 = "H116";
	private static final String e308 = "E308";
	private static final String h115 = "H115";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String vprcrec = "VPRCREC";
	private static final String utrnrec = "UTRNREC";
	private static final String utrsrec = "UTRSREC";
	private static final String agcmrec = "AGCMREC   ";
	private static final String agcmdbcrec = "AGCMREC   ";
	private static final String itdmrec = "ITEMREC   ";
	private static final String hitrrec = "HITRREC";
	private static final String hitsrec = "HITSREC";
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private VprcTableDAM vprcIO = new VprcTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Trmreasrec trmreasrec = new Trmreasrec();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private T6651rec t6651rec = new T6651rec();
	private T5688rec t5688rec = new T5688rec();
	private T6647rec t6647rec = new T6647rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Th605rec th605rec = new Th605rec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Ovrduerec ovrduerec = new Ovrduerec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		finalCheck308,
		validUtrs308,
		writeTableEntry355,
		exit359,
		callAgcmio1020,
		overrideComm1375,
		initialiseFields1380,
		a250WriteTableEntry,
		a290Exit
	}

	public Pupm001() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		start000();
		exit000();
	}

protected void start000()
	{
		syserrrec.subrname.set(wsaaSubr);
		ovrduerec.statuz.set(varcom.oK);
		/* Get today's date                                                */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			databaseError500();
		}
		/* Set the business date                                           */
		wsaaBusinessDate.set(datcon1rec.intDate);
		readT56451600();
		/* Read T5688 using contract type from OVRDUEREC                   */
		readT56881700();
		/* Read T6647 to get the Unit Linked contract details              */
		readTableT66471800();
		readTableTh6051200();
		/* PERFORM 100-CALC-FEE.                                        */
		/* IF OVRD-PTDATE          NOT = OVRD-BTDATE               <001>*/
		/*    PERFORM 1500-CALL-REVERBILL.                         <001>*/
		if (isEQ(ovrduerec.pupfee, ZERO)) {
			commissionClawback1000();
		}
		else {
			proRata300();
			commissionClawback1000();
		}
		processReassurance2000();
	}

	/**
	* <pre>
	***  GO TO  000-EXIT.                                     <S19FIX>
	* </pre>
	*/
protected void exit000()
	{
		exitProgram();
	}

	/**
	* <pre>
	*500-DATABASE-ERROR.                                      <S19FIX>
	* </pre>
	*/
protected void databaseError500()
	{
		/*BEGIN*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		ovrduerec.statuz.set(syserrrec.statuz);
		ovrduerec.newSumins.set(ovrduerec.sumins);
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	**** This program no longer calculates a pup fee, as the  *
	**** online program calculates the value by performing    *
	**** a section.                                           *
	* </pre>
	*/
protected void calcFee100()
	{
	}

protected void proRata300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start310();
				case finalCheck308:
					finalCheck308();
				case validUtrs308:
					validUtrs308();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start310()
	{
		/*    Read T6651 to obtain the paidIup9processing fee rules        */
		readT6651400();
		wsaaTotalComponentsFunds.set(ZERO);
		wsaaTotalRealFunds.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 40)); wsaaSub.add(1)){
			initTable600();
		}
		wsaaSub.set(ZERO);
		/*    Begin reading the UTRS file                                  */
		utrsIO.setDataKey(SPACES);
		utrsIO.setChdrcoy(ovrduerec.chdrcoy);
		utrsIO.setChdrnum(ovrduerec.chdrnum);
		utrsIO.setLife(ovrduerec.life);
		utrsIO.setCoverage(ovrduerec.coverage);
		utrsIO.setRider(ovrduerec.rider);
		utrsIO.setPlanSuffix(ovrduerec.planSuffix);
		utrsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		utrsIO.setFormat(utrsrec);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK)
		&& isNE(utrsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrsIO.getParams());
			databaseError500();
		}
		/*     GO TO 500-DATABASE-ERROR.                         <S19FIX>*/
		if (isNE(ovrduerec.chdrcoy, utrsIO.getChdrcoy())
		|| isNE(ovrduerec.chdrnum, utrsIO.getChdrnum())
		|| isNE(ovrduerec.life, utrsIO.getLife())
		|| isNE(ovrduerec.coverage, utrsIO.getCoverage())
		|| isNE(ovrduerec.rider, utrsIO.getRider())
		|| isNE(ovrduerec.planSuffix, utrsIO.getPlanSuffix())
		|| isEQ(utrsIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.finalCheck308);
		}
		wsaaT5515Fund.set(utrsIO.getUnitVirtualFund());
		readT5515700();
		getFundPrice800();
		wsaaValidUtrs = "N";
		wsaaEndOfFund.set("N");
		while ( !(endOfFund.isTrue())) {
			processFunds330();
		}

	}

protected void finalCheck308()
	{
		if (isEQ(wsaaValidUtrs, "Y")) {
			processFund350();
		}
		wsaaFundValue.set(ZERO);
		wsaaEndOfFund.set("N");
		/*    Begin reading the HITS file                                  */
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(ovrduerec.chdrcoy);
		hitsIO.setChdrnum(ovrduerec.chdrnum);
		hitsIO.setLife(ovrduerec.life);
		hitsIO.setCoverage(ovrduerec.coverage);
		hitsIO.setRider(ovrduerec.rider);
		hitsIO.setPlanSuffix(ovrduerec.planSuffix);
		hitsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		hitsIO.setFormat(hitsrec);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			/*      GO TO 500-DATABASE-ERROR                         <S19FIX>*/
			databaseError500();
		}
		if (isNE(ovrduerec.chdrcoy, hitsIO.getChdrcoy())
		|| isNE(ovrduerec.chdrnum, hitsIO.getChdrnum())
		|| isNE(ovrduerec.life, hitsIO.getLife())
		|| isNE(ovrduerec.coverage, hitsIO.getCoverage())
		|| isNE(ovrduerec.rider, hitsIO.getRider())
		|| isNE(ovrduerec.planSuffix, hitsIO.getPlanSuffix())
		|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
			wsaaEndOfFund.set("Y");
			wsaaValidHits = "N";
			goTo(GotoLabel.validUtrs308);
		}
		wsaaT5515Fund.set(hitsIO.getZintbfnd());
		readT5515700();
		wsaaValidHits = "N";
		wsaaUnitVirtualFund.set(hitsIO.getZintbfnd());
		while ( !(endOfFund.isTrue())) {
			a100ProcessIbFunds();
		}

		if (isEQ(wsaaValidHits, "Y")) {
			a200ProcessIbFund();
			a300SetupCommonHitr();
			for (wsaaSub.set(1); !(isGT(wsaaSub, 40)); wsaaSub.add(1)){
				a400HitrFromTable();
			}
		}
	}

protected void validUtrs308()
	{
		if (isEQ(wsaaValidUtrs, "Y")) {
			setupCommonUtrn900();
			for (wsaaSub.set(1); !(isGT(wsaaSub, 40)); wsaaSub.add(1)){
				utrnFromTable390();
			}
		}
		/*EXIT*/
	}

protected void processFunds330()
	{
		start331();
		nextr335();
	}

protected void start331()
	{
		if (isEQ(utrsIO.getCurrentUnitBal(), ZERO)) {
			return ;
		}
		if (isNE(utrsIO.getUnitVirtualFund(), wsaaUnitVirtualFund)) {
			processFund350();
			wsaaT5515Fund.set(utrsIO.getUnitVirtualFund());
			readT5515700();
			getFundPrice800();
		}
		if (isEQ(utrsIO.getUnitType(), "I")) {
			wsaaValidUtrs = "Y";
			if (isEQ(t6651rec.adjustiu, "Y")) {
				wsaaTotalRealFunds.add(utrsIO.getCurrentUnitBal());
				writeAdjustUtrns370();
			}
			else {
				return ;
			}
		}
		else {
			wsaaTotalRealFunds.add(utrsIO.getCurrentUnitBal());
		}
		wsaaValidUtrs = "Y";
	}

protected void nextr335()
	{
		utrsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK)
		&& isNE(utrsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrsIO.getParams());
			databaseError500();
		}
		/*     GO TO 500-DATABASE-ERROR.                         <S19FIX>*/
		if (isNE(ovrduerec.chdrcoy, utrsIO.getChdrcoy())
		|| isNE(ovrduerec.chdrnum, utrsIO.getChdrnum())
		|| isNE(ovrduerec.life, utrsIO.getLife())
		|| isNE(ovrduerec.coverage, utrsIO.getCoverage())
		|| isNE(ovrduerec.rider, utrsIO.getRider())
		|| isNE(ovrduerec.planSuffix, utrsIO.getPlanSuffix())
		|| isEQ(utrsIO.getStatuz(), varcom.endp)) {
			wsaaEndOfFund.set("Y");
		}
		/*EXIT*/
	}

protected void processFund350()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start351();
				case writeTableEntry355:
					writeTableEntry355();
				case exit359:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start351()
	{
		if (isEQ(wsaaTotalRealFunds, ZERO)) {
			goTo(GotoLabel.exit359);
		}
		compute(wsaaFundValue, 5).set(mult(wsaaTotalRealFunds, wsaaBidofferPrice));
		if (isEQ(t5515rec.currcode, ovrduerec.cntcurr)) {
			goTo(GotoLabel.writeTableEntry355);
		}
		/*    As the payment currency is different to the fund currency,   */
		/*    a conversion is required, resulting in an amount in the      */
		/*    payment currency                                             */
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(ovrduerec.chdrcoy);
		conlinkrec.cashdate.set(ovrduerec.btdate);
		conlinkrec.currIn.set(t5515rec.currcode);
		conlinkrec.currOut.set(ovrduerec.cntcurr);
		conlinkrec.amountIn.set(wsaaFundValue);
		conlinkrec.amountOut.set(ZERO);
		/* MOVE 'CVRT'                TO CLNK-FUNCTION.                 */
		conlinkrec.function.set("SURR");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isEQ(conlinkrec.statuz, varcom.bomb)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			databaseError500();
		}
		/*      GO TO  500-DATABASE-ERROR.                       <S19FIX>*/
		wsaaFundValue.set(conlinkrec.amountOut);
		zrdecplrec.amountIn.set(wsaaFundValue);
		b000CallRounding();
		wsaaFundValue.set(zrdecplrec.amountOut);
	}

protected void writeTableEntry355()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 41)) {
			syserrrec.statuz.set(h351);
			databaseError500();
		}
		/*     GO TO  500-DATABASE-ERROR.                        <S19FIX>*/
		wsaaFund[wsaaSub.toInt()].set(wsaaUnitVirtualFund);
		/* MOVE OVRD-CNTCURR           TO WSAA-FUND-CURR(WSAA-SUB).<014>*/
		wsaaFundCurr[wsaaSub.toInt()].set(t5515rec.currcode);
		wsaaFundAmount[wsaaSub.toInt()].set(wsaaFundValue);
		/*    Add the fund value to the total fund value                   */
		wsaaTotalComponentsFunds.add(wsaaFundValue);
		/*    Initialise the fund accumulator                              */
		wsaaTotalRealFunds.set(ZERO);
	}

protected void writeAdjustUtrns370()
	{
		start371();
	}

protected void start371()
	{
		setupCommonUtrn900();
		utrnIO.setUnitVirtualFund(wsaaUnitVirtualFund);
		utrnIO.setUnitType("I");
		utrnIO.setUnitSubAccount("INIU");
		setPrecision(utrnIO.getNofUnits(), 5);
		utrnIO.setNofUnits(mult(utrsIO.getCurrentUnitBal(), -1));
		setPrecision(utrnIO.getNofDunits(), 5);
		utrnIO.setNofDunits(mult(utrsIO.getCurrentDunitBal(), -1));
		utrnIO.setFundCurrency(t5515rec.currcode);
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			databaseError500();
		}
		/*     GO TO  500-DATABASE-ERROR.                        <S19FIX>*/
		utrnIO.setUnitType("A");
		utrnIO.setUnitSubAccount("ACMU");
		utrnIO.setNofUnits(utrsIO.getCurrentUnitBal());
		utrnIO.setNofDunits(utrsIO.getCurrentUnitBal());
		utrnIO.setTermid(ovrduerec.termid);
		utrnIO.setSvp(1);
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			databaseError500();
		}
	}

protected void utrnFromTable390()
	{
		start391();
	}

protected void start391()
	{
		if (isEQ(wsaaFund[wsaaSub.toInt()], SPACES)) {
			wsaaSub.set(41);
			return ;
		}
		utrnIO.setUnitVirtualFund(wsaaFund[wsaaSub.toInt()]);
		utrnIO.setUnitType("A");
		utrnIO.setUnitSubAccount("ACMU");
		utrnIO.setFundCurrency(wsaaFundCurr[wsaaSub.toInt()]);
		setPrecision(utrnIO.getContractAmount(), 5);
		utrnIO.setContractAmount(mult(div(mult(ovrduerec.pupfee, wsaaFundAmount[wsaaSub.toInt()]), wsaaTotalComponentsFunds), -1));
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		b000CallRounding();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			databaseError500();
		}
	}

protected void readT6651400()
	{
		start410();
	}

protected void start410()
	{
		/*    Read the Paid Up Processing Rules Table                      */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(t6651);
		wsaaT6651Pumeth.set(ovrduerec.pumeth);
		wsaaT6651Cntcurr.set(ovrduerec.cntcurr);
		itdmIO.setItemitem(wsaaT6651Item);
		itdmIO.setItmfrm(ovrduerec.btdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.params.set(itdmIO.getStatuz());
			databaseError500();
		}
		/*     GO TO 500-DATABASE-ERROR.                         <S19FIX>*/
		if ((isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(), t6651))
		|| (isNE(itdmIO.getItemitem(), wsaaT6651Item))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemitem(wsaaT6651Item);
			itdmIO.setStatuz(h114);
			syserrrec.params.set(itdmIO.getParams());
			databaseError500();
			/****     GO TO 500-DATABASE-ERROR                          <S19FIX>*/
		}
		else {
			t6651rec.t6651Rec.set(itdmIO.getGenarea());
		}
	}

protected void initTable600()
	{
		/*START*/
		wsaaFund[wsaaSub.toInt()].set(SPACES);
		wsaaFundCurr[wsaaSub.toInt()].set(SPACES);
		wsaaFundAmount[wsaaSub.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void readT5515700()
	{
		start710();
	}

protected void start710()
	{
		/*    Read the Virtual Fund DetailsITable                          */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(t5515);
		/* MOVE UTRS-UNIT-VIRTUAL-FUND TO ITDM-ITEMITEM.        <INTBR> */
		itdmIO.setItemitem(wsaaT5515Fund);
		itdmIO.setItmfrm(ovrduerec.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError500();
		}
		/*     GO TO 500-DATABASE-ERROR.                         <S19FIX>*/
		if ((isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(), t5515))
		|| (isNE(itdmIO.getItemitem(), wsaaT5515Fund))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			/*    MOVE UTRS-UNIT-VIRTUAL-FUND TO ITDM-ITEMITEM      <INTBR> */
			syserrrec.syserrType.set("2");
			itdmIO.setItemitem(wsaaT5515Fund);
			itdmIO.setStatuz(h116);
			syserrrec.params.set(itdmIO.getParams());
			databaseError500();
			/****     GO TO 500-DATABASE-ERROR                          <S19FIX>*/
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
	}

protected void getFundPrice800()
	{
		start810();
	}

protected void start810()
	{
		/*    Read the Virtual Price File toCobtain the fund price         */
		wsaaBidofferPrice.set(ZERO);
		wsaaTotalRealFunds.set(ZERO);
		vprcIO.setDataArea(SPACES);
		vprcIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		/*  MOVE 'A'                    TO VPRC-UNIT-TYPE.               */
		vprcIO.setUnitType(utrsIO.getUnitType());
		vprcIO.setEffdate(ovrduerec.btdate);
		vprcIO.setJobno(ZERO);
		vprcIO.setFormat(vprcrec);
		vprcIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		vprcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprcIO.setFitKeysSearch("VRTFND","ULTYPE");
		SmartFileCode.execute(appVars, vprcIO);
		if (isNE(vprcIO.getStatuz(), varcom.oK)
		&& isNE(vprcIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(vprcIO.getParams());
			databaseError500();
		}
		/*      GO TO 500-DATABASE-ERROR.                        <S19FIX>*/
		if (isNE(vprcIO.getUnitVirtualFund(), utrsIO.getUnitVirtualFund())
		|| isNE(vprcIO.getUnitType(), utrsIO.getUnitType())) {
			syserrrec.statuz.set(varcom.mrnf);
			syserrrec.params.set(vprcIO.getParams());
			databaseError500();
		}
		/*      GO TO 500-DATABASE-ERROR.                        <S19FIX>*/
		wsaaUnitVirtualFund.set(utrsIO.getUnitVirtualFund());
		if (isEQ(t6651rec.bidoffer, "B")) {
			wsaaBidofferPrice.set(vprcIO.getUnitBidPrice());
		}
		else {
			if (isEQ(t6651rec.bidoffer, "O")) {
				wsaaBidofferPrice.set(vprcIO.getUnitOfferPrice());
			}
		}
	}

protected void setupCommonUtrn900()
	{
		start910();
	}

protected void start910()
	{
		utrnIO.setDataKey(SPACES);
		utrnIO.setChdrcoy(ovrduerec.chdrcoy);
		utrnIO.setChdrnum(ovrduerec.chdrnum);
		utrnIO.setLife(ovrduerec.life);
		utrnIO.setCoverage(ovrduerec.coverage);
		utrnIO.setRider(ovrduerec.rider);
		utrnIO.setPlanSuffix(ovrduerec.planSuffix);
		utrnIO.setTranno(ovrduerec.tranno);
		/* As Vrcm variables only serve to initialise Utrn variables we    */
		/* must initialise vrcm variables first..                          */
		varcom.vrcmTranidN.set(ZERO);
		utrnIO.setTermid(ovrduerec.termid);
		/*  MOVE VRCM-USER              TO UTRN-USER.                    */
		utrnIO.setUser(ovrduerec.user);
		utrnIO.setTransactionDate(ovrduerec.tranDate);
		utrnIO.setTransactionTime(ovrduerec.tranTime);
		/* MOVE OVRD-COMPANY           TO UTRN-BATCCOY.            <001>*/
		utrnIO.setBatccoy(ovrduerec.chdrcoy);
		utrnIO.setBatcbrn(ovrduerec.batcbrn);
		utrnIO.setBatcactyr(ovrduerec.acctyear);
		utrnIO.setBatcactmn(ovrduerec.acctmonth);
		utrnIO.setBatctrcde(ovrduerec.trancode);
		utrnIO.setBatcbatch(ovrduerec.batcbatch);
		/* MOVE OVRD-ALOIND            TO UTRN-NOW-DEFER-IND.      <001>*/
		utrnIO.setNowDeferInd(t6647rec.aloind);
		/* IF   OVRD-EFDCODE           = 'BD'                      <001>*/
		if (isEQ(t6647rec.efdcode, "BD")) {
			utrnIO.setMoniesDate(wsaaBusinessDate);
		}
		else {
			/* IF   OVRD-EFDCODE           = 'DD'                      <001>*/
			if (isEQ(t6647rec.efdcode, "DD")) {
				utrnIO.setMoniesDate(ovrduerec.btdate);
			}
			else {
				if (isEQ(t6647rec.efdcode, "RD")) {
					utrnIO.setMoniesDate(ovrduerec.runDate);
				}
				else {
					/* IF   OVRD-EFDCODE           = 'LO'                      <001>*/
					if (isEQ(t6647rec.efdcode, "LO")) {
						utrnIO.setMoniesDate(ovrduerec.btdate);
					}
					else {
						utrnIO.setMoniesDate(ZERO);
					}
				}
			}
		}
		/* IF   OVRD-EFDCODE           = 'LO'                      <001>*/
		if (isEQ(t6647rec.efdcode, "LO")
		&& isGT(wsaaBusinessDate, ovrduerec.btdate)) {
			utrnIO.setMoniesDate(wsaaBusinessDate);
		}
		utrnIO.setCrtable(ovrduerec.crtable);
		utrnIO.setCntcurr(ovrduerec.cntcurr);
		/* MOVE T5645-SACSCODE-03      TO UTRN-SACSCODE.           <007>*/
		/* MOVE T5645-SACSTYPE-03      TO UTRN-SACSTYP.            <007>*/
		/* MOVE T5645-GLMAP-03         TO UTRN-GENLCDE.            <007>*/
		/* Check for component level accounting & act accordingly          */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			utrnIO.setSacscode(t5645rec.sacscode07);
			utrnIO.setSacstyp(t5645rec.sacstype07);
			utrnIO.setGenlcde(t5645rec.glmap07);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode03);
			utrnIO.setSacstyp(t5645rec.sacstype03);
			utrnIO.setGenlcde(t5645rec.glmap03);
		}
		utrnIO.setContractType(ovrduerec.cnttype);
		/* MOVE OVRD-PROC-SEQ-NO       TO UTRN-PROC-SEQ-NO.        <001>*/
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setCrComDate(ovrduerec.crrcd);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setContractAmount(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setSvp(1);
	}

	/**
	* <pre>
	*1000-COMMISSION-CLAWBACK SECTION.                           <001>
	*1010-START.                                                 <001>
	**** MOVE SPACES                 TO AGCMPUP-DATA-AREA.       <001>
	**** MOVE OVRD-CHDRCOY           TO AGCMPUP-CHDRCOY.         <001>
	**** MOVE OVRD-CHDRNUM           TO AGCMPUP-CHDRNUM.         <001>
	**** MOVE OVRD-LIFE              TO AGCMPUP-LIFE.            <001>
	**** MOVE OVRD-COVERAGE          TO AGCMPUP-COVERAGE.        <001>
	**** MOVE OVRD-RIDER             TO AGCMPUP-RIDER.           <001>
	**** MOVE OVRD-PLAN-SUFFIX       TO AGCMPUP-PLAN-SUFFIX.     <001>
	**** MOVE BEGN                   TO AGCMPUP-FUNCTION.        <001>
	**** MOVE AGCMPUPREC             TO AGCMPUP-FORMAT.          <001>
	**** CALL 'AGCMPUPIO'            USING AGCMPUP-PARAMS.       <001>
	**** IF AGCMPUP-STATUZ           NOT = O-K                   <001>
	**** AND AGCMPUP-STATUZ          NOT = ENDP                  <001>
	****    MOVE AGCMPUP-PARAMS      TO SYSR-PARAMS              <001>
	****    GO TO 500-DATABASE-ERROR.                            <001>
	**** IF OVRD-CHDRCOY             NOT = AGCMPUP-CHDRCOY       <001>
	**** OR OVRD-CHDRNUM             NOT = AGCMPUP-CHDRNUM       <001>
	**** OR OVRD-LIFE                NOT = AGCMPUP-LIFE          <001>
	**** OR OVRD-COVERAGE            NOT = AGCMPUP-COVERAGE      <001>
	**** OR OVRD-RIDER               NOT = AGCMPUP-RIDER         <001>
	**** OR OVRD-PLAN-SUFFIX         NOT = AGCMPUP-PLAN-SUFFIX   <001>
	**** OR AGCMPUP-STATUZ           = ENDP                      <001>
	****    GO 1090-EXIT.                                        <001>
	**** MOVE AGCMPUP-AGNTNUM        TO WSAA-STORED-AGNTNUM.     <001>
	**** MOVE ZEROS                  TO WSAA-ACCUMULATE-EARNED   <001>
	****                                WSAA-ACCUMULATE-PAID     <001>
	****                                WSAA-ACCUM-OVRD-EARNED   <001>
	****                                WSAA-ACCUM-OVRD-PAID.    <001>
	**** MOVE 'N'                    TO WSAA-END-OF-AGCMPUP.     <001>
	**** PERFORM 1200-PROCESS-AGCMPUP UNTIL END-OF-AGCMPUP.      <001>
	*****PERFORM 1300-POST-CLAWBACK.                             <001>
	*1090-EXIT.                                                  <001>
	*    EXIT.                                                   <001>
	*1200-PROCESS-AGCMPUP SECTION.                               <001>
	*1201-GO.                                                    <001>
	*****IF WSAA-STORED-AGNTNUM      = AGCMPUP-AGNTNUM           <003>
	*****IF (WSAA-STORED-AGNTNUM     = AGCMPUP-AGNTNUM) OR       <007>
	********(AGCMPUP-COMPAY          = AGCMPUP-COMERN)           <007>
	********GO 1205-NEXTR.
	**** IF AGCMPUP-OVRDCAT          = 'O'                       <007>
	****    ADD AGCMPUP-COMERN          TO WSAA-ACCUM-OVRD-EARNED<007>
	****    ADD AGCMPUP-COMPAY          TO WSAA-ACCUM-OVRD-PAID  <007>
	**** ELSE                                                    <007>
	****    ADD AGCMPUP-COMERN          TO WSAA-ACCUMULATE-EARNED<007>
	****    ADD AGCMPUP-COMPAY          TO WSAA-ACCUMULATE-PAID  <007>
	**** END-IF.                                                 <007>
	**** PERFORM 1300-POST-CLAWBACK.                             <001>
	**** MOVE ZEROS                  TO WSAA-ACCUMULATE-EARNED   <001>
	****                                WSAA-ACCUMULATE-PAID     <001>
	****                                WSAA-ACCUM-OVRD-EARNED   <001>
	****                                WSAA-ACCUM-OVRD-PAID.    <001>
	**** MOVE AGCMPUP-AGNTNUM        TO WSAA-STORED-AGNTNUM.     <001>
	* Rewrite the AGCM record, to update commission payed
	**** MOVE READH                  TO AGCMPUP-FUNCTION.        <004>
	****                                                         <004>
	**** CALL 'AGCMPUPIO'         USING AGCMPUP-PARAMS.          <004>
	****                                                         <004>
	**** IF AGCMPUP-STATUZ        NOT = O-K                      <004>
	****     MOVE AGCMPUP-PARAMS     TO SYSR-PARAMS              <004>
	****     MOVE AGCMPUP-STATUZ     TO SYSR-STATUZ              <004>
	****     GO TO 500-DATABASE-ERROR                            <004>
	**** END-IF.                                                 <004>
	**** MOVE AGCMPUP-COMERN         TO AGCMPUP-COMPAY.          <003>
	**** MOVE REWRT                  TO AGCMPUP-FUNCTION.        <003>
	****                                                         <003>
	**** CALL 'AGCMPUPIO'            USING AGCMPUP-PARAMS.       <003>
	****                                                         <003>
	**** IF AGCMPUP-STATUZ       NOT = O-K                       <003>
	****     MOVE AGCMPUP-PARAMS     TO SYSR-PARAMS              <003>
	****     MOVE AGCMPUP-STATUZ     TO SYSR-STATUZ              <003>
	****     GO TO 500-DATABASE-ERROR.                           <003>
	*1205-NEXTR.                                                 <001>
	*    IF AGCMPUP-OVRDCAT          = 'O'                       <007>
	*       ADD AGCMPUP-COMERN          TO WSAA-ACCUM-OVRD-EARNED<007>
	*       ADD AGCMPUP-COMPAY          TO WSAA-ACCUM-OVRD-PAID  <007>
	*    ELSE                                                    <007>
	*       ADD AGCMPUP-COMERN          TO WSAA-ACCUMULATE-EARNED<007>
	*       ADD AGCMPUP-COMPAY          TO WSAA-ACCUMULATE-PAID  <007>
	*    END-IF.                                                 <007>
	*    ADD AGCMPUP-COMERN          TO WSAA-ACCUMULATE-EARNED.
	*    ADD AGCMPUP-COMPAY          TO WSAA-ACCUMULATE-PAID.
	**** MOVE NEXTR                  TO AGCMPUP-FUNCTION.        <001>
	**** CALL 'AGCMPUPIO'            USING AGCMPUP-PARAMS.       <001>
	**** IF AGCMPUP-STATUZ           = ENDP                      <001>
	****    MOVE 'Y'                 TO WSAA-END-OF-AGCMPUP      <001>
	****    GO 1209-EXIT.                                        <001>
	**** IF AGCMPUP-STATUZ           NOT = O-K                   <001>
	****    MOVE AGCMPUP-PARAMS      TO SYSR-PARAMS              <001>
	****    GO TO 500-DATABASE-ERROR.                            <001>
	**** IF OVRD-CHDRCOY             NOT = AGCMPUP-CHDRCOY       <001>
	**** OR OVRD-CHDRNUM             NOT = AGCMPUP-CHDRNUM       <001>
	**** OR OVRD-LIFE                NOT = AGCMPUP-LIFE          <001>
	**** OR OVRD-COVERAGE            NOT = AGCMPUP-COVERAGE      <001>
	**** OR OVRD-RIDER               NOT = AGCMPUP-RIDER         <001>
	**** OR OVRD-PLAN-SUFFIX         NOT = AGCMPUP-PLAN-SUFFIX   <001>
	****    MOVE 'Y'                 TO WSAA-END-OF-AGCMPUP.     <001>
	*1209-EXIT.                                                  <001>
	**** EXIT.                                                   <001>
	* </pre>
	*/
protected void commissionClawback1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1010();
				case callAgcmio1020:
					callAgcmio1020();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1010()
	{
		/* Read the first AGCM record for this contract.                   */
		agcmIO.setParams(SPACES);
		agcmIO.setChdrnum(ovrduerec.chdrnum);
		agcmIO.setChdrcoy(ovrduerec.chdrcoy);
		agcmIO.setPlanSuffix(ZERO);
		agcmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		agcmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		agcmIO.setFormat(agcmrec);
	}

protected void callAgcmio1020()
	{
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), varcom.oK)
		&& isNE(agcmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			databaseError500();
		}
		/* If the end of the AGCM file has been reached or an AGCM         */
		/* record was read but not for the contract being processed,       */
		/* then exit the section.                                          */
		if (isEQ(agcmIO.getStatuz(), varcom.endp)
		|| isNE(agcmIO.getChdrcoy(), ovrduerec.chdrcoy)
		|| isNE(agcmIO.getChdrnum(), ovrduerec.chdrnum)) {
			return ;
		}
		/* for the AGCM record just read.                                  */
		if (isEQ(agcmIO.getChdrcoy(), ovrduerec.chdrcoy)
		&& isEQ(agcmIO.getChdrnum(), ovrduerec.chdrnum)
		&& isEQ(agcmIO.getLife(), ovrduerec.life)
		&& isEQ(agcmIO.getCoverage(), ovrduerec.coverage)
		&& isEQ(agcmIO.getRider(), ovrduerec.rider)
		&& isEQ(agcmIO.getPlanSuffix(), ovrduerec.planSuffix)
		&& isNE(agcmIO.getCompay(), agcmIO.getComern())) {
			updateAgcmFile1100();
		}
		/* Loop round to see if there any other AGCM records to            */
		/* process for the contract.                                       */
		agcmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callAgcmio1020);
	}

protected void updateAgcmFile1100()
	{
		readh1110();
		rewrt1120();
		writr1130();
	}

	/**
	* <pre>
	* previous processing loop on AGCM is not upset.
	* created with commission paid set to earned, valid-flag of "1"
	* and correct tranno. Also create accounts records (ACMV).
	* </pre>
	*/
protected void readh1110()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmIO.getAgntnum());
		agcmdbcIO.setLife(agcmIO.getLife());
		agcmdbcIO.setCoverage(agcmIO.getCoverage());
		agcmdbcIO.setRider(agcmIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			databaseError500();
		}
	}

protected void rewrt1120()
	{
		agcmdbcIO.setValidflag("2");
		agcmdbcIO.setCurrto(ovrduerec.btdate);
		agcmdbcIO.setFunction(varcom.rewrt);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			databaseError500();
		}
	}

protected void writr1130()
	{
		/* correct tranno, and new commission paid.                        */
		if (isEQ(agcmIO.getOvrdcat(), "O")) {
			wsaaAccumOvrdEarned.set(agcmIO.getComern());
			wsaaAccumOvrdPaid.set(agcmIO.getCompay());
		}
		else {
			wsaaAccumulateEarned.set(agcmIO.getComern());
			wsaaAccumulatePaid.set(agcmIO.getCompay());
		}
		postClawback1300();
		agcmdbcIO.setCompay(agcmIO.getComern());
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrfrom(ovrduerec.btdate);
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setTranno(ovrduerec.tranno);
		agcmdbcIO.setFunction(varcom.writr);
		agcmdbcIO.setFormat(agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			databaseError500();
		}
	}

protected void readTableTh6051200()
	{
		start1210();
	}

protected void start1210()
	{
		/*  Read TH605 to see whether system is "Override based on Agent   */
		/*  Details"                                                       */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		//itemIO.setItemcoy(ovrduerec.company); //MIBT-133
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(th605);
		//itemIO.setItemitem(ovrduerec.company);//MIBT-133
				itemIO.setItemitem(ovrduerec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			databaseError500();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

protected void postClawback1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1371();
				case overrideComm1375:
					overrideComm1375();
				case initialiseFields1380:
					initialiseFields1380();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1371()
	{
		compute(wsaaOvrdCommClawback, 2).set(sub(wsaaAccumOvrdEarned, wsaaAccumOvrdPaid));
		compute(wsaaCommClawback, 2).set(sub(wsaaAccumulateEarned, wsaaAccumulatePaid));
		setupCommonAcmv1400();
		/*   If commission due = 0 then do not write a ACMV record.        */
		if (isEQ(wsaaCommClawback, 0)) {
			goTo(GotoLabel.overrideComm1375);
		}
		lifacmvrec.origamt.set(wsaaCommClawback);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		wsaaRldgChdrnum.set(ovrduerec.chdrnum);
		wsaaRldgLife.set(ovrduerec.life);
		wsaaRldgCoverage.set(ovrduerec.coverage);
		wsaaRldgRider.set(ovrduerec.rider);
		wsaaPlansuff.set(ovrduerec.planSuffix);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.tranref.set(wsaaRldgacct);
		/* First posting is to LA account and should post agent num*/
		/* as entity type.*/
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(ovrduerec.agntnum);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			databaseError500();
		}
		/*       GO TO 500-DATABASE-ERROR.                       <S19FIX>*/
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmIO.getAnnprem());
			a500CallZorcompy();
		}
		lifacmvrec.origamt.set(wsaaCommClawback);
		/* MOVE T5645-SACSCODE-02      TO LIFA-SACSCODE.           <007>*/
		/* MOVE T5645-SACSTYPE-02      TO LIFA-SACSTYP.            <007>*/
		/* MOVE T5645-GLMAP-02         TO LIFA-GLCODE.             <007>*/
		/* MOVE T5645-SIGN-02          TO LIFA-GLSIGN.             <007>*/
		/* MOVE T5645-CNTTOT-02        TO LIFA-CONTOT.             <007>*/
		/* MOVE OVRD-CHDRNUM           TO WSAA-RLDG-CHDRNUM.       <007>*/
		/* MOVE OVRD-LIFE              TO WSAA-RLDG-LIFE.          <007>*/
		/* MOVE OVRD-COVERAGE          TO WSAA-RLDG-COVERAGE.      <007>*/
		/* MOVE OVRD-RIDER             TO WSAA-RLDG-RIDER.         <007>*/
		/* MOVE OVRD-PLAN-SUFFIX       TO WSAA-PLANSUFF.           <007>*/
		/* MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.   <007>*/
		/* MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.           <007>*/
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		/* MOVE OVRD-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6). <007>*/
		/* check if component level accounting is required & act forthwith */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.contot.set(t5645rec.cnttot06);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlansuff.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(ovrduerec.chdrnum);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			databaseError500();
		}
	}

	/**
	* <pre>
	***       GO TO 500-DATABASE-ERROR.                       <S19FIX>
	* </pre>
	*/
protected void overrideComm1375()
	{
		/*   If commission due = 0 then do not write a ACMV record.        */
		if (isEQ(wsaaOvrdCommClawback, ZERO)) {
			/*      GO TO 1379-EXIT                                 <A06295>*/
			goTo(GotoLabel.initialiseFields1380);
		}
		lifacmvrec.origamt.set(wsaaOvrdCommClawback);
		/* MOVE AGCMPUP-CEDAGENT       TO LIFA-TRANREF.            <001>*/
		lifacmvrec.tranref.set(agcmIO.getCedagent());
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.contot.set(t5645rec.cnttot04);
		/* MOVE OVRD-CHDRNUM           TO WSAA-RLDG-CHDRNUM.       <007>*/
		/* MOVE OVRD-LIFE              TO WSAA-RLDG-LIFE.          <007>*/
		/* MOVE OVRD-COVERAGE          TO WSAA-RLDG-COVERAGE.      <007>*/
		/* MOVE OVRD-RIDER             TO WSAA-RLDG-RIDER.         <007>*/
		/* MOVE OVRD-PLAN-SUFFIX       TO WSAA-PLANSUFF.           <007>*/
		/* MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.   <007>*/
		/* MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.           <007>*/
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		/* Fourth posting is to LA account and should post agent num*/
		/* as entity type.*/
		lifacmvrec.rldgacct.set(SPACES);
		/* MOVE OVRD-AGNTNUM           TO LIFA-RLDGACCT.           <013>*/
		lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			databaseError500();
		}
		/*       GO TO 500-DATABASE-ERROR.                       <S19FIX>*/
		lifacmvrec.origamt.set(wsaaOvrdCommClawback);
		/* MOVE AGCMPUP-CEDAGENT       TO LIFA-TRANREF.            <001>*/
		lifacmvrec.tranref.set(agcmIO.getCedagent());
		/* MOVE T5645-SACSCODE-05      TO LIFA-SACSCODE.           <007>*/
		/* MOVE T5645-SACSTYPE-05      TO LIFA-SACSTYP.            <007>*/
		/* MOVE T5645-GLMAP-05         TO LIFA-GLCODE.             <007>*/
		/* MOVE T5645-SIGN-05          TO LIFA-GLSIGN.             <007>*/
		/* MOVE T5645-CNTTOT-05        TO LIFA-CONTOT.             <007>*/
		/* MOVE OVRD-CHDRNUM           TO WSAA-RLDG-CHDRNUM.       <007>*/
		/* MOVE OVRD-LIFE              TO WSAA-RLDG-LIFE.          <007>*/
		/* MOVE OVRD-COVERAGE          TO WSAA-RLDG-COVERAGE.      <007>*/
		/* MOVE OVRD-RIDER             TO WSAA-RLDG-RIDER.         <007>*/
		/* MOVE OVRD-PLAN-SUFFIX       TO WSAA-PLANSUFF.           <007>*/
		/* MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.   <007>*/
		/* MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.           <007>*/
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		/* MOVE OVRD-CRTABLE           TO LIFA-SUBSTITUTE-CODE(6). <007>*/
		/* check if component level accounting is required & act forthwith */
		if ((isEQ(t5688rec.comlvlacc, "Y"))) {
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
			lifacmvrec.contot.set(t5645rec.cnttot08);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlansuff.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(ovrduerec.chdrnum);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			databaseError500();
		}
	}

protected void initialiseFields1380()
	{
		wsaaAccumOvrdEarned.set(ZERO);
		wsaaAccumOvrdPaid.set(ZERO);
		wsaaAccumulateEarned.set(ZERO);
		wsaaAccumulatePaid.set(ZERO);
		/*EXIT*/
	}

protected void setupCommonAcmv1400()
	{
		start1410();
	}

protected void start1410()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		/*MOVE WSAA-BATCKEY           TO LIFA-BATCKEY.                 */
		/* MOVE OVRD-COMPANY           TO LIFA-BATCCOY.            <002>*/
		lifacmvrec.batccoy.set(ovrduerec.chdrcoy);
		lifacmvrec.batcbrn.set(ovrduerec.batcbrn);
		lifacmvrec.batcactyr.set(ovrduerec.acctyear);
		lifacmvrec.batcactmn.set(ovrduerec.acctmonth);
		lifacmvrec.batctrcde.set(ovrduerec.trancode);
		lifacmvrec.batcbatch.set(ovrduerec.batcbatch);
		lifacmvrec.rdocnum.set(ovrduerec.chdrnum);
		lifacmvrec.tranno.set(ovrduerec.tranno);
		lifacmvrec.rldgcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec.chdrcoy);
		/*    MOVE AGCMPUP-AGNTNUM        TO LIFA-RLDGACCT.           <007>*/
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		/*    MOVE WSAA-COMM-CLAWBACK     TO LIFA-ORIGAMT.                 */
		lifacmvrec.tranref.set(ovrduerec.tranno);
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(ovrduerec.btdate);
		lifacmvrec.termid.set(ovrduerec.termid);
		lifacmvrec.user.set(ovrduerec.user);
		lifacmvrec.transactionTime.set(ovrduerec.tranTime);
		lifacmvrec.transactionDate.set(ovrduerec.tranDate);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(ZERO);
		lifacmvrec.postmonth.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
	}

	/**
	* <pre>
	*1500-CALL-REVERBILL SECTION.                                <001>
	*                                                            <001>
	*1510-START.                                                 <001>
	**** MOVE OVRD-CHDRCOY           TO REVE-COMPANY.            <001>
	**** MOVE OVRD-CHDRNUM           TO REVE-CHDRNUM.            <001>
	**** MOVE OVRD-LIFE              TO REVE-LIFE.               <001>
	**** MOVE OVRD-COVERAGE          TO REVE-COVERAGE.           <001>
	**** MOVE OVRD-RIDER             TO REVE-RIDER.              <001>
	**** MOVE OVRD-PLAN-SUFFIX       TO REVE-PLAN-SUFFIX.        <001>
	**** MOVE SPACES                 TO REVE-BATCPFX.            <001>
	**** MOVE OVRD-COMPANY           TO REVE-BATCCOY.            <001>
	**** MOVE OVRD-BATCBRN           TO REVE-BATCBRN.            <001>
	**** MOVE OVRD-ACCTYEAR          TO REVE-BATCACTYR.          <001>
	**** MOVE OVRD-ACCTMONTH         TO REVE-BATCACTMN.          <001>
	**** MOVE SPACES                 TO REVE-BATCTRCDE.          <001>
	**** MOVE OVRD-BATCBATCH         TO REVE-BATCBATCH.          <001>
	**** MOVE OVRD-PTDATE            TO REVE-EFFDATE-1.          <001>
	**** MOVE VRCM-MAX-DATE          TO REVE-EFFDATE-2.          <001>
	**** MOVE ZEROS                  TO REVE-TRANNO.             <001>
	**** MOVE OVRD-TRANNO            TO REVE-NEW-TRANNO.         <001>
	**** MOVE OVRD-TRANCODE          TO REVE-OLD-BATCTRCDE.      <001>
	**** MOVE ZEROES                 TO REVE-PTRNEFF.            <005>
	****                                                         <001>
	**** CALL 'REVBILL' USING REVE-REVERSE-REC.                  <001>
	****                                                         <001>
	**** IF REVE-STATUZ           NOT = O-K                      <001>
	****    MOVE REVE-STATUZ          TO SYSR-STATUZ             <001>
	****    MOVE REVE-REVERSE-REC     TO SYSR-PARAMS             <001>
	****    GO TO 500-DATABASE-ERROR.                            <001>
	****                                                         <001>
	*1590-EXIT.                                                  <001>
	****  EXIT.                                                  <001>
	* </pre>
	*/
protected void readT56451600()
	{
		start1610();
	}

protected void start1610()
	{
		/*    Read the Transaction Accounting Rules Table*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		/* MOVE OVRD-COMPANY           TO ITEM-ITEMCOY.                 */
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(t5645);
		/* MOVE WSAA-TABLE-PROG        TO ITEM-ITEMITEM.                */
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			databaseError500();
		}
		/*      GO TO  500-DATABASE-ERROR.                               */
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		/* MOVE OVRD-COMPANY           TO DESC-DESCCOY.                 */
		descIO.setDesccoy(ovrduerec.chdrcoy);
		descIO.setDesctabl(t5645);
		descIO.setLanguage(ovrduerec.language);
		/* MOVE WSAA-TABLE-PROG        TO DESC-DESCITEM.                */
		descIO.setDescitem(wsaaSubr);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			databaseError500();
		}
	}

protected void readT56881700()
	{
		read1710();
	}

protected void read1710()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		/* MOVE OVRD-COMPANY           TO ITDM-ITEMCOY.            <007>*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(ovrduerec.cnttype);
		itdmIO.setItmfrm(ovrduerec.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError500();
		}
		/*      GO TO 500-DATABASE-ERROR.                        <S19FIX>*/
		/* IF ITDM-ITEMCOY             NOT = OVRD-COMPANY OR       <007>*/
		if (isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), ovrduerec.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(ovrduerec.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			databaseError500();
			/****      GO TO 500-DATABASE-ERROR                         <S19FIX>*/
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTableT66471800()
	{
		start1800();
	}

protected void start1800()
	{
		/*    Read the Unit Linked Contract Details Table                  */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaT6647Batctrcde.set(ovrduerec.trancode);
		wsaaT6647Cnttype.set(ovrduerec.cnttype);
		itdmIO.setItemitem(wsaaT6647Item);
		itdmIO.setItmfrm(ovrduerec.occdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError500();
		}
		if ((isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(), t6647))
		|| (isNE(itdmIO.getItemitem(), wsaaT6647Item))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemitem(wsaaT6647Item);
			syserrrec.params.set(itdmIO.getParams());
			itdmIO.setStatuz(h115);
			databaseError500();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void processReassurance2000()
	{
		trmreas2001();
	}

protected void trmreas2001()
	{
		wsaaBatckey.batcBatcpfx.set("BA");
		wsaaBatckey.batcBatccoy.set(ovrduerec.chdrcoy);
		wsaaBatckey.batcBatcbrn.set(ovrduerec.batcbrn);
		wsaaBatckey.batcBatcactyr.set(ovrduerec.acctyear);
		wsaaBatckey.batcBatcactmn.set(ovrduerec.acctmonth);
		wsaaBatckey.batcBatctrcde.set(ovrduerec.trancode);
		wsaaBatckey.batcBatcbatch.set(ovrduerec.batcbatch);
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("CHGR");
		trmreasrec.chdrcoy.set(ovrduerec.chdrcoy);
		trmreasrec.chdrnum.set(ovrduerec.chdrnum);
		trmreasrec.life.set(ovrduerec.life);
		trmreasrec.coverage.set(ovrduerec.coverage);
		trmreasrec.rider.set(ovrduerec.rider);
		trmreasrec.planSuffix.set(ovrduerec.planSuffix);
		trmreasrec.cnttype.set(ovrduerec.cnttype);
		trmreasrec.crtable.set(ovrduerec.crtable);
		trmreasrec.polsum.set(ovrduerec.polsum);
		trmreasrec.effdate.set(ovrduerec.ptdate);
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(ovrduerec.tranno);
		trmreasrec.language.set(ovrduerec.language);
		trmreasrec.billfreq.set(ovrduerec.billfreq);
		trmreasrec.ptdate.set(ovrduerec.ptdate);
		trmreasrec.origcurr.set(ovrduerec.cntcurr);
		trmreasrec.acctcurr.set(ovrduerec.cntcurr);
		trmreasrec.crrcd.set(ovrduerec.crrcd);
		trmreasrec.convUnits.set(ZERO);
		trmreasrec.jlife.set(SPACES);
		trmreasrec.singp.set(ovrduerec.instprem);
		trmreasrec.pstatcode.set(ovrduerec.pstatcode);
		trmreasrec.oldSumins.set(ovrduerec.sumins);
		trmreasrec.newSumins.set(ovrduerec.newSumins);
		trmreasrec.clmPercent.set(ZERO);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(trmreasrec.statuz);
			syserrrec.params.set(trmreasrec.trracalRec);
			databaseError500();
		}
	}

protected void a100ProcessIbFunds()
	{
		a110Start();
		a180Nextr();
	}

protected void a110Start()
	{
		if (isEQ(hitsIO.getZcurprmbal(), ZERO)) {
			return ;
		}
		if (isNE(hitsIO.getZintbfnd(), wsaaUnitVirtualFund)) {
			a200ProcessIbFund();
			wsaaT5515Fund.set(hitsIO.getZintbfnd());
			readT5515700();
			wsaaUnitVirtualFund.set(hitsIO.getZintbfnd());
		}
		wsaaTotalRealFunds.add(hitsIO.getZcurprmbal());
		wsaaValidHits = "Y";
	}

protected void a180Nextr()
	{
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			databaseError500();
			/****       GO TO 500-DATABASE-ERROR                        <S19FIX>*/
		}
		if (isNE(ovrduerec.chdrcoy, hitsIO.getChdrcoy())
		|| isNE(ovrduerec.chdrnum, hitsIO.getChdrnum())
		|| isNE(ovrduerec.life, hitsIO.getLife())
		|| isNE(ovrduerec.coverage, hitsIO.getCoverage())
		|| isNE(ovrduerec.rider, hitsIO.getRider())
		|| isNE(ovrduerec.planSuffix, hitsIO.getPlanSuffix())
		|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
			wsaaEndOfFund.set("Y");
		}
		/*A190-EXIT*/
	}

protected void a200ProcessIbFund()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a210Start();
				case a250WriteTableEntry:
					a250WriteTableEntry();
				case a290Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a210Start()
	{
		if (isEQ(wsaaTotalRealFunds, ZERO)) {
			goTo(GotoLabel.a290Exit);
		}
		wsaaFundValue.set(wsaaTotalRealFunds);
		if (isEQ(t5515rec.currcode, ovrduerec.cntcurr)) {
			goTo(GotoLabel.a250WriteTableEntry);
		}
		/*    As the payment currency is different to the fund currency    */
		/*    a conversion is required, resulting in an amount in the      */
		/*    payment currency                                             */
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(ovrduerec.chdrcoy);
		conlinkrec.cashdate.set(ovrduerec.btdate);
		conlinkrec.currIn.set(t5515rec.currcode);
		conlinkrec.currOut.set(ovrduerec.cntcurr);
		conlinkrec.amountIn.set(wsaaFundValue);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("SURR");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isEQ(conlinkrec.statuz, varcom.bomb)) {
			syserrrec.syserrType.set("2");
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			/*      GO TO 500-DATABASE-ERROR                         <S19FIX>*/
			databaseError500();
		}
		wsaaFundValue.set(conlinkrec.amountOut);
		zrdecplrec.amountIn.set(wsaaFundValue);
		b000CallRounding();
		wsaaFundValue.set(zrdecplrec.amountOut);
	}

protected void a250WriteTableEntry()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 41)) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(h351);
			/*      GO TO 500-DATABASE-ERROR                         <S19FIX>*/
			databaseError500();
		}
		wsaaIbFund[wsaaSub.toInt()].set(wsaaUnitVirtualFund);
		wsaaIbFundCurr[wsaaSub.toInt()].set(t5515rec.currcode);
		wsaaIbFundAmount[wsaaSub.toInt()].set(wsaaFundValue);
		wsaaTotalComponentsFunds.add(wsaaFundValue);
		wsaaTotalRealFunds.set(ZERO);
	}

protected void a300SetupCommonHitr()
	{
		a310Start();
	}

protected void a310Start()
	{
		hitrIO.setParams(SPACES);
		hitrIO.setChdrcoy(ovrduerec.chdrcoy);
		hitrIO.setChdrnum(ovrduerec.chdrnum);
		hitrIO.setLife(ovrduerec.life);
		hitrIO.setCoverage(ovrduerec.coverage);
		hitrIO.setRider(ovrduerec.rider);
		hitrIO.setPlanSuffix(ovrduerec.planSuffix);
		hitrIO.setTranno(ovrduerec.tranno);
		hitrIO.setBatccoy(ovrduerec.chdrcoy);
		hitrIO.setBatcbrn(ovrduerec.batcbrn);
		hitrIO.setBatcactyr(ovrduerec.acctyear);
		hitrIO.setBatcactmn(ovrduerec.acctmonth);
		hitrIO.setBatctrcde(ovrduerec.trancode);
		hitrIO.setBatcbatch(ovrduerec.batcbatch);
		hitrIO.setEffdate(wsaaBusinessDate);
		hitrIO.setCrtable(ovrduerec.crtable);
		hitrIO.setCntcurr(ovrduerec.cntcurr);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			hitrIO.setSacscode(t5645rec.sacscode07);
			hitrIO.setSacstyp(t5645rec.sacstype07);
			hitrIO.setGenlcde(t5645rec.glmap07);
		}
		else {
			hitrIO.setSacscode(t5645rec.sacscode03);
			hitrIO.setSacstyp(t5645rec.sacstype03);
			hitrIO.setGenlcde(t5645rec.glmap03);
		}
		hitrIO.setCnttyp(ovrduerec.cnttype);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setContractAmount(ZERO);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setZintrate(ZERO);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setSvp(1);
	}

protected void a400HitrFromTable()
	{
		a410Start();
	}

protected void a410Start()
	{
		if (isEQ(wsaaIbFund[wsaaSub.toInt()], SPACES)) {
			wsaaSub.set(41);
			return ;
		}
		hitrIO.setZintbfnd(wsaaIbFund[wsaaSub.toInt()]);
		hitrIO.setZrectyp("P");
		hitrIO.setZintalloc("N");
		hitrIO.setFundCurrency(wsaaIbFundCurr[wsaaSub.toInt()]);
		setPrecision(hitrIO.getContractAmount(), 5);
		hitrIO.setContractAmount(mult(div(mult(ovrduerec.pupfee, wsaaIbFundAmount[wsaaSub.toInt()]), wsaaTotalComponentsFunds), -1));
		zrdecplrec.amountIn.set(hitrIO.getContractAmount());
		b000CallRounding();
		hitrIO.setContractAmount(zrdecplrec.amountOut);
		hitrIO.setFunction(varcom.writr);
		hitrIO.setFormat(hitrrec);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			databaseError500();
			/****     GO TO  500-DATABASE-ERROR                         <S19FIX>*/
		}
	}

protected void a500CallZorcompy()
	{
		a510Start();
	}

protected void a510Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set("Y");
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.effdate.set(agcmIO.getEfdate());
		zorlnkrec.ptdate.set(agcmIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(lifacmvrec.tranno);
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set("Y");
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			databaseError500();
		}
	}

protected void b000CallRounding()
	{
		/*B010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(ovrduerec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(ovrduerec.cntcurr);
		zrdecplrec.batctrcde.set(ovrduerec.trancode);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			databaseError500();
		}
		/*B090-EXIT*/
	}
}
