package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Hsudpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HsudpfDAO extends BaseDAO<Hsudpf> {
	void insertHsudpfList(List<Hsudpf> pfList);
}
