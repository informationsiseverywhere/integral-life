/*
 * File: P5238.java
 * Date: 30 August 2009 0:21:45
 * Author: Quipoz Limited
 * 
 * Class transformed from P5238.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.procedures.Trcdechk;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.contractservicing.tablestructures.Tranchkrec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.screens.S5238ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* OVERVIEW
* ========
*
* This is a new screen which forms part of  the  9405  Annuities
* Development.    It  is  part of the transaction to Reverse the
* Registration of Death on a Life Assured.
*
* If  the  last  transaction  on  the  contract  was   NOT   the
* Registration  of  Death  of a Life Assured, display the screen
* with an error message and do not allow reversal processing  to
* proceed.    (The  other  transactions will have to be reversed
* first either using full contract reversal or  the  appropriate
* other reversal processing).
*
* The   details  displayed  on  the  screen  are  displayed  for
* information only and are the  details  that  were  input  when
* death of the life was registered, i.e. date of death and cause
* of death.  Pressing ENTER on this screen confirms that this is
* the  information  to  be  reversed  and the AT request will be
* submitted.
*
*****************************************************************
* </pre>
*/
public class P5238 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5238");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(32);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaProcessingMsg = new FixedLengthStringData(43);
	private ZonedDecimalData wsaaMsgTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaProcessingMsg, 0).setUnsigned();
	private FixedLengthStringData filler1 = new FixedLengthStringData(3).isAPartOf(wsaaProcessingMsg, 5, FILLER).init(" - ");
	private FixedLengthStringData wsaaMsgTrantype = new FixedLengthStringData(4).isAPartOf(wsaaProcessingMsg, 8);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaProcessingMsg, 12, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsgTrandesc = new FixedLengthStringData(30).isAPartOf(wsaaProcessingMsg, 13);
	private FixedLengthStringData wsaaForwardTranscode = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private String wsaaTranError = "";
	private String wsaaNoDeath = "";
		/* ERRORS */
	private static final String i034 = "I034";
	private static final String f910 = "F910";
	private static final String h225 = "H225";
		/* TABLES */
	private static final String t6661 = "T6661";
	private static final String t1688 = "T1688";
		/* FORMATS */
	private static final String lifemjarec = "LIFEMJAREC";
	private static final String ptrnrevrec = "PTRNREVREC";
	private static final String clmhclmrec = "CLMHCLMREC";
	private static final String itemrec = "ITEMREC";
	private static final String cltsrec = "CLTSREC";
	private static final String descrec = "DESCREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private FixedLengthStringData wsaaDeadLife = new FixedLengthStringData(8);
	private Atreqrec atreqrec = new Atreqrec();
		/*Claims Contract Header*/
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
		/*Claim Header file*/
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T6661rec t6661rec = new T6661rec();
	private Tranchkrec tranchkrec = new Tranchkrec();
	private Batckey wsaaBatckey = new Batckey();
	private S5238ScreenVars sv = ScreenProgram.getScreenVars( S5238ScreenVars.class);
	private WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1209, 
		exit1319, 
		preExit, 
		exit3090
	}

	public P5238() {
		super();
		screenVars = sv;
		new ScreenModel("S5238", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
{
	/*LGNM-100*/
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(cltsIO.getClttype(), "C")) {
		corpname();
		return ;
	}
	wsspcomn.longconfname.set(cltsIO.getSurname());
	/*LGNM-EXIT*/
}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsaaTranError = "N";
		wsaaNoDeath = "N";
		wsaaForwardTranscode.set(SPACES);
		wsaaMsgTrantype.set(SPACES);
		wsaaMsgTrandesc.set(SPACES);
		wsaaTransAreaInner.wsaaTransArea.set(SPACES);
		wsaaToday.set(ZERO);
		wsaaMsgTranno.set(ZERO);
		wsaaPrimaryChdrnum.set(ZERO);
		wsaaTransAreaInner.wsaaTranDate.set(ZERO);
		wsaaTransAreaInner.wsaaTranTime.set(ZERO);
		wsaaTransAreaInner.wsaaUser.set(ZERO);
		wsaaTransAreaInner.wsaaTodate.set(ZERO);
		wsaaTransAreaInner.wsaaSuppressTo.set(ZERO);
		wsaaTransAreaInner.wsaaTranNum.set(ZERO);
		wsaaTransAreaInner.wsaaPlnsfx.set(ZERO);
		wsaaTransAreaInner.wsaaCfiafiTranno.set(ZERO);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		/* Get today's date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		getT6661TransCode1100();
		setUpScreenFields1200();
		/* Check for a valid transaction, assuming that a register death*/
		/* of a life transaction has occurred previously.*/
		if (isEQ(wsaaNoDeath,"N")) {
			checkIfValidTrans1300();
		}
	}

	/**
	* <pre>
	**** Sections performed from the 1000 section above.
	* </pre>
	*/
protected void getT6661TransCode1100()
	{
		starts1100();
	}

protected void starts1100()
	{
		/* Obtain Forward Transaction Code from T6661.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		wsaaForwardTranscode.set(t6661rec.trcode);
	}

protected void setUpScreenFields1200()
	{
		starts1200();
	}

protected void starts1200()
	{
		/* Retrieve CHDRCLM stored in P5117 and set up screen header*/
		/* fields.*/
		chdrclmIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			fatalError600();
		}
		/* Read CLMHCLM with the key retrieved from the CHDRCLM.*/
		clmhclmIO.setDataArea(SPACES);
		clmhclmIO.setChdrcoy(chdrclmIO.getChdrcoy());
		clmhclmIO.setChdrnum(chdrclmIO.getChdrnum());
		clmhclmIO.setFormat(clmhclmrec);
		clmhclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(),varcom.oK)
		&& isNE(clmhclmIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clmhclmIO.getParams());
			syserrrec.statuz.set(clmhclmIO.getStatuz());
			fatalError600();
		}
		/* If there are no death claim details (Statuz = MRNF),*/
		/* initialise the screen values with spaces.*/
		if (isEQ(clmhclmIO.getStatuz(),varcom.mrnf)) {
			sv.chdrnum.set(chdrclmIO.getChdrnum());
			sv.dtofdeath.set(varcom.vrcmMaxDate);
			sv.dob.set(varcom.vrcmMaxDate);
			sv.reasoncd.set(SPACES);
			sv.resndesc.set(SPACES);
			scrnparams.errorCode.set(h225);
			sv.trandesOut[varcom.nd.toInt()].set("Y");
			wsspcomn.edterror.set("Y");
			wsaaNoDeath = "Y";
			return ;
		}
		else {
			sv.chdrnum.set(clmhclmIO.getChdrnum());
			sv.dtofdeath.set(clmhclmIO.getDtofdeath());
			sv.reasoncd.set(clmhclmIO.getReasoncd());
			sv.resndesc.set(clmhclmIO.getResndesc());
		}
		/* Read the life and joint life details.  Use a BEGN in case*/
		/* LIFE '01' has been deleted.*/
		lifemjaIO.setChdrcoy(chdrclmIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrclmIO.getChdrnum());
		lifemjaIO.setLife(clmhclmIO.getLife());
		lifemjaIO.setJlife(clmhclmIO.getJlife());
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		
		lifemjaIO.setFormat(lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		|| isNE(chdrclmIO.getChdrcoy(),lifemjaIO.getChdrcoy())
		|| isNE(chdrclmIO.getChdrnum(),lifemjaIO.getChdrnum())
		|| isNE(clmhclmIO.getLife(),lifemjaIO.getLife())
		|| isNE(clmhclmIO.getJlife(),lifemjaIO.getJlife())) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		/* Get the client name.*/
		sv.life.set(lifemjaIO.getLife());
		sv.jlife.set(lifemjaIO.getJlife());
		sv.lifenum.set(lifemjaIO.getLifcnum());
		wsaaDeadLife.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		sv.sex.set(cltsIO.getCltsex());
		sv.dob.set(cltsIO.getCltdob());
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void checkIfValidTrans1300()
	{
		starts1300();
	}

protected void starts1300()
	{
		/* Check that last active transaction is a Register Death of*/
		/* Life transaction by reading PTRNREV which returns all valid*/
		/* PTRN's in descending order by transaction number (TRANNO).*/
		tranchkrec.tcdeStatuz.set(varcom.oK);
		ptrnrevIO.setDataArea(SPACES);
		ptrnrevIO.setChdrcoy(chdrclmIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrclmIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		ptrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnrevIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		ptrnrevIO.setStatuz(varcom.oK);
		ptrnrevIO.setFormat(ptrnrevrec);
		while ( !(isEQ(ptrnrevIO.getBatctrcde(),wsaaForwardTranscode)
		|| isEQ(ptrnrevIO.getStatuz(),varcom.endp)
		|| isEQ(tranchkrec.tcdeStatuz,varcom.mrnf))) {
			readPtrn1310();
		}
		
		/* If the last TRANNO doesn't match then don't display*/
		/* conditional output fields and display error.*/
		if (isNE(ptrnrevIO.getBatctrcde(),wsaaForwardTranscode)) {
			sv.trandesOut[varcom.nd.toInt()].set("Y");
			sv.chdrnumErr.set(i034);
			wsspcomn.edterror.set("Y");
			wsaaTranError = "Y";
		}
	}

protected void readPtrn1310()
	{
		starts1310();
	}

protected void starts1310()
	{
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(ptrnrevIO.getChdrcoy(),chdrclmIO.getChdrcoy())
		|| isNE(ptrnrevIO.getChdrnum(),chdrclmIO.getChdrnum())
		|| isEQ(ptrnrevIO.getStatuz(),varcom.endp)) {
			ptrnrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(ptrnrevIO.getBatctrcde(),wsaaForwardTranscode)) {
			setMessages1320();
			return ;
		}
		/* Call the TRCDECHK subroutine to check that the Transaction*/
		/* Code is acceptable, based on what is on T6661. The proviso*/
		/* that the transaction is rejected if not the last one, is*/
		/* modified if any subsequent changes are minor, i.e. change*/
		/* of address. A MRNF will show that the change was important*/
		/* and thus the transaction will error.*/
		tranchkrec.codeCheckRec.set(SPACES);
		tranchkrec.tcdeTranCode.set(ptrnrevIO.getBatctrcde());
		tranchkrec.tcdeCompany.set(wsspcomn.company);
		callProgram(Trcdechk.class, tranchkrec.codeCheckRec);
		if (isNE(tranchkrec.tcdeStatuz,varcom.oK)
		&& isNE(tranchkrec.tcdeStatuz,varcom.mrnf)) {
			syserrrec.params.set(tranchkrec.codeCheckRec);
			syserrrec.statuz.set(tranchkrec.tcdeStatuz);
			fatalError600();
		}
		ptrnrevIO.setFunction(varcom.nextr);
	}

protected void setMessages1320()
	{
		starts1320();
	}

protected void starts1320()
	{
		wsaaMsgTranno.set(ptrnrevIO.getTranno());
		wsaaMsgTrantype.set(ptrnrevIO.getBatctrcde());
		/* Get transaction description from T1688.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(ptrnrevIO.getBatctrcde());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		/* Set messages and display.*/
		wsaaMsgTrandesc.set(descIO.getLongdesc());
		sv.trandes.set(wsaaProcessingMsg);
		sv.trandesOut[varcom.nd.toInt()].set(SPACES);
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/* Force the user to exit out of the screen if the register*/
		/* death of life cannot be reversed.*/
		if (isEQ(wsaaTranError,"Y")) {
			sv.chdrnumErr.set(i034);
		}
		if (isEQ(wsaaNoDeath,"Y")) {
			scrnparams.errorCode.set(h225);
			wsspcomn.edterror.set("Y");
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			updateDatabase3010();
			updateDatabase3020();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void updateDatabase3010()
	{
		/*  Call Softlock and call ATREQ:*/
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void updateDatabase3020()
	{
		/* Softlock the contract.*/
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.enttyp.set(chdrclmIO.getChdrpfx());
		sftlockrec.company.set(chdrclmIO.getChdrcoy());
		sftlockrec.entity.set(chdrclmIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		updateClient3600();
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrclmIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransAreaInner.wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransAreaInner.wsaaTranDate.set(varcom.vrcmDate);
		wsaaTransAreaInner.wsaaTranTime.set(varcom.vrcmTime);
		wsaaTransAreaInner.wsaaUser.set(varcom.vrcmUser);
		wsaaTransAreaInner.wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransAreaInner.wsaaTranNum.set(ptrnrevIO.getTranno());
		wsaaTransAreaInner.wsaaTranCode.set(ptrnrevIO.getBatctrcde());
		wsaaTransAreaInner.wsaaPlnsfx.set(ZERO);
		wsaaTransAreaInner.wsaaSupflag.set("N");
		wsaaTransAreaInner.wsaaTodate.set(ptrnrevIO.getPtrneff());
		wsaaTransAreaInner.wsaaSuppressTo.set(ZERO);
		wsaaTransAreaInner.wsaaCfiafiTranno.set(ZERO);
		wsaaTransAreaInner.wsaaCfiafiTranCode.set(SPACES);
		wsaaTransAreaInner.wsaaBbldat.set(ZERO);
		atreqrec.transArea.set(wsaaTransAreaInner.wsaaTransArea);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			syserrrec.statuz.set(atreqrec.statuz);
			fatalError600();
		}
	}

protected void updateClient3600()
{
	update3600();
}

protected void update3600()
{
	cltsIO.setClntpfx("CN");
	cltsIO.setClntcoy(wsspcomn.fsuco);
	/*     MOVE CHDRCLM-COWNNUM       TO CLTS-CLNTNUM.                 */
	cltsIO.setClntnum(wsaaDeadLife);
	/*   Read and hold the record for updating.                        */
	cltsIO.setFunction(varcom.readh);
	getClientDetails3700();
	/* IF CLTS-STATUZ               = O-K                   <V76F10>*/
	/*    MOVE '2'                 TO CLTS-VALIDFLAG        <V76F10>*/
	/*    MOVE CLTSREC             TO CLTS-FORMAT           <V76F10>*/
	/*    MOVE REWRT               TO CLTS-FUNCTION         <V76F10>*/
	/*                                                      <V76F10>*/
	/*    CALL 'CLTSIO'         USING CLTS-PARAMS           <V76F10>*/
	/*    IF CLTS-STATUZ        NOT = O-K                   <V76F10>*/
	/*       MOVE CLTS-PARAMS      TO SYSR-PARAMS           <V76F10>*/
	/*       MOVE CLTS-STATUZ      TO SYSR-STATUZ           <V76F10>*/
	/*       PERFORM 600-FATAL-ERROR                        <V76F10>*/
	/*    END-IF                                            <V76F10>*/
	/* END-IF.                                              <V76F10>*/
	/*   Re-initialise the Date of Death.                              */
	cltsIO.setCltdod(varcom.vrcmMaxDate);
	cltsIO.setClntpfx("CN");
	cltsIO.setClntcoy(wsspcomn.fsuco);
	/*     MOVE CHDRCLM-COWNNUM     TO CLTS-CLNTNUM.                   */
	cltsIO.setClntnum(wsaaDeadLife);
	cltsIO.setCltstat("AC");
	/*   Rewrite the record without the Date of Death.                 */
	/*  MOVE REWRT               TO CLTS-FUNCTION.          <V73F02>*/
	/*  MOVE '1'                 TO CLTS-VALIDFLAG.         <V76F10>*/
	/*  MOVE WRITR               TO CLTS-FUNCTION.          <V76F10>*/
	cltsIO.setFunction(varcom.rewrt);
	getClientDetails3700();
}

protected void getClientDetails3700()
{
	/*PARA*/
	/*  Call to I-O Module.                                            */
	cltsIO.setFormat(cltsrec);
	SmartFileCode.execute(appVars, cltsIO);
	if (isNE(cltsIO.getStatuz(), varcom.oK)
	&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(cltsIO.getParams());
		fatalError600();
	}
	/*EXIT*/
}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
	protected void whereNext4000()
		{
			/*NEXT-PROGRAM*/
			wsspcomn.programPtr.add(1);
			/*EXIT*/
		}
	/*
	 * Class transformed  from Data Structure WSAA-TRANS-AREA--INNER
	 */
	private static final class WsaaTransAreaInner { 
	
		private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(70);
		private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
		private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
		private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
		private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
		private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
		private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
		private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
		private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
		private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
		private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
		private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
		private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
		private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
		private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
	}
}
