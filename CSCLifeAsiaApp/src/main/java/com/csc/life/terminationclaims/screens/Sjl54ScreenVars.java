package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sjl54ScreenVars extends SmartVarModel{
	
	public FixedLengthStringData dataArea = new FixedLengthStringData(330);  
	public FixedLengthStringData dataFields = new FixedLengthStringData(170).isAPartOf(dataArea, 0);
	public FixedLengthStringData payNum = DD.reqno.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData claimNum =  DD.claimnumber.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData contractNum =  DD.chdrnum.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 26);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 29);
	public ZonedDecimalData payDate = DD.paydte.copyToZonedDecimal().isAPartOf(dataFields,59);
	public FixedLengthStringData reqntype =  DD.reqntype.copy().isAPartOf(dataFields,67);
	public ZonedDecimalData totalPayAmt = DD.qpaymnt.copyToZonedDecimal().isAPartOf(dataFields,68);
	public FixedLengthStringData clientnum = DD.lifcnum.copy().isAPartOf(dataFields,85);
	public FixedLengthStringData clntName = DD.linsname.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData scrndesc = DD.scrndesc.copy().isAPartOf(dataFields,140);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 170);
	public FixedLengthStringData payNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData claimNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData contractNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData payDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData reqntypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData totalPayAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData clientnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData clntNameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 210);
	public FixedLengthStringData[] payNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] claimNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] contractNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] payDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] reqntypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] totalPayAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] clientnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] clntNameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(408);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(229).isAPartOf(subfileArea, 0);
	public ZonedDecimalData seqenum = DD.seqno.copyToZonedDecimal().isAPartOf(subfileArea,0);
	public FixedLengthStringData clttwo = DD.clttwo.copy().isAPartOf(subfileArea,2);
	public FixedLengthStringData clntID = DD.linsname.copy().isAPartOf(subfileArea,10);
	public FixedLengthStringData babrdc = DD.babrdc.copy().isAPartOf(subfileArea,57);
	public FixedLengthStringData accdesc = DD.accdesc.copy().isAPartOf(subfileArea,117);
	public FixedLengthStringData bankacckey = DD.bankacckeyclaim.copy().isAPartOf(subfileArea,147);
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(subfileArea,167);
	public ZonedDecimalData prcent = DD.prcent.copyToZonedDecimal().isAPartOf(subfileArea,197);
	public ZonedDecimalData pymt = DD.pymt.copyToZonedDecimal().isAPartOf(subfileArea,202);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,219);
	public FixedLengthStringData cheqpaynum = DD.reqno.copy().isAPartOf(subfileFields,220);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(44).isAPartOf(subfileArea, 229);
	public FixedLengthStringData seqenumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData clttwoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData clntIDErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData babrdcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData accdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData prcentErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData pymtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData cheqpaynumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(132).isAPartOf(subfileArea,273);
	public FixedLengthStringData[] seqenumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] clttwoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] clntIDOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] babrdcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] accdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] prcentOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] pymtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] cheqpaynumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea,405);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public FixedLengthStringData payDateDisp = new FixedLengthStringData(10);
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	
	public LongData Sjl54screensflWritten = new LongData(0);
	public LongData Sjl54screenctlWritten = new LongData(0);
	public LongData Sjl54screenWritten = new LongData(0);
	public LongData Sjl54windowWritten = new LongData(0);
	public LongData Sjl54protectWritten = new LongData(0);
	public GeneralTable sjl54screensfl = new GeneralTable(AppVars.getInstance());

	public GeneralTable getScreenSubfileTable() {
		return sjl54screensfl;
	}

	public Sjl54ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		fieldIndMap.put(payNumOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimNumOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(contractNumOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cnttypeOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypedesOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payDateOut,new String[] {"08","30", "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reqntypeOut,new String[] {"09","29", "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(totalPayAmtOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clientnumOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntNameOut,new String[] {"7",null, "-7",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(seqenumOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clttwoOut,new String[] {"13","23", "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntIDOut,new String[] {"14","24", "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(babrdcOut,new String[] {"15","25", "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(accdescOut,new String[] {"16","26", "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankacckeyOut,new String[] {"17","27", "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankaccdscOut,new String[] {"18","28", "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prcentOut,new String[] {"19","21", "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pymtOut,new String[] {"20","22", "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"31","32", "-31",null, null, null, null, null, null, null, null, null});
		
		
		screenFields = new BaseData[] {payNum, claimNum, contractNum, cnttype, ctypedes, reqntype, totalPayAmt, clientnum, clntName};
		screenOutFields = new BaseData[][] {payNumOut, claimNumOut, contractNumOut, cnttypeOut, reqntypeOut, totalPayAmtOut, clientnumOut, clntNameOut};
		screenErrFields = new BaseData[] {payNumErr, claimNumErr, contractNumErr, cnttypeErr, reqntypeErr, totalPayAmtErr, clientnumErr, clntNameErr};
		
		screenDateFields = new BaseData[] {payDate};
		screenDateErrFields = new BaseData[] {payDateErr};
		screenDateDispFields = new BaseData[] {payDateDisp};
		
		screenSflFields = new BaseData[] {seqenum, clttwo, clntID, babrdc, accdesc, bankacckey, bankaccdsc, prcent, pymt, select, cheqpaynum};
		screenSflOutFields = new BaseData[][] {seqenumOut, clttwoOut, clntIDOut, babrdcOut, accdescOut, bankacckeyOut, bankaccdscOut, prcentOut, pymtOut, selectOut, cheqpaynumOut};
		screenSflErrFields = new BaseData[] {seqenumErr, clttwoErr, clntIDErr,babrdcErr, accdescErr, bankacckeyErr, bankaccdscErr, prcentErr, pymtErr, selectErr, cheqpaynumErr};
		
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sjl54screen.class;
		screenSflRecord = Sjl54screensfl.class;
		screenCtlRecord = Sjl54screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sjl54protect.class;
		
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl54screenctl.lrec.pageSubfile);
	}

	private long maxRow;
	
	public long getMaxRow() {
		return maxRow;
	}

	public void setMaxRow(long maxRow) {
		this.maxRow = maxRow;
	}
	
	public boolean hasSubfile() {
		return true;
	}
}

