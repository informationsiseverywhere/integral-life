package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: SurhpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:30
 * Class transformed from SURHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class SurhpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 223;
	public FixedLengthStringData surhrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData surhpfRecord = surhrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(surhrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(surhrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(surhrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(surhrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(surhrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(surhrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(surhrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(surhrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(surhrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(surhrec);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(surhrec);
	public PackedDecimalData policyloan = DD.policyloan.copy().isAPartOf(surhrec);
	public PackedDecimalData tdbtamt = DD.tdbtamt.copy().isAPartOf(surhrec);
	public PackedDecimalData taxamt = DD.taxamt.copy().isAPartOf(surhrec);
	public PackedDecimalData otheradjst = DD.otheradjst.copy().isAPartOf(surhrec);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(surhrec);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(surhrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(surhrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(surhrec);
	public PackedDecimalData zrcshamt = DD.zrcshamt.copy().isAPartOf(surhrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(surhrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(surhrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(surhrec);
	public PackedDecimalData suspenseamt = DD.sacscurbal.copy().isAPartOf(surhrec);
	public PackedDecimalData unexpiredprm = DD.unexpiredprm.copy().isAPartOf(surhrec);
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public SurhpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for SurhpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public SurhpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for SurhpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public SurhpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for SurhpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public SurhpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("SURHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"PLNSFX, " +
							"LIFE, " +
							"JLIFE, " +
							"TRANNO, " +
							"TERMID, " +
							"TRDT, " +
							"USER_T, " +
							"EFFDATE, " +
							"CURRCD, " +
							"POLICYLOAN, " +
							"TDBTAMT, " +
							"TAXAMT, " +
							"OTHERADJST, " +
							"REASONCD, " +
							"RESNDESC, " +
							"CNTTYPE, " +
							"TRTM, " +
							"ZRCSHAMT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"SUSPENSEAMT, "+
							"UNEXPIREDPRM, "+
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     planSuffix,
                                     life,
                                     jlife,
                                     tranno,
                                     termid,
                                     transactionDate,
                                     user,
                                     effdate,
                                     currcd,
                                     policyloan,
                                     tdbtamt,
                                     taxamt,
                                     otheradjst,
                                     reasoncd,
                                     resndesc,
                                     cnttype,
                                     transactionTime,
                                     zrcshamt,
                                     userProfile,
                                     jobName,
                                     datime,
                                     suspenseamt,
                                     unexpiredprm,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		planSuffix.clear();
  		life.clear();
  		jlife.clear();
  		tranno.clear();
  		termid.clear();
  		transactionDate.clear();
  		user.clear();
  		effdate.clear();
  		currcd.clear();
  		policyloan.clear();
  		tdbtamt.clear();
  		taxamt.clear();
  		otheradjst.clear();
  		reasoncd.clear();
  		resndesc.clear();
  		cnttype.clear();
  		transactionTime.clear();
  		zrcshamt.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		suspenseamt.clear();
  		unexpiredprm.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getSurhrec() {
  		return surhrec;
	}

	public FixedLengthStringData getSurhpfRecord() {
  		return surhpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setSurhrec(what);
	}

	public void setSurhrec(Object what) {
  		this.surhrec.set(what);
	}

	public void setSurhpfRecord(Object what) {
  		this.surhpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(surhrec.getLength());
		result.set(surhrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}