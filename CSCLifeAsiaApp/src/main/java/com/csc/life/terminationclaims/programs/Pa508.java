package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;

import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.csc.fsu.general.recordstructures.Alocnorec;



import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.smart.procedures.Genssw;
import com.quipoz.framework.datatype.BaseScreenData;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.FlupclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.ClnnpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.InvspfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NohspfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.life.terminationclaims.dataaccess.model.Nohspf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.life.terminationclaims.screens.Sa508ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.dao.CrelpfDAO;
import com.csc.fsu.clients.dataaccess.model.Crelpf;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.smart.recordstructures.Subprogrec;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The details of  the  contract  being  enquired  upon  will be
*     stored in the  CHDRENQ  I/O  module. Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*     The PAYR file is read for the contract,followed by T3620.
*     If the contract requires bank details(i.e Direct Debit with
****  T3620-DDIND NOT = SPACES),then the Mandate file is read
****  to obtain the Mandate detalis.
*     T3620-DDIND NOT = SPACES or Credit Card with T3620-CRCIND
*     NOT = SPACES),then the Mandate file is read to obtain the
*     Mandate detalis.
*
*     Set up the  Factoring  House, (CHDR-FACTHOUS), on the screen.
*     Valid Factoring House  codes  are held on T3684. (See GETDESC
*     for an example of how to obtain the long description from the
*     DESC data-set. Do  not  call  GETDESC,  the  appropriate code
*     should be moved into the mainline.
*
*     Set up the  Bank  Code,  (CHDR-BANKKEY),  on  the  screen and
*     obtain the  corresponding  description  from the Bank Details
*     data-set BABR.  Read  BABR  with  a  key  of CHDR-BANKKEY and
*     obtain BABR-BANKDESC.
*
*     Set up the  Account  Number, (CHDR-BANKACCKEY), on the screen
*     and  obtain  the  correeponding  description  from  the  CLBL
*     data-set.  Read  CLBL   with   a   key  of  CHDR-BANKKEY  and
*     CHDR-BANKACCKEY and obtain CLBL-BANKADDDSC and CLBL-CURRCODE.
*
*     If the Assignee client, (CHDR-ASGNNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the usual way.
*
*     If the Despatch client, (CHDR-DESPNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the ususal way.
*
*
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The only validation  required  is  of  the  indicator field -
*     CONBEN. This may be space or 'X'.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was  pressed  move  spaces  to  the current program
*     position in the program stack and exit.
*
*     If returning from  processing  a  selection  further down the
*     program stack, (current  stack  action  field  will  be '*'),
*     restore the next  8  programs  from  the  WSSP and remove the
*     asterisk.
*
*     If nothing was  selected move '*' to the current stack action
*     field, add 1 to the program pointer and exit.
*
*     If a selection has  been found use GENSWCH to locate the next
*     program(s)  to process  the  selection,  (up  to  8).  Use  a
*     function of 'A'. Save  the  next  8 programs in the stack and
*     replace them with  the  ones  returned from GENSWCH. Place an
*     asterisk in the  current  stack  action  field,  add 1 to the
*     program pointer and exit.
*
*
* Notes.
* ------
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5688 - Contract Structure                Key: CNTTYPE
* T3684 - Factoring House                   Key: FACTHOUS
* T3620 - Billing Channel                   Key: BILLCHNL
*
*
*****************************************************************
* </pre>
*/
public class Pa508 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final String CLMPREFIX = "CLMNTF";
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA508");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private static final String NF = "NF";
	private static final String NEXT = "NEXT";
	
	private static final String t1688 = "T1688";
	
	private Chdrpf chdrpf = new Chdrpf();

	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sa508ScreenVars sv = getPScreenVars() ;

	private Clntpf clntpf;
	private Clexpf clexpf;
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	private NohspfDAO nohspfDAO = getApplicationContext().getBean("nohspfDAO", NohspfDAO.class);
	
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ClexpfDAO clexpfDAO = getApplicationContext().getBean("clexpfDAO", ClexpfDAO.class);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private CrelpfDAO crelpfDAO = getApplicationContext().getBean("crelpfDAO", CrelpfDAO.class);
	private Crelpf crelpf = null;	
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private FixedLengthStringData wsaaGenkey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaGenkey, 2);
	private Fluppf fluppf = null ;
	protected static final String h093 = "H093";
	private Batckey wsaaBatchkey = new Batckey();
	private Subprogrec subprogrec = new Subprogrec();

	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	protected static final String chdrlnbrec = "CHDRLNBREC";
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Notipf notipf=new Notipf();
	private Nohspf nohspf=new Nohspf();
	
	private ErrorsInner errorsInner = new ErrorsInner();
	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	protected static final String e186 = "E186";
	 String inctype="DC";
	 String inctype1="RP";
	 String rgpytype="HS";
	  private int count=0;	//ICIL-1286
	 private String notifistatus="Open";
	 private String notifistatuscl="Closed";
	 private String claimRegi="Claim Registered";
	 private String wsaaChdrnum=null;
	 private String CLMNTF="CLMNTF";
	 private Alocnorec alocnorec = new Alocnorec();
	 private FlupclmTableDAM flupclmIO = new FlupclmTableDAM();	//ICIL-1286
	 private Notipf notipf2=new Notipf();
	 private Tr386rec tr386rec = new Tr386rec();
	private String notifinum="";
	private int transnoCount = 1;
	private int noTransnoCount = 1;
	 //ICIL-1502
	 private String effectiveFlag="1";
	 private String expiredFlag="2";

	private boolean clntClaimant  = false;
	private String wsaaCltreln = null;
	private List<Clnnpf> clnnpfList = new ArrayList<Clnnpf>();
	private ClnnpfDAO clnnpfDAO= getApplicationContext().getBean("clnnpfDAO",ClnnpfDAO.class);
	private List<Invspf> invspfList = new ArrayList<Invspf>();
	private InvspfDAO invspfDAO= getApplicationContext().getBean("invspfDAO",InvspfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);

	 


	public Pa508() {
		super();
		screenVars = sv;
		new ScreenModel("Sa508", AppVars.getInstance(), sv);
	}

	protected Sa508ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sa508ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		
		}catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
			e.printStackTrace();
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
			e.printStackTrace();
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		
		initialise1010();
		setscreen();
	}

private void setscreen() {

	
	
	/*    If entering on enquiry or close then protect the screen fields.*/
	if (isEQ(wsspcomn.flag, "I") || isEQ(wsspcomn.flag, "D")) {
		sv.clntselOut[varcom.pr.toInt()].set("Y");
		sv.cltrelnOut[varcom.pr.toInt()].set("Y");
		sv.incurdtOut[varcom.pr.toInt()].set("Y");
		sv.locationOut[varcom.pr.toInt()].set("Y");
		sv.inctypeOut[varcom.pr.toInt()].set("Y");
		sv.cltdodxOut[varcom.pr.toInt()].set("Y");
		sv.causedeathOut[varcom.pr.toInt()].set("Y");
		sv.rgpytypeOut[varcom.pr.toInt()].set("Y");
		sv.clamamtOut[varcom.pr.toInt()].set("Y");
		sv.zdoctorOut[varcom.pr.toInt()].set("Y");
		sv.zmedprvOut[varcom.pr.toInt()].set("Y");
		sv.hospitalLevelOut[varcom.pr.toInt()].set("Y");
		sv.diagcdeOut[varcom.pr.toInt()].set("Y");
		sv.admiDateOut[varcom.pr.toInt()].set("Y");
		sv.dischargeDateOut[varcom.pr.toInt()].set("Y");
		sv.accdescOut[varcom.pr.toInt()].set("Y");
	}
	
		sv.cltreln.setEnabled(BaseScreenData.ENABLED);
		sv.clntsel.setEnabled(BaseScreenData.ENABLED);
	
		
	/*	judge if needs enquiring an notification*/
		if(isEQ(wsspcomn.flag, "I")) {
			sv.notifhistoryOut[varcom.pr.toInt()].set("Y");
//			sv.notifnotes.setEnabled(BaseScreenData.ENABLED);
		}
		if(isEQ(wsspcomn.flag, "C")) {
			sv.notifhistoryOut[varcom.nd.toInt()].set("N");
		}
		
	}



protected void initialise1010()
	{
	/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*      GO TO 1090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		
		/**
		 * need to identify if we select two records or more,if we finish
		 * the hyperlink,and click the continue
		 */
		String chdrNumStr = wsspcomn.chdrNumList.getData();
		ArrayList<String> chdrnumList=new ArrayList(Arrays.asList(chdrNumStr.split(",")));
		// get screen head
		getLifeAssSection();
		//If we click csreate
		if(isEQ(wsspcomn.flag, "C")){
			datcon1rec.datcon1Rec.set(SPACES);
			datcon1rec.intDate.set(ZERO);
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				syserrrec.statuz.set(datcon1rec.statuz);
				fatalError600();
			}
			wsaaToday.set(datcon1rec.intDate);
			//auto genarate Notifinum number
			allocateNumber2600();
			sv.notifiDate.set(wsaaToday);
			sv.cltdodxDisp.set(SPACES);
			sv.incurdtDisp.set(SPACES);
			sv.admiDateDisp.set(SPACES);
			sv.dischargeDateDisp.set(SPACES);
		}else if(isEQ(wsspcomn.flag, "M")|| isEQ(wsspcomn.flag, "D")||isEQ(wsspcomn.flag, "I")){
			//get notification number
			//notipf2=notipfDAO.getNotiReByNotifin(wsspcomn.chdrChdrnum.getData().toString(), wsspcomn.company.toString());
			notipf2=notipfDAO.getNotiReByNotifinAndValidflag(wsspcomn.chdrChdrnum.getData(), wsspcomn.company.toString(),effectiveFlag);/* IJTI-1523 */
			sv.notifinum.set(CLMNTF+notipf2.getNotifinum());
			notifinum=notipf2.getNotifinum();
			wsspcomn.wsaanotificationNum.set(sv.notifinum);
			//get relationship longdesc by relationship code
			sv.cltreln.set(notipf2.getRelationcnum());
			sv.clntsel.set(notipf2.getClaimant());
			getClaimantSection();
			sv.notifiDate.set(notipf2.getNotifidate());
			sv.incurdt.set(notipf2.getIncurdate());
			sv.location.set(notipf2.getLocation());
			//get incident type inctype
			sv.inctype.set(notipf2.getIncidentt());
			sv.cltdodx.set(notipf2.getDeathdate());
			// cause of death causedeath
			sv.causedeath.set(notipf2.getCausedeath());
			sv.rgpytype.set(notipf2.getRclaimreason());
		
			
			sv.clamamt.set(notipf2.getClaimat());
			sv.zdoctor.set(notipf2.getDoctor());
			// medical provider zmedprv
			sv.zmedprv.set(notipf2.getMedicalpd());
			// Hospital Level hospitalLevel
			sv.hospitalLevel.set(notipf2.getHospitalle());
			// Diagnosis diagcde
			sv.diagcde.set(notipf2.getDiagnosis());
			sv.admiDate.set(notipf2.getAdmintdate());
			sv.dischargeDate.set(notipf2.getDischargedate());
			sv.accdesc.set(notipf2.getAccidentdesc());
			gensswrec.function.set(" ");
		}if(isEQ(wsspcomn.WsspCoinsuranceFlag,"T")) {
			//ICIL-1502 retrive the notification
			Notipf notipf = notipfDAO.getExactNotipfByTransno(wsspcomn.wsaanotificationNum.toString().trim().replace(CLMPREFIX, ""),wsspcomn.tranno.getData().toString().trim());
			sv.notifinum.set(CLMNTF+notipf.getNotifinum());
			//get relationship longdesc by relationship code
			sv.cltreln.set(notipf.getRelationcnum());
			sv.clntsel.set(notipf.getClaimant());
			getClaimantSection();
			sv.notifiDate.set(notipf.getNotifidate());
			sv.incurdt.set(notipf.getIncurdate());
			sv.location.set(notipf.getLocation());
			//get incident type inctype
			sv.inctype.set(notipf.getIncidentt());
			sv.cltdodx.set(notipf.getDeathdate());
			// cause of death causedeath
			sv.causedeath.set(notipf.getCausedeath());
			sv.rgpytype.set(notipf.getRclaimreason());
			sv.clamamt.set(notipf.getClaimat());
			sv.zdoctor.set(notipf.getDoctor());
			// medical provider zmedprv
			sv.zmedprv.set(notipf.getMedicalpd());
			// Hospital Level hospitalLevel
			sv.hospitalLevel.set(notipf.getHospitalle());
			// Diagnosis diagcde
			sv.diagcde.set(notipf.getDiagnosis());
			sv.admiDate.set(notipf.getAdmintdate());
			sv.dischargeDate.set(notipf.getDischargedate());
			sv.accdesc.set(notipf.getAccidentdesc());
			gensswrec.function.set(" ");
			wsspcomn.WsspCoinsuranceFlag.clear();
		}
		 setTitleName();
			}
	

private void setTitleName() {
	
	wsaaTr386Lang.set(wsspcomn.language);
	wsaaTr386Pgm.set(wsaaProg);
	Itempf itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "TR386",wsaaTr386Key.toString());
	
	tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	
	if (isEQ(wsspcomn.flag, "C")) {
		sv.scrndesc.set(tr386rec.progdesc[1].toString());
	} else if (isEQ(wsspcomn.flag, "M")) {
		sv.scrndesc.set(tr386rec.progdesc[2].toString());
	}else if (isEQ(wsspcomn.flag, "D")) {
		sv.scrndesc.set(tr386rec.progdesc[3].toString());
	}else if (isEQ(wsspcomn.flag, "I")) {
		sv.scrndesc.set(tr386rec.progdesc[4].toString());
	}
	
	
}

/**
 * <pre>
 *    Used to be 3200-ALLOCATE-NUMBER !!!
 * </pre>
 */
protected void allocateNumber2600()
{
	alocnorec.function.set(NEXT);
	alocnorec.prefix.set(NF);
	alocnorec.company.set(wsspcomn.company);
	wsaaBranch.set("");
	alocnorec.genkey.set(wsaaGenkey);
	callProgram(Alocno.class, alocnorec.alocnoRec);
	if (isEQ(alocnorec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(alocnorec.statuz);
		fatalError600();
	}
	if (isNE(alocnorec.statuz, varcom.oK)) {
		sv.notifinumErr.set(alocnorec.statuz);
	}
	else {
		
		sv.notifinum.set(CLMPREFIX+alocnorec.alocNo);
		//put Notifinum number into cache
		wsspcomn.wsaanotificationNum.set(sv.notifinum);
		notifinum=alocnorec.alocNo.toString();
	}
}
	
	private void getClaimantSection() {
		clexpf=clexpfDAO.getClexpfByClntkey("CN", wsspcomn.fsuco.toString(), sv.clntsel.toString().trim());
		if(clexpf!=null){
			sv.rinternet.set(clexpf.getRinternet());
			if(isEQ(sv.cltphoneidd, SPACES)||sv.cltphoneidd.getFormData().trim()==""||sv.cltphoneidd.getFormData()==null){
				sv.cltphoneidd.set(clexpf.getRmblphone());
			}
		}
		
		clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), sv.clntsel.toString().trim());
		if(clntpf!=null){
			clntClaimant = false;
			sv.cidtype.set(clntpf.getIdtype());
			sv.csecuityno.set(clntpf.getSecuityno());
			sv.ccltsex.set(clntpf.getCltsex());
			sv.cltaddr01.set(clntpf.getCltaddr01());
			sv.cltaddr02.set(clntpf.getCltaddr02());
			sv.cltaddr03.set(clntpf.getCltaddr03());
			sv.cltaddr04.set(clntpf.getCltaddr04());
			sv.cltaddr05.set(clntpf.getCltaddr05());
			sv.cltpcode.set(clntpf.getCltpcode());
			sv.cltname.set(clntpf.getSurname().trim()+","+clntpf.getGivname().trim());
			if(isEQ(sv.cltphoneidd, SPACES)||sv.cltphoneidd.getFormData().trim()==""||sv.cltphoneidd.getFormData()==null){
				sv.cltphoneidd.set(clntpf.getCltphone02());
			}
			if(isEQ(sv.cltphoneidd, SPACES)||sv.cltphoneidd.getFormData().trim()==""||sv.cltphoneidd.getFormData()==null){
				sv.cltphoneidd.set(clntpf.getCltphone01());
			}
			
		}
		else {
			clntClaimant = true;
		}	
}

	private void getLifeAssSection() {
	try {	
		clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), wsspcomn.chdrCownnum.toString().trim());
		if(clntpf!=null){
			sv.lifcnum.set(wsspcomn.chdrCownnum);
			String Surname = clntpf.getSurname() != null ? clntpf
						.getSurname().trim() : " ";
				String GivenName = clntpf.getGivname() != null ? clntpf
						.getGivname().trim() : " ";
				sv.lifename.set(Surname + "," + GivenName);

			sv.lidtype.set(clntpf.getIdtype());
			sv.lsecuityno.set(clntpf.getSecuityno());
			//Get Gender value
			sv.lcltsex.set(clntpf.getCltsex());
		}
} catch (Exception e) {
			e.printStackTrace();
		
		}	
		
	}

protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* IF   WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                  */
		/*      MOVE O-K               TO WSSP-EDTERROR                 */
		/*      GO TO 2090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
	}
	

	


protected void screenEdit2000()
	{
		screenIo2010();
		
		//CHECK-FOR-ERRORS
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		//EXIT	
		
	}

	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
		
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			return;
			
		}
		
		wsspcomn.edterror.set(varcom.oK);
		/*    Catering for F11.                                    <V4L011>*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return;
		
		}
		validate2020();
	}

protected void validate2020()
	{
	 if (isEQ(scrnparams.statuz, varcom.kill)) {
         return;
     }
     if (isEQ(wsspcomn.flag, "I")) {
    	 if (isEQ(sv.fupflg, "X")) {
    		 keepChdr3071();
    	 }
         return;
     }
     if (isEQ(scrnparams.statuz, varcom.calc)) {
         wsspcomn.edterror.set("Y");
     }
     

    if(isEQ(sv.clntsel, SPACES)) {
 	   	sv.clntselErr.set(errorsInner.RRCQ);
 		return ;
 	}
    
	getClaimantSection();
	
	if(clntClaimant){
		sv.clntselErr.set(errorsInner.RPGR);
		return;
	}
	
	getRelationShip();
	
	
	if(sv.cltreln.equals(SPACES)||sv.cltreln==null){
   	 sv.cltrelnErr.set(errorsInner.RRNJ);
			return ;
	}

	if(sv.incurdtDisp.equals(SPACES)||sv.incurdtDisp==null){
   	 sv.incurdtErr.set(e186);
			return ;
	}
	
	if (isLT(sv.notifiDate, sv.incurdt)) {
		sv.notifiDateErr.set(errorsInner.p132);
		wsspcomn.edterror.set("Y");
	}
	
	if(sv.inctype.equals(SPACES)||sv.inctype==null){
	   	 sv.inctypeErr.set(e186);
				return ;
		}
	
	
	
	if(sv.inctype.getData().trim().equals(inctype)){
		if(isEQ(sv.cltdodxDisp, SPACES)){
	    	sv.cltdodxErr.set(e186);
	    	return ;
	    }else{
	    	if(isEQ(sv.causedeath, SPACES)){
	    		sv.causedeathErr.set(e186);
	    		return ;
	    	}
	    }
		
	}else if(sv.inctype.getData().trim().equals(inctype1)){
		if (isEQ(sv.rgpytype, SPACES)) {
			sv.rgpytypeErr.set(e186);
			return ;
		}
	}
	if(sv.rgpytype.getData().trim().equals(rgpytype)){
		if(isEQ(sv.hospitalLevel, SPACES)){
			sv.hospitalLevelErr.set(e186);
			return ;
		}else{
			if(isEQ(sv.diagcde, SPACES)){
				sv.diagcdeErr.set(e186);
				return ;
			}else{
				if(isEQ(sv.admiDateDisp, SPACES)){
					sv.admiDateErr.set(e186);
					return ;
				}else{
					
					if(isEQ(sv.dischargeDateDisp, SPACES)){
						sv.dischargeDateErr.set(e186);
						return ;
					}else{
						if(isLTE(sv.clamamt,ZERO)){
							sv.clamamtErr.set(e186);
							return ;
							
						}else{
							if(isEQ(sv.zdoctor,SPACES)){
								sv.zdoctorErr.set(e186);
								return ;
							}else{
								if(isEQ(sv.zmedprv,SPACES)){
									sv.zmedprvErr.set(e186);
									return ;
								}
							}
						}	
					}
				}
			}
		}
			
	}
	if(isEQ(wsspcomn.flag, "C")){
		ArrayList<String> chdrnumList=new ArrayList(Arrays.asList(wsspcomn.chdrNumList.getData().split(",")));
		if(chdrnumList != null && !chdrnumList.isEmpty())
			wsaaChdrnum=chdrnumList.get(0).trim();
	}	
	keepChdr3071();
	}
	private void keepChdr3071(){
		if(wsaaChdrnum==null || wsaaChdrnum.isEmpty()){
			notipf=notipfDAO.getChdrByFinum(notifinum);
			if(notipf != null){
				wsaaChdrnum=notipf.getChdrnum();
			}
		}
		if(wsaaChdrnum!=null && !wsaaChdrnum.isEmpty()){
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(wsaaChdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		if (isNE(wsaaChdrnum, SPACES)) {
			SmartFileCode.execute(appVars, chdrlnbIO);
		}
		else {
			chdrlnbIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
				&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());			
			fatalError600();
		}
		keeps3070();
		}
	}

	private void getRelationShip() {
		
		crelpf = crelpfDAO.getMcrlByCoyAndNum(wsspcomn.fsuco.toString(), wsspcomn.chdrCownnum.toString(), wsspcomn.fsuco.toString(), sv.clntsel.toString());
		if (crelpf!=null && isEQ(sv.cltreln, SPACES)) {
			sv.cltreln.set(crelpf.getCltreln());
			wsaaCltreln= crelpf.getCltreln();/* IJTI-1523 */
			return ;
		}
	
}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
	if (isEQ(scrnparams.statuz,varcom.kill)|| (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*"))) {	//ICIL-1286
		wsspcomn.chdrChdrnum.set(sv.notifinum.toString().replace(CLMNTF, ""));
		return;
	}
	if (isEQ(sv.fupflg, "X")) {
		return ;
	}
	if (isEQ(sv.invresults, "X")) {
		return ;
	}
	if (isEQ(sv.notifnotes, "X")) {
		return ;
	}
	if (isEQ(sv.notifhistory, "X")) {
		return ;
	}
		if(isEQ(wsspcomn.flag, "C")){
			insertNotipfRecord();
			// ICIL-1502
			nohspf.setTransdesc(notifistatus);
			insertNohspf3020();
		}
		// ICIL-1502
		else if (isEQ(wsspcomn.flag, "M")) {
			// This method is to update old flag to 2 and insert new data with flag 1
			modifyNotipfRecord();
			nohspf.setTransdesc(claimRegi);
			insertNohspf3020();
		} else {
			if (isEQ(wsspcomn.flag, "D")) {
				// update notification status to 'Closed'
				closeNotipfRecord();
//				nohspf.setTransdesc(notifistatuscl);
//				insertNohspfForClose3030();
//				}else if(isEQ(wsspcomn.flag, "M")){
//					updateNotipfRecord();
//				}
			}

		}
	/*If user make change to the relationship on this screen, 
	 * the change will also take effect on the Establish Client 
	 * Relationship screen S2420.*/		
	if(wsaaCltreln == null){
		//If it's null in table crelpf.we need to insert else we need to update
		crelpf = new Crelpf();
		crelpf.setClntcoy(wsspcomn.fsuco.toString());
		crelpf.setMcltcoy(wsspcomn.fsuco.toString());
		crelpf.setMcltnum(wsspcomn.chdrCownnum.toString());
		crelpf.setClntnum(sv.clntsel.toString());
		crelpf.setCltreln(sv.cltreln.toString());
		crelpf.setValidflag("1");
		varcom.vrcmTranid.set(wsspcomn.tranid);
		crelpf.setTermid(varcom.vrcmTermid.toString());
		varcom.vrcmTimex.set(getCobolTime());
		varcom.vrcmTime.set(varcom.vrcmTimen);
		varcom.vrcmDate.set(getCobolDate());
		crelpf.setTermid(varcom.vrcmTermid.toString());
		crelpf.setUser_t(varcom.vrcmUser.toInt());
		crelpf.setTrdt(varcom.vrcmDate.toInt());
		crelpf.setTrtm(varcom.vrcmTime.toInt());
		
		if (crelpfDAO.inertMcrl(crelpf)==0) {
			syserrrec.params.set(wsspcomn.fsuco.toString().concat(wsspcomn.chdrCownnum.toString()).concat(sv.clntsel.toString()));
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}	
	}else{
		if(isNE(sv.cltreln,wsaaCltreln)) {
			crelpf = new Crelpf();
			crelpf.setClntcoy(wsspcomn.fsuco.toString());
			crelpf.setMcltcoy(wsspcomn.fsuco.toString());
			crelpf.setMcltnum(wsspcomn.chdrCownnum.toString());
			crelpf.setClntnum(sv.clntsel.toString());
			crelpf.setCltreln(sv.cltreln.toString());
			if (crelpfDAO.updateMcrl(crelpf)==0) {
				syserrrec.params.set(wsspcomn.fsuco.toString().concat(wsspcomn.chdrCownnum.toString()).concat(sv.clntsel.toString()));
				syserrrec.statuz.set("MRNF");
				fatalError600();
			}	
		}
		
	}
	updateWssp3010();
	}

private void closeNotipfRecord() {
	//update Notipf validflag to 2
	setNotipf();
	notipf.setNotifinum(notipf2.getNotifinum());
	notipfDAO.updateNotipfValidFlagToClose(expiredFlag,notipf2.getNotifinum());
	//insert new Notipf record with validFlag 1 and Nohspf;
	notipf.setValidFlag(effectiveFlag);
	notipf.setNotifistatus(notifistatuscl);
	addTransnoToNotipf(notipf.getNotifinum());
	notipfDAO.insertNotipf(notipf);
	insertNohspfForClose3030(notipf);
}

public void insertNohspfForClose3030(Notipf notipf){
	datcon1rec.function.set(varcom.tday);
    Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
    wsaaToday.set(datcon1rec.intDate);
    nohspf.setNotihycoy(wsspcomn.company.toString().trim());
    nohspf.setNotihsnum(notipf.getNotifinum());
	nohspf.setTransdate(wsaaToday.toString().trim());
	nohspf.setEffectdate(wsaaToday.toInt());
	nohspf.setTranscode(wsaaBatckey.batcBatctrcde.getData());
	nohspf.setTransdesc(notifistatuscl);
//	descpf=descDAO.getdescData("IT", t1688, "TAPL", wsspcomn.company.toString(), wsspcomn.language.toString());
//	if(descpf==null){
//		nohspf.setTransdesc(descpf.getLongdesc());	
//	}
	addNewNotificationRecordForClose(nohspf);
}

private void modifyNotipfRecord() {
	//update Notipf validflag to 2
	setNotipf();
	notipfDAO.updateNotipfValidFlag(expiredFlag,notipf.getNotifinum());
	//insert new Notipf record with validFlag 1 and Nohspf;
	notipf.setValidFlag(effectiveFlag);
	notipf.setNotifistatus(notifistatus);
	//ICIL-15 modified
//	addTransnoToNotipf(alocnorec.alocNo.toString().trim());
	addTransnoToNotipf(wsspcomn.wsaanotificationNum.toString().trim().replace(CLMPREFIX, ""));
	notipfDAO.insertNotipf(notipf);
}

private void updateNotipfRecord() {
	notifistatus=claimRegi;
	setNotipf();
	notipf.setNotifinum(notifinum.trim());
	notipfDAO.updateNotipf(notipf);
	

	}

private void insertNotipfRecord() {
	count++;
	//insert notipf
	String chdrNumStr = wsspcomn.chdrNumList.getData();
	ArrayList<String> chdrnumList=new ArrayList(Arrays.asList(chdrNumStr.split(",")));
	setNotipf();
	//ICIL-1502
	notipf.setValidFlag(effectiveFlag);
	//ICIL-1502 add transno
	addTransnoToNotipf(alocnorec.alocNo.toString().trim());
	//check if system already insert record
	if(null==notipfDAO.getChdrByFinum(alocnorec.alocNo.toString().trim())){
		notipf.setChdrnum(chdrnumList.get(count-1).trim());
		wsaaChdrnum=chdrnumList.get(count-1).trim();
		notipf.setUsrprf(wsspcomn.userid.toString());
		notipfDAO.insertNotipf(notipf);

	}
}

private void addTransnoToNotipf(String notifinum) {
	int newTransno;
	String maxTransnum = notipfDAO.getMaxTransno(notifinum);
	if("".equals(maxTransnum) || maxTransnum==null) {
		notipf.setTransno(String.valueOf(noTransnoCount));
	}else {
		newTransno = Integer.valueOf(maxTransnum.trim())+noTransnoCount;	//IBPLIFE-1071
		notipf.setTransno(String.valueOf(newTransno));
	}
}

private void setNotipf() {
	notipf.setAccidentdesc(sv.accdesc.toString().trim());
	notipf.setAdmintdate(sv.admiDate.toInt());
	notipf.setCausedeath(sv.causedeath.toString().trim());
	notipf.setClaimant(sv.clntsel.toString().trim());
	notipf.setClaimat(sv.clamamt.toDouble());
	notipf.setDeathdate(sv.cltdodx.toInt());
	notipf.setDiagnosis(sv.diagcde.toString().trim());
	notipf.setDischargedate(sv.dischargeDate.toInt());
	notipf.setDoctor(sv.zdoctor.toString().trim());
	notipf.setHospitalle(sv.hospitalLevel.toString().trim());
	notipf.setIncidentt(sv.inctype.toString().trim());
	notipf.setIncurdate(sv.incurdt.toInt());
	notipf.setLocation(sv.location.toString().trim());
	notipf.setMedicalpd(sv.zmedprv.toString().trim());
	notipf.setNotificnum(sv.lifcnum.toString().trim());
	notipf.setNotificoy(wsspcomn.company.toString().trim());
	notipf.setNotifidate(sv.notifiDate.toInt());
	//ICIL-1502 modified
	notipf.setNotifinum(wsspcomn.wsaanotificationNum.toString().trim().replace(CLMPREFIX, ""));
//	notipf.setNotifinum(alocnorec.alocNo.toString().trim());
	notipf.setNotifistatus(notifistatus.trim());
	notipf.setRclaimreason(sv.rgpytype.toString().trim());
	notipf.setRelationcnum(sv.cltreln.toString().trim());
	
}
public void insertNohspf3020(){
	//ICIL-1502 modified
	nohspf.setNotihsnum(wsspcomn.wsaanotificationNum.toString().trim().replace(CLMPREFIX, ""));
//	nohspf.setNotihsnum(alocnorec.alocNo.toString().trim());
	nohspf.setNotihycoy(wsspcomn.company.toString());
	datcon1rec.function.set(varcom.tday);
    Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
    wsaaToday.set(datcon1rec.intDate);
	nohspf.setTransdate(wsaaToday.toString().trim());
	nohspf.setEffectdate(wsaaToday.toInt());
	nohspf.setTranscode(wsaaBatckey.batcBatctrcde.getData());
//	descpf=descDAO.getdescData("IT", t1688, "TAPL", wsspcomn.company.toString(), wsspcomn.language.toString());
//	if(descpf==null){
//		nohspf.setTransdesc(descpf.getLongdesc());	
//	}
	addNewNotificationRecordForClose(nohspf);
}

public void addNewNotificationRecordForClose(Nohspf nohspf) {
	int newTransno;
	String maxTransnum = nohspfDAO.getMaxTransnum(nohspf.getNotihsnum());
	if("".equals(maxTransnum) || maxTransnum==null) {
		nohspf.setTransno(String.valueOf(transnoCount));
		nohspfDAO.insertNohspf(nohspf);
	}else {
		newTransno = Integer.valueOf(maxTransnum)+transnoCount;
		nohspf.setTransno(String.valueOf(newTransno));
		nohspfDAO.insertNohspf(nohspf);
	}
}

public void addNewNotificationRecord(Nohspf nohspf) {
	String maxTransnum = nohspfDAO.getMaxTransnum(alocnorec.alocNo.toString().trim());
	if("".equals(maxTransnum) || maxTransnum==null) {
		nohspf.setTransno(String.valueOf(transnoCount));
		nohspfDAO.insertNohspf(nohspf);
	}else {
		transnoCount = transnoCount+Integer.valueOf(maxTransnum);
		nohspf.setTransno(String.valueOf(transnoCount));
		nohspfDAO.insertNohspf(nohspf);
	}
}

protected void updateWssp3010()
{
	wsspcomn.sbmaction.set(scrnparams.action);
	wsaaBatchkey.set(wsspcomn.batchkey);
	wsspcomn.batchkey.set(wsaaBatchkey);
	
	if (isEQ(scrnparams.statuz, "BACH")) {
		return;
	}
	
	/*  Update WSSP Key details*/
		//ILB-459 start
	//if wsaaChdrnum is null or isEmpty, means it's close or modify
}



protected void keeps3070()
{
	chdrlnbIO.setFunction("KEEPS");
	chdrlnbIO.setFormat(chdrlnbrec);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		fatalError600();
	}
	chdrpf = chdrpfDAO.getChdrpf(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
	if(null==chdrpf) {
		fatalError600();
	}
	else {
		chdrpfDAO.setCacheObject(chdrpf);
	}

}


	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
{
	nextProgram4010();
	/*NEXT-PROGRAM
	wsspcomn.programPtr.add(1);
	EXIT*/
}




protected void wayout4800()
{
	/*PROGRAM-EXIT*/
	/* If control is passed to this  part of the 4000 section on the*/
	/*   way out of the program, i.e. after  screen  I/O,  then  the*/
	/*   current stack position action  flag will be  blank.  If the*/
	/*   4000-section  is  being   performed  after  returning  from*/
	/*   processing another program then  the current stack position*/
	/*   action flag will be '*'.*/
	/* If 'KILL' has been requested, (CF11), then move spaces to the*/
	/*   current program entry in the program stack and exit.*/
	if (isEQ(scrnparams.statuz, "KILL")) {
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		return ;
	}
	/* Add 1 to the program pointer and exit.*/
	wsspcomn.programPtr.add(1);
	/*EXIT*/
}

protected void nextProgram4010()
{
	
	gensswrec.company.set(wsspcomn.company);
	gensswrec.progIn.set(wsaaProg);
	gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
	wsspcomn.nextprog.set(wsaaProg);
	
	//*    If returning from a program further down the stack then*/
		/*    first restore the original programs in the program stack.*/
	/*if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}*/
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
						wsaaX.set(wsspcomn.programPtr);
						wsaaY.set(1);
						for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
							saveProgram4100();
						}
					} 
			 

	if (isEQ(sv.fupflg, "?")) {
		checkFollowups1850();
	}
	if (isEQ(sv.fupflg, "X")) {
		gensswrec.function.set("A");
		sv.fupflg.set("?");
		callGenssw4300();
		return ;
	}
	if (isEQ(sv.invresults, "X")) {
		gensswrec.function.set("B");
		sv.invresults.set("?");
		setupWsaaData();
		callGenssw4300();
		return ;
	}
	if (isEQ(sv.invresults, "?")) {
    	checkInvspf();
	}
	if (isEQ(sv.notifnotes, "X")) {
		setupWsaaData();
		gensswrec.function.set("C");
		sv.notifnotes.set("?");
		callGenssw4300();
		return ;
	}
	if (isEQ(sv.notifnotes, "?")) {
		checkClnnpf();
	}
	if (isEQ(sv.notifhistory, "X")) {
		gensswrec.function.set("D");
		sv.notifhistory.set("?");
		wsspcomn.wsaarelationship.set(sv.cltreln);
		wsspcomn.wsaaclaimant.set(sv.clntsel);
		callGenssw4300();
		return ;
	}
	/*  Check if beneficiary selected previously*/
	
	/*   No more selected (or none)*/
	/*      - restore stack form wsaa to wssp*/
	wsaaX.set(wsspcomn.programPtr);
	wsaaY.set(1);
	for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
		restoreProgram4200();
	}
	
	


	/* If we are processing a Whole Plan selection then we must*/
	/*   process all Policies for the selected Coverage/Rider,*/
	/*   Looping back to the 2000-section after having loaded the*/
	/*   screen for the next Policy*/
	/* Else we exit to the next program in the stack.*/
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.nextprog.set(scrnparams.scrname);
	}
	else {
		wayout4800();
	}
	
	
	
	
}
protected void setupWsaaData(){
	wsspcomn.wsaaclaimant.set(sv.clntsel.toString());
	wsspcomn.wsaarelationship.set(sv.cltreln.toString());
}

protected void checkInvspf(){
	invspfList =  invspfDAO.getInvspfList(wsspcomn.company.toString(), notifinum.trim(),"");/* IJTI-1523 */
	if(invspfList!=null && !invspfList.isEmpty()){
		sv.invresults.set("+");
	}
	else{
		sv.invresults.set(SPACES);
	}
}
protected void checkClnnpf(){
	clnnpfList =  clnnpfDAO.getClnnpfList(wsspcomn.company.toString(), notifinum.trim(),"");/* IJTI-1523 */
	if(clnnpfList!=null && !clnnpfList.isEmpty()){
		sv.notifnotes.set("+");
	}
	else{
		sv.notifnotes.set(SPACES);
	}
}

protected void saveProgram4100()
{
	/*SAVE*/
	wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
} 
protected void restoreProgram4100()
{
	/*PARA*/
	wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
	sub1.add(1);
	sub2.add(1);
	/*EXIT*/
}

protected void callGenssw4300()
{
	callSubroutine4310();
}

protected void callSubroutine4310()
{
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)
	&& isNE(gensswrec.statuz, varcom.mrnf)) {
		syserrrec.params.set(gensswrec.gensswRec);
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	/* If an entry on T1675 was not found by genswch redisplay the scre*/
	/* with an error and the options and extras indicator*/
	/* with its initial load value*/
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*     MOVE V045                TO SCRN-ERROR-CODE               */
		scrnparams.errorCode.set(h093);//ILB-459
		wsspcomn.nextprog.set(scrnparams.scrname);
		return ;
	}
	/*    load from gensw to wssp*/
	compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
	wsaaY.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		loadProgram4400();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
}
protected void checkFollowups1850() {
	//ILIFE-6296 by wli31
	boolean recFound = fluppfDAO.checkFluppfRecordByChdrnum(wsspcomn.company.toString(), wsaaChdrnum);
	if (!recFound) {
		//fatalError600();
		sv.fupflg.set(SPACES);
	}  else {
		sv.fupflg.set("+");
	}

}
protected void restoreProgram4200()
{
	/*RESTORE*/
	wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
}

protected void loadProgram4400()
{
	/*RESTORE1*/
	wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT1*/
}
public Chdrpf getChdrpf() {
		return chdrpf;
	}

	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}



	protected void prepareName(String firstName, String lastName) {
		String fullName = "";
		if (isNE(firstName, SPACES)) {
			fullName = getStringUtil().plainName(firstName, lastName, ",");
			 
		} else {
			fullName = lastName;
		}
		
		wsspcomn.longconfname.set(fullName);

	}

	/**
	 * can be overriden to change the its behaviour
	 * @return
	 */
	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}
	
	public Subprogrec getSubprogrec() {
		return subprogrec;
	}
	public void setSubprogrec(Subprogrec subprogrec) {
		this.subprogrec = subprogrec;
	}
	
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 
		 private FixedLengthStringData RRNJ = new FixedLengthStringData(4).init("RRNJ");
		 private FixedLengthStringData RRCQ = new FixedLengthStringData(4).init("RRCQ");
		 private FixedLengthStringData RPGR = new FixedLengthStringData(4).init("RPGR");
		 private FixedLengthStringData p132 = new FixedLengthStringData(4).init("P132");

		
	}

}
