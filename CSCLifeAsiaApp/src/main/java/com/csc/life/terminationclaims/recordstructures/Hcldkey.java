package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:28
 * Description:
 * Copybook name: HCLDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hcldkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hcldFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hcldKey = new FixedLengthStringData(256).isAPartOf(hcldFileKey, 0, REDEFINE);
  	public FixedLengthStringData hcldChdrcoy = new FixedLengthStringData(1).isAPartOf(hcldKey, 0);
  	public FixedLengthStringData hcldChdrnum = new FixedLengthStringData(8).isAPartOf(hcldKey, 1);
  	public FixedLengthStringData hcldLife = new FixedLengthStringData(2).isAPartOf(hcldKey, 9);
  	public FixedLengthStringData hcldCoverage = new FixedLengthStringData(2).isAPartOf(hcldKey, 11);
  	public FixedLengthStringData hcldRider = new FixedLengthStringData(2).isAPartOf(hcldKey, 13);
  	public PackedDecimalData hcldRgpynum = new PackedDecimalData(5, 0).isAPartOf(hcldKey, 15);
  	public FixedLengthStringData hcldHosben = new FixedLengthStringData(5).isAPartOf(hcldKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(233).isAPartOf(hcldKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hcldFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hcldFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}