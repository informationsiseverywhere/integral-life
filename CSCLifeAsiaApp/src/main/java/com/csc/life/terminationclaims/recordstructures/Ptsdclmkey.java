package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:15
 * Description:
 * Copybook name: PTSDCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ptsdclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ptsdclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ptsdclmKey = new FixedLengthStringData(64).isAPartOf(ptsdclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData ptsdclmChdrcoy = new FixedLengthStringData(1).isAPartOf(ptsdclmKey, 0);
  	public FixedLengthStringData ptsdclmChdrnum = new FixedLengthStringData(8).isAPartOf(ptsdclmKey, 1);
  	public PackedDecimalData ptsdclmTranno = new PackedDecimalData(5, 0).isAPartOf(ptsdclmKey, 9);
  	public PackedDecimalData ptsdclmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ptsdclmKey, 12);
  	public FixedLengthStringData ptsdclmLife = new FixedLengthStringData(2).isAPartOf(ptsdclmKey, 15);
  	public FixedLengthStringData ptsdclmCoverage = new FixedLengthStringData(2).isAPartOf(ptsdclmKey, 17);
  	public FixedLengthStringData ptsdclmRider = new FixedLengthStringData(2).isAPartOf(ptsdclmKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(ptsdclmKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ptsdclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ptsdclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}