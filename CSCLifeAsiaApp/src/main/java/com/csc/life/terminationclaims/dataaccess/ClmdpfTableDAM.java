package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ClmdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:27
 * Class transformed from CLMDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ClmdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 249;
	public FixedLengthStringData clmdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData clmdpfRecord = clmdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(clmdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(clmdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(clmdrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(clmdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(clmdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(clmdrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(clmdrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(clmdrec);
	public FixedLengthStringData recode = DD.recode.copy().isAPartOf(clmdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(clmdrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(clmdrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(clmdrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(clmdrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(clmdrec);
	public FixedLengthStringData clamstat = DD.clamstat.copy().isAPartOf(clmdrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(clmdrec);
	public PackedDecimalData occdate = DD.occdate.copy().isAPartOf(clmdrec);
	public FixedLengthStringData reinFlag = DD.riflag.copy().isAPartOf(clmdrec);
	public FixedLengthStringData covRiskStat = DD.crrstat.copy().isAPartOf(clmdrec);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(clmdrec);
	public PackedDecimalData crInstamt = DD.crinst.copy().isAPartOf(clmdrec);
	public PackedDecimalData clamamt = DD.clamamt.copy().isAPartOf(clmdrec);
	public FixedLengthStringData indexationInd = DD.indxin.copy().isAPartOf(clmdrec);
	public FixedLengthStringData bonusInd = DD.bnusin.copy().isAPartOf(clmdrec);
	public FixedLengthStringData sivarin = DD.sivarin.copy().isAPartOf(clmdrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(clmdrec);
	public FixedLengthStringData chgflag = DD.chgflag.copy().isAPartOf(clmdrec);
	public FixedLengthStringData clamtyp = DD.clamtyp.copy().isAPartOf(clmdrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(clmdrec);
	public FixedLengthStringData crterm = DD.crterm.copy().isAPartOf(clmdrec);
	public FixedLengthStringData clamdata = DD.clamdata.copy().isAPartOf(clmdrec);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(clmdrec);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(clmdrec);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(clmdrec);
	public PackedDecimalData estMatValue = DD.emv.copy().isAPartOf(clmdrec);
	public PackedDecimalData actvalue = DD.actvalue.copy().isAPartOf(clmdrec);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(clmdrec);
	public FixedLengthStringData virtualFund = DD.virtfnd.copy().isAPartOf(clmdrec);
	public FixedLengthStringData annypind = DD.annypind.copy().isAPartOf(clmdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(clmdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(clmdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(clmdrec);
	public FixedLengthStringData claimno = DD.claimnumber.copy().isAPartOf(clmdrec);
	public FixedLengthStringData claimnotifino = DD.accno.copy().isAPartOf(clmdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ClmdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ClmdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ClmdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ClmdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ClmdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ClmdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ClmdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CLMDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"VALIDFLAG, " +
							"CURRFROM, " +
							"RECODE, " +
							"TRANNO, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"CLAMSTAT, " +
							"CRTABLE, " +
							"OCCDATE, " +
							"RIFLAG, " +
							"CRRSTAT, " +
							"REASONCD, " +
							"CRINST, " +
							"CLAMAMT, " +
							"INDXIN, " +
							"BNUSIN, " +
							"SIVARIN, " +
							"CURRTO, " +
							"CHGFLAG, " +
							"CLAMTYP, " +
							"EFFDATE, " +
							"CRTERM, " +
							"CLAMDATA, " +
							"SHORTDS, " +
							"LIENCD, " +
							"CNSTCUR, " +
							"EMV, " +
							"ACTVALUE, " +
							"TYPE_T, " +
							"VIRTFND, " +
							"ANNYPIND, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"CLAIMNO, " +
							"CLAIMNOTIFINO, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     validflag,
                                     currfrom,
                                     recode,
                                     tranno,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     clamstat,
                                     crtable,
                                     occdate,
                                     reinFlag,
                                     covRiskStat,
                                     reasoncd,
                                     crInstamt,
                                     clamamt,
                                     indexationInd,
                                     bonusInd,
                                     sivarin,
                                     currto,
                                     chgflag,
                                     clamtyp,
                                     effdate,
                                     crterm,
                                     clamdata,
                                     shortds,
                                     liencd,
                                     cnstcur,
                                     estMatValue,
                                     actvalue,
                                     fieldType,
                                     virtualFund,
                                     annypind,
                                     userProfile,
                                     jobName,
                                     datime,
                                     claimno,
                                     claimnotifino,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		validflag.clear();
  		currfrom.clear();
  		recode.clear();
  		tranno.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		clamstat.clear();
  		crtable.clear();
  		occdate.clear();
  		reinFlag.clear();
  		covRiskStat.clear();
  		reasoncd.clear();
  		crInstamt.clear();
  		clamamt.clear();
  		indexationInd.clear();
  		bonusInd.clear();
  		sivarin.clear();
  		currto.clear();
  		chgflag.clear();
  		clamtyp.clear();
  		effdate.clear();
  		crterm.clear();
  		clamdata.clear();
  		shortds.clear();
  		liencd.clear();
  		cnstcur.clear();
  		estMatValue.clear();
  		actvalue.clear();
  		fieldType.clear();
  		virtualFund.clear();
  		annypind.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		claimno.clear();
  		claimnotifino.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getClmdrec() {
  		return clmdrec;
	}

	public FixedLengthStringData getClmdpfRecord() {
  		return clmdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setClmdrec(what);
	}

	public void setClmdrec(Object what) {
  		this.clmdrec.set(what);
	}

	public void setClmdpfRecord(Object what) {
  		this.clmdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(clmdrec.getLength());
		result.set(clmdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}