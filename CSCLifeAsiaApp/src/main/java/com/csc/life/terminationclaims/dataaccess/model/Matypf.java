package com.csc.life.terminationclaims.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Matypf implements Serializable {

	private static final long serialVersionUID = -6730620534295170458L;

	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
	private String cownum;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private String crtable;
	private String validflag;
	private int tranno;
	private BigDecimal actvalue;
	private String maturitycalcmeth;
	private String singlepremind;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getCownum() {
		return cownum;
	}
	public void setCownum(String cownum) {
		this.cownum = cownum;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public BigDecimal getActvalue() {
		return actvalue;
	}
	public void setActvalue(BigDecimal actvalue) {
		this.actvalue = actvalue;
	}
	public String getMaturitycalcmeth() {
		return maturitycalcmeth;
	}
	public void setMaturitycalcmeth(String maturitycalcmeth) {
		this.maturitycalcmeth = maturitycalcmeth;
	}
	public String getSinglepremind() {
		return singlepremind;
	}
	public void setSinglepremind(String singlepremind) {
		this.singlepremind = singlepremind;
	}
	
}