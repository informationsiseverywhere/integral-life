package com.csc.life.terminationclaims.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.ZhlcpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Zhlcpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;

import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public  class ZhlcpfDAOImpl extends  BaseDAOImpl<Zhlcpf> implements ZhlcpfDAO
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ZhlcpfDAOImpl.class);
	 PreparedStatement stmt ;  
	
	 
	 public boolean insertIntoZhlcpf(Zhlcpf zhlcpf){
	           
		stmt = null;
		java.util.Date date = new java.util.Date();
		Timestamp currentTimestamp = new Timestamp(date.getTime());
		
		try{
	                String queryString = "INSERT INTO ZHLCPF " + 
	                		"  (CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,VALIDFLAG,TRANNO,TRDT,TRTM,USR,ACTVALUE,ZCLMADJST,ZHLDCLMV,ZHLDCLMA,USRPRF,JOBNM,DATIME) "+
	                		" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	         
	          
	          stmt = getConnection().prepareStatement(queryString); // IJTI-714
	//stmt.executeUpdate(queryString);
	          
	          String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTimestamp);
				Timestamp dattime = Timestamp.valueOf(timeStamp);

	          stmt.setString(1, zhlcpf.getChdrcoy());              
              stmt.setString(2, zhlcpf.getChdrnum());
              stmt.setString(3, zhlcpf.getLife());
              stmt.setString(4, zhlcpf.getJlife());
              stmt.setString(5, zhlcpf.getCoverage());
              stmt.setString(6, zhlcpf.getRider());
              stmt.setString(7, zhlcpf.getCrtable());
              stmt.setString(8, zhlcpf.getValidflag());
              stmt.setInt(9, zhlcpf.getTranno());
              stmt.setInt(10, zhlcpf.getTrdt());
              stmt.setInt(11, zhlcpf.getTrtm());
              stmt.setInt(12, zhlcpf.getUsr());
              stmt.setBigDecimal(13, zhlcpf.getActvalue());
              stmt.setBigDecimal(14, zhlcpf.getZclmadjst());
              stmt.setBigDecimal(15, zhlcpf.getZhldclmv());
              stmt.setBigDecimal(16, zhlcpf.getZhldclma());
              stmt.setString(17, getUsrprf());
              stmt.setString(18, getJobnm());
            // stmt.setTimestamp(19,   new Timestamp(System.currentTimeMillis()));
              stmt.setTimestamp(19,dattime);
              
              

	                
              int recordsRows= stmt.executeUpdate(); 
              stmt.close();
              if(recordsRows != 0)
              	{
             	 	return true;
              	}
              	else
              	{
              		return false;
              	}
             
		}
		
		catch (SQLException e) {
	           LOGGER.error("insertIntoZhlcpf()", e);//IJTI-1561
	           throw new SQLRuntimeException(e);
	    } finally {
	    	close(stmt, null);
	    }
	                
	}

public List<Zhlcpf> readZhlcpf(Zhlcpf zhlcpf){
	
	
    List<Zhlcpf> zhlcpfList = null;
    String query = "select life,jlife,coverage,rider,crtable,tranno,trdt,trtm,usr,actvalue,zclmadjst,zhldclmv,zhldclma,usrprf,jobnm,datime"
    		+ " from zhlcpf where (chdrcoy = ? and chdrnum = ? and validflag = ? )";
    
    ResultSet rs = null;
    Zhlcpf zhlcpfData = null;
    stmt = null;
    try {

    	zhlcpfList = new ArrayList<Zhlcpf>();
           stmt = getConnection().prepareStatement(query); // IJTI-714
           stmt.setString(1, zhlcpf.getChdrcoy());
           stmt.setString(2, zhlcpf.getChdrnum());
           stmt.setString(3, zhlcpf.getValidflag());
           
           

           rs = stmt.executeQuery();
           while (rs.next()) {
        	   	zhlcpfData= new Zhlcpf();
                 if (rs.getString(1) != null) {
                	 zhlcpfData.setChdrcoy(zhlcpf.getChdrcoy());
                	 zhlcpfData.setChdrnum(zhlcpf.getChdrnum());
                	 zhlcpfData.setValidflag(zhlcpf.getValidflag());
                	 zhlcpfData.setLife(rs.getString(1).trim());
                	 zhlcpfData.setJlife(rs.getString(2));
                	 zhlcpfData.setCoverage(rs.getString(3));
                	 zhlcpfData.setRider(rs.getString(4));
                	 zhlcpfData.setCrtable(rs.getString(5).trim());
                
                	 zhlcpfData.setTranno(rs.getInt(6));
                	 zhlcpfData.setTrdt(rs.getInt(7));
                	 zhlcpfData.setTrtm(rs.getInt(8));
                	 zhlcpfData.setUsr(rs.getInt(9));
                	 zhlcpfData.setActvalue(rs.getBigDecimal(10));
                	 zhlcpfData.setZclmadjst(rs.getBigDecimal(11));
                	 zhlcpfData.setZhldclmv(rs.getBigDecimal(12));
                	 zhlcpfData.setZhldclma(rs.getBigDecimal(13));
                	 zhlcpfData.setUsrprf(rs.getString(14));
                	 zhlcpfData.setJobnm(rs.getString(15));
                	zhlcpfData.setDatime(rs.getTimestamp(16));
                	 zhlcpfList.add(zhlcpfData);
                 }
           }
    } catch (SQLException e) {
           LOGGER.error("readZhlcpf()", e);//IJTI-1561
           throw new SQLRuntimeException(e);
    } finally {
           close(stmt, rs);
    }

    return zhlcpfList;
	
	
}

public List<Zhlcpf> readZhlcpfData(Zhlcpf zhlcpf){
	
	
	List<Zhlcpf> zhlcpfList = null;
	String query = "select life,jlife,coverage,rider,crtable,tranno,trdt,trtm,usr,actvalue,zclmadjst,zhldclmv,zhldclma,usrprf,jobnm ,datime"
					+ " from zhlcpf where (chdrcoy = ? and chdrnum = ? and validflag = ? )";

	ResultSet rs = null;
	Zhlcpf zhlcpfData = null;
	stmt = null;
	try {

		zhlcpfList = new ArrayList<Zhlcpf>();
		stmt = getConnection().prepareStatement(query); // IJTI-714
		stmt.setString(1, zhlcpf.getChdrcoy());
		stmt.setString(2, zhlcpf.getChdrnum());
		stmt.setString(3, zhlcpf.getValidflag());
  

		rs = stmt.executeQuery();
		while (rs.next()) {
			zhlcpfData = new Zhlcpf();
			if (rs.getString(1) != null) {
				zhlcpfData.setChdrcoy(zhlcpf.getChdrcoy());
				zhlcpfData.setChdrnum(zhlcpf.getChdrnum());
			
				zhlcpfData.setValidflag(zhlcpf.getValidflag());
				zhlcpfData.setLife(rs.getString(1).trim());
           	 zhlcpfData.setJlife(rs.getString(2));
           	 zhlcpfData.setCoverage(rs.getString(3));
           	 zhlcpfData.setRider(rs.getString(4));
           	 zhlcpfData.setCrtable(rs.getString(5).trim());
           
           	 zhlcpfData.setTranno(rs.getInt(6));
           	zhlcpfData.setTrdt(rs.getInt(7));
           	zhlcpfData.setTrtm(rs.getInt(8));
           	zhlcpfData.setUsr(rs.getInt(9));
           	zhlcpfData.setActvalue(rs.getBigDecimal(10));
           	zhlcpfData.setZclmadjst(rs.getBigDecimal(11));
           	zhlcpfData.setZhldclmv(rs.getBigDecimal(12));
           	zhlcpfData.setZhldclma(rs.getBigDecimal(13));
           	zhlcpfData.setUsrprf(rs.getString(14));
           	zhlcpfData.setJobnm(rs.getString(15));
           	zhlcpfData.setDatime(rs.getTimestamp(16));
       	 		zhlcpfList.add(zhlcpfData);
			}
		}
		} catch (SQLException e) {
				LOGGER.error("readZhlcpfData()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
		} finally {
				close(stmt, rs);
				}

		return zhlcpfList;


	}

public boolean updateIntoZhlcpf(Zhlcpf zhlcpf){
    
	stmt = null;
	
	try{
               StringBuilder queryString = new StringBuilder();

/*String dbType = AppVars.getInstance().getAppConfig().getDatabaseType();*/
/*  if (QPBaseDataSource.DATABASE_SQLSERVER.equals(dbType)) {
queryString.append("SET IDENTITY_INSERT CLNTQY ON   ");
}
*/

queryString.append("UPDATE  ZHLCPF  SET ZHLDCLMV =? ,ZHLDCLMA =? , VALIDFLAG = ?");

queryString.append(" where CHDRCOY = ? AND CHDRNUM =? AND LIFE = ? AND COVERAGE =? AND RIDER = ? AND VALIDFLAG = 1");

        
         
         stmt = getConnection().prepareStatement(queryString.toString()); 
//stmt.executeUpdate(queryString);

         stmt.setBigDecimal(1, zhlcpf.getZhldclmv());              
         stmt.setBigDecimal(2, zhlcpf.getZhldclma());
         stmt.setString(3, zhlcpf.getValidflag());
         stmt.setString(4, zhlcpf.getChdrcoy());
         stmt.setString(5, zhlcpf.getChdrnum());
         stmt.setString(6, zhlcpf.getLife());
         stmt.setString(7, zhlcpf.getCoverage());
         stmt.setString(8, zhlcpf.getRider());
         
      
         int recordsRows= stmt.executeUpdate(); 
         stmt.close();
         if(recordsRows != 0)
         	{
        	 	return true;
         	}
         	else
         	{
         		return false;
         	}
      
	}
	
	catch (SQLException e) {
          LOGGER.error("updateIntoZhlcpf()", e);//IJTI-1561
          throw new SQLRuntimeException(e);
   } finally {
	  close(stmt, null);
   }
               
}



}