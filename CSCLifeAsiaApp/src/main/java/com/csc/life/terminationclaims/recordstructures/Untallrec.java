package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Untallrec extends ExternalData {
	
	public FixedLengthStringData untallrec = new FixedLengthStringData(221);
	public FixedLengthStringData untaStatuz = new FixedLengthStringData(4).isAPartOf(untallrec, 0);
	public FixedLengthStringData untaFunction = new FixedLengthStringData(5).isAPartOf(untallrec, 4);
	public FixedLengthStringData untaCompany = new FixedLengthStringData(1).isAPartOf(untallrec, 9);
	public PackedDecimalData untaEffdate = new PackedDecimalData(8).isAPartOf(untallrec, 10);

	public FixedLengthStringData untaAalbas = new FixedLengthStringData(4).isAPartOf(untallrec, 18);
	public FixedLengthStringData untaBillfreq = new FixedLengthStringData(2).isAPartOf(untallrec, 22);
	public FixedLengthStringData untaSinglePremInd = new FixedLengthStringData(1).isAPartOf(untallrec, 24);

	public PackedDecimalData untaMaxPeriod01 = new PackedDecimalData(4,0).isAPartOf(untallrec, 25);
	public PackedDecimalData untaMaxPeriod02 = new PackedDecimalData(4,0).isAPartOf(untallrec, 29);
	public PackedDecimalData untaMaxPeriod03 = new PackedDecimalData(4,0).isAPartOf(untallrec, 33);
	public PackedDecimalData untaMaxPeriod04 = new PackedDecimalData(4,0).isAPartOf(untallrec, 37);
	public PackedDecimalData untaMaxPeriod05 = new PackedDecimalData(4,0).isAPartOf(untallrec, 41);
	public PackedDecimalData untaMaxPeriod06 = new PackedDecimalData(4,0).isAPartOf(untallrec, 45);
	public PackedDecimalData untaMaxPeriod07 = new PackedDecimalData(4,0).isAPartOf(untallrec, 49);

	public ZonedDecimalData untaPcUnit01 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 53);
	public ZonedDecimalData untaPcUnit02 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 58);
	public ZonedDecimalData untaPcUnit03 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 63);
	public ZonedDecimalData untaPcUnit04 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 68);
	public ZonedDecimalData untaPcUnit05= new ZonedDecimalData(5, 2).isAPartOf(untallrec, 73);
	public ZonedDecimalData untaPcUnit06= new ZonedDecimalData(5, 2).isAPartOf(untallrec, 78);
	public ZonedDecimalData untaPcUnit07= new ZonedDecimalData(5, 2).isAPartOf(untallrec, 83);

	public ZonedDecimalData untaPcInitUnit01 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 88);
	public ZonedDecimalData untaPcInitUnit02 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 93);
	public ZonedDecimalData untaPcInitUnit03 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 98);
	public ZonedDecimalData untaPcInitUnit04 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 103);
	public ZonedDecimalData untaPcInitUnit05 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 108);
	public ZonedDecimalData untaPcInitUnit06 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 113);
	public ZonedDecimalData untaPcInitUnit07 = new ZonedDecimalData(5, 2).isAPartOf(untallrec, 118);


	public FixedLengthStringData untaBatctrcde = new FixedLengthStringData(4).isAPartOf(untallrec, 123);
	public FixedLengthStringData untaEnhall = new FixedLengthStringData(4).isAPartOf(untallrec, 127);
	public FixedLengthStringData untaCntcurr = new FixedLengthStringData(3).isAPartOf(untallrec, 131);
	public FixedLengthStringData untaCalcType = new FixedLengthStringData(1).isAPartOf(untallrec, 134);
	public PackedDecimalData untaInstprem = new PackedDecimalData(17, 2).isAPartOf(untallrec, 135);
	public PackedDecimalData untaAllocInit = new PackedDecimalData(17, 2).isAPartOf(untallrec, 152);
	public PackedDecimalData untaAllocAccum = new PackedDecimalData(17, 2).isAPartOf(untallrec, 169);

	public PackedDecimalData untaEnhancePerc = new PackedDecimalData(17, 2).isAPartOf(untallrec, 186);
	public PackedDecimalData untaEnhanceAlloc = new PackedDecimalData(17, 2).isAPartOf(untallrec, 203);
	public FixedLengthStringData fundPool  = new FixedLengthStringData(1).isAPartOf(untallrec, 220);


public void initialize() {
	COBOLFunctions.initialize(untallrec);
}	


public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			untallrec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
		
}
}

