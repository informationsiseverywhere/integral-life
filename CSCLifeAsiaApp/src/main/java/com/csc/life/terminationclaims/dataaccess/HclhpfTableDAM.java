package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HclhpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:26
 * Class transformed from HCLHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HclhpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 149;
	public FixedLengthStringData hclhrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hclhpfRecord = hclhrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hclhrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hclhrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hclhrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hclhrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hclhrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hclhrec);
	public PackedDecimalData rgpynum = DD.rgpynum.copy().isAPartOf(hclhrec);
	public FixedLengthStringData benpln = DD.benpln.copy().isAPartOf(hclhrec);
	public PackedDecimalData recvdDate = DD.zclmrecd.copy().isAPartOf(hclhrec);
	public PackedDecimalData incurdt = DD.incurdt.copy().isAPartOf(hclhrec);
	public PackedDecimalData gcadmdt = DD.gcadmdt.copy().isAPartOf(hclhrec);
	public PackedDecimalData dischdt = DD.dischdt.copy().isAPartOf(hclhrec);
	public FixedLengthStringData diagcde = DD.diagcde.copy().isAPartOf(hclhrec);
	public FixedLengthStringData zdoctor = DD.zdoctor.copy().isAPartOf(hclhrec);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(hclhrec);
	public PackedDecimalData tactexp = DD.tactexp.copy().isAPartOf(hclhrec);
	public PackedDecimalData tdeduct = DD.tdeduct.copy().isAPartOf(hclhrec);
	public PackedDecimalData tcoiamt = DD.tcoiamt.copy().isAPartOf(hclhrec);
	public PackedDecimalData aad = DD.aad.copy().isAPartOf(hclhrec);
	public PackedDecimalData tclmamt = DD.tclmamt.copy().isAPartOf(hclhrec);
	public FixedLengthStringData lmtyear = DD.lmtyear.copy().isAPartOf(hclhrec);
	public FixedLengthStringData lmtlife = DD.lmtlife.copy().isAPartOf(hclhrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hclhrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hclhrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hclhrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HclhpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HclhpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HclhpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HclhpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HclhpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HclhpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HclhpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HCLHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"RGPYNUM, " +
							"BENPLN, " +
							"ZCLMRECD, " +
							"INCURDT, " +
							"GCADMDT, " +
							"DISCHDT, " +
							"DIAGCDE, " +
							"ZDOCTOR, " +
							"ZMEDPRV, " +
							"TACTEXP, " +
							"TDEDUCT, " +
							"TCOIAMT, " +
							"AAD, " +
							"TCLMAMT, " +
							"LMTYEAR, " +
							"LMTLIFE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     crtable,
                                     rgpynum,
                                     benpln,
                                     recvdDate,
                                     incurdt,
                                     gcadmdt,
                                     dischdt,
                                     diagcde,
                                     zdoctor,
                                     zmedprv,
                                     tactexp,
                                     tdeduct,
                                     tcoiamt,
                                     aad,
                                     tclmamt,
                                     lmtyear,
                                     lmtlife,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		rgpynum.clear();
  		benpln.clear();
  		recvdDate.clear();
  		incurdt.clear();
  		gcadmdt.clear();
  		dischdt.clear();
  		diagcde.clear();
  		zdoctor.clear();
  		zmedprv.clear();
  		tactexp.clear();
  		tdeduct.clear();
  		tcoiamt.clear();
  		aad.clear();
  		tclmamt.clear();
  		lmtyear.clear();
  		lmtlife.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHclhrec() {
  		return hclhrec;
	}

	public FixedLengthStringData getHclhpfRecord() {
  		return hclhpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHclhrec(what);
	}

	public void setHclhrec(Object what) {
  		this.hclhrec.set(what);
	}

	public void setHclhpfRecord(Object what) {
  		this.hclhpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hclhrec.getLength());
		result.set(hclhrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}