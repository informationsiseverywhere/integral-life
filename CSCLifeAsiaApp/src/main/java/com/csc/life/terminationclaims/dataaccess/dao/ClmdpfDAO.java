package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Clmdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ClmdpfDAO extends BaseDAO<Clmdpf> {
    public List<Clmdpf> selectClmdpfRecord(Clmdpf clmdpf);
	//ILIFE-6811
	public void insertClmdRecord(List<Clmdpf> clmdpfList);
	public void updateCnstCurActValue(Clmdpf clmdpf);
	public void deleteRcdByChdrnum(String chdrcoy,String chdrnum);
}