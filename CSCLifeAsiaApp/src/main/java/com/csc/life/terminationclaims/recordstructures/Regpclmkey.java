package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:43
 * Description:
 * Copybook name: REGPCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regpclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regpclmFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData regpclmKey = new FixedLengthStringData(256).isAPartOf(regpclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData regpclmChdrcoy = new FixedLengthStringData(1).isAPartOf(regpclmKey, 0);
  	public FixedLengthStringData regpclmChdrnum = new FixedLengthStringData(8).isAPartOf(regpclmKey, 1);
  	public FixedLengthStringData regpclmLife = new FixedLengthStringData(2).isAPartOf(regpclmKey, 9);
  	public FixedLengthStringData regpclmCoverage = new FixedLengthStringData(2).isAPartOf(regpclmKey, 11);
  	public FixedLengthStringData regpclmRider = new FixedLengthStringData(2).isAPartOf(regpclmKey, 13);
  	public PackedDecimalData regpclmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(regpclmKey, 15);
  	public PackedDecimalData regpclmRgpynum = new PackedDecimalData(5, 0).isAPartOf(regpclmKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(235).isAPartOf(regpclmKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regpclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regpclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}