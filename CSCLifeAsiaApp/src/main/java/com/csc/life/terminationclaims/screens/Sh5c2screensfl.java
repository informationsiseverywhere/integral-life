package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.TableModel.Subfile.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sh5c2screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 27;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2, 80, 2, 3, 4}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 21, 2, 79}); 
	}
	
	/**
	 * Writes a record to the screen.
	 * @param errorInd - will be set on if an error occurs
	 * @param noRecordFoundInd - will be set on if no changed record is found
	 */
		public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
			Sh5c2ScreenVars sv = (Sh5c2ScreenVars) pv;
			if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sh5c2screensfl.getRowCount())) {
				ind3.setOn();
				return;
			}
			TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sh5c2screensfl, 
				sv.Sh5c2screensflWritten , ind2, ind3, maxRecords);
			if (ind2.isOn() || ind3.isOn()) {
				return;
			}
			setSubfileData(tm.bufferedRow, av, pv);
			if (av.getInd(nextChangeIndicator)) {
				tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
			} else {
				tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
			}
			clearInds(av, pfInds);
			tm.write();
		}
		
		public static void update(COBOLAppVars av, VarModel pv,
				Indicator ind2) {
				Sh5c2ScreenVars sv = (Sh5c2ScreenVars) pv;
				TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sh5c2screensfl, ind2);
				setSubfileData(tm.bufferedRow, av, pv);
				if (av.getInd(nextChangeIndicator)) {
					tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
				} else {
					tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
				}
				tm.update();
			}
		public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
				Indicator ind2, Indicator ind3, DecimalData sflIndex) {
				Sh5c2ScreenVars sv = (Sh5c2ScreenVars) pv;
				DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sh5c2screensfl, ind2, ind3, sflIndex);
				getSubfileData(dm, av, pv);
				// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
				// we return to the start of the subfile for subsequent calls
				if (ind3.isOn() && sv.Sh5c2screensflWritten.gt(0))
				{
					sv.sh5c2screensfl.setCurrentIndex(0);
					sv.Sh5c2screensflWritten.set(0);
				}
				restoreInds(dm, av, affectedInds);
			}

			public static void chain(COBOLAppVars av, VarModel pv,
				int record, Indicator ind2, Indicator ind3) {
				Sh5c2ScreenVars sv = (Sh5c2ScreenVars) pv;
				DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sh5c2screensfl, record, ind2, ind3);
				getSubfileData(dm, av, pv);
				restoreInds(dm, av, affectedInds);
			}

			public static void chain(COBOLAppVars av, VarModel pv,
				BaseData record, Indicator ind2, Indicator ind3) {
				chain(av, pv, record.toInt(), ind2, ind3);
			}
			public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
				int record, Indicator ind2, Indicator ind3) {
				av.COBOLFileError = false;
				chain(av, pv, record, ind2, ind3);
				if (ind3.isOn()) av.COBOLFileError = true;
			}

			public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
				BaseData record, Indicator ind2, Indicator ind3) {
				chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
			}
			public static void getSubfileData(DataModel dm, COBOLAppVars av,
					 VarModel pv) {
					if (dm != null) {
						Sh5c2ScreenVars screenVars = (Sh5c2ScreenVars) pv;
						if (screenVars.screenIndicArea.getFieldName() == null) {
							screenVars.screenIndicArea.setFieldName("screenIndicArea");
							screenVars.coverage.setFieldName("coverage");
							screenVars.rider.setFieldName("rider");
							screenVars.component.setFieldName("component");
							screenVars.compdesc.setFieldName("compdesc");
							screenVars.instPrem.setFieldName("instPrem");
							screenVars.cashvalare.setFieldName("cashvalare");
							screenVars.fundVal.setFieldName("fundVal");
							screenVars.polReserve.setFieldName("polReserve");
						}
						screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
						screenVars.coverage.set(dm.getField("coverage"));
						screenVars.rider.set(dm.getField("rider"));
						screenVars.component.set(dm.getField("component"));
						screenVars.compdesc.set(dm.getField("compdesc"));
						screenVars.instPrem.set(dm.getField("instPrem"));
						screenVars.cashvalare.set(dm.getField("cashvalare"));
						screenVars.fundVal.set(dm.getField("fundVal"));
						screenVars.polReserve.set(dm.getField("polReserve"));
						
					}
				}
			
			public static void setSubfileData(DataModel dm, COBOLAppVars av,
					 VarModel pv) {
					if (dm != null) {
						Sh5c2ScreenVars screenVars = (Sh5c2ScreenVars) pv;
						if (screenVars.screenIndicArea.getFieldName() == null) {
							screenVars.screenIndicArea.setFieldName("screenIndicArea");
							screenVars.screenIndicArea.setFieldName("screenIndicArea");
							screenVars.coverage.setFieldName("coverage");
							screenVars.rider.setFieldName("rider");
							screenVars.component.setFieldName("component");
							screenVars.compdesc.setFieldName("compdesc");
							screenVars.instPrem.setFieldName("instPrem");
							screenVars.cashvalare.setFieldName("cashvalare");
							screenVars.fundVal.setFieldName("fundVal");
							screenVars.polReserve.setFieldName("polReserve");
						}
						dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
						dm.getField("coverage").set(screenVars.coverage);
						dm.getField("rider").set(screenVars.rider);
						dm.getField("component").set(screenVars.component);
						dm.getField("compdesc").set(screenVars.compdesc);
						dm.getField("instPrem").set(screenVars.instPrem);
						dm.getField("cashvalare").set(screenVars.cashvalare);
						dm.getField("fundVal").set(screenVars.fundVal);
						dm.getField("polReserve").set(screenVars.polReserve);
						
					}
				}

				public static String getRecName() {
					return ROUTINE;
				}

				public static int getMaxRecords() {
					return maxRecords;
				}

				public static void getMaxRecords(int maxRecords) {
					Sh5c2screensfl.maxRecords = maxRecords;
				}

				public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
					gt.set1stScreenRow();
					getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
					restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
					clearFormatting(pv);
				}

				public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
					gt.setNextScreenRow();
					getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
					restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
					clearFormatting(pv);
				}

				public static void clearFormatting(VarModel pv) {
					Sh5c2ScreenVars screenVars = (Sh5c2ScreenVars)pv;
					screenVars.screenIndicArea.clearFormatting();
					screenVars.coverage.clearFormatting();
					screenVars.rider.clearFormatting();
					screenVars.component.clearFormatting();
					screenVars.compdesc.clearFormatting();
					screenVars.instPrem.clearFormatting();
					screenVars.cashvalare.clearFormatting();
					screenVars.fundVal.clearFormatting();
					screenVars.polReserve.clearFormatting();
					clearClassString(pv);
				}

				public static void clearClassString(VarModel pv) {
					Sh5c2ScreenVars screenVars = (Sh5c2ScreenVars)pv;
					screenVars.coverage.setClassString("");
					screenVars.rider.setClassString("");
					screenVars.component.setClassString("");
					screenVars.compdesc.setClassString("");
					screenVars.instPrem.setClassString("");
					screenVars.cashvalare.setClassString("");
					screenVars.fundVal.setClassString("");
					screenVars.polReserve.setClassString("");
				}

			/**
			 * Clear all the variables in Sh5c2screensfl
			 */
				public static void clear(VarModel pv) {
					Sh5c2ScreenVars screenVars = (Sh5c2ScreenVars) pv;
					screenVars.screenIndicArea.clear();
					screenVars.coverage.clear();
					screenVars.rider.clear();
					screenVars.component.clear();
					screenVars.compdesc.clear();
					screenVars.instPrem.clear();
					screenVars.cashvalare.clear();
					screenVars.fundVal.clear();
					screenVars.polReserve.clear();
					
				}

}
