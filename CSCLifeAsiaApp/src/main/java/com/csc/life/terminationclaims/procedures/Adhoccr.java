package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.dao.CovrpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.recordstructures.AdhoccrRec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.dataaccess.model.Descpf;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Adhoccr extends COBOLConvCodeModel {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "ADHOCCR";
	
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private AdhoccrRec adhoccrRec = new AdhoccrRec();
	private Map<String,List<String>> inPutParam = new HashMap<>();
	private Map<String,List<List<String>>> inPutParam2 = new HashMap<>();
	private List<Covrpf> covrList = new ArrayList<Covrpf>();
	private List<Covrpf> covrList1;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAOP6351",CovrpfDAO.class);
	private ItempfDAO itemDAO = getApplicationContext().getBean("itempfDAO",ItempfDAO.class);
	private T5687rec t5687rec = new T5687rec();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private FixedLengthStringData wsaabasecovrsvmethod = new FixedLengthStringData(16);
	
	private Map<String, BigDecimal> outputParam = new HashMap<String, BigDecimal>();
	
	private List<String> coverageList = new ArrayList<String>();
	private List<String> amountList = new ArrayList<String>();
	
	private DescpfDAO descpfDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class); 
	private Map<String, Descpf> descpfMap = new HashMap<String, Descpf>();
	
	private int countCoverage = 0;
	private PackedDecimalData policyFeeAmount = new PackedDecimalData(17, 2).init(0);
	private Datcon2rec datcon2rec = new Datcon2rec();
	
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	
	private FixedLengthStringData wsaaRunIndicator = new FixedLengthStringData(1);
	private Validator LastTimeThrough = new Validator(wsaaRunIndicator, "E");
	
	private BigDecimal wsaaInstprem = new BigDecimal(0);
	private FixedLengthStringData oldFreq = new FixedLengthStringData(2);
	private BigDecimal oldPolicyFee = new BigDecimal(0);
	private Datcon3rec datcon3rec = new Datcon3rec();
	
	public Adhoccr() {
		super();
	}
	
	public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}
	
	protected void mainline1000()
	{
		if(LastTimeThrough.isTrue() || isEQ(srcalcpy.effdate, srcalcpy.ptdate)) { //PINNACLE-3012
			srcalcpy.status.set(varcom.endp);
			return;
		}
		if (isNE(wsaaLife, srcalcpy.lifeLife)) {
			wsaaLife.set(srcalcpy.lifeLife);
			wsaabasecovrsvmethod.set(srcalcpy.surrCalcMeth);
			initMap();
			setMap1();
			setMap2();
			initialise2000();
			loadDescriptions();
		}
		else {
			if(isEQ(srcalcpy.status, varcom.oK)) {
				wsaaRunIndicator.set("E");
				srcalcpy.status.set(varcom.endp);
				srcalcpy.description.set("Policy Fee");
				srcalcpy.type.set("P");
				srcalcpy.actualVal.set(policyFeeAmount);
				countCoverage = 0;
				return;
			}
			else {
				srcalcpy.status.set(varcom.endp);
			}
		}
		setOutgoingvariables();
		if(covrList.size() == countCoverage) {
			srcalcpy.status.set(varcom.oK);
		}
		if(countCoverage == coverageList.size()) {
			coverageList.clear();
			outputParam.clear();
			amountList.clear();
			covrList.clear();
		}
	}
	
	protected void initialise2000() {
		
		srcalcpy.status.set(Varcom.endp);
		
		adhoccrRec.setChdrnum(srcalcpy.chdrChdrnum.toString());
		
		adhoccrRec.setCnttype(inPutParam.get("cnttype"));
		adhoccrRec.setSvMethod(inPutParam.get("svMethod"));
		adhoccrRec.setRiskComDate(inPutParam.get("riskComDate"));
		adhoccrRec.setPtdate(inPutParam.get("ptdate"));
		adhoccrRec.setBillfreq(inPutParam.get("billfreq"));
		adhoccrRec.setPaidPrem(inPutParam.get("paidPrem"));
		adhoccrRec.setRiskCesDate(inPutParam.get("riskCesDate"));
		
		adhoccrRec.setCovrCoverage(inPutParam2.get("covrCoverageParamList"));
		adhoccrRec.setCrtable(inPutParam2.get("crtableParamList"));
		adhoccrRec.setCovrSVMethod(inPutParam2.get("covrSVMethodParamList"));
		adhoccrRec.setCovrcd(inPutParam2.get("covrcdParamList"));
		adhoccrRec.setCovrPaidPrem(inPutParam2.get("covrPaidPremParamList"));
		
		adhoccrRec.initialise();
		
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && ExternalisedRules.isCallExternal("SURADHOCREFUND")) {
            callProgram("SURADHOCREFUND",adhoccrRec);
        }
		
		if(adhoccrRec.statuz.toString().equals(Varcom.oK.toString())) {
			amountList = adhoccrRec.getSurrValCovr().get(0);
			int i = 0;
			for(String s: coverageList) {
				if(!(outputParam.containsKey(s))) {
					outputParam.put(s, new BigDecimal(amountList.get(i)));
				}
				i++;
			}
			String sum = adhoccrRec.getSurrValCovr().get(0).get(i);
			policyFeeAmount.set(sum);
		} 
		else {
			syserrrec.subrname.set(wsaaSubr);
			srcalcpy.status.set(adhoccrRec.statuz);
		}
	}
	
	private void initMap()	{
		inPutParam.put("cnttype", new ArrayList<String>());
		inPutParam.put("riskComDate", new ArrayList<String>());
		inPutParam.put("ptdate", new ArrayList<String>());
		inPutParam.put("billfreq", new ArrayList<String>());
		inPutParam.put("paidPrem", new ArrayList<String>());
		inPutParam.put("billfreq", new ArrayList<String>());
		inPutParam.put("svMethod", new ArrayList<String>());
		
		inPutParam.put("riskCesDate", new ArrayList<String>());
		inPutParam.put("covrCoverage", new ArrayList<String>());
		inPutParam.put("crtable", new ArrayList<String>());
		inPutParam.put("covrSVMethod", new ArrayList<String>());
		inPutParam.put("covrcd", new ArrayList<String>());
		inPutParam.put("covrPaidPrem", new ArrayList<String>());
		
		inPutParam2.put("covrCoverageParamList",new ArrayList<List<String>>());
		inPutParam2.put("crtableParamList",new ArrayList<List<String>>());
		inPutParam2.put("covrSVMethodParamList",new ArrayList<List<String>>());
		inPutParam2.put("covrcdParamList", new ArrayList<List<String>>());
		inPutParam2.put("covrPaidPremParamList", new ArrayList<List<String>>());
	}

	private void setMap1()	{
		readChdrpf();
		readCovrpf();
		inPutParam.get("cnttype").add(srcalcpy.cnttype.toString());
		inPutParam.get("ptdate").add(srcalcpy.ptdate.toString());
		inPutParam.get("riskComDate").add(chdrpf.getOccdate().toString());
		inPutParam.get("svMethod").add(wsaabasecovrsvmethod.toString());
		inPutParam.get("riskCesDate").add(srcalcpy.effdate.toString());
	}
	
	private void readChdrpf() {
		chdrpf = chdrpfDAO.getchdrRecord(srcalcpy.chdrChdrcoy.toString(), srcalcpy.chdrChdrnum.toString());
		if(isEQ(chdrpf.getCurrfrom(), srcalcpy.ptdate)) {
			
			Chdrpf newChdrpf = new Chdrpf();
			newChdrpf = chdrpfDAO.getBillfreqSinstamt06Sinstamt02(srcalcpy.chdrChdrcoy.toString(), srcalcpy.chdrChdrnum.toString(), chdrpf.getInstfrom().toString());
			inPutParam.get("billfreq").add(newChdrpf.getBillfreq().toString());
			inPutParam.get("paidPrem").add(newChdrpf.getSinstamt06().toString());
			oldFreq.set(newChdrpf.getBillfreq());
			oldPolicyFee = newChdrpf.getSinstamt02();
			
		}
		else {
			oldPolicyFee = chdrpf.getSinstamt02();
			inPutParam.get("billfreq").add(srcalcpy.billfreq.toString());
			inPutParam.get("paidPrem").add(chdrpf.getSinstamt06().toString());
		}
	}
	
	private void setMap2()	{
		inPutParam2.get("covrCoverageParamList").add(inPutParam.get("covrCoverage"));
		inPutParam2.get("crtableParamList").add(inPutParam.get("crtable"));
		inPutParam2.get("covrSVMethodParamList").add(inPutParam.get("covrSVMethod"));
		inPutParam2.get("covrcdParamList").add(inPutParam.get("covrcd"));
		inPutParam2.get("covrPaidPremParamList").add(inPutParam.get("covrPaidPrem"));
	}
	
	private void readCovrpf()	{
		covrList1 =covrpfDAO.getCovrpfRecord(srcalcpy.chdrChdrcoy.toString(), srcalcpy.chdrChdrnum.toString());
		if(!covrList1.isEmpty()){
			for(Covrpf covr : covrList1) {
				if(isLTE(covr.getCrrcd(), srcalcpy.effdate)) {
					covrList.add(covr);
				}
			}
			for(Covrpf covr : covrList) {
				if(covr.getLife().equals(wsaaLife.toString())) {
					coverageList.add(covr.getCrtable()+covr.getCoverage());
					inPutParam.get("covrCoverage").add(covr.getCoverage());
					inPutParam.get("crtable").add(covr.getCrtable());
					inPutParam.get("covrcd").add(String.valueOf(covr.getCrrcd()));
					getSVMethod(covr.getCrtable(), String.valueOf(covr.getCrrcd()));
					if(isEQ(covr.getCurrfrom(), srcalcpy.ptdate)) {
						datcon2rec.intDatex1.set(covr.getCurrfrom());
						datcon2rec.frequency.set(oldFreq);
						datcon2rec.freqFactor.set(-1);
						callProgram(Datcon2.class, datcon2rec.datcon2Rec);
						wsaaInstprem = covrpfDAO.getPreviousInstprem(covr.getChdrcoy(), covr.getChdrnum(), 
								covr.getLife(), covr.getCoverage(), covr.getRider(), covr.getCrtable(), datcon2rec.intDatex2.toString());
						inPutParam.get("covrPaidPrem").add(wsaaInstprem.toString());
					}
					else {
						inPutParam.get("covrPaidPrem").add(covr.getInstprem().toString());
					}
				}
			}
		}
		//If policy fee is to be calculated
		inPutParam.get("covrPaidPrem").add(oldPolicyFee.toString());
		inPutParam.get("covrSVMethod").add(wsaabasecovrsvmethod.toString());
		inPutParam.get("covrcd").add(String.valueOf(chdrpf.getOccdate()));
		inPutParam.get("covrCoverage").add("");
		inPutParam.get("crtable").add("");
	}
	
	private void getSVMethod(String item, String itemfrm) {
		Itempf itempf = itemDAO.findItemByDate("IT", srcalcpy.chdrChdrcoy.toString(), "T5687", item, itemfrm);
		if (itempf != null) {
			t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			inPutParam.get("covrSVMethod").add(t5687rec.svMethod.toString());
		}
	}
	
	protected void setOutgoingvariables() {
		
		srcalcpy.type.set("R");
		String matchData = srcalcpy.crtable.toString().trim() + srcalcpy.covrCoverage.toString().trim();
		if(outputParam.containsKey(matchData)) {
			srcalcpy.actualVal.set(outputParam.get(matchData));
			srcalcpy.estimatedVal.set(0);
			getDescription2400(srcalcpy.covrCoverage.toString());
			countCoverage++;
		}
	}
	
	private void loadDescriptions() {
		descpfMap = descpfDAO.getMapDescItems("IT", srcalcpy.chdrChdrcoy.toString(), "T5687", srcalcpy.language.toString());
	}
	
	protected void getDescription2400(String coverage)
	{
		start2400(coverage);
	}

	protected void start2400(String coverage)
	{
		if(descpfMap.containsKey(srcalcpy.crtable)) {
			Descpf d = new Descpf();
			d = descpfMap.get(srcalcpy.crtable);
			if (isEQ(d.getShortdesc(),SPACES)) {
				srcalcpy.description.fill("?");
			}
			else {
				srcalcpy.description.set(d.getShortdesc());
			}
		}
	}

}