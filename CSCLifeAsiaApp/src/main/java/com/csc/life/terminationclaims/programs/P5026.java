/*
 * File: P5026.java
 * Date: 31 August 2009 11:47:41
 * Author: Quipoz Limited
 * 
 * Class transformed from P5026.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.financials.dataaccess.dao.CheqpfDAO;
import com.csc.fsu.financials.dataaccess.dao.model.Cheqpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3688rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.enquiries.recordstructures.Uwlmtrec;
import com.csc.life.general.tablestructures.T5611rec;
import com.csc.life.interestbearing.dataaccess.HitrrnlTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrtpmTableDAM;//IJS-58
import com.csc.life.terminationclaims.dataaccess.LifesurTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.HsudpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.PyoupfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.SurdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.SurhpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.HsudpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.SurdpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.SurhpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Hsudpf;
import com.csc.life.terminationclaims.dataaccess.model.Pyoupf;
import com.csc.life.terminationclaims.dataaccess.model.Surdpf;
import com.csc.life.terminationclaims.dataaccess.model.Surhpf;
import com.csc.life.terminationclaims.recordstructures.Cmcl001Rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.recordstructures.SurtaxRec;
import com.csc.life.terminationclaims.recordstructures.UnexpiredPrmrec;
import com.csc.life.terminationclaims.screens.S5026ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Td5h6rec;
import com.csc.life.terminationclaims.tablestructures.Tr691rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZutrpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.UsrdTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.recordstructures.Msgboxrec;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Full Surrender
*
* This   transaction,  Full  Surrender  is  selected  from  the
* surrender sub-menu  S5245/P5245.  This  program surrenders an
* entire Plan, or one  or  many policies. If it is surrendering
* many policies, it will be called for each policy required.
* A whole policy surrender is not permitted if any of the
* individual policies have been broken out.
*
*Initialise
*----------
*
* Read the  Contract  header  (function  RETRV)  and  read  the
* relevant data necessary for obtaining the status description,
* the Owner,  the  Life-assured,  Joint-life, CCD, Paid-to-date
* and the Bill-to-date.
*
* RETRV the "Plan" or  the  policy  to  be  worked  on from the
* COVRSUR  I/O  module,  it was selected from program P5015 (or
* defaulted if only one  policy present). If the Plan suffix in
* the key "kept" is zero,  the whole Plan is to be surrendered,
* otherwise, a single policy is to be surrendered.
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*      - READR  the   life  details  using  LIFESUR  (for  this
*           contract  number,  joint life number '00').  Format
*           the name accordingly.
*      - READR the  joint-life  details using LIFESUR (for this
*           contract  number,  joint life number '01').  Format
*           the name accordingly.
*
*
* Obtain the  necessary  descriptions  by  calling 'DESCIO' and
* output the descriptions of the statuses to the screen.
*
* Format Name
*
*      Read the  client  details  record  and  use the relevant
*           copybook in order to format the required names.
*
* Build the Coverage/Rider review details
*
*      If the  entire  Plan is being surrendered, then read all
*           the  coverage/riders  for  each  policy  within the
*           Plan.  The  literal  "Policy  no."  and  the policy
*           number  field  are non-display for the surrender of
*           an entire Plan.
*
*      If only one policy  within  a Plan is being surrendered,
*           read only the revelant coverage/riders.
*
*      N.B. the currency is decided by the surrender subroutine
*           and passed back.
*
*      If all the returned  details  are  of  the same currency
*           throughout, then default  the currency field at the
*           bottom of the screen to the same currency.
*
*      For each coverage/rider attached to the policy
*
*           - Check that the component has a valid status for
*                surrendering by reading T5679. Invalid
*                status components will not call the surrender
*                calculation routine and will display a
*                blank line in the subfile. Valid status
*                components will further have their risk
*                cessation dates checked against the
*                effective date. Any components which have
*                risk cessation dates prior to the effective
*                date and which are still in force, will prevent
*                the transaction from continuing.
*
*           - call  the  surrender  calculation  subroutine  as
*                defined on  T5687,  this  method  is  used  to
*                access  T6598,  which contains the subroutines
*                necessary   for   the  surrender  calculation,
*                processing.
*
* Linkage area passed to the surrender calculation subroutine:-
*
*        - company
*        - contract header number
*        - suffix
*        - life number
*        - joint-life number
*        - coverage
*        - rider
*        - crtable
*        - language
*        - estimated value
*        - actual value
*        - currency
*        - element code
*        - description
*        - type code
*        - status
*
*      DOWHILE
*         surrender calculation subroutine status not = ENDP
*            - load the subfile record
*            - accumulate single currency if applicable
*            - call surrender calculation subroutine
*      ENDDO
*
*      Surrender claim subroutines  may  work at Plan or policy
*           level,  therefore   the   DOWHILE  process  may  be
*           processed for end of  COVR  for a Plan or a Policy,
*           determine the type of surrender being processed.
*
*      If the  policy  selected is part of a summary, then  the
*           amount  returned  must be divided by "n" ("n" being
*           the  number of policies in the plan). Note:- If the
*           policy  selected  is not only part of a summary but
*           also  the  first policy from the summary, then  the
*           value  returned  is calculated as:  n - (n-1), this
*           caters for any rounding problems that may occur.
*
*      Establish whether, for a single policy selection this is
*           part of a multipolicy surrender and if so is this
*           the first policy to be surrendered. This is done by
*           checking for the existence of a Surrender Header
*           record for this Contract Header and for the
*           relevant Transaction Number.
*
* Policy Loans
*
*      this amount is the total value of loans held against
*           the contract. It is obtained by calling TOTLOAN
*           using this contract header number.
*           Once returned, this figure will be deducted from
*           the Surrender Value. The figure remaining will be
*           displayed in the Net of Surrender Value Debt field.
*           Before deducting the value of loans outstanding
*           from the Surrender Value, any previous Surrender
*           records belonging to this transaction and hence
*           unprocessed are read. These records will have
*           already reduced the loan amount outstanding
*           and must be taken into consideration.
*           These records may be in a different currency to
*           the CHDR currency and if so must first be converted.
*
* Net of Surrender Value Debt
*
*      this is the value of loans outstanding after this
*           policy has been surrendered and taking into
*           account any previous surrenders for the current
*           transaction.
*
* Total Estimated value
*
*      this amount  is  the  total of the estimated values from
*           the  coverage/rider  subfile review. This amount is
*           the  sum  of the individual amounts returned by the
*           subroutine. (If all the amounts returned are in the
*           same currency).
*
* Total Actual value
*
*      this amount  is  the total of the actual values from the
*           coverage/rider  subfile review (i.e. the sum of the
*           individual amounts returned  by  the subroutine, if
*           all the amounts returned are  in the same currency)
*           together with the policy loans and manually entered
*           adjustments, initially zero.
*
* The above details are returned  to  the user and if he wishes
* to continue and  perform the Full Surrender routine he enters
* the required data  at  the bottom of the screen, otherwise he
* opts for KILL to exit from this transaction.
*
*Validation
*----------
*
* If  KILL   was entered, then skip the remainder of the
* validation and exit from the program.
*
* Effective-Date(optional)
*
*      Check that the Effective-date entered  on  the screen is
*           not less than  the contract commencement date (CCD)
*           and  that  it is  not  greater  than  today's date.
*           Default is today's date.
*
* Surrender adjustment reason (optional)
*
*      Validated by the I/O module.
*
* Surrender adjustment reason description (optional)
*
*      Check whether an  adjustment  reason was entered or not.
*           If an  adjustment  reason  code  was entered and no
*           adjustment description was entered, then read T5500
*           and output the description found. This is displayed
*           if  CALC  is   pressed  (redisplay).  If  a  reason
*           description was entered, the system will default to
*           use this value  instead  of accessing T5500 for the
*           description.
*
* Currency (optional)
*
*      Validated by the I/O module.
*
*      A blank entry defaults to the contract currency.
*
* Other adjustment amount
*
*      A value may or may not be entered.
*
*      If this is part of a multipolicy transaction, then
*      adjustments may only be entered for the first policy
*      processed.
*
*      Do not allow full surrenders when no premiums have been
*           paid.
*      If CALC is pressed, then  the screen is re-displayed and
*           the adjusted  amount  is  included  with  the final
*           total amount, i.e.  if an adjusted amount exists.
*
*      If the    currency    code     was     changed,     then
*           re-calculate/convert all of the amounts held in the
*           subfile.
*
*Updating
*--------
*
* If the KILL function key was pressed,  skip  the updating and
* exit from the program.
*
* CREATE SURRENDER CLAIMS HEADER RECORD.
*
* Create a  surrender  claim  header  record (SURHCLM) for this
* contract and for this policy. If this is a part of a
* multipolicy surrender then a SURH is written only for the first
* policy. ie only one SURH record is written. Thereafter,
* subsequent policy surrenders will update the SURH with the
* policy number being surrendered, Thereby ensuring that the
* plan suffix passed to the AT via the Surrender Header is that
* of the lowest policy which was processed. This is
* important in establishing how many policies to break out.
* Note also that for multipolicy surrenders, only a single
* transaction number will be used representing the
* number currently held on the contract header sequentially
* incremented by one.
* Output  the  details  entered  on  the screen.
* Keyed by Company, Contract number, Suffix.
*
*           - company
*           - contract number
*           - suffix
*           - transaction
*           - life number
*           - joint-life number
*           - effective date
*           - currency
*           - policy loan amount
*           - tax amount
*           - other adjustments amount
*           - adjustment reason code
*           - adjustment reason description
*
* CREATE SURRENDER CLAIMS DETAILS RECORD.
*
* DOWHILE there are records in the subfile
*
*      - create a SURDCLM record for each subfile entry
*        output the following fields, Keyed by:-
*        Company, Contract no, Suffix,
*        Coverage, Rider, Element type
*
*           - company
*           - contract number
*           - suffix
*           - transaction
*           - life number
*           - joint-life number
*           - coverage
*           - rider
*           - element type (4 character code)
*           - description
*           - re-insurance indicator
*           - currency
*           - estimated value
*           - actual value
*           - type (1 character code) returned by surrender calc.
*
* ENDDO
*
*Next Program
*------------
*
* For KILL or 'Enter', add 1 to the program pointer and exit.
*
*Modifications
*-------------
*
* Rewrite P5026 with the above processing included.
*
* Include the AT module P5026AT.
*
* Include  the  following logical views (access only the fields
* required).
*
*       - CHDRSUR
*       - LIFESUR
*       - COVRSUR
*       - SURHCLM
*       - SURDCLM
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*  If there are outstanding unprocessed HITR records, output
*  a warning message : HL08 'Unprocessed HITR Pending', but
*  still allow the program to continue. This functionality
*  is part of the Interest Bearing Product specification.
*
*****************************************************************
* </pre>
*/
public class P5026 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(P5026.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5026");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);

	private ZonedDecimalData wsaaSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaSwitch, "1");
	private Validator summaryPartPlan = new Validator(wsaaSwitch, "3");

	
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaComponent = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaLife1 = new FixedLengthStringData(2).isAPartOf(wsaaComponent, 0);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaComponent, 2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaComponent, 4);
	private FixedLengthStringData wsaaPlansuff = new FixedLengthStringData(1).isAPartOf(wsaaComponent, 6);
	private FixedLengthStringData wsaaPlan = new FixedLengthStringData(1).isAPartOf(wsaaComponent, 7);
	
	private ZonedDecimalData wsaaSwitch2 = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator firstTime = new Validator(wsaaSwitch2, "1");
	private String wsaaUnprocUnits = "";
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(0);
	private ZonedDecimalData wsaaPlanSuffStore = new ZonedDecimalData(4, 0).init(0);
	private FixedLengthStringData wsaaStoredLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);

	private ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator detailsSameCurrency = new Validator(wsaaCurrencySwitch, "0");
	private Validator detailsDifferent = new Validator(wsaaCurrencySwitch, "1");
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).init(SPACES);
	
	private ZonedDecimalData wsaaNetSV = new ZonedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData amountap = new ZonedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPenaltyTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaLoanValue = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaActvalue = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTaxAmt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaFeeTax = new PackedDecimalData(17, 2).init(0);
	private final String wsaaLargeName = "LGNMS";
	
	
	
	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaBankdesc = new FixedLengthStringData(30).isAPartOf(wsaaBankkey, 0);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private FixedLengthStringData wsaaErrorIndicators = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaFirstTimeFlag = new FixedLengthStringData(1).init("Y");
	private Validator firstTimeThrough = new Validator(wsaaFirstTimeFlag, "Y");
	private String wsaaValidStatus = "";
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaMaturityFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSumFlag = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaSurrenderRecStore = new FixedLengthStringData(150);
	private PackedDecimalData wsaaPrem1 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrem2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wssaSum = new PackedDecimalData(17, 2);
	private PackedDecimalData wssaFinal = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaNetOfSvDebt = new ZonedDecimalData(17, 2);

	
	
	private FixedLengthStringData wsaaMsgboxParams = new FixedLengthStringData(176);
	private FixedLengthStringData wsaaCpfmsg = new FixedLengthStringData(7).isAPartOf(wsaaMsgboxParams, 0);
	private FixedLengthStringData wsaaInsert = new FixedLengthStringData(100).isAPartOf(wsaaMsgboxParams, 7);
	private FixedLengthStringData wsaaReply = new FixedLengthStringData(65).isAPartOf(wsaaMsgboxParams, 107);
	private FixedLengthStringData wsaaResult = new FixedLengthStringData(4).isAPartOf(wsaaMsgboxParams, 172);
	
	
	private FixedLengthStringData wsaaPaxmsg = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaPaxmsg, 0);
/*	private FixedLengthStringData wsaaServiceUnit = new FixedLengthStringData(2).isAPartOf(wsaaPaxmsg, 1).init("GP");*/
	private FixedLengthStringData wsaaMsgid = new FixedLengthStringData(4).isAPartOf(wsaaPaxmsg, 1);
	private BigDecimal subTotal;
	
	
	private FixedLengthStringData wsaaT5611Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5611Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5611Item, 0);
	private FixedLengthStringData wsaaT5611Pstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 4);
	private String wsaaNoPrice = "";
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private BabrTableDAM babrIO = new BabrTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrtpmTableDAM covrclmIO = new CovrtpmTableDAM();  //IJS-58
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrrnlTableDAM hitrrnlIO = new HitrrnlTableDAM();
//	private HsudTableDAM hsudIO = new HsudTableDAM();
	private IncrmjaTableDAM incrmjaIO = new IncrmjaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifesurTableDAM lifesurIO = new LifesurTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	//private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	private Surdpf surdclmIO = new Surdpf();
	private Rdockey wsaaRdockey = new Rdockey();
	private Alocnorec alocnorec = new Alocnorec();
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
//	private Surhpf surhpf = new Surhpf();
	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Cmcl001Rec cmcl001Rec = new Cmcl001Rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private T3695rec t3695rec = new T3695rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T5679rec t5679rec = new T5679rec();
	private T5611rec t5611rec = new T5611rec();
	private Tr691rec tr691rec = new Tr691rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Td5h6rec td5h6rec = new Td5h6rec();
	private T3629rec t3629rec = new T3629rec();
	private T3688rec t3688rec = new T3688rec();
	private T5644rec t5644rec = new T5644rec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5026ScreenVars sv = ScreenProgram.getScreenVars( S5026ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private ExternalisedRules er = new ExternalisedRules();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Msgboxrec msgboxrec = new Msgboxrec();
	private FixedLengthStringData preFunction = new FixedLengthStringData(50);
	private FixedLengthStringData preStatus = new FixedLengthStringData(50);
	private String p129 = "P129";
	
	//fwang3
	private SurdpfDAO surdpfDAO = getApplicationContext().getBean("surdpfDAO", SurdpfDAOImpl.class);
	private HsudpfDAO hsudpfDAO = getApplicationContext().getBean("hsudpfDAO", HsudpfDAOImpl.class);
	private SurhpfDAO surhpfDAO = getApplicationContext().getBean("surhpfDAO", SurhpfDAOImpl.class);
	private CheqpfDAO cheqpfDAO = getApplicationContext().getBean("cheqpfDAO", CheqpfDAO.class);
	
	private Map<String, List<Surdpf>> surdpfMap = new HashMap<String, List<Surdpf>>();
	private List<Surdpf> surdpfList = new ArrayList<>();
	private List<Hsudpf> hsudpfList = new ArrayList<>();
	private List<Surhpf> surhpfList = new ArrayList<>();
	private List<Cheqpf> cheqBulkInsList = new ArrayList<>();
	
	private Zutrpf zutrpf = new Zutrpf(); //ILIFE-5452
	private Pyoupf pyoupf = new Pyoupf();
	private Uwlmtrec uwlmtrec = new Uwlmtrec();    
	
	private PyoupfDAO pyoupfDAO = getApplicationContext().getBean("pyoupfDAO", PyoupfDAO.class);
	private ZutrpfDAO zutrpfDAO = getApplicationContext().getBean("zutrpfDAO", ZutrpfDAO.class);//ILIFE-5452
	private PackedDecimalData wsaaBusinessDate = new PackedDecimalData(8, 0);//ILIFE-5452
	private static final String t6640 = "T6640";//ILIFE-5980
	private static final String td5h6 = "TD5H6";
	private static final String t5687 = "T5687";
	private FixedLengthStringData wsaaTreditionslFlag = new FixedLengthStringData(1);//ILIFE-5980
	private Itempf itempf = null;
	private Cheqpf cheqpf = null;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Acblpf acblpf = null;
	private Payrpf payrpf = null;
    private Covrpf covrpf = null;
    private String uwlvrec = "UWLVREC";
	private String usrdrec = "USRDREC";
    
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Chdrpf chdrpf;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private UsrdTableDAM usrdIO = new UsrdTableDAM();
	boolean susur002Permission = false;    //ICIL-279
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private Clbapf clbapf;
	private ZonedDecimalData wsaaNetSecurebal = new ZonedDecimalData(17, 2).init(ZERO);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	boolean susur013Permission  = false;
	private Covrpf covrpfobj;
	private FixedLengthStringData wsaaSurrenderMethod = new FixedLengthStringData(4).init("SC14");
	private Sftlockrec sftlockrec = new Sftlockrec();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue1030, 
		chkUtrn1050, 
		exit1640, 
		exit1840, 
		checkForErrors2050, 
		exit2090, 
		updateErrorIndicators2270, 
		readSubfile3070, 
		exit3090, 
		exit5190
	}

	public P5026() {
		super();
		screenVars = sv;
		new ScreenModel("S5026", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case continue1030: 
					continue1030();
				case chkUtrn1050: 
					chkUtrn1050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/* Skip this section if returning from an optional selection,*/
		/* i.e. check the stack action flag equals '*'.*/ 
	    susur002Permission = FeaConfg.isFeatureExist("2", "SUSUR002", appVars, "IT");        //ICIL-279
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaSwitch.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		wsaaCurrencySwitch.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		wsaaSwitch2.set(1);
		wsaaStoredLife.set(SPACES);
		wsaaStoredCoverage.set(SPACES);
		wsaaStoredRider.set(SPACES);
		wsaaStoredCurrency.set(SPACES);
		wsaaLife.set(SPACES);
		wsaaJlife.set(SPACES);
		wsaaErrorIndicators.set(SPACES);
		wsaaMaturityFlag.set(SPACES);
		wsaaSumFlag.set(SPACES);
		wsaaCrtable.set(SPACES);
		wsaaNoPrice = "N";
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.bankacckey.set(SPACES);
		sv.bankkey.set(SPACES);
		sv.msgPopup.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		susur013Permission  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR013", appVars, "IT");
		if(!susur013Permission) {
			sv.unexpiredprmOut[Varcom.nd.toInt()].set("Y");
			sv.suspenseamtOut[Varcom.nd.toInt()].set("Y");
		}
		
		if (!susur002Permission) {
			sv.susur002flag.set("N");
		}
		else {
			sv.susur002flag.set("Y");
		}
		subTotal = BigDecimal.ZERO;
		/* Dummy subfile initalisation for prototype- replace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("S5026", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* retrieve the coverage in order to find out if a whole*/
		/* plan (plan suffix = 0 ) or and individual policy is being*/
		/* surrendered*/
		covrsurIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		chdrsurIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		
		if(isNE(chdrsurIO.getCnttype(), "TEN") && isNE(chdrsurIO.getCnttype(), "TWL") 
				&& isNE(chdrsurIO.getCnttype(), "JEA") && isNE(chdrsurIO.getCnttype(), "LCE")) {
			susur013Permission = false;
		}
		
		List<String> chdrnumList = new ArrayList<>();
		chdrnumList.add(chdrsurIO.getChdrnum().toString());
		this.surdpfMap = this.surdpfDAO.getSurdpfMap(chdrsurIO.getChdrcoy().toString(), chdrnumList);
		
		srcalcpy.ptdate.set(chdrsurIO.getPtdate());
		/* decide which part of the plan is being surrended*/
		/* POLSUM = 1 doesn't imply there is a summarised record.          */
		srcalcpy.planSuffix.set(covrsurIO.getPlanSuffix());
		if (isEQ(covrsurIO.getPlanSuffix(), ZERO)) {
			wsaaSwitch.set(1);
		}
		else {
			if (isGT(covrsurIO.getPlanSuffix(), chdrsurIO.getPolsum())
			|| isEQ(chdrsurIO.getPolsum(), 1)) {
				wsaaSwitch.set(2);
			}
			else {
				wsaaSwitch.set(3);
			}
		}
		/*       MOVE 0                   TO SURC-PLAN-SUFFIX.*/
		/* Find the PAYR Billing frequency.                                */
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrsurIO.getChdrcoy());
		payrIO.setChdrnum(chdrsurIO.getChdrnum());
		payrIO.setValidflag("1");
		payrIO.setPayrseqno(covrsurIO.getPayrseqno());
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		surhclmIO.setPlanSuffix(covrsurIO.getPlanSuffix());
		/* MOVE CHDRSUR-BILLFREQ       TO SURC-BILLFREQ.                */
		srcalcpy.chdrCurr.set(chdrsurIO.getCntcurr());
		/*    IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'*/
		/*       GO TO 1090-EXIT.*/
		/*   MOVE SPACES                 TO S5026-DATA-AREA.*/
		/*   MOVE SPACES                 TO S5026-SUBFILE-AREA.*/
		/* Dummy subfile initalisation for prototype - replace with SCLR*/
		/*   MOVE SCLR                   TO SCRN-FUNCTION.*/
		/*   CALL 'S5026IO'              USING SCRN-SCREEN-PARAMS*/
		/*                               S5026-DATA-AREA*/
		/*                               S5026-SUBFILE-AREA.*/
		/*   IF SCRN-STATUZ              NOT = O-K*/
		/*      MOVE SCRN-STATUZ         TO SYSR-STATUZ*/
		/*      PERFORM 600-FATAL-ERROR.*/
		/*   MOVE 1                      TO SCRN-SUBFILE-RRN.*/
		/*    Dummy field initilisation for prototype version.*/
		sv.estimateTotalValue.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.policyloan.set(ZERO);
		sv.tdbtamt.set(ZERO);
		sv.zrcshamt.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.clamant.set(ZERO);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(chdrsurIO.getOccdate());
		sv.chdrnum.set(chdrsurIO.getChdrnum());
		sv.cnttype.set(chdrsurIO.getCnttype());
//		ILIFE-5452 
		sv.reserveUnitsDate.set(varcom.vrcmMaxDate);
		sv.reserveUnitsInd.set(SPACES);
		descIO.setDescitem(chdrsurIO.getCnttype());
		sv.effdate.set(wsspcomn.currfrom);
		descIO.setDesctabl(tablesInner.t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(tablesInner.t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.descrip.set(descIO.getLongdesc());
		}
		else {
			sv.descrip.fill("?");
		}
		sv.cownnum.set(chdrsurIO.getCownnum());
		cltsIO.setClntnum(chdrsurIO.getCownnum());
		getClientDetails1200();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.btdate.set(chdrsurIO.getBtdate());
		sv.ptdate.set(chdrsurIO.getPtdate());
		if (isNE(sv.ptdate, sv.btdate)) {
			/*        MOVE G008               TO S5026-PTDATE-ERR.        <011>*/
			sv.ptdateErr.set(errorsInner.g008);
			sv.btdateErr.set(errorsInner.g008);
		}
		if (isEQ(chdrsurIO.getPtdate(), chdrsurIO.getCcdate())) {
			sv.ptdateErr.set(errorsInner.t065);
		}
		/*    Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrsurIO.getStatcode());
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrsurIO.getPstatcode());
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/* read the life details and the joint life details if they exist*/
		/* and format the names*/
		lifesurIO.setDataArea(SPACES);
		lifesurIO.setChdrcoy(chdrsurIO.getChdrcoy());
		lifesurIO.setChdrnum(chdrsurIO.getChdrnum());
		lifesurIO.setLife(covrsurIO.getLife());
		wsaaLife.set(covrsurIO.getLife());
		lifesurIO.setJlife("00");
		lifesurIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifesurIO);
		if (isNE(lifesurIO.getStatuz(), varcom.oK)
		&& isNE(lifesurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company, lifesurIO.getChdrcoy())
		|| isNE(chdrsurIO.getChdrnum(), lifesurIO.getChdrnum())
		|| isNE(lifesurIO.getJlife(), "00")
		&& isNE(lifesurIO.getJlife(), "  ")
		|| isEQ(lifesurIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		/* Move the last record read to the screen and get the*/
		/* description.*/
		sv.lifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1200();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.linsnameErr.set(errorsInner.e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		/*    look for joint life.*/
		lifesurIO.setJlife("01");
		lifesurIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifesurIO);
		if ((isNE(lifesurIO.getStatuz(), varcom.oK))
		&& (isNE(lifesurIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		/*if (isEQ(lifesurIO.getStatuz(), varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.continue1030);
	
		}*/
		sv.jlifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1200();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.jlinsnameErr.set(errorsInner.e304);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
		
		if(susur002Permission)
		{
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(sv.chdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		if (isNE(sv.chdrnum,SPACES)) {
			SmartFileCode.execute(appVars, chdrlnbIO);
		}
		else {
			chdrlnbIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		
		sv.payrnum.set(chdrlnbIO.getCownnum());
		sv.reqntype.set(chdrlnbIO.getReqntype());
		
		
		
		//ILIFE-2472-START
		if( null!=chdrlnbIO.getPayclt() && isNE(chdrlnbIO.getPayclt(), SPACES) ) {
			sv.payrnum.set(chdrlnbIO.getPayclt());
		}
		//ILIFE-2472-END
		a1000GetPayorname();
		sv.payorname.set(namadrsrec.name);
		wsaaClntkey.set(SPACES);
		//ILIFE-2472-START
		if ( isNE(chdrlnbIO.getCownnum(),SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrlnbIO.getCownpfx());
			stringVariable1.addExpression(chdrlnbIO.getCowncoy());
			stringVariable1.addExpression(chdrlnbIO.getCownnum());
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		
		if (isNE(sv.reqntype, '4') || isNE(sv.reqntype, 'C')) {
			sv.bankacckeyOut[varcom.nd.toInt()].set("Y");
			sv.crdtcrdOut[varcom.nd.toInt()].set("Y");
			sv.bankacckey.set(SPACES);
			sv.crdtcrd.set(SPACES);
			
		}
		
		wsaaLanguage.set(wsspcomn.language);
		wsaaMsgid.set(SPACES);
		preFunction.set(SPACE);
		preStatus.set(SPACE);
		sv.msgDisplay.set(ZERO);
		sv.msgresult.set(SPACE);
		sv.msgPopup.set(SPACE);
		
		
		
	}
		if (isEQ(lifesurIO.getStatuz(), varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.continue1030);
	
		}
	}


protected void bankBranchDesc1120()
	{
	babrIO.setDataKey(SPACES);
	babrIO.setBankkey(sv.bankkey);
	babrIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, babrIO);
	if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
		sv.bankkeyErr.set(errorsInner.f906);
	}
	if (isNE(babrIO.getStatuz(),varcom.oK)
	&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(babrIO.getParams());
		fatalError600();
	}
	wsaaBankkey.set(babrIO.getBankdesc());
	sv.bankdesc.set(wsaaBankdesc);
	}


protected void a1000GetPayorname()	{
	/*A1010-NAMADRS*/
	initialize(namadrsrec.namadrsRec);
	namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
	namadrsrec.clntCompany.set(wsspcomn.fsuco);
	namadrsrec.clntNumber.set(sv.payrnum);
	namadrsrec.language.set(wsspcomn.language);
	namadrsrec.function.set(wsaaLargeName);
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(namadrsrec.statuz);
		fatalError600();
	}
	/*A1090-EXIT*/
}

protected void continue1030()
	{
		/* Check for a pending Increase record for any component on     */
		/* this contract.  If one exists, display a warning message.    */
		checkForIncrease1500();
		/*  Read SURH for this CHDRNUM for this TRANNO, if one is found    */
		/*  then this cannot be the first-time-through. Set f	lag.          */
		
		//ILIFE-7520 starts		
				payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString());
				
				if (payrpf == null) {
					syserrrec.statuz.set("MRNF");
					syserrrec.params.set("2".concat(chdrsurIO.getChdrnum().toString()));
					fatalError600();
				}
				//ILIFE-7520 ends 
		if(susur002Permission || susur013Permission)
		{
			sv.bankkey.set(SPACES);
			
	wsaaBatckey.set(wsspcomn.batchkey);
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl("T5645");
	itempf.setItemitem(wsaaProg.toString());
    itempf.setItemseq("  ");
	itempf = itemDao.getItemRecordByItemkey(itempf);
	
	if (itempf == null) {
		syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5645").concat(wsaaProg.toString()));
		fatalError600();
	}
	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	
	    itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T3695");
		itempf.setItemitem(t5645rec.sacstype01.toString().trim());
		itempf = itemDao.getItemRecordByItemkey(itempf);
		
		if (itempf==null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T3695").concat(t5645rec.sacstype01.toString()));
			fatalError600();
		}
		t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		/*payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString());
		
		if (payrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set("2".concat(chdrsurIO.getChdrnum().toString()));
			fatalError600();
		}*/
		//ILIFE-7520
		if(susur013Permission) {
			getContractSuspense013();
		}else {
			getContractSuspense();
			wsaaPrem1.set(ZERO);
			wsaaPrem2.set(ZERO);
			wsaaTotalPrem.set(ZERO);
			wssaSum.set(ZERO);
			wssaFinal.set(ZERO);
			
			wsaaChdrnum.set(sv.chdrnum);
			List<Covrpf> list = covrpfDAO.getCovrmjaByComAndNum(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString());
			if(list !=null && list.size()!=0){
				for (int i=0; i<list.size(); i++ ) {
					covrpf = list.get(i);
					wsaaLife1.set(covrpf.getLife());
					wsaaCoverage.set(covrpf.getCoverage());
					wsaaRider.set(covrpf.getRider());
					wsaaPlansuff.set(covrpf.getPlanSuffix());
					wsaaPlan.set(ZERO);
					getTotalPremiumPaid();
					compute(wssaFinal, 2).set(add(wssaFinal,wssaSum));
					
				}
				
			}
			
			sv.sacscurbal1.set(wssaFinal);
		}
		
		
		
		
		
		}
		
		
		surhclmIO.setParams(SPACES);
		surhclmIO.setPlanSuffix(0);
		surhclmIO.setChdrcoy(wsspcomn.company);
		surhclmIO.setChdrnum(chdrsurIO.getChdrnum());
		surhclmIO.setTranno(chdrsurIO.getTranno());
		surhclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		surhclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surhclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		surhclmIO.setFormat(formatsInner.surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)
		&& isNE(surhclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surhclmIO.getParams());
			syserrrec.statuz.set(surhclmIO.getStatuz());
			fatalError600();
		}
		if (isNE(surhclmIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(surhclmIO.getChdrnum(), chdrsurIO.getChdrnum())
		|| isNE(surhclmIO.getTranno(), chdrsurIO.getTranno())
		|| isEQ(surhclmIO.getStatuz(), varcom.endp)) {
			wsaaFirstTimeFlag.set("Y");
		}
		else {
			wsaaFirstTimeFlag.set("N");
		}
		/*  Build the coverage and rider details*/
		/*  If the entire plan is being surrendered, then read all the*/
		/*  coverage/riders for each policy within the plan.*/
		/*  If only one policy within  a plan is being surrender read only*/
		/*  the relevent coverage/riders.*/
		if (isEQ(covrsurIO.getPlanSuffix(), ZERO)) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.planSuffix.set(covrsurIO.getPlanSuffix());
		}
		if (wholePlan.isTrue()) {
			wholePlan1300();
		}
		else {
			partPlan1700();
		}
		sv.policyloan.set(ZERO);
		/*  Computation of tax amount to be imposed.                       */
		
		/*IVE-705 Surrender Tax Calc Started*/
		//getTaxAmount1880();
		//sv.taxamt.set(wsaaTaxAmt);
		/*ILIFE-2397 Start */
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("SURTAX") && er.isExternalized(sv.cnttype.toString(), covrsurIO.crtable.toString())))//ILIFE-4833  
		{
			getTaxAmount1880();
			sv.taxamt.set(wsaaTaxAmt);
		}
		else
		{
			SurtaxRec surtaxRec = new SurtaxRec();	
			surtaxRec.cnttype.set(sv.cnttype);
			surtaxRec.cntcurr.set(sv.cnstcur);
			surtaxRec.effectiveDate.set(sv.effdate);
			surtaxRec.occDate.set(sv.effdate);
			surtaxRec.actualAmount.set(wsaaActualTot);						
			callProgram("SURTAX", surtaxRec.surtaxRec);			
			sv.taxamt.set(surtaxRec.taxAmount);
			sv.htype.set(surtaxRec.amountType);
		}
		/*ILIFE-2397 End */
			/*IVE-705 Surrender Tax Calc end*/
			
		/*  Calculate tax                                     .            */
		checkCalcTax5100();
		/*  Get the value of any Loans held against this component.        */
		getLoanDetails1900();
		getPolicyDebt1990();
		if(susur013Permission) {
			getUnexpiredPremium();
		}
		/* COMPUTE S5026-NET-OF-SV-DEBT = S5026-POLICYLOAN -            */
		/*                                WSAA-ACTUAL-TOT.              */
		if(susur002Permission)
		{
			compute(wsaaNetSV, 2).set(sub(sub(sv.policyloan, sv.zrcshamt), wsaaActualTot));
			compute(wsaaNetSecurebal, 2).set(mult(sv.sacscurbal,-1));
			compute(sv.netOfSvDebt, 2).set(add(wsaaNetSV,wsaaNetSecurebal));
		}
		else
		{
		compute(sv.netOfSvDebt, 2).set(sub(sub(sv.policyloan, sv.zrcshamt), wsaaActualTot));
		}
		
		
		if (detailsSameCurrency.isTrue()) {
			sv.estimateTotalValue.set(wsaaEstimateTot);
			/*         COMPUTE S5026-CLAMANT = WSAA-ACTUAL-TOT +               */
			/*            S5026-POLICYLOAN                                     */
			/*      COMPUTE S5026-CLAMANT = WSAA-ACTUAL-TOT -               */
			/*         S5026-POLICYLOAN                                     */
			if(susur002Permission)
			{
				
				compute(sv.clamant, 2).set(add(sub(sub(add(sub(wsaaActualTot, sv.policyloan), sv.zrcshamt), sv.tdbtamt), sv.taxamt),sv.sacscurbal));
			}
			else if(susur013Permission)
			{
				compute(sv.clamant, 2).set(add(add(sub(add(sub(wsaaActualTot, sv.policyloan), sv.zrcshamt), sv.tdbtamt), sv.suspenseamt), sv.unexpiredprm));
			}
			else
			{
				compute(sv.clamant, 2).set(sub(sub(add(sub(wsaaActualTot, sv.policyloan), sv.zrcshamt), sv.tdbtamt), sv.taxamt));
			}
			
			/* In the case of SUM products with premium adjustments add the    */
			/* premium adjustments to the total value displayed on screen...   */
			if (isNE(wsaaSumFlag, SPACES)
			&& isNE(sv.otheradjst, ZERO)) {
				sv.clamant.add(sv.otheradjst);
			}
			/*         MOVE CHDRSUR-CNTCURR   TO S5026-CURRCD.                 */
			sv.currcd.set(chdrsurIO.getCntcurr());
			wsaaStoredCurrency.set(chdrsurIO.getCntcurr());
		}
		else {
			wsaaCurrencySwitch.set(1);
			wsaaStoredCurrency.set(SPACES);
		}
		
		/*  If a multi-currency contract skip validation if                */
		/*  totals = zero                                                  */
		if (isEQ(wsaaActualTot, ZERO)
		&& isEQ(wsaaEstimateTot, ZERO)
		&& detailsDifferent.isTrue()) {
			goTo(GotoLabel.chkUtrn1050);
			}
		/*  If Surrender Value totalled from the detail records is         */
		/*  =< 0 then put out message to this effect and prevent further   */
		/*  processing                                                     */
		if (isNE(srcalcpy.status, varcom.mrnf) && isLTE(wsaaActualTot, 0)
		&& isLTE(wsaaEstimateTot, 0) && !susur013Permission) {
			//ILIFE-7520 starts 
			if (payrpf == null) {
				acblpf = null;
			}
			else{
				acblpf = acblpfDAO.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode04.toString(), chdrsurIO.getChdrnum().toString(), payrpf.getBillcurr(), t5645rec.sacstype04.toString());
			}
			//ILIFE-7520 ends
			if (acblpf == null) {
				sv.netOfSvDebt.set(sv.policyloan);
			}
		}
		else {
			/*        IF  S5026-NET-OF-SV-DEBT    < 0                          */
			/*            MOVE 0                  TO S5026-NET-OF-SV-DEBT      */
			/*        ELSE                                                     */
			
			if(susur002Permission)
			{
			if (isEQ(sv.clamant, 0)
			&& (setPrecision(wsaaEstimateTot, 2)
			&& isGT((mult(sv.clamant, -1)), wsaaEstimateTot))) {
				/*                MOVE 0              TO S5026-CLAMANT       <P010>*/
				sv.clamantErr.set(errorsInner.hl36);   //ILIFE-6578
		    	}
			}
			else
			{
				if (isLT(sv.clamant, 0)
						&& (setPrecision(wsaaEstimateTot, 2)
						&& isGT((mult(sv.clamant, -1)), wsaaEstimateTot))) {
							/*                MOVE 0              TO S5026-CLAMANT       <P010>*/
					 sv.clamantErr.set(errorsInner.hl36);   //ILIFE-6578
						}
			}
			//ILIFE-6578  starts
			//WARNING MESSAGE : ZERO SURRENDER VALUE
			if(isEQ(sv.clamant, wsaaEstimateTot)) {
				scrnparams.errorCode.set(errorsInner.rr88);
				 
					}
			//ILIFE-6578  ends
			
			if (isEQ(srcalcpy.status, varcom.mrnf)) {	
				sv.clamantErr.set(errorsInner.rpy5);
			}
			/**        END-IF                                                   */
		}
		
//		ILIFE-5452
		varcom.vrcmTranid.set(wsspcomn.tranid);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaBusinessDate.set(datcon1rec.intDate);
		
		if(susur002Permission)
		{
		itempfList=itempfDAO.getAllItemitem("IT",chdrsurIO.getChdrcoy().toString(), t6640, covrsurIO.getCrtable().toString()); 
		
		if (isLT(sv.netOfSvDebt, 0))
		{
			wsaaNetOfSvDebt.set(mult(sv.netOfSvDebt,-1));
		
		      if (itempfList.size() >= 1 )
		      {
			
			if(isGT(wsaaNetOfSvDebt,sv.sacscurbal1))
				/*if(isGT(wsaaNetOfSvDebt,30))*/
		    	{
					
				scrnparams.errorCode.set(errorsInner.rrfl);
				
		    	}
				
	    	  }
			
		}
		
		else
		{
			if (itempfList.size() >= 1 ){
				
				if(isGT(sv.netOfSvDebt,sv.sacscurbal1))
					/*if(isGT(sv.netOfSvDebt,30))*/
				{
						scrnparams.errorCode.set(errorsInner.rrfl);
				}
					
			}	
		}
		
		}
		
	}


private void getUnexpiredPremium() {
	
	if(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("UNEXPIREDPRM"))
	{
		UnexpiredPrmrec unexpiredPrmRec = new UnexpiredPrmrec();
		unexpiredPrmRec.cnttype.set(sv.cnttype);
		String isPrmCollected;
		if(isGT(payrIO.getPtdate(), sv.effdate) && isEQ(payrIO.getPtdate() , payrIO.getBtdate())) {
			isPrmCollected = "Y";
		}else {
			isPrmCollected = "N";
		}
		unexpiredPrmRec.prmCollectedFlag.set(isPrmCollected);
		
		ZonedDecimalData calcPremium = new ZonedDecimalData(17,2);
		if(isGT(payrIO.getEffdate(),sv.effdate)) {
			List<Payrpf> payrpflist = payrpfDAO.getPayrListByCoyAndNumAndSeq(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString(), payrIO.getPayrseqno().toInt(), "2");
			for(Payrpf payrpf1: payrpflist) {
				if(isLT(payrpf1.getEffdate(),sv.effdate)) {
					calcPremium.set(payrpf1.getSinstamt06());
					unexpiredPrmRec.frequency.set(payrpf1.getBillfreq());
				}
			}
		}else {
			calcPremium.set(payrIO.getSinstamt06());
			unexpiredPrmRec.frequency.set(payrIO.getBillfreq());
		}

		unexpiredPrmRec.calcPremium.set(calcPremium);
		unexpiredPrmRec.sccDate.set(sv.occdate);
		unexpiredPrmRec.sEffDate.set(sv.effdate);
		unexpiredPrmRec.sptDate.set(sv.ptdate);//ibplife-3582
		callProgram("UNEXPIREDPRM",unexpiredPrmRec.unexpiredPrmRec);
		sv.unexpiredprm.set(unexpiredPrmRec.unexpiredPrmOut);
	}
}

protected void getContractSuspense013()
{

	acblpf = acblpfDAO.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode01.toString(), chdrsurIO.getChdrnum().toString(), payrpf.getBillcurr(), t5645rec.sacstype01.toString());
	
	if (acblpf == null) {
		sv.suspenseamt.set(ZERO);
	}
	else {
		if (isEQ(t3695rec.sign,"-")) {
			compute(sv.suspenseamt, 2).set(mult(acblpf.getSacscurbal(),-1));
		}
		else {
			sv.suspenseamt.set(acblpf.getSacscurbal());
		}
	}
}


protected void getContractSuspense()
	{
	
		acblpf = acblpfDAO.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode01.toString(), chdrsurIO.getChdrnum().toString(), payrpf.getBillcurr(), t5645rec.sacstype01.toString());
		
		if (acblpf == null) {
			sv.sacscurbal.set(ZERO);
		}
		else {
			if (isEQ(t3695rec.sign,"-")) {
				compute(sv.sacscurbal, 2).set(mult(acblpf.getSacscurbal(),-1));
			}
			else {
				sv.sacscurbal.set(acblpf.getSacscurbal());
			}
		}
	}

protected void getTotalPremiumPaid()
	{

		acblpf = acblpfDAO.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode02.toString(), wsaaRldgacct.toString(), payrpf.getBillcurr(), t5645rec.sacstype02.toString());
		
		if (acblpf == null) {
			wsaaPrem1.set(ZERO);
		}
		else {
			if (isEQ(t3695rec.sign,"-")) {
				compute(wsaaPrem1, 2).set(mult(acblpf.getSacscurbal(),-1));
			}
			else {
				wsaaPrem1.set(acblpf.getSacscurbal());
			}
		}
			
			acblpf = acblpfDAO.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode03.toString(), wsaaRldgacct.toString(), payrpf.getBillcurr(), t5645rec.sacstype03.toString());
			
			if (acblpf == null) {
				wsaaPrem2.set(ZERO);
			}
			else {
				if (isEQ(t3695rec.sign,"-")) {
					compute(wsaaPrem2, 2).set(mult(acblpf.getSacscurbal(),-1));
				}
				else {
					wsaaPrem2.set(acblpf.getSacscurbal());
				}
		}
			compute(wsaaTotalPrem, 2).set(add(wsaaPrem1,wsaaPrem2));
			/*sv.sacscurbal1.set(wsaaTotalPrem);*/
			wssaSum.set(wsaaTotalPrem);
			
	
	}

protected void chkUtrn1050()
	{
		/*  Check the UTRN file for any unprocessed and put out a          */
		/*  warning message if the Feedback indicator not = 'Y'.           */
		wsaaUnprocUnits = "N";
		checkUtrns2500();
		if (isEQ(scrnparams.errorCode, SPACES)) {
			checkHitrs2550();
		}
		/*  Store any errors that have been encountered.                   */
		wsaaErrorIndicators.set(sv.errorIndicators);
		/* Write dummy subfile line if subfile is empty. This is to        */
		/* initialise the subfile area.                                    */
		if (isEQ(scrnparams.subfileRrn, 0)) {
			sv.cnstcur.set(SPACES);
			sv.coverage.set(SPACES);
			sv.hcnstcur.set(SPACES);
			sv.hcover.set(SPACES);
			sv.hcrtable.set(SPACES);
			sv.hjlife.set(SPACES);
			sv.htype.set(SPACES);
			sv.life.set(SPACES);
			sv.rider.set(SPACES);
			sv.shortds.set(SPACES);
			sv.fieldType.set(SPACES);
			sv.fund.set(SPACES);
			sv.actvalue.set(ZERO);
			sv.estMatValue.set(ZERO);
			sv.hactval.set(ZERO);
			sv.hemv.set(ZERO);
			scrnparams.function.set(varcom.sadd);
			processScreen("S5026", sv);
			if (isNE(scrnparams.statuz, varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
	}

protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void wholePlan1300()
	{
		read1310();
	}

protected void read1310()
	{
		/* Begin on the coverage/rider record*/
		covrclmIO.setDataArea(SPACES);
		covrclmIO.setChdrcoy(covrsurIO.getChdrcoy());
		covrclmIO.setChdrnum(covrsurIO.getChdrnum());
		//Start--IJS-58
		/*covrclmIO.setLife(covrsurIO.getLife());
		covrclmIO.setCoverage(covrsurIO.getCoverage());
		covrclmIO.setRider(covrsurIO.getRider());*/
		//End--IJS-58
		covrclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(), varcom.oK)
		&& isNE(covrclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrclmIO.getParams());
			fatalError600();
		}
		if (isNE(covrclmIO.getChdrcoy(), covrsurIO.getChdrcoy())
		|| isNE(covrclmIO.getChdrnum(), covrsurIO.getChdrnum())
		//Start--IJS-58
		/*|| isNE(covrclmIO.getLife(), covrsurIO.getLife())
		|| isNE(covrclmIO.getCoverage(), covrsurIO.getCoverage())
		|| isNE(covrclmIO.getRider(), covrsurIO.getRider())*/
		//End--IJS-58
		|| isEQ(covrclmIO.getStatuz(), varcom.endp)) {
			covrclmIO.setStatuz(varcom.endp);
		}
		wsaaStoredLife.set(covrclmIO.getLife());
		wsaaStoredCoverage.set(covrclmIO.getCoverage());
		wsaaStoredRider.set(covrclmIO.getRider());
		wsaaStoredCurrency.set(covrclmIO.getPremCurrency());
		while ( !(isEQ(covrclmIO.getStatuz(), varcom.endp))) {
			processComponents1350();
		}
		
	}

protected void processComponents1350()
	{
		read1351();
	}

protected void read1351()
	{
		/* Read table T5687 to find the surrender calculation subroutine*/
		wsaaCrtable.set(covrclmIO.getCrtable());
		obtainSurrenderCalc1400();
		/* add fields to subfile*/
		sv.life.set(covrclmIO.getLife());
		sv.hjlife.set(covrclmIO.getJlife());
		sv.coverage.set(covrclmIO.getCoverage());
		sv.hcover.set(covrclmIO.getCoverage());
		if (isEQ(covrclmIO.getRider(), "00")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(covrclmIO.getRider());
		}
		if (isNE(covrclmIO.getRider(), "00")
		&& isNE(covrclmIO.getRider(), "  ")) {
			sv.coverage.set(SPACES);
		}
		sv.cnstcur.set(covrclmIO.getPremCurrency());
		sv.hcnstcur.set(covrclmIO.getPremCurrency());
		srcalcpy.currcode.set(covrclmIO.getPremCurrency());
		validateStatusesWp1650();
		if (isEQ(wsaaValidStatus, "Y")
		&& isLT(covrclmIO.getRiskCessDate(), sv.effdate)) {
			wsaaMaturityFlag.set("Y");
			if (isEQ(covrclmIO.getRider(), "00")) {
				sv.chdrnumErr.set(errorsInner.t062);
			}
			else {
				sv.chdrnumErr.set(errorsInner.t062);
			}
		}
		/* check the reinsurance file at a later date and move*/
		/* 'y' to this field if they are present*/
		/* This Check never actually took place, but does now.             */
		/*  MOVE SPACES                    TO S5026-RIIND.               */
		/*   PERFORM 1980-READ-RACD.                              <A06843>*/
		/*                                                        <A06843>*/
		/*   IF RACDMJA-STATUZ            = O-K                   <A06843>*/
		/*       MOVE 'Y'                TO S5026-RIIND           <A06843>*/
		/*   END-IF.                                              <A06843>*/
		srcalcpy.endf.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrclmIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrclmIO.getChdrnum());
		srcalcpy.lifeLife.set(covrclmIO.getLife());
		srcalcpy.lifeJlife.set(covrclmIO.getJlife());
		srcalcpy.covrCoverage.set(covrclmIO.getCoverage());
		srcalcpy.covrRider.set(covrclmIO.getRider());
		srcalcpy.crtable.set(covrclmIO.getCrtable());
		srcalcpy.crrcd.set(covrclmIO.getCrrcd());
		srcalcpy.convUnits.set(covrclmIO.getConvertInitialUnits());
		srcalcpy.pstatcode.set(covrclmIO.getPstatcode());
		srcalcpy.status.set(SPACES);
		srcalcpy.polsum.set(chdrsurIO.getPolsum());
		srcalcpy.language.set(wsspcomn.language);
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(payrIO.getBillfreq());
		}
		while (isNE(srcalcpy.status, varcom.mrnf) && !(isEQ(srcalcpy.status, varcom.endp))) {
			callSurMethodWhole1600();
		}
		
		while ( !((isNE(wsaaStoredLife, covrclmIO.getLife()))
		|| (isNE(wsaaStoredRider, covrclmIO.getRider()))
		|| (isNE(wsaaStoredCoverage, covrclmIO.getCoverage()))
		|| isEQ(covrclmIO.getStatuz(), varcom.endp))) {
			findNextComponent1550();
		}
		
		wsaaStoredLife.set(covrclmIO.getLife());
		wsaaStoredCoverage.set(covrclmIO.getCoverage());
		wsaaStoredRider.set(covrclmIO.getRider());
		wsaaStoredCurrency.set(covrclmIO.getPremCurrency());
	}

protected void obtainSurrenderCalc1400()
	{
		read1410();
		readT66402986();//ILIFE-5980
	}

protected void read1410()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(wsaaCrtable);
		/* MOVE CHDRSUR-OCCDATE           TO ITDM-ITMFRM.               */
		itdmIO.setItmfrm(covrclmIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), wsaaCrtable)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			scrnparams.errorCode.set(errorsInner.f294);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t6598);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t5644);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(t5687rec.bascpy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5644rec.t5644Rec.set(SPACES);
		} else {
			t5644rec.t5644Rec.set(itemIO.getGenarea());
		}
	}

protected void checkForIncrease1500()
	{
		begn1510();
	}

protected void begn1510()
	{
		/* Check for any pending Increase records (Validflag = '1')     */
		/* for the contract.                                            */
		incrmjaIO.setParams(SPACES);
		incrmjaIO.setChdrcoy(chdrsurIO.getChdrcoy());
		incrmjaIO.setChdrnum(chdrsurIO.getChdrnum());
		incrmjaIO.setLife(ZERO);
		incrmjaIO.setCoverage(ZERO);
		incrmjaIO.setRider(ZERO);
		incrmjaIO.setPlanSuffix(ZERO);
		incrmjaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		incrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		incrmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		incrmjaIO.setFormat(formatsInner.incrmjarec);
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)
		&& isNE(incrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(incrmjaIO.getStatuz());
			syserrrec.params.set(incrmjaIO.getParams());
			fatalError600();
		}
		/* If the record retrieved is not for the correct contract,     */
		/* or the end of the file has been reached, leave the section.  */
		if (isNE(incrmjaIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(incrmjaIO.getChdrnum(), chdrsurIO.getChdrnum())
		|| isEQ(incrmjaIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* Display the warning message.                                 */
		scrnparams.errorCode.set(errorsInner.j008);
	}

protected void findNextComponent1550()
	{
		/*READ*/
		/* Read the next coverage/rider record.*/
		covrclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(), varcom.oK)
		&& isNE(covrclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrclmIO.getParams());
			fatalError600();
		}
		if (isNE(covrclmIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(covrclmIO.getChdrnum(), chdrsurIO.getChdrnum())) {
			/* OR  COVRCLM-LIFE            NOT = WSAA-LIFE                  */
			covrclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void callSurMethodWhole1600()
	{
		try {
			read1610();
			if(isNE(t5644rec.rfdsbrtine,SPACES)) {
				callRefunCommSubtoutine();
			} else {
				sv.commclaw.set(BigDecimal.ZERO);
			}
			addToSubfile1630();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void read1610()
	{
		srcalcpy.effdate.set(sv.effdate);
		srcalcpy.type.set("F");
		/* IF COVRCLM-INSTPREM         > ZERO                           */
		srcalcpy.singp.set(covrclmIO.getInstprem());
		/* ELSE                                                         */
		/*    MOVE COVRCLM-SINGP       TO SURC-SINGP.                   */
		/* if no surrender method found print zeros as surrender value*/
		if (isEQ(t6598rec.calcprog, SPACES)
		|| isEQ(wsaaValidStatus, "N")) {
			sv.estMatValue.set(ZERO);
			sv.hemv.set(ZERO);
			sv.actvalue.set(ZERO);
			sv.hactval.set(ZERO);
			sv.fieldType.set(SPACES);
			sv.shortds.set(SPACES);
			sv.fund.set(SPACES);
			sv.cnstcur.set(SPACES);
			srcalcpy.status.set(varcom.endp);
			/*     GO TO 1630-ADD-TO-SUBFILE.                               */
			goTo(GotoLabel.exit1640);
		}
		
		/*IVE-797 RUL Product - Full Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		/*ILIFE-7548 start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()) && er.isExternalized(chdrsurIO.getCnttype().toString(), srcalcpy.crtable.toString())))  
		{
			srcalcpy.surrCalcMeth.set(t5687rec.svMethod);
			srcalcpy.cnttype.set(chdrsurIO.getCnttype());
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
			callProgramX(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		/*ILIFE-7548 end*/
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrsurIO);//VPMS call
			//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.endf.set("Y");
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
			/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(vpxsurcrec.statuz.equals(varcom.endp))
				srcalcpy.status.set(varcom.endp);
			/* ILIFE-3142 End*/
		}
		
		/*IVE-797 RUL Product - Full Surrender Calculation end*/
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)
		&& isNE(srcalcpy.status, "NOPR") && isNE(srcalcpy.status, varcom.mrnf)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isEQ(srcalcpy.status, "NOPR")) {
			wsaaNoPrice = "Y";
			sv.effdateErr.set(errorsInner.g094);
		}
		if (isEQ(srcalcpy.status, varcom.oK)
		|| isEQ(srcalcpy.status, varcom.endp)) {
			if (isEQ(srcalcpy.currcode, SPACES)) {
				srcalcpy.currcode.set(chdrsurIO.getCntcurr());
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			callRounding6000();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			callRounding6000();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		if (isEQ(srcalcpy.type, "C")) {
			compute(srcalcpy.actualVal, 2).set(mult(srcalcpy.actualVal, -1));
		}
		/* Check the amount returned for being negative. In the case of    */
		/* SUM products this is possible and so set these values to zero.  */
		/* Note SUM products do not have to have the same PT & BT dates.   */
		checkT56112700();
		if (isNE(srcalcpy.type, "E")) {
			if (isNE(wsaaSumFlag, SPACES)) {
				if (isNE(sv.ptdate, sv.btdate)) {
					sv.ptdateErr.set(SPACES);
					sv.btdateErr.set(SPACES);
				}
				if (isLT(srcalcpy.actualVal, ZERO)) {
					srcalcpy.actualVal.set(ZERO);
				}
			}
		}
		if (isEQ(srcalcpy.currcode, SPACES)) {
			sv.cnstcur.set(chdrsurIO.getCntcurr());
			sv.hcnstcur.set(chdrsurIO.getCntcurr());
		}
		else {
			sv.cnstcur.set(srcalcpy.currcode);
			sv.hcnstcur.set(srcalcpy.currcode);
		}
		sv.hcrtable.set(covrclmIO.getCrtable());
		sv.htype.set(srcalcpy.type);
		sv.fieldType.set(srcalcpy.type);
		sv.estMatValue.set(srcalcpy.estimatedVal);
		sv.hemv.set(srcalcpy.estimatedVal);
		sv.actvalue.set(srcalcpy.actualVal);
		sv.hactval.set(srcalcpy.actualVal);
		sv.fund.set(srcalcpy.fund);
		sv.shortds.set(srcalcpy.description);
		sv.effdate.set(srcalcpy.effdate);
		/*  IF S5026-SHORTDS         NOT = COVRCLM-CRTABLE       <A06843>*/
		/*      MOVE SPACES             TO S5026-RIIND           <A06843>*/
		/*  END-IF.                                              <A06843>*/
		/*  If the description is 'PENALTY', as set up in subroutine       */
		/*  UNLSURC then subract this amount to give a true actual value.  */
		/* IF FIRST-TIME                                                */
		/*    MOVE 0                   TO WSAA-SWITCH2                  */
		/*    MOVE S5026-CNSTCUR       TO WSAA-STORED-CURRENCY          */
		/*    ADD SURC-ESTIMATED-VAL   TO  WSAA-ESTIMATE-TOT            */
		/*    ADD SURC-ACTUAL-VAL      TO  WSAA-ACTUAL-TOT              */
		/* ELSE                                                         */
		/*    IF SURC-CURRCODE            = WSAA-STORED-CURRENCY           */
		/* IF S5026-CNSTCUR            = WSAA-STORED-CURRENCY      <010>*/
		/*    ADD SURC-ESTIMATED-VAL   TO  WSAA-ESTIMATE-TOT            */
		/*    ADD SURC-ACTUAL-VAL      TO  WSAA-ACTUAL-TOT              */
		/* ELSE                                                         */
		/*    MOVE ZEROES              TO WSAA-ESTIMATE-TOT             */
		/*                                WSAA-ACTUAL-TOT               */
		/*      MOVE 1                 TO WSAA-CURRENCY-SWITCH.         */
		if (firstTime.isTrue()) {
			wsaaSwitch2.set(0);
			wsaaStoredCurrency.set(sv.cnstcur);
			wsaaEstimateTot.add(srcalcpy.estimatedVal);
			/*    IF SURC-DESCRIPTION      = 'PENALTY'                 <CAS1*/
			if (isEQ(srcalcpy.type, "C")) {
				compute(wsaaActualTot, 2).set(sub(wsaaActualTot, srcalcpy.actualVal));
				compute(wsaaPenaltyTot, 2).set(add(wsaaPenaltyTot, srcalcpy.actualVal));
			}
			else {
				compute(wsaaActualTot, 2).set(add(wsaaActualTot, srcalcpy.actualVal));
			}
		}
		else {
			if (isEQ(sv.cnstcur, wsaaStoredCurrency)) {
				wsaaEstimateTot.add(srcalcpy.estimatedVal);
				if (isEQ(srcalcpy.description, "Penalty")) {
					compute(wsaaActualTot, 2).set(sub(wsaaActualTot, srcalcpy.actualVal));
					compute(wsaaPenaltyTot, 2).set(add(wsaaPenaltyTot, srcalcpy.actualVal));
				}
				else {
					compute(wsaaActualTot, 2).set(add(wsaaActualTot, srcalcpy.actualVal));
				}
			}
			else {
				wsaaEstimateTot.set(ZERO);
				wsaaActualTot.set(ZERO);
				wsaaCurrencySwitch.set(1);
			}
		}
		/* Check the effective date of the surrender against the paid to   */
		/* date of the contract. If there are to be premiums paid or       */
		/* refunded then call the appropriate subroutine held on T5611.    */
		/* Note that if the item does not exist then the coverage does not */
		/* use the SUM calculation package & is hence not applicable....   */
		if (isNE(srcalcpy.effdate, srcalcpy.ptdate)
		&& isNE(wsaaSumFlag, SPACES)
		&& isNE(t5611rec.calcprog, SPACES)) {
			checkPremadj2600();
		}
		if (isEQ(srcalcpy.type, "P") && isEQ(t5687rec.svMethod, wsaaSurrenderMethod)) {
			sv.hcover.set(SPACES);
			sv.hcrtable.set(SPACES);
			sv.coverage.set(SPACES);
			sv.life.set(SPACES);
			sv.rider.set(SPACES);
		}
	}

	/**
	* <pre>
	*  add record to subfile
	* </pre>
	*/
protected void addToSubfile1630()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5026", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void validateStatusesWp1650()
	{
		start1650();
	}

	/**
	* <pre>
	*  Read T5679 to establish whether this component has a           
	*  valid status for this transaction.                             
	* </pre>
	*/
protected void start1650()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			riskStatusCheckWp1660();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				premStatusCheckWp1670();
			}
		}
	}

protected void riskStatusCheckWp1660()
	{
		/*START*/
		if (isEQ(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrclmIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrclmIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void premStatusCheckWp1670()
	{
		/*START*/
		if (isEQ(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrclmIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrclmIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void partPlan1700()
	{
		partPlan1710();
	}

protected void partPlan1710()
	{
		/* Begin on the coverage/rider record*/
		/*    MOVE 'RETRV'                TO COVRSUR-FUNCTION.             */
		covrsurIO.setFunction(varcom.begn);
		/*  Store the actual plan suffix for this transaction.             */
		wsaaPlanSuffStore.set(covrsurIO.getPlanSuffix());
		if (summaryPartPlan.isTrue()) {
			covrsurIO.setPlanSuffix(0);
		}
		wsaaStoredLife.set(covrsurIO.getLife());
		wsaaStoredCoverage.set(covrsurIO.getCoverage());
		wsaaStoredRider.set(covrsurIO.getRider());
		wsaaPlanSuffix.set(covrsurIO.getPlanSuffix());
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if (isNE(covrsurIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(covrsurIO.getChdrnum(), chdrsurIO.getChdrnum())
		|| isNE(covrsurIO.getPlanSuffix(), wsaaPlanSuffix)
		|| isNE(covrclmIO.getLife(), wsaaStoredLife)
		|| isNE(covrclmIO.getCoverage(), wsaaStoredCoverage)
		|| isNE(covrclmIO.getRider(), wsaaStoredRider)
		|| isEQ(covrclmIO.getStatuz(), varcom.endp)) {
			covrclmIO.setStatuz(varcom.endp);
		}
		/*    MOVE COVRSUR-COVERAGE          TO WSAA-STORED-COVERAGE.      */
		/*    MOVE COVRSUR-RIDER             TO WSAA-STORED-RIDER.         */
		wsaaStoredCurrency.set(covrsurIO.getPremCurrency());
		/*    MOVE COVRSUR-PLAN-SUFFIX       TO WSAA-PLAN-SUFFIX.          */
		while ( !(isEQ(covrsurIO.getStatuz(), varcom.endp))) {
			processPartComponents1750();
		}
		
		wsaaPlanSuffix.set(wsaaPlanSuffStore);
	}

protected void processPartComponents1750()
	{
		read1751();
	}

protected void read1751()
	{
		/* Read table T5687 to find the surrender calculation subroutine*/
		wsaaCrtable.set(covrsurIO.getCrtable());
		obtainSurrenderCalc1400();
		/* add fields to subfile*/
		sv.life.set(covrsurIO.getLife());
		sv.hjlife.set(covrsurIO.getJlife());
		sv.coverage.set(covrsurIO.getCoverage());
		sv.hcover.set(covrsurIO.getCoverage());
		if (isEQ(covrsurIO.getRider(), "00")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(covrsurIO.getRider());
		}
		if (isNE(covrsurIO.getRider(), "00")
		&& isNE(covrsurIO.getRider(), "  ")) {
			sv.coverage.set(SPACES);
		}
		sv.cnstcur.set(covrsurIO.getPremCurrency());
		sv.hcnstcur.set(covrsurIO.getPremCurrency());
		srcalcpy.currcode.set(covrsurIO.getPremCurrency());
		validateStatusesPp1950();
		/* IF  WSAA-VALID-STATUS = 'Y'                             <011>*/
		/* AND COVRSUR-RISK-CESS-DATE < S5026-EFFDATE              <011>*/
		/*     MOVE 'Y'                    TO WSAA-MATURITY-FLAG   <011>*/
		/*     IF  COVRCLM-RIDER = '00'                            <011>*/
		/*         MOVE T062               TO S5026-CHDRNUM-ERR    <011>*/
		/*     ELSE                                                <011>*/
		/*         MOVE T062               TO S5026-CHDRNUM-ERR    <011>*/
		/*     MOVE T062                   TO S5026-CHDRNUM-ERR    <011 */
		/*     END-IF                                              <011>*/
		/* END-IF.                                                 <011>*/
		if (isEQ(wsaaValidStatus, "Y")
		&& isLT(covrsurIO.getRiskCessDate(), sv.effdate)) {
			wsaaMaturityFlag.set("Y");
			sv.chdrnumErr.set(errorsInner.t062);
		}
		/* find description for crtable.*/
		/* check the reinsurance file at a later date and move*/
		/* 'y' to this field if they are present*/
		/* This Check never actually took place, but does now.             */
		/*   MOVE SPACES                 TO S5026-RIIND.                  */
		/*  PERFORM 1980-READ-RACD.                              <A06843>*/
		/*                                                       <A06843>*/
		/*  IF RACDMJA-STATUZ            = O-K                   <A06843>*/
		/*      MOVE 'Y'                TO S5026-RIIND           <A06843>*/
		/*  END-IF.                                              <A06843>*/
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrsurIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrsurIO.getChdrnum());
		srcalcpy.lifeLife.set(covrsurIO.getLife());
		srcalcpy.lifeJlife.set(covrsurIO.getJlife());
		srcalcpy.covrCoverage.set(covrsurIO.getCoverage());
		srcalcpy.covrRider.set(covrsurIO.getRider());
		srcalcpy.crtable.set(covrsurIO.getCrtable());
		srcalcpy.crrcd.set(covrsurIO.getCrrcd());
		srcalcpy.convUnits.set(covrsurIO.getConvertInitialUnits());
		srcalcpy.status.set(SPACES);
		srcalcpy.pstatcode.set(covrsurIO.getPstatcode());
		srcalcpy.polsum.set(chdrsurIO.getPolsum());
		srcalcpy.language.set(wsspcomn.language);
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(payrIO.getBillfreq());
		}
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			callSurMethodPart1800();
		}
		
		/* Read the next coverage/rider record.*/
		covrsurIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)
		&& isNE(covrsurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if (isNE(covrsurIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(covrsurIO.getChdrnum(), chdrsurIO.getChdrnum())
		|| isNE(covrsurIO.getPlanSuffix(), wsaaPlanSuffix)) {
			covrsurIO.setStatuz(varcom.endp);
		}
	}

protected void callSurMethodPart1800()
	{
		try {
			read1810();
			if(isNE(t5644rec.rfdsbrtine,SPACES)) {
				callRefunCommSubtoutine();
			} else {
				sv.commclaw.set(BigDecimal.ZERO);
			}
			addToSubfile1830();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void callRefunCommSubtoutine() {
	if (isEQ(srcalcpy.type, "P")) {
		sv.commclaw.set(BigDecimal.ZERO);
		return;
	}
	cmcl001Rec.setTranEffdate(srcalcpy.effdate.toString());
	cmcl001Rec.setChdrnum(srcalcpy.chdrChdrnum.toString());
	cmcl001Rec.setLanguage(wsspcomn.language.toString());
	cmcl001Rec.setLife(srcalcpy.lifeLife.toString());
	cmcl001Rec.setChdrcoy(srcalcpy.chdrChdrcoy.toString());
	cmcl001Rec.setCnttype(srcalcpy.cnttype.toString());
	cmcl001Rec.setCovcrtable(srcalcpy.crtable.toString().trim());
	cmcl001Rec.setCovcovrCoverage(srcalcpy.covrCoverage.toString().trim());
	
	callProgram(t5644rec.rfdsbrtine,cmcl001Rec);
	if(isEQ(cmcl001Rec.statuz, varcom.oK) || isEQ(cmcl001Rec.statuz, varcom.endp)) {
		sv.commclaw.set(new BigDecimal(cmcl001Rec.getCommclaw()));
	} else {
		syserrrec.statuz.set(cmcl001Rec.statuz);
		fatalError600();
	}
}
protected void read1810()
	{
		srcalcpy.effdate.set(sv.effdate);
		srcalcpy.type.set("F");
		if (isGT(covrsurIO.getInstprem(), ZERO)) {
			srcalcpy.singp.set(covrsurIO.getInstprem());
		}
		else {
			srcalcpy.singp.set(covrsurIO.getSingp());
		}
		/* if no surrender method found print zeros as surrender value*/
		if (isEQ(t6598rec.calcprog, SPACES)
		|| isEQ(wsaaValidStatus, "N")) {
			sv.estMatValue.set(ZERO);
			sv.hemv.set(ZERO);
			sv.actvalue.set(ZERO);
			sv.hactval.set(ZERO);
			sv.fieldType.set(SPACES);
			sv.shortds.set(SPACES);
			sv.fund.set(SPACES);
			sv.cnstcur.set(SPACES);
			srcalcpy.status.set(varcom.endp);
			/*     GO TO 1830-ADD-TO-SUBFILE.*/
			goTo(GotoLabel.exit1840);
		}
		/*IVE-797 RUL Product - Full Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		/*ILIFE-7548 start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())&& er.isExternalized(chdrsurIO.getCnttype().toString(), srcalcpy.crtable.toString())))  
		{
			srcalcpy.surrCalcMeth.set(t5687rec.svMethod);
			srcalcpy.cnttype.set(chdrsurIO.getCnttype());
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
			callProgramX(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		/*ILIFE-7548 end*/
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrsurIO);//VPMS call
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.endf.set("Y");
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
			/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(vpxsurcrec.statuz.equals(varcom.endp))
				srcalcpy.status.set(varcom.endp);
			/* ILIFE-3142 End*/
		}
		/*IVE-797 RUL Product - Full Surrender Calculation end*/
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isEQ(srcalcpy.status, varcom.oK)
		|| isEQ(srcalcpy.status, varcom.endp)) {
			if (isEQ(srcalcpy.currcode, SPACES)) {
				srcalcpy.currcode.set(chdrsurIO.getCntcurr());
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			callRounding6000();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			callRounding6000();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		if (isEQ(srcalcpy.type, "C")) {
			compute(srcalcpy.actualVal, 2).set(mult(srcalcpy.actualVal, -1));
		}
		/* Check the amount returned for being negative. In the case of    */
		/* SUM products this is possible and so set these values to zero.  */
		/* Note SUM products do not have to have the same PT & BT dates.   */
		checkT56112700();
		if (isNE(wsaaSumFlag, SPACES)) {
			if (isNE(sv.ptdate, sv.btdate)) {
				sv.ptdateErr.set(SPACES);
				sv.btdateErr.set(SPACES);
			}
			if (isLT(srcalcpy.actualVal, ZERO)) {
				srcalcpy.actualVal.set(ZERO);
			}
		}
		/*    IF SURC-ENDF  = 'Y'                                          */
		/*       GO TO 1840-EXIT.                                          */
		if (isEQ(srcalcpy.estimatedVal, ZERO)
		&& isEQ(srcalcpy.actualVal, ZERO)) {
			goTo(GotoLabel.exit1840);
		}
		sv.hcrtable.set(covrsurIO.getCrtable());
		if (isEQ(srcalcpy.currcode, SPACES)) {
			sv.cnstcur.set(chdrsurIO.getCntcurr());
			sv.hcnstcur.set(chdrsurIO.getCntcurr());
		}
		else {
			sv.cnstcur.set(srcalcpy.currcode);
			sv.hcnstcur.set(srcalcpy.currcode);
		}
		sv.htype.set(srcalcpy.type);
		sv.fieldType.set(srcalcpy.type);
		sv.estMatValue.set(srcalcpy.estimatedVal);
		sv.hemv.set(srcalcpy.estimatedVal);
		sv.actvalue.set(srcalcpy.actualVal);
		sv.hactval.set(srcalcpy.actualVal);
		/* 'IF SURC-ACTUAL-VAL' should also be part of the nested 'IF      */
		/*  SUMMARY-PART-PLAN'.                                            */
		if (summaryPartPlan.isTrue()) {
			if (isNE(srcalcpy.estimatedVal, ZERO)) {
				compute(sv.estMatValue, 2).set(div(srcalcpy.estimatedVal, chdrsurIO.getPolsum()));
				compute(sv.hemv, 2).set(div(srcalcpy.estimatedVal, chdrsurIO.getPolsum()));
			}
		}
		zrdecplrec.amountIn.set(sv.estMatValue);
		zrdecplrec.currency.set(srcalcpy.currcode);
		callRounding6000();
		sv.estMatValue.set(zrdecplrec.amountOut);
		sv.hemv.set(zrdecplrec.amountOut);
		if (summaryPartPlan.isTrue()) {
			if (isNE(srcalcpy.actualVal, ZERO)) {
				compute(sv.actvalue, 2).set(div(srcalcpy.actualVal, chdrsurIO.getPolsum()));
				compute(sv.hactval, 2).set(div(srcalcpy.actualVal, chdrsurIO.getPolsum()));
			}
		}
		zrdecplrec.amountIn.set(sv.actvalue);
		zrdecplrec.currency.set(srcalcpy.currcode);
		callRounding6000();
		sv.actvalue.set(zrdecplrec.amountOut);
		sv.hactval.set(zrdecplrec.amountOut);
		sv.fund.set(srcalcpy.fund);
		sv.shortds.set(srcalcpy.description);
		if (firstTime.isTrue()) {
			wsaaSwitch2.set(0);
			wsaaStoredCurrency.set(sv.cnstcur);
			wsaaEstimateTot.add(sv.estMatValue);
			if (isEQ(sv.fieldType, "C")) {
				compute(wsaaActualTot, 2).set(sub(wsaaActualTot, sv.actvalue));
			}
			else {
				wsaaActualTot.add(sv.actvalue);
			}
		}
		else {
			/*    IF SURC-CURRCODE            = WSAA-STORED-CURRENCY           */
			if (isEQ(sv.cnstcur, wsaaStoredCurrency)) {
				wsaaEstimateTot.add(sv.estMatValue);
				if (isEQ(sv.fieldType, "C")) {
					compute(wsaaActualTot, 2).set(sub(wsaaActualTot, sv.actvalue));
				}
				else {
					wsaaActualTot.add(sv.actvalue);
				}
			}
			else {
				wsaaEstimateTot.set(ZERO);
				wsaaActualTot.set(ZERO);
				wsaaCurrencySwitch.set(1);
			}
		}
		/*   IF S5026-SHORTDS         NOT = COVRSUR-CRTABLE       <A06843>*/
		/*       MOVE SPACES             TO S5026-RIIND           <A06843>*/
		/*   END-IF.                                              <A06843>*/
		/* Check the effective date of the surrender against the paid to   */
		/* date of the contract. If there are to be premiums paid or       */
		/* refunded then call the appropriate subroutine held on T5611.    */
		/* Note that if the item does not exist then the coverage does not */
		/* use the SUM calculation package & is hence not applicable....   */
		if (isNE(srcalcpy.effdate, srcalcpy.ptdate)
		&& isNE(wsaaSumFlag, SPACES)
		&& isNE(t5611rec.calcprog, SPACES)) {
			checkPremadj2600();
		}
	}

	/**
	* <pre>
	*  add record to subfile
	* </pre>
	*/
protected void addToSubfile1830()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5026", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void getTaxAmount1880()
	{
		start1880();
	}

protected void start1880()
	{
		wsaaTaxAmt.set(0);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr691);
		/* MOVE CHDRSUR-CNTTYPE        TO ITEM-ITEMITEM.        <LA2924>*/
		if (isEQ(sv.currcd, SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrsurIO.getCnttype());
			stringVariable1.addExpression(chdrsurIO.getCntcurr());
			stringVariable1.setStringInto(itemIO.getItemitem());
		}
		else {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(chdrsurIO.getCnttype());
			stringVariable2.addExpression(sv.currcd);
			stringVariable2.setStringInto(itemIO.getItemitem());
		}
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setParams(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr691);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <LA2924>*/
			if (isEQ(sv.currcd, SPACES)) {
				StringUtil stringVariable3 = new StringUtil();
				stringVariable3.addExpression("***");
				stringVariable3.addExpression(chdrsurIO.getCntcurr());
				stringVariable3.setStringInto(itemIO.getItemitem());
			}
			else {
				StringUtil stringVariable4 = new StringUtil();
				stringVariable4.addExpression("***");
				stringVariable4.addExpression(sv.currcd);
				stringVariable4.setStringInto(itemIO.getItemitem());
			}
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				return ;
			}
		}
		tr691rec.tr691Rec.set(itemIO.getGenarea());
		datcon3rec.intDate2.set(sv.effdate);
		datcon3rec.intDate1.set(sv.occdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isGTE(datcon3rec.freqFactor, tr691rec.tyearno)) {
			return ;
		}
		if (isNE(tr691rec.pcnt, 0)) {
			//MIBT-111
//			compute(wsaaTaxAmt, 3).setRounded(mult(wsaaActualTot, (div(tr691rec.pcnt, 100))));
			compute(wsaaTaxAmt, 3).setRounded(div(mult(wsaaActualTot,tr691rec.pcnt), 100));
		}
		if (isNE(tr691rec.flatrate, 0)) {
			wsaaTaxAmt.set(tr691rec.flatrate);
		}
		zrdecplrec.amountIn.set(wsaaTaxAmt);
		if (isEQ(sv.currcd, SPACES)) {
			zrdecplrec.currency.set(chdrsurIO.getCntcurr());
		}
		else {
			zrdecplrec.currency.set(sv.currcd);
		}
		callRounding6000();
		wsaaTaxAmt.set(zrdecplrec.amountOut);
	}

protected void getLoanDetails1900()
	{
		start1900();
	}

	/**
	* <pre>
	*  Get the details of all loans currently held against this       
	*  Contract. If this is not the first component within the        
	*  current Surrender transaction then need to read the SURD       
	*  to get details of all previous surrender records for this      
	*  transaction.                                                   
	*  If there are no records (there will be none for Unit Linked),  
	*  then call TOTLOAN to get the current loan value. If there are  
	*  records then sum up their values before calling TOTLOAN and    
	*  subtracting the total value from the LOAN VALUE returned from  
	*  TOTLOAN.                                                       
	*  Previous surrenders may have been done in a different          
	*  currency to the present one and hence, a check should be       
	*  made for this and a conversion done where necessary before     
	*  the accumulation is done. Note that for the initial screen     
	*  display, the currency will always be CHDR currency.            
	*  Note also that TOTLOAN always returns details in the CHDR      
	*  currency.                                                      
	* </pre>
	*/
protected void start1900()
	{
		wsaaLoanValue.set(0);
		if (surdpfMap != null && surdpfMap.containsKey(chdrsurIO.getChdrnum())) {
			for (Surdpf c : surdpfMap.get(chdrsurIO.getChdrnum())) {
				if (c.getLife().equals(ZERO)
						&& c.getCoverage().equals(ZERO)
						&& c.getRider().equals(ZERO)
						&& c.getPlnsfx() == 0
						&& c.getTranno() == chdrsurIO.getTranno().toInt()) {
					surdclmIO = c;
				if (isNE(surdclmIO.getCurrcd(), chdrsurIO.getCntcurr())) {
					readjustSurd1920();
					} else {
					wsaaActvalue.set(surdclmIO.getActvalue());
				}
				wsaaLoanValue.add(wsaaActvalue);
			}
		}
		}
//		surdclmIO.setDataArea(SPACES);
//		surdclmIO.setChdrcoy(chdrsurIO.getChdrcoy());
//		surdclmIO.setChdrnum(chdrsurIO.getChdrnum());
//		surdclmIO.setLife(ZERO);
//		surdclmIO.setCoverage(ZERO);
//		surdclmIO.setRider(ZERO);
//		surdclmIO.setPlanSuffix(ZERO);
//		surdclmIO.setTranno(chdrsurIO.getTranno());
//		surdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		surdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
//
//		surdclmIO.setFormat(formatsInner.surdclmrec);
//		surdclmIO.setStatuz(varcom.oK);
//		wsaaLoanValue.set(0);
		/*  Read all SURD records currently unprocessed.                   */
//		while ( !(isEQ(surdclmIO.getStatuz(), varcom.endp))) {
//			SmartFileCode.execute(appVars, surdclmIO);
//			if (isNE(surdclmIO.getStatuz(), varcom.oK)
//			&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
//				syserrrec.params.set(surdclmIO.getParams());
//				fatalError600();
//			}
//			if (isNE(chdrsurIO.getChdrnum(), surdclmIO.getChdrnum())
//			|| isNE(chdrsurIO.getChdrcoy(), surdclmIO.getChdrcoy())
//			|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
//				surdclmIO.setStatuz(varcom.endp);
//			}
//			else {
//				if (isNE(surdclmIO.getCurrcd(), chdrsurIO.getCntcurr())) {
//					readjustSurd1920();
//				}
//				else {
//					wsaaActvalue.set(surdclmIO.getActvalue());
//				}
//				wsaaLoanValue.add(wsaaActvalue);
//			}
//			surdclmIO.setFunction(varcom.nextr);
//		}
		
		/*  Apply the adjustment for any previous surrendered policies     */
		/*  where this is part of a multipolicy surrender.                 */
		if (!firstTimeThrough.isTrue()) {
			wsaaLoanValue.add(surhclmIO.getOtheradjst());
		}
		/*  Read TOTLOAN to get value of loans for this contract.          */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrsurIO.getChdrcoy());
		totloanrec.chdrnum.set(chdrsurIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.function.set("LOAN");
		totloanrec.language.set(wsspcomn.language);
		/* CALL 'TOTLOAN' USING TOTL-TOTLOAN-REC.                       */
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal, totloanrec.interest));
		if (isLT(wsaaLoanValue, wsaaHeldCurrLoans)) {
			wsaaHeldCurrLoans.subtract(wsaaLoanValue);
		}
		else {
			wsaaHeldCurrLoans.set(0);
		}
		sv.policyloan.set(wsaaHeldCurrLoans);
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrsurIO.getChdrcoy());
		totloanrec.chdrnum.set(chdrsurIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.function.set("CASH");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(sv.zrcshamt, 2).set(add(totloanrec.principal, totloanrec.interest));
		/* Change the sign to reflect the Client's context.             */
		if (isLT(sv.zrcshamt, 0)) {
			compute(sv.zrcshamt, 2).set(mult(sv.zrcshamt, (-1)));
		}
	}

protected void readjustSurd1920()
	{
		start1920();
	}

protected void start1920()
	{
		/*  Convert the SURD into the CHDR currency.                       */
		conlinkrec.function.set("SURR");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(surdclmIO.getCurrcd());
		/* MOVE VRCM-MAX-DATE          TO CLNK-CASHDATE.        <LA4958>*/
		conlinkrec.cashdate.set(sv.effdate);
		conlinkrec.currOut.set(chdrsurIO.getCntcurr());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(surdclmIO.getActvalue());
		conlinkrec.company.set(chdrsurIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		wsaaActvalue.set(conlinkrec.amountOut);
	}

protected void validateStatusesPp1950()
	{
		start1950();
	}

	/**
	* <pre>
	*  Read T5679 to establish whether this component has a           
	*  valid status for this transaction.                             
	* </pre>
	*/
protected void start1950()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			riskStatusCheckPp1960();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				premStatusCheckPp1970();
			}
		}
	}

protected void riskStatusCheckPp1960()
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrsurIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrsurIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void premStatusCheckPp1970()
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrsurIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrsurIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*1980-READ-RACD SECTION.                                  <A06843>
	**************************                                <A06843>
	*                                                         <A06843>
	*1971-REASSURANCE.                                        <A06843>
	*                                                         <A06843>
	*    MOVE SPACES                 TO RACDMJA-PARAMS.       <A06843>
	*                                                         <A06843>
	*    MOVE WSAA-RACD-CHDRCOY      TO RACDMJA-CHDRCOY.      <A06843>
	*    MOVE WSAA-RACD-CHDRNUM      TO RACDMJA-CHDRNUM.      <A06843>
	*    MOVE WSAA-RACD-LIFE         TO RACDMJA-LIFE.         <A06843>
	*    MOVE WSAA-RACD-COVERAGE     TO RACDMJA-COVERAGE.     <A06843>
	*    MOVE WSAA-RACD-RIDER        TO RACDMJA-RIDER.        <A06843>
	*    MOVE ZEROES                 TO RACDMJA-PLAN-SUFFIX.  <A06843>
	*    MOVE 99                     TO RACDMJA-SEQNO.        <A06843>
	*                                                         <A06843>
	*    MOVE 'RACDMJAREC'           TO RACDMJA-FORMAT.       <A06843>
	*    MOVE BEGN                   TO RACDMJA-FUNCTION.     <A06843>
	*                                                         <A06843>
	*    CALL 'RACDMJAIO'            USING RACDMJA-PARAMS.    <A06843>
	*                                                         <A06843>
	*    IF RACDMJA-STATUZ        NOT = O-K AND NOT = 'ENDP'  <A06843>
	*       MOVE RACDMJA-PARAMS      TO SYSR-PARAMS           <A06843>
	*       PERFORM 600-FATAL-ERROR                           <A06843>
	*    END-IF.                                              <A06843>
	*                                                         <A06843>
	*    IF RACDMJA-CHDRCOY       NOT = WSAA-RACD-CHDRCOY OR  <A06843>
	*       RACDMJA-CHDRNUM       NOT = WSAA-RACD-CHDRNUM OR  <A06843>
	*       RACDMJA-LIFE          NOT = WSAA-RACD-LIFE OR     <A06843>
	*       RACDMJA-COVERAGE      NOT = WSAA-RACD-COVERAGE OR <A06843>
	*       RACDMJA-RIDER         NOT = WSAA-RACD-RIDER OR    <A06843>
	*       RACDMJA-STATUZ            = ENDP                  <A06843>
	*         MOVE ENDP              TO RACDMJA-STATUZ        <A06843>
	*    END-IF.                                              <A06843>
	*                                                         <A06843>
	*1989-EXIT.                                               <A06843>
	*     EXIT.                                               <A06843>
	* </pre>
	*/
protected void getPolicyDebt1990()
	{
		start1990();
		read1991();
	}

protected void start1990()
	{
		sv.tdbtamt.set(ZERO);
		tpoldbtIO.setRecKeyData(SPACES);
		tpoldbtIO.setChdrcoy(chdrsurIO.getChdrcoy());
		tpoldbtIO.setChdrnum(chdrsurIO.getChdrnum());
		tpoldbtIO.setTranno(ZERO);
		tpoldbtIO.setFormat(formatsInner.tpoldbtrec);
		tpoldbtIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		tpoldbtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		tpoldbtIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

	}

protected void read1991()
	{
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
		&& isNE(tpoldbtIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			fatalError600();
		}
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
		|| isNE(tpoldbtIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(tpoldbtIO.getChdrnum(), chdrsurIO.getChdrnum())) {
			return ;
		}
		sv.tdbtamt.add(tpoldbtIO.getTdbtamt());
		tpoldbtIO.setFunction(varcom.nextr);
		read1991();
		return ;
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsaaEstimateTot, ZERO)) {
			wsspcomn.edterror.set("Y");
		}
		/*        MOVE 'Y' TO WSSP-EDTERROR         S5026-CURRCD-OUT(PR)   */
		/*                    S5026-EFFDATE-OUT(PR)                        */
		/*        MOVE H377 TO SCRN-ERROR-CODE.                            */
		/*  If this is not the first time through then protect the         */
		/*  Currency code, Adjustment amount, Adjustment Code and          */
		/*  Adjustment reason fields.                                      */
		if (!firstTimeThrough.isTrue()) {
			sv.currcdOut[varcom.pr.toInt()].set("Y");
		}
		/*  If First time the Currency code has not been entered and       */
		/*  this is a multi-currency Contract the error message that       */
		/*  Currency must be entered before calculations begin.            */
		if (firstTimeThrough.isTrue()
		&& isEQ(sv.currcd, SPACES)
		&& detailsDifferent.isTrue()) {
			sv.currcdErr.set(errorsInner.h960);
			wsspcomn.edterror.set("Y");
		}
		/*  Change the values back as they has been changed in             */
		/*  5100-CHECK-CALC-TAX sction                                     */
		scrnparams.function.set("INIT ");
		scrnparams.statuz.set("****");
		scrnparams.subfileRrn.set(1);
		
		// ILIFE-5980 Start
		if(isEQ(wsaaTreditionslFlag, "Y")){
			sv.rundteOut[varcom.pr.toInt()].set("Y");
			sv.rsuninOut[varcom.pr.toInt()].set("Y");
		}// ILIFE-5980 END
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateScreen2010();
					validateSelectionFields2070();
				case checkForErrors2050: 
					checkForErrors2050();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5026IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5026-DATA-AREA                         */
		/*                         S5026-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		/*  Pull in any errors from the 1000 section.                      */
		sv.errorIndicators.set(wsaaErrorIndicators);
		wsspcomn.edterror.set(varcom.oK);
		
		if(susur002Permission)
		{
		if(isEQ(sv.payrnum, SPACES)){
			sv.payrnum.set(chdrlnbIO.getCownnum());
		}
		if (isNE(chdrlnbIO.getCownnum(), SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrlnbIO.getCownpfx());
			stringVariable1.addExpression(chdrlnbIO.getCowncoy());
			stringVariable1.addExpression(sv.payrnum);
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(sv.payrnum);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
	
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))
		&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.payrnumErr.set(errorsInner.e335);
			wsspcomn.edterror.set("Y");
			sv.payorname.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
		a1000GetPayorname();
		sv.payorname.set(namadrsrec.name);
		
		if (isEQ(sv.reqntype, "4")) {
			sv.crdtcrd.set(SPACES);
			sv.bankacckeyOut[varcom.nd.toInt()].set("N");
			sv.crdtcrdOut[varcom.nd.toInt()].set("Y");
			if (isEQ(sv.bankacckey, SPACES)) {
				sv.bankacckeyErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				checkForErrors2050();
			}
			
		}
		
		if (isEQ(sv.reqntype, "C")) {
			sv.bankacckey.set(SPACES);
			sv.crdtcrdOut[varcom.nd.toInt()].set("N");
			sv.bankacckeyOut[varcom.nd.toInt()].set("Y");
			if (isEQ(sv.crdtcrd, SPACES)) {
				sv.crdtcrdErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				checkForErrors2050();
			}
		}
		
		if (isNE(sv.reqntype, '4') && isNE(sv.reqntype, 'C') ) {
			sv.bankacckeyOut[varcom.nd.toInt()].set("Y");
			sv.crdtcrdOut[varcom.nd.toInt()].set("Y");
			sv.bankacckey.set(SPACES);
			sv.crdtcrd.set(SPACES);
			
		}
		
		if (isEQ(sv.reqntype, SPACES) ) {
			sv.reqntypeErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			checkForErrors2050();
			
		}
		sv.bankkey.set(SPACES);

	}
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, "CALC")
		&& isEQ(sv.currcd, SPACES)
		&& !detailsDifferent.isTrue()) {
			sv.currcd.set(chdrsurIO.getCntcurr());
		}
		/*  If the Currency code has not been entered and this is          */
		/*  a multi-currency Contract the error message that               */
		/*  Currency must be entered before calculations begin.            */
		if (isEQ(sv.currcd, SPACES)
		&& detailsDifferent.isTrue()) {
			sv.currcdErr.set(errorsInner.h960);
			wsspcomn.edterror.set("Y");
//			goTo(GotoLabel.checkForErrors2050);
			checkForErrors2050();
		}
		if (isEQ(sv.currcd, SPACES)) {
			sv.currcd.set(chdrsurIO.getCntcurr());
		}
		/* Read table T3000 to insure that the currency code entered       */
		/* has conversion rates. If we do not do this, and there are       */
		/* no rates XCVRT will fall over with a System Error.              */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t3000);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(sv.currcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.currcdErr.set(errorsInner.h962);
			/*        MOVE 'Y'                TO WSSP-EDTERROR.                */
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.ptdate, sv.btdate)) {
			sv.ptdateErr.set(errorsInner.g008);
			sv.btdateErr.set(errorsInner.g008);
			wsspcomn.edterror.set("Y");
		}
		/*    IF S5026-CURRCD                 = SPACES                     */
		/*        MOVE CHDRSUR-CNTCURR        TO S5026-CURRCD.             */
		/*  If currency has changed, don't update stored currency          */
		/*  until after currency conversion.                               */
		/*  Additionally, subtract the policy loan amount rather than      */
		/*  add it to the total.                                           */
		if (isNE(sv.currcd, wsaaStoredCurrency)) {
			/*    OR SCRN-STATUZ = 'CALC'                                      */
			/*    OR SCRN-STATUZ = 'CALC' OR NOT DETAILS-SAME-CURRENCY*/
			/*        MOVE S5026-CURRCD TO WSAA-STORED-CURRENCY                */
			wsaaCurrencySwitch.set(0);
			wsspcomn.edterror.set("Y");
			readjustCurrencies2100();
			wsaaStoredCurrency.set(sv.currcd);
			sv.estimateTotalValue.set(wsaaEstimateTot);
			/*      COMPUTE S5026-CLAMANT = WSAA-ACTUAL-TOT +               */
			/*            S5026-OTHERADJST +                                   */
			/*         S5026-OTHERADJST -                             <CAS1.*/
			/*         WSAA-PENALTY-TOT -                             <CAS1.*/
			/*         S5026-POLICYLOAN.                                    */
			if(susur002Permission)
			{
				
				compute(sv.clamant, 2).set(add(sub(sub(sub(sub(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), wsaaPenaltyTot), sv.tdbtamt), sv.taxamt), sv.policyloan),sv.sacscurbal));
		
			}
			else if(susur013Permission)
			{
				compute(sv.clamant, 2).set(add(add(sub(sub(sub(sub(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), wsaaPenaltyTot), sv.tdbtamt), sv.taxamt), sv.policyloan), sv.suspenseamt), sv.unexpiredprm));
			}
			else
			{
			compute(sv.clamant, 2).set(sub(sub(sub(sub(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), wsaaPenaltyTot), sv.tdbtamt), sv.taxamt), sv.policyloan));
			}
		}
		/*  If the total amount < 0 then display on the left side in SV.   */
		wsaaPenaltyTot.set(ZERO);
		
		if(susur002Permission || susur013Permission)
		{
			
			if (isEQ(sv.clamant, 0)
					&& (setPrecision(wsaaEstimateTot, 2)
					&& isGT((mult(sv.clamant, -1)), wsaaEstimateTot))) {
						/*        MOVE 0                  TO S5026-CLAMANT           <P010>*/
						sv.clamantErr.set(errorsInner.hl36);  //ILIFE-6578
						sv.netOfSvDebt.set(sv.clamant);
					}
		}
		else
		{
			if (isLT(sv.clamant, ZERO)
					&& (setPrecision(wsaaEstimateTot, 2)
					&& isGT((mult(sv.clamant, -1)), wsaaEstimateTot))) {
						/*        MOVE 0                  TO S5026-CLAMANT           <P010>*/
						sv.clamantErr.set(errorsInner.hl36);  //ILIFE-6578
						sv.netOfSvDebt.set(sv.clamant);
					}
		}
		
		/*       effective date must be entered - should be not less than*/
		/*       the ccd and not less than date of death.*/
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)) {
			sv.effdateErr.set(errorsInner.e186);
		}
		else {
			if (isLT(sv.effdate, chdrsurIO.getOccdate())) {
				sv.effdateErr.set(errorsInner.f616);
			}
		}
		if (isNE(sv.reasoncd, SPACES)) {
			if (isEQ(sv.resndesc, SPACES)) {
				scrnparams.statuz.set("CALC");
				descIO.setDesctabl(tablesInner.t5500);
				descIO.setDescitem(sv.reasoncd);
				findDesc1100();
				if (isEQ(descIO.getStatuz(), varcom.oK)) {
					sv.resndesc.set(descIO.getLongdesc());
				}
				else {
					sv.resndesc.fill("?");
				}
			}
		}
		/*  Subtract rather than add policy loan amount.*/
		/*      COMPUTE S5026-CLAMANT = WSAA-ACTUAL-TOT +               */
		/*            S5026-OTHERADJST +                                   */
		/*         S5026-OTHERADJST -                             <CAS1.*/
		/*         S5026-POLICYLOAN.                                    */
		if(susur002Permission)
		{
			
			compute(sv.clamant, 2).set(add(sub(sub(sub(sub(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), wsaaPenaltyTot), sv.tdbtamt), sv.taxamt), sv.policyloan),sv.sacscurbal));
	
		}
		else if(susur013Permission)
		{
			compute(sv.clamant, 2).set(add(add(sub(sub(sub(sub(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), wsaaPenaltyTot), sv.tdbtamt), sv.taxamt), sv.policyloan), sv.suspenseamt), sv.unexpiredprm));
		}
		else
		{
		compute(sv.clamant, 2).set(sub(sub(sub(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), sv.tdbtamt), sv.taxamt), sv.policyloan));
		}
		/*  Recalculate the Net of Surr Val Debt in the new Currency       */
		/* COMPUTE S5026-NET-OF-SV-DEBT = S5026-POLICYLOAN -            */
		/*                                WSAA-ACTUAL-TOT -             */
		/*                                S5026-OTHERADJST.             */
		if(susur002Permission)
		{
			compute(wsaaNetSV, 2).set(sub(sub(sub(sv.policyloan, wsaaActualTot), sv.zrcshamt), sv.otheradjst));
			if (isLT(wsaaNetSV,ZERO)) {
				compute(wsaaNetSV, 2).set(mult(wsaaNetSV,-1));
			}
			compute(sv.netOfSvDebt, 2).set(add(wsaaNetSV,sv.sacscurbal));
		}
		else
		{
			compute(sv.netOfSvDebt, 2).set(sub(sub(sub(sv.policyloan, wsaaActualTot), sv.zrcshamt), sv.otheradjst));
		}
		
		
		
		/*if (isLT(sv.netOfSvDebt, 0)) {
			sv.netOfSvDebt.set(0);
		}*/
		
		if(susur002Permission)
		{
			if (isEQ(sv.clamant, 0)
					&& (setPrecision(wsaaEstimateTot, 2)
					&& isGT((mult(sv.clamant, -1)), wsaaEstimateTot))) {
						//sv.clamantErr.set(errorsInner.p296);  
						sv.clamantErr.set(errorsInner.hl36);   //ILIFE-6578
					}
		}
		
		else
		{
			if (isLT(sv.clamant, 0)
					&& (setPrecision(wsaaEstimateTot, 2)
					&& isGT((mult(sv.clamant, -1)), wsaaEstimateTot))) {
						//sv.clamantErr.set(errorsInner.p296);  
						sv.clamantErr.set(errorsInner.hl36);   //ILIFE-6578
					}
		}
		
		
		
		
		
		
		
		if (isEQ(wsaaNoPrice, "Y")) {
			sv.effdateErr.set(errorsInner.g094);
		}
		if (isEQ(wsaaUnprocUnits, "Y")) {
			sv.effdateErr.set(errorsInner.h355);
		}
		
		
		// ILIFE-5452 Start
		if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("Y")){
			if (isEQ(sv.reserveUnitsDate, 99999999) || isEQ(sv.reserveUnitsDate, SPACES) || isEQ(sv.reserveUnitsDate, 00000000)) { 
				sv.rundteErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			
			if (isLT(sv.reserveUnitsDate, chdrsurIO.getCcdate())) {
				sv.rundteErr.set(errorsInner.h359);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			if (isGT(sv.reserveUnitsDate, wsaaBusinessDate)) {
				sv.rundteErr.set(errorsInner.rlcc);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		
		if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("N") 
				&& !sv.reserveUnitsDate.equals(varcom.vrcmMaxDate)){
			sv.rundteErr.set(errorsInner.g099);
			sv.rsuninErr.set(errorsInner.g099);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		
		if (isEQ(sv.reserveUnitsInd, SPACES) && isNE(sv.reserveUnitsDate, varcom.vrcmMaxDate)) {
			if(isNE(wsaaTreditionslFlag, "Y")){// ILIFE-5980
				sv.rsuninErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		// ILIFE-5452 end  
	
		if(susur002Permission)
			{
			 checkLimit();
			} 
		
		if(susur002Permission)
  		{
		 
		wsaaBatckey.set(wsspcomn.batchkey);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5645");
		itempf.setItemitem(wsaaProg.toString());
	    itempf.setItemseq("  ");
		itempf = itemDao.getItemRecordByItemkey(itempf);
		
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5645").concat(wsaaProg.toString()));
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		    itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl("T3695");
			itempf.setItemitem(t5645rec.sacstype01.toString().trim());
			itempf = itemDao.getItemRecordByItemkey(itempf);
			
			if (itempf==null) {
				syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T3695").concat(t5645rec.sacstype01.toString()));
				fatalError600();
			}
			t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			
			payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString());
			
			if (payrpf == null) {
				syserrrec.statuz.set("MRNF");
				syserrrec.params.set("2".concat(chdrsurIO.getChdrnum().toString()));
				fatalError600();
			}
			
			acblpf = acblpfDAO.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode04.toString(), chdrsurIO.getChdrnum().toString(), payrpf.getBillcurr(), t5645rec.sacstype04.toString());
			
			if (acblpf == null) {
				
			}
			else {
				if (isLT(acblpf.getSacscurbal(),ZERO)) {
					compute(amountap, 2).set(mult(acblpf.getSacscurbal(),-1));
				
				if(isGT(amountap,ZERO))
				{
					sv.rsuninErr.set(errorsInner.rrfm);
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.exit2090);
				}
				}		
				}
				
			if (isNE(sv.bankacckey,SPACES) || isNE(sv.crdtcrd,SPACES)) {
			clbapf = new Clbapf();
			clbapf.setClntpfx("CN");
			clbapf.setClntcoy(wsspcomn.fsuco.toString().trim());
			clbapf.setClntnum(sv.payrnum.toString().trim());
			List<Clbapf> clbalist = clbapfDAO.searchClbapfDatabyObject(clbapf);
			if (clbalist != null && clbalist.size() > 0) {
				for (Clbapf clbaitem : clbalist) {
					sv.bankkey.set(clbaitem.getBankkey());
					
				}
			}
			}
			
			if (isNE(sv.bankacckey,SPACES)) {
				bankBranchDesc1120();
			}
			
			if (isNE(sv.crdtcrd,SPACES)) {
				bankBranchDesc1120();
			}
			
			if (isNE(sv.errorIndicators, SPACES)) {
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
  		}
		
		
	
	}

protected void checkLimit()

{
	uwlvIO.setParams(SPACES);
	uwlvIO.setUserid(wsspcomn.userid);
	uwlvIO.setCompany(wsspcomn.company);
	uwlvIO.setFunction(varcom.readr);
	uwlvIO.setFormat(formatsInner.uwlvrec);
	SmartFileCode.execute(appVars, uwlvIO);
	if (isNE(uwlvIO.getStatuz(),varcom.oK)
	&& isNE(uwlvIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(uwlvIO.getParams());
		syserrrec.statuz.set(uwlvIO.getStatuz());
		fatalError600();
	}
	
	if (isEQ(uwlvIO.getStatuz(), varcom.mrnf)) {
		sv.clamantErr.set(errorsInner.rf05);
		wsspcomn.edterror.set("Y");
		return;
	}
	
	
	else {	
		
		if((null!=uwlvIO.getPoslevel()) && isEQ(uwlvIO.getPoslevel(),SPACES)) {
			sv.clamantErr.set(errorsInner.rf05);
			wsspcomn.edterror.set("Y");
			return;
		}
		 readTd5h6();
		 
		 itempfList=itempfDAO.getAllItemitem("IT",chdrsurIO.getChdrcoy().toString(), t6640, covrsurIO.getCrtable().toString()); 
			
		 if (itempfList.size() >= 1 )
		 {
		
		 if(isGT(sv.clamant,td5h6rec.fullsurauthlim))
		   {
		    	sv.clamantErr.set(errorsInner.rrfn);
		    	wsspcomn.edterror.set("Y");
				return;
		   }
		}
		 else
		 {
			 if(isGT(sv.estimateTotalValue,td5h6rec.fullsurauthlim))
			   {
			    	sv.estimtotalErr.set(errorsInner.rrfn);
			    	wsspcomn.edterror.set("Y");
					return;
			   }
		 }
		
	}
	
	
	usrdIO.setParams(SPACES);
	usrdIO.setCompany(wsspcomn.company);
	usrdIO.setUserid(wsspcomn.userid);
	usrdIO.setFormat(usrdrec);
	usrdIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, usrdIO);
	if (isNE(usrdIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(usrdIO.getParams());
		syserrrec.statuz.set(usrdIO.getStatuz());
		fatalError600();
	}
	
	}
	
	
protected void setMsgPoup()
	{
		wsaaLanguage.set(wsspcomn.language);
		wsaaMsgid.set("RRFO");
		msgboxrec.cpfmsg.set(wsaaPaxmsg);
		msgboxrec.insert.set(SPACES);
		msgboxrec.result.set(SPACES);
		msgboxrec.reply.set("X");
		msgbocrecScreen.cpfmsg.set(msgboxrec.cpfmsg);
		msgbocrecScreen.insert.set(msgboxrec.insert);
		msgbocrecScreen.reply.set(msgboxrec.reply);
		msgbocrecScreen.result.set(msgboxrec.result);
		proScreenPopup.set(wsaaProg);
		preFunction.set(scrnparams.function);
		preStatus.set(scrnparams.statuz);
		scrnparams.function.set(Varcom.popup);
		processScreen("S5026", sv);
		if(isNE(scrnparams.statuz, varcom.oK) 
		&& isNE(scrnparams.statuz, Varcom.ppup)){
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	scrnparams.function.set(preFunction);
	scrnparams.statuz.set(preStatus);
	sv.msgPopup.set(msgTextScreen);
	}

protected void resetMsgData()
	{
		sv.msgDisplay.set(ZERO);
		sv.msgresult.set(SPACE);
		sv.msgPopup.set(SPACE);
	}



protected void readTd5h6() 
	{
	itempf = new Itempf();
	itempf.setItempfx(smtpfxcpy.item.toString());
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(td5h6);/* IJTI-1523 */
	itempf.setItemitem(uwlvIO.getPoslevel().toString());
	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf== null ) {
		syserrrec.params.set(itempf);
		fatalError600();
	}
	else {
		td5h6rec.td5h6Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	}
	
	
	
protected void validateSelectionFields2070()
	{
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaMaturityFlag, "Y")) {
			wsspcomn.edterror.set("Y");
		}
		
		if(susur002Permission)
		{
		if (isNE(scrnparams.statuz, varcom.calc )&&
				isNE(scrnparams.statuz,varcom.swch)) {
			
				if(isEQ(sv.msgPopup, SPACE)){
					sv.msgresult.set(SPACE);
					setMsgPoup();
					wsspcomn.edterror.set("Y");
					goTo(GotoLabel.checkForErrors2050);
				}else {
					wsaaResult.set(varcom.oK);
					if (isNE(sv.msgresult, "Y")) {
						varcom.vrcmInit.set(SPACES);
						resetMsgData();
						wsspcomn.edterror.set("Y");
						goTo(GotoLabel.checkForErrors2050);
					}
					resetMsgData();
				}
		}
		
		
	}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		
		
	}


protected void readjustCurrencies2100()
	{
		g02150();
	}

protected void g02150()
	{
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5026", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile2200();
		}
		
		/*IVE-705 Surrender Tax Calc Started*/		
		//getTaxAmount1880();
		//sv.taxamt.set(wsaaTaxAmt);
		/*ILIFE-2397 Start */
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("SURTAX") && er.isExternalized(sv.cnttype.toString(), covrsurIO.crtable.toString())))  //ILIFE-4833
		{
			getTaxAmount1880();
			sv.taxamt.set(wsaaTaxAmt);
		}
		else
		{
			SurtaxRec surtaxRec = new SurtaxRec();	
			surtaxRec.cnttype.set(sv.cnttype);
			surtaxRec.cntcurr.set(sv.cnstcur);
			surtaxRec.effectiveDate.set(sv.effdate);
			surtaxRec.occDate.set(sv.effdate);
			surtaxRec.actualAmount.set(wsaaActualTot);						
			callProgram("SURTAX", surtaxRec.surtaxRec);			
			sv.taxamt.set(surtaxRec.taxAmount);
			sv.htype.set(surtaxRec.amountType);
		}
		/*ILIFE-2397 End */	
			/*IVE-705 Surrender Tax Calc end*/
		
		/*  Calculate tax                                     .            */
		checkCalcTax5100();
		convertPolicyloan2400();
		if (isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readSubfile2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go2250();
				case updateErrorIndicators2270: 
					updateErrorIndicators2270();
					readNextRecord2280();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go2250()
	{
		/*convert the estimated value if it is non zero.*/
		if (isEQ(sv.currcd, sv.hcnstcur)
		|| isEQ(sv.currcd, SPACES)) {
			/*    IF S5026-CURRCD   = S5026-CNSTCUR                            */
			sv.cnstcur.set(sv.hcnstcur);
			sv.estMatValue.set(sv.hemv);
			wsaaEstimateTot.add(sv.estMatValue);
			sv.actvalue.set(sv.hactval);
			wsaaPenaltyTot.set(ZERO);
			if (isEQ(sv.fieldType, "C")) {
				compute(wsaaActualTot, 2).set(sub(wsaaActualTot, sv.actvalue));
			}
			else {
				wsaaActualTot.add(sv.actvalue);
			}
			//goTo(GotoLabel.updateErrorIndicators2270);
			updateErrorIndicators2270();
			readNextRecord2280();
		}
		/*convert the estimated value if it is non zero.*/
		if (isNE(sv.hemv, ZERO)) {
			conlinkrec.amountIn.set(sv.hemv);
			conlinkrec.function.set("CVRT");
			callXcvrt2300();
			sv.estMatValue.set(conlinkrec.amountOut);
			wsaaEstimateTot.add(conlinkrec.amountOut);
		}
		/*convert the actual value if it is non zero.*/
		if (isNE(sv.hactval, ZERO)) {
			conlinkrec.amountIn.set(sv.hactval);
			if (isEQ(sv.fieldType, "C")) {
				conlinkrec.function.set("SURR");
			}
			else {
				conlinkrec.function.set("CVRT");
			}
			callXcvrt2300();
			sv.actvalue.set(conlinkrec.amountOut);
			if (isEQ(sv.fieldType, "C")) {
				compute(wsaaActualTot, 2).set(sub(wsaaActualTot, conlinkrec.amountOut));
			}
			else {
				wsaaActualTot.add(conlinkrec.amountOut);
			}
		}
		sv.cnstcur.set(conlinkrec.currOut);
	}

protected void updateErrorIndicators2270()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5026", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextRecord2280()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5026", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callXcvrt2300()
	{
		g02350();
	}

protected void g02350()
	{
		/* Read table T3000 to insure that the currency code entered       */
		/* has conversion rates. If we do not do this, and there are       */
		/* no rates XCVRT will fall over with a System Error.              */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t3000);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(sv.currcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.currcdErr.set(errorsInner.h962);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/* MOVE 'CVRT'                 TO CLNK-FUNCTION.                */
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(sv.hcnstcur);
		/* MOVE VRCM-MAX-DATE          TO CLNK-CASHDATE.                */
		conlinkrec.cashdate.set(sv.effdate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrsurIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, "****")) {
			/*        MOVE CLNK-STATUZ        TO SCRN-ERROR-CODE               */
			/*        MOVE 'Y'                TO WSSP-EDTERROR.                */
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(sv.currcd);
		callRounding6000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void convertPolicyloan2400()
	{
		start2400();
	}

protected void start2400()
	{
		/* Read table T3000 to insure that the currency code entered       */
		/* has conversion rates. If we do not do this, and there are       */
		/* no rates XCVRT will fall over with a System Error.              */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t3000);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(sv.currcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.currcdErr.set(errorsInner.h962);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2400-EXIT.                                         */
			return ;
		}
		/*  Convert the Policy Loan into the requested Currency.           */
		/*  Note, the LOAN value as returned from TOTLOAN will always*/
		/*  be in the CHDR currency. Therefore always use that value*/
		/*  and that currency to convert from.*/
		/* Repayment of loan, so use function 'SURR', just as in the case  */
		/* of penalty conversion.                                          */
		/* MOVE 'CVRT'                 TO CLNK-FUNCTION.           <CAS1*/
		conlinkrec.function.set("SURR");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(chdrsurIO.getCntcurr());
		/* MOVE VRCM-MAX-DATE          TO CLNK-CASHDATE.        <LA4958>*/
		conlinkrec.cashdate.set(sv.effdate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(wsaaHeldCurrLoans);
		conlinkrec.company.set(chdrsurIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		conlinkrec.currOut.set(sv.currcd);
		callRounding6000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		sv.policyloan.set(conlinkrec.amountOut);
	}

protected void checkUtrns2500()
	{
		setupUtrns2510();
		nextUtrn2550();
	}

	/**
	* <pre>
	*    On a full surrender request, check that the contract         
	*    has no unprocessed UTRNs pending.                            
	* </pre>
	*/
protected void setupUtrns2510()
	{
		utrnIO.setDataArea(SPACES);
		utrnIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		utrnIO.setChdrcoy(wsspcomn.company);
		utrnIO.setChdrnum(chdrsurIO.getChdrnum());
		utrnIO.setPlanSuffix(0);
		utrnIO.setTranno(0);
	}

protected void nextUtrn2550()
	{
		SmartFileCode.execute(appVars, utrnIO);
		if (isEQ(utrnIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError600();
		}
		if (isNE(utrnIO.getChdrcoy(), wsspcomn.company)
		|| isNE(utrnIO.getChdrnum(), chdrsurIO.getChdrnum())) {
			return ;
		}
		else {
			if (isEQ(utrnIO.getFeedbackInd(), "Y")) {
				utrnIO.setFunction(varcom.nextr);
				nextUtrn2550();
				return ;
			}
			else {
				scrnparams.errorCode.set(errorsInner.h355);
				wsaaUnprocUnits = "Y";
			}
		}
	}

protected void checkHitrs2550()
	{
		setupHitrs2551();
	}

protected void setupHitrs2551()
	{
		/*    On a full surrender request, check that the contract         */
		/*    has no unprocessed HITRs pending.                            */
		hitrrnlIO.setParams(SPACES);
		hitrrnlIO.setChdrcoy(wsspcomn.company);
		hitrrnlIO.setChdrnum(chdrsurIO.getChdrnum());
		hitrrnlIO.setFormat(formatsInner.hitrrnlrec);
		hitrrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrrnlIO);
		if (isNE(hitrrnlIO.getStatuz(), varcom.oK)
		&& isNE(hitrrnlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hitrrnlIO.getParams());
			fatalError600();
		}
		if (isEQ(hitrrnlIO.getStatuz(), varcom.oK)) {
			scrnparams.errorCode.set(errorsInner.hl08);
		}
	}

protected void checkPremadj2600()
	{
		start2610();
	}

protected void start2610()
	{
		/* Call the surrender routine for premium adjustments. Note that   */
		/* for the moment this uses the same copy-book as the surrender    */
		/* routine, which is stored and then re-instated .........         */
		wsaaSurrenderRecStore.set(srcalcpy.surrenderRec);
		/* MOVE CHDRSUR-BILLFREQ         TO WSAA-BILLFREQ.      <A05691>*/
		/* If this is a Single Premium Component then use a Billing        */
		/* frequency of '00'.                                              */
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(payrIO.getBillfreq());
		}
		srcalcpy.estimatedVal.set(covrsurIO.getInstprem());
		srcalcpy.actualVal.set(ZERO);
		callProgram(t5611rec.calcprog, srcalcpy.surrenderRec);
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		else {
			if (isNE(srcalcpy.status, varcom.mrnf) && isNE(srcalcpy.status, varcom.oK)
			&& isNE(srcalcpy.status, varcom.endp)) {
				syserrrec.params.set(srcalcpy.surrenderRec);
				syserrrec.statuz.set(srcalcpy.status);
				fatalError600();
			}
		}
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		zrdecplrec.currency.set(sv.cnstcur);
		callRounding6000();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		/* Note adjustments are subtracted from the total.                 */
		/* The actual value here is the premium adjustment returned...     */
		sv.otheradjst.add(srcalcpy.actualVal);
		srcalcpy.surrenderRec.set(wsaaSurrenderRecStore);
	}

protected void checkT56112700()
	{
		start2710();
	}

protected void start2710()
	{
		/* Read the Coverage Surrender/Paid-Up/Bonus parameter table.      */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(tablesInner.t5611);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		wsaaT5611Crtable.set(srcalcpy.crtable);
		wsaaT5611Pstatcode.set(srcalcpy.pstatcode);
		itdmIO.setItemitem(wsaaT5611Item);
		itdmIO.setItmfrm(srcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			if (isNE(itdmIO.getStatuz(), varcom.endp)
			&& isEQ(itdmIO.getItemtabl(), tablesInner.t5611)
			&& isEQ(itdmIO.getItemcoy(), srcalcpy.chdrChdrcoy)
			&& isEQ(itdmIO.getItemitem(), wsaaT5611Item)) {
				t5611rec.t5611Rec.set(itdmIO.getGenarea());
				wsaaSumFlag.set("Y");
			}
			else {
				wsaaSumFlag.set(SPACES);
			}
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
	protected void update3000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateDatabase3010();
					checkPreviousSurrender3030();
					callSurhclmio3040();
				case readSubfile3070:
					readSubfile3070();
				case exit3090:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
		this.surdpfDAO.insertSurdpfList(this.surdpfList);
		this.hsudpfDAO.insertHsudpfList(this.hsudpfList);
		this.surhpfDAO.insertSurhpfList(surhpfList);
		
		
	}



protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit3090);
		}
		
	}

protected void checkPreviousSurrender3030()
	{
		/* What we try to do here is to give each set of surrender details */
		/* its own TRANNO. So that surrendering more than one policy       */
		/* within a plan can be handled by the triggering module. If we    */
		/* don't do this, the triggering module will get very confused and */
		/* will only read the first header record(SURH) and it thinks      */
		/* all the detail records(SURD) with the same TRANNO are belonging */
		/* to this header record!                                          */
		/* Check if the present TRANNO has been used by the previous       */
		/* surrender. If it has, add 1 to TRANNO and check again until     */
		/* a TRANNO has not been used before.                              */
		surhclmIO.setParams(SPACES);
		surhclmIO.setPlanSuffix(0);
		surhclmIO.setTranno(chdrsurIO.getTranno());
		surhclmIO.setChdrcoy(wsspcomn.company);
		surhclmIO.setChdrnum(chdrsurIO.getChdrnum());
		surhclmIO.setFunction("BEGN");
		surhclmIO.setFormat(formatsInner.surhclmrec);
	}

protected void callSurhclmio3040()
	{
		SmartFileCode.execute(appVars, surhclmIO);
		if ((isNE(surhclmIO.getStatuz(), varcom.oK))
		&& (isNE(surhclmIO.getStatuz(), "ENDP"))) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, surhclmIO.getChdrcoy()))
		|| (isNE(chdrsurIO.getChdrnum(), surhclmIO.getChdrnum()))
		|| isEQ(surhclmIO.getStatuz(), "ENDP")) {
			surhclmIO.setParams(SPACES);
			goTo(GotoLabel.readSubfile3070);
			//readSubfile3070();
		}
		/*  A record was found for this TRANNO, READH this record          */
		/*  and update the PLAN SUFFIX with that of the most recent        */
		/*  policy and REWRT.                                              */
		/*    ADD 1                       TO CHDRSUR-TRANNO.          <011>*/
		/*    MOVE NEXTR                  TO SURHCLM-FUNCTION.        <011>*/
		/*    GO TO 3040-CALL-SURHCLMIO.                              <011>*/
		/*  Read and hold the SURH record.*/
		/*ILIFE-6384*/
		if ((isEQ(surhclmIO.getStatuz(), varcom.oK))){
				surhclmIO.setFunction(varcom.readh);
				SmartFileCode.execute(appVars, surhclmIO);
				if (isNE(surhclmIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(surhclmIO.getParams());
					fatalError600();	
				}
				/*  Update the plan suffix on the SURH record and rewrite.         */
				surhclmIO.setPlanSuffix(wsaaPlanSuffix);
				surhclmIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, surhclmIO);
				if (isNE(surhclmIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(surhclmIO.getParams());
					fatalError600();
				}
			}
		/*ILIFE-6384*/
		
			if ((isNE(wsspcomn.company, surhclmIO.getChdrcoy()))
			|| (isNE(chdrsurIO.getChdrnum(), surhclmIO.getChdrnum()))
			|| isEQ(surhclmIO.getStatuz(), "ENDP")) {
				surhclmIO.setParams(SPACES);
				//goTo(GotoLabel.readSubfile3070);
				readSubfile3070();
			}
		
	}

	/**
	* <pre>
	*   restart the subfile and read each record and update the
	*   claims detail file.
	* </pre>
	*/
protected void readSubfile3070()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5026", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile3100();
		}
		
		/*  Only write a SURH the first time through.                      */
		/*  Set the flag accordingly to indicate which 'time through'      */
		/*  this is.                                                       */
		if (firstTimeThrough.isTrue()) { 
			createSurhHeader3400();
			if(susur002Permission)
			{
			/*getReqNo();
			writeCheqRecord();*/
			writeSurr();
			}
			
			// ILIFE-5452 Start	
			if(isEQ(sv.reserveUnitsInd, "Y") && isNE(sv.reserveUnitsDate, 0) && 
					isNE(sv.reserveUnitsDate, varcom.maxdate)){
				writeReservePricing();
			}// ILIFE-5452 End
		}
		
	}

protected void readSubfile3100()
	{
		/*READ-NEXT-RECORD*/
		createSurdDetail3300();
		scrnparams.function.set(varcom.srdn);
		processScreen("S5026", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void createSurdDetail3300()
	{
		start3300();
	}

protected void start3300()
	{
	if(susur013Permission) {
		
		List<Covrpf> covrpfobjList = covrpfDAO.getCovrmjaByComAndNum(chdrsurIO.getChdrcoy().toString(), 
				chdrsurIO.getChdrnum().toString());
		
		for(Covrpf tempitem : covrpfobjList) {
			
			itempf = new Itempf();
			itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(t5687);
			itempf.setItemitem(tempitem.getCrtable());
			itempf = itempfDAO.getItempfRecord(itempf);
			
			covrpfobj = tempitem;
			
			if(itempf!=null) {
				jpnlocUpdate();
			}
		}
	}
		
		/* COVRSUR is not current anymore, so can not use the fields.      */
		
		//surdclmIO.setDataArea(SPACES);
	if(!susur013Permission) {
		surdclmIO = new Surdpf();
		surdclmIO.setPlnsfx(wsaaPlanSuffix.toInt());
		surdclmIO.setChdrcoy(wsspcomn.company.toString());
		surdclmIO.setChdrnum(chdrsurIO.getChdrnum().toString());
		/*MOVE COVRSUR-LIFE           TO SURDCLM-LIFE.                 */
		/* MOVE WSAA-LIFE              TO SURDCLM-LIFE.         <CAS1.0>*/
		surdclmIO.setLife(sv.life.toString());
		surdclmIO.setCrtable(sv.hcrtable.toString());
		/*MOVE COVRSUR-JLIFE          TO SURDCLM-JLIFE.                */
		/* MOVE WSAA-JLIFE             TO SURDCLM-JLIFE.        <CAS1.0>*/
		surdclmIO.setJlife(sv.hjlife.toString());
		surdclmIO.setCoverage(sv.hcover.toString());
		if (isEQ(sv.rider, SPACES)) {
			surdclmIO.setRider("00");
		}
		else {
			surdclmIO.setRider(sv.rider.toString());
		}
		surdclmIO.setVrtfund(sv.fund.toString());
		surdclmIO.setShortds(sv.shortds.toString());
		surdclmIO.setTranno(chdrsurIO.getTranno().toInt());
		/*  MOVE S5026-RIIND            TO SURDCLM-RIIND.                */
		surdclmIO.setCurrcd(sv.cnstcur.toString());
		surdclmIO.setEmv(sv.estMatValue.getbigdata());
		surdclmIO.setActvalue(sv.actvalue.getbigdata());	
		surdclmIO.setTypeT(sv.htype.toString());
		surdclmIO.setCommclaw(sv.commclaw.getbigdata());
		//surdclmIO.setFunction("WRITR");
		//surdclmIO.setFormat(formatsInner.surdclmrec);
//		SmartFileCode.execute(appVars, surdclmIO);
//		if (isNE(surdclmIO.getStatuz(), varcom.oK)) {
//			syserrrec.params.set(surdclmIO.getParams());
//			fatalError600();
//		}
		this.surdpfList.add(surdclmIO);
		/* For every SURDCLM written, write a HSUD                         */
		Hsudpf hsudIO = new Hsudpf();
		//hsudIO.setParams(SPACES);//fwang3
		hsudIO.setChdrcoy(surdclmIO.getChdrcoy());
		hsudIO.setChdrnum(surdclmIO.getChdrnum());
		hsudIO.setLife(surdclmIO.getLife());
		hsudIO.setCoverage(surdclmIO.getCoverage());
		hsudIO.setRider(surdclmIO.getRider());
		hsudIO.setPlanSuffix(surdclmIO.getPlnsfx());
		hsudIO.setCrtable(surdclmIO.getCrtable());
		hsudIO.setJlife(surdclmIO.getJlife());
		hsudIO.setTranno(surdclmIO.getTranno());
		hsudIO.setHactval(sv.actvalue.getbigdata());
		hsudIO.setHemv(sv.estMatValue.getbigdata());
		hsudIO.setHcnstcur(sv.cnstcur.toString());
		hsudIO.setFieldType(sv.htype.toString());
		this.hsudpfList.add(hsudIO);
	}
	
	
//		hsudIO.setFormat(formatsInner.hsudrec);
//		hsudIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, hsudIO);
//		if (isNE(hsudIO.getStatuz(), varcom.oK)) {
//			syserrrec.params.set(hsudIO.getParams());
//			fatalError600();
//		}
}


protected void jpnlocUpdate() {
	t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	if(!t5687rec.svMethod.equals(SPACES)) {
		surdclmIO = new Surdpf();
		surdclmIO.setPlnsfx(wsaaPlanSuffix.toInt());
		surdclmIO.setChdrcoy(wsspcomn.company.toString());
		surdclmIO.setChdrnum(covrpfobj.getChdrnum());
		surdclmIO.setLife(covrpfobj.getLife());
		surdclmIO.setCrtable(covrpfobj.getCrtable());
		surdclmIO.setJlife(covrpfobj.getJlife());
		surdclmIO.setCoverage(covrpfobj.getCoverage());
		if (isEQ(sv.rider, SPACES)) {
			surdclmIO.setRider("00");
		}
		else {
			surdclmIO.setRider(covrpfobj.getRider());
		}
		surdclmIO.setShortds(covrpfobj.getCrtable());
		surdclmIO.setTranno(chdrsurIO.getTranno().toInt());
		surdclmIO.setCurrcd(covrpfobj.getPremCurrency());
		surdclmIO.setEmv(sv.estMatValue.getbigdata());
		surdclmIO.setActvalue(sv.actvalue.getbigdata());	
		surdclmIO.setTypeT("S");
		this.surdpfList.add(surdclmIO);
		
		Hsudpf hsudIO = new Hsudpf();
		hsudIO.setChdrcoy(surdclmIO.getChdrcoy());
		hsudIO.setChdrnum(surdclmIO.getChdrnum());
		hsudIO.setLife(surdclmIO.getLife());
		hsudIO.setCoverage(surdclmIO.getCoverage());
		hsudIO.setRider(surdclmIO.getRider());
		hsudIO.setPlanSuffix(surdclmIO.getPlnsfx());
		hsudIO.setCrtable(surdclmIO.getCrtable());
		hsudIO.setJlife(surdclmIO.getJlife());
		hsudIO.setTranno(surdclmIO.getTranno());
		hsudIO.setHactval(sv.actvalue.getbigdata());
		hsudIO.setHemv(sv.estMatValue.getbigdata());
		hsudIO.setHcnstcur(surdclmIO.getCurrcd());//IJTI-1793
		hsudIO.setFieldType(surdclmIO.getTypeT());//IJTI-1793
		this.hsudpfList.add(hsudIO);
	}
}



protected void createSurhHeader3400()
{
	write3410();
}

protected void write3410()
	{
		Surhpf surhclmIO = new Surhpf();
//		surhclmIO.setDataArea(SPACES);
		surhclmIO.setPlanSuffix(wsaaPlanSuffix.toInt());
		surhclmIO.setChdrcoy(wsspcomn.company.toString());
		surhclmIO.setChdrnum(chdrsurIO.getChdrnum().toString());
		surhclmIO.setLife(wsaaLife.toString());
		surhclmIO.setJlife(wsaaJlife.toString());
		surhclmIO.setCurrcd(sv.currcd.toString());
		surhclmIO.setEffdate(sv.effdate.toInt());
		surhclmIO.setTranno(chdrsurIO.getTranno().toInt());
		surhclmIO.setCnttype(chdrsurIO.getCnttype().toString());
		surhclmIO.setReasoncd(sv.reasoncd.toString());
		surhclmIO.setResndesc(sv.resndesc.toString());
		surhclmIO.setPolicyloan(sv.policyloan.getbigdata());
		surhclmIO.setTdbtamt(sv.tdbtamt.getbigdata());
		surhclmIO.setZrcshamt(sv.zrcshamt.getbigdata());
		surhclmIO.setOtheradjst(sv.otheradjst.getbigdata());
		surhclmIO.setTaxamt(sv.taxamt.getbigdata());
		setPrecision(surhclmIO.getTaxamt(), 2);
		surhclmIO.setTaxamt(sub(surhclmIO.getTaxamt(), wsaaFeeTax).getbigdata());
		if(susur002Permission){
			surhclmIO.setSuspenseamt(sv.sacscurbal.getbigdata());
		}
		if(susur013Permission) {
			surhclmIO.setSuspenseamt(sv.suspenseamt.getbigdata());
			surhclmIO.setUnexpiredPremium(sv.unexpiredprm.getbigdata());
		}
		this.surhpfList.add(surhclmIO);
//		surhclmIO.setFunction("WRITR");
//		surhclmIO.setFormat(formatsInner.surhclmrec);
//		SmartFileCode.execute(appVars, surhclmIO);
//		if (isNE(surhclmIO.getStatuz(), varcom.oK)) {
//			syserrrec.params.set(surhclmIO.getParams());
//			fatalError600();
//		}
		}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/


protected void writeSurr()
	{
		pyoupf.setChdrpfx("CH");
    	pyoupf.setChdrcoy(wsspcomn.company.toString());
    	pyoupf.setChdrnum(chdrsurIO.getChdrnum().toString());
    	pyoupf.setTranno(chdrsurIO.getTranno().toInt());
    	pyoupf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
       	pyoupf.setPayrnum(sv.payrnum.toString());
    	pyoupf.setReqntype(sv.reqntype.toString());
       
    	
    	if(isNE(sv.bankacckey,SPACE)){
    	pyoupf.setBankacckey(sv.bankacckey.toString());
    	}
    	if(isNE(sv.crdtcrd,SPACE)){
        pyoupf.setBankacckey(sv.crdtcrd.toString());
        }
    	
    	
    	if(isEQ(sv.bankacckey,SPACE)){
        	pyoupf.setBankacckey(sv.crdtcrd.toString());
        	}
        if(isEQ(sv.crdtcrd,SPACE)){
            pyoupf.setBankacckey(sv.bankacckey.toString());
            }
    	pyoupf.setBankkey(sv.bankkey.toString());
    	pyoupf.setBankdesc(sv.bankdesc.toString());
    	pyoupf.setPaymentflag(SPACE);
    	pyoupf.setReqnno(SPACE);
    	
    	try{
    		pyoupfDAO.insertPyoupfRecord(pyoupf);
    	}catch(Exception e){
    		e.printStackTrace();
    		fatalError600();
    	}

		}



protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			releaseSoftlock();
			return ;
		}
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void checkCalcTax5100()
	{
		try {
			start5110();
			readTr52d5120();
			readSubFile5130();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start5110()
	{
		wsaaFeeTax.set(ZERO);
		/* Read CHDRENQ                                                    */
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setChdrcoy(chdrsurIO.getChdrcoy());
		chdrenqIO.setChdrnum(chdrsurIO.getChdrnum());
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
	}

protected void readTr52d5120()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrenqIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrenqIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrenqIO.getChdrcoy());
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isEQ(tr52drec.txcode, SPACES)) {
			goTo(GotoLabel.exit5190);
		}
	}

protected void readSubFile5130()
	{
		/* Accumulate TOTAL FEE                                            */
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5026", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			if (isEQ(sv.fieldType, "C")
			|| isEQ(sv.fieldType, "J")) {
				callTaxsubr5200();
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("S5026", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		sv.taxamt.add(wsaaFeeTax);
	}

protected void callTaxsubr5200()
	{
		start5200();
	}

protected void start5200()
	{
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
		wsaaTr52eCrtable.set(sv.hcrtable);
		readTr52e5300();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e5300();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e5300();
		}
		/* IF TR52E-TAXIND-05          NOT = 'Y'                        */
		if (isNE(tr52erec.taxind03, "Y")) {
			return ;
			/****     GO TO 5190-EXIT                                   <S19FIX>*/
		}
		/* Call tax subroutine                                             */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(sv.life);
		txcalcrec.coverage.set(sv.coverage);
		txcalcrec.rider.set(sv.rider);
		txcalcrec.crtable.set(sv.hcrtable);
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.register.set(chdrenqIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		if (isEQ(sv.currcd, SPACES)) {
			txcalcrec.ccy.set(chdrsurIO.getCntcurr());
			wsaaCntCurr.set(chdrsurIO.getCntcurr());
		}
		else {
			txcalcrec.ccy.set(sv.currcd);
			wsaaCntCurr.set(sv.currcd);
		}
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.cntTaxInd.set(SPACES);
		/*   MOVE WSAA-TOTAL-FEE         TO TXCL-AMOUNT-IN.               */
		txcalcrec.amountIn.set(sv.actvalue);
		txcalcrec.transType.set("SURF");
		txcalcrec.effdate.set(sv.effdate);
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void readTr52e5300()
	{
		start5300();
	}

protected void start5300()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(wsaaTr52eKey);
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy()))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void callRounding6000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

/**
 * ILIFE-5452 Start
 */
protected void writeReservePricing(){
	
	zutrpf.setChdrpfx("CH");
	zutrpf.setChdrcoy(surhpfList.get(0).getChdrcoy());
	zutrpf.setChdrnum(sv.chdrnum.toString().trim());
	zutrpf.setLife(surhpfList.get(0).getLife());	
	zutrpf.setCoverage(surdclmIO.getCoverage().trim());/* IJTI-1523 */
	zutrpf.setRider(surdclmIO.getRider().trim());/* IJTI-1523 */
	zutrpf.setPlanSuffix(surhpfList.get(0).getPlanSuffix());
	zutrpf.setTranno(surhpfList.get(0).getTranno()); 
	zutrpf.setReserveUnitsInd(sv.reserveUnitsInd.toString().trim());
	zutrpf.setReserveUnitsDate(sv.reserveUnitsDate.toInt());
	zutrpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());	
	zutrpf.setTransactionDate(varcom.vrcmDate.toInt());
	zutrpf.setTransactionTime(varcom.vrcmTime.toInt());
	zutrpf.setUser(varcom.vrcmUser.toInt());
	zutrpf.setEffdate(sv.effdate.toInt());
	zutrpf.setValidflag("1");
	zutrpf.setDatesub(wsaaBusinessDate.toInt()); 
	zutrpf.setCrtuser(wsspcomn.userid.toString());  
	zutrpf.setUserProfile(wsspcomn.userid.toString()); 
	zutrpf.setJobName(appVars.getLoggedOnUser());
	zutrpf.setDatime("");
	
	try{
		zutrpfDAO.insertZutrpfRecord(zutrpf);
	}catch(Exception e){
		//syserrrec.params.set(covtlnbIO.getParams());	
		LOGGER.error("Exception occured ",e);
		fatalError600();
	}
	
}// ILIFE-5452 End


//ILIFE-5980 Start 
protected void readT66402986(){

	itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),t6640,covrsurIO.getCrtable().toString(),sv.effdate.toInt());

	if (itempfList.size() >= 1 ){
		wsaaTreditionslFlag.set("Y");
	}
	else {
		wsaaTreditionslFlag.set("N");
	}

}//ILIFE-5980 End

protected void releaseSoftlock()
{
	sftlockrec.sftlockRec.set(SPACES);
	sftlockrec.company.set(chdrsurIO.getChdrcoy());
	sftlockrec.entity.set(chdrsurIO.getChdrnum());
	sftlockrec.enttyp.set("CH");
	sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde.toString());
	sftlockrec.statuz.set(SPACES);
	sftlockrec.function.set("UNLK");
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
}

/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData g008 = new FixedLengthStringData(4).init("G008");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData h377 = new FixedLengthStringData(4).init("H377");
	private FixedLengthStringData t062 = new FixedLengthStringData(4).init("T062");
	private FixedLengthStringData t065 = new FixedLengthStringData(4).init("T065");
	private FixedLengthStringData h962 = new FixedLengthStringData(4).init("H962");
	private FixedLengthStringData h355 = new FixedLengthStringData(4).init("H355");
	private FixedLengthStringData h960 = new FixedLengthStringData(4).init("H960");
	private FixedLengthStringData j008 = new FixedLengthStringData(4).init("J008");
	private FixedLengthStringData g094 = new FixedLengthStringData(4).init("G094");
	private FixedLengthStringData hl08 = new FixedLengthStringData(4).init("HL08");
	private FixedLengthStringData p296 = new FixedLengthStringData(4).init("P296");
	private FixedLengthStringData rpy5 = new FixedLengthStringData(4).init("RPY5");
	private FixedLengthStringData h359 = new FixedLengthStringData(4).init("H359");
	private FixedLengthStringData rlcc = new FixedLengthStringData(4).init("RLCC");
	private FixedLengthStringData g099 = new FixedLengthStringData(4).init("G099");
	private FixedLengthStringData hl36 = new FixedLengthStringData(5).init("HL36");  //ILIFE-6578
	private FixedLengthStringData rr88 = new FixedLengthStringData(5).init("RR88");   //ILIFE-6578  
	private FixedLengthStringData rrfl = new FixedLengthStringData(5).init("RRFL"); 
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
	private FixedLengthStringData f906 = new FixedLengthStringData(4).init("F906");
	private FixedLengthStringData rrfm = new FixedLengthStringData(4).init("RRFM");
	private FixedLengthStringData rrfn = new FixedLengthStringData(4).init("RRFN");
	private FixedLengthStringData rf05 = new FixedLengthStringData(4).init("RF05");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t6598 = new FixedLengthStringData(5).init("T6598");
	private FixedLengthStringData t5500 = new FixedLengthStringData(5).init("T5500");
	private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
	private FixedLengthStringData t3000 = new FixedLengthStringData(5).init("T3000");
	private FixedLengthStringData t5611 = new FixedLengthStringData(5).init("T5611");
	private FixedLengthStringData tr691 = new FixedLengthStringData(5).init("TR691");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t3629 = new FixedLengthStringData(5).init("T3629");
	private FixedLengthStringData t3688 = new FixedLengthStringData(5).init("T3688");
	private FixedLengthStringData t5644 = new FixedLengthStringData(5).init("T5644");
	
	
	
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData surhclmrec = new FixedLengthStringData(10).init("SURHCLMREC");
	private FixedLengthStringData surdclmrec = new FixedLengthStringData(10).init("SURDCLMREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData incrmjarec = new FixedLengthStringData(10).init("INCRMJAREC");
	private FixedLengthStringData hitrrnlrec = new FixedLengthStringData(10).init("HITRRNLREC");
	private FixedLengthStringData tpoldbtrec = new FixedLengthStringData(10).init("TPOLDBTREC");
	private FixedLengthStringData hsudrec = new FixedLengthStringData(10).init("HSUDREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData uwlvrec = new FixedLengthStringData(10).init("UWLVREC");
}
}
