package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:26
 * Description:
 * Copybook name: T6625REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6625rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6625Rec = new FixedLengthStringData(getRecSize());
  	public FixedLengthStringData bonalloc = new FixedLengthStringData(1).isAPartOf(t6625Rec, 0);
  	public FixedLengthStringData comtmeth = new FixedLengthStringData(4).isAPartOf(t6625Rec, 1);
  	public FixedLengthStringData comtmthj = new FixedLengthStringData(4).isAPartOf(t6625Rec, 5);
  	public ZonedDecimalData comtperc = new ZonedDecimalData(5, 2).isAPartOf(t6625Rec, 9);
  	public ZonedDecimalData dthpercn = new ZonedDecimalData(5, 2).isAPartOf(t6625Rec, 14);
  	public ZonedDecimalData dthperco = new ZonedDecimalData(5, 2).isAPartOf(t6625Rec, 19);
  	public ZonedDecimalData evstperd = new ZonedDecimalData(3, 0).isAPartOf(t6625Rec, 24);
  	public FixedLengthStringData freqann = new FixedLengthStringData(2).isAPartOf(t6625Rec, 27);
  	public FixedLengthStringData frequency = new FixedLengthStringData(2).isAPartOf(t6625Rec, 29);
  	public ZonedDecimalData guarperd = new ZonedDecimalData(3, 0).isAPartOf(t6625Rec, 31);
  	public ZonedDecimalData lvstperd = new ZonedDecimalData(3, 0).isAPartOf(t6625Rec, 34);
  	public FixedLengthStringData ncovvest = new FixedLengthStringData(4).isAPartOf(t6625Rec, 37);
  	public FixedLengthStringData revBonusMeth = new FixedLengthStringData(4).isAPartOf(t6625Rec, 41);
  	public FixedLengthStringData vcalcmth = new FixedLengthStringData(4).isAPartOf(t6625Rec, 45);
  	public FixedLengthStringData vclcmthj = new FixedLengthStringData(4).isAPartOf(t6625Rec, 49);
  	public FixedLengthStringData annuty = new FixedLengthStringData(1).isAPartOf(t6625Rec, 53);//ILIFE-3595
  	public FixedLengthStringData filler = new FixedLengthStringData(447).isAPartOf(t6625Rec, 54, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6625Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6625Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	public int getRecSize() {
		return 501;
	}
	

}