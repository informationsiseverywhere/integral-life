package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr688screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 8;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {13, 12, 8, 4, 5, 80, 1, 2, 3}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 16, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr688ScreenVars sv = (Sr688ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr688screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr688screensfl, 
			sv.Sr688screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr688ScreenVars sv = (Sr688ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr688screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr688ScreenVars sv = (Sr688ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr688screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr688screensflWritten.gt(0))
		{
			sv.sr688screensfl.setCurrentIndex(0);
			sv.Sr688screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr688ScreenVars sv = (Sr688ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr688screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr688ScreenVars screenVars = (Sr688ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.mbrcopay.setFieldName("mbrcopay");
				screenVars.bnflmta.setFieldName("bnflmta");
				screenVars.bnflmtb.setFieldName("bnflmtb");
				screenVars.mbrdeduc.setFieldName("mbrdeduc");
				screenVars.benefits.setFieldName("benefits");
				screenVars.crrcdDisp.setFieldName("crrcdDisp");
				screenVars.crtdateDisp.setFieldName("crtdateDisp");
				screenVars.coverc.setFieldName("coverc");
				screenVars.sel.setFieldName("sel");
				screenVars.hosben.setFieldName("hosben");
				screenVars.shortds.setFieldName("shortds");
				screenVars.limitc.setFieldName("limitc");
				screenVars.benfamt.setFieldName("benfamt");
				screenVars.accday.setFieldName("accday");
				screenVars.actexp.setFieldName("actexp");
				screenVars.nofday.setFieldName("nofday");
				screenVars.gcnetpy.setFieldName("gcnetpy");
				screenVars.zdaycov.setFieldName("zdaycov");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.mbrcopay.set(dm.getField("mbrcopay"));
			screenVars.bnflmta.set(dm.getField("bnflmta"));
			screenVars.bnflmtb.set(dm.getField("bnflmtb"));
			screenVars.mbrdeduc.set(dm.getField("mbrdeduc"));
			screenVars.benefits.set(dm.getField("benefits"));
			screenVars.crrcdDisp.set(dm.getField("crrcdDisp"));
			screenVars.crtdateDisp.set(dm.getField("crtdateDisp"));
			screenVars.coverc.set(dm.getField("coverc"));
			screenVars.sel.set(dm.getField("sel"));
			screenVars.hosben.set(dm.getField("hosben"));
			screenVars.shortds.set(dm.getField("shortds"));
			screenVars.limitc.set(dm.getField("limitc"));
			screenVars.benfamt.set(dm.getField("benfamt"));
			screenVars.accday.set(dm.getField("accday"));
			screenVars.actexp.set(dm.getField("actexp"));
			screenVars.nofday.set(dm.getField("nofday"));
			screenVars.gcnetpy.set(dm.getField("gcnetpy"));
			screenVars.zdaycov.set(dm.getField("zdaycov"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr688ScreenVars screenVars = (Sr688ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.mbrcopay.setFieldName("mbrcopay");
				screenVars.bnflmta.setFieldName("bnflmta");
				screenVars.bnflmtb.setFieldName("bnflmtb");
				screenVars.mbrdeduc.setFieldName("mbrdeduc");
				screenVars.benefits.setFieldName("benefits");
				screenVars.crrcdDisp.setFieldName("crrcdDisp");
				screenVars.crtdateDisp.setFieldName("crtdateDisp");
				screenVars.coverc.setFieldName("coverc");
				screenVars.sel.setFieldName("sel");
				screenVars.hosben.setFieldName("hosben");
				screenVars.shortds.setFieldName("shortds");
				screenVars.limitc.setFieldName("limitc");
				screenVars.benfamt.setFieldName("benfamt");
				screenVars.accday.setFieldName("accday");
				screenVars.actexp.setFieldName("actexp");
				screenVars.nofday.setFieldName("nofday");
				screenVars.gcnetpy.setFieldName("gcnetpy");
				screenVars.zdaycov.setFieldName("zdaycov");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("mbrcopay").set(screenVars.mbrcopay);
			dm.getField("bnflmta").set(screenVars.bnflmta);
			dm.getField("bnflmtb").set(screenVars.bnflmtb);
			dm.getField("mbrdeduc").set(screenVars.mbrdeduc);
			dm.getField("benefits").set(screenVars.benefits);
			dm.getField("crrcdDisp").set(screenVars.crrcdDisp);
			dm.getField("crtdateDisp").set(screenVars.crtdateDisp);
			dm.getField("coverc").set(screenVars.coverc);
			dm.getField("sel").set(screenVars.sel);
			dm.getField("hosben").set(screenVars.hosben);
			dm.getField("shortds").set(screenVars.shortds);
			dm.getField("limitc").set(screenVars.limitc);
			dm.getField("benfamt").set(screenVars.benfamt);
			dm.getField("accday").set(screenVars.accday);
			dm.getField("actexp").set(screenVars.actexp);
			dm.getField("nofday").set(screenVars.nofday);
			dm.getField("gcnetpy").set(screenVars.gcnetpy);
			dm.getField("zdaycov").set(screenVars.zdaycov);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr688screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr688ScreenVars screenVars = (Sr688ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.mbrcopay.clearFormatting();
		screenVars.bnflmta.clearFormatting();
		screenVars.bnflmtb.clearFormatting();
		screenVars.mbrdeduc.clearFormatting();
		screenVars.benefits.clearFormatting();
		screenVars.crrcdDisp.clearFormatting();
		screenVars.crtdateDisp.clearFormatting();
		screenVars.coverc.clearFormatting();
		screenVars.sel.clearFormatting();
		screenVars.hosben.clearFormatting();
		screenVars.shortds.clearFormatting();
		screenVars.limitc.clearFormatting();
		screenVars.benfamt.clearFormatting();
		screenVars.accday.clearFormatting();
		screenVars.actexp.clearFormatting();
		screenVars.nofday.clearFormatting();
		screenVars.gcnetpy.clearFormatting();
		screenVars.zdaycov.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr688ScreenVars screenVars = (Sr688ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.mbrcopay.setClassString("");
		screenVars.bnflmta.setClassString("");
		screenVars.bnflmtb.setClassString("");
		screenVars.mbrdeduc.setClassString("");
		screenVars.benefits.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.crtdateDisp.setClassString("");
		screenVars.coverc.setClassString("");
		screenVars.sel.setClassString("");
		screenVars.hosben.setClassString("");
		screenVars.shortds.setClassString("");
		screenVars.limitc.setClassString("");
		screenVars.benfamt.setClassString("");
		screenVars.accday.setClassString("");
		screenVars.actexp.setClassString("");
		screenVars.nofday.setClassString("");
		screenVars.gcnetpy.setClassString("");
		screenVars.zdaycov.setClassString("");
	}

/**
 * Clear all the variables in Sr688screensfl
 */
	public static void clear(VarModel pv) {
		Sr688ScreenVars screenVars = (Sr688ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.mbrcopay.clear();
		screenVars.bnflmta.clear();
		screenVars.bnflmtb.clear();
		screenVars.mbrdeduc.clear();
		screenVars.benefits.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.crtdateDisp.clear();
		screenVars.crtdate.clear();
		screenVars.coverc.clear();
		screenVars.sel.clear();
		screenVars.hosben.clear();
		screenVars.shortds.clear();
		screenVars.limitc.clear();
		screenVars.benfamt.clear();
		screenVars.accday.clear();
		screenVars.actexp.clear();
		screenVars.nofday.clear();
		screenVars.gcnetpy.clear();
		screenVars.zdaycov.clear();
	}
}
