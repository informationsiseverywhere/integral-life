/*
 * File: T6691pt.java
 * Date: 30 August 2009 2:30:17
 * Author: Quipoz Limited
 * 
 * Class transformed from T6691PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.terminationclaims.tablestructures.T6691rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6691.
*
*
*****************************************************************
* </pre>
*/
public class T6691pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine001, 31, FILLER).init("Pension Releif Rates                    S6691");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(76);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init("  Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 46);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(44);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 25, FILLER).init("    To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 34);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(63);
	private FixedLengthStringData filler9 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Percentage Releif:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine004, 22).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 28, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine004, 36, FILLER).init("Earnings Cap:");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(11, 0).isAPartOf(wsaaPrtLine004, 52).setPattern("ZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(47);
	private FixedLengthStringData filler12 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(35).isAPartOf(wsaaPrtLine005, 12, FILLER).init("To Age              % Funding Limit");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(41);
	private FixedLengthStringData filler14 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 13).setPattern("ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine006, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(41);
	private FixedLengthStringData filler16 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 13).setPattern("ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine007, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(41);
	private FixedLengthStringData filler18 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 13).setPattern("ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine008, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(41);
	private FixedLengthStringData filler20 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 13).setPattern("ZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine009, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(41);
	private FixedLengthStringData filler22 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 13).setPattern("ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine010, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(41);
	private FixedLengthStringData filler24 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 13).setPattern("ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine011, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(41);
	private FixedLengthStringData filler26 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 13).setPattern("ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine012, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(41);
	private FixedLengthStringData filler28 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 13).setPattern("ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine013, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(41);
	private FixedLengthStringData filler30 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 13).setPattern("ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine014, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 35).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(41);
	private FixedLengthStringData filler32 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 13).setPattern("ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine015, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 35).setPattern("ZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6691rec t6691rec = new T6691rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6691pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6691rec.t6691Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo009.set(t6691rec.toage01);
		fieldNo010.set(t6691rec.pcFundingLimit01);
		fieldNo011.set(t6691rec.toage02);
		fieldNo012.set(t6691rec.pcFundingLimit02);
		fieldNo013.set(t6691rec.toage03);
		fieldNo014.set(t6691rec.pcFundingLimit03);
		fieldNo015.set(t6691rec.toage04);
		fieldNo016.set(t6691rec.pcFundingLimit04);
		fieldNo017.set(t6691rec.toage05);
		fieldNo018.set(t6691rec.pcFundingLimit05);
		fieldNo019.set(t6691rec.toage06);
		fieldNo020.set(t6691rec.pcFundingLimit06);
		fieldNo021.set(t6691rec.toage07);
		fieldNo022.set(t6691rec.pcFundingLimit07);
		fieldNo023.set(t6691rec.toage08);
		fieldNo024.set(t6691rec.pcFundingLimit08);
		fieldNo025.set(t6691rec.toage09);
		fieldNo026.set(t6691rec.pcFundingLimit09);
		fieldNo027.set(t6691rec.toage10);
		fieldNo028.set(t6691rec.pcFundingLimit10);
		fieldNo007.set(t6691rec.pcReleif);
		fieldNo008.set(t6691rec.earningCap);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
