package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Mathpf;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MathpfDAO extends BaseDAO<Mathpf> {
	
	public boolean insertMathpf(List<Mathpf> mathpfList);
	public List<Mathpf> getmathclmRecordList(String chdrcoy,String chdrnum,int tranno);

}
