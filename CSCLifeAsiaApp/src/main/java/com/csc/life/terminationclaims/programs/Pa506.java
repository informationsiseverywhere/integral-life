/*
 * File: Pa506.java
 * Date: 30 August 2009 0:17:31
 * Author: Quipoz Limited
 * 
 * Class transformed from Pa506.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.screens.Sa506ScreenVars;
import com.csc.smart.procedures.Batcchk;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcchkrec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Pa506 - Regular Payments Sub-Menu.
*
* This is the Regular Payments Sub Menu Program.
* It performs all the tasks necessary of a Sub Menu.
*
* 1000-
*
* Initialise the Screen fields and all the appropriate Working
* Storage Variables.
*
* 2000-
*
* Display the Screen by Calling the Sa506IO Module.
*
* Call the SUBPROG Subroutine.
*
* Call the SANCTN Subroutine.
*
* Load up the next four Programs in the Stack.
*
* Validate the Screen fields. Both fields are mandatory.
*
* Read the CHDR Record and validate it against Table T5679.
*
*
* 3000-
*
* Softlock and perform a KEEPS on the the Contract.
*
* Update the WSSP fields.
*
* Open a new BATCH.
*
* 4000-
*
* Move 1 to the WSSP-PROGRAM-PTR.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pa506 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA506");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaContractStatuzCheck = new FixedLengthStringData(5);
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcchkrec batcchkrec = new Batcchkrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Wssplife wssplife = new Wssplife();
	private Sa506ScreenVars sv = ScreenProgram.getScreenVars( Sa506ScreenVars.class);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ErrorsInner errorsInner = new ErrorsInner();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	Clntpf clntpf=new Clntpf();
	String cltstat="DC";
	



	public Pa506() {
		super();
		screenVars = sv;
		new ScreenModel("Sa506", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		wsaaContractStatuzCheck.set(SPACES);
		sv.efdateDisp.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		wsspcomn.submenu.set(wsaaProg);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*    CALL 'Sa506IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                Sa506-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if(validateAction2100()){
			validate2200();
		}else{
			verifyBatchControl2300();
		}
		
		
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}


protected boolean validateAction2100()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			return true;
		}
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			return true;
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.statuz, "BACH")) {
			return false;
		}
		return true;
	}

protected void validate2200()
	{
	
	validateKey12210();
		
		                                     
	}

protected void validateKey12210()
{
	
	if (isEQ(subprogrec.key1,"Y")
			&& isEQ(sv.clttwo,SPACES)) {
				sv.clttwoErr.set(errorsInner.e186);
				return ;
			}
	
	if(isGT(sv.efdate, wsaaToday)){
		sv.efdateErr.set(errorsInner.RRNE);
		return ;
	}
	if (isEQ(sv.action,"A")) {
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), sv.clttwo.toString());
		if(clntpf==null){
			sv.clttwoErr.set(errorsInner.F393);
			return ;
		}else{
			if(clntpf.getCltstat().equals(cltstat)){
				sv.clttwoErr.set(errorsInner.RRNF);
				return ;
			}
			
		}
		
		
	}
	
		if (isEQ(sv.action,"D")||isEQ(sv.action,"B")||isEQ(sv.action,"C")) {
		if(notipfDAO.getCountByLifeAssNum(sv.clttwo.toString())<=0){
			sv.clttwoErr.set(errorsInner.RRNG);
			return ;	
		}
		
		
	}
	
    
    
    
    
    
    
    
    
    
    
}



protected void verifyBatchControl2300()
	{
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT                                           */
			return ;
		}
		wsspcomn.secProg[1].set(bcbprogrec.nxtprog1);
		wsspcomn.secProg[2].set(bcbprogrec.nxtprog2);
		wsspcomn.secProg[3].set(bcbprogrec.nxtprog3);
		wsspcomn.secProg[4].set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		if(updateWssp3010()){
			if(keepRecord3100()){
				if(updateBatchControl3200()){
					batcdorrec.function.set("AUTO");
					continue3400();
				}
			}
		}
	}

protected boolean updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		/*  Set the mode indicator WSSP-FLAG according to action.*/
		wsspcomn.flag.set("I");
			if (isEQ(sv.action, "A")) {
			wsspcomn.flag.set("C");
		}
		if (isEQ(sv.action, "B")) {
			wsspcomn.flag.set("M");
		}
		if (isEQ(sv.action, "C")) {
			wsspcomn.flag.set("D");
		}
		if (isEQ(sv.action, "D")) {
			wsspcomn.flag.set("I");
		}
		/* Softlock the contract header record for update modes, unless*/
		/* we are approving the payment when we lock it but set the flag*/
		/* to 'I'.*/
		if (isEQ(wsspcomn.flag, "I")
		&& isNE(scrnparams.action, "C")) {
			return true;
		}
		return true;
	}

protected boolean keepRecord3100()
	{
		/*AUTOMATIC-BATCHING*/
	batcdorrec.function.set("AUTO");
	batcdorrec.tranid.set(wsspcomn.tranid);
	batcdorrec.batchkey.set(wsspcomn.batchkey);
	callProgram(Batcdor.class, batcdorrec.batcdorRec);
	if (isNE(batcdorrec.statuz, varcom.oK)) {
		sv.actionErr.set(batcdorrec.statuz);
	}
	wsspcomn.batchkey.set(batcdorrec.batchkey);
	/*EXIT*/
		return true;
	}

protected boolean updateBatchControl3200()
	{
		batcchkrec.batcchkRec.set(SPACES);
		batcchkrec.function.set("CHECK");
		batcchkrec.batchkey.set(wsspcomn.batchkey);
		batcchkrec.tranid.set(wsspcomn.tranid);
		callProgram(Batcchk.class, batcchkrec.batcchkRec);
		if (isEQ(batcchkrec.statuz, varcom.oK)) {
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			wsspcomn.tranid.set(batcchkrec.tranid);
			return false;
		}
		if (isEQ(batcchkrec.statuz, "INAC")) {
			batcdorrec.function.set("ACTIV");
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			continue3400();
			return false;
		}
		if (isNE(batcchkrec.statuz, varcom.mrnf)) {
			sv.actionErr.set(batcchkrec.statuz);
			wsspcomn.edterror.set("Y");
			return false;
		}
		return true;
	}

protected void continue3400()
	{
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
			wsspcomn.edterror.set("Y");
			rollback();
			return ;
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
	
		wsspcomn.chdrCownnum.set(sv.clttwo);
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.set(1);
		/*EXIT*/
	}

/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData RRNE = new FixedLengthStringData(4).init("RRNE");
	private FixedLengthStringData RRNF = new FixedLengthStringData(4).init("RRNF");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	///F393
	private FixedLengthStringData F393 = new FixedLengthStringData(4).init("F393");
	private FixedLengthStringData RRNG = new FixedLengthStringData(4).init("RRNG");

	
	
	
	
	
}





}
