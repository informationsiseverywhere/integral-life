package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Regrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface RegrDAO extends BaseDAO<Regrpf>{
	// added for ILIFE-3168
	public	List<Regrpf> getRegrpfList(String programName, String chrdnum,String chdrnum1);
	void insertRecords(List<Regrpf> list);
}
