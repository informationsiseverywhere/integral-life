package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:44
 * Description:
 * Copybook name: HPCLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hpclkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hpclFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hpclKey = new FixedLengthStringData(64).isAPartOf(hpclFileKey, 0, REDEFINE);
  	public FixedLengthStringData hpclChdrcoy = new FixedLengthStringData(1).isAPartOf(hpclKey, 0);
  	public FixedLengthStringData hpclChdrnum = new FixedLengthStringData(8).isAPartOf(hpclKey, 1);
  	public FixedLengthStringData hpclLife = new FixedLengthStringData(2).isAPartOf(hpclKey, 9);
  	public FixedLengthStringData hpclCoverage = new FixedLengthStringData(2).isAPartOf(hpclKey, 11);
  	public FixedLengthStringData hpclRider = new FixedLengthStringData(2).isAPartOf(hpclKey, 13);
  	public PackedDecimalData hpclRgpynum = new PackedDecimalData(5, 0).isAPartOf(hpclKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(hpclKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hpclFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hpclFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}