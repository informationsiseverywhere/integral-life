/*
 * File: Annytax.java
 * Date: 29 August 2009 20:14:49
 * Author: Quipoz Limited
 * 
 * Class transformed from ANNYTAX.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.tablestructures.Taxcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* This is a new subroutine which forms part of the 9405 Annuities
* Development.  It will be called from the Dissection Methods
* table, T6624, during Regular Payment Batch Processing (B6681) in
* order to calculate the tax to deduct from the payment before it
* is paid out.
*
* The tax amount will be calculated as follows:
*
* Tax = (REGP payment amount - ANNY Capital Content) *
*       (T6624 Percentage / 100)
*
* Accounting movements will be created in accordance with the
* entries on T5645 for this subroutine as follows:
*
*        Line #         Description
*        ======         ===========
*          1            Contract Level Debit to Reg. Benefit
*                       Funding Account
*          2            Component Level Debit to Reg. Benefit
*                       Funding Account
*          3            Contract Level Credit to Tax Account
*          4            Component Level Credit to Tax Account
*
* Therefore posting will be made using lines 1 and 3 if the
* contract type indicates that contract level accounting applies
* on T5688, or lines 2 and 4 if the contract type indicates that
* component level accounting applies on T5688.
*****************************************************************
* </pre>
*/
public class Annytax extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ANNYTAX";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaFreqann = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaRegpayfreq = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String h134 = "H134";
		/* FORMATS */
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Taxcpy taxcpy = new Taxcpy();
	private ExternalisedRules er = new ExternalisedRules();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endProgram1990
	}

	public Annytax() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		taxcpy.taxcpyRec = convertAndSetParam(taxcpy.taxcpyRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1010();
				case endProgram1990: 
					endProgram1990();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1010()
	{
		syserrrec.subrname.set(wsaaSubr);
		wsaaSequenceNo.set(ZERO);
		wsaaRldgacct.set(SPACES);
		/* Read the ANNY to get the Capital Content*/
		annyIO.setChdrcoy(taxcpy.chdrcoy);
		annyIO.setChdrnum(taxcpy.chdrnum);
		annyIO.setLife(taxcpy.life);
		annyIO.setCoverage(taxcpy.coverage);
		annyIO.setRider(taxcpy.rider);
		annyIO.setPlanSuffix(taxcpy.planSuffix);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)) {
			taxcpy.statuz.set(annyIO.getStatuz());
			goTo(GotoLabel.endProgram1990);
		}
		wsaaFreqann.set(annyIO.getFreqann());
		wsaaRegpayfreq.set(taxcpy.regpayfreq);
		/* ILIFE-8465 */
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("ANNYTAXCALC") && er.isExternalized(taxcpy.cnttype.toString(), taxcpy.crtable.toString())))
		{
			compute(taxcpy.capcont, 3).set((mult(mult(annyIO.getCapcont(),(div(wsaaFreqann,wsaaRegpayfreq))),(div(taxcpy.regpPrcnt,100)))));
			/* Calculate tax amount.  If this amount is equal to zero, exit*/
			/* the subroutine.*/
			compute(taxcpy.taxamount, 4).setRounded(mult((sub(taxcpy.pymt,taxcpy.capcont)),(div(taxcpy.prcnt,100))));
		}
		else {
			taxcpy.capcont.set(annyIO.getCapcont());
			taxcpy.paymentFreq.set(wsaaFreqann);
			taxcpy.regpayfreq.set(wsaaRegpayfreq);
			//Call externalize calculation method
			callProgram("ANNYTAXCALC", taxcpy.taxcpyRec);
		}
		/* ILIFE-8465 */
		zrdecplrec.amountIn.set(taxcpy.taxamount);
		zrdecplrec.currency.set(taxcpy.cntcurr);
		a000CallRounding();
		taxcpy.taxamount.set(zrdecplrec.amountOut);
		if (isEQ(taxcpy.taxamount,0)) {
			goTo(GotoLabel.endProgram1990);
		}
		/* Read T5645 to obtain the accounting rules.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(taxcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(h134);
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T5688 to ascertain whether component or contract level*/
		/* accounting should be used.*/
		itdmIO.setItemcoy(taxcpy.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(taxcpy.cnttype);
		itdmIO.setItmfrm(taxcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),taxcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),taxcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/* Read T1688 for transaction code description.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(taxcpy.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(taxcpy.batctrcde);
		descIO.setLanguage(taxcpy.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		wsaaLongdesc.set(descIO.getLongdesc());
		setUpPosting2000();
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			componentLevel3000();
		}
		else {
			contractLevel3100();
		}
	}

protected void endProgram1990()
	{
		exitProgram();
	}

protected void setUpPosting2000()
	{
			setupLifacmv2010();
		}

protected void setupLifacmv2010()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(taxcpy.batckey);
		lifacmvrec.rdocnum.set(taxcpy.chdrnum);
		lifacmvrec.tranno.set(taxcpy.tranno);
		lifacmvrec.rldgcoy.set(taxcpy.batccoy);
		lifacmvrec.origcurr.set(taxcpy.cntcurr);
		lifacmvrec.tranref.set(taxcpy.chdrnum);
		lifacmvrec.trandesc.set(wsaaLongdesc);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.genlcoy.set(taxcpy.batccoy);
		lifacmvrec.genlcur.set(taxcpy.paycurr);
		lifacmvrec.effdate.set(taxcpy.effdate);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(taxcpy.user);
		lifacmvrec.termid.set(taxcpy.termid);
		lifacmvrec.substituteCode[1].set(taxcpy.cnttype);
		lifacmvrec.substituteCode[6].set(taxcpy.crtable);
		lifacmvrec.origamt.set(taxcpy.taxamount);
		/* Convert the tax amount if the contract and pay currencies*/
		/* differ and move the converted amount into the a/c amount*/
		/* Otherwise, move the original amount into the a/c amount.*/
		if (isEQ(taxcpy.cntcurr,taxcpy.paycurr)) {
			lifacmvrec.acctamt.set(taxcpy.taxamount);
			return ;
		}
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(taxcpy.nextPaydate);
		conlinkrec.currIn.set(taxcpy.cntcurr);
		conlinkrec.currOut.set(taxcpy.paycurr);
		conlinkrec.amountIn.set(taxcpy.taxamount);
		conlinkrec.company.set(taxcpy.chdrcoy);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
		lifacmvrec.acctamt.set(conlinkrec.amountOut);
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(taxcpy.paycurr);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void componentLevel3000()
	{
		setUpRecord3010();
	}

protected void setUpRecord3010()
	{
		/* Set up entity field LIFA-RLDGACCT.*/
		wsaaRldgChdrnum.set(taxcpy.chdrnum);
		wsaaPlan.set(taxcpy.planSuffix);
		wsaaRldgLife.set(taxcpy.life);
		wsaaRldgCoverage.set(taxcpy.coverage);
		wsaaRldgRider.set(taxcpy.rider);
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		postRecord3200();
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.contot.set(t5645rec.cnttot04);
		postRecord3200();
	}

protected void contractLevel3100()
	{
		setUpRecord3110();
	}

protected void setUpRecord3110()
	{
		/* For contract level accounting, only the contract header no.*/
		/* is needed in the entity field LIFA-RLDGACCT.*/
		wsaaRldgChdrnum.set(taxcpy.chdrnum);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		postRecord3200();
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		postRecord3200();
	}

protected void postRecord3200()
	{
		/*WRITE-LIFACMV*/
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.company.set(taxcpy.chdrcoy);
		zrdecplrec.batctrcde.set(taxcpy.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A090-EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL-ERROR*/
		if ((isNE(syserrrec.statuz,SPACES))
		|| (isNE(syserrrec.syserrStatuz,SPACES))) {
			syserrrec.syserrType.set("2");
		}
		else {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		taxcpy.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
