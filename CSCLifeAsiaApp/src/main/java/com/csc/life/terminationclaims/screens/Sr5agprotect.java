package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sr5agprotect extends ScreenRecord{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
			Sr5agScreenVars sv = (Sr5agScreenVars) pv;
			clearInds(av, pfInds);
			write(lrec, sv.Sr5agprotectWritten, null, av, null, ind2, ind3);
		}

		public static void read(Indicator ind2, Indicator ind3) {}

		public static void clearClassString(VarModel pv) {
			Sr5agScreenVars screenVars = (Sr5agScreenVars)pv;
		}

	/**
	 * Clear all the variables in Sr5agprotect
	 */
		public static void clear(VarModel pv) {
			Sr5agScreenVars screenVars = (Sr5agScreenVars) pv;
		}


}
