package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HclapfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:25
 * Class transformed from HCLAPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HclapfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 91;
	public FixedLengthStringData hclarec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hclapfRecord = hclarec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hclarec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hclarec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hclarec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hclarec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hclarec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hclarec);
	public FixedLengthStringData benpln = DD.benpln.copy().isAPartOf(hclarec);
	public FixedLengthStringData hosben = DD.hosben.copy().isAPartOf(hclarec);
	public PackedDecimalData acumactyr = DD.acumactyr.copy().isAPartOf(hclarec);
	public PackedDecimalData aad = DD.aad.copy().isAPartOf(hclarec);
	public PackedDecimalData clmpaid = DD.clmpaid.copy().isAPartOf(hclarec);
	public PackedDecimalData accday = DD.accday.copy().isAPartOf(hclarec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hclarec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hclarec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hclarec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HclapfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HclapfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HclapfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HclapfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HclapfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HclapfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HclapfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HCLAPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"BENPLN, " +
							"HOSBEN, " +
							"ACUMACTYR, " +
							"AAD, " +
							"CLMPAID, " +
							"ACCDAY, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     crtable,
                                     benpln,
                                     hosben,
                                     acumactyr,
                                     aad,
                                     clmpaid,
                                     accday,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		benpln.clear();
  		hosben.clear();
  		acumactyr.clear();
  		aad.clear();
  		clmpaid.clear();
  		accday.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHclarec() {
  		return hclarec;
	}

	public FixedLengthStringData getHclapfRecord() {
  		return hclapfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHclarec(what);
	}

	public void setHclarec(Object what) {
  		this.hclarec.set(what);
	}

	public void setHclapfRecord(Object what) {
  		this.hclapfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hclarec.getLength());
		result.set(hclarec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}