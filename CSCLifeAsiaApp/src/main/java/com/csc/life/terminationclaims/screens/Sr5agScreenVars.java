package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sr5agScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(2204);
	public FixedLengthStringData dataFields = new FixedLengthStringData(2044).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData progdescs = new FixedLengthStringData(500).isAPartOf(dataFields, 44);
	public FixedLengthStringData[] progdesc = FLSArrayPartOfStructure(5, 100, progdescs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(500).isAPartOf(progdescs, 0, FILLER_REDEFINE);
	public FixedLengthStringData progdesc01 = DD.refrmks.copy().isAPartOf(filler,0);
	public FixedLengthStringData progdesc02 = DD.refrmks.copy().isAPartOf(filler,100);
	public FixedLengthStringData progdesc03 = DD.refrmks.copy().isAPartOf(filler,200);
	public FixedLengthStringData progdesc04 = DD.refrmks.copy().isAPartOf(filler,300);
	public FixedLengthStringData progdesc05 = DD.refrmks.copy().isAPartOf(filler,400);
	public FixedLengthStringData excltxt = new FixedLengthStringData(1500).isAPartOf(dataFields, 544);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 2044);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData progdescsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] progdescErr = FLSArrayPartOfStructure(5, 4, progdescsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(progdescsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData progdesc01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData progdesc02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData progdesc03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData progdesc04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData progdesc05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData excltxtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 2084);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData progdescsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] progdescOut = FLSArrayPartOfStructure(5, 12, progdescsOut, 0);
	public FixedLengthStringData[][] progdescO = FLSDArrayPartOfArrayStructure(12, 1, progdescOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(60).isAPartOf(progdescsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] progdesc01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] progdesc02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] progdesc03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] progdesc04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] progdesc05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] excltxtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	

	/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr5agscreenWritten = new LongData(0);
	public LongData Sr5agprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


public Sr5agScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
fieldIndMap.put(excltxtOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});

screenFields = new BaseData[] {company, tabl, item,longdesc, progdesc01, progdesc02, progdesc03, progdesc04, progdesc05,excltxt};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut,longdescOut, progdesc01Out, progdesc02Out, progdesc03Out, progdesc04Out, progdesc05Out,excltxtOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr,longdescErr, progdesc01Err, progdesc02Err, progdesc03Err, progdesc04Err, progdesc05Err,excltxtErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr5agscreen.class;
		protectRecord = Sr5agprotect.class;
	}

}

