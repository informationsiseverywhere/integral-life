package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:24
 * Description:
 * Copybook name: COVRPUPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrpupkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrpupFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrpupKey = new FixedLengthStringData(64).isAPartOf(covrpupFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrpupChdrcoy = new FixedLengthStringData(1).isAPartOf(covrpupKey, 0);
  	public FixedLengthStringData covrpupChdrnum = new FixedLengthStringData(8).isAPartOf(covrpupKey, 1);
  	public PackedDecimalData covrpupPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrpupKey, 9);
  	public FixedLengthStringData covrpupLife = new FixedLengthStringData(2).isAPartOf(covrpupKey, 12);
  	public FixedLengthStringData covrpupCoverage = new FixedLengthStringData(2).isAPartOf(covrpupKey, 14);
  	public FixedLengthStringData covrpupRider = new FixedLengthStringData(2).isAPartOf(covrpupKey, 16);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrpupKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrpupFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrpupFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}