/*
 * File: P5247.java
 * Date: 30 August 2009 0:22:25
 * Author: Quipoz Limited
 * 
 * Class transformed from P5247.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Iterator;//ILB-459
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;//ILB-459
import com.csc.fsu.clients.dataaccess.model.Clntpf;//ILB-459
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;//ILB-459
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;//ILB-459
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;//ILB-459
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;
import com.csc.life.terminationclaims.screens.S5247ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;//ILB-459
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILB-459
import com.csc.smart400framework.dataaccess.model.Itempf;//ILB-459
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;//ILB-459
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Death Claim - Life Selection.
*
*  This transaction, Life Selection, is  selected  from  the
*  Claims sub-menu S6319/P6319. It is entered into first, in
*  order that the correct life may be  selected, if there is
*  only one life, then it will default to this life.
*
*
*     Initialise
*     ----------
*
*  Clear the subfile ready for loading.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To obtain the relevant life :
*
*  BEGN using LIFECLM  for  this  contract and only lives
*  with a valid flag of '1' are selected (defined in logical
*  view).
*
*  - one line is written to  the subfile for each life read
*    from the LIFECLM file.
*
*  - check the status of  each life against the status code
*    applicable to it on  T5679.  If  the  codes  do not
*    match, then protect  the  selection  field  on that
*    life, i.e. this life may not be returned.
*
*  - if there is only  one life which can be selected, then
*    return this life  as  the  selected life and do not
*    display the life selection screen S5247.
*
*  Obtain the  necessary  descriptions  by  calling 'DESCIO'
*  and output the descriptions of the statuses to the screen.
*
*  Only read the  Contract header (function RETRV) if the
*  screen is going to be returned. Read the relevant data
*  necessary for obtaining   the   status   description,
*  the   Owner,   CCD, Paid-to-date and the Bill-to-date.
*
*  Read the client  details record and use the relevant
*  copybook in order to format the required names.
*
*  In all cases, load all  pages required in the subfile and
*  set the subfile indicator to no.
*
*  The above details are returned to the user.
*
*
*     Validation
*     ----------
*
*  If 'KILL' was entered, then skip the remainder of the
*  validation and blank out the correct program on the
*  stack and exit from the program.
*
*  Read all modified  records and if more than one selection
*  was made return the  screen  to the user requesting that
*  only one life be selected.  Likewise, return a message if
*  no selection was made, as one life must be selected.
*
*
*     Updating
*     --------
*
*  If the KILL function key was pressed, skip the updating,
*  blank out the correct program on the stack and exit from
*  the program.
*
*  For the life selected do a KEEPS on this life.
*
*  Next Program
*  ------------
*
*  For KILL or 'Enter', add 1 to the program pointer and exit.
*
*
*  Modifications
*  -------------
*
*  Rewrite P5247 with the above processing included.
*
*  Include the following logical views:
*
*            - CHDRCLM
*            - LIFECLM
*****************************************************************
* </pre>
*/
public class P5247 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5247");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected PackedDecimalData wsaaActualWritten = new PackedDecimalData(3, 0).init(ZERO);
	protected PackedDecimalData wsaaNoOfSelect = new PackedDecimalData(3, 0).init(ZERO);
	protected ZonedDecimalData wsaaRrn = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	protected PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
		/* ERRORS */
	private String e304 = "E304";
	protected String h070 = "H070";
	protected String h071 = "H071";
		/* TABLES */
	private String t5688 = "T5688";
	protected String t5680 = "T5680";
	private String t5679 = "T5679";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
		/* FORMATS */
	private String chdrclmrec = "CHDRCLMREC";
	private String lifeclmrec = "LIFECLMREC";
		/*Claims Contract Header*/
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	protected DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Claims Life Record*/
	private LifeclmTableDAM lifeclmIO = new LifeclmTableDAM();
	protected T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	protected S5247ScreenVars sv = ScreenProgram.getScreenVars( S5247ScreenVars.class);
	
	//ILIFE-7323 starts
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class); 
	private List<Covrpf> covrpfList;
	private Covrpf covrpf = null;
	//private ErrorsInner errorsInner = new ErrorsInner();//ILB-459
	//ILIFE-7323 ends
	
	//ILB-459 start
	protected LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	protected List<Lifepf> LifepfList;
	protected Lifepf lifepf = new Lifepf();
	protected Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	protected Clntpf clntpf= new Clntpf();
	protected ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	protected List<Clntpf> clntpfList = null;
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private FormatsInner formatsInner = new FormatsInner();
	protected Iterator<Lifepf> iteratorLife;
	Iterator<Clntpf> iteratorClnt; //ILB-459
	 protected static final String e351 = "E351";
	 protected static final String RF54 = "RF54";
	//ILB-459 end
	//ILJ-49 Starts
	 private boolean cntDteFlag = false;
	 private String cntDteFeature = "NBPRP113";
	//ILJ-49 End
	
	
	
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		search1510, 
		exit1590, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		exit3090
	}

	public P5247() {
		super();
		screenVars = sv;
		new ScreenModel("S5247", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			beginLifeFile1020();
			loadSubfile1030();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		wsaaActualWritten.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5247", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		
		//ILB-459 start
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrclmIO.setFunction(varcom.retrv);
			chdrclmIO.setFormat(formatsInner.chdrclmrec);
			SmartFileCode.execute(appVars, chdrclmIO);
			if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrclmIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrclmIO.getChdrcoy().toString(), chdrclmIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		//ILB-459 end
	}

protected void beginLifeFile1020()
	{
	//ILB-459 start
		LifepfList = lifepfDAO.searchLifeRecordByChdrNum(wsspcomn.company.toString(),chdrpf.getChdrnum());/* IJTI-1523 */
		
		if(LifepfList == null || LifepfList.size() <= 0 ){
			fatalError600();
		}
		//ILB-459 end
	}

protected void loadSubfile1030()
	{
	//ILB-459 start
		for(Lifepf life : LifepfList){
			if (isNE(life.getJlife(),"00")) {
				sv.jlife.set(SPACES);
			}
			else {
				sv.jlife.set(life.getJlife());
			}
			sv.life.set(life.getLife());
			
			clntpf.setClntpfx("CN");
			clntpf.setClntcoy(wsspcomn.fsuco.toString());
			sv.lifcnum.set(life.getLifcnum());
			clntpf.setClntnum(life.getLifcnum());
			clntpfList = clntpfDAO.readClientpfData(clntpf);
			
			if(clntpfList==null || (clntpfList!=null && clntpfList.size()==0))
			{
				return;
			}
			for(Clntpf clntpf :clntpfList)
			{
				plainname(clntpf);
			}
		sv.lifename.set(wsspcomn.longconfname);
		checkStatus1400(life);
		descIO.setDescitem(life.getStatcode());
		descIO.setDesctabl(t5680);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.stdescsh.set(descIO.getLongdesc());
		}
		else {
			sv.stdescsh.fill("?");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5247", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			wsaaActualWritten.add(1);
		}
		}//ILB-459 
		if (isGT(wsaaActualWritten,1)) {
			fillScreen1200();
		}
	}

//ILB-459 end
protected void fillScreen1200()
	{
		headings1210();
	}

protected void headings1210()
	{
	////ILB-459 start
	sv.occdate.set(chdrpf.getOccdate());
	sv.chdrnum.set(chdrpf.getChdrnum());
	sv.cnttype.set(chdrpf.getCnttype());
	descIO.setDescitem(chdrpf.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrpf.getCownnum());
		getClientDetails1700();
		sv.btdate.set(chdrpf.getBtdate());
		sv.ptdate.set(chdrpf.getPtdate());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		//ILB-459 end
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
	}

protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

//ILB-459 start
protected void checkStatus1400(Lifepf life)
	{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl("T5679");
	itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	
	if (itempf == null) {
		syserrrec.statuz.set("MRNF");
		fatalError600();
	}
	t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	wsaaSub.set(ZERO);
	lookForStat1500(life);
	}

protected void lookForStat1500(Lifepf life)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1505();
				}
				case search1510: {
					search1510(life);
				}
				case exit1590: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
//ILB-459 end
protected void para1505()
	{
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
	}
//ILB-459 start
protected void search1510(Lifepf life)
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,6)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit1590);
		}
		if (isEQ(life.getJlife(),"01")
		&& isNE(life.getStatcode(),t5679rec.jlifeStat[wsaaSub.toInt()])) {
			goTo(GotoLabel.search1510);
		}
		if (isEQ(life.getJlife(),"00")
		&& isNE(life.getStatcode(),t5679rec.lifeStat[wsaaSub.toInt()])) {
			goTo(GotoLabel.search1510);
		}
	}
//ILB-459 end
protected void getClientDetails1700()
	{
		read1710();
	}

protected void read1710()
	{
	//ILB-459 start
	clntpf.setClntpfx("CN");
	clntpf.setClntcoy(wsspcomn.fsuco.toString());
	clntpf.setClntnum(chdrpf.getCownnum());/* IJTI-1523 */
	clntpfList = clntpfDAO.readClientpfData(clntpf);
	
	if(clntpfList==null || (clntpfList!=null && clntpfList.size()==0))
	{
		sv.ownernumErr.set(e304);
		sv.ownernum.set(SPACES);
	}
	else {
		iteratorClnt=clntpfList.iterator();
		while (iteratorClnt.hasNext()) {
			Clntpf clntpf = (Clntpf)iteratorClnt.next();
			plainname(clntpf);
		}
		
		sv.ownernum.set(wsspcomn.longconfname);
	}
	//ILB-459 end
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()	
	{
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(),"C")) {
		corpname(clntpf);
		goTo(GotoLabel.lgnmExit);
	}
	wsspcomn.longconfname.set(clntpf.getSurname());
	//ILB-459 start
	}

protected void plainname(Clntpf clntpf)
	{
	wsspcomn.longconfname.set(SPACES);
	if (clntpf.getClttype().equals("C")) {
		corpname(clntpf);
		return ;
	}
	if (isNE(clntpf.getGivname(), SPACES)) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getSurname(), "  ");
		stringVariable1.addExpression(", ");
		stringVariable1.addExpression(clntpf.getGivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
	}
	else {
		wsspcomn.longconfname.set(clntpf.getSurname());
	}
	}

//ILB-459 end
protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
	//ILB-459 start
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(),"C")) {
		corpname(clntpf);
		goTo(GotoLabel.payeeExit);
	}
	if (isEQ(clntpf.getEthorig(),"1")) {
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntpf.getSalutl(), "  "));
		stringVariable1.append(". ");
		stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		goTo(GotoLabel.payeeExit);
	}
	StringBuilder stringVariable2 = new StringBuilder();
	stringVariable2.append(delimitedExp(clntpf.getSalutl(), "  "));
	stringVariable2.append(". ");
	stringVariable2.append(delimitedExp(clntpf.getGivname(), "  "));
	stringVariable2.append(" ");
	stringVariable2.append(delimitedExp(clntpf.getSurname(), "  "));
	wsspcomn.longconfname.setLeft(stringVariable2.toString());
	//ILB-459 end
	}

//ILB-459 start
protected void corpname(Clntpf clntpf)
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntpf.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clntpf.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}
//ILB-459 end

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsaaNoOfSelect.set(0);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(wsaaActualWritten,1)) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			startSubfile2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void startSubfile2060()
	{
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5247", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaRrn.set(scrnparams.subfileRrn);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		if (isGT(wsaaNoOfSelect,1)) {
			scrnparams.errorCode.set(h070);
			wsspcomn.edterror.set("Y");
			scrnparams.subfileRrn.set(1);
			goTo(GotoLabel.exit2090);
		}
		if (isLT(wsaaNoOfSelect,1)) {
			scrnparams.errorCode.set(h071);
			scrnparams.subfileRrn.set(1);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		
		scrnparams.subfileRrn.set(wsaaRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S5247", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		//ILIFE-7323 starts
		covrpf = new Covrpf();
		//ILB-459
		iteratorLife = LifepfList.iterator();
		while(iteratorLife.hasNext()) {
			Lifepf lifepf = (Lifepf)iteratorLife.next();
			covrpf.setChdrcoy(lifepf.getChdrcoy());/* IJTI-1523 */
			covrpf.setChdrnum(lifepf.getChdrnum());/* IJTI-1523 */
		}
		
		covrpf.setLife(sv.life.toString());
		covrpf.setPlanSuffix(0);
		covrpf.setValidflag("1");
		covrpfList = covrpfDAO.selectCovrRecord(covrpf);
		if(covrpfList.isEmpty()){
			
			wsspcomn.edterror.set("Y");
			scrnparams.errorCode.set(e351);
		}
		  
		 validateDeceased();  // ILIFE-7321
		//ILIFE-7323 ends
	}

protected void validateSubfile2600()
	{
		/*VALIDATION*/
		if (isNE(sv.select,SPACES)) {
			wsaaNoOfSelect.add(1);
			wsaaRrn.set(scrnparams.subfileRrn);
		}
		/*READ-NEXT-RECORD*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5247", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
 
protected void validateDeceased()
{
	wsspcomn.edterror.set(varcom.oK);
	scrnparams.function.set(varcom.sstrt);
	processScreen("S5247", sv);
	if (isNE(scrnparams.statuz,varcom.oK)
	&& isNE(scrnparams.statuz,varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	wsaaRrn.set(scrnparams.subfileRrn);
	while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
		validateDeceasedSubfile2600();
	}
	 

}
 
protected void validateDeceasedSubfile2600()
{
	/*VALIDATION*/
	if (isNE(sv.select,SPACES)) 
	{
		
		if(isEQ(sv.stdescsh,"Deceased"))
		{
			wsspcomn.edterror.set("Y");
			scrnparams.errorCode.set(RF54);  // ILIFE-7321
			sv.select.set(SPACES);
		}		 
		 
	}
	scrnparams.function.set(varcom.supd);
	screenIo9000();
	/*READ-NEXT-RECORD*/
	scrnparams.function.set(varcom.srdn);
	processScreen("S5247", sv);
	if (isNE(scrnparams.statuz,varcom.oK)
	&& isNE(scrnparams.statuz,varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	/*EXIT*/
}
protected void screenIo9000()
{
	processScreen("S5247", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}
 
 
protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5247", sv);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			if (isNE(sv.select,SPACES)) {
				break;
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("S5247", sv);
			if (isNE(scrnparams.statuz,varcom.oK)
					&& isNE(scrnparams.statuz,varcom.endp)) {
						syserrrec.statuz.set(scrnparams.statuz);
						fatalError600();
			}
		}
		//ILB-459 start
		lifepf.setChdrcoy(chdrpf.getChdrcoy().toString());//ILB-459
		lifepf.setChdrnum(chdrpf.getChdrnum());//ILB-459
		lifepf.setLife(sv.life.toString());
		lifepf.setValidflag("1");
/*		LifepfList = lifepfDAO.selectLifepfRecord(lifepf);
		
		if(LifepfList == null || LifepfList.size() <= 0 ){
			fatalError600();
		}
*/
		if (isEQ(sv.jlife,SPACES)) {
			lifepf.setJlife("00");
		}
		else {
			lifepf.setJlife(sv.jlife.toString());
		}
		LifepfList = lifepfDAO.findByKey(lifepf);		
		if(LifepfList == null || LifepfList.size() <= 0 ){
			fatalError600();
		}
		iteratorLife = LifepfList.iterator();
		if(iteratorLife.hasNext())
		{
			lifepf = iteratorLife.next();
			lifepfDAO.setCacheObject(lifepf);
		}
		//ILB-459 end
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

//ILB-459 start
/*private static final class ErrorsInner { 
	private FixedLengthStringData e351 = new FixedLengthStringData(4).init("E351");
	private FixedLengthStringData RF54   = new FixedLengthStringData(4).init("RF54");
}*/

private static final class FormatsInner {
	private FixedLengthStringData chdrclmrec = new FixedLengthStringData(10).init("CHDRCLMREC");
}
//ILB-459 end
}
