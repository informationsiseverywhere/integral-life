package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.csc.life.terminationclaims.dataaccess.dao.MatypfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Matypf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MatypfDAOImpl  extends  BaseDAOImpl<Matypf> implements MatypfDAO{
	 private static final Logger LOGGER = LoggerFactory.getLogger(MatypfDAOImpl.class);

	/**
	 * This method will delete all the data.
	 */
	public void deleteMatypf(String tableName) {
		StringBuilder sql = new StringBuilder("DELETE FROM ");
		sql.append(tableName);
		//IJTI-306 START
		PreparedStatement stmt=null;
		try
		{
			stmt = getPrepareStatement(sql.toString());
			stmt.executeUpdate();
			//IJTI-306 END		
		}
		catch(SQLException e)
		{
			LOGGER.error("deleteMatypf()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(stmt,null);				
		}
	}
	
	public boolean insertMatyTempRecords(List<Matypf> insertmatypfList, String tempTableName){
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO " );
		sb.append(tempTableName);
		sb.append("(COWNUM, CHDRCOY, CHDRNUM,  LIFE, JLIFE, COVERAGE, RIDER, CRTABLE, VALIDFLAG, TRANNO, ACTVALUE,MATURITYCALCMETH,SINGLEPREMIND) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?) ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;	
		boolean isUpdated = false;
		try{
			ps = getPrepareStatement(sb.toString());	
			for (Matypf maty : insertmatypfList) {
				
				ps.setString(1, maty.getCownum());   
				ps.setString(2,maty.getChdrcoy());
				ps.setString(3, maty.getChdrnum());
				ps.setString(4, maty.getLife());
				ps.setString(5, maty.getJlife());
				ps.setString(6, maty.getCoverage());
				ps.setString(7, maty.getRider());
				ps.setString(8, maty.getCrtable());
				ps.setString(9, maty.getValidflag());
				ps.setInt(10, maty.getTranno());
				ps.setBigDecimal(11, maty.getActvalue());
				ps.setString(12,maty.getMaturitycalcmeth());
				ps.setString(13, maty.getSinglepremind());			    
			    ps.addBatch();				
			}		
			ps.executeBatch();
			isUpdated = true;
		}catch (SQLException e) {
			LOGGER.error("insertmatyTempRecords()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}	
		return isUpdated;
	}	
	
	public int getMatyCount(String tableName) {
		int count = 0;
		String query = new String("SELECT COUNT(*) FROM " + tableName);
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			stmt = getPrepareStatement(query);/* IJTI-1523 */
			rs = stmt.executeQuery();
			rs.next();
			count = rs.getInt(1);
		} catch (SQLException e) {
			LOGGER.error("getMatycount()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
		}

		return count;
	}

	@Override
	public List<Matypf> getMatypfList(String tableName) {
		
		String query = new String("SELECT COWNUM,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,VALIDFLAG,TRANNO,ACTVALUE,MATURITYCALCMETH,SINGLEPREMIND FROM " + tableName);
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Matypf> list = new ArrayList<Matypf>();
		Matypf matypf = null;
		
		try {
			stmt = getPrepareStatement(query);/* IJTI-1523 */
			rs = stmt.executeQuery();
			while (rs.next()) {
				matypf = new Matypf();
				matypf.setCownum(rs.getString(1));
				matypf.setChdrcoy(rs.getString(2));
				matypf.setChdrnum(rs.getString(3));
				matypf.setLife(rs.getString(4));
				matypf.setJlife(rs.getString(5));
				matypf.setCoverage(rs.getString(6));
				matypf.setRider(rs.getString(7));
				matypf.setCrtable(rs.getString(8));
				matypf.setValidflag(rs.getString(9));
				matypf.setTranno(rs.getInt(10));
				matypf.setActvalue(rs.getBigDecimal(11));
				matypf.setMaturitycalcmeth(rs.getString(12));
				matypf.setSinglepremind(rs.getString(13));
				
				list.add(matypf);
			}
			
		} catch (SQLException e) {
			LOGGER.error("getMatypfList()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
		}
		return list;
	}
	
	public Matypf getMatyRecord(String chdrcoy,String chdrnum,String coverage,String rider,String life,String tableName){
		StringBuilder sqlMatySelect1 = new StringBuilder(
                "SELECT COWNUM,CHDRCOY,CHDRNUM,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,VALIDFLAG,TRANNO,ACTVALUE,MATURITYCALCMETH,SINGLEPREMIND ");
        sqlMatySelect1.append(" FROM " + tableName);
        sqlMatySelect1.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND COVERAGE = ? and RIDER = ? and LIFE = ? AND VALIDFLAG = '1' ");
    
        sqlMatySelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC,UNIQUE_NUMBER DESC ");
     
   	
        PreparedStatement ps = null;
   	 	ResultSet rs = null;
   	 	Matypf matypf = null;
    	 try {
    		 ps = getPrepareStatement(sqlMatySelect1.toString());
    		 ps.setString(1, chdrcoy);
 			 ps.setString(2, chdrnum);
 			 ps.setString(3, coverage);
 			 ps.setString(4, rider);
 			 ps.setString(5, life);
 			
 			 rs =  ps.executeQuery();
			 while(rs.next()){
                matypf = new Matypf();
                matypf.setCownum(rs.getString(1));
                matypf.setChdrcoy(rs.getString(2));
                matypf.setChdrnum(rs.getString(3));
                matypf.setLife(rs.getString(4));
                matypf.setJlife(rs.getString(5));
                matypf.setCoverage(rs.getString(6));
                matypf.setRider(rs.getString(7));
                matypf.setCrtable(rs.getString(8));
                matypf.setValidflag(rs.getString(9));
                matypf.setTranno(rs.getInt(10));
                matypf.setActvalue(rs.getBigDecimal(11));
                matypf.setMaturitycalcmeth(rs.getString(12));
                matypf.setSinglepremind(rs.getString(13));
				}
			} catch (SQLException e) {
	            LOGGER.error("getMatyRecord()", e);//IJTI-1561
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
    	 return matypf;
	    	 }

	

	}
	
	


