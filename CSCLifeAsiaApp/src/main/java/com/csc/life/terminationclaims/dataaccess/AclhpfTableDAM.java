package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AclhpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:36
 * Class transformed from ACLHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AclhpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 118;
	public FixedLengthStringData aclhrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData aclhpfRecord = aclhrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(aclhrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(aclhrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(aclhrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(aclhrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(aclhrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(aclhrec);
	public PackedDecimalData rgpynum = DD.rgpynum.copy().isAPartOf(aclhrec);
	public PackedDecimalData recvdDate = DD.zclmrecd.copy().isAPartOf(aclhrec);
	public PackedDecimalData incurdt = DD.incurdt.copy().isAPartOf(aclhrec);
	public PackedDecimalData gcadmdt = DD.gcadmdt.copy().isAPartOf(aclhrec);
	public PackedDecimalData dischdt = DD.dischdt.copy().isAPartOf(aclhrec);
	public FixedLengthStringData diagcde = DD.diagcde.copy().isAPartOf(aclhrec);
	public FixedLengthStringData zdoctor = DD.zdoctor.copy().isAPartOf(aclhrec);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(aclhrec);
	public PackedDecimalData tclmamt = DD.tclmamt.copy().isAPartOf(aclhrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(aclhrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(aclhrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(aclhrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AclhpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AclhpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AclhpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AclhpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AclhpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AclhpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AclhpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ACLHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"RGPYNUM, " +
							"ZCLMRECD, " +
							"INCURDT, " +
							"GCADMDT, " +
							"DISCHDT, " +
							"DIAGCDE, " +
							"ZDOCTOR, " +
							"ZMEDPRV, " +
							"TCLMAMT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     crtable,
                                     rgpynum,
                                     recvdDate,
                                     incurdt,
                                     gcadmdt,
                                     dischdt,
                                     diagcde,
                                     zdoctor,
                                     zmedprv,
                                     tclmamt,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		rgpynum.clear();
  		recvdDate.clear();
  		incurdt.clear();
  		gcadmdt.clear();
  		dischdt.clear();
  		diagcde.clear();
  		zdoctor.clear();
  		zmedprv.clear();
  		tclmamt.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAclhrec() {
  		return aclhrec;
	}

	public FixedLengthStringData getAclhpfRecord() {
  		return aclhpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAclhrec(what);
	}

	public void setAclhrec(Object what) {
  		this.aclhrec.set(what);
	}

	public void setAclhpfRecord(Object what) {
  		this.aclhpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(aclhrec.getLength());
		result.set(aclhrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}