package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:46
 * Description:
 * Copybook name: REGRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData regrKey = new FixedLengthStringData(64).isAPartOf(regrFileKey, 0, REDEFINE);
  	public FixedLengthStringData regrProgname = new FixedLengthStringData(10).isAPartOf(regrKey, 0);
  	public FixedLengthStringData regrExreport = new FixedLengthStringData(1).isAPartOf(regrKey, 10);
  	public FixedLengthStringData regrChdrnum = new FixedLengthStringData(8).isAPartOf(regrKey, 11);
  	public FixedLengthStringData regrRgpytype = new FixedLengthStringData(2).isAPartOf(regrKey, 19);
  	public FixedLengthStringData regrCrtable = new FixedLengthStringData(4).isAPartOf(regrKey, 21);
  	public PackedDecimalData regrTranno = new PackedDecimalData(5, 0).isAPartOf(regrKey, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(regrKey, 28, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}