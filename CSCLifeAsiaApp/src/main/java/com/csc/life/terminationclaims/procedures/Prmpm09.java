/*
 * File: Prmpm09.java
 * Date: 30 August 2009 1:58:31
 * Author: Quipoz Limited
 * 
 * Class transformed from PRMPM09.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.tablestructures.T5658rec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5659rec;
import com.csc.life.productdefinition.tablestructures.T5664rec;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.recordstructures.Pr676cpy;
import com.csc.life.terminationclaims.tablestructures.Tr686rec;
import com.csc.life.terminationclaims.tablestructures.Tr687rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*PROGRAM-ID.     ZRPM09.
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  PREMIUM CALCULATION METHOD 09 -
*  AGE BASED CALCULATION SUBROUTINE
*  CLONED FROM PRMPM04 -- FOR HOSPITAL PLAN BENEFIT
*  NO STAFF DISCOUNT APPLY HERE.
* PROCESSING.
* ----------
*
* Initialise all working storage fields and set keys to read
* tables. Include a table (occurs 8) to hold the Options/Extras
* (LEXT) record details.
*
* Build a key.  This key (see below) will read table T5664.
* This table contains the parameters to be used in the
* calculation of the Basic Annual Premium for
* Coverage/Rider components.
*
* The key is a concatenation of the following fields:-
*
* Coverage/Rider table code
* Mortality class  (Plan code)
* Lives No.        (In this case we use CPRM-REASIND as we do not
*                   want to change the copy book of PREMIUMREC.)
*
* Access the required table by reading the table directly (ITDM).
* The contents of the table are then stored. This table is dated
* use:
*
*  1) Rating Date
*
* CALCULATE-BASIC-ANNNUAL-PREMIUM (and apply age rates)
* (Age, Sex & Duration taken from linkage)
*
* Obtain the age rates from the (LEXT) record.
*
*  - read all the LEXT records for this contract, life and
*  coverage into the working-storage table. Compute the
*  adjusted age as being the summation of the LEXT age
*  rates plus the ANB @ RCD.
*
* Use the age calculated above to access the table T5664 and
* check the following:
*
*  - that the basic annual premium (indexed by year) from
*  the T5664 table is not zero. If it is zero, then display
*  an error message and skip the additional procedures.
*  Otherwise store the premium as the (BAP).
*
* - we should now have an age rated BAP.
*
* APPLY-RATE-PER-MILLE-LOADINGS
*
* - sum the rates per mille from the LEXT W/S table.
*
*  - add rates per mille to the BAP
*
* - we should now have a BAP with rates / mille applied.
*
* APPLY-DISCOUNT.
*
* Access the discount table T5659 using the key:-
*
* - Discount method from T5664 concatenated with currency.
*
*  - check the sum insured against the volume band ranges
*  and when within a range store the discount amount.
*
*  - compute the BAP as the BAP - discount
*
* - we should now have a BAP with discount applied.
*
* APPLY-PREMIUM-UNIT
*
* - Obtain the risk-unit from T5664
*
*  - multiply BAP by the sum-insured and divide
*    by the risk-unit
*
* - we should now have a BAP with premium applied.
*
* APPLY-PERCENTAGE-LOADINGS
*
* - from the LEXT working-storage (W/S) table apply the
* percentage loadings. For each loading entry on the table
* compute the BAP as follows:
*
*  BAP = BAP * loading percentage / 100.
*
* - we should now have a loaded BAP.
*
* CALCULATE-INSTALMENT-PREMIUM.
*
* Determine which billing frequency to use.
*
* compute the basic-instalment-premium (BIP) as:-
*
* basic-premium * factor (FACTOR is from T5664).
*
* CALCULATE-ROUNDING.
*
* - round up depending on the rounding factor (obtained from
* the T5659 table).
*
* - if the prem-unit from T5664 is greater than zero, then
* compute the BIP as the rounded number / the premium-unit
* (from T5664). The premium unit is the quantity in which the
* currency is denominated.
*
* CALCULATE-THE-ANNUAL-PREMIUM.
*
* There is no need to calculate the annual premium, because
* at issue time, when the COVR records are being created from
* the COVT records, the annual premium will then be calculated.
*
* After calculate the basic premium & premium, it will calculate
* waiver premium from T5664 with the waiver code (from calling
* program PR676), mortality & live number.
* Last step is calculate the basic premium with sum( premium,
* waiver premium) & model premium.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Prmpm09 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PRMPM09";
		/* ERRORS */
	private static final String e107 = "E107";
	private static final String f264 = "F264";
	private static final String f358 = "F358";
	private static final String t070 = "T070";
	private static final String hl27 = "HL27";
	private static final String rl78 = "RL78";
		/* TABLES */
	private static final String t5664 = "T5664";
	private static final String t5659 = "T5659";
	private static final String th549 = "TH549";
	private static final String tr687 = "TR687";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String lextrec = "LEXTREC";

	private FixedLengthStringData wsbbTr687Itemkey = new FixedLengthStringData(14);
	private FixedLengthStringData wsbbTr687Itemcoy = new FixedLengthStringData(1).isAPartOf(wsbbTr687Itemkey, 0);
	private FixedLengthStringData wsbbTr687Itemtabl = new FixedLengthStringData(5).isAPartOf(wsbbTr687Itemkey, 1);
	private FixedLengthStringData wsbbTr687Crtable = new FixedLengthStringData(4).isAPartOf(wsbbTr687Itemkey, 6);
	private FixedLengthStringData wsbbTr687Benpln = new FixedLengthStringData(2).isAPartOf(wsbbTr687Itemkey, 10);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsbbTr687Itemkey, 12, FILLER).init(SPACES);

	private FixedLengthStringData wsaaTempItemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTempCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTempItemitem, 0);
	private FixedLengthStringData wsaaTempBenpln = new FixedLengthStringData(2).isAPartOf(wsaaTempItemitem, 4);

		/* WSAA-LEXT-OPPC-RECS */
	private FixedLengthStringData[] wsaaLextOppcs = FLSInittedArray (8, 3);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);

		/* WSAA-LEXT-ZMORTPCT-RECS */
	private FixedLengthStringData[] wsaaLextZmortpcts = FLSInittedArray (8, 2);
	private PackedDecimalData[] wsaaLextZmortpct = PDArrayPartOfArrayStructure(3, 0, wsaaLextZmortpcts, 0, UNSIGNED_TRUE);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaDiscountAmt = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaBap = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBip = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaWopPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaWopStPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaModalFactor = new PackedDecimalData(5, 4);
	private String wsaaBasicPremium = "";
	private String wsaaMortalityLoad = "";

	private FixedLengthStringData wsaaT5664Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5664Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5664Key, 0);
	private FixedLengthStringData wsaaT5664Benpln = new FixedLengthStringData(2).isAPartOf(wsaaT5664Key, 4);
	private FixedLengthStringData wsaaT5664Livesno = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 6);

	private FixedLengthStringData wsaaT5659Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaDisccntmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5659Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaT5659Key, 4);

	private FixedLengthStringData wsaaTh549Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh549Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh549Key, 0);
	private ZonedDecimalData wsaaTh549Zmortpct = new ZonedDecimalData(3, 0).isAPartOf(wsaaTh549Key, 4).setUnsigned();
	private FixedLengthStringData wsaaTh549Zsexmort = new FixedLengthStringData(1).isAPartOf(wsaaTh549Key, 7);
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5664rec t5664rec = new T5664rec();
	private T5658rec t5658rec = new T5658rec();
	private T5659rec t5659rec = new T5659rec();
	private Tr687rec tr687rec = new Tr687rec();
	private Premiumrec premiumrec = new Premiumrec();
	private Pr676cpy pr676cpy = new Pr676cpy();
	//ILAE-71
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
	private Tr686rec tr686rec = new Tr686rec();
	private String tr686 = "TR686";
	/*BRD-306 START */
	private PackedDecimalData wsaaPremiumAdjustTot = new PackedDecimalData(7, 0).init(0);
	boolean isLoadingAva = false;
	/*BRD-306 END */


/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		t5664120, 
		readLext210, 
		loopForAdjustedAge220, 
		checkT5664Insprm230, 
		checkSumInsuredRange430, 
		exit490, 
		calculateLoadings610, 
		calcMortLoadings910, 
		exit990, 
		calcLoadings1010, 
		calcEnd1080
	}

	public Prmpm09() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		pr676cpy.parmRecord = convertAndSetParam(pr676cpy.parmRecord, parmArray, 1);
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		premiumrec.statuz.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaBasicPremium = "N";
		initialize100();
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			basicAnnualPremium200();
			/*BRD-306 START */
			premiumrec.adjustageamt.set(wsaaBap);
			/*BRD-306 END */
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			ratesPerMillieLoadings300();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			volumeDiscountBap1400();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			premiumUnit500();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			mortalityLoadings900();
		}
		/*  The Special Term % loading is always applied for the Waiver Of*/
		/*  Premium for Indonesia Manulife*/
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			instalmentPremium700();
		}
		if (isEQ(premiumrec.statuz, varcom.oK)) {
			rounding800();
		}
		/*BRD-306 START */
		if (isEQ(premiumrec.statuz, "****")) {
			premiumAdjustedLoadings900();
		}
		/*BRD-306 END */
		wsaaBasicPremium = "Y";
		if (isEQ(premiumrec.statuz, "****")) {
			basicAnnualPremium200();
			/*BRD-306 START */
			//Calculate adjusted age amount
			compute(premiumrec.adjustageamt, 2).set(sub(premiumrec.adjustageamt, wsaaBap));
			/*BRD-306 END */
		}
		if (isEQ(premiumrec.statuz, "****")) {
			volumeDiscountBap1400();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			premiumUnit500();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			instalmentPremium700();
		}
		if (isEQ(premiumrec.statuz, "****")) {
			rounding800();
		}
		/* Calculate the loaded premium as the gross premium minus the*/
		/* basic premium.*/
		if (isEQ(premiumrec.statuz, "****")) {
			if (isEQ(pr676cpy.waiverprem, "Y")) {
				checkWaiverPremium900();
				wsaaWopPrem.set(wsaaBap);
				calcPercLoadingsWop1000();
				compute(premiumrec.calcPrem, 4).set(add(premiumrec.calcPrem, (mult(wsaaWopStPrem, wsaaModalFactor))));
				premiumrec.calcBasPrem.set(premiumrec.calcPrem);
			}
		}
		if (isEQ(premiumrec.statuz, "****")) {
			checkTr687Premunit950();
		}
		
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialize100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para100();
				case t5664120: 
					t5664120();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para100()
	{
		/* Initialise all working storage fields and set keys to read*/
		/* tables. Include a table (occurs 8) to hold the Options/Extras*/
		/* (LEXT) record details.*/
		wsaaRatesPerMillieTot.set(ZERO);
		/*BRD-306 START */
		wsaaPremiumAdjustTot.set(ZERO);
		/*BRD-306 END */
		wsaaBap.set(ZERO);
		wsaaBip.set(ZERO);
		wsaaAdjustedAge.set(ZERO);
		wsaaDiscountAmt.set(ZERO);
		wsaaAgerateTot.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaWopStPrem.set(ZERO);
		wsaaT5659Key.set(SPACES);
		/*BRD-306 START */
		premiumrec.fltmort.set(ZERO);
		premiumrec.loadper.set(ZERO);
		/*BRD-306 END */
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			clearLextRecs110();
		}
		goTo(GotoLabel.t5664120);
	}

protected void clearLextRecs110()
	{
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(ZERO);
		wsaaLextZmortpct[wsaaSub.toInt()].set(ZERO);
	}

protected void t5664120()
	{
	//ILAE - 71 STARTS
	if(isEQ(premiumrec.benpln,SPACE))
	{		
		hbnfIO.setChdrcoy(premiumrec.chdrChdrcoy);
		hbnfIO.setChdrnum(premiumrec.chdrChdrnum);
		//ILIFE-2629--Key columns set to read record from HBNF table--starts
		hbnfIO.setLife(premiumrec.lifeLife);
		hbnfIO.setCoverage(premiumrec.covrCoverage);
		hbnfIO.setRider(premiumrec.covrRider);
		//ILIFE-2629--end
		hbnfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isEQ(hbnfIO.getStatuz(),varcom.oK))
		 {
			
			premiumrec.benpln.set(hbnfIO.getBenpln());
			pr676cpy.waiverprem.set(hbnfIO.getWaiverprem());
			pr676cpy.livesno.set(hbnfIO.getLivesno());
			
			itdmIO.setDataKey(SPACES);
			itdmIO.setParams(SPACES);
			itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
			itdmIO.setItemtabl(tr686);
			itdmIO.setItemitem(premiumrec.crtable);
			itdmIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itdmIO);
			if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
				tr686rec.tr686Rec.set(itdmIO.getGenarea());
				pr676cpy.waiverCrtable.set(tr686rec.waivercode);
			}
			else
			{
			pr676cpy.waiverCrtable.set(SPACES);
			}
			
		 }
	 }
//ILAE - 71 ENDS
		/* Build a key.*/
		/* This key (see below)*/
		/* will read table T5664. This table contains the parameters to be*/
		/* used in the calculation of the Basic Annual Premium for*/
		/* Coverage/Rider components.*/
		/* The key is a concatenation of the following fields:-*/
		/* Coverage/Rider table code*/
		/* Mortality Class*/
		/* Lives No.*/
		/* Access the required table by reading the table directly (ITDM).*/
		/* The contents of the table are then stored. This table is dated*/
		/* use:*/
		/*  1) Rating Date*/
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5664);
		wsaaT5664Crtable.set(premiumrec.crtable);
		/*    MOVE CPRM-MORTCLS           TO WSAA-T5664-PLAN-CODE.         */
		wsaaT5664Benpln.set(premiumrec.benpln);
		wsaaT5664Livesno.set(pr676cpy.livesno);
		itdmIO.setItemitem(wsaaT5664Key);
		if (isEQ(premiumrec.ratingdate, ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaT5664Key, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5664)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			premiumrec.statuz.set(f358);
		}
		else {
			t5664rec.t5664Rec.set(itdmIO.getGenarea());
		}
	}

protected void basicAnnualPremium200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setupLextKey200();
				case readLext210: 
					readLext210();
				case loopForAdjustedAge220: 
					loopForAdjustedAge220();
				case checkT5664Insprm230: 
					checkT5664Insprm230();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setupLextKey200()
	{
		/* If calculating the basic premium, do not adjust the age.*/
		if (isEQ(wsaaBasicPremium, "Y")) {
			wsaaAdjustedAge.set(premiumrec.lage);
			goTo(GotoLabel.checkT5664Insprm230);
		}
		/* Obtain the age rates from the (LEXT) record.*/
		/*  - read all the LEXT records for this contract, life and*/
		/*  coverage into the working-storage table. Compute the*/
		/*  adjusted age as being the summation of the LEXT age*/
		/*  rates plus the ANB @ RCD.*/
		lextIO.setChdrcoy(premiumrec.chdrChdrcoy);
		lextIO.setChdrnum(premiumrec.chdrChdrnum);
		lextIO.setLife(premiumrec.lifeLife);
		lextIO.setCoverage(premiumrec.covrCoverage);
		lextIO.setRider(premiumrec.covrRider);
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		wsaaSub.set(0);
	}

protected void readLext210()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError9000();
		}
		if (isEQ(lextIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.loopForAdjustedAge220);
		}
		if (isEQ(lextIO.getChdrcoy(), premiumrec.chdrChdrcoy)
		&& isEQ(lextIO.getChdrnum(), premiumrec.chdrChdrnum)
		&& isEQ(lextIO.getLife(), premiumrec.lifeLife)
		&& isEQ(lextIO.getCoverage(), premiumrec.covrCoverage)
		&& isEQ(lextIO.getRider(), premiumrec.covrRider)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.loopForAdjustedAge220);
		}
		/*  Skip any expired special terms.*/
		lextIO.setFunction(varcom.nextr);
		if (isLTE(lextIO.getExtCessDate(), premiumrec.reRateDate)) {
			goTo(GotoLabel.readLext210);
		}
		wsaaSub.add(1);
		wsaaLextOppc[wsaaSub.toInt()].set(lextIO.getOppc());
		wsaaLextZmortpct[wsaaSub.toInt()].set(lextIO.getZmortpct());
		wsaaRatesPerMillieTot.add(lextIO.getInsprm());
		/*BRD-306 START */
		wsaaPremiumAdjustTot.add(lextIO.getPremadj());
		/*BRD-306 END */
		wsaaAgerateTot.add(lextIO.getAgerate());
		goTo(GotoLabel.readLext210);
	}

protected void loopForAdjustedAge220()
	{
		compute(wsaaAdjustedAge, 0).set((add(wsaaAgerateTot, premiumrec.lage)));
	}

protected void checkT5664Insprm230()
	{
		/* Use the age calculated above to access the table T5664 and*/
		/* check the following:*/
		/*  - that the basic annual premium (indexed by year) from*/
		/*  the T5664 table is not zero. If it is zero, then display*/
		/*  an error message and skip the additional procedures.*/
		/*  Otherwise store the premium as the (BAP).*/
		/*    IF WSAA-ADJUSTED-AGE        < 1 OR                           */
		if (isLT(wsaaAdjustedAge, 0)
		|| isGT(wsaaAdjustedAge, 110)) {
			premiumrec.statuz.set(e107);
			return ;
		}
		/*  Check for adjusted age = 0; move the premium rate              */
		if (isEQ(wsaaAdjustedAge, 0)) {
			if (isEQ(t5664rec.insprem, ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(t5664rec.insprem);
			}
			return ;
		}
		/*  Note - only 99 out of the 100 rates fit in the occurs table.*/
		/*    IF WSAA-ADJUSTED-AGE = 100                                   */
		/*       IF T5664-INSTPR = ZERO                                    */
		/*          MOVE E107                TO CPRM-STATUZ                */
		/*       ELSE                                                      */
		/*          MOVE T5664-INSTPR        TO WSAA-BAP                   */
		/*  Extend the age band to 110.                                    */
		if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)) {
			if ((setPrecision(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], 0)
			&& isEQ(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], ZERO))) {
				premiumrec.statuz.set(e107);
			}
			else {
				compute(wsaaBap, 2).set(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()]);
			}
		}
		else {
			if (isEQ(t5664rec.insprm[wsaaAdjustedAge.toInt()], ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(t5664rec.insprm[wsaaAdjustedAge.toInt()]);
			}
		}
	}

protected void ratesPerMillieLoadings300()
	{
		/*PARA*/
		/* APPLY-RATE-PER-MILLE-LOADINGS*/
		/* - sum the rates per mille from the LEXT W/S table.*/
		/*  - add rates per mille to the BAP*/
		compute(wsaaBap, 2).set(add(wsaaRatesPerMillieTot, wsaaBap));
		/*EXIT*/
		/*BRD-306 START */
		compute(premiumrec.rateadj, 2).set(wsaaRatesPerMillieTot);
		/*BRD-306 END */
	}

protected void volumeDiscountBap1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readT5659410();
				case checkSumInsuredRange430: 
					checkSumInsuredRange430();
				case exit490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readT5659410()
	{
		/* APPLY-DISCOUNT.*/
		/* Access the discount table T5659 using the key:-*/
		/* - Discount method from T5664 concatenated with currency.*/
		/*  - check the sum insurred against the volume band ranges*/
		/*  and when within a range store the discount amount.*/
		/*  - compute the BAP as the BAP - discount*/
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5659);
		wsaaDisccntmeth.set(t5664rec.disccntmeth);
		wsaaCurrcode.set(premiumrec.currcode);
		itdmIO.setItemitem(wsaaT5659Key);
		if (isEQ(premiumrec.ratingdate, ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5659)
		|| isNE(wsaaT5659Key, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			premiumrec.statuz.set(f264);
			goTo(GotoLabel.exit490);
		}
		t5659rec.t5659Rec.set(itdmIO.getGenarea());
		wsaaSub.set(0);
	}

protected void checkSumInsuredRange430()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 4)) {
			return ;
		}
		if (isLT(premiumrec.sumin, t5659rec.volbanfr[wsaaSub.toInt()])
		|| isGT(premiumrec.sumin, t5659rec.volbanto[wsaaSub.toInt()])) {
			goTo(GotoLabel.checkSumInsuredRange430);
		}
		else {
			wsaaDiscountAmt.set(t5659rec.volbanle[wsaaSub.toInt()]);
		}
		compute(wsaaBap, 2).set(sub(wsaaBap, wsaaDiscountAmt));
	}

protected void premiumUnit500()
	{
		/*PARA*/
		/** APPLY-PREMIUM-UNIT*/
		/** - Obtain the risk-unit from T5664*/
		/**  - multiply BAP by the sum-insured and divide*/
		/**    by the risk-unit*/
		/**    COMPUTE WSAA-BAP = ((WSAA-BAP * CPRM-SUMIN) / T5664-UNIT)   .*/
		/** - we should now have a BAP with premium applied.*/
		/*EXIT*/
	}

protected void percentageLoadings600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para600();
				case calculateLoadings610: 
					calculateLoadings610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para600()
	{
		/* APPLY-PERCENTAGE-LOADINGS*/
		/* - from the LEXT working-storage (W/S) table apply the*/
		/* percentage loadings. For each loading entry on the table*/
		/* compute the BAP as follows:*/
		/*  BAP = BAP * loading percentage / 100.*/
		wsaaSub.set(0);
	}

protected void calculateLoadings610()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 8)) {
			return ;
		}
		if (isNE(wsaaLextOppc[wsaaSub.toInt()], 0)) {
			compute(wsaaBap, 2).set((div((mult(wsaaBap, wsaaLextOppc[wsaaSub.toInt()])), 100)));
			isLoadingAva = true;
		}
		goTo(GotoLabel.calculateLoadings610);
	}

protected void instalmentPremium700()
	{
		para700();
		instalmentPrem710();
	}

protected void para700()
	{
		/* CALCULATE-INSTALMENT-PREMIUM.*/
		/* Determine which billing frequency to use.*/
		/* compute the basic-instalment-premium (BIP) as:-*/
		/* basic-premium * factor (FACTOR is from T5664).*/
		wsaaBip.set(0);
	}

protected void instalmentPrem710()
	{
		wsaaModalFactor.set(0);
		if (isEQ(premiumrec.billfreq, "01")
		|| isEQ(premiumrec.billfreq, "00")) {
			wsaaModalFactor.set(t5664rec.mfacty);
		}
		else {
			if (isEQ(premiumrec.billfreq, "02")) {
				wsaaModalFactor.set(t5664rec.mfacthy);
			}
			else {
				if (isEQ(premiumrec.billfreq, "04")) {
					wsaaModalFactor.set(t5664rec.mfactq);
				}
				else {
					if (isEQ(premiumrec.billfreq, "12")) {
						wsaaModalFactor.set(t5664rec.mfactm);
					}
					else {
						if (isEQ(premiumrec.billfreq, "13")) {
							wsaaModalFactor.set(t5664rec.mfact4w);
						}
						else {
							if (isEQ(premiumrec.billfreq, "24")) {
								wsaaModalFactor.set(t5664rec.mfacthm);
							}
							else {
								if (isEQ(premiumrec.billfreq, "26")) {
									wsaaModalFactor.set(t5664rec.mfact2w);
								}
								else {
									if (isEQ(premiumrec.billfreq, "52")) {
										wsaaModalFactor.set(t5664rec.mfactw);
									}
								}
							}
						}
					}
				}
			}
		}
		if (isEQ(wsaaModalFactor, 0)) {
			premiumrec.statuz.set(t070);
		}
		else {
			compute(wsaaBip, 4).set(mult(wsaaBap, wsaaModalFactor));
		}
	}

protected void rounding800()
	{
		/*PARA*/
		/** CALCULATE-ROUNDING.*/
		/** - if the prem-unit from T5664 is greater than zero, then*/
		/** compute the BIP as the rounded number / the premium-unit*/
		/** (from T5664). The premium unit is the quantity in which the*/
		/** currency is dominated in.*/
		/** - round up depending on the rounding factor (obtained from*/
		/** the T5659 table).*/
		/*PREM-UNIT*/
		if (isEQ(t5664rec.premUnit, 0)) {
			premiumrec.calcPrem.set(wsaaBip);
		}
		else {
			if (isEQ(wsaaBasicPremium, "Y")) {
				compute(premiumrec.calcBasPrem, 2).set((div(wsaaBip, t5664rec.premUnit)));
			}
			else {
				compute(premiumrec.calcPrem, 2).set((div(wsaaBip, t5664rec.premUnit)));
			}
		}
		/*EXIT*/
	}

protected void mortalityLoadings900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para900();
				case calcMortLoadings910: 
					calcMortLoadings910();
				case exit990: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para900()
	{
		/*  First check if there is a need to perform this section at all*/
		wsaaMortalityLoad = "N";
		premiumrec.fltmort.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 8)
		|| isEQ(wsaaMortalityLoad, "Y")); wsaaSub.add(1)){
			if (isNE(wsaaLextZmortpct[wsaaSub.toInt()], 0)) {
				wsaaMortalityLoad = "Y";
			}
		}
		if (isEQ(wsaaMortalityLoad, "N")) {
			goTo(GotoLabel.exit990);
		}
		/*APPLY-SEX/MORTALITY LOADINGS                                    */
		/*Read TH606 using concatenated key CPRM-LSEX + CPRM-MORTPCT      */
		/*to obtain the Sex/Mortality Class Indicator.                    */
		/*For each LEXT-ZMORTPCT kept in working-storage (w/s) table      */
		/*read TH549 using concatenated key CPRM-CRTABLE + w/s-ZMORTPCT   */
		/*using concatenated key CPRM-CRTABLE + LEXT-ZMORTPCT +           */
		/*TH606-ZSEXMORT to compute BAP as:                               */
		/*  BAP = BAP + TH549-rate * sum insured / risk unit / prem unit   */
		/* MOVE SPACES                 TO ITEM-DATA-KEY.                */
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/* MOVE CPRM-CHDR-CHDRCOY      TO ITEM-ITEMCOY.                 */
		/* MOVE TH606                  TO ITEM-ITEMTABL.                */
		/* MOVE CPRM-LSEX              TO WSAA-TH606-SEX.               */
		/* MOVE CPRM-MORTCLS           TO WSAA-TH606-MORTCLS.           */
		/* MOVE WSAA-TH606-KEY         TO ITEM-ITEMITEM.                */
		/* MOVE ITEMREC                TO ITEM-FORMAT.                  */
		/* MOVE 'READR'                TO ITEM-FUNCTION.                */
		/* CALL 'ITEMIO'               USING ITEM-PARAMS.               */
		/* IF  ITEM-STATUZ             NOT = '****'                     */
		/* AND ITEM-STATUZ             NOT = 'MRNF'                     */
		/*    MOVE ITEM-PARAMS         TO SYSR-PARAMS                   */
		/*    PERFORM 9000-FATAL-ERROR                                  */
		/* END-IF.                                                      */
		/* IF  ITEM-STATUZ             = 'MRNF'                         */
		/*     MOVE HL26               TO CPRM-STATUZ                   */
		/*     GO TO 990-EXIT                                           */
		/* END-IF.                                                      */
		/* MOVE ITEM-GENAREA           TO TH606-TH606-REC.              */
		wsaaSub.set(0);
	}

protected void calcMortLoadings910()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 8)) {
			return ;
		}
		if (isEQ(wsaaLextZmortpct[wsaaSub.toInt()], 0)) {
			goTo(GotoLabel.calcMortLoadings910);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(th549);
		wsaaTh549Crtable.set(premiumrec.crtable);
		wsaaTh549Zmortpct.set(wsaaLextZmortpct[wsaaSub.toInt()]);
		/* MOVE TH606-ZSEXMORT         TO WSAA-TH549-ZSEXMORT.          */
		wsaaTh549Zsexmort.set(premiumrec.lsex);
		itdmIO.setItemitem(wsaaTh549Key);
		if (isEQ(premiumrec.ratingdate, ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), "****")
		&& isNE(itdmIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaTh549Key, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), th549)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			premiumrec.statuz.set(hl27);
			return ;
		}
		else {
			t5658rec.t5658Rec.set(itdmIO.getGenarea());
		}
		/*    COMPUTE WSAA-BAP =                                           */
		/*            WSAA-BAP + (((T5658-INSPRM (CPRM-LAGE) *             */
		/*            CPRM-SUMIN) / T5658-UNIT) *                          */
		/*                        (WSAA-LEXT-ZMORTPCT (WSAA-SUB) / 100)).  */
		if (isEQ(premiumrec.lage, 0)){
			compute(wsaaBap, 2).set(add(wsaaBap, (mult((div((mult(t5658rec.insprem, premiumrec.sumin)), t5658rec.unit)), (div(wsaaLextZmortpct[wsaaSub.toInt()], 100))))));
			/**      WHEN 100                                           <V73L03>*/
			/**        COMPUTE WSAA-BAP =                               <V73L03>*/
			/**            WSAA-BAP + (((T5658-INSTPR *                 <V73L03>*/
			/**                        CPRM-SUMIN) / T5658-UNIT) *      <V73L03>*/
			/**                   (WSAA-LEXT-ZMORTPCT (WSAA-SUB) / 100))<V73L03>*/
			/**  Extend the age band to 110.                                    */
		}
		else if (isGTE(premiumrec.lage, 100)
		&& isLTE(premiumrec.lage, 110)){
			compute(wsaaBap, 2).set(add(wsaaBap, (mult((div((mult(t5658rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], premiumrec.sumin)), t5658rec.unit)), (div(wsaaLextZmortpct[wsaaSub.toInt()], 100))))));
		}
		else{
			compute(wsaaBap, 2).set(add(wsaaBap, (mult((div((mult(t5658rec.insprm[premiumrec.lage.toInt()], premiumrec.sumin)), t5658rec.unit)), (div(wsaaLextZmortpct[wsaaSub.toInt()], 100))))));
		}
		goTo(GotoLabel.calcMortLoadings910);
	}
/*BRD-306 START */
protected void premiumAdjustedLoadings900()
	{
		/*PARA*/
		/* APPLY-PREMIUM-ADJUSTED-LOADINGS*/
		/* - sum os the Premium adjusted records from the LEXT W/S table.*/
		/*  - add rates per mille to the BAP*/
		compute(premiumrec.calcPrem, 2).set(add(wsaaPremiumAdjustTot, premiumrec.calcPrem));
		
		/*EXIT*/
		
		compute(premiumrec.premadj,2).set(wsaaPremiumAdjustTot);
	}
/*BRD-306 END */

protected void checkWaiverPremium900()
	{
		para1900();
		checkT5664Insprm910();
	}

protected void para1900()
	{
		/* This section will only be called when the waiver of premium in*/
		/* program PR676 = 'Y'.*/
		/* Read table T5664 for premium amount of the waiver.*/
		/* key:*/
		/* Coverage  (the waiver code is maintained in table TNM61)*/
		/* Mortality Class*/
		/* Number of Lives*/
		/* Access the required table by reading the table directly (ITDM).*/
		/* The contents of the table are then stored. This table is dated*/
		/* use:*/
		/*  1) Rating Date*/
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5664);
		wsaaT5664Crtable.set(pr676cpy.waiverCrtable);
		/*    MOVE CPRM-MORTCLS           TO WSAA-T5664-PLAN-CODE.         */
		wsaaT5664Benpln.set(premiumrec.benpln);
		wsaaT5664Livesno.set(pr676cpy.livesno);
		itdmIO.setItemitem(wsaaT5664Key);
		if (isEQ(premiumrec.ratingdate, ZERO)) {
			itdmIO.setItmfrm(99999999);
		}
		else {
			itdmIO.setItmfrm(premiumrec.ratingdate);
		}
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaT5664Key, itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5664)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			premiumrec.statuz.set(rl78);
		}
		else {
			t5664rec.t5664Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkT5664Insprm910()
	{
		/* Use the age calculated above to access the table T5664 and*/
		/* check the following:*/
		if (isLT(wsaaAdjustedAge, 1)
		|| isGT(wsaaAdjustedAge, 110)) {
			premiumrec.statuz.set(e107);
			return ;
		}
		/*  Note - only 99 out of the 100 rates fit in the occurs table.*/
		/*    IF WSAA-ADJUSTED-AGE = 100                                   */
		/*       IF T5664-INSTPR = ZERO                                    */
		/*          MOVE E107                TO CPRM-STATUZ                */
		/*       ELSE                                                      */
		/*          MOVE T5664-INSTPR        TO WSAA-BAP                   */
		/*  Extend the age band to 110.                                    */
		if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)) {
			if ((setPrecision(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], 0)
			&& isEQ(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], ZERO))) {
				premiumrec.statuz.set(e107);
			}
			else {
				compute(wsaaBap, 2).set(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()]);
			}
		}
		else {
			if (isEQ(t5664rec.insprm[wsaaAdjustedAge.toInt()], ZERO)) {
				premiumrec.statuz.set(e107);
			}
			else {
				wsaaBap.set(t5664rec.insprm[wsaaAdjustedAge.toInt()]);
			}
		}
		if (isEQ(t5664rec.premUnit, 0)) {
			/*NEXT_SENTENCE*/
		}
		else {
			compute(wsaaBap, 2).set((div(wsaaBap, t5664rec.premUnit)));
		}
	}

protected void checkTr687Premunit950()
	{
		para951();
	}

protected void para951()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setDataKey(SPACES);
		initialize(wsaaTempItemitem);
		wsbbTr687Itemcoy.set(premiumrec.chdrChdrcoy);
		wsbbTr687Itemtabl.set(tr687);
		/*    MOVE CPRM-LANGUAGE          TO WSBB-TR687-LANGUAGE.  <V71L09>*/
		wsbbTr687Crtable.set(premiumrec.crtable);
		/* MOVE CPRM-MORTCLS           TO WSBB-TR687-MORTCLS.           */
		wsbbTr687Benpln.set(premiumrec.benpln);
		itdmIO.setDataKey(wsbbTr687Itemkey);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		wsaaTempItemitem.set(itdmIO.getItemitem());
		if (isNE(wsaaTempCrtable, wsbbTr687Crtable)
		|| isNE(wsaaTempBenpln, wsbbTr687Benpln)
		|| isNE(itdmIO.getItemtabl(), "TR687")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		tr687rec.tr687Rec.set(itdmIO.getGenarea());
		if (isNE(tr687rec.premunit, 0)) {
			compute(premiumrec.calcPrem, 2).set(mult(premiumrec.calcPrem, tr687rec.premunit));
			compute(premiumrec.calcBasPrem, 2).set(mult(premiumrec.calcBasPrem, tr687rec.premunit));
		}
	}

protected void calcPercLoadingsWop1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1000();
				case calcLoadings1010: 
					calcLoadings1010();
				case calcEnd1080: 
					calcEnd1080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		/* APPLY-PERCENTAGE-LOADINGS for Waiver Of Premium*/
		/* - from the LEXT working-storage (W/S) table apply the*/
		/* percentage loadings. For each loading entry on the table*/
		/* compute the WOP as follows:*/
		/*  WOP = WOP * loading percentage / 100.*/
		wsaaSub.set(ZERO);
		wsaaWopStPrem.set(ZERO);
	}

protected void calcLoadings1010()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 8)) {
			goTo(GotoLabel.calcEnd1080);
		}
		if (isNE(wsaaLextOppc[wsaaSub.toInt()], 0)) {
			compute(wsaaWopStPrem, 2).set(mult(wsaaWopPrem, (div(wsaaLextOppc[wsaaSub.toInt()], 100))));
		}
		goTo(GotoLabel.calcLoadings1010);
	}

protected void calcEnd1080()
	{
		/* - we should now have a loaded WOP.*/
		if (isEQ(wsaaWopStPrem, ZERO)) {
			wsaaWopStPrem.set(wsaaWopPrem);
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
