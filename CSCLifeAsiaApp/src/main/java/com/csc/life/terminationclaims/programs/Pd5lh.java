/*
 * File: Sd5lh.java
 * Date: 30 August 2009 0:45:00
 * Author: Quipoz Limited
 * 
 * Class transformed from Sd5lh.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved..
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CpsupfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.PyoupfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.SurdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.SurhpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.SurdpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.SurhpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Cpsupf;
import com.csc.life.terminationclaims.dataaccess.model.Pyoupf;
import com.csc.life.terminationclaims.dataaccess.model.Surdpf;
import com.csc.life.terminationclaims.dataaccess.model.Surhpf;

import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.screens.Sd5lhScreenVars;
import com.csc.life.terminationclaims.tablestructures.Td5h6rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.UsrdTableDAM;
import com.csc.smart.dataaccess.dao.CcfkpfDAO;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.CreditCardUtility;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

	
/**
 * <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Version #2 - Incorporating Plan Processing.
* This program is the main screen for the contract enquiry.
*
* Initialise
* ----------
*
*     Skip  this  section  if  returning from an optional selection
*     (current stack position action flag = '*').
*
*     The details of the contract being enqired upon will be stored
*     in  the  CHDR I/O module.  Retrieve the details, then release
*     the  CHDR I/O module with a RLSE function and perform a READS
*     on  CHDRENQ so that the appropriate details are held for this
*     sub-system.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract  Status,  (STATCODE)  -  short description from
*          T3623,
*
*          Premium  Status,  (PSTATCODE)  -  short description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The  joint  owner's client (CLTS) details if they exist.
*
*
*          The agent (AGNT) to get the client number, and hence the
*          client (agent's) name.
*
*
* Validation
* ----------
*
*     Skip  this  section  if  returning from an optional selection
*     (current stack position action flag = '*').
*
*          If  the  'KILL'  function  key  was pressed skip all the
*          validation.
*
*     Validation  is  only  required  against the indicators at the
*     foot  of  the  screen. The only valid entries in those are an
*     'X'.
*
*     If  the  Plan Level Component indicator has been selected and
*     the number of policies in plan, (from the Contract Header) is
*     1 then highlight the indicator as being in error.
*

 *
 *****************************************************************
 * </pre>
 */
public class Pd5lh extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5LH");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private PackedDecimalData wsaaPrsub = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaWsspChdrky = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaWsspChdrpfx = new FixedLengthStringData(2).isAPartOf(wsaaWsspChdrky, 0);
	private FixedLengthStringData wsaaWsspChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaWsspChdrky, 2);
	private FixedLengthStringData wsaaWsspChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaWsspChdrky, 3);
	private Covrpf covrpf = new Covrpf();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatckey = new Batckey();	
	private Wssplife wssplife = new Wssplife();
	private Sd5lhScreenVars sv = getPScreenVars();
	protected FormatsInner formatsInner = new FormatsInner();
	protected Payrpf payrpf ;
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Linspf linspf = null;
	protected LinspfDAO linspfdao = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String t5687 = "T5687";
	private static final String tr386 = "TR386";
	private static final String td5h6 = "TD5H6";
	private static final String t6640 = "T6640";
	private static final String t6598 = "T6598";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private String usrdrec = "USRDREC";
	private static final String f826 = "F826";
	private static final String e133 = "E133";
	private static final String f906 = "F906";
	private static final String e186 = "E186";
	private static final String e304 = "E304";
	private static final String rrbs = "RRBS";
	private static final String rrmp = "RRMP";
	private static final String rrmh = "RRMH";
	private FixedLengthStringData wsaaBnkkey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);
	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaBankdesc = new FixedLengthStringData(30).isAPartOf(wsaaBankkey, 0);
	private BabrTableDAM babrIO = new BabrTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Tr386rec tr386rec = new Tr386rec();
	private T5679rec t5679rec = new T5679rec();
	protected Chdrpf chdrpf ;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Descpf descpf = null;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Clntpf clntpf = new Clntpf();
	private List<Clntpf> clntpfList = null;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Lifepf lifepf = new Lifepf();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private Clbapf clbapf;
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private Mandpf mandpf;
	private CcfkpfDAO ccfkpfDAO = getApplicationContext().getBean("ccfkpfDAO", CcfkpfDAO.class);
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);	
	
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	

	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	
	private Covrpf covrpup = new Covrpf();
	private CovrpfDAO covrpupDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	protected Map<String, List<Itempf>> tr52dListMap;
	protected Map<String, List<Itempf>> tr52eListMap;
	protected Tr52drec tr52drec = new Tr52drec();
	protected Tr52erec tr52erec = new Tr52erec();	
	private Pyoupf pyoupf = new Pyoupf();
	private PyoupfDAO pyoupfDAO = getApplicationContext().getBean("pyoupfDAO", PyoupfDAO.class);
	private Map<String, List<Chdrpf>> chdrMap = null;
	private Cpsupf cpsupf = new Cpsupf();
	private CpsupfDAO cpsupfDAO = getApplicationContext().getBean("cpsupfDAO", CpsupfDAO.class);
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	private Td5h6rec td5h6rec = new Td5h6rec();
	private Itempf itempf = null;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private UsrdTableDAM usrdIO = new UsrdTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaSumFlag = new FixedLengthStringData(1).init(SPACES);
	private Txcalcrec txcalcrec = new Txcalcrec();
	private ExternalisedRules er = new ExternalisedRules();
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaStoredLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");	
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaTranno = new FixedLengthStringData(5);
	private List<Covrpf> insertCovrlnbList = null;
	private String t5679 = "T5679";
	private SurdpfDAO surdpfDAO = getApplicationContext().getBean("surdpfDAO", SurdpfDAOImpl.class);
	private SurhpfDAO surhpfDAO = getApplicationContext().getBean("surhpfDAO", SurhpfDAOImpl.class);
	private List<Surdpf> surdpfList;
	private List<Surhpf> surhpfList; 
	Surhpf surhpf =  new Surhpf();
	Surdpf surdpf =  new Surdpf();
	private ErrorsInner errorsInner = new ErrorsInner();
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	/**
	 * Contains all possible labels used by goTo action.
	 */

	public Pd5lh() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5lh", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	protected Sd5lhScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars(Sd5lhScreenVars.class);
	}

	protected PayrpfDAO getBeanPayerpf() {
		return getApplicationContext().getBean("payrDAO", PayrpfDAO.class);
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	/**
	 * <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	protected void initialise1000() {
		initialise1010();
	}

	protected void initialise1010() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		new ArrayList<Chdrpf>();
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		wsaaSumFlag.set(SPACES);
		wsaaCrtable.set(SPACES);
		
		wsaaToday.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		
		readTr386();
		if (isEQ(wsspcomn.sbmaction, "A")) {
			sv.descn.set(tr386rec.progdesc01);
		} else if (isEQ(wsspcomn.sbmaction, "B")) {
			sv.descn.set(tr386rec.progdesc02);
		} else if (isEQ(wsspcomn.sbmaction, "C")) {
			sv.descn.set(tr386rec.progdesc03);
		}
		
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
				else {
					covrpfDAO.setCacheObject(covrpf);
				}
			}
		}else {
			covrpfDAO.setCacheObject(covrpf);
		}
		/* Get chdrpf details */
		
		chdrlnbIO.setFunction("RETRV"); //Move this to top and dont call chdrpf
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		
		chdrpf = chdrpfDAO.getChdrpfByChdrnum("CH",chdrlnbIO.getChdrcoy().toString().trim(),chdrlnbIO.getChdrnum().toString().trim(),"1");
		if (chdrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set("2".concat(chdrlnbIO.getChdrnum().toString()));
			fatalError600();
		}
		payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(wsspcomn.company.toString(),chdrlnbIO.getChdrnum().toString().trim());	
		if (payrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set("2".concat(chdrlnbIO.getChdrnum().toString()));
			fatalError600();
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.register.set(chdrlnbIO.getRegister());
		wsaaTranno.set(chdrpf.getTranno()+1);
		if(isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "C")){

			cpsupf = cpsupfDAO.getCpsupfItem(sv.chdrnum.toString().trim(),covrpf.getCrtable().trim());//pick up only statu 'R /* IJTI-1386 */
					
			sv.mop.set(cpsupf.getReqntype());	
			
			if(isEQ(sv.mop ,"4") || isEQ(sv.mop, "C")){
				sv.bnkcode.set(cpsupf.getBankkey());
				sv.bnkcdedsc.set(cpsupf.getBankdesc());
				if (isEQ(cpsupf.getReqntype().trim(), "4")) {/* IJTI-1386 */
					sv.drctdbtno.set(cpsupf.getBankacckey());
				} else if (isEQ(cpsupf.getReqntype().trim(), "C")) {/* IJTI-1386 */
					sv.crdtcrdno.set(cpsupf.getBankacckey());
				}
			
			}else{
				sv.bnkcode.set(SPACES);
				sv.drctdbtno.set(SPACES);
				sv.crdtcrdno.set(SPACES);
			}
					
		}else{
			sv.mop.set(chdrpf.getReqntype());			
			if (((isEQ(chdrpf.getReqntype(), "C") || isEQ(chdrpf.getReqntype(), "4"))
					&& isNE(chdrpf.getZmandref(), SPACES))) {
				loadBnkdetailsMendref();
			} else if (isEQ(chdrpf.getReqntype(), "C")
					|| isEQ(chdrpf.getReqntype(), "4") && isEQ(chdrpf.getReqntype(), SPACES)) {
				loadBnkdetailsChdr();
			}else if (isNE(chdrpf.getReqntype().trim(), "4")/* IJTI-1386 */
					&& isNE(chdrpf.getReqntype().trim(), "C")) {/* IJTI-1386 */
				sv.drctdbtno.set(SPACES);
				sv.crdtcrdno.set(SPACES);
				sv.bnkcode.set(SPACES);
			}
			
		}
		
		Map<String, String> itemMap = new HashMap<>();
		itemMap.put(t5688, chdrpf.getCnttype()); /* IJTI-1386 */
		itemMap.put(t3623, chdrpf.getStatcode());/* IJTI-1386 */
		itemMap.put(t3588, chdrpf.getPstcde()); /* IJTI-1386 */
		
		Map<String, Descpf> descMap = descDAO.searchMultiDescpf(wsspcomn.company.toString(),
				wsspcomn.language.toString(), itemMap);

		if (!descMap.containsKey(t5688)) {
			sv.ctypedes.set(SPACES);
		} else {
			sv.ctypedes.set(descMap.get(t5688).getLongdesc());
		}
		if (!descMap.containsKey(t3623)) {
			sv.chdrstatus.set(SPACES);
		} else {
			sv.chdrstatus.set(descMap.get(t3623).getLongdesc());
		}
		if (!descMap.containsKey(t3588)) {
			sv.premstatus.set(SPACES);
		} else {
			sv.premstatus.set(descMap.get(t3588).getLongdesc());
		}
		// Life details

		lifepf.setChdrcoy(chdrpf.getChdrcoy().toString());
		lifepf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		lifepf.setLife("01");
		lifepf.setJlife("00");
		lifepf.setValidflag("1");
		Map<String, Lifepf> lifepfMap = lifepfDAO.getLifeEnqData(lifepf);
		Lifepf lifepfModel = lifepfMap.get(lifepf.getChdrcoy().trim() + "" + lifepf.getChdrnum().trim() + ""
				+ lifepf.getLife().trim() + "" + lifepf.getJlife().trim() + "" + lifepf.getValidflag().trim());
		if (lifepfModel == null) {
			return;
		}
		sv.lifenum.set(lifepfModel.getLifcnum());
		clntpf.setClntnum(lifepfModel.getLifcnum());
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		clntpf.setClntpfx("CN");
		clntpfList = clntpfDAO.readClientpfData(clntpf);
		if (clntpfList == null || (clntpfList != null && clntpfList.size() == 0)) {
			return;
		}
		for (Clntpf clntpf : clntpfList) {
			plainname(clntpf);
		}
		sv.lifename.set(wsspcomn.longconfname);
		
		// Owner details
		sv.cownnum.set(chdrpf.getCownnum());
		cltsIO.setClntnum(sv.cownnum);
		getClientDetails1200();
		/* Get the confirmation name. */
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf) || isNE(cltsIO.getValidflag(), 1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		} else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}

		sv.currfrom.set(chdrpf.getOccdate());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.btdate.set(chdrpf.getBtdate());

		sv.payfreq.set(chdrpf.getBillfreq());
		// Payee details
		if(isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "C")){		
			sv.payeeno.set(cpsupf.getPayrnum());
		}else{
			sv.payeeno.set(chdrpf.getCownnum());
		}
		sv.payeenme.set(SPACES);
		wsspcomn.chdrCownnum.set(sv.payeeno);
		cltsIO.setClntnum(sv.payeeno);
		getClientDetails1200();
		/* Get the confirmation name. */
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf) || isNE(cltsIO.getValidflag(), 1)) {
			sv.payeenoErr.set(e133);
			sv.payeenme.set(SPACES);
		} else {
			plainname();
			sv.payeenme.set(wsspcomn.longconfname);
		}
		componentDetails();				
					
		sv.compno.set(covrpup.getRider());
		sv.compcode.set(covrpup.getCrtable());
		
		descpf = descDAO.getdescData("IT", t5687, covrpup.getCrtable(), wsspcomn.company.toString(),
				wsspcomn.language.toString()); 
		
		sv.comprskcode.set(covrpup.getStatcode());
		sv.comppremcode.set(covrpup.getPstatcode());
		if (descpf == null) {
			sv.compdesc.set(SPACES);
		} else {
			sv.compdesc.set(descpf.getLongdesc());
		}	
	
		itemMap.put("T5682", covrpup.getStatcode());
		itemMap.put("T5681", covrpup.getPstatcode()); 
		
		
	        if(isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "C")){	        	
	        	
			    sv.compsumss.set(cpsupf.getCompsumss());				
				sv.compbilprm.set(cpsupf.getCompbilprm());				
				sv.refprm.set(cpsupf.getRefprm());
				sv.refadjust.set(cpsupf.getRefadjust());
				sv.refamt.set(cpsupf.getRefamt());
			    
			}else{				
				
			    			    
			    sv.compsumss.set(covrpup.getSumins());
				
				tr52dListMap = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), tr52d); //Create string variable and use
				tr52eListMap = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), tr52e);	
				StringBuilder sbTr52eKey = new StringBuilder("");
				sbTr52eKey.append(tr52drec.txcode.toString().trim());
				sbTr52eKey.append(chdrpf.getCnttype().trim());/* IJTI-1386 */
				sbTr52eKey.append(covrpup.getCrtable().trim());/* IJTI-1386 */
				readTr52e(sbTr52eKey.toString(), Integer.parseInt(chdrpf.getBtdate().toString()));
				
				if (isEQ(tr52erec.zbastyp, "Y")) {
					sv.compbilprm.set(covrpf.getZbinstprem());
				}else {
					sv.compbilprm.set(covrpf.getInstprem());
				}
				
				sv.refprm.set(ZERO);
				sv.refadjust.set(ZERO);
				sv.refamt.set(ZERO);
			    
			}
	        
	        descMap = descDAO.searchMultiDescpf(wsspcomn.company.toString(),
	                wsspcomn.language.toString(), itemMap); //itemMap.put(t5688, chdrpf.getCn
	       
	        	
	        if (!descMap.containsKey("T5682")) {
			        sv.comprskdesc.fill("?");
			   } else {
			        sv.comprskdesc.set(descMap.get("T5682").getLongdesc());
			    }
	        	
	        	
	        if (!descMap.containsKey("T5681")) {
	  	           sv.comppremdesc.fill("?");
	  	       } else {
	  	           sv.comppremdesc.set(descMap.get("T5681").getLongdesc());
	  	       }
	        
			
		readT5679();			        	        
		protrctField();
		if(isEQ(wsspcomn.sbmaction, "A"))
			wholePlan1300();
		
		
		
	}
	protected void readT5679(){
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(wsaaBatckey.batcBatctrcde.toString());

		FixedLengthStringData itemitem = new FixedLengthStringData(8);
		itemitem.setLeft(stringVariable1.toString());
		List<Itempf> itemlist = this.itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), t5679, itemitem.toString());
		if(itemlist.isEmpty()) {
			syserrrec.params.set(t5679);
			fatalError600();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itemlist.get(0).getGenarea()));
	}
	
	protected void wholePlan1300()
	{
		
		wsaaStoredLife.set(covrpf.getLife());
		wsaaStoredCoverage.set(covrpf.getCoverage());
		wsaaStoredRider.set(covrpf.getRider());
		wsaaStoredCurrency.set(covrpf.getPremCurrency());
		
		processComponents1350();
		
	}

protected void processComponents1350()
	{
	
		/* Read table T5687 to find the surrender calculation subroutine*/
		wsaaCrtable.set(covrpf.getCrtable());
		obtainSurrenderCalc1400();	
		//set refund premium to zero if surrender method is empty and return
		
		if(!t5687rec.svMethod.toString().trim().isEmpty()){
					
			/* check the reinsurance file at a later date and move*/
			/* 'y' to this field if they are present*/
			/* This Check never actually took place, but does now.             */
			/*  MOVE SPACES                    TO S5026-RIIND.               */
			/*   PERFORM 1980-READ-RACD.                              <A06843>*/
			/*                                                        <A06843>*/
			/*   IF RACDMJA-STATUZ            = O-K                   <A06843>*/
			/*       MOVE 'Y'                TO S5026-RIIND           <A06843>*/
			/*   END-IF.                                              <A06843>*/
			srcalcpy.currcode.set(covrpf.getPremCurrency());
			srcalcpy.endf.set(SPACES);
			srcalcpy.fund.set(SPACES);
			srcalcpy.description.set(SPACES);
			srcalcpy.status.set(varcom.oK);
			srcalcpy.element.set(SPACES);
			srcalcpy.neUnits.set(SPACES);
			srcalcpy.tmUnits.set(SPACES);
			srcalcpy.psNotAllwd.set(SPACES); 
			srcalcpy.tsvtot.set(ZERO);
			srcalcpy.tsv1tot.set(ZERO);
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
			srcalcpy.chdrChdrcoy.set(covrpf.getChdrcoy());
			srcalcpy.chdrChdrnum.set(covrpf.getChdrnum());
			srcalcpy.lifeLife.set(covrpf.getLife());
			srcalcpy.lifeJlife.set(covrpf.getJlife());
			srcalcpy.covrCoverage.set(covrpf.getCoverage());
			srcalcpy.covrRider.set(covrpf.getRider());
			srcalcpy.crtable.set(covrpf.getCrtable());
			srcalcpy.crrcd.set(covrpf.getCrrcd());
			srcalcpy.convUnits.set(covrpf.getConvertInitialUnits());
			srcalcpy.pstatcode.set(covrpf.getPstatcode());
			srcalcpy.status.set(SPACES);
			srcalcpy.polsum.set(chdrlnbIO.polsum);
			srcalcpy.language.set(wsspcomn.language);
			srcalcpy.ptdate.set(payrpf.getPtdate());
			srcalcpy.planSuffix.set(covrpf.getPlanSuffix()); 			
			srcalcpy.effdate.set(wsaaToday);
			srcalcpy.type.set("S");		
			srcalcpy.singp.set(covrpf.getInstprem());
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				srcalcpy.billfreq.set("00");
			}
			else {
				srcalcpy.billfreq.set(chdrpf.getBillfreq());
			}
			
			callSurMethodWhole1600();
		
		}else{
			sv.refprm.set(ZERO);
		}
	
	}

protected void obtainSurrenderCalc1400()
{	
	itempf = new Itempf();
	itempf.setItempfx("IT");	
	itempf.setItemtabl(t5687);
	itempf.setItemcoy(wsspcomn.company.toString());	
	itempf.setItemitem(wsaaCrtable.toString());
	itempf.setItemseq("  ");
	itempf = itemDao.getItemRecordByItemkey(itempf);
	

	if (itempf == null) {
		syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5687").concat(wsaaProg.toString()));
		fatalError600();
	}
	
	t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));

	if(!t5687rec.svMethod.toString().trim().isEmpty()){
		itempf = new Itempf();
		itempf.setItempfx("IT");	
		itempf.setItemtabl(t6598);
		itempf.setItemcoy(wsspcomn.company.toString());	
		itempf.setItemitem(t5687rec.svMethod.toString());
		itempf.setItemseq("  ");
		itempf = itemDao.getItemRecordByItemkey(itempf);
	
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T6598").concat(wsaaProg.toString()));
			fatalError600();
		}
		
		t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
}

protected void callSurMethodWhole1600()
{

	
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()) 
				&& er.isExternalized(txcalcrec.cnttype.toString(), srcalcpy.crtable.toString())))  
		{
			callProgramX(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();
	
			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			
	
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrpf);//VPMS call		
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.endf.set("Y");
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
			/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(vpxsurcrec.statuz.equals(varcom.endp))
				srcalcpy.status.set(varcom.endp);
			/* ILIFE-3142 End*/
		}	
		
		/*IVE-797 RUL Product - Full Surrender Calculation end*/
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)
		&& isNE(srcalcpy.status, "NOPR") && isNE(srcalcpy.status, varcom.mrnf)) {
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isEQ(srcalcpy.status, "NOPR")) {
		}
		if (isEQ(srcalcpy.status, varcom.oK)
		|| isEQ(srcalcpy.status, varcom.endp)) {
			if (isEQ(srcalcpy.currcode, SPACES)) {
				srcalcpy.currcode.set(chdrpf.getCntcurr());
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			callRounding6000();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			callRounding6000();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		if (isEQ(srcalcpy.type, "C")) {
			compute(srcalcpy.actualVal, 2).set(mult(srcalcpy.actualVal, -1));
		}
		/* Check the amount returned for being negative. In the case of    */
		/* SUM products this is possible and so set these values to zero.  */
		/* Note SUM products do not have to have the same PT & BT dates.   */	
		
		
		if (isLT(srcalcpy.actualVal, ZERO)) {
			srcalcpy.actualVal.set(ZERO);
		}
				
		
		/*    IF SURC-ENDF  = 'Y'                                          */
		/*       GO TO 1640-EXIT.                                          */
		if (isEQ(srcalcpy.actualVal, ZERO)) {
			sv.refprm.set(ZERO);
		} else{
			sv.refprm.set(srcalcpy.actualVal);
		}

	}


protected void callRounding6000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

	protected void readTr386() {
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString().trim());
		itempf.setItemcoy(wsspcomn.company.toString().trim());
		itempf.setItemtabl(tr386);
		wsaaTr386Lang.set(wsspcomn.language.toString());
		wsaaTr386Pgm.set(wsaaProg);
		itempf.setItemitem(wsaaTr386Key.toString().trim());
		itempf.setItemseq("  ");
		itempf = itemDao.getItemRecordByItemkey(itempf);

		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("TR386").concat(wsaaProg.toString()));
			fatalError600();
		}
		tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	
	protected void readTr52e(String itemKey, Integer effDate){
		String keyItemitem = itemKey.trim();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (tr52eListMap.containsKey(keyItemitem)){	
			itempfList = tr52eListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((effDate >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& effDate <= Integer.parseInt(itempf.getItmto().toString())){
						tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound = true;
						break;
					}
				}else{
					tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));	
					itemFound = true;
					break;
				}				
			}		
		}		
	}
	

	protected void protrctField() {
		if (isEQ(wsspcomn.sbmaction, "B") || isEQ(wsspcomn.sbmaction, "C")) {
			sv.crdtcrdnoOut[varcom.pr.toInt()].set("Y");
			sv.drctdbtnoOut[varcom.pr.toInt()].set("Y");
			sv.payeenoOut[varcom.pr.toInt()].set("Y");
			sv.mopOut[varcom.pr.toInt()].set("Y");
			sv.refadjustOut[varcom.pr.toInt()].set("Y");
		}
	}

	protected void loadBnkdetailsMendref() {
		mandpf = mandpfDAO.searchMandpfRecordData(chdrpf.getCowncoy().toString(), chdrpf.getCownnum(),
				chdrpf.getZmandref(), "MANDPF");/* IJTI-1386 */
		if (mandpf != null) {
			String creditCardInformation = null;
			sv.bnkcode.set(mandpf.getBankkey());
			if (isEQ(chdrpf.getReqntype().trim(), "4")) {/* IJTI-1386 */
				sv.drctdbtno.set(mandpf.getBankacckey());
			} else if (isEQ(chdrpf.getReqntype().trim(), "C")) {/* IJTI-1386 */

				sv.crdtcrdno.set(mandpf.getBankacckey());
				creditCardInformation = CreditCardUtility.getInstance().getCreditCard(sv.crdtcrdno.toString().trim());
				if (creditCardInformation != null && !creditCardInformation.isEmpty()) {
					sv.crdtcrdno.set(creditCardInformation);
				}
			}
		}
		// Bank Account description and Bank Description
		babrIO.setBankkey(sv.bnkcode);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		sv.bnkcdedsc.set(babrIO.getBankdesc());

	}

	protected void loadBnkdetailsChdr() {

		sv.bnkcode.set(chdrpf.getBankkey());
		if (isEQ(chdrpf.getReqntype().trim(), "4")) {/* IJTI-1386 */
			sv.drctdbtno.set(chdrpf.getBankacckey());
		} else if (isEQ(chdrpf.getReqntype().trim(), "C")) {/* IJTI-1386 */
			sv.crdtcrdno.set(chdrpf.getBankacckey());
		}
		// Bank Account description and Bank Description
		babrIO.setBankkey(sv.bnkcode);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		sv.bnkcdedsc.set(babrIO.getBankdesc());
	}
		
	protected void componentDetails(){
	
		covrpup = covrpupDAO.getCovrRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), 
				covrpf.getCoverage(),covrpf.getRider(), covrpf.getPlanSuffix(), "1");/* IJTI-1386 */
			if(covrpup == null){
				fatalError600();
			}
	
		
	}
	
	/**
	 * <pre>
	*    Sections performed from the 1000 section above.
	 * </pre>
	 */
	protected void largename() {
		/* LGNM-100 */
		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().equals("C")) {
			corpname(clntpf);
			return;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/* LGNM-EXIT */
	}

	protected void plainname() {
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/* PLAIN-EXIT */
	}

	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/* CORP-EXIT */
	}

	protected void plainname(Clntpf clntpf) {
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().equals("C")) {
			corpname(clntpf);
			return;
		}
		// if (clntpf.getGivname().equals("")) {
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		} else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/* PLAIN-EXIT */
	}

	protected void corpname(Clntpf clntpf) {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/* CORP-EXIT */
	}

	/* ILIFE-3149 starts */
	/**
	 * <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	 * </pre>
	 */
	protected void preScreenEdit() {
		/* PRE-START */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return;
		}
		/* 2010-SCREEN-IO. */
		return;
		/* PRE-EXIT */
	}

	protected void screenEdit2000() {
		try {
			screenIo2010();
			if (isEQ(wsspcomn.sbmaction, "A")) {
				if((isEQ(sv.crdtcrdno,SPACE)&& isEQ(sv.mop,"C"))||(isEQ(sv.drctdbtno,SPACE)&& isEQ(sv.mop,"4"))){			
					getBankorCCdetails();
				}
	
				checkAdjustment();
				validate2020();
			}
			if (isEQ(wsspcomn.sbmaction, "C")) {
				checkLimit();
			}
			checkForErrors2080();
		} catch (GOTOException e) {
			/* Expected exception for control flow purposes. */
		}
	}

	
	protected void screenIo2010() {
		/* CALL 'Sd5lhIO' USING SCRN-SCREEN-PARAMS */
		/* Sd5lh-DATA-AREA. */
		if (isEQ(scrnparams.statuz, "KILL")) {
			exit2090();
		}
		/* Screen errors are now handled in the calling program. */
		/* PERFORM 200-SCREEN-ERRORS. */
		wsspcomn.edterror.set(varcom.oK);
		wsspcomn.chdrCownnum.set(sv.payeeno);
		
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");			
		}
	}

	protected void exit2090() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	protected void getBankorCCdetails(){
		if(isEQ(sv.mop, "C")){
			clbapf = new Clbapf();
			clbapf.setClntpfx("CN");
			clbapf.setClntcoy(wsspcomn.fsuco.toString().trim());
			clbapf.setClntnum(sv.payeeno.toString().trim());
			clbapf.setClntrel("CC");
			List<Clbapf> clbalist = clbapfDAO.searchClbapfDatabyUniquNo(clbapf);
			if (clbalist != null && clbalist.size() > 0) {
				for (Clbapf clbaitem : clbalist) {					
					clbapf = clbaitem;				
				}
				String creditCard=CreditCardUtility.getInstance().getCreditCard(clbapf.getBankacckey().trim());
				if(creditCard!=null && !creditCard.isEmpty()){
					sv.crdtcrdno.set(creditCard);
				} else {
					sv.crdtcrdno.set(clbapf.getBankacckey());	
				}				
			}
			

		}else if(isEQ(sv.mop, "4")){
			clbapf = new Clbapf();
			clbapf.setClntpfx("CN");
			clbapf.setClntcoy(wsspcomn.fsuco.toString().trim());
			clbapf.setClntnum(sv.payeeno.toString().trim());
			clbapf.setClntrel("CB");
			List<Clbapf> clbalist = clbapfDAO.searchClbapfDatabyUniquNo(clbapf);
			if (clbalist != null && clbalist.size() > 0) {
				for (Clbapf clbaitem : clbalist) {					
					clbapf = clbaitem;				
				}
				String clientBankAccount=CreditCardUtility.getInstance().getCreditCard(clbapf.getBankacckey().trim());
				if(clientBankAccount!=null && !clientBankAccount.isEmpty()){
					sv.drctdbtno.set(clientBankAccount);
				} else {
					sv.drctdbtno.set(clbapf.getBankacckey());	
				}				
			}
			
		}
	}
	
	  protected void checkAdjustment() {
	        double temp = sv.refprm.getbigdata().doubleValue() + sv.refadjust.getbigdata().doubleValue();
	        sv.refamt.set(temp);
	        if(temp < 0){	        	
	        	sv.refamtErr.set(rrmp);
	        	wsspcomn.edterror.set("Y");
	        }
	    }
	protected void validate2020() {
		if (isNE(sv.drctdbtno, SPACES)) {
			clbapf = new Clbapf();
			clbapf.setClntpfx("CN");
			clbapf.setClntcoy(wsspcomn.fsuco.toString().trim());
			clbapf.setClntnum(sv.payeeno.toString().trim());
			clbapf.setBankacckey(sv.drctdbtno.toString().trim());
			List<? extends Clbapf> clbalist = clbapfDAO.findByBankAccKey(clbapf);

			if (clbalist != null && clbalist.size() > 0) {
				for (Clbapf clbaitem : clbalist) {
					wsaaBnkkey.set(clbaitem.getBankkey());
					sv.bnkcode.set(wsaaBnkkey);
				}
				bankBranchDesc();
			} else {
				sv.drctdbtnoErr.set(f826);
				wsspcomn.edterror.set("Y");
			}

		}
		if (isNE(sv.crdtcrdno, SPACES)) {
			clbapf = new Clbapf();
			clbapf.setClntpfx("CN");
			clbapf.setClntcoy(wsspcomn.fsuco.toString().trim());
			clbapf.setClntnum(sv.payeeno.toString().trim());
			clbapf.setBankacckey(ccfkpfDAO.retreiveReferenceNumber(sv.crdtcrdno.toString().trim()));
			List<? extends Clbapf> clbalist = clbapfDAO.findByBankAccKey(clbapf);

			if (clbalist != null && clbalist.size() > 0) {
				for (Clbapf clbaitem : clbalist) {
					wsaaBnkkey.set(clbaitem.getBankkey());/* IJTI-1386 */
					sv.bnkcode.set(wsaaBnkkey);
				}
				bankBranchDesc();
			} else {
				sv.crdtcrdnoErr.set(f826);
				wsspcomn.edterror.set("Y");
			}
		}

		cltsIO.setClntnum(sv.payeeno);
		getClientDetails1200();
		/* Get the confirmation name. */
		if (isEQ(sv.payeeno,SPACES) || isEQ(sv.payeeno,"")) {
			sv.payeenoErr.set(e133);
			sv.payeenme.set(SPACES);
		} else {
			plainname();
			sv.payeenme.set(wsspcomn.longconfname);
		}
		

		if (isNE(chdrpf.getCownnum(), SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspwindow.numsel.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrpf.getCownpfx());
			stringVariable1.addExpression(chdrpf.getCowncoy());
			stringVariable1.addExpression(sv.payeeno);
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}

		

		if (isNE(sv.mop, "4") && isNE(sv.mop, "C")) {
			sv.crdtcrdno.set(SPACES);
			sv.drctdbtno.set(SPACES);
			sv.bnkcode.set(SPACES);
			sv.bnkcdedsc.set(SPACES);
		}

		if (isEQ(sv.drctdbtno, SPACES) && isEQ(sv.mop, "4")) {
			sv.bnkcode.set(SPACES);
			sv.bnkcdedsc.set(SPACES);
		} else if (isEQ(sv.crdtcrdno, SPACES) && isEQ(sv.mop, "C")) {
			sv.bnkcode.set(SPACES);
			sv.bnkcdedsc.set(SPACES);
		}

		if (isEQ(sv.mop, SPACES)) {
			sv.mopErr.set(rrbs);
		}
		if (isEQ(sv.mop, "4")) {
			sv.crdtcrdno.set(SPACES);
			if (isEQ(sv.drctdbtno, SPACES)) {
				sv.drctdbtnoErr.set(e186);
				wsspcomn.edterror.set("Y");
			}
		}

		if (isEQ(sv.mop, "C")) {
			sv.drctdbtno.set(SPACES);
			if (isEQ(sv.crdtcrdno, SPACES)) {
				sv.crdtcrdnoErr.set(e186);
				wsspcomn.edterror.set("Y");
			}
		}
		
		

	}

	protected void checkLimit()
	{
		uwlvIO.setParams(SPACES);
		uwlvIO.setUserid(wsspcomn.userid);
		uwlvIO.setCompany(wsspcomn.company);
		uwlvIO.setFunction(varcom.readr);
		uwlvIO.setFormat(formatsInner.uwlvrec);
		SmartFileCode.execute(appVars, uwlvIO);
		if (isNE(uwlvIO.getStatuz(),varcom.oK)
		&& isNE(uwlvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(uwlvIO.getParams());
			syserrrec.statuz.set(uwlvIO.getStatuz());
			fatalError600();
		}
		if (isEQ(uwlvIO.getStatuz(), varcom.mrnf)) {
			sv.refamtErr.set(errorsInner.rf05);
			wsspcomn.edterror.set("Y");
			return;
		}
		
		else {

			
			if((null!=uwlvIO.getPoslevel()) && isEQ(uwlvIO.getPoslevel(),SPACES))
			{
				sv.refamtErr.set(errorsInner.rf05);
				wsspcomn.edterror.set("Y");
				return;
			}
			
			
			readTd5h6();
			 
			 itempfList=itempfDAO.getAllItemitem("IT",chdrpf.getChdrcoy().toString(), t6640, covrpf.getCrtable());/* IJTI-1386 */
				
				 if (itempfList.size() >= 1 ){				 
					if(isGT(sv.refamt,td5h6rec.fullsurauthlim))
					   {
				    	sv.refamtErr.set(rrmh);
				    	wsspcomn.edterror.set("Y");
						return;
					   }
				}
			
		}		
		
		usrdIO.setParams(SPACES);
		usrdIO.setCompany(wsspcomn.company);
		usrdIO.setUserid(wsspcomn.userid);
		usrdIO.setFormat(usrdrec);
		usrdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, usrdIO);
		if (isNE(usrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(usrdIO.getParams());
			syserrrec.statuz.set(usrdIO.getStatuz());
			fatalError600();
		}
	/*	uwlvIO.setParams(SPACES);
		uwlvIO.setUserid(wsspcomn.userid);
		uwlvIO.setCompany(wsspcomn.company);
		uwlvIO.setFunction(varcom.readr);
		uwlvIO.setFormat(formatsInner.uwlvrec);
		SmartFileCode.execute(appVars, uwlvIO);
		if (isNE(uwlvIO.getStatuz(),varcom.oK)
		&& isNE(uwlvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(uwlvIO.getParams());
			syserrrec.statuz.set(uwlvIO.getStatuz());
			fatalError600();
		}
		
		else {
			if(isNE(uwlvIO.getPoslevel(),SPACES))
			{
			 readTd5h6();
			 if(isGT(sv.refamt,td5h6rec.fullsurauthlim))
			   {
			    	sv.refamtErr.set(rrmh);
			    	wsspcomn.edterror.set("Y");
					return;
			   }
			}
			}*/
	}
	

protected void readTd5h6() 
	{
	itempf = new Itempf();
	itempf.setItempfx(smtpfxcpy.item.toString());
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(td5h6);/* IJTI-1386 */
	itempf.setItemitem(uwlvIO.getPoslevel().toString());
	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf== null ) {
		syserrrec.params.set(itempf);
		fatalError600();
	}
	else {
		td5h6rec.td5h6Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	}
	
	protected void getClientDetails1200() {
		/* READ */
		/* Look up the contract details of the client owner (CLTS) */
		/* and format the name as a CONFIRMATION NAME. */
 		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK) && isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/* EXIT */
	}

	protected void checkForErrors2080() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	 * <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000() {
		/* PARA */
		/* There is no updating required in this program. */
		wsaaWsspChdrnum.set(sv.chdrnum);
		wsaaWsspChdrcoy.set(chdrpf.getChdrcoy());
		wsaaWsspChdrpfx.set(SPACES);
		wssplife.chdrky.set(wsaaWsspChdrky);
		wsaaPrsub.add(1);
		wsaaIndex.set(1);
		if (isEQ(wsspcomn.sbmaction, "A")) {
			
			insertCpsupf();	
			updateInsertCovrpf();
		}else if(isEQ(wsspcomn.sbmaction, "B")){
			deleteCpsupf();	
			deleteupdateCovrpf();
		}
		else if(isEQ(wsspcomn.sbmaction, "C")){
		
		
		
		surhclmIO.setDataKey(SPACES);
		surhclmIO.setChdrcoy(chdrpf.getChdrcoy());
		surhclmIO.setChdrnum(chdrpf.getChdrnum());
		surhclmIO.setTranno(wsaaTranno.toInt());		
		surhclmIO.setPlanSuffix(covrpf.getPlanSuffix());
		surhclmIO.setFormat(formatsInner.surhclmrec);
		surhclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)
		&& isNE(surhclmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		
		if (isEQ(surhclmIO.getStatuz(), varcom.mrnf)) {
			wsaaFirstTime.set("Y");
		}
		else {
			wsaaFirstTime.set("N");
		}
		
		if (firstTime.isTrue()) {
			createSurhclm3200();
		}
		createSurdpf();
		//writePyou();
		
		}
	
		return;
		/* EXIT */
	}
	
	protected void createSurdpf()
	{
	
		if (isEQ(sv.refamt, ZERO) && isEQ(sv.refprm, ZERO) && isEQ(sv.refadjust, ZERO)) {
			return;
		}
		surdpf = new Surdpf();
		surdpf.setChdrcoy(covrpup.getChdrcoy());
		surdpf.setChdrnum(covrpup.getChdrnum());
		surdpf.setTranno(wsaaTranno.toInt());		
		surdpf.setPlnsfx(covrpup.getPlanSuffix());
		surdpf.setLife(covrpup.getLife());
		surdpf.setCoverage(covrpup.getCoverage());
		surdpf.setRider(covrpup.getRider());
		surdpfList = new ArrayList<Surdpf>();
		surdpfList = surdpfDAO.searchSurdclmRecord(surdpf.getChdrcoy(), surdpf.getChdrnum(), surdpf.getLife(), surdpf.getCoverage(), surdpf.getRider(), surdpf.getPlnsfx(), surdpf.getTranno());
		if(surdpfList.size() == 0)
		{
			surdpf.setCrtable(covrpup.getCrtable());	
			surdpf.setShortds(SPACE);
			surdpf.setTypeT("C");
			surdpf.setVrtfund(SPACE);
			surdpf.setActvalue(sv.refprm.getbigdata());
			surdpf.setEmv(BigDecimal.ZERO);
			surdpf.setCurrcd(sv.cntcurr.toString());	
			surdpf.setEffdate(wsspcomn.currfrom.toInt());
			surdpf.setOtheradjst(sv.refadjust.getbigdata());
			surdpfList.add(surdpf);	
			
			if(surdpfList != null && surdpfList.size() > 0){
				 surdpfDAO.insertSurdpfList(surdpfList);
				 surdpfList.clear();
			}writePyou();
		}
	
	}
	protected void createSurhclm3200()
	{			
		 surhpf = new Surhpf();

		 surhpf.setPlanSuffix(covrpup.getPlanSuffix());
		 surhpf.setChdrcoy(wsspcomn.company.toString());
		 surhpf.setChdrnum(sv.chdrnum.toString());
		 surhpf.setLife(covrpup.getLife());/* IJTI-1386 */
		 surhpf.setJlife(SPACE);
		 surhpf.setCurrcd(sv.cntcurr.toString());
		 surhpf.setEffdate(wsspcomn.currfrom.toInt());
		 surhpf.setTranno(wsaaTranno.toInt());
		 surhpf.setCnttype(chdrpf.getCnttype());/* IJTI-1386 */
		 surhpf.setReasoncd(SPACE);
		 surhpf.setResndesc(SPACE);
		 surhpf.setPolicyloan(BigDecimal.ZERO);
		 surhpf.setTdbtamt(BigDecimal.ZERO);
		 surhpf.setZrcshamt(BigDecimal.ZERO);
		 surhpf.setOtheradjst(BigDecimal.ZERO);	
		 surhpf.setTaxamt(BigDecimal.ZERO);
		
		surhpfList = new ArrayList<Surhpf>();
		surhpfList.add(surhpf);
		
		if(surhpfList != null && surhpfList.size() > 0){
			surhpfDAO.insertSurhpfList(surhpfList);
			surhpfList.clear();
		}
		
	}
	
	protected void writePyou()
	{
		pyoupf.setChdrpfx("CH");
		pyoupf.setChdrcoy(wsspcomn.company.toString());
		pyoupf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
		pyoupf.setTranno(wsaaTranno.toInt());
		pyoupf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	   	pyoupf.setPayrnum(sv.payeeno.toString().trim());
		pyoupf.setReqntype(sv.mop.toString());
		
		wsaaRldgChdrnum.set(chdrpf.getChdrnum());
		wsaaRldgLife.set(surdpf.getLife());
		wsaaRldgCoverage.set(surdpf.getCoverage());
		wsaaRldgRider.set(surdpf.getRider());
		wsaaPlan.set(surdpf.getPlnsfx());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		
		pyoupf.setRldgacct(wsaaRldgacct.toString());
	   		
		if(isNE(sv.drctdbtno,SPACE)){
			pyoupf.setBankacckey(sv.drctdbtno.toString());
		}else if(isNE(sv.crdtcrdno,SPACE)){
			pyoupf.setBankacckey(sv.crdtcrdno.toString());
	    }else if(isEQ(sv.drctdbtno,SPACE)){
	    	pyoupf.setBankacckey(sv.crdtcrdno.toString());
	    }else if(isEQ(sv.crdtcrdno,SPACE)){
	        pyoupf.setBankacckey(sv.drctdbtno.toString());
	     }
		
		pyoupf.setBankkey(sv.bnkcode.toString());
		pyoupf.setBankdesc(sv.bnkcdedsc.toString());
		pyoupf.setPaymentflag(SPACE);  
		pyoupf.setReqnno(SPACE);
		
		try{
			pyoupfDAO.insertPyoupfRecord(pyoupf);
			
		}catch(Exception e){		
			e.printStackTrace();
			fatalError600();
		}
	}
	

	protected void insertCpsupf(){
		cpsupf.setChdrpfx(chdrpf.getChdrpfx());/* IJTI-1386 */
		cpsupf.setChdrcoy(chdrpf.getChdrcoy().toString());
		cpsupf.setChdrnum(sv.chdrnum.toString());
		cpsupf.setTranno(wsaaTranno.toInt());
		cpsupf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		cpsupf.setRider(covrpup.getRider());
		cpsupf.setCrtable(covrpup.getCrtable());
		cpsupf.setCnttype(sv.cnttype.toString());
		cpsupf.setComprskcode(t5679rec.setRidRiskStat.toString().trim());
		cpsupf.setComppremcode(sv.comppremcode.toString());
		cpsupf.setCompsumss(sv.compsumss.toDouble());
		cpsupf.setCompbilprm(sv.compbilprm.toDouble());
		cpsupf.setRefprm(sv.refprm.toDouble());
		cpsupf.setRefadjust(sv.refadjust.toDouble());
		cpsupf.setRefamt(sv.refamt.toDouble());
		cpsupf.setStatus("R");
		cpsupf.setPayrnum(sv.payeeno.toString().trim());
		cpsupf.setReqntype(sv.mop.toString());
		if(isNE(sv.drctdbtno,SPACE)){
			cpsupf.setBankacckey(sv.drctdbtno.toString());
		}else if(isNE(sv.crdtcrdno,SPACE)){
			cpsupf.setBankacckey(sv.crdtcrdno.toString());
	    }else if(isEQ(sv.drctdbtno,SPACE)){
	    	cpsupf.setBankacckey(sv.crdtcrdno.toString());
	    }else if(isEQ(sv.crdtcrdno,SPACE)){
	    	cpsupf.setBankacckey(sv.drctdbtno.toString());
	     }		
		cpsupf.setBankkey(sv.bnkcode.toString());
		cpsupf.setBankdesc(sv.bnkcdedsc.toString());		
		cpsupf.setCompstatus(chdrpf.getStatcode());/* IJTI-1386 */
		cpsupf.setValidflag("1");
		cpsupf.setLife(covrpup.getLife());
		cpsupf.setCoverage(covrpup.getCoverage());		
		cpsupf.setPlnsfx(covrpf.getPlanSuffix());
				
		cpsupfDAO.insertCpsupfRecord(cpsupf);
	}
		
	protected void deleteCpsupf(){  // Reversal
		cpsupf.setChdrnum(sv.chdrnum.toString().trim());
		cpsupf.setCrtable(sv.compcode.toString().trim());
		cpsupf.setStatus("R");
		cpsupfDAO.deleteCpsupfRecord(cpsupf);
	}
		
		
	protected void updateInsertCovrpf(){ // Component Register
		//Update covrpf validflag=2	
		
		covrpup.setValidflag("2");	
		covrpf.setCurrto((payrpf.getPtdate()-1));
		covrpupDAO.updateCoverPf(covrpup);
		
		//insert new record with validflag=1
		Covrpf covrpfNew = new Covrpf(covrpup);
		covrpfNew.setValidflag("1");		
		covrpfNew.setTranno(wsaaTranno.toInt()); 	
		covrpfNew.setStatcode(t5679rec.setRidRiskStat.toString().trim());
		covrpfNew.setPstatcode(covrpup.getPstatcode());
		covrpfNew.setCurrto(varcom.vrcmMaxDate.toInt());
		covrpfNew.setCurrfrom(payrpf.getPtdate());
		if(insertCovrlnbList == null){
			insertCovrlnbList = new LinkedList<>();
		}
		insertCovrlnbList.add(covrpfNew);
		
		if (insertCovrlnbList != null && !insertCovrlnbList.isEmpty()) {
			covrpupDAO.insertCovrRecord(insertCovrlnbList);
			insertCovrlnbList.clear();
		}	
	}
	
	protected void deleteupdateCovrpf(){ // Component Register Reversal
		
		
		covrpupDAO.deleteByFlag("1", sv.chdrnum.trim(), covrpf.getCrtable());/* IJTI-1386 */
		
		covrpup.setValidflag("1");
		covrpupDAO.updateUniqueNo(covrpup);		
	}
	
	protected void updateCovrpf(){ // component Approval
		covrpf.setChdrnum(sv.chdrnum.toString().trim());
		covrpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		covrpf.setCrtable(covrpf.getCrtable());/* IJTI-1386 */
		covrpf.setValidflag("1");		
		covrpf.setPstatcode(t5679rec.setRidPremStat.toString().trim());
		
		covrpupDAO.updateCoverPfStatus(covrpf);
	}

	

	protected void bankBranchDesc() {
		babrIO.setDataKey(SPACES);
		babrIO.setBankkey(wsaaBnkkey.trim());
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isEQ(babrIO.getStatuz(), varcom.mrnf)) {
			sv.bnkcodeErr.set(f906);
		}
		if (isNE(babrIO.getStatuz(), varcom.oK) && isNE(babrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		wsaaBankkey.set(babrIO.getBankdesc());
		sv.bnkcdedsc.set(wsaaBankdesc);
	}

	/**
	 * <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000() {

		/* NEXT-PROGRAM */
		if (isEQ(scrnparams.statuz, "KILL")) {
			return;
		}
		wsspcomn.confirmationKey.set(sv.chdrnum);
		wsspcomn.programPtr.add(1);
		/* EXIT */

	}

	/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */

	
	private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData rf05 = new FixedLengthStringData(4).init("RF05");
	}
	
	protected static final class FormatsInner {
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
		private FixedLengthStringData uwlvrec = new FixedLengthStringData(10).init("UWLVREC");
		private FixedLengthStringData surhclmrec = new FixedLengthStringData(10).init("SURHCLMREC");

		public FixedLengthStringData getItemrec() {
			return itemrec;
		}

		public void setItemrec(FixedLengthStringData itemrec) {
			this.itemrec = itemrec;
		}
	}

}