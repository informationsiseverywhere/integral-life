/******************************************************************************
 * File Name 		: ZlreinpfDAO.java
 * Author			: rsubramani42
 * Creation Date	: 13 April 2018
 * Project			: Integral Life
 * Description		: The DAO Interface for ZLREINPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.terminationclaims.dataaccess.dao;


import com.csc.smart400framework.dataaccess.dao.BaseDAO;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Zlreinpf;

public interface ZlreinpfDAO extends BaseDAO<Zlreinpf> {

	//public List<Zlreinpf> searchZlreinpfRecord(<Your_Arguments>) throws SQLRuntimeException;

	//public void insertIntoZlreinpf(Zlreinpf zlreinpf)  ;
	
	public void bulkUpdateZlreinpf(List<Zlreinpf> zlreinpfUpdList); 

	public void bulkDeleteZlreinpf(List<String> chdrList) ;

}
