/*

 * File: Pr679hca.java
 * Date: 30 August 2009 1:54:16
 * Author: Quipoz Limited
 * 
 * Class transformed from PR679HCA.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.terminationclaims.dataaccess.HclaTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  This  subroutine  is  called  from  OPTSWCH Table T1661. It
*  determines whether any HCLA records exist for the component
*  being enquired upon in PR679.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr679hca extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "PR679HCA";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String hclarec = "HCLAREC";
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
		/*Contract Enquiry - Coverage Details.*/
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
		/*Hospital Benefit Claim Accumulation(all)*/
	private HclaTableDAM hclaIO = new HclaTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		errorProg610
	}

	public Pr679hca() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			performs0010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void performs0010()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
		covrpf = covrDao.getCacheObject(covrpf);
		if(covrpf==null)
		{
			return;
		}
		hclaIO.setParams(SPACES);
		hclaIO.setChdrcoy(covrpf.getChdrcoy());
		hclaIO.setChdrnum(covrpf.getChdrnum());
		hclaIO.setLife(covrpf.getLife());
		hclaIO.setCoverage(covrpf.getCoverage());
		hclaIO.setRider(covrpf.getRider());
		hclaIO.setFormat(hclarec);
		hclaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hclaIO);
		if (isNE(hclaIO.getStatuz(),varcom.oK)
		&& isNE(hclaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hclaIO.getParams());
			syserrrec.statuz.set(hclaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(hclaIO.getStatuz(),varcom.oK)) {
			wsaaChkpStatuz.set("DFND");
		}
		else {
			wsaaChkpStatuz.set("NFND");
			goTo(GotoLabel.exit090);
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
			syserrrec.syserrStatuz.set(syserrrec.statuz);
		}
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
