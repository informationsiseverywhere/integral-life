package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.RegxpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Regxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RegxpfDAOImpl extends BaseDAOImpl<Regxpf> implements RegxpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(RegxpfDAOImpl.class);

	public List<Regxpf> searchRegxpfRecord(String tableId, String memName, int batchExtractSize, int batchID) {
		StringBuilder sqlRegxSelect = new StringBuilder();
		sqlRegxSelect.append(" SELECT * FROM ( ");
		sqlRegxSelect
				.append(" SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,RGPYNUM,PLNSFX,TRANNO,NPAYDATE,RGPYTYPE,CRTABLE,VALIDFLAG,EPAYDATE,REVDTE,RGPYSTAT ");
		sqlRegxSelect
				.append(" ,FLOOR((ROW_NUMBER()OVER( ORDER BY CHDRCOY ASC, CHDRNUM ASC, UNIQUE_NUMBER DESC)-1)/?) BATCHNUM ");
		sqlRegxSelect.append("  FROM ");
		sqlRegxSelect.append(tableId);
		sqlRegxSelect.append("   WHERE MEMBER_NAME = ? ");
		sqlRegxSelect.append(" ) MAIN WHERE BATCHNUM = ? ");

		PreparedStatement psRegxSelect = getPrepareStatement(sqlRegxSelect.toString());
		ResultSet sqlregxpf1rs = null;
		List<Regxpf> regxpfList = new ArrayList<>();
		try {
			psRegxSelect.setInt(1, batchExtractSize);
			psRegxSelect.setString(2, memName);
			psRegxSelect.setInt(3, batchID);

			sqlregxpf1rs = executeQuery(psRegxSelect);
			while (sqlregxpf1rs.next()) {
				Regxpf regxpf = new Regxpf();
				regxpf.setChdrcoy(sqlregxpf1rs.getString("chdrcoy"));
				regxpf.setChdrnum(sqlregxpf1rs.getString("chdrnum"));
				regxpf.setLife(sqlregxpf1rs.getString("life"));
				regxpf.setCoverage(sqlregxpf1rs.getString("coverage"));
				regxpf.setRider(sqlregxpf1rs.getString("rider"));
				regxpf.setPlanSuffix(sqlregxpf1rs.getInt("plnsfx"));
				regxpf.setTranno(sqlregxpf1rs.getInt("tranno"));
				regxpf.setRgpynum(sqlregxpf1rs.getInt("rgpynum"));
				regxpf.setNextPaydate(sqlregxpf1rs.getInt("npaydate"));
				regxpf.setRgpytype(sqlregxpf1rs.getString("rgpytype"));
				regxpf.setCrtable(sqlregxpf1rs.getString("crtable"));
				regxpf.setValidflag(sqlregxpf1rs.getString("validflag"));
				regxpf.setFinalPaydate(sqlregxpf1rs.getInt("epaydate"));
				regxpf.setRevdte(sqlregxpf1rs.getInt("revdte"));
				regxpf.setRgpystat(sqlregxpf1rs.getString("rgpystat"));
				regxpf.setUniqueNumber(sqlregxpf1rs.getLong("UNIQUE_NUMBER"));
				regxpfList.add(regxpf);
			}
		} catch (SQLException e) {
			LOGGER.error("searchRegxpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psRegxSelect, sqlregxpf1rs);
		}
		return regxpfList;
	}

	@Override
	public List<Regxpf> findResult(String tableName, String memName, int batchExtractSize, int batchID) {
        StringBuilder sqlStr = new StringBuilder();
        sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT FLOOR((ROW_NUMBER() OVER (ORDER BY TE.CHDRCOY, TE.CHDRNUM)-1)/?) ROWNM, TE.* FROM ");
        sqlStr.append(tableName);
        sqlStr.append(" TE");
        sqlStr.append(" WHERE MEMBER_NAME = ? )cc WHERE cc.ROWNM = ?");
        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		List<Regxpf> pfList = new ArrayList<Regxpf>();
		ResultSet rs = null;
        try {
        	ps.setInt(1, batchExtractSize);
        	ps.setString(2, memName);
        	ps.setInt(3, batchID);
        	rs = ps.executeQuery();
            while (rs.next()) {
            	Regxpf pf = new Regxpf();
            	pf.setChdrcoy(rs.getString("CHDRCOY"));
            	pf.setChdrnum(rs.getString("CHDRNUM"));
            	pf.setLife(rs.getString("LIFE"));
            	pf.setCoverage(rs.getString("COVERAGE"));
             	pf.setRider(rs.getString("RIDER"));
            	pf.setRgpynum(rs.getInt("RGPYNUM"));
             	pf.setPlanSuffix(rs.getInt("plnsfx"));
            	pf.setTranno(rs.getInt("tranno"));
             	pf.setNextPaydate(rs.getInt("npaydate"));
            	pf.setRgpytype(rs.getString("rgpytype"));
             	pf.setCrtable(rs.getString("crtable"));
            	pf.setValidflag(rs.getString("validflag"));
             	pf.setFinalPaydate(rs.getInt("epaydate"));
            	pf.setRevdte(rs.getInt("revdte"));
            	pf.setRgpystat(rs.getString("rgpystat"));
            	pfList.add(pf);
            }

        } catch (SQLException e) {
            LOGGER.error("findResult()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return pfList;

	}
}
