package com.csc.life.terminationclaims.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5231.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5231Report extends SMARTReportLayout { 

	private ZonedDecimalData bonusvalue = new ZonedDecimalData(17, 2);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private FixedLengthStringData currency = new FixedLengthStringData(3);
	private FixedLengthStringData datecfrom = new FixedLengthStringData(10);
	private FixedLengthStringData datecto = new FixedLengthStringData(10);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private ZonedDecimalData emvi = new ZonedDecimalData(17, 2);
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData rcesdte = new FixedLengthStringData(10);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData sumin = new ZonedDecimalData(15, 0);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5231Report() {
		super();
	}


	/**
	 * Print the XML for R5231d01
	 */
	public void printR5231d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 9, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 11, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 13, 2));
		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 15, 4));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 19, 30));
		sumin.setFieldName("sumin");
		sumin.setInternal(subString(recordData, 49, 15));
		rcesdte.setFieldName("rcesdte");
		rcesdte.setInternal(subString(recordData, 64, 10));
		currency.setFieldName("currency");
		currency.setInternal(subString(recordData, 74, 3));
		bonusvalue.setFieldName("bonusvalue");
		bonusvalue.setInternal(subString(recordData, 77, 17));
		emvi.setFieldName("emvi");
		emvi.setInternal(subString(recordData, 94, 17));
		printLayout("R5231d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				life,
				coverage,
				rider,
				crtable,
				descrip,
				sumin,
				rcesdte,
				currency,
				bonusvalue,
				emvi
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for R5231d02
	 */
	public void printR5231d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		printLayout("R5231d02",			// Record name
			new BaseData[]{			// Fields:
				chdrnum
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5231h01
	 */
	public void printR5231h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		datecfrom.setFieldName("datecfrom");
		datecfrom.setInternal(subString(recordData, 1, 10));
		datecto.setFieldName("datecto");
		datecto.setInternal(subString(recordData, 11, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 21, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 22, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 52, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 62, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 64, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5231h01",			// Record name
			new BaseData[]{			// Fields:
				datecfrom,
				datecto,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(15);
	}


}
