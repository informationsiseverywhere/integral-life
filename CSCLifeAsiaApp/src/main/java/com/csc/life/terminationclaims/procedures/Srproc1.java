/*
 * File: Srproc1.java
 * Date: 30 August 2009 2:15:34
 * Author: Quipoz Limited
 * 
 * Class transformed from SRPROC1.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  PROCESSING FOR FULL SURRENDER VALUE.
*
*****************************************************************
* </pre>
*/
public class Srproc1 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "SRPROC1";
		/* ERRORS */
	private String h115 = "H115";
	private String f294 = "F294";
	private String h072 = "H072";
		/* TABLES */
	private String t6647 = "T6647";
	private String t5645 = "T5645";
	private String t5687 = "T5687";
	private String t6598 = "T6598";
	private String utrnrec = "UTRNREC";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaTrigger = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaTriggerChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTrigger, 0);
	private FixedLengthStringData wsaaTriggerLife = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 8);
	private FixedLengthStringData wsaaTriggerCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 10);
	private FixedLengthStringData wsaaTriggerRider = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 12);
	private FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(wsaaTrigger, 14, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTabBatckey = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);

	private FixedLengthStringData wsaaTime = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaHhmmss = new ZonedDecimalData(6, 0).isAPartOf(wsaaTime, 0).setUnsigned();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Surpcpy surpcpy = new Surpcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T6647rec t6647rec = new T6647rec();
		/*UTRN LOGICAL VIEW.*/
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit290, 
		exit9020
	}

	public Srproc1() {
		super();
	}

public void mainline(Object... parmArray)
	{
		surpcpy.surrenderRec = convertAndSetParam(surpcpy.surrenderRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		surpcpy.status.set("****");
		syserrrec.subrname.set(wsaaSubr);
		wsaaBatckey.set(surpcpy.batckey);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaTime.set(getCobolTime());
		if (isEQ(surpcpy.type,"F")) {
			mainProcessing200();
		}
		/*EXIT*/
		exitProgram();
	}

protected void mainProcessing200()
	{
		try {
			para201();
		}
		catch (GOTOException e){
		}
	}

protected void para201()
	{
		if (isEQ(surpcpy.estimatedVal,0)) {
			goTo(GotoLabel.exit290);
		}
		if (isEQ(surpcpy.status,"****")) {
			readTabT6647300();
		}
		if (isEQ(surpcpy.status,"****")) {
			readTabT5645400();
		}
		if (isEQ(surpcpy.status,"****")) {
			getTrigger500();
		}
		setupUtrns600();
	}

protected void readTabT6647300()
	{
		read331();
	}

protected void read331()
	{
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaTabBatckey.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(surpcpy.cnttype);
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaT6647Key,itdmIO.getItemitem())
		|| isNE(surpcpy.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t6647)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			surpcpy.status.set(h115);
			fatalError9000();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTabT5645400()
	{
		/*READ*/
		itemIO.setItemcoy(surpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),"****")) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void getTrigger500()
	{
		read531();
	}

protected void read531()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(surpcpy.crtable);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),surpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),surpcpy.crtable)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			surpcpy.status.set(f294);
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t6598);
		itdmIO.setItemitem(t5687rec.svMethod);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),surpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6598)
		|| isNE(itdmIO.getItemitem(),t5687rec.svMethod)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			surpcpy.status.set(h072);
			fatalError9000();
		}
		else {
			t6598rec.t6598Rec.set(itdmIO.getGenarea());
		}
	}

protected void setupUtrns600()
	{
		setupUtrnsPara600();
	}

protected void setupUtrnsPara600()
	{
		utrnIO.setParams(SPACES);
		utrnIO.setTranno(ZERO);
		utrnIO.setPlanSuffix(ZERO);
		utrnIO.setBatcactyr(ZERO);
		utrnIO.setBatcactmn(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setContractAmount(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setProcSeqNo(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setSvp(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setCrComDate(ZERO);
		utrnIO.setChdrcoy(surpcpy.chdrcoy);
		wsaaTriggerChdrnum.set(surpcpy.chdrcoy);
		utrnIO.setChdrnum(surpcpy.chdrnum);
		utrnIO.setLife(surpcpy.life);
		wsaaTriggerLife.set(surpcpy.life);
		utrnIO.setCoverage(surpcpy.coverage);
		wsaaTriggerCoverage.set(surpcpy.coverage);
		utrnIO.setRider(surpcpy.rider);
		wsaaTriggerRider.set(surpcpy.rider);
		utrnIO.setCrtable(surpcpy.crtable);
		utrnIO.setMoniesDate(surpcpy.effdate);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setSurrenderPercent(100);
		utrnIO.setFundAmount(0);
		utrnIO.setUnitVirtualFund(surpcpy.element);
		utrnIO.setContractType(surpcpy.cnttype);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setNowDeferInd(t6647rec.dealin);
		utrnIO.setSacscode(t5645rec.sacscode01);
		utrnIO.setSacstyp(t5645rec.sacstype01);
		utrnIO.setGenlcde(t5645rec.glmap01);
		utrnIO.setTriggerModule(t6598rec.procesprog);
		utrnIO.setTriggerKey(wsaaTrigger);
		utrnIO.setNofUnits(0);
		utrnIO.setTransactionDate(wsaaToday);
		utrnIO.setTransactionTime(wsaaTime);
		utrnIO.setTranno(0);
		utrnIO.setSvp(1);
		utrnIO.setTermid(SPACES);
		utrnIO.setFormat(utrnrec);
		utrnIO.setFunction("WRITR");
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getParams(),"****")) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		surpcpy.status.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
