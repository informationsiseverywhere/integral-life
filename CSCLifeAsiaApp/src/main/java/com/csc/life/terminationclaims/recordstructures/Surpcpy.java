package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:57
 * Description:
 * Copybook name: SURPCPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Surpcpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
/* bug #ILIFE-778 start*/  
//  	public FixedLengthStringData surrenderRec = new FixedLengthStringData(159);
	public FixedLengthStringData surrenderRec = new FixedLengthStringData(160);
/* bug #ILIFE-778 end*/ 
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(surrenderRec, 0);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(surrenderRec, 1);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(surrenderRec, 9);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(surrenderRec, 11);
  	public PackedDecimalData planSuffix = new PackedDecimalData(5, 0).isAPartOf(surrenderRec, 13);
  	public FixedLengthStringData fund = new FixedLengthStringData(4).isAPartOf(surrenderRec, 16);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(surrenderRec, 20);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(surrenderRec, 22);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(surrenderRec, 24);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(surrenderRec, 28);
  	public PackedDecimalData estimatedVal = new PackedDecimalData(17, 2).isAPartOf(surrenderRec, 33);
  	public PackedDecimalData actualVal = new PackedDecimalData(17, 2).isAPartOf(surrenderRec, 42);
  	public PackedDecimalData percreqd = new PackedDecimalData(5, 2).isAPartOf(surrenderRec, 51).setUnsigned();
  	//MIBT-387
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(surrenderRec, 55);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(surrenderRec, 58);
  	public FixedLengthStringData element = new FixedLengthStringData(1).isAPartOf(surrenderRec, 61);
  	public FixedLengthStringData type = new FixedLengthStringData(1).isAPartOf(surrenderRec, 62);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(surrenderRec, 63);
  	public FixedLengthStringData batckey = new FixedLengthStringData(64).isAPartOf(surrenderRec, 67);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(surrenderRec, 131);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(surrenderRec, 134);
  	public ZonedDecimalData date_var = new ZonedDecimalData(6, 0).isAPartOf(surrenderRec, 138).setUnsigned();
  	public ZonedDecimalData time = new ZonedDecimalData(6, 0).isAPartOf(surrenderRec, 144).setUnsigned();
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(surrenderRec, 150).setUnsigned();
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(surrenderRec, 156);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(surrenderRec, 159);


	public void initialize() {
		COBOLFunctions.initialize(surrenderRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		surrenderRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}