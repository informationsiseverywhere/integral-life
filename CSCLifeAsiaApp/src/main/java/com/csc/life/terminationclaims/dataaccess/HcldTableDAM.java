package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HcldTableDAM.java
 * Date: Sun, 30 Aug 2009 03:40:24
 * Class transformed from HCLD.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HcldTableDAM extends HcldpfTableDAM {

	public HcldTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HCLD");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", RGPYNUM"
		             + ", HOSBEN";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "CRTABLE, " +
		            "RGPYNUM, " +
		            "BENPLN, " +
		            "HOSBEN, " +
		            "DATEFRM, " +
		            "DATETO, " +
		            "ACTEXP, " +
		            "NOFDAY, " +
		            "BENFAMT, " +
		            "ZDAYCOV, " +
		            "GDEDUCT, " +
		            "COIAMT, " +
		            "GCNETPY, " +
		            "AMTFLD, " +
		            "LMTYEAR, " +
		            "LMTLIFE, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "RGPYNUM DESC, " +
		            "HOSBEN ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "RGPYNUM ASC, " +
		            "HOSBEN DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               crtable,
                               rgpynum,
                               benpln,
                               hosben,
                               datefrm,
                               dateto,
                               actexp,
                               nofday,
                               benfamt,
                               zdaycov,
                               gdeduct,
                               coiamt,
                               gcnetpy,
                               amtfld,
                               lmtyear,
                               lmtlife,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(233);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getRgpynum().toInternal()
					+ getHosben().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, rgpynum);
			what = ExternalData.chop(what, hosben);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller70.setInternal(rgpynum.toInternal());
	nonKeyFiller90.setInternal(hosben.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(129);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getCrtable().toInternal()
					+ nonKeyFiller70.toInternal()
					+ getBenpln().toInternal()
					+ nonKeyFiller90.toInternal()
					+ getDatefrm().toInternal()
					+ getDateto().toInternal()
					+ getActexp().toInternal()
					+ getNofday().toInternal()
					+ getBenfamt().toInternal()
					+ getZdaycov().toInternal()
					+ getGdeduct().toInternal()
					+ getCoiamt().toInternal()
					+ getGcnetpy().toInternal()
					+ getAmtfld().toInternal()
					+ getLmtyear().toInternal()
					+ getLmtlife().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, benpln);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, datefrm);
			what = ExternalData.chop(what, dateto);
			what = ExternalData.chop(what, actexp);
			what = ExternalData.chop(what, nofday);
			what = ExternalData.chop(what, benfamt);
			what = ExternalData.chop(what, zdaycov);
			what = ExternalData.chop(what, gdeduct);
			what = ExternalData.chop(what, coiamt);
			what = ExternalData.chop(what, gcnetpy);
			what = ExternalData.chop(what, amtfld);
			what = ExternalData.chop(what, lmtyear);
			what = ExternalData.chop(what, lmtlife);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getRgpynum() {
		return rgpynum;
	}
	public void setRgpynum(Object what) {
		setRgpynum(what, false);
	}
	public void setRgpynum(Object what, boolean rounded) {
		if (rounded)
			rgpynum.setRounded(what);
		else
			rgpynum.set(what);
	}
	public FixedLengthStringData getHosben() {
		return hosben;
	}
	public void setHosben(Object what) {
		hosben.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getBenpln() {
		return benpln;
	}
	public void setBenpln(Object what) {
		benpln.set(what);
	}	
	public PackedDecimalData getDatefrm() {
		return datefrm;
	}
	public void setDatefrm(Object what) {
		setDatefrm(what, false);
	}
	public void setDatefrm(Object what, boolean rounded) {
		if (rounded)
			datefrm.setRounded(what);
		else
			datefrm.set(what);
	}	
	public PackedDecimalData getDateto() {
		return dateto;
	}
	public void setDateto(Object what) {
		setDateto(what, false);
	}
	public void setDateto(Object what, boolean rounded) {
		if (rounded)
			dateto.setRounded(what);
		else
			dateto.set(what);
	}	
	public PackedDecimalData getActexp() {
		return actexp;
	}
	public void setActexp(Object what) {
		setActexp(what, false);
	}
	public void setActexp(Object what, boolean rounded) {
		if (rounded)
			actexp.setRounded(what);
		else
			actexp.set(what);
	}	
	public PackedDecimalData getNofday() {
		return nofday;
	}
	public void setNofday(Object what) {
		setNofday(what, false);
	}
	public void setNofday(Object what, boolean rounded) {
		if (rounded)
			nofday.setRounded(what);
		else
			nofday.set(what);
	}	
	public PackedDecimalData getBenfamt() {
		return benfamt;
	}
	public void setBenfamt(Object what) {
		setBenfamt(what, false);
	}
	public void setBenfamt(Object what, boolean rounded) {
		if (rounded)
			benfamt.setRounded(what);
		else
			benfamt.set(what);
	}	
	public PackedDecimalData getZdaycov() {
		return zdaycov;
	}
	public void setZdaycov(Object what) {
		setZdaycov(what, false);
	}
	public void setZdaycov(Object what, boolean rounded) {
		if (rounded)
			zdaycov.setRounded(what);
		else
			zdaycov.set(what);
	}	
	public PackedDecimalData getGdeduct() {
		return gdeduct;
	}
	public void setGdeduct(Object what) {
		setGdeduct(what, false);
	}
	public void setGdeduct(Object what, boolean rounded) {
		if (rounded)
			gdeduct.setRounded(what);
		else
			gdeduct.set(what);
	}	
	public PackedDecimalData getCoiamt() {
		return coiamt;
	}
	public void setCoiamt(Object what) {
		setCoiamt(what, false);
	}
	public void setCoiamt(Object what, boolean rounded) {
		if (rounded)
			coiamt.setRounded(what);
		else
			coiamt.set(what);
	}	
	public PackedDecimalData getGcnetpy() {
		return gcnetpy;
	}
	public void setGcnetpy(Object what) {
		setGcnetpy(what, false);
	}
	public void setGcnetpy(Object what, boolean rounded) {
		if (rounded)
			gcnetpy.setRounded(what);
		else
			gcnetpy.set(what);
	}	
	public PackedDecimalData getAmtfld() {
		return amtfld;
	}
	public void setAmtfld(Object what) {
		setAmtfld(what, false);
	}
	public void setAmtfld(Object what, boolean rounded) {
		if (rounded)
			amtfld.setRounded(what);
		else
			amtfld.set(what);
	}	
	public FixedLengthStringData getLmtyear() {
		return lmtyear;
	}
	public void setLmtyear(Object what) {
		lmtyear.set(what);
	}	
	public FixedLengthStringData getLmtlife() {
		return lmtlife;
	}
	public void setLmtlife(Object what) {
		lmtlife.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		rgpynum.clear();
		hosben.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		crtable.clear();
		nonKeyFiller70.clear();
		benpln.clear();
		nonKeyFiller90.clear();
		datefrm.clear();
		dateto.clear();
		actexp.clear();
		nofday.clear();
		benfamt.clear();
		zdaycov.clear();
		gdeduct.clear();
		coiamt.clear();
		gcnetpy.clear();
		amtfld.clear();
		lmtyear.clear();
		lmtlife.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}