package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:13
 * Description:
 * Copybook name: CHDRCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrclmKey = new FixedLengthStringData(64).isAPartOf(chdrclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrclmChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrclmKey, 0);
  	public FixedLengthStringData chdrclmChdrnum = new FixedLengthStringData(8).isAPartOf(chdrclmKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrclmKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}