package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.financials.dataaccess.dao.CheqpfDAO;
import com.csc.fsu.financials.dataaccess.dao.model.Cheqpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.dao.PymtpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.dataaccess.model.Pymtpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.tablestructures.T3699rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.CrsvpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Crsvpf;
import com.csc.life.terminationclaims.screens.Sjl55ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart.utils.UserMapingUtils;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* CLAIM PAYMENT ENQUIRY/APPROVE/AUTHORIZE.
**/

public class Pjl55 extends ScreenProgCS {

	private static final String SJL55 = "Sjl55";
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl55.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL55");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private static final String T3593 = "T3593";

	private Sjl55ScreenVars sv =ScreenProgram.getScreenVars( Sjl55ScreenVars.class);
	private Wsspsmart wsspsmart = new Wsspsmart();
	private CheqpfDAO cheqpfDAO = getApplicationContext().getBean("cheqpfDAO",CheqpfDAO.class);//ILB-463
	private Pymtpf pymtpf = new Pymtpf();
	private ZonedDecimalData startingIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private PymtpfDAO pymtpfDAO = getApplicationContext().getBean("pymtpfDAO",PymtpfDAO.class);
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Tr386rec tr386rec = new Tr386rec();
	private List<Pymtpf> pytmpfList = new ArrayList<>();
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private Cattpf cattpf;
	private CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private FixedLengthStringData wsaaToday = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaTodayDate = new ZonedDecimalData(8).isAPartOf(wsaaToday);
	private ZonedDecimalData wsaaTodayYear = new ZonedDecimalData(4).isAPartOf(wsaaToday, 0);
	private ZonedDecimalData wsaaTodayMnth = new ZonedDecimalData(2).isAPartOf(wsaaToday);
	private ZonedDecimalData wsaaTodayDay = new ZonedDecimalData(2).isAPartOf(wsaaToday);
	private FixedLengthStringData wsaaNextday = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaNextdayYear = new ZonedDecimalData(4).isAPartOf(wsaaNextday, 0);
	private ZonedDecimalData wsaaNextdayMnth = new ZonedDecimalData(2).isAPartOf(wsaaNextday);
	private ZonedDecimalData wsaaNextdayDay = new ZonedDecimalData(2).isAPartOf(wsaaNextday);
	private FixedLengthStringData wsaaNexttoday = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaNexttodayDate = new ZonedDecimalData(8).isAPartOf(wsaaNexttoday);
	private ZonedDecimalData wsaaNexttodayYear = new ZonedDecimalData(4).isAPartOf(wsaaNexttoday, 0);
	private ZonedDecimalData wsaaNexttodayMnth = new ZonedDecimalData(2).isAPartOf(wsaaNexttoday);
	private ZonedDecimalData wsaaNexttodayDay = new ZonedDecimalData(2).isAPartOf(wsaaNexttoday);
	private Datcon1rec datcon1 = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private StringBuilder totaldays = new StringBuilder();
	private List<Pymtpf> updatePytmpfList = new ArrayList<>(); 
	private List<Cheqpf> cheqpfList = new ArrayList<>(); 
	private Crsvpf crsvpfItem = new Crsvpf();
	private CrsvpfDAO crsvpfDAO = getApplicationContext().getBean("crsvpfDAO", CrsvpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Ptrnpf ptrnpf = new Ptrnpf();
	private List<Ptrnpf> ptrnpfList = new ArrayList<>();
	private Batckey wsaaBatckey1 = new Batckey();
	private String longUserID;
/**
* Contains all possible labels used by goTo action.
*/

	public Pjl55() {
		super();
		screenVars = sv;
		new ScreenModel(SJL55, AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
@Override
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.info("mainline {}", e);
		}
	}
@Override
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo {}", e);
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
@Override
protected void initialise1000()
	{
		initialise1010();	
	}

protected void initialise1010()
	{
		longUserID = UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString());
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(Varcom.sclr);
		processScreen(SJL55, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		a7050Begin();
		initializeScreen1020();
		
		sv.clttwoOut[Varcom.pr.toInt()].set("Y");
		sv.bankacckeyOut[Varcom.pr.toInt()].set("Y");
		sv.prcentOut[Varcom.pr.toInt()].set("Y");
		sv.pymtOut[Varcom.pr.toInt()].set("Y");
		scrnparams.subfileRrn.set(1);
		loadSubfile1002();
	}

private void initializeScreen1020() {
	pytmpfList = pymtpfDAO.getpymtpfList(wsspcomn.confirmationKey.toString());
	if(!pytmpfList.isEmpty()){
		pymtpf = pytmpfList.get(0);
	}else {
		return;
	}
	
	sv.payNum.set(pymtpf.getPaynum_pymt());
	sv.claimNum.set(pymtpf.getClaimnum());
	
	cattpf = cattpfDAO.approvedClaimRecord(sv.claimNum.toString());
	if(null != cattpf) {
		Chdrpf chdrpfObj = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(), cattpf.getChdrnum());
		sv.contractNum.set(cattpf.getChdrnum().trim());
		sv.cnttype.set(chdrpfObj.getCnttype());
		Descpf descpfObj1 = descDAO.getdescData("IT", "T5688", chdrpfObj.getCnttype(), 
				wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpfObj1!=null) {
			sv.ctypedes.set(descpfObj1.getLongdesc().trim());
		}
		
		sv.clientnum.set(cattpf.getLifcnum().trim());
		Clntpf clntpfObj = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), cattpf.getLifcnum().trim());
		sv.clntName.set(clntpfObj.getLsurname().trim().concat(" ")
				.concat(clntpfObj.getLgivname().trim()));		
	}
	if(isEQ(wsspcomn.sbmaction, "F")) {
		sv.payDate.set(pymtpf.getPaydate());
	}else {
		setDate1005();
	}
	Descpf descpfObj;
	descpfObj = descDAO.getdescData("IT",  "T3672", pymtpf.getReqntype(), wsspcomn.company.toString(),
			wsspcomn.language.toString());
	if(descpfObj != null) {
		sv.payMethod.set(descpfObj.getLongdesc());
	}
	sv.totalPayAmt.set(pymtpf.getTotalpyblamount());
	
	sv.createdUser.set(pymtpf.getCreateuser());
	sv.createdDate.set(pymtpf.getCreatedate());
	sv.modifiedUser.set(pymtpf.getModuser());
	sv.modifiedDate.set(pymtpf.getModdate());
	sv.approvedUser.set(pymtpf.getAppruser());
	sv.approvedDate.set(pymtpf.getApprdte());
	sv.authorizedUser.set(pymtpf.getAuthuser());
	sv.authorizedDate.set(pymtpf.getAuthdate());
	
	/*    Get status description*/
	Descpf descpfObj1;
	descpfObj1 = descDAO.getdescData("IT",  T3593, pymtpf.getProcind(), wsspcomn.company.toString(),
			wsspcomn.language.toString());
	if(descpfObj1 != null) {
		sv.payStatus.set(pymtpf.getProcind() +"-"+ descpfObj1.getLongdesc().trim());
	}
}

protected void setDate1005() {
	datcon1.function.set(Varcom.tday);
	Datcon1.getInstance().mainline(datcon1.datcon1Rec);
	wsaaToday.set(datcon1.intDate);
	
	datcon2rec.intDate1.set(wsaaToday);
	datcon2rec.frequency.set("DY");
	datcon2rec.freqFactor.set(1);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	wsaaNextday.set(datcon2rec.intDate2); 
	
	datcon2rec.intDate1.set(wsaaNextday);
	datcon2rec.frequency.set("DY");
	datcon2rec.freqFactor.set(1);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	wsaaNexttoday.set(datcon2rec.intDate2); 
	
	if(isEQ(wsaaTodayYear,wsaaNextdayYear)) {
		setPaydateTypeOne();
	}
	else {
		setPaydateTypeTwo();
	}
}

protected void setPaydateTypeOne() {
	Itempf itempf = itempfDAO.findItemByItem(wsspcomn.fsuco.toString(), "T3699", wsaaTodayYear.toString());
	T3699rec t3699recTemp = new T3699rec();
	if(null != itempf) {
		t3699recTemp.t3699Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		if(isEQ(wsaaTodayMnth,wsaaNextdayMnth)) {
			for (int i = wsaaTodayMnth.toInt(); i <= 12; i++) {
				totaldays.append((t3699recTemp.daywh[i]).toString());
			}
		}
		else
		{
			for (int i = wsaaNextdayMnth.toInt(); i <= 12; i++) {
				totaldays.append((t3699recTemp.daywh[i]).toString());
			}
		}
		setPaydate();
	}
}

protected void setPaydate() {
	for(int i=wsaaNextdayDay.toInt(); i<=totaldays.length(); i++) {
		if(totaldays.toString().charAt(i-1) =='.') {
			int newDay = i;
			wsaaTodayDay.set(newDay);
			if(isEQ(wsaaTodayMnth,wsaaNextdayMnth)) {
				sv.payDate.set(wsaaTodayDate);
			}
			else {
				wsaaTodayMnth.set(wsaaNextdayMnth);
				sv.payDate.set(wsaaTodayDate);
			}
			break;
		}
	}
}

protected void setPaydateTypeTwo() {
	Itempf itempf = itempfDAO.findItemByItem(wsspcomn.fsuco.toString(), "T3699", wsaaNextdayYear.toString());
	T3699rec t3699recTemp = new T3699rec();
	if(null != itempf) {
		t3699recTemp.t3699Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		if(isEQ(wsaaNextdayMnth,wsaaNexttodayMnth)) {
			for (int i = wsaaNextdayMnth.toInt(); i <= 12; i++) {
				totaldays.append((t3699recTemp.daywh[i]).toString());
			}
		}	
		else
		{
			for (int i = wsaaNexttodayMnth.toInt(); i <= 12; i++) {
				totaldays.append((t3699recTemp.daywh[i]).toString());
			}
		}
		setPaydateTwo();
	}
}

protected void setPaydateTwo() {
	for(int i=wsaaNexttodayDay.toInt(); i<=totaldays.length(); i++) {
		if(totaldays.toString().charAt(i-1) =='.') {
			int newDay = i;
			wsaaNexttodayDay.set(newDay);
			if(isEQ(wsaaNextdayMnth,wsaaNexttodayMnth)) {
				sv.payDate.set(wsaaNexttodayDate);
			}
			else {
				wsaaNextdayMnth.set(wsaaNexttodayMnth);
				sv.payDate.set(wsaaNexttodayDate);
			}
			break;
		}
	}
}

	protected void a7050Begin()
	{
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		Itempf itempf = itempfDAO.findItemByItem(wsspcomn.company.toString(), "TR386",wsaaTr386Key.toString());
		if(itempf == null){
			return;
		}
		tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		if (isEQ(wsspcomn.sbmaction, "C")) {
			sv.scrndesc.set(tr386rec.progdesc[3].toString());
		} else if (isEQ(wsspcomn.sbmaction, "D")) {
			sv.scrndesc.set(tr386rec.progdesc[4].toString());
		} else if (isEQ(wsspcomn.sbmaction, "F")) {
			sv.scrndesc.set(tr386rec.progdesc[6].toString());
		}
	}

private void loadSubfile1002() {

	try{
		
		startingIndex.set(1);
	for (Pymtpf pymtpfrec: pytmpfList){
			sv.seqenum.set(startingIndex);
			sv.clttwo.set(pymtpfrec.getPayee());
			sv.clntID.set(pymtpfrec.getClntname());
			sv.babrdc.set(pymtpfrec.getBankkey()+" , "+ pymtpfrec.getBankdesc());	
			sv.accdesc.set(pymtpfrec.getAcctype());
			sv.bankacckey.set(pymtpfrec.getBankacckey());
			sv.bankaccdsc.set(pymtpfrec.getAcctname());
			sv.prcent.set(pymtpfrec.getPrnt());
			sv.pymt.set(pymtpfrec.getAmount());
		
			scrnparams.function.set(Varcom.sadd);
			processScreen("SJL55", sv);
			if (isNE(scrnparams.statuz, Varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			startingIndex.add(1);
		}
	}
	catch(Exception e){
		LOGGER.info("loadSubfile1002 {}", e);
	}

	
}

protected void preScreenEdit()
	{
	scrnparams.subfileRrn.set(1);
	}

@Override
protected void screenEdit2000()
	{
		scrnparams.subfileRrn.set(1); 
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(Varcom.srnch);
		processScreen(SJL55, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

@Override
protected void update3000()
	{
		if(isEQ(wsspcomn.sbmaction, "F")) {
			return;
		}
	
		if(isEQ(wsspcomn.sbmaction, "C")) {
			updateDatabaseForApproved();
		}else if(isEQ(wsspcomn.sbmaction, "D")) {
			updateDatabaseForAuthorized();
		}
		addPtrnData();
		addCrsvData();
		insertPtrnpf(ptrnpfList);
		updateCrsvpf(crsvpfItem);
	}

private void updateDatabaseForApproved()
	{
		for(Pymtpf newPymtpf : pytmpfList) {
			newPymtpf.setPaydate(sv.payDate.toInt());
			newPymtpf.setProcind("AQ");
			newPymtpf.setAppruser(longUserID);
			newPymtpf.setApprdte(datcon1.intDate.toInt());
			Cheqpf cheqpfRec = cheqpfDAO.getcheqPf(wsspcomn.company.toString(), newPymtpf.getPaynum_cheq());
			if(cheqpfRec != null) {
				cheqpfList.add(cheqpfRec);
			}
			updatePytmpfList.add(newPymtpf);
		}
		
		boolean isUpdatePymtPF = pymtpfDAO.updateApprovedPymtpf(updatePytmpfList);
		if (!isUpdatePymtPF) {
			LOGGER.error("Update pymtPF record failed.");
		}
		List<Cheqpf> updateCheqpfList = new ArrayList<>(); 
		for(Cheqpf newCheqpf: cheqpfList) {
			newCheqpf.setPaydate(sv.payDate.toInt());
			newCheqpf.setProcind("AQ");
			newCheqpf.setAppruser(varcom.vrcmUser.toInt());
			newCheqpf.setApprdte(datcon1.intDate.toInt());
			updateCheqpfList.add(newCheqpf);
		}
		boolean isUpdateCheqPF = cheqpfDAO.updateCheqpfRecords(updateCheqpfList);
		if (!isUpdateCheqPF) {
			LOGGER.error("Update CheqPF record failed.");
		}
		
	}

private void updateDatabaseForAuthorized()
{
	for(Pymtpf newPymtpf : pytmpfList) {
		newPymtpf.setPaydate(sv.payDate.toInt());
		newPymtpf.setProcind("AU");
		newPymtpf.setAuthuser(longUserID);
		newPymtpf.setAuthdate(datcon1.intDate.toInt());
		Cheqpf cheqpfRec = cheqpfDAO.getcheqPf(wsspcomn.company.toString(), newPymtpf.getPaynum_cheq());
		if(cheqpfRec != null) {
			cheqpfList.add(cheqpfRec);
		}
		updatePytmpfList.add(newPymtpf);
	}
	
	boolean isUpdatePymtPF = pymtpfDAO.updateApprovedPymtpf(updatePytmpfList);
	if (!isUpdatePymtPF) {
		LOGGER.error("Update pymtPF record failed.");
	}
	List<Cheqpf> updateCheqpfList = new ArrayList<>(); 
	for(Cheqpf newCheqpf: cheqpfList) {
		newCheqpf.setPaydate(sv.payDate.toInt());
		newCheqpf.setProcind("AU");
		newCheqpf.setAuthuser(varcom.vrcmUser.toInt());
		newCheqpf.setAuthdate(datcon1.intDate.toInt());
		updateCheqpfList.add(newCheqpf);
	}
	boolean isUpdateCheqPF = cheqpfDAO.updateCheqpfRecords(updateCheqpfList);
	if (!isUpdateCheqPF) {
		LOGGER.error("Update CheqPF record failed.");
	}
}

protected void addCrsvData(){
	if (isEQ(wsspcomn.sbmaction, "C")){ // need to change
		crsvpfItem.setPaydate(sv.payDate.toInt());
		crsvpfItem.setWreqamt(sv.totalPayAmt.getbigdata());
		crsvpfItem.setBalo(sv.totalPayAmt.getbigdata());
		crsvpfItem.setWpaid(BigDecimal.ZERO);
	}
	if (isEQ(wsspcomn.sbmaction, "D")){
		crsvpfItem.setPaydate(sv.payDate.toInt());
		crsvpfItem.setWreqamt(BigDecimal.ZERO);
		crsvpfItem.setBalo(BigDecimal.ZERO);
		crsvpfItem.setWpaid(sv.totalPayAmt.getbigdata());
	}
}

protected void updateCrsvpf(Crsvpf crsvpfItem) {
	crsvpfItem.setChdrnum(sv.contractNum.trim());
	crsvpfDAO.updateCrsvRecord(crsvpfItem);
}

protected void addPtrnData(){
	
	Chdrpf chdrpfObj = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(),sv.contractNum.trim());
	
	ptrnpf.setChdrpfx("CH");
	ptrnpf.setChdrcoy(wsspcomn.company.toString());
	ptrnpf.setChdrnum(sv.contractNum.trim());
	ptrnpf.setRecode(SPACES.stringValue());
	ptrnpf.setTranno(chdrpfObj.getTranno() + 1);
	ptrnpf.setPtrneff(sv.payDate.toInt());
	ptrnpf.setTrdt(varcom.vrcmDate.toInt());
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	ptrnpf.setTermid(varcom.vrcmTermid.toString());
	ptrnpf.setUserT(varcom.vrcmUser.toInt());
	ptrnpf.setBatcpfx("BA");
	ptrnpf.setBatccoy(wsspcomn.company.toString());
	wsaaBatckey1.set(wsspcomn.batchkey);
	ptrnpf.setBatcbrn(wsaaBatckey1.batcBatcbrn.toString());
	ptrnpf.setBatcactyr(wsaaBatckey1.batcBatcactyr.toInt());
	ptrnpf.setBatcactmn(wsaaBatckey1.batcBatcactmn.toInt());
	ptrnpf.setBatctrcde(wsaaBatckey1.batcBatctrcde.toString());
	ptrnpf.setPrtflg(SPACES.stringValue());
	ptrnpf.setValidflag("1");
	ptrnpf.setDatesub(datcon1.intDate.toInt());
	ptrnpf.setBatcbatch(wsaaBatckey1.batcBatcbatch.toString());
	ptrnpf.setCrtuser(SPACES.stringValue());
	ptrnpfList.add(ptrnpf);
	
	updateTrannoChdrpf(chdrpfObj);
}

private void updateTrannoChdrpf(Chdrpf chdrpfObj) {
	
	Chdrpf chdrpf =  new Chdrpf();
	chdrpf.setChdrnum(chdrpfObj.getChdrnum());
	chdrpf.setChdrcoy(chdrpfObj.getChdrcoy());
	chdrpf.setTranno(chdrpfObj.getTranno() + 1);
	List<Chdrpf> chdrpfList = new ArrayList<>();
	chdrpfList.add(chdrpf);
	boolean isUpdateContractHeader = chdrpfDAO.updateChdrTrannoByChdrnum(chdrpfList);
	if (!isUpdateContractHeader) {
		LOGGER.error("Update Contract Header failed.");
		fatalError600();
	}else {
		chdrpfList.clear();
	}
}

protected void insertPtrnpf(List<Ptrnpf> ptrnpfList) {
	if(ptrnpfList != null && !ptrnpfList.isEmpty()){
		boolean result = ptrnpfDAO.insertPtrnPF(ptrnpfList);
		if (!result) {
			fatalError600();
		} else ptrnpfList.clear();
	}
}
@Override
protected void whereNext4000()
	{
		wsspcomn.chdrValidflag.set("F");
		wsspcomn.programPtr.add(1);
	}
}

