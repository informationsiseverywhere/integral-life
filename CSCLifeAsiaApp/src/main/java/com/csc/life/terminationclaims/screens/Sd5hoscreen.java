package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sd5hoscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {22, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5hoScreenVars sv = (Sd5hoScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5hoscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5hoScreenVars screenVars = (Sd5hoScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.item.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.remamon01.setClassString("");
		screenVars.remamon02.setClassString("");
		screenVars.remamon03.setClassString("");
		screenVars.remamon04.setClassString("");
		screenVars.remamon05.setClassString("");
		screenVars.remamon06.setClassString("");
		screenVars.remamon07.setClassString("");
		screenVars.remamon08.setClassString("");
		screenVars.remamon09.setClassString("");
		screenVars.remamon10.setClassString("");
		screenVars.remamon11.setClassString("");
		screenVars.remamon12.setClassString("");
		screenVars.remamon13.setClassString("");
		screenVars.remamon14.setClassString("");
		screenVars.remamon15.setClassString("");
		screenVars.remamon16.setClassString("");
		screenVars.remamon17.setClassString("");
		screenVars.remamon18.setClassString("");
		screenVars.remamon19.setClassString("");
		screenVars.remamon20.setClassString("");
		screenVars.rate01.setClassString("");
		screenVars.rate02.setClassString("");
		screenVars.rate03.setClassString("");
		screenVars.rate04.setClassString("");
		screenVars.rate05.setClassString("");
		screenVars.rate06.setClassString("");
		screenVars.rate07.setClassString("");
		screenVars.rate08.setClassString("");
		screenVars.rate09.setClassString("");
		screenVars.rate10.setClassString("");
		screenVars.rate11.setClassString("");
		screenVars.rate12.setClassString("");
		screenVars.rate13.setClassString("");
		screenVars.rate14.setClassString("");
		screenVars.rate15.setClassString("");
		screenVars.rate16.setClassString("");
		screenVars.rate17.setClassString("");
		screenVars.rate18.setClassString("");
		screenVars.rate19.setClassString("");
		screenVars.rate20.setClassString("");
	}

/**
 * Clear all the variables in Sd5hoscreen
 */
	public static void clear(VarModel pv) {
		Sd5hoScreenVars screenVars = (Sd5hoScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.remamon01.clear();
		screenVars.remamon02.clear();
		screenVars.remamon03.clear();
		screenVars.remamon04.clear();
		screenVars.remamon05.clear();
		screenVars.remamon06.clear();
		screenVars.remamon07.clear();
		screenVars.remamon08.clear();
		screenVars.remamon09.clear();
		screenVars.remamon10.clear();
		screenVars.remamon11.clear();
		screenVars.remamon12.clear();
		screenVars.remamon13.clear();
		screenVars.remamon14.clear();
		screenVars.remamon15.clear();
		screenVars.remamon16.clear();
		screenVars.remamon17.clear();
		screenVars.remamon18.clear();
		screenVars.remamon19.clear();
		screenVars.remamon20.clear();
		screenVars.rate01.clear();
		screenVars.rate02.clear();
		screenVars.rate03.clear();
		screenVars.rate04.clear();
		screenVars.rate05.clear();
		screenVars.rate06.clear();
		screenVars.rate07.clear();
		screenVars.rate08.clear();
		screenVars.rate09.clear();
		screenVars.rate10.clear();
		screenVars.rate11.clear();
		screenVars.rate12.clear();
		screenVars.rate13.clear();
		screenVars.rate14.clear();
		screenVars.rate15.clear();
		screenVars.rate16.clear();
		screenVars.rate17.clear();
		screenVars.rate18.clear();
		screenVars.rate19.clear();
		screenVars.rate20.clear();
	}
}
