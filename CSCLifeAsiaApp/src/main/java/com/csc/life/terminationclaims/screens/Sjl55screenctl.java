package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl55screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 7, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sjl55screensfl";
		lrec.subfileClass = Sjl55screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 5;
		lrec.pageSubfile = 4;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 12, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl55ScreenVars sv = (Sjl55ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl55screenctlWritten, sv.Sjl55screensflWritten, av, sv.sjl55screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl55ScreenVars screenVars = (Sjl55ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.payNum.setClassString("");
		screenVars.claimNum.setClassString("");
		screenVars.contractNum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.payDateDisp.setClassString("");
		screenVars.payMethod.setClassString("");
		screenVars.totalPayAmt.setClassString("");
		screenVars.clientnum.setClassString("");
		screenVars.clntName.setClassString("");
		screenVars.createdUser.setClassString("");
		screenVars.modifiedUser.setClassString("");
		screenVars.approvedUser.setClassString("");
		screenVars.authorizedUser.setClassString("");
		screenVars.createdDateDisp.setClassString("");
		screenVars.modifiedDateDisp.setClassString("");
		screenVars.approvedDateDisp.setClassString("");
		screenVars.authorizedDateDisp.setClassString("");
		screenVars.payStatus.setClassString("");
		
		
	}

/**
 * Clear all the variables in Sjl55screenctl
 */
	public static void clear(VarModel pv) {
		Sjl55ScreenVars screenVars = (Sjl55ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.payNum.clear();
		screenVars.claimNum.clear();
		screenVars.contractNum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.payDateDisp.clear();
		screenVars.payMethod.clear();
		screenVars.totalPayAmt.clear();
		screenVars.clientnum.clear();
		screenVars.clntName.clear();
		screenVars.createdUser.clear();
		screenVars.modifiedUser.clear();
		screenVars.approvedUser.clear();
		screenVars.authorizedUser.clear();
		screenVars.createdDateDisp.clear();
		screenVars.modifiedDateDisp.clear();
		screenVars.approvedDateDisp.clear();
		screenVars.authorizedDateDisp.clear();
		screenVars.payStatus.clear();
		
	}
}
