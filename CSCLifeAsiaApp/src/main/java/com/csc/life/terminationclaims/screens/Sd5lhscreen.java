package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sd5lhscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 23, 18, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5lhScreenVars sv = (Sd5lhScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5lhscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5lhScreenVars screenVars = (Sd5lhScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.register.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.currfromDisp.setClassString("");
		screenVars.currfrom.setClassString("");
		screenVars.ptdate.setClassString("");
		screenVars.btdate.setClassString("");
		screenVars.payfreq.setClassString("");		
		screenVars.compno.setClassString("");
		screenVars.compcode.setClassString("");
		screenVars.compdesc.setClassString("");
		screenVars.comprskcode.setClassString("");
		screenVars.comprskdesc.setClassString("");
		screenVars.comppremcode.setClassString("");
		screenVars.comppremdesc.setClassString("");
		screenVars.compsumss.setClassString("");
		screenVars.compbilprm.setClassString("");
		screenVars.refprm.setClassString("");
		screenVars.refadjust.setClassString("");
		screenVars.refamt.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.payeeno.setClassString("");
		screenVars.payeenme.setClassString("");
		//screenVars.bankaccno.setClassString("");
		screenVars.bnkcode.setClassString("");
		screenVars.bnkcdedsc.setClassString("");
		screenVars.crdtcrdno.setClassString("");
		screenVars.drctdbtno.setClassString("");
		screenVars.crdtcrdflg.setClassString("");
		screenVars.drctdbtflg.setClassString("");
		screenVars.descn.setClassString("");
	}

/**
 * Clear all the variables in Sd5lhscreen
 */
	public static void clear(VarModel pv) {
		Sd5lhScreenVars screenVars = (Sd5lhScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.cntcurr.clear();
		screenVars.register.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.currfromDisp.clear();
		screenVars.currfrom.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.payfreq.clear();			
		screenVars.compno.clear();
		screenVars.compcode.clear();
		screenVars.compdesc.clear();
		screenVars.comprskcode.clear();
		screenVars.comprskdesc.clear();
		screenVars.comppremcode.clear();
		screenVars.comppremdesc.clear();
		screenVars.compsumss.clear();		
		screenVars.compbilprm.clear();		
		screenVars.refprm.clear();		
		screenVars.refadjust.clear();		
		screenVars.refamt.clear();		
		screenVars.mop.clear();		
		screenVars.payeeno.clear();	
		screenVars.payeenme.clear();	
	//	screenVars.bankaccno.clear();		
		screenVars.bnkcode.clear();		
		screenVars.bnkcdedsc.clear();		
		screenVars.crdtcrdno.clear();
		screenVars.drctdbtno.clear();
		screenVars.crdtcrdflg.clear();
		screenVars.drctdbtflg.clear();
		screenVars.descn.clear();

	}
}
