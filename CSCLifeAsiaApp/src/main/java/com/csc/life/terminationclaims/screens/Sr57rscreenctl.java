package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class Sr57rscreenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr57rscreensfl";
		lrec.subfileClass = Sr57rscreensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 6;
		lrec.pageSubfile = 5;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 11, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr57rScreenVars sv = (Sr57rScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr57rscreenctlWritten, sv.Sr57rscreensflWritten, av, sv.sr57rscreensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr57rScreenVars screenVars = (Sr57rScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		
		screenVars.causeofdth.setClassString("");
		screenVars.causeofdthdsc.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.dtofdeath.setClassString("");
		screenVars.effdate.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.otheradjst.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.totclaim.setClassString("");
		
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.asterisk.setClassString("");
		screenVars.astrsk.setClassString("");
	}

/**
 * Clear all the variables in Sr57rscreenctl
 */
	public static void clear(VarModel pv) {
		Sr57rScreenVars screenVars = (Sr57rScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		
		screenVars.causeofdth.clear();
		screenVars.causeofdthdsc.clear();
		screenVars.currcd.clear();
		screenVars.dtofdeath.clear();
		screenVars.effdate.clear();
		screenVars.longdesc.clear();
		screenVars.otheradjst.clear();
		screenVars.reasoncd.clear();
		screenVars.totclaim.clear();
		
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.asterisk.clear();
		screenVars.astrsk.clear();
	}
}
