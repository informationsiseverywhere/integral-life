package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HpclpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:33
 * Class transformed from HPCLPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HpclpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 538;
	public FixedLengthStringData hpclrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hpclpfRecord = hpclrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hpclrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hpclrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hpclrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hpclrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hpclrec);
	public PackedDecimalData rgpynum = DD.rgpynum.copy().isAPartOf(hpclrec);
	public PackedDecimalData itmfrm = DD.itmfrm.copy().isAPartOf(hpclrec);
	public PackedDecimalData itmto = DD.itmto.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits01 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits02 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits03 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits04 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits05 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits06 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits07 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits08 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits09 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits10 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits11 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits12 = DD.benefits.copy().isAPartOf(hpclrec);
	public FixedLengthStringData benefits13 = DD.benefits.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt01 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt02 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt03 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt04 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt05 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt06 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt07 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt08 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt09 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt10 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt11 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt12 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData benfamt13 = DD.benfamt.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim01 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim02 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim03 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim04 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim05 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim06 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim07 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim08 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim09 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim10 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim11 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim12 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData daclaim13 = DD.daclaim.copy().isAPartOf(hpclrec);
	public PackedDecimalData claimtot = DD.claimtot.copy().isAPartOf(hpclrec);
	public PackedDecimalData premunit = DD.premunit.copy().isAPartOf(hpclrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hpclrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hpclrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hpclrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HpclpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HpclpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HpclpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HpclpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HpclpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HpclpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HpclpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HPCLPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"RGPYNUM, " +
							"ITMFRM, " +
							"ITMTO, " +
							"BENEFITS01, " +
							"BENEFITS02, " +
							"BENEFITS03, " +
							"BENEFITS04, " +
							"BENEFITS05, " +
							"BENEFITS06, " +
							"BENEFITS07, " +
							"BENEFITS08, " +
							"BENEFITS09, " +
							"BENEFITS10, " +
							"BENEFITS11, " +
							"BENEFITS12, " +
							"BENEFITS13, " +
							"BENFAMT01, " +
							"BENFAMT02, " +
							"BENFAMT03, " +
							"BENFAMT04, " +
							"BENFAMT05, " +
							"BENFAMT06, " +
							"BENFAMT07, " +
							"BENFAMT08, " +
							"BENFAMT09, " +
							"BENFAMT10, " +
							"BENFAMT11, " +
							"BENFAMT12, " +
							"BENFAMT13, " +
							"DACLAIM01, " +
							"DACLAIM02, " +
							"DACLAIM03, " +
							"DACLAIM04, " +
							"DACLAIM05, " +
							"DACLAIM06, " +
							"DACLAIM07, " +
							"DACLAIM08, " +
							"DACLAIM09, " +
							"DACLAIM10, " +
							"DACLAIM11, " +
							"DACLAIM12, " +
							"DACLAIM13, " +
							"CLAIMTOT, " +
							"PREMUNIT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     rgpynum,
                                     itmfrm,
                                     itmto,
                                     benefits01,
                                     benefits02,
                                     benefits03,
                                     benefits04,
                                     benefits05,
                                     benefits06,
                                     benefits07,
                                     benefits08,
                                     benefits09,
                                     benefits10,
                                     benefits11,
                                     benefits12,
                                     benefits13,
                                     benfamt01,
                                     benfamt02,
                                     benfamt03,
                                     benfamt04,
                                     benfamt05,
                                     benfamt06,
                                     benfamt07,
                                     benfamt08,
                                     benfamt09,
                                     benfamt10,
                                     benfamt11,
                                     benfamt12,
                                     benfamt13,
                                     daclaim01,
                                     daclaim02,
                                     daclaim03,
                                     daclaim04,
                                     daclaim05,
                                     daclaim06,
                                     daclaim07,
                                     daclaim08,
                                     daclaim09,
                                     daclaim10,
                                     daclaim11,
                                     daclaim12,
                                     daclaim13,
                                     claimtot,
                                     premunit,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		rgpynum.clear();
  		itmfrm.clear();
  		itmto.clear();
  		benefits01.clear();
  		benefits02.clear();
  		benefits03.clear();
  		benefits04.clear();
  		benefits05.clear();
  		benefits06.clear();
  		benefits07.clear();
  		benefits08.clear();
  		benefits09.clear();
  		benefits10.clear();
  		benefits11.clear();
  		benefits12.clear();
  		benefits13.clear();
  		benfamt01.clear();
  		benfamt02.clear();
  		benfamt03.clear();
  		benfamt04.clear();
  		benfamt05.clear();
  		benfamt06.clear();
  		benfamt07.clear();
  		benfamt08.clear();
  		benfamt09.clear();
  		benfamt10.clear();
  		benfamt11.clear();
  		benfamt12.clear();
  		benfamt13.clear();
  		daclaim01.clear();
  		daclaim02.clear();
  		daclaim03.clear();
  		daclaim04.clear();
  		daclaim05.clear();
  		daclaim06.clear();
  		daclaim07.clear();
  		daclaim08.clear();
  		daclaim09.clear();
  		daclaim10.clear();
  		daclaim11.clear();
  		daclaim12.clear();
  		daclaim13.clear();
  		claimtot.clear();
  		premunit.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHpclrec() {
  		return hpclrec;
	}

	public FixedLengthStringData getHpclpfRecord() {
  		return hpclpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHpclrec(what);
	}

	public void setHpclrec(Object what) {
  		this.hpclrec.set(what);
	}

	public void setHpclpfRecord(Object what) {
  		this.hpclpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hpclrec.getLength());
		result.set(hpclrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}