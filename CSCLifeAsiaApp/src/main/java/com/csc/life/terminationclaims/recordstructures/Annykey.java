package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:16
 * Description:
 * Copybook name: ANNYKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Annykey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData annyFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData annyKey = new FixedLengthStringData(64).isAPartOf(annyFileKey, 0, REDEFINE);
  	public FixedLengthStringData annyChdrcoy = new FixedLengthStringData(1).isAPartOf(annyKey, 0);
  	public FixedLengthStringData annyChdrnum = new FixedLengthStringData(8).isAPartOf(annyKey, 1);
  	public FixedLengthStringData annyLife = new FixedLengthStringData(2).isAPartOf(annyKey, 9);
  	public FixedLengthStringData annyCoverage = new FixedLengthStringData(2).isAPartOf(annyKey, 11);
  	public FixedLengthStringData annyRider = new FixedLengthStringData(2).isAPartOf(annyKey, 13);
  	public PackedDecimalData annyPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(annyKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(annyKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(annyFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		annyFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}