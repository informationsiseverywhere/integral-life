package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.terminationclaims.screens.Sjl56ScreenVars;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl56 extends ScreenProgCS{

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL56");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Sjl56ScreenVars sv = ScreenProgram.getScreenVars( Sjl56ScreenVars.class);
	private Wsspsmart wsspsmart = new Wsspsmart();
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Tr386rec tr386rec = new Tr386rec();
	private String e315 = "E315";
	
	public Pjl56() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl56", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			throw e;
		}
	}
	
	@Override
	protected void initialise1000()
	{
		if(!isEQ(wsspcomn.chdrValidflag,"Y")) {
			initialise1001();
		}
	}
	
	protected void initialise1001() {
		sv.dataArea.clear();
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		Itempf itempf = itempfDAO.findItemByItem(wsspcomn.company.toString(), "TR386",wsaaTr386Key.toString());
		if(itempf != null) {
		tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
			if (isEQ(wsspcomn.sbmaction, "A")) {
				sv.scrndesc.set(tr386rec.progdesc[1].toString());
			} else if (isEQ(wsspcomn.sbmaction, "B")) {
				sv.scrndesc.set(tr386rec.progdesc[2].toString());
			} else if (isEQ(wsspcomn.sbmaction, "C")) {
				sv.scrndesc.set(tr386rec.progdesc[3].toString());
			} else if (isEQ(wsspcomn.sbmaction, "D")) {
				sv.scrndesc.set(tr386rec.progdesc[4].toString());
			} else if (isEQ(wsspcomn.sbmaction, "E")) {
				sv.scrndesc.set(tr386rec.progdesc[5].toString());
			}
		}
	}

	@Override
	protected void preScreenEdit() {
		// No actions
	}
	
	@Override
	protected void screenEdit2000() {
		if(isEQ(sv.confirmation,SPACES)){
			sv.confirmationErr.set(e315);
			sv.confirmationOut[Varcom.ri.toInt()].set("Y");
		}
		if (isEQ(sv.confirmation,"N")) {
			if (isEQ(wsspcomn.sbmaction, "A") || (isEQ(wsspcomn.sbmaction, "B"))) {
				return;
			}
			else {
				scrnparams.statuz.set("KILL");
				rollback();
			}
		}
	}
	
	@Override
	protected void update3000() {
		//No actions
	}
	
	@Override
	protected void whereNext4000() {

		if (isEQ(wsspcomn.sbmaction, "A") || (isEQ(wsspcomn.sbmaction, "B"))) {
			wsspcomn.flag.set("Y");
			if (isEQ(sv.confirmation,"N")) {
				wsspcomn.programPtr.add(-1);
			}
			else if(isEQ(sv.confirmation,"Y")){
				wsspcomn.programPtr.add(1);
			}
		}
		else {
			if (isEQ(scrnparams.statuz, "KILL")) {
				wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
				return ;
			}
			if(!isEQ(sv.confirmation,SPACES))
				wsspcomn.programPtr.add(1);
		}
		wsspcomn.chdrValidflag.set("Y");
	}
}
