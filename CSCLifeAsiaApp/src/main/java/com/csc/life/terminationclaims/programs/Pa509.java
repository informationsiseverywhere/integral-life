/**
 * Pa509 —— Investigation Results from Sa508 hyperlink
 */
package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart.procedures.Genssw;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.dao.InvspfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.life.terminationclaims.screens.Sa509ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;

public class Pa509 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final String CLMPREFIX = "CLMNTF";
	public static String notificationNum;
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA509");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	
	private Chdrpf chdrpf = new Chdrpf();
	
	private CltsTableDAM cltsIO = new CltsTableDAM();

	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sa509ScreenVars sv = getPScreenVars() ;
	private Clntpf clntpf;
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private Fluppf fluppf = null ;
	/* ERRORS */
	protected static final String h093 = "H093";
	private Batckey wsaaBatchkey = new Batckey();
	private Subprogrec subprogrec = new Subprogrec();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	//CML001
	private InvspfDAO invspfDAO= getApplicationContext().getBean("invspfDAO",InvspfDAO.class);
	protected static final String chdrenqrec = "CHDRENQREC";
	protected ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator planLevel = new Validator(wsaaPlanproc, "Y");
	protected static final String e186 = "E186";
	protected static final String nextScreen = "PA630";
	private String wsaaChdrnum=null;
	private int count=0;
	private Invspf invspf = null;
	List<Invspf> invspfList = null;
	private Optswchrec optswchrec = new Optswchrec();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();

	
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2090,
		updateErrorIndicators2670, 
		exit3090
	}

	public Pa509() {
		super();
		screenVars = sv;
		new ScreenModel("Sa509", AppVars.getInstance(), sv);
	}

	protected Sa509ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sa509ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
	
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			e.printStackTrace();
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}
public void processBo(Object... parmArray) {
		
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		}catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			e.printStackTrace();
		}catch (Exception ex){
			ex.printStackTrace();
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}


protected void initialise1010()
	{
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*      GO TO 1090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		sv.select.set(ZERO);
		/*     MOVE "Y"             TO Sa509-SELECT-OUT(PR).       <GBF>*/
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		sv.relation.set(wsspcomn.wsaarelationship.toString());
		getLifeAssSection1020();
		//display the list area
		getInvestigationResultsList1030();
		callOptSwitch();
		
	}
	

	private void getLifeAssSection1020() {
		try {
			clntpf = clntpfDAO.searchClntRecord("CN",
					wsspcomn.fsuco.toString(), wsspcomn.chdrCownnum.toString());
			if (clntpf != null) {
				sv.lifcnum.set(wsspcomn.chdrCownnum);
				// auto genarate Notifinum number
				if(isNE(wsspcomn.wsaanotificationNum,SPACES))
					sv.notifinum.set(wsspcomn.wsaanotificationNum.toString().trim());
				else
					sv.notifinum.set(SPACES);	
				String Surname = clntpf.getSurname() != null ? clntpf
						.getSurname().trim() : " ";
				String GivenName = clntpf.getGivname() != null ? clntpf
						.getGivname().trim() : " ";
				sv.lifename.set(Surname + "," + GivenName);
				sv.relation.set(wsspcomn.wsaarelationship);
				sv.claimant.set(wsspcomn.wsaaclaimant);
				clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), sv.claimant.toString());
				if(clntpf ==null){
					syserrrec.params.set(wsspcomn.fsuco.toString().concat(wsspcomn.chdrCownnum.toString()));
					fatalError600();
				}
				else{
					String Surname1 = clntpf.getSurname() != null?clntpf.getSurname().trim(): " ";
		            String GivenName1 = clntpf.getGivname() != null?clntpf.getGivname().trim(): " ";
		            sv.clamnme.set(Surname1+","+GivenName1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void getInvestigationResultsList1030(){
		invspfList = invspfDAO.getInvspfByNotifinum(wsspcomn.company.toString(), sv.notifinum.toString().trim().replace(CLMPREFIX, "")); //IBPLIFE-2338
		try {
			if(invspfList!=null && !invspfList.isEmpty()){
				wsaaCount.set(invspfList.size());
				for(Invspf invspf : invspfList){
					sv.select.set(ZERO);
					sv.seqnoen.set(wsaaCount);
					sv.seqnogp.set(wsaaCount);
					sv.accdesc.set(invspf.getInvestigationResult().trim());
					Date parse=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(invspf.getDatime().substring(0,16));
			        String dateString = new SimpleDateFormat("yyyyMMdd").format(parse);
					sv.effdates.set(dateString);
					dateString = parse.toString().substring(11, 16);
					sv.acctime.set(dateString);
					sv.userid.set(invspf.getUsrprf());
					addToSubfile1500();
					wsaaCount.minusminus();
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	protected void addToSubfile1500()
	{
		/*ADD-LINE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("Sa509", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
		syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
	private void callOptSwitch(){
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if(isEQ(wsspcomn.flag,"I")){
			sv.indxflgOut[varcom.pr.toInt()].set("Y");
		}
	}
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* IF   WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                  */
		/*      MOVE O-K               TO WSSP-EDTERROR                 */
		/*      GO TO 2090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		
		try{
			screenIo2010();
			validateSubfile2600();
		}catch(Exception e){
			e.printStackTrace();
		}
	
 	}


	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
		
		
	/*    CALL 'SIO' USING SCRN-SCREEN-PARAMS                      */
	/*                         S-DATA-AREA                         */
	/*                         S-SUBFILE-AREA.                     */
	
	/* Screen errors are now handled in the calling program.           */
	/*    PERFORM 200-SCREEN-ERRORS.                                   */
	wsspcomn.edterror.set(varcom.oK);
	/* If termination of processing then go to exit. P6350 will*/
	/*  release the soft lock.*/
	if (isEQ(scrnparams.statuz, "KILL")) {
		wsspcomn.edterror.set(varcom.oK);
		exit2090();
	}
	 if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
			exit2090();
	}
		
}

protected void exit2090(){
	
	if (isNE(sv.errorSubfile, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}
protected void validateSubfile2600()
{
	try{
		validation2610();
		updateErrorIndicators2670();
		readNextModifiedRecord2680();
	}catch(Exception e){
		e.printStackTrace();
	}
}


protected void validation2610()
{
	 	if (isEQ(scrnparams.statuz, varcom.kill)) {
	 		return;
	 	}
	 	scrnparams.function.set(varcom.srnch);
		screenIo9000();
		/*    if user has chosed one selection and to create,then   trigger  error message*/
		 if (isEQ(sv.indxflg, "Y") && isNE(sv.select,ZERO)) {
	    	 scrnparams.errorCode.set(errorsInner.RRSL);
			 sv.select.set(ZERO);
	    	 wsspcomn.edterror.set("Y");
	    	 return;
	     }
		 if(isEQ(sv.select,3) || isEQ(sv.select,4)){
			if(isNE(wsspcomn.userid, sv.userid)){
				scrnparams.errorCode.set(errorsInner.RRSM);
				sv.select.set(ZERO);
				 wsspcomn.edterror.set("Y");
				 return;
			}
		 }
		
}


protected void updateErrorIndicators2670()
{
	if (isNE(sv.errorSubfile, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	scrnparams.function.set(varcom.supd);
	processScreen("Sa509", sv);
	if (isNE(scrnparams.statuz, varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	if (isNE(sv.errorSubfile, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
}

protected void readNextModifiedRecord2680()
{
	scrnparams.function.set(varcom.srnch);
	processScreen("Sa509", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	/*EXIT*/
}


protected void update3000()
	{
	updateWssp3010();
	return ;
	}

protected void updateWssp3010()
{

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		if(isEQ(sv.indxflg,"Y") && isEQ(sv.select,ZERO)){
			sv.select.set(2);
		}
		 if(invspfList!=null && !invspfList.isEmpty()){
				wsaaIndex.set(sub(invspfList.size(),sv.seqnoen.toInt()));
				invspf = invspfList.get(wsaaIndex.toInt());
				invspfDAO.setCacheObject(invspf);
		}
		String chdrNumStr = wsspcomn.chdrNumList.getData();
		
		ArrayList<String> chdrnumList=new ArrayList(Arrays.asList(chdrNumStr.split(",")));
		count = chdrnumList.size()-1;
		wsaaChdrnum = chdrnumList.get(count).trim();
	
}


	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
{

	nextProgram4020();
	/*NEXT-PROGRAM
	EXIT*/
}

protected void bypassStart4010()
{
	if (isEQ(sv.select, SPACES)) {
		while ( !(isNE(sv.select, SPACES)
		|| isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile4100();
		}
		
	}
	/* All requests services,*/
	if (isEQ(scrnparams.statuz, varcom.endp)
	&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
//		goTo(GotoLabel.exit4090);
		return;
	}
	if (isEQ(scrnparams.statuz, varcom.endp)) {
		nextProgram4085();
	}
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
		saveProgramStack4200();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wssplife.unitType.set(SPACES);
	if (isEQ(sv.select, "2")) {
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set("B");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by Genswch, redisplay the    */
		/*   the screen with an error.                                     */
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
//			goTo(GotoLabel.exit4090);
			return;
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			loadProgramStack4300();
		}
//		goTo(GotoLabel.nextProgram4080);
		nextProgram4080();
	}
	if (isEQ(sv.select, "1")) {
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set("A");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by Genswch, redisplay the    */
		/*   the screen with an error.                                     */
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
//			goTo(GotoLabel.exit4090);
			return;
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
		}
//		goTo(GotoLabel.nextProgram4080);
		nextProgram4080();
	}
		if (isEQ(sv.select, "3")) {
			int removeInvspfRecord = invspfDAO.removeInvspfRecord(sv.seqnogp.getData().longValue());
		}
		if (isEQ(sv.select, "4")) {
			// unitEnquiry4800();
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			gensswrec.function.set("D");
			callProgram(Genssw.class, gensswrec.gensswRec);
			if (isNE(gensswrec.statuz, varcom.oK)
					&& isNE(gensswrec.statuz, varcom.mrnf)) {
				syserrrec.statuz.set(gensswrec.statuz);
				fatalError600();
			}
			/* If an entry on T1675 was not found by Genswch, redisplay the */
			/* the screen with an error. */
			if (isEQ(gensswrec.statuz, varcom.mrnf)) {
				// goTo(GotoLabel.exit4090);
				return;
			}
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1) {
				loadProgramStack4300();
			}
			// goTo(GotoLabel.nextProgram4080);
			nextProgram4080();
		}

	}

protected void nextProgram4080()
{
	sv.select.set(ZERO);
	scrnparams.function.set(varcom.supd);
	processScreen("SA509", sv);
	if (isNE(scrnparams.statuz, varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	/* If an entry on T1675 was not found by Genswch, redisplay the    */
	/*   the screen with an error.                                     */
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		scrnparams.errorCode.set(h093);
		wsspcomn.nextprog.set(scrnparams.scrname);
//		goTo(GotoLabel.exit4090);
		return;
	}
}

protected void loadProgramStack4300()
{
	/*PARA*/
	wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
	sub1.add(1);
	sub2.add(1);
	/*EXIT*/
}

protected void saveProgramStack4200()
{
	/*PARA*/
	sub1.add(1);
	sub2.add(1);
	/*EXIT*/
}

protected void readSubfile4100()
{
	/*READ*/
	scrnparams.function.set(varcom.srdn);
	processScreen("SA509", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	/*EXIT*/
}

protected void nextProgram4085()
{
	wsspcomn.nextprog.set(wsaaProg);
	wsspcomn.programPtr.add(1);
}


protected void nextProgram4020()
{
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
		wsspcomn.nextprog.set(wsaaProg);
	}
	optswchCall4500();
}

protected void optswchCall4500()
{
	optswchrec.optsCallingProg.set(wsaaProg);
	optswchrec.optsDteeff.set(ZERO);
	optswchrec.optsCompany.set(wsspcomn.company);
	optswchrec.optsItemCompany.set(wsspcomn.company);
	optswchrec.optsLanguage.set(wsspcomn.language);
	varcom.vrcmTranid.set(wsspcomn.tranid);
	optswchrec.optsUser.set(varcom.vrcmUser);
	optswchrec.optsSelCode.set(SPACES);
	optswchrec.optsSelOptno.set(sv.select);
	optswchrec.optsSelType.set("L");
	optswchrec.optsItemCompany.set(wsspcomn.company);
	optswchrec.optsFunction.set("STCK");
	sv.select.set(ZERO);
	callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	if ((isNE(optswchrec.optsStatuz,varcom.oK)
	&& isNE(optswchrec.optsStatuz,varcom.endp))) {
		optswchrec.optsItemCompany.set(wsspcomn.company);
		syserrrec.function.set("STCK");
		syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
		syserrrec.statuz.set(optswchrec.optsStatuz);
		syserrrec.iomod.set("OPTSWCH");
		fatalError600();
	}
	if (isEQ(optswchrec.optsStatuz, varcom.endp)) {
		sv.select.set(ZERO);
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		getInvestigationResultsList1030();
		wsspcomn.nextprog.set(scrnparams.scrname);
	}
	else {
		wsspcomn.programPtr.add(1);		
	}
}

protected void screenIo9000()
{
	/*BEGIN*/
	processScreen("SA509", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	/*EXIT*/
}

protected void saveProgram4100()
{
	/*SAVE*/
	wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
} 
protected void restoreProgram4100()
{
	/*PARA*/
	wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
	sub1.add(1);
	sub2.add(1);
	/*EXIT*/
}

protected void callGenssw4300()
{
	callSubroutine4310();
}

protected void callSubroutine4310()
{
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)
	&& isNE(gensswrec.statuz, varcom.mrnf)) {
		syserrrec.params.set(gensswrec.gensswRec);
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	/* If an entry on T1675 was not found by genswch redisplay the scre*/
	/* with an error and the options and extras indicator*/
	/* with its initial load value*/
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*     MOVE V045                TO SCRN-ERROR-CODE               */
		scrnparams.errorCode.set(h093);//ILB-459
		wsspcomn.nextprog.set(scrnparams.scrname);
		return ;
	}
	/*    load from gensw to wssp*/
	compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
	wsaaY.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		loadProgram4400();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
//	wsspcomn.programPtr.add(1);
}

protected void restoreProgram4200()
{
	/*RESTORE*/
	wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
}

protected void loadProgram4400()
{
	/*RESTORE1*/
	wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT1*/
}

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	
    private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
    
}

	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 
		 private FixedLengthStringData RRSM = new FixedLengthStringData(4).init("RRSM");
		 private FixedLengthStringData RRSL = new FixedLengthStringData(4).init("RRSL");
	}

}
