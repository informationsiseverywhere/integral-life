/*
 * File: P5237.java
 * Date: 30 August 2009 0:21:31
 * Author: Quipoz Limited
 * 
 * Class transformed from P5237.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.annuities.dataaccess.AnbkTableDAM;
import com.csc.life.annuities.dataaccess.RegtbrkTableDAM;
import com.csc.life.annuities.dataaccess.RegtvstTableDAM;
import com.csc.life.annuities.dataaccess.VstdTableDAM;
import com.csc.life.annuities.screens.S5237ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.AnnylnbTableDAM;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* VESTING ANNUITY PAYMENT SELECT SCREEN
* =====================================
*
* This program forms part of  the  9405  Annuities  Development.
*
* It  forms  part  of  the new business processing for Vesting
* Annuity Components and controls the Vesting Annuity Payment
* Details Screen, S5222.
*
* When  creating  the first Vesting Annuity Payment there will
* be no Regular Payment Temporary record so the screen will  not
* be  displayed.  A Regular Payment Temporary record (REGTVST) will
* be created with the appropriate defaults and the  next  screen
* will  be  displayed  with  these details.  The defaults are as
* follows:
*
*   -  Payment currency from the CHDR
*
*   -  Effective date to the Risk Commencement Date from the
*      CHDR.
*
*   -  Frequency = 00  if a lump sum payment is being made,
*      otherwise it comes from the ANNY.
*
*   -  The first payment date is calculated as follows:
*           Annuity payments In Advance(from ANNY)
*           The first payment date is the effective date of the
*           annuity.
*           Annuity payments In Arrears(from ANNY)
*           The first payment date is the effective date of the
*           annuity plus one frequency.
*
*    - If there is a Guaranteed Period the review date will be
*      set to the effective date plus this number of years.
*
* If Regular Payment Temporary Records (REGTVST) exist, the  screen
* will be displayed with the 'select' field available for input.
*
* If  in  proposal  enquiry,  ADD and DELETE options will not be
* valid and will not be displayed.
*
* If in proposal modify all options will be  available.    If  a
* payment  record  is  selected  the  details  for  the  Regular
* Payment Temporary record (REGTVST) will be displayed on the  next
* program in the sequence.
*
* If  ADD  is  selected  this does not necessarily need to be
* against the last REGTVST, but can be against any REGTVST. The
* Annuity Details Screen will then be displayed, allowing the
* addition of another REGTVST.
*
* If  any  of  the following items are modified on the proposal,
* all of the regular payment temporary records (REGTVSTs)  for  the
* component  should  be  selected  to force recalculation of the
* appropriate dates:
*
*      Risk commencement date        Effective date
*       (COVR/CHDR)                  First payment date
*                                    Review date
*                                    Anniversary date
*
*      Guaranteed period (ANNY)      Review date
*      In advance/In arrears (ANNY)  First payment date
*      Annuity payment frequency     First payment date.
*            (ANNY)
*
*  It will be able  to determine which of these two states it is
*  in by examining the Stack Action Flag.
*
*****************************************************************
* </pre>
*/
public class P5237 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5237");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData index1 = new PackedDecimalData(1, 0).setUnsigned();

	private FixedLengthStringData wsaaTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscd = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 4);

	private FixedLengthStringData wsaaTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTran = new FixedLengthStringData(5).isAPartOf(wsaaTranCurrency, 0);
	private FixedLengthStringData wsaaCurrency = new FixedLengthStringData(3).isAPartOf(wsaaTranCurrency, 5);
	private ZonedDecimalData wsaaNextRgpy = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaNoRegt = new FixedLengthStringData(1);
	private String wsaaFirstTime = "";
	private FixedLengthStringData wsaaForceBack = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSubsequent = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaPayrseqno = new ZonedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaBkfreqann = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaFreqann = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaBenfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaAdvance = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaArrears = new FixedLengthStringData(1);
	private PackedDecimalData wsaaGuarperd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaT5671Pgm = new FixedLengthStringData(4);
		/* ERRORS */
	private String f069 = "F069";
	private String h220 = "H220";
	private String e944 = "E944";
		/* TABLES */
	private String t5606 = "T5606";
	private String t5671 = "T5671";
	private String t5688 = "T5688";
	private String t6692 = "T6692";
	private String cltsrec = "CLTSREC";
	private String descrec = "DESCREC";
	private String payrrec = "PAYRREC";
	private String regtvstrec = "REGTVSTREC";
		/*Annuities Breakout logical*/
	private AnbkTableDAM anbkIO = new AnbkTableDAM();
		/*Annuity Details Life New Business*/
	private AnnylnbTableDAM annylnbIO = new AnnylnbTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
		/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
		/*Regular Payments Temporary Details*/
	private RegtbrkTableDAM regtbrkIO = new RegtbrkTableDAM();
		/*Regular Payment Temporary Record Vesting*/
	private RegtvstTableDAM regtvstIO = new RegtvstTableDAM();
	private T5606rec t5606rec = new T5606rec();
	private T5671rec t5671rec = new T5671rec();
		/*Vesting Details Logical*/
	private VstdTableDAM vstdIO = new VstdTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5237ScreenVars sv = ScreenProgram.getScreenVars( S5237ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		exit1190, 
		preExit, 
		updateErrorIndicators2120, 
		exit3090, 
		next4010, 
		continue4020, 
		exit4090, 
		exit4190, 
		exit4390
	}

	public P5237() {
		super();
		screenVars = sv;
		new ScreenModel("S5237", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaPayrseqno.set(ZERO);
		wsaaNoRegt.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaForceBack.set(SPACES);
		if (isEQ(wsaaSubsequent,SPACES)) {
			wssplife.updateFlag.set("Y");
			wsaaSubsequent.set("Y");
		}
		scrnparams.function.set(varcom.sclr);
		processScreen("S5237", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsspcomn.flag,"I")) {
			sv.hselectOut[varcom.nd.toInt()].set("Y");
			sv.hselect.set(SPACES);
		}
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		covrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isGT(covrmjaIO.getPayrseqno(),0)) {
			wsaaPayrseqno.set(covrmjaIO.getPayrseqno());
		}
		else {
			wsaaPayrseqno.set(1);
		}
		vstdIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, vstdIO);
		if (isNE(vstdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vstdIO.getParams());
			fatalError600();
		}
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmjaIO.getChdrnum());
		payrIO.setPayrseqno(wsaaPayrseqno);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getStatuz());
			fatalError600();
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5671);
		wsaaTranscd.set(wsaaBatckey.batcBatctrcde);
		wsaaCrtable.set(covrmjaIO.getCrtable());
		itemIO.setItemitem(wsaaTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		for (index1.set(1); !(isGT(index1,4)); index1.add(1)){
			wsaaT5671Pgm.set(subString(t5671rec.pgm[index1.toInt()], 2, 4));
			if (isEQ(wsaaT5671Pgm,subString(wsspcomn.lastprog, 2, 4))) {
				wsaaTran.set(t5671rec.edtitm[index1.toInt()]);
				index1.set(5);
			}
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5606);
		wsaaCurrency.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsaaTranCurrency);
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(t5606,itdmIO.getItemtabl())
		|| isNE(wsaaTranCurrency,itdmIO.getItemitem())) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5606rec.t5606Rec.set(itdmIO.getGenarea());
		if (isEQ(covrmjaIO.getPlanSuffix(),0)) {
			annylnbIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
			readAnny1200();
		}
		else {
			if (isGT(covrmjaIO.getPlanSuffix(),chdrmjaIO.getPolsum())) {
				annylnbIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
				readAnny1200();
			}
			else {
				readAnbk1300();
				if (isEQ(anbkIO.getStatuz(),varcom.mrnf)) {
					annylnbIO.setPlanSuffix(0);
					readAnny1200();
				}
			}
		}
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrmjaIO.getCownnum());
		sv.cownnum.set(chdrmjaIO.getCownnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		regtbrkIO.setParams(SPACES);
		regtbrkIO.setChdrcoy(covrmjaIO.getChdrcoy());
		regtbrkIO.setChdrnum(covrmjaIO.getChdrnum());
		regtbrkIO.setLife(covrmjaIO.getLife());
		regtbrkIO.setCoverage(covrmjaIO.getCoverage());
		regtbrkIO.setRider(covrmjaIO.getRider());
		regtbrkIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		regtbrkIO.setSeqnbr(ZERO);
		regtbrkIO.setRgpynum(ZERO);
		regtbrkIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		regtbrkIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtbrkIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		wsaaFirstTime = "Y";
		while ( !(isEQ(regtbrkIO.getStatuz(),varcom.endp))) {
			loadSubfile1100();
		}
		
	}

protected void loadSubfile1100()
	{
		try {
			screenIo1110();
			sadd1150();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo1110()
	{
		SmartFileCode.execute(appVars, regtbrkIO);
		if ((isNE(regtbrkIO.getStatuz(),varcom.oK)
		&& isNE(regtbrkIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regtbrkIO.getParams());
			fatalError600();
		}
		if ((isEQ(regtbrkIO.getStatuz(),varcom.endp))
		|| (isNE(regtbrkIO.getChdrcoy(),covrmjaIO.getChdrcoy()))
		|| (isNE(regtbrkIO.getChdrnum(),covrmjaIO.getChdrnum()))
		|| (isNE(regtbrkIO.getLife(),covrmjaIO.getLife()))
		|| (isNE(regtbrkIO.getCoverage(),covrmjaIO.getCoverage()))
		|| (isNE(regtbrkIO.getRider(),covrmjaIO.getRider()))
		|| (isNE(regtbrkIO.getPlanSuffix(),covrmjaIO.getPlanSuffix()))) {
			regtbrkIO.setStatuz(varcom.endp);
			if (isEQ(wsaaFirstTime,"Y")) {
				wsaaNoRegt.set("Y");
			}
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(wsaaFirstTime,"Y")) {
			wsaaFirstTime = "N";
		}
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t6692);
		descIO.setDescitem(regtbrkIO.getPayreason());
		sv.rgpytype.set(regtbrkIO.getPayreason());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rptldesc.set(descIO.getLongdesc());
		}
		else {
			sv.rptldesc.fill("?");
		}
		sv.pymt.set(regtbrkIO.getPymt());
		sv.rgpynum.set(regtbrkIO.getRgpynum());
		sv.select.set(SPACES);
		sv.hrrn.set(regtbrkIO.getRrn());
		sv.hflag.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		sv.select.set(SPACES);
	}

protected void sadd1150()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5237", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		regtbrkIO.setFunction(varcom.nextr);
	}

protected void readAnny1200()
	{
		startRead1210();
	}

protected void startRead1210()
	{
		annylnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
		annylnbIO.setChdrnum(covrmjaIO.getChdrnum());
		annylnbIO.setLife(covrmjaIO.getLife());
		annylnbIO.setCoverage(covrmjaIO.getCoverage());
		annylnbIO.setRider(covrmjaIO.getRider());
		annylnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annylnbIO);
		if (isNE(annylnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annylnbIO.getParams());
			fatalError600();
		}
		wsaaBkfreqann.set(annylnbIO.getFreqann());
		wsaaAdvance.set(annylnbIO.getAdvance());
		wsaaArrears.set(annylnbIO.getArrears());
		wsaaGuarperd.set(annylnbIO.getGuarperd());
		wsaaFreqann.set(annylnbIO.getFreqann());
	}

protected void readAnbk1300()
	{
		startRead1310();
	}

protected void startRead1310()
	{
		anbkIO.setChdrcoy(covrmjaIO.getChdrcoy());
		anbkIO.setChdrnum(covrmjaIO.getChdrnum());
		anbkIO.setLife(covrmjaIO.getLife());
		anbkIO.setCoverage(covrmjaIO.getCoverage());
		anbkIO.setRider(covrmjaIO.getRider());
		anbkIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		anbkIO.setTranno(wsspcomn.tranno);
		anbkIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, anbkIO);
		if ((isNE(anbkIO.getStatuz(),varcom.oK)
		&& isNE(anbkIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(anbkIO.getParams());
			fatalError600();
		}
		if (isNE(anbkIO.getStatuz(),varcom.mrnf)) {
			wsaaBkfreqann.set(anbkIO.getFreqann());
			wsaaAdvance.set(anbkIO.getAdvance());
			wsaaArrears.set(anbkIO.getArrears());
			wsaaGuarperd.set(anbkIO.getGuarperd());
			wsaaFreqann.set(anbkIO.getFreqann());
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsaaNoRegt,"Y")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("S5237", sv);
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if ((isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
		/*EXIT*/
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateSelect2110();
				}
				case updateErrorIndicators2120: {
					updateErrorIndicators2120();
					readNextModifiedRecord2180();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSelect2110()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2120);
		}
		if (isNE(sv.select,NUMERIC)) {
			sv.selectErr.set(e944);
		}
		else {
			optswchrec.optsFunction.set("CHCK");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsItemCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelCode.set(SPACES);
			optswchrec.optsSelOptno.set(sv.select);
			optswchrec.optsSelType.set("L");
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz,varcom.oK)) {
				sv.selectErr.set(optswchrec.optsStatuz);
			}
		}
	}

protected void updateErrorIndicators2120()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5237", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2180()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5237", sv);
		if ((isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsaaNoRegt,"Y")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case next4010: {
					next4010();
				}
				case continue4020: {
					continue4020();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if ((isEQ(wsaaNoRegt,"Y")
		&& isNE(vstdIO.getVstlump(),0))) {
			newLumpsumRegt4450();
			begnRegt4200();
			wsaaNoRegt.set(SPACES);
			regtvstIO.setFormat(regtvstrec);
			regtvstIO.setFunction(varcom.keeps);
			regtvstio4600();
			optswchrec.optsSelOptno.set(1);
			sv.select.set("0");
			optswchrec.optsSelType.set("L");
			optswchCall4500();
			goTo(GotoLabel.exit4090);
		}
		if ((isEQ(wsaaNoRegt,"Y")
		&& isEQ(vstdIO.getVstlump(),0))) {
			newNormalRegt4470();
			begnRegt4200();
			wsaaNoRegt.set(SPACES);
			regtvstIO.setFormat(regtvstrec);
			regtvstIO.setFunction(varcom.keeps);
			regtvstio4600();
			optswchrec.optsSelOptno.set(1);
			sv.select.set("0");
			optswchrec.optsSelType.set("L");
			optswchCall4500();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			if (isEQ(sv.select,"2")) {
				sv.select.set(SPACES);
				scrnparams.function.set(varcom.supd);
				screenio4800();
				subfileRec4100();
				if (isEQ(regtvstIO.getStatuz(),varcom.oK)) {
					scrnparams.function.set(varcom.sadd);
					screenio4800();
				}
				goTo(GotoLabel.next4010);
			}
			if (isEQ(sv.select,"1")) {
				sv.select.set(SPACES);
				sv.selectOut[varcom.pr.toInt()].set(SPACES);
				scrnparams.function.set(varcom.supd);
				subfileRec4100();
				screenio4800();
				goTo(GotoLabel.next4010);
			}
			if (isEQ(sv.select,"0")) {
				sv.select.set(SPACES);
				scrnparams.function.set(varcom.sadd);
				subfileRec4100();
				if (isEQ(wsaaForceBack,"Y")) {
					wsaaForceBack.set(SPACES);
					if (isNE(vstdIO.getVstlump(),0)) {
						newLumpsumRegt4450();
						begnRegt4200();
					}
					else {
						newNormalRegt4470();
						begnRegt4200();
					}
					regtvstIO.setFormat(regtvstrec);
					regtvstIO.setFunction(varcom.keeps);
					regtvstio4600();
					optswchrec.optsSelOptno.set(1);
					sv.select.set("0");
					optswchrec.optsSelType.set("L");
					optswchCall4500();
					goTo(GotoLabel.exit4090);
				}
				screenio4800();
				goTo(GotoLabel.next4010);
			}
			scrnparams.function.set(varcom.srdn);
		}
		else {
			scrnparams.function.set(varcom.sstrt);
		}
		goTo(GotoLabel.continue4020);
	}

protected void next4010()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void continue4020()
	{
		screenio4800();
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
			|| isEQ(wssplife.updateFlag,"Y")
			|| isEQ(wsspcomn.flag,"I")) {
				optswchrec.optsSelType.set(SPACES);
				optswchrec.optsSelOptno.set(ZERO);
				optswchCall4500();
				goTo(GotoLabel.exit4090);
			}
			else {
				wsspcomn.nextprog.set(scrnparams.scrname);
				scrnparams.errorCode.set(f069);
				goTo(GotoLabel.exit4090);
			}
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.next4010);
		}
		if (isEQ(sv.select,"1")) {
			optswchrec.optsSelOptno.set(1);
			optswchrec.optsSelType.set("L");
			regtvstIO.setChdrcoy(covrmjaIO.getChdrcoy());
			regtvstIO.setChdrnum(sv.chdrnum);
			regtvstIO.setRrn(sv.hrrn);
			//tom chi bug 847
			regtvstIO.setFormat(regtvstrec);
			//end
			regtvstIO.setFunction(varcom.readd);
			regtvstio4600();
			regtvstIO.setFunction(varcom.keeps);
			regtvstio4600();
			optswchCall4500();
		}
		if (isEQ(sv.select,"2")) {
			optswchrec.optsSelOptno.set(1);
			optswchrec.optsSelType.set("L");
			regtvstIO.setChdrcoy(covrmjaIO.getChdrcoy());
			regtvstIO.setChdrnum(sv.chdrnum);
			newNormalRegt4470();
			begnRegt4200();
			regtvstIO.setFormat(regtvstrec);
			regtvstIO.setFunction(varcom.keeps);
			regtvstio4600();
			optswchCall4500();
		}
		if (isEQ(sv.select,"9")) {
			regtvstIO.setChdrcoy(covrmjaIO.getChdrcoy());
			regtvstIO.setChdrnum(sv.chdrnum);
			regtvstIO.setRrn(sv.hrrn);
			regtvstIO.setFunction(varcom.readd);
			regtvstio4600();
			if (isEQ(regtvstIO.getRegpayfreq(),ZERO)) {
				wsspcomn.nextprog.set(scrnparams.scrname);
				scrnparams.errorCode.set(h220);
				goTo(GotoLabel.exit4090);
			}
			regtvstIO.setFunction(varcom.deltd);
			regtvstio4600();
			wssplife.updateFlag.set("N");
		}
	}

protected void subfileRec4100()
	{
		try {
			find4110();
		}
		catch (GOTOException e){
		}
	}

protected void find4110()
	{
		regtvstIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, regtvstIO);
		if ((isNE(regtvstIO.getStatuz(),varcom.oK)
		&& isNE(regtvstIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(regtvstIO.getParams());
			fatalError600();
		}
		if ((isEQ(regtvstIO.getStatuz(),varcom.mrnf))) {
			checkIfAnyRegts4900();
			goTo(GotoLabel.exit4190);
		}
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t6692);
		descIO.setDescitem(regtvstIO.getPayreason());
		sv.rgpytype.set(regtvstIO.getPayreason());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rptldesc.set(descIO.getLongdesc());
		}
		else {
			sv.rptldesc.fill("?");
		}
		sv.pymt.set(regtvstIO.getPymt());
		sv.rgpynum.set(regtvstIO.getRgpynum());
		sv.select.set(SPACES);
		sv.hrrn.set(regtvstIO.getRrn());
	}

protected void begnRegt4200()
	{
		find4210();
	}

protected void find4210()
	{
		wsaaNextRgpy.set(ZERO);
		regtbrkIO.setParams(SPACES);
		regtbrkIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		regtbrkIO.setChdrnum(chdrmjaIO.getChdrnum());
		regtbrkIO.setLife(covrmjaIO.getLife());
		regtbrkIO.setCoverage(covrmjaIO.getCoverage());
		regtbrkIO.setRider(covrmjaIO.getRider());
		regtbrkIO.setPlanSuffix(ZERO);
		regtbrkIO.setRgpynum(ZERO);
		regtbrkIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		regtbrkIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtbrkIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");

		while ( !(isEQ(regtbrkIO.getStatuz(),varcom.endp))) {
			findNextRegt4300();
		}
		
		setPrecision(regtvstIO.getRgpynum(), 1);
		regtvstIO.setRgpynum(add(wsaaNextRgpy,1), true);
	}

protected void findNextRegt4300()
	{
		try {
			find4310();
		}
		catch (GOTOException e){
		}
	}

protected void find4310()
	{
		SmartFileCode.execute(appVars, regtbrkIO);
		if ((isNE(regtbrkIO.getStatuz(),varcom.oK)
		&& isNE(regtbrkIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regtbrkIO.getParams());
			fatalError600();
		}
		if (isEQ(regtbrkIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit4390);
		}
		if (isEQ(chdrmjaIO.getChdrcoy(),regtbrkIO.getChdrcoy())
		&& isEQ(chdrmjaIO.getChdrnum(),regtbrkIO.getChdrnum())
		&& isEQ(covrmjaIO.getLife(),regtbrkIO.getLife())
		&& isEQ(covrmjaIO.getCoverage(),regtbrkIO.getCoverage())
		&& isEQ(covrmjaIO.getRider(),regtbrkIO.getRider())) {
			if (isGT(regtbrkIO.getRgpynum(),wsaaNextRgpy)) {
				wsaaNextRgpy.set(regtbrkIO.getRgpynum());
			}
		}
		else {
			regtbrkIO.setStatuz(varcom.endp);
		}
		regtbrkIO.setFunction(varcom.nextr);
	}

protected void newLumpsumRegt4450()
	{
		setup4460();
	}

protected void setup4460()
	{
		regtvstIO.setDataArea(SPACES);
		regtvstIO.setChdrcoy(covrmjaIO.getChdrcoy());
		regtvstIO.setChdrnum(covrmjaIO.getChdrnum());
		regtvstIO.setLife(covrmjaIO.getLife());
		regtvstIO.setCoverage(covrmjaIO.getCoverage());
		regtvstIO.setRider(covrmjaIO.getRider());
		regtvstIO.setSeqnbr(ZERO);
		regtvstIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		regtvstIO.setCrtdate(wsspcomn.currfrom);
		regtvstIO.setCurrcd(chdrmjaIO.getCntcurr());
		regtvstIO.setRegpayfreq("00");
		regtvstIO.setPaycoy(wsspcomn.fsuco);
		regtvstIO.setPrcnt(ZERO);
		regtvstIO.setTotamnt(ZERO);
		regtvstIO.setFirstPaydate(wsspcomn.currfrom);
		regtvstIO.setFinalPaydate(wsspcomn.currfrom);
		regtvstIO.setRevdte(varcom.vrcmMaxDate);
		regtvstIO.setAnvdate(varcom.vrcmMaxDate);
		regtvstIO.setPymt(vstdIO.getVstlump());
		regtvstIO.setSacscode(SPACES);
		regtvstIO.setSacstype(SPACES);
		regtvstIO.setGlact(SPACES);
		regtvstIO.setDebcred(SPACES);
		regtvstIO.setDestkey(SPACES);
		regtvstIO.setPayclt(SPACES);
		regtvstIO.setRgpymop(SPACES);
		regtvstIO.setPayreason(SPACES);
		regtvstIO.setClaimevd(SPACES);
		regtvstIO.setBankkey(SPACES);
		regtvstIO.setBankacckey(SPACES);
		regtvstIO.setRgpytype(SPACES);
	}

protected void newNormalRegt4470()
	{
		setup4480();
	}

protected void setup4480()
	{
		regtvstIO.setDataArea(SPACES);
		regtvstIO.setChdrcoy(covrmjaIO.getChdrcoy());
		regtvstIO.setChdrnum(covrmjaIO.getChdrnum());
		regtvstIO.setLife(covrmjaIO.getLife());
		regtvstIO.setCoverage(covrmjaIO.getCoverage());
		regtvstIO.setRider(covrmjaIO.getRider());
		regtvstIO.setSeqnbr(ZERO);
		regtvstIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		regtvstIO.setCurrcd(chdrmjaIO.getCntcurr());
		regtvstIO.setRegpayfreq(wsaaBkfreqann);
		regtvstIO.setPaycoy(wsspcomn.fsuco);
		regtvstIO.setPrcnt(ZERO);
		regtvstIO.setTotamnt(ZERO);
		regtvstIO.setFinalPaydate(wsspcomn.currfrom);
		regtvstIO.setRevdte(varcom.vrcmMaxDate);
		regtvstIO.setAnvdate(varcom.vrcmMaxDate);
		if (isNE(t5606rec.benfreq,annylnbIO.getFreqann())) {
			wsaaBenfreq.set(t5606rec.benfreq);
			setPrecision(regtvstIO.getPymt(), 3);
			regtvstIO.setPymt(div((mult(vstdIO.getVstpay(),wsaaBenfreq)),wsaaFreqann), true);
		}
		else {
			regtvstIO.setPymt(vstdIO.getVstpay());
		}
		regtvstIO.setSacscode(SPACES);
		regtvstIO.setSacstype(SPACES);
		regtvstIO.setGlact(SPACES);
		regtvstIO.setDebcred(SPACES);
		regtvstIO.setDestkey(SPACES);
		regtvstIO.setPayclt(SPACES);
		regtvstIO.setRgpymop(SPACES);
		regtvstIO.setPayreason(SPACES);
		regtvstIO.setClaimevd(SPACES);
		regtvstIO.setBankkey(SPACES);
		regtvstIO.setBankacckey(SPACES);
		regtvstIO.setRgpytype(SPACES);
		defaultDates4700();
	}

protected void optswchCall4500()
	{
		call4510();
	}

protected void call4510()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.oK)) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void regtvstio4600()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, regtvstIO);
		if ((isNE(regtvstIO.getStatuz(),varcom.oK)
		&& isNE(regtvstIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regtvstIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void defaultDates4700()
	{
		setup4710();
	}

protected void setup4710()
	{
		regtvstIO.setCrtdate(wsspcomn.currfrom);
		if (isNE(wsaaAdvance,SPACES)) {
			regtvstIO.setFirstPaydate(wsspcomn.currfrom);
		}
		if (isNE(wsaaArrears,SPACES)) {
			datcon2rec.intDate1.set(wsspcomn.currfrom);
			datcon2rec.frequency.set(wsaaBkfreqann);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			regtvstIO.setFirstPaydate(datcon2rec.intDate2);
		}
		if (isNE(wsaaGuarperd,ZERO)) {
			datcon2rec.intDate1.set(wsspcomn.currfrom);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(wsaaGuarperd);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			regtvstIO.setRevdte(datcon2rec.intDate2);
		}
	}

protected void screenio4800()
	{
		/*CALL*/
		processScreen("S5237", sv);
		if ((isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkIfAnyRegts4900()
	{
		find4910();
	}

protected void find4910()
	{
		wsaaNextRgpy.set(ZERO);
		regtbrkIO.setParams(SPACES);
		regtbrkIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		regtbrkIO.setChdrnum(chdrmjaIO.getChdrnum());
		regtbrkIO.setLife(ZERO);
		regtbrkIO.setCoverage(ZERO);
		regtbrkIO.setRider(ZERO);
		regtbrkIO.setRgpynum(ZERO);
		regtbrkIO.setSeqnbr(ZERO);
		regtbrkIO.setPlanSuffix(ZERO);
		regtbrkIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		regtbrkIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtbrkIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");

		SmartFileCode.execute(appVars, regtbrkIO);
		if ((isNE(regtbrkIO.getStatuz(),varcom.oK)
		&& isNE(regtbrkIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regtbrkIO.getParams());
			fatalError600();
		}
		if ((isEQ(regtbrkIO.getStatuz(),varcom.endp))
		|| (isNE(chdrmjaIO.getChdrcoy(),regtbrkIO.getChdrcoy()))
		|| (isNE(chdrmjaIO.getChdrnum(),regtbrkIO.getChdrnum()))
		|| (isNE(covrmjaIO.getLife(),regtbrkIO.getLife()))
		|| (isNE(covrmjaIO.getCoverage(),regtbrkIO.getCoverage()))
		|| (isNE(covrmjaIO.getRider(),regtbrkIO.getRider()))) {
			wsaaForceBack.set("Y");
		}
	}
}
