package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Td5hqrec  extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData td5hqRec = new FixedLengthStringData(50);
		public FixedLengthStringData calBasis = new FixedLengthStringData(8).isAPartOf(td5hqRec, 0);
	  	public FixedLengthStringData filler1 = new FixedLengthStringData(42).isAPartOf(td5hqRec, 8, FILLER);


		public void initialize() {
			COBOLFunctions.initialize(td5hqRec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			td5hqRec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}

}
