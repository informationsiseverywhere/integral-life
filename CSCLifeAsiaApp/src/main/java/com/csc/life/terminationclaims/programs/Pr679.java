/*
 * File: Pr679.java
 * Date: 30 August 2009 1:53:53
 * Author: Quipoz Limited
 * 
 * Class transformed from PR679.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.screens.Sr679ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* REGULAR BENEFIT BASED COMPONENT ENQUIRY
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The details of the  contract  being  enquired  upon  will  be
*     stored in the CHDRENQ  I/O  module.  Retrieve the details and
*     set up the header portion of the screen as follows:
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*
*
*     Perform a RETRV  on  COVRENQ.  This  will  provide  the  COVR
*     record that is being enquired upon. At this point the program
*     must work out whether it is to provide a Plan Level or Policy
*     Level  enquiry.  This  can  be  done  by  checking  the field
*     WSSP-PLAN-POLICY-FLAG.  If it is 'L', then Plan Level Enquiry
*     is  required,  if  it  is  'O',  then Policy Level Enquiry is
*     required.  For Plan Level Enquiry all the COVRENQ records for
*     the  given  Life, Coverage and Rider must be read and the Sum
*     Insured  and Premium values added together so a total for the
*     whole  Plan  may  be  displayed.  For  Policy  Level  Enquiry
*     processing  will  be different depending on whether a summary
*     record or non-summary record is being displayed. Normally the
*     program  could  determine whether or not a summary record was
*     being  displayed  by  checking  the  Plan Suffix. If this was
*     '0000',  then  it  was  a summary record. However, for Policy
*     Level Enquiry when a summary record is held in the I/O module
*     for  processing  the previous function will replace the value
*     of  '0000'  in  the  Plan  Suffix  with the calculated suffix
*     number of the actual policy that the user selected. Therefore
*     it  will  be  necessary  to check the Plan Suffix against the
*     number  of policies summarised, (CHDRENQ-POLSUM). If the Plan
*     Suffix is not greater the CHDRENQ-POLSUM then it is a summary
*     record.
*
*     For  a  non-summary  record,  (Plan Suffix > CHDRENQ-POLSUM),
*     then display the Sum Insured and Premium as found.
*
*     For  a  summary  record,  (Plan Suffix not > CHDRENQ-POLSUM),
*     then  the  values  held on the COVRENQ record will be for all
*     the  policies summarised, and therefore a calculation must be
*     performed  to  arrive  at the value for one of the summarised
*     policies.  For  all  summarised policies execpt the first one
*     simply divide the total by the number of policies summarised,
*     (CHDRENQ-POLSUM)  and  display the result. This may give rise
*     to  a  rounding  discrepancy. This discrepancy is absorbed by
*     the  first policy summarised, the notional policy number one.
*     If  this  policy  has  been  selected for display perform the
*     following calculation:
*
*     Value To Display = TOTAL - (TOTAL * (POLSUM - 1))
*                                          ~~~~~~~~~~~           *                                            POLSUM
*
*     Where  TOTAL  is  either  the Sum Insured or the Premium, and
*     POLSUM is the number of policies summarised, CHDRENQ-POLSUM.
*
*     For example if the premium is $100 and the number of policies
*     summarised  is 3, the Premium for notional policies #2 and #3
*     will  be  calculated  and  displayed as $33. For policy #1 it
*     will be:
*
*                        100 - (100 * (3 - 1)
*                                      ~~~~~                     *                                        3
*
*                     =  100 - (100 * 2/3)
*
*                     =  $34
*
*
*     The screen title  is  the  component  description from T5687.
*     (See P5123 for how this is obtained and centred).
*
*     Read table T5671,  key  is Transaction Code concatenated with
*     Coverage/Rider Code,  (CRTABLE).  Take  the  validation  item
*     that matches the current program and use it concatenated with
*     the Coverage/Rider Currency to read T5606.
*
*     If Options and Extras  are  not allowed, as defined on T5606,
*     then protect and  non-display  the  prompt and Options/Extras
*     Indicator, (OPTIND02).
*
*     Call  OPTSWCH  (Option  Switching) to load the  two check box
*     fields  at  the  bottom  of the  screen - Annuity Details and
*     Options & Extras.  T1661 / T1660  drive  what  appears in the
*     check box literals.
*
*     Subroutines  exist - PR679ANY  and  PR679LEX  that  check  to
*     see  if  any  ANNY or  LEXT records exist for this  component.
*     OPTSWCH drives  whether  a '+' or spaces  appears in the check
*     box indicator fields.
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
* Updating
* --------
*
*     There is no updating in this program.
*
* Next Program
* ------------
*
*     If "CF11" was requested  move  spaces  to the current program
*     field in the program stack field and exit.
*
*     Call  OPTSWCH  (Option  Switching)  to  switch  to  the   two
*     check boxes at the bottom of the screen if applicable.
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5606 - Reg Benefit Edit Rules            Key: Val. Item||Currency Code
* T5671 - Coverage/Rider Switching          Key: Tr. Code||CRTABLE
* T5687 - General Coverage/Rider Details    Key: CRTABLE
* T5688 - Contract Structure                Key: CNTTYPE
*
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr679 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR679");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/*    VALUE 'Loaded Inst. Prem  :'.                                */
	protected FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	protected FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	protected FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	protected FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(26);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(26, 1, wsaaHeading, 0);

	protected FixedLengthStringData wsaaHedline = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(24, 1, wsaaHedline, 0);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaCovrPstatcode = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaPremStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaPremStatus, "Y");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	protected FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	protected FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	protected static final String chdrlnbrec = "CHDRLNBREC";
	private static final String hbnfrec = "HBNFREC   ";
	protected static final String itemrec = "ITEMREC   ";
	private static final String povrrec = "POVRREC   ";
	
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected CltsTableDAM cltsIO = new CltsTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private PovrTableDAM povrIO = new PovrTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
	protected Batckey wsaaBatckey = new Batckey();
	protected T2240rec t2240rec = new T2240rec();
	private T5606rec t5606rec = new T5606rec();
	protected T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	protected Tr386rec tr386rec = new Tr386rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	protected Wssplife wssplife = new Wssplife();
	private Sr679ScreenVars sv = getLScreenVars();
	
	protected Sr679ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars( Sr679ScreenVars.class);
	}
	protected TablesInner tablesInner = new TablesInner();
	protected WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	// ILIFE -3421 -STARTS
	protected boolean incomeProtectionflag = false;
	protected boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	protected Rcvdpf rcvdPFObject= new Rcvdpf();
	// ILIFE -3421 -ENDS
	protected boolean loadingFlag = false;//IILIFE-3399
	protected boolean dialdownFlag = false;
	protected Chdrpf chdrenqIO = new Chdrpf();//ILIFE-3854
	protected ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);//ILIFE-3854
	protected CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	protected Covrpf covrenqIO = new Covrpf();
	protected boolean exclFlag = false;
	//ILJ-45 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private String itemPFX = "IT";
	//ILJ-45 End

	// ILJ-387 start
	private String cntEnqFeature = "CTENQ010";
	// ILJ-387 end

/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endPolicy1020, 
		nextProg10640, 
		exit1090, 
		search1320, 
		exit1790
	}

	public Pr679() {
		super();
		screenVars = sv;
		new ScreenModel("Sr679", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case endPolicy1020: 
					endPolicy1020();
					gotRecord10620();
				case nextProg10640: 
					nextProg10640();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		boolean cntEnqFlag = false;//ILJ-387
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}		
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set(wssplife.planPolicy);
		wsaaMiscellaneousInner.wsaaSingp.set(ZERO);
		wsaaMiscellaneousInner.wsaaTaxamt.set(ZERO);
		sv.singlePremium.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		/*BRD-306 START */		
		sv.loadper.set(ZERO);
		sv.rateadj.set(ZERO);
		sv.fltmort.set(ZERO);
		sv.premadj.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.adjustageamt.set(ZERO);
		
		/*BRD-306 END */
		//ILIFE-3421
		sv.waitperiod.set(SPACES);
		sv.bentrm.set(SPACES);
		sv.prmbasis.set(SPACES);
		sv.poltyp.set(SPACES);
		//ILIFE-3421
		sv.sumin.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.numpols.set(ZERO);
		sv.taxamt.set(ZERO);
		//ILJ-45 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{					
			//sv.rcessageOut[varcom.nd.toInt()].set("Y");	
			sv.contDtCalcScreenflag.set("N");
		}
		//ILJ-45 End
		else
		{
			sv.contDtCalcScreenflag.set("Y");	
		}

		// ILJ-387 Start
		cntEnqFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");
		if (cntEnqFlag) {
			sv.cntEnqScreenflag.set("Y");
		}else {
			sv.cntEnqScreenflag.set("N");
		}
		// ILJ-387 End

		/*    Set screen fields*/
		/* Read CHDRENQ (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		/*chdrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}*/
		chdrenqIO=chdrpfDAO.getCacheObject(chdrenqIO);//ILIFE-3854
		/*  To read and Keep CHDRLNB for use in Covered Lives Program...*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(chdrenqIO.getChdrcoy());
		chdrlnbIO.setChdrnum(chdrenqIO.getChdrnum());
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction("READR");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		/*  To read HBNF file..........
		hbnfIO.setChdrcoy(chdrenqIO.getChdrcoy());
		hbnfIO.setChdrnum(chdrenqIO.getChdrnum());
		hbnfIO.setFunction(varcom.readr);
		hbnfIO.setFormat(hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)
		&& isNE(hbnfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			syserrrec.statuz.set(hbnfIO.getStatuz());
			fatalError600();
		}
		hbnfIO.setFunction(varcom.keeps);
		hbnfIO.setFormat(hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hbnfIO.getParams());
			syserrrec.statuz.set(hbnfIO.getStatuz());
			fatalError600();
		}
		sv.livesno.set(hbnfIO.getLivesno());
		sv.waiverprem.set(hbnfIO.getWaiverprem());*/
		/*  Read T2240 for age definition*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRENQ-CNTTYPE        TO ITEM-ITEMITEM.                */
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrenqIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM                 */
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else { //MTL002
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}//MTL002

			}
		}
		sv.chdrnum.set(chdrenqIO.getChdrnum());
		sv.cnttype.set(chdrenqIO.getCnttype());
		sv.cntcurr.set(chdrenqIO.getCntcurr());
		sv.register.set(chdrenqIO.getReg());
		sv.numpols.set(chdrenqIO.getPolinc());
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		//ILIFE-3421-STARTS
		/*covrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}*/
		covrenqIO=covrDao.getCacheObject(covrenqIO);
			dialdownFlag = FeaConfg.isFeatureExist(covrenqIO.getChdrcoy().trim(), "NBPRP012", appVars, "IT"); /* IJTI-1479 */
			incomeProtectionflag = FeaConfg.isFeatureExist(covrenqIO.getChdrcoy().trim(), "NBPROP02", appVars, "IT"); /* IJTI-1479 */
			premiumflag = FeaConfg.isFeatureExist(covrenqIO.getChdrcoy().trim(), "NBPROP03", appVars, "IT"); /* IJTI-1479 */
			if(incomeProtectionflag || premiumflag || dialdownFlag){
				callReadRCVDPF();
			}
			if(!incomeProtectionflag){
				sv.waitperiodOut[varcom.nd.toInt()].set("Y");
				sv.bentrmOut[varcom.nd.toInt()].set("Y");
				sv.poltypOut[varcom.nd.toInt()].set("Y");
				
			}else{
				sv.waitperiodOut[varcom.nd.toInt()].set("N");
				sv.bentrmOut[varcom.nd.toInt()].set("N");
				sv.poltypOut[varcom.nd.toInt()].set("N");
				if(rcvdPFObject!=null){
					if(rcvdPFObject.getWaitperiod()!=null && !rcvdPFObject.getWaitperiod().trim().equals("") && isEQ(sv.waitperiod,SPACES)){
						sv.waitperiod.set(rcvdPFObject.getWaitperiod());
					}
					if(rcvdPFObject.getPoltyp()!=null && !rcvdPFObject.getPoltyp().trim().equals("") && isEQ(sv.poltyp,SPACES)){
						sv.poltyp.set(rcvdPFObject.getPoltyp());
					}
					if(rcvdPFObject.getBentrm()!=null && !rcvdPFObject.getBentrm().trim().equals("") && isEQ(sv.bentrm,SPACES)){
						sv.bentrm.set(rcvdPFObject.getBentrm());
					}
				}
			}
			if(!premiumflag){
				sv.prmbasisOut[varcom.nd.toInt()].set("Y");
			}else{
				sv.prmbasisOut[varcom.nd.toInt()].set("N");
				if(rcvdPFObject!=null){
					if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
						sv.prmbasis.set(rcvdPFObject.getPrmbasis());
					}
				}
}
			//ILIFE-3421-ENDS
			//ILIFE-3399-STARTS
			//BRD-NBP-011 starts				
			if(!dialdownFlag)
					sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
			else
			{
				sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
				if(rcvdPFObject != null){
					if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
						sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
					}
				}
			}
			//BRD-NBP-011 ends
			loadingFlag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
			if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
			}
			//ILIFE-3399-ENDS
			exclFlag = FeaConfg.isFeatureExist(chdrenqIO.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
			if(!exclFlag){
				sv.optind08Out[varcom.nd.toInt()].set("Y");
			}
			
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for the existence of Joint Life details.*/
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isEQ(lifeenqIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(lifeenqIO.getLifcnum());
			cltsIO.setClntnum(lifeenqIO.getLifcnum());
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrenqIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrenqIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrenqIO.getPstcde());		//modified for ILIFE-4345
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/* Retrieve valid status'.*/
		readT5679Table1400();
		/*    Read the first LIFE details on the contract.*/
		/*covrenqIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}*/
		covrenqIO=covrDao.getCacheObject(covrenqIO);
		hbnfIO.setChdrcoy(chdrenqIO.getChdrcoy());
		hbnfIO.setChdrnum(chdrenqIO.getChdrnum());
		hbnfIO.setLife(covrenqIO.getLife());
		hbnfIO.setCoverage(covrenqIO.getCoverage());
		hbnfIO.setRider(covrenqIO.getRider());
		hbnfIO.setFunction(varcom.readr);
		hbnfIO.setFormat(hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)
		&& isNE(hbnfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			syserrrec.statuz.set(hbnfIO.getStatuz());
			fatalError600();
		}
		hbnfIO.setFunction(varcom.keeps);
		hbnfIO.setFormat(hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hbnfIO.getParams());
			syserrrec.statuz.set(hbnfIO.getStatuz());
			fatalError600();
		}
		sv.livesno.set(hbnfIO.getLivesno());
		sv.waiverprem.set(hbnfIO.getWaiverprem()); 
		sv.zunit.set(hbnfIO.getZunit());

		if (wsaaMiscellaneousInner.planLevelEnquiry.isTrue()) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaMiscellaneousInner.wsaaSingp.set(covrenqIO.getSingp());
			wsaaMiscellaneousInner.wsaaPremium.set(covrenqIO.getInstprem());
			wsaaMiscellaneousInner.wsaaZlinstprem.set(covrenqIO.getZlinstprem());
			wsaaMiscellaneousInner.wsaaSumins.set(covrenqIO.getSumins());
			setPrecision(covrenqIO.getPlanSuffix(), 0);
			covrenqIO.setPlanSuffix(add(chdrenqIO.getPolsum(), 1).toInt());
			
            List<Covrpf> covrenqList = covrDao.searchValidCovrpfRecord(covrenqIO.getChdrcoy(),
                    covrenqIO.getChdrnum(),covrenqIO.getLife(),covrenqIO.getCoverage(),covrenqIO.getRider());
			while ( !(isGT(covrenqIO.getPlanSuffix(), chdrenqIO.getPolinc()))) {
				planComponent1100(covrenqList);
			}
			
			goTo(GotoLabel.endPolicy1020);
		}
		if (isGT(covrenqIO.getPlanSuffix(), chdrenqIO.getPolsum())) {
			wsaaMiscellaneousInner.wsaaSingp.set(covrenqIO.getSingp());
			wsaaMiscellaneousInner.wsaaPremium.set(covrenqIO.getInstprem());
			wsaaMiscellaneousInner.wsaaZlinstprem.set(covrenqIO.getZlinstprem());
			wsaaMiscellaneousInner.wsaaSumins.set(covrenqIO.getSumins());
			goTo(GotoLabel.endPolicy1020);
		}
		wsaaMiscellaneousInner.wsaaPremium.set(covrenqIO.getInstprem());
		if (isEQ(covrenqIO.getPlanSuffix(), 1)) {
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(sub(covrenqIO.getSingp(), (div(mult(covrenqIO.getSingp(), (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(sub(wsaaMiscellaneousInner.wsaaPremium, (div(mult(wsaaMiscellaneousInner.wsaaPremium, (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 0).set(sub(wsaaMiscellaneousInner.wsaaZlinstprem, (div(mult(wsaaMiscellaneousInner.wsaaZlinstprem, (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(sub(covrenqIO.getSumins(), (div(mult(covrenqIO.getSumins(), (sub(chdrenqIO.getPolsum(), 1))), chdrenqIO.getPolsum()))));
		}
		else {
			compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(div(covrenqIO.getSingp(), chdrenqIO.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaPremium, 0).set(div(wsaaMiscellaneousInner.wsaaPremium, chdrenqIO.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaZlinstprem, 0).set(div(wsaaMiscellaneousInner.wsaaZlinstprem, chdrenqIO.getPolsum()));
			compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(div(covrenqIO.getSumins(), chdrenqIO.getPolsum()));
		}
	}
//ILIFE-3421-STARTS
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();	
	/* IJTI-1479 START*/
	rcvdPFObject.setChdrcoy(covrenqIO.getChdrcoy());
	rcvdPFObject.setChdrnum(covrenqIO.getChdrnum());
	rcvdPFObject.setLife(covrenqIO.getLife());
	rcvdPFObject.setCoverage(covrenqIO.getCoverage());
	rcvdPFObject.setRider(covrenqIO.getRider());
	rcvdPFObject.setCrtable(covrenqIO.getCrtable());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	/* IJTI-1479 END*/
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}
//ILIFE-3421-ENDS
protected void endPolicy1020()
	{
		sv.benBillDate.set(covrenqIO.getBenBillDate());
		if (isEQ(covrenqIO.getBenBillDate(), varcom.vrcmMaxDate)
		|| isEQ(covrenqIO.getBenBillDate(), 0)) {
			sv.bbldatOut[varcom.nd.toInt()].set("Y");
		}
		/*    MOVE COVRENQ-MORTCLS        TO SR679-MORTCLS.                */
		sv.benpln.set(hbnfIO.getBenpln());
		sv.numpols.set(chdrenqIO.getPolinc());
		sv.anbAtCcd.set(covrenqIO.getAnbAtCcd());
		sv.crrcd.set(covrenqIO.getCrrcd());
		sv.annivProcDate.set(covrenqIO.getAnnivProcDate());
		sv.premcess.set(covrenqIO.getPremCessDate());
		sv.liencd.set(covrenqIO.getLiencd());
		sv.riskCessDate.set(covrenqIO.getRiskCessDate());
		sv.riskCessAge.set(covrenqIO.getRiskCessAge());
		sv.premCessAge.set(covrenqIO.getPremCessAge());
		sv.riskCessTerm.set(covrenqIO.getRiskCessTerm());
		sv.premCessTerm.set(covrenqIO.getPremCessTerm());
		sv.register.set(chdrenqIO.getReg());
		sv.life.set(covrenqIO.getLife());
		if (isNE(covrenqIO.getLife(), lifeenqIO.getLife())) {
			getLifeName4900();
		}
		sv.rider.set(covrenqIO.getRider());
		sv.coverage.set(covrenqIO.getCoverage());
		sv.rerateDate.set(covrenqIO.getRerateDate());
		sv.rerateFromDate.set(covrenqIO.getRerateFromDate());
		/*BRD-306 START */
		sv.zbinstprem.set(covrenqIO.getZbinstprem());
		sv.loadper.set(covrenqIO.getLoadper());
		sv.rateadj.set(covrenqIO.getRateadj());
		sv.fltmort.set(covrenqIO.getFltmort());
		sv.premadj.set(covrenqIO.getPremadj());
		sv.adjustageamt.set(covrenqIO.getPremadj());
		/*BRD-306 END */
		sv.singlePremium.set(wsaaMiscellaneousInner.wsaaSingp);
		sv.instPrem.set(wsaaMiscellaneousInner.wsaaPremium);
		sv.zlinstprem.set(wsaaMiscellaneousInner.wsaaZlinstprem);
		
		if (isNE(wsaaMiscellaneousInner.wsaaPremium, 0)) {
			/*--- Read TR386 table to get screen literals                      */
			itemIO.setFormat(itemrec);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr386);
			wsaaTr386Lang.set(wsspcomn.language);
			wsaaTr386Pgm.set(wsaaProg);
			wsaaTr386Id.set(SPACES);
			itemIO.setItemitem(wsaaTr386Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			tr386rec.tr386Rec.set(itemIO.getGenarea());
			/*      MOVE TR386-PROGDESC-1    TO SR679-ZDESC                  */
			sv.zdesc.set(tr386rec.progdesc01);
			/*****     MOVE WSAA-REG-DESC      TO SR679-ZDESC                   */
		}
		sv.zdescOut[varcom.hi.toInt()].set("N");
		sv.statFund.set(covrenqIO.getStatFund());
		sv.statSect.set(covrenqIO.getStatSect());
		sv.statSubsect.set(covrenqIO.getStatSubsect());
		sv.sumin.set(wsaaMiscellaneousInner.wsaaSumins);
		if (isNE(covrenqIO.getPlanSuffix(), ZERO)) {
			sv.planSuffix.set(covrenqIO.getPlanSuffix());
		}
		else {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			sv.planSuffix.set(ZERO);
		}
		sv.instPrem.set(wsaaMiscellaneousInner.wsaaPremium);
		sv.planSuffix.set(ZERO);
		setupBonus1500();
		sv.bappmeth.set(covrenqIO.getBappmeth());
		/* GET THE COMPONENT DESCRIPTION*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrenqIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1700();
		/* READ T5671*/
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaMiscellaneousInner.wsaaTrCode.set(wsaaBatckey.batcBatctrcde);
		wsaaMiscellaneousInner.wsaaCrtable.set(covrenqIO.getCrtable());
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrenqIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setItemitem(wsaaMiscellaneousInner.wsaaT5671Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	**** MOVE SPACES                 TO ITDM-DATA-KEY.                
	**** MOVE CHDRENQ-CHDRCOY        TO ITDM-ITEMCOY.                 
	**** MOVE T5671                  TO ITDM-ITEMTABL.                
	**** MOVE WSAA-T5671-KEY         TO ITDM-ITEMITEM.                
	**** MOVE CHDRENQ-OCCDATE        TO ITDM-ITMFRM.                  
	**** MOVE BEGN                   TO ITDM-FUNCTION.                
	**** CALL 'ITDMIO' USING ITDM-PARAMS.                             
	**** IF ITDM-STATUZ              NOT = O-K                        
	****                         AND NOT = ENDP                       
	****     MOVE ITDM-STATUZ        TO SYSR-STATUZ                   
	****     MOVE ITDM-PARAMS        TO SYSR-PARAMS                   
	****     PERFORM 600-FATAL-ERROR.                                 
	**** IF ITDM-ITEMCOY NOT = CHDRENQ-CHDRCOY                        
	****  OR ITDM-ITEMTABL NOT = T5671                                
	****  OR ITDM-ITEMITEM NOT = WSAA-T5671-KEY                       
	****  OR ITDM-STATUZ = ENDP                                       
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   
	****    PERFORM 600-FATAL-ERROR                                   
	**** ELSE                                                         
	****    MOVE ITDM-GENAREA        TO T5671-T5671-REC.              
	* </pre>
	*/
protected void gotRecord10620()
	{
		sub1.set(0);
	}

protected void nextProg10640()
	{
		sub1.add(1);
		if (isNE(t5671rec.pgm[sub1.toInt()], wsaaProg)) {
			goTo(GotoLabel.nextProg10640);
		}
		else {
			wsaaMiscellaneousInner.wsaaValidItem.set(t5671rec.edtitm[sub1.toInt()]);
		}
		/* READ T5606*/
		wsaaMiscellaneousInner.wsaaCrcyCode.set(chdrenqIO.getCntcurr());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5606);
		itdmIO.setItemitem(wsaaMiscellaneousInner.wsaaT5606Key);
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5606)
		|| isNE(itdmIO.getItemitem(), wsaaMiscellaneousInner.wsaaT5606Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5606rec.t5606Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5606rec.specind, "N")) {
			/*     MOVE 'Y'                 TO SR679-OPTEXTIND-OUT (ND)*/
			sv.optdsc02Out[varcom.nd.toInt()].set("Y");
			sv.optdsc02Out[varcom.pr.toInt()].set("Y");
			sv.optdsc06Out[varcom.nd.toInt()].set("Y");
			sv.optdsc06Out[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.optdsc02Out[varcom.nd.toInt()].set(SPACES);
			sv.optdsc02Out[varcom.pr.toInt()].set(SPACES);
			sv.optdsc06Out[varcom.nd.toInt()].set(SPACES);
			sv.optdsc06Out[varcom.pr.toInt()].set(SPACES);
		}
		//ILIFE-3517 STARTS
		if(isEQ(t5606rec.waitperiod, SPACES)){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(t5606rec.bentrm, SPACES)){
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(t5606rec.poltyp, SPACES)){
			sv.poltypOut[varcom.nd.toInt()].set("Y");
		}
		if(isEQ(t5606rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		//ILIFE-3517 ENDS
		/*    Obtain the frequency short description from T3590.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3590);
		descIO.setDescitem(t5606rec.benfreq);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		else {
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.frqdesc.set(descIO.getShortdesc());
			}
			else {
				sv.frqdesc.fill("?");
			}
		}
		sv.frqdescOut[varcom.nd.toInt()].set("Y");
		checkPovr1800();
		optswchInit1200();
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void planComponent1100(List<Covrpf> covrenqList)
 {
        /* For Plan Level Enquiry the first coverage record read will */
        /* have a Plan Suffix of 1. All the components for the given */
        /* Life, Coverage and Rider must be read and the amounts added */
        /* up to give the total for the component being enquired upon. */
        /*
         * if (isEQ(covrenqIO.getPlanSuffix(), 1) && isNE(chdrenqIO.getPolinc(),
         * 1)) { wsaaMiscellaneousInner.wsaaSumins.set(ZERO); }
         */
        if (isEQ(covrenqIO.getPlanSuffix(), 1) && isNE(chdrenqIO.getPolinc(), 1)) {
            wsaaMiscellaneousInner.wsaaSumins.set(ZERO);
        }
        int curPln = covrenqIO.getPlanSuffix();
        Covrpf covr = null;
        if (covrenqList != null && !covrenqList.isEmpty()) {
            for (Covrpf c : covrenqList) {
                if (c.getPlanSuffix() == covrenqIO.getPlanSuffix()) {
                    covr = c;
                    break;
                }
            }
            if (covr != null) {
                covrenqIO = covr;
                wsaaCovrPstatcode.set(covrenqIO.getPstatcode());
                checkPremStatus1300();
            }
        }
        if (validStatus.isTrue()) {
            compute(wsaaMiscellaneousInner.wsaaSingp, 2).set(
                    add(wsaaMiscellaneousInner.wsaaSingp, covrenqIO.getSingp()));
            /* + COVRENQ-SINGP. <005> */
            /* + COVRENQ-SINGP */
            compute(wsaaMiscellaneousInner.wsaaPremium, 2).set(
                    add(wsaaMiscellaneousInner.wsaaPremium, covrenqIO.getInstprem()));
            /* + COVRENQ-INSTPREM. <005> */
            /* + COVRENQ-INSTPREM <005> */
            compute(wsaaMiscellaneousInner.wsaaZlinstprem, 2).set(
                    add(wsaaMiscellaneousInner.wsaaZlinstprem, covrenqIO.getZlinstprem()));
            compute(wsaaMiscellaneousInner.wsaaSumins, 2).set(
                    add(wsaaMiscellaneousInner.wsaaSumins, covrenqIO.getSumins()));
        }
        setPrecision(covrenqIO.getPlanSuffix(), 0);
        covrenqIO.setPlanSuffix(curPln + 1);
    }

protected void optswchInit1200()
	{
		call1210();
	}

protected void call1210()
	{
		/* The following lines checks whether a window to Annuity*/
		/* Details is made available. This is done by referring to*/
		/* Table T6625*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6625);
		itdmIO.setItemitem(covrenqIO.getCrtable());
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		/* Check that the record is either found or at EOF*/
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6625)
		|| isNE(itdmIO.getItemitem(), covrenqIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sv.optdsc01Out[varcom.nd.toInt()].set("Y");
			sv.optdsc01Out[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.optdsc01Out[varcom.nd.toInt()].set(SPACES);
			sv.optdsc01Out[varcom.pr.toInt()].set(SPACES);
		}
		/* Read TR52D for Taxcode                                          */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.optdsc07Out[varcom.nd.toInt()].set("Y");
		sv.optind07Out[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.optdsc07Out[varcom.pr.toInt()].set("Y");
		sv.optind07Out[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrenqIO.getReg());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.optdsc07Out[varcom.nd.toInt()].set("N");
			sv.optind07Out[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.optdsc07Out[varcom.pr.toInt()].set("N");
			sv.optind07Out[varcom.pr.toInt()].set("N");
		}
		/* Call OPTSWCH to retrieve check box text description*/
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			optswchrec.optsItemCompany.set("0");
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optdsc01.set(optswchrec.optsDsc[1]);
		sv.optind01.set(optswchrec.optsInd[1]);
		sv.optdsc02.set(optswchrec.optsDsc[2]);
		sv.optind02.set(optswchrec.optsInd[2]);
		sv.optdsc03.set(optswchrec.optsDsc[3]);
		sv.optind03.set(optswchrec.optsInd[3]);
		sv.optdsc04.set(optswchrec.optsDsc[4]);
		sv.optind04.set(optswchrec.optsInd[4]);
		sv.optdsc05.set(optswchrec.optsDsc[5]);
		sv.optind05.set(optswchrec.optsInd[5]);
		sv.optdsc06.set(optswchrec.optsDsc[6]);
		sv.optind06.set(optswchrec.optsInd[6]);
		sv.optdsc07.set(optswchrec.optsDsc[7]);
		sv.optind07.set(optswchrec.optsInd[7]);
		sv.optdsc08.set(optswchrec.optsDsc[8]);
		sv.optind08.set(optswchrec.optsInd[8]);
	
	}

protected void checkPremStatus1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1310();
				case search1320: 
					search1320();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* Check the twelve premium status' retrieved from table T5679
	* to see if any match the coverage premium status. If so, set
	* the VALID-STATUS flag to yes.
	* </pre>
	*/
protected void start1310()
	{
		wsaaPremStatus.set(SPACES);
		wsaaSub.set(ZERO);
	}

protected void search1320()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			return ;
		}
		else {
			if (isNE(wsaaCovrPstatcode, t5679rec.covPremStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.search1320);
			}
			else {
				wsaaPremStatus.set("Y");
				return ;
			}
		}
		/*EXIT*/
	}

protected void readT5679Table1400()
	{
		read1410();
	}

protected void read1410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void setupBonus1500()
	{
		/*PARA*/
		/* Check if Coverage/Rider is a SUM product. If not SUM product*/
		/* do not display field.*/
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covrenqIO.getChdrcoy());
		itemIO.setItemitem(covrenqIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		/*EXIT*/
	}

protected void loadHeading1700()
	{
		try {
			loadScreen1710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadScreen1710()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(24); !(isLT(wsaaX, 1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(24, wsaaX));
		if (isNE(wsaaY, 0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		//wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1730();
		}
		/*   WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		//wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1790);
	}

protected void moveChar1730()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY, wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void checkPovr1800()
	{
		readPovr1810();
	}

protected void readPovr1810()
	{
		/* Test the POVR file for the existance of a Premium Breakdown*/
		/* record. If one exists then allow the option to display the*/
		/* select window for enquiry....if not then protect.*/
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(covrenqIO.getChdrcoy());
		povrIO.setChdrnum(covrenqIO.getChdrnum());
		povrIO.setLife(covrenqIO.getLife());
		povrIO.setCoverage(covrenqIO.getCoverage());
		povrIO.setRider(covrenqIO.getRider());
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(povrrec);
		povrIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)
		&& isNE(povrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(povrIO.getStatuz(), varcom.endp)
		|| isNE(povrIO.getChdrcoy(), covrenqIO.getChdrcoy())
		|| isNE(povrIO.getChdrnum(), covrenqIO.getChdrnum())
		|| isNE(povrIO.getLife(), covrenqIO.getLife())
		|| isNE(povrIO.getCoverage(), covrenqIO.getCoverage())
		|| isNE(povrIO.getRider(), covrenqIO.getRider())) {
			sv.optdsc03Out[varcom.nd.toInt()].set("Y");
			sv.optdsc03Out[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.optdsc03Out[varcom.nd.toInt()].set(SPACES);
			sv.optdsc03Out[varcom.pr.toInt()].set(SPACES);
			pbKeeps5100();
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Skip this section if returning from an optional selection,*/
		/* i.e. check the stack action flag equals '*'.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax6000();
		}
		if (isEQ(sv.taxamt, ZERO)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.optdsc07Out[varcom.nd.toInt()].set("Y");
			sv.optind07Out[varcom.nd.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.optdsc07Out[varcom.pr.toInt()].set("Y");
			sv.optind07Out[varcom.pr.toInt()].set("Y");
		}
		/*2010-SCREEN-IO.*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
	}

	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
		/*    CALL 'SR679IO'           USING SCRN-SCREEN-PARAMS*/
		/*                                   SR679-DATA-AREA .*/
		/* Screen errors are now handled in the calling program.*/
		/*    PERFORM 200-SCREEN-ERRORS.*/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/*VALIDATE-SCREEN*/
		optswchChck2100();
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void optswchChck2100()
	{
		call2110();
	}

protected void call2110()
	{
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set("CBOX");
		optswchrec.optsInd[1].set(sv.optind01);
		optswchrec.optsInd[2].set(sv.optind02);
		optswchrec.optsInd[3].set(sv.optind03);
		optswchrec.optsInd[4].set(sv.optind04);
		optswchrec.optsInd[5].set(sv.optind05);
		optswchrec.optsInd[6].set(sv.optind06);
		optswchrec.optsInd[7].set(sv.optind07);
		optswchrec.optsInd[8].set(sv.optind08);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		/* If the check has detected an error in OPTSWCH a non o-k*/
		/* status is returned. Unfortunately, it doesn't tell you*/
		/* which indicator was wrong! Rather than hard code a check*/
		/* for 'X' the status is moved to both indicators.*/
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			sv.optind01Err.set(optswchrec.optsStatuz);
			sv.optind02Err.set(optswchrec.optsStatuz);
			sv.optind03Err.set(optswchrec.optsStatuz);
			sv.optind04Err.set(optswchrec.optsStatuz);
			sv.optind05Err.set(optswchrec.optsStatuz);
			sv.optind06Err.set(optswchrec.optsStatuz);
			sv.optind07Err.set(optswchrec.optsStatuz);
			sv.optind08Err.set(optswchrec.optsStatuz);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  Update database files as required*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*PARA*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			pbRlse5200();
			return ;
		}
		optswchCall4800();
		/*EXIT*/
	}

protected void optswchCall4800()
	{
		call4800();
	}

protected void call4800()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/* Call 'OPTSWCH' with a function of 'STCK' to determine*/
		/* which screen to display next.*/
		wsspcomn.chdrChdrnum.set(covrenqIO.getChdrnum());
		wsspcomn.crtable.set(covrenqIO.getCrtable());
		
		wsspcomn.cmode.set("IFE");
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz, varcom.oK))
		&& (isNE(optswchrec.optsStatuz, varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		sv.optind01.set(optswchrec.optsInd[1]);
		sv.optind02.set(optswchrec.optsInd[2]);
		sv.optind03.set(optswchrec.optsInd[3]);
		sv.optind04.set(optswchrec.optsInd[4]);
		sv.optind05.set(optswchrec.optsInd[5]);
		sv.optind06.set(optswchrec.optsInd[6]);
		sv.optind07.set(optswchrec.optsInd[7]);
		sv.optind08.set(optswchrec.optsInd[8]);
		if (isEQ(optswchrec.optsStatuz, varcom.oK)) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void getLifeName4900()
	{
		para4900();
	}

protected void para4900()
	{
		lifeenqIO.setChdrcoy(covrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(covrenqIO.getChdrnum());
		lifeenqIO.setLife(covrenqIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		sv.lifenum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void pbKeeps5100()
	{
		/*START*/
		/*  - Keep the POVR record for Premium Breakdown enquiry.*/
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void pbRlse5200()
	{
		/*START*/
		/* Release the POVR record as it's no longer required.*/
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkCalcTax6000()
	{
		start6010();
	}

protected void start6010()
	{
		/* Call TR52D tax subroutine                                       */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(covrenqIO.getLife());
		txcalcrec.coverage.set(covrenqIO.getCoverage());
		txcalcrec.rider.set(covrenqIO.getRider());
		txcalcrec.planSuffix.set(covrenqIO.getPlanSuffix());
		txcalcrec.crtable.set(covrenqIO.getCrtable());
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.register.set(chdrenqIO.getReg());
		txcalcrec.taxrule.set(SPACES);
		txcalcrec.ccy.set(chdrenqIO.getCntcurr());
		txcalcrec.rateItem.set(SPACES);
		txcalcrec.amountIn.set(ZERO);
		txcalcrec.transType.set("PREM");
		txcalcrec.txcode.set(tr52drec.txcode);
		if (isEQ(chdrenqIO.getBillfreq(), "00")) {
			txcalcrec.effdate.set(chdrenqIO.getOccdate());
		}
		else {
			txcalcrec.effdate.set(chdrenqIO.getPtdate());
		}
		txcalcrec.tranno.set(chdrenqIO.getTranno());
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			wsaaMiscellaneousInner.wsaaTaxamt.set(sv.instPrem);
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaMiscellaneousInner.wsaaTaxamt.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaMiscellaneousInner.wsaaTaxamt.add(txcalcrec.taxAmt[2]);
			}
			sv.taxamt.set(wsaaMiscellaneousInner.wsaaTaxamt);
			sv.optind07.set("+");
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.optdsc07Out[varcom.nd.toInt()].set("N");
			sv.optind07Out[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.optdsc07Out[varcom.pr.toInt()].set("N");
			sv.optind07Out[varcom.pr.toInt()].set("N");
		}
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
public static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	public FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	public Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");
	private PackedDecimalData wsaaSuffixCount = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaT5606Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaValidItem = new FixedLengthStringData(5).isAPartOf(wsaaT5606Key, 0);
	private FixedLengthStringData wsaaCrcyCode = new FixedLengthStringData(3).isAPartOf(wsaaT5606Key, 5);

	public FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	public FixedLengthStringData wsaaTrCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	public FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);
	public PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
	public PackedDecimalData wsaaSingp = new PackedDecimalData(17, 2);
	public PackedDecimalData wsaaPremium = new PackedDecimalData(17, 2);
	public PackedDecimalData wsaaZlinstprem = new PackedDecimalData(17, 2);
	public PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
public static final class TablesInner { 
	public FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	public FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	public FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	public FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	public FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	public FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t6625 = new FixedLengthStringData(5).init("T6625");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	public FixedLengthStringData tr386 = new FixedLengthStringData(5).init("TR386");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
}
}
