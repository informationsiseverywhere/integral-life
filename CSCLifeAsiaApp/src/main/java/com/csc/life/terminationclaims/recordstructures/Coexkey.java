package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:12
 * Description:
 * Copybook name: COEXKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Coexkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData coexFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData coexKey = new FixedLengthStringData(64).isAPartOf(coexFileKey, 0, REDEFINE);
  	public FixedLengthStringData coexAracde = new FixedLengthStringData(3).isAPartOf(coexKey, 0);
  	public FixedLengthStringData coexAgntnum = new FixedLengthStringData(8).isAPartOf(coexKey, 3);
  	public FixedLengthStringData coexChdrnum = new FixedLengthStringData(8).isAPartOf(coexKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(45).isAPartOf(coexKey, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(coexFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		coexFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}