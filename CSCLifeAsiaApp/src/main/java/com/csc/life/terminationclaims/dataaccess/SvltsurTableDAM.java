package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: SvltsurTableDAM.java
 * Date: Sun, 30 Aug 2009 03:49:31
 * Class transformed from SVLTSUR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class SvltsurTableDAM extends SvltpfTableDAM {

	public SvltsurTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("SVLTSUR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "CNTTYPE, " +
		            "COWNNUM, " +
		            "CTYPEDES, " +
		            "JLIFCNUM, " +
		            "JLINSNAME, " +
		            "LIFCNUM, " +
		            "LINSNAME, " +
		            "OCCDATE, " +
		            "OWNERNAME, " +
		            "PLNSFX, " +
		            "VALIDFLAG, " +
		            "PSTATE, " +
		            "RSTATE, " +
		            "PTDATE, " +
		            "COVERAGE01, " +
		            "COVERAGE02, " +
		            "COVERAGE03, " +
		            "COVERAGE04, " +
		            "COVERAGE05, " +
		            "COVERAGE06, " +
		            "COVERAGE07, " +
		            "COVERAGE08, " +
		            "COVERAGE09, " +
		            "COVERAGE10, " +
		            "RIDER01, " +
		            "RIDER02, " +
		            "RIDER03, " +
		            "RIDER04, " +
		            "RIDER05, " +
		            "RIDER06, " +
		            "RIDER07, " +
		            "RIDER08, " +
		            "RIDER09, " +
		            "RIDER10, " +
		            "SHORTDS01, " +
		            "SHORTDS02, " +
		            "SHORTDS03, " +
		            "SHORTDS04, " +
		            "SHORTDS05, " +
		            "SHORTDS06, " +
		            "SHORTDS07, " +
		            "SHORTDS08, " +
		            "SHORTDS09, " +
		            "SHORTDS10, " +
		            "TYPE01, " +
		            "TYPE02, " +
		            "TYPE03, " +
		            "TYPE04, " +
		            "TYPE05, " +
		            "TYPE06, " +
		            "TYPE07, " +
		            "TYPE08, " +
		            "TYPE09, " +
		            "TYPE10, " +
		            "VIRTFUND01, " +
		            "VIRTFUND02, " +
		            "VIRTFUND03, " +
		            "VIRTFUND04, " +
		            "VIRTFUND05, " +
		            "VIRTFUND06, " +
		            "VIRTFUND07, " +
		            "VIRTFUND08, " +
		            "VIRTFUND09, " +
		            "VIRTFUND10, " +
		            "CNSTCUR01, " +
		            "CNSTCUR02, " +
		            "CNSTCUR03, " +
		            "CNSTCUR04, " +
		            "CNSTCUR05, " +
		            "CNSTCUR06, " +
		            "CNSTCUR07, " +
		            "CNSTCUR08, " +
		            "CNSTCUR09, " +
		            "CNSTCUR10, " +
		            "EMV01, " +
		            "EMV02, " +
		            "EMV03, " +
		            "EMV04, " +
		            "EMV05, " +
		            "EMV06, " +
		            "EMV07, " +
		            "EMV08, " +
		            "EMV09, " +
		            "EMV10, " +
		            "ACTVALUE01, " +
		            "ACTVALUE02, " +
		            "ACTVALUE03, " +
		            "ACTVALUE04, " +
		            "ACTVALUE05, " +
		            "ACTVALUE06, " +
		            "ACTVALUE07, " +
		            "ACTVALUE08, " +
		            "ACTVALUE09, " +
		            "ACTVALUE10, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               cnttype,
                               cownnum,
                               ctypedes,
                               jlifcnum,
                               jlinsname,
                               lifcnum,
                               linsname,
                               occdate,
                               ownername,
                               planSuffix,
                               validflag,
                               pstate,
                               rstate,
                               ptdate,
                               coverage01,
                               coverage02,
                               coverage03,
                               coverage04,
                               coverage05,
                               coverage06,
                               coverage07,
                               coverage08,
                               coverage09,
                               coverage10,
                               rider01,
                               rider02,
                               rider03,
                               rider04,
                               rider05,
                               rider06,
                               rider07,
                               rider08,
                               rider09,
                               rider10,
                               shortds01,
                               shortds02,
                               shortds03,
                               shortds04,
                               shortds05,
                               shortds06,
                               shortds07,
                               shortds08,
                               shortds09,
                               shortds10,
                               fieldType01,
                               fieldType02,
                               fieldType03,
                               fieldType04,
                               fieldType05,
                               fieldType06,
                               fieldType07,
                               fieldType08,
                               fieldType09,
                               fieldType10,
                               fund01,
                               fund02,
                               fund03,
                               fund04,
                               fund05,
                               fund06,
                               fund07,
                               fund08,
                               fund09,
                               fund10,
                               cnstcur01,
                               cnstcur02,
                               cnstcur03,
                               cnstcur04,
                               cnstcur05,
                               cnstcur06,
                               cnstcur07,
                               cnstcur08,
                               cnstcur09,
                               cnstcur10,
                               estMatValue01,
                               estMatValue02,
                               estMatValue03,
                               estMatValue04,
                               estMatValue05,
                               estMatValue06,
                               estMatValue07,
                               estMatValue08,
                               estMatValue09,
                               estMatValue10,
                               actvalue01,
                               actvalue02,
                               actvalue03,
                               actvalue04,
                               actvalue05,
                               actvalue06,
                               actvalue07,
                               actvalue08,
                               actvalue09,
                               actvalue10,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(703);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getCnttype().toInternal()
					+ getCownnum().toInternal()
					+ getCtypedes().toInternal()
					+ getJlifcnum().toInternal()
					+ getJlinsname().toInternal()
					+ getLifcnum().toInternal()
					+ getLinsname().toInternal()
					+ getOccdate().toInternal()
					+ getOwnername().toInternal()
					+ getPlanSuffix().toInternal()
					+ getValidflag().toInternal()
					+ getPstate().toInternal()
					+ getRstate().toInternal()
					+ getPtdate().toInternal()
					+ getCoverage01().toInternal()
					+ getCoverage02().toInternal()
					+ getCoverage03().toInternal()
					+ getCoverage04().toInternal()
					+ getCoverage05().toInternal()
					+ getCoverage06().toInternal()
					+ getCoverage07().toInternal()
					+ getCoverage08().toInternal()
					+ getCoverage09().toInternal()
					+ getCoverage10().toInternal()
					+ getRider01().toInternal()
					+ getRider02().toInternal()
					+ getRider03().toInternal()
					+ getRider04().toInternal()
					+ getRider05().toInternal()
					+ getRider06().toInternal()
					+ getRider07().toInternal()
					+ getRider08().toInternal()
					+ getRider09().toInternal()
					+ getRider10().toInternal()
					+ getShortds01().toInternal()
					+ getShortds02().toInternal()
					+ getShortds03().toInternal()
					+ getShortds04().toInternal()
					+ getShortds05().toInternal()
					+ getShortds06().toInternal()
					+ getShortds07().toInternal()
					+ getShortds08().toInternal()
					+ getShortds09().toInternal()
					+ getShortds10().toInternal()
					+ getFieldType01().toInternal()
					+ getFieldType02().toInternal()
					+ getFieldType03().toInternal()
					+ getFieldType04().toInternal()
					+ getFieldType05().toInternal()
					+ getFieldType06().toInternal()
					+ getFieldType07().toInternal()
					+ getFieldType08().toInternal()
					+ getFieldType09().toInternal()
					+ getFieldType10().toInternal()
					+ getFund01().toInternal()
					+ getFund02().toInternal()
					+ getFund03().toInternal()
					+ getFund04().toInternal()
					+ getFund05().toInternal()
					+ getFund06().toInternal()
					+ getFund07().toInternal()
					+ getFund08().toInternal()
					+ getFund09().toInternal()
					+ getFund10().toInternal()
					+ getCnstcur01().toInternal()
					+ getCnstcur02().toInternal()
					+ getCnstcur03().toInternal()
					+ getCnstcur04().toInternal()
					+ getCnstcur05().toInternal()
					+ getCnstcur06().toInternal()
					+ getCnstcur07().toInternal()
					+ getCnstcur08().toInternal()
					+ getCnstcur09().toInternal()
					+ getCnstcur10().toInternal()
					+ getEstMatValue01().toInternal()
					+ getEstMatValue02().toInternal()
					+ getEstMatValue03().toInternal()
					+ getEstMatValue04().toInternal()
					+ getEstMatValue05().toInternal()
					+ getEstMatValue06().toInternal()
					+ getEstMatValue07().toInternal()
					+ getEstMatValue08().toInternal()
					+ getEstMatValue09().toInternal()
					+ getEstMatValue10().toInternal()
					+ getActvalue01().toInternal()
					+ getActvalue02().toInternal()
					+ getActvalue03().toInternal()
					+ getActvalue04().toInternal()
					+ getActvalue05().toInternal()
					+ getActvalue06().toInternal()
					+ getActvalue07().toInternal()
					+ getActvalue08().toInternal()
					+ getActvalue09().toInternal()
					+ getActvalue10().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, ctypedes);
			what = ExternalData.chop(what, jlifcnum);
			what = ExternalData.chop(what, jlinsname);
			what = ExternalData.chop(what, lifcnum);
			what = ExternalData.chop(what, linsname);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, ownername);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, pstate);
			what = ExternalData.chop(what, rstate);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, coverage01);
			what = ExternalData.chop(what, coverage02);
			what = ExternalData.chop(what, coverage03);
			what = ExternalData.chop(what, coverage04);
			what = ExternalData.chop(what, coverage05);
			what = ExternalData.chop(what, coverage06);
			what = ExternalData.chop(what, coverage07);
			what = ExternalData.chop(what, coverage08);
			what = ExternalData.chop(what, coverage09);
			what = ExternalData.chop(what, coverage10);
			what = ExternalData.chop(what, rider01);
			what = ExternalData.chop(what, rider02);
			what = ExternalData.chop(what, rider03);
			what = ExternalData.chop(what, rider04);
			what = ExternalData.chop(what, rider05);
			what = ExternalData.chop(what, rider06);
			what = ExternalData.chop(what, rider07);
			what = ExternalData.chop(what, rider08);
			what = ExternalData.chop(what, rider09);
			what = ExternalData.chop(what, rider10);
			what = ExternalData.chop(what, shortds01);
			what = ExternalData.chop(what, shortds02);
			what = ExternalData.chop(what, shortds03);
			what = ExternalData.chop(what, shortds04);
			what = ExternalData.chop(what, shortds05);
			what = ExternalData.chop(what, shortds06);
			what = ExternalData.chop(what, shortds07);
			what = ExternalData.chop(what, shortds08);
			what = ExternalData.chop(what, shortds09);
			what = ExternalData.chop(what, shortds10);
			what = ExternalData.chop(what, fieldType01);
			what = ExternalData.chop(what, fieldType02);
			what = ExternalData.chop(what, fieldType03);
			what = ExternalData.chop(what, fieldType04);
			what = ExternalData.chop(what, fieldType05);
			what = ExternalData.chop(what, fieldType06);
			what = ExternalData.chop(what, fieldType07);
			what = ExternalData.chop(what, fieldType08);
			what = ExternalData.chop(what, fieldType09);
			what = ExternalData.chop(what, fieldType10);
			what = ExternalData.chop(what, fund01);
			what = ExternalData.chop(what, fund02);
			what = ExternalData.chop(what, fund03);
			what = ExternalData.chop(what, fund04);
			what = ExternalData.chop(what, fund05);
			what = ExternalData.chop(what, fund06);
			what = ExternalData.chop(what, fund07);
			what = ExternalData.chop(what, fund08);
			what = ExternalData.chop(what, fund09);
			what = ExternalData.chop(what, fund10);
			what = ExternalData.chop(what, cnstcur01);
			what = ExternalData.chop(what, cnstcur02);
			what = ExternalData.chop(what, cnstcur03);
			what = ExternalData.chop(what, cnstcur04);
			what = ExternalData.chop(what, cnstcur05);
			what = ExternalData.chop(what, cnstcur06);
			what = ExternalData.chop(what, cnstcur07);
			what = ExternalData.chop(what, cnstcur08);
			what = ExternalData.chop(what, cnstcur09);
			what = ExternalData.chop(what, cnstcur10);
			what = ExternalData.chop(what, estMatValue01);
			what = ExternalData.chop(what, estMatValue02);
			what = ExternalData.chop(what, estMatValue03);
			what = ExternalData.chop(what, estMatValue04);
			what = ExternalData.chop(what, estMatValue05);
			what = ExternalData.chop(what, estMatValue06);
			what = ExternalData.chop(what, estMatValue07);
			what = ExternalData.chop(what, estMatValue08);
			what = ExternalData.chop(what, estMatValue09);
			what = ExternalData.chop(what, estMatValue10);
			what = ExternalData.chop(what, actvalue01);
			what = ExternalData.chop(what, actvalue02);
			what = ExternalData.chop(what, actvalue03);
			what = ExternalData.chop(what, actvalue04);
			what = ExternalData.chop(what, actvalue05);
			what = ExternalData.chop(what, actvalue06);
			what = ExternalData.chop(what, actvalue07);
			what = ExternalData.chop(what, actvalue08);
			what = ExternalData.chop(what, actvalue09);
			what = ExternalData.chop(what, actvalue10);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public FixedLengthStringData getCtypedes() {
		return ctypedes;
	}
	public void setCtypedes(Object what) {
		ctypedes.set(what);
	}	
	public FixedLengthStringData getJlifcnum() {
		return jlifcnum;
	}
	public void setJlifcnum(Object what) {
		jlifcnum.set(what);
	}	
	public FixedLengthStringData getJlinsname() {
		return jlinsname;
	}
	public void setJlinsname(Object what) {
		jlinsname.set(what);
	}	
	public FixedLengthStringData getLifcnum() {
		return lifcnum;
	}
	public void setLifcnum(Object what) {
		lifcnum.set(what);
	}	
	public FixedLengthStringData getLinsname() {
		return linsname;
	}
	public void setLinsname(Object what) {
		linsname.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public FixedLengthStringData getOwnername() {
		return ownername;
	}
	public void setOwnername(Object what) {
		ownername.set(what);
	}	
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getPstate() {
		return pstate;
	}
	public void setPstate(Object what) {
		pstate.set(what);
	}	
	public FixedLengthStringData getRstate() {
		return rstate;
	}
	public void setRstate(Object what) {
		rstate.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public FixedLengthStringData getCoverage01() {
		return coverage01;
	}
	public void setCoverage01(Object what) {
		coverage01.set(what);
	}	
	public FixedLengthStringData getCoverage02() {
		return coverage02;
	}
	public void setCoverage02(Object what) {
		coverage02.set(what);
	}	
	public FixedLengthStringData getCoverage03() {
		return coverage03;
	}
	public void setCoverage03(Object what) {
		coverage03.set(what);
	}	
	public FixedLengthStringData getCoverage04() {
		return coverage04;
	}
	public void setCoverage04(Object what) {
		coverage04.set(what);
	}	
	public FixedLengthStringData getCoverage05() {
		return coverage05;
	}
	public void setCoverage05(Object what) {
		coverage05.set(what);
	}	
	public FixedLengthStringData getCoverage06() {
		return coverage06;
	}
	public void setCoverage06(Object what) {
		coverage06.set(what);
	}	
	public FixedLengthStringData getCoverage07() {
		return coverage07;
	}
	public void setCoverage07(Object what) {
		coverage07.set(what);
	}	
	public FixedLengthStringData getCoverage08() {
		return coverage08;
	}
	public void setCoverage08(Object what) {
		coverage08.set(what);
	}	
	public FixedLengthStringData getCoverage09() {
		return coverage09;
	}
	public void setCoverage09(Object what) {
		coverage09.set(what);
	}	
	public FixedLengthStringData getCoverage10() {
		return coverage10;
	}
	public void setCoverage10(Object what) {
		coverage10.set(what);
	}	
	public FixedLengthStringData getRider01() {
		return rider01;
	}
	public void setRider01(Object what) {
		rider01.set(what);
	}	
	public FixedLengthStringData getRider02() {
		return rider02;
	}
	public void setRider02(Object what) {
		rider02.set(what);
	}	
	public FixedLengthStringData getRider03() {
		return rider03;
	}
	public void setRider03(Object what) {
		rider03.set(what);
	}	
	public FixedLengthStringData getRider04() {
		return rider04;
	}
	public void setRider04(Object what) {
		rider04.set(what);
	}	
	public FixedLengthStringData getRider05() {
		return rider05;
	}
	public void setRider05(Object what) {
		rider05.set(what);
	}	
	public FixedLengthStringData getRider06() {
		return rider06;
	}
	public void setRider06(Object what) {
		rider06.set(what);
	}	
	public FixedLengthStringData getRider07() {
		return rider07;
	}
	public void setRider07(Object what) {
		rider07.set(what);
	}	
	public FixedLengthStringData getRider08() {
		return rider08;
	}
	public void setRider08(Object what) {
		rider08.set(what);
	}	
	public FixedLengthStringData getRider09() {
		return rider09;
	}
	public void setRider09(Object what) {
		rider09.set(what);
	}	
	public FixedLengthStringData getRider10() {
		return rider10;
	}
	public void setRider10(Object what) {
		rider10.set(what);
	}	
	public FixedLengthStringData getShortds01() {
		return shortds01;
	}
	public void setShortds01(Object what) {
		shortds01.set(what);
	}	
	public FixedLengthStringData getShortds02() {
		return shortds02;
	}
	public void setShortds02(Object what) {
		shortds02.set(what);
	}	
	public FixedLengthStringData getShortds03() {
		return shortds03;
	}
	public void setShortds03(Object what) {
		shortds03.set(what);
	}	
	public FixedLengthStringData getShortds04() {
		return shortds04;
	}
	public void setShortds04(Object what) {
		shortds04.set(what);
	}	
	public FixedLengthStringData getShortds05() {
		return shortds05;
	}
	public void setShortds05(Object what) {
		shortds05.set(what);
	}	
	public FixedLengthStringData getShortds06() {
		return shortds06;
	}
	public void setShortds06(Object what) {
		shortds06.set(what);
	}	
	public FixedLengthStringData getShortds07() {
		return shortds07;
	}
	public void setShortds07(Object what) {
		shortds07.set(what);
	}	
	public FixedLengthStringData getShortds08() {
		return shortds08;
	}
	public void setShortds08(Object what) {
		shortds08.set(what);
	}	
	public FixedLengthStringData getShortds09() {
		return shortds09;
	}
	public void setShortds09(Object what) {
		shortds09.set(what);
	}	
	public FixedLengthStringData getShortds10() {
		return shortds10;
	}
	public void setShortds10(Object what) {
		shortds10.set(what);
	}	
	public FixedLengthStringData getFieldType01() {
		return fieldType01;
	}
	public void setFieldType01(Object what) {
		fieldType01.set(what);
	}	
	public FixedLengthStringData getFieldType02() {
		return fieldType02;
	}
	public void setFieldType02(Object what) {
		fieldType02.set(what);
	}	
	public FixedLengthStringData getFieldType03() {
		return fieldType03;
	}
	public void setFieldType03(Object what) {
		fieldType03.set(what);
	}	
	public FixedLengthStringData getFieldType04() {
		return fieldType04;
	}
	public void setFieldType04(Object what) {
		fieldType04.set(what);
	}	
	public FixedLengthStringData getFieldType05() {
		return fieldType05;
	}
	public void setFieldType05(Object what) {
		fieldType05.set(what);
	}	
	public FixedLengthStringData getFieldType06() {
		return fieldType06;
	}
	public void setFieldType06(Object what) {
		fieldType06.set(what);
	}	
	public FixedLengthStringData getFieldType07() {
		return fieldType07;
	}
	public void setFieldType07(Object what) {
		fieldType07.set(what);
	}	
	public FixedLengthStringData getFieldType08() {
		return fieldType08;
	}
	public void setFieldType08(Object what) {
		fieldType08.set(what);
	}	
	public FixedLengthStringData getFieldType09() {
		return fieldType09;
	}
	public void setFieldType09(Object what) {
		fieldType09.set(what);
	}	
	public FixedLengthStringData getFieldType10() {
		return fieldType10;
	}
	public void setFieldType10(Object what) {
		fieldType10.set(what);
	}	
	public FixedLengthStringData getFund01() {
		return fund01;
	}
	public void setFund01(Object what) {
		fund01.set(what);
	}	
	public FixedLengthStringData getFund02() {
		return fund02;
	}
	public void setFund02(Object what) {
		fund02.set(what);
	}	
	public FixedLengthStringData getFund03() {
		return fund03;
	}
	public void setFund03(Object what) {
		fund03.set(what);
	}	
	public FixedLengthStringData getFund04() {
		return fund04;
	}
	public void setFund04(Object what) {
		fund04.set(what);
	}	
	public FixedLengthStringData getFund05() {
		return fund05;
	}
	public void setFund05(Object what) {
		fund05.set(what);
	}	
	public FixedLengthStringData getFund06() {
		return fund06;
	}
	public void setFund06(Object what) {
		fund06.set(what);
	}	
	public FixedLengthStringData getFund07() {
		return fund07;
	}
	public void setFund07(Object what) {
		fund07.set(what);
	}	
	public FixedLengthStringData getFund08() {
		return fund08;
	}
	public void setFund08(Object what) {
		fund08.set(what);
	}	
	public FixedLengthStringData getFund09() {
		return fund09;
	}
	public void setFund09(Object what) {
		fund09.set(what);
	}	
	public FixedLengthStringData getFund10() {
		return fund10;
	}
	public void setFund10(Object what) {
		fund10.set(what);
	}	
	public FixedLengthStringData getCnstcur01() {
		return cnstcur01;
	}
	public void setCnstcur01(Object what) {
		cnstcur01.set(what);
	}	
	public FixedLengthStringData getCnstcur02() {
		return cnstcur02;
	}
	public void setCnstcur02(Object what) {
		cnstcur02.set(what);
	}	
	public FixedLengthStringData getCnstcur03() {
		return cnstcur03;
	}
	public void setCnstcur03(Object what) {
		cnstcur03.set(what);
	}	
	public FixedLengthStringData getCnstcur04() {
		return cnstcur04;
	}
	public void setCnstcur04(Object what) {
		cnstcur04.set(what);
	}	
	public FixedLengthStringData getCnstcur05() {
		return cnstcur05;
	}
	public void setCnstcur05(Object what) {
		cnstcur05.set(what);
	}	
	public FixedLengthStringData getCnstcur06() {
		return cnstcur06;
	}
	public void setCnstcur06(Object what) {
		cnstcur06.set(what);
	}	
	public FixedLengthStringData getCnstcur07() {
		return cnstcur07;
	}
	public void setCnstcur07(Object what) {
		cnstcur07.set(what);
	}	
	public FixedLengthStringData getCnstcur08() {
		return cnstcur08;
	}
	public void setCnstcur08(Object what) {
		cnstcur08.set(what);
	}	
	public FixedLengthStringData getCnstcur09() {
		return cnstcur09;
	}
	public void setCnstcur09(Object what) {
		cnstcur09.set(what);
	}	
	public FixedLengthStringData getCnstcur10() {
		return cnstcur10;
	}
	public void setCnstcur10(Object what) {
		cnstcur10.set(what);
	}	
	public PackedDecimalData getEstMatValue01() {
		return estMatValue01;
	}
	public void setEstMatValue01(Object what) {
		setEstMatValue01(what, false);
	}
	public void setEstMatValue01(Object what, boolean rounded) {
		if (rounded)
			estMatValue01.setRounded(what);
		else
			estMatValue01.set(what);
	}	
	public PackedDecimalData getEstMatValue02() {
		return estMatValue02;
	}
	public void setEstMatValue02(Object what) {
		setEstMatValue02(what, false);
	}
	public void setEstMatValue02(Object what, boolean rounded) {
		if (rounded)
			estMatValue02.setRounded(what);
		else
			estMatValue02.set(what);
	}	
	public PackedDecimalData getEstMatValue03() {
		return estMatValue03;
	}
	public void setEstMatValue03(Object what) {
		setEstMatValue03(what, false);
	}
	public void setEstMatValue03(Object what, boolean rounded) {
		if (rounded)
			estMatValue03.setRounded(what);
		else
			estMatValue03.set(what);
	}	
	public PackedDecimalData getEstMatValue04() {
		return estMatValue04;
	}
	public void setEstMatValue04(Object what) {
		setEstMatValue04(what, false);
	}
	public void setEstMatValue04(Object what, boolean rounded) {
		if (rounded)
			estMatValue04.setRounded(what);
		else
			estMatValue04.set(what);
	}	
	public PackedDecimalData getEstMatValue05() {
		return estMatValue05;
	}
	public void setEstMatValue05(Object what) {
		setEstMatValue05(what, false);
	}
	public void setEstMatValue05(Object what, boolean rounded) {
		if (rounded)
			estMatValue05.setRounded(what);
		else
			estMatValue05.set(what);
	}	
	public PackedDecimalData getEstMatValue06() {
		return estMatValue06;
	}
	public void setEstMatValue06(Object what) {
		setEstMatValue06(what, false);
	}
	public void setEstMatValue06(Object what, boolean rounded) {
		if (rounded)
			estMatValue06.setRounded(what);
		else
			estMatValue06.set(what);
	}	
	public PackedDecimalData getEstMatValue07() {
		return estMatValue07;
	}
	public void setEstMatValue07(Object what) {
		setEstMatValue07(what, false);
	}
	public void setEstMatValue07(Object what, boolean rounded) {
		if (rounded)
			estMatValue07.setRounded(what);
		else
			estMatValue07.set(what);
	}	
	public PackedDecimalData getEstMatValue08() {
		return estMatValue08;
	}
	public void setEstMatValue08(Object what) {
		setEstMatValue08(what, false);
	}
	public void setEstMatValue08(Object what, boolean rounded) {
		if (rounded)
			estMatValue08.setRounded(what);
		else
			estMatValue08.set(what);
	}	
	public PackedDecimalData getEstMatValue09() {
		return estMatValue09;
	}
	public void setEstMatValue09(Object what) {
		setEstMatValue09(what, false);
	}
	public void setEstMatValue09(Object what, boolean rounded) {
		if (rounded)
			estMatValue09.setRounded(what);
		else
			estMatValue09.set(what);
	}	
	public PackedDecimalData getEstMatValue10() {
		return estMatValue10;
	}
	public void setEstMatValue10(Object what) {
		setEstMatValue10(what, false);
	}
	public void setEstMatValue10(Object what, boolean rounded) {
		if (rounded)
			estMatValue10.setRounded(what);
		else
			estMatValue10.set(what);
	}	
	public PackedDecimalData getActvalue01() {
		return actvalue01;
	}
	public void setActvalue01(Object what) {
		setActvalue01(what, false);
	}
	public void setActvalue01(Object what, boolean rounded) {
		if (rounded)
			actvalue01.setRounded(what);
		else
			actvalue01.set(what);
	}	
	public PackedDecimalData getActvalue02() {
		return actvalue02;
	}
	public void setActvalue02(Object what) {
		setActvalue02(what, false);
	}
	public void setActvalue02(Object what, boolean rounded) {
		if (rounded)
			actvalue02.setRounded(what);
		else
			actvalue02.set(what);
	}	
	public PackedDecimalData getActvalue03() {
		return actvalue03;
	}
	public void setActvalue03(Object what) {
		setActvalue03(what, false);
	}
	public void setActvalue03(Object what, boolean rounded) {
		if (rounded)
			actvalue03.setRounded(what);
		else
			actvalue03.set(what);
	}	
	public PackedDecimalData getActvalue04() {
		return actvalue04;
	}
	public void setActvalue04(Object what) {
		setActvalue04(what, false);
	}
	public void setActvalue04(Object what, boolean rounded) {
		if (rounded)
			actvalue04.setRounded(what);
		else
			actvalue04.set(what);
	}	
	public PackedDecimalData getActvalue05() {
		return actvalue05;
	}
	public void setActvalue05(Object what) {
		setActvalue05(what, false);
	}
	public void setActvalue05(Object what, boolean rounded) {
		if (rounded)
			actvalue05.setRounded(what);
		else
			actvalue05.set(what);
	}	
	public PackedDecimalData getActvalue06() {
		return actvalue06;
	}
	public void setActvalue06(Object what) {
		setActvalue06(what, false);
	}
	public void setActvalue06(Object what, boolean rounded) {
		if (rounded)
			actvalue06.setRounded(what);
		else
			actvalue06.set(what);
	}	
	public PackedDecimalData getActvalue07() {
		return actvalue07;
	}
	public void setActvalue07(Object what) {
		setActvalue07(what, false);
	}
	public void setActvalue07(Object what, boolean rounded) {
		if (rounded)
			actvalue07.setRounded(what);
		else
			actvalue07.set(what);
	}	
	public PackedDecimalData getActvalue08() {
		return actvalue08;
	}
	public void setActvalue08(Object what) {
		setActvalue08(what, false);
	}
	public void setActvalue08(Object what, boolean rounded) {
		if (rounded)
			actvalue08.setRounded(what);
		else
			actvalue08.set(what);
	}	
	public PackedDecimalData getActvalue09() {
		return actvalue09;
	}
	public void setActvalue09(Object what) {
		setActvalue09(what, false);
	}
	public void setActvalue09(Object what, boolean rounded) {
		if (rounded)
			actvalue09.setRounded(what);
		else
			actvalue09.set(what);
	}	
	public PackedDecimalData getActvalue10() {
		return actvalue10;
	}
	public void setActvalue10(Object what) {
		setActvalue10(what, false);
	}
	public void setActvalue10(Object what, boolean rounded) {
		if (rounded)
			actvalue10.setRounded(what);
		else
			actvalue10.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getVirtfunds() {
		return new FixedLengthStringData(fund01.toInternal()
										+ fund02.toInternal()
										+ fund03.toInternal()
										+ fund04.toInternal()
										+ fund05.toInternal()
										+ fund06.toInternal()
										+ fund07.toInternal()
										+ fund08.toInternal()
										+ fund09.toInternal()
										+ fund10.toInternal());
	}
	public void setVirtfunds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getVirtfunds().getLength()).init(obj);
	
		what = ExternalData.chop(what, fund01);
		what = ExternalData.chop(what, fund02);
		what = ExternalData.chop(what, fund03);
		what = ExternalData.chop(what, fund04);
		what = ExternalData.chop(what, fund05);
		what = ExternalData.chop(what, fund06);
		what = ExternalData.chop(what, fund07);
		what = ExternalData.chop(what, fund08);
		what = ExternalData.chop(what, fund09);
		what = ExternalData.chop(what, fund10);
	}
	public FixedLengthStringData getVirtfund(BaseData indx) {
		return getVirtfund(indx.toInt());
	}
	public FixedLengthStringData getVirtfund(int indx) {

		switch (indx) {
			case 1 : return fund01;
			case 2 : return fund02;
			case 3 : return fund03;
			case 4 : return fund04;
			case 5 : return fund05;
			case 6 : return fund06;
			case 7 : return fund07;
			case 8 : return fund08;
			case 9 : return fund09;
			case 10 : return fund10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setVirtfund(BaseData indx, Object what) {
		setVirtfund(indx.toInt(), what);
	}
	public void setVirtfund(int indx, Object what) {

		switch (indx) {
			case 1 : setFund01(what);
					 break;
			case 2 : setFund02(what);
					 break;
			case 3 : setFund03(what);
					 break;
			case 4 : setFund04(what);
					 break;
			case 5 : setFund05(what);
					 break;
			case 6 : setFund06(what);
					 break;
			case 7 : setFund07(what);
					 break;
			case 8 : setFund08(what);
					 break;
			case 9 : setFund09(what);
					 break;
			case 10 : setFund10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getTypes() {
		return new FixedLengthStringData(fieldType01.toInternal()
										+ fieldType02.toInternal()
										+ fieldType03.toInternal()
										+ fieldType04.toInternal()
										+ fieldType05.toInternal()
										+ fieldType06.toInternal()
										+ fieldType07.toInternal()
										+ fieldType08.toInternal()
										+ fieldType09.toInternal()
										+ fieldType10.toInternal());
	}
	public void setTypes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTypes().getLength()).init(obj);
	
		what = ExternalData.chop(what, fieldType01);
		what = ExternalData.chop(what, fieldType02);
		what = ExternalData.chop(what, fieldType03);
		what = ExternalData.chop(what, fieldType04);
		what = ExternalData.chop(what, fieldType05);
		what = ExternalData.chop(what, fieldType06);
		what = ExternalData.chop(what, fieldType07);
		what = ExternalData.chop(what, fieldType08);
		what = ExternalData.chop(what, fieldType09);
		what = ExternalData.chop(what, fieldType10);
	}
	public FixedLengthStringData getType(BaseData indx) {
		return getType(indx.toInt());
	}
	public FixedLengthStringData getType(int indx) {

		switch (indx) {
			case 1 : return fieldType01;
			case 2 : return fieldType02;
			case 3 : return fieldType03;
			case 4 : return fieldType04;
			case 5 : return fieldType05;
			case 6 : return fieldType06;
			case 7 : return fieldType07;
			case 8 : return fieldType08;
			case 9 : return fieldType09;
			case 10 : return fieldType10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setType(BaseData indx, Object what) {
		setType(indx.toInt(), what);
	}
	public void setType(int indx, Object what) {

		switch (indx) {
			case 1 : setFieldType01(what);
					 break;
			case 2 : setFieldType02(what);
					 break;
			case 3 : setFieldType03(what);
					 break;
			case 4 : setFieldType04(what);
					 break;
			case 5 : setFieldType05(what);
					 break;
			case 6 : setFieldType06(what);
					 break;
			case 7 : setFieldType07(what);
					 break;
			case 8 : setFieldType08(what);
					 break;
			case 9 : setFieldType09(what);
					 break;
			case 10 : setFieldType10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getShortdss() {
		return new FixedLengthStringData(shortds01.toInternal()
										+ shortds02.toInternal()
										+ shortds03.toInternal()
										+ shortds04.toInternal()
										+ shortds05.toInternal()
										+ shortds06.toInternal()
										+ shortds07.toInternal()
										+ shortds08.toInternal()
										+ shortds09.toInternal()
										+ shortds10.toInternal());
	}
	public void setShortdss(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getShortdss().getLength()).init(obj);
	
		what = ExternalData.chop(what, shortds01);
		what = ExternalData.chop(what, shortds02);
		what = ExternalData.chop(what, shortds03);
		what = ExternalData.chop(what, shortds04);
		what = ExternalData.chop(what, shortds05);
		what = ExternalData.chop(what, shortds06);
		what = ExternalData.chop(what, shortds07);
		what = ExternalData.chop(what, shortds08);
		what = ExternalData.chop(what, shortds09);
		what = ExternalData.chop(what, shortds10);
	}
	public FixedLengthStringData getShortds(BaseData indx) {
		return getShortds(indx.toInt());
	}
	public FixedLengthStringData getShortds(int indx) {

		switch (indx) {
			case 1 : return shortds01;
			case 2 : return shortds02;
			case 3 : return shortds03;
			case 4 : return shortds04;
			case 5 : return shortds05;
			case 6 : return shortds06;
			case 7 : return shortds07;
			case 8 : return shortds08;
			case 9 : return shortds09;
			case 10 : return shortds10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setShortds(BaseData indx, Object what) {
		setShortds(indx.toInt(), what);
	}
	public void setShortds(int indx, Object what) {

		switch (indx) {
			case 1 : setShortds01(what);
					 break;
			case 2 : setShortds02(what);
					 break;
			case 3 : setShortds03(what);
					 break;
			case 4 : setShortds04(what);
					 break;
			case 5 : setShortds05(what);
					 break;
			case 6 : setShortds06(what);
					 break;
			case 7 : setShortds07(what);
					 break;
			case 8 : setShortds08(what);
					 break;
			case 9 : setShortds09(what);
					 break;
			case 10 : setShortds10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getRiders() {
		return new FixedLengthStringData(rider01.toInternal()
										+ rider02.toInternal()
										+ rider03.toInternal()
										+ rider04.toInternal()
										+ rider05.toInternal()
										+ rider06.toInternal()
										+ rider07.toInternal()
										+ rider08.toInternal()
										+ rider09.toInternal()
										+ rider10.toInternal());
	}
	public void setRiders(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getRiders().getLength()).init(obj);
	
		what = ExternalData.chop(what, rider01);
		what = ExternalData.chop(what, rider02);
		what = ExternalData.chop(what, rider03);
		what = ExternalData.chop(what, rider04);
		what = ExternalData.chop(what, rider05);
		what = ExternalData.chop(what, rider06);
		what = ExternalData.chop(what, rider07);
		what = ExternalData.chop(what, rider08);
		what = ExternalData.chop(what, rider09);
		what = ExternalData.chop(what, rider10);
	}
	public FixedLengthStringData getRider(BaseData indx) {
		return getRider(indx.toInt());
	}
	public FixedLengthStringData getRider(int indx) {

		switch (indx) {
			case 1 : return rider01;
			case 2 : return rider02;
			case 3 : return rider03;
			case 4 : return rider04;
			case 5 : return rider05;
			case 6 : return rider06;
			case 7 : return rider07;
			case 8 : return rider08;
			case 9 : return rider09;
			case 10 : return rider10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setRider(BaseData indx, Object what) {
		setRider(indx.toInt(), what);
	}
	public void setRider(int indx, Object what) {

		switch (indx) {
			case 1 : setRider01(what);
					 break;
			case 2 : setRider02(what);
					 break;
			case 3 : setRider03(what);
					 break;
			case 4 : setRider04(what);
					 break;
			case 5 : setRider05(what);
					 break;
			case 6 : setRider06(what);
					 break;
			case 7 : setRider07(what);
					 break;
			case 8 : setRider08(what);
					 break;
			case 9 : setRider09(what);
					 break;
			case 10 : setRider10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getEmvs() {
		return new FixedLengthStringData(estMatValue01.toInternal()
										+ estMatValue02.toInternal()
										+ estMatValue03.toInternal()
										+ estMatValue04.toInternal()
										+ estMatValue05.toInternal()
										+ estMatValue06.toInternal()
										+ estMatValue07.toInternal()
										+ estMatValue08.toInternal()
										+ estMatValue09.toInternal()
										+ estMatValue10.toInternal());
	}
	public void setEmvs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getEmvs().getLength()).init(obj);
	
		what = ExternalData.chop(what, estMatValue01);
		what = ExternalData.chop(what, estMatValue02);
		what = ExternalData.chop(what, estMatValue03);
		what = ExternalData.chop(what, estMatValue04);
		what = ExternalData.chop(what, estMatValue05);
		what = ExternalData.chop(what, estMatValue06);
		what = ExternalData.chop(what, estMatValue07);
		what = ExternalData.chop(what, estMatValue08);
		what = ExternalData.chop(what, estMatValue09);
		what = ExternalData.chop(what, estMatValue10);
	}
	public PackedDecimalData getEmv(BaseData indx) {
		return getEmv(indx.toInt());
	}
	public PackedDecimalData getEmv(int indx) {

		switch (indx) {
			case 1 : return estMatValue01;
			case 2 : return estMatValue02;
			case 3 : return estMatValue03;
			case 4 : return estMatValue04;
			case 5 : return estMatValue05;
			case 6 : return estMatValue06;
			case 7 : return estMatValue07;
			case 8 : return estMatValue08;
			case 9 : return estMatValue09;
			case 10 : return estMatValue10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setEmv(BaseData indx, Object what) {
		setEmv(indx, what, false);
	}
	public void setEmv(BaseData indx, Object what, boolean rounded) {
		setEmv(indx.toInt(), what, rounded);
	}
	public void setEmv(int indx, Object what) {
		setEmv(indx, what, false);
	}
	public void setEmv(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setEstMatValue01(what, rounded);
					 break;
			case 2 : setEstMatValue02(what, rounded);
					 break;
			case 3 : setEstMatValue03(what, rounded);
					 break;
			case 4 : setEstMatValue04(what, rounded);
					 break;
			case 5 : setEstMatValue05(what, rounded);
					 break;
			case 6 : setEstMatValue06(what, rounded);
					 break;
			case 7 : setEstMatValue07(what, rounded);
					 break;
			case 8 : setEstMatValue08(what, rounded);
					 break;
			case 9 : setEstMatValue09(what, rounded);
					 break;
			case 10 : setEstMatValue10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getCoverages() {
		return new FixedLengthStringData(coverage01.toInternal()
										+ coverage02.toInternal()
										+ coverage03.toInternal()
										+ coverage04.toInternal()
										+ coverage05.toInternal()
										+ coverage06.toInternal()
										+ coverage07.toInternal()
										+ coverage08.toInternal()
										+ coverage09.toInternal()
										+ coverage10.toInternal());
	}
	public void setCoverages(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getCoverages().getLength()).init(obj);
	
		what = ExternalData.chop(what, coverage01);
		what = ExternalData.chop(what, coverage02);
		what = ExternalData.chop(what, coverage03);
		what = ExternalData.chop(what, coverage04);
		what = ExternalData.chop(what, coverage05);
		what = ExternalData.chop(what, coverage06);
		what = ExternalData.chop(what, coverage07);
		what = ExternalData.chop(what, coverage08);
		what = ExternalData.chop(what, coverage09);
		what = ExternalData.chop(what, coverage10);
	}
	public FixedLengthStringData getCoverage(BaseData indx) {
		return getCoverage(indx.toInt());
	}
	public FixedLengthStringData getCoverage(int indx) {

		switch (indx) {
			case 1 : return coverage01;
			case 2 : return coverage02;
			case 3 : return coverage03;
			case 4 : return coverage04;
			case 5 : return coverage05;
			case 6 : return coverage06;
			case 7 : return coverage07;
			case 8 : return coverage08;
			case 9 : return coverage09;
			case 10 : return coverage10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setCoverage(BaseData indx, Object what) {
		setCoverage(indx.toInt(), what);
	}
	public void setCoverage(int indx, Object what) {

		switch (indx) {
			case 1 : setCoverage01(what);
					 break;
			case 2 : setCoverage02(what);
					 break;
			case 3 : setCoverage03(what);
					 break;
			case 4 : setCoverage04(what);
					 break;
			case 5 : setCoverage05(what);
					 break;
			case 6 : setCoverage06(what);
					 break;
			case 7 : setCoverage07(what);
					 break;
			case 8 : setCoverage08(what);
					 break;
			case 9 : setCoverage09(what);
					 break;
			case 10 : setCoverage10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getCnstcurs() {
		return new FixedLengthStringData(cnstcur01.toInternal()
										+ cnstcur02.toInternal()
										+ cnstcur03.toInternal()
										+ cnstcur04.toInternal()
										+ cnstcur05.toInternal()
										+ cnstcur06.toInternal()
										+ cnstcur07.toInternal()
										+ cnstcur08.toInternal()
										+ cnstcur09.toInternal()
										+ cnstcur10.toInternal());
	}
	public void setCnstcurs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getCnstcurs().getLength()).init(obj);
	
		what = ExternalData.chop(what, cnstcur01);
		what = ExternalData.chop(what, cnstcur02);
		what = ExternalData.chop(what, cnstcur03);
		what = ExternalData.chop(what, cnstcur04);
		what = ExternalData.chop(what, cnstcur05);
		what = ExternalData.chop(what, cnstcur06);
		what = ExternalData.chop(what, cnstcur07);
		what = ExternalData.chop(what, cnstcur08);
		what = ExternalData.chop(what, cnstcur09);
		what = ExternalData.chop(what, cnstcur10);
	}
	public FixedLengthStringData getCnstcur(BaseData indx) {
		return getCnstcur(indx.toInt());
	}
	public FixedLengthStringData getCnstcur(int indx) {

		switch (indx) {
			case 1 : return cnstcur01;
			case 2 : return cnstcur02;
			case 3 : return cnstcur03;
			case 4 : return cnstcur04;
			case 5 : return cnstcur05;
			case 6 : return cnstcur06;
			case 7 : return cnstcur07;
			case 8 : return cnstcur08;
			case 9 : return cnstcur09;
			case 10 : return cnstcur10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setCnstcur(BaseData indx, Object what) {
		setCnstcur(indx.toInt(), what);
	}
	public void setCnstcur(int indx, Object what) {

		switch (indx) {
			case 1 : setCnstcur01(what);
					 break;
			case 2 : setCnstcur02(what);
					 break;
			case 3 : setCnstcur03(what);
					 break;
			case 4 : setCnstcur04(what);
					 break;
			case 5 : setCnstcur05(what);
					 break;
			case 6 : setCnstcur06(what);
					 break;
			case 7 : setCnstcur07(what);
					 break;
			case 8 : setCnstcur08(what);
					 break;
			case 9 : setCnstcur09(what);
					 break;
			case 10 : setCnstcur10(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getActvalues() {
		return new FixedLengthStringData(actvalue01.toInternal()
										+ actvalue02.toInternal()
										+ actvalue03.toInternal()
										+ actvalue04.toInternal()
										+ actvalue05.toInternal()
										+ actvalue06.toInternal()
										+ actvalue07.toInternal()
										+ actvalue08.toInternal()
										+ actvalue09.toInternal()
										+ actvalue10.toInternal());
	}
	public void setActvalues(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getActvalues().getLength()).init(obj);
	
		what = ExternalData.chop(what, actvalue01);
		what = ExternalData.chop(what, actvalue02);
		what = ExternalData.chop(what, actvalue03);
		what = ExternalData.chop(what, actvalue04);
		what = ExternalData.chop(what, actvalue05);
		what = ExternalData.chop(what, actvalue06);
		what = ExternalData.chop(what, actvalue07);
		what = ExternalData.chop(what, actvalue08);
		what = ExternalData.chop(what, actvalue09);
		what = ExternalData.chop(what, actvalue10);
	}
	public PackedDecimalData getActvalue(BaseData indx) {
		return getActvalue(indx.toInt());
	}
	public PackedDecimalData getActvalue(int indx) {

		switch (indx) {
			case 1 : return actvalue01;
			case 2 : return actvalue02;
			case 3 : return actvalue03;
			case 4 : return actvalue04;
			case 5 : return actvalue05;
			case 6 : return actvalue06;
			case 7 : return actvalue07;
			case 8 : return actvalue08;
			case 9 : return actvalue09;
			case 10 : return actvalue10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setActvalue(BaseData indx, Object what) {
		setActvalue(indx, what, false);
	}
	public void setActvalue(BaseData indx, Object what, boolean rounded) {
		setActvalue(indx.toInt(), what, rounded);
	}
	public void setActvalue(int indx, Object what) {
		setActvalue(indx, what, false);
	}
	public void setActvalue(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setActvalue01(what, rounded);
					 break;
			case 2 : setActvalue02(what, rounded);
					 break;
			case 3 : setActvalue03(what, rounded);
					 break;
			case 4 : setActvalue04(what, rounded);
					 break;
			case 5 : setActvalue05(what, rounded);
					 break;
			case 6 : setActvalue06(what, rounded);
					 break;
			case 7 : setActvalue07(what, rounded);
					 break;
			case 8 : setActvalue08(what, rounded);
					 break;
			case 9 : setActvalue09(what, rounded);
					 break;
			case 10 : setActvalue10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		cnttype.clear();
		cownnum.clear();
		ctypedes.clear();
		jlifcnum.clear();
		jlinsname.clear();
		lifcnum.clear();
		linsname.clear();
		occdate.clear();
		ownername.clear();
		planSuffix.clear();
		validflag.clear();
		pstate.clear();
		rstate.clear();
		ptdate.clear();
		coverage01.clear();
		coverage02.clear();
		coverage03.clear();
		coverage04.clear();
		coverage05.clear();
		coverage06.clear();
		coverage07.clear();
		coverage08.clear();
		coverage09.clear();
		coverage10.clear();
		rider01.clear();
		rider02.clear();
		rider03.clear();
		rider04.clear();
		rider05.clear();
		rider06.clear();
		rider07.clear();
		rider08.clear();
		rider09.clear();
		rider10.clear();
		shortds01.clear();
		shortds02.clear();
		shortds03.clear();
		shortds04.clear();
		shortds05.clear();
		shortds06.clear();
		shortds07.clear();
		shortds08.clear();
		shortds09.clear();
		shortds10.clear();
		fieldType01.clear();
		fieldType02.clear();
		fieldType03.clear();
		fieldType04.clear();
		fieldType05.clear();
		fieldType06.clear();
		fieldType07.clear();
		fieldType08.clear();
		fieldType09.clear();
		fieldType10.clear();
		fund01.clear();
		fund02.clear();
		fund03.clear();
		fund04.clear();
		fund05.clear();
		fund06.clear();
		fund07.clear();
		fund08.clear();
		fund09.clear();
		fund10.clear();
		cnstcur01.clear();
		cnstcur02.clear();
		cnstcur03.clear();
		cnstcur04.clear();
		cnstcur05.clear();
		cnstcur06.clear();
		cnstcur07.clear();
		cnstcur08.clear();
		cnstcur09.clear();
		cnstcur10.clear();
		estMatValue01.clear();
		estMatValue02.clear();
		estMatValue03.clear();
		estMatValue04.clear();
		estMatValue05.clear();
		estMatValue06.clear();
		estMatValue07.clear();
		estMatValue08.clear();
		estMatValue09.clear();
		estMatValue10.clear();
		actvalue01.clear();
		actvalue02.clear();
		actvalue03.clear();
		actvalue04.clear();
		actvalue05.clear();
		actvalue06.clear();
		actvalue07.clear();
		actvalue08.clear();
		actvalue09.clear();
		actvalue10.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}