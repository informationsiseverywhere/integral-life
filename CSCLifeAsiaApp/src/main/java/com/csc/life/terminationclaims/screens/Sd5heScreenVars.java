package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sd5heScreenVars  extends SmartVarModel {
	public FixedLengthStringData dataArea = new FixedLengthStringData(158);
	public FixedLengthStringData dataFields = new FixedLengthStringData(62).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,14);
	public ZonedDecimalData minpayamnt = DD.payamount.copyToZonedDecimal().isAPartOf(dataFields, 44);
	public ZonedDecimalData maxpayamnt = DD.payamount.copyToZonedDecimal().isAPartOf(dataFields, 53);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 62);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData minpayamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData maxpayamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 86);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] minpayamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] maxpayamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public LongData Sd5hescreenWritten = new LongData(0);
	public LongData Sd5heprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5heScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, item, tabl, longdesc, minpayamnt, maxpayamnt};
		screenOutFields = new BaseData[][] {companyOut, itemOut, tablOut, longdescOut, minpayamntOut, maxpayamntOut};
		screenErrFields = new BaseData[] {companyErr, itemErr, tablErr, longdescErr, minpayamntErr, maxpayamntErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5hescreen.class;
		protectRecord = Sd5heprotect.class;
	}

}
