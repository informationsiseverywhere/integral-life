package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RegrdelTableDAM.java
 * Date: Sun, 30 Aug 2009 03:46:04
 * Class transformed from REGRDEL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RegrdelTableDAM extends RegrpfTableDAM {

	public RegrdelTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("REGRDEL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "RGPYNUM, " +
		            "PYMT, " +
		            "CURRCD, " +
		            "PRCNT, " +
		            "RGPYTYPE, " +
		            "PAYREASON, " +
		            "RGPYSTAT, " +
		            "REVDTE, " +
		            "FPAYDATE, " +
		            "LPAYDATE, " +
		            "PROGNAME, " +
		            "EXCODE, " +
		            "EXREPORT, " +
		            "CRTABLE, " +
		            "TRANNO, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               rgpynum,
                               pymt,
                               currcd,
                               prcnt,
                               rgpytype,
                               payreason,
                               rgpystat,
                               revdte,
                               firstPaydate,
                               lastPaydate,
                               progname,
                               excode,
                               exreport,
                               crtable,
                               tranno,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(122);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getRgpynum().toInternal()
					+ getPymt().toInternal()
					+ getCurrcd().toInternal()
					+ getPrcnt().toInternal()
					+ getRgpytype().toInternal()
					+ getPayreason().toInternal()
					+ getRgpystat().toInternal()
					+ getRevdte().toInternal()
					+ getFirstPaydate().toInternal()
					+ getLastPaydate().toInternal()
					+ getProgname().toInternal()
					+ getExcode().toInternal()
					+ getExreport().toInternal()
					+ getCrtable().toInternal()
					+ getTranno().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, rgpynum);
			what = ExternalData.chop(what, pymt);
			what = ExternalData.chop(what, currcd);
			what = ExternalData.chop(what, prcnt);
			what = ExternalData.chop(what, rgpytype);
			what = ExternalData.chop(what, payreason);
			what = ExternalData.chop(what, rgpystat);
			what = ExternalData.chop(what, revdte);
			what = ExternalData.chop(what, firstPaydate);
			what = ExternalData.chop(what, lastPaydate);
			what = ExternalData.chop(what, progname);
			what = ExternalData.chop(what, excode);
			what = ExternalData.chop(what, exreport);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public PackedDecimalData getRgpynum() {
		return rgpynum;
	}
	public void setRgpynum(Object what) {
		setRgpynum(what, false);
	}
	public void setRgpynum(Object what, boolean rounded) {
		if (rounded)
			rgpynum.setRounded(what);
		else
			rgpynum.set(what);
	}	
	public PackedDecimalData getPymt() {
		return pymt;
	}
	public void setPymt(Object what) {
		setPymt(what, false);
	}
	public void setPymt(Object what, boolean rounded) {
		if (rounded)
			pymt.setRounded(what);
		else
			pymt.set(what);
	}	
	public FixedLengthStringData getCurrcd() {
		return currcd;
	}
	public void setCurrcd(Object what) {
		currcd.set(what);
	}	
	public PackedDecimalData getPrcnt() {
		return prcnt;
	}
	public void setPrcnt(Object what) {
		setPrcnt(what, false);
	}
	public void setPrcnt(Object what, boolean rounded) {
		if (rounded)
			prcnt.setRounded(what);
		else
			prcnt.set(what);
	}	
	public FixedLengthStringData getRgpytype() {
		return rgpytype;
	}
	public void setRgpytype(Object what) {
		rgpytype.set(what);
	}	
	public FixedLengthStringData getPayreason() {
		return payreason;
	}
	public void setPayreason(Object what) {
		payreason.set(what);
	}	
	public FixedLengthStringData getRgpystat() {
		return rgpystat;
	}
	public void setRgpystat(Object what) {
		rgpystat.set(what);
	}	
	public PackedDecimalData getRevdte() {
		return revdte;
	}
	public void setRevdte(Object what) {
		setRevdte(what, false);
	}
	public void setRevdte(Object what, boolean rounded) {
		if (rounded)
			revdte.setRounded(what);
		else
			revdte.set(what);
	}	
	public PackedDecimalData getFirstPaydate() {
		return firstPaydate;
	}
	public void setFirstPaydate(Object what) {
		setFirstPaydate(what, false);
	}
	public void setFirstPaydate(Object what, boolean rounded) {
		if (rounded)
			firstPaydate.setRounded(what);
		else
			firstPaydate.set(what);
	}	
	public PackedDecimalData getLastPaydate() {
		return lastPaydate;
	}
	public void setLastPaydate(Object what) {
		setLastPaydate(what, false);
	}
	public void setLastPaydate(Object what, boolean rounded) {
		if (rounded)
			lastPaydate.setRounded(what);
		else
			lastPaydate.set(what);
	}	
	public FixedLengthStringData getProgname() {
		return progname;
	}
	public void setProgname(Object what) {
		progname.set(what);
	}	
	public FixedLengthStringData getExcode() {
		return excode;
	}
	public void setExcode(Object what) {
		excode.set(what);
	}	
	public FixedLengthStringData getExreport() {
		return exreport;
	}
	public void setExreport(Object what) {
		exreport.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		rgpynum.clear();
		pymt.clear();
		currcd.clear();
		prcnt.clear();
		rgpytype.clear();
		payreason.clear();
		rgpystat.clear();
		revdte.clear();
		firstPaydate.clear();
		lastPaydate.clear();
		progname.clear();
		excode.clear();
		exreport.clear();
		crtable.clear();
		tranno.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}