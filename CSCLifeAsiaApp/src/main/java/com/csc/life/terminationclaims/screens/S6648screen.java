package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6648screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6648ScreenVars sv = (S6648ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6648screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6648ScreenVars screenVars = (S6648ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.minisuma01.setClassString("");
		screenVars.minterm01.setClassString("");
		screenVars.minmthif01.setClassString("");
		screenVars.minpusa01.setClassString("");
		screenVars.minipup01.setClassString("");
		screenVars.minisuma02.setClassString("");
		screenVars.minterm02.setClassString("");
		screenVars.minmthif02.setClassString("");
		screenVars.minpusa02.setClassString("");
		screenVars.minipup02.setClassString("");
		screenVars.minisuma03.setClassString("");
		screenVars.minterm03.setClassString("");
		screenVars.minmthif03.setClassString("");
		screenVars.minpusa03.setClassString("");
		screenVars.minipup03.setClassString("");
		screenVars.minisuma04.setClassString("");
		screenVars.minterm04.setClassString("");
		screenVars.minmthif04.setClassString("");
		screenVars.minpusa04.setClassString("");
		screenVars.minipup04.setClassString("");
		screenVars.minisuma05.setClassString("");
		screenVars.minterm05.setClassString("");
		screenVars.minmthif05.setClassString("");
		screenVars.minpusa05.setClassString("");
		screenVars.minipup05.setClassString("");
	}

/**
 * Clear all the variables in S6648screen
 */
	public static void clear(VarModel pv) {
		S6648ScreenVars screenVars = (S6648ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.minisuma01.clear();
		screenVars.minterm01.clear();
		screenVars.minmthif01.clear();
		screenVars.minpusa01.clear();
		screenVars.minipup01.clear();
		screenVars.minisuma02.clear();
		screenVars.minterm02.clear();
		screenVars.minmthif02.clear();
		screenVars.minpusa02.clear();
		screenVars.minipup02.clear();
		screenVars.minisuma03.clear();
		screenVars.minterm03.clear();
		screenVars.minmthif03.clear();
		screenVars.minpusa03.clear();
		screenVars.minipup03.clear();
		screenVars.minisuma04.clear();
		screenVars.minterm04.clear();
		screenVars.minmthif04.clear();
		screenVars.minpusa04.clear();
		screenVars.minipup04.clear();
		screenVars.minisuma05.clear();
		screenVars.minterm05.clear();
		screenVars.minmthif05.clear();
		screenVars.minpusa05.clear();
		screenVars.minipup05.clear();
	}
}
