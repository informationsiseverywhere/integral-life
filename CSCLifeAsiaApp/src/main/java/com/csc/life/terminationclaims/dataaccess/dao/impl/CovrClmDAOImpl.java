package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.model.CovrClm;
import com.csc.life.terminationclaims.dataaccess.dao.CovrClmDAO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CovrClmDAOImpl extends BaseDAOImpl<CovrClm> implements CovrClmDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(CovrClmDAOImpl.class);
		
	   public List<CovrClm> selectCovrClmData(CovrClm covrClmData) {
			
			
		    
		
			List<CovrClm> data = new LinkedList<CovrClm>();
			PreparedStatement stmt = null;
			ResultSet rs = null;
			StringBuilder sql = new StringBuilder();
			
			// ---------------------------------
			// Construct Query
			// ---------------------------------
			sql.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PRMCUR, JLIFE, CRTABLE,RCESDTE, CRRCD,");
			sql.append("INSTPREM, SINGP, CBCVIN, PSTATCODE,VALIDFLAG,RCESDTE,SUMINS,STATCODE,CBUNST ");
		    sql.append(" FROM COVRCLM WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND VALIDFLAG = '1' ");
			sql.append(" ORDER BY ");
			sql.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
			
			stmt = getPrepareStatement(sql.toString());
			
			try {
				// ---------------------------------
				// Set Parameters dynamically
				// ---------------------------------
				stmt.setString(1, covrClmData.getChdrcoy());
				stmt.setString(2, covrClmData.getChdrnum());
				stmt.setString(3, covrClmData.getLife());
				stmt.setString(4, covrClmData.getCoverage());
				stmt.setString(5, covrClmData.getRider());
				
				
				// ---------------------------------
				// Execute Query
				// ---------------------------------
				rs = executeQuery(stmt);
				CovrClm covrClm = null;
				while(rs.next()) {
					covrClm = new CovrClm();
					covrClm.setChdrcoy(rs.getString(1));
					covrClm.setChdrnum(rs.getString(2));
					covrClm.setLife(rs.getString(3));
					covrClm.setCoverage(rs.getString(4));
					covrClm.setRider(rs.getString(5));
					covrClm.setPremCurrency(rs.getString(6));
					covrClm.setJlife(rs.getString(7));
					covrClm.setCrtable(rs.getString(8));
					covrClm.setRiskCessDate(rs.getInt(9));
					covrClm.setCrrcd(rs.getInt(10));
					covrClm.setInstprem(rs.getBigDecimal(11));
					covrClm.setSingp(rs.getBigDecimal(12));
					covrClm.setConvertInitialUnits(rs.getInt(13));
					covrClm.setPstatcode(rs.getString(14));
					covrClm.setValidflag(rs.getString(15));
					covrClm.setRiskCessDate(rs.getInt(16));
					covrClm.setSumins(rs.getBigDecimal(17));
					covrClm.setStatcode(rs.getString(18));
					covrClm.setUnitStatementDate(rs.getInt(19));
				
					data.add(covrClm);
				}
			} catch (SQLException e) {
				LOGGER.error("selectCovrClMData()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(stmt,rs);
				
				}
			return data;
		}
	     
	   public List<CovrClm> selectCovrClmCoverageData(CovrClm covrClmData) {
			
			
		    
			
			List<CovrClm> data = new LinkedList<CovrClm>();
			PreparedStatement stmt = null;
			ResultSet rs = null;
			StringBuilder sql = new StringBuilder();
			
			// ---------------------------------
			// Construct Query
			// ---------------------------------
			sql.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PRMCUR, JLIFE, CRTABLE,RCESDTE, CRRCD,");
			sql.append("INSTPREM, SINGP, CBCVIN, PSTATCODE,VALIDFLAG,RCESDTE,SUMINS,STATCODE,CBUNST ");
		    sql.append(" FROM COVRCLM WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ?  AND VALIDFLAG = '1' ");
			sql.append(" ORDER BY ");
			sql.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
			
			stmt = getPrepareStatement(sql.toString());
			
			try {
				// ---------------------------------
				// Set Parameters dynamically
				// ---------------------------------
				stmt.setString(1, covrClmData.getChdrcoy());
				stmt.setString(2, covrClmData.getChdrnum());
				stmt.setString(3, covrClmData.getLife());
				stmt.setString(4, covrClmData.getCoverage());
				//stmt.setString(5, covrClmData.getRider());
				
				 
				// ---------------------------------
				// Execute Query
				// ---------------------------------
				rs = executeQuery(stmt);
				CovrClm covrClm = null;
				while(rs.next()) {
					covrClm = new CovrClm();
					covrClm.setChdrcoy(rs.getString(1));
					covrClm.setChdrnum(rs.getString(2));
					covrClm.setLife(rs.getString(3));
					covrClm.setCoverage(rs.getString(4));
					covrClm.setRider(rs.getString(5));
					covrClm.setPremCurrency(rs.getString(6));
					covrClm.setJlife(rs.getString(7));
					covrClm.setCrtable(rs.getString(8));
					covrClm.setRiskCessDate(rs.getInt(9));
					covrClm.setCrrcd(rs.getInt(10));
					covrClm.setInstprem(rs.getBigDecimal(11));
					covrClm.setSingp(rs.getBigDecimal(12));
					covrClm.setConvertInitialUnits(rs.getInt(13));
					covrClm.setPstatcode(rs.getString(14));
					covrClm.setValidflag(rs.getString(15));
					covrClm.setRiskCessDate(rs.getInt(16));
					covrClm.setSumins(rs.getBigDecimal(17));
					covrClm.setStatcode(rs.getString(18));
					covrClm.setUnitStatementDate(rs.getInt(19));
				
					data.add(covrClm);
				}
			} catch (SQLException e) {
				LOGGER.error("selectCovrClMData()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(stmt,rs);
				
				}
			return data;
		}
	   //Ilife-4406 by liwei
		 public List<CovrClm> searchCovrclmRecord(String chdrcoy, String chdrnum){
					
					StringBuilder sql = new StringBuilder("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX, PRMCUR,JLIFE, CRTABLE,");
					sql.append(" RCESDTE, CRRCD,CBCVIN,PSTATCODE, STATCODE, INSTPREM,STATREASN FROM VM1DTA.COVRCLM");
					sql.append(" WHERE CHDRCOY=? AND CHDRNUM=?");
					sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC");
					
					PreparedStatement ps = null;
					ResultSet rs = null;
					List<CovrClm> covrpfList = new ArrayList<>();
					try{
						ps = getPrepareStatement(sql.toString());
						ps.setString(1, chdrcoy.trim());
						ps.setString(2, chdrnum.trim());
						rs = ps.executeQuery();
						
						while(rs.next()){
							CovrClm covrpf = new CovrClm();
							covrpf.setChdrcoy(rs.getString("CHDRCOY"));
							covrpf.setChdrnum(rs.getString("CHDRNUM"));
							covrpf.setLife(rs.getString("LIFE"));
							covrpf.setCoverage(rs.getString("COVERAGE"));
							covrpf.setRider(rs.getString("RIDER"));
							covrpf.setPlanSuffix(rs.getInt("PLNSFX"));
							covrpf.setJlife(rs.getString("JLIFE"));
							covrpf.setStatcode(rs.getString("STATCODE"));
							covrpf.setPstatcode(rs.getString("PSTATCODE"));
							covrpf.setStatreasn(rs.getString("STATREASN"));
							covrpf.setCrrcd(rs.getInt("CRRCD"));
							covrpf.setPremCurrency(rs.getString("PRMCUR"));
							covrpf.setConvertInitialUnits(rs.getInt("CBCVIN"));
							covrpf.setPremCurrency(rs.getString("PRMCUR"));
							covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
							covrpf.setCrtable(rs.getString("CRTABLE"));
							covrpfList.add(covrpf);
						}
					}catch (SQLException e) {
						LOGGER.error("searchCovrclmRecord()", e);//IJTI-1561
						throw new SQLRuntimeException(e);
					}
					finally
					{
						close(ps,rs);			
					}		
					
					return covrpfList;
		 }
}