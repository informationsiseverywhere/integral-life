package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR688
 * @version 1.0 generated on 30/08/09 07:24
 * @author Quipoz
 */
public class Sr688ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(737);
	public FixedLengthStringData dataFields = new FixedLengthStringData(305).isAPartOf(dataArea, 0);
	public ZonedDecimalData clamamt = DD.clamamt.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData desc = DD.desc.copy().isAPartOf(dataFields,17);
	public FixedLengthStringData taccamts = new FixedLengthStringData(24).isAPartOf(dataFields, 67);
	public ZonedDecimalData[] taccamt = ZDArrayPartOfStructure(2, 12, 2, taccamts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(taccamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData taccamt1 = new ZonedDecimalData(12, 2).isAPartOf(filler, 0);
	public ZonedDecimalData taccamt2 = new ZonedDecimalData(12, 2).isAPartOf(filler, 12);
	public ZonedDecimalData tactexp = DD.tactexp.copyToZonedDecimal().isAPartOf(dataFields,91);
	public ZonedDecimalData tclmamt = DD.tclmamt.copyToZonedDecimal().isAPartOf(dataFields,103);
	public FixedLengthStringData tdeducts = new FixedLengthStringData(36).isAPartOf(dataFields, 115);
	public ZonedDecimalData[] tdeduct = ZDArrayPartOfStructure(3, 12, 2, tdeducts, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(tdeducts, 0, FILLER_REDEFINE);
	public ZonedDecimalData tdeduct1 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 0);
	public ZonedDecimalData tdeduct2 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 12);
	public ZonedDecimalData tdeduct3 = new ZonedDecimalData(12, 2).isAPartOf(filler1, 24);
	public ZonedDecimalData amtlife = DD.amtlife.copyToZonedDecimal().isAPartOf(dataFields,151);
	public ZonedDecimalData amtyear = DD.amtyear.copyToZonedDecimal().isAPartOf(dataFields,161);
	public FixedLengthStringData benpln = DD.benpln.copy().isAPartOf(dataFields,171);
	public FixedLengthStringData cdesc = DD.cdesc.copy().isAPartOf(dataFields,173);
	public ZonedDecimalData copay = DD.copay.copyToZonedDecimal().isAPartOf(dataFields,193);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData diagcde = DD.diagcde.copy().isAPartOf(dataFields,200);
	public ZonedDecimalData dischdt = DD.dischdt.copyToZonedDecimal().isAPartOf(dataFields,205);
	public ZonedDecimalData gcadmdt = DD.gcadmdt.copyToZonedDecimal().isAPartOf(dataFields,213);
	public ZonedDecimalData gdeduct = DD.gdeduct.copyToZonedDecimal().isAPartOf(dataFields,221);
	public FixedLengthStringData givname = DD.givname.copy().isAPartOf(dataFields,231);
	public ZonedDecimalData incurdt = DD.incurdt.copyToZonedDecimal().isAPartOf(dataFields,251);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,259);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,267);
	public FixedLengthStringData shortdesc = DD.shortdesc.copy().isAPartOf(dataFields,275);
	public FixedLengthStringData zdoctor = DD.zdoctor.copy().isAPartOf(dataFields,285);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(dataFields,293);
	public ZonedDecimalData zunit = DD.zunit.copyToZonedDecimal().isAPartOf(dataFields,303);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 305);
	public FixedLengthStringData clamamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData descErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData taccamtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] taccamtErr = FLSArrayPartOfStructure(2, 4, taccamtsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(taccamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData taccamt1Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData taccamt2Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData tactexpErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tclmamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData tdeductsErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] tdeductErr = FLSArrayPartOfStructure(3, 4, tdeductsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(tdeductsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData tdeduct1Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData tdeduct2Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData tdeduct3Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData amtlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData amtyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData benplnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData cdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData copayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData diagcdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData dischdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData gcadmdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData gdeductErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData givnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData incurdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData shortdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData zdoctorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData zmedprvErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData zunitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(324).isAPartOf(dataArea, 413);
	public FixedLengthStringData[] clamamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] descOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData taccamtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] taccamtOut = FLSArrayPartOfStructure(2, 12, taccamtsOut, 0);
	public FixedLengthStringData[][] taccamtO = FLSDArrayPartOfArrayStructure(12, 1, taccamtOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(taccamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] taccamt1Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] taccamt2Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] tactexpOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tclmamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData tdeductsOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] tdeductOut = FLSArrayPartOfStructure(3, 12, tdeductsOut, 0);
	public FixedLengthStringData[][] tdeductO = FLSDArrayPartOfArrayStructure(12, 1, tdeductOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(36).isAPartOf(tdeductsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] tdeduct1Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] tdeduct2Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] tdeduct3Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] amtlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] amtyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] benplnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] cdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] copayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] diagcdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] dischdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] gcadmdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] gdeductOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] givnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] incurdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] shortdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] zdoctorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] zmedprvOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] zunitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(430);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(140).isAPartOf(subfileArea, 0);
	public ZonedDecimalData accday = DD.accday.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData actexp = DD.actexp.copyToZonedDecimal().isAPartOf(subfileFields,3);
	public FixedLengthStringData benefits = DD.benefits.copy().isAPartOf(subfileFields,15);
	public ZonedDecimalData benfamt = DD.benfamt.copyToZonedDecimal().isAPartOf(subfileFields,44);
	public ZonedDecimalData bnflmta = DD.bnflmta.copyToZonedDecimal().isAPartOf(subfileFields,51);
	public ZonedDecimalData bnflmtb = DD.bnflmtb.copyToZonedDecimal().isAPartOf(subfileFields,61);
	public ZonedDecimalData coverc = DD.coverc.copyToZonedDecimal().isAPartOf(subfileFields,71);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(subfileFields,73);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(subfileFields,81);
	public ZonedDecimalData gcnetpy = DD.gcnetpy.copyToZonedDecimal().isAPartOf(subfileFields,89);
	public FixedLengthStringData hosben = DD.hosben.copy().isAPartOf(subfileFields,101);
	public FixedLengthStringData limitc = DD.limitc.copy().isAPartOf(subfileFields,106);
	public ZonedDecimalData mbrcopay = DD.mbrcopay.copyToZonedDecimal().isAPartOf(subfileFields,110);
	public ZonedDecimalData mbrdeduc = DD.mbrdeduc.copyToZonedDecimal().isAPartOf(subfileFields,113);
	public ZonedDecimalData nofday = DD.nofday.copyToZonedDecimal().isAPartOf(subfileFields,123);
	public FixedLengthStringData sel = DD.sel.copy().isAPartOf(subfileFields,126);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(subfileFields,127);
	public ZonedDecimalData zdaycov = DD.zdaycov.copyToZonedDecimal().isAPartOf(subfileFields,137);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 140);
	public FixedLengthStringData accdayErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData actexpErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData benefitsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData benfamtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData bnflmtaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData bnflmtbErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData covercErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData gcnetpyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData hosbenErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData limitcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData mbrcopayErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData mbrdeducErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData nofdayErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData selErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData shortdsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 64);
	public FixedLengthStringData zdaycovErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 68);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(216).isAPartOf(subfileArea, 212);
	public FixedLengthStringData[] accdayOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] actexpOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] benefitsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] benfamtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] bnflmtaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] bnflmtbOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] covercOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] gcnetpyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] hosbenOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] limitcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] mbrcopayOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] mbrdeducOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] nofdayOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public FixedLengthStringData[] selOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 192);
	public FixedLengthStringData[] zdaycovOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 204);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 428);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData dischdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData gcadmdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData incurdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);

	public LongData Sr688screensflWritten = new LongData(0);
	public LongData Sr688screenctlWritten = new LongData(0);
	public LongData Sr688screenWritten = new LongData(0);
	public LongData Sr688protectWritten = new LongData(0);
	public GeneralTable sr688screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr688screensfl;
	}

	public Sr688ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actexpOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gcnetpyOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(incurdtOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gcadmdtOut,new String[] {"02","80","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dischdtOut,new String[] {"03","80","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(diagcdeOut,new String[] {"04","80","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdoctorOut,new String[] {"05","80","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmedprvOut,new String[] {"08","80","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taccamt1Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tactexpOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tclmamtOut,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taccamt2Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tdeduct1Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clamamtOut,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tdeduct2Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tdeduct3Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {mbrcopay, bnflmta, bnflmtb, mbrdeduc, benefits, crrcd, crtdate, coverc, sel, hosben, shortds, limitc, benfamt, accday, actexp, nofday, gcnetpy, zdaycov};
		screenSflOutFields = new BaseData[][] {mbrcopayOut, bnflmtaOut, bnflmtbOut, mbrdeducOut, benefitsOut, crrcdOut, crtdateOut, covercOut, selOut, hosbenOut, shortdsOut, limitcOut, benfamtOut, accdayOut, actexpOut, nofdayOut, gcnetpyOut, zdaycovOut};
		screenSflErrFields = new BaseData[] {mbrcopayErr, bnflmtaErr, bnflmtbErr, mbrdeducErr, benefitsErr, crrcdErr, crtdateErr, covercErr, selErr, hosbenErr, shortdsErr, limitcErr, benfamtErr, accdayErr, actexpErr, nofdayErr, gcnetpyErr, zdaycovErr};
		screenSflDateFields = new BaseData[] {crrcd, crtdate};
		screenSflDateErrFields = new BaseData[] {crrcdErr, crtdateErr};
		screenSflDateDispFields = new BaseData[] {crrcdDisp, crtdateDisp};

		screenFields = new BaseData[] {itmfrm, itmto, crtable, benpln, incurdt, gcadmdt, dischdt, diagcde, shortdesc, zdoctor, givname, zmedprv, cdesc, zunit, amtlife, amtyear, gdeduct, copay, tactexp, tclmamt, clamamt, desc};
		screenOutFields = new BaseData[][] {itmfrmOut, itmtoOut, crtableOut, benplnOut, incurdtOut, gcadmdtOut, dischdtOut, diagcdeOut, shortdescOut, zdoctorOut, givnameOut, zmedprvOut, cdescOut, zunitOut, amtlifeOut, amtyearOut, gdeductOut, copayOut, tactexpOut, tclmamtOut, clamamtOut, descOut};
		screenErrFields = new BaseData[] {itmfrmErr, itmtoErr, crtableErr, benplnErr, incurdtErr, gcadmdtErr, dischdtErr, diagcdeErr, shortdescErr, zdoctorErr, givnameErr, zmedprvErr, cdescErr, zunitErr, amtlifeErr, amtyearErr, gdeductErr, copayErr, tactexpErr, tclmamtErr, clamamtErr, descErr};
		screenDateFields = new BaseData[] {itmfrm, itmto, incurdt, gcadmdt, dischdt};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr, incurdtErr, gcadmdtErr, dischdtErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp, incurdtDisp, gcadmdtDisp, dischdtDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr688screen.class;
		screenSflRecord = Sr688screensfl.class;
		screenCtlRecord = Sr688screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr688protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr688screenctl.lrec.pageSubfile);
	}
}
