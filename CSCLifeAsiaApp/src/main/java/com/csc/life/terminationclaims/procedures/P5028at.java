/*
 * File: P5028at.java
 * Date: 29 August 2009 23:57:48
 * Author: Quipoz Limited
 *
 * Class transformed from P5028AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.List;

import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.IncrmjaTableDAM;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.reassurance.procedures.Trmreas;
import com.csc.life.reassurance.tablestructures.Trmreasrec;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.MatdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.MathclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.MatdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.MathpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Matdpf;
import com.csc.life.terminationclaims.dataaccess.model.Mathpf;
import com.csc.life.terminationclaims.recordstructures.Matpcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
* P5028AT - Maturity/Expiry AT Processing
* ---------------------------------------
*
* This program will be run under AT and will perform the general
* function  for  the  finalisation  of Maturity/Expiry  on  line
* processing.
*
* This program will carry out the following functions:
*
* 1.  Increment the TRANNO field on the contract Header.
*
* 1.1 Increment the TRANNO field on the PAYR.
*
* 2.  Set the status of the contract/components.
*
* 3.  Create  a LETC record that will be a claims letter to  the
*     client.
*
* 4.  Write a PTRN record to mark the transaction.
*
* 5.  Batch Header Update.
*
* 6.  Release Softlock.
*
* NOTE:
*
*     - where "NEW  SUMMARISED" is mentioned it implies the
*       value contained in the suffix field on the maturity
*       claim header record MATHCLM.
*
*     - where "OLD SUMMARISED" is mentioned it implies the
*       value contained in the suffix field on the
*       contract header record CHDRCLM.
*
*
* PROCESSING:
* -----------
*
* Contract Header.
* ----------------
*
* The ATMOD-PRIMARY-KEY  will contain  the  Company and Contract
* number of the contract being processed.  Use  these to perform
* a  READH on  CHDRMAT.  REWRT this record with  a Valid Flag '2'
* and Currto date set as effective date.
*
* Increment the TRANNO field by 1 and WRITR a new Contract Header
* record with  Valid Flag '1',  Currfrom date to be the effective
* date and Currto date set to all 9's.
*
*
* Get the Statuses.
* -----------------
*
* - obtain  the  status codes required by reading table T5679,
*   i.e. the 'Component Statuses For Transactions'. This table
*   is keyed by the transaction number from BATCKEY.
*
* Get today date from DATCON1 with a function 'TDAY'.
*
* Check  the  return status  codes  for  each  of  the following
* records being updated.
*
* Read  the  Maturity  Claim Header  record  (MATHCLM)  for this
* contract.
*
* Read  the  Contract  Header  "RETRV"  in  orde r to obtain the
* TRANNO.
*
*
* PROCESS COMPONENTS
* ------------------
*
* Process all the MATHCLM records.
*
* If the  MATHCLM-PLAN-SUFFIX = '0000', then for all the COVRMAT
* records  present,  keyed; CHDRCOY, CHDRNUM, PLAN-SUFFIX, LIFE,
* COVERAGE and RIDER:-
*
* - set the  Valid Flag  to  '2', COVRMAT-CURRTO to the Maturity
*   date and update the record.
*
* - write a new record as follows:
*
* - update  the  transaction  number with the transaction number
*   from the contract header  and  update  the  transaction date
*   with the Maturity effective date (default to  today's date).
*   Update the  status  code  with the  status code  from T5679.
*   Use the regular premium status code if the billing frequency
*   from the contract header,  is equal to '00'  and  the single
*   premium  indicator  on  table  T5687  (Keyed; Coverage/Rider
*   table) is not = 'Y', otherwise use the single premium status
*   from table T5679.
*
* If  the  MATHCLM-PLAN-SUFFIX  NOT  = '0000',  then for all the
* COVRMAT records present, (keyed; CHDRCOY, CHDRNUM, PLAN-SUFFIX,
* LIFE, COVERAGE and RIDER):
*
* If  the  components  selected is  a summary and the total "new
* summarised" is less than the total  "old Summarised" the break
* the COVR records by calling subroutine BRKOUT.
*
* If the policy is not a summary and matured:-
*
* - set  the  Valid Flag to '2',  set COVRMAT-CURRTO date to the
*   Maturity Effective date and update the record.
*
* - write a new record as follows:
*
* - update  the transaction  number with  the transaction number
*   from the contract header and update the transaction date with
*   the Maturity Effective date (default  is today date). Update
*   the status code with the  status code  from table T5679. Use
*   the  regular  premium status code if the  billing frequency,
*   from the contract header, is equal to '00'  and  the  single
*   premium indicator on  table T5679 is not 'Y',  otherwise use
*   the single-premium status from table T5679. T5687 is a dated
*   table and access to it is by coverage/rider code.
*
*
* PROCESS MATURITY CLAIM DETAIL RECORDS (MATDCLM)
* -----------------------------------------------
*
* Read all the MATDCLM records for this contract.
*
* Access  table T5687,  the Coverage/Rider  table,  and  for the
* Maturity method found  and read  the table  T6598 behind  this
* method and access the 'Maturity Processing' subroutine.
*
* DOWHILE
*
*     there are MATDCLM records for this contract/life call
*     the "Maturity Processing Subroutine (T5687/T6598).
*
* ENDDO
*
* Linkage  area passed  to the Maturity Processing subroutine is
* MATPCPY copybook.
*
*
* PROCESS LIVES
* -------------
*
* Only  update  the  contract  header  if the  contract is fully
* matured.
*
* Read  all  the  lives being processed  (LIFE) for the contract
* and update the life details as follows:
*
* - set Valid Flag  to  '2';  set 'CURRTO'  date to the Maturity
*   Effective date and update the record.
*
* - write a new record as follows:
*
* - if the  number  summarised  is equal to zero, then alter the
*   status as follows:
*
* - update  the transaction  number with  the transaction number
*   from  the contract  header  and update the  transaction date
*   with  the Maturity Effective date (default is today's date).
*   Update  the status  code with  the status code,  from  table
*   T5679,  for the  life or joint-life,  whichever is currently
*   being  processed.  (LIFE-NUMBER =  spaces or '00' for single
*   life, equal to '01' for joint life case).
*
*
* PROCESS CONTRACT HEADER
* ------------------------
*
* Read  the  contract  header  (CHDRLIF)  for  the  contract and
* update as follows:
*
* - if the Claim Header number summarized is equal to zero, then
*   alter the status as follows:
*
* - alter the  premium status if the status field on table T5679
*   for regular  or  single  premium have entries.  Only do this
*   however,  when no in force components exist.  This is not to
*   be hard  coded, rather check that all components don't match
*   the item entries  that were  used to  enter the transaction.
*   i.e. table T5679 is  set up stating that the components must
*   be  'IF' to process.  It is only when  all do not match this
*   that you can set the contract status. If entries are present,
*   then check the Billing Frequency (BILLFREQ)  on the contract
*   header. If the BILLFREQ is '00', this implies single premium,
*   otherwise use the regular premium status field.
*
* Create a Claims Letter
* ----------------------
*
* Create  a  LETC record  for the  contract you  are maturing by
* calling 'LETRQST' with a function of  'ADD' and  the following
* parameters:
*
* - REQUEST-COMPANY      -  CHDRCOY
* - LETTER-TYPE          -  T6634-LETTER-TYPE
* - LETTER-REQUEST-DATE  -  Effective Date entered in submenu
* - CLNTCOY              -  COWNCOY
* - CLNTNUM              -  COWNNUM
* - OTHER-KEYS           -  CHDRNUM
*
* For  the component you are processing concatenate the Contract
* Type   (CHDR-CNTTYPE)  from  the  contract  header  with   the
* Transaction  Code  for the  batch job to set up the key into a
* new table T6634.
*
* A a  new  letter "MATCLAIM"  on tables T3609 and T6634 will be
* necessary.
*
* PTRN Record.
* ------------
*
* Write a PTRN record with the new TRANNO.
*
*  . PTRN-BATCTRCDE         - Transaction Code
*  . PTRN-CHDRCOY           - CHDRMAT-CHDRCOY
*  . PTRN-CHDRNUM           - CHDRMAT-CHDRNUM
*  . PTRN-TRANNO            - CHDRMAT-TRANNO
*  . PTRN-TRANSACTION-DATE  - Current Date
*  . PTRN-TRANSACTION-TIME  - Current Time
*  . PTRN-PTRNEFF           - Current Date
*  . PTRN-TERMID            - Passed in Linkage
*  . PTRN-USER              - Passed in Linkage.
*
* Batch Header
* ------------
*
* Update the batch header by calling BATCUP with a function of
* WRITS and the following parameters:
*
* - transaction count equals to 1
* - all other amounts zero
* - batch key from AT linkage.
*
*
* Release Contract SFTLOCK
* ------------------------
*
* Release the soft lock on the contract by calling 'SFTLOCK' with
* a function of 'UNLK'.
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through it's life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as it's item name, the transaction code and
* the contract status of a contract, eg. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, eg AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to Covrsta records, and
* current categories refer to Covrsts records. Agcmsts
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the Agcmsts, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
*
*
*****************************************************************
* </pre>
*/
public class P5028at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5028AT");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaFullyMatured = new FixedLengthStringData(1).init(SPACES);
	private Validator fullyMatured = new Validator(wsaaFullyMatured, "Y");

	private FixedLengthStringData wsaaOkey = new FixedLengthStringData(29);
	private PackedDecimalData wsaaOkey1 = new PackedDecimalData(4, 0).isAPartOf(wsaaOkey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(209);
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 0);
	private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 14);
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 20);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 26);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 30);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);

	private FixedLengthStringData wsaaMaturePlan = new FixedLengthStringData(1).init("Y");
	private Validator matureWholePlan = new Validator(wsaaMaturePlan, "Y");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(ZERO);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaInstpremTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private PackedDecimalData wsaaChdrlifTranno = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaEligibleDate = new ZonedDecimalData(8, 0).setUnsigned();
	private String wsaaValidStatus = "";
		/* ERRORS */
	private static final String e652 = "E652";
	private static final String t5534 = "T5534";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t6598 = "T6598";
	private static final String tr384 = "TR384";
	private static final String t1693 = "T1693";
	private static final String t7508 = "T7508";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrmatTableDAM covrmatIO = new CovrmatTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private IncrTableDAM incrIO = new IncrTableDAM();
	private IncrmjaTableDAM incrmjaIO = new IncrmjaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private MatdclmTableDAM matdclmIO = new MatdclmTableDAM();
	private MathclmTableDAM mathclmIO = new MathclmTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private T5534rec t5534rec = new T5534rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T1693rec t1693rec = new T1693rec();
	private T7508rec t7508rec = new T7508rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Matpcpy matpcpy = new Matpcpy();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Trmreasrec trmreasrec = new Trmreasrec();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private List<Covrpf> covrmatList = new ArrayList<Covrpf>();
	private List<Mathpf> mathclmList = new ArrayList<Mathpf>();
	private List<Matdpf> matdclmList = new ArrayList<Matdpf>();
	private List<Covrpf> covrpfUpdateList = new ArrayList<Covrpf>();
	private List<Covrpf> covrpfInsertList = new ArrayList<Covrpf>();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private MathpfDAO mathpfDAO = getApplicationContext().getBean("mathpfDAO", MathpfDAO.class);
	private MatdpfDAO matdpfDAO = getApplicationContext().getBean("matdpfDAO", MatdpfDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		continue2104,
		readNextCovrmat2105,
		loopRisk2202,
		initialPrem2205,
		loopPrem2207,
		exit2209,
		continue2802,
		readNextMatdclm3104,
		exit3109,
		readNextLife4104,
		updateInstprem4501,
		chkStatusCode4502,
		rewriteChdrlifPayr4504,
		exit4529,
		loop4532,
		exit5009
	}

	public P5028at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainlineRountine0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainlineRountine0000()
	{
		initialize0000();
		exit0009();
	}

protected void initialize0000()
	{
		initialisation1000();
		/* Continue processing until contract break.*/
		for(Mathpf math : mathclmList) {
			if (isEQ(math.getPlnsfx(), ZERO)) {
				wsaaMaturePlan.set("Y");
				matureWholeContract2000(math);
			}
			else {
				wsaaMaturePlan.set("N");
				matureSelectedPolicies2500(math);
			}
			/* PERFORM 3000-PROCESS-MATDCLMS.*/
			updateContractHeader4500();
			if (fullyMatured.isTrue()) {
				processLives4000(math);
			}
			writeLetcRecord5000(math);
		}
		writePtrnTransaction6000();
		updateBatchHeader7000();
		dryProcessing10000();
		releaseSoftlock8000();
		a000Statistics();
	}

protected void exit0009()
	{
		exitProgram();
	}

	/**
	* <pre>
	* This section performs the following tasks:
	*     1.  Increment the transaction number (TRANNO) in the
	*         Contract Header by 1.
	*     2.  Get the status of the contract/components
	*     3.  Get Today date from DATCON1.
	*     4.  Get the first MATHCLM record for this run.
	* </pre>
	*/
protected void initialisation1000()
	{
		initialise1000();
	}

protected void initialise1000()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		wsaaInstpremTot.set(ZERO);
		wsaaFullyMatured.set("Y");
		datcon2rec.intDate1.set(wsaaEffectiveDate);
		datcon2rec.frequency.set("12");
		datcon2rec.freqFactor.set(3);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaEligibleDate.set(datcon2rec.intDate2);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(atmodrec.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			systemError9000();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		updateContractHeader1050();
		getContractStatus1100();
		getTodayDate1150();
		getFirstMathclm1200();
		pendingIncrease1300();
	}

	/**
	* <pre>
	* This section process the contract header record. The ATMOD-
	* PRIMARY-KEY will contain the Company and Contract number of
	* the contract being processed. Use these to perform a READH
	* on CHDRLIF. REWRT this record with a Valid Flag '2' and the
	* CURRTO date set as effective date.
	* Increment the TRANNNO field by 1 and WRITR a new Contract
	* Header record with Valid Flag '1', CURRFROM to be the
	* effective date and CURRTO set to all 9's.
	* </pre>
	*/
protected void updateContractHeader1050()
	{
		readContract1050();
		updatePayr1055();
	}

protected void readContract1050()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError9100();
		}
		compute(wsaaChdrlifTranno, 0).set(add(chdrlifIO.getTranno(), 1));
		chdrlifIO.setValidflag("2");
		/*  COMPUTE CHDRLIF-CURRTO      = WSAA-EFFECTIVE-DATE - 1.       */
		chdrlifIO.setCurrto(wsaaEffectiveDate);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError9100();
		}
		chdrlifIO.setTranno(wsaaChdrlifTranno);
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(wsaaEffectiveDate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError9100();
		}
	}

	/**
	* <pre>
	* Do a READH on PAYR and do a REWRT on this record with a
	* Valid Flag '2' and the EFFDATE set to effective date.
	* WRITR  a new PAYR record with the TRANNO from above and with
	* Valid Flag '1', EFFDATE to be the effective date.
	* </pre>
	*/
protected void updatePayr1055()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError9100();
		}
		payrIO.setValidflag("2");
		/* MOVE WSAA-EFFECTIVE-DATE    TO PAYR-EFFDATE.                 */
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError9100();
		}
		payrIO.setTranno(wsaaChdrlifTranno);
		payrIO.setValidflag("1");
		payrIO.setEffdate(wsaaEffectiveDate);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError9100();
		}
	}

	/**
	* <pre>
	* Read T5679 to get the status codes for the contract type.
	* </pre>
	*/
protected void getContractStatus1100()
	{
		readTableT56791100();
	}

protected void readTableT56791100()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	* Get Today date by Calling DATCON1 subroutine.
	* </pre>
	*/
protected void getTodayDate1150()
	{
		/*CALL-DATCON1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT1*/
	}

	/**
	* <pre>
	* Read the first policy for this contract from MATHCLM
	* </pre>
	*/
protected void getFirstMathclm1200()
	{
		begnMathclm1200();
	}

protected void begnMathclm1200()
	{
		mathclmList = mathpfDAO.getmathclmRecordList(atmodrec.company.toString(), wsaaPrimaryChdrnum.toString(), wsaaChdrlifTranno.toInt());
		if(mathclmList.isEmpty()) {
			return;
		}
	}

protected void pendingIncrease1300()
	{
		/*BEGNH*/
		/* Check for any pending Increase records (Validflag = '1')     */
		/* for the contract and delete them.                            */
		incrmjaIO.setParams(SPACES);
		incrmjaIO.setChdrcoy(atmodrec.company);
		incrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		incrmjaIO.setLife(ZERO);
		incrmjaIO.setCoverage(ZERO);
		incrmjaIO.setRider(ZERO);
		incrmjaIO.setPlanSuffix(ZERO);
		incrmjaIO.setFunction(varcom.begnh);
		incrmjaIO.setFormat(formatsInner.incrmjarec);
		while ( !(isEQ(incrmjaIO.getStatuz(), varcom.endp))) {
			deletePendingIncrs1350();
		}

		/*EXIT*/
	}

protected void deletePendingIncrs1350()
	{
		delete1360();
	}

protected void delete1360()
	{
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)
		&& isNE(incrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(incrmjaIO.getStatuz());
			syserrrec.params.set(incrmjaIO.getParams());
			dbError9100();
		}
		/* If the end of the file has been reached, leave the section.  */
		if (isEQ(incrmjaIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* If the record retrieved is not for the correct contract,     */
		/* rewrite the record to release it, move ENDP to the file      */
		/* status and leave the section.                                */
		if (isNE(incrmjaIO.getChdrcoy(), atmodrec.company)
		|| isNE(incrmjaIO.getChdrnum(), wsaaPrimaryChdrnum)) {
			incrmjaIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, incrmjaIO);
			if (isNE(incrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(incrmjaIO.getStatuz());
				syserrrec.params.set(incrmjaIO.getParams());
				dbError9100();
			}
			incrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		/* Update the COVR record for this pending increase, setting    */
		/* the indexation indication to spaces to force Pending         */
		/* Increases to reprocess the component.                        */
		resetCovrIndexation1400();
		/* Delete the record then move NEXTR to the function to look    */
		/* for the next record.                                         */
		incrmjaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrmjaIO);
		if (isNE(incrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(incrmjaIO.getStatuz());
			syserrrec.params.set(incrmjaIO.getParams());
			dbError9100();
		}
		incrmjaIO.setFunction(varcom.nextr);
	}

protected void resetCovrIndexation1400()
	{
		readh1410();
	}

protected void readh1410()
	{
		covrmatIO.setParams(SPACES);
		covrmatIO.setChdrcoy(incrmjaIO.getChdrcoy());
		covrmatIO.setChdrnum(incrmjaIO.getChdrnum());
		covrmatIO.setLife(incrmjaIO.getLife());
		covrmatIO.setCoverage(incrmjaIO.getCoverage());
		covrmatIO.setRider(incrmjaIO.getRider());
		covrmatIO.setPlanSuffix(incrmjaIO.getPlanSuffix());
		covrmatIO.setFunction(varcom.readh);
		covrmatIO.setFormat(formatsInner.covrmatrec);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covrmatIO.getStatuz());
			syserrrec.params.set(covrmatIO.getParams());
			dbError9100();
		}
		covrmatIO.setIndexationInd(SPACES);
		covrmatIO.setFunction(varcom.rewrt);
		covrmatIO.setFormat(formatsInner.covrmatrec);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covrmatIO.getStatuz());
			syserrrec.params.set(covrmatIO.getParams());
			dbError9100();
		}
	}

	/**
	* <pre>
	* Read T5687 to get the single premium indicator.
	* </pre>
	*/
protected void readTableT56871500()
	{
		readT56871500();
	}

protected void readT56871500()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(wsaaEffectiveDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(), atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), wsaaCrtable)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaCrtable);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e652);
			dbError9100();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	}

protected void readTableT65981600()
	{
		readT65981600();
	}

protected void readT65981600()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.maturityCalcMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}

protected void matureWholeContract2000(Mathpf math)
	{
		processCovrmat2000();
		processRestCovrmat2002(math);
	}

protected void processCovrmat2000()
	{
		covrmatList = covrpfDAO.getcovrmatRecordsForMature(atmodrec.company.toString(), chdrlifIO.getChdrnum().toString());	
		if (covrmatList.isEmpty()) {
			return;
		}
	}

protected void processRestCovrmat2002(Mathpf math)
	{
		for(Covrpf covr : covrmatList) {
			updateCovrmats2100(covr);
		}
		processMatdclms3000(math);
		/*EXIT*/
	}

protected void updateCovrmats2100(Covrpf covr)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					checkCoverage2100(covr);
					inactiveCovrmatRec2102(covr);
				case continue2104:
					continue2104(covr);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkCoverage2100(Covrpf covr)
	{
		if (isLT(wsaaEligibleDate, covr.getRiskCessDate())) {
			wsaaFullyMatured.set("N");
			return;
		}
		wsaaValidStatus = "N";
		chkComponentsStatii2200(covr);
		if (isEQ(wsaaValidStatus, "N")) {
			return;
		}
		processReassurance2900(covr);
	}

protected void inactiveCovrmatRec2102(Covrpf covr)
	{
		/*  Update the COVRMAT record with a valid flag of 2 and set*/
		/*  'COVRMAT-CURRTO' date to the maturity effective date.*/
		Covrpf covrpfupdate = new Covrpf(covr);
		covrpfupdate.setValidflag("2");
		covrpfupdate.setCurrto(wsaaEffectiveDate.toInt());
		covrpfUpdateList.add(covrpfupdate);
		covrpfDAO.updateCovrBulk(covrpfUpdateList);
		covrpfUpdateList.clear();
		/* If a historical increase record exists, write a new record   */
		/* for the transaction with updated status codes.               */
		incrCheck2900(covr);
		/*          Write a new COVRMAT record.*/
		/*  Update the transaction number with the transaction the*/
		/*  contract header and transaction date with the maturity*/
		/*  effective date (default is today's date).*/
		wsaaCrtable.set(covr.getCrtable());
		readTableT56871500();
		/*  Accumulate the matured installment premiums which will be*/
		/*  minus from the Contract Header in section 4500-....*/
		/*  Do not deduct the premium if the premium cessation date <*/
		/*  risk cessation date or if it's a rider and billing method*/
		/*  is benefit billing (ie. U/Linked where premiums are by unit*/
		/*  surrender).*/
		if (isLT(covr.getPremCessDate(), covr.getRiskCessDate())) {
			goTo(GotoLabel.continue2104);
		}
		if ((isNE(covr.getRider(), "00")
		&& isNE(covr.getRider(), "  "))
		&& isNE(t5687rec.bbmeth, SPACES)) {
			readTableT55342150();
			if (isNE(t5534rec.unitFreq, SPACES)
			&& isNE(t5534rec.subprog, SPACES)
			&& isNE(t5534rec.adfeemth, SPACES)) {
				goTo(GotoLabel.continue2104);
			}
		}
		wsaaInstpremTot.add(covr.getInstprem().doubleValue());
	}

protected void continue2104(Covrpf covr)
	{
		Covrpf covrInsertRecord = new Covrpf(covr);
		covrInsertRecord.setTranno(wsaaChdrlifTranno.toInt());
		covrInsertRecord.setCurrfrom(wsaaEffectiveDate.toInt());
		/*  Update the status code with status code from T5679. Use*/
		/*  the regular premium status code if the billing frequency,*/
		/*  from the contract header, is equal to '00' and the single*/
		/*  premium indicator on table T5687 is not 'Y', otherwise use*/
		/*  the single-premium status from T5679.*/
		/* IF CHDRLIF-QILLFREQ         = '00'                           */
		/* AND T5687-SINGLE-PREM-IND   NOT = 'Y'                        */
		/*     IF T5679-SET-COV-PREM-STAT NOT = SPACES                  */
		/*         MOVE T5679-SET-COV-PREM-STAT                         */
		/*                             TO COVRMAT-PSTATCODE             */
		/*     ELSE                                                     */
		/*         NEXT SENTENCE                                        */
		/* ELSE                                                         */
		/*     IF T5679-SET-SNGP-COV-STAT NOT = SPACES                  */
		/*         MOVE T5679-SET-SNGP-COV-STAT                         */
		/*                             TO COVRMAT-PSTATCODE.            */
		/* IF CHDRLIF-BILLFREQ                     = '00'         <A0569*/
		if (isEQ(payrIO.getBillfreq(), "00")) {
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				if (isEQ(covr.getRider(), "00")) {
					if (isNE(t5679rec.setSngpCovStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setSngpCovStat.toString());
					}
				}
				else {
					if (isNE(t5679rec.setSngpRidStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setSngpRidStat.toString());
					}
				}
			}
		}
		/* IF CHDRLIF-BILLFREQ                 NOT = '00'         <A0569*/
		if (isNE(payrIO.getBillfreq(), "00")) {
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				if (isEQ(covr.getRider(), "00")) {
					if (isNE(t5679rec.setSngpCovStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setSngpCovStat.toString());
					}
				}
				else {
					if (isNE(t5679rec.setSngpRidStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setSngpRidStat.toString());
					}
				}
			}
			else {
				if (isEQ(covr.getRider(), "00")) {
					if (isNE(t5679rec.setCovPremStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setCovPremStat.toString());
					}
				}
				else {
					if (isNE(t5679rec.setRidPremStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setRidPremStat.toString());
					}
				}
			}
		}
		if (isNE(t5679rec.setCovRiskStat, SPACES)) {
			covrInsertRecord.setStatcode(t5679rec.setCovRiskStat.toString());
		}
		covrInsertRecord.setValidflag("1");
		covrInsertRecord.setCurrto(varcom.vrcmMaxDate.toInt());
		covrInsertRecord.setPayrseqno(1);
		covrInsertRecord.setTransactionDate(wsaaTransactionDate.toInt());
		covrInsertRecord.setTransactionTime(wsaaTransactionTime.toInt());
		covrInsertRecord.setUser(wsaaUser.toInt());
		covrInsertRecord.setTermid(wsaaTermid.toString());
		covrpfInsertList.add(covrInsertRecord);
		covrpfDAO.insertCovrBulk(covrpfInsertList);
		covrpfInsertList.clear();
	}

protected void readNextCovrmat2105()
	{
		covrmatIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(), varcom.oK)
		&& isNE(covrmatIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmatIO.getParams());
			dbError9100();
		}
		if (isNE(covrmatIO.getChdrcoy(), atmodrec.company)
		|| isNE(covrmatIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isNE(covrmatIO.getPlanSuffix(), ZERO)
		|| isEQ(covrmatIO.getStatuz(), varcom.endp)) {
			covrmatIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void readTableT55342150()
	{
		readT55342150();
	}

protected void readT55342150()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5534);
		itemIO.setItemitem(t5687rec.bbmeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5534rec.t5534Rec.set(SPACES);
		}
		else {
			t5534rec.t5534Rec.set(itemIO.getGenarea());
		}
	}

protected void chkComponentsStatii2200(Covrpf covr)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialRisk2200();
				case loopRisk2202:
					loopRisk2202(covr);
				case initialPrem2205:
					initialPrem2205();
				case loopPrem2207:
					loopPrem2207(covr);
				case exit2209:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialRisk2200()
	{
		wsaaSub1.set(ZERO);
	}

protected void loopRisk2202(Covrpf covr)
	{
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, 12)) {
			goTo(GotoLabel.exit2209);
		}
		if (isEQ(covr.getStatcode(), t5679rec.covRiskStat[wsaaSub1.toInt()])) {
			goTo(GotoLabel.initialPrem2205);
		}
		goTo(GotoLabel.loopRisk2202);
	}

protected void initialPrem2205()
	{
		wsaaSub1.set(ZERO);
	}

protected void loopPrem2207(Covrpf covr)
	{
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, 12)) {
			return ;
		}
		if (isEQ(covr.getPstatcode(), t5679rec.covPremStat[wsaaSub1.toInt()])) {
			wsaaValidStatus = "Y";
		}
		goTo(GotoLabel.loopPrem2207);
	}

protected void matureSelectedPolicies2500(Mathpf math)
	{
		/*CALL-BREAKOUT*/
		callBreakout2600();
		getFirstMathclm1200();
		for(Mathpf mathpf : mathclmList) {
			processMathclm2700(mathpf);
		}

		/*EXIT*/
	}

	/**
	* <pre>
	* Call the BRKOUT subroutine to breakout COVR and AGCM.
	* Break out the contract from the smallest plan suffix which is
	* the first in that transaction number group.
	* </pre>
	*/
protected void callBreakout2600()
	{
		readMathclm2600();
		readContinue2602();
		callBreakout2604();
	}

protected void readMathclm2600()
	{
		mathclmList = mathpfDAO.getmathclmRecordList(atmodrec.company.toString(), wsaaPrimaryChdrnum.toString(), wsaaChdrlifTranno.toInt());
	}

protected void readContinue2602()
	{	//performance improvement --  atiwari23
		if (mathclmList.isEmpty()) {
			dbError9100();
		}
	}

protected void callBreakout2604()
	{
		/*  call the breakout routine in order to breakout the COVR*/
		/*  and the AGCM records - this is done only once for the*/
		/*  first record read from the maturity header file*/
		for(Mathpf math : mathclmList) {
			if (isGT(math.getPlnsfx(), chdrlifIO.getPolsum())
			|| isEQ(chdrlifIO.getPolsum(), 1)
			|| isEQ(math.getPlnsfx(), 0)) {
				return ;
			}
			brkoutrec.brkOldSummary.set(chdrlifIO.getPolsum());
			compute(brkoutrec.brkNewSummary, 0).set(sub(math.getPlnsfx(), 1));
			brkoutrec.brkChdrnum.set(chdrlifIO.getChdrnum());
			brkoutrec.brkChdrcoy.set(chdrlifIO.getChdrcoy());
			brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
			brkoutrec.brkStatuz.set(SPACES);
			callProgram(Brkout.class, brkoutrec.outRec);
			if (isNE(brkoutrec.brkStatuz, varcom.oK)) {
				syserrrec.params.set(brkoutrec.outRec);
				syserrrec.statuz.set(brkoutrec.brkStatuz);
				systemError9000();
			}
		}
	}

protected void processMathclm2700(Mathpf mathpf)
	{
		processCovrmat2700(mathpf);
		processSameSuffix2702(mathpf);
	}

protected void processCovrmat2700(Mathpf mathpf)
	{
		covrmatList = covrpfDAO.SearchRecordsforMatd(atmodrec.company.toString(), chdrlifIO.getChdrnum().toString(), mathpf.getPlnsfx());	
		if (covrmatList.isEmpty()) {
			return;
		}
	}

protected void processSameSuffix2702(Mathpf math)
	{
		for(Covrpf covr : covrmatList) {
			updateCovrmats2800(covr);
		}

		processMatdclms3000(math);
	}

protected void updateCovrmats2800(Covrpf covr)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateCovrmatRec2800(covr);
				case continue2802:
					continue2802(covr);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*  Update the COVRMAT record with a valid flag of 2 and set
	*  'COVRMAT-CURRTO' date to the maturity effective date.
	* </pre>
	*/
protected void updateCovrmatRec2800(Covrpf covr)
	{
		processReassurance2900(covr);
		Covrpf covrpfupdate = new Covrpf(covr);
		covrpfupdate.setValidflag("2");
		covrpfupdate.setCurrto(wsaaEffectiveDate.toInt());
		covrpfUpdateList.add(covrpfupdate);
		covrpfDAO.updateCovrBulk(covrpfUpdateList);
		covrpfUpdateList.clear();
		/* If a historical increase record exists, write a new record   */
		/* for the transaction with updated status codes.               */
		incrCheck2900(covr);
		/*          Write a new COVRMAT record.*/
		/*  Update the transaction number with the transaction the*/
		/*  contract header and transaction date with the maturity*/
		/*  effective date (default is today's date).*/
		wsaaCrtable.set(covr.getCrtable());
		readTableT56871500();
		/*  Accumulate the premiums of the policies to be matured. So*/
		/*  that we can deduct it from the installment premium on the*/
		/*  contract header record in section 4300-.*/
		/*  Do not deduct the premium if the premium cessation date <*/
		/*  risk cessation date or if it's a rider and billing method*/
		/*  is benefit billing (ie. U/Linked where premiums are by unit*/
		/*  surrender).*/
		if (isLT(covr.getPremCessDate(), covr.getRiskCessDate())) {
			goTo(GotoLabel.continue2802);
		}
		if ((isNE(covr.getRider(), "00")
		&& isNE(covr.getRider(), "  "))
		&& isNE(t5687rec.bbmeth, SPACES)) {
			readTableT55342150();
			if (isNE(t5534rec.unitFreq, SPACES)
			&& isNE(t5534rec.subprog, SPACES)
			&& isNE(t5534rec.adfeemth, SPACES)) {
				goTo(GotoLabel.continue2802);
			}
		}
		wsaaInstpremTot.add(covr.getInstprem().doubleValue());
	}

protected void continue2802(Covrpf covr)
	{
		Covrpf covrInsertRecord = new Covrpf(covr);
		covrInsertRecord.setTranno(wsaaChdrlifTranno.toInt());
		covrInsertRecord.setCurrfrom(wsaaEffectiveDate.toInt());
		/*  Update the status code with status code from T5679. Use*/
		/*  the regular premium status code if the billing frequency,*/
		/*  from the contract header, is equal to '00' and the single*/
		/*  premium indicator on table T5687 is not 'Y', otherwise use*/
		/*  the single-premium status from T5679.*/
		/* IF CHDRLIF-BILLFREQ         = '00'                           */
		/* AND T5687-SINGLE-PREM-IND   NOT = 'Y'                        */
		/*     IF T5679-SET-COV-PREM-STAT NOT = SPACES                  */
		/*         MOVE T5679-SET-COV-PREM-STAT                         */
		/*                             TO COVRMAT-PSTATCODE             */
		/*     ELSE                                                     */
		/*         NEXT SENTENCE                                        */
		/* ELSE                                                         */
		/*     IF T5679-SET-SNGP-COV-STAT NOT = SPACES                  */
		/*         MOVE T5679-SET-SNGP-COV-STAT                         */
		/*                             TO COVRMAT-PSTATCODE.            */
		/* IF CHDRLIF-BILLFREQ                     = '00'         <A0569*/
		if (isEQ(payrIO.getBillfreq(), "00")) {
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				if (isEQ(covr.getRider(), "00")) {
					if (isNE(t5679rec.setSngpCovStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setSngpCovStat.toString());
					}
				}
				else {
					if (isNE(t5679rec.setSngpRidStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setSngpRidStat.toString());
					}
				}
			}
		}
		/*  IF CHDRLIF-BILLFREQ                 NOT = '00'         <A0569*/
		if (isNE(payrIO.getBillfreq(), "00")) {
			if (isEQ(t5687rec.singlePremInd, "Y")) {
				if (isEQ(covr.getRider(), "00")) {
					if (isNE(t5679rec.setSngpCovStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setSngpCovStat.toString());
					}
				}
				else {
					if (isNE(t5679rec.setSngpRidStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setSngpRidStat.toString());
					}
				}
			}
			else {
				if (isEQ(covr.getRider(), "00")) {
					if (isNE(t5679rec.setCovPremStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setCovPremStat.toString());
					}
				}
				else {
					if (isNE(t5679rec.setRidPremStat, SPACES)) {
						covrInsertRecord.setPstatcode(t5679rec.setRidPremStat.toString());
					}
				}
			}
		}
		if (isNE(t5679rec.setCovRiskStat, SPACES)) {
			covrInsertRecord.setStatcode(t5679rec.setCovRiskStat.toString());
		}
		covrInsertRecord.setValidflag("1");
		covrInsertRecord.setCurrto(varcom.vrcmMaxDate.toInt());
		covrInsertRecord.setPayrseqno(1);
		covrInsertRecord.setTransactionDate(wsaaTransactionDate.toInt());
		covrInsertRecord.setTransactionTime(wsaaTransactionTime.toInt());
		covrInsertRecord.setUser(wsaaUser.toInt());
		covrInsertRecord.setTermid(wsaaTermid.toString());
		covrpfInsertList.add(covrInsertRecord);
		covrpfDAO.insertCovrBulk(covrpfInsertList);
		covrpfInsertList.clear();
	}

protected void incrCheck2900(Covrpf covr)
	{
		para2910(covr);
	}

protected void para2910(Covrpf covr)
	{
		incrIO.setDataArea(SPACES);
		incrIO.setChdrcoy(covr.getChdrcoy());
		incrIO.setChdrnum(covr.getChdrnum());
		incrIO.setLife(covr.getLife());
		incrIO.setCoverage(covr.getCoverage());
		incrIO.setRider(covr.getRider());
		incrIO.setPlanSuffix(covr.getPlanSuffix());
		incrIO.setFunction(varcom.readr);
		incrIO.setFormat(formatsInner.incrrec);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), varcom.oK)
		&& isNE(incrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(incrIO.getStatuz());
			syserrrec.params.set(incrIO.getParams());
			dbError9100();
		}
		if (isEQ(incrIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		incrIO.setValidflag("2");
		if (isNE(t5679rec.setCovPremStat, SPACES)) {
			incrIO.setPstatcode(t5679rec.setCovPremStat);
		}
		if (isNE(t5679rec.setCovRiskStat, SPACES)) {
			incrIO.setStatcode(t5679rec.setCovRiskStat);
		}
		incrIO.setTransactionDate(wsaaTransactionDate);
		incrIO.setTransactionTime(wsaaTransactionTime);
		incrIO.setUser(wsaaUser);
		incrIO.setTermid(wsaaTermid);
		incrIO.setTranno(wsaaChdrlifTranno);
		incrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(incrIO.getStatuz());
			syserrrec.params.set(incrIO.getParams());
			dbError9100();
		}
	}

protected void processReassurance2900(Covrpf covr)
	{
		trmreas2901(covr);
	}

protected void trmreas2901(Covrpf covr)
	{
		trmreasrec.trracalRec.set(SPACES);
		trmreasrec.statuz.set(varcom.oK);
		trmreasrec.function.set("TRMR");
		trmreasrec.chdrcoy.set(covr.getChdrcoy());
		trmreasrec.chdrnum.set(covr.getChdrnum());
		trmreasrec.life.set(covr.getLife());
		trmreasrec.coverage.set(covr.getCoverage());
		trmreasrec.rider.set(covr.getRider());
		trmreasrec.planSuffix.set(covr.getPlanSuffix());
		trmreasrec.crtable.set(covr.getCrtable());
		trmreasrec.cnttype.set(chdrlifIO.getCnttype());
		trmreasrec.polsum.set(chdrlifIO.getPolsum());
		trmreasrec.effdate.set(wsaaEffectiveDate);
		trmreasrec.batckey.set(wsaaBatckey);
		trmreasrec.tranno.set(chdrlifIO.getTranno());
		trmreasrec.language.set(atmodrec.language);
		trmreasrec.billfreq.set(chdrlifIO.getBillfreq());
		trmreasrec.ptdate.set(chdrlifIO.getPtdate());
		trmreasrec.origcurr.set(chdrlifIO.getCntcurr());
		trmreasrec.acctcurr.set(covr.getPremCurrency());
		trmreasrec.crrcd.set(covr.getCrrcd());
		trmreasrec.convUnits.set(covr.getConvertInitialUnits());
		trmreasrec.jlife.set(covr.getJlife());
		trmreasrec.singp.set(covr.getSingp());
		trmreasrec.oldSumins.set(covr.getSumins());
		trmreasrec.pstatcode.set(covr.getPstatcode());
		trmreasrec.newSumins.set(ZERO);
		trmreasrec.clmPercent.set(100);
		callProgram(Trmreas.class, trmreasrec.trracalRec);
		if (isNE(trmreasrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(trmreasrec.statuz);
			syserrrec.params.set(trmreasrec.trracalRec);
			dbError9100();
		}
	}

protected void processMatdclms3000(Mathpf math)
	{
		begnMatdclm3000();
		processSameSuffix3004(math);
	}

protected void begnMatdclm3000()
	{
		matdclmList = matdpfDAO.getmatdClmRecord(atmodrec.company.toString(), chdrlifIO.getChdrnum().toString());
		if (matdclmList.isEmpty()) {
			return;
		}
	}

protected void processSameSuffix3004(Mathpf math)
	{
		for(Matdpf matd : matdclmList) {
			updateMatdclmRecs3100(matd, math);
			if (isEQ(matpcpy.status, varcom.endp)
			|| isEQ(matpcpy.status, varcom.bomb)) {
				break;
			}
		}

		/*EXIT*/
	}

protected void updateMatdclmRecs3100(Matdpf matd, Mathpf math)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					getProcessProg3100(matd, math);
				case exit3109:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void getProcessProg3100(Matdpf matd, Mathpf math)
	{
		wsaaCrtable.set(matd.getCrtable());
		if (isEQ(wsaaCrtable, SPACES)) {
			return;
		}
		readTableT56871500();
		readTableT65981600();
		if (isEQ(t6598rec.procesprog, SPACES)) {
			return;
		}
		matpcpy.planSuffix.set(matd.getPlnsfx());
		matpcpy.crtable.set(matd.getCrtable());
		matpcpy.cnttype.set(math.getCnttype());
		matpcpy.chdrcoy.set(matd.getChdrcoy());
		matpcpy.chdrnum.set(matd.getChdrnum());
		matpcpy.life.set(matd.getLife());
		matpcpy.jlife.set(matd.getJlife());
		matpcpy.coverage.set(matd.getCoverage());
		matpcpy.rider.set(matd.getRider());
		matpcpy.fund.set(matd.getVrtfund());
		matpcpy.tranno.set(wsaaChdrlifTranno);
		matpcpy.estimatedVal.set(matd.getEmv());
		matpcpy.actualVal.set(matd.getActvalue());
		matpcpy.type.set(matd.getType_t());
		if (isEQ(matd.getCurrcd(), SPACES)) {
			matpcpy.currcode.set(matd.getCurrcd());
			matpcpy.cntcurr.set(chdrlifIO.getCntcurr());
		}
		else {
			matpcpy.currcode.set(math.getCurrcd());
			matpcpy.cntcurr.set(matd.getCurrcd());
		}
		matpcpy.batckey.set(wsaaBatckey.batcFileKey);
		matpcpy.termid.set(wsaaTermid);
		matpcpy.effdate.set(wsaaEffectiveDate);
		matpcpy.date_var.set(wsaaTransactionDate);
		matpcpy.time.set(wsaaTransactionTime);
		matpcpy.user.set(wsaaUser);
		matpcpy.language.set(atmodrec.language);
		matpcpy.status.set(varcom.oK);
		callMaturityProcess3200();
		if (isEQ(matpcpy.status, varcom.endp)
		|| isEQ(matpcpy.status, varcom.bomb)) {
			goTo(GotoLabel.exit3109);
		}
	}

protected void callMaturityProcess3200()
	{
		/*CHK-PROCESS-PROG*/
		if (isEQ(t6598rec.procesprog, SPACES)) {
			return ;
		}
		callProgram(t6598rec.procesprog, matpcpy.maturityRec);
		if (isEQ(matpcpy.status, varcom.bomb)) {
			syserrrec.params.set(matpcpy.maturityRec);
			syserrrec.statuz.set(matpcpy.status);
			systemError9000();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Process the lives.                                          *
	* </pre>
	*/
protected void processLives4000(Mathpf math)
	{
		readLives4000(math);
	}

protected void readLives4000(Mathpf math)
	{
		lifeIO.setParams(SPACES);
		lifeIO.setChdrnum(chdrlifIO.getChdrnum());
		lifeIO.setChdrcoy(atmodrec.company);
		lifeIO.setCurrfrom(ZERO);
		lifeIO.setFormat(formatsInner.liferec);
		lifeIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)
		&& isNE(lifeIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			dbError9100();
		}
		if (isEQ(lifeIO.getStatuz(), varcom.endp)
		|| isNE(lifeIO.getChdrnum(), math.getChdrnum())
		|| isNE(lifeIO.getChdrcoy(), atmodrec.company)) {
			lifeIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(lifeIO.getStatuz(), varcom.endp))) {
			updateLives4100(math);
		}

	}

	/**
	* <pre>
	* Update life records.                                        *
	* </pre>
	*/
protected void updateLives4100(Mathpf math)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateRecs4100();
					writeNewLife4102(math);
				case readNextLife4104:
					readNextLife4104(math);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateRecs4100()
	{
		if (isNE(lifeIO.getValidflag(), "1")) {
			goTo(GotoLabel.readNextLife4104);
		}
		/* MOVE WSAA-TRANSACTION-DATE  TO LIFE-TRANSACTION-DATE.        */
		/* MOVE WSAA-TRANSACTION-TIME  TO LIFE-TRANSACTION-TIME.        */
		/* MOVE WSAA-USER              TO LIFE-USER.                    */
		/* MOVE WSAA-TERMID            TO LIFE-TERMID.                  */
		lifeIO.setValidflag("2");
		lifeIO.setCurrto(wsaaEffectiveDate);
		lifeIO.setFunction(varcom.rewrt);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			dbError9100();
		}
	}

	/**
	* <pre>
	*  Write a new record.
	* </pre>
	*/
protected void writeNewLife4102(Mathpf math)
	{
		lifeIO.setTransactionDate(wsaaTransactionDate);
		lifeIO.setTransactionTime(wsaaTransactionTime);
		lifeIO.setUser(wsaaUser);
		lifeIO.setTermid(wsaaTermid);
		lifeIO.setTranno(wsaaChdrlifTranno);
		lifeIO.setValidflag("1");
		lifeIO.setCurrto(varcom.vrcmMaxDate);
		lifeIO.setCurrfrom(wsaaEffectiveDate);
		if (isEQ(math.getPlnsfx(), ZERO)) {
			if (isEQ(lifeIO.getJlife(), "00")
			|| isEQ(lifeIO.getJlife(), "  ")
			&& isNE(t5679rec.setLifeStat, SPACES)) {
				lifeIO.setStatcode(t5679rec.setLifeStat);
			}
			else {
				if (isNE(t5679rec.setJlifeStat, SPACES)) {
					lifeIO.setStatcode(t5679rec.setJlifeStat);
				}
			}
		}
		lifeIO.setFunction(varcom.writr);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			dbError9100();
		}
	}

protected void readNextLife4104(Mathpf math)
	{
		lifeIO.setFunction(varcom.nextr);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)
		&& isNE(lifeIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			dbError9100();
		}
		if (isEQ(lifeIO.getStatuz(), varcom.endp)
		|| isNE(lifeIO.getChdrnum(), math.getChdrnum())
		|| isNE(lifeIO.getChdrcoy(), atmodrec.company)) {
			lifeIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void updateContractHeader4500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readChdrlif4500();
					readPayr4500();
				case updateInstprem4501:
					updateInstprem4501();
				case chkStatusCode4502:
					chkStatusCode4502();
				case rewriteChdrlifPayr4504:
					rewriteChdrlifPayr4504();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readChdrlif4500()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError9100();
		}
	}

protected void readPayr4500()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError9100();
		}
		/*  If the number of summarized is equal to zero, matured*/
		/*  the whole plan, else mature selected components.*/
		if (matureWholePlan.isTrue()) {
			if (fullyMatured.isTrue()) {
				goTo(GotoLabel.chkStatusCode4502);
			}
			else {
				goTo(GotoLabel.updateInstprem4501);
			}
		}
		chkComponents4510();
		if (fullyMatured.isTrue()) {
			goTo(GotoLabel.chkStatusCode4502);
		}
	}

protected void updateInstprem4501()
	{
		if (isNE(payrIO.getBillfreq(), "00")
		&& isNE(payrIO.getBillfreq(), "  ")) {
			setPrecision(chdrlifIO.getSinstamt01(), 2);
			chdrlifIO.setSinstamt01(sub(chdrlifIO.getSinstamt01(), wsaaInstpremTot));
			setPrecision(payrIO.getSinstamt01(), 2);
			payrIO.setSinstamt01(sub(payrIO.getSinstamt01(), wsaaInstpremTot));
			setPrecision(chdrlifIO.getSinstamt06(), 2);
			chdrlifIO.setSinstamt06(sub(chdrlifIO.getSinstamt06(), wsaaInstpremTot));
			setPrecision(payrIO.getSinstamt06(), 2);
			payrIO.setSinstamt06(sub(payrIO.getSinstamt06(), wsaaInstpremTot));
			goTo(GotoLabel.rewriteChdrlifPayr4504);
		}
	}

protected void chkStatusCode4502()
	{
		if (isNE(t5679rec.setCnPremStat, SPACES)
		|| isNE(t5679rec.setSngpCnStat, SPACES)) {
			if (isEQ(payrIO.getBillfreq(), "00")
			|| isEQ(payrIO.getBillfreq(), "  ")) {
				chdrlifIO.setPstatcode(t5679rec.setSngpCnStat);
			}
			else {
				chdrlifIO.setPstatcode(t5679rec.setCnPremStat);
			}
		}
		if (isNE(t5679rec.setCnRiskStat, SPACES)) {
			chdrlifIO.setStatcode(t5679rec.setCnRiskStat);
		}
	}

protected void rewriteChdrlifPayr4504()
	{
		chdrlifIO.setStattran(wsaaChdrlifTranno);
		chdrlifIO.setCurrfrom(wsaaEffectiveDate);
		chdrlifIO.setStatdate(wsaaEffectiveDate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		varcom.vrcmCompTermid.set(wsaaTermid);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransactionTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrlifIO.setTranid(varcom.vrcmCompTranid);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			dbError9100();
		}
		/* Rewrite the PAYR record.*/
		payrIO.setEffdate(wsaaEffectiveDate);
		payrIO.setTermid(wsaaTermid);
		payrIO.setUser(wsaaUser);
		payrIO.setTransactionDate(wsaaTransactionDate);
		payrIO.setTransactionTime(wsaaTransactionTime);
		payrIO.setPstatcode(chdrlifIO.getPstatcode());
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			dbError9100();
		}
	}

protected void chkComponents4510()
	{
		readBegnCovrmat4510();
		processCovrmatRec4512();
	}

protected void readBegnCovrmat4510()
	{
		covrmatList = covrpfDAO.getcovrmatRecordsForMature(atmodrec.company.toString(), chdrlifIO.getChdrnum().toString());	
		if (covrmatList.isEmpty()) {
			return;
		}
	}

protected void processCovrmatRec4512()
	{
		wsaaFullyMatured.set("Y");
		for(Covrpf covr : covrmatList) {
			processCovrmats4520(covr);
		}

		/*EXIT*/
	}

protected void processCovrmats4520(Covrpf covr)
	{
		try {
			chkStatcode4520(covr);
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void chkStatcode4520(Covrpf covr)
	{
		lookForStat4530(covr);
		if (!fullyMatured.isTrue()) {
			goTo(GotoLabel.exit4529);
		}
	}

protected void readNextCovrmat4522()
	{
		covrmatIO.setFormat(formatsInner.covrmatrec);
		covrmatIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(), varcom.oK)
		&& isNE(covrmatIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmatIO.getParams());
			dbError9100();
		}
		if (isNE(covrmatIO.getChdrcoy(), atmodrec.company)
		|| isNE(covrmatIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isEQ(covrmatIO.getStatuz(), varcom.endp)) {
			covrmatIO.setStatuz(varcom.endp);
		}
	}

protected void lookForStat4530(Covrpf covr)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					searchStat4530();
				case loop4532:
					loop4532(covr);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void searchStat4530()
	{
		wsaaSub.set(ZERO);
	}

protected void loop4532(Covrpf covr)
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			wsaaFullyMatured.set("Y");
			return ;
		}
		if (isEQ(covr.getStatcode(), t5679rec.covRiskStat[wsaaSub.toInt()])) {
			wsaaFullyMatured.set("N");
			return ;
		}
		goTo(GotoLabel.loop4532);
	}

	/**
	* <pre>
	*  This section write a corresponding LETC record for each MATH
	*  record. The LETOKEYS field is the contract header number
	*  CHDRNUM.
	* </pre>
	*/
protected void writeLetcRecord5000(Mathpf math)
	{
		try {
			readT6634Record5000();
			writeRecord5001(math);
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*  Read T6634 the Auto Letters table to get the letter type.
	*  The ITEM-ITEMITEM key is made up of CHDRLIF-CNTTYPE and
	*  the transaction code (WSKY-BATC-BATCTRCDE).
	* </pre>
	*/
protected void readT6634Record5000()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		/* MOVE T6634                  TO ITEM-ITEMTABL.                */
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from contract type & transaction code.      */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlifIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.exit5009);
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.              */
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF  T6634-LETTER-TYPE       = SPACES                         */
		if (isEQ(tr384rec.letterType, SPACES)) {
			goTo(GotoLabel.exit5009);
		}
	}

protected void writeRecord5001(Mathpf math)
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrlifIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.          */
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaEffectiveDate);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(chdrlifIO.getTranno());
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		wsaaOkey1.set(math.getPlnsfx());
		letrqstrec.otherKeys.set(wsaaOkey);
		/* MOVE CHDRLIF-CHDRNUM        TO LETRQST-OTHER-KEYS.           */
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			systemError9000();
		}
	}

	/**
	* <pre>
	* Write PTRN transaction.                                     *
	* </pre>
	*/
protected void writePtrnTransaction6000()
	{
		ptrnTransaction6000();
	}

protected void ptrnTransaction6000()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(wsaaEffectiveDate);
		/* MOVE WSAA-EFFECTIVE-DATE    TO PTRN-DATESUB.         <LA5184>*/
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			dbError9100();
		}
	}

	/**
	* <pre>
	* Update batch header transaction.                            *
	* </pre>
	*/
protected void updateBatchHeader7000()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			systemError9000();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Release Soft locked record.                                 *
	* </pre>
	*/
protected void releaseSoftlock8000()
	{
		releaseContract8000();
	}

protected void releaseContract8000()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			systemError9000();
		}
	}

protected void systemError9000()
	{
		start9000();
		errorProg9010();
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			/*     GO TO 9990-EXIT.                                         */
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg9010()
	{
		/*AT                                                               */
		atmodrec.statuz.set(varcom.bomb);
		/*EXIT*/
		/* EXIT.                                                        */
		exitProgram();
	}

protected void dbError9100()
	{
		start9100();
		errorProg9100();
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			/*     GO TO 9199-EXIT.                                         */
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg9100()
	{
		/*AT                                                               */
		atmodrec.statuz.set(varcom.bomb);
		/*EXIT*/
		/* EXIT.                                                        */
		exitProgram();
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(atmodrec.company);
		lifsttrrec.chdrnum.set(wsaaPrimaryChdrnum);
		lifsttrrec.tranno.set(wsaaChdrlifTranno);
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			dbError9100();
		}
	}

protected void dryProcessing10000()
	{
		start10010();
	}

protected void start10010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrlifIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT750811000();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT750811000();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrlifIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		//ILPI-145
//		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			systemError9000();
		}
	}

protected void readT750811000()
	{
		start11010();
	}

protected void start11010()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError9000();
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
	private FixedLengthStringData mathclmrec = new FixedLengthStringData(10).init("MATHCLMREC");
	private FixedLengthStringData matdclmrec = new FixedLengthStringData(10).init("MATDCLMREC");
	private FixedLengthStringData covrmatrec = new FixedLengthStringData(10).init("COVRMATREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData incrmjarec = new FixedLengthStringData(10).init("INCRMJAREC");
	private FixedLengthStringData incrrec = new FixedLengthStringData(10).init("INCRREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
