/*
 * File: Presprc.java
 * Date: 30 August 2009 1:57:51
 * Author: Quipoz Limited
 * 
 * Class transformed from PRESPRC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult; //MIBT-196
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.contractservicing.procedures.Totloan;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*         TRADITIONAL LIFE SURRENDER PROCESSING PROGRAM.
*
*   This subroutine is used as the processing routine for
*   traditional type component surrenders.
*
*   It is used for 'permanent' assurances and hence bonuses
*   and loans will be applicable.
*
*   It is called from the surrender processing AT module P5084AT.
*
*   It is called once per contract using the SURPCPY linkage
*   area.
*
*   It accesses information stored in the SURRENDER HEADER file
*   (SURH) and the SURRENDER DETAIL file (SURD) to write
*   accounting entries to the ACCOUNT MOVEMENTS file (ACMV).
*
*   A BEGN on the SURD file gets the first surrender record.
*   The SURH file is first read to get the contract level
*   details. The SURD file is then processed sequentially to
*   get surrender records for each of the components on the
*   contract. There may be a Sum Assured record and a Bonus
*   record for each component.
*
*   For each SURD record read, an account movement record is
*   written to the ACMV file.
*
*   When all SURD records have been processed for this contract,
*   write an ACMV record for the Adjustment value read from the
*   SURH record.
*   Process any loans by first calling TOTLOAN to bring
*   the interest on loans for this contract up to date.
*   ACMVs are then required for paying off loans. For each of
*   entries 8 - 11 from T5645, call LOANPYMT with the outstanding
*   Surrender amount until all surrender is 'allocated' or
*   all entries have been processed and some of the surrender
*   value remains.
*   Then write an ACMV record to suspense for the residual of
*   the value to be surrendered after the above.
*
*   Return control to the calling program.
*
*   FILE READS.
*   ----------
*
*   Surrender Header file for :
*   - Contract level details.
*   - Adjustment amount entered at time of surrender.
*
*   Surrender Detail file for :
*   - details of individual surrender amounts.
*
*   PROCESSING.
*   ----------
*
*   INITIAL VALUES.
*   --------------
*
*   - Read T5688 to establish whether contract or component
*     level accounting for this contract type.
*
*   - Read T5645 to get the accounting rules for this
*     subroutine.
*
*   - Read T1688 to get description of the transaction
*     which initiated the surrender.
*
*   - Initial read of SURD file.
*
*   - Read SURH file.
*
*   - Set up ACMV fields which are common to all components on
*     this contract.
*
*   PROCESSING LOOP
*   ---------------
*
*   - Read SURD file sequentially for all surrender records
*     associated with this contract.
*
*   - For each Record :
*
*   - There is no requirement to distinguish between 'S' type
*     records (Sum Assured) and 'B' type records (Reversionary
*     Bonus).
*
*   - Write an ACMV record.
*     - Fields common to all ACMV's
*
*       LIFA-FUNCTION         - 'PSTW'
*       LIFA-BATCCOY          - Contract Company from linkage
*       LIFA-BATCKEY          - Batch Key details from linkage
*       LIFA-RDOCNUM          - Contract Number from linkage
*       LIFA-TRANNO           - Transaction Number from SURD
*       LIFA-JRNSEQ           - Start from 1, increment by 1
*       LIFA-RLDGCOY          - Contract Company from linkage
*       LIFA-ORIGCURR         - Contract Currency from linkage
*       LIFA-TRANREF          - Contract Number from linkage
*       LIFA-TRANDESC         - Tran. Code description from T1688
*       LIFA-CRATE            - 0
*       LIFA-ACCTAMT          - 0
*       LIFA-GENLCOY          - Contract Company from linkage
*       LIFA-GENLCUR          - Contract Currency from linkage
*       LIFA-POSTYEAR         - Spaces
*       LIFA-POSTMONTH        - Spaces
*       LIFA-EFFDATE          - Effective Surr. Date from linkage
*       LIFA-RCAMT            - 0
*       LIFA-FRCDATE          - Max-date
*       LIFA-TRANSACTION-DATE - Effective Date from linkage
*       LIFA-TRANSACTION-TIME - Time of transaction
*       LIFA-USER             - Transaction User from linkage
*       LIFA-TERMID           - Transaction Term-ID from linkage
*       LIFA-SUBSTITUTE-CODE  - 1 - Contract Type
*                               2 - 5 not set
*     - Fields varying :
*
*       LIFA-SACSCODE         - Account Code from T5645
*       LIFA-RLDGACCT         - If Component Level Accounting :
*                                 Full component Key
*                               If Contract Level Accounting :
*                                 Contract Number
*       LIFA-SACSTYP          - Account Type from T5645
*       LIFA-ORIGAMT          - Amount of posting from SURD rec.
*       LIFA-GLCODE           - General Ledger Code from T5645
*       LIFA-GLSIGN           - General Ledger Sign from T5645
*       LIFA-CONTOT           - Control Total from T5645
*       LIFA-SUBSTITUTE-CODE  - 6 - Component Type
*
*   - Accounting Rules from T5645
*
*     Line 1  : Sum Assured Surrender Value for contract level
*     Line 2  : Bonus Surrender Value for contract level
*     Line 3  : Sum Assured Surrender Value for component level
*     Line 4  : Bonus Surrender Value for component level
*     Line 5  : Adjustment per contract from SURH record
*     Line 6  : Balance of all totalled
*     Line 7  : Amount of bonus held for this component
*     Line 8  :  Entries 8 - 11 are the prioritised
*     Line 9  :  list of CODE/TYPE combinations for use
*     Line 10 :  with loan processing. ie. if entry 8 is
*     Line 11 :  for Policy Loan Principal then for any loans
*                which exist, these are the first accounts
*                the program will try to clear, then 9 etc.
*
*   - Write ACMV for contract adjustment.
*
*   - Call LOANPYMT using the P2068REC Linkage area for the
*     T5645 entries in the order that they are listed. Each
*     time check to see whether any outstanding surrender
*     remains unallocated.
*
*   - Write balancing ACMV for the total.
*
*   SET EXIT VARIABLES.
*   ------------------
*   set linkage variables as follows :
*
*   To indicate that processing complete :
*
*   - STATUS -        'ENDP'
*
*****************************************************************
* </pre>
*/
public class Presprc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("PRESPRC");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String h150 = "H150";
	private static final String h134 = "H134";
	private static final String e044 = "E044";
	private static final String surdclmrec = "SURDCLMREC";
	private static final String surhclmrec = "SURHCLMREC";
	private static final String acblrec = "ACBLREC";
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t1688 = "T1688";

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

		/* WSAA-T5645 */
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (4, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	private PackedDecimalData wsaaAccumValue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaTranTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaTranUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);

	private FixedLengthStringData wsaaAcctLevel = new FixedLengthStringData(1);
	private Validator componentLevelAccounting = new Validator(wsaaAcctLevel, "Y");

	private FixedLengthStringData wsaaEndOfContFlag = new FixedLengthStringData(1).init("N");
	private Validator endOfContract = new Validator(wsaaEndOfContFlag, "Y");

	private FixedLengthStringData wsaaAcblKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAcblChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaAcblKey, 0);
	private FixedLengthStringData wsaaAcblLife = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 8);
	private FixedLengthStringData wsaaAcblCoverage = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 10);
	private FixedLengthStringData wsaaAcblRider = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 12);
	private FixedLengthStringData wsaaAcblPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 14);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Cashedrec cashedrec = new Cashedrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Surpcpy surpcpy = new Surpcpy();
	private static final String pos041Confg = "SUSUR008";
	boolean compSurrPosPermission = false;
	private boolean endowFlag = false;
	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private Zraepf zraepf = new Zraepf();
	public PackedDecimalData interestAmount = new PackedDecimalData(17, 2);
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private List<Acblpf> acblenqListLPAE = null;
	private List<Acblpf> acblenqListLPAS = null;
	private BigDecimal accAmtLPAE = new BigDecimal(0);
	private BigDecimal accAmtLPAS = new BigDecimal(0);
	private Annypyintcalcrec annypyintcalcrec = new Annypyintcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private static final String PRPRO001 = "PRPRO001";
	private boolean susur002Permission = false;
	private static final String susur002 = "SUSUR002";
	private boolean susur013Permission = false;
	private static final String SUSUR013 = "SUSUR013";
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		seExit9090, 
		dbExit9190
	}

	public Presprc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		surpcpy.surrenderRec = convertAndSetParam(surpcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline1000()
	{
		/*START*/
		initialise2000();
		while ( !(endOfContract.isTrue())) {
			processSurdRecords3000();
		}
		//For component surrender transaction adjustment amount is getting stored at component level table 
		//which is SURD, so before posting adjustment amount from header we are assigning the amount from
		//SURD to SURH
		if(isEQ(surpcpy.type, "C"))
		{
			surhclmIO.setOtheradjst(surdclmIO.getOtheradjst());
		}
		endOfContract4000();
		postPoldebt4500();
		
		exitProgram();
	}

protected void updateZrae()
{
    List<Zraepf> zraepfList;
    List<String> chdrnumList = new ArrayList<String>();
    chdrnumList.add(surdclmIO.getChdrnum().toString());
    zraepfList = zraepfDAO.searchZraepfRecord(chdrnumList, surdclmIO.getChdrcoy().toString());
    
    if(zraepfList!= null && !zraepfList.isEmpty()){
    	 zraepfDAO.updateValidflag(zraepfList);	
     }
    Zraepf Zraepftmp = new Zraepf();
		if (zraepfList != null && !zraepfList.isEmpty()) {
			Zraepftmp = new Zraepf(zraepfList.get(0));
			Zraepftmp.setApcaplamt(new BigDecimal(0));
			Zraepftmp.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
			Zraepftmp.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
			Zraepftmp.setFlag("2");
			Zraepftmp.setValidflag("1");
			Zraepftmp.setTranno(surdclmIO.getTranno().toInt());
			Zraepftmp.setApintamt(BigDecimal.ZERO);
			Zraepftmp.setTotamnt(0);
			zraepfDAO.insertZraeRecord(Zraepftmp);
		}
}

private void endowmentposting() {
	lifacmvrec1.tranno.set(surdclmIO.getTranno());
	if(zraepf.getChdrnum().length() > 0)
		calculateInterest();	
	
	
	acblenqListLPAE = acblpfDAO.getAcblenqRecord(
			surdclmIO.getChdrcoy().toString(), StringUtils.rightPad(surdclmIO.getChdrnum().toString(), 16),t5645rec.sacscode14.toString(),t5645rec.sacstype14.toString());

	acblenqListLPAS =acblpfDAO.getAcblenqRecord(
			surdclmIO.getChdrcoy().toString(), StringUtils.rightPad(surdclmIO.getChdrnum().toString(), 16),t5645rec.sacscode15.toString(),t5645rec.sacstype15.toString());
	
	if (acblenqListLPAE != null && !acblenqListLPAE.isEmpty()) {
		accAmtLPAE = acblenqListLPAE.get(0).getSacscurbal();
		accAmtLPAE = new BigDecimal(ZERO.toString()).subtract(accAmtLPAE);	
	}
	if (acblenqListLPAS != null && !acblenqListLPAS.isEmpty()) {
		accAmtLPAS =  acblenqListLPAS.get(0).getSacscurbal();
		accAmtLPAS = new BigDecimal(ZERO.toString()).subtract(accAmtLPAS);
	}
			
	if(isNE(accAmtLPAE, 0))
	{	lifacmvrec1.tranno.set(surdclmIO.getTranno());
		lifacmvrec1.rldgacct.set(surdclmIO.getChdrnum());
		lifacmvrec1.sacscode.set(t5645rec.sacscode14);// LP AE
		lifacmvrec1.sacstyp.set(t5645rec.sacstype14);
		lifacmvrec1.glcode.set(t5645rec.glmap14);
		lifacmvrec1.glsign.set(t5645rec.sign14);
		lifacmvrec1.contot.set(t5645rec.cnttot14);
		lifacmvrec1.origamt.set(accAmtLPAE); 
		lifacmvrec1.acctamt.set(accAmtLPAE);
		wsaaAccumValue.add(accAmtLPAE.doubleValue());
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			return ; 
		}
		lifacmvrec1.sacscode.set(t5645rec.sacscode15);// LP AS
		lifacmvrec1.sacstyp.set(t5645rec.sacstype15);
		lifacmvrec1.glcode.set(t5645rec.glmap15);
		lifacmvrec1.glsign.set(t5645rec.sign15);
		lifacmvrec1.contot.set(t5645rec.cnttot15);
		accAmtLPAS=	add(accAmtLPAS,interestAmount).getbigdata();
		lifacmvrec1.origamt.set(accAmtLPAS);
		lifacmvrec1.acctamt.set(accAmtLPAS);
		wsaaAccumValue.add(accAmtLPAS.doubleValue());
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
		}
	}
	updateZrae();
	
}



private void calculateInterest() {
	annypyintcalcrec.intcalcRec.set(SPACES);
	annypyintcalcrec.chdrcoy.set(surdclmIO.getChdrcoy());
	annypyintcalcrec.chdrnum.set(surdclmIO.getChdrnum());
	annypyintcalcrec.cnttype.set(surhclmIO.cnttype);
	annypyintcalcrec.interestTo.set(surhclmIO.getEffdate());
	annypyintcalcrec.interestFrom.set(zraepf.getAplstintbdte());
	annypyintcalcrec.annPaymt.set(zraepf.getApcaplamt());
	annypyintcalcrec.lastCaplsnDate.set(zraepf.getAplstcapdate());		
	annypyintcalcrec.interestAmount.set(ZERO);
	annypyintcalcrec.currency.set(zraepf.getPaycurr());
	annypyintcalcrec.transaction.set("ENDOWMENT");
	if (isLT(annypyintcalcrec.interestFrom,annypyintcalcrec.interestTo)) {
		callProgram(Anpyintcal.class, annypyintcalcrec.intcalcRec);
		if (isNE(annypyintcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(annypyintcalcrec.statuz);
			systemError9000();	
		}
	}
	/* MOVE INTC-INTEREST-AMOUNT    TO ZRDP-AMOUNT-IN.              */
	/* PERFORM 5000-CALL-ROUNDING.                                  */
	/* MOVE ZRDP-AMOUNT-OUT         TO INTC-INTEREST-AMOUNT.        */
	if (isNE(annypyintcalcrec.interestAmount, 0)) {
		zrdecplrec.amountIn.set(annypyintcalcrec.interestAmount);
		callRounding5100();
		annypyintcalcrec.interestAmount.set(zrdecplrec.amountOut);
		interestAmount.add(annypyintcalcrec.interestAmount);
	}
	
}



private void callRounding5100() {
	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(surdclmIO.getChdrcoy());
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(zraepf.getPaycurr());
	zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		systemError9000();	
	}
	/*EXIT*/
}



protected void initialise2000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaProg);
		varcom.vrcmTime.set(getCobolTime());
		wsaaEndOfContFlag.set("N");
		wsaaAccumValue.set(ZERO);
		wsaaSequenceNo.set(ZERO);
		compSurrPosPermission = FeaConfg.isFeatureExist("2", pos041Confg, appVars, "IT");
		endowFlag = FeaConfg.isFeatureExist("2", PRPRO001, appVars, "IT");
		susur002Permission = FeaConfg.isFeatureExist("2",susur002, appVars, "IT"); 
		susur013Permission = FeaConfg.isFeatureExist("2",SUSUR013, appVars, "IT");
		readTables2100();
		initialSurdRead2200();
		readSurhRecord2300();
		initialAcmvFields2400();
		
		if(endowFlag){
			zraepf=zraepfDAO.getItemByContractNum(surpcpy.chdrnum.toString());
			
		}
		/*EXIT*/
	}

protected void readTables2100()
	{
		accountingLevel2110();
		accountingRules2120();
		transactionCodeDesc2130();
	}

protected void accountingLevel2110()
	{
		/*  Read T5688 to fund out whether contract or component level*/
		/*  accounting.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(surpcpy.cnttype);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError9000();
		}
		if (isNE(itdmIO.getItemcoy(), surpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), surpcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surpcpy.cnttype);
			syserrrec.statuz.set(e308);
			systemError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaAcctLevel.set(t5688rec.comlvlacc);
	}

protected void accountingRules2120()
	{
		/*  Read T5645 to get accounting rules.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(surpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(wsaaProg);
			syserrrec.statuz.set(h134);
			systemError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*  Store entries 8 - 11 incl. for use in clearing loan*/
		/*  amounts later.*/
		for (wsaaSub1.set(8); !(isGT(wsaaSub1, 11)); wsaaSub1.add(1)){
			compute(wsaaSub2, 0).set(sub(wsaaSub1, 7));
			wsaaT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsaaT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsaaT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsaaT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
	}

protected void transactionCodeDesc2130()
	{
		/*  Read T1688 to get transaction code description.*/
		wsaaBatckey.set(surpcpy.batckey);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(surpcpy.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setLanguage(surpcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(wsaaBatckey.batcBatctrcde);
			syserrrec.statuz.set(e044);
			systemError9000();
		}
		wsaaLongdesc.set(descIO.getLongdesc());
	}

protected void initialSurdRead2200()
	{
		start2200();
	}

protected void start2200()
	{
		/*  Read SURDCLM file using key information from copybook.*/
		/*  Read first using a BEGN and use the full component key.*/
		/*  This is actually a re-read of the SURD record which has*/
		/*  already been read in the calling program.*/
		surdclmIO.setParams(SPACES);
		surdclmIO.setChdrcoy(surpcpy.chdrcoy);
		surdclmIO.setChdrnum(surpcpy.chdrnum);
		surdclmIO.setTranno(surpcpy.tranno);
		surdclmIO.setPlanSuffix(surpcpy.planSuffix);
		surdclmIO.setLife(surpcpy.life);
		surdclmIO.setCoverage(surpcpy.coverage);
		surdclmIO.setRider(surpcpy.rider);
		surdclmIO.setCrtable(surpcpy.crtable);
		
		//For component surrender transaction record will read only one record at a time,
		//not all the records from SURDPF 
		
		if(isEQ(surpcpy.type,"C") && compSurrPosPermission){
			
			surdclmIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, surdclmIO);
			
			if (isNE(surdclmIO.getStatuz(), varcom.oK) && isNE(surdclmIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(surdclmIO.getParams());
				dbError9100();
			}
		}else{
			surdclmIO.setFunction(varcom.begn);
			surdclmIO.setFormat(surdclmrec);
			//performance improvement --  Niharika Modi 
			surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			surdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
			SmartFileCode.execute(appVars, surdclmIO);
			if (isNE(surdclmIO.getStatuz(), varcom.oK)
			&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(surdclmIO.getParams());
				syserrrec.statuz.set(surdclmIO.getStatuz());
				dbError9100();
			}
			if (isNE(surdclmIO.getChdrcoy(), surpcpy.chdrcoy)
			|| isNE(surdclmIO.getChdrnum(), surpcpy.chdrnum)
			|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(surpcpy.chdrnum);
				syserrrec.statuz.set(h150);
				dbError9100();
			}
		}
	
	}

protected void readSurhRecord2300()
	{
		start2300();
	}

protected void start2300()
	{
		/*  Read SURHCLM file using key information from SURD record.*/
		surhclmIO.setParams(SPACES);
		surhclmIO.setChdrcoy(surdclmIO.getChdrcoy());
		surhclmIO.setChdrnum(surdclmIO.getChdrnum());
		surhclmIO.setTranno(surdclmIO.getTranno());
		surhclmIO.setPlanSuffix(surdclmIO.getPlanSuffix());
		surhclmIO.setFunction(varcom.readr);
		surhclmIO.setFormat(surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surhclmIO.getParams());
			syserrrec.statuz.set(surhclmIO.getStatuz());
			dbError9100();
		}
	}

protected void initialAcmvFields2400()
	{
		start2400();
	}

protected void start2400()
	{
		/*  Set up LIFACMV parameters which are common to all postings.*/
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batccoy.set(surpcpy.chdrcoy);
		lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec1.rdocnum.set(surpcpy.chdrnum);
		lifacmvrec1.rldgcoy.set(surpcpy.chdrcoy);
		lifacmvrec1.origcurr.set(surpcpy.cntcurr);
		lifacmvrec1.tranref.set(surpcpy.chdrnum);
		lifacmvrec1.trandesc.set(wsaaLongdesc);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.genlcoy.set(surpcpy.chdrcoy);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.effdate.set(surpcpy.effdate);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.transactionDate.set(surpcpy.effdate);
		lifacmvrec1.transactionTime.set(varcom.vrcmTime);
		lifacmvrec1.user.set(surpcpy.user);
		lifacmvrec1.termid.set(surpcpy.termid);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.substituteCode[1].set(surpcpy.cnttype);
	}

protected void processSurdRecords3000()
	{			
		if (isNE(surdclmIO.fieldType, 'E')) {
			start3000();
		} else {
			endowmentposting();
		}
		readNextSurdclm();
	}

protected void start3000()
	{
		/*  Process a SURD record.*/
		lifacmvrec1.origamt.set(surdclmIO.getActvalue());
		lifacmvrec1.tranno.set(surdclmIO.getTranno());
		//Field type C has been added here in order to do SO postings for component surrender also 
		if (isEQ(surdclmIO.getFieldType(), "S") || isEQ(surpcpy.type, "C")) { 
			/*Field type C has been added here in order to do postings on component level irrespective 
			of settings in T5688 in case if this subroutine has been called from 
			traditional component surrender transactions*/  
			if (componentLevelAccounting.isTrue() || isEQ(surpcpy.type, "C")) {
				lifacmvrec1.sacscode.set(t5645rec.sacscode03);
				lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
				lifacmvrec1.glcode.set(t5645rec.glmap03);
				lifacmvrec1.glsign.set(t5645rec.sign03);
				lifacmvrec1.contot.set(t5645rec.cnttot03);
			}
			else {
				lifacmvrec1.sacscode.set(t5645rec.sacscode01);
				lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
				lifacmvrec1.glcode.set(t5645rec.glmap01);
				lifacmvrec1.glsign.set(t5645rec.sign01);
				lifacmvrec1.contot.set(t5645rec.cnttot01);
			}
		}
		else {
			if (componentLevelAccounting.isTrue()) {
				lifacmvrec1.sacscode.set(t5645rec.sacscode04);
				lifacmvrec1.sacstyp.set(t5645rec.sacstype04);
				lifacmvrec1.glcode.set(t5645rec.glmap04);
				lifacmvrec1.glsign.set(t5645rec.sign04);
				lifacmvrec1.contot.set(t5645rec.cnttot04);
			}
			else {
				lifacmvrec1.sacscode.set(t5645rec.sacscode02);
				lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
				lifacmvrec1.glcode.set(t5645rec.glmap02);
				lifacmvrec1.glsign.set(t5645rec.sign02);
				lifacmvrec1.contot.set(t5645rec.cnttot02);
			}
		}
		wsaaAccumValue.add(surdclmIO.getActvalue());
		lifacmvrec1.rldgacct.set(SPACES);
		if (componentLevelAccounting.isTrue()||  isEQ(surdclmIO.getFieldType(), "C") ) {
			wsaaRldgChdrnum.set(surpcpy.chdrnum);
			wsaaPlan.set(surdclmIO.getPlanSuffix());
			/*        MOVE SURP-LIFE              TO WSAA-RLDG-LIFE            */
			/*        MOVE SURP-COVERAGE          TO WSAA-RLDG-COVERAGE        */
			/*        MOVE SURP-RIDER             TO WSAA-RLDG-RIDER           */
			wsaaRldgLife.set(surdclmIO.getLife());
			wsaaRldgCoverage.set(surdclmIO.getCoverage());
			wsaaRldgRider.set(surdclmIO.getRider());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
		}
		else {
			lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
		}
		/* MOVE SURP-CRTABLE               TO LIFA-SUBSTITUTE-CODE(6).  */
		lifacmvrec1.substituteCode[6].set(surdclmIO.getCrtable());
		if (isNE(lifacmvrec1.origamt, 0)) {
			postAcmvRecord5000();
		}
		if (isEQ(surdclmIO.getFieldType(), "B")) {
			postBonusAmt3200();
		}
		
	}



protected void readNextSurdclm()
{
/*  Do a read next on the file and set processing flags.*/
	if(isEQ(surpcpy.type,"C") && compSurrPosPermission){
		wsaaEndOfContFlag.set("Y");				
	}else{
		surdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)
		&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			syserrrec.statuz.set(surdclmIO.getStatuz());
			dbError9100();
		}
	
		/*  Check for end of contract.*/
		/*  Check for end of file.*/
		if (isNE(surdclmIO.getChdrcoy(), surpcpy.chdrcoy)
		|| isNE(surdclmIO.getChdrnum(), surpcpy.chdrnum)
		|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
			wsaaEndOfContFlag.set("Y");
		}
	}
}



protected void postBonusAmt3200()
	{
		start3200();
	}

	/**
	* <pre>
	*  Create a LIFACMV posting to clear the ACBL of any bonus
	*  held for this component.
	* </pre>
	*/
protected void start3200()
	{
		/*  First read the ACBL file.*/
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(surpcpy.chdrcoy);
		acblIO.setSacscode(t5645rec.sacscode07);
		wsaaAcblChdrnum.set(surpcpy.chdrnum);
		/*    MOVE SURP-LIFE                  TO WSAA-ACBL-LIFE.           */
		/*    MOVE SURP-COVERAGE              TO WSAA-ACBL-COVERAGE.       */
		/*    MOVE SURP-RIDER                 TO WSAA-ACBL-RIDER.          */
		wsaaAcblLife.set(surdclmIO.getLife());
		wsaaAcblCoverage.set(surdclmIO.getCoverage());
		wsaaAcblRider.set(surdclmIO.getRider());
		wsaaPlan.set(surdclmIO.getPlanSuffix());
		wsaaAcblPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaAcblKey);
		acblIO.setOrigcurr(surpcpy.currcode);
		acblIO.setSacstyp(t5645rec.sacstype07);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			dbError9100();
		}
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			lifacmvrec1.origamt.set(0);
		}
		else {
			lifacmvrec1.origamt.set(acblIO.getSacscurbal());
			//ILIFE-1261, value will be added or subtracted depending on the table value T5645, commenting the below fix. 
			// MIBT-196, Modified to avoid incorrect sub account value for bonus
			//compute(lifacmvrec1.origamt,2).set(mult(lifacmvrec1.origamt,-1));
		}
		if (isNE(surdclmIO.getEstMatValue(), 0)) {
			lifacmvrec1.origamt.set(surdclmIO.getEstMatValue());
		}
		/*  Always post bonus clearance record at component level.         */
		wsaaRldgChdrnum.set(surpcpy.chdrnum);
		wsaaPlan.set(surdclmIO.getPlanSuffix());
		wsaaRldgLife.set(surdclmIO.getLife());
		wsaaRldgCoverage.set(surdclmIO.getCoverage());
		wsaaRldgRider.set(surdclmIO.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.sacscode.set(t5645rec.sacscode07);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype07);
		lifacmvrec1.glcode.set(t5645rec.glmap07);
		lifacmvrec1.glsign.set(t5645rec.sign07);
		lifacmvrec1.contot.set(t5645rec.cnttot07);
		if (isNE(lifacmvrec1.origamt, 0)) {
			postAcmvRecord5000();
		}
	}

protected void endOfContract4000()
	{
		start4000();
	}

protected void start4000()
	{
		/*  Now post records for Adjustment value (read from the header)*/
		/*  and for Total value of all postings made (suspense).*/
		lifacmvrec1.rldgacct.set(SPACES);
		if (isEQ(surdclmIO.getFieldType(), "C") ) {
			wsaaRldgChdrnum.set(surpcpy.chdrnum);
			wsaaPlan.set(surdclmIO.getPlanSuffix());
			/*        MOVE SURP-LIFE              TO WSAA-RLDG-LIFE            */
			/*        MOVE SURP-COVERAGE          TO WSAA-RLDG-COVERAGE        */
			/*        MOVE SURP-RIDER             TO WSAA-RLDG-RIDER           */
			wsaaRldgLife.set(surdclmIO.getLife());
			wsaaRldgCoverage.set(surdclmIO.getCoverage());
			wsaaRldgRider.set(surdclmIO.getRider());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.sacscode.set(t5645rec.sacscode13);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype13);
			lifacmvrec1.glcode.set(t5645rec.glmap13);
			lifacmvrec1.glsign.set(t5645rec.sign13);
			lifacmvrec1.contot.set(t5645rec.cnttot13);
			lifacmvrec1.substituteCode[6].set(surdclmIO.getCrtable());
		} else {
			lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
			lifacmvrec1.sacscode.set(t5645rec.sacscode05);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec1.glcode.set(t5645rec.glmap05);
			lifacmvrec1.glsign.set(t5645rec.sign05);
			lifacmvrec1.contot.set(t5645rec.cnttot05);
		}
		
		/*  Postings 5 and 6 always done at contract level.                */
		/*    IF  COMPONENT-LEVEL-ACCOUNTING                          <002>*/
		/*        MOVE SURP-CHDRNUM           TO WSAA-RLDG-CHDRNUM    <002>*/
		/*        MOVE SURDCLM-PLAN-SUFFIX    TO WSAA-PLAN            <002>*/
		/*        MOVE SURP-LIFE              TO WSAA-RLDG-LIFE       <002>*/
		/*        MOVE SURP-COVERAGE          TO WSAA-RLDG-COVERAGE   <002>*/
		/*        MOVE SURP-RIDER             TO WSAA-RLDG-RIDER      <002>*/
		/*        MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX<002>*/
		/*        MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT        <002>*/
		/*    ELSE                                                    <002>*/
		/*        MOVE SURP-CHDRNUM           TO LIFA-RLDGACCT        <002>*/
		/*    END-IF                                                  <002>*/
		/*                                                            <002>*/
		lifacmvrec1.origamt.set(surhclmIO.getOtheradjst());
		if (isNE(lifacmvrec1.origamt, 0)) {
			postAcmvRecord5000();
		}
		wsaaAccumValue.add(lifacmvrec1.origamt);
		/*  Take value in WSAA-ACCUM-VALUE and allocate to any*/
		/*  outstanding loans.*/
		if (isNE(surpcpy.type, "P") && isNE(surpcpy.type, "C")) {
			allocateToLoans6000();
		}
		/*Posting of LP S is done in P5084At. Including suspense amount to post in LP PS only.*/
		if(susur002Permission){
			wsaaAccumValue.add(surhclmIO.getSuspenseamt());
		}
		
		// Japan localization- ILB-1016 : Posting LP-UP and LP-S
		if(susur013Permission) {
			lifacmvrec1.origamt.set(surhclmIO.unexpiredprm);
			lifacmvrec1.sacscode.set(t5645rec.sacscode13);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype13);
			lifacmvrec1.glcode.set(t5645rec.glmap13);
			lifacmvrec1.glsign.set(t5645rec.sign13);
			lifacmvrec1.contot.set(t5645rec.cnttot13);
			if (isNE(lifacmvrec1.origamt, 0)) {
				postAcmvRecord5000();
			}
			wsaaAccumValue.add(surhclmIO.getUnexpiredprm());
			
			
			lifacmvrec1.origamt.set(surhclmIO.suspenseamt);
			lifacmvrec1.sacscode.set(t5645rec.sacscode14);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype14);
			lifacmvrec1.glcode.set(t5645rec.glmap14);
			lifacmvrec1.glsign.set(t5645rec.sign14);
			lifacmvrec1.contot.set(t5645rec.cnttot14);
			if (isNE(lifacmvrec1.origamt, 0)) {
				postAcmvRecord5000();
			}
			wsaaAccumValue.add(surhclmIO.getSuspenseamt());
		}
		/*  Get net amount payable to be posted to Suspense.               */
		/*  To get net amount, Policy Debts and Tax Amount must be         */
		/*  deducted.                                                      */
		compute(wsaaAccumValue, 2).set(sub(sub(wsaaAccumValue, surhclmIO.getTdbtamt()), surhclmIO.getTaxamt()));
		/*  Post any remaining money to Suspense.*/
		lifacmvrec1.sacscode.set(t5645rec.sacscode06);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec1.glcode.set(t5645rec.glmap06);
		lifacmvrec1.glsign.set(t5645rec.sign06);
		lifacmvrec1.contot.set(t5645rec.cnttot06);
		lifacmvrec1.origamt.set(wsaaAccumValue);
		if (isNE(lifacmvrec1.origamt, 0)) {
			postAcmvRecord5000();
		}
		surpcpy.status.set(varcom.endp);
	}

protected void postPoldebt4500()

	{
		
	start4500();
	
	}

protected void start4500()
	{
		/*      GO TO 4599-EXIT.                                <V43L004>*/
		if ((setPrecision(ZERO, 2)
		&& isEQ((add(surhclmIO.getTdbtamt(), surhclmIO.getTaxamt())), ZERO))) {
			return ;
		}
		lifacmvrec1.sacscode.set(t5645rec.sacscode12);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype12);
		lifacmvrec1.glcode.set(t5645rec.glmap12);
		lifacmvrec1.glsign.set(t5645rec.sign12);
		lifacmvrec1.contot.set(t5645rec.cnttot12);
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.rldgacct.set(surhclmIO.getChdrnum());
		lifacmvrec1.origcurr.set(surhclmIO.getCurrcd());
		compute(lifacmvrec1.origamt, 2).set(add(surhclmIO.getTdbtamt(), surhclmIO.getTaxamt()));
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.genlcoy.set(surhclmIO.getChdrcoy());
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.effdate.set(surhclmIO.getEffdate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(surhclmIO.getCnttype());
		postAcmvRecord5000();
	}

protected void postAcmvRecord5000()
	{
		/*START*/
		/*  Call "LIFACMV" to write account movement records to the ACMV*/
		/*  file.*/
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			systemError9000();
		}
		/*EXIT*/
	}

protected void allocateToLoans6000()
	{
		start6000();
	}

	/**
	* <pre>
	*  Call the loan payment routine for each T5645 entry.
	* </pre>
	*/
protected void start6000()
	{
		/*  First load in the 'common' fields for the linkage areas.*/
		/* MOVE SPACES                     TO R2068-P2068-REC.          */
		/* MOVE 'RESID'                    TO R2068-FUNCTION.           */
		/* MOVE WSAA-BATCKEY               TO R2068-BATCHKEY.           */
		/* MOVE LIFA-RDOCNUM               TO R2068-DOCT-NUMBER.        */
		/* MOVE LIFA-BATCCOY               TO R2068-DOCT-COMPANY.       */
		/* MOVE LIFA-EFFDATE               TO R2068-TRANDATE.           */
		/* MOVE 0                          TO R2068-DOCORIGAMT.         */
		/* MOVE 0                          TO R2068-DOCACCTAMT.         */
		/* MOVE 0                          TO R2068-HDR-CONTOT.         */
		/* MOVE 0                          TO R2068-HEDRRATE.           */
		/* MOVE WSAA-SEQUENCE-NO           TO R2068-TRANSEQ.            */
		/* MOVE 0                          TO R2068-ACCTAMT.            */
		/* MOVE LIFA-ORIGCURR              TO R2068-ORIGCCY.            */
		/* MOVE LIFA-CRATE                 TO R2068-DISSRATE.           */
		/* MOVE LIFA-TRANDESC              TO R2068-TRANDESC.           */
		wsaaGlCompany.set(lifacmvrec1.genlcoy);
		wsaaGlCurrency.set(lifacmvrec1.genlcur);
		/* MOVE 0                          TO R2068-SCROLL.             */
		/* MOVE 0                          TO R2068-SCROLL-TRANSEQ.     */
		/* MOVE 0                          TO R2068-HDR-CHANGED.        */
		/* MOVE 0                          TO R2068-TRN-CHANGED.        */
		/* MOVE SURHCLM-CHDRCOY            TO R2068-CHDRCOY.            */
		/* MOVE SURHCLM-CHDRNUM            TO R2068-CHDRNUM.            */
		/* MOVE LIFA-TRANNO                TO R2068-TRANNO.             */
		wsaaTranTermid.set(surpcpy.termid);
		wsaaTranUser.set(surpcpy.user);
		wsaaTranTime.set(surpcpy.time);
		wsaaTranDate.set(surpcpy.date_var);
		/* MOVE WSAA-TRANID                TO R2068-TRANID.             */
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(surpcpy.chdrcoy);
		wsaaTranEntity.set(surpcpy.chdrnum);
		/* MOVE WSAA-TRANKEY               TO R2068-TRANKEY.            */
		cashedrec.cashedRec.set(SPACES);
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(wsaaBatckey);
		cashedrec.doctNumber.set(lifacmvrec1.rdocnum);
		cashedrec.doctCompany.set(lifacmvrec1.batccoy);
		cashedrec.trandate.set(lifacmvrec1.effdate);
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		cashedrec.transeq.set(wsaaSequenceNo);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(lifacmvrec1.origcurr);
		cashedrec.dissrate.set(lifacmvrec1.crate);
		cashedrec.trandesc.set(lifacmvrec1.trandesc);
		cashedrec.chdrcoy.set(surhclmIO.getChdrcoy());
		cashedrec.chdrnum.set(surhclmIO.getChdrnum());
		cashedrec.tranno.set(lifacmvrec1.tranno);
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.trankey.set(wsaaTrankey);
		cashedrec.language.set(surpcpy.language);
		/*  Call TOTLOAN to bring interest up to date on this*/
		/*  contract.*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(surhclmIO.getChdrcoy());
		totloanrec.chdrnum.set(surhclmIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(surhclmIO.getEffdate());
		totloanrec.tranno.set(surpcpy.tranno);
		totloanrec.batchkey.set(wsaaBatckey);
		totloanrec.tranTerm.set(surpcpy.termid);
		totloanrec.tranDate.set(surpcpy.date_var);
		totloanrec.tranTime.set(surpcpy.time);
		totloanrec.tranUser.set(surpcpy.user);
		totloanrec.language.set(surpcpy.language);
		totloanrec.function.set("POST");
		callProgram(Totloan.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError9000();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal, totloanrec.interest));
		if (isGT(wsaaHeldCurrLoans, 0)) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1, 4)
			|| isEQ(wsaaAccumValue, 0)); wsaaSub1.add(1)){
				loans6100();
			}
		}
		/* Retrieve JRNSEQ back from LOANPYMT subroutine so that we can*/
		/*  continue sequence*/
		wsaaSequenceNo.set(cashedrec.transeq);
	}

protected void loans6100()
	{
		start6100();
	}

protected void start6100()
	{
		/* MOVE WSAA-T5645-SIGN (WSAA-SUB1)     TO R2068-SIGN.          */
		/* MOVE WSAA-T5645-SACSCODE (WSAA-SUB1) TO R2068-SACSCODE.      */
		/* MOVE WSAA-T5645-SACSTYPE (WSAA-SUB1) TO R2068-SACSTYP.       */
		wsaaGlMap.set(wsaaT5645Glmap[wsaaSub1.toInt()]);
		/* MOVE WSAA-GENLKEY                    TO R2068-GENLKEY.       */
		/* MOVE WSAA-ACCUM-VALUE                TO R2068-ORIGAMT.       */
		cashedrec.sign.set(wsaaT5645Sign[wsaaSub1.toInt()]);
		cashedrec.sacscode.set(wsaaT5645Sacscode[wsaaSub1.toInt()]);
		cashedrec.sacstyp.set(wsaaT5645Sacstype[wsaaSub1.toInt()]);
		cashedrec.genlkey.set(wsaaGenlkey);
		cashedrec.origamt.set(wsaaAccumValue);
		/* CALL 'LOANPYMT' USING R2068-P2068-REC.                       */
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		/* IF  R2068-STATUZ            NOT = O-K                        */
		/*     MOVE R2068-STATUZ           TO SYSR-STATUZ               */
		/*     PERFORM 9000-SYSTEM-ERROR.                               */
		if (isNE(cashedrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			systemError9000();
		}
		/* MOVE R2068-DOCORIGAMT           TO WSAA-ACCUM-VALUE.         */
		wsaaAccumValue.set(cashedrec.docamt);
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
				case seExit9090: 
					seExit9090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		surpcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9100();
				case dbExit9190: 
					dbExit9190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		surpcpy.status.set(varcom.bomb);
		exitProgram();
	}
}
