package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sd5lh
 * @version 1.0 generated on 17/01/19 07:06
 * @author Quipoz
 */
public class Sd5lhScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(1134);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(584).isAPartOf(dataArea, 0);

	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,122);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,130);	
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,177);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,185);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,193);
	public FixedLengthStringData payfreq = DD.payfreq.copy().isAPartOf(dataFields,201);	
	
	public FixedLengthStringData compno = DD.compcode.copy().isAPartOf(dataFields,203);
	public FixedLengthStringData compcode = DD.hcrtable.copy().isAPartOf(dataFields,205);
	public FixedLengthStringData compdesc = DD.compdesc.copy().isAPartOf(dataFields,209);
	public FixedLengthStringData comprskcode = DD.compcode.copy().isAPartOf(dataFields,239);
	public FixedLengthStringData comprskdesc = DD.compdesc.copy().isAPartOf(dataFields,241);
	public FixedLengthStringData comppremcode = DD.compcode.copy().isAPartOf(dataFields,271);
	public FixedLengthStringData comppremdesc = DD.compdesc.copy().isAPartOf(dataFields,273);
	public ZonedDecimalData compsumss = DD.compsumss.copyToZonedDecimal().isAPartOf(dataFields, 303);
	public ZonedDecimalData compbilprm = DD.compbilprm.copyToZonedDecimal().isAPartOf(dataFields, 320);
	public ZonedDecimalData refprm = DD.refprm.copyToZonedDecimal().isAPartOf(dataFields, 337);
	public ZonedDecimalData refadjust = DD.adjamt.copyToZonedDecimal().isAPartOf(dataFields, 354);
	public ZonedDecimalData refamt = DD.refamt.copyToZonedDecimal().isAPartOf(dataFields, 366);
	public FixedLengthStringData mop = DD.reqntype.copy().isAPartOf(dataFields,384);
	public FixedLengthStringData payeeno = DD.payeesel.copy().isAPartOf(dataFields,385);
	public FixedLengthStringData payeenme = DD.payeename.copy().isAPartOf(dataFields,395);
//	public FixedLengthStringData bankaccno
	public FixedLengthStringData bnkcode = DD.bankkey.copy().isAPartOf(dataFields,425);
	public FixedLengthStringData bnkcdedsc = DD.bnkcdedsc.copy().isAPartOf(dataFields,435);	
	public FixedLengthStringData crdtcrdno = DD.crdtcrd.copy().isAPartOf(dataFields,465); 
	public FixedLengthStringData drctdbtno  = DD.bankacckey.copy().isAPartOf(dataFields,485);
	public FixedLengthStringData crdtcrdflg  = DD.crdtcrdflg.copy().isAPartOf(dataFields,505);
	public FixedLengthStringData drctdbtflg  = DD.drctdbtflg.copy().isAPartOf(dataFields,506);
	public FixedLengthStringData descn  = DD.descn.copy().isAPartOf(dataFields,507);
		
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(148).isAPartOf(dataArea, 542);
	
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);	
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData payfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData compnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData compcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData compdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData comprskcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData comprskdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData comppremcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData comppremdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);		
	public FixedLengthStringData compsumssErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData compbilprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData refprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData refadjustErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData refamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData payeenoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData payeenmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	//public FixedLengthStringData bankaccnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData bnkcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData bnkcdedscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData crdtcrdnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData drctdbtnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData crdtcrdflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData drctdbtflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData descnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(444).isAPartOf(dataArea, 690);	

	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);	
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] payfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);	
	public FixedLengthStringData[] compnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] compcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] compdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] comprskcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] comprskdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] comppremcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] comppremdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] compsumssOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] compbilprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] refprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] refadjustOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] refamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] payeenoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] payeenmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	//public FixedLengthStringData[] bankaccnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] bnkcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] bnkcdedscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] crdtcrdnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] drctdbtnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] crdtcrdflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] drctdbtflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] descnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
		
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);

	public LongData Sd5lhscreenWritten = new LongData(0);
	public LongData Sd5lhprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5lhScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		fieldIndMap.put(refadjustOut,new String[] {"06","07", "-06","08", null, null, null, null, null, null, null, null});
		fieldIndMap.put(mopOut,new String[] {"01","04", "-01","05", null, null, null, null, null, null, null, null});
		fieldIndMap.put(payeenoOut,new String[] {"02","09", "-02","10", null, null, null, null, null, null, null, null});		
		fieldIndMap.put(crdtcrdnoOut,new String[] {"03","11", "-03","12", null, null, null, null, null, null, null, null});
		fieldIndMap.put(drctdbtnoOut,new String[] {"13","14", "-13","15", null, null, null, null, null, null, null, null});
		fieldIndMap.put(refamtOut,new String[] {"16","17", "-16","18", null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {chdrnum,cnttype,ctypedes,chdrstatus,premstatus,cntcurr,register,cownnum,ownername,lifenum,lifename,currfrom,ptdate,btdate,payfreq,compno,compcode,compdesc,comprskcode,comprskdesc,comppremcode,comppremdesc,compsumss,compbilprm,	refprm,refadjust,refamt,mop,payeeno,payeenme,bnkcode,bnkcdedsc,crdtcrdno,drctdbtno,crdtcrdflg,drctdbtflg,descn};
		screenOutFields = new BaseData[][] {chdrnumOut,cnttypeOut,ctypedesOut,chdrstatusOut,premstatusOut,cntcurrOut,registerOut,cownnumOut,ownernameOut,lifenumOut,lifenameOut,currfromOut,ptdateOut,btdateOut,payfreqOut,compnoOut,compcodeOut,compdescOut,comprskcodeOut,comprskdescOut,comppremcodeOut,comppremdescOut,compsumssOut,compbilprmOut,refprmOut,refadjustOut,refamtOut,mopOut,payeenoOut,payeenmeOut,bnkcodeOut,bnkcdedscOut,crdtcrdnoOut,drctdbtnoOut,crdtcrdflgOut,drctdbtflgOut,descnOut};
		screenErrFields = new BaseData[] {chdrnumErr,cnttypeErr,ctypedesErr,chdrstatusErr,premstatusErr,cntcurrErr,registerErr,cownnumErr,ownernameErr,lifenumErr,lifenameErr,currfromErr,ptdateErr,btdateErr,payfreqErr,compnoErr,compcodeErr,compdescErr,comprskcodeErr,comprskdescErr,comppremcodeErr,comppremdescErr,compsumssErr,compbilprmErr,refprmErr,refadjustErr,refamtErr,mopErr,payeenoErr,payeenmeErr,bnkcodeErr,bnkcdedscErr,crdtcrdnoErr,drctdbtnoErr,crdtcrdflgErr,drctdbtflgErr,descnErr};
		screenDateFields = new BaseData[] {currfrom,ptdate,btdate};
		screenDateErrFields = new BaseData[] {currfromErr,ptdateErr,btdateErr};
		screenDateDispFields = new BaseData[] {currfromDisp,ptdateDisp,btdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		/*screenActionVar = action;*/
		screenRecord = Sd5lhscreen.class;
		protectRecord = Sd5lhprotect.class;
	}

}
