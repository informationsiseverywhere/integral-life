/*
 * File: P5024.java
 * Date: 29 August 2009 23:56:58
 * Author: Quipoz Limited
 * 
 * Class transformed from P5024.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.terminationclaims.recordstructures.P5024par;
import com.csc.life.terminationclaims.screens.S5024ScreenVars;
import com.csc.smart.dataaccess.BparTableDAM;
import com.csc.smart.dataaccess.BppdTableDAM;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2002.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*            P5024 MAINLINE.
*
*   Parameter prompt program for Pending Maturity/Expiry
*
****************************************************************** ****
* </pre>
*/
public class P5024 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5024");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private PackedDecimalData wsaaIntDatecfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaIntDatecto = new PackedDecimalData(8, 0);
	private final int wsaaMaxdate = 99991231;
		/* ERRORS */
	private static final String a123 = "A123";
	private static final String f571 = "F571";
	private static final String g730 = "G730";
	private static final String g640 = "G640";
	private static final String g639 = "G639";
		/* FORMATS */
	private static final String bscdrec = "BSCDREC";
	private static final String bsscrec = "BSSCREC";
	private static final String buparec = "BUPAREC";
	private static final String bppdrec = "BPPDREC";
	private static final String bparrec = "BPARREC";
	private BparTableDAM bparIO = new BparTableDAM();
	private BppdTableDAM bppdIO = new BppdTableDAM();
	private BscdTableDAM bscdIO = new BscdTableDAM();
	private BsscTableDAM bsscIO = new BsscTableDAM();
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private P5024par p5024par = new P5024par();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5024ScreenVars sv = ScreenProgram.getScreenVars( S5024ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090
	}

	public P5024() {
		super();
		screenVars = sv;
		new ScreenModel("S5024", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Retrieve Schedule.*/
		bsscIO.setFormat(bsscrec);
		bsscIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bsscIO);
		if (isNE(bsscIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bsscIO.getParams());
			fatalError600();
		}
		/* Retrieve Schedule Definition.*/
		bscdIO.setFormat(bscdrec);
		bscdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		/* Retrieve Parameter Prompt Definition.*/
		bppdIO.setFormat(bppdrec);
		bppdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bppdIO);
		if (isNE(bppdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bppdIO.getParams());
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		sv.scheduleName.set(bsscIO.getScheduleName());
		sv.scheduleNumber.set(bsscIO.getScheduleNumber());
		sv.effdate.set(bsscIO.getEffectiveDate());
		sv.acctmonth.set(bsscIO.getAcctMonth());
		sv.acctyear.set(bsscIO.getAcctYear());
		sv.jobq.set(bscdIO.getJobq());
		sv.bcompany.set(bppdIO.getCompany());
		sv.bbranch.set(bsscIO.getInitBranch());
		if (isNE(wsspcomn.flag, "I")
		&& isNE(wsspcomn.flag, "M")) {
			return ;
		}
		bparIO.setParams(SPACES);
		bparIO.setScheduleName(bsscIO.getScheduleName());
		bparIO.setCompany(bppdIO.getCompany());
		bparIO.setParmPromptProg(wsaaProg);
		bparIO.setBruntype(subString(wsspcomn.inqkey, 1, 8));
		bparIO.setBrunoccur(subString(wsspcomn.inqkey, 9, 3));
		bparIO.setEffectiveDate(bsscIO.getEffectiveDate());
		bparIO.setAcctmonth(bsscIO.getAcctMonth());
		bparIO.setAcctyear(bsscIO.getAcctYear());
		bparIO.setFunction(varcom.readr);
		bparIO.setFormat(bparrec);
		SmartFileCode.execute(appVars, bparIO);
		if (isNE(bparIO.getStatuz(), varcom.oK)
		&& isNE(bparIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(bparIO.getStatuz());
			syserrrec.params.set(bparIO.getParams());
			fatalError600();
		}
		if (isEQ(bparIO.getStatuz(), varcom.mrnf)) {
			scrnparams.errorCode.set(a123);
			return ;
		}
		p5024par.parmRecord.set(bparIO.getParmarea());
		sv.chdrnum.set(p5024par.chdrnum);
		sv.chdrnum1.set(p5024par.chdrnum1);
		sv.datecfrom.set(p5024par.datecfrom);
		sv.datecto.set(p5024par.datecto);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		/*    Validate fields*/
		/*  Validate the Date From and stored internal form into           */
		/*  WSAA-INT-DATECFROM.                                            */
		sv.errorIndicators.set(SPACES);
		datcon1rec.function.set(varcom.edit);
		datcon1rec.extDate.set(sv.datecfrom);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			sv.datecfromErr.set(g640);
		}
		else {
			wsaaIntDatecfrom.set(datcon1rec.intDate);
		}
		/*  Validate the Date To and stored internal form into             */
		/*  WSAA-INT-DATECTO.                                              */
		if (isEQ(sv.datecto, SPACES)) {
			datcon1rec.intDate.set(wsaaMaxdate);
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				syserrrec.statuz.set(datcon1rec.statuz);
				fatalError600();
			}
			sv.datecto.set(datcon1rec.extDate);
			goTo(GotoLabel.checkForErrors2080);
		}
		datcon1rec.function.set(varcom.edit);
		datcon1rec.extDate.set(sv.datecto);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			sv.datectoErr.set(g639);
		}
		else {
			wsaaIntDatecto.set(datcon1rec.intDate);
		}
		if (isEQ(sv.errorIndicators, SPACES)) {
			if (isGT(wsaaIntDatecfrom, wsaaIntDatecto)) {
				sv.datecfromErr.set(g730);
			}
		}
		if (isNE(sv.chdrnum, SPACES)
		&& isNE(sv.chdrnum1, SPACES)) {
			if (isGT(sv.chdrnum, sv.chdrnum1)) {
				sv.chdrnumfrmErr.set(f571);
				sv.chdrnumtoErr.set(f571);
			}
		}
		else {
			if (isEQ(sv.chdrnum, SPACES)
			&& isNE(sv.chdrnum1, SPACES)) {
				sv.chdrnumfrmErr.set(f571);
				sv.chdrnumtoErr.set(f571);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*LOAD-FIELDS*/
		moveParameters3100();
		writeParamRecord3200();
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATING SECTIONS
	* </pre>
	*/
protected void moveParameters3100()
	{
		/*MOVE-TO-PARM-RECORD*/
		p5024par.chdrnum.set(sv.chdrnum);
		p5024par.chdrnum1.set(sv.chdrnum1);
		p5024par.datecfrom.set(sv.datecfrom);
		p5024par.datecto.set(sv.datecto);
		p5024par.intDatecfrom.set(wsaaIntDatecfrom);
		p5024par.intDatecto.set(wsaaIntDatecto);
		/*EXIT*/
	}

protected void writeParamRecord3200()
	{
		writBupa3210();
	}

protected void writBupa3210()
	{
		bupaIO.setParams(SPACES);
		bupaIO.setScheduleName(sv.scheduleName);
		bupaIO.setScheduleNumber(sv.scheduleNumber);
		bupaIO.setEffectiveDate(sv.effdate);
		bupaIO.setAcctMonth(sv.acctmonth);
		bupaIO.setAcctYear(sv.acctyear);
		bupaIO.setCompany(sv.bcompany);
		bupaIO.setBranch(sv.bbranch);
		bupaIO.setParmarea(p5024par.parmRecord);
		bupaIO.setParmPromptProg(bppdIO.getParmPromptProg());
		bupaIO.setFormat(buparec);
		bupaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bupaIO);
		if (isNE(bupaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bupaIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
