package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.StringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.datadictionarydatatype.FLSDDObj;
import com.quipoz.framework.datatype.*;

/**
 * Screen variables for SR50D
 * @version 1.0 generated on 30/08/09 07:14
 * @author Quipoz
 */
public class Sa510ScreenVars extends SmartVarModel { 


public FixedLengthStringData dataArea = new FixedLengthStringData(213); //ILIFE-2472 
	public FixedLengthStringData dataFields = new FixedLengthStringData(101).isAPartOf(dataArea, 0); //ILIFE-2472
	
	//upper part
	public FixedLengthStringData notifinum = DD.aacct.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,22);
	public FixedLengthStringData claimant = DD.claimant.copy().isAPartOf(dataFields,69);
	public FixedLengthStringData clamnme = DD.clamnme.copy().isAPartOf(dataFields,77);
	public FixedLengthStringData relation = DD.relationwithowner.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData indxflg = DD.indxflg.copy().isAPartOf(dataFields,100);
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 101);
	public FixedLengthStringData notifinumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData claimantErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData clamnmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData relationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData indxflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 129);
	public FixedLengthStringData[] notifinumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] claimantOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] clamnmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] relationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] indxflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(3126);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(3027).isAPartOf(subfileArea, 0);
	public ZonedDecimalData select = DD.select.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData sequence = DD.seq.copyToZonedDecimal().isAPartOf(subfileFields,1);
	public FixedLengthStringData accdesc = DD.accdesc1.copy().isAPartOf(subfileFields,4);
	public ZonedDecimalData date = DD.date.copyToZonedDecimal().isAPartOf(subfileFields,3004);
	public FixedLengthStringData acctime = DD.acctime.copy().isAPartOf(subfileFields,3012);
	public FixedLengthStringData userID = DD.userID.copy().isAPartOf(subfileFields,3017);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 3027);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData sequenceErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData accdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData dateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData acctimeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData userIDErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 3051);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] sequenceOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] accdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] dateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] acctimeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] userIDOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 3123);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	
		
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	public LongData Sa510screenWritten = new LongData(0);
	public LongData Sa510protectWritten = new LongData(0);
	public LongData Sa510screensflWritten = new LongData(0);
	public LongData Sa510screenctlWritten = new LongData(0);
	public GeneralTable sa510screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData dateDisp = new FixedLengthStringData(10);
	
	public boolean hasSubfile() {
		return true;
	}
	public GeneralTable getScreenSubfileTable() {
		return sa510screensfl;
	}

	public Sa510ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(notifinumOut,new String[] {"96","40","-96","44", "90", null, null, null, null, null, null, null});	
		fieldIndMap.put(lifcnumOut,new String[] {"97","41","-97","45", "91", null, null, null, null, null, null, null});		
		fieldIndMap.put(lifenameOut,new String[] {"98","42","-97","45", "92", null, null, null, null, null, null, null});		
		fieldIndMap.put(claimantOut,new String[] {"99","43","-98","46", "93", null, null, null, null, null, null, null});	
		fieldIndMap.put(clamnmeOut,new String[] {"101","45","-100","48", "95", null, null, null, null, null, null, null});		
		fieldIndMap.put(relationOut,new String[] {"104","48","-103","51", "98", null, null, null, null, null, null, null});		
		fieldIndMap.put(relationOut,new String[] {"104","48","-103","51", "98", null, null, null, null, null, null, null});	
		fieldIndMap.put(selectOut,new String[] {"01","25","-01","02", null, null, null, null, null, null, null, null});
		fieldIndMap.put(indxflgOut,new String[] {"03","26","-03","04", null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {notifinum,lifcnum,lifename,claimant,clamnme,relation,indxflg};
		screenOutFields = new BaseData[][] {notifinumOut,lifcnumOut,lifenameOut,claimantOut,clamnmeOut,relationOut,indxflgOut };
		screenErrFields = new BaseData[] {notifinumErr,lifcnumErr,lifenameErr,notifinumErr,lifcnumErr,lifenameErr,claimantErr,clamnmeErr,relationErr,indxflgErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		
		screenSflFields = new BaseData[] {select, sequence, accdesc, date,acctime, userID};
		screenSflOutFields = new BaseData[][] {selectOut, sequenceOut, accdescOut, dateOut,acctimeOut, userIDOut};
		screenSflErrFields = new BaseData[] {selectErr, sequenceErr, accdescErr, dateErr,acctimeErr, userIDErr};
		screenSflDateFields = new BaseData[] {date};
		screenSflDateErrFields = new BaseData[] {dateErr};
		screenSflDateDispFields = new BaseData[] {dateDisp};
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sa510screen.class;
		screenSflRecord = Sa510screensfl.class;
		screenCtlRecord = Sa510screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sa510protect.class;
	}
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sa510screenctl.lrec.pageSubfile);
	}
}
