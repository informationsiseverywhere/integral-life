package com.csc.life.terminationclaims.dataaccess.dao;

import com.csc.life.terminationclaims.dataaccess.model.Covppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CovppfDAO extends BaseDAO<Covppf> {
	public void insertCovppfRecord(Covppf covppf);
	public Covppf getCovppfCrtable(String chdrcoy, String chdrnum, String crtable);//IBPLIFE-2133
	public int updateCovppfCrtable(Covppf covppf);//IBPLIFE-2133
	Covppf getCovppfCrtable(String chdrcoy, String chdrnum, String crtable,String coverage, String rider);
}
