package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:21
 * Description:
 * Copybook name: TR686REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr686rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr686Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData waivercode = new FixedLengthStringData(4).isAPartOf(tr686Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(496).isAPartOf(tr686Rec, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr686Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr686Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}