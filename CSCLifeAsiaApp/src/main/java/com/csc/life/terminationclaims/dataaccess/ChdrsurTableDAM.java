package com.csc.life.terminationclaims.dataaccess;

import com.csc.fsu.general.dataaccess.ChdrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChdrsurTableDAM.java
 * Date: Sun, 30 Aug 2009 03:32:43
 * Class transformed from CHDRSUR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChdrsurTableDAM extends ChdrpfTableDAM {

	public ChdrsurTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CHDRSUR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "SERVUNIT, " +
		            "CNTTYPE, " +
		            "POLINC, " +
		            "POLSUM, " +
		            "NXTSFX, " +
		            "TRANNO, " +
		            "TRANID, " +
		            "VALIDFLAG, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "AVLISU, " +
		            "STATCODE, " +
		            "STATDATE, " +
		            "STATTRAN, " +
		            "PSTCDE, " +
		            "PSTDAT, " +
		            "PSTTRN, " +
		            "TRANLUSED, " +
		            "OCCDATE, " +
		            "CCDATE, " +
		            "COWNPFX, " +
		            "COWNCOY, " +
		            "COWNNUM, " +
		            "JOWNNUM, " +
		            "CNTBRANCH, " +
		            "AGNTPFX, " +
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "CNTCURR, " +
		            "PAYPLAN, " +
		            "ACCTMETH, " +
		            "BILLFREQ, " +
		            "BILLCHNL, " +
		            "COLLCHNL, " +
		            "BILLDAY, " +
		            "BILLMONTH, " +
		            "BILLCD, " +
		            "BTDATE, " +
		            "PTDATE, " +
		            "SRCEBUS, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               servunit,
                               cnttype,
                               polinc,
                               polsum,
                               nxtsfx,
                               tranno,
                               tranid,
                               validflag,
                               currfrom,
                               currto,
                               avlisu,
                               statcode,
                               statdate,
                               stattran,
                               pstatcode,
                               pstatdate,
                               pstattran,
                               tranlused,
                               occdate,
                               ccdate,
                               cownpfx,
                               cowncoy,
                               cownnum,
                               jownnum,
                               cntbranch,
                               agntpfx,
                               agntcoy,
                               agntnum,
                               cntcurr,
                               payplan,
                               acctmeth,
                               billfreq,
                               billchnl,
                               collchnl,
                               billday,
                               billmonth,
                               billcd,
                               btdate,
                               ptdate,
                               srcebus,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(198);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getServunit().toInternal()
					+ getCnttype().toInternal()
					+ getPolinc().toInternal()
					+ getPolsum().toInternal()
					+ getNxtsfx().toInternal()
					+ getTranno().toInternal()
					+ getTranid().toInternal()
					+ getValidflag().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getAvlisu().toInternal()
					+ getStatcode().toInternal()
					+ getStatdate().toInternal()
					+ getStattran().toInternal()
					+ getPstatcode().toInternal()
					+ getPstatdate().toInternal()
					+ getPstattran().toInternal()
					+ getTranlused().toInternal()
					+ getOccdate().toInternal()
					+ getCcdate().toInternal()
					+ getCownpfx().toInternal()
					+ getCowncoy().toInternal()
					+ getCownnum().toInternal()
					+ getJownnum().toInternal()
					+ getCntbranch().toInternal()
					+ getAgntpfx().toInternal()
					+ getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getCntcurr().toInternal()
					+ getPayplan().toInternal()
					+ getAcctmeth().toInternal()
					+ getBillfreq().toInternal()
					+ getBillchnl().toInternal()
					+ getCollchnl().toInternal()
					+ getBillday().toInternal()
					+ getBillmonth().toInternal()
					+ getBillcd().toInternal()
					+ getBtdate().toInternal()
					+ getPtdate().toInternal()
					+ getSrcebus().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, servunit);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, polinc);
			what = ExternalData.chop(what, polsum);
			what = ExternalData.chop(what, nxtsfx);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, tranid);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, avlisu);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, statdate);
			what = ExternalData.chop(what, stattran);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, pstatdate);
			what = ExternalData.chop(what, pstattran);
			what = ExternalData.chop(what, tranlused);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, ccdate);
			what = ExternalData.chop(what, cownpfx);
			what = ExternalData.chop(what, cowncoy);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, jownnum);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, agntpfx);
			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, payplan);
			what = ExternalData.chop(what, acctmeth);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, collchnl);
			what = ExternalData.chop(what, billday);
			what = ExternalData.chop(what, billmonth);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, srcebus);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getServunit() {
		return servunit;
	}
	public void setServunit(Object what) {
		servunit.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public PackedDecimalData getPolinc() {
		return polinc;
	}
	public void setPolinc(Object what) {
		setPolinc(what, false);
	}
	public void setPolinc(Object what, boolean rounded) {
		if (rounded)
			polinc.setRounded(what);
		else
			polinc.set(what);
	}	
	public PackedDecimalData getPolsum() {
		return polsum;
	}
	public void setPolsum(Object what) {
		setPolsum(what, false);
	}
	public void setPolsum(Object what, boolean rounded) {
		if (rounded)
			polsum.setRounded(what);
		else
			polsum.set(what);
	}	
	public PackedDecimalData getNxtsfx() {
		return nxtsfx;
	}
	public void setNxtsfx(Object what) {
		setNxtsfx(what, false);
	}
	public void setNxtsfx(Object what, boolean rounded) {
		if (rounded)
			nxtsfx.setRounded(what);
		else
			nxtsfx.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getTranid() {
		return tranid;
	}
	public void setTranid(Object what) {
		tranid.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getAvlisu() {
		return avlisu;
	}
	public void setAvlisu(Object what) {
		avlisu.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public PackedDecimalData getStatdate() {
		return statdate;
	}
	public void setStatdate(Object what) {
		setStatdate(what, false);
	}
	public void setStatdate(Object what, boolean rounded) {
		if (rounded)
			statdate.setRounded(what);
		else
			statdate.set(what);
	}	
	public PackedDecimalData getStattran() {
		return stattran;
	}
	public void setStattran(Object what) {
		setStattran(what, false);
	}
	public void setStattran(Object what, boolean rounded) {
		if (rounded)
			stattran.setRounded(what);
		else
			stattran.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public PackedDecimalData getPstatdate() {
		return pstatdate;
	}
	public void setPstatdate(Object what) {
		setPstatdate(what, false);
	}
	public void setPstatdate(Object what, boolean rounded) {
		if (rounded)
			pstatdate.setRounded(what);
		else
			pstatdate.set(what);
	}	
	public PackedDecimalData getPstattran() {
		return pstattran;
	}
	public void setPstattran(Object what) {
		setPstattran(what, false);
	}
	public void setPstattran(Object what, boolean rounded) {
		if (rounded)
			pstattran.setRounded(what);
		else
			pstattran.set(what);
	}	
	public PackedDecimalData getTranlused() {
		return tranlused;
	}
	public void setTranlused(Object what) {
		setTranlused(what, false);
	}
	public void setTranlused(Object what, boolean rounded) {
		if (rounded)
			tranlused.setRounded(what);
		else
			tranlused.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public PackedDecimalData getCcdate() {
		return ccdate;
	}
	public void setCcdate(Object what) {
		setCcdate(what, false);
	}
	public void setCcdate(Object what, boolean rounded) {
		if (rounded)
			ccdate.setRounded(what);
		else
			ccdate.set(what);
	}	
	public FixedLengthStringData getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(Object what) {
		cownpfx.set(what);
	}	
	public FixedLengthStringData getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(Object what) {
		cowncoy.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public FixedLengthStringData getJownnum() {
		return jownnum;
	}
	public void setJownnum(Object what) {
		jownnum.set(what);
	}	
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}	
	public FixedLengthStringData getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(Object what) {
		agntpfx.set(what);
	}	
	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getPayplan() {
		return payplan;
	}
	public void setPayplan(Object what) {
		payplan.set(what);
	}	
	public FixedLengthStringData getAcctmeth() {
		return acctmeth;
	}
	public void setAcctmeth(Object what) {
		acctmeth.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public FixedLengthStringData getCollchnl() {
		return collchnl;
	}
	public void setCollchnl(Object what) {
		collchnl.set(what);
	}	
	public FixedLengthStringData getBillday() {
		return billday;
	}
	public void setBillday(Object what) {
		billday.set(what);
	}	
	public FixedLengthStringData getBillmonth() {
		return billmonth;
	}
	public void setBillmonth(Object what) {
		billmonth.set(what);
	}	
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(Object what) {
		srcebus.set(what);
	}
	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		servunit.clear();
		cnttype.clear();
		polinc.clear();
		polsum.clear();
		nxtsfx.clear();
		tranno.clear();
		tranid.clear();
		validflag.clear();
		currfrom.clear();
		currto.clear();
		avlisu.clear();
		statcode.clear();
		statdate.clear();
		stattran.clear();
		pstatcode.clear();
		pstatdate.clear();
		pstattran.clear();
		tranlused.clear();
		occdate.clear();
		ccdate.clear();
		cownpfx.clear();
		cowncoy.clear();
		cownnum.clear();
		jownnum.clear();
		cntbranch.clear();
		agntpfx.clear();
		agntcoy.clear();
		agntnum.clear();
		cntcurr.clear();
		payplan.clear();
		acctmeth.clear();
		billfreq.clear();
		billchnl.clear();
		collchnl.clear();
		billday.clear();
		billmonth.clear();
		billcd.clear();
		btdate.clear();
		ptdate.clear();
		srcebus.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}