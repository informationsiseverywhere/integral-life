/*
 * File: Ph613.java
 * Date: 30 August 2009 1:11:10
 * Author: Quipoz Limited
 * 
 * Class transformed from PH613.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.csc.common.DD;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5677rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.terminationclaims.procedures.Calprpm;
import com.csc.life.terminationclaims.recordstructures.Calprpmrec;
import com.csc.life.terminationclaims.screens.Sh613ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Td5j1rec;
import com.csc.life.terminationclaims.tablestructures.Th614rec;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.recordstructures.Msgboxrec;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * <pre>
 * Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
 *REMARKS.
 *
 *   Lapse Reinstatement Process
 *
 *   This is the Lapse Reinstatement Process program to reinstate
 *   or enquiry upon a lapsed policy. A Reinstatement fee is
 *   included which in turns based on the outstanding premium
 *   at the time of the reinstatement. This fee is calculated as
 *   an annual interest rate, which is applied to the premium
 *   at regular frequencies.
 *
 *   But Reinstatement will not be permitted if there are
 *   insufficient funds in the billing currency suspense account
 *   to cover both the outstanding premium and fee.
 *
 *Initialise
 *----------
 *  The  details of the contract being worked with will have been
 *  stored  in  the  CHDRMJA I/O module. Retrieve the details and
 *  set up the header portion of the screen.
 *
 *  Look up the following descriptions and names:
 *    -  Contract Type, (CNTTYPE) - long description from T5688,
 *    -  Contract  Status,  (STATCODE)  -  short description from
 *       T3623,
 *    -  Premium  Status,  (PSTATCODE)  -  short description from
 *       T3588,
 *    -  The owner's client (CLTS) details.
 *    -  The joint  owner's  client (CLTS) details if they exist.
 *
 *  Ensure that the last active transaction is a Lapse/Paid-up
 *  transaction by sequentially reading PTRNENQ and so as to
 *  get the Reverse Transactions details.
 *
 *    -  Payment frequency, Payment Method and Regular Premium
 *       from (PAYR) details
 *    -  the no. of O/S premium instalements = Today - PAYR-BILLCD
 *    -  Total O/S premium = Regular premium *
 *                           no. of O/S premium instalements.
 *    -  Reinstatement Fee = simple interest based on the
 *                           reinstatement rate for a certain
 *                           chargeable period +
 *                           compound interest of the rest
 *                           O/S instalment period if
 *                           capitalisation is exceeded.
 *
 *Validate.
 *--------
 *  If the suspense amount is not enough to cover the sum of
 *  outstanding premium and reinstatement fee, the warning will
 *  give the user to fill the discreprency into the suspense or
 *  amend the outstanding premium. Then the new outstanding
 *  premium and reinstatement fee will work out accordingly.
 *
 *Updating
 *--------
 *
 *  Call the AT module to perform the lapse reinstatement.
 *  Store Reinstatement Date and Reinstatement Fee in ATREQ
 *  copybook.
 *
 *Next Program
 *------------
 *
 *  Control will return to the sub-menu from this program.
 *
 *****************************************************************
 * </pre>
 */
public class Ph613 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH613");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaInstOutst = new ZonedDecimalData(2, 0).setUnsigned();
	/* Ticket #ILife-2374 started by vjain60 */
	private ZonedDecimalData wsaaToamount = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaToamountBill = new ZonedDecimalData(17, 2);
	protected ZonedDecimalData wsaaPremTot = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFee = new ZonedDecimalData(17, 2);
	protected ZonedDecimalData wsaaReinstatementFee = new ZonedDecimalData(17, 2);
	protected ZonedDecimalData wsaaSuspAvail = new ZonedDecimalData(17, 2);
	/* Ticket  #ILife-2374 finished by vjain60 */
	private ZonedDecimalData wsaaIntRate = new ZonedDecimalData(8, 5).setUnsigned();
	private ZonedDecimalData wsaaNewIntRate = new ZonedDecimalData(11, 5).setUnsigned();
	private ZonedDecimalData wsaaStdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaEnddate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNxtCapnDate = new ZonedDecimalData(8, 0).setUnsigned();
	protected ZonedDecimalData wsaaTolerance = new ZonedDecimalData(7, 2);
	protected ZonedDecimalData wsaaTolerance2 = new ZonedDecimalData(7, 2);

	protected FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(32);
	protected FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	protected ZonedDecimalData wsaaAdjustmentAmt = new ZonedDecimalData(9, 2);

	private FixedLengthStringData wsaaAgtTerminateFlag = new FixedLengthStringData(1);
	private Validator agtTerminated = new Validator(wsaaAgtTerminateFlag, "Y");
	private Validator agtNotTerminated = new Validator(wsaaAgtTerminateFlag, "N");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(6);

	private FixedLengthStringData wsaaFreqMatch = new FixedLengthStringData(1);
	private Validator freqMatch = new Validator(wsaaFreqMatch, "Y");

	private FixedLengthStringData wsaaProcessingMsg = new FixedLengthStringData(60);
	/*     ' Reverse Trans No.'.                                    */
	private FixedLengthStringData wsaaMsgProcess = new FixedLengthStringData(19).isAPartOf(wsaaProcessingMsg, 0).init(SPACES);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaProcessingMsg, 19, FILLER).init(SPACES);
	private ZonedDecimalData wsaaMsgTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaProcessingMsg, 20).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(3).isAPartOf(wsaaProcessingMsg, 25, FILLER).init(" - ");
	private FixedLengthStringData wsaaMsgTrantype = new FixedLengthStringData(4).isAPartOf(wsaaProcessingMsg, 28).init(SPACES);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaProcessingMsg, 32, FILLER).init(SPACES);
	private FixedLengthStringData wsaaMsgTrandesc = new FixedLengthStringData(27).isAPartOf(wsaaProcessingMsg, 33);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
	private PackedDecimalData wsaaPremTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFeeTax = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaMsgboxParams = new FixedLengthStringData(176);
	private FixedLengthStringData wsaaCpfmsg = new FixedLengthStringData(7).isAPartOf(wsaaMsgboxParams, 0);
	private FixedLengthStringData wsaaInsert = new FixedLengthStringData(100).isAPartOf(wsaaMsgboxParams, 7);
	private FixedLengthStringData wsaaReply = new FixedLengthStringData(65).isAPartOf(wsaaMsgboxParams, 107);
	private FixedLengthStringData wsaaResult = new FixedLengthStringData(4).isAPartOf(wsaaMsgboxParams, 172);
	
	private Msgboxrec msgboxrec = new Msgboxrec();
	private FixedLengthStringData preFunction = new FixedLengthStringData(50);
	private FixedLengthStringData preStatus = new FixedLengthStringData(50);
	
	
	private FixedLengthStringData wsaaPaxmsg = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaPaxmsg, 0);
	private FixedLengthStringData wsaaMsgid = new FixedLengthStringData(4).isAPartOf(wsaaPaxmsg, 1);
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);

	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Batckey wsaaBatchkey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Atreqrec atreqrec = new Atreqrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T3695rec t3695rec = new T3695rec();
	private T5645rec t5645rec = new T5645rec();
	private T5667rec t5667rec = new T5667rec();
	private Th614rec th614rec = new Th614rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Sh613ScreenVars sv = getLScreenVars();
	
	protected Sh613ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sh613ScreenVars.class);
	}  
	private ErrorsInner errorsInner = new ErrorsInner();
	private TablesInner tablesInner = new TablesInner();
	protected WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();
	//MIBT-240 STARTS
	private static final String b673 = "B673";
	private static final String t514 = "T514";
	private static final String t575 = "T575";
	//	MIBT-240 ENDS
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO",  PayrpfDAO.class);
	private Payrpf payrpf = new Payrpf();
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO",  AcblpfDAO.class);
	private Acblpf acblpf = new Acblpf();
	
	// ILIFE-3392: Starts
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	// ILIFE-3392: Ends
	//fwang3
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private static final String h017 = "H017";
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);

	private Batckey wsbbBatcKey = new Batckey();
    private Itempf itempf = null;
	private Itempf itempf1 = null;
    private T5688rec t5688rec = new T5688rec();
    private T5677rec t5677rec = new T5677rec();
    private FixedLengthStringData wsaaT5677Key = new FixedLengthStringData(8);
    private FixedLengthStringData wsaaT5677Tranno = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 0);
    private FixedLengthStringData wsaaT5677FollowUp = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 4);
	boolean CSMIN003Permission  = false;
	private static final String ta85 = "TA85";
	private Calprpmrec calprpmrec = new Calprpmrec();
	private Td5j1rec td5j1rec = new Td5j1rec();
	private static final String td5j1 = "TD5J1";
	private boolean reinstflag = false;
	
	
	private ZonedDecimalData wsaareratesub = new ZonedDecimalData(11, 5).setUnsigned();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Descpf descpf;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf;
	private Iterator<Lifepf> iterator ; 
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO",  PtrnpfDAO.class);
	private  Ptrnpf ptrnpf;
	private Boolean wsaaEof;
	private List<Itempf> tr52eList ;
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",  LifepfDAO.class);
	private  Lifepf lifepf;
	private  List<Covrpf> covrpfwopl = new ArrayList<Covrpf>();
	private PackedDecimalData wsaapremiumtax = new PackedDecimalData(17, 2);
	private HashMap<Long,List<Covrpf>> reratedPremKeeps  = new LinkedHashMap<>();
	List<Covrpf> covrpfList = null;
	
	
	//ILIFE-7857 start
	protected ZonedDecimalData wsaaLastPremTot = new ZonedDecimalData(17, 2);
	protected ZonedDecimalData wsaaLastFee = new ZonedDecimalData(17, 2);
	//ILIFE-7857 end 
	private boolean prmhldtrad = false;//ILIFE-8509
	private Calprpm calprpmph = getApplicationContext().getBean("prorateCalcUtils", Calprpm.class);
	private static final String tr7h = "TR7H";
	private static final String tr7d = "TR7D";
	private int reinstdate;
	private boolean reinstated = false;
	private boolean phtran = false;
	private List<Covrpf> covrUpdateList;
	private List<Covrpf> covrInsertList;
	private boolean covrReinstated = false;
	private Map<String,Descpf> descMap = new HashMap<>();
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO", AglfpfDAO.class);
    private Aglfpf aglfpf = null;
    
    private final String chdrmjarec = "CHDRMJAREC";
    
    private boolean lapseReinstJpnFlag = false;	
	private static final String CSLRI007="CSLRI007"; 
	private BigDecimal instprm = BigDecimal.ZERO;
	private PackedDecimalData wsaaPremTotal = new PackedDecimalData(17, 2);
	private boolean isAiaAusDirectDebit;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {

		exit2090, 
		exit2450, 
		exit2500,
		//		MIBT-240 STARTS
		exit1290
		//		MIBT-240 ENDS
	}

	public Ph613() {
		super();
		screenVars = sv;
		new ScreenModel("Sh613", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}


	/**
	 * <pre>
	 *      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	protected void initialise1000()
	{
		initialise1010();
	}

	protected void initialise1010()
	{	
		isAiaAusDirectDebit = FeaConfg.isFeatureExist("2", "BTPRO029", appVars, "IT");
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sv.dataArea.set(SPACES);
		wsaaEof = false;
		wsaapremiumtax.set(ZERO);
		initialize(sv.dataFields);
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.billcurr.set(chdrmjaIO.getBillcurr());
		sv.cownnum.set(chdrmjaIO.getCownnum());
		sv.ptdate.set(chdrmjaIO.getPtdate());
		sv.btdate.set(chdrmjaIO.getBtdate());
		sv.currfrom.set(chdrmjaIO.getOccdate());
		contractTypeStatusDesc();

	
		clntpf = clntpfDAO.searchClntRecord(fsupfxcpy.clnt.toString(), chdrmjaIO.getCowncoy().toString(), chdrmjaIO.getCownnum().toString());
		if(null == clntpf){
			syserrrec.params.set(fsupfxcpy.clnt.toString()+" "+ chdrmjaIO.getCowncoy().toString()+" "+ chdrmjaIO.getCownnum().toString());
			fatalError600();
		}
		if(!(clntpf.getValidflag().equals("1"))){
			sv.ownerdescErr.set(errorsInner.e304);
			sv.ownerdesc.set(SPACES);
		}
		else{
			plainname();
			sv.ownerdesc.set(wsspcomn.longconfname);	
		}

		List<Lifepf>  lifepfList = lifepfDAO.getLifeRecords(chdrmjaIO.getChdrcoy().toString(),chdrmjaIO.getChdrnum().toString() , "1");
		
		if(lifepfList.isEmpty()){
			syserrrec.params.set(chdrmjaIO.getChdrcoy().toString()+"  "+chdrmjaIO.getChdrnum().toString()+"  "+ "01");
			fatalError600();
		}
		else{
			iterator =   lifepfList.iterator();
			if(iterator.hasNext())
				lifepf = iterator.next();	
			if(lifepf.getLife().equals("01")&& lifepf.getJlife().equals("00")){
				sv.lifenum.set(lifepf.getLifcnum());
				plainname();
				sv.lifedesc.set(wsspcomn.longconfname);
			}
			else
			{
				sv.lifedescErr.set(errorsInner.e355);
				sv.lifedesc.set(SPACES);
			}

		}	
		if(iterator.hasNext()){
			lifepf = iterator.next();
			if(lifepf.getJlife().equals("01")){
				sv.jlife.set(lifepf.getLifcnum());
				plainname();
				sv.jlifedesc.set(wsspcomn.longconfname);	
			}
			else{
				sv.jlife.set(SPACES);
				sv.jlifedesc.set(SPACES);
			}
			
		}	
		reinstflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSLRI003", appVars, "IT");
		if(reinstflag){
			getTd5j1();
		}
		//ILIFE-8509
		prmhldtrad = FeaConfg.isFeatureExist(chdrmjaIO.getChdrcoy().toString(), "CSOTH010", appVars, "IT");
		// ILB-460
		payrpf = payrpfDAO.getpayrRecord(chdrmjaIO.getChdrcoy().toString(),
				chdrmjaIO.getChdrnum().toString(), 1);
		if (payrpf == null) {
			syserrrec.statuz.set(Varcom.mrnf);
			syserrrec.params.set(chdrmjaIO.getChdrcoy().toString() + " " +
					chdrmjaIO.getChdrnum().toString() + " 1");
			fatalError600();
		}
		
		sv.btdate.set(payrpf.getBtdate());
		sv.ptdate.set(payrpf.getPtdate());
		sv.billfreq.set(payrpf.getBillfreq());
		if(payrpf.getPstatcode().equals("LA") && isAiaAusDirectDebit && payrpf.getSinstamt06().compareTo(BigDecimal.ZERO) == 0 && isEQ(wsspcomn.flag,"I")){
			calcPremTotal();
			
		}
		else {
			calcPremTotal();
		}
		sv.mop.set(payrpf.getBillchnl());
		sv.effdate.set(wsspcomn.currfrom);
		// ILB-460
		wsaaBatckey.set(wsspcomn.batchkey2);
		List<Ptrnpf> ptrnpfList = ptrnpfDAO.searchPtrnenqRecord(chdrmjaIO.getChdrcoy().toString(),chdrmjaIO.getChdrnum().toString());

		if(ptrnpfList.isEmpty()){
			syserrrec.params.set("searchPtrnenqRecord" +"  "+chdrmjaIO.getChdrcoy()+"  "+ chdrmjaIO.getChdrnum());

			fatalError600();
		}

		Iterator<Ptrnpf> iteratorPtrnpf =   ptrnpfList.iterator();

		while(!(wsaaEof)){
			if(iteratorPtrnpf.hasNext()){
				ptrnpf = iteratorPtrnpf.next();
				processPtrn1200();
			}
			else
				wsaaEof = true;	

		}
		
		if(prmhldtrad) {//ILIFE-8509
			int phcount = 0;
			int phreinstcount = 0;
			for(Ptrnpf ptrn: ptrnpfList) {
				if(tr7d.equals(ptrn.getBatctrcde()))
					phreinstcount++;
				if(tr7h.equals(ptrn.getBatctrcde()))
					phcount++;
			}
			if( phcount == phreinstcount)
				reinstated = true;
			else
				reinstated = false;
		}

		List<Itempf> itemList = null;
		if (isEQ(ptrnpf.getBatctrcde(),wsaaBatckey.batcBatctrcde)) {
			wsaaMsgTranno.set(ptrnpf.getTranno());
			wsaaMsgTrantype.set(ptrnpf.getBatctrcde());
			getDesc1800();
			wsaaMsgTrandesc.set(descpf.getLongdesc());
			wsaaTr386Lang.set(wsspcomn.language);
			wsaaTr386Pgm.set(wsaaProg);
			wsaaTr386Id.set(SPACES);
			itemList = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), tablesInner.tr386.toString(), wsaaTr386Key.toString());
			if(itemList.isEmpty()){
				syserrrec.params.set(tablesInner.tr386);
				fatalError600();
			}
			tr386rec.tr386Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
			wsaaMsgProcess.set(tr386rec.progdesc01);
			sv.sfcdesc.set(wsaaProcessingMsg);
		}
		else {
			sv.sfcdesc.set(SPACES);
		}

		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype());
		stringVariable1.addExpression(chdrmjaIO.getCntcurr());
		stringVariable1.setStringInto(wsaaCntcurr);

		itemList = itemDAO.getAllItemitemByDateFrm(smtpfxcpy.item.toString(),chdrmjaIO.getChdrcoy().toString(),tablesInner.th614.toString().trim(),wsaaCntcurr.toString(), wsspcomn.currfrom.toInt());
		if(itemList.isEmpty()){
			scrnparams.function.set(Varcom.prot);
			scrnparams.errorCode.set(errorsInner.rl01);
			wsspcomn.edterror.set("Y");
			return;
		}
		th614rec.th614Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		sv.intanny.set(th614rec.intRate);
		itemList = itemDAO.getAllItemitem(smtpfxcpy.item.toString(), chdrmjaIO.getChdrcoy().toString(), tablesInner.t5645.toString().trim(), "PH613AT");
		if(itemList.isEmpty()){
		syserrrec.params.set(tablesInner.t5645);
		syserrrec.statuz.set(errorsInner.h134);
		fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));

		itemList = itemDAO.getAllItemitem(smtpfxcpy.item.toString(), chdrmjaIO.getChdrcoy().toString(), tablesInner.t3695.toString().trim(), t5645rec.sacstype01.toString());
		if(itemList.isEmpty()){
			syserrrec.params.set(tablesInner.t3695);
			syserrrec.statuz.set(errorsInner.h420);
			fatalError600();
		}
		t3695rec.t3695Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));

		// ILB-460
		acblpf = acblpfDAO.loadDataByBatch(chdrmjaIO.getChdrcoy().toString(), t5645rec.sacscode01.toString(),
				chdrmjaIO.getChdrnum().toString(), chdrmjaIO.getBillcurr().toString(),
				t5645rec.sacstype01.toString());
		if (acblpf == null) {
			wsaaSuspAvail.set(0);
		} else {
			PackedDecimalData sacscurbal = DD.sacscurbal.copy();
			sacscurbal.set(acblpf.getSacscurbal());
			if (isEQ(t3695rec.sign,"-")) {
				compute(wsaaSuspAvail, 2).set(mult(sacscurbal,-1));
			}
			else {
				wsaaSuspAvail.set(sacscurbal);
			}
		}
	
		sv.sacscurbal.set(wsaaSuspAvail);
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		datcon3rec.datcon3Rec.set(SPACES);
		// ILB-460
		datcon3rec.intDate1.set(payrpf.getPtdate());
//		datcon3rec.intDate1.set(payrIO.getBillcd());
		datcon3rec.intDate2.set(wsaaToday); 
		datcon3rec.frequency.set(payrpf.getBillfreq());
//		datcon3rec.frequency.set(payrIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaInstOutst.set(datcon3rec.freqFactor);
		wsaaInstOutst.add(1);
		datcon2rec.datcon2Rec.set(SPACES);
		// ILB-460
		datcon2rec.intDate1.set(payrpf.getPtdate());
//		datcon2rec.intDate1.set(payrIO.getPtdate());
		datcon2rec.freqFactor.set(wsaaInstOutst);
		datcon2rec.frequency.set(payrpf.getBillfreq());
//		datcon2rec.frequency.set(payrIO.getBillfreq());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,Varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError600();
		}
		sv.hprjptdate.set(datcon2rec.intDate2);
		// ILB-460
//		wsaaPremTot.set(payrIO.getSinstamt06());
//		wsaaPremTot.set(payrpf.getSinstamt06());
	  /*if(payrpf.getPstatcode().equals("LA") && isAiaAusDirectDebit && payrpf.getSinstamt06().compareTo(BigDecimal.ZERO) == 0 && isEQ(wsspcomn.flag,"I")){
			wsaaPremTot.set(wsaaPremTotal);
		}
		else {
			wsaaPremTot.set(payrpf.getSinstamt06());
		}*/
		wsaaPremTot.set(wsaaPremTotal);
		compute(wsaaPremTot, 2).set(mult(wsaaPremTot,wsaaInstOutst));
//		wsaaStdate.set(payrIO.getBillcd());
		wsaaStdate.set(payrpf.getBillcd());
		wsaaEnddate.set(0);
		wsaaReinstatementFee.set(0);
		dsp1200CustomerSpecific();
		while ( !(isGTE(wsaaEnddate,sv.effdate))) {
			if (isEQ(th614rec.compfreq,SPACES)) {
				wsaaEnddate.set(sv.effdate);
			}
			else {
				datcon2rec.datcon2Rec.set(SPACES);
				datcon2rec.intDate1.set(wsaaStdate);
				datcon2rec.freqFactor.set(1);
				datcon2rec.frequency.set(th614rec.compfreq);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz,Varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					fatalError600();
				}
				wsaaEnddate.set(datcon2rec.intDate2);
				wsaaNxtCapnDate.set(datcon2rec.intDate2);
				if (isGT(wsaaEnddate,sv.effdate)) {
					wsaaEnddate.set(sv.effdate);
				}
			}
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(wsaaStdate);
			datcon3rec.intDate2.set(wsaaEnddate);
			datcon3rec.frequency.set(th614rec.interestFrequency);
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,"****")) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
			wsaaIntRate.set(th614rec.intRate);
			wsaaNewIntRate.set(ZERO);
			compute(wsaaNewIntRate, 5).set(div((mult(wsaaIntRate,wsaaPremTot)),100));
			compute(wsaaFee, 5).set(mult(wsaaNewIntRate,datcon3rec.freqFactor));
			zrdecplrec.amountIn.set(wsaaFee);
			zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
			a000CallRounding();
			wsaaFee.set(zrdecplrec.amountOut);
			wsaaReinstatementFee.add(wsaaFee);
			if (isLT(wsaaNxtCapnDate,sv.effdate)) {
				/*wsaaPremTot.add(wsaaFee);*/ //  IBPLIFE-6926
				wsaaStdate.set(wsaaNxtCapnDate);
			}
		}

		getval1300CustomerSpecific();
		
		wsaaAdjustmentAmt.set(ZERO);
		calcPremTax2400();
		sv.hregp.add(wsaaPremTax);
		if(reinstflag && isNE(td5j1rec.subroutine,SPACES) || (prmhldtrad && !reinstated && phtran)){//ILIFE-8509
			calprpmrec.totlprem.set(ZERO);
			wsaaBatchkey.set(wsspcomn.batchkey);
			covrProLoop();
			//calcCntfeeTax2450();
			compute(wsaaPremTot, 2).set(add(calprpmrec.totlprem,wsaaPremTax));
			if(prmhldtrad && !reinstated && phtran) {//ILIFE-8509
				if(isEQ(calprpmrec.totlprem, ZERO))
					compute(wsaaPremTot, 2).set(add(wsaaPremTot, mult(wsaaPremTax, wsaaInstOutst)));
				else {
					compute(wsaaPremTot,2).set(add(wsaaPremTot, payrpf.getSinstamt02()));					
				}
			}
		}
		else{
			compute(wsaaPremTot, 2).set(add(wsaaPremTot, mult(wsaaPremTax, wsaaInstOutst)));
		}
		calcTotals2100();
		CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");	
		lapseReinstJpnFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), CSLRI007, appVars, "IT"); //IBPLIFE-3733
		if(lapseReinstJpnFlag){
			getTd5j1();
		}
	}
	
	protected void calcPremTotal(){
		instprm=covrpfDAO.getSumInstPrem(sv.chdrnum.toString());
		compute(wsaaPremTotal, 2).set(add(wsaaPremTotal,instprm));
		compute(wsaaPremTotal, 2).set(add(wsaaPremTotal,payrpf.getSinstamt02()));
		compute(wsaaPremTotal, 2).set(add(wsaaPremTotal,payrpf.getSinstamt03()));
		compute(wsaaPremTotal, 2).set(add(wsaaPremTotal,payrpf.getSinstamt04()));
		compute(wsaaPremTotal, 2).set(add(wsaaPremTotal,payrpf.getSinstamt05()));
		sv.hregp.set(wsaaPremTotal);
	}
	
	protected void contractTypeStatusDesc()
	{
		Map<String,String> statusDescMap = new HashMap<>();
		statusDescMap.put(tablesInner.t5688.toString(), chdrmjaIO.getCnttype().toString());
		statusDescMap.put(tablesInner.t3623.toString(), chdrmjaIO.getStatcode().toString());
		statusDescMap.put(tablesInner.t3588.toString(), chdrmjaIO.getPstatcode().toString());
		descMap = descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), statusDescMap);
		contractTypeStatus1070();
	}
	protected void contractTypeStatus1070()
		{
			
			if (descMap == null || descMap.isEmpty()) {
				sv.ctypedes.fill("?");
				sv.chdrstatus.fill("?");
				sv.premstatus.fill("?");
			}
			else {
				if(descMap.get(tablesInner.t5688.toString().trim())==null) {
					sv.ctypedes.fill("?");
				}
				else {
					sv.ctypedes.set(descMap.get(tablesInner.t5688.toString().trim()).getLongdesc());
				}
			
				if (descMap.get(tablesInner.t3623.toString().trim())==null) {
					sv.chdrstatus.fill("?");
				}
				else {
					sv.chdrstatus.set(descMap.get(tablesInner.t3623.toString().trim()).getShortdesc());
				}
				if (descMap.get(tablesInner.t3588.toString().trim())==null) {
					sv.premstatus.fill("?");
				}
				else {
					sv.premstatus.set(descMap.get(tablesInner.t3588.toString().trim()).getLongdesc());
				}
			}
		}
	
protected void getTd5j1(){
	itempf1 = new Itempf();

	itempf1.setItempfx(smtpfxcpy.item.toString());
	itempf1.setItemcoy(chdrmjaIO.getChdrcoy().toString().trim());
	itempf1.setItemtabl(td5j1);
	itempf1.setItemitem(chdrmjaIO.getCnttype().toString());
	itempf1 = itemDAO.getItemRecordByItemkey(itempf1);
	if (itempf1 == null) {
		itempf1 = new Itempf();
		itempf1.setItempfx("IT");
		itempf1.setItemcoy(chdrmjaIO.getChdrcoy().toString());
		itempf1.setItemtabl(td5j1);
		itempf1.setItemitem("***");
        itempf1 = itemDAO.getItempfRecord(itempf1);
        if(itempf1 == null){
        	syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat(td5j1).concat("***"));
			fatalError600();
        }
	}
	td5j1rec.td5j1Rec.set(StringUtil.rawToString(itempf1.getGenarea()));	
}

	//MIBT-240 STARTS
	protected void processPtrn1200()
	{
		try {
			start1210();
		}
		catch (GOTOException e){
		}
	}


	protected void start1210()
	{
		if ((isEQ(ptrnpf.getBatctrcde(),b673)) ||
				(isEQ(ptrnpf.getBatctrcde(),t514)) ||
				(isEQ(ptrnpf.getBatctrcde(),t575))) {
			wsaaEof = true;
			goTo(GotoLabel.exit1290);
		}
	}


	//MIBT-240 ENDS



	protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
	}

	protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}

	protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(clntpf.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}

	protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	protected void getDesc1800()
	{
		descpf=descDAO.getdescData(smtpfxcpy.item.toString(), tablesInner.t1688.toString().trim(), ptrnpf.getBatctrcde(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (null==descpf) {
			syserrrec.params.set(smtpfxcpy.item.toString() +"  "+tablesInner.t1688.toString()+"  "+ptrnpf.getBatctrcde()+"  "+wsspcomn.company.toString()+"  "+wsspcomn.language.toString());
			fatalError600();
		}

	}
	
	protected void dsp1200CustomerSpecific()
	{
		
	}
	
	protected void getval1300CustomerSpecific()
	{
		
	}

	protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(Varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsspcomn.flag,"I")) {
			sv.osbalOut[Varcom.pr.toInt()].set("Y");
		}
		return ;
	}

	protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2000();
			/*validateSelectionFields2070();*/
			checkForErrors2000();
		}
		catch (GOTOException e){
		}
	}

	protected void screenIo2010()
	{
		wsspcomn.edterror.set(Varcom.oK);
        if(CSMIN003Permission){
            //validate follow up status if R/W then continue
            wsbbBatcKey.set(wsspcomn.batchkey);
            if (isEQ(wsbbBatcKey.batcBatctrcde, ta85)) {
                chekIfRequired3410();
                readDefaultsTable3420();
                validateFollowUpStatus2a00();
            }
        }
	}

	/**
	 * <pre>
	 *2030-VALIDATE.                                                   
	 * </pre>
	 */
	protected void validate2000()
	{
		/* IF SH613-OSBAL         NOT  = WSAA-LAST-PREM-TOT OR          */
		/*    SH613-HRIFEECNT     NOT  = WSAA-LAST-FEE      OR  <LA3420>*/
		/*    SH613-XAMT          NOT  = WSAA-ADJUSTMENT-AMT    <LA3420>*/
		/*    MOVE SH613-OSBAL         TO WSAA-PREM-TOT                 */
		/*    MOVE SH613-HRIFEECNT     TO WSAA-REINSTATEMENT-FEE        */
		/*    MOVE SH613-XAMT          TO WSAA-ADJUSTMENT-AMT   <LA3420>*/
		/*    PERFORM 2100-CALC-TOTALS                                  */
		/*    MOVE  'Y'                TO WSSP-EDTERROR                 */
		/*    GO TO 2090-EXIT                                           */
		/* END-IF.                                                      */
		if (isNE(sv.osbal, ZERO)) {
			zrdecplrec.amountIn.set(sv.osbal);
			zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
			a000CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.osbal)) {
				sv.osbalErr.set(errorsInner.rfik);
			}
		}
		if (isNE(sv.hrifeecnt, ZERO)) {
			zrdecplrec.amountIn.set(sv.hrifeecnt);
			zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
			a000CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.hrifeecnt)) {
				sv.hrifeecntErr.set(errorsInner.rfik);
			}
		}
		if (isNE(sv.xamt, ZERO)) {
			zrdecplrec.amountIn.set(sv.xamt);
			zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
			a000CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.xamt)) {
				sv.xamtErr.set(errorsInner.rfik);
			}
		}
		wsaaPremTot.set(sv.osbal);
		wsaaReinstatementFee.set(sv.hrifeecnt);
		wsaaAdjustmentAmt.set(sv.xamt);
		calcTotals2100();
		if(reinstflag){
			medicalCheck();
			if (isNE(wsspcomn.edterror,"Y")) {
			if (isNE(scrnparams.statuz, varcom.calc )&&
					isNE(scrnparams.statuz,varcom.swch)) {
				
					if(isEQ(sv.msgPopup, SPACE)){
						sv.msgresult.set(SPACE);
						setMsgPoup();
						wsspcomn.edterror.set("Y");
						checkForErrors2000();
					}else {
						wsaaResult.set(varcom.oK);
						if (isNE(sv.msgresult, "Y")) {
							varcom.vrcmInit.set(SPACES);
							resetMsgData();
							wsspcomn.edterror.set("Y");
							checkForErrors2000();
						}
						resetMsgData();
					}
			}
		}
			else{
				wsspcomn.edterror.set("Y");
			}
	}
	
		if(lapseReinstJpnFlag && isNE(wsspcomn.flag,"I")){
			medicalCheckJpn();
			if (isNE(wsspcomn.edterror,"Y")) {
			if (isNE(scrnparams.statuz, varcom.calc )&&
					isNE(scrnparams.statuz,varcom.swch) && isGT(datcon3rec.freqFactor,td5j1rec.daycheck)) {
				
					if(isEQ(sv.msgPopup, SPACE)){
						sv.msgresult.set(SPACE);
						setMsgPoupJpn();
						wsspcomn.edterror.set("Y");
						checkForErrors2000();
					}else {
						wsaaResult.set(varcom.oK);
						if (isNE(sv.msgresult, "Y")) {
							varcom.vrcmInit.set(SPACES);
							resetMsgData();
							wsspcomn.edterror.set("Y");
							checkForErrors2000();
						}
						resetMsgData();
					}
			}
		}
			else{
				wsspcomn.edterror.set("Y");
			}
	}
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.edterror.set(Varcom.oK);
			goTo(GotoLabel.exit2090);
		}
	}

protected void medicalCheck()
{
	initialize(datcon3rec.datcon3Rec);
	datcon3rec.intDate1.set(sv.ptdate);
	datcon3rec.intDate2.set(sv.effdate);
	datcon3rec.frequency.set("DY");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}

	if(isGT(datcon3rec.freqFactor,td5j1rec.daycheck)){
		scrnparams.errorCode.set(errorsInner.rrj0);
	}
}

protected void medicalCheckJpn()
{
	initialize(datcon3rec.datcon3Rec);
	datcon3rec.intDate1.set(sv.ptdate);
	datcon3rec.intDate2.set(sv.effdate);
	datcon3rec.frequency.set("DY");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}	
}


protected void resetMsgData()
{
	sv.msgDisplay.set(ZERO);
	sv.msgresult.set(SPACE);
	sv.msgPopup.set(SPACE);
}
protected void setMsgPoup()
{
	wsaaLanguage.set(wsspcomn.language);
	wsaaMsgid.set("RRFO");
	msgboxrec.cpfmsg.set(wsaaPaxmsg);
	msgboxrec.insert.set(SPACES);
	msgboxrec.result.set(SPACES);
	msgboxrec.reply.set("X");
	msgbocrecScreen.cpfmsg.set(msgboxrec.cpfmsg);
	msgbocrecScreen.insert.set(msgboxrec.insert);
	msgbocrecScreen.reply.set(msgboxrec.reply);
	msgbocrecScreen.result.set(msgboxrec.result);
	proScreenPopup.set(wsaaProg);
	preFunction.set(scrnparams.function);
	preStatus.set(scrnparams.statuz);
	scrnparams.function.set(Varcom.popup);
	processScreen("S5026", sv);
	if(isNE(scrnparams.statuz, varcom.oK) 
	&& isNE(scrnparams.statuz, Varcom.ppup)){
	syserrrec.statuz.set(scrnparams.statuz);
	fatalError600();
}
scrnparams.function.set(preFunction);
scrnparams.statuz.set(preStatus);
sv.msgPopup.set(msgTextScreen);
}

protected void setMsgPoupJpn()
{
	wsaaLanguage.set(wsspcomn.language);
	wsaaMsgid.set("RUON");
	msgboxrec.cpfmsg.set(wsaaPaxmsg);
	msgboxrec.insert.set(SPACES);
	msgboxrec.result.set(SPACES);
	msgboxrec.reply.set("X");
	msgbocrecScreen.cpfmsg.set(msgboxrec.cpfmsg);
	msgbocrecScreen.insert.set(msgboxrec.insert);
	msgbocrecScreen.reply.set(msgboxrec.reply);
	msgbocrecScreen.result.set(msgboxrec.result);
	proScreenPopup.set(wsaaProg);
	preFunction.set(scrnparams.function);
	preStatus.set(scrnparams.statuz);
	scrnparams.function.set(Varcom.popup);
	processScreen("Sh613", sv);
	if(isNE(scrnparams.statuz, varcom.oK) 
	&& isNE(scrnparams.statuz, Varcom.ppup)){
	syserrrec.statuz.set(scrnparams.statuz);
	fatalError600();
}
scrnparams.function.set(preFunction);
scrnparams.statuz.set(preStatus);
sv.msgPopup.set(msgTextScreen);
}
    protected void validateFollowUpStatus2a00() {
        //In T5688, default is none, then break
        if (isEQ(t5688rec.defFupMeth, SPACES)) {
            return;
        }
		if (isEQ(t5677rec.fupcdess, SPACES)) {
			return;
		}
        boolean showOutstandingErr = false;
		List<String> flupList = new ArrayList<>();
        List<Fluppf> fluprevIOList = fluppfDAO.searchFlupRecordByChdrNum(chdrmjaIO.getChdrcoy().toString(),chdrmjaIO.getChdrnum().toString());
        if(fluprevIOList!=null&&!fluprevIOList.isEmpty()){
            for (Fluppf fluppf: fluprevIOList) {
                for (FixedLengthStringData fixedLengthStringData : t5677rec.fupcdes) {
					if(fixedLengthStringData !=null && !fixedLengthStringData.trim().equals("")) {
                        if (isEQ(fluppf.getFupCde(),fixedLengthStringData.toString().trim())) {
							flupList.add(fluppf.getFupCde());
                            if(isEQ(fluppf.getFupSts(),"R") || isEQ(fluppf.getFupSts(),"W")){
                                showOutstandingErr = false;
                            }else{
                                showOutstandingErr = true;
                                break;
                            }
                        }
                    }
                }
				if(showOutstandingErr){
					break;
				}
            }
			if(flupList.isEmpty()){
				return;
			}
            if(showOutstandingErr){
                scrnparams.errorCode.set(h017);
                wsspcomn.edterror.set("Y");
                goTo(GotoLabel.exit2090);
            }
        }
    }

	protected void chekIfRequired3410()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5688");
		itempf.setItemitem(chdrmjaIO.getCnttype().toString().trim());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if(itempf != null){
			t5688rec.t5688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}

	}

	protected void readDefaultsTable3420()
	{
		if (isEQ(t5688rec.defFupMeth, SPACES)) {
			return;
		}
		wsaaT5677Tranno.set(wsbbBatcKey.batcBatctrcde);
		wsaaT5677FollowUp.set(t5688rec.defFupMeth);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5677");
		itempf.setItemitem(wsaaT5677Key.toString().trim());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if(itempf != null){
			t5677rec.t5677Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

protected void checkForErrors2000()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}

	protected void calcTotals2100()
	{
		calcTot2110();
	}

	protected void calcTot2110()
	{
		calcFeeTax2500();
		sv.taxamt01.set(wsaaFeeTax);
		wsaaToamount.set(wsaaPremTot);
		sv.osbal.set(wsaaPremTot);
		wsaaToamount.add(wsaaFeeTax);
		wsaaToamount.add(wsaaReinstatementFee);
		wsaaToamount.add(wsaaAdjustmentAmt);
		sv.hrifeecnt.set(wsaaReinstatementFee);
		sv.toamount.set(wsaaToamount);
		// ILB-460
//		if (isNE(payrIO.getBillcurr(),payrIO.getCntcurr())) { 
		if (isNE(payrpf.getBillcurr(),payrpf.getCntcurr())) {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.amountIn.set(wsaaPremTot);
			conlinkrec.statuz.set(SPACES);
			conlinkrec.function.set("SURR");
			conlinkrec.currIn.set(payrpf.getCntcurr());
//			conlinkrec.currIn.set(payrIO.getCntcurr());
			conlinkrec.cashdate.set(varcom.vrcmMaxDate);
			conlinkrec.currOut.set(payrpf.getBillcurr());
//			conlinkrec.currOut.set(payrIO.getBillcurr());
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(chdrmjaIO.getChdrcoy());
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz,Varcom.oK)) {
				scrnparams.errorCode.set(conlinkrec.statuz);
				wsspcomn.edterror.set("Y");
				return ;
			}
			else {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				zrdecplrec.currency.set(conlinkrec.currOut);
				a000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
				sv.hbillosprm.set(conlinkrec.amountOut);
				wsaaToamountBill.set(conlinkrec.amountOut);
			}
			conlinkrec.amountIn.set(wsaaReinstatementFee);
			conlinkrec.amountOut.set(ZERO);
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz,Varcom.oK)) {
				scrnparams.errorCode.set(conlinkrec.statuz);
				wsspcomn.edterror.set("Y");
				return ;
			}
			else {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				zrdecplrec.currency.set(conlinkrec.currOut);
				a000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
				sv.hreinstfee.set(conlinkrec.amountOut);
				wsaaToamountBill.add(conlinkrec.amountOut);
				sv.zlstfndval.set(wsaaToamountBill);
			}
			conlinkrec.amountIn.set(wsaaAdjustmentAmt);
			conlinkrec.amountOut.set(ZERO);
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz,Varcom.oK)) {
				scrnparams.errorCode.set(conlinkrec.statuz);
				wsspcomn.edterror.set("Y");
				return ;
			}
			else {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				zrdecplrec.currency.set(conlinkrec.currOut);
				a000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
				sv.newamnt.set(conlinkrec.amountOut);
				wsaaToamountBill.add(conlinkrec.amountOut);
				sv.zlstfndval.set(wsaaToamountBill);
			}
			conlinkrec.amountIn.set(wsaaFeeTax);
			conlinkrec.amountOut.set(ZERO);
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, Varcom.oK)) {
				scrnparams.errorCode.set(conlinkrec.statuz);
				wsspcomn.edterror.set("Y");
				return ;
			}
			else {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				zrdecplrec.currency.set(conlinkrec.currOut);
				a000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
				sv.taxamt02.set(conlinkrec.amountOut);
			}
		}
		else {
			sv.hbillosprm.set(wsaaPremTot);
			sv.hreinstfee.set(wsaaReinstatementFee);
			sv.newamnt.set(wsaaAdjustmentAmt);
			sv.zlstfndval.set(wsaaToamount);
			sv.taxamt02.set(wsaaFeeTax);
		}
		sv.tolerance.set(0);
		wsaaTolerance.set(0);
		wsaaTolerance2.set(0);
		wsaaSuspAvail.set(sv.sacscurbal);
		if (isGT(sv.zlstfndval,wsaaSuspAvail)) {
			checkAgent2300();
			calcTolerance2200();
		}
		wsaaLastPremTot.set(sv.osbal);//ILIFE-7857
		wsaaLastFee.set(sv.hreinstfee);//ILIFE-7857
		wsaaSuspAvail.add(wsaaTolerance);
		if (isEQ(wsspcomn.flag,"C")) {
			if (isGT(sv.zlstfndval,wsaaSuspAvail)) {
				wsaaSuspAvail.subtract(wsaaTolerance);
				wsaaSuspAvail.add(wsaaTolerance2);
				if (isGT(sv.zlstfndval,wsaaSuspAvail)) {
					sv.sacscurbalErr.set(errorsInner.e961);
					wsspcomn.edterror.set("Y");
				}
			}
		}
	}

	protected void calcTolerance2200()
	{
		tolerance2210();
	}

	protected void tolerance2210()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.addExpression(chdrmjaIO.getBillcurr());

		List<Itempf> itemList = itemDAO.getAllItemitem(smtpfxcpy.item.toString(), chdrmjaIO.getChdrcoy().toString(), tablesInner.t5667.toString().trim(), stringVariable1.toString());
		if(itemList.isEmpty()){
			scrnparams.errorCode.set(errorsInner.f247);
			scrnparams.function.set(Varcom.prot);
			wsspcomn.edterror.set("Y");
			return ;
		}
		t5667rec.t5667Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));

		wsaaFreqMatch.set(SPACES);
		// ILB-460
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,11)
				|| freqMatch.isTrue()); wsaaCnt.add(1)){
			if (isEQ(t5667rec.freq[wsaaCnt.toInt()],payrpf.getBillfreq())) {
				freqMatch.setTrue();
				compute(wsaaCnt, 0).set(sub(wsaaCnt,1));
			}
		}
		if (freqMatch.isTrue()) {
//			wsaaTolerance.set(payrIO.getSinstamt06());
//			wsaaTolerance.set(payrpf.getSinstamt06());
			if(payrpf.getPstatcode().equals("LA") && isAiaAusDirectDebit && payrpf.getSinstamt06().compareTo(BigDecimal.ZERO) == 0 && isEQ(wsspcomn.flag,"I")){
				wsaaTolerance.set(wsaaPremTotal);
			}
			else {
				wsaaTolerance.set(payrpf.getSinstamt06());
			}
			wsaaTolerance.set(sv.zlstfndval);
			compute(wsaaTolerance, 2).set(div(mult(wsaaTolerance,t5667rec.prmtol[wsaaCnt.toInt()]),100));
			zrdecplrec.amountIn.set(wsaaTolerance);
			zrdecplrec.currency.set(payrpf.getBillcurr());
			a000CallRounding();
			wsaaTolerance.set(zrdecplrec.amountOut);
			if (isGT(wsaaTolerance,t5667rec.maxAmount[wsaaCnt.toInt()])) {
				wsaaTolerance.set(t5667rec.maxAmount[wsaaCnt.toInt()]);
			}
			if (isNE(t5667rec.maxamt[wsaaCnt.toInt()],0)) {
				if (agtNotTerminated.isTrue()
						|| (agtTerminated.isTrue()
								&& isEQ(t5667rec.sfind,"2"))) {
//					wsaaTolerance2.set(payrIO.getSinstamt06());
//					wsaaTolerance2.set(payrpf.getSinstamt06());
					if(payrpf.getPstatcode().equals("LA") && isAiaAusDirectDebit && payrpf.getSinstamt06().compareTo(BigDecimal.ZERO) == 0 && isEQ(wsspcomn.flag,"I")){
						wsaaTolerance2.set(wsaaPremTotal);
					}
					else {
						wsaaTolerance2.set(payrpf.getSinstamt06());
					}
					wsaaTolerance2.set(sv.zlstfndval);
					compute(wsaaTolerance2, 2).set(div(mult(wsaaTolerance2,t5667rec.prmtoln[wsaaCnt.toInt()]),100));
					zrdecplrec.amountIn.set(wsaaTolerance2);
					zrdecplrec.currency.set(payrpf.getBillcurr());
					a000CallRounding();
					wsaaTolerance2.set(zrdecplrec.amountOut);
					if (isGT(wsaaTolerance2,t5667rec.maxamt[wsaaCnt.toInt()])) {
						wsaaTolerance2.set(t5667rec.maxamt[wsaaCnt.toInt()]);
					}
				}
			}
		}
		if (isNE(wsaaTolerance2,0)) {
			sv.tolerance.set(wsaaTolerance2);
		}
		else {
			sv.tolerance.set(wsaaTolerance);
		}
	}

	protected void checkAgent2300()
	{
		readAglf2310();
	}

	protected void readAglf2310()
	{
		wsaaAgtTerminateFlag.set("N");

		aglfpf = aglfpfDAO.searchAglfRecord(chdrmjaIO.getAgntcoy().toString(), chdrmjaIO.getAgntnum().toString());
		if (aglfpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(chdrmjaIO.getAgntcoy().toString().concat(chdrmjaIO.getAgntnum().toString()));
			fatalError600();
		}
		if (isLT(aglfpf.getDtetrm(),datcon1rec.intDate)
				|| isLT(aglfpf.getDteexp(),datcon1rec.intDate)
				|| isGT(aglfpf.getDteapp(),datcon1rec.intDate)) {
			wsaaAgtTerminateFlag.set("Y");
		}
	}

	protected void calcPremTax2400()
	{
		/* Initialize and read TR52D                                       */
		wsaaPremTax.set(ZERO);
		sv.taxamt01Out[Varcom.nd.toInt()].set("Y");
		/* Read TR52D.                                                     */
		List<Itempf> itemList = itemDAO.getAllItemitem("IT", chdrmjaIO.getChdrcoy().toString(), tablesInner.tr52d.toString(), chdrmjaIO.getRegister().toString());
		if(itemList.isEmpty()){
			itemList = itemDAO.getAllItemitem("IT", chdrmjaIO.getChdrcoy().toString(), tablesInner.tr52d.toString(), "***");
			if(itemList.isEmpty()){
				syserrrec.params.set(tablesInner.tr52d);
				fatalError600();
			}
		}

		tr52drec.tr52dRec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));

		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		sv.taxamt01Out[Varcom.nd.toInt()].set("N");
		/* Read all records in COVRENQ under the same contract             */
		// ILIFE-3392: Starts
		Covrpf covrpfModel = new Covrpf();
		covrpfModel.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		covrpfModel.setChdrnum(chdrmjaIO.getChdrnum().toString());
		covrpfModel.setLife("01");
		covrpfModel.setPlanSuffix(ZERO.intValue());
		covrpfModel.setCoverage(SPACES.toString());
		covrpfModel.setRider(SPACES.toString());


		readTr52e(covrpfModel);
		covrLoop2410(covrpfModel);
		// ILIFE-3392: Ends
		calcCntfeeTax2450();
	}

	/**
	 * Loop through the list of COVRPF records associated with the Contrct and 
	 * calculate tax if condition is met.
	 * Introduced as part of ILIFE-3392
	 *
	 * @param covrpfModel the covrpf model
	 * @since ILIFE-16.3
	 */
	protected void covrLoop2410(Covrpf covrpfModel) {

		

		try{
			//ILIFE-7857 start
		//	covrpfList = covrpfDAO.searchCovrRecordForContract(covrpfModel);
			covrpfList = covrpfDAO.getCovrByComAndNum(chdrmjaIO.getChdrcoy().toString(),chdrmjaIO.getChdrnum().toString());
//ILIFE-7857 end
			for(Covrpf covrpf : covrpfList){				
				if(prmhldtrad && !reinstated && covrpf.getReinstated() != null && !covrpf.getReinstated().trim().isEmpty())//ILIFE-8509
					phtran = true;
				if(isNE(covrpf.getInstprem(), ZERO.intValue())
						&& isGT(covrpf.getPremCessDate() , chdrmjaIO.getPtdate().toInt())){
					calcCompTax2420(covrpf,1);
				}
		}
		
	}catch(SQLRuntimeException e){
			fatalError600();
		}

	}
	protected void covrProLoop() {
		wsaaPremTax.set(ZERO);
		reratedPremKeeps.clear();
		BigDecimal totalPrem = BigDecimal.ZERO;
		try{
			Incrpf incrIO = new Incrpf();
			for(Covrpf covrpf : covrpfList){
					covrReinstated=false;
					//calprpmrec.calprpmRec.set(SPACES);
				    calprpmrec.increasePrem.set(ZERO);
					calprpmrec.zlinstprem.set(ZERO);
					calprpmrec.rerateprem.set(ZERO);
					calprpmrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
					calprpmrec.chdrnum.set(chdrmjaIO.getChdrnum());
					calprpmrec.lastPTDate.set(payrpf.getPtdate());
					calprpmrec.nextPTDate.set(sv.hprjptdate);
					calprpmrec.reinstDate.set(sv.effdate);
					calprpmrec.transcode.set(wsaaBatchkey.batcBatctrcde);
					calprpmrec.language.set(wsspcomn.language);
					calprpmrec.uniqueno.set(covrpf.getUniqueNumber());
					if(!prmhldtrad)
						callProgram(td5j1rec.subroutine, calprpmrec.calprpmRec,reratedPremKeeps);
					else if(prmhldtrad && (covrpf.getReinstated()==null || !"Y".equals(covrpf.getReinstated()))){
						calprpmph.mainline(calprpmrec.calprpmRec,reratedPremKeeps, appVars, incrIO);
					}
					else if(prmhldtrad && (covrpf.getReinstated()!=null || "Y".equals(covrpf.getReinstated()))) {
						totalPrem = totalPrem.add(covrpf.getInstprem());
						covrReinstated = true;
					}
					if(!covrReinstated) {
						if (isNE(calprpmrec.statuz, varcom.oK)) {
							syserrrec.params.set(calprpmrec.statuz);
							fatalError600();
						}
						
						if(isEQ(calprpmrec.mode,"Y")){
							covrpfwopl.add(covrpf);
							calprpmrec.mode.set(SPACES);
							continue;
						}
						if(isNE(calprpmrec.rerateprem, ZERO)){
							compute(calprpmrec.prem, 2).set(add(calprpmrec.prem, calprpmrec.rerateprem));		
						}				
					
						if(isNE(calprpmrec.prem, ZERO.intValue())
								&& isGT(covrpf.getPremCessDate() , chdrmjaIO.getPtdate().toInt())){
							calcCompTax2420(covrpf,2);
						}
						calcFinalPrem(covrpf);
					}
				}
		
		if (!(covrpfwopl == null || covrpfwopl.size() == 0 )){
		for(Covrpf covrpf : covrpfwopl){
			calprpmrec.increasePrem.set(ZERO);
			calprpmrec.zlinstprem.set(ZERO);
			calprpmrec.rerateprem.set(ZERO);
			calprpmrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
			calprpmrec.chdrnum.set(chdrmjaIO.getChdrnum());
			calprpmrec.lastPTDate.set(payrpf.getPtdate());
			calprpmrec.nextPTDate.set(sv.hprjptdate);
			calprpmrec.reinstDate.set(sv.effdate);
			calprpmrec.transcode.set(wsaaBatchkey.batcBatctrcde);
			calprpmrec.language.set(wsspcomn.language);
			calprpmrec.uniqueno.set(covrpf.getUniqueNumber());
			calprpmrec.mode.set("T");	
			if(!prmhldtrad)
				callProgram(td5j1rec.subroutine, calprpmrec.calprpmRec,reratedPremKeeps);
			else if(prmhldtrad && (covrpf.getReinstated()==null || !"Y".equals(covrpf.getReinstated())))
				calprpmph.mainline(calprpmrec.calprpmRec,reratedPremKeeps, appVars, incrIO);
			else if(prmhldtrad && (covrpf.getReinstated()!=null || "Y".equals(covrpf.getReinstated()))) {
				totalPrem = totalPrem.add(covrpf.getInstprem());
				covrReinstated = true;
			}
			if(!covrReinstated) {
				if (isNE(calprpmrec.statuz, varcom.oK)) {
					syserrrec.params.set(calprpmrec.statuz);
					fatalError600();
				}	
				if(isNE(calprpmrec.rerateprem, ZERO)){
					if(!prmhldtrad)
						compute(calprpmrec.prem, 2).set(add(calprpmrec.prem, calprpmrec.rerateprem));
					else
						compute(calprpmrec.prem, 2).set(add(covrpf.getInstprem(), calprpmrec.rerateprem));
				}
				if(isNE(calprpmrec.prem, ZERO.intValue())
						&& isGT(covrpf.getPremCessDate() , chdrmjaIO.getPtdate().toInt())){
					calcCompTax2420(covrpf,2);
				}
				calcFinalPrem(covrpf);
			  }
			}
		}	
		
		if(covrReinstated)
			calprpmrec.totlprem.set(totalPrem);
	}catch(SQLRuntimeException e){
			fatalError600();
		}
	}
	
		
	protected void calcCompTax2420(Covrpf covrpf,int woplBen)
	{
		
		if(tr52eList.isEmpty()){
			return;
		}
		tr52erec.tr52eRec.set(StringUtil.rawToString(tr52eList.get(0).getGenarea()));
		if (isNE(tr52erec.taxind01, "Y")) {
			return ;
		}
		/* Call TR52D tax subroutine                                       */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
		// ILIFE-3392: Starts
		// Begin conversion from DAM to DAO
		txcalcrec.life.set(covrpf.getLife());
		txcalcrec.coverage.set(covrpf.getCoverage());
		txcalcrec.rider.set(covrpf.getRider());
		txcalcrec.planSuffix.set(covrpf.getPlanSuffix());
		txcalcrec.crtable.set(covrpf.getCrtable());
		txcalcrec.cnttype.set(chdrmjaIO.getCnttype());
		txcalcrec.register.set(chdrmjaIO.getRegister());
		txcalcrec.transType.set("PREM");
		txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
		txcalcrec.effdate.set(sv.effdate);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCntcurr());
		stringVariable1.addExpression(tr52erec.txitem);
		stringVariable1.setStringInto(txcalcrec.rateItem);
		if (isEQ(tr52erec.zbastyp, "Y")) {
				if(isEQ(woplBen,1)) 
					compute(txcalcrec.amountIn, 2).set(sub(covrpf.getInstprem(), covrpf.getZlinstprem()));				
				else 
					compute(txcalcrec.amountIn, 2).set(sub(calprpmrec.prem, calprpmrec.zlinstprem));
		}	
		else {
			if(isEQ(woplBen,1))
				txcalcrec.amountIn.set(covrpf.getInstprem());
			else
				txcalcrec.amountIn.set(calprpmrec.prem);
			
				
		}
		// ILIFE-3392: Ends
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
				|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaPremTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaPremTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

	protected void calcCntfeeTax2450()
	{
		start2450();
	}

	protected void start2450()
	{
		// ILB-460
//		if (isEQ(payrIO.getSinstamt02(), ZERO)) {
		if (isEQ(payrpf.getSinstamt02(), ZERO)) {
			return;
		}

		if(!(tr52eList.isEmpty())){
			gotCntfeeTr52e2450();
			return;
		}
		else{
			syserrrec.params.set(wsaaTr52eKey.toString());
			fatalError600();
		}	
	}

	protected void gotCntfeeTr52e2450()
	{
		tr52erec.tr52eRec.set(StringUtil.rawToString(tr52eList.get(0).getGenarea()));
		if (isNE(tr52erec.taxind02, "Y")) {
			return ;
		}
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.planSuffix.set(0);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.cnttype.set(chdrmjaIO.getCnttype());
		txcalcrec.register.set(chdrmjaIO.getRegister());
		txcalcrec.transType.set("CNTF");
		txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
		txcalcrec.effdate.set(sv.effdate);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCntcurr());
		stringVariable1.addExpression(tr52erec.txitem);
		stringVariable1.setStringInto(txcalcrec.rateItem);
		// ILB-460
//		txcalcrec.amountIn.set(payrIO.getSinstamt02());
		txcalcrec.amountIn.set(payrpf.getSinstamt02());
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
				|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaPremTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaPremTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

	protected void calcFeeTax2500()
	{
		start2500();
	}

	protected void start2500()
	{
		wsaaFeeTax.set(ZERO);
		if ((setPrecision(ZERO, 2)
				&& isEQ((add(wsaaReinstatementFee, wsaaAdjustmentAmt)), ZERO))
				|| isEQ(tr52drec.txcode, SPACES)) {
			return;
		}

		if(!(tr52eList.isEmpty())){
			gotTr52e2500();
			return;
		}
		else{
			syserrrec.params.set(wsaaTr52eKey);	
			fatalError600();
		}
	}

	protected void gotTr52e2500()
	{
		tr52erec.tr52eRec.set(StringUtil.rawToString(tr52eList.get(0).getGenarea()));
		if (isNE(tr52erec.taxind12, "Y")) {
			return ;
		}
		/* Call TR52D tax subroutine                                       */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrmjaIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.planSuffix.set(0);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.cnttype.set(chdrmjaIO.getCnttype());
		txcalcrec.register.set(chdrmjaIO.getRegister());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.transType.set("RSTF");
		txcalcrec.ccy.set(chdrmjaIO.getCntcurr());
		txcalcrec.effdate.set(sv.effdate);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		txcalcrec.rateItem.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCntcurr());
		stringVariable1.addExpression(tr52erec.txitem);
		stringVariable1.setStringInto(txcalcrec.rateItem);
		compute(txcalcrec.amountIn, 2).set(add(wsaaReinstatementFee, wsaaAdjustmentAmt));
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
				|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

	/**
	 * <pre>
	 *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000()
	{
		updateDatabase3010();
	}

	protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(Varcom.oK);
		sftlockrec.enttyp.set(chdrmjaIO.getChdrpfx());
		sftlockrec.company.set(chdrmjaIO.getChdrcoy());
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,Varcom.oK)
				&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("PH613AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrmjaIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransAreaInner.wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransAreaInner.wsaaTranDate.set(varcom.vrcmDate);
		wsaaTransAreaInner.wsaaTranTime.set(varcom.vrcmTime);
		wsaaTransAreaInner.wsaaUser.set(varcom.vrcmUser);
		wsaaTransAreaInner.wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransAreaInner.wsaaPlnsfx.set(ZERO);
		if(!prmhldtrad)//ILIFE-8509
			wsaaTransAreaInner.wsaaSuppressTo.set(ZERO);
		else
			wsaaTransAreaInner.wsaaSuppressTo.set(sv.hprjptdate);
		wsaaTransAreaInner.wsaaCfiafiTranno.set(ZERO);
		wsaaTransAreaInner.wsaaBbldat.set(ZERO);
		wsaaTransAreaInner.wsaaCfiafiTranCode.set(SPACES);
		wsaaTransAreaInner.wsaaSupflag.set("N");
		wsaaTransAreaInner.wsaaReinstDate.set(wsspcomn.currfrom);
		wsaaTransAreaInner.wsaaTranCode.set(ptrnpf.getBatctrcde());
		wsaaTransAreaInner.wsaaTranNum.set(ptrnpf.getTranno());
		wsaaTransAreaInner.wsaaTodate.set(ptrnpf.getPtrneff());
		if (isNE(wsaaAdjustmentAmt,ZERO)) {
			wsaaReinstatementFee.add(wsaaAdjustmentAmt);
		}
		wsaaTransAreaInner.wsaaTotalfee.set(wsaaReinstatementFee);
		atreqrec.transArea.set(wsaaTransAreaInner.wsaaTransArea);
		atreqrec.statuz.set(Varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,Varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			syserrrec.statuz.set(atreqrec.statuz);
			fatalError600();
		}
	}

	protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(Varcom.oK);
		/* MOVE PAYR-BILLCURR          TO ZRDP-CURRENCY.                */
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
	protected void readTr52e(Covrpf covrpf)
	{
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
		// ILIFE-3392: Starts
		wsaaTr52eCrtable.set(covrpf.getCrtable());

		tr52eList = itemDAO.getAllItemitem("IT", chdrmjaIO.getChdrcoy().toString(), tablesInner.tr52e.toString(), wsaaTr52eKey.toString());
		if(tr52eList.isEmpty()){
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrmjaIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			tr52eList = itemDAO.getAllItemitem("IT", chdrmjaIO.getChdrcoy().toString(), tablesInner.tr52e.toString(), wsaaTr52eKey.toString());
			if(tr52eList.isEmpty()){
				wsaaTr52eKey.set(SPACES);
				wsaaTr52eTxcode.set(tr52drec.txcode);
				wsaaTr52eCnttype.set("***");
				wsaaTr52eCrtable.set("****");
				tr52eList = itemDAO.getAllItemitem("IT", chdrmjaIO.getChdrcoy().toString(), tablesInner.tr52e.toString(), wsaaTr52eKey.toString());	
				if(tr52eList.isEmpty()){
					syserrrec.params.set("IT"+"  "+chdrmjaIO.getChdrcoy().toString()+"  "+ tablesInner.tr52e.toString()+"  "+wsaaTr52eKey.toString());
					fatalError600();
				}
			}
		}

	}
	protected void calcFinalPrem(Covrpf covrpf){
		if(isNE(calprpmrec.prem, ZERO)){
		/*compute(calprpmrec.prem, 2).set(sub(calprpmrec.prem,covrpf.getInstprem()));*/
		calprpmrec.totlprem.add(calprpmrec.prem);
	  }
	}
	
protected void callDatcon5500()
{
	datcon3rec.function.set("CMDF");
	datcon3rec.frequency.set(chdrmjaIO.getBillfreq());
    datcon3rec.intDate2.set(calprpmrec.nextPTDate);
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	else {
		wsaareratesub.set(datcon3rec.freqFactor);
	}
	
	/*EXIT*/
}

/*
 * Class transformed  from Data Structure WSAA-TRANS-AREA--INNER
 */
protected static final class WsaaTransAreaInner { 

	public FixedLengthStringData wsaaTransArea = new FixedLengthStringData(95);
	public FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	public ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	public ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	public ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	public FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	public FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	public ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	public ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	public ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	public ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	public FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	public FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	public ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	public ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
	public FixedLengthStringData wsaaReinstDate = new FixedLengthStringData(8).isAPartOf(wsaaTransArea, 70);
	public ZonedDecimalData wsaaTotalfee = new ZonedDecimalData(17, 2).isAPartOf(wsaaTransArea, 78);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData e961 = new FixedLengthStringData(4).init("E961");
	private FixedLengthStringData f247 = new FixedLengthStringData(4).init("F247");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData h134 = new FixedLengthStringData(4).init("H134");
	private FixedLengthStringData h420 = new FixedLengthStringData(4).init("H420");
	private FixedLengthStringData rl01 = new FixedLengthStringData(4).init("RL01");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData rrj0 = new FixedLengthStringData(4).init("RRJ0");
	private FixedLengthStringData ruon = new FixedLengthStringData(4).init("RUON");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 

		/* TABLES */
		private FixedLengthStringData t1688 = new FixedLengthStringData(6).init("T1688");
		private FixedLengthStringData t3588 = new FixedLengthStringData(6).init("T3588");
		private FixedLengthStringData t3623 = new FixedLengthStringData(6).init("T3623");
		private FixedLengthStringData t3695 = new FixedLengthStringData(6).init("T3695");
		private FixedLengthStringData t5645 = new FixedLengthStringData(6).init("T5645");
		private FixedLengthStringData t5667 = new FixedLengthStringData(6).init("T5667");
		private FixedLengthStringData t5688 = new FixedLengthStringData(6).init("T5688");
		private FixedLengthStringData th614 = new FixedLengthStringData(6).init("TH614");
		private FixedLengthStringData tr386 = new FixedLengthStringData(5).init("TR386");
		private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
		private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	}

}
