package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;

public class Hsudpf {
	private String chdrcoy;
	private String chdrnum;
	private int tranno;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private String crtable;
	private int planSuffix;
	private BigDecimal hactval;
	private BigDecimal hemv;
	private String hcnstcur;
	private String fieldType;
	private String userProfile;
	private String jobName;
	private String datime;
	private long uniqueNumber;

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public int getPlanSuffix() {
		return planSuffix;
	}

	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	public BigDecimal getHactval() {
		return hactval;
	}

	public void setHactval(BigDecimal hactval) {
		this.hactval = hactval;
	}

	public BigDecimal getHemv() {
		return hemv;
	}

	public void setHemv(BigDecimal hemv) {
		this.hemv = hemv;
	}

	public String getHcnstcur() {
		return hcnstcur;
	}

	public void setHcnstcur(String hcnstcur) {
		this.hcnstcur = hcnstcur;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

}
