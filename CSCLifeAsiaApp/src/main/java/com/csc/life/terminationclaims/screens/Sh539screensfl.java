package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class Sh539screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 4;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {10}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {15, 17, 2, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh539ScreenVars sv = (Sh539ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sh539screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sh539screensfl, 
			sv.Sh539screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sh539ScreenVars sv = (Sh539ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sh539screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sh539ScreenVars sv = (Sh539ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sh539screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sh539screensflWritten.gt(0))
		{
			sv.sh539screensfl.setCurrentIndex(0);
			sv.Sh539screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sh539ScreenVars sv = (Sh539ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sh539screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sh539ScreenVars screenVars = (Sh539ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.riskCessDateDisp.setFieldName("riskCessDateDisp");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.hetiosi.setFieldName("hetiosi");
				screenVars.hsurval.setFieldName("hsurval");
				screenVars.hetinsi.setFieldName("hetinsi");
				screenVars.hrefval.setFieldName("hrefval");
				screenVars.etiyear.setFieldName("etiyear");
				screenVars.etidays.setFieldName("etidays");
				screenVars.zrdateDisp.setFieldName("zrdateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.riskCessDateDisp.set(dm.getField("riskCessDateDisp"));
			screenVars.life.set(dm.getField("life"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.hetiosi.set(dm.getField("hetiosi"));
			screenVars.hsurval.set(dm.getField("hsurval"));
			screenVars.hetinsi.set(dm.getField("hetinsi"));
			screenVars.hrefval.set(dm.getField("hrefval"));
			screenVars.etiyear.set(dm.getField("etiyear"));
			screenVars.etidays.set(dm.getField("etidays"));
			screenVars.zrdateDisp.set(dm.getField("zrdateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sh539ScreenVars screenVars = (Sh539ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.riskCessDateDisp.setFieldName("riskCessDateDisp");
				screenVars.life.setFieldName("life");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.hetiosi.setFieldName("hetiosi");
				screenVars.hsurval.setFieldName("hsurval");
				screenVars.hetinsi.setFieldName("hetinsi");
				screenVars.hrefval.setFieldName("hrefval");
				screenVars.etiyear.setFieldName("etiyear");
				screenVars.etidays.setFieldName("etidays");
				screenVars.zrdateDisp.setFieldName("zrdateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("riskCessDateDisp").set(screenVars.riskCessDateDisp);
			dm.getField("life").set(screenVars.life);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("hetiosi").set(screenVars.hetiosi);
			dm.getField("hsurval").set(screenVars.hsurval);
			dm.getField("hetinsi").set(screenVars.hetinsi);
			dm.getField("hrefval").set(screenVars.hrefval);
			dm.getField("etiyear").set(screenVars.etiyear);
			dm.getField("etidays").set(screenVars.etidays);
			dm.getField("zrdateDisp").set(screenVars.zrdateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sh539screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sh539ScreenVars screenVars = (Sh539ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.riskCessDateDisp.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.hetiosi.clearFormatting();
		screenVars.hsurval.clearFormatting();
		screenVars.hetinsi.clearFormatting();
		screenVars.hrefval.clearFormatting();
		screenVars.etiyear.clearFormatting();
		screenVars.etidays.clearFormatting();
		screenVars.zrdateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sh539ScreenVars screenVars = (Sh539ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.riskCessDateDisp.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.hetiosi.setClassString("");
		screenVars.hsurval.setClassString("");
		screenVars.hetinsi.setClassString("");
		screenVars.hrefval.setClassString("");
		screenVars.etiyear.setClassString("");
		screenVars.etidays.setClassString("");
		screenVars.zrdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sh539screensfl
 */
	public static void clear(VarModel pv) {
		Sh539ScreenVars screenVars = (Sh539ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.riskCessDateDisp.clear();
		screenVars.riskCessDate.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.hetiosi.clear();
		screenVars.hsurval.clear();
		screenVars.hetinsi.clear();
		screenVars.hrefval.clear();
		screenVars.etiyear.clear();
		screenVars.etidays.clear();
		screenVars.zrdateDisp.clear();
		screenVars.zrdate.clear();
	}
}
