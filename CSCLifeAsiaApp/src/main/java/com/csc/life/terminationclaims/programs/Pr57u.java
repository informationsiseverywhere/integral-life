package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;


import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ZhlbpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.ZhlbpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Zhlbpf;
import com.csc.life.terminationclaims.dataaccess.model.Zhlcpf;
import com.csc.life.terminationclaims.screens.Sr57uScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;

import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;

import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;

import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


public class Pr57u extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR57U");
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaDeadLife = new FixedLengthStringData(8);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
		/* ERRORS */
	private static final String e304 = "E304";
		/* TABLES */

	private static final String t5688 = "T5688";
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5548 = "T5548";
	private static final String clmhclmrec = "CLMHCLMREC";
	

	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM();
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();

	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();

	private LifeclmTableDAM lifeclmIO = new LifeclmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	

	private Gensswrec gensswrec = new Gensswrec();	
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr57uScreenVars sv = ScreenProgram.getScreenVars( Sr57uScreenVars.class);
	private WsaaTransactionRecInner wsaaTransactionRecInner = new WsaaTransactionRecInner();	
	private Zhlbpf zhlbpf = new Zhlbpf();
	private ZhlbpfDAO zhlbpfDAO = new ZhlbpfDAOImpl();
	private List<Zhlbpf> ls= null;
	private Iterator lsIterator = null;
	
	//ILB-459
  	private Chdrpf chdrpf = new Chdrpf();
  	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
  //ILJ-49 Starts
  	private boolean cntDteFlag = false;
  	private String cntDteFeature = "NBPRP113";
  	//ILJ-49 End 
  	protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	protected Cattpf cattpf = new Cattpf();	 
  	boolean CMDTH010Permission  = false;
  	private static final String feaConfigPreRegistartion= "CMDTH010";


	public Pr57u() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57u", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

protected void initialise1000()
	{
		initialise1010();
		
	}

protected void initialise1010()
	{	
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
		
			return;
		}
	                      
		wsaaActualTot.set(0);
		callDatcons1300();
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", feaConfigPreRegistartion, appVars, "IT");
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("Sr57u", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Dummy field initilisation for prototype version.*/
		sv.totclaim.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.totclaim.set(ZERO);
		sv.dtofdeath.set(varcom.vrcmMaxDate);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		//ILB-459 starts
		/*chdrclmIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}*/
		
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrclmIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrclmIO);
			if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrclmIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrclmIO.getChdrcoy().toString(), chdrclmIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		//ILB-459 ends
		
		/* Currency Code on the screen now defaults to Contract Currency   */

		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.occdate.set(chdrpf.getOccdate());
		/*    Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}

		sv.cownnum.set(chdrpf.getCownnum());
		cltsIO.setClntnum(chdrpf.getCownnum());
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.ptdate.set(chdrpf.getPtdate());
		sv.btdate.set(chdrpf.getBtdate());
		sv.currcd.set(chdrpf.getCntcurr());
		
		if(!CMDTH010Permission){
			sv.claimnumberOut[varcom.nd.toInt()].set("Y");
		
		}

		/* read claim header record*/
		clmhclmIO.setDataArea(SPACES);
		clmhclmIO.setChdrcoy(chdrpf.getChdrcoy());
		clmhclmIO.setChdrnum(chdrpf.getChdrnum());
		clmhclmIO.setFunction(varcom.readr);
		clmhclmIO.setFormat(clmhclmrec);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
		if (isEQ(clmhclmIO.getJlife(), "01")) {
			sv.astrsk.set("*");
		}
		else {
			sv.asterisk.set("*");
		}
		sv.effdate.set(clmhclmIO.getEffdate());
		sv.dtofdeath.set(clmhclmIO.getDtofdeath());
		sv.causeofdth.set(clmhclmIO.getCauseofdth());
		
		if(CMDTH010Permission){
			cattpf = cattpfDAO.selectRecords(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());
			if(null == cattpf){
			syserrrec.params.set(chdrpf.getChdrcoy().toString()+ chdrpf.getChdrnum());
			fatalError600();	
			}
			else{
			sv.claimnumber.set(cattpf.getClaim());
			}
			}
		
		/*Obtain cause of death description*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5548);
		descIO.setDescitem(sv.causeofdth);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/*         MOVE ALL '?'           TO S6353-CTYPEDES*/
			sv.causeofdthdsc.set(SPACES);
		}
		else {
			sv.causeofdthdsc.set(descIO.getLongdesc());
		}
		
		sv.reasoncd.set(clmhclmIO.getReasoncd());
		sv.longdesc.set(clmhclmIO.getResndesc());
		wsaaTransactionRecInner.wsaaOtheradjst.set(clmhclmIO.getOtheradjst());
		sv.otheradjst.set(clmhclmIO.getOtheradjst());
		lifeclmIO.setDataArea(SPACES);
		lifeclmIO.setChdrcoy(clmhclmIO.getChdrcoy());
		lifeclmIO.setChdrnum(clmhclmIO.getChdrnum());
		lifeclmIO.setLife(clmhclmIO.getLife());
		lifeclmIO.setJlife(SPACES);
		lifeclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		lifeclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		SmartFileCode.execute(appVars, lifeclmIO);
		if (isNE(lifeclmIO.getStatuz(), varcom.oK)
		&& isNE(lifeclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isNE(clmhclmIO.getChdrcoy(), lifeclmIO.getChdrcoy())
		|| isNE(clmhclmIO.getChdrnum(), lifeclmIO.getChdrnum())
		|| isNE(clmhclmIO.getLife(), lifeclmIO.getLife())
		|| isNE(lifeclmIO.getJlife(), "00")
		&& isNE(lifeclmIO.getJlife(), "  ")
		|| isEQ(lifeclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifeclmIO.getLifcnum());
		wsaaDeadLife.set(lifeclmIO.getLifcnum());
		cltsIO.setClntnum(lifeclmIO.getLifcnum());
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		/*    look for joint life.*/
		lifeclmIO.setJlife("01");
		lifeclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeclmIO);
		if ((isNE(lifeclmIO.getStatuz(), varcom.oK))
		&& (isNE(lifeclmIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isEQ(lifeclmIO.getStatuz(), varcom.mrnf)) {
			lifeclmIO.setJlife(SPACES);
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			} else {
		
			cltsIO.setClntnum(lifeclmIO.getLifcnum());
			if (isEQ(clmhclmIO.getJlife(), "01")) {
				wsaaDeadLife.set(lifeclmIO.getLifcnum());
			}
			getClientDetails1400();
			
			if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
			|| isNE(cltsIO.getValidflag(), 1)) {
				sv.jlinsnameErr.set(e304);
				sv.jlinsname.set(SPACES);
			}
			else {
				plainname();
			}
		}
		/*BRD-34*/
		continue1030();
	}

protected void continue1030()
	{
		getTotalClaimValue1500();

		compute(sv.totclaim,2).set(add(sv.totclaim,wsaaActualTot));
		
		
		zhlbpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		zhlbpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1523 */
		zhlbpf.setValidflag("1");
		ls=	zhlbpfDAO.readZhlbpfData(zhlbpf); 
		
		if(ls == null || (ls != null  && ls.size()==0)){
			return;
		}
		
		
		
		else{
			
			for (Zhlbpf zhlbpf:ls) {
					dispSubfile1800(zhlbpf);
				} 
		
			}
		
		

		sv.dtofdeathOut[varcom.pr.toInt()].set("Y");
		sv.causeofdthOut[varcom.pr.toInt()].set("Y");
		sv.reasoncdOut[varcom.pr.toInt()].set("Y");
		sv.longdescOut[varcom.pr.toInt()].set("Y");
		sv.currcdOut[varcom.pr.toInt()].set("Y");
		sv.otheradjstOut[varcom.pr.toInt()].set("Y");
		sv.effdateOut[varcom.pr.toInt()].set("Y");
	}

protected void callDatcons1300()
	{
		/*CALL-DATCONS*/
		/*DATCON1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}


protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1400()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void	getTotalClaimValue1500() {

		clmdclmIO.setDataArea(SPACES);
		clmdclmIO.setChdrcoy(chdrpf.getChdrcoy());
		clmdclmIO.setChdrnum(chdrpf.getChdrnum());
		clmdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		clmdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clmdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.endp)
		&& isNE(clmdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), clmdclmIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), clmdclmIO.getChdrnum())
		|| isEQ(clmdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		/*  store currency*/
		wsaaStoredCurrency.set(clmdclmIO.getCnstcur());
		while ( !(isEQ(clmdclmIO.getStatuz(), varcom.endp))) {
			getTotalClaimClmdclm1600();
		}

}

protected void getTotalClaimClmdclm1600()

{
		if (isNE(chdrpf.getChdrcoy(), clmdclmIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), clmdclmIO.getChdrnum())
		|| isEQ(clmdclmIO.getStatuz(), varcom.endp)) {
			clmdclmIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(clmdclmIO.getCnstcur(), wsaaStoredCurrency)) {
			wsaaEstimateTot.add(clmdclmIO.getEstMatValue());
			wsaaActualTot.add(clmdclmIO.getActvalue());
		}
		clmdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.endp)
		&& isNE(clmdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}

}


protected void dispSubfile1800(Zhlbpf zhlbpf) {

		sv.bnyclt.set(zhlbpf.getBnyclt());
		cltsIO.setClntnum(sv.bnyclt);
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.bnynamErr.set(e304);
			sv.bnynam.set(SPACES);
		}
		else {
			plainname();
			sv.bnynam.set(wsspcomn.longconfname);
		}

		sv.bnypc.set(zhlbpf.getBnypc());
		sv.actvalue.set(zhlbpf.getActvalue());
		sv.zclmadjst.set(zhlbpf.getZclmadjst());
		sv.zhldclmv.set(zhlbpf.getZhldclmv());
		sv.zhldclma.set(zhlbpf.getZhldclma());
		
		scrnparams.function.set(varcom.sadd);
		processScreen("Sr57u", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{

		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}


protected void preScreenEdit()
	{

		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		scrnparams.subfileRrn.set(1);
		return ;

	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateScreen2010();
		
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			/*goTo(GotoLabel.exit2090);*/
			return;
		}
	}


protected void validateScreen2010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
		
			validateSelectionFields2070();
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			 dispSubfile2300();
		}	
		
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		
			return;
		}
		validateSelectionFields2070();
	}

protected void validateSelectionFields2070()
	{
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
	
			return;
		}
	
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			return ;
		}
	}

protected void  dispSubfile2300() {

	
		if(ls == null || (ls != null  && ls.size()==0)){
			/*syserrrec.params.set(zhlbIO.getParams());*/
			return;
		}
		else{
			for (Zhlbpf zhlbpf:ls) {
				dispSubfile1800(zhlbpf);
			} 
		}	
	

}

protected void update3000()
	{	
		updateDatabase3010();
		
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			
			return;
		}
		if (isEQ(wsspcomn.flag, "I")) {
		
			return;
		}
	}


protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}		
	}

protected void saveProgram4100()
{
	/*SAVE*/
	wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
}

protected void restoreProgram4200()
{
	/*RESTORE*/
	wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
}

/*
 * Class transformed  from Data Structure WSAA-TRANSACTION-REC--INNER
 */
private static final class WsaaTransactionRecInner { 

		/*01  WSAA-TRANSACTION-REC.*/
	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(215);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaClamamtOld = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 1);
	private PackedDecimalData wsaaClamamtNew = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 10);
	private PackedDecimalData wsaaOtheradjst = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 19);
	private FixedLengthStringData wsaaCurrcd = new FixedLengthStringData(3).isAPartOf(wsaaTransactionRec, 37);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 40);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 44);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 48);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 52);
	private FixedLengthStringData filler = new FixedLengthStringData(159).isAPartOf(wsaaTransactionRec, 56, FILLER).init(SPACES);
}

/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData et11 = new FixedLengthStringData(4).init("ET11");
	private FixedLengthStringData et10 = new FixedLengthStringData(4).init("ET10");
	private FixedLengthStringData hl08 = new FixedLengthStringData(4).init("HL08");
	private FixedLengthStringData h355 = new FixedLengthStringData(4).init("H355");	
	
}

/*
 * Class transformed  from Data Structure FORMATS_INNER
 */
private static final class FormatsInner { 
	/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	
}
}
