package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:10
 * Description:
 * Copybook name: CNFSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Cnfskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData cnfsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData cnfsKey = new FixedLengthStringData(64).isAPartOf(cnfsFileKey, 0, REDEFINE);
  	public FixedLengthStringData cnfsChdrcoy = new FixedLengthStringData(1).isAPartOf(cnfsKey, 0);
  	public FixedLengthStringData cnfsChdrnum = new FixedLengthStringData(8).isAPartOf(cnfsKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(cnfsKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(cnfsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		cnfsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}