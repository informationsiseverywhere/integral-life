package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PtsdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:09
 * Class transformed from PTSDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PtsdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 119;
	public FixedLengthStringData ptsdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ptsdpfRecord = ptsdrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ptsdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(ptsdrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(ptsdrec);
	public PackedDecimalData actvalue = DD.actvalue.copy().isAPartOf(ptsdrec);
	public PackedDecimalData percreqd = DD.percreqd.copy().isAPartOf(ptsdrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(ptsdrec);
	public PackedDecimalData estMatValue = DD.emv.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData virtualFund = DD.vrtfund.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ptsdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ptsdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PtsdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PtsdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PtsdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PtsdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PtsdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PtsdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PtsdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PTSDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"LIFE, " +
							"JLIFE, " +
							"EFFDATE, " +
							"CURRCD, " +
							"ACTVALUE, " +
							"PERCREQD, " +
							"PLNSFX, " +
							"TYPE_T, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"SHORTDS, " +
							"LIENCD, " +
							"EMV, " +
							"VRTFUND, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     life,
                                     jlife,
                                     effdate,
                                     currcd,
                                     actvalue,
                                     percreqd,
                                     planSuffix,
                                     fieldType,
                                     coverage,
                                     rider,
                                     crtable,
                                     shortds,
                                     liencd,
                                     estMatValue,
                                     virtualFund,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		life.clear();
  		jlife.clear();
  		effdate.clear();
  		currcd.clear();
  		actvalue.clear();
  		percreqd.clear();
  		planSuffix.clear();
  		fieldType.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		shortds.clear();
  		liencd.clear();
  		estMatValue.clear();
  		virtualFund.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPtsdrec() {
  		return ptsdrec;
	}

	public FixedLengthStringData getPtsdpfRecord() {
  		return ptsdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPtsdrec(what);
	}

	public void setPtsdrec(Object what) {
  		this.ptsdrec.set(what);
	}

	public void setPtsdpfRecord(Object what) {
  		this.ptsdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ptsdrec.getLength());
		result.set(ptsdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}