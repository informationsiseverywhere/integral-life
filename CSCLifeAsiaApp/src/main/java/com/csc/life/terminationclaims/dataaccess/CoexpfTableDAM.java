package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CoexpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:32
 * Class transformed from COEXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CoexpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 106;
	public FixedLengthStringData coexrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData coexpfRecord = coexrec;
	
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(coexrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(coexrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(coexrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(coexrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(coexrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(coexrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(coexrec);
	public PackedDecimalData grospre = DD.grospre.copy().isAPartOf(coexrec);
	public PackedDecimalData tyearno = DD.tyearno.copy().isAPartOf(coexrec);
	public PackedDecimalData payperd = DD.payperd.copy().isAPartOf(coexrec);
	public PackedDecimalData trandate = DD.trandate.copy().isAPartOf(coexrec);
	public PackedDecimalData sexrat = DD.sexrat.copy().isAPartOf(coexrec);
	public FixedLengthStringData trancd = DD.trancd.copy().isAPartOf(coexrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(coexrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(coexrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(coexrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CoexpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for CoexpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CoexpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CoexpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CoexpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CoexpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CoexpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("COEXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"ARACDE, " +
							"AGNTNUM, " +
							"CHDRNUM, " +
							"CNTCURR, " +
							"BILLFREQ, " +
							"CRTABLE, " +
							"EFFDATE, " +
							"GROSPRE, " +
							"TYEARNO, " +
							"PAYPERD, " +
							"TRANDATE, " +
							"SEXRAT, " +
							"TRANCD, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     aracde,
                                     agntnum,
                                     chdrnum,
                                     cntcurr,
                                     billfreq,
                                     crtable,
                                     effdate,
                                     grospre,
                                     tyearno,
                                     payperd,
                                     trandate,
                                     sexrat,
                                     trancd,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		aracde.clear();
  		agntnum.clear();
  		chdrnum.clear();
  		cntcurr.clear();
  		billfreq.clear();
  		crtable.clear();
  		effdate.clear();
  		grospre.clear();
  		tyearno.clear();
  		payperd.clear();
  		trandate.clear();
  		sexrat.clear();
  		trancd.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCoexrec() {
  		return coexrec;
	}

	public FixedLengthStringData getCoexpfRecord() {
  		return coexpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCoexrec(what);
	}

	public void setCoexrec(Object what) {
  		this.coexrec.set(what);
	}

	public void setCoexpfRecord(Object what) {
  		this.coexpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(coexrec.getLength());
		result.set(coexrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}