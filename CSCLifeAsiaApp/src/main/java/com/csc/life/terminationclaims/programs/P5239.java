/*
 * File: P5239.java
 * Date: 30 August 2009 0:21:55
 * Author: Quipoz Limited
 * 
 * Class transformed from P5239.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.AnntlnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegplnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegtTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegtenqTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegtlnbTableDAM;
import com.csc.life.terminationclaims.screens.S5239ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* REGULAR PAYMENT SELECT SCREEN
* =============================
*
* This program forms part of  the  9405  Annuities  Development.
*
* It  forms  part  of  the new business processing for Immediate
* Annuity Components and controls the Immediate Annuity  Payment
* Details Screen, S5226.
*
* When  creating  the first Immediate Annuity Payment there will
* be no Regular Payment Temporary record so the screen will  not
* be  displayed.  A Regular Payment Temporary record (REGTLNB) will
* be created with the appropriate defaults and the  next  screen
* will  be  displayed  with  these details.  The defaults are as
* follows:
*
*   -  Payment currency from the CHDR
*
*   -  Effective date to the Risk Commencement Date from the
*      CHDR.
*
*   -  Frequency from the ANNT.
*
*   -  The first payment date is calculated as follows:
*           Annuity payments In Advance(from ANNT)
*           The first payment date is the effective date of the
*           annuity.
*           Annuity payments In Arrears(from ANNT)
*           The first payment date is the effective date of the
*           annuity plus one frequency.
*
*    - If there is a Guaranteed Period the review date will be
*      set to the effective date plus this number of years.
*
* If Regular Payment Temporary Records (REGTLNB) exist, the  screen
* will be displayed with the 'select' field available for input.
*
* If  in  proposal  enquiry,  ADD and DELETE options will not be
* valid and will not be displayed.
*
* If in proposal modify all options will be  available.    If  a
* payment  record  is  selected  the  details  for  the  Regular
* Payment Temporary record (REGTLNB) will be displayed on the  next
* program in the sequence.
*
* If  ADD  is  selected  this does not necessarily need to be
* against the last REGT, but can be against any REGT. The
* Annuity Details Screen will then be displayed, allowing the
* addition of another REGT.
*
* If  any  of  the following items are modified on the proposal,
* all of the regular payment temporary records (REGTLNBs)  for  the
* component  should  be  selected  to force recalculation of the
* appropriate dates:
*
*      Risk commencement date        Effective date
*       (COVR/CHDR)                  First payment date
*                                    Review date
*                                    Anniversary date
*
*      Guaranteed period (ANNT)      Review date
*      In advance/In arrears (ANNT)  First payment date
*      Annuity payment frequency     First payment date.
*            (ANNT)
*
*  This program will be either searching  for the FIRST selected
*  record  in  order  to pass  control  to  the next screen,  or
*  it will be  returning from  P5226 after  having displayed the
*  previous REGT.
*
*  It will be able  to determine which of these two states it is
*  in by examining the Stack Action Flag.
*
*****************************************************************
* </pre>
*/
public class P5239 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5239");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaNextRgpy = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaNoRegt = new FixedLengthStringData(1);
	private String wsaaFirstTime = "";
	private FixedLengthStringData wsaaForceBack = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSubsequent = new FixedLengthStringData(1).init(SPACES);
		/* ERRORS */
	private String f069 = "F069";
	private String e944 = "E944";
		/* TABLES */
	private String t5688 = "T5688";
	private String t6692 = "T6692";
	private String regtlnbrec = "REGTLNBREC";
	private String descrec = "DESCREC";
	private String cltsrec = "CLTSREC";
	private String regplnbrec = "REGPLNBREC";
		/*Annuity Details - Temporary*/
	private AnntlnbTableDAM anntlnbIO = new AnntlnbTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/rider transactions - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Optswchrec optswchrec = new Optswchrec();
		/*Regular Payment Temporary Record*/
	private RegtTableDAM regtIO = new RegtTableDAM();
		/*Regular Payment Temporary Record*/
	private RegtenqTableDAM regtenqIO = new RegtenqTableDAM();
		/*Regular Payment Temporary Record New Bus*/
	private RegtlnbTableDAM regtlnbIO = new RegtlnbTableDAM();
	private RegplnbTableDAM regplnbIO = new RegplnbTableDAM();
	private Wssplife wssplife = new Wssplife();
	private S5239ScreenVars sv = ScreenProgram.getScreenVars( S5239ScreenVars.class);
	private RegppfDAO regpDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private RegpTableDAM regpIO = new RegpTableDAM();
	private Descpf descpf;
	private static final String regprec = "REGPREC";
	private Batckey wsaaBatchkey = new Batckey();
	private boolean isData;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	protected Chdrpf chdrpf = null;
	protected static final String T609 = "T609";

	

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		exit1190, 
		preExit, 
		updateErrorIndicators2120, 
		exit3090, 
		next4010, 
		continue4020, 
		exit4090, 
		exit4190, 
		exit4390
	}

	public P5239() {
		super();
		screenVars = sv;
		new ScreenModel("S5239", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaNoRegt.set(SPACES);
		wsaaForceBack.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		if (isEQ(wsaaSubsequent,SPACES)) {
			wssplife.updateFlag.set("Y");
			wsaaSubsequent.set("Y");
		}
		scrnparams.function.set(varcom.sclr);
		processScreen("S5239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);

		if (isEQ(wsspcomn.flag,"I")) {
			sv.hselectOut[varcom.nd.toInt()].set("Y");
			sv.hselect.set(SPACES);
			sv.scflag.set("Y");
		}else{
			sv.scflag.set("N");
		}	
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
			chdrpf = new Chdrpf();
			chdrpf = chdrpfDAO.getCacheObject(chdrpf);
					if(null==chdrpf) {
						chdrlnbIO.setFunction(varcom.retrv);
						SmartFileCode.execute(appVars, chdrlnbIO);
						if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
							syserrrec.params.set(chdrlnbIO.getParams());
							fatalError600();
						}
						else {
							chdrpf = chdrpfDAO.getChdrpf(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
							chdrpf = new Chdrpf();
							if(isNE(wsspcomn.flag,"I")) {
								chdrpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
								chdrpf.setChdrcoy(chdrlnbIO.getChdrcoy().charat(0));
								chdrpf.setCntcurr(chdrlnbIO.getCntcurr().toString());
								chdrpf.setCnttype(chdrlnbIO.getCnttype().toString());								
								chdrpf.setOccdate(chdrlnbIO.getOccdate().toInt());
								chdrpf.setCownnum(chdrlnbIO.getCownnum().toString());
							}  else if(null==chdrpf) {
								fatalError600();
							}
							else {
								chdrpfDAO.setCacheObject(chdrpf);
							}
						}
					} 
		covtlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if(isNE(wsspcomn.flag, "I"))
		{
			anntlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
			anntlnbIO.setChdrnum(covtlnbIO.getChdrnum());
			anntlnbIO.setLife(covtlnbIO.getLife());
			anntlnbIO.setCoverage(covtlnbIO.getCoverage());
			anntlnbIO.setRider(covtlnbIO.getRider());
			anntlnbIO.setSeqnbr(covtlnbIO.getSeqnbr());
			anntlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, anntlnbIO);
			if (isNE(anntlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(anntlnbIO.getParams());
				fatalError600();
			}
		} 
		
		sv.chdrnum.set(chdrpf.getChdrnum());
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		sv.cnttype.set(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrpf.getCownnum());
		sv.cownnum.set(chdrpf.getCownnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		wsaaBatchkey.set(wsspcomn.batchkey);
		 
		regtIO.setParams(SPACES);
		regtIO.setChdrcoy(covtlnbIO.getChdrcoy());
		regtIO.setChdrnum(covtlnbIO.getChdrnum());
		regtIO.setLife(covtlnbIO.getLife());
		regtIO.setCoverage(covtlnbIO.getCoverage());
		regtIO.setRider(covtlnbIO.getRider());
		regtIO.setSeqnbr(ZERO);
		regtIO.setPlanSuffix(ZERO);
		regtIO.setRgpynum(ZERO);
		regtIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		regtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		wsaaFirstTime = "Y";
		isData = true;
		while ( !(isEQ(regtIO.getStatuz(),varcom.endp))) {
			loadSubfile1100();
		}
		
		if(!isData){
			List<Regppf> regppfList = regpDAO.readRecord(covtlnbIO.getChdrcoy().toString(), covtlnbIO.getChdrnum().toString(), covtlnbIO.getLife().toString(), covtlnbIO.getCoverage().toString(), covtlnbIO.getRider().toString());
			if (regppfList != null && regppfList.size() > 0) {
				loadRegpSubFile(regppfList);
			}
		}
		if(isEQ(wsspcomn.flag, "I")){
			wsaaNoRegt.set("N");
		}
	}

private void loadRegpSubFile(List<Regppf> regppfList) {
	for(Regppf regpobj : regppfList){
		if(isEQ(regpobj.getValidflag(),"1")){
			descpf=descDAO.getdescData("IT", t6692, regpobj.getPayreason().trim(), wsspcomn.company.toString(), wsspcomn.language.toString());/* IJTI-1523 */
			
			if (descpf != null) {
				sv.rptldesc.set(descpf.getLongdesc());
			}
			else {
				sv.rptldesc.fill("?");
			}
			sv.pymt.set(regpobj.getPymt());
			sv.rgpynum.set(regpobj.getRgpynum());
			sv.select.set(SPACES);
			sv.hrrn.set(regpobj.getUniqueNumber());
			sv.hflag.set(SPACES);
			sv.selectOut[varcom.pr.toInt()].set(SPACES);
			if (isNE(chdrpf.getOccdate(),regpobj.getCrtdate())
			|| isEQ(wssplife.fuptype,"Y")) {
				sv.hflag.set("Y");
				sv.select.set("1");
				sv.selectOut[varcom.pr.toInt()].set("Y");
			}
		
			scrnparams.function.set(varcom.sadd);
			processScreen("S5239", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.params.set(scrnparams.screenParams);
				fatalError600();
			}
		}
	}
	
}

protected void loadSubfile1100()
	{
		try {
			screenIo1110();
			sadd1150();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo1110()
	{
		SmartFileCode.execute(appVars, regtIO);
		if ((isNE(regtIO.getStatuz(),varcom.oK))
		&& (isNE(regtIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regtIO.getParams());
			fatalError600();
		}
		if ((isEQ(regtIO.getStatuz(),varcom.endp))
		|| (isNE(regtIO.getChdrcoy(),covtlnbIO.getChdrcoy()))
		|| (isNE(regtIO.getChdrnum(),covtlnbIO.getChdrnum()))
		|| (isNE(regtIO.getLife(),covtlnbIO.getLife()))
		|| (isNE(regtIO.getCoverage(),covtlnbIO.getCoverage()))
		|| (isNE(regtIO.getRider(),covtlnbIO.getRider()))
		|| (isNE(regtIO.getSeqnbr(),covtlnbIO.getSeqnbr()))) {
			regtIO.setStatuz(varcom.endp);
			if (isEQ(wsaaFirstTime,"Y")) {
				wsaaNoRegt.set("Y");
			}
			isData = false;
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(wsaaFirstTime,"Y")) {
			wsaaFirstTime = "N";
		}
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t6692);
		descIO.setDescitem(regtIO.getPayreason());
		sv.rgpytype.set(regtIO.getPayreason());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rptldesc.set(descIO.getLongdesc());
		}
		else {
			sv.rptldesc.fill("?");
		}
		sv.pymt.set(regtIO.getPymt());
		sv.rgpynum.set(regtIO.getRgpynum());
		sv.select.set(SPACES);
		sv.hrrn.set(regtIO.getRrn());
		sv.hflag.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		if (isNE(chdrpf.getOccdate(),regtIO.getCrtdate())
		|| isEQ(wssplife.fuptype,"Y")) {
			sv.hflag.set("Y");
			sv.select.set("1");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void sadd1150()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		regtIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		wsspcomn.edterror.set(varcom.oK);
		if(isNE(wsspcomn.flag, "I")){
			if (isEQ(wsaaNoRegt,"Y")) {
				wsspcomn.sectionno.set("3000");
				goTo(GotoLabel.preExit);
			}
		}
		
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("S5239", sv);
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
		/*EXIT*/
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateSelect2110();
				}
				case updateErrorIndicators2120: {
					updateErrorIndicators2120();
					readNextModifiedRecord2130();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSelect2110()
	{
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2120);
		}
		if (isNE(sv.select,NUMERIC)) {
			sv.selectErr.set(e944);
		}
		else {
			optswchrec.optsFunction.set("CHCK");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsItemCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelCode.set(SPACES);
			optswchrec.optsSelOptno.set(sv.select);
			optswchrec.optsSelType.set("L");
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz,varcom.oK)) {
				sv.selectErr.set(optswchrec.optsStatuz);
			}
		}
	}

protected void updateErrorIndicators2120()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2130()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsaaNoRegt,"Y")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case next4010: {
					next4010();
				}
				case continue4020: {
					continue4020();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(wsaaNoRegt,"Y")) {
			wsaaNoRegt.set(SPACES);
			newRegt4400();
			regtlnbIO.setFormat(regtlnbrec);
			regtlnbIO.setFunction(varcom.keeps);
			regtlnbio4600();
			optswchrec.optsSelOptno.set(1);
			sv.select.set("0");
			optswchrec.optsSelType.set("L");
			optswchCall4500();
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			if (isEQ(sv.select,"2")) {
				sv.select.set(SPACES);
				scrnparams.function.set(varcom.supd);
				screenio4800();
				subfileRec4100();
				if (isEQ(regtlnbIO.getStatuz(),varcom.oK)) {
					scrnparams.function.set(varcom.sadd);
					screenio4800();
				}
				goTo(GotoLabel.next4010);
			}
			if (isEQ(sv.select,"1")) {
				sv.select.set(SPACES);
				sv.selectOut[varcom.pr.toInt()].set(SPACES);
				scrnparams.function.set(varcom.supd);
				subfileRec4100();
				screenio4800();
				goTo(GotoLabel.next4010);
			}
			if (isEQ(sv.select,0)) {
				sv.select.set(SPACES);
				scrnparams.function.set(varcom.sadd);
				subfileRec4100();
				if (isEQ(wsaaForceBack,"Y")) {
					wsaaForceBack.set(SPACES);
					newRegt4400();
					regtlnbIO.setFormat(regtlnbrec);
					regtlnbIO.setFunction(varcom.keeps);
					regtlnbio4600();
					optswchrec.optsSelOptno.set(1);
					sv.select.set("0");
					optswchrec.optsSelType.set("L");
					optswchCall4500();
					goTo(GotoLabel.exit4090);
				}
				screenio4800();
				goTo(GotoLabel.next4010);
			}
			scrnparams.function.set(varcom.srdn);
		}
		else {
			scrnparams.function.set(varcom.sstrt);
		}
		goTo(GotoLabel.continue4020);
	}

protected void next4010()
	{
		scrnparams.function.set(varcom.srdn);
	}

protected void continue4020()
	{
		screenio4800();
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
			|| isEQ(wssplife.updateFlag,"Y")
			|| isEQ(wsspcomn.flag,"I")) {
				optswchrec.optsSelType.set(SPACES);
				optswchrec.optsSelOptno.set(ZERO);
				optswchCall4500();
				goTo(GotoLabel.exit4090);
			}
			else {
				wsspcomn.nextprog.set(scrnparams.scrname);
				scrnparams.errorCode.set(f069);
				goTo(GotoLabel.exit4090);
			}
		}
		if (isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.next4010);
		}
		if (isEQ(sv.select,"1")) {
				optswchrec.optsSelOptno.set(1);
				optswchrec.optsSelType.set("L");
				
				if(isEQ(wsaaBatchkey.batcBatctrcde, T609)) {
					optswchrec.optsSelOptno.set(2);
					optswchrec.optsSelType.set("E");
				}
				regtlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
				regtlnbIO.setChdrnum(sv.chdrnum);
				regtlnbIO.setRrn(sv.hrrn);
				regtlnbIO.setFunction(varcom.readd);
				
				regtlnbio4600();
				if(isEQ(regtlnbIO.getStatuz(),varcom.mrnf)) {
					regplnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
					regplnbIO.setChdrnum(sv.chdrnum);
					regplnbIO.setRrn(sv.hrrn);
					regplnbIO.setFunction(varcom.readd);
					regplnbio4600();
				}
				if (isNE(sv.hflag,SPACES)) {
					defaultDates4700();
				}
				if(isEQ(regtlnbIO.getStatuz(),varcom.mrnf)) {
					regplnbIO.setFormat(regplnbrec);
					regplnbIO.setFunction(varcom.keeps);
					regplnbio4600();
				}else {
//			MIBT-217 STARTS
					regtlnbIO.setFormat(regtlnbrec);
//			MIBT-217 ENDS
					regtlnbIO.setFunction(varcom.keeps);
					regtlnbio4600();
				}

				optswchCall4500();
		}
		if (isEQ(sv.select,"2")) {
			optswchrec.optsSelOptno.set(1);
			optswchrec.optsSelType.set("L");
			regtlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
			regtlnbIO.setChdrnum(sv.chdrnum);
			newRegt4400();
			begnRegt4200();
			regtlnbIO.setFormat(regtlnbrec);
			regtlnbIO.setFunction(varcom.keeps);
			regtlnbio4600();
			optswchCall4500();
		}
		if (isEQ(sv.select,"9")) {
			regtlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
			regtlnbIO.setChdrnum(sv.chdrnum);
			regtlnbIO.setRrn(sv.hrrn);
			regtlnbIO.setFunction(varcom.readd);
			regtlnbio4600();
			regtlnbIO.setFunction(varcom.deltd);
			regtlnbio4600();
			wssplife.updateFlag.set("N");
		}
	}

protected void subfileRec4100()
	{
		try {
			find4110();
		}
		catch (GOTOException e){
		}
	}

protected void find4110()
	{
		regtlnbIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, regtlnbIO);
		if ((isNE(regtlnbIO.getStatuz(),varcom.oK))
		&& (isNE(regtlnbIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(regtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(regtlnbIO.getStatuz(),varcom.mrnf)) {
			checkIfAnyRegts4900();
			goTo(GotoLabel.exit4190);
		}
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t6692);
		descIO.setDescitem(regtlnbIO.getPayreason());
		sv.rgpytype.set(regtlnbIO.getPayreason());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rptldesc.set(descIO.getLongdesc());
		}
		else {
			sv.rptldesc.fill("?");
		}
		sv.pymt.set(regtlnbIO.getPymt());
		sv.rgpynum.set(regtlnbIO.getRgpynum());
		sv.select.set(SPACES);
		sv.hrrn.set(regtlnbIO.getRrn());
		sv.hflag.set(SPACES);
	}

protected void begnRegt4200()
	{
		find4210();
	}

protected void find4210()
	{
		wsaaNextRgpy.set(ZERO);
		regtenqIO.setParams(SPACES);
		regtenqIO.setChdrcoy(chdrpf.getChdrcoy());
		regtenqIO.setChdrnum(chdrpf.getChdrnum());
		regtenqIO.setLife(covtlnbIO.getLife());
		regtenqIO.setCoverage(covtlnbIO.getCoverage());
		regtenqIO.setRider(covtlnbIO.getRider());
		regtenqIO.setRgpynum(ZERO);
		regtenqIO.setSeqnbr(ZERO);
		regtenqIO.setPlanSuffix(ZERO);
		regtenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		regtenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		while ( !(isEQ(regtenqIO.getStatuz(),varcom.endp))) {
			findNextRegt4300();
		}
		
		setPrecision(regtlnbIO.getRgpynum(), 0);
		regtlnbIO.setRgpynum(add(wsaaNextRgpy,1));
	}

protected void findNextRegt4300()
	{
		try {
			find4310();
		}
		catch (GOTOException e){
		}
	}

protected void find4310()
	{
		SmartFileCode.execute(appVars, regtenqIO);
		if ((isNE(regtenqIO.getStatuz(),varcom.oK))
		&& (isNE(regtenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regtenqIO.getParams());
			fatalError600();
		}
		if (isEQ(regtenqIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit4390);
		}
		if ((isEQ(chdrpf.getChdrcoy(),regtenqIO.getChdrcoy()))
		&& (isEQ(chdrpf.getChdrnum(),regtenqIO.getChdrnum()))
		&& (isEQ(covtlnbIO.getLife(),regtenqIO.getLife()))
		&& (isEQ(covtlnbIO.getCoverage(),regtenqIO.getCoverage()))
		&& (isEQ(covtlnbIO.getRider(),regtenqIO.getRider()))) {
			if (isGT(regtenqIO.getRgpynum(),wsaaNextRgpy)) {
				wsaaNextRgpy.set(regtenqIO.getRgpynum());
			}
		}
		else {
			regtenqIO.setStatuz(varcom.endp);
		}
		regtenqIO.setFunction(varcom.nextr);
	}

protected void newRegt4400()
	{
		setup4410();
	}

protected void setup4410()
	{
		regtlnbIO.setDataArea(SPACES);
		regtlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		regtlnbIO.setChdrnum(covtlnbIO.getChdrnum());
		regtlnbIO.setLife(covtlnbIO.getLife());
		regtlnbIO.setCoverage(covtlnbIO.getCoverage());
		regtlnbIO.setRider(covtlnbIO.getRider());
		regtlnbIO.setSeqnbr(covtlnbIO.getSeqnbr());
		regtlnbIO.setPlanSuffix(ZERO);
		regtlnbIO.setRgpynum(1);
		regtlnbIO.setCrtable(covtlnbIO.getCrtable());
		regtlnbIO.setCurrcd(chdrpf.getCntcurr());
		regtlnbIO.setRegpayfreq(anntlnbIO.getFreqann());
		regtlnbIO.setPaycoy(wsspcomn.fsuco);
		regtlnbIO.setPymt(ZERO);
		regtlnbIO.setPrcnt(ZERO);
		regtlnbIO.setTotamnt(ZERO);
		regtlnbIO.setFirstPaydate(varcom.vrcmMaxDate);
		regtlnbIO.setRevdte(varcom.vrcmMaxDate);
		regtlnbIO.setFinalPaydate(varcom.vrcmMaxDate);
		regtlnbIO.setAnvdate(varcom.vrcmMaxDate);
		regtlnbIO.setSacscode(SPACES);
		regtlnbIO.setSacstype(SPACES);
		regtlnbIO.setGlact(SPACES);
		regtlnbIO.setDebcred(SPACES);
		regtlnbIO.setDestkey(SPACES);
		regtlnbIO.setPayclt(SPACES);
		regtlnbIO.setRgpymop(SPACES);
		regtlnbIO.setPayreason(SPACES);
		regtlnbIO.setClaimevd(SPACES);
		regtlnbIO.setBankkey(SPACES);
		regtlnbIO.setBankacckey(SPACES);
		regtlnbIO.setRgpytype(SPACES);
		defaultDates4700();
	}

protected void optswchCall4500()
	{
		call4510();
	}

protected void call4510()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz,varcom.oK))
		&& (isNE(optswchrec.optsStatuz,varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.oK)) {
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void regtlnbio4600()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, regtlnbIO);
		if ((isNE(regtlnbIO.getStatuz(),varcom.oK))
		&& (isNE(regtlnbIO.getStatuz(),varcom.endp)) && (isNE(regtlnbIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(regtlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void regplnbio4600()
	{
	/*CALL*/
		SmartFileCode.execute(appVars, regplnbIO);
		if ((isNE(regplnbIO.getStatuz(), varcom.oK)) 
				&& (isNE(regplnbIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(regplnbIO.getParams());
			fatalError600();
		}
		/* EXIT */
	}
protected void defaultDates4700()
	{
		setup4710();
	}

protected void setup4710()
	{
		regtlnbIO.setCrtdate(chdrpf.getOccdate());
		if (isNE(anntlnbIO.getAdvance(),SPACES)) {
			regtlnbIO.setFirstPaydate(chdrpf.getOccdate());
		}
		if (isNE(anntlnbIO.getArrears(),SPACES)) {
			datcon2rec.intDate1.set(chdrpf.getOccdate());
			datcon2rec.frequency.set(anntlnbIO.getFreqann());
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			regtlnbIO.setFirstPaydate(datcon2rec.intDate2);
		}
		if (isNE(anntlnbIO.getGuarperd(),ZERO)) {
			datcon2rec.intDate1.set(chdrpf.getOccdate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(anntlnbIO.getGuarperd());
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			regtlnbIO.setRevdte(datcon2rec.intDate2);
		}
		else {
			regtlnbIO.setRevdte(varcom.vrcmMaxDate);
		}
	}

protected void screenio4800()
	{
		/*CALL*/
		processScreen("S5239", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkIfAnyRegts4900()
	{
		find4910();
	}

protected void find4910()
	{
		wsaaNextRgpy.set(ZERO);
		regtenqIO.setParams(SPACES);
		regtenqIO.setChdrcoy(chdrpf.getChdrcoy());
		regtenqIO.setChdrnum(chdrpf.getChdrnum());
		regtenqIO.setLife(ZERO);
		regtenqIO.setCoverage(ZERO);
		regtenqIO.setRider(ZERO);
		regtenqIO.setRgpynum(ZERO);
		regtenqIO.setSeqnbr(ZERO);
		regtenqIO.setPlanSuffix(ZERO);
		regtenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		regtenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, regtenqIO);
		if ((isNE(regtenqIO.getStatuz(),varcom.oK))
		&& (isNE(regtenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regtenqIO.getParams());
			fatalError600();
		}
		if ((isEQ(regtenqIO.getStatuz(),varcom.endp))
		|| (isNE(chdrpf.getChdrcoy(),regtenqIO.getChdrcoy()))
		|| (isNE(chdrpf.getChdrnum(),regtenqIO.getChdrnum()))
		|| (isNE(covtlnbIO.getLife(),regtenqIO.getLife()))
		|| (isNE(covtlnbIO.getCoverage(),regtenqIO.getCoverage()))
		|| (isNE(covtlnbIO.getRider(),regtenqIO.getRider()))) {
			wsaaForceBack.set("Y");
		}
	}
}
