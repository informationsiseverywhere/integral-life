package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:07
 * Description:
 * Copybook name: LIFEMATKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifematkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifematFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifematKey = new FixedLengthStringData(256).isAPartOf(lifematFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifematChdrcoy = new FixedLengthStringData(1).isAPartOf(lifematKey, 0);
  	public FixedLengthStringData lifematChdrnum = new FixedLengthStringData(8).isAPartOf(lifematKey, 1);
  	public FixedLengthStringData lifematLife = new FixedLengthStringData(2).isAPartOf(lifematKey, 9);
  	public FixedLengthStringData lifematJlife = new FixedLengthStringData(2).isAPartOf(lifematKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(lifematKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifematFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifematFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}