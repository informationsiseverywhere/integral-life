package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Cpsupf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CpsupfDAO extends BaseDAO<Cpsupf> {
	
	public void insertCpsupfRecord(Cpsupf cpsupf);
	public void updateCpsupfRecord(Cpsupf cpsupf);
	public void deleteCpsupfRecord(Cpsupf cpsupf);
	public Cpsupf getCpsupfItem(String chdrnum , String crtable);
	public List<Cpsupf> getCpsupfRcdList(String chdrcoy,String chdrnum,String Status);
	public void updateCpsupf(Cpsupf cpsupf);
	public List<Cpsupf> getCpsubyTranno(String chdrnum , String tranno);

}