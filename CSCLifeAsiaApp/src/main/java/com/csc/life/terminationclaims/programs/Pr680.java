/*
 * File: Pr680.java
 * Date: 30 August 2009 1:54:26
 * Author: Quipoz Limited
 * 
 * Class transformed from PR680.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.AnntmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LextrevTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.general.tablestructures.T6005rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.CoexTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.recordstructures.Pr676cpy;
import com.csc.life.terminationclaims.screens.Sr680ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.terminationclaims.tablestructures.Tr686rec;
import com.csc.life.terminationclaims.tablestructures.Tr687rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrrgwTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  COMPONENT CHANGE - DISABILITY COMPONENTS.
*  -----------------------------------------
*
*  This screen/program PR680 is  used  to  capture any  changes
*  to  the    coverage   and   rider  details  for  Disability
*  components.
*
*  Initialise.
*  -----------
*
*  Skip this section  if    returning    from    an    optional
*  selection (current stack position action flag = '*').
*
*  Read  CHDRLIF  (RETRV)  in    order  to  obtain the contract
*  header information.
*
*  The key for Coverage/rider  to    be    worked  on  will  be
*  available  from  COVR.   This is obtained by using the RETRV
*  function.
*
*  Firstly  we  need  to check  if  there  has  already    been
*  an   alteration   to   this     coverage/rider  within  this
*  transaction. If this  is   the  case  there  will    be    a
*  record      present     on    the  temporary  coverage/rider
*  transaction  file    COVTPF.    Read  this  file  using  the
*  logical  view    COVTMAJ  and  the  key  from  the retrieved
*  coverage/rider record. If  a  record  is  found  go  to  the
*  SET-UP-SCREEN section.
*
*  Compare  the  plan  suffix  of  the  retrieved  record  with
*  the contract header no of policies  in  plan    field.    If
*  the  plan  suffix  is '00' go to SET-UP-COVT.  If it is less
*  than or equal to this field the we need  to  'breakout'  the
*  amount fields as follows:
*
*  Divide the COVR sum assured  field  by  the CHDR no of
*                policies in plan.
*  Divide  the COVR premium  field  by  the  CHDR  no  of
*                policies in plan.
*
*  ******* SET-UP-COVT.
*
*  Move    the  appropriate fields  from  the  COVR  record  to
*  the COVTMAJ record.
*
*  ******* SET-UP-SCREEN.
*
*  Read  the  contract definition  details  from   T5688    for
*  the  contract  type  held on  CHDRLIF.  Access  the  version
*  of this item for the original commencement date of the risk.
*
*  Read  the  general  coverage/rider  details from  T5687  and
*  the  traditional/Disability   edit  rules from T5608 for the
*  coverage type held  on  COVTMAJ.  Access  the    version  of
*  these item for the original commencement date of the risk.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To  obtain the life assured  and joint-life details (if any)
*  do the following;-
*
*       - read the life  details  using  LIFEMAJ  (life  number
*       from  COVTMAJ,  joint  life number  '00').  Look up the
*       name from the client details (CLTS)  and  format  as  a
*       "confirmation name".
*
*       -  read  the  joint    life details using LIFEMAJ (life
*       number from COVTMAJ,  joint  life  number  '01').    If
*       found,  look  up  the  name  from  the  client  details
*       (CLTS) and format as a "confirmation name".
*
*  To determine which premium  calculation    method    to  use
*  decide  whether  or  not  it  is  a   Single  or  Joint-life
*  case  (it is a joint life case, the  joint    life    record
*  was  found  above). Then:
*
*       -  if  it  is a single-life case use, the single-method
*       from T5687. The age to be used  for validation will  be
*       the age of the main life.
*
*       -  if  the joint-life  indicator (from T5687) is blank,
*       and  if  it  is  a    Joint-life    case,    use    the
*       joint-method  from  T5687.  The  age  to  be  used  for
*       validation will be the age of the main life.
*
*       -  if the Joint-life  indicator  is  'N',   then    use
*       the  Single-method.    But,  if  there  is a joint-life
*       (this must be a rider to have got  this  far)    prompt
*       for  the  joint  life  indicator  to  determine   which
*       life  the rider is to attach to.  In all  other  cases,
*       the  joint  life  indicator should be non-displayed and
*       protected.  The  age  to  be used for  validation  will
*       be  the    age  of the main or joint life, depending on
*       the one selected.
*
*       - use the premium-method  selected  from   T5687,    if
*       not  blank, to access T5675.  This gives the subroutine
*       to use for the calculation.
*
*      COVERAGE/RIDER DETAILS
*
*  The fields to be displayed  on  the  screen  are  determined
*  from the entry in table T5608 read earlier as follows:
*
*       -  if  the  maximum and minimum sum insured amounts are
*       both zero, non-display  and  protect  the  sum  insured
*       amount.    If  the  minimum and maximum  are  both  the
*       same, display the  amount  protected.  Otherwise  allow
*       input.
*
*       -    if    all    the   valid   mortality  classes  are
*       blank, non-display and protect this   field.  If  there
*       is  only one mortality class, display  and  protect  it
*       (no validation will be required).
*
*       - if all the valid  lien codes are  blank,  non-display
*       and  protect this field. Otherwise, this is an optional
*       field.
*
*       - if the cessation  AGE  section  on  T5608  is  blank,
*       protect the two age related fields.
*
*       -    if  the  cessation  Disability  section  on  T5608
*       is  blank, protect the two Disability related fields.
*
*       - using the age next  birthday  (ANB  at   RCD)    from
*       the  applicable  life (see above), look up Issue Age on
*       the AGE and Disability sections.   If  the    age  fits
*       into  only  one  "slot" in one of these  sections,  and
*       the risk cessation limits are the same,  default    and
*       protect    the    risk  cessation fields.  Also  do the
*       same  for  the  premium  cessation details.    In  this
*       case,    also    calculate    the    risk   and premium
*       cessation dates.
*
*       -  please note that it  is  possible  for   the    risk
*       and  premium  cessation  dates    to   be  overwritten,
*       i.e.  even after they are calculated.
*
*      OPTIONS AND EXTRAS
*
*  If options and extras are  not   allowed    (as  defined  by
*  T5608) non-display and protect the fields.
*
*  Otherwise,  read  the  options  and  extras  details for the
*  current coverage/rider.   If any records  exist,    put    a
*  '+'    in   the Options/Extras indicator (to show that there
*  are some).
*
*  If options and extras  are  not  allowed,  then  non-display
*  and  protect  the  special  terms    narrative  clause  code
*  fields. If special terms are present,    then    obtain  the
*  clause  codes  from  the  coverage  record and output to the
*  screen.
*
* REASSURANCE
*
* If Reassurance is not allowed ( as defined by T5687 ), then
* non-display and protect the field ( S5123-RATYPIND ).
*
* Otherwise,  read  the  Reassurance  details  for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the  Reassurance  indicator (to show that there are some).
*
*  Finally after setting up  all  of  the    screen  attributes
*  move over the relevant fields from COVTMAJ where applicable.
*
*  Validation.
*  -----------
*
*  Skip  this  section  if    returning    from    an  optional
*  selection (current stack position action flag = '*').
*
*  If CF11 (KILL) is requested then skip the validation.
*
*  Before the premium amount  is  calculated,  the screen  must
*  be  valid.    So  all  editing    is  completed  before  the
*  premium is calculated.
*
*  Table T5608 (previously read)  is    used    to  obtain  the
*  editing  rules.    Edit  the  screen according  to the rules
*  defined by the help. In particular:-
*
*       1) Check the  sum-assured,  if   applicable,    against
*       the limits.
*
*       2)   Check  the  consistency  of    the  risk  age  and
*       Disability  fields  and  premium  age  and   Disability
*       fields.  Either,  risk  age and and premium age must be
*       used  or  risk  Disability and premium Disability  must
*       be  used.    They must not be combined. Note that these
*       only need validating if they were not defaulted.
*
*       3) Mortality-Class, if the  mortality    class  appears
*       on  a  coverage/rider  screen  it is a compulsory field
*       because  it  will    be  used  in    calculating    the
*       premium    amount.    The  mortality class entered must
*       one  of the ones in the edit rules table.
*
*  Calculate the following:-
*
*       - risk cessation date
*
*       - premium cessation date
*
*       - note, the risk and premium  cessation  dates  may  be
*       overwritten by the user.
*
*  OPTIONS AND EXTRAS
*
*  If  options/extras  already  exist, there  will  be a '+' in
*  this field.  A request to access  the  details  is  made  by
*  entering  'X'.    No other values  (other  than  blank)  are
*  allowed.  If  options  and  extras  are  requested,  DO  NOT
*  CALCULATE THE PREMIUM THIS TIME AROUND.
*
* REASSURANCE
*
* If  Reassurance  already exists, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed.
*
*  PREMIUM CALCULATION.
*  --------------------
*
*  The    premium   amount is  required  on  all  products  and
*  all validating  must  be   successfully  completed    before
*  it    is  calculated.  If  there  is    no   premium  method
*  defined (i.e the relevant code  was  blank),    the  premium
*  amount  must  be  entered.  Otherwise,  it  is  optional and
*  always calculated.
*
*  To  calculate  it,    call    the    relevant    calculation
*  subroutine worked out above passing:
*
*       - 'CALC' for the function
*       - Coverage code (crtable)
*       - Company
*       - Contract number
*       - Life number
*       -  Joint life number (if the screen indicator is set to
*       'J' ,otherwise pass '00')
*       - Coverage number
*       - Rider number (0 for coverages)
*       - Effective date (Premium / Risk)
*       - Termination date (i.e. Disability cessation date)
*       - Currency
*       - Sum insured
*       - Calculation "Basis" (i.e. mortality code)
*       - Payment frequency (from the contract header)
*       - Method of payment (from the contract header)
*       - Premium amount ("instalment")
*       - Return Status
*       - Rating Date (effective date of transaction)
*
*  Subroutine may look up:
*
*       - Life and Joint-life details
*       - Options/Extras
*
*  Having calculated it, the    entered    value,  if  any,  is
*  compared  with  it  to check that it  is  within  acceptable
*  limits of the automatically  calculated  figure.    If    it
*  is    less    than    the  amount    calculated  and  within
*  tolerance  then  the  manually entered  amount  is  allowed.
*  If    the    entered  value  exceeds the calculated one, the
*  calculated value is used.
*
*  To check the tolerance  amount,  read  the  tolerance  limit
*  from  T5667  (item  is  transaction  and  coverage  code, if
*  not found, item and '****'). Although this  is    a    dated
*  table, just read the latest one (using ITEM).
*
*      If 'CALC' was entered then re-display the screen.
*
*  Updating.
*  ---------
*
*  Updating  occurs  if  any of the  fields  on the screen have
*  been amended from the original COVTMAJ details.
*
*  If the 'KILL' function key was pressed skip the updating.
*
*  Update the COVTMAJ record with the details from  the  screen
*  and write/rewrite the record to the database.
*
*  IF no Coverage/rider details have been changed, but
*    Reassurance or Options/Extras details have been amended,
*    we force the program to produce a COVT record so that the
*    Component change AT module is executed, thus writing a PTRN
*    indicating that something has happened to the Coverage/Rider
*    details.
*
*  Next Program.
*  -------------
*
*  The    first  thing  to   consider   is   the   handling  of
*  an options/extras request. If  the    indicator  is  'X',  a
*  request  to  visit options and extras has been made. In this
*  case:
*
*       - change the options/extras request indicator to '?',
*
*       - save the next 8 programs from the program stack,
*
*       - call GENSSWCH with and  action  of  'A'  to  retrieve
*       the  program  switching  required, and move them to the
*       stack,
*
*       - set the current stack "action" to '*',
*
*       - add one to the program pointer and exit.
*
*  On return from this  request,  the  current  stack  "action"
*  will  be    '*' and the  options/extras  indicator  will  be
*  '?'.  To handle the return from options and extras:
*
*       -  calculate  the  premium  as  described   above    in
*       the  'Validation'  section,  and  check    that  it  is
*       within the tolerance limit,
*
*       -  blank  out  the  stack   "action",    restore    the
*       saved programs to the program stack,
*
*       -  if  the  tolerance   checking  results  in  an error
*       being detected, set WSSP-NEXTPROG  to    the    current
*       screen name (thus returning to re-display the screen).
*
* If Reassurance has been requested (the indicator = 'X'), then..
*
*  - change the Reassurance request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with and  action  of 'B' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  Reassurance  indicator  will  be  '?'.  To
* handle the return from Reassurance:
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
*  If  control  is passed to this  part  of the 4000 section on
*  the way  out  of the program,  ie.   after    screen    I/O,
*  then    the  current  stack  position  action  flag  will be
*  blank.  If the 4000 section  is  being    performed    after
*  returning      from  processing  another  program  then  the
*  current  stack  position action flag will be '*'.
*
*  If 'Enter' has been pressed  add 1 to  the  program  pointer
*  and exit.
*
*  If  'KILL'  has  been requested, (CF11), then move spaces to
*  the current program entry in the program stack and exit.
*
*  ANNUITY DETAILS SELECT SCREEN
*  =============================
*
*  As part of the 9405 Annuities Development a new field,
*  Annuity Details, is displayed if the component is an annuity
*  component.
*
*  These components are identified by their coverage code
*  (CRTABLE) being a valid item on the Annuity Component Edit
*  Rules Table (T6625). The new field should only be displayed
*  if the coverage code is on this table.
*
*  If the details have not already been created for this
*  component, an X is displayed  in ANNTIND, otherwise a +
*  is displayed.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr680 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR680");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaTempPrev = new ZonedDecimalData(8, 0).setUnsigned();//ILIFE-2388
	private ZonedDecimalData wsaaSumins = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaMinsumins = new ZonedDecimalData(17, 2);
	private PackedDecimalData wsaaWorkingAnb = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaBenCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaPovrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPovrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPovrLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPovrCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPovrRider = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaBcesageStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaPcestermStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRcestermStore = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaBcestermStore = new ZonedDecimalData(3, 0);
	private String wsaaPovrModified = "N";
	private final int wsaaMaxOcc = 8;
	private final int wsaaMaxMort = 6;
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);
	private String wsaaAnnuity = "";

	private FixedLengthStringData wsaaPremStatuz = new FixedLengthStringData(1);
	private Validator premReqd = new Validator(wsaaPremStatuz, "Y");
	private Validator premNotReqd = new Validator(wsaaPremStatuz, "N");
	private Validator userPremEntered = new Validator(wsaaPremStatuz, "U");

	private FixedLengthStringData wsbbTr687Itemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsbbTr687Crtable = new FixedLengthStringData(4).isAPartOf(wsbbTr687Itemitem, 0);
	private FixedLengthStringData wsbbTr687Benpln = new FixedLengthStringData(2).isAPartOf(wsbbTr687Itemitem, 4);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaTranDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaTranMm = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();

	private ZonedDecimalData wsaaPolicyYear = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator n1stYearPolicy = new Validator(wsaaPolicyYear, 1);
	private Validator n2ndYearPolicy = new Validator(wsaaPolicyYear, 2);
		/* WSAA-PERIOD-ARRAYS */
	private FixedLengthStringData wsaaMonthlyArray = new FixedLengthStringData(24).init("010203040506070809101112");

	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaMonthlyArray, 0, FILLER_REDEFINE);
	private ZonedDecimalData[] wsaaMonth = ZDArrayPartOfStructure(12, 2, 0, filler1, 0, UNSIGNED_TRUE);
	private FixedLengthStringData wsaaQuarterlyArray = new FixedLengthStringData(8).init("03060912");

	private FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(wsaaQuarterlyArray, 0, FILLER_REDEFINE);
	private ZonedDecimalData[] wsaaQuarter = ZDArrayPartOfStructure(4, 2, 0, filler2, 0, UNSIGNED_TRUE);
	private FixedLengthStringData wsaaHalfYearlyArray = new FixedLengthStringData(4).init("0612");

	private FixedLengthStringData filler3 = new FixedLengthStringData(4).isAPartOf(wsaaHalfYearlyArray, 0, FILLER_REDEFINE);
	private ZonedDecimalData[] wsaaHalfYear = ZDArrayPartOfStructure(2, 2, 0, filler3, 0, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaFrequency = new FixedLengthStringData(2);
	private Validator yearly = new Validator(wsaaFrequency, "01");
	private Validator halfYearly = new Validator(wsaaFrequency, "02");
	private Validator quarterly = new Validator(wsaaFrequency, "04");
	private Validator monthly = new Validator(wsaaFrequency, "12");
	private Validator single = new Validator(wsaaFrequency, "00");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaCalcPrem = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator policyLevel = new Validator(wsaaPlanproc, "N");
	private Validator planLevel = new Validator(wsaaPlanproc, "Y");

	private FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	private Validator firstTaxCalc = new Validator(wsaaFirstTaxCalc, "Y");
	private Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");

	private FixedLengthStringData wsaaLifeind = new FixedLengthStringData(1);
	private Validator jointlif = new Validator(wsaaLifeind, "J");
	private Validator singlif = new Validator(wsaaLifeind, "S");

	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private Validator addComp = new Validator(wsaaFlag, "A");
	private Validator modifyComp = new Validator(wsaaFlag, "M");
	private Validator compEnquiry = new Validator(wsaaFlag, "I");
//	MIBT-324
	private Validator compApp = new Validator(wsaaFlag, "P");
	
	//ILIFE-1407 START by nnazeer
	private Validator compModifyApp = new Validator(wsaaFlag, "R");
	
	//ILIFE-1407 END
	
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaStorePlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaCredit = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNextSeqnbr = new PackedDecimalData(3, 0).init(999);
	private PackedDecimalData wsaaStoreSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(15, 0);
	private ZonedDecimalData wsaaOldPrem = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaSuminsDiff = new PackedDecimalData(17, 0);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaOptext = new FixedLengthStringData(1);
	private Validator optextYes = new Validator(wsaaOptext, "Y");
	private Validator optextNo = new Validator(wsaaOptext, "N");

	private FixedLengthStringData wsaaIfSuminsChanged = new FixedLengthStringData(1);
	private Validator wsaaSuminsChanged = new Validator(wsaaIfSuminsChanged, "I", "D");
	private Validator wsaaSuminsIncreased = new Validator(wsaaIfSuminsChanged, "I");
	private Validator wsaaSuminsDecreased = new Validator(wsaaIfSuminsChanged, "D");

	private FixedLengthStringData wsaaIfPremChanged = new FixedLengthStringData(1);
	private Validator wsaaPremChanged = new Validator(wsaaIfPremChanged, "I", "D");
	private Validator wsaaPremIncreased = new Validator(wsaaIfPremChanged, "I");
	private Validator wsaaPremDecreased = new Validator(wsaaIfPremChanged, "D");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
		/* WSAA-MAIN-LIFE-DETS */
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler4, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler6, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);

	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private FixedLengthStringData filler8 = new FixedLengthStringData(49).isAPartOf(wsaaCovrKey, 15, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaOldMortcls = new FixedLengthStringData(1);
		/* WSBB-JOINT-LIFE-DETS */
	private PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsbbCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
	private ZonedDecimalData wsddRiskCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddBenCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddRiskCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddBenCessTerm = new ZonedDecimalData(3, 0);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzRiskCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzBenCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzRiskCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzBenCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsccSingPremFlag = new FixedLengthStringData(1);
	private Validator singlePremium = new Validator(wsccSingPremFlag, "Y");
	private FixedLengthStringData wsaaLextUpdates = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private FixedLengthStringData wsaaHbnfLivesnoChar = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaHbnfLivesnoNum = new ZonedDecimalData(1, 0).isAPartOf(wsaaHbnfLivesnoChar, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaLivesno = new FixedLengthStringData(1);
	private Validator validLivesno = new Validator(wsaaLivesno, "1", "2", "3", "4");

	private FixedLengthStringData wsaaWaiverprem = new FixedLengthStringData(1);
	private Validator validWaiverprem = new Validator(wsaaWaiverprem, "Y", "N");

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private AnntmjaTableDAM anntmjaIO = new AnntmjaTableDAM();
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CoexTableDAM coexIO = new CoexTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovrrgwTableDAM covrrgwIO = new CovrrgwTableDAM();
	//private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextrevTableDAM lextrevIO = new LextrevTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private MandTableDAM mandIO = new MandTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private PovrTableDAM povrIO = new PovrTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Gensswrec gensswrec = new Gensswrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Pr676cpy pr676cpy = new Pr676cpy();
	private T2240rec t2240rec = new T2240rec();
	private T5606rec t5606rec = new T5606rec();
	private T5667rec t5667rec = new T5667rec();
	private T5671rec t5671rec = new T5671rec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T6005rec t6005rec = new T6005rec();
	private Tr687rec tr687rec = new Tr687rec();
	private Tr686rec tr686rec = new Tr686rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Wssplife wssplife = new Wssplife();
	private Sr680ScreenVars sv = ScreenProgram.getScreenVars( Sr680ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaDefaultsInner wsaaDefaultsInner = new WsaaDefaultsInner();

    private Datcon4rec datcon4rec = new Datcon4rec();	// TSD-308
	private ZonedDecimalData wsaaMnthVers = new ZonedDecimalData(8, 0);	// TSD-308
	private ZonedDecimalData wsaaLastAnnv = new ZonedDecimalData(8, 0);	// TSD-308
	private CovrTableDAM covrIO = new CovrTableDAM();	// TSD-308
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	boolean isFeaAllowed = false;
	/*BRD-306 START */	
	private ZonedDecimalData wsaaPrevDue = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNextDue = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	/*BRD-306 END */
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	private boolean loadingFlag = false;/* BRD-306 */
	
	private boolean incomeProtectionflag = false;
	private boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private String occuptationCode="";
	private boolean waitperiodFlag=false;
	private boolean bentrmFlag=false;
	private boolean poltypFlag=false;
	private boolean prmbasisFlag=false;
	private boolean dialdownFlag = false;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private Exclpf exclpf=null;
	private boolean exclFlag = false;
    private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
    boolean CSMIN003Permission  = false;
    private static final String t6a7 = "T6A7";
    private static final String t6a8 = "T6A8";
    
    boolean NBPRP056Permission  = false;
    private Itempf itempf = null;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private T3644rec t3644rec = new T3644rec();
	private T5661rec t5661rec = new T5661rec();
	private Ta610rec ta610rec = new Ta610rec();
	private FixedLengthStringData wsaaOccupationClass = new FixedLengthStringData(2);
	boolean isFollowUpRequired  = false;
	private Fluppf fluppf;
	private int fupno = 0;
	private List<Fluppf> fluppfList = new ArrayList<Fluppf>();
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	private Descpf fupDescpf;
	private PackedDecimalData wsaaCount = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private static final String NBPRP056 = "NBPRP056";
	private static final String t5661 = "T5661";
	private static final String tA610 = "TA610";
	private static final String t3644 = "T3644";
	List<Fluppf> fluppfAvaList =  new ArrayList<Fluppf>();	//ICIL-1494
	private List<Itempf> itempfList = new ArrayList<Itempf>();	//ICIL-1494
	//ILB-456 starts
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf=new Chdrpf();
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList;
	//ILJ-47 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	private String itemPFX = "IT";
	//ILJ-47 End
	private CovtpfDAO covtpfDAO= getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private Covtpf covtpf = new Covtpf();
	private List<Covtpf> listCovtpf = new ArrayList<Covtpf>();

	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		setScreen1060, 
		exit1090, 
		exit1190, 
		preExit, 
		cont2030, 
		beforeExit2080, 
		exit2090, 
		checkSuminCont2526, 
		checkRcessFields2120, 
		validDate2140, 
		datesCont2140, 
		ageAnniversary2140, 
		check2140, 
		checkOccurance2140, 
		checkTermFields2150, 
		checkComplete2160, 
		checkLiencd2180, 
		checkMore2190, 
		cont, 
		exit2290, 
		exit3290, 
		exit50z0, 
		exit5290, 
		exit5390, 
		covtExist6025, 
		exit8290
	}

	public Pr680() {
		super();
		screenVars = sv;
		new ScreenModel("Sr680", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					contractHeader1020();
					getPolicyYear1025();
					contractCoverage1030();
					headerToScreen1040();
					lifeDetails1050();
				case setScreen1060: 
					setScreen1060();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		NBPRP056Permission  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), NBPRP056, appVars, "IT");		
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		if(isNE(wsspcomn.flag,"X"))
		{
			wsaaFlag.set(wsspcomn.flag); 
		}
		else {
			if (isEQ(wsspcomn.sbmaction,"C")) {  
				wsaaFlag.set("P"); 
				
			}
			 
		}
		//wsaaFlag.set(wsspcomn.flag);
		
        CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");
		isFeaAllowed = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "MTL30801", appVars,smtpfxcpy.item.toString());
		sv.dataArea.set(SPACES);
		sv.anbAtCcd.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.zlinstprem.set(ZERO);
		sv.adjustageamt.set(ZERO);
		prmbasisFlag=false;
		poltypFlag=false;
		bentrmFlag=false;
		waitperiodFlag=false;
		/*BRD-306 START */
		sv.loadper.set(0);
		sv.rateadj.set(0);
		sv.fltmort.set(0);
		sv.premadj.set(0);
		/*BRD-306 END */
		sv.waitperiod.set(SPACES);
		sv.bentrm.set(SPACES);
		sv.prmbasis.set(SPACES);
		sv.poltyp.set(SPACES);
		sv.dialdownoption.set(SPACES);
		sv.premCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.benCessAge.set(ZERO);
		sv.benCessTerm.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.sumin.set(ZERO);
		sv.numpols.set(ZERO);
		sv.planSuffix.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		wsaaSumin.set(ZERO);
		wsaaOldPrem.set(ZERO);
		wsaaOldSumins.set(ZERO);
		wsaaPremDiff.set(ZERO);
		wsaaSuminsDiff.set(ZERO);
		wsaaTaxamt.set(ZERO);
		wsaaIndex.set(ZERO);
		wsaaAnnuity = "N";
		sv.premCessDate.set(varcom.vrcmMaxDate);
		sv.benCessDate.set(varcom.vrcmMaxDate);
		sv.riskCessDate.set(varcom.vrcmMaxDate);
		wsaaPcesageStore.set(ZERO);
		wsaaRcesageStore.set(ZERO);
		wsaaBcesageStore.set(ZERO);
		wsaaPcestermStore.set(ZERO);
		wsaaRcestermStore.set(ZERO);
		wsaaBcestermStore.set(ZERO);
		wsaaFirstTaxCalc.set("Y");
		//ILJ-47 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{
			//sv.rcessageOut[varcom.nd.toInt()].set("Y");	
			sv.contDtCalcScreenflag.set("N");
		}
		else
		{
			sv.contDtCalcScreenflag.set("Y");	
		}
		//ILJ-47 End
	}

protected void contractHeader1020()
	{
	//ILB-456
	chdrpf = chdrpfDAO.getCacheObject(chdrpf);
	if(null==chdrpf) {
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		else {
			chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
			if(null==chdrpf) {
				fatalError600();
			}
			else {
				chdrpfDAO.setCacheObject(chdrpf);
			}
		}
	}
		/*chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		/*BRD-306 START */
		sv.cnttype.set(chdrpf.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*BRD-306 END */
		
		loadingFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		
		/*BRD-306 END */
		
		
		chdrlnbIO.setChdrcoy(chdrpf.getChdrcoy());
		chdrlnbIO.setChdrnum(chdrpf.getChdrnum());
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		
		
			if (addComp.isTrue()) {   
				sv.effdate.set(wsspcomn.currfrom);
			}
			else {
				sv.effdate.set(chdrpf.getOccdate());
			}		
			
			
		
	}

protected void getPolicyYear1025()
	{
		datcon3rec.intDate1.set(chdrpf.getOccdate());
		datcon3rec.intDate2.set(chdrpf.getBtdate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaPolicyYear.set(datcon3rec.freqFactor);
		if (isGT(wsaaPolicyYear,3)) {
			wsaaPolicyYear.set(3);
		}
	}

protected void contractCoverage1030()
	{
			//ILB-456 
			covrpf = covrpfDAO.getCacheObject(covrpf);
			if(null==covrpf) {
				covrmjaIO.setFunction(varcom.retrv);
				SmartFileCode.execute(appVars, covrmjaIO);
				if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(covrmjaIO.getParams());
					fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
			else {
				covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		/*covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		if (addComp.isTrue()) {
		sv.effdate.set(wsspcomn.currfrom);
			}
		else {
			sv.effdate.set(covrpf.getCurrfrom());
		}
		covrenqIO.setChdrcoy(covrpf.getChdrcoy());
		covrenqIO.setChdrnum(covrpf.getChdrnum());
		covrenqIO.setLife(covrpf.getLife());
		covrenqIO.setCoverage(covrpf.getCoverage());
		covrenqIO.setRider("00");
		covrenqIO.setPlanSuffix(covrpf.getPlanSuffix());
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		wsaaSumins.set(covrenqIO.getSumins());
		covrenqIO.setChdrcoy(covrpf.getChdrcoy());
		covrenqIO.setChdrnum(covrpf.getChdrnum());
		covrenqIO.setLife(covrpf.getLife());
		covrenqIO.setCoverage(covrpf.getCoverage());
		covrenqIO.setRider(covrpf.getRider());
		covrenqIO.setPlanSuffix(covrpf.getPlanSuffix());
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqIO.getStatuz(),varcom.mrnf)) {
			covrenqIO.setFormat(formatsInner.covrenqrec);
			covrenqIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, covrenqIO);
			if (isNE(covrenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrenqIO.getParams());
				fatalError600();
			}
		}
		hbnfIO.setChdrcoy(chdrpf.getChdrcoy());
		hbnfIO.setChdrnum(chdrpf.getChdrnum());
		hbnfIO.setLife(covrpf.getLife());
		hbnfIO.setCoverage(covrpf.getCoverage());
		hbnfIO.setRider(covrpf.getRider());

		hbnfIO.setFunction(varcom.readr);
		hbnfIO.setFormat(formatsInner.hbnfrec);
		callHbnf1950();
		if (isEQ(hbnfIO.getStatuz(),varcom.mrnf)) {
			hbnfIO.setRecNonKeyData(SPACES);
		}
		if (isEQ(hbnfIO.getStatuz(),varcom.mrnf)) {
			sv.waiverprem.set(SPACES);
			sv.livesno.set(SPACES);
			sv.viewplan.set(SPACES);
			sv.benpln.set(SPACES);
			sv.coverdtl.set("X");
			sv.zunit.set(ZERO);
		}
		else {
			sv.coverdtl.set("+");
			sv.waiverprem.set(hbnfIO.getWaiverprem());
			sv.livesno.set(hbnfIO.getLivesno());
			sv.zunit.set(hbnfIO.getZunit());
			sv.benpln.set(hbnfIO.getBenpln());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrpf.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01,SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02,SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else { //MTL002
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}//MTL002
			}
		}
		/*  Read TR52D for Taxcode                                         */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrpf.getReg());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");
			sv.taxindOut[varcom.pr.toInt()].set("N");
	
		}
		/* Read the PAYR record to get the Billing Details.*/
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrpf.getChdrnum());
		payrIO.setPayrseqno(covrpf.getPayrseqno());
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		wsaaCrtable.set(covrpf.getCrtable());
		if (isEQ(wsaaPlanSuffix,ZERO)) {
			covrpfDAO.deleteCacheObject(covrpf);
		}
		if (modifyComp.isTrue()
		&& isEQ(payrIO.getBillfreq(),ZERO)) {
			wsccSingPremFlag.set("Y");
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr686);
		itemIO.setItemitem(wsaaCrtable);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr686rec.tr686Rec.set(itemIO.getGenarea());
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			tr686rec.waivercode.set(SPACES);
		}
	}
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	rcvdPFObject.setChdrcoy(covrenqIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covrenqIO.getChdrnum().toString());
	rcvdPFObject.setLife(covrenqIO.getLife().toString());
	rcvdPFObject.setCoverage(covrenqIO.getCoverage().toString());
	rcvdPFObject.setRider(covrenqIO.getRider().toString());
	rcvdPFObject.setCrtable(covrenqIO.getCrtable().toString());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}

protected void headerToScreen1040()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1100();
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6625);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(tablesInner.t6625, itdmIO.getItemtabl())
		|| isNE(covrpf.getCrtable(),itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		getAnny8000();
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			sv.anntindOut[varcom.nd.toInt()].set("N");
			wsaaAnnuity = "Y";
			if (isEQ(annyIO.getStatuz(),varcom.mrnf)
			&& addComp.isTrue()) {
				sv.anntind.set("X");
				sv.anntindOut[varcom.pr.toInt()].set("Y");
			}
			else {
				sv.anntind.set("+");
			}
		}
		else {
			sv.anntindOut[varcom.nd.toInt()].set("Y");
			sv.anntindOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void lifeDetails1050()
	{
		lifemjaIO.setChdrcoy(covrpf.getChdrcoy());
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		calcAge1200();
		wsaaAnbAtCcd.set(wsaaWorkingAnb);
		if (!addComp.isTrue()) {
			wsaaAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		wsaaCltdob.set(lifemjaIO.getCltdob());
		wsaaSex.set(lifemjaIO.getCltsex());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifemjaIO.getLifcnum());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		if(isNE(cltsIO.getStatcode(), SPACES)&& (incomeProtectionflag||premiumflag)){
			occuptationCode=cltsIO.getStatcode().toString();
		}
		lifemjaIO.setChdrcoy(covrpf.getChdrcoy());
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsbbAnbAtCcd.set(0);
			wsaaLifeind.set("S");
			wsbbSex.set(SPACES);
			goTo(GotoLabel.setScreen1060);
		}
		else {
			wsaaLifeind.set("J");
		}
		if (addComp.isTrue()) {
			calcAge1200();
			wsbbAnbAtCcd.set(wsaaWorkingAnb);
		}
		else {
			wsbbAnbAtCcd.set(lifemjaIO.getAnbAtCcd());
		}
		wsbbCltdob.set(lifemjaIO.getCltdob());
		wsbbSex.set(lifemjaIO.getCltsex());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.jlifcnum.set(lifemjaIO.getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void setScreen1060()
	{
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.numpols.set(chdrpf.getPolinc());
		sv.life.set(lifemjaIO.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.anbAtCcd.set(wsaaAnbAtCcd);
		sv.currcd.set(payrIO.getCntcurr());
		wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		//covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		covtpf.setChdrcoy(covrpf.getChdrcoy());
		covtpf.setChdrnum(covrpf.getChdrnum());
		covtpf.setLife(covrpf.getLife());
		covtpf.setCoverage(covrpf.getCoverage());
		covtpf.setRider(covrpf.getRider());
		if (isEQ(covrpf.getPlanSuffix(),ZERO)
		&& !addComp.isTrue()
//		MIBT-324
			&& !compApp.isTrue()
			&& !compModifyApp.isTrue()) {
			covrpf.setPlanSuffix(9999);
			covtpf.setPlnsfx(9999);
			covrpfList=covrpfDAO.selectCovrRecord(covrpf);
			//covtpf.setFunction(varcom.begn);
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaPlanproc.set("Y");
		}
		else {
			wsaaPlanproc.set("N");
			listCovtpf = covtpfDAO.getCovtpfData(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife().trim(),covrpf.getCoverage().trim(),covrpf.getRider(),covrpf.getPlanSuffix());
			if (!(listCovtpf.isEmpty())) {
				covtpf = listCovtpf.get(0);
			}
			//covtmjaIO.setDataKey(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
			//covtmjaIO.setFunction(varcom.readr);
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		if (modifyComp.isTrue()
		&& isNE(covrpf.getCpiDate(),ZERO)
		&& isNE(covrpf.getCpiDate(),varcom.vrcmMaxDate)) {
			scrnparams.errorCode.set(errorsInner.f862);
		}
		dialdownFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		incomeProtectionflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
		premiumflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(incomeProtectionflag || premiumflag || dialdownFlag){
			callReadRCVDPF();
		}if(!incomeProtectionflag){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
			sv.poltypOut[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.waitperiodOut[varcom.nd.toInt()].set("N");
			sv.bentrmOut[varcom.nd.toInt()].set("N");
			sv.poltypOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getWaitperiod()!=null && !rcvdPFObject.getWaitperiod().trim().equals("") && isEQ(sv.waitperiod,SPACES)){
					sv.waitperiod.set(rcvdPFObject.getWaitperiod());
				}
				if(rcvdPFObject.getPoltyp()!=null && !rcvdPFObject.getPoltyp().trim().equals("") && isEQ(sv.poltyp,SPACES)){
					sv.poltyp.set(rcvdPFObject.getPoltyp());
				}
				if(rcvdPFObject.getBentrm()!=null && !rcvdPFObject.getBentrm().trim().equals("") && isEQ(sv.bentrm,SPACES)){
					sv.bentrm.set(rcvdPFObject.getBentrm());
				}
			}
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
 			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
				}
			}
		}
		exclFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP020", appVars, "IT");//IJTI-1410
		if(!exclFlag){
			sv.exclindOut[varcom.nd.toInt()].set("Y");
		}
		else{
			checkExcl();
		}
		policyLoad5000();
		sv.frqdescOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.linstamtOut[varcom.pr.toInt()].set("Y");
	}
protected void checkExcl()
{
	exclpf=exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),covrpf.getCrtable(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());//IJTI-1410
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
}
protected void loadHeading1100()
	{
		try {
			loadScreen1110();
		}
		catch (GOTOException e){
		}
	}

protected void loadScreen1110()
	{
		wsaaHeading.set(SPACES);
		for (wsaaX.set(30); !(isLT(wsaaX,1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(30,wsaaX));
		if (isNE(wsaaY,0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		wsaaZ.set(0);
		wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1130();
		}
		wsaaX.add(1);
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1190);
	}

protected void moveChar1130()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY,wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void calcAge1200()
	{
		init1210();
	}

protected void init1210()
	{
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrpf.getCnttype());
		agecalcrec.intDate1.set(lifemjaIO.getCltdob());
		agecalcrec.intDate2.set(payrIO.getBtdate());
		agecalcrec.company.set(wsspcomn.fsuco);
		if (isFeaAllowed) {
			covrIO.setChdrcoy(covrpf.getChdrcoy());
			covrIO.setChdrnum(covrpf.getChdrnum());
			covrIO.setLife(covrpf.getLife());
			covrIO.setCoverage(covrpf.getCoverage());
			covrIO.setRider("00");
			covrIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, covrIO);
			if (isNE(covrIO.getStatuz(),varcom.oK)
			&& isNE(covrIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(covrIO.getParams());
				fatalError600();
			}
			caluclateTransactionDate();	
			calculateMonthiversary(); 
			caluculateAnniversarydate(); 
			agecalcrec.intDate2.set(wsaaLastAnnv);	
		}
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		wsaaWorkingAnb.set(agecalcrec.agerating);
	}
protected void caluclateTransactionDate(){
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	if (isNE(datcon1rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon1rec.datcon1Rec);
		syserrrec.statuz.set(datcon1rec.statuz);
		fatalError600();
	}
}
protected void calculateMonthiversary(){
	datcon3rec.intDate1.set(chdrpf.getOccdate());
	datcon3rec.intDate2.set(datcon1rec.intDate);
	datcon3rec.frequency.set("12");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	ZonedDecimalData var = new ZonedDecimalData(11, 5).init(0);
	compute(var).set(add(datcon3rec.freqFactor, .99999));
	datcon2rec.intDate1.set(covrIO.getCrrcd());
	datcon2rec.frequency.set("12");
	datcon2rec.freqFactor.set(var);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isEQ(datcon2rec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		syserrrec.params.set(datcon2rec.datcon2Rec);
		fatalError600();
	}
	wsaaMnthVers.set(datcon2rec.intDate2);
}
protected void caluculateAnniversarydate(){
	datcon4rec.datcon4Rec.set(SPACES);
	datcon4rec.frequency.set("01");
	datcon4rec.freqFactor.set(001);                               
	datcon4rec.intDate1.set(covrIO.getCurrfrom());                       
	datcon4rec.billmonth.set(covrIO.getCurrfrom().toStringRaw().substring(4, 6));
	datcon4rec.billday.set(covrIO.getCurrfrom().toStringRaw().substring(6, 8));
	while ( !(isGT(datcon4rec.intDate2,wsaaMnthVers))) {
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalError600();
		}
		wsaaLastAnnv.set(datcon4rec.intDate1);
		datcon4rec.intDate1.set(datcon4rec.intDate2);
	}	
}

protected void setupBonus1300()
	{
//	MIBT-324
	   if (addComp.isTrue()
		    	|| modifyComp.isTrue()){
		        para1300();
		       }
		}

protected void para1300()
	{
		sv.bappmethOut[varcom.pr.toInt()].set(SPACES);
		sv.bappmethOut[varcom.nd.toInt()].set(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covtpf.getChdrcoy());
		itemIO.setItemitem(covtpf.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
			return ;
		}
		t6005rec.t6005Rec.set(itemIO.getGenarea());
		if (isEQ(t6005rec.ind,"1")) {
			sv.bappmeth.set(t6005rec.bappmeth01);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"2")) {
			sv.bappmeth.set(t6005rec.bappmeth02);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"3")) {
			sv.bappmeth.set(t6005rec.bappmeth03);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"4")) {
			sv.bappmeth.set(t6005rec.bappmeth04);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind,"5")) {
			sv.bappmeth.set(t6005rec.bappmeth05);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.nd.toInt()].set("Y");
		}
	}
protected void getNextEffdate1400()
{
	/*GET-EFFDATE*/
	datcon2rec.freqFactor.set(1);
	datcon2rec.frequency.set(chdrpf.getBillfreq());
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isEQ(datcon2rec.statuz,varcom.bomb)) {
		syserrrec.params.set(datcon2rec.datcon2Rec);
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	wsaaTempPrev.set(datcon2rec.intDate1);
	datcon2rec.intDate1.set(datcon2rec.intDate2);
	wsaaFreq.add(1);
	/*EXIT*/
}
protected void callHbnf1950()
	{
		/*READ-HBNF*/
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(),varcom.oK)
		&& isNE(hbnfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
			callScreenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
	if (isEQ(wsspcomn.sbmaction,"D") || isEQ(wsspcomn.sbmaction,"C")) {  // Ticket #ILIFE-7244
		scrnparams.function.set(varcom.prot);
	}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		readPovr9000();
		if (isEQ(povrIO.getStatuz(),varcom.endp)
		|| isNE(povrIO.getChdrcoy(),wsaaPovrChdrcoy)
		|| isNE(povrIO.getChdrnum(),wsaaPovrChdrnum)
		|| isNE(povrIO.getLife(),wsaaPovrLife)
		|| isNE(povrIO.getCoverage(),wsaaPovrCoverage)
		|| isNE(povrIO.getRider(),wsaaPovrRider)) {
			sv.pbind.set(SPACES);
			sv.pbindOut[varcom.nd.toInt()].set("Y");
			sv.pbindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.pbind.set("+");
			sv.pbindOut[varcom.nd.toInt()].set("N");
			sv.pbindOut[varcom.pr.toInt()].set("N");
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax8200();
		}
		/*if (isEQ(sv.taxamt, ZERO)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.taxindOut[varcom.nd.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("Y");
		}*/
        if (CSMIN003Permission &&
                ((isEQ(wsaaBatckey.batcBatctrcde, t6a7) ||isEQ(wsaaBatckey.batcBatctrcde, t6a8)))) {
            sv.fuindOut[varcom.nd.toInt()].set("N");
            sv.fuindOut[varcom.pr.toInt()].set("N");
        }
        else {
            sv.fuind.set("+");
            sv.fuindOut[varcom.nd.toInt()].set("Y");
            sv.fuindOut[varcom.pr.toInt()].set("Y");
        }
	}

protected void callScreenIo2010()
	{
		wsaaOldSumins.set(sv.sumin);
		wsaaOldPrem.set(sv.instPrem);
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2001();
					validate2020();
				case cont2030: 
					cont2030();
				case beforeExit2080: 
					beforeExit2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2001()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaIfSuminsChanged.set("N");
		wsaaIfPremChanged.set("N");
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (compEnquiry.isTrue()
				|| compApp.isTrue()
				
				//ILIFE-1407 START by nnazeer
				
				|| compModifyApp.isTrue())
		        
			   //ILIFE-1407 END
		
		{ //MIBT-324
			goTo(GotoLabel.cont2030);
		}
		if (isEQ(sv.rcesdteOut[varcom.pr.toInt()],"Y")
		&& (isNE(sv.premCessAge,wsaaPcesageStore)
		|| isNE(sv.riskCessAge,wsaaRcesageStore)
		|| isNE(sv.benCessAge,wsaaBcesageStore)
		|| isNE(sv.premCessTerm,wsaaPcestermStore)
		|| isNE(sv.riskCessTerm,wsaaRcestermStore)
		|| isNE(sv.benCessTerm,wsaaBcestermStore))) {
			sv.premCessDate.set(varcom.vrcmMaxDate);
			sv.riskCessDate.set(varcom.vrcmMaxDate);
			sv.benCessDate.set(varcom.vrcmMaxDate);
		}
	}

protected void validate2020()
	{
		if (!compEnquiry.isTrue()
				&& !compApp.isTrue()) { //MIBT-324
			editCoverage2100();
			if(NBPRP056Permission) {
				validateOccupationOrOccupationClass();
			}
		}
	}

protected void cont2030()
	{
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind,"+")
		&& isNE(sv.optextind,"X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind,"+")
		&& isNE(sv.pbind,"X")) {
			sv.pbindErr.set(errorsInner.g620);
		}
		if (isNE(sv.anntind, " ")
		&& isNE(sv.anntind,"+")
		&& isNE(sv.anntind,"X")) {
			sv.anntindErr.set(errorsInner.g620);
		}
		if (isNE(sv.coverdtl, " ")
		&& isNE(sv.coverdtl,"+")
		&& isNE(sv.coverdtl,"X")) {
			sv.coverdtlErr.set(errorsInner.g620);
		}
		if (isNE(sv.viewplan, " ")
		&& isNE(sv.viewplan,"+")
		&& isNE(sv.viewplan,"X")) {
			sv.viewplanErr.set(errorsInner.g620);
		}
		/* Check the taxcode indicator.                                    */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		if (exclFlag && isNE(sv.exclind, " ")
		&& isNE(sv.exclind, "+")
		&& isNE(sv.exclind, "X")) {
			sv.exclindErr.set(errorsInner.g620);
		}
		/* If Component Enquiry then skip validation.*/
		if (compEnquiry.isTrue()
				 || compApp.isTrue()) { //MIBT-324
			goTo(GotoLabel.beforeExit2080);
		}
		if (isEQ(sv.zunit,ZERO)) {
			sv.zunitErr.set(errorsInner.w067);
		}
		if (isEQ(sv.benpln,SPACES)) {
			sv.benplnErr.set(errorsInner.f646);
			goTo(GotoLabel.beforeExit2080);
		}
		wsaaLivesno.set(sv.livesno);
		if (!validLivesno.isTrue()) {
			sv.livesnoErr.set(errorsInner.rl87);
		}
		checkLivesno10000();
		if (isEQ(sv.livesno,4)) {
			if (isLTE(wsaaHbnfLivesnoNum,3)) {
				sv.coverdtl.set("X");
			}
		}
		else {
			if (isNE(wsaaHbnfLivesnoChar,sv.livesno)) {
				sv.coverdtl.set("X");
			}
		}
		wsaaWaiverprem.set(sv.waiverprem);
		if (!validWaiverprem.isTrue()) {
			sv.waiverpremErr.set(errorsInner.g616);
		}
		if (isEQ(sv.waiverprem,"Y")) {
			if (isEQ(tr686rec.waivercode,SPACES)) {
				sv.waiverpremErr.set(errorsInner.rl84);
			}
		}
		if (isNE(sv.sumin,wsaaOldSumins)) {
			compute(wsaaSuminsDiff, 0).set(sub(sv.sumin,wsaaOldSumins));
			if (isLT(wsaaSuminsDiff,0)) {
				compute(wsaaSuminsDiff, 0).set(mult(wsaaSuminsDiff,-1));
				wsaaIfSuminsChanged.set("D");
			}
			else {
				wsaaIfSuminsChanged.set("I");
			}
		}
		if (isNE(sv.instPrem,wsaaOldPrem)) {
			compute(wsaaPremDiff, 2).set(sub(sv.instPrem,wsaaOldPrem));
			if (isLT(wsaaPremDiff,0)) {
				compute(wsaaPremDiff, 2).set(mult(wsaaPremDiff,-1));
				wsaaIfPremChanged.set("D");
			}
			else {
				wsaaIfPremChanged.set("I");
			}
		}
		calcSumins2650();
		if(isEQ(sv.dialdownoptionErr,"E034") && !dialdownFlag)
		{
			sv.dialdownoptionErr.set(SPACES);
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.beforeExit2080);
		}
		if (isEQ(sv.sumin,ZERO)) {
			sv.suminErr.set(errorsInner.h113);
		}
		if (isLT(wsaaSumins,wsaaMinsumins)) {
			sv.suminErr.set(errorsInner.rl85);
		}
		calcPremium2200();
		if (isNE(sv.optextind,"X")
		&& isNE(sv.anntind,"X")
		&& isNE(sv.coverdtl,"X")
		&& isNE(sv.viewplan,"X")
		&& isEQ(sv.errorIndicators,SPACES)
		&& addComp.isTrue()) {
			calcPremium2200();
		}
		if (isNE(sv.optextind,"X")
		&& isNE(sv.comind,"X")
		&& isNE(sv.anntind,"X")
		&& isNE(sv.coverdtl,"X")
		&& isNE(sv.viewplan,"X")
		&& isEQ(sv.errorIndicators,SPACES)
		&& modifyComp.isTrue()) {
			calcPremium2200();
		}
	}

protected void beforeExit2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void editCoverage2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkSumin2110();
				case checkSuminCont2526: 
				case checkRcessFields2120: 
					checkRcessFields2120();
					checkPcessFields2130();
					checkBcessFields2135();
					checkAgeTerm2140();
				case validDate2140: 
					validDate2140();
				case datesCont2140: 
					datesCont2140();
				case ageAnniversary2140: 
					ageAnniversary2140();
				case check2140: 
					check2140();
				case checkOccurance2140: 
					checkOccurance2140();
				case checkTermFields2150: 
					checkTermFields2150();
				case checkComplete2160: 
					checkComplete2160();
				case checkLiencd2180: 
					checkLiencd2180();
					loop2185();
				case checkMore2190: 
					checkMore2190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkSumin2110()
	{
		if (isEQ(t5606rec.sumInsMax,t5606rec.sumInsMin)) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkRcessFields2120);
		}
		if (isEQ(sv.sumin,covrpf.getSumins())) {
			wsaaSumin.set(sv.sumin);
			goTo(GotoLabel.checkSuminCont2526);
		}
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)) {
			compute(wsaaSumin, 1).setRounded((div(mult(sv.sumin,chdrpf.getPolinc()),chdrpf.getPolsum())));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
	}

protected void checkRcessFields2120()
	{
		if (isEQ(sv.select,"J")) {
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
			wszzCltdob.set(wsbbCltdob);
		}
		else {
			wszzAnbAtCcd.set(wsaaAnbAtCcd);
			wszzCltdob.set(wsaaCltdob);
		}
		if (isNE(t5687rec.jlifePresent,SPACES)
		&& jointlif.isTrue()) {
			checkDefaults5300();
		}
		if (isGT(sv.riskCessAge,0)
		&& isGT(sv.riskCessTerm,0)) {
			sv.rcessageErr.set(errorsInner.f220);
			sv.rcesstrmErr.set(errorsInner.f220);
		}
		if (isNE(t5606rec.eaage,SPACES)
		&& isEQ(sv.riskCessAge,0)
		&& isEQ(sv.riskCessTerm,0)) {
			sv.rcessageErr.set(errorsInner.e560);
			sv.rcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkPcessFields2130()
	{
		if (isGT(sv.premCessAge,0)
		&& isGT(sv.premCessTerm,0)) {
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.premCessAge,0)
		&& isEQ(sv.premCessTerm,0)
		&& isEQ(sv.rcessageErr,SPACES)
		&& isEQ(sv.rcesstrmErr,SPACES)) {
			sv.premCessAge.set(sv.riskCessAge);
			sv.premCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5606rec.eaage,SPACES)
		&& isEQ(sv.premCessAge,0)
		&& isEQ(sv.premCessTerm,0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkBcessFields2135()
	{
		if (isGT(sv.benCessAge,0)
		&& isGT(sv.benCessTerm,0)) {
			sv.bcessageErr.set(errorsInner.f220);
			sv.bcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.benCessAge,0)
		&& isEQ(sv.benCessTerm,0)
		&& isEQ(sv.rcessageErr,SPACES)
		&& isEQ(sv.rcesstrmErr,SPACES)) {
			sv.benCessAge.set(sv.riskCessAge);
			sv.benCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5606rec.eaage,SPACES)
		&& isEQ(sv.benCessAge,0)
		&& isEQ(sv.benCessTerm,0)) {
			sv.bcessageErr.set(errorsInner.e560);
			sv.bcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkAgeTerm2140()
	{
		if ((isNE(sv.rcessageErr,SPACES))
		|| (isNE(sv.rcesstrmErr,SPACES))
		|| (isNE(sv.pcessageErr,SPACES))
		|| (isNE(sv.pcesstrmErr,SPACES))
		|| (isNE(sv.bcessageErr,SPACES))
		|| (isNE(sv.bcesstrmErr,SPACES))) {
			goTo(GotoLabel.checkLiencd2180);
		}
		if ((isNE(sv.pcessageErr,SPACES))
		|| (isNE(sv.pcesstrmErr,SPACES))
		|| (isNE(sv.rcessageErr,SPACES))
		|| (isNE(sv.rcesstrmErr,SPACES))) {
			goTo(GotoLabel.checkLiencd2180);
		}
		riskCessDate5400();
		premCessDate5450();
		benCessDate5470();
		if (isNE(sv.rcesdteErr,SPACES)
		|| isNE(sv.pcesdteErr,SPACES)
		|| isNE(sv.bcesdteErr,SPACES)) {
			goTo(GotoLabel.checkLiencd2180);
		}
		if (isEQ(sv.premCessDate,varcom.vrcmMaxDate)) {
			sv.premCessDate.set(sv.riskCessDate);
		}
		if (isEQ(sv.benCessDate,varcom.vrcmMaxDate)) {
			sv.benCessDate.set(sv.riskCessDate);
		}
		if (isGT(sv.premCessDate,sv.riskCessDate)) {
			sv.pcesdteErr.set(errorsInner.e566);
			sv.rcesdteErr.set(errorsInner.e566);
		}
		if (isEQ(sv.rider,"00")) {
			goTo(GotoLabel.datesCont2140);
		}
		wsaaStorePlanSuffix.set(covrpf.getPlanSuffix());
		listCovtpf = covtpfDAO.getCovtpfData(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife().trim(),covrpf.getCoverage().trim(),covrpf.getRider(),covrpf.getPlanSuffix());
		if (!(listCovtpf.isEmpty())) {
			covtpf = listCovtpf.get(0);
			goTo(GotoLabel.validDate2140);
		}
		if (modifyComp.isTrue()) {
			if (isEQ(chdrpf.getPolsum(),ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isGT(chdrpf.getPolsum(),covrpf.getPlanSuffix())
				|| isEQ(chdrpf.getPolsum(),covrpf.getPlanSuffix())) {
					covrpf.setPlanSuffix(0);
				}
			}
		}
		if (addComp.isTrue()) {
			if (isEQ(chdrpf.getPolsum(),ZERO)
			&& isNE(chdrpf.getPolinc(),1)) {
				covrpf.setPlanSuffix(1);
			}
			else {
				covrpf.setPlanSuffix(0);
			}
		}
		covrpf.setRider("00");
		//ILB-456
		covrpf=covrpfDAO.getCovrRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider(),covrpf.getPlanSuffix(),covrpf.getValidflag());
		if (covrpf == null) {
			fatalError600();
		}
		/*covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		covtpf.setRcesdte(covrpf.getRiskCessDate());
		covtpf.setPcesdte(covrpf.getPremCessDate());
		covtpf.setBcesdte(covrpf.getBenCessDate());
	}

protected void validDate2140()
	{
		if (isGT(sv.riskCessDate,covtpf.getRcesdte())) {
			sv.rcesdteErr.set(errorsInner.h033);
		}
		if (isGT(sv.premCessDate,covtpf.getPcesdte())) {
			sv.pcesdteErr.set(errorsInner.h044);
		}
		if (isGT(sv.benCessDate,covtpf.getBcesdte())) {
			sv.bcesdteErr.set(errorsInner.d030);
		}
		listCovtpf = covtpfDAO.getCovtpfData(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife().trim(),covrpf.getCoverage().trim(),covrpf.getRider(),covrpf.getPlanSuffix());
		if(!(listCovtpf.isEmpty())) {
			covtpf = listCovtpf.get(0);
		}
		covrpf.setChdrcoy(covtpf.getChdrcoy());
		covrpf.setChdrnum(covtpf.getChdrnum());
		covrpf.setLife(sv.life.toString());
		covrpf.setCoverage(sv.coverage.toString());
		covrpf.setRider(sv.rider.toString());
		covrpf.setPlanSuffix(wsaaStorePlanSuffix.toInt());
		//ILB-456 starts
		//ILIFE-8146 start
		covrpfList = covrpfDAO.searchCovrpfRecord(covrpf);
		if (covrpfList.isEmpty()) {
			goTo(GotoLabel.datesCont2140);
		}
		//ILIFE-8146 end
	}

protected void datesCont2140()
	{
		if (isEQ(sv.rider,"00")
		&& modifyComp.isTrue()) {
			covrrgwIO.setParams(SPACES);
			covrrgwIO.setChdrcoy(covrpf.getChdrcoy());
			covrrgwIO.setChdrnum(covrpf.getChdrnum());
			covrrgwIO.setLife(covrpf.getLife());
			covrrgwIO.setCoverage(covrpf.getCoverage());
			covrrgwIO.setRider(covrpf.getRider());
			covrrgwIO.setPlanSuffix(covrpf.getPlanSuffix());
			covrrgwIO.setFormat(formatsInner.covrrgwrec);
			covrrgwIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi 
			covrrgwIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrrgwIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE");
			SmartFileCode.execute(appVars, covrrgwIO);
			if (isNE(covrrgwIO.getStatuz(),varcom.oK)
			&& isNE(covrrgwIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(covrrgwIO.getParams());
				syserrrec.statuz.set(covrrgwIO.getStatuz());
				fatalError600();
			}
			while ( !(isEQ(covrrgwIO.getStatuz(),varcom.endp))) {
				riderCessation2140();
			}
			
		}
		if (isNE(sv.rcesdteErr,SPACES)
		|| isNE(sv.pcesdteErr,SPACES)
		|| isNE(sv.bcesdteErr,SPACES)) {
			goTo(GotoLabel.checkLiencd2180);
		}
		if (isEQ(t5606rec.eaage,"A")) {
			goTo(GotoLabel.ageAnniversary2140);
		}
		if (isEQ(sv.riskCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		if (isEQ(sv.benCessAge,ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32300();
			wszzBenCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzBenCessAge.set(sv.benCessAge);
		}
		goTo(GotoLabel.check2140);
	}

protected void ageAnniversary2140()
	{
		if (isEQ(sv.riskCessAge,ZERO)) {
			datcon3rec.intDate1.set(payrIO.getBtdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			compute(wszzRiskCessAge, 5).set(add(datcon3rec.freqFactor,wszzAnbAtCcd));
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge,ZERO)) {
			datcon3rec.intDate1.set(payrIO.getBtdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			compute(wszzPremCessAge, 5).set(add(datcon3rec.freqFactor,wszzAnbAtCcd));
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		if (isEQ(sv.benCessAge,ZERO)) {
			datcon3rec.intDate1.set(payrIO.getBtdate());
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32300();
			compute(wszzBenCessAge, 5).set(add(datcon3rec.freqFactor,wszzAnbAtCcd));
		}
		else {
			wszzBenCessAge.set(sv.benCessAge);
		}
	}

protected void check2140()
	{
		if (addComp.isTrue()) {
			datcon3rec.intDate1.set(payrIO.getBtdate());
		}
		else {
			datcon3rec.intDate1.set(covrpf.getCrrcd());
		}
		if (isEQ(sv.riskCessTerm,ZERO)) {
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32300();
			wszzRiskCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm,ZERO)) {
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32300();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		if (isEQ(sv.benCessTerm,ZERO)) {
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32300();
			wszzBenCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzBenCessTerm.set(sv.benCessTerm);
		}
		/* Assume the dates are invalid until proved otherwise.*/
		sv.rcessageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.bcessageErr.set(errorsInner.d028);
		sv.rcesstrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		sv.bcesstrmErr.set(errorsInner.d029);
		x.set(0);
	}

protected void checkOccurance2140()
	{
		x.add(1);
		if (isGT(x,wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2160);
		}
		if ((isEQ(t5606rec.ageIssageFrm[x.toInt()],0)
		&& isEQ(t5606rec.ageIssageTo[x.toInt()],0))
		|| (isLT(wszzAnbAtCcd,t5606rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd,t5606rec.ageIssageTo[x.toInt()]))) {
			goTo(GotoLabel.checkTermFields2150);
		}
		if (isGTE(wszzRiskCessAge,t5606rec.riskCessageFrom[x.toInt()])
		&& isLTE(wszzRiskCessAge,t5606rec.riskCessageTo[x.toInt()])) {
			sv.rcessageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge,t5606rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge,t5606rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
		if (isGTE(wszzBenCessAge,t5606rec.benCessageFrom[x.toInt()])
		&& isLTE(wszzBenCessAge,t5606rec.benCessageTo[x.toInt()])) {
			sv.bcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2150()
	{
		if ((isEQ(t5606rec.termIssageFrm[x.toInt()],0)
		&& isEQ(t5606rec.termIssageTo[x.toInt()],0))
		|| (isLT(wszzAnbAtCcd,t5606rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd,t5606rec.termIssageTo[x.toInt()]))) {
			goTo(GotoLabel.checkOccurance2140);
		}
		if (isGTE(wszzRiskCessTerm,t5606rec.riskCesstermFrom[x.toInt()])
		&& isLTE(wszzRiskCessTerm,t5606rec.riskCesstermTo[x.toInt()])) {
			sv.rcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm,t5606rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm,t5606rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzBenCessTerm,t5606rec.benCesstermFrm[x.toInt()])
		&& isLTE(wszzBenCessTerm,t5606rec.benCesstermTo[x.toInt()])) {
			sv.bcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2140);
	}

protected void checkComplete2160()
	{
		if (isNE(sv.rcesstrmErr,SPACES)
		&& isEQ(sv.riskCessTerm,ZERO)) {
			sv.rcesdteErr.set(sv.rcesstrmErr);
			sv.rcesstrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr,SPACES)
		&& isEQ(sv.premCessTerm,ZERO)) {
			sv.pcesdteErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.bcesstrmErr,SPACES)
		&& isEQ(sv.benCessTerm,ZERO)) {
			sv.bcesdteErr.set(sv.bcesstrmErr);
			sv.bcesstrmErr.set(SPACES);
		}
		if (isNE(sv.rcessageErr,SPACES)
		&& isEQ(sv.riskCessAge,ZERO)) {
			sv.rcesdteErr.set(sv.rcessageErr);
			sv.rcessageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr,SPACES)
		&& isEQ(sv.premCessAge,ZERO)) {
			sv.pcesdteErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
		if (isNE(sv.bcessageErr,SPACES)
		&& isEQ(sv.benCessAge,ZERO)) {
			sv.bcesdteErr.set(sv.bcessageErr);
			sv.bcessageErr.set(SPACES);
		}
	}

protected void checkLiencd2180()
	{
		if (isEQ(sv.liencdOut[varcom.pr.toInt()],"Y")
		|| isEQ(sv.liencd,SPACES)) {
			goTo(GotoLabel.checkMore2190);
		}
		x.set(0);
	}

protected void loop2185()
	{
		x.add(1);
		if (isGT(x,wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2190);
		}
		if (isEQ(t5606rec.liencd[x.toInt()],SPACES)
		|| isNE(t5606rec.liencd[x.toInt()],sv.liencd)) {
			loop2185();
			return ;
		}
	}

protected void checkMore2190()
	{
		if (isNE(sv.bappmeth,SPACES)
		&& isNE(sv.bappmeth,t6005rec.bappmeth01)
		&& isNE(sv.bappmeth,t6005rec.bappmeth02)
		&& isNE(sv.bappmeth,t6005rec.bappmeth03)
		&& isNE(sv.bappmeth,t6005rec.bappmeth04)
		&& isNE(sv.bappmeth,t6005rec.bappmeth05)) {
			sv.bappmethErr.set(errorsInner.d352);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			if (isNE(sv.select,SPACES)
			&& isNE(sv.select,"J")
			&& isNE(sv.select,"L")) {
				sv.selectErr.set(errorsInner.h093);
			}
		}
		if(incomeProtectionflag || premiumflag){
			checkIPPmandatory();
			checkIPPfields();
		}
		/*Z0-EXIT*/
	}
protected void checkIPPmandatory()
{
	if(AppVars.getInstance().getAppConfig().isVpmsEnable())
	{
		if(incomeProtectionflag)
		{
			if((isEQ(sv.waitperiod,SPACES)) && waitperiodFlag )
				sv.waitperiodErr.set(errorsInner.e186);
			
			if((isEQ(sv.bentrm,SPACES)) && bentrmFlag )
				sv.bentrmErr.set(errorsInner.e186);
			
			if((isEQ(sv.poltyp,SPACES)) && poltypFlag )
				sv.poltypErr.set(errorsInner.e186);
		
		}
		
		if(premiumflag)
		{
			if((isEQ(sv.prmbasis,SPACES)) && prmbasisFlag )
				sv.prmbasisErr.set(errorsInner.e186);
		}
	}
}
protected void checkIPPfields()
{
	boolean t5606Flag=false;
	for(int counter=1; counter< t5606rec.waitperiod.length;counter++){
		if(isNE(sv.waitperiod,SPACES)){
			if (isEQ(t5606rec.waitperiod[counter], sv.waitperiod)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.waitperiod,SPACES)){
		sv.waitperiodErr.set("RFUY");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.bentrm.length;counter++){
		if(isNE(sv.bentrm,SPACES)){
			if (isEQ(t5606rec.bentrm[counter], sv.bentrm)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.bentrm,SPACES)){
		sv.bentrmErr.set("RFUZ");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.poltyp.length;counter++){
		if(isNE(sv.poltyp,SPACES)){
			if (isEQ(t5606rec.poltyp[counter], sv.poltyp)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.poltyp,SPACES)){
		sv.poltypErr.set("RFV0");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.prmbasis.length;counter++){
		if(isNE(sv.prmbasis,SPACES)){
			if (isEQ(t5606rec.prmbasis[counter], sv.prmbasis)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.prmbasis,SPACES)){
		sv.prmbasisErr.set("RFV1");// new error code
	}
	
}


protected void riderCessation2140()
	{
			para2140();
		}

protected void para2140()
	{
		if (isNE(covrrgwIO.getChdrcoy(), wsaaCovrChdrcoy)
		|| isNE(covrrgwIO.getChdrnum(),wsaaCovrChdrnum)
		|| isNE(covrrgwIO.getLife(),wsaaCovrLife)
		|| isNE(covrrgwIO.getCoverage(),wsaaCovrCoverage)) {
			covrrgwIO.setStatuz(varcom.endp);
			return ;
		}
		if (isGT(covrrgwIO.getRiskCessDate(),sv.riskCessDate)) {
			sv.rcesdteErr.set(errorsInner.h033);
			covrrgwIO.setStatuz(varcom.endp);
		}
		if (isGT(covrrgwIO.getPremCessDate(),sv.premCessDate)) {
			sv.pcesdteErr.set(errorsInner.h044);
			covrrgwIO.setStatuz(varcom.endp);
		}
		if (isGT(covrrgwIO.getBenCessDate(),sv.benCessDate)) {
			sv.bcesdteErr.set(errorsInner.d030);
			covrrgwIO.setStatuz(varcom.endp);
		}
		if (isEQ(covrrgwIO.getStatuz(),varcom.endp)) {
			return ;
		}
		covrrgwIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrrgwIO);
		if (isNE(covrrgwIO.getStatuz(),varcom.oK)
		&& isNE(covrrgwIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrrgwIO.getParams());
			syserrrec.statuz.set(covrrgwIO.getStatuz());
			fatalError600();
		}
	}

protected void calcPremium2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para2210();
					callSubr2220();
					adjustPrem2225();
				}
				case cont: {
					cont();
				}
				case exit2290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2210()
	{
		if (isNE(t5687rec.bbmeth,SPACES)) {
			goTo(GotoLabel.exit2290);
		}
		if (isEQ(t5675rec.premsubr,SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			wsaaPremStatuz.set("N");
		}
		if (premReqd.isTrue()) {
			if (isEQ(sv.instPrem,0)) {
				sv.instprmErr.set(errorsInner.g818);
			}
			goTo(GotoLabel.exit2290);
		}
		if (modifyComp.isTrue()
		&& userPremEntered.isTrue()
		&& premReqd.isTrue()) {
			if (isGT(sv.instPrem,0)) {
				goTo(GotoLabel.exit2290);
			}
		}
		if (isGT(sv.instPrem,0)
		&& premReqd.isTrue()) {
			if (wsaaPremChanged.isTrue()
			&& modifyComp.isTrue()) {
				wsaaPremStatuz.set("U");
				sv.instprmErr.set(errorsInner.f404);
				goTo(GotoLabel.exit2290);
			}
		}
		if (isGT(sv.instPrem,0)
		&& premReqd.isTrue()) {
			if (!wsaaPremChanged.isTrue()) {
				goTo(GotoLabel.exit2290);
			}
		}
	}

protected void callSubr2220()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(wsaaCrtable);
		premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		if (isEQ(sv.select,"J")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.effectdt.set(payrIO.getBtdate());
		premiumrec.termdate.set(sv.premCessDate);
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.benpln.set(sv.benpln);
		premiumrec.mortcls.set(sv.mortcls);
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrpf.getOccdate());
		premiumrec.reRateDate.set(chdrpf.getOccdate());
		premiumrec.calcPrem.set(sv.instPrem);
		premiumrec.calcBasPrem.set(sv.instPrem);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			/*     MOVE DTC3-STATUZ        TO SYSR-STATUZ*/
			/*     PERFORM 600-FATAL-ERROR.*/
			sv.pcesdteErr.set(errorsInner.sc72);
			goTo(GotoLabel.exit2290);
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.cnttype.set(chdrpf.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.riskCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.benCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaBenCessTerm.set(datcon3rec.freqFactor);
		premiumrec.benCessTerm.set(wsaaBenCessTerm);
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem,chdrpf.getPolinc()),chdrpf.getPolsum())));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem,chdrpf.getPolinc()),chdrpf.getPolsum())));
		}
		if (isEQ(wsaaAnnuity,"Y")) {
			getNewAnnuityDetails8100();
		}
		else {
			premiumrec.freqann.set(SPACES);
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
		/* If the coverage uses the calculation package SUM and the*/
		/* annuity frequency is not being used then pass the benefit*/
		/* frequency to the calculation method using this field.*/
		/*    IF     WSAA-SUMFLAG NOT = SPACE                              */
		/*       AND CPRM-FREQANN     = SPACES                             */
		/*           MOVE T5606-BENFREQ   TO CPRM-FREQANN.                 */
		/* If a component has a premium breakdown record (indicates also*/
		/* it is a SUM product) and it is being modified, then set the*/
		/* valid flag on the premium breakdown record to '2'. Note this*/
		/* must be done only the once & so set and check a flag.*/
		/* Note that as of 18/4/95 a function of 'MALT' will only apply*/
		/* to coverage's using SUM.*/
		if (modifyComp.isTrue()
		&& isEQ(sv.pbindOut[varcom.nd.toInt()],"N")) {
			premiumrec.function.set("MALT");
			if (isNE(wsaaPovrModified,"Y")) {
				vf2MaltPovr9400();
				wsaaPovrModified = "Y";
			}
		}
		initialize(pr676cpy.parmRecord);
		pr676cpy.mortcls.set(sv.mortcls);
		pr676cpy.livesno.set(sv.livesno);
		pr676cpy.waiverprem.set(sv.waiverprem);
		pr676cpy.waiverCrtable.set(tr686rec.waivercode);
		pr676cpy.benpln.set(sv.benpln);
		premiumrec.language.set(wsspcomn.language);
		//IVE-750  Start- TRM product - Premium calculation for HBNF rider - PM09 method
		//Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models	
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
		}
		else
		{		
			premiumrec.livesno.set(sv.livesno);
			premiumrec.liveno.set(sv.livesno);
			premiumrec.indic.set(sv.waiverprem);
			premiumrec.ccode.set(pr676cpy.waiverCrtable);
			//premiumrec.totprem.set(sv.instPrem); 
			premiumrec.percent.set(ZERO);
			if(isNE(sv.dialdownoption,SPACES)){
				if(sv.dialdownoption.toString().startsWith("0"))
					premiumrec.dialdownoption.set(sv.dialdownoption.substring(1));
				else
					premiumrec.dialdownoption.set(sv.dialdownoption);
			}
			else
				premiumrec.dialdownoption.set("100");
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			//ILIFE-2465 Sr676_Hospital & Surgical Benefit
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
				
		}
		//Ticket #IVE-792 - End
		//IVE-750  End- TRM product - Premium calculation for HBNF rider - PM09 method
						
		if (isEQ(premiumrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz,varcom.oK)) {
			sv.instprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2290);
		}
		if (isGT(sv.zunit,1)) {
			compute(premiumrec.calcBasPrem, 3).setRounded(mult(premiumrec.calcBasPrem,sv.zunit));
			compute(premiumrec.calcLoaPrem, 3).setRounded(mult(premiumrec.calcLoaPrem,sv.zunit));
			compute(premiumrec.calcPrem, 3).setRounded(mult(premiumrec.calcPrem,sv.zunit));
		}
		wsaaPcesageStore.set(sv.premCessAge);
		wsaaRcesageStore.set(sv.riskCessAge);
		wsaaBcesageStore.set(sv.benCessAge);
		wsaaPcestermStore.set(sv.premCessTerm);
		wsaaRcestermStore.set(sv.riskCessTerm);
		wsaaBcestermStore.set(sv.benCessTerm);
	}

protected void adjustPrem2225()
	{
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		if (planLevel.isTrue()
		&& isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& isGT(chdrpf.getPolsum(),ZERO)) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin,chdrpf.getPolsum()),chdrpf.getPolinc())));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		/*BRD-306 START */
		sv.loadper.set(premiumrec.loadper);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		/*BRD-306 END */

		if (isEQ(sv.instPrem,0)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2290);
		}
		if (isGTE(sv.instPrem,premiumrec.calcPrem)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2290);
		}
		if (isLTE(payrIO.getMandref(),SPACES)) {
			goTo(GotoLabel.cont);
		}
		clrfIO.setParams(SPACES);
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaForenum.set(SPACES);
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(formatsInner.clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		mandIO.setPayrcoy(clrfIO.getClntcoy());
		mandIO.setPayrnum(clrfIO.getClntnum());
		mandIO.setMandref(payrIO.getMandref());
		mandIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(mandIO.getParams());
			fatalError600();
		}
		if (isEQ(mandIO.getMandAmt(),ZERO)) {
			goTo(GotoLabel.cont);
		}
		if (isGT(chdrpf.getBillfreq(),0)) {
			compute(wsaaCalcPrem, 2).set(sub(covrpf.getInstprem(),(sub(payrIO.getSinstamt06(),sv.instPrem))));
		}
		if (isNE(mandIO.getMandAmt(),wsaaCalcPrem)) {
			sv.instprmErr.set(errorsInner.ev01);
			wsspcomn.edterror.set("Y");
		}
	}

protected void cont()
	{
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem,sv.instPrem));
		sv.instprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub,11)
		|| isEQ(sv.instprmErr,SPACES)); wsaaSub.add(1)){
			searchForTolerance2240();
		}
		if (isEQ(sv.instprmErr,SPACES)) {
			compute(sv.zbinstprem, 2).set(sub(sv.instPrem,sv.zlinstprem));
		}
		goTo(GotoLabel.exit2290);
	}

protected void searchForTolerance2240()
	{
		if (isEQ(payrIO.getBillfreq(),t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem,t5667rec.prmtol[wsaaSub.toInt()])),100));
			if (isLTE(wsaaDiff,wsaaTol)
			&& isLTE(wsaaDiff,t5667rec.maxAmount[wsaaSub.toInt()])) {
				sv.instprmErr.set(SPACES);
			}
		}
	}

protected void callDatcon32300()
	{
		/*CALL*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcSumins2650()
	{
		para2650();
		readNext2651();
	}

protected void para2650()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setDataKey(SPACES);
		initialize(wsbbTr687Itemitem);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr687);
		wsbbTr687Crtable.set(wsaaCrtable);
		wsbbTr687Benpln.set(sv.benpln);
		itdmIO.setItemitem(wsbbTr687Itemitem);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	}

protected void readNext2651()
	{
		itdmIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr687)
		|| isNE(itdmIO.getItemitem(),wsbbTr687Itemitem)) {
			/*    MOVE RL81                TO SR680-SUMIN-ERR               */
			sv.benplnErr.set(errorsInner.rl81);
			return ;
		}
		if (isGT(itdmIO.getItmfrm(),chdrpf.getOccdate())
		|| isLT(itdmIO.getItmto(),chdrpf.getOccdate())) {
			itdmIO.setFunction(varcom.nextr);
			readNext2651();
			return ;
		}
		tr687rec.tr687Rec.set(itdmIO.getGenarea());
		tr687rec.tr687Rec.set(itdmIO.getGenarea());
		compute(sv.sumin, 1).setRounded(mult(mult(tr687rec.factor,tr687rec.premunit),tr687rec.benfamt02));
		wsaaSumin.set(sv.sumin);
		compute(wsaaMinsumins, 2).set(mult(tr687rec.zssi,tr687rec.premunit));
	}


protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		/* If termination of processing or enquiry go to exit.*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		if (compEnquiry.isTrue()) {
			return ;
		}
		
		updateHbnf3300();
		setupCovtmja3100();
		if(incomeProtectionflag || premiumflag || dialdownFlag){
		insertAndUpdateRcvdpf();
		}
		updateCovtmja3200();
		if (isEQ(wsaaUpdateFlag,"N")) {
			return ;
		}
		pcdtmjaIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtpf.getPlnsfx());
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(chdrpf.getChdrcoy(),pcdtmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),pcdtmjaIO.getChdrnum())
		|| isNE(sv.life,pcdtmjaIO.getLife())
		|| isNE(sv.coverage,pcdtmjaIO.getCoverage())
		|| isNE(sv.rider,pcdtmjaIO.getRider())
		|| isNE(covtpf.getPlnsfx(),pcdtmjaIO.getPlanSuffix())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(pcdtmjaIO.getStatuz(),varcom.endp)) {
			defaultPcdt3800();
		}
		
		if(isFollowUpRequired){
			createFollowUps3400();
		}
	}

protected void setupCovtmja3100()
	{
		updateReqd3110();
	}

protected void updateReqd3110()
	{
		wsaaUpdateFlag.set("N");
		if (isNE(sv.riskCessDate,covtpf.getRcesdte())) {
			covtpf.setRcesdte(sv.riskCessDate.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessDate,covtpf.getPcesdte())) {
			covtpf.setPcesdte(sv.premCessDate.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessDate,covtpf.getBcesdte())) {
			covtpf.setBcesdte(sv.benCessDate.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessAge,covtpf.getRcesage())) {
			covtpf.setRcesage(sv.riskCessAge.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessAge,covtpf.getPcesage())) {
			covtpf.setPcesage(sv.premCessAge.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessAge,covtpf.getBcesage())) {
			covtpf.setBcesage(sv.benCessAge.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessTerm,covtpf.getRcestrm())) {
			covtpf.setRcestrm(sv.riskCessTerm.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessTerm,covtpf.getPcestrm())) {
			covtpf.setPcestrm(sv.premCessTerm.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessTerm,covtpf.getBcestrm())) {
			covtpf.setBcestrm(sv.benCessTerm.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.sumin,covtpf.getSumins())) {
			covtpf.setSumins(sv.sumin.getbigdata());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zbinstprem,covtpf.getZbinstprem())) {
			covtpf.setZbinstprem(sv.zbinstprem.getbigdata());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zlinstprem,covtpf.getZlinstprem())) {
			covtpf.setZlinstprem(sv.zlinstprem.getbigdata());
			/*BRD-306 START */			
			covtpf.setLoadper(sv.loadper.getbigdata());
			covtpf.setRateadj(sv.rateadj.getbigdata());
			covtpf.setFltmort(sv.fltmort.getbigdata());
			covtpf.setPremadj(sv.premadj.getbigdata());
			covtpf.setAgeadj(sv.adjustageamt.getbigdata());
			/*BRD-306 END */
			wsaaUpdateFlag.set("Y");
		}
		if (isEQ(payrIO.getBillfreq(),"00")) {
			if (isNE(sv.instPrem,covtpf.getSingp())) {
				writeExtractRecord3900();
				covtpf.setSingp(sv.instPrem.getbigdata());
				wsaaUpdateFlag.set("Y");
			}
		}
		else {
			if (isNE(sv.instPrem,covtpf.getInstprem())) {
				writeExtractRecord3900();
				covtpf.setInstprem(sv.instPrem.getbigdata());
				wsaaUpdateFlag.set("Y");
			}
		}
		if (isNE(sv.mortcls,covtpf.getMortcls())) {
			covtpf.setMortcls(sv.mortcls.toString());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.liencd,covtpf.getLiencd())) {
			covtpf.setLiencd(sv.liencd.toString());
			wsaaUpdateFlag.set("Y");
		}
		if ((isEQ(sv.select,"J")
		&& (isEQ(covtpf.getJlife(),"00")
		|| isEQ(covtpf.getJlife(), "  ")))
		|| ((isEQ(sv.select, " ")
		|| isEQ(sv.select,"L"))
		&& isEQ(covtpf.getJlife(),"01"))) {
			wsaaUpdateFlag.set("Y");
			if (isEQ(sv.select,"J")) {
				covtpf.setJlife("01");
			}
			else {
				covtpf.setJlife("00");
			}
		}
		if (isNE(payrIO.getBillfreq(),covtpf.getBillfreq())) {
			covtpf.setBillfreq(payrIO.getBillfreq().toString());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(payrIO.getBillchnl(),covtpf.getBillchnl())) {
			covtpf.setBillchnl(payrIO.getBillchnl().toString());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaAnbAtCcd,covtpf.getAnbccd01())) {
			covtpf.setAnbccd01(wsaaAnbAtCcd.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex,covtpf.getSex01())) {
			covtpf.setSex01(wsaaSex.toString());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsbbAnbAtCcd,covtpf.getAnbccd02())) {
			covtpf.setAnbccd02(wsbbAnbAtCcd.toInt());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex,covtpf.getSex02())) {
			covtpf.setSex02(wsbbSex.toString());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.bappmeth,covtpf.getBappmeth())) {
			covtpf.setBappmeth(sv.bappmeth.toString());
			wsaaUpdateFlag.set("Y");
		}
	}

protected void insertAndUpdateRcvdpf(){
	boolean rcvdUpdateFlag=false;
if(rcvdPFObject!=null){
	rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());//IJTI-1410
	rcvdPFObject.setChdrnum(covrpf.getChdrnum());//IJTI-1410
	rcvdPFObject.setCoverage(covrpf.getCoverage());//IJTI-1410
	rcvdPFObject.setCrtable(covrpf.getCrtable());//IJTI-1410
	rcvdPFObject.setLife(covrpf.getLife());//IJTI-1410
	rcvdPFObject.setRider(covrpf.getRider());//IJTI-1410
	
	if(rcvdPFObject.getWaitperiod() != null){
		if (isNE(sv.waitperiod, rcvdPFObject.getWaitperiod())) {
			rcvdPFObject.setWaitperiod(sv.waitperiod.toString());
			rcvdUpdateFlag=true;
		}
	}
		if(rcvdPFObject.getPoltyp() != null){
		if (isNE(sv.poltyp, rcvdPFObject.getPoltyp())) {
			rcvdPFObject.setPoltyp(sv.poltyp.toString());
			rcvdUpdateFlag=true;
		}
		}
		if(rcvdPFObject.getPrmbasis()!=null){
		if (isNE(sv.prmbasis, rcvdPFObject.getPrmbasis())) {
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
			rcvdUpdateFlag=true;
		}
		}
		if(rcvdPFObject.getBentrm()!=null){
		if (isNE(sv.bentrm, rcvdPFObject.getBentrm())) {
			rcvdPFObject.setBentrm(sv.bentrm.toString());
			rcvdUpdateFlag=true;
		}
		}
		//BRD-NBP-011 starts
		if(rcvdPFObject.getDialdownoption()!=null){
		if (isNE(sv.dialdownoption, rcvdPFObject.getDialdownoption())) {
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
			rcvdUpdateFlag=true;
		}
		}
		//BRD-NBP-011 ends
		if(rcvdUpdateFlag){
			rcvdDAO.updateIntoRcvdpf(rcvdPFObject);
		}
	}else{ 
		rcvdPFObject=new Rcvdpf();
		rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());//IJTI-1410
		rcvdPFObject.setChdrnum(covrpf.getChdrnum());//IJTI-1410
		rcvdPFObject.setCoverage(covrpf.getCoverage());//IJTI-1410
		rcvdPFObject.setCrtable(covrpf.getCrtable());//IJTI-1410
		rcvdPFObject.setLife(covrpf.getLife());//IJTI-1410
		rcvdPFObject.setRider(covrpf.getRider());//IJTI-1410
		if(isNE(sv.waitperiod,SPACES)){
			rcvdPFObject.setWaitperiod(sv.waitperiod.toString());
		}
		//snayeni added for ILIFE-3630 -start
		else{
		rcvdPFObject.setWaitperiod(SPACE);
		}
		if(isNE(sv.poltyp,SPACES)){
			rcvdPFObject.setPoltyp(sv.poltyp.toString());
		}
		else{
			rcvdPFObject.setPoltyp(SPACE);
			}
		if(isNE(sv.prmbasis,SPACES)){
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
		}
		else{
			rcvdPFObject.setPrmbasis(SPACE);
			}
		if(isNE(sv.bentrm,SPACES)){
			rcvdPFObject.setBentrm(sv.bentrm.toString());
		}
		else{
			rcvdPFObject.setBentrm(SPACE);
			}
		//snayeni added for ILIFE-3630 -End
		//BRD-NBP-011 starts
		if(isNE(sv.dialdownoption,SPACES)){
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
		}
		
		//BRD-NBP-011 ends
		rcvdDAO.insertRcvdpfRecord((rcvdPFObject));
	}
	
}

protected void updateCovtmja3200()
	{
		try {
			noUpdate3210();
			update3220();
		}
		catch (GOTOException e){
		}
	}

protected void noUpdate3210()
	{
		if (isEQ(wsaaUpdateFlag,"N")
		&& isNE(sv.optextind,"X")) {
			goTo(GotoLabel.exit3290);
		}
	}

protected void update3220()
	{
		covtpf.setTermid(varcom.vrcmTermid.toString());
		covtpf.setUserT(varcom.vrcmUser.toInt());
		covtpf.setTrdt(varcom.vrcmDate.toInt());
		covtpf.setTrtm(varcom.vrcmTime.toInt());
		covtpf.setChdrcoy(covrpf.getChdrcoy());
		covtpf.setChdrnum(covrpf.getChdrnum());
		covtpf.setLife(covrpf.getLife());
		covtpf.setCoverage(wsaaCovrCoverage.toString());
		covtpf.setEffdate(payrIO.getBtdate().toInt());
		covtpf.setRider(wsaaCovrRider.toString());
		covtpf.setPayrseqno(1);
		if (addComp.isTrue()) {
			if (isEQ(payrIO.getBillfreq(),"00")) {
				covtpf.setEffdate(datcon1rec.intDate.toInt());
			}
			covtpf.setPlnsfx(0);
			covtpf.setSeqnbr(0);
		}
		else {
			covtpf.setPlnsfx(covrpf.getPlanSuffix());
			covtpf.setSeqnbr(wsaaNextSeqnbr.toInt());
		}
		wsaaNextSeqnbr.subtract(1);
		covtpf.setCrtable(wsaaCrtable.toString());
		covtpf.setRsunin("");
		covtpf.setRundte(0);
		covtpf.setPolinc(0);
		if (covtpf.getUniqueNumber()>0) {
			covtpfDAO.updateCovtData(covtpf);
		}
		else {
			covtpfDAO.insertCovtData(covtpf);
		}
	}

protected void defaultPcdt3800()
	{
		moveValues3800();
		call3830();
	}

protected void moveValues3800()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setInstprem(ZERO);
		pcdtmjaIO.setPlanSuffix(ZERO);
		pcdtmjaIO.setSplitBcomm01(ZERO);
		pcdtmjaIO.setSplitBcomm02(ZERO);
		pcdtmjaIO.setSplitBcomm03(ZERO);
		pcdtmjaIO.setSplitBcomm04(ZERO);
		pcdtmjaIO.setSplitBcomm05(ZERO);
		pcdtmjaIO.setSplitBcomm06(ZERO);
		pcdtmjaIO.setSplitBcomm07(ZERO);
		pcdtmjaIO.setSplitBcomm08(ZERO);
		pcdtmjaIO.setSplitBcomm09(ZERO);
		pcdtmjaIO.setSplitBcomm10(ZERO);
		pcdtmjaIO.setTranno(ZERO);
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		pcdtmjaIO.setLife(sv.life);
		pcdtmjaIO.setCoverage(sv.coverage);
		pcdtmjaIO.setRider(sv.rider);
		pcdtmjaIO.setPlanSuffix(covtpf.getPlnsfx());
		pcdtmjaIO.setInstprem(ZERO);
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(),1));
		wsaaIndex.set(1);
	}

protected void call3830()
	{
		pcdtmjaIO.setAgntnum(wsaaIndex, chdrpf.getAgntnum());
		pcdtmjaIO.setSplitc(wsaaIndex, 100);
		pcdtmjaIO.setFunction(varcom.writr);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(),varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void writeExtractRecord3900()
	{
		write3910();
	}

protected void write3910()
	{
		coexIO.setDataArea(SPACES);
		coexIO.setChdrnum(chdrpf.getChdrnum());
		coexIO.setCntcurr(chdrpf.getCntcurr());
		coexIO.setAgntnum(chdrpf.getAgntnum());
		coexIO.setEffdate(chdrpf.getOccdate());
		coexIO.setTrandate(wsaaToday);
		wsaaTranDate.set(wsaaToday);
		coexIO.setBillfreq(chdrpf.getBillfreq());
		wsaaFrequency.set(chdrpf.getBillfreq());
		coexIO.setCrtable(wsaaCrtable);
		if (single.isTrue()) {
			setPrecision(coexIO.getGrospre(), 2);
			coexIO.setGrospre(sub(sv.instPrem,covtpf.getSingp()));
		}
		else {
			setPrecision(coexIO.getGrospre(), 2);
			coexIO.setGrospre(sub(sv.instPrem,covtpf.getInstprem()));
		}
		coexIO.setTyearno(wsaaPolicyYear);
		if (yearly.isTrue()) {
			coexIO.setPayperd(1);
		}
		else {
			if (halfYearly.isTrue()) {
				for (wsaaSub.set(1); !(isGT(wsaaSub,2)); wsaaSub.add(1)){
					if (isLTE(wsaaTranMm,wsaaHalfYear[wsaaSub.toInt()])) {
						coexIO.setPayperd(wsaaHalfYear[wsaaSub.toInt()]);
						wsaaSub.set(3);
					}
				}
			}
			else {
				if (quarterly.isTrue()) {
					for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
						if (isLTE(wsaaTranMm,wsaaQuarter[wsaaSub.toInt()])) {
							coexIO.setPayperd(wsaaQuarter[wsaaSub.toInt()]);
							wsaaSub.set(5);
						}
					}
				}
				else {
					if (monthly.isTrue()) {
						for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
							if (isLTE(wsaaTranMm,wsaaMonth[wsaaSub.toInt()])) {
								coexIO.setPayperd(wsaaMonth[wsaaSub.toInt()]);
								wsaaSub.set(13);
							}
						}
					}
					else {
						if (single.isTrue()) {
							coexIO.setPayperd(ZERO);
						}
					}
				}
			}
		}
		coexIO.setSexrat(ZERO);
		if (n1stYearPolicy.isTrue()) {
			if (isGT(coexIO.getGrospre(),ZERO)) {
				coexIO.setTrancd("009");
			}
			else {
				coexIO.setTrancd("010");
			}
		}
		else {
			if (isGT(coexIO.getGrospre(),ZERO)) {
				coexIO.setTrancd("011");
			}
			else {
				coexIO.setTrancd("012");
			}
		}
		aglflnbIO.setAgntcoy(chdrpf.getChdrcoy());
		aglflnbIO.setAgntnum(chdrpf.getAgntnum());
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglflnbIO.getParams());
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			fatalError600();
		}
		coexIO.setAracde(aglflnbIO.getAracde());
		coexIO.setFunction(varcom.writr);
		coexIO.setFormat(formatsInner.coexrec);
		SmartFileCode.execute(appVars, coexIO);
		if (isNE(coexIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(coexIO.getParams());
			syserrrec.statuz.set(coexIO.getStatuz());
			fatalError600();
		}
	}

protected void updateHbnf3300()
	{
		para3300();
	}

protected void para3300()
	{
		hbnfIO.setChdrcoy(chdrpf.getChdrcoy());
		hbnfIO.setChdrnum(chdrpf.getChdrnum());
		hbnfIO.setLife(covrpf.getLife());
		hbnfIO.setCoverage(covrpf.getCoverage());
		hbnfIO.setRider(covrpf.getRider());
		hbnfIO.setFunction(varcom.readr);
		callHbnf1950();
		if (isEQ(hbnfIO.getStatuz(),varcom.mrnf)) {
			hbnfIO.setRecNonKeyData(SPACES);
		}
		hbnfIO.setClntnum01(sv.lifcnum);
		hbnfIO.setMortcls(sv.mortcls);
		hbnfIO.setLivesno(sv.livesno);
		hbnfIO.setWaiverprem(sv.waiverprem);
		hbnfIO.setBenpln(sv.benpln);
		hbnfIO.setZunit(sv.zunit);
		hbnfIO.setLife(wsaaCovrLife.trim());
		hbnfIO.setCoverage(wsaaCovrCoverage.trim());
		hbnfIO.setRider(wsaaCovrRider.trim());
		hbnfIO.setEffdate(payrIO.getBtdate());
		hbnfIO.setCrtable(wsaaCrtable);
		if (isEQ(hbnfIO.getStatuz(),varcom.oK)) {
			hbnfIO.setFunction(varcom.writd);
		}
		else {
			hbnfIO.setFunction(varcom.writr);
			try {
				hbnfIO.setClusterWsaaInUse("N");
			} catch (SQLException e) {
			}
		}
		callHbnf1950();
	}

protected void whereNext4000()
	{
			nextProgram4010();
		}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(sv.pbind,"X")) {
			pbindExe9100();
			return ;
		}
		else {
			if (isEQ(sv.pbind,"?")) {
				pbindRet9200();
				return ;
			}
		}
		if (isEQ(sv.optextind,"X")) {
			optionsExe4500();
			return ;
		}
		else {
			if (isEQ(sv.optextind,"?")) {
				optionsRet4600();
				return ;
			}
		}
		if (isEQ(sv.anntind,"X")) {
			annuityExe4900();
			return ;
		}
		else {
			if (isEQ(sv.anntind,"?")) {
				annuityRet4950();
				return ;
			}
		}
		if (isEQ(sv.coverdtl,"X")) {
			coverdtlExe4700();
			return ;
		}
		else {
			if (isEQ(sv.coverdtl,"?")) {
				coverdtlRet4750();
				return ;
			}
		}
		if (isEQ(sv.viewplan,"X")) {
			viewplanExe4800();
			return ;
		}
		else {
			if (isEQ(sv.viewplan,"?")) {
				viewplanRet4850();
				return ;
			}
		}
		if (isEQ(sv.comind,"X")) {
			comindExe4a00();
			return ;
		}
		else {
			if (isEQ(sv.comind,"?")) {
				comindRet4a50();
				return ;
			}
		}
		if (isEQ(sv.taxind, "X")) {
			taxExe4b00();
			return ;
		}
		else {
			if (isEQ(sv.taxind, "?")) {
				taxRet4b50();
				return ;
			}
		}
		if (isEQ(sv.exclind, "X")) {
			exclExe6000();
			return;
		}
		if (isEQ(sv.exclind, "?")) {
			exclRet6100();
			return;
		}
        if (isEQ(sv.fuind, "X")) {
            fuindExe4d00();
            return;
        }else {
            if (isEQ(sv.fuind,"?")) {
                fuindRec4d50();
                return ;
            }
        }
		/* If we are processing a Whole Plan selection then we must*/
		/*   process all Policies for the selected Coverage/Rider,*/
		/*   Looping back to the 2000-section after having loaded the*/
		/*   screen for the next Policy*/
		/* Else we exit to the next program in the stack.*/
		if (planLevel.isTrue()) {
			clearScreen4700();
			policyLoad5000();
			if (covrpf != null) { //ILB-456
				wsspcomn.nextprog.set(wsaaProg);
				wayout4800();
			}
			else {
				wsspcomn.nextprog.set(scrnparams.scrname);
			}
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wayout4800();
		}
	}
    protected void fuindExe4d00()
    {
        start4d00();
    }

    protected void start4d00()
    {
        /* Keep the COVRMJA record if Whole Plan selected or Add component*/
        /*   as this will have been released within the 1000-section to*/
        /*   enable all policies to be processed.*/
    	//ILB-456
        /*chdrmjaIO.setFunction(varcom.keeps);
        SmartFileCode.execute(appVars, chdrmjaIO);
        if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
            syserrrec.params.set(chdrmjaIO.getParams());
            syserrrec.statuz.set(chdrmjaIO.getStatuz());
            fatalError600();
        }*/
    	chdrpfDAO.setCacheObject(chdrpf);
        /* The first thing to consider is the handling of an Options/*/
        /*   Extras request. If the indicator is 'X', a request to visit*/
        /*   options and extras has been made. In this case:*/
        /*    - change the options/extras request indicator to '?',*/
        sv.fuind.set("?");
        /* Save the next 8 programs from the program stack.*/
        compute(sub1, 0).set(add(1,wsspcomn.programPtr));
        sub2.set(1);
        for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
            save4510();
        }
        /* Call GENSSWCH with and action of 'H' to retrieve the program*/
        /*   switching required, and move them to the stack.*/
        gensswrec.function.set("J");
        gensww4210();
        compute(sub1, 0).set(add(1,wsspcomn.programPtr));
        sub2.set(1);
        for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
            load4530();
        }
        /* Set the current stack "action" to '*'.*/
        /* Add one to the program pointer and exit.*/
        wsspcomn.nextprog.set(wsaaProg);
        wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
        wsspcomn.programPtr.add(1);
    }

    protected void fuindRec4d50(){

        /* Restore the saved programs to the program stack                 */
        compute(sub1, 0).set(add(1, wsspcomn.programPtr));
        sub2.set(1);
        for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
            restore4610();
        }
        //ILB-456
        /*chdrmjaIO.setFunction(varcom.keeps);
        SmartFileCode.execute(appVars, chdrmjaIO);
        if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
            syserrrec.params.set(chdrmjaIO.getParams());
            syserrrec.statuz.set(chdrmjaIO.getStatuz());
            fatalError600();
        }*/
        chdrpfDAO.setCacheObject(chdrpf);
        readFlupal1500();
        /* Blank out the stack  action".                                   */
        wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
        /* Set WSSP-NEXTPROG to the current screen name (thus returning    */
        /*   returning to re-display the screen).                          */
        wsspcomn.nextprog.set(scrnparams.scrname);
    }

    protected void readFlupal1500()
    {
        readFlupal1510();
    }

    protected void readFlupal1510()
    {
        List<Fluppf> fluprevIOList = fluppfDAO.searchFlupRecordByChdrNum(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());//IJTI-1410
        if(fluprevIOList!=null&&!fluprevIOList.isEmpty()){
            sv.fuind.set("+");
        }else{
            sv.fuind.set(" ");
        }
    }

protected void exclExe6000(){
	covrpfDAO.setCacheObject(covrpf);
	sv.exclind.set("?");
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
		gensswrec.function.set("H");
		wsspcomn.chdrChdrnum.set(covrpf.getChdrnum());
		wsspcomn.crtable.set(wsaaCrtable);
		
		
		if(isNE(wsspcomn.flag,"I")){
		wsspcomn.flag.set("X");
		wsspcomn.cmode.set("IF");}
		else
			wsspcomn.cmode.set("IFE");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	
}
protected void exclRet6100(){
	
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	exclpf=exclpfDAO.readRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),wsaaCrtable.toString(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());//IJTI-1410
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
	
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	wsspcomn.nextprog.set(scrnparams.scrname);
}
protected void gensww4210()
	{
			para4211();
		}

protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h039);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextrevIO.getStatuz(),varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

protected void optionsExe4500()
	{
		keepCovr4510();
	}

protected void keepCovr4510()
	{
		//ILB-456
		/*covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		covrpfDAO.setCacheObject(covrpf);
		sv.optextind.set("?");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			save4510();
		}
		gensswrec.function.set("A");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			load4530();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		para4600();
	}

protected void para4600()
	{
		if (!compEnquiry.isTrue()) {
			wsaaOptext.set("Y");
			calcPremium2200();
		}
		wsaaOptext.set("N");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			restore4610();
		}
		checkLext5600();
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(covtpf.getLife());
		wsaaPovrCoverage.set(covtpf.getCoverage());
		wsaaPovrRider.set(covtpf.getRider());
		if (isNE(sv.anntind,"X")
		&& isNE(sv.comind,"X")
		&& isNE(sv.coverdtl,"X")
		&& isNE(sv.viewplan,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void coverdtlExe4700()
	{
		keepCovr4710();
	}

protected void keepCovr4710()
	{
		sv.coverdtl.set("?");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			save4710();
		}
		gensswrec.function.set("E");
		//ILIFE-2036 starts by slakkala
		hbnfIO.setFunction(varcom.keeps);
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		//ILIFE-2036 ends
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			load4730();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4710()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4730()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void coverdtlRet4750()
	{
		/*PARA*/
		sv.coverdtl.set("+");
		if (!compEnquiry.isTrue()) {
			calcPremium2200();
		}
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			restore4760();
		}
		if (isNE(sv.anntind,"X")
		&& isNE(sv.comind,"X")
		&& isNE(sv.viewplan,"X")
		&& isNE(sv.optextind,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		/*EXIT*/
	}

protected void restore4760()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void clearScreen4700()
	{
		/*PARA*/
		sv.premCessDate.set(ZERO);
		sv.benCessDate.set(ZERO);
		sv.riskCessDate.set(ZERO);
		sv.premCessAge.set(ZERO);
		sv.benCessAge.set(ZERO);
		sv.riskCessAge.set(ZERO);
		sv.premCessTerm.set(ZERO);
		sv.benCessTerm.set(ZERO);
		sv.riskCessTerm.set(ZERO);
		sv.liencd.set(SPACES);
		sv.mortcls.set(SPACES);
		sv.optextind.set(SPACES);
		sv.select.set(SPACES);
		sv.statFund.set(SPACES);
		sv.statSect.set(SPACES);
		sv.bappmeth.set(SPACES);
		sv.statSubsect.set(SPACES);
		/*EXIT*/
	}

protected void viewplanExe4800()
	{
		start4810();
	}

protected void start4810()
	{
		covrpfDAO.setCacheObject(covrpf);
		if (isNE(covrenqIO.getMortcls(),sv.mortcls)) {
			rewriteKeepMortcls4840();
		}
		hbnfIO.setFunction(varcom.keeps);
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		sv.viewplan.set("?");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			save4810();
		}
		gensswrec.function.set("F");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			load4830();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4810()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4830()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void rewriteKeepMortcls4840()
	{
					para4841();
					keepCovtmja4842();
				}

protected void para4841()
	{
		if (isEQ(covrenqIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		covrenqIO.setFunction(varcom.writd);
		covrenqIO.setMortcls(sv.mortcls);
		covrenqIO.setFormat(formatsInner.covrenqrec);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		covrenqIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
	}

protected void keepCovtmja4842()
	{
		covtpfDAO.setCacheObject(covtpf);
		/*EXIT*/
	}

protected void viewplanRet4850()
	{
		/*PARA*/
		sv.viewplan.set("+");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restore4860();
		}
		if (isNE(sv.anntind,"X")
		&& isNE(sv.comind,"X")
		&& isNE(sv.coverdtl,"X")
		&& isNE(sv.optextind,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		/*EXIT*/
	}

protected void restore4860()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void wayout4800()
	{
		/*PROGRAM-EXIT*/
		/* If control is passed to this  part of the 4000 section on the*/
		/*   way out of the program, i.e. after  screen  I/O,  then  the*/
		/*   current stack position action  flag will be  blank.  If the*/
		/*   4000-section  is  being   performed  after  returning  from*/
		/*   processing another program then  the current stack position*/
		/*   action flag will be '*'.*/
		/* If 'KILL' has been requested, (CF11), then move spaces to the*/
		/*   current program entry in the program stack and exit.*/
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		wsspcomn.programPtr.add(1);
	}

protected void annuityExe4900()
	{
		para4900();
	}

protected void para4900()
	{
		//ILB-456
		/*covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		covrpfDAO.setCacheObject(covrpf);
		if (!compEnquiry.isTrue()) {
			covtpfDAO.setCacheObject(covtpf);
		}
		sv.anntind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar11 = 0; !(loopVar11 == 8); loopVar11 += 1){
			save4510();
		}
		gensswrec.function.set("D");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar12 = 0; !(loopVar12 == 8); loopVar12 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		wssplife.bigAmt.set(sv.sumin);
		wssplife.effdate.set(payrIO.getPtdate());
	}

protected void annuityRet4950()
	{
		/*PARA*/
		sv.anntind.set("+");
		if (!compEnquiry.isTrue()) {
			calcPremium2200();
		}
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar13 = 0; !(loopVar13 == 8); loopVar13 += 1){
			restore4610();
		}
		if (isNE(sv.comind,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		sv.anntindOut[varcom.pr.toInt()].set("N");
		/*EXIT*/
	}

protected void comindExe4a00()
	{
		para4a00();
	}

protected void para4a00()
	{
		wsspcomn.tranrate.set(sv.instPrem);
		//ILB-456
		/*covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		covrpfDAO.setCacheObject(covrpf);
		if (!compEnquiry.isTrue()) {
			covtpfDAO.setCacheObject(covtpf);
		}
		sv.comind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar14 = 0; !(loopVar14 == 8); loopVar14 += 1){
			save4510();
		}
		gensswrec.function.set("B");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar15 = 0; !(loopVar15 == 8); loopVar15 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void comindRet4a50()
	{
		/*A50-PARA*/
		sv.comind.set("+");
		if (!compEnquiry.isTrue()) {
			calcPremium2200();
		}
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar16 = 0; !(loopVar16 == 8); loopVar16 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsspcomn.nextprog.set(scrnparams.scrname);
		sv.comindOut[varcom.pr.toInt()].set("N");
		/*A99-EXIT*/
	}

protected void taxExe4b00()
	{
		start4b10();
	}

protected void start4b10()
	{
		/* Keep the CHDR/COVT record                                       */
		//ILB-456
		/*chdrmjaIO.setFunction("KEEPS");
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		chdrpfDAO.setCacheObject(chdrpf);
		covtpfDAO.setCacheObject(covtpf);
		/* Perform the standard GENSWCH setup  and execution               */
		sv.taxind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar17 = 0; !(loopVar17 == 8); loopVar17 += 1){
			save4510();
		}
		gensswrec.function.set("G");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar18 = 0; !(loopVar18 == 8); loopVar18 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet4b50()
	{
		/*B50-START*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		sv.taxind.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar19 = 0; !(loopVar19 == 8); loopVar19 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*B59-EXIT*/
	}

protected void policyLoad5000()
	{
		try {
			loadPolicy5010();
			readCovt5020();
			fieldsToScreen5080();
			mortalityLien5090();
			ageProcessing50a0();
			optionsExtras50b0();
			enquiryProtect50c0();
		}
		catch (GOTOException e){
		}
	}

protected void loadPolicy5010()
	{
		wsaaOldPrem.set(0);
		wsaaOldSumins.set(0);
		if (planLevel.isTrue()) {
			readCovr5100();
			//ILB-456
			for (Covrpf covr : covrpfList) {
				if (covr == null) {
					goTo(GotoLabel.exit50z0);
				}
				else {
					//covtmjaIO.setFunction(varcom.readr);
					if (addComp.isTrue()) {
						covtpf.setCoverage(wsaaCovrCoverage.toString());
						covtpf.setRider(wsaaCovrRider.toString());
						covtpf.setPlnsfx(covr.getPlanSuffix());
					}
					else {
						//covtpf.setDataKey(wsaaCovrKey.trim());
						if (isNE(covr.getPlanSuffix(),ZERO)) {
							covtpf.setPlnsfx(covr.getPlanSuffix());
						}
					}
					covrpf.setPlanSuffix(covr.getPlanSuffix());
				}
				
			}
		}
		wsaaOldSumins.set(covrpf.getSumins());
		wsaaOldPrem.set(covrpf.getInstprem());
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			sv.planSuffix.set(chdrpf.getPolsum());
			sv.plnsfxOut[varcom.hi.toInt()].set("Y");
		}
		else {
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
	}

protected void readCovt5020()
	{
		/* Read the Temporary COVT record which will contain any previous*/
		/*   changes before AT submission creates the COVRMJA record.*/
		listCovtpf = covtpfDAO.getCovtpfData(covrpf.getChdrcoy(),covrpf.getChdrnum(),covrpf.getLife().trim(),covrpf.getCoverage().trim(),covrpf.getRider(),covrpf.getPlanSuffix());
		if (!addComp.isTrue()) {
			recordToScreen6000();
			return;
		}
		else {
			if (listCovtpf.isEmpty()) {
				covtpf.setAnbccd01(0);
				covtpf.setAnbccd02(0);
				covtpf.setSingp(BigDecimal.ZERO);
				covtpf.setPlnsfx(0);
				covtpf.setInstprem(BigDecimal.ZERO);
				covtpf.setZlinstprem(BigDecimal.ZERO);
				/*BRD-306 START */
				covtpf.setLoadper(BigDecimal.ZERO);
				covtpf.setRateadj(BigDecimal.ZERO);
				covtpf.setFltmort(BigDecimal.ZERO);
				covtpf.setPremadj(BigDecimal.ZERO);
				covtpf.setAgeadj(BigDecimal.ZERO);
				/*BRD-306 END */
				covtpf.setZbinstprem(BigDecimal.ZERO);
				covtpf.setPcesage(0);
				covtpf.setPcestrm(0);
				covtpf.setRcesage(0);
				covtpf.setRcestrm(0);
				covtpf.setBcesage(0);
				covtpf.setBcestrm(0);
				covtpf.setRsunin("");
				covtpf.setSumins(BigDecimal.ZERO);
				covtpf.setEffdate(payrIO.getBtdate().toInt());
				covtpf.setPcesdte(varcom.vrcmMaxDate.toInt());
				covtpf.setBcesdte(varcom.vrcmMaxDate.toInt());
				covtpf.setRcesdte(varcom.vrcmMaxDate.toInt());
				covtpf.setPolinc(chdrpf.getPolinc());
				covtpf.setCrtable(wsaaCrtable.toString());
				tableLoads7000();
			}
			else {
				tableLoads7000();
			}
		}
		if (isEQ(covtpf.getJlife(),"01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
	}

protected void fieldsToScreen5080()
	{
		sv.planSuffix.set(chdrpf.getPolinc());
		sv.liencd.set(covtpf.getLiencd());
		sv.mortcls.set(covtpf.getMortcls());
		sv.premCessDate.set(covtpf.getPcesdte());
		sv.premCessAge.set(covtpf.getPcesage());
		sv.premCessTerm.set(covtpf.getPcestrm());
		sv.riskCessDate.set(covtpf.getRcesdte());
		sv.riskCessAge.set(covtpf.getRcesage());
		sv.riskCessTerm.set(covtpf.getRcestrm());
		sv.benCessDate.set(covtpf.getBcesdte());
		sv.benCessAge.set(covtpf.getBcesage());
		sv.benCessTerm.set(covtpf.getBcestrm());
		sv.instPrem.set(covtpf.getInstprem());
		sv.zbinstprem.set(covtpf.getZbinstprem());
		sv.zlinstprem.set(covtpf.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtpf.getLoadper());
		sv.rateadj.set(covtpf.getRateadj());
		sv.fltmort.set(covtpf.getFltmort());
		sv.premadj.set(covtpf.getPremadj());
		sv.adjustageamt.set(covtpf.getAgeadj());
		/*BRD-306 END */
		sv.sumin.set(covtpf.getSumins());
		setupBonus1300();
		sv.bappmeth.set(covtpf.getBappmeth());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		if (isEQ(t5606rec.sumInsMax,0)
		&& isEQ(t5606rec.sumInsMin,0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5606rec.sumInsMax,t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax,0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5606rec.sumInsMax);
		}
		if (isNE(t5606rec.sumInsMax,0)
		|| isNE(t5606rec.sumInsMin,0)) {
			descIO.setDataArea(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(tablesInner.t3590);
			descIO.setDescitem(t5606rec.benfreq);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)
			&& isNE(descIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			else {
				if (isEQ(descIO.getStatuz(),varcom.oK)) {
					sv.frqdesc.set(descIO.getShortdesc());
				}
				else {
					sv.frqdesc.fill("?");
				}
			}
		}
	}

protected void mortalityLien5090()
	{
		if (isEQ(t5606rec.mortclss,SPACES)) {
			/*sv.mortclsOut[varcom.nd.toInt()].set("Y");*/
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		
		
		if (isNE(t5606rec.mortcls01,SPACES)
		&& isEQ(t5606rec.mortcls02,SPACES)
		&& isEQ(t5606rec.mortcls03,SPACES)
		&& isEQ(t5606rec.mortcls04,SPACES)
		&& isEQ(t5606rec.mortcls05,SPACES)
		&& isEQ(t5606rec.mortcls06,SPACES)) {
			sv.mortcls.set(t5606rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5606rec.liencds,SPACES)) {
			//sv.liencdOut[varcom.nd.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void ageProcessing50a0()
	{
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		if (isEQ(covtpf.getJlife(),"01")) {
			wszzCltdob.set(wsbbCltdob);
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
		}
		checkDefaults5300();
		if ((wsaaDefaultsInner.defaultRa.isTrue()
		|| wsaaDefaultsInner.defaultRt.isTrue())
		&& isNE(t5606rec.eaage,SPACES)) {
			riskCessDate5400();
		}
		if ((wsaaDefaultsInner.defaultPa.isTrue()
		|| wsaaDefaultsInner.defaultPt.isTrue())
		&& isNE(t5606rec.eaage,SPACES)) {
			premCessDate5450();
		}
		if ((wsaaDefaultsInner.defaultBa.isTrue()
		|| wsaaDefaultsInner.defaultBt.isTrue())
		&& isNE(t5606rec.eaage,SPACES)) {
			benCessDate5470();
		}
		if (isNE(t5606rec.eaage,SPACES)) {
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void optionsExtras50b0()
	{
		if (isEQ(t5606rec.specind,"N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext5600();
		}
		/*B2-PREMIUM-BREAKDOWN*/
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(sv.life);
		wsaaPovrCoverage.set(sv.coverage);
		wsaaPovrRider.set(sv.rider);
	}

protected void enquiryProtect50c0()
	{
		if (compEnquiry.isTrue()) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.livesnoOut[varcom.pr.toInt()].set("Y");
			sv.benplnOut[varcom.pr.toInt()].set("Y");
			sv.waiverpremOut[varcom.pr.toInt()].set("Y");
			sv.waitperiodOut[varcom.pr.toInt()].set("Y");
			sv.bentrmOut[varcom.pr.toInt()].set("Y");
			sv.poltypOut[varcom.pr.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void readCovr5100()
	{
		/*READ-COVRMJA*/
		//ILIFE-8167
		covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
		for (Covrpf covr:covrpfList) {
		if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
				|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
				|| isNE(covr.getLife(),wsaaCovrLife)
				|| isNE(covr.getCoverage(),wsaaCovrCoverage)
				|| isNE(covr.getRider(),wsaaCovrRider)) {
					return;
				}
		}

		/*EXIT*/
	}

protected void calcPremium5200()
	{
		try {
			calcPremium5210();
			methodTable5220();
		}
		catch (GOTOException e){
		}
	}

protected void calcPremium5210()
	{
		wsaaPremStatuz.set("N");
		if (isNE(t5687rec.bbmeth,SPACES)) {
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.exit5290);
		}
	}

protected void methodTable5220()
	{
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(AppVars.getInstance().getAppConfig().isVpmsEnable())
			{
				premiumrec.premMethod.set(itemIO.getItemitem());
			}
			/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] End */
			t5675rec.t5675Rec.set(itemIO.getGenarea());
		}
	}

protected void checkDefaults5300()
	{
		try {
			searchTable5310();
		}
		catch (GOTOException e){
		}
	}

protected void searchTable5310()
	{
		sub1.set(1);
		wsaaDefaultsInner.wsaaDefaults.set(SPACES);
		while ( !(isGT(sub1,wsaaMaxOcc))) {
			nextColumn5320();
		}
		
		if (wsaaDefaultsInner.defaultRa.isTrue()) {
			sv.riskCessAge.set(wsddRiskCessAge);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultRt.isTrue()) {
			sv.riskCessTerm.set(wsddRiskCessTerm);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPa.isTrue()) {
			sv.premCessAge.set(wsddPremCessAge);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPt.isTrue()) {
			sv.premCessTerm.set(wsddPremCessTerm);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultBa.isTrue()) {
			sv.benCessAge.set(wsddBenCessAge);
			sv.bcessageOut[varcom.pr.toInt()].set("Y");
			sv.bcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultBt.isTrue()) {
			sv.benCessTerm.set(wsddBenCessTerm);
			sv.bcessageOut[varcom.pr.toInt()].set("Y");
			sv.bcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		goTo(GotoLabel.exit5390);
	}

protected void nextColumn5320()
	{
		if (!wsaaDefaultsInner.nodefRa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.riskCessageFrom[sub1.toInt()],t5606rec.riskCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRa.isTrue()
				&& !wsaaDefaultsInner.defaultRt.isTrue()) {
					wsddRiskCessAge.set(t5606rec.riskCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.premCessageFrom[sub1.toInt()],t5606rec.premCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPa.isTrue()
				&& !wsaaDefaultsInner.defaultPt.isTrue()) {
					wsddPremCessAge.set(t5606rec.premCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefBa.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.benCessageFrom[sub1.toInt()],t5606rec.benCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultBa.isTrue()
				&& !wsaaDefaultsInner.defaultBt.isTrue()) {
					wsddBenCessAge.set(t5606rec.benCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultBa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultBa.set("N");
					wsaaDefaultsInner.wsaaDefaultBt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefRt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.riskCesstermFrom[sub1.toInt()],t5606rec.riskCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRt.isTrue()
				&& !wsaaDefaultsInner.defaultRa.isTrue()) {
					wsddRiskCessTerm.set(t5606rec.riskCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.premCesstermFrom[sub1.toInt()],t5606rec.premCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPt.isTrue()
				&& !wsaaDefaultsInner.defaultPa.isTrue()) {
					wsddPremCessTerm.set(t5606rec.premCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefBt.isTrue()) {
			if (isGTE(wszzAnbAtCcd,t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd,t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.benCesstermFrm[sub1.toInt()],t5606rec.benCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultBt.isTrue()
				&& !wsaaDefaultsInner.defaultBa.isTrue()) {
					wsddBenCessTerm.set(t5606rec.benCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultBt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultBa.set("N");
					wsaaDefaultsInner.wsaaDefaultBt.set("N");
				}
			}
		}
		sub1.add(1);
	}

protected void riskCessDate5400()
	{
			riskCessDatePara5400();
		}

protected void riskCessDatePara5400()
	{
		if (isEQ(sv.riskCessAge,ZERO)
		&& isEQ(sv.riskCessTerm,ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.riskCessAge,0),true)
		&& isEQ(isGT(sv.riskCessTerm,0),false)){
			if (isEQ(t5606rec.eaage,"A")) {
				if (addComp.isTrue()) {
					if (isEQ(payrIO.getBillfreq(),ZERO)) {
						datcon2rec.intDate1.set(datcon1rec.intDate);
					}
					else {
						datcon2rec.intDate1.set(payrIO.getBtdate());
					}
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.riskCessAge,wszzAnbAtCcd));
			}
			if (isEQ(t5606rec.eaage,"E")
			|| isEQ(t5606rec.eaage,SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.riskCessAge);
			}
		}
		else if (isEQ(isGT(sv.riskCessAge,0),false)
		&& isEQ(isGT(sv.riskCessTerm,0),true)){
			if (isEQ(t5606rec.eaage,"A")
			|| isEQ(t5606rec.eaage,SPACES)) {
				if (addComp.isTrue()) {
					if (isEQ(payrIO.getBillfreq(),ZERO)) {
						datcon2rec.intDate1.set(datcon1rec.intDate);
					}
					else {
						datcon2rec.intDate1.set(payrIO.getBtdate());
					}
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.riskCessTerm);
			}
			if (isEQ(t5606rec.eaage,"E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.riskCessTerm,wszzAnbAtCcd),1));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.riskCessDate.set(datcon2rec.intDate2);
		}
	}

protected void premCessDate5450()
	{
			premCessDatePara5450();
		}

protected void premCessDatePara5450()
	{
		if (isEQ(sv.premCessAge,ZERO)
		&& isEQ(sv.premCessTerm,ZERO)) {
			/*        GO TO 5490-EXIT.*/
			return ;
		}
		if (isEQ(isGT(sv.premCessAge,0),true)
		&& isEQ(isGT(sv.premCessTerm,0),false)){
			if (isEQ(t5606rec.eaage,"A")) {
				if (addComp.isTrue()) {
					if (isEQ(payrIO.getBillfreq(),ZERO)) {
						datcon2rec.intDate1.set(datcon1rec.intDate);
					}
					else {
						datcon2rec.intDate1.set(payrIO.getBtdate());
					}
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge,wszzAnbAtCcd));
			}
			if (isEQ(t5606rec.eaage,"E")
			|| isEQ(t5606rec.eaage,SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.premCessAge);
			}
		}
		else if (isEQ(isGT(sv.premCessAge,0),false)
		&& isEQ(isGT(sv.premCessTerm,0),true)){
			if (isEQ(t5606rec.eaage,"A")
			|| isEQ(t5606rec.eaage,SPACES)) {
				if (addComp.isTrue()) {
					if (isEQ(payrIO.getBillfreq(),ZERO)) {
						datcon2rec.intDate1.set(datcon1rec.intDate);
					}
					else {
						datcon2rec.intDate1.set(payrIO.getBtdate());
					}
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.premCessTerm);
			}
			if (isEQ(t5606rec.eaage,"E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.premCessTerm,wszzAnbAtCcd),1));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.premCessDate.set(datcon2rec.intDate2);
		}
	}

protected void benCessDate5470()
	{
			benCessDatePara5470();
		}

protected void benCessDatePara5470()
	{
		if (isEQ(sv.benCessAge,ZERO)
		&& isEQ(sv.benCessTerm,ZERO)) {
			return ;
		}
		if (isEQ(isGT(sv.benCessAge,0),true)
		&& isEQ(isGT(sv.benCessTerm,0),false)){
			if (isEQ(t5606rec.eaage,"A")) {
				if (addComp.isTrue()) {
					if (isEQ(payrIO.getBillfreq(),ZERO)) {
						datcon2rec.intDate1.set(datcon1rec.intDate);
					}
					else {
						datcon2rec.intDate1.set(payrIO.getBtdate());
					}
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				compute(datcon2rec.freqFactor, 0).set(sub(sv.benCessAge,wszzAnbAtCcd));
			}
			if (isEQ(t5606rec.eaage,"E")
			|| isEQ(t5606rec.eaage,SPACES)) {
				datcon2rec.intDate1.set(wszzCltdob);
				datcon2rec.freqFactor.set(sv.benCessAge);
			}
		}
		else if (isEQ(isGT(sv.benCessAge,0),false)
		&& isEQ(isGT(sv.benCessTerm,0),true)){
			if (isEQ(t5606rec.eaage,"A")
			|| isEQ(t5606rec.eaage,SPACES)) {
				if (addComp.isTrue()) {
					if (isEQ(payrIO.getBillfreq(),ZERO)) {
						datcon2rec.intDate1.set(datcon1rec.intDate);
					}
					else {
						datcon2rec.intDate1.set(payrIO.getBtdate());
					}
				}
				else {
					datcon2rec.intDate1.set(covrpf.getCrrcd());
				}
				datcon2rec.freqFactor.set(sv.benCessTerm);
			}
			if (isEQ(t5606rec.eaage,"E")) {
				datcon2rec.intDate1.set(wszzCltdob);
				compute(datcon2rec.freqFactor, 0).set(sub(add(sv.benCessTerm,wszzAnbAtCcd),1));
			}
		}
		callDatcon25500();
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			sv.bcesdteErr.set(datcon2rec.statuz);
		}
		else {
			sv.benCessDate.set(datcon2rec.intDate2);
		}
	}

protected void callDatcon25500()
	{
		/*CALL*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkLext5600()
	{
		readLext5610();
	}

protected void readLext5610()
	{
		lextrevIO.setParams(SPACES);
		lextrevIO.setChdrcoy(chdrpf.getChdrcoy());
		lextrevIO.setChdrnum(chdrpf.getChdrnum());
		lextrevIO.setLife(covtpf.getLife());
		lextrevIO.setCoverage(covtpf.getCoverage());
		lextrevIO.setRider(covtpf.getRider());
		lextrevIO.setSeqnbr(ZERO);
		lextrevIO.setTranno(ZERO);
		lextrevIO.setFormat(formatsInner.lextrevrec);
		lextrevIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		lextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextrevIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, lextrevIO);
		if (isNE(lextrevIO.getStatuz(),varcom.oK)
		&& isNE(lextrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextrevIO.getParams());
			syserrrec.statuz.set(lextrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(),lextrevIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),lextrevIO.getChdrnum())
		|| isNE(covtpf.getLife(),lextrevIO.getLife())
		|| isNE(covtpf.getCoverage(),lextrevIO.getCoverage())
		|| isNE(covtpf.getRider(),lextrevIO.getRider())) {
			lextrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextrevIO.getStatuz(),varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
		if (isNE(lextrevIO.getStatuz(),varcom.endp)) {
			if (isGT(lextrevIO.getTranno(),chdrpf.getTranno())) {
				lextrevIO.setStatuz(varcom.endp);
			}
		}
		while ( !(isEQ(lextrevIO.getStatuz(),varcom.endp))) {
			lextrevIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, lextrevIO);
			if (isNE(lextrevIO.getStatuz(),varcom.oK)
			&& isNE(lextrevIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(lextrevIO.getParams());
				syserrrec.statuz.set(lextrevIO.getStatuz());
				fatalError600();
			}
			if (isNE(chdrpf.getChdrcoy(),lextrevIO.getChdrcoy())
			|| isNE(chdrpf.getChdrnum(),lextrevIO.getChdrnum())
			|| isNE(covtpf.getLife(),lextrevIO.getLife())
			|| isNE(covtpf.getCoverage(),lextrevIO.getCoverage())
			|| isNE(covtpf.getRider(),lextrevIO.getRider())) {
				lextrevIO.setStatuz(varcom.endp);
			}
			else {
				if (isGT(lextrevIO.getTranno(),chdrpf.getTranno())) {
					lextrevIO.setStatuz(varcom.endp);
				}
			}
		}
        readFlupal1500();
	}

protected void recordToScreen6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covrRecord6010();
					fieldsToScreen6020();
				case covtExist6025: 
					covtExist6025();
					enquiryProtect6030();
					optionsExtras603a();
					premiumBreakdown603c();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covrRecord6010()
	{
		if (listCovtpf.isEmpty()) {
			covtpf.setSex01(wsaaSex.toString());
			covtpf.setSex02(wsbbSex.toString());
			covtpf.setAnbccd01(wsaaAnbAtCcd.toInt());
			covtpf.setAnbccd02(wsbbAnbAtCcd.toInt());
			covtpf.setCrtable(covrpf.getCrtable());
			covtpf.setRcesdte(covrpf.getRiskCessDate());
			covtpf.setPcesdte(covrpf.getPremCessDate());
			covtpf.setBcesdte(covrpf.getBenCessDate());
			covtpf.setRcesage(covrpf.getRiskCessAge());
			covtpf.setPcesage(covrpf.getPremCessAge());
			covtpf.setBcesage(covrpf.getBenCessAge());
			covtpf.setRcestrm(covrpf.getRiskCessTerm());
			covtpf.setPcestrm(covrpf.getPremCessTerm());
			covtpf.setBcestrm(covrpf.getBenCessTerm());
			covtpf.setRundte(covrpf.getReserveUnitsDate());
			covtpf.setRsunin(covrpf.getReserveUnitsInd());
			covtpf.setSumins(covrpf.getSumins());
			covtpf.setInstprem(covrpf.getInstprem());
			covtpf.setZbinstprem(covrpf.getZbinstprem());
			covtpf.setZlinstprem(covrpf.getZlinstprem());
			/*BRD-306 START */
			covtpf.setLoadper(covrpf.getLoadper());
			covtpf.setRateadj(covrpf.getRateadj());
			covtpf.setFltmort(covrpf.getFltmort());
			covtpf.setPremadj(covrpf.getPremadj());
			covtpf.setAgeadj(covrpf.getAgeadj());
			/*BRD-306 END */
			covtpf.setMortcls(covrpf.getMortcls());
			covtpf.setLiencd(covrpf.getLiencd());
			covtpf.setJlife(covrpf.getJlife());
			covtpf.setEffdate(covrpf.getCrrcd());
			covtpf.setBillfreq(payrIO.getBillfreq().toString());
			covtpf.setBillchnl(payrIO.getBillchnl().toString());
			covtpf.setSingp(covrpf.getSingp());
			covtpf.setBappmeth(covrpf.getBappmeth());
			sv.statFund.set(covrpf.getStatFund());
			sv.statSect.set(covrpf.getStatSect());
			sv.statSubsect.set(covrpf.getStatSubsect());
		}
		covtpf.setPolinc(chdrpf.getPolinc());
		covtpf.setPayrseqno(covrpf.getPayrseqno());
	}

protected void fieldsToScreen6020()
	{
		if (isEQ(covtpf.getJlife(),"01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
		if (listCovtpf.isEmpty()) {
			/*NEXT_SENTENCE*/
		}
		else {
			covtpf = listCovtpf.get(0);
			goTo(GotoLabel.covtExist6025);
		}
		if (isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& !planLevel.isTrue()) {
			if (isEQ(sv.planSuffix,1)) {
				compute(sv.sumin, 2).set((sub(covtpf.getSumins(),(div(mult(covtpf.getSumins(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.instPrem, 2).set((sub(covtpf.getInstprem(),(div(mult(covtpf.getInstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.zbinstprem, 2).set((sub(covtpf.getZbinstprem(),(div(mult(covtpf.getZbinstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(sv.zlinstprem, 2).set((sub(covtpf.getZlinstprem(),(div(mult(covtpf.getZlinstprem(),(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
			}
			else {
				compute(sv.sumin, 3).setRounded(div(covtpf.getSumins(),chdrpf.getPolsum()));
				compute(sv.instPrem, 3).setRounded(div(covtpf.getInstprem(),chdrpf.getPolsum()));
				compute(sv.zbinstprem, 3).setRounded(div(covtpf.getZbinstprem(),chdrpf.getPolsum()));
				compute(sv.zlinstprem, 3).setRounded(div(covtpf.getZlinstprem(),chdrpf.getPolsum()));
			}
		}
		else {
			sv.sumin.set(covtpf.getSumins());
			sv.zbinstprem.set(covtpf.getZbinstprem());
			sv.zlinstprem.set(covtpf.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtpf.getLoadper());
			sv.rateadj.set(covtpf.getRateadj());
			sv.fltmort.set(covtpf.getFltmort());
			sv.premadj.set(covtpf.getPremadj());
			sv.adjustageamt.set(covtpf.getAgeadj());
			/*BRD-306 END */

			sv.instPrem.set(covtpf.getInstprem());
		}
		if (isLTE(sv.planSuffix,chdrpf.getPolsum())
		&& !planLevel.isTrue()) {
			if (isEQ(sv.planSuffix,1)) {
				compute(wsaaOldSumins, 0).set((sub(wsaaOldSumins,(div(mult(wsaaOldSumins,(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
				compute(wsaaOldPrem, 2).set((sub(wsaaOldPrem,(div(mult(wsaaOldPrem,(sub(chdrpf.getPolsum(),1))),chdrpf.getPolsum())))));
			}
			else {
				compute(wsaaOldSumins, 1).setRounded(div(wsaaOldSumins,chdrpf.getPolsum()));
				compute(wsaaOldPrem, 3).setRounded(div(wsaaOldPrem,chdrpf.getPolsum()));
			}
		}
	}

protected void covtExist6025()
	{
		if (!(listCovtpf.isEmpty())) {
			sv.sumin.set(covtpf.getSumins());
			sv.zbinstprem.set(covtpf.getZbinstprem());
			sv.zlinstprem.set(covtpf.getZlinstprem());
			/*BRD-306 START */
			sv.loadper.set(covtpf.getLoadper());
			sv.rateadj.set(covtpf.getRateadj());
			sv.fltmort.set(covtpf.getFltmort());
			sv.premadj.set(covtpf.getPremadj());
			sv.adjustageamt.set(covtpf.getAgeadj());
			/*BRD-306 END */

			sv.instPrem.set(covtpf.getInstprem());
		}
		sv.liencd.set(covtpf.getLiencd());
		sv.mortcls.set(covtpf.getMortcls());
		sv.premCessDate.set(covtpf.getPcesdte());
		sv.premCessAge.set(covtpf.getPcesage());
		sv.premCessTerm.set(covtpf.getPcestrm());
		sv.riskCessDate.set(covtpf.getRcesdte());
		sv.riskCessAge.set(covtpf.getRcesage());
		sv.riskCessTerm.set(covtpf.getRcestrm());
		sv.benCessDate.set(covtpf.getBcesdte());
		sv.benCessAge.set(covtpf.getBcesage());
		sv.benCessTerm.set(covtpf.getBcestrm());
		setupBonus1300();
		sv.bappmeth.set(covtpf.getBappmeth());
	}

protected void enquiryProtect6030()


	

	{
		if (isEQ(wsspcomn.flag,"I")
//				MIBT-324
				|| isEQ(wsspcomn.flag,"P")
				|| isEQ(wsspcomn.flag,"S") 
				
				//ILIFE-1407 START by nnazeer
				
	            || isEQ(wsspcomn.flag,"R")) {
                 
			    //ILIFE-1407 END
	
	
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
//			sv.bappmethOut[varcom.pr.toInt()].set("Y"); //MIBT-324
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.livesnoOut[varcom.pr.toInt()].set("Y");
			sv.benplnOut[varcom.pr.toInt()].set("Y");
			sv.waiverpremOut[varcom.pr.toInt()].set("Y");
		sv.waitperiodOut[varcom.pr.toInt()].set("Y");
			sv.bentrmOut[varcom.pr.toInt()].set("Y");
			sv.poltypOut[varcom.pr.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
		}
//		MIBT-324
		if (isEQ(wsspcomn.flag,"I")){
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			}
		if (!addComp.isTrue()) {
			tableLoads7000();
		}
		if (isEQ(t5606rec.sumInsMax,0)
		&& isEQ(t5606rec.sumInsMin,0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5606rec.sumInsMax,t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax,0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5606rec.sumInsMax);
		}
	}

protected void optionsExtras603a()
	{
		if (isEQ(t5606rec.specind,"N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext5600();
		}
	}

protected void premiumBreakdown603c()
	{
		wsaaPovrChdrcoy.set(chdrpf.getChdrcoy());
		wsaaPovrChdrnum.set(chdrpf.getChdrnum());
		wsaaPovrLife.set(sv.life);
		wsaaPovrCoverage.set(sv.coverage);
		wsaaPovrRider.set(sv.rider);
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/*EXIT*/
	}

protected void tableLoads7000()
	{
		t5687Load7010();
		t5671Load7020();
		editRules7030();
		t5606Load7040();
		t5667Load7045();
		t5675Load7050();
	}

protected void t5687Load7010()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covtpf.getCrtable());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),covtpf.getCrtable())
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			scrnparams.errorCode.set(errorsInner.f294);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void t5671Load7020()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtpf.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(covtpf.getCrtable());
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
	}

protected void editRules7030()
	{
		if (isEQ(t5671rec.pgm[1],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3],wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
	}

protected void t5606Load7040()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5606);
		wsbbCurrency.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(tablesInner.t5606, itdmIO.getItemtabl())
		|| isNE(wsbbTranCurrency,itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			t5606rec.t5606Rec.set(itdmIO.getGenarea());
		}
		else {
			t5606rec.t5606Rec.set(SPACES);
			t5606rec.ageIssageFrms.fill("0");
			t5606rec.ageIssageTos.fill("0");
			t5606rec.termIssageFrms.fill("0");
			t5606rec.termIssageTos.fill("0");
			t5606rec.premCessageFroms.fill("0");
			t5606rec.premCessageTos.fill("0");
			t5606rec.premCesstermFroms.fill("0");
			t5606rec.premCesstermTos.fill("0");
			t5606rec.riskCessageFroms.fill("0");
			t5606rec.riskCessageTos.fill("0");
			t5606rec.riskCesstermFroms.fill("0");
			t5606rec.riskCesstermTos.fill("0");
			t5606rec.benCessageFroms.fill("0");
			t5606rec.benCessageTos.fill("0");
			t5606rec.benCesstermFrms.fill("0");
			t5606rec.benCesstermTos.fill("0");
			t5606rec.sumInsMax.set(ZERO);
			t5606rec.sumInsMin.set(ZERO);
			if (isEQ(scrnparams.errorCode,SPACES)) {
				scrnparams.errorCode.set(errorsInner.f335);
			}
		}
		if(isEQ(t5606rec.waitperiod, SPACES)){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
		}
		else{
			waitperiodFlag=true;
		}
		if(isEQ(t5606rec.bentrm, SPACES)){
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
		}
		else{
			bentrmFlag=true;
		}
		if(isEQ(t5606rec.poltyp, SPACES)){
			sv.poltypOut[varcom.nd.toInt()].set("Y");
		}
		else{
			poltypFlag=true;
		}
		if(isEQ(t5606rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		else{
			prmbasisFlag=true;
		}
	}

protected void t5667Load7045()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtpf.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5667);
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(wsbbCurrency);
		itemIO.setItemitem(wsbbT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
	}

protected void t5675Load7050()
	{
		if (singlif.isTrue()) {
			itemIO.setItemitem(t5687rec.premmeth);
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			itemIO.setItemitem(t5687rec.jlPremMeth);
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			wsaaPremStatuz.set("Y");
		}
		else {
			calcPremium5200();
		}
		/*EXIT*/
	}

protected void getAnny8000()
	{
		read8010();
	}

protected void read8010()
	{
		annyIO.setChdrcoy(chdrpf.getChdrcoy());
		annyIO.setChdrnum(chdrpf.getChdrnum());
		annyIO.setLife(covrpf.getLife());
		annyIO.setCoverage(covrpf.getCoverage());
		annyIO.setRider(covrpf.getRider());
		if (isNE(wsaaPlanSuffix,ZERO)
		&& isLTE(chdrpf.getPolsum(),wsaaPlanSuffix)) {
			annyIO.setPlanSuffix(ZERO);
		}
		else {
			annyIO.setPlanSuffix(wsaaStorePlanSuffix);
		}
		annyIO.setFormat(formatsInner.annyrec);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
	}

protected void getNewAnnuityDetails8100()
	{
			checkForAnnt8110();
		}

protected void checkForAnnt8110()
	{
		anntmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		anntmjaIO.setChdrnum(chdrpf.getChdrnum());
		anntmjaIO.setLife(covrpf.getLife());
		anntmjaIO.setCoverage(covrpf.getCoverage());
		anntmjaIO.setRider(covrpf.getRider());
		anntmjaIO.setPlanSuffix(wsaaStorePlanSuffix);
		anntmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, anntmjaIO);
		if (isNE(anntmjaIO.getStatuz(),varcom.oK)
		&& isNE(anntmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(anntmjaIO.getParams());
			syserrrec.statuz.set(anntmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(anntmjaIO.getStatuz(),varcom.oK)) {
			premiumrec.freqann.set(anntmjaIO.getFreqann());
			premiumrec.advance.set(anntmjaIO.getAdvance());
			premiumrec.arrears.set(anntmjaIO.getArrears());
			premiumrec.guarperd.set(anntmjaIO.getGuarperd());
			premiumrec.intanny.set(anntmjaIO.getIntanny());
			premiumrec.capcont.set(anntmjaIO.getCapcont());
			premiumrec.withprop.set(anntmjaIO.getWithprop());
			premiumrec.withoprop.set(anntmjaIO.getWithoprop());
			premiumrec.ppind.set(anntmjaIO.getPpind());
			premiumrec.nomlife.set(anntmjaIO.getNomlife());
			premiumrec.dthpercn.set(anntmjaIO.getDthpercn());
			premiumrec.dthperco.set(anntmjaIO.getDthperco());
			return ;
		}
		annyIO.setChdrcoy(chdrpf.getChdrcoy());
		annyIO.setChdrnum(chdrpf.getChdrnum());
		annyIO.setLife(covrpf.getLife());
		annyIO.setCoverage(covrpf.getCoverage());
		annyIO.setRider(covrpf.getRider());
		if (isNE(wsaaPlanSuffix,ZERO)
		&& isLTE(chdrpf.getPolsum(),wsaaPlanSuffix)) {
			annyIO.setPlanSuffix(ZERO);
		}
		else {
			annyIO.setPlanSuffix(wsaaStorePlanSuffix);
		}
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		premiumrec.freqann.set(annyIO.getFreqann());
		premiumrec.advance.set(annyIO.getAdvance());
		premiumrec.arrears.set(annyIO.getArrears());
		premiumrec.guarperd.set(annyIO.getGuarperd());
		premiumrec.intanny.set(annyIO.getIntanny());
		premiumrec.capcont.set(annyIO.getCapcont());
		premiumrec.withprop.set(annyIO.getWithprop());
		premiumrec.withoprop.set(annyIO.getWithoprop());
		premiumrec.ppind.set(annyIO.getPpind());
		premiumrec.nomlife.set(annyIO.getNomlife());
		premiumrec.dthpercn.set(annyIO.getDthpercn());
		premiumrec.dthperco.set(annyIO.getDthperco());
	}

protected void checkCalcTax8200()
	{
		try {
			start8210();
			callTaxSubr8250();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start8210()
	{
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.exit8290);
		}
		if (notFirstTaxCalc.isTrue()) {
			callTaxSubr8250();
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set(covtpf.getCrtable());
		readTr52e8300();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrpf.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e8300();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e8300();
		}
		wsaaFirstTaxCalc.set("N");
	}

protected void callTaxSubr8250()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
			txcalcrec.chdrnum.set(chdrpf.getChdrnum());
			txcalcrec.life.set(covtpf.getLife());
			txcalcrec.coverage.set(covtpf.getCoverage());
			txcalcrec.rider.set(covtpf.getRider());
			txcalcrec.planSuffix.set(covtpf.getPlnsfx());
			txcalcrec.crtable.set(covtpf.getCrtable());
			txcalcrec.cnttype.set(chdrpf.getCnttype());
			txcalcrec.register.set(chdrpf.getReg());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrpf.getCntcurr());
			wsaaCntCurr.set(chdrpf.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(sv.zbinstprem);
			}
			else {
				txcalcrec.amountIn.set(sv.instPrem);
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrpf.getPtdate());
			txcalcrec.tranno.set(chdrpf.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				wsaaTaxamt.set(sv.instPrem);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				sv.taxind.set("+");
				sv.taxamtOut[varcom.nd.toInt()].set("N");
				sv.taxindOut[varcom.nd.toInt()].set("N");
				sv.taxamtOut[varcom.pr.toInt()].set("Y");
				sv.taxindOut[varcom.pr.toInt()].set("N");
			}
		}
	}

protected void readTr52e8300()
	{
		start8310();
	}

protected void start8310()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), wsspcomn.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void readPovr9000()
	{
		start9010();
	}

protected void start9010()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsaaPovrChdrcoy);
		povrIO.setChdrnum(wsaaPovrChdrnum);
		povrIO.setLife(wsaaPovrLife);
		povrIO.setCoverage(wsaaPovrCoverage);
		povrIO.setRider(wsaaPovrRider);
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)
		&& isNE(povrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

protected void pbindExe9100()
	{
		start9110();
	}

protected void start9110()
	{
		readPovr9000();
		if (isEQ(povrIO.getStatuz(),varcom.endp)
		|| isNE(povrIO.getChdrcoy(),wsaaPovrChdrcoy)
		|| isNE(povrIO.getChdrnum(),wsaaPovrChdrnum)
		|| isNE(povrIO.getLife(),wsaaPovrLife)
		|| isNE(povrIO.getCoverage(),wsaaPovrCoverage)
		|| isNE(povrIO.getRider(),wsaaPovrRider)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		//ILB-456
		/*chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}*/
		chdrpfDAO.setCacheObject(chdrpf);
		sv.pbind.set("?");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar20 = 0; !(loopVar20 == 8); loopVar20 += 1){
			save4510();
		}
		gensswrec.function.set("E");
		gensww4210();
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar21 = 0; !(loopVar21 == 8); loopVar21 += 1){
			load4530();
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void pbindRet9200()
	{
		start9210();
	}

protected void start9210()
	{
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		sv.pbind.set("+");
		compute(sub1, 0).set(add(1,wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar22 = 0; !(loopVar22 == 8); loopVar22 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

	/**
	* <pre>
	*9300-CHECK-T5602 SECTION.                                        
	*9310-START.                                                      
	* Read the SUM Coverage table & check if the coverage is
	* a WOP. If so then non-display and protect the benefit amount
	* and benefit frequency description on screen.....
	*    MOVE SPACES                 TO ITEM-PARAMS.                  
	*    MOVE 'IT'                   TO ITEM-ITEMPFX.                 
	*    MOVE CHDRMJA-CHDRCOY        TO ITEM-ITEMCOY.                 
	*    MOVE T5602                  TO ITEM-ITEMTABL.                
	*    MOVE WSAA-CRTABLE           TO ITEM-ITEMITEM.                
	*    MOVE ITEMREC                TO ITEM-FORMAT.                  
	*    MOVE READR                  TO ITEM-FUNCTION.                
	*    CALL 'ITEMIO' USING ITEM-PARAMS.                             
	*    IF ITEM-STATUZ = O-K                                         
	*       MOVE 'Y'                 TO WSAA-SUMFLAG                  
	*       MOVE ITEM-GENAREA        TO T5602-T5602-REC               
	*       IF T5602-INDIC-01 NOT = SPACE                             
	*          MOVE ZERO             TO SR680-SUMIN                   
	*          MOVE 'Y'              TO SR680-SUMIN-OUT (ND)          
	*                                   SR680-SUMIN-OUT (PR)          
	*       END-IF                                                    
	*    ELSE                                                         
	*       MOVE SPACE               TO WSAA-SUMFLAG.                 
	*9390-EXIT.                                                       
	*    EXIT.                                                        
	* </pre>
	*/
protected void vf2MaltPovr9400()
	{
		start9410();
	}

protected void start9410()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(wsaaPovrChdrcoy);
		povrIO.setChdrnum(wsaaPovrChdrnum);
		povrIO.setLife(wsaaPovrLife);
		povrIO.setCoverage(wsaaPovrCoverage);
		povrIO.setRider(wsaaPovrRider);
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setValidflag("2");
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

protected void checkLivesno10000()
	{
		start10010();
	}

protected void start10010()
	{
		wsaaHbnfLivesnoNum.set(ZERO);
		hbnfIO.setChdrcoy(chdrpf.getChdrcoy());
		hbnfIO.setChdrnum(chdrpf.getChdrnum());
		hbnfIO.setLife(covrpf.getLife());
		hbnfIO.setCoverage(covrpf.getCoverage());
		hbnfIO.setRider(covrpf.getRider());
		hbnfIO.setFunction(varcom.readr);
		callHbnf1950();
		if (isEQ(hbnfIO.getStatuz(),varcom.mrnf)) {
			hbnfIO.setRecNonKeyData(SPACES);
		}
		if (isNE(hbnfIO.getStatuz(),varcom.mrnf)) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
				if (isNE(hbnfIO.getClntnum(wsaaSub),SPACES)) {
					wsaaHbnfLivesnoNum.add(1);
				}
			}
		}
	}

protected void validateOccupationOrOccupationClass() 
	{
		readTa610();
		String occupation = lifemjaIO.getOccup().toString();
		if( occupation != null && !occupation.trim().isEmpty()) {
			for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
					&& isNE(ta610rec.occclassdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
				if(isEQ(occupation,ta610rec.occclassdes[wsaaCount.toInt()])){
					isFollowUpRequired = true;
					scrnparams.errorCode.set(errorsInner.rrsu);
					break;
				}
			}
			
			if(!isFollowUpRequired){
				getOccupationClass2900(occupation);
				for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
						&& isNE(ta610rec.occcode[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
					if(isEQ(wsaaOccupationClass,ta610rec.occcode[wsaaCount.toInt()])){
						isFollowUpRequired = true;
						scrnparams.errorCode.set(errorsInner.rrsu);
						break;
					}
				}	
			}
		}
	}

protected void getOccupationClass2900(String occupation) 
	{
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl(t3644);
	   itempf.setItemitem(occupation);
	   itempf = itempfDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
	   wsaaOccupationClass.set(t3644rec.occupationClass.toString());
	}

protected void readTa610() 
	{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(tA610);
	itempf.setItemitem(covtpf.getCrtable());//IJTI-1793
	itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
	itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
	itempfList  = itempfDAO.findByItemDates(itempf);	//ICIL-1494
	if (itempfList.size()>0 && itempfList.get(0).getGenarea()!=null) {
		ta610rec.tA610Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}
	}

protected void createFollowUps3400()
	{
		try {
			fluppfAvaList = fluppfDAO.searchFlupRecordByChdrNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());//IJTI-1410
			fupno = fluppfAvaList.size();	//ICIL-1494
			writeFollowUps3410();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void writeFollowUps3410()
	{
		for (wsaaCount.set(1); !(isGT(wsaaCount, 5))
				&& isNE(ta610rec.fupcdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
			writeFollowUp3500();
		}
		fluppfDAO.insertFlupRecord(fluppfList);
	}

protected void writeFollowUp3500()
	{
		try {
			fluppf = new Fluppf();
			lookUpStatus3510();
			lookUpDescription3520();
			writeRecord3530();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void lookUpStatus3510()
	{
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(ta610rec.fupcdes[wsaaCount.toInt()]);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
	
		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.e557);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}

protected void lookUpDescription3520()
	{
		fupDescpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	}

private void writeRecord3530() 
	{
		fupno++;
		fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
		fluppf.setChdrnum(chdrpf.getChdrnum());
		setPrecision(fupno, 0);
		fluppf.setFupNo(fupno);
		fluppf.setLife("01");
		fluppf.setTranNo(chdrpf.getTranno());
		fluppf.setFupDt(wsaaToday.toInt());
		fluppf.setFupCde(ta610rec.fupcdes[wsaaCount.toInt()].toString().trim());
		fluppf.setFupTyp('P');
		fluppf.setFupRmk(fupDescpf.getLongdesc());
		fluppf.setjLife("00");
		fluppf.setTrdt(varcom.vrcmDate.toInt());
		fluppf.setTrtm(varcom.vrcmTime.toInt());
		fluppf.setUserT(varcom.vrcmUser.toInt());
		fluppf.setEffDate(wsaaToday.toInt());
		fluppf.setCrtUser(varcom.vrcmUser.toString());
		fluppf.setCrtDate(wsaaToday.toInt());
		fluppf.setLstUpUser(varcom.vrcmUser.toString());
		fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
		fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
		fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
		fluppfList.add(fluppf);
	}

/*
 * Class transformed  from Data Structure WSAA-DEFAULTS--INNER
 */
private static final class WsaaDefaultsInner { 

	private FixedLengthStringData wsaaDefaults = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaDefaultRa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 0);
	private Validator defaultRa = new Validator(wsaaDefaultRa, "Y");
	private Validator nodefRa = new Validator(wsaaDefaultRa, "N");
	private FixedLengthStringData wsaaDefaultPa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 1);
	private Validator defaultPa = new Validator(wsaaDefaultPa, "Y");
	private Validator nodefPa = new Validator(wsaaDefaultPa, "N");
	private FixedLengthStringData wsaaDefaultBa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 2);
	private Validator defaultBa = new Validator(wsaaDefaultBa, "Y");
	private Validator nodefBa = new Validator(wsaaDefaultBa, "N");
	private FixedLengthStringData wsaaDefaultRt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 3);
	private Validator defaultRt = new Validator(wsaaDefaultRt, "Y");
	private Validator nodefRt = new Validator(wsaaDefaultRt, "N");
	private FixedLengthStringData wsaaDefaultPt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 4);
	private Validator defaultPt = new Validator(wsaaDefaultPt, "Y");
	private Validator nodefPt = new Validator(wsaaDefaultPt, "N");
	private FixedLengthStringData wsaaDefaultBt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 5);
	private Validator defaultBt = new Validator(wsaaDefaultBt, "Y");
	private Validator nodefBt = new Validator(wsaaDefaultBt, "N");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f335 = new FixedLengthStringData(4).init("F335");
	private FixedLengthStringData f404 = new FixedLengthStringData(4).init("F404");
	private FixedLengthStringData g616 = new FixedLengthStringData(4).init("G616");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData ev01 = new FixedLengthStringData(4).init("EV01");
	private FixedLengthStringData h033 = new FixedLengthStringData(4).init("H033");
	private FixedLengthStringData h044 = new FixedLengthStringData(4).init("H044");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData h039 = new FixedLengthStringData(4).init("H039");
	private FixedLengthStringData d028 = new FixedLengthStringData(4).init("D028");
	private FixedLengthStringData d029 = new FixedLengthStringData(4).init("D029");
	private FixedLengthStringData d030 = new FixedLengthStringData(4).init("D030");
	private FixedLengthStringData d352 = new FixedLengthStringData(4).init("D352");
	private FixedLengthStringData f862 = new FixedLengthStringData(4).init("F862");
	private FixedLengthStringData rl87 = new FixedLengthStringData(4).init("RL87");
	private FixedLengthStringData rl81 = new FixedLengthStringData(4).init("RL81");
	private FixedLengthStringData h113 = new FixedLengthStringData(4).init("H113");
	private FixedLengthStringData rl84 = new FixedLengthStringData(4).init("RL84");
	private FixedLengthStringData sc72 = new FixedLengthStringData(4).init("SC72");
	private FixedLengthStringData rl85 = new FixedLengthStringData(4).init("RL85");
	private FixedLengthStringData w067 = new FixedLengthStringData(4).init("W067");
	private FixedLengthStringData f646 = new FixedLengthStringData(4).init("F646");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t6625 = new FixedLengthStringData(5).init("T6625");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData tr687 = new FixedLengthStringData(5).init("TR687");
	private FixedLengthStringData tr686 = new FixedLengthStringData(5).init("TR686");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC   ");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData covrrgwrec = new FixedLengthStringData(10).init("COVRRGWREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData hbnfrec = new FixedLengthStringData(10).init("HBNFREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData lextrevrec = new FixedLengthStringData(10).init("LEXTREVREC");
	private FixedLengthStringData annyrec = new FixedLengthStringData(10).init("ANNYREC");
	private FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC   ");
	private FixedLengthStringData coexrec = new FixedLengthStringData(10).init("COEXREC   ");
}
}
