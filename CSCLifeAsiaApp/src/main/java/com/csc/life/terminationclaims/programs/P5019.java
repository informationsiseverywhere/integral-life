/*
 * File: P5019.java
 * Date: 29 August 2009 23:55:53
 * Author: Quipoz Limited
 * 
 * Class transformed from P5019.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.dao.TpdbpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Tpdbpf;
import com.csc.life.contractservicing.procedures.Totloan;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.procedures.Vpxmatc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxmatcrec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CovrClmDAO;
import com.csc.life.terminationclaims.dataaccess.dao.HmtdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.MatdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.MathpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.CovrClm;
import com.csc.life.terminationclaims.dataaccess.model.Hmtdpf;
import com.csc.life.terminationclaims.dataaccess.model.Matdpf;
import com.csc.life.terminationclaims.dataaccess.model.Mathpf;
import com.csc.life.terminationclaims.recordstructures.Matccpy;
import com.csc.life.terminationclaims.screens.S5019ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Maturity/Expiry details program.
* --------------------------------
*
* This  transaction,  Maturity/Expiry, is  selected   from  the
* Maturity/Expiry  Sub-menu  S5025/P5025.  This program matures
* or expires  an entire Plan  or  one  or many policies.  If it
* is   maturing  or expiring  many policies, it will be  called
* for each policy required.
*
*Initialise
*----------
*
* Read the  Contract  header  (Function  RETRV)  and  read  the
* relevant data necessary for obtaining the status description,
* the Owner,  the  Life-assured,  Joint-life, CCD, Paid-to-date
* and the Bill-to-date.
*
* RETRV the "Plan" or  the  policy  to  be  worked  on from the
* COVRMAT  I/O  module;  it was selected from program P5029 (or
* defaulted if only one  policy present). If the Plan suffix in
* the key "kept" is zero,  the  whole Plan is to  be matured or
* expired,  otherwise,  a  single   policy is  to be matured or
* expired.
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*      - READR  the   life  details  using  LIFEMAT  (for  this
*           contract  number,  joint life number '00').  Format
*           the name accordingly.
*      - READR the  joint-life  details using LIFEMAT (for this
*           contract  number,  joint life number '01').  Format
*           the name accordingly.
*
*
* Obtain the  necessary  descriptions  by  calling 'DESCIO' and
* output the descriptions of the statuses to the screen.
*
* Format Name
*
*      Read the  client  details  record  and  use the relevant
*           copybook in order to format the required names.
*
* Build the Coverage/Rider review details
*
*      If the entire Plan is being  matured  or  expired,  read
*           all the coverage/riders for  each policy within the
*           Plan. The literal "Plan Suffix  No:"  and  the Plan
*           Suffix   number  field   are  non-display  for  the
*           maturity or expiry of the entire Plan.
*
*      If only one policy  within  a  Plan  is being matured or
*           expired, read only the relevant coverage/riders.
*
*      N.B. the currency is decided by the  maturity subroutine
*           and passed back.
*
*      If all the returned  details  are  of  the same currency
*           throughout, then default  the currency field at the
*           bottom of the screen to the same currency.
*
*      For each coverage/rider attached to the policy:-
*
*           - call  the  maturity  calculation   subroutine  as
*                defined on  T5687.  This  method  is  used  to
*                access  T6598   which contains the subroutines
*                necessary   for the  maturity  calculation and
*                processing.
*
* Linkage area passed to the maturity calculation subroutine:-
*
*        - company
*        - contract header number
*        - suffix
*        - policy sum
*        - life number
*        - joint-life number
*        - coverage
*        - rider
*        - crtable
*        - crrcd
*        - ptdate
*        - effdate
*        - conv-units
*        - language
*        - estimated value
*        - actual value
*        - currency
*        - chdr-currency
*        - chdr-contract-type
*        - maturity-calc-meth
*        - pstatcode
*        - element code
*        - description
*        - single premium
*        - billing frequency
*        - type code
*        - virtual fund
*        - batckey
*        - status
*        - plan-switch
*
*      DOWHILE
*         maturity calculation subroutine status not = ENDP
*            - load the subfile record
*            - accumulate single currency if applicable
*            - call maturity calculation subroutine
*      ENDDO
*
*      maturity claim subroutines  may  work at Plan or policy
*           level,  therefore   the   DOWHILE  process  may  be
*           processed for end of  COVR  for a Plan or a Policy,
*           determine the type of maturity being processed.
*
*      If the  policy  selected is part of a summary, then  the
*           amount  returned  must be divided by "n" ("n" being
*           the  number of policies in the plan). Note:- If the
*           policy  selected  is not only part of a summary but
*           also  the  first policy from the summary, then  the
*           value  returned  is calculated as:  n - (n-1); this
*           caters for any rounding problems that may occur.
*
* Policy Loans
*
*      Loan Principal plus  Interest  calculated  to  effective
*      date of Maturity is deducted from the Maturity Value.
*
*      Get the details of all loans currently held against this
*      Contract. If this is not the  first component within the
*      current maturity transaction, then need to read the SURD
*      to get details of all previous maturity records for this
*      transaction.
*      If there are no records (there will  be  none  for  Unit
*      Linked) then call TOTLOAN to get the current loan value.
*      If there are records, then  sum  up  their values before
*      calling TOTLOAN and subtracting the total value from the
*      LOAN VALUE returned from TOTLOAN.
*      Previous maturities may have been done  in  a  different
*      currency to the present one and hence  a check should be
*      made for this and  a  conversion  done  where  necessary
*      before the accumulation  is  done.   Note  that, for the
*      initial screen display, the currency will always be CHDR
*      currency.
*      Note  also  that  TOTLOAN  always returns details in the
*      CHDR currency.
*
* Total Estimated Value
*
*      This amount  is  the  total of the estimated values from
*           the  coverage/rider  subfile review. This amount is
*           the  sum  of the individual amounts returned by the
*           subroutine. (If all the amounts returned are in the
*           same currency).
*
* Total Actual Value
*
*      This amount  is  the total of the actual values from the
*           coverage/rider  subfile review (i.e. the sum of the
*           individual amounts returned  by  the subroutine, if
*           all the amounts returned are  in the same currency)
*           together with the policy loans and manually entered
*           adjustments, initially zero.
*
* The above details are returned  to  the user and if he wishes
* to  continue and  perform the Full maturity routine he enters
* the required data  at  the bottom of the screen, otherwise he
* opts for 'KILL' to kill and exit from this transaction.
*
*Validation
*----------
*
* If 'KILL  was entered, then skip the remainder of the  valid-
* ation and exit from the program.
*
* Effective-Date(optional)
*
*      Check that the Effective-date entered  on  the screen is
*           not less than  the contract commencement date (CCD)
*           and  that  it is  not  greater  than  today's date.
*           Default is today's date.
*
* maturity adjustment reason (optional)
*
*      Validated by the I/O module.
*
* maturity adjustment reason description (optional)
*
*      Check whether an  adjustment  reason was entered or not.
*           If an  adjustment  reason  code  was entered and no
*           adjustment description was entered, then read T5500
*           and output the description found. This is displayed
*           if 'CALC' is   pressed  (redisplay).  If  a  reason
*           description was entered, the system will default to
*           use this value  instead  of accessing T5500 for the
*           description.
*
* Currency (optional)
*
*      Validated by the I/O module.
*      A blank entry defaults to the contract currency.
*
* Other adjustment amount
*
*      A value may or may not be entered
*
*      If 'CALC' is pressed, then the screen is redisplayed and
*           the adjusted  amount  is  included  with  the final
*           total amount, i.e.  if an adjusted amount exists.
*
*      If the    currency    code     was     changed,     then
*           re-calculate/convert all of the amounts held in the
*           subfile.
*
*Updating
*--------
*
* If the KILL  function  key  was  pressed,  skip  the updating
* and exit from the program.
*
* CREATE maturity CLAIMS HEADER RECORD.
*
* Create a  maturity  claim  header  record (MATHCLM) for  this
* contract and for this policy  (i.e.  a MATH record is created
* for  each policy) and  output  the  details  entered  on  the
* screen.  Keyed by Company, Contract number, Suffix.
*
* Keys:     - company
*           - contract number
*           - transaction
*           - suffix
* Data:     - life number
*           - joint-life number
*           - effective date
*           - currency
*           - policy loan amount
*           - other adjustments amount
*           - adjustment reason code
*           - adjustment reason description
*           - contract type
*           - estimate total amount
*           - actual total amount
*           - transaction date
*           - transaction time
*           - terminal id
*           - user id
*
* CREATE maturity CLAIMS DETAILS RECORD.
*
* DOWHILE there are records in the subfile
*
*      - create a MATDCLM record for each subfile entry
*        output the following fields, Keyed by:-
*        Company, Contract no, Suffix,
*        Coverage, Rider, Element type
*
* Keys:     - company
*           - contract number
*           - transaction
*           - suffix
*           - life number
*           - coverage
*           - rider
*           - line number
* Data:     - joint-life number
*           - element type (4 character code)
*           - description
*           - re-insurance indicator
*           - currency
*           - estimated value
*           - actual value
*           - type (1 character code) returned by maturity calc.
*           - virtual fund
*           - effective date
*           - transaction date
*           - transaction time
*           - terminal id
*           - user id
*
* ENDDO
*
*
*Next Program
*------------
*
* For 'KILL' or 'Enter', add 1 to the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5019 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5019");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	protected ZonedDecimalData wsaaSwitchPlan = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator wholePlan = new Validator(wsaaSwitchPlan, 1);
	private Validator partPlan = new Validator(wsaaSwitchPlan, 2);
	private Validator summaryPartPlan = new Validator(wsaaSwitchPlan, 3);

	protected ZonedDecimalData wsaaSwitchFirst = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator firstTime = new Validator(wsaaSwitchFirst, 1);

	protected ZonedDecimalData wsaaSwitchCurrency = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator detailsSameCurrency = new Validator(wsaaSwitchCurrency, 0);
	private Validator detailsDifferent = new Validator(wsaaSwitchCurrency, 1);
	private Validator totalCurrDifferent = new Validator(wsaaSwitchCurrency, 2);
	protected ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(0);
	protected FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
	protected FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaPremCurrency = new FixedLengthStringData(3).init(SPACES);
	protected ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	protected PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	protected FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaLineno = new ZonedDecimalData(2, 0).setUnsigned();
	protected ZonedDecimalData wsaaEligibleDate = new ZonedDecimalData(8, 0).setUnsigned();
	protected PackedDecimalData wsaaRiskCessDate = new PackedDecimalData(8, 0);
		/* WSAA-TRANSACTION-REC */
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4);
	protected PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaLoanValue = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaActvalue = new PackedDecimalData(17, 2).init(0);
		/* ERRORS */
	protected static final String e304 = "E304";
	private static final String f294 = "F294";
	protected static final String g588 = "G588";
	private static final String h034 = "H034";
	private static final String j008 = "J008";
	protected static final String rfik = "RFIK";
	
		/* TABLES */
	protected static final String t1688 = "T1688";
	private static final String t3000 = "T3000";
	protected static final String t5688 = "T5688";
	protected static final String t3623 = "T3623";
	protected static final String t3588 = "T3588";
	private static final String t5687 = "T5687";
	private static final String t6598 = "T6598";
	protected static final String t5500 = "T5500";
	
	protected ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	protected CovrmatTableDAM covrmatIO = new CovrmatTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	protected Matccpy matccpy = new Matccpy();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected T5687rec t5687rec = new T5687rec();
	protected T6598rec t6598rec = new T6598rec();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	protected Totloanrec totloanrec = new Totloanrec();
	protected Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	protected S5019ScreenVars sv = getLScreenVars();
	
	protected Clntpf clntpf = new Clntpf();
	protected ExternalisedRules er = new ExternalisedRules();
	protected List<Covrpf> covrpfList =new ArrayList<Covrpf>(); 
	protected Covrpf covrpf;
	protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private boolean wsaaEof;
	protected Iterator<Covrpf> iterator ; 
	protected Descpf descpf;
	protected DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private List<Lifepf> lifepfList =new ArrayList<Lifepf>(); 
	private Lifepf lifepf;
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	Iterator<Lifepf> iteratorLife ;
	protected ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected List<Itempf> itempfList;
	protected Itempf itempf = null;
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private List<Incrpf> incrpfList;
	private Itempf incrpf = null;
	protected MatdpfDAO matdpfDAO = getApplicationContext().getBean("matdpfDAO", MatdpfDAO.class);
	protected List<Matdpf> matdpfList;
	protected Matdpf matdpf = null;
	protected Iterator<Matdpf> iteratorMatd ; 
	protected boolean endOfFile;
	protected Chdrpf chdrpf;
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	
	//ILIFE-6593
	private HmtdpfDAO hmtdpfDAO = getApplicationContext().getBean("hmtdpfDAO", HmtdpfDAO.class);
	private TpdbpfDAO tpdbpfDAO = getApplicationContext().getBean("tpdbpfDAO", TpdbpfDAO.class);
	private MathpfDAO mathpfDAO = getApplicationContext().getBean("mathpfDAO", MathpfDAO.class);
	protected PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	protected Hmtdpf hmtdpf = null;
	protected Payrpf payrpf = null;
	protected Mathpf mathpf = new Mathpf();
	private List<Hmtdpf> hmtdpfList = null;
	private List<Mathpf> mathpfList = null;
	private List<Tpdbpf> tpdbpfList = new ArrayList<Tpdbpf>();

	private boolean flag = false;
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	protected S5019ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5019ScreenVars.class);
	}

/**
 * Contains all possible labels used by goTo action.
 */
	public enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		buildCovRider1030, 
		exit1090, 
		getNextComponent1456, 
		exit1509, 
		exit1839, 
		read1981, 
		exit2090, 
		exit3090
	}

	public P5019() {
		super();
		screenVars = sv;
		new ScreenModel("S5019", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		init1000();
		getLifeNoName1010();
		getJlifeNoName1020(); 
		buildCovRider1030();
		displayScreenfields();
	}

protected void displayScreenfields() {
		// TODO Auto-generated method stub
		
	}

protected void init1000()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		scrnparams.function.set(varcom.sclr);
		processScreen("S5019", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(ZERO);
		wsaaSwitchPlan.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		wsaaSwitchCurrency.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaRiskCessDate.set(ZERO);
		wsaaActualTot.set(ZERO);
		wsaaSwitchFirst.set(1);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaLife.set(SPACES);
		wsaaPremCurrency.set(SPACES);
		wsaaCrtable.set(SPACES);
		chdrpf = new Chdrpf();
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf){
		chdrmatIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmatIO);
		if (isNE(chdrmatIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmatIO.getParams());
			fatalError600();
		}
			else {
				chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsspcomn.company.toString(),chdrmatIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
			}
		} 
		covrpf = new Covrpf();
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf){
		covrmatIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmatIO.getParams());
			fatalError600();
		}
			else {
				covrpfList = covrpfDAO.SearchRecordsforMatd(wsspcomn.company.toString(),covrmatIO.getChdrnum().toString(),covrmatIO.getPlanSuffix().toInt());
				if(null==covrpfList) {
					fatalError600();
				}
				else {
					iterator = covrpfList.iterator();
					if(iterator.hasNext())
						covrpf = iterator.next();
				}
			}
		} 
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			wsaaSwitchPlan.set(1);
		}
		else {
			if (isGT(covrpf.getPlanSuffix(),chdrpf.getPolsum())
					|| isEQ(chdrpf.getPolsum(),1)) {
				wsaaSwitchPlan.set(2);
			}
			else {
				wsaaSwitchPlan.set(3);
			}
		}
		mathpf.setPlnsfx(covrpf.getPlanSuffix());
		payrpf = payrpfDAO.getpayrRecordByCoyAndNumAndSeq(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), covrpf.getPayrseqno(), "1");


		if (payrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(chdrpf.getChdrcoy().toString().concat(chdrpf.getChdrnum()).concat(covrpf.getPayrseqno()+"1"));
			fatalError600();
		}
		sv.estimateTotalValue.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.clamant.set(ZERO);

		descpf=descDAO.getdescData("IT", t1688, wsaaBatckey.batcBatctrcde.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (null == descpf) 
			sv.descrip.set("");
		else
			sv.descrip.set(descpf.getLongdesc());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		descpf=descDAO.getdescData("IT", t5688, chdrpf.getCnttype(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (null == descpf) 
			sv.ctypedes.set("");
		else
			sv.ctypedes.set(descpf.getShortdesc());
		descpf=descDAO.getdescData("IT", t3623, chdrpf.getStatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if(null == descpf)
			sv.rstate.set("");
		else
			sv.rstate.set(descpf.getShortdesc());
		descpf=descDAO.getdescData("IT", t3588, chdrpf.getPstcde(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if(null == descpf)
			sv.pstate.set("");
		else
			sv.pstate.set(descpf.getShortdesc());
		sv.occdate.set(chdrpf.getOccdate());
		sv.cownnum.set(chdrpf.getCownnum());
		clntpf.setClntnum(chdrpf.getCownnum());//IJTI-1410
		getClientName1200();
		if (isNE(clntpf.getValidflag(),1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.ptdate.set(chdrpf.getPtdate());
		sv.btdate.set(chdrpf.getBtdate());
		sv.effdate.set(wsspcomn.currfrom);
		
		
	}


protected void getLifeNoName1010()
	{
		lifepf = new Lifepf();
		lifepfList = lifepfDAO.getLifematRecords(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), covrpf.getLife());
		if(null == lifepfList)
			fatalError600();
		iteratorLife = lifepfList.iterator();
		if(iteratorLife.hasNext())
		{
			lifepf = iteratorLife.next();
			if(lifepf.getJlife() == "00" && lifepf.getJlife() == " "){
			fatalError600();
		}
			sv.lifcnum.set(lifepf.getLifcnum());
			clntpf.setClntnum(lifepf.getLifcnum());//IJTI-1410
		getClientName1200();
		if (isNE(clntpf.getValidflag(),1)) {
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
	}
	}
protected void getJlifeNoName1020()
	{
		if(iteratorLife.hasNext())
		{
			lifepf = iteratorLife.next();
			sv.jlifcnum.set(lifepf.getLifcnum());
			clntpf.setClntnum(lifepf.getLifcnum());//IJTI-1410
			getClientName1200();
			if(isNE(clntpf.getValidflag(),1)) {
				sv.jlinsnameErr.set(e304);
				sv.jlinsname.set(SPACES);
			}
			else {
				plainname();
				sv.jlinsname.set(wsspcomn.longconfname);
				}
		}
		else{
			sv.jlifcnum.set("*NONE");
			sv.jlinsname.set(SPACES);
			//goTo(GotoLabel.buildCovRider1030);
			
		}
	}

protected void buildCovRider1030()
	{
		checkForIncrease1700();
		datcon2rec.intDate1.set(wsspcomn.currfrom);
		datcon2rec.frequency.set("12");
		datcon2rec.freqFactor.set(3);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaEligibleDate.set(datcon2rec.intDate2);
		sv.planSuffix.set(covrpf.getPlanSuffix());
		if (wholePlan.isTrue()) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			processWholePlan1400();
		}
		else {
			processPolicy1800();
		}
		sv.policyloan.set(0);
		getLoanDetails1950();
		sv.policyloan.set(wsaaHeldCurrLoans);
		if (detailsSameCurrency.isTrue()) {
			sv.estimateTotalValue.set(wsaaEstimateTot);
			sv.clamant.set(wsaaActualTot);
			sv.clamant.subtract(sv.policyloan);
			sv.currcd.set(chdrpf.getCntcurr());
			sv.hcurrcd.set(chdrpf.getCntcurr());
		}
		else {
			wsaaPremCurrency.set(SPACES);
		}
		getPolicyDebt1980();
		sv.clamant.subtract(sv.tdbtamt);
		if (isLT(sv.clamant,0)) {
			sv.clamant.set(0);
		}
		if (isEQ(scrnparams.subfileRrn,ZERO)) {
			initializeSubfileRec1900();
			sv.coverage.set(SPACES);
			addSubfileRec1910();
		}
	}

protected void getDescription1100()
	{
		/*READ-DESC*/
		/*EXIT*/
	}

protected void getClientName1200()
	{
		
		   /*Ilife 3132 Starts */
		Clntpf clntData = new Clntpf();		
		ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);		
		clntData.setClntpfx("CN");
		clntData.setClntnum(chdrpf.getCownnum());//IJTI-1410
		clntData.setClntcoy(wsspcomn.fsuco.toString());
		 clntpf = clntpfDAO.selectClient(clntData);
		 /*Ilife 3132 Ends */
		
	}

protected void readTableT56871300()
	{
		itempfList=itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),t5687,wsaaCrtable.toString(),wsaaRiskCessDate.toInt());
		if(null == itempfList)

	{
			syserrrec.statuz.set(f294);
			fatalError600();
		}
		else
			t5687rec.t5687Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}

protected void readTableT65981310()
	{
		itempf = new Itempf();

		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t6598);
		itempf.setItemitem(t5687rec.maturityCalcMeth.toString());
		itempf = itempfDAO. getItempfRecord(itempf);
		if (null == itempf) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

protected void processWholePlan1400()
	{

	/*Ilife 3132 Starts */
CovrClm covrClmData = new CovrClm();		
CovrClmDAO covrClmDAO = getApplicationContext().getBean("covrClmDAO",CovrClmDAO.class);		
		covrClmData.setChdrcoy(covrpf.getChdrcoy());//IJTI-1410
		covrClmData.setChdrnum(covrpf.getChdrnum());//IJTI-1410
		covrClmData.setLife(covrpf.getLife());//IJTI-1410
		covrClmData.setCoverage(covrpf.getCoverage());//IJTI-1410
		covrClmData.setRider(covrpf.getRider());//IJTI-1410
covrClmData.setValidflag("1");
List<CovrClm> covrClmList = covrClmDAO.selectCovrClmCoverageData(covrClmData);


if(covrClmList.size()>0)
{
for (CovrClm covrClm: covrClmList) {
	wsaaChdrcoy.set(covrClm.getChdrcoy());
	wsaaChdrnum.set(covrClm.getChdrnum());
	
	//PROCESS COMPONENTS
	
	wsaaLife.set(covrClm.getLife());
	wsaaCoverage.set(covrClm.getCoverage());
	wsaaRider.set(covrClm.getRider());
	wsaaPremCurrency.set(covrClm.getPremCurrency());
	sv.currcd.set(wsaaPremCurrency);//ILIFE-4640	
	sv.hlife.set(covrClm.getLife());
	sv.hjlife.set(covrClm.getJlife());
	sv.coverage.set(covrClm.getCoverage());
	if (isEQ(covrClm.getRider(),"00")) {
		sv.rider.set(SPACES);
	}
	else {
		sv.rider.set(covrClm.getRider());
	}
	wsaaCrtable.set(covrClm.getCrtable());
	wsaaRiskCessDate.set(covrClm.getRiskCessDate());
	readTableT56871300();
	if (isEQ(t5687rec.maturityCalcMeth,SPACES)) {
		processExpiry1500(covrClm);
		//goTo(GotoLabel.getNextComponent1456);
	}
	readTableT65981310();
	matccpy.maturityRec.set(SPACES);
	matccpy.batckey.set(wsspcomn.batchkey);
	matccpy.chdrChdrcoy.set(covrClm.getChdrcoy());
	matccpy.chdrChdrnum.set(covrClm.getChdrnum());
	matccpy.planSuffix.set(ZERO);
				matccpy.polsum.set(chdrpf.getPolsum());
	matccpy.lifeLife.set(covrClm.getLife());
	matccpy.lifeJlife.set(covrClm.getJlife());
	matccpy.covrCoverage.set(covrClm.getCoverage());
	matccpy.covrRider.set(covrClm.getRider());
	matccpy.crtable.set(covrClm.getCrtable());
	matccpy.crrcd.set(covrClm.getCrrcd());
	matccpy.effdate.set(covrClm.getRiskCessDate());
	if (isEQ(t5687rec.singlePremInd,"Y")) {
		matccpy.billfreq.set("00");
	}
	else {
		matccpy.billfreq.set(payrpf.getBillfreq());
	}
				matccpy.chdrCurr.set(chdrpf.getCntcurr());
				matccpy.chdrType.set(chdrpf.getCnttype());
	if (isGT(covrClm.getInstprem(),ZERO)) {
		matccpy.singp.set(covrClm.getInstprem());
	}
	else {
		matccpy.singp.set(covrClm.getSingp());
	}
	matccpy.convUnits.set(covrClm.getConvertInitialUnits());
	matccpy.language.set(wsspcomn.language);
	matccpy.estimatedVal.set(ZERO);
	matccpy.actualVal.set(ZERO);
	matccpy.currcode.set(covrClm.getPremCurrency());
	matccpy.matCalcMeth.set(t5687rec.maturityCalcMeth);
	matccpy.pstatcode.set(covrClm.getPstatcode());
	matccpy.planSwitch.set(wsaaSwitchPlan);
	matccpy.status.set(varcom.oK);
	matccpy.endf.set(SPACES);
	while ( !(isEQ(matccpy.status,varcom.endp)
	|| isEQ(matccpy.status,"NETM"))) {
		callMatMethodWhole1600();
	}
	
}
	
	
	
}		

	}

/*Ilife 3132 Starts */


protected void processExpiry1500(CovrClm covrClm)
	{
		try {
			checkValidflag1500(covrClm);
			getDescription1500(covrClm);
		}
		catch (GOTOException e){
		}
	}

protected void checkValidflag1500(CovrClm covrClm)
	{
		if (isNE(covrClm.getValidflag(),"1")
		|| isGT(covrClm.getRiskCessDate(),wsaaEligibleDate)) {
			goTo(GotoLabel.exit1509);
		}
	}

protected void getDescription1500(CovrClm covrClm)
	{
		initializeSubfileRec1900();
		descpf=descDAO.getdescData("IT", t5687, covrClm.getCrtable(), covrClm.getChdrcoy(), wsspcomn.language.toString());//IJTI-1410
		if (null == descpf) {
			//	syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(h034);
			fatalError600();
		}
		if (isEQ(descpf.getShortdesc(),SPACES)) {
			sv.shortds.fill("?");
		}
		else {
			sv.shortds.set(descpf.getShortdesc());
		}
		sv.cnstcur.set(chdrpf.getCntcurr());
		sv.effdate.set(wsspcomn.currfrom);
		addSubfileRec1910();
	}


protected void callMatMethodWhole1600()
	{	
		//add VPMS code to redirect to LIFERUL 
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		Vpxmatcrec vpxmatcRec = new Vpxmatcrec();
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()) && er.isExternalized(matccpy.chdrType.toString(), matccpy.crtable.toString())) && isNE(t5687rec.maturityCalcMeth,SPACES))//ILIFE-6928 If there is no maturity method available in T5687 (TRM1) then it won't be allow to go inside this method.  
		{
			callProgram(t6598rec.calcprog, matccpy.maturityRec);
		}
		else
		{	
			vpmcalcrec.linkageArea.set(matccpy.maturityRec);		
			vpxmatcRec.function.set("INIT");
			callProgram(Vpxmatc.class, vpmcalcrec.vpmcalcRec,vpxmatcRec);	
			matccpy.maturityRec.set(vpmcalcrec.linkageArea);			
			if(isEQ(matccpy.fund,SPACES))
				return;	
			callProgram(t6598rec.calcprog, matccpy.maturityRec, vpxmatcRec);
		} 	
	
		if (isEQ(matccpy.status,varcom.bomb)) {
			syserrrec.statuz.set(matccpy.status);
			fatalError600();
		}
		if (isEQ(matccpy.status,"NETM")) {
			return ;
		}
		if (isEQ(matccpy.estimatedVal,ZERO)
		&& isEQ(matccpy.actualVal,ZERO)) {
			return ;
		}
		if (isEQ(matccpy.currcode,SPACES)) {
			sv.cnstcur.set(chdrpf.getCntcurr());
			sv.hcnstcur.set(chdrpf.getCntcurr());
		}
		else {
			sv.cnstcur.set(matccpy.currcode);
			sv.hcnstcur.set(matccpy.currcode);
		}
		sv.hcrtable.set(matccpy.crtable);
		sv.fieldType.set(matccpy.type);
		zrdecplrec.amountIn.set(matccpy.estimatedVal);
		callRounding5000();
		matccpy.estimatedVal.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(matccpy.actualVal);
		callRounding5000();
		matccpy.actualVal.set(zrdecplrec.amountOut);
		sv.estMatValue.set(matccpy.estimatedVal);
		sv.hemv.set(matccpy.estimatedVal);
		sv.actvalue.set(matccpy.actualVal);
		sv.hactval.set(matccpy.actualVal);
		sv.fund.set(matccpy.fund);
		sv.shortds.set(matccpy.description);
		sv.effdate.set(wsspcomn.currfrom);
		if (firstTime.isTrue()) {
			wsaaSwitchFirst.set(0);
			wsaaPremCurrency.set(sv.cnstcur);
			wsaaEstimateTot.add(matccpy.estimatedVal);
			wsaaActualTot.add(matccpy.actualVal);
		}
		else {
			if (isEQ(sv.cnstcur,wsaaPremCurrency)) {
				wsaaEstimateTot.add(matccpy.estimatedVal);
				wsaaActualTot.add(matccpy.actualVal);
			}
			else {
				wsaaEstimateTot.set(ZERO);
				wsaaActualTot.set(ZERO);
				wsaaSwitchCurrency.set(1);
			}
		}
		addSubfileRec1910();
	}

protected void checkForIncrease1700()
	{

		incrpfList = incrpfDAO.getIncrpfList(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());
		if(null != incrpfList && !incrpfList.isEmpty() )	//IJTI-462
			scrnparams.errorCode.set(j008);	//IJTI-462
	}

protected void processPolicy1800()
	{
		/*RETRIEVE-FIRST-POLICY*/
		wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		if (summaryPartPlan.isTrue()) {
			wsaaPlanSuffix.set(ZERO);
		}
		while ( !(wsaaEof)) {
			processPartComponents1810();
		}
		
		/*EXIT*/
	}

protected void processPartComponents1810()
	{
					processMaturityCalc1810();
					readNextCovRider1812();
				}

protected void processMaturityCalc1810()
	{
		sv.hlife.set(covrpf.getLife());
		sv.hjlife.set(covrpf.getJlife());
		sv.coverage.set(covrpf.getCoverage());
		if (isEQ(covrpf.getRider(),"00")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(covrpf.getRider());
		}
		wsaaCrtable.set(covrpf.getCrtable());
		wsaaRiskCessDate.set(covrpf.getRiskCessDate());
		readTableT56871300();
		if (isEQ(t5687rec.maturityCalcMeth,SPACES)) {
			processExpiry1830();
			return ;
		}
		readTableT65981310();
		sv.cnstcur.set(covrpf.getPremCurrency());
		sv.hcnstcur.set(covrpf.getPremCurrency());
		matccpy.maturityRec.set(SPACES);
		matccpy.batckey.set(wsspcomn.batchkey);
		matccpy.chdrChdrcoy.set(covrpf.getChdrcoy());
		matccpy.chdrChdrnum.set(covrpf.getChdrnum());
		matccpy.planSuffix.set(covrpf.getPlanSuffix());
		matccpy.polsum.set(chdrpf.getPolsum());
		matccpy.lifeLife.set(covrpf.getLife());
		matccpy.lifeJlife.set(covrpf.getJlife());
		matccpy.covrCoverage.set(covrpf.getCoverage());
		matccpy.covrRider.set(covrpf.getRider());
		matccpy.crtable.set(covrpf.getCrtable());
		matccpy.crrcd.set(covrpf.getCrrcd());
		matccpy.ptdate.set(chdrpf.getPtdate());
		matccpy.effdate.set(covrpf.getRiskCessDate());
		if (isEQ(t5687rec.singlePremInd,"Y")) {
			matccpy.billfreq.set("00");
		}
		else {
			matccpy.billfreq.set(payrpf.getBillfreq());
		}
		matccpy.chdrCurr.set(chdrpf.getCntcurr());
		matccpy.chdrType.set(chdrpf.getCnttype());
		if (isGT(covrpf.getInstprem(),0)) {
			matccpy.singp.set(covrpf.getInstprem());
		}
		else {
			matccpy.singp.set(covrpf.getSingp());
		}
		matccpy.convUnits.set(covrpf.getConvertInitialUnits());
		matccpy.language.set(wsspcomn.language);
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		matccpy.currcode.set(covrpf.getPremCurrency());
		matccpy.matCalcMeth.set(t5687rec.maturityCalcMeth);
		matccpy.pstatcode.set(covrpf.getPstatcode());
		matccpy.planSwitch.set(wsaaSwitchPlan);
		matccpy.status.set(varcom.oK);
		while ( !(isEQ(matccpy.status,varcom.endp))) {
			callMatCalcProg1820();
		}
		
	}

protected void readNextCovRider1812()
	{
		if(iterator.hasNext()){
			covrpf = iterator.next();
		}
		else
			wsaaEof = true;
	}

protected void callMatCalcProg1820()
	{
			callCalcProg1820();
		}

protected void callCalcProg1820()
	{
		//add VPMS code to redirect to LIFERUL 
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		Vpxmatcrec vpxmatcRec = new Vpxmatcrec();
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()) && er.isExternalized(matccpy.chdrType.toString(), matccpy.crtable.toString()))) 
		{
			callProgram(t6598rec.calcprog, matccpy.maturityRec);
		}
		else
		{	
			vpmcalcrec.linkageArea.set(matccpy.maturityRec);		
			vpxmatcRec.function.set("INIT");
			callProgram(Vpxmatc.class, vpmcalcrec.vpmcalcRec,vpxmatcRec);	
			matccpy.maturityRec.set(vpmcalcrec.linkageArea);			
			if(isEQ(matccpy.fund,SPACES))
				return;	
			callProgram(t6598rec.calcprog, matccpy.maturityRec, vpxmatcRec);
		} 	
		if (isEQ(matccpy.status,varcom.bomb)) {
			syserrrec.statuz.set(matccpy.status);
			fatalError600();
		}
		if (isEQ(matccpy.status,"NETM")) {
			matccpy.status.set(varcom.endp);
			return ;
		}
		if (isEQ(matccpy.estimatedVal,ZERO)
		&& isEQ(matccpy.actualVal,ZERO)) {
			return ;
		}
		sv.hcrtable.set(matccpy.crtable);
		if (isEQ(matccpy.currcode,SPACES)) {
			sv.cnstcur.set(chdrpf.getCntcurr());
			sv.hcnstcur.set(chdrpf.getCntcurr());
		}
		else {
			sv.cnstcur.set(matccpy.currcode);
			sv.hcnstcur.set(matccpy.currcode);
		}
		sv.fieldType.set(matccpy.type);
		sv.estMatValue.set(matccpy.estimatedVal);
		sv.hemv.set(matccpy.estimatedVal);
		sv.actvalue.set(matccpy.actualVal);
		sv.hactval.set(matccpy.actualVal);
		sv.estMatValue.set(matccpy.estimatedVal);
		sv.hemv.set(matccpy.estimatedVal);
		sv.actvalue.set(matccpy.actualVal);
		sv.hactval.set(matccpy.actualVal);
		sv.fund.set(matccpy.fund);
		sv.shortds.set(matccpy.description);
		if (firstTime.isTrue()) {
			wsaaSwitchFirst.set(ZERO);
			wsaaPremCurrency.set(sv.cnstcur);
			wsaaEstimateTot.add(sv.estMatValue);
			wsaaActualTot.add(sv.actvalue);
		}
		else {
			if (isEQ(matccpy.currcode,wsaaPremCurrency)) {
				wsaaEstimateTot.add(sv.estMatValue);
				wsaaActualTot.add(sv.actvalue);
			}
			else {
				wsaaEstimateTot.set(ZERO);
				wsaaActualTot.set(ZERO);
				wsaaSwitchCurrency.set(1);
			}
		}
		addSubfileRec1910();
	}

protected void processExpiry1830()
	{
		try {
			checkValidflag1830();
			getDescription1830();
		}
		catch (GOTOException e){
		}
	}

protected void checkValidflag1830()
	{
		if (isNE(covrpf.getValidflag(),"1")
				|| isGT(covrpf.getRiskCessDate(),wsaaEligibleDate)) {
			goTo(GotoLabel.exit1839);
		}
	}

protected void getDescription1830()
	{
		initializeSubfileRec1900();
		descpf=descDAO.getdescData("IT", t5687, covrpf.getCrtable(), covrpf.getChdrcoy(), wsspcomn.language.toString());
		if (null == descpf) {
			scrnparams.errorCode.set(h034);
			//syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descpf.getShortdesc(),SPACES)) {
			sv.shortds.fill("?");
		}
		else {
			sv.shortds.set(descpf.getShortdesc());
		}
		sv.cnstcur.set(chdrpf.getCntcurr());
		sv.effdate.set(wsspcomn.currfrom);
		addSubfileRec1910();
	}

protected void initializeSubfileRec1900()
	{
		/*MOVE-ZERO-SPACES*/
		sv.estMatValue.set(ZERO);
		sv.hemv.set(ZERO);
		sv.actvalue.set(ZERO);
		sv.hactval.set(ZERO);
		sv.fieldType.set(SPACES);
		sv.shortds.set(SPACES);
		sv.fund.set(SPACES);
		sv.cnstcur.set(SPACES);
		sv.hcnstcur.set(SPACES);
		matccpy.status.set(varcom.endp);
		/*EXIT*/
	}

protected void addSubfileRec1910()
	{
		/*ADD-RECORD*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5019", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void getLoanDetails1950()
	{
		endOfFile = false;

		wsaaLoanValue.set(0);
		matdpfList = matdpfDAO.getmatdClmRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if(null == matdpfList){
				fatalError600();
			}
		iteratorMatd = matdpfList.iterator();
		if(iteratorMatd.hasNext())
			matdpf = iteratorMatd.next();
		else
			endOfFile = true;
		while (!(endOfFile)) {
			if (isNE(matdpf.getCurrcd(),chdrpf.getCntcurr())) {
					readjustMatd1970();
				}
				else {
				wsaaActvalue.set(matdpf.getActvalue());
				}
				wsaaLoanValue.add(wsaaActvalue);
			if(iteratorMatd.hasNext())
				matdpf = iteratorMatd.next();
			else
				endOfFile = true;
		}
		
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy());
		totloanrec.chdrnum.set(chdrpf.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.language.set(wsspcomn.language);
		callProgram(Totloan.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal,totloanrec.interest));
		if (isLT(wsaaLoanValue,wsaaHeldCurrLoans)) {
			wsaaHeldCurrLoans.subtract(wsaaLoanValue);
		}
		else {
			wsaaHeldCurrLoans.set(0);
		}
	}

protected void readjustMatd1970()
	{

		if (isEQ(matdpf.getActvalue(),ZERO)) {
			wsaaActvalue.set(ZERO);
			return ;
		}
		conlinkrec.function.set("SURR");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(matdpf.getCurrcd());
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(chdrpf.getCntcurr());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(matdpf.getActvalue());
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		wsaaActvalue.set(conlinkrec.amountOut);
	}

protected void getPolicyDebt1980()
	{
	  sv.tdbtamt.set(ZERO);
	  tpdbpfList = tpdbpfDAO.getTpoldbtAll(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
	  if(tpdbpfList == null || tpdbpfList.size() == 0) 
		  return;
	  for(Tpdbpf tpdbpf : tpdbpfList) {
		  sv.tdbtamt.add(tpdbpf.getTdbtamt().doubleValue());
	  }
	}



protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(clntpf.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{			
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsaaBatckey.batcBatctrcde,"T539")) {
			sv.currcdOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.currcdOut[varcom.nd.toInt()].set("N");
		}
		if (isLT(wsaaActualTot,ZERO)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.subfileRrn.set(1);
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			validateSelectionFields2070();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,"CALC")
		&& isEQ(sv.currcd,SPACES)) {
			sv.currcd.set(chdrpf.getCntcurr());
		}
		chkValidCntcurr2100();
		if (null == itempfList) {
			sv.currcdErr.set(g588);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.currcd,SPACES)) {
			sv.currcd.set(chdrpf.getCntcurr());
			sv.hcurrcd.set(chdrpf.getCntcurr());
		}
		if (isNE(sv.currcd,sv.hcurrcd)) {
			sv.hcurrcd.set(sv.currcd);
			wsaaSwitchCurrency.set(0);
			wsspcomn.edterror.set("Y");
			readjustCurrencies2200();
		}
		if (isNE(sv.otheradjst, ZERO)) {
			zrdecplrec.amountIn.set(sv.otheradjst);
			callRounding5000();
			if (isNE(zrdecplrec.amountOut, sv.otheradjst)) {
				sv.otheradjstErr.set(rfik);
			}
		}
		if (isNE(wsaaEstimateTot,ZERO)) {
			compute(sv.estimateTotalValue, 2).set(add(wsaaEstimateTot,sv.otheradjst));
		}
		else {
			compute(sv.clamant, 2).set(add(sub(wsaaActualTot,sv.policyloan),sv.otheradjst));
		}
		sv.clamant.subtract(sv.tdbtamt);
		if (isLT(sv.clamant,0)) {
			sv.clamant.set(0);
		}
		if (isNE(sv.reasoncd,SPACES)) {
			if (isEQ(sv.resndesc,SPACES)) {
				scrnparams.statuz.set("CALC");
				descpf=descDAO.getdescData("IT", t5500, sv.reasoncd.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
				sv.resndesc.set(descpf.getLongdesc());
			}
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSelectionFields2070()
	{
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void chkValidCntcurr2100()
	{
		itempfList=itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),t3000,sv.currcd.toString(),wsspcomn.currfrom.toInt());


		sv.currcd.set(SPACES);//ILIFE-4640
	}

protected void readjustCurrencies2200()
	{
		/*READ-FIRST-SUBFILE-REC*/
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5019", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			readSubfile2300();
		}
		
		if (isNE(scrnparams.errorCode,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		convertPolicyloan2500();
		/*EXIT*/
	}

protected void readSubfile2300()
	{
		checkOriginalCurrency2300();
		updateErrorIndicators2370();
		readNextRecord2390();
				
	}

protected void checkOriginalCurrency2300()
	{
		if (isEQ(wsaaPremCurrency,sv.currcd)) {
			sv.cnstcur.set(sv.hcnstcur);
			sv.estMatValue.set(sv.hemv);
			sv.actvalue.set(sv.hactval);
			wsaaEstimateTot.add(sv.hemv);
			wsaaActualTot.add(sv.hactval);
			return ;
		}
		if (isEQ(wsaaPremCurrency,SPACES)) {
			if (isEQ(sv.hcnstcur,sv.currcd)) {
				sv.cnstcur.set(sv.hcnstcur);
				sv.estMatValue.set(sv.hemv);
				sv.actvalue.set(sv.hactval);
				wsaaEstimateTot.add(sv.hemv);
				wsaaActualTot.add(sv.hactval);
				return ;
			}
		}
		if (isNE(sv.hemv,ZERO)) {
			conlinkrec.amountIn.set(sv.hemv);
			conlinkrec.function.set("CVRT");
			callXcvrt2400();
			sv.estMatValue.set(conlinkrec.amountOut);
			wsaaEstimateTot.add(conlinkrec.amountOut);
		}
		/* Convert the actual value if it is non zero.*/
		if (isNE(sv.hactval,ZERO)) {
			conlinkrec.amountIn.set(sv.hactval);
			if (isEQ(sv.fieldType,"C")) {
				conlinkrec.function.set("SURR");
			}
			else {
				conlinkrec.function.set("CVRT");
			}
			callXcvrt2400();
			sv.actvalue.set(conlinkrec.amountOut);
			wsaaActualTot.add(conlinkrec.amountOut);
		}
		sv.cnstcur.set(conlinkrec.currOut);
	}

protected void updateErrorIndicators2370()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5019", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextRecord2390()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5019", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callXcvrt2400()
	{

		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(sv.hcnstcur);
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			scrnparams.errorCode.set(conlinkrec.statuz);
			wsspcomn.edterror.set("Y");
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		callRounding5000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void convertPolicyloan2500()
	{

		conlinkrec.function.set("CVRT");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(chdrpf.getCntcurr());
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(wsaaHeldCurrLoans);
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		callRounding5000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		sv.policyloan.set(conlinkrec.amountOut);
	}

protected void update3000()
	{
		try {
			updateDatabase3000();
			readSubfile3070();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3000()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}

	}


public Integer getCurrentBusinessDate() {
	final Datcon1rec datcon1rec = new Datcon1rec();
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	return datcon1rec.intDate.toInt();
   } 




protected void readSubfile3070()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5019", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		compute(wsaaTranno, 0).set(add(chdrpf.getTranno(),1));
		wsaaLineno.set(0);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			readSubfile3100();
		}
		
		createMathHeader3400();
	}

protected void readSubfile3100()
	{
		/*READ-NEXT-RECORD*/
		createMatdDetail3300();
		scrnparams.function.set(varcom.srdn);
		processScreen("S5019", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void createMatdDetail3300()
	{
		matdpf = new Matdpf();

		wsaaLineno.add(1);
		matdpf.setChdrcoy(wsspcomn.company.toString());
		matdpf.setChdrnum(chdrpf.getChdrnum());//IJTI-1410
		matdpf.setTranno(wsaaTranno.toInt());
		matdpf.setPlnsfx(sv.planSuffix.toInt());
		matdpf.setCoverage(sv.coverage.toString());
		if (isEQ(sv.rider,SPACES)) {
			matdpf.setRider("00");
		}
		else {
			matdpf.setRider(sv.rider.toString());
		}
		matdpf.setCrtable(sv.hcrtable.toString());
		matdpf.setLife(sv.hlife.toString());
		matdpf.setJlife(sv.hjlife.toString());
		matdpf.setShortds(sv.shortds.toString());
		matdpf.setCurrcd(sv.cnstcur.toString());
		matdpf.setEmv(sv.estMatValue.getbigdata());
		matdpf.setActvalue(sv.actvalue.getbigdata());
		matdpf.setType_t(sv.fieldType.toString());
		matdpf.setVrtfund(sv.fund.toString());
		matdpf.setEffdate(sv.effdate.toInt());
		matdpf.setLiencd(wsaaLineno.toString());
		matdpf.setTrdt(wsaaTransactionDate.toInt());
		matdpf.setTrtm(wsaaTransactionTime.toInt());
		matdpf.setUser_t(wsaaUser.toInt());
		matdpf.setTermid(wsaaTermid.toString());
		matdpfDAO.insertMatdpfRecord(matdpf);

		hmtdpf = new Hmtdpf();
		hmtdpfList = new ArrayList();
		hmtdpf.setChdrcoy(matdpf.getChdrcoy());
		hmtdpf.setChdrnum(matdpf.getChdrnum());
		hmtdpf.setLife(matdpf.getLife());
		hmtdpf.setCoverage(matdpf.getCoverage());
		hmtdpf.setRider(matdpf.getRider());
		hmtdpf.setPlnsfx(matdpf.getPlnsfx());
		hmtdpf.setCrtable(matdpf.getCrtable());
		hmtdpf.setJlife(matdpf.getJlife());
		hmtdpf.setTranno(matdpf.getTranno());
		hmtdpf.setHactval(sv.hactval.getbigdata());
		hmtdpf.setHemv(sv.hemv.getbigdata());
		//hmtdpf.setHcnstcur(sv.hcnstcur.toString());
		hmtdpf.setHcnstcur(matdpf.getCurrcd());//ILIFE-8476 IJTI-1410
		hmtdpf.setType_t(sv.fieldType.toString());
        
		hmtdpfList.add(hmtdpf);
		flag = hmtdpfDAO.insertHmtdpf(hmtdpfList);

		if (!flag) {
			syserrrec.params.set(matdpf.getChdrcoy().concat(matdpf.getChdrnum()));
			fatalError600();
		}
	}

protected void createMathHeader3400()
	{
		mathpf = new Mathpf();
		mathpfList = new ArrayList<Mathpf>();
		mathpf.setChdrcoy(wsspcomn.company.toString());
		mathpf.setChdrnum(chdrpf.getChdrnum());
		mathpf.setTranno(wsaaTranno.toInt());
		mathpf.setPlnsfx(sv.planSuffix.toInt());
		mathpf.setLife(sv.hlife.toString());
		mathpf.setJlife(sv.hjlife.toString());
		mathpf.setEffdate(sv.effdate.toInt());
		mathpf.setCurrcd(sv.currcd.toString());
		/* MOVE ZEROES                 TO MATHCLM-POLICYLOAN.           */
		mathpf.setPolicyloan(BigDecimal.valueOf(sv.policyloan.toDouble()));
		mathpf.setTdbtamt(BigDecimal.valueOf(sv.tdbtamt.toDouble()));
		mathpf.setOtheradjst(BigDecimal.valueOf(sv.otheradjst.toDouble()));
		mathpf.setReasoncd(sv.reasoncd.toString());
		mathpf.setResndesc(sv.resndesc.toString());
		mathpf.setCnttype(chdrpf.getCnttype());
		mathpf.setClamamt(BigDecimal.valueOf(sv.clamant.toDouble()));
		mathpf.setEstimtotal(BigDecimal.valueOf(sv.estimateTotalValue.toDouble()));
		mathpf.setTrdt(wsaaTransactionDate.toInt());
		mathpf.setTrtm(wsaaTransactionTime.toInt());
		mathpf.setUser_t(wsaaUser.toInt());
		mathpf.setTermid(wsaaTermid.toString());
		mathpf.setZrcshamt(BigDecimal.ZERO);
        mathpfList.add(mathpf);
        flag = false;
        flag = mathpfDAO.insertMathpf(mathpfList);
		if (!flag) {
			syserrrec.params.set(mathpf.getChdrcoy().concat(mathpf.getChdrnum()));
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
		/*EXIT*/
	}

protected void callRounding5000()

	{
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		if (isNE(sv.currcd, SPACES)) {
			zrdecplrec.currency.set(sv.currcd);
		}
		else {
			zrdecplrec.currency.set(sv.cnstcur);
		}
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
	}

}