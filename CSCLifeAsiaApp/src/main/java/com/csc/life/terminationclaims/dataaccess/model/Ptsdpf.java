package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;

public class Ptsdpf {
	private long uniqueNumber;
	private String chdrcoy;
    private String chdrnum;
    private int tranno;
    private String life;
    private String jlife;
    private Long effdate;
    private String currcd;
    private BigDecimal actvalue;
    private BigDecimal percreqd;
    private BigDecimal planSuffix;
    private String fieldType;
    private String coverage;
    private String rider;
    private String crtable;
    private String shortds;
    private String liencd;
    private BigDecimal estMatValue;
    private String virtualFund;
    private String userProfile;
    private String jobName;
    private String datime;
    
	public String getCurrcd() {
		return currcd;
	}
	public void setCurrcd(String currcd) {
		this.currcd = currcd;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public Long getEffdate() {
		return effdate;
	}
	public void setEffdate(Long effdate) {
		this.effdate = effdate;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public BigDecimal getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(BigDecimal planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public BigDecimal getActvalue() {
		return actvalue;
	}
	public void setActvalue(BigDecimal actvalue) {
		this.actvalue = actvalue;
	}
	public BigDecimal getPercreqd() {
		return percreqd;
	}
	public void setPercreqd(BigDecimal percreqd) {
		this.percreqd = percreqd;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getShortds() {
		return shortds;
	}
	public void setShortds(String shortds) {
		this.shortds = shortds;
	}
	public String getLiencd() {
		return liencd;
	}
	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}
	public BigDecimal getEstMatValue() {
		return estMatValue;
	}
	public void setEstMatValue(BigDecimal estMatValue) {
		this.estMatValue = estMatValue;
	}
	public String getVirtualFund() {
		return virtualFund;
	}
	public void setVirtualFund(String virtualFund) {
		this.virtualFund = virtualFund;
	}

    
    
}
