/*
 * File: P5256.java
 * Date: 30 August 2009 0:22:32
 * Author: Quipoz Limited
 * 
 * Class transformed from P5256.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.dao.TpdbpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Tpdbpf;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
/*BRD-140*/
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CpbnfypfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Cpbnfypf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.recordstructures.Mrtdc01rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
//ILB-459 start
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
/*BRD-140*/
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmhpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClnnpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.CrsvpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.InvspfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmdpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmhpf;
import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.csc.life.terminationclaims.dataaccess.model.Crsvpf;
import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.life.terminationclaims.recordstructures.Dthclmflxrec;
import com.csc.life.terminationclaims.screens.S5256ScreenVars;
import com.csc.life.terminationclaims.screens.Sd5jlScreenVars;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UlnkpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZutrpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Ulnkpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;//ILB-459 
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
//ILB-459
//ILB-459 end

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Claims Registration.
*
*  This transaction, Claims  Registration,  is selected from
*  the Claims sub-menu S6319/P6319.
*
*     Initialise
*     ----------
*
*  Read the  Contract  header  (function  RETRV)  and  read
*  the relevant data necessary for obtaining the status
*  description, the Owner,  the  Life-assured,  Joint-life,
*  CCD, Paid-to-date and the Bill-to-date.
*
*  RETRV  the  "dead"  life to be worked on from the LIFECLM
*  I/O module, it was  selected  (or  defaulted  if  only
*  one  life present)  from  program P5247. For the life
*  selected output a highlighted  'asterisk'  to  the
*  corresponding  life  on  the screen.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To obtain the life assured and joint-life details (if any)
*  do the following;-
*
*  - READR  the   life  details  using  LIFECLM  (for  this
*    contract  number,  joint life number '00').  Format
*    the name accordingly.
*
*  - READR the  joint-life  details using LIFECLM (for this
*    contract  number,  joint life number '01').  Format the
*    name accordingly.
*
*
*  Obtain the  necessary  descriptions  by  calling 'DESCIO'
*  and output the descriptions of the statuses to the screen.
*
*  Format Name
*
*  Read the  client  details  record  and  use the relevant
*  copybook in order to format the required names.
*
*  Build the Coverage/Rider review details.
*
*  Only read  the   coverage/rider  details  (logical  view
*  COVRCLM)  for  the  "dead"  life  (N.B.  components
*  attaching to a single  life  within  a  joint  life
*  contract). In order  to  calculate  the death claim
*  value, call the generic subroutines applicable.
*
*  N.B. the currency is decided by the surrender subroutine
*  and passed back.
*
*  If all  the  returned  funds  are  of  the same currency
*  throughout,  then default the currency field at the
*  bottom of the screen to the same currency.
*
*  For each coverage/rider attached to the life/joint-life
*
*  - call the  death calculation subroutine as defined on
*    T5687. The death calculation  method on T5687 is
*    used  to  access  T6598,  which  contains  the
*    subroutines for the  death  calculation, death
*    processing  and  if   necessary   the  trigger
*    (additional) processing.
*
*  Read   the   re-insurance  detail  record  (RACT)    and
*  if re-insurance  details  are  found,  output  a  'Y' to
*  the R/I indicator.
*
*  Linkage area passed to the death calculation subroutine:-
*
*             - company
*             - contract header number
*             - life number
*             - joint-life number
*             - coverage
*             - rider
*             - crtable
*             - language
*             - estimated value
*             - actual value
*             - currency
*             - element code
*             - description
*             - type code
*             - status
*
*           DOWHILE
*              death calculation subroutine status not = ENDP
*                 - load the subfile record
*                 - accumulate single currency if applicable
*                 - call death calculation subroutine
*           ENDDO
*
*  Claim subroutines  must  work  at  Plan level, therefore
*  only do  the  DOWHILE process for end of COVR for a
*  Plan.
*
*  Policy Loans
*
*  Call TOTLOAN to get the value of any loans currently held
*  against this contract.
*
*  Note that TOTLOAN always returns details in the CHDR
*  currency.
*
*  Total Estimated value
*
*  This amount  is  the  total of the estimated values from
*  the  coverage/rider  subfile review. This amount is
*  the  sum  of the individual amounts returned by the
*  subroutine. (If all the amounts returned are in the
*  same currency).
*
*  Total Actual value
*
*  This amount  is  the total of the actual values from the
*  coverage/rider  subfile review (i.e. the sum of the
*  individual amounts returned  by  the subroutine, if
*  all the amounts returned are  in the same currency)
*  together with the policy loans and manually entered
*  adjustments, initially zero.
*
*  Follow-ups
*
*  Check the  follow-up  physical  file  FLUPCLM  for  this
*  contract and if  there  are  any follow-ups at all,
*  put a '+' in the follow-up selection field.
*
*  The above details are returned  to  the user and if he
*  wishes to continue and  perform  the  Claims Registration
*  routine he enters the  required  data  at  the  bottom  of
*  the  screen, otherwise he opts  for 'KILL' to exit from
*  this transaction.
*
*     Validation
*     ----------
*
*  Skip  this  section  if  returning from an optional
*  selection (current stack position action flag = '*').
*
*  If 'KILL'  was entered, then skip the remainder of the
*  validation and exit from the program.
*
*  Date of Death (optional)
*
*  Check that the Date-of-Death  entered  on  the screen is
*  not less than  the contract commencement date (CCD)
*  and that is not greater than today's date.
*
*  Effective Date
*
*  Check that the Effective-date entered  on  the screen is
*  not less  than the contract commencement date (CCD)
*  and not less than the Date-of-death (if entered).
*
*  Cause-of-Death (optional)
*
*  Validated by the I/O module.
*
*  Adjustment reason (optional)
*
*  Validated by the I/O module.
*
*  Adjustment reason description (optional)
*
*  Check whether an  adjustment  reason was entered or not.
*  If an  adjustment  reason  code  was entered and no
*  adjustment description was entered, then read T5500
*  and output the description found. This is displayed
*  if  'CALC' is  pressed  (redisplay).  If  a  reason
*  description was entered, the system will default to
*  use this value  instead  of accessing T5500 for the
*  description.
*
*  Currency (optional)
*
*  Validated by the I/O module.
*
*  A blank entry defaults to the contract currency.
*
*  Other adjustment amount
*
*  A value may or may not be entered
*
*  If 'CALC' is pressed,  the screen is re-displayed and
*  the adjusted  amount  is  included  with  the final
*  total amount, i.e.  if an adjusted amount exists.
*
*  If the currency code was changed,then
*  re-calculate/convert all of the amounts held in the
*  subfile.
*
*     Updating
*     --------
*
*  If the  'KILL'  function  key  was  pressed,  skip
*  the updating and exit from the program. Also skip if
*  follow-up selected.
*
*  CREATE CLAIMS HEADER RECORD.
*
*  Create a claim  header record (CLMHCLM) for this contract
*  and output the details entered on the  screen.  Keyed by
*  Company, Contract number.
*
*                - company
*                - contract number
*                - transaction
*                - life number
*                - joint-life number
*                - date of death
*                - effective date
*                - cause of death
*                - follow-ups
*                - currency
*                - policy loan amount
*                - other adjustments amount
*                - adjustment reason code
*                - adjustment reason description
*
*  CREATE CLAIMS DETAILS RECORD.
*
*  DOWHILE there are records in the subfile
*
*  - create a CLMDCLM record for each subfile entry - output
*    the following fields, Keyed by:-
*  Company, Contract no, Coverage, Rider, Element type
*
*                - company
*                - contract number
*                - transaction
*                - life number
*                - joint-life number
*                - coverage
*                - rider
*                - element type (4 character code)
*                - description
*                - lien code
*                - re-insurance indicator
*                - currency
*                - estimated value
*                - actual value
*                - type (1 character code)
*
*  ENDDO
*
*  Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
*  Call the AT  submission module to submit P5256AT, passing
*  the Contract  number, as the 'primary  key',  this
*  performs  the Claims Registration transaction.
*
*  Next Program
*  ------------
*
*  For KILL or 'Enter', add 1 to the program pointer and exit.
*
*  If a selection  is made (selection field X), load the
*  program stack with the programs returned by GENSSWCH,
*  replace the 'X' in the selection field  with  a '?', add 1
*  to the pointer and exit.
*
*  If  the  selection  field  is '?', you have returned from
*  the follow-ups  screen.  Re-check  the  follow-ups  file
*  (as  in initialise)  and  not  a '+' in the select field if
*  there are follow-ups, and a space if there are not.
*
*  Once all the selections have been serviced, re-load the
*  saved programs onto the stack,  and blank out the '*'. Set
*  the next program name to  the  current  screen name (to
*  re-display the screen).
*
*  If nothing is  selected,  continue  by just adding one to
*  the program pointer.
*
*
*  Modifications
*  -------------
*
*  Rewrite P5256 with the above processing included.
*
*  Include the AT module P5256AT.
*
*  Include  the  following logical views (access only the
*  fields required).
*
*            - CHDRCLM
*            - LIFECLM
*            - COVRCLM
*            - RESNCLM
*            - CLMHCLM
*            - CLMDCLM
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*  If there are any outstanding unprocessed UTRN records,             *
*  output a warning message : H355 'Unprocessed UTRN                  *
*  Pending', but still allow the program to continue.                 *
*                                                                     *
*****************************************************************
* </pre>
*/
public class P5256 extends ScreenProgCS {
	private static final Logger LOGGER = LoggerFactory.getLogger(P5256.class);
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5256");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaStoredCurrency2 = new FixedLengthStringData(3).init(SPACES);

	protected ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator detailsSameCurrency = new Validator(wsaaCurrencySwitch, "0");
	private Validator detailsDifferent = new Validator(wsaaCurrencySwitch, "1");
	private Validator totalCurrDifferent = new Validator(wsaaCurrencySwitch, "2");
	protected FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).init(SPACES);
	protected PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaDeadLife = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData filler2 = new FixedLengthStringData(183).isAPartOf(wsaaTransactionRec, 17, FILLER).init(SPACES);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
		/* TABLES */

	protected ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private LifeclmTableDAM lifeclmIO = new LifeclmTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	private Atreqrec atreqrec = new Atreqrec();
	protected Deathrec deathrec = getDeathrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected Sftlockrec sftlockrec = new Sftlockrec();
	protected Gensswrec gensswrec = new Gensswrec();
	protected T5687rec t5687rec = new T5687rec();
	protected T6598rec t6598rec = new T6598rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	protected Totloanrec totloanrec = new Totloanrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	//protected S5256ScreenVars sv = ScreenProgram.getScreenVars( S5256ScreenVars.class);
	//private ErrorsInner errorsInner = new ErrorsInner();//ILB-459
	//private FormatsInner formatsInner = new FormatsInner();
	protected S5256ScreenVars sv =getLScreenVars();
	//private ErrorsInner errorsInner = new ErrorsInner();
	protected FormatsInner formatsInner = new FormatsInner();
	
	protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected List<Covrpf> covrpfList;
	protected Covrpf covrpf = null;
	
	protected ItemDAO itempfDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected List<Itempf> itempfList;
	protected Itempf itempf = null;
	
	//added for death claim flexibility		
	private T5688rec t5688rec = new T5688rec();
	protected Dthclmflxrec dthclmflxRec =  getDthclmflxrec();//new Dthclmflxrec();
	private static final String e308 = "E308";
	//end added for death claim flexibility	
	/*BRD-140*/
	protected Mrtdc01rec mrtdc01rec = getMrtdc01rec();
	
	protected HpadTableDAM hpadIO = new HpadTableDAM();
	private PackedDecimalData wsaaFacedValue = new PackedDecimalData(17, 2).init(0);
	private CovrTableDAM covrIO = new CovrTableDAM();
	private PackedDecimalData wsaaoldActualTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaanewActualTot = new PackedDecimalData(17, 2).init(0);
	private UtrnpfDAO utrnDao =getApplicationContext().getBean("utrnpfDAO",UtrnpfDAO.class);
	private Utrnpf rsUtrnpf = new Utrnpf();
	private UlnkpfDAO ulnkpfDAO = getApplicationContext().getBean("ulnkpfDAO", UlnkpfDAO.class);
	private Ulnkpf ulnkpf=new Ulnkpf();
	protected Zutrpf zutrpf = new Zutrpf();//ILIFE-5462
	protected ZutrpfDAO zutrpfDAO = getApplicationContext().getBean("zutrpfDAO", ZutrpfDAO.class);//ILIFE-5462
	private FixedLengthStringData wsaaTreditionslFlag = new FixedLengthStringData(1);//ILIFE-5980
	private String  t6640Item;
	//ILIFE-6296 by wli31
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
	private TpdbpfDAO tpdbpfDAO = getApplicationContext().getBean("tpdbpfDAO", TpdbpfDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	protected Descpf descpf = null;
	private Fluppf fluppf = null ;
	private List<Tpdbpf> tpdbpfList = new ArrayList<Tpdbpf>();
	private Tpdbpf tpdbpf;
	private List<Incrpf> incrpfList;
	/*BRD-140*/
	//ILIFE-6811
	protected ClntpfDAO clntpfDao = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ClmhpfDAO clmhpfDAO = getApplicationContext().getBean("clmhpfDAO", ClmhpfDAO.class);
	private ClmdpfDAO clmdpfDAO = getApplicationContext().getBean("clmdpfDAO", ClmdpfDAO.class);
	protected Clmdpf clmdclm = null;
	protected Clntpf cltspf = new Clntpf();
	private Clmhpf clmhclm = null;
	protected List<Clmdpf> clmdpfList = new ArrayList<Clmdpf>();
	
	//ILB-459 start
	protected LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	protected Lifepf lifepf = new Lifepf();	
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	protected Chdrpf chdrpf = new Chdrpf();
	private HpadpfDAO hpadpfDao = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private Hpadpf hpadpf = new Hpadpf();
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	protected ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
//	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
//	Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
	private CrsvpfDAO crsvpfDAO = getApplicationContext().getBean("crsvpfDAO", CrsvpfDAO.class);
	protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	protected Cattpf cattpf = new Cattpf();	
	
	 protected static final String e304 = "E304";
	 protected static final String f616 = "F616";
	 protected static final String e186 = "E186";
	 protected static final String g620 = "G620";
	 protected static final String h072 = "H072";
	 protected static final String h073 = "H073";
	 protected static final String h093 = "H093";
	 protected static final String j008 = "J008";
	 protected static final String t045 = "T045";
	 protected static final String t064 = "T064";
	 protected static final String hl08 = "Hl08";
	 protected static final String h355 = "H355";
	 protected static final String rfik = "RFIK";
	 protected static final String h359 = "H359";
	 protected static final String rlcc = "RLCC";
	 protected static final String g099 = "G099";
	 protected static final String f910 = "F910";
	 protected static final String rrsn = "RRSN";
	 protected static final String jl25 = "JL25";	//ILJ-48
	 protected static final String jl26 = "JL26";	//ILJ-48
	 protected static final String jl58 = "JL58";//ILJ-431
	 protected static final String jl59 = "JL59";
	 protected static final String rfs8 = "RFS8";//ILJ-431
	 protected static final String jl67 = "JL67";//ILJ-431
	 protected static final String h903 = "H903";//ILJ-431
	 protected AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class); // ILIFE-7944
	 protected Acblpf acblpf; // ILIFE-7944
	 
    private Sd5jlScreenVars sd5jl = ScreenProgram.getScreenVars(Sd5jlScreenVars.class);
    boolean CMDTH006Permission  = false;
	private CpbnfypfDAO cpbnfypfDAO = getApplicationContext().getBean("cpbnfypfDAO", CpbnfypfDAO.class);
	private boolean CMOTH003Permission  = false;
	private static final String feaConfigClmNotify= "CMOTH003";
	private static final String DC= "DC";
	private Alocnorec alocnorec = new Alocnorec();
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	private Notipf notipf=null;
	public static final String CLMPREFIX = "CLMNTF";
	private List<Clnnpf> clnnpfList1 = new ArrayList<Clnnpf>();
	private ClnnpfDAO clnnpfDAO= getApplicationContext().getBean("clnnpfDAO",ClnnpfDAO.class);
	private List<Invspf> invspfList1 = new ArrayList<Invspf>();
	private InvspfDAO invspfDAO= getApplicationContext().getBean("invspfDAO",InvspfDAO.class);
	private FixedLengthStringData wsaaNotifinum = new FixedLengthStringData(8);
	private boolean entryFlag = false;
	private static final String SPLIT_SIGN = "_";
	//ILJ-48 Starts
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private String itemPFX = "IT";
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	boolean CMDTH010Permission = false;
	private static final String feaConfigPreRegistartion= "CMDTH010";
	//ILJ-48 End
	//ILB-459 end
		public Deathrec getDeathrec() {
			return new Deathrec();
		}
	
		public Mrtdc01rec getMrtdc01rec() {
			return new Mrtdc01rec();
		}
		
		public Dthclmflxrec getDthclmflxrec() {
			return new Dthclmflxrec();
		}
		protected FixedLengthStringData wsaaCauseofDeath= new FixedLengthStringData(10);
		protected PackedDecimalData wsaasusamt 		= new PackedDecimalData(17, 2);
		protected FixedLengthStringData wsaareasoncd = new FixedLengthStringData(10);
		protected FixedLengthStringData wsaaDateofdeath = new FixedLengthStringData(8);
		//IBPLIFE-1702
		private String expiredFlag="2";
		private String effectiveFlag="1";
		private String claimRegi="Claim Registered";
		private int noTransnoCount = 1;
/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue1030, 
		exit1090, 
		addFields1720, 
		setUpScreenFields1850, 
		setUpScreenFields1860,
		exit1840, 
		checkCalc2012, 
		exit2090,		
	}

	public P5256() {
		super();
		screenVars = sv;
		new ScreenModel("S5256", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


protected S5256ScreenVars getLScreenVars() {
	return ScreenProgram.getScreenVars(S5256ScreenVars.class);
}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case continue1030: 
					continue1030();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

//BRD-140
protected void initialise1010()
	{
		CMDTH006Permission  = FeaConfg.isFeatureExist("2", "CMDTH006", appVars, "IT");
		CMOTH003Permission  = FeaConfg.isFeatureExist("2", feaConfigClmNotify, appVars, "IT");
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", "CMDTH010", appVars, "IT");
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}		
		/* Get Todays Date and pass this as the effective date             */
		/* to the Death Claim Calculation subroutine, this                 */
		/* date will be used there to calculate the fund prices            */
		sv.dataArea.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		deathrec.effdate.set(datcon1rec.intDate);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/* Skip this section if returning from an optional selection,*/
		/* i.e. check the stack action flag equals '*'.*/
		/* MOVE WSSP-BATCHKEY          TO WSAA-BATCKEY.                 */
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*    GO TO 1090-EXIT.                                          */
		/* Initialise Working Storage fields.                              */
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		wsaaX.set(ZERO);
		wsaaY.set(ZERO);
		wsaaCurrencySwitch.set(ZERO);
		wsaaLife.set(SPACES);
		wsaaJlife.set(SPACES);
		wsaaStoredCurrency.set(SPACES);
		wsaaStoredCurrency2.set(SPACES);
		wsaaDeadLife.set(SPACES);
		wsaaoldActualTot.set(SPACES);//TSD-140
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-48 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{
			sv.riskcommdteOut[varcom.nd.toInt()].set("Y");
		}
		//ILJ-48 End
		set_death_category();
		/* Dummy subfile initalisation for prototype - replace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("S5256", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Dummy field initilisation for prototype version.*/
		sv.estimateTotalValue.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.tdbtamt.set(ZERO);
		sv.policyloan.set(ZERO);
		sv.clamamt.set(ZERO);
		//ILIFE-1137
		//added for death claim flexibility
		sv.susamt.set(ZERO);
		sv.nextinsamt.set(ZERO);
		//end
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.dtofdeath.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.effdate.set(wsspcomn.currfrom);
		sv.contactDate.set(varcom.vrcmMaxDate);
		sv.riskcommdte.set(varcom.vrcmMaxDate);	//ILJ-48
		//ILB-459 start
		if(!CMOTH003Permission){
			sv.notifinumberOut[varcom.nd.toInt()].set("Y");
			sv.claimnotesOut[varcom.nd.toInt()].set("Y");
			sv.investresOut[varcom.nd.toInt()].set("Y");
		}
		if(!CMDTH010Permission && !CMOTH003Permission) {
			sv.claimnumberOut[varcom.nd.toInt()].set("Y");
		}
		
		if(!CMDTH010Permission) {
			sv.claimStatOut[varcom.nd.toInt()].set("Y");
			sv.claimTypOut[varcom.nd.toInt()].set("Y");
			sv.contactDateOut[varcom.nd.toInt()].set("Y");
		}
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrclmIO.setFunction(varcom.retrv);
			chdrclmIO.setFormat(formatsInner.chdrclmrec);
			SmartFileCode.execute(appVars, chdrclmIO);
			if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrclmIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrclmIO.getChdrcoy().toString(), chdrclmIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		ulnkpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ulnkpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */

		int count=ulnkpfDAO.checkUlnkpf(ulnkpf);
		if(count==0){
			deathrec.effdate.set(wsspcomn.currfrom);
		}
		if(CMOTH003Permission){
			allocateNumber2600();
		}
		/* The Currency Code field on the screen is set up here            */
		sv.currcd.set(chdrpf.getCntcurr());
		wsaaStoredCurrency2.set(chdrpf.getCntcurr());
		sv.occdate.set(chdrpf.getOccdate());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		//ILJ-48 Starts
		if(cntDteFlag) {
			Hpadpf hpadpf = hpadpfDAO.getHpadData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
			if(hpadpf!=null) {
				sv.riskcommdte.set(hpadpf.getRskcommdate());
			}
		}
		//ILJ-48 End
		descpf = this.getDescData("T5688", chdrpf.getCnttype().trim());/* IJTI-1386 */
		if (descpf != null) {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrpf.getCownnum());
		//ILIFE-6811

		List<String> clntnumList = new ArrayList();
		clntnumList.add(chdrpf.getCownnum());

		sv.btdate.set(chdrpf.getBtdate());
		sv.ptdate.set(chdrpf.getPtdate());
		/*    Retrieve contract status from T3623*/

		descpf = this.getDescData("T3623", chdrpf.getStatcode().trim());/* IJTI-1386 */
		if (descpf != null) {
			sv.rstate.set(descpf.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		
		descpf = this.getDescData("T3588", chdrpf.getPstcde().trim());/* IJTI-1386 */
		if (descpf != null) {
			sv.pstate.set(descpf.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		if(CMDTH010Permission){
		checkPreRegistration();
		}
		lifepf = lifepfDAO.getCacheObject(lifepf);
		if(null==lifepf) {
			lifeclmIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, lifeclmIO);
			if (isNE(lifeclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lifeclmIO.getParams());
				fatalError600();
			}
			else {
				lifepf = lifepfDAO.getLifeRecord(lifeclmIO.getChdrcoy().toString(), lifeclmIO.getChdrnum().toString(),lifeclmIO.getLife().toString(),lifeclmIO.getJlife().toString());
				if(null==lifepf) {
					fatalError600();
				}
				else {
					lifepfDAO.setCacheObject(lifepf);
				}
			}
		}
		wsaaLife.set(lifepf.getLife());
		wsaaJlife.set(lifepf.getJlife());
		wsaaDeadLife.set(lifepf.getLifcnum());
		if (isEQ(lifepf.getJlife(), "01")) {
			sv.astrsk.set("*");
			sv.asterisk.set(SPACES);
		}
		else {
			sv.astrsk.set(" ");
			sv.asterisk.set("*");
		}
		/*    Read the first life as the life retrieved above*/
		/*    may have been a joint life.*/


		List<Lifepf> lifepfList = lifepfDAO.selectLifeDetails(lifepf);

		/* Move the last record read to the screen and get the*/
		/* description.*/
		if(lifepfList==null){
			fatalError600();
		}


		sv.lifcnum.set(lifepfList.get(0).getLifcnum());
		if(CMOTH003Permission){
			wsspcomn.chdrCownnum.set(sv.lifcnum.toString());	
		}
		for(Lifepf lifepf:lifepfList){
			clntnumList.add(lifepf.getLifcnum());
		}

		Map<String,Clntpf> cltspfMap = clntpfDao.searchClntRecord("CN", wsspcomn.fsuco.toString(), clntnumList);
		if(cltspfMap.containsKey(wsspcomn.fsuco.toString()+ getSplitSign() +chdrpf.getCownnum())){/* IJTI-1523 */
			cltspf = cltspfMap.get(wsspcomn.fsuco.toString()+ getSplitSign() +chdrpf.getCownnum());/* IJTI-1523 */
			plainname();
			sv.ownername.set(wsspcomn.longconfname);}
		else{
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}


		//ILIFE-6811

		if(cltspfMap.containsKey(wsspcomn.fsuco.toString()+ getSplitSign() +lifepfList.get(0).getLifcnum())){
			cltspf = cltspfMap.get(wsspcomn.fsuco.toString()+ getSplitSign() +lifepfList.get(0).getLifcnum());
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		else{
			sv.linsnameErr.set(e304);//ILB-459
			sv.linsname.set(SPACES);
		}

		/*    look for joint life.*/



		if(lifepfList.size()>1 && lifepfList.get(1).getJlife() == "01"){
			customerSpecificJointLife();
			sv.jlifcnum.set(lifepfList.get(1).getLifcnum());
			if(cltspfMap.containsKey(wsspcomn.fsuco.toString()+ getSplitSign() +lifepfList.get(1).getLifcnum())){
				cltspf = cltspfMap.get(wsspcomn.fsuco.toString()+ getSplitSign() +lifepfList.get(1).getLifcnum());
				plainname();
				sv.jlinsname.set(wsspcomn.longconfname);
				goTo(GotoLabel.continue1030);
			}
			else{
				sv.jlinsnameErr.set(e304);//ILB-459
				sv.jlinsname.set(SPACES);
			}
		}	
		else{
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
		}
	}
private void checkPreRegistration() {
	if(cattpfDAO.isExists(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum())){
		cattpf = cattpfDAO.selectRecords(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		sv.claimnumber.set(cattpf.getClaim());
		sv.claimStat.set(cattpf.getClamstat());
		sv.claimTyp.set(cattpf.getClamtyp());
		sv.contactDate.set(cattpf.getEffdate());
		sv.dtofdeath.set(cattpf.getDtofdeath());
		sv.causeofdth.set(cattpf.getCauseofdth());
	}
	else{
	allocateNumber2600();	
	sv.claimStat.set("PR");
	sv.claimTyp.set("DC");
	}
}
//ILB-459 end
protected void set_death_category(){
	
}

protected void allocateNumber2600()
{
	alocnorec.function.set("NEXT");
	alocnorec.prefix.set("CM");
	alocnorec.company.set(wsspcomn.company);
	/*wsaaBranch.set(wsspcomn.branch);*/
	alocnorec.genkey.set(SPACES);
	callProgram(Alocno.class, alocnorec.alocnoRec);
	if (isEQ(alocnorec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(alocnorec.statuz);
		fatalError600();
	}
	if (isNE(alocnorec.statuz, varcom.oK)) {
		sv.claimnumberErr.set(alocnorec.statuz);
	}
	else {
		sv.claimnumber.set(alocnorec.alocNo);
	}
}
protected void continue1030()
	{
		/* Check for a pending Increase record for any component on     */
		/* this contract.  If one exists, display a warning message.    */
		checkForIncrease1100();
		//ILB-459 start
		lifepf = lifepfDAO.getCacheObject(lifepf);
		if(null==lifepf) {
			lifeclmIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, lifeclmIO);
			if (isNE(lifeclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lifeclmIO.getParams());
				fatalError600();
			}
			else {
				lifepf = lifepfDAO.getLifeRecord(lifeclmIO.getChdrcoy().toString(), lifeclmIO.getChdrnum().toString(),lifeclmIO.getLife().toString(),lifeclmIO.getJlife().toString());
				if(null==lifepf) {
					fatalError600();
				}
				else {
					lifepfDAO.setCacheObject(lifepf);
				}
			}
		}

		/*  store coverage and rider and currency
		
		*/
		covrpf = new Covrpf();
		covrpf.setChdrcoy(lifepf.getChdrcoy());//ILB-459 /* IJTI-1386 */
		covrpf.setChdrnum(lifepf.getChdrnum());//ILB-459 /* IJTI-1386 */
		covrpf.setLife(lifepf.getLife());/* IJTI-1386 */
		covrpf.setPlanSuffix(0);
		covrpf.setValidflag("1");
		covrpfList = covrpfDAO.selectCovrRecord(covrpf);
		
		if(covrpfList != null && covrpfList.size() > 0)
		{
			t6640Item = covrpfList.get(0).getCrtable();//ILIFE-5980 /* IJTI-1386 */
			for(Covrpf covr : covrpfList)
			{
				wsaaStoredCoverage.set(covr.getCoverage());
				wsaaStoredRider.set(covr.getRider());			
				wsaaStoredCurrency.set(covr.getPremCurrency());
				
				 if(isEQ(covr.getCrtable(),"AADB")  )
				{
					if(  isEQ(wsaaCauseofDeath,"D6"))
					{
						processComponents1500(covr);
					}
				}
				else
				{ 
					processComponents1500(covr);	
				 }
			}	
			
		}
		
		else{
			t6640Item = " ";  // ILIFE-7321
		}
		
		
		sv.policyloan.set(ZERO);
		/*  Get the value of any loans held against this contract.         */
		getLoanDetails1950();
		getPolicyDebt1970();
		/*  IF DETAILS-SAME-CURRENCY                                     */
		sv.estimateTotalValue.set(wsaaEstimateTot);
		/*    COMPUTE S5256-CLAMAMT = WSAA-ACTUAL-TOT + S5256-POLICYLOAN.  */
		/* COMPUTE S5256-CLAMAMT = WSAA-ACTUAL-TOT - S5256-POLICYLOAN.  */
		
		//added for death claim flexibility
		readTabT5688250();
		setPensionDetailsCustomerSpecific();
		if (isNE(t5688rec.dflexmeth,SPACES)){
			dthclmflxRec.chdrChdrcoy.set(chdrpf.getChdrcoy());//ILB-459 
			dthclmflxRec.chdrChdrnum.set(chdrpf.getChdrnum());	//ILB-459 		
			dthclmflxRec.ptdate.set(sv.ptdate);
			if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
				dthclmflxRec.dtofdeath.set(sv.dtofdeath);
			}else{
				datcon1rec.function.set(varcom.tday);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				wsaaToday.set(datcon1rec.intDate);				
				dthclmflxRec.dtofdeath.set(wsaaToday);
			}			
			dthclmflxRec.btdate.set(sv.btdate);
			dthclmflxRec.cnttype.set(sv.cnttype);
			dthclmflxRec.register.set(chdrpf.getReg());//ILB-459
			dthclmflxRec.cntcurr.set(chdrpf.getCntcurr());//ILB-459
			dthclmflxRec.effdate.set(chdrpf.getCurrfrom());//ILB-459
			callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
			if (isNE(dthclmflxRec.status, varcom.oK)) {
				syserrrec.statuz.set(dthclmflxRec.status);
				syserrrec.params.set(dthclmflxRec.dthclmflxrec);
				fatalError600();
			}	
			sv.susamt.set(dthclmflxRec.susamt);
			sv.nextinsamt.set(dthclmflxRec.nextinsamt);
			customerSpecificDeathBeneficiaryCalc();
		}
		  else{
		initCustomerSpecific4(); //ILIFE-7944
		}
		//modified for death claim flexibility
		//compute(sv.clamamt, 2).set(add(sub(add(wsaaActualTot, sv.policyloan), sv.tdbtamt), sv.zrcshamt));
		//ILIFE-1137
		compute(sv.clamamt, 2).set(add(sub(add(wsaaActualTot, sv.policyloan), sv.tdbtamt,sv.susamt,sv.nextinsamt), sv.zrcshamt));
		customerSpecificPensionCalc();
		sv.susamt.set(mult(sv.susamt,-1));
		/*         MOVE WSAA-STORED-CURRENCY    TO S5256-CURRCD.*/
		checkFollowups1850();
		if (isLT(sv.clamamt, 0)) {
			sv.clamamt.set(0);
		}
		wsaaoldActualTot.set(sv.clamamt);
		/*  move total lines to the screen*/
		/*  Perform the following check for Overpaid Premium.              */
		checkCalc1960();
		checkUtrns1200();
		checkHitrs1250();
	}

//Start ILIFE-7944

protected void initCustomerSpecific4() {

		acblpf = acblDao.getAcblpfRecord(chdrpf.getChdrcoy().toString(), "LP", chdrpf.getChdrnum().trim(),
				"S", null);/* IJTI-1386 */
                   if(acblpf!=null){
                          BigDecimal contractFee = acblpf.getSacscurbal();
                          sv.susamt.set(contractFee);             
                   }
      

}
//End ILIFE-7944



protected void checkForIncrease1100(){
		incrpfList = incrpfDAO.getIncrpfList(chdrpf.getChdrcoy().toString().trim(),chdrpf.getChdrnum().trim());//ILB-459
		if(null != incrpfList && incrpfList.size() >0)
		{ 			 
			scrnparams.errorCode.set(j008);//ILB-459
		}
}

protected void checkUtrns1200()
	{
		setupUtrns1201();
	}

protected void setupUtrns1201()
	{
	
	if(rsUtrnpf==null)
	{
		rsUtrnpf = new Utrnpf();
	}
	
	rsUtrnpf.setChdrcoy(wsspcomn.company.toString());
	rsUtrnpf.setChdrnum(chdrpf.getChdrnum());//ILB-459 /* IJTI-1386 */
	rsUtrnpf=utrnDao.findUtrnpf(rsUtrnpf);
	if (rsUtrnpf!=null) {
		scrnparams.errorCode.set(h355);//ILB-459
		}
	}

//ILIFE-6296 by wli31 start
	protected void checkHitrs1250() {
		/*    Check that the contract has no unprocessed HITRs pending.    */
		List<Hitrpf> hitrrnlList = hitrpfDAO.searchHitrrnlRecord(wsspcomn.company.toString().trim(), chdrpf.getChdrnum().trim());//ILB-459 /* IJTI-1386 */
		if (hitrrnlList.size() > 0) {
			scrnparams.errorCode.set(hl08);//ILB-459
		}
	}

	protected Descpf getDescData(String desctable, String descitem){
		Descpf descpf=descDAO.getdescData("IT", desctable, descitem, wsspcomn.company.toString().trim(), wsspcomn.language.toString().trim());
		return descpf;
		}
	//ILIFE-6296 by wli31 end


protected void processComponents1500(Covrpf covr)
	{
	
		/* Loop through the coverages and riders to find which ones relate*/
		/* to the dead life*/
		/* Read table T5687 to find the death method calculation*/
		/* subroutine and also if the coverage or rider read applies to*/
		/* one or both lives.*/
	//ILB-459
		readT56871650(covr);
		if (isEQ(lifepf.getJlife(), "00")
		|| isEQ(lifepf.getJlife(), "  ")) {
			/* if a main life*/
			if (isNE(covr.getJlife(), "01")) {
				/* coverage or rider applies to this life*/
				selectCoverRider1700(covr);
			}
		}
		/* JOINT LIFE - 01*/
		if (isEQ(lifepf.getJlife(), "01")) {
			if ((isEQ(covr.getJlife(), "01")
			|| isEQ(t5687rec.jlifePresent, "Y"))) {
				/* coverage or rider applied to both lives*/
				selectCoverRider1700(covr);
			}
		}
		//ILB-459 end
		
		wsaaStoredCoverage.set(covr.getCoverage());
		wsaaStoredRider.set(covr.getRider());
	}


protected void readT56871650(Covrpf covr)
	{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemitem(covr.getCrtable());/* IJTI-1386 */
	itempf.setItemtabl("T5687");
	itempf.setItmfrm(new BigDecimal(covr.getCrrcd()));
	itempf.setItmto(new BigDecimal(covr.getCrrcd()));
	
	itempfList = itempfDAO.findByItemDates(itempf);
	if(itempfList.size() > 0) {
		for (Itempf it : itempfList) {				
			t5687rec.t5687Rec.set(StringUtil.rawToString(it.getGenarea()));
			
		}
	} 
	}

protected void selectCoverRider1700(Covrpf covr)
	{
	 	addFields1720(covr);
	}

protected void read1710()
	{
		
	}

	/**
	* <pre>
	*****IF T6598-CALCPROG = SPACES                              <016>
	********GO TO 1740-EXIT.                                     <016>
	* </pre>
	*/
protected void addFields1720(Covrpf covr)
	{
		/* add fields to subfile*/
	
	if (isEQ(t5687rec.dcmeth, SPACES)) {
		t6598rec.calcprog.set(SPACES);		
	}
	
	readT65981750(covr);
		sv.coverage.set(covr.getCoverage());
		sv.hcover.set(covr.getCoverage());
		if (isEQ(covr.getRider(), "00")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(covr.getRider());
		}
		if (isNE(covr.getRider(), "00")
		&& isNE(covr.getRider(), "  ")) {
			sv.coverage.set(SPACES);
		}
		sv.cnstcur.set(covr.getPremCurrency());
		sv.hcnstcur.set(covr.getPremCurrency());
		/* find description for crtable.*/
		/*    MOVE COVRCLM-CRTABLE        TO S5256-CRTABLE,*/
		sv.hcrtable.set(covr.getCrtable());

		descpf = this.getDescData("T5687", covr.getCrtable().trim());/* IJTI-1386 */
		if (isNE(deathrec.description, SPACES)) {
			/*    IF DESC-STATUZ              = O-K                            */
			/*       MOVE DESC-LONGDESC       TO S5256-SHORTDS                 */
			/*    ELSE                                                         */
			/*       MOVE ALL '?'             TO S5256-SHORTDS.                */
			if (descpf != null) {
				sv.shortds.set(descpf.getShortdesc());
			}
			else {
				sv.shortds.fill("?");
			}
		}
		if (isNE(deathrec.description, SPACES)) {
			sv.shortds.set(deathrec.description);
		}
		//BRD-140
		//ILB-459 start
		hpadpf = hpadpfDao.getHpadData(covr.getChdrcoy(), covr.getChdrnum());
		if(hpadpf == null){
			syserrrec.statuz.set("hpadpf MRNF");
			fatalError600();
		} 

		//ILB-459 end
		/*BRD-140*/
			String policyType = chdrpf.getCnttype().trim();//ILB-459
		if (policyType != null && policyType.equals("MRT"))

		{
			mrtdc01rec.mrtdc01Rec.set(SPACES);
			mrtdc01rec.chdrcoy.set(covr.getChdrcoy()); 
			mrtdc01rec.chdrnum.set(covr.getChdrnum());
			mrtdc01rec.life.set(covr.getLife());
			mrtdc01rec.jlife.set(covr.getJlife());
			mrtdc01rec.coverage.set(covr.getCoverage());
			mrtdc01rec.rider.set(covr.getRider());
			mrtdc01rec.riskTerm.set(ZERO);
			mrtdc01rec.sumins.set(covr.getSumins());
			mrtdc01rec.currcode.set(ZERO);
			mrtdc01rec.crtable.set(covr.getCrtable());
			mrtdc01rec.occDate.set(chdrpf.getOccdate());//ILB-459
			/* MOVE ZEROS TO CDTH-EFFDATE. */
			mrtdc01rec.estimatedVal.set(ZERO); /* BRD-140*/
			mrtdc01rec.endf.set(ZERO);
			mrtdc01rec.batckey.set(wsaaBatckey);
			mrtdc01rec.element.set(ZERO);
			mrtdc01rec.fieldType.set(SPACES);  /*ILIFE-3723*/
			mrtdc01rec.virtualFundStore.set(ZERO);
			mrtdc01rec.unitTypeStore.set(ZERO);
			mrtdc01rec.processInd.set(ZERO);
			mrtdc01rec.language.set(wsspcomn.language);
			mrtdc01rec.status.set(varcom.oK);
			mrtdc01rec.actualVal.set(ZERO);
			callDeathMethod1810(covr);
			
		} else {		
		/* check the reinsurance file at a later date and move*/
		/* 'y' to this field if they are present*/
		/*  MOVE SPACES                 TO S5256-RIIND.                  */
		/* PERFORM 1900-READ-RACT.                              <R96REA>*/
		/* PERFORM 1900-READ-RACD.                              <R96REA>*/
		/* IF RACDMJA-STATUZ            = O-K                   <R96REA>*/
		/*     MOVE 'Y'                TO S5256-RIIND           <R96REA>*/
		/* END-IF.                                              <R96REA>*/
		sv.liencd.set(covr.getLiencd());
		deathrec.estimatedVal.set(ZERO);
		deathrec.actualVal.set(ZERO);
		deathrec.chdrChdrcoy.set(covr.getChdrcoy());
		/*MOVE ZEROS                  TO CDTH-EFFDATE.                 */
		deathrec.chdrChdrnum.set(covr.getChdrnum());
		deathrec.lifeLife.set(covr.getLife());
		deathrec.lifeJlife.set(covr.getJlife());
		deathrec.covrCoverage.set(covr.getCoverage());
		deathrec.covrRider.set(covr.getRider());
		deathrec.crtable.set(covr.getCrtable());
		/*MOVE COVRCLM-CURRTO         TO CDTH-EFFDATE.                 */
		/*MOVE COVRCLM-CURRFROM       TO CDTH-EFFDATE.                 */
		deathrec.status.set(SPACES);
		deathrec.endf.set(SPACES);
		deathrec.batckey.set(wsaaBatckey);
		deathrec.element.set(SPACES);
		deathrec.fieldType.set(SPACES);
		deathrec.virtualFundStore.set(SPACES);
		deathrec.unitTypeStore.set(SPACES);
		deathrec.processInd.set(SPACES);
		deathrec.language.set(wsspcomn.language);
		while ( !(isEQ(deathrec.status, varcom.endp))) {
			callDeathMethod1800(covr);
		}
		}
		/*BRD-140*/
	}

protected void readT65981750(Covrpf covr)
	{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemitem(t5687rec.dcmeth.toString());
	itempf.setItemtabl("T6598");
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate()));//ILB-459
	itempf.setItmto(new BigDecimal(0));
	
	itempfList = itempfDAO.findByItemDates(itempf);
	if(itempfList.size() > 0) {
		for (Itempf it : itempfList) {				
			t6598rec.t6598Rec.set(StringUtil.rawToString(it.getGenarea()));
			
		}
	} 
	}

protected void callDeathMethod1800(Covrpf covr)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read1810();
				case setUpScreenFields1850: 
					setUpScreenFields1850(covr);
				case exit1840: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
/*BRD-140*/
protected void callDeathMethod1810(Covrpf covr)
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: 
				read1820(covr);
			case setUpScreenFields1860: 
				setUpScreenFields1860(covr);
			case exit1840: 
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}
protected void read1820(Covrpf covr) {
	mrtdc01rec.element.set(SPACES);
	/*BRD-140*/
	mrtdc01rec.chdrcoy.set(covr.getChdrcoy());
	/*MOVE ZEROS                  TO CDTH-EFFDATE.                 */
	mrtdc01rec.chdrnum.set(covr.getChdrnum());
 	mrtdc01rec.occDate.set(covr.getCrrcd());
    mrtdc01rec.currcode.set(sv.currcd);
	if (isEQ(t6598rec.calcprog, SPACES)) {
		mrtdc01rec.actualVal.set(mrtdc01rec.sumins);
		/*BRD-140*/
		mrtdc01rec.status.set(varcom.endp);
		goTo(GotoLabel.setUpScreenFields1860);
	}
	callProgram(t6598rec.calcprog, mrtdc01rec.mrtdc01Rec);
	if (isEQ(mrtdc01rec.statuz, varcom.bomb)) {
		syserrrec.params.set(mrtdc01rec.mrtdc01Rec);
		syserrrec.statuz.set(mrtdc01rec.statuz);
		fatalError600();
	}
	if (isNE(mrtdc01rec.statuz, varcom.oK) && isNE(mrtdc01rec.statuz, varcom.endp)) {
		syserrrec.statuz.set(mrtdc01rec.statuz);
		fatalError600();
	}
	if (isEQ(mrtdc01rec.statuz, varcom.endp)) {
		goTo(GotoLabel.exit1840);
	}
	//ILB-459 start
	zrdecplcPojo.setCurrency(mrtdc01rec.currcode.toString());
	zrdecplcPojo.setAmountIn(mrtdc01rec.sumins.getbigdata());
	callRounding5000();
	mrtdc01rec.estimatedVal.set(SPACES); 
	zrdecplcPojo.setCurrency(mrtdc01rec.currcode.toString());
	zrdecplcPojo.setAmountIn(mrtdc01rec.actualVal.getbigdata());
	callRounding5000();
        mrtdc01rec.actualVal.set(mrtdc01rec.sumins);
      //ILB-459 end
}

protected void setUpScreenFields1860(Covrpf covr) {
	/* MOVE COVRCLM-PREM-CURRENCY TO S5256-CNSTCUR, */
	/* S5256-HCNSTCUR. */
	if (isEQ(mrtdc01rec.currcode, SPACES)) {
		sv.cnstcur.set(covr.getPremCurrency());
		sv.hcnstcur.set(covr.getPremCurrency());
	} else {
		wsaaCurrencySwitch.set(1);
		sv.cnstcur.set(mrtdc01rec.currcode);
		sv.hcnstcur.set(mrtdc01rec.currcode);
	}
	/* MOVE CDTH-TYPE TO S5256-HTYPE. */
	/* MOVE CDTH-TYPE TO S5256-FIELD-TYPE. */
	sv.htype.set(mrtdc01rec.fieldType);
	sv.fieldType.set(mrtdc01rec.fieldType);
	sv.vfund.set(mrtdc01rec.element);
	sv.estMatValue.set(mrtdc01rec.estimatedVal);
	sv.hemv.set(mrtdc01rec.estimatedVal);
	sv.actvalue.set(mrtdc01rec.actualVal);
	sv.hactval.set(mrtdc01rec.actualVal);

	descpf = this.getDescData("T5687", mrtdc01rec.crtable.toString().trim());
	if (descpf != null) {
		sv.shortds.set(descpf.getShortdesc());
	} else {
		sv.shortds.fill("?");
	}
	if (isNE(sv.vfund, SPACES)) {
		descpf = this.getDescData("T5515", sv.vfund.toString().trim());
		if (descpf != null) {
			sv.shortds.set(descpf.getShortdesc());
		} else {
			sv.shortds.fill("?");
		}
	}
	if (isNE(mrtdc01rec.description, SPACES)) {
		sv.shortds.set(mrtdc01rec.description);
	}
	if (isEQ(covr.getPremCurrency(), wsaaStoredCurrency)) {
		wsaaEstimateTot.add(mrtdc01rec.estimatedVal);
		wsaaActualTot.add(mrtdc01rec.actualVal);
	} else {
		wsaaCurrencySwitch.set(1);
	}
	/* add record to subfile */
	if (isEQ(mrtdc01rec.fieldType, "S")) {
		/* NEXT_SENTENCE */
	} else {
		if (isEQ(mrtdc01rec.estimatedVal, ZERO) && isEQ(mrtdc01rec.actualVal, ZERO)
				&& isEQ(mrtdc01rec.processInd, SPACES)) {
			return;
		}
	}
	scrnparams.function.set(varcom.sadd);
	processScreen("S5256", sv);
	if (isNE(scrnparams.statuz, varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}



/*ILIFE-3723*/
protected void setUpScreenFields1870() {
	 
	scrnparams.function.set(varcom.sstrt);
	processScreen("S5256", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
		 
		if (isNE(sv.actvalue,wsaaActualTot) )
		{
		  sv.actvalue.set(wsaaActualTot);
		  scrnparams.statuz.set(varcom.kill);
		}
		else			
			 scrnparams.statuz.set(varcom.oK); 
		scrnparams.function.set(varcom.supd);
		processScreen("S5256", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)){
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
		scrnparams.function.set(varcom.srdn);
		processScreen("S5256", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
			
	}
	
//	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");

}
/*ILIFE-3723*/


protected void read1810()
	{
		deathrec.element.set(SPACES);
		/*  MOVE SPACES                 TO CDTH-TYPE.                    */
		/*MOVE SPACES                 TO CDTH-FIELD-TYPE.         <022>*/
		deathrec.currcode.set(sv.currcd);
		if (isEQ(t6598rec.calcprog, SPACES)) {
			deathrec.status.set(varcom.endp);
			goTo(GotoLabel.setUpScreenFields1850);
		}
		callProgram(t6598rec.calcprog, deathrec.deathRec);
		if (isEQ(deathrec.status, varcom.bomb)) {
			syserrrec.params.set(deathrec.deathRec);
			syserrrec.statuz.set(deathrec.status);
			fatalError600();
		}
		if (isNE(deathrec.status, varcom.oK)
		&& isNE(deathrec.status, varcom.endp)) {
			syserrrec.params.set(deathrec.deathRec);
			syserrrec.statuz.set(deathrec.status);
			fatalError600();
		}
		if (isEQ(deathrec.status, varcom.endp)) {
			goTo(GotoLabel.exit1840);
		}
		//ILB-459 start
		zrdecplcPojo.setCurrency(deathrec.currcode.toString());
		zrdecplcPojo.setAmountIn(deathrec.estimatedVal.getbigdata());
		callRounding5000();
		deathrec.estimatedVal.set(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setCurrency(deathrec.currcode.toString());
		zrdecplcPojo.setAmountIn(deathrec.actualVal.getbigdata());//ILIFE-7679
		callRounding5000();
		deathrec.actualVal.set(zrdecplcPojo.getAmountOut());
		//ILB-459 end
	}

protected void setUpScreenFields1850(Covrpf covr)
	{
		/*    MOVE COVRCLM-PREM-CURRENCY  TO S5256-CNSTCUR,                */
		/*                                   S5256-HCNSTCUR.               */
		if (isEQ(deathrec.currcode, SPACES)) {
			sv.cnstcur.set(covr.getPremCurrency());
			sv.hcnstcur.set(covr.getPremCurrency());
		}
		else {
			wsaaCurrencySwitch.set(1);
			sv.cnstcur.set(deathrec.currcode);
			sv.hcnstcur.set(deathrec.currcode);
		}
		/*  MOVE CDTH-TYPE              TO S5256-HTYPE.                  */
		/*  MOVE CDTH-TYPE              TO S5256-FIELD-TYPE.             */
		sv.htype.set(deathrec.fieldType);
		sv.fieldType.set(deathrec.fieldType);
		if(isEQ(deathrec.element,"00"))
			sv.vfund.set(SPACES);
		else
			sv.vfund.set(deathrec.element);

		sv.vfund.set(deathrec.element);
		sv.estMatValue.set(deathrec.estimatedVal);
		sv.hemv.set(deathrec.estimatedVal);
		sv.actvalue.set(deathrec.actualVal);
		sv.hactval.set(deathrec.actualVal);
		descpf = this.getDescData("T5687", deathrec.crtable.toString().trim());
		if (descpf != null) {
			sv.shortds.set(descpf.getShortdesc());
		}
		else {
			sv.shortds.fill("?");
		}
		if (isNE(sv.vfund, SPACES)) {
			descpf = this.getDescData("T5515", sv.vfund.toString().trim());
			if (descpf != null) {
				sv.shortds.set(descpf.getShortdesc());
			}
			else {
				sv.shortds.fill("?");
			}
		}
		/**
		 * Below if condition comment because of ILIFE 5232 of issue 2 
		 * and put new deathrec.crtable condition.
		 */
		
		if (isNE(deathrec.crtable, SPACES)) {
			sv.shortds.set(deathrec.crtable);
		}
		
		if (isEQ(covr.getPremCurrency(), wsaaStoredCurrency)) {
			wsaaEstimateTot.add(deathrec.estimatedVal);
			wsaaActualTot.add(deathrec.actualVal);
		}
		else {
			wsaaCurrencySwitch.set(1);
		}
		/*  add record to subfile*/
		if (isEQ(deathrec.fieldType, "S")) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(deathrec.estimatedVal, ZERO)
			&& isEQ(deathrec.actualVal, ZERO)
			&& isEQ(deathrec.processInd, SPACES)) {
				return ;
			}
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5256", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	protected void checkFollowups1850() {
		//ILIFE-6296 by wli31
		String chdrnum = (chdrpf.getChdrnum()!= null?chdrpf.getChdrnum(): " ");//ILB-459 /* IJTI-1386 */
		String chdrcoy = (chdrpf.getChdrcoy()!=null?chdrpf.getChdrcoy().toString():" ");//ILB-459
		boolean recFound = fluppfDAO.checkFluppfRecordByChdrnum(chdrcoy, chdrnum);
		if (!recFound) {
			//fatalError600();
			sv.fupflg.set(SPACES);
		}  else {
			sv.fupflg.set("+");
		}

	}
/*LGNM-EXIT*/
	
protected void largename()
{
	/*LGNM-100*/
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(cltspf.getClttype(), "C")) {
		corpname();
	} else {
		wsspcomn.longconfname.set(cltspf.getSurname());
	}
	/*LGNM-EXIT*/
}

protected void plainname()
{
	/*PLAIN-100*/
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(cltspf.getClttype(), "C")) {
		corpname();

	} else if (isNE(cltspf.getGivname(), SPACES)) {
		String firstName = cltspf.getGivname();/* IJTI-1386 */
		String lastName = cltspf.getSurname();/* IJTI-1386 */
		String delimiter = ",";
		
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		
	} else {
		wsspcomn.longconfname.set(cltspf.getSurname());
	}
	/*PLAIN-EXIT*/
}

protected void payeename()
{
	/*PAYEE-100*/
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(cltspf.getClttype(), "C")) {
		corpname();
	} else if (isEQ(cltspf.getEthorig(), "1")) {
		String firstName = cltspf.getGivname();/* IJTI-1386 */
		String lastName = cltspf.getSurname();/* IJTI-1386 */
		String delimiter = "";
		String salute = cltspf.getSalutl();/* IJTI-1386 */
		
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		String fullNameWithSalute = stringUtil.includeSalute(salute, fullName);
	
		wsspcomn.longconfname.set(fullNameWithSalute);
	} else {
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltspf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltspf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltspf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}
	/*PAYEE-EXIT*/
}

protected void corpname()
{
	/* PAYEE-1001 */
	wsspcomn.longconfname.set(SPACES);
	/* STRING CLTS-SURNAME DELIMITED SIZE */
	/* CLTS-GIVNAME DELIMITED ' ' */
	String firstName = cltspf.getLgivname();/* IJTI-1386 */
	String lastName = cltspf.getLsurname();/* IJTI-1386 */
	String delimiter = "";
	
	// this way we can override StringUtil behaviour in formatName()
	String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
	wsspcomn.longconfname.set(fullName);
	/*CORP-EXIT*/
}
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*1900-READ-RACT      SECTION.                             <R96REA>
	*1900-READ.                                               <R96REA>
	**** MOVE COVRCLM-CHDRCOY        TO RACT-CHDRCOY.         <R96REA>
	**** MOVE COVRCLM-CHDRNUM        TO RACT-CHDRNUM.         <R96REA>
	**** MOVE COVRCLM-LIFE           TO RACT-LIFE.            <R96REA>
	**** MOVE COVRCLM-COVERAGE       TO RACT-COVERAGE.        <R96REA>
	**** MOVE COVRCLM-RIDER          TO RACT-RIDER.           <R96REA>
	**** MOVE SPACES                 TO RACT-RASNUM           <R96REA>
	****                                RACT-RATYPE           <R96REA>
	****                                RACT-VALIDFLAG.       <R96REA>
	**** MOVE BEGN                   TO RACT-FUNCTION.        <R96REA>
	**** MOVE RACTREC                TO RACT-FORMAT.          <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTIO'               USING RACT-PARAMS.       <R96REA>
	****                                                      <R96REA>
	**** IF RACT-STATUZ          NOT = O-K                    <R96REA>
	****                     AND NOT = ENDP                   <R96REA>
	****    MOVE RACT-PARAMS         TO SYSR-PARAMS           <R96REA>
	****    MOVE RACT-STATUZ         TO SYSR-STATUZ           <R96REA>
	****    PERFORM 600-FATAL-ERROR.                          <R96REA>
	**** IF RACT-CHDRCOY         NOT = COVRCLM-CHDRCOY        <R96REA>
	**** OR RACT-CHDRNUM         NOT = COVRCLM-CHDRNUM        <R96REA>
	**** OR RACT-LIFE            NOT = COVRCLM-LIFE           <R96REA>
	**** OR RACT-COVERAGE        NOT = COVRCLM-COVERAGE       <R96REA>
	**** OR RACT-RIDER           NOT = COVRCLM-RIDER          <R96REA>
	**** OR RACT-VALIDFLAG       NOT = '1'                    <R96REA>
	**** OR RACT-STATUZ              = ENDP                   <R96REA>
	****    GO TO 1900-EXIT.                                  <R96REA>
	**** MOVE 'Y'                 TO   S5256-RIIND.           <R96REA>
	*1990-EXIT.                                                  <027>
	*1900-EXIT.                                               <R96REA>
	**** EXIT.                                                <R96REA>
	*1900-READ-RACD SECTION.                                          
	*************************                                 <R96REA>
	**                                                        <R96REA>
	*1901-READ.                                               <R96REA>
	**                                                        <R96REA>
	**   MOVE SPACES                 TO RACDMJA-CHDRCOY.      <R96REA>
	**   MOVE COVRCLM-CHDRCOY        TO RACDMJA-CHDRCOY.      <R96REA>
	**   MOVE COVRCLM-CHDRCOY        TO RACDMJA-CHDRCOY.      <R96REA>
	**   MOVE COVRCLM-CHDRNUM        TO RACDMJA-CHDRNUM.      <R96REA>
	**   MOVE COVRCLM-LIFE           TO RACDMJA-LIFE.         <R96REA>
	**   MOVE COVRCLM-COVERAGE       TO RACDMJA-COVERAGE.     <R96REA>
	**   MOVE COVRCLM-RIDER          TO RACDMJA-RIDER.        <R96REA>
	**   MOVE ZEROES                 TO RACDMJA-PLAN-SUFFIX.  <R96REA>
	**   MOVE 99                     TO RACDMJA-SEQNO.        <R96REA>
	**                                                        <R96REA>
	**   MOVE BEGN                   TO RACDMJA-FUNCTION.     <R96REA>
	**   MOVE RACDMJAREC             TO RACDMJA-FORMAT.       <R96REA>
	**                                                        <R96REA>
	**   CALL 'RACDMJAIO'            USING RACDMJA-PARAMS.    <R96REA>
	**                                                        <R96REA>
	**   IF RACDMJA-STATUZ        NOT = O-K AND NOT = ENDP    <R96REA>
	**        MOVE RACDMJA-PARAMS    TO SYSR-PARAMS           <R96REA>
	**        MOVE RACDMJA-STATUZ    TO SYSR-STATUZ           <R96REA>
	**        PERFORM 600-FATAL-ERROR                         <R96REA>
	**   END-IF.                                              <R96REA>
	**                                                        <R96REA>
	**   IF RACDMJA-CHDRCOY       NOT = COVRCLM-CHDRCOY OR    <R96REA>
	**      RACDMJA-CHDRNUM       NOT = COVRCLM-CHDRNUM OR    <R96REA>
	**      RACDMJA-LIFE          NOT = COVRCLM-LIFE OR       <R96REA>
	**      RACDMJA-COVERAGE      NOT = COVRCLM-COVERAGE OR   <R96REA>
	**      RACDMJA-RIDER         NOT = COVRCLM-RIDER OR      <R96REA>
	**      RACDMJA-STATUZ            = ENDP                  <R96REA>
	**        MOVE ENDP              TO RACDMJA-STATUZ        <R96REA>
	**   END-IF.                                              <R96REA>
	**                                                        <R96REA>
	*1999-EXIT.                                               <R96REA>
	**    EXIT.                                               <R96REA>
	* </pre>
	*/
protected void getLoanDetails1950()
	{
		start1950();
	}

	/**
	* <pre>
	*  Call TOTLOAN to get the current loan value.                    
	*  Note that TOTLOAN always returns details in the CHDR           
	*  currency.                                                      
	* </pre>
	*/
protected void start1950()
	{
		/*  Read TOTLOAN to get value of loans for this contract.          */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy().toString());//ILB-459
		totloanrec.chdrnum.set(chdrpf.getChdrnum());//ILB-459
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.function.set("LOAN");
		totloanrec.language.set(wsspcomn.language);
		/* CALL 'TOTLOAN' USING TOTL-TOTLOAN-REC.                       */
		/* CALL 'ZRTOTLON'          USING TOTL-TOTLOAN-REC.             */
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal, totloanrec.interest));
		saveIntrstCustomerSpecific();
		/* MOVE WSAA-HELD-CURR-LOANS   TO S5256-POLICYLOAN.             */
		/* Change the sign to reflect the Client's context.*/
		compute(sv.policyloan, 2).set(mult(wsaaHeldCurrLoans, (-1)));
		/* Calculate Cash Deposit accounts related to the Contract.     */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy());//ILB-459
		totloanrec.chdrnum.set(chdrpf.getChdrnum());//ILB-459
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.function.set("CASH");
		totloanrec.language.set(wsspcomn.language);
		/*    CALL 'ZRTOTLON'          USING TOTL-TOTLOAN-REC.             */
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(sv.zrcshamt, 2).set(add(totloanrec.principal, totloanrec.interest));
	}

protected void saveIntrstCustomerSpecific() {
		// TODO Auto-generated method stub
		
	}

protected void checkCalc1960()
	{
		/*START*/
		/* Check for overpaid premium on contracts which are not Single    */
		/* Premium Contracts.                                              */
		/* Display warning message if there is Overpaid Premium.           */
	//ILB-459 start
		if (isNE(chdrpf.getBillfreq(), "00")) {
			
			datcon2rec.intDate1.set(wsspcomn.currfrom);
			datcon2rec.frequency.set(chdrpf.getBillfreq());
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isGT(chdrpf.getPtdate(), datcon2rec.intDate2)) {
				scrnparams.errorCode.set("T045");
				wsspcomn.edterror.set(varcom.oK);
			}
			//ILB-459 end
		}
		/*EXIT*/
	}

	protected void getPolicyDebt1970(){
		sv.tdbtamt.set(ZERO);
		tpdbpfList = tpdbpfDAO.getTpdbtamt((chdrpf.getChdrcoy()).toString().trim(),chdrpf.getChdrnum().trim());//ILB-459 /* IJTI-1386 */
		if (tpdbpfList.isEmpty()){
			return;
		}
		
		for(Tpdbpf tpdbpf : tpdbpfList){
			sv.tdbtamt.add(Double.parseDouble(tpdbpf.getTdbtamt().toString()));
		}
	}


	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		if (CMDTH006Permission) {
			sv.bnfyingOut[varcom.nd.toInt()].set("N");
		} else {
			sv.bnfyingOut[varcom.nd.toInt()].set("Y");
		}
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		scrnparams.subfileRrn.set(1);
		
		// ILIFE-5980 Start
		if(isEQ(wsaaTreditionslFlag, "Y")){
			sv.rundteOut[varcom.pr.toInt()].set("Y");
			sv.rsuninOut[varcom.pr.toInt()].set("Y");
		}// ILIFE-5980 END
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateScreen2010();
				case checkCalc2012: 
					checkCalc2012();
					next2020();
					validateSelectionFields2070();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5256IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5256-DATA-AREA                         */
		/*                         S5256-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if(CMOTH003Permission){
			if(isNE(sv.notifinumber,SPACES)){
				wsaaNotifinum.set(sv.notifinumber.toString().replace(CLMPREFIX, ""));
				notipf = notipfDAO.getNotiReByNotifin(wsaaNotifinum.toString(),wsspcomn.company.toString());
				if(notipf != null && isEQ(notipf.getIncidentt().trim(),DC)){
					if(isNE(notipf.getDeathdate(),SPACES) && notipf.getCausedeath()!=null  && !entryFlag ){
					entryFlag = true;
					sv.dtofdeath.set(notipf.getDeathdate());
					sv.causeofdth.set(notipf.getCausedeath());
					}
				}
				else{
				sv.notifinumberErr.set(rrsn);
				}
			}
			else{
				wsaaNotifinum.set(SPACES);
			}
			clnnpfList1 =  clnnpfDAO.getClnnpfList(wsspcomn.company.toString(), wsaaNotifinum.toString(),sv.claimnumber.toString().trim());
			invspfList1 =  invspfDAO.getInvspfList(wsspcomn.company.toString(), wsaaNotifinum.toString(),sv.claimnumber.toString().trim());			
		}
		/*Date of Death field is mandatory for Death Claim Registration
		MIBT-181*/
		if(CMOTH003Permission){
			 if((isEQ(sv.dtofdeath, SPACES)|| isEQ(sv.dtofdeath, varcom.vrcmMaxDate)) && isEQ(sv.dtofdeathDisp, SPACES)){
			    sv.dtofdeathErr.set(e186);
			  }
		}
		else {
			if(isEQ(sv.dtofdeathDisp, SPACES)){
				sv.dtofdeathErr.set(e186);//ILB-459
			}
	    }
	    
		/*       if date of death entered it must not be less than*/
		/*       the original commencment date and not > than today*/
		
		if(CMDTH010Permission){
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		deathrec.effdate.set(datcon1rec.intDate);
		wsaaToday.set(datcon1rec.intDate);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		}
		if (isEQ(sv.dtofdeath, ZERO)) {
			goTo(GotoLabel.checkCalc2012);
		}
		if(CMDTH010Permission){
			
		if((isEQ(sv.contactDate, SPACES)||isEQ(sv.contactDate,varcom.vrcmMaxDate)) && isEQ(sv.contactDateErr, SPACES)){
			sv.contactDateErr.set(e186);//ILB-459
		}
		
		if(isLT(sv.contactDate, sv.riskcommdte) && isEQ(sv.contactDateErr, SPACES)){
			sv.contactDateErr.set(jl58);
		}
		
		if(isGT(sv.contactDate,wsaaToday)&&isNE(sv.contactDate,varcom.vrcmMaxDate) && isEQ(sv.contactDateErr, SPACES)){
			sv.contactDateErr.set(rfs8); 
		}
		
		if(isLT(sv.contactDate,sv.dtofdeath) &&  isEQ(sv.contactDateErr, SPACES)){
			sv.contactDateErr.set(jl67);
		}
		}
		
		if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
			if (isGT(sv.dtofdeath, wsaaToday)) {
				/*MOVE E923             TO S5256-DTOFDEATH-ERR        */
				sv.dtofdeathErr.set(t064);//ILB-459
			}
			else {
				if (!cntDteFlag && isLT(sv.dtofdeath, chdrpf.getOccdate()))//ILB-459	//ILJ-48
				{
					/*               MOVE U041         TO S5256-DTOFDEATH-ERR.       */
					/*            MOVE H073         TO S5256-DTOFDEATH-ERR.  <024>*/
					sv.dtofdeathErr.set(h073);//ILB-459
				}
				else {
					datcon1rec.function.set(varcom.tday);
					Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
					wsaaToday.set(datcon1rec.intDate);
				}
			}
		}
		//ILJ-48 Starts
		if(cntDteFlag && isNE(sv.dtofdeath,varcom.vrcmMaxDate)) {
			if (isLT(sv.dtofdeath, sv.riskcommdte)) {
				sv.dtofdeathErr.set(jl26);
				wsspcomn.edterror.set("Y");
			}
			if(isGTE(sv.dtofdeath, sv.riskcommdte) && isLT(sv.dtofdeath,sv.occdate)) {
				scrnparams.errorCode.set(jl25);
			}
		}
		//ILJ-48 End
		/*       effective date must be entered - should be not less than*/
		/*       the ccd and not less than date of death.*/
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)) {
			sv.effdateErr.set(e186);//ILB-459
		}
		else {
			if(!cntDteFlag) //ILJ-93
			{
				if (isLT(sv.effdate, chdrpf.getOccdate()))//ILB-459
					{
						sv.effdateErr.set(f616);//ILB-459
					}
				else {
					if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
						if (isLT(sv.effdate, sv.dtofdeath)) {
						/*MOVE F990  TO S5256-EFFDATE-ERR.                    */
						sv.dtofdeathErr.set(t064);//ILB-459
					}
			   	  }
				}
		     }
			else
			{
				if (isLT(sv.effdate,sv.riskcommdte)) 
				{
					sv.effdateErr.set(f616);
				}
			else {
				if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
					if (isLT(sv.effdate, sv.dtofdeath)) {
					sv.dtofdeathErr.set(t064);
				}
		   	  }
			}
		  }
		}
		// ILIFE-5462 Start
		if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("Y")){
			if (isEQ(sv.reserveUnitsDate, 99999999) || isEQ(sv.reserveUnitsDate, SPACES) || isEQ(sv.reserveUnitsDate, 00000000)) { 
				sv.rundteErr.set(e186);//ILB-459
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			if (isLT(sv.reserveUnitsDate, chdrpf.getCcdate()))//ILB-459
				{
				sv.rundteErr.set(h359);//ILB-459
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			if (isGT(sv.reserveUnitsDate, datcon1rec.intDate)) {
				sv.rundteErr.set(rlcc);//ILB-459
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}
		if(sv.reserveUnitsInd.toString().trim().equalsIgnoreCase("N") 
				&& !sv.reserveUnitsDate.equals(varcom.vrcmMaxDate)){
			sv.rundteErr.set(g099);//ILB-459
			sv.rsuninErr.set(g099);//ILB-459
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		
		if (isEQ(sv.reserveUnitsInd, SPACES) && isNE(sv.reserveUnitsDate, varcom.vrcmMaxDate)) { 
			if(isNE(wsaaTreditionslFlag, "Y")){// ILIFE-5980
				sv.rsuninErr.set(e186);//ILB-459
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}// ILIFE-5462 end  
		
		
	}

protected void checkCalc2012()
	{
		/* IF SCRN-STATUZ                  = CALC               <A06386>*/
	//ILB-459 start
		if (isNE(chdrpf.getBillfreq(), "00")
		&& isEQ(scrnparams.statuz, varcom.calc)) {
			
			datcon2rec.intDate1.set(wsspcomn.currfrom);
			datcon2rec.frequency.set(chdrpf.getBillfreq());
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isGT(chdrpf.getPtdate(), datcon2rec.intDate2)) {
				scrnparams.errorCode.set("T045");
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
			//ILB-459 end
		}
	}

protected void next2020()
	{
		if (isNE(sv.reasoncd, SPACES)) {
			if (isEQ(sv.resndesc, SPACES)) {
				scrnparams.statuz.set(varcom.calc);
				descpf = this.getDescData("T5500", sv.reasoncd.toString().trim());
				if (descpf != null) {
					sv.resndesc.set(descpf.getLongdesc());
				}
				else {
					sv.resndesc.fill("?");
				}
			}
		}
		/*      COMPUTE S5256-CLAMAMT = WSAA-ACTUAL-TOT +               */
		/*         S5256-OTHERADJST +                             <CAS1.*/
		/*         S5256-OTHERADJST -                             <CAS1.*/
		/*         S5256-POLICYLOAN.                                    */
		if (isNE(sv.otheradjst, ZERO)) {
			//ILB-459 start
			zrdecplcPojo.setAmountIn(sv.otheradjst.getbigdata());
			zrdecplcPojo.setCurrency(sv.currcd.toString());
			/*zrdecplrec.amountIn.set(sv.otheradjst);
			zrdecplrec.currency.set(sv.currcd);*/
			callRounding5000();
			if (isNE(zrdecplcPojo.getAmountOut(), sv.otheradjst)) {
				sv.otheradjstErr.set(rfik);//ILB-459
			}
			//ILB-459 end
		}
		customerSpecificPensionSubroutineImport();		
		//ILIFE-1137
		//added for death claim flexibility
		readTabT5688250();
		if (isNE(t5688rec.dflexmeth,SPACES)){
			dthclmflxRec.chdrChdrcoy.set(chdrpf.getChdrcoy());//ILB-459
			dthclmflxRec.chdrChdrnum.set(chdrpf.getChdrnum());	//ILB-459		
			dthclmflxRec.ptdate.set(sv.ptdate);
			if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
				dthclmflxRec.dtofdeath.set(sv.dtofdeath);
			}else{
				datcon1rec.function.set(varcom.tday);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				wsaaToday.set(datcon1rec.intDate);				
				dthclmflxRec.dtofdeath.set(wsaaToday);
			}			
			dthclmflxRec.btdate.set(sv.btdate);
			dthclmflxRec.cnttype.set(sv.cnttype);
			dthclmflxRec.register.set(chdrpf.getReg());//ILB-459
			dthclmflxRec.cntcurr.set(chdrpf.getCntcurr());//ILB-459
			dthclmflxRec.effdate.set(chdrpf.getCurrfrom());//ILB-459
			callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
			if (isNE(dthclmflxRec.status, varcom.oK)) {
				syserrrec.statuz.set(dthclmflxRec.status);
				syserrrec.params.set(dthclmflxRec.dthclmflxrec);
				fatalError600();
			}	
			sv.susamt.set(dthclmflxRec.susamt);
			sv.nextinsamt.set(dthclmflxRec.nextinsamt);
			customerSpecificDeathBeneficiaryCalc();
		}	
		readCovrPF3801();
		/*BRD-140*/
		/* ILIFE-3723*/
		String policyType = chdrpf.getCnttype().trim();/*ILIFE-4839*///ILB-459
		if (policyType != null && policyType.equals("MRT")){
		if(isEQ(sv.causeofdth,"D6")){

			compute(wsaaActualTot, 2).set(wsaaFacedValue);

		} 
                 else {

			wsaaActualTot.set(wsaaoldActualTot);
		}

		if (isNE(sv.actvalue, wsaaActualTot)) {
			setUpScreenFields1870();
		} else {

			//wsspcomn.edterror.set("N");
		}
		}
		//ILIFE-8394  starts
		if(isGT(sv.susamt,ZERO)){
			sv.susamt.set(mult(sv.susamt,-1));
		}
		//ILIFE-8394  ends
		/* ILIFE-3723*/
		/*BRD-140*/
		//modified for death claim flexibility		
		//compute(sv.clamamt, 2).set(sub(add(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), sv.policyloan), sv.tdbtamt));	
		compute(sv.clamamt, 2).set(sub(add(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), sv.policyloan), sv.tdbtamt, sv.susamt, sv.nextinsamt));
		customerSpecificPensionCalc();
		sv.susamt.set(mult(sv.susamt,-1));
		/* Currency Code on the screen defaults to the Contract            */
		/* Currency                                                        */
		/*IF S5256-CURRCD                 = SPACES                     */
		/*MOVE CHDRCLM-CNTCURR        TO S5256-CURRCD.             */
		sv.currcd.set(chdrpf.getCntcurr());
		/*    IF S5256-ERROR-INDICATORS       NOT = SPACES                 */
		/*        MOVE 'Y'                    TO WSSP-EDTERROR             */
		/*        GO TO 2090-EXIT.                                         */
		if (isNE(sv.currcd, wsaaStoredCurrency2)) {
			wsaaStoredCurrency2.set(sv.currcd);
			wsaaCurrencySwitch.set(3);
			wsspcomn.edterror.set("Y");
			readjustCurrencies2100();
			sv.estimateTotalValue.set(wsaaEstimateTot);
			/*      COMPUTE S5256-CLAMAMT = WSAA-ACTUAL-TOT +         <CAS1.*/
			/*         S5256-OTHERADJST +                        <CAS1.0><02*/
			/*         S5256-OTHERADJST -                             <CAS1.*/
			/*         S5256-POLICYLOAN.                              <CAS1.*/
			/*                                                        <CAS1.*/
			//ILIFE-1137
			//added for death claim flexibility
			readTabT5688250();
			if (isNE(t5688rec.dflexmeth,SPACES)){
				dthclmflxRec.chdrChdrcoy.set(chdrpf.getChdrcoy());
				dthclmflxRec.chdrChdrnum.set(chdrpf.getChdrnum());			
				dthclmflxRec.ptdate.set(sv.ptdate);
				if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
					dthclmflxRec.dtofdeath.set(sv.dtofdeath);
				}else{
					datcon1rec.function.set(varcom.tday);
					Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
					wsaaToday.set(datcon1rec.intDate);				
					dthclmflxRec.dtofdeath.set(wsaaToday);
				}				
				dthclmflxRec.btdate.set(sv.btdate);
				dthclmflxRec.cnttype.set(sv.cnttype);
				dthclmflxRec.register.set(chdrpf.getReg());
				dthclmflxRec.cntcurr.set(chdrpf.getCntcurr());
				dthclmflxRec.effdate.set(chdrpf.getCurrfrom());
				callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
				if (isNE(dthclmflxRec.status, varcom.oK)) {
					syserrrec.statuz.set(dthclmflxRec.status);
					syserrrec.params.set(dthclmflxRec.dthclmflxrec);
					fatalError600();
				}	
				sv.susamt.set(dthclmflxRec.susamt);
				sv.nextinsamt.set(dthclmflxRec.nextinsamt);
				customerSpecificDeathBeneficiaryCalc();
			}
			
			//modified for death claim flexibility				
			//compute(sv.clamamt, 2).set(sub(add(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), sv.policyloan), sv.tdbtamt));
			compute(sv.clamamt, 2).set(sub(add(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), sv.policyloan), sv.tdbtamt, sv.susamt, sv.nextinsamt));
			customerSpecificPensionCalc();
			sv.susamt.set(mult(sv.susamt,-1));
		}
		if (isLT(sv.clamamt, 0)) {
			sv.clamamt.set(0);
		}
	}
	//BRD-140	
protected void readCovrPF3801() {
	/* CREATE */
	//ILB-459 start
	covrpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	covrpf.setChdrnum(chdrpf.getChdrnum());
	covrpfList = covrpfDAO.readCovrRecord(covrpf);
	
	for(Covrpf covrpf : covrpfList){
		if ((covrpf!=null) || isNE(covrpf.getChdrcoy(), covrpf.getChdrcoy())
				|| isNE(covrpf.getChdrnum(), covrpf.getChdrnum())) {
			return;
		}
		if ((covrpf!=null) && isEQ(covrpf.getCoverage(), "01") && isEQ(covrpf.getRider(), "00")) {
			wsaaFacedValue.set(covrIO.sumins);
		}
	}
	
	/* EXIT */
}
//ILB-459 end
protected void changeCauseOfdeath()
{
	if(  isNE(wsaaCauseofDeath,sv.causeofdth ))
	{   
		wsaaCauseofDeath.set(sv.causeofdth);
		wsaareasoncd.set(sv.reasoncd);
		wsaasusamt.set(sv.susamt);
		wsaaDateofdeath.set(sv.dtofdeath);
		initialise1000();
		sv.reasoncd.set(wsaareasoncd);
		sv.susamt.set(wsaasusamt);
		sv.causeofdth.set(wsaaCauseofDeath);
		sv.dtofdeath.set(wsaaDateofdeath);
	}
}
protected void validateSelectionFields2070()
	{
		if (isNE(sv.fupflg, "X")
		&& isNE(sv.fupflg, "+")
		&& isNE(sv.fupflg, " ")
		&& isNE(sv.fupflg, "?")) {
			sv.fupflgErr.set(g620);//ILB-459
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			changeCauseOfdeath();
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.bnfying, "X")
				&& isNE(sv.bnfying, "+")
				&& isNE(sv.bnfying, " ")) {
			sv.bnfyingErr.set(g620);
		}
		if(CMOTH003Permission){
		if (isNE(sv.investres, "X")
				&& isNE(sv.investres, "+")
				&& isNE(sv.investres, " ")) {
			sv.investresErr.set(g620);
		}
		if (isNE(sv.claimnotes, "X")
				&& isNE(sv.claimnotes, "+")
				&& isNE(sv.claimnotes, " ")) {
			sv.claimnotesErr.set(g620);
		}
		}
	}

protected void readjustCurrencies2100()
	{
		/*G0*/
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5256", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile2200();
		}
		
		if (isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void readSubfile2200()
	{
		go2250();
		updateErrorIndicators2270();
		readNextRecord2280();
	}

protected void go2250()
	{
		/*convert the estimated value if it is non zero.*/
		/*Don't convert if the value is alread in the currency required.   */
		/*    IF S5256-HEMV NOT = ZERO                                     */
		if (isEQ(sv.cnstcur, sv.currcd)) {
			wsaaEstimateTot.add(sv.hemv);
			wsaaActualTot.add(sv.hactval);
		}
		if (isNE(sv.hemv, ZERO)
		&& isNE(sv.cnstcur, sv.currcd)) {
			conlinkrec.amountIn.set(sv.hemv);
			callXcvrt2300();
			sv.estMatValue.set(conlinkrec.amountOut);
			wsaaEstimateTot.add(conlinkrec.amountOut);
			sv.cnstcur.set(conlinkrec.currOut);
		}
		/*convert the actual value if it is non zero.*/
		/*Don't convert if the value is alread in the currency required.   */
		/*    IF S5256-HACTVAL   NOT = ZERO                                */
		if (isNE(sv.hactval, ZERO)
		&& isNE(sv.cnstcur, sv.currcd)) {
			conlinkrec.amountIn.set(sv.hactval);
			callXcvrt2300();
			sv.actvalue.set(conlinkrec.amountOut);
			wsaaActualTot.add(conlinkrec.amountOut);
			sv.cnstcur.set(conlinkrec.currOut);
		}
	}

	/**
	* <pre>
	*    MOVE CLNK-CURR-OUT              TO S5256-CNSTCUR.            
	* </pre>
	*/
protected void updateErrorIndicators2270()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5256", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextRecord2280()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5256", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callXcvrt2300()
	{
		g02350();
	}

protected void g02350()
	{
		conlinkrec.function.set("CVRT");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(sv.hcnstcur);
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			scrnparams.errorCode.set(conlinkrec.statuz);
			wsspcomn.edterror.set("Y");
		}
		zrdecplrec.currency.set(sv.currcd);
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		callRounding5000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		if (isEQ(sv.fupflg, "X")) {
			return ;
		}
		if (isEQ(sv.bnfying, "X")) {
			return ;
		}
		if(CMOTH003Permission){
		if (isEQ(sv.investres, "X")) {
			return ;
		}
		if (isEQ(sv.claimnotes, "X")) {
			return ;
		}
		}
		/*   restart the subfile and read each record and update the*/
		/*   claims detail file.*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5256", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile3100();
		}
		//ILIFE-6811
		insertClmdRecord3350();  //MLIL-1403
		createClmhHeader3400();
		if(CMDTH010Permission){
		updateCattpf();
		writeCattpf();
		}
		//ILJ-383 starts
		if(CMDTH010Permission){
		createCrsvRecord3410(clmdpfList);
		}
		// ILIFE-5462 Start	
				if(isEQ(sv.reserveUnitsInd, "Y") && isNE(sv.reserveUnitsDate, 0) && 
						isNE(sv.reserveUnitsDate, varcom.maxdate)){
					writeReservePricing();
			}// ILIFE-5462 End
		/*  Update the Client Details file with the Date of Death.         */
		updateClient3600();
		/*  transfer the contract soft lock to AT*/
		if(CMOTH003Permission){
			if(isNE(wsaaNotifinum,SPACES) && isNE(wsspcomn.flag,"I")){
				clnnpfDAO.updateClnnpfClaimno(sv.claimnumber.toString(),wsaaNotifinum.toString());
			}
			if(isNE(wsaaNotifinum,SPACES) && isNE(wsspcomn.flag,"I")){
				invspfDAO.updateInvspfClaimno(sv.claimnumber.toString(),wsaaNotifinum.toString());
			}
			
		}
		
		//IBPLIFE-1702
		setupNotipf();
		if(notipf != null) {
			
			notipfDAO.updateNotipfValidFlag(expiredFlag,notipf.getNotifinum());
			//insert new Notipf record with validFlag 1;
			notipf.setValidFlag(effectiveFlag);
			notipf.setNotifistatus(claimRegi);
			addTransnoToNotipf(wsspcomn.wsaanotificationNum.toString().trim().replace(CLMPREFIX, ""));
			notipfDAO.insertNotipf(notipf);
		}
		//end
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpf.getChdrnum());//ILB-459
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrnumErr.set(f910);//ILB-459
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*  call the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5256AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		/* MOVE VRCM-TERM            TO  ATRT-REQ-TERM.                 */
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrpf.getChdrnum());//ILB-459
		wsaaFsuco.set(wsspcomn.fsuco);
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		/* MOVE VRCM-TERM            TO WSAA-TERMID.                    */
		wsaaTermid.set(varcom.vrcmTermid);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

//IBPLIFE-1702
private void addTransnoToNotipf(String notifinum) {
	int newTransno;
	String maxTransnum = notipfDAO.getMaxTransno(notifinum);
	if("".equals(maxTransnum) || maxTransnum==null) {
		notipf.setTransno(String.valueOf(noTransnoCount));
	}else {
		newTransno = Integer.valueOf(maxTransnum.trim())+noTransnoCount;	//IBPLIFE-1071
		notipf.setTransno(String.valueOf(newTransno));
	}
}

protected void readSubfile3100()
	{
		/*READ-NEXT-RECORD*/
		createClmdDetail3300();
		scrnparams.function.set(varcom.srdn);
		processScreen("S5256", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void createClmdDetail3300(){
	//ILIFE-6811
	clmdclm = new Clmdpf();
	clmdclm.setChdrcoy(wsspcomn.company.toString());
	clmdclm.setChdrnum(chdrpf.getChdrnum());//ILB-459 /* IJTI-1386 */
	/*MOVE COVRCLM-LIFE           TO CLMDCLM-LIFE.                 */
	/*MOVE COVRCLM-JLIFE          TO CLMDCLM-JLIFE.                */
	clmdclm.setLife(wsaaLife.toString());
	clmdclm.setJlife(wsaaJlife.toString());
	clmdclm.setCoverage(sv.hcover.toString());
	if (isEQ(sv.rider, SPACES)) {
		clmdclm.setRider("00");
	}
	else {
		clmdclm.setRider(sv.rider.toString());
	}
	/*    MOVE S5256-CRTABLE          TO CLMDCLM-CRTABLE.*/
	clmdclm.setCrtable(sv.hcrtable.toString());
	clmdclm.setShortds(sv.shortds.toString());
	clmdclm.setTranno(chdrpf.getTranno());//ILB-459
	clmdclm.setLiencd(sv.liencd.toString());
	/*   MOVE S5256-RIIND            TO CLMDCLM-RIIND.                */
	clmdclm.setCnstcur(sv.cnstcur.toString());
	clmdclm.setEstMatValue(sv.estMatValue.toDouble());
	clmdclm.setActvalue(sv.actvalue.toDouble());
	clmdclm.setVirtualFund(sv.vfund.toString());
	clmdclm.setFieldType(sv.htype.toString());
	clmdclm.setAnnypind(deathrec.processInd.toString());
	clmdclm.setValidflag("1");
	if(CMDTH010Permission){
	clmdclm.setClaimno(sv.claimnumber.toString());
	}
	if(CMOTH003Permission){
		clmdclm.setClaimno(sv.claimnumber.toString());
		clmdclm.setClaimnotifino(wsaaNotifinum.toString());
	}
	clmdpfList.add(clmdclm);
}

protected void createClmhHeader3400(){
	//ILIFE-6811
	clmhclm = new Clmhpf();
	clmhclm.setChdrcoy(wsspcomn.company.toString());
	clmhclm.setChdrnum(chdrpf.getChdrnum());//ILB-459 /* IJTI-1386 */
	clmhclm.setLife(wsaaLife.toString());
	clmhclm.setJlife(wsaaJlife.toString());
	clmhclm.setCurrcd(sv.currcd.toString());
	clmhclm.setDtofdeath(sv.dtofdeath.toInt());
	clmhclm.setEffdate(sv.effdate.toInt());
	clmhclm.setTranno(chdrpf.getTranno());//ILB-459
	clmhclm.setCnttype(chdrpf.getCnttype());//ILB-459 /* IJTI-1386 */
	clmhclm.setReasoncd(sv.reasoncd.toString());
	clmhclm.setResndesc(sv.resndesc.toString());
	clmhclm.setPolicyloan(sv.policyloan.getbigdata());
	clmhclm.setTdbtamt(sv.tdbtamt.getbigdata());
	clmhclm.setZrcshamt(sv.zrcshamt.getbigdata());
	clmhclm.setCauseofdth(sv.causeofdth.toString());
	clmhclm.setOtheradjst(sv.otheradjst.getbigdata());
	clmhclm.setValidflag("1");
	clmhclm.setIntdays(0);
	clmhclm.setInterest(new BigDecimal(0));
	clmhclm.setOfcharge(new BigDecimal(0));
	clmhclm.setCurrfrom(0);
	clmhclm.setCurrto(0);
	clmhclm.setDtereg(wsaaToday.toInt());
	clmhclm.setTrdt(varcom.vrcmDate.toInt());
	clmhclm.setTrtm(varcom.vrcmTime.toInt());
	clmhclm.setProceeds(sv.clamamt.getbigdata());
	clmhclm.setDteappr(0);
	if(CMDTH010Permission){
	clmhclm.setClaim(sv.claimnumber.toString());
	clmhclm.setClamval(BigDecimal.ZERO);
	clmhclm.setClamstat(sv.claimStat.toString());
	clmhclm.setClamtyp(sv.claimTyp.toString());
	clmhclm.setCondte(sv.contactDate.toInt());
	clmhclm.setZclaimer(sv.lifcnum.toString());
	clmhclm.setUserT(varcom.vrcmUser.toInt());
	
	}
	clmhpfDAO.insert(clmhclm);
}
protected void createCrsvRecord3410(List<Clmdpf> clmdpfList){
	BigDecimal actualsTotal = BigDecimal.ZERO;
	List<Crsvpf> crsvpflist = crsvpfDAO.getCrsvpfRecord(chdrpf.getChdrnum(), "L", "1", "TASO");
	if(null != crsvpflist  && !crsvpflist.isEmpty()){
		crsvpfDAO.updateValidFlgCrsvRecord(crsvpflist.get(0),"2");
	}
	Crsvpf crsvpf = new Crsvpf();
	crsvpf.setWclmcoy("L");
	crsvpf.setChdrnum(chdrpf.getChdrnum());
	crsvpf.setTranno(chdrpf.getTranno());
	for(Clmdpf clmdpf:clmdpfList){
		actualsTotal=actualsTotal.add(BigDecimal.valueOf(clmdpf.getActvalue()));
	}
	crsvpf.setBalo(actualsTotal);
	crsvpf.setValidflag("1");
	crsvpf.setCondte(sv.contactDate.toInt());
	crsvpf.setClaim(sv.claimnumber.toString());
	crsvpf.setWclmpfx("CL");
	crsvpf.setWprcl("LIFE");
	crsvpf.setWrscd("**");
	crsvpf.setTrcode(wsaaBatckey.batcBatctrcde.toString());
	crsvpf.setTrdt(wsaaToday.toInt());
	crsvpf.setTrtm(varcom.vrcmTime.toInt());
	crsvpf.setUserT(wsaaUser.toInt());
	crsvpf.setTermid(wsaaTermid.toString());
	crsvpfDAO.insertCrsvRecord(crsvpf);
	}
	//ILJ-383 ends
protected void updateCattpf(){
  int count = cattpfDAO.updateCattpf(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
}
protected void writeCattpf(){
	Cattpf cattpf = new Cattpf();
	cattpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	cattpf.setChdrnum(chdrpf.getChdrnum());
	cattpf.setClaim(sv.claimnumber.toString());
	cattpf.setClamamt(BigDecimal.ZERO);
	cattpf.setClamstat(sv.claimStat.toString());
	cattpf.setClamtyp(sv.claimTyp.toString());
	cattpf.setCoverage(sv.coverage.toString());
	cattpf.setCauseofdth(sv.causeofdth.toString());
	cattpf.setDoceffdate(99999999);
	cattpf.setDtofdeath(sv.dtofdeath.toInt());
	cattpf.setEffdate(sv.effdate.toInt());
	cattpf.setFlag(" ");
	cattpf.setFulfilltrm(" ");
	cattpf.setIntbasedt(99999999);
	cattpf.setIntcalflag(" ");
	cattpf.setJlife(sv.jlifcnum.toString());
	cattpf.setKariflag(" ");
	cattpf.setKpaydate(99999999);
	cattpf.setLifcnum(sv.lifcnum.toString());	
	cattpf.setLife(lifepf.getLife());
	cattpf.setRider(covrpf.getRider());
	cattpf.setSeqenum("000");
	cattpf.setTranno(chdrpf.getTranno());
	cattpf.setTrcode(wsaaBatckey.batcBatctrcde.toString());
	cattpf.setValidflag("1");
	cattpf.setCondte(sv.contactDate.toInt());//ILJ-487
	cattpfDAO.insertCattpfRecord(cattpf);
}


	/**
	* <pre>
	*  REDUNDANT CODE FOLLOWS.                                        
	*3500-CALL-XCVRT  SECTION.                                        
	*3510-SCREEN-IO.                                                  
	*3590-EXIT.                                                       
	*     EXIT.                                                       
	* </pre>
	*/
protected void updateClient3600(){
	//ILIFE-6811
	clntpfDao.updateClntDeathDate(wsaaDeadLife.toString(), wsspcomn.fsuco.toString(), "CN", sv.dtofdeath.toString(),varcom.vrcmMaxDate.toString());
	clmdpfList.clear();
}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		/*    Catering for F11.                                            */
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			/*    Unlock contract.                                             */
			sftlockrec.function.set("UNLK");
			sftlockrec.statuz.set(varcom.oK);
			sftlockrec.company.set(wsspcomn.company);
			sftlockrec.enttyp.set("CH");
			sftlockrec.entity.set(chdrpf.getChdrnum());//ILB-459
			sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
			sftlockrec.user.set(varcom.vrcmUser);
			callProgram(Sftlock.class, sftlockrec.sftlockRec);
			if (isNE(sftlockrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
				return ;
			}
		}
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		/*  If first time into this section (stack action blank)*/
		/*  save next eight programs in stack*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		if (isEQ(sv.fupflg, "?")) {
			checkFollowups1850();
		}
		if (isEQ(sv.fupflg, "X")) {
			gensswrec.function.set("A");
			sv.fupflg.set("?");
			callGenssw4300();
			return ;
		}
		/*  Check if beneficiary selected previously*/
        if (isEQ(sv.bnfying, "?")) {
            checkBeneficiaries3800();
        }
		if (isEQ(sv.bnfying, "X")) {
			wsaanewActualTot.set(sv.clamamt);
			sd5jl.totalAmount.set(wsaanewActualTot);
			gensswrec.function.set("B");
			sv.bnfying.set("?");
			callGenssw4300();
			return;
		}
		if(CMOTH003Permission){
	    if (isEQ(sv.investres, "?")) {
	    	checkInvspf();
		}
		if (isEQ(sv.investres, "X")) {
			gensswrec.function.set("C");
			sv.investres.set("?");
			setupNotipf();
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.claimnotes, "?")) {
				updateClaimnoInAll();
		}
		if (isEQ(sv.claimnotes, "X")) {
			gensswrec.function.set("D");
			sv.claimnotes.set("?");
			setupNotipf();
			callGenssw4300();
			return ;
		}
		}
		/*   No more selected (or none)*/
		/*      - restore stack form wsaa to wssp*/
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		/*  If current stack action is * then re-display screen*/
		/*     (in this case, some other option(s) were requested)*/
		/*  Otherwise continue as normal.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}
	protected void setupNotipf(){
		wsspcomn.chdrCownnum.set(sv.lifcnum.toString());	
		wsspcomn.wsaaclaimno.set(sv.claimnumber.toString().trim());
		if(isEQ(sv.notifinumber,SPACES)){
			wsspcomn.wsaarelationship.set(SPACES);
			wsspcomn.wsaaclaimant.set(sv.lifcnum.toString());
			wsspcomn.wsaanotificationNum.set(SPACES);
		}
		else{
			notipf = notipfDAO.getNotiReByNotifin(wsaaNotifinum.toString(),wsspcomn.company.toString());
			wsspcomn.wsaarelationship.set(notipf.getRelationcnum());
			wsspcomn.wsaaclaimant.set(notipf.getClaimant());
			wsspcomn.wsaanotificationNum.set(sv.notifinumber.toString());
		}
	}
	protected void updateClaimnoInAll(){
		
		if(clnnpfList1!=null && !clnnpfList1.isEmpty()){
			sv.claimnotes.set("+");
		}
		else{
			sv.claimnotes.set(SPACES);
		}
	}
	protected void checkInvspf(){
		
		if(invspfList1!=null && !invspfList1.isEmpty()){
			sv.investres.set("+");
		}
		else{
			sv.investres.set(SPACES);
		}
	}
    protected void checkBeneficiaries3800()
    {
		List<Cpbnfypf> cpbnfypfList = cpbnfypfDAO.searchCpbnfypfRecord(wsspcomn.company.toString(),chdrpf.getChdrnum());
		if (cpbnfypfList!=null && !cpbnfypfList.isEmpty()) {
			if ((isNE(cpbnfypfList.get(0).getChdrcoy(), chdrpf.getChdrcoy()))
					|| (isNE(cpbnfypfList.get(0).getChdrnum(), chdrpf.getChdrnum()))) {
				sv.bnfying.set(SPACES);
			} else {
				sv.bnfying.set("+");
			}
		}
    }
protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
		callSubroutine4310();
	}

protected void callSubroutine4310()
	{
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.params.set(gensswrec.gensswRec);
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(h093);//ILB-459
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/*    load from gensw to wssp*/
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void loadProgram4400()
	{
		/*RESTORE1*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT1*/
	}

protected void callRounding5000()
	{
		/*CALL*/
	//ILB-459 start
	zrdecplcPojo.setFunction(SPACES.toString());
	zrdecplcPojo.setCompany(wsspcomn.company.toString());
	zrdecplcPojo.setStatuz(Varcom.oK.toString());
	zrdecplcPojo.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
	if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
		syserrrec.statuz.set(zrdecplcPojo.getStatuz());
		syserrrec.params.set(zrdecplcPojo.toString());
		fatalError600();
	}
		/*EXIT*/
	//ILB-459 end
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
//ILB-459 start
/*private static final class ErrorsInner { 
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData h072 = new FixedLengthStringData(4).init("H072");
	private FixedLengthStringData h073 = new FixedLengthStringData(4).init("H073");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData j008 = new FixedLengthStringData(4).init("J008");
	private FixedLengthStringData t045 = new FixedLengthStringData(4).init("T045");
	private FixedLengthStringData t064 = new FixedLengthStringData(4).init("T064");
	private FixedLengthStringData hl08 = new FixedLengthStringData(4).init("HL08");
	private FixedLengthStringData h355 = new FixedLengthStringData(4).init("H355");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData h359 = new FixedLengthStringData(4).init("H359");
	private FixedLengthStringData rlcc = new FixedLengthStringData(4).init("RLCC");
	private FixedLengthStringData g099 = new FixedLengthStringData(4).init("G099");
}*/
//ILB-459 end
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData flupclmrec = new FixedLengthStringData(10).init("FLUPCLMREC");
	private FixedLengthStringData clmhclmrec = new FixedLengthStringData(10).init("CLMHCLMREC");
	
	private FixedLengthStringData clmdclmrec = new FixedLengthStringData(10).init("CLMDCLMREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData incrmjarec = new FixedLengthStringData(10).init("INCRMJAREC");
	private FixedLengthStringData hitrrnlrec = new FixedLengthStringData(10).init("HITRRNLREC");
	private FixedLengthStringData tpoldbtrec = new FixedLengthStringData(10).init("TPOLDBTREC");	
	private FixedLengthStringData covtcsnrec = new FixedLengthStringData(10).init("COVTCSNREC"); /*BRD-140*/
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC"); /*BRD-140*/
	private FixedLengthStringData chdrclmrec = new FixedLengthStringData(10).init("CHDRCLMREC");//ILB-459
	
	public FixedLengthStringData getClmhclmrec() {
		return clmhclmrec;
	}
	public void setClmhclmrec(FixedLengthStringData clmhclmrec) {
		this.clmhclmrec = clmhclmrec;
	}
}
//ILIFE-1137
protected void readTabT5688250()
{
	read251();
	readT66402986();//ILIFE-5980
}

//ILIFE-6296 by wli31
protected void read251(){
	itempfList = itempfDAO.getItdmByFrmdate(chdrpf.getChdrcoy().toString().trim(),"T5688",chdrpf.getCnttype().trim(),sv.effdate.toInt());/* IJTI-1386 */

	if(itempfList.size() > 0) {
		for (Itempf it : itempfList) {				
			t5688rec.t5688Rec.set(StringUtil.rawToString(it.getGenarea()));
			}
		}else{
		syserrrec.statuz.set(e308);
		//ILFIE-6406 by mverma54
		//fatalError600();
	}

}

/**
 * ILIFE-5462 Start
 */
protected void writeReservePricing(){
	
	zutrpf.setChdrpfx("CH");
	zutrpf.setChdrcoy(wsspcomn.company.toString().trim());
	zutrpf.setChdrnum(chdrpf.getChdrnum().trim());//ILB-459 /* IJTI-1386 */
	zutrpf.setLife(wsaaLife.toString().trim());	
	zutrpf.setCoverage(sv.coverage.toString().trim());
	zutrpf.setRider(sv.rider.toString().trim());
	zutrpf.setPlanSuffix(0);//TODO
	zutrpf.setTranno(chdrpf.getTranno()); //ILB-459
	zutrpf.setReserveUnitsInd(sv.reserveUnitsInd.toString().trim());
	zutrpf.setReserveUnitsDate(sv.reserveUnitsDate.toInt());
	zutrpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());	
	zutrpf.setTransactionDate(varcom.vrcmDate.toInt());
	zutrpf.setTransactionTime(varcom.vrcmTime.toInt());
	zutrpf.setUser(varcom.vrcmUser.toInt());
	zutrpf.setEffdate(sv.effdate.toInt());
	zutrpf.setValidflag("1");
	zutrpf.setDatesub(wsaaToday.toInt()); 
	zutrpf.setCrtuser(wsspcomn.userid.toString());  
	zutrpf.setUserProfile(wsspcomn.userid.toString()); 
	zutrpf.setJobName(appVars.getLoggedOnUser());
	zutrpf.setDatime("");
	
	try{
		zutrpfDAO.insertZutrpfRecord(zutrpf);
	}catch(Exception e){
		LOGGER.error("Exception occured in writeReservePricing()",e);
		fatalError600();
	}
	
}// ILIFE-5462 End

//ILIFE-5980 Start 
protected void readT66402986()
{
	itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),"T6640",t6640Item,sv.effdate.toInt());
	
	if (itempfList.size() >= 1 ){
		wsaaTreditionslFlag.set("Y");
	}
	else {
		wsaaTreditionslFlag.set("N");
	}



	}//ILIFE-5980 End

	public String getSplitSign() {
		return SPLIT_SIGN;
	}
	public StringUtil getStringUtil() {
		return stringUtil;
	}
	
	protected void customerSpecificPensionCalc() {
		
	}
	
	protected void customerSpecificPensionSubroutineImport() {
		
	}
	
	protected void customerSpecificJointLife() {
		
	}
	protected void setPensionDetailsCustomerSpecific(){
		
	}
	protected void insertClmdRecord3350() {
		clmdpfDAO.insertClmdRecord(clmdpfList);
	}
	protected void customerSpecificDeathBeneficiaryCalc(){
		
	}
}

