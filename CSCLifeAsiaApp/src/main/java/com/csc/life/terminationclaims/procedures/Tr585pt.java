/*
 * File: Tr585pt.java
 * Date: 30 August 2009 2:43:21
 * Author: Quipoz Limited
 * 
 * Class transformed from TR585PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.terminationclaims.tablestructures.Tr585rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR585.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr585pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine001, 32, FILLER).init("Accident Benefit Plan                  SR585");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(77);
	private FixedLengthStringData filler7 = new FixedLengthStringData(77).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Rider                                 Benefit     Max      Max  Double");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(79);
	private FixedLengthStringData filler8 = new FixedLengthStringData(79).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Benefit Description                  %SA/Unit Unit Unit   Amount Indemnity");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(76);
	private FixedLengthStringData filler9 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 1);
	private FixedLengthStringData filler10 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine005, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine005, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 48);
	private FixedLengthStringData filler12 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 52).setPattern("ZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine005, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 75);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(76);
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 1);
	private FixedLengthStringData filler16 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine006, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 48);
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 52).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine006, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 75);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(76);
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 1);
	private FixedLengthStringData filler22 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine007, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 48);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 52).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine007, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 75);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(76);
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 1);
	private FixedLengthStringData filler28 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine008, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 48);
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 52).setPattern("ZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine008, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 75);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(76);
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 1);
	private FixedLengthStringData filler34 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine009, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 48);
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 52).setPattern("ZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine009, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 75);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(76);
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 1);
	private FixedLengthStringData filler40 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine010, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 48);
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 52).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine010, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 75);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(76);
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 1);
	private FixedLengthStringData filler46 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine011, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 48);
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 52).setPattern("ZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine011, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 75);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(76);
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 1);
	private FixedLengthStringData filler52 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine012, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 48);
	private FixedLengthStringData filler54 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 52).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine012, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 75);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(76);
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 1);
	private FixedLengthStringData filler58 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine013, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 48);
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 52).setPattern("ZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine013, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 75);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(76);
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 1);
	private FixedLengthStringData filler64 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine014, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 48);
	private FixedLengthStringData filler66 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 52).setPattern("ZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine014, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 75);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(76);
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 1);
	private FixedLengthStringData filler70 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine015, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 48);
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 52).setPattern("ZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine015, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 75);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(76);
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine016, 1);
	private FixedLengthStringData filler76 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine016, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 48);
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 52).setPattern("ZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine016, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine016, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 75);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(76);
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine017, 1);
	private FixedLengthStringData filler82 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine017, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 48);
	private FixedLengthStringData filler84 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 52).setPattern("ZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine017, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 75);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(76);
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine018, 1);
	private FixedLengthStringData filler88 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine018, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine018, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 48);
	private FixedLengthStringData filler90 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine018, 52).setPattern("ZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine018, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine018, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 75);

	private FixedLengthStringData wsaaPrtLine019 = new FixedLengthStringData(76);
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine019, 1);
	private FixedLengthStringData filler94 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine019, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine019, 40).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine019, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine019, 48);
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine019, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine019, 52).setPattern("ZZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(12, 2).isAPartOf(wsaaPrtLine019, 56).setPattern("ZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine019, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo094 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine019, 75);

	private FixedLengthStringData wsaaPrtLine020 = new FixedLengthStringData(76);
	private FixedLengthStringData filler99 = new FixedLengthStringData(68).isAPartOf(wsaaPrtLine020, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt                   Continuation Item :");
	private FixedLengthStringData fieldNo095 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine020, 68);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr585rec tr585rec = new Tr585rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr585pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr585rec.tr585Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(tr585rec.acdben01);
		fieldNo006.set(tr585rec.dfclmpct01);
		fieldNo007.set(tr585rec.benfreq01);
		fieldNo008.set(tr585rec.mxbenunt01);
		fieldNo010.set(tr585rec.gcdblind01);
		fieldNo011.set(tr585rec.acdben02);
		fieldNo012.set(tr585rec.dfclmpct02);
		fieldNo013.set(tr585rec.benfreq02);
		fieldNo014.set(tr585rec.mxbenunt02);
		fieldNo016.set(tr585rec.gcdblind02);
		fieldNo017.set(tr585rec.acdben03);
		fieldNo018.set(tr585rec.dfclmpct03);
		fieldNo019.set(tr585rec.benfreq03);
		fieldNo020.set(tr585rec.mxbenunt03);
		fieldNo022.set(tr585rec.gcdblind03);
		fieldNo023.set(tr585rec.acdben04);
		fieldNo024.set(tr585rec.dfclmpct04);
		fieldNo025.set(tr585rec.benfreq04);
		fieldNo026.set(tr585rec.mxbenunt04);
		fieldNo028.set(tr585rec.gcdblind04);
		fieldNo029.set(tr585rec.acdben05);
		fieldNo030.set(tr585rec.dfclmpct05);
		fieldNo031.set(tr585rec.benfreq05);
		fieldNo032.set(tr585rec.mxbenunt05);
		fieldNo034.set(tr585rec.gcdblind05);
		fieldNo035.set(tr585rec.acdben06);
		fieldNo036.set(tr585rec.dfclmpct06);
		fieldNo037.set(tr585rec.benfreq06);
		fieldNo038.set(tr585rec.mxbenunt06);
		fieldNo040.set(tr585rec.gcdblind06);
		fieldNo041.set(tr585rec.acdben07);
		fieldNo042.set(tr585rec.dfclmpct07);
		fieldNo043.set(tr585rec.benfreq07);
		fieldNo044.set(tr585rec.mxbenunt07);
		fieldNo046.set(tr585rec.gcdblind07);
		fieldNo047.set(tr585rec.acdben08);
		fieldNo048.set(tr585rec.dfclmpct08);
		fieldNo049.set(tr585rec.benfreq08);
		fieldNo050.set(tr585rec.mxbenunt08);
		fieldNo052.set(tr585rec.gcdblind08);
		fieldNo053.set(tr585rec.acdben09);
		fieldNo054.set(tr585rec.dfclmpct09);
		fieldNo055.set(tr585rec.benfreq09);
		fieldNo056.set(tr585rec.mxbenunt09);
		fieldNo058.set(tr585rec.gcdblind09);
		fieldNo059.set(tr585rec.acdben10);
		fieldNo060.set(tr585rec.dfclmpct10);
		fieldNo061.set(tr585rec.benfreq10);
		fieldNo062.set(tr585rec.mxbenunt10);
		fieldNo064.set(tr585rec.gcdblind10);
		fieldNo065.set(tr585rec.acdben11);
		fieldNo066.set(tr585rec.dfclmpct11);
		fieldNo067.set(tr585rec.benfreq11);
		fieldNo068.set(tr585rec.mxbenunt11);
		fieldNo070.set(tr585rec.gcdblind11);
		fieldNo071.set(tr585rec.acdben12);
		fieldNo072.set(tr585rec.dfclmpct12);
		fieldNo073.set(tr585rec.benfreq12);
		fieldNo074.set(tr585rec.mxbenunt12);
		fieldNo076.set(tr585rec.gcdblind12);
		fieldNo077.set(tr585rec.acdben13);
		fieldNo078.set(tr585rec.dfclmpct13);
		fieldNo079.set(tr585rec.benfreq13);
		fieldNo080.set(tr585rec.mxbenunt13);
		fieldNo082.set(tr585rec.gcdblind13);
		fieldNo083.set(tr585rec.acdben14);
		fieldNo084.set(tr585rec.dfclmpct14);
		fieldNo085.set(tr585rec.benfreq14);
		fieldNo086.set(tr585rec.mxbenunt14);
		fieldNo088.set(tr585rec.gcdblind14);
		fieldNo089.set(tr585rec.acdben15);
		fieldNo090.set(tr585rec.dfclmpct15);
		fieldNo091.set(tr585rec.benfreq15);
		fieldNo092.set(tr585rec.mxbenunt15);
		fieldNo094.set(tr585rec.gcdblind15);
		fieldNo095.set(tr585rec.contitem);
		fieldNo009.set(tr585rec.amtfld01);
		fieldNo015.set(tr585rec.amtfld02);
		fieldNo021.set(tr585rec.amtfld03);
		fieldNo027.set(tr585rec.amtfld04);
		fieldNo033.set(tr585rec.amtfld05);
		fieldNo039.set(tr585rec.amtfld06);
		fieldNo045.set(tr585rec.amtfld07);
		fieldNo051.set(tr585rec.amtfld08);
		fieldNo057.set(tr585rec.amtfld09);
		fieldNo063.set(tr585rec.amtfld10);
		fieldNo069.set(tr585rec.amtfld11);
		fieldNo075.set(tr585rec.amtfld12);
		fieldNo081.set(tr585rec.amtfld13);
		fieldNo087.set(tr585rec.amtfld14);
		fieldNo093.set(tr585rec.amtfld15);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine019);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine020);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
