package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh612screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh612ScreenVars sv = (Sh612ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh612screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh612ScreenVars screenVars = (Sh612ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrsel.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.action.setClassString("");
		screenVars.fuflag.setClassString("");
		screenVars.uworflag.setClassString("");
		screenVars.uworreason.setClassString("");
	}

/**
 * Clear all the variables in Sh612screen
 */
	public static void clear(VarModel pv) {
		Sh612ScreenVars screenVars = (Sh612ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrsel.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.action.clear();
		screenVars.fuflag.clear();
		screenVars.uworflag.clear();
		screenVars.uworreason.clear();
	}
}
