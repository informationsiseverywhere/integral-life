package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR689
 * @version 1.0 generated on 30/08/09 07:24
 * @author Quipoz
 */
public class Sr689ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(552);
	public FixedLengthStringData dataFields = new FixedLengthStringData(216).isAPartOf(dataArea, 0);
	public ZonedDecimalData amtfld = DD.amtfld.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData amtlife = DD.amtlife.copyToZonedDecimal().isAPartOf(dataFields,12);
	public ZonedDecimalData amtyear = DD.amtyear.copyToZonedDecimal().isAPartOf(dataFields,22);
	public FixedLengthStringData benefits = DD.benefits.copy().isAPartOf(dataFields,32);
	public ZonedDecimalData benlmt = DD.benlmt.copyToZonedDecimal().isAPartOf(dataFields,61);
	public FixedLengthStringData comt = DD.comt.copy().isAPartOf(dataFields,71);
	public ZonedDecimalData copay = DD.copay.copyToZonedDecimal().isAPartOf(dataFields,101);
	public ZonedDecimalData daclaim = DD.daclaim.copyToZonedDecimal().isAPartOf(dataFields,104);
	public ZonedDecimalData datefrom = new ZonedDecimalData(8, 0).isAPartOf(dataFields, 107);
	public ZonedDecimalData dateto = DD.dateto.copyToZonedDecimal().isAPartOf(dataFields,115);
	public ZonedDecimalData gcnetpy = DD.gcnetpy.copyToZonedDecimal().isAPartOf(dataFields,123);
	public ZonedDecimalData gdeduct = DD.gdeduct.copyToZonedDecimal().isAPartOf(dataFields,135);
	public ZonedDecimalData gincurr = DD.gincurr.copyToZonedDecimal().isAPartOf(dataFields,145);
	public ZonedDecimalData gtotinc = DD.gtotinc.copyToZonedDecimal().isAPartOf(dataFields,158);
	public FixedLengthStringData hosben = DD.hosben.copy().isAPartOf(dataFields,171);
	public FixedLengthStringData inqopt = DD.inqopt.copy().isAPartOf(dataFields,176);
	public ZonedDecimalData nofday = DD.nofday.copyToZonedDecimal().isAPartOf(dataFields,184);
	public ZonedDecimalData tamtyear = DD.tamtyear.copyToZonedDecimal().isAPartOf(dataFields,187);
	public ZonedDecimalData tclmamt = DD.tclmamt.copyToZonedDecimal().isAPartOf(dataFields,199);
	public ZonedDecimalData zdaycov = DD.zdaycov.copyToZonedDecimal().isAPartOf(dataFields,211);
	public ZonedDecimalData zunit = DD.zunit.copyToZonedDecimal().isAPartOf(dataFields,214);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 216);
	public FixedLengthStringData amtfldErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData amtlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData amtyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData benefitsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData benlmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData comtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData copayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData daclaimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData datefromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData datetoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData gcnetpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData gdeductErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData gincurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData gtotincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData hosbenErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData inqoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData nofdayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData tamtyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData tclmamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData zdaycovErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData zunitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(252).isAPartOf(dataArea, 300);
	public FixedLengthStringData[] amtfldOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] amtlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] amtyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] benefitsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] benlmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] comtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] copayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] daclaimOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] datefromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] datetoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] gcnetpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] gdeductOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] gincurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] gtotincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] hosbenOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] inqoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] nofdayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] tamtyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] tclmamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] zdaycovOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] zunitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData datefromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData datetoDisp = new FixedLengthStringData(10);

	public LongData Sr689screenWritten = new LongData(0);
	public LongData Sr689windowWritten = new LongData(0);
	public LongData Sr689hideWritten = new LongData(0);
	public LongData Sr689protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr689ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(datefromOut,new String[] {"20","70","-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datetoOut,new String[] {"21","70","-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gincurrOut,new String[] {"22","70","-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(daclaimOut,new String[] {"23","71","-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gcnetpyOut,new String[] {"24","70","-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comtOut,new String[] {null, null, null, "75",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {inqopt, hosben, benefits, zunit, datefrom, dateto, gincurr, daclaim, benlmt, nofday, gtotinc, zdaycov, gdeduct, copay, tamtyear, amtyear, tclmamt, amtlife, amtfld, gcnetpy, comt};
		screenOutFields = new BaseData[][] {inqoptOut, hosbenOut, benefitsOut, zunitOut, datefromOut, datetoOut, gincurrOut, daclaimOut, benlmtOut, nofdayOut, gtotincOut, zdaycovOut, gdeductOut, copayOut, tamtyearOut, amtyearOut, tclmamtOut, amtlifeOut, amtfldOut, gcnetpyOut, comtOut};
		screenErrFields = new BaseData[] {inqoptErr, hosbenErr, benefitsErr, zunitErr, datefromErr, datetoErr, gincurrErr, daclaimErr, benlmtErr, nofdayErr, gtotincErr, zdaycovErr, gdeductErr, copayErr, tamtyearErr, amtyearErr, tclmamtErr, amtlifeErr, amtfldErr, gcnetpyErr, comtErr};
		screenDateFields = new BaseData[] {datefrom, dateto};
		screenDateErrFields = new BaseData[] {datefromErr, datetoErr};
		screenDateDispFields = new BaseData[] {datefromDisp, datetoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr689screen.class;
		protectRecord = Sr689protect.class;
		hideRecord = Sr689hide.class;
	}

}
