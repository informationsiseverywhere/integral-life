package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;
	
	public class Sa510screensfl extends Subfile { 

		public static final String ROUTINE = QPUtilities.getThisClass();
		public static final boolean overlay = false;
		public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
		public static int maxRecords = 10;
		public static int nextChangeIndicator = 94;
		public static int[] affectedInds = new int[] {1}; 

		public static RecInfo lrec = new RecInfo();
		static {
			COBOLAppVars.recordSizes.put(ROUTINE, new int[] {13, 21, 3, 63}); 
		}

	/**
	 * Writes a record to the screen.
	 * @param errorInd - will be set on if an error occurs
	 * @param noRecordFoundInd - will be set on if no changed record is found
	 */
		public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
			Sa510ScreenVars sv = (Sa510ScreenVars) pv;
			if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sa510screensfl.getRowCount())) {
				ind3.setOn();
				return;
			}
			TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sa510screensfl, 
				sv.Sa510screensflWritten , ind2, ind3, maxRecords);
			if (ind2.isOn() || ind3.isOn()) {
				return;
			}
			setSubfileData(tm.bufferedRow, av, pv);
			if (av.getInd(nextChangeIndicator)) {
				tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
			} else {
				tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
			}
			clearInds(av, pfInds);
			tm.write();
		}

		public static void update(COBOLAppVars av, VarModel pv,
			Indicator ind2) {
			Sa510ScreenVars sv = (Sa510ScreenVars) pv;
			TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sa510screensfl, ind2);
			setSubfileData(tm.bufferedRow, av, pv);
			if (av.getInd(nextChangeIndicator)) {
				tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
			} else {
				tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
			}
			tm.update();
		}

		public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3, DecimalData sflIndex) {
			Sa510ScreenVars sv = (Sa510ScreenVars) pv;
			DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sa510screensfl, ind2, ind3, sflIndex);
			getSubfileData(dm, av, pv);
			// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
			// we return to the start of the subfile for subsequent calls
			if (ind3.isOn() && sv.Sa510screensflWritten.gt(0))
			{
				sv.sa510screensfl.setCurrentIndex(0);
				sv.Sa510screensflWritten.set(0);
			}
			restoreInds(dm, av, affectedInds);
		}

		public static void chain(COBOLAppVars av, VarModel pv,
			int record, Indicator ind2, Indicator ind3) {
			Sa510ScreenVars sv = (Sa510ScreenVars) pv;
			DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sa510screensfl, record, ind2, ind3);
			getSubfileData(dm, av, pv);
			restoreInds(dm, av, affectedInds);
		}

		public static void chain(COBOLAppVars av, VarModel pv,
			BaseData record, Indicator ind2, Indicator ind3) {
			chain(av, pv, record.toInt(), ind2, ind3);
		}
		public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
			int record, Indicator ind2, Indicator ind3) {
			av.COBOLFileError = false;
			chain(av, pv, record, ind2, ind3);
			if (ind3.isOn()) av.COBOLFileError = true;
		}

		public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
			BaseData record, Indicator ind2, Indicator ind3) {
			chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
		}

		public static void getSubfileData(DataModel dm, COBOLAppVars av,
			 VarModel pv) {
			if (dm != null) {
				Sa510ScreenVars screenVars = (Sa510ScreenVars) pv;
				if (screenVars.screenIndicArea.getFieldName() == null) {
					screenVars.screenIndicArea.setFieldName("screenIndicArea");
					screenVars.select.setFieldName("select");
					screenVars.sequence.setFieldName("sequence");
					screenVars.accdesc.setFieldName("accdesc");
					screenVars.date.setFieldName("date");
					screenVars.acctime.setFieldName("acctime");
					screenVars.userID.setFieldName("userID");
				}
				screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
				screenVars.select.set(dm.getField("select"));
				screenVars.sequence.set(dm.getField("sequence"));
				screenVars.accdesc.set(dm.getField("accdesc"));
				screenVars.date.set(dm.getField("date"));
				screenVars.acctime.set(dm.getField("acctime"));
				screenVars.userID.set(dm.getField("userID"));
			}
		}

		public static void setSubfileData(DataModel dm, COBOLAppVars av,
			 VarModel pv) {
			if (dm != null) {
				Sa510ScreenVars screenVars = (Sa510ScreenVars) pv;
				if (screenVars.screenIndicArea.getFieldName() == null) {
					screenVars.screenIndicArea.setFieldName("screenIndicArea");
					screenVars.select.setFieldName("select");
					screenVars.sequence.setFieldName("sequence");
					screenVars.accdesc.setFieldName("accdesc");
					screenVars.date.setFieldName("date");
					screenVars.acctime.setFieldName("acctime");
					screenVars.userID.setFieldName("userID");
				}
				dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
				dm.getField("select").set(screenVars.select);
				dm.getField("sequence").set(screenVars.sequence);
				dm.getField("accdesc").set(screenVars.accdesc);
				dm.getField("date").set(screenVars.date);
				dm.getField("acctime").set(screenVars.acctime);
				dm.getField("userID").set(screenVars.userID);
			}
		}

		public static String getRecName() {
			return ROUTINE;
		}

		public static int getMaxRecords() {
			return maxRecords;
		}

		public static void getMaxRecords(int maxRecords) {
			Sa510screensfl.maxRecords = maxRecords;
		}

		public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
			gt.set1stScreenRow();
			getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
			restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
			clearFormatting(pv);
		}

		public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
			gt.setNextScreenRow();
			getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
			restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
			clearFormatting(pv);
		}

		public static void clearFormatting(VarModel pv) {
			Sa510ScreenVars screenVars = (Sa510ScreenVars)pv;
			screenVars.screenIndicArea.clearFormatting();
			screenVars.select.clearFormatting();
			screenVars.sequence.clearFormatting();
			screenVars.accdesc.clearFormatting();
			screenVars.date.clearFormatting();
			screenVars.acctime.clearFormatting();
			screenVars.userID.clearFormatting();
			clearClassString(pv);
		}

		public static void clearClassString(VarModel pv) {
			Sa510ScreenVars screenVars = (Sa510ScreenVars)pv;
			screenVars.screenIndicArea.setClassString("");
			screenVars.select.setClassString("");
			screenVars.sequence.setClassString("");
			screenVars.accdesc.setClassString("");
			screenVars.date.setClassString("");
			screenVars.acctime.setClassString("");
			screenVars.userID.setClassString("");
		}

	/**
	 * Clear all the variables in S5015screensfl
	 */
		public static void clear(VarModel pv) {
			Sa510ScreenVars screenVars = (Sa510ScreenVars) pv;
			screenVars.screenIndicArea.clear();
			screenVars.select.clear();
			screenVars.sequence.clear();
			screenVars.accdesc.clear();
			screenVars.date.clear();
			screenVars.acctime.clear();
			screenVars.userID.clear();
		}

}
