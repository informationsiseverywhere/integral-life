package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5026screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {18, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5026ScreenVars sv = (S5026ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5026screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5026ScreenVars screenVars = (S5026ScreenVars)pv;
		screenVars.policyloan.setClassString("");
		screenVars.clamant.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.resndesc.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.otheradjst.setClassString("");
		screenVars.estimateTotalValue.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.netOfSvDebt.setClassString("");
		screenVars.zrcshamt.setClassString("");
		screenVars.tdbtamt.setClassString("");
		screenVars.taxamt.setClassString("");
		screenVars.reserveUnitsInd.setClassString("");//ILIFE-5452
		screenVars.reserveUnitsDateDisp.setClassString("");//ILIFE-5452
	}

/**
 * Clear all the variables in S5026screen
 */
	public static void clear(VarModel pv) {
		S5026ScreenVars screenVars = (S5026ScreenVars) pv;
		screenVars.policyloan.clear();
		screenVars.clamant.clear();
		screenVars.reasoncd.clear();
		screenVars.resndesc.clear();
		screenVars.currcd.clear();
		screenVars.otheradjst.clear();
		screenVars.estimateTotalValue.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.netOfSvDebt.clear();
		screenVars.zrcshamt.clear();
		screenVars.tdbtamt.clear();
		screenVars.taxamt.clear();
		screenVars.reserveUnitsInd.clear();//ILIFE-5452
		screenVars.reserveUnitsDateDisp.clear();//ILIFE-5452
	}
}
