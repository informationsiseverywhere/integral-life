/*
 * File: Threeti.java
 * Date: 30 August 2009 2:37:32
 * Author: Quipoz Limited
 * 
 * Class transformed from THREETI.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.recordstructures.Hdivdrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.reports.Rh538Report;
import com.csc.life.terminationclaims.tablestructures.Th536rec;
import com.csc.life.underwriting.tablestructures.T6642rec;
import com.csc.life.underwriting.tablestructures.Tt562rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        ETI PROCESSING SUBROUTINE (THAI SPECIFIC)
*        Cloned from HNFETI
*
*   This is called from various programs via T6597.
*
*   This program will first calculate the Surrender Value of the
*   contract, which will include the Basic CV, the CV of PUA, any
*   accumulated dividend plus outstanding interest. After repaying
*   any outstanding loan plus any outstanding interest, the remain
*   of the Surrender Value will be used to purchase an extended
*   term coverage. The extended term will be added to the current
*   Paid To Date to determine new Risk Cessation Date, Premium
*   Cessation Date and Next Rerate Date. If the Surrender Value
*   is large enough to coverage beyond the original Cessation
*   Date, a refund is to be made to the Policy holder. Also,
*   perform any commission clawback if necessary.
*
*   Initialise
*     - read various tables.
*
*   Processing
*     - calculate the Surrender Value.
*     - if Surrender Value is zero, exit program with a LAPSE
*       status and write a line to the exception report.
*     - repay any outstanding loan and loan interest.
*     - calculate the new issue age.
*     - calculate the new ETI SI.
*     - use the Surrender Value to calculate the ETI Value
*       which is in turn used to deduce the length of the
*       extended term, and hence the new cessation dates.
*     - calculate refund if applicable. Apply multiplier factor.
*     - if it is an online quotation, exit program. Otherwise,
*       perform various accounting posting.
*
*
*****************************************************************
* </pre>
*/
public class Threeti extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rh538Report printerFile = new Rh538Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(500);
	private final String wsaaSubr = "THREETI";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final int wsaaMaxOcc = 110;
		/* WSAA-AGE-LIMITS */
	private int wsaaMinimumAge = 0;
	private int wsaaMaximumAge = 0;

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaTranTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaTranUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
	private String wsaaTh536Found = "";
	private String wsaaTh536Exhausted = "";

	private FixedLengthStringData wsaaTh536Keys = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh536Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh536Keys, 0);
	private FixedLengthStringData wsaaTh536Issage = new FixedLengthStringData(2).isAPartOf(wsaaTh536Keys, 4);
	private FixedLengthStringData wsaaTh536Etiage = new FixedLengthStringData(2).isAPartOf(wsaaTh536Keys, 6);
	private ZonedDecimalData wsaaTh536EtiageNum = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT6639Item = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6639Divdmth = new FixedLengthStringData(4).isAPartOf(wsaaT6639Item, 0);
	private FixedLengthStringData wsaaT6639Pstcd = new FixedLengthStringData(2).isAPartOf(wsaaT6639Item, 4);
	private PackedDecimalData wsaaNewBonusDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0, REDEFINE).setUnsigned();
	private String wsaaCashDividend = "";
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbp1 = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaTage = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaTagep1 = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaT5645Sub = new ZonedDecimalData(2, 0).setUnsigned();
	//private static final int wsaaT5645Size = 27;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5645Size = 1000;


		/* WSAA-T5645 */
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);
		/* WSAA-ETI-YR-DAY */
	private ZonedDecimalData wsaaYears = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaDays = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaYearsPlus1 = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private String wsaaPrtOpen = "N";
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaException = new FixedLengthStringData(30);
		/* WSAA-EXCEPTION-MSG */
	private static final String nilSvMsg = "Lapsed. No surrender value.";
	private static final String refundMsg = "Refund is required.";
	private static final String lonErMsg = "Insufficient surrender value.";
		/* WSAA-DATES */
	private PackedDecimalData wsaaCessDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaNextIntDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNextIntdate = new FixedLengthStringData(8).isAPartOf(wsaaNextIntDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNextintYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNextIntdate, 0).setUnsigned();
	private ZonedDecimalData wsaaNextintMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextIntdate, 4).setUnsigned();
	private ZonedDecimalData wsaaNextintDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextIntdate, 6).setUnsigned();
	private FixedLengthStringData wsaaLastdayOfMonths = new FixedLengthStringData(24).init("312831303130313130313031");

	private FixedLengthStringData wsaaLastddOfMonths = new FixedLengthStringData(24).isAPartOf(wsaaLastdayOfMonths, 0, REDEFINE);
	private ZonedDecimalData[] wsaaLastdd = ZDArrayPartOfStructure(12, 2, 0, wsaaLastddOfMonths, 0, UNSIGNED_TRUE);
		/* WSAA-TEMP-VARS */
	private ZonedDecimalData wsaaDecimalDays = new ZonedDecimalData(5, 2).setUnsigned();
	private ZonedDecimalData wsaaDiff = new ZonedDecimalData(2, 2);
	private PackedDecimalData wsaaEtiDiff = new PackedDecimalData(18, 3);
	private ZonedDecimalData wsaaDtc3FreqFactor = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaDtc3Diff = new ZonedDecimalData(5, 5);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/* RH538-H01 */
	private FixedLengthStringData rh538h01O = new FixedLengthStringData(83);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rh538h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rh538h01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rh538h01O, 11);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rh538h01O, 41);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rh538h01O, 51);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rh538h01O, 53);

		/* RH538-D01 */
	private FixedLengthStringData rh538d01O = new FixedLengthStringData(95);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rh538d01O, 0);
	private FixedLengthStringData rd01Cnttype = new FixedLengthStringData(3).isAPartOf(rh538d01O, 8);
	private FixedLengthStringData rd01Cntcurr = new FixedLengthStringData(3).isAPartOf(rh538d01O, 11);
	private ZonedDecimalData rd01Surrval = new ZonedDecimalData(17, 2).isAPartOf(rh538d01O, 14);
	private ZonedDecimalData rd01Loanorigam = new ZonedDecimalData(17, 2).isAPartOf(rh538d01O, 31);
	private ZonedDecimalData rd01Refundfe = new ZonedDecimalData(17, 2).isAPartOf(rh538d01O, 48);
	private FixedLengthStringData rd01Descrip = new FixedLengthStringData(30).isAPartOf(rh538d01O, 65);
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private HdivTableDAM hdivIO = new HdivTableDAM();
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Cashedrec cashedrec = new Cashedrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T1693rec t1693rec = new T1693rec();
	private T5645rec t5645rec = new T5645rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T6598rec t6598rec = new T6598rec();
	private T6639rec t6639rec = new T6639rec();
	private Th506rec th506rec = new Th506rec();
	private Th536rec th536rec = new Th536rec();
	private Th501rec th501rec = new Th501rec();
	private T6642rec t6642rec = new T6642rec();
	private Tt562rec tt562rec = new Tt562rec();
	private Totloanrec totloanrec = new Totloanrec();
	private Hdivdrec hdivdrec = new Hdivdrec();
	private Varcom varcom = new Varcom();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private ErrorsInner errorsInner = new ErrorsInner();
	private LifeatsInner lifeatsInner = new LifeatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaAgerowPtrInner wsaaAgerowPtrInner = new WsaaAgerowPtrInner();
	private WsaaTt562ValuesInner wsaaTt562ValuesInner = new WsaaTt562ValuesInner();
	private WsaaValuesInner wsaaValuesInner = new WsaaValuesInner();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ExternalisedRules er = new ExternalisedRules();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readT56451020, 
		tableLoop1030, 
		loopCheck1040, 
		puaDvdHdiv7725, 
		dividendWithdrawal7763, 
		osInt7764, 
		bookNewInterest7765, 
		datcon27769a, 
		exit7769a, 
		callAgcmio7794, 
		exit8890, 
		seExit9090, 
		dbExit9190
	}

	public Threeti() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			main0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main0000()
	{
		/*START*/
		/* Main logic*/
		initialize1000();
		calSurrVal3000();
		if (isEQ(ovrduerec.statuz, varcom.oK)) {
			settleLoan5000();
			if (isEQ(ovrduerec.statuz, varcom.oK)) {
				calEtiTerm7000();
				if (isEQ(ovrduerec.statuz, varcom.oK)) {
					close8900();
				}
			}
		}
		/*EXIT*/
		exitProgram();
	}

protected void initialize1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case readT56451020: 
					readT56451020();
				case tableLoop1030: 
					tableLoop1030();
				case loopCheck1040: 
					loopCheck1040();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		wsaaValuesInner.wsaaBasicCv.set(ZERO);
		wsaaValuesInner.wsaaPuaCv.set(ZERO);
		wsaaValuesInner.wsaaDvd.set(ZERO);
		wsaaValuesInner.wsaaNewDvd.set(ZERO);
		wsaaValuesInner.wsaaBasicDvd.set(ZERO);
		wsaaValuesInner.wsaaInt.set(ZERO);
		wsaaValuesInner.wsaaNewInt.set(ZERO);
		wsaaValuesInner.wsaaHdisOsInt.set(ZERO);
		wsaaValuesInner.wsaaTotSv.set(ZERO);
		wsaaValuesInner.wsaaTotLoan.set(ZERO);
		wsaaValuesInner.wsaaAccumValue.set(ZERO);
		wsaaValuesInner.wsaaRefund.set(ZERO);
		wsaaValuesInner.wsaaCommRecovered.set(ZERO);
		wsaaValuesInner.wsaaEtiCost.set(ZERO);
		wsaaValuesInner.wsaaEtiSi.set(ZERO);
		wsaaValuesInner.wsaaEtiAct.set(ZERO);
		wsaaTt562ValuesInner.wsaaTt562Values.set(ZERO);
		wsaaSequenceNo.set(0);
		wsaaMinimumAge = 0;
		/*    MOVE 102                    TO WSAA-MAXIMUM-AGE.             */
		wsaaMaximumAge = 110;
		wsaaCashDividend = "Y";
		varcom.vrcmTime.set(getCobolTime());
		wsaaBatckey.batcBatcpfx.set(smtpfxcpy.batc);
		wsaaBatckey.batcBatcbrn.set(ovrduerec.batcbrn);
		wsaaBatckey.batcBatccoy.set(ovrduerec.chdrcoy);
		wsaaBatckey.batcBatcactyr.set(ovrduerec.acctyear);
		wsaaBatckey.batcBatcactmn.set(ovrduerec.acctmonth);
		wsaaBatckey.batcBatctrcde.set(ovrduerec.trancode);
		wsaaBatckey.batcBatcbatch.set(ovrduerec.batcbatch);
		ovrduerec.statuz.set(varcom.oK);
		/* Obtain transaction description from T1688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(ovrduerec.chdrcoy);
		descIO.setDesctabl(tablesInner.t1688);
		descIO.setDescitem(ovrduerec.trancode);
		descIO.setLanguage(ovrduerec.language);
		descIO.setFormat(lifeatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError9100();
		}
		wsaaTransDesc.set(descIO.getLongdesc());
		/* Read T5645 for accounting details. This call will read the 2nd*/
		/* page of the table item(this item has more than one page of*/
		/* accounting details). This first page will be read in individual*/
		/* section. The details read here will be stored away in working*/
		/* storage and to be used later on.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		wsaaT5645Sub.set(1);
	}

protected void readT56451020()
	{
		/*    ADD  1                      TO WSAA-SEQNO.*/
		/*    MOVE WSAA-SEQNO             TO ITEM-ITEMSEQ.*/
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			dbError9100();
		}
		if (isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItemcoy(), ovrduerec.chdrcoy)
		|| isNE(itemIO.getItemtabl(), tablesInner.t5645)
		|| isNE(itemIO.getItemitem(), wsaaSubr)) {
			itemIO.setGenarea(SPACES);
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaSub1.set(1);
	}

protected void tableLoop1030()
	{
		if (isEQ(t5645rec.sacscode[wsaaSub1.toInt()], SPACES)
		&& isEQ(t5645rec.sacstype[wsaaSub1.toInt()], SPACES)
		&& isEQ(t5645rec.glmap[wsaaSub1.toInt()], SPACES)) {
			wsaaSub1.set(17);
			goTo(GotoLabel.loopCheck1040);
		}
		if (isGT(wsaaT5645Sub, wsaaT5645Size)) {
			syserrrec.statuz.set(errorsInner.h791);
			systemError9000();
		}
		wsaaT5645Cnttot[wsaaT5645Sub.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
		wsaaT5645Glmap[wsaaT5645Sub.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
		wsaaT5645Sacscode[wsaaT5645Sub.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
		wsaaT5645Sacstype[wsaaT5645Sub.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
		wsaaT5645Sign[wsaaT5645Sub.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		wsaaSub1.add(1);
		wsaaT5645Sub.add(1);
	}

protected void loopCheck1040()
	{
		/* Next Line*/
		if (isLT(wsaaSub1, 16)) {
			goTo(GotoLabel.tableLoop1030);
		}
		/* Next Page*/
		if (isEQ(wsaaSub1, 16)) {
			itemIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readT56451020);
		}
		/* Read TH506*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(tablesInner.th506);
		itemIO.setItemitem(ovrduerec.cnttype);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		/* Read T5687*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(ovrduerec.crtable);
		itdmIO.setItmfrm(ovrduerec.crrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(ovrduerec.crtable, itdmIO.getItemitem())
		|| isNE(ovrduerec.chdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			ovrduerec.statuz.set(errorsInner.h053);
			return ;
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5687rec.svMethod, SPACES)) {
			ovrduerec.statuz.set(errorsInner.e076);
			return ;
		}
		/* Read T6598*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(tablesInner.t6598);
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		/* Read T5688*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(ovrduerec.cnttype);
		itdmIO.setItmfrm(ovrduerec.crrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(ovrduerec.cnttype, itdmIO.getItemitem())
		|| isNE(ovrduerec.chdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			ovrduerec.statuz.set(errorsInner.e308);
			return ;
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		/* Read HCSD to obtain the Cash Div Method.*/
		hcsdIO.setChdrcoy(ovrduerec.chdrcoy);
		hcsdIO.setChdrnum(ovrduerec.chdrnum);
		hcsdIO.setLife(ovrduerec.life);
		hcsdIO.setCoverage(ovrduerec.coverage);
		hcsdIO.setRider(ovrduerec.rider);
		hcsdIO.setPlanSuffix(ovrduerec.planSuffix);
		hcsdIO.setFormat(lifeatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isEQ(hcsdIO.getStatuz(), varcom.mrnf)) {
			wsaaCashDividend = "N";
			initialize(th501rec.th501Rec);
			return ;
		}
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			dbError9100();
		}
		/* Read TH501.*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.th501);
		itdmIO.setItemitem(hcsdIO.getZcshdivmth());
		itdmIO.setItmfrm(ovrduerec.crrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		itdmIO.setFormat(lifeatsInner.itemrec);
		itdmIO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.th501)
		|| isNE(itdmIO.getItemitem(), hcsdIO.getZcshdivmth())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(errorsInner.z039);
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		th501rec.th501Rec.set(itdmIO.getGenarea());
	}

protected void calSurrVal3000()
	{
		start3010();
		checkSurrVal3020();
	}

protected void start3010()
	{
		/* Set up SRCALCPY from linkage*/
		initialize(srcalcpy.surrenderRec);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(ovrduerec.chdrcoy);
		srcalcpy.chdrChdrnum.set(ovrduerec.chdrnum);
		srcalcpy.planSuffix.set(ovrduerec.planSuffix);
		srcalcpy.polsum.set(ovrduerec.polsum);
		srcalcpy.lifeLife.set(ovrduerec.life);
		srcalcpy.covrCoverage.set(ovrduerec.coverage);
		srcalcpy.covrRider.set(ovrduerec.rider);
		srcalcpy.crtable.set(ovrduerec.crtable);
		srcalcpy.crrcd.set(ovrduerec.crrcd);
		srcalcpy.ptdate.set(ovrduerec.ptdate);
		srcalcpy.effdate.set(ovrduerec.effdate);
		srcalcpy.language.set(ovrduerec.language);
		srcalcpy.chdrCurr.set(ovrduerec.cntcurr);
		srcalcpy.pstatcode.set(ovrduerec.pstatcode);
		srcalcpy.billfreq.set(ovrduerec.billfreq);
		srcalcpy.status.set(varcom.oK);
		while ( !(isEQ(srcalcpy.status, varcom.endp)
		|| isEQ(srcalcpy.status, varcom.bomb))) {
			/*IVE-796 RUL Product - Partial Surrender Calculation started*/
			//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))  
			{
				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			}
			else
			{
		 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
				Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
				vpxsurcrec.function.set("INIT");
				callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
				vpmfmtrec.initialize();
				vpmfmtrec.amount02.set(wsaaEstimateTot);
				
				chdrlnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
				chdrlnbIO.setChdrnum(srcalcpy.chdrChdrnum);
				chdrlnbIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, chdrlnbIO);

				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
				
				
				if(isEQ(srcalcpy.type,"L"))
				{
					vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
					callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
					srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
					srcalcpy.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpy.type,"C"))
				{
					srcalcpy.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
				{
					srcalcpy.status.set(varcom.endp);
				}
				else
				{
					srcalcpy.status.set(varcom.oK);
				}
			}

			/*IVE-796 RUL Product - Partial Surrender Calculation end*/
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			a000CallRounding();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			if (isEQ(srcalcpy.type, "S")){
				wsaaValuesInner.wsaaBasicCv.set(srcalcpy.actualVal);
			}
			else if (isEQ(srcalcpy.type, "P")){
				wsaaValuesInner.wsaaPuaCv.set(srcalcpy.actualVal);
			}
			else if (isEQ(srcalcpy.type, "D")){
				wsaaValuesInner.wsaaDvd.set(srcalcpy.actualVal);
			}
			else if (isEQ(srcalcpy.type, "I")){
				wsaaValuesInner.wsaaInt.set(srcalcpy.actualVal);
			}
			wsaaValuesInner.wsaaTotSv.add(srcalcpy.actualVal);
			srcalcpy.actualVal.set(ZERO);
		}
		
		/* Set up to call dividend allocation if premium is fully paid*/
		hcsdIO.setDataKey(SPACES);
		hcsdIO.setChdrcoy(ovrduerec.chdrcoy);
		hcsdIO.setChdrnum(ovrduerec.chdrnum);
		hcsdIO.setLife(ovrduerec.life);
		hcsdIO.setCoverage(ovrduerec.coverage);
		hcsdIO.setRider(ovrduerec.rider);
		hcsdIO.setPlanSuffix(ovrduerec.planSuffix);
		hcsdIO.setFormat(lifeatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)
		&& isNE(hcsdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			dbError9100();
		}
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setStatuz(varcom.oK);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE");
		covrmjaIO.setFormat(lifeatsInner.covrmjarec);
		covrmjaIO.setChdrcoy(ovrduerec.chdrcoy);
		covrmjaIO.setChdrnum(ovrduerec.chdrnum);
		covrmjaIO.setLife(ovrduerec.life);
		covrmjaIO.setCoverage(ovrduerec.coverage);
		covrmjaIO.setRider(ovrduerec.rider);
		covrmjaIO.setPlanSuffix(ovrduerec.planSuffix);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			dbError9100();
		}
		if (isNE(covrmjaIO.getChdrcoy(), ovrduerec.chdrcoy)
		|| isNE(covrmjaIO.getChdrnum(), ovrduerec.chdrnum)
		|| isNE(covrmjaIO.getLife(), ovrduerec.life)
		|| isNE(covrmjaIO.getCoverage(), ovrduerec.coverage)
		|| isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			covrmjaIO.setStatuz(errorsInner.f259);
			ovrduerec.statuz.set(errorsInner.f259);
			return ;
		}
		if (isEQ(wsaaCashDividend, "N")) {
			return ;
		}
		datcon2rec.intDate1.set(covrmjaIO.getUnitStatementDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaNewBonusDate.set(datcon2rec.intDate2);
		if (isGT(wsaaNewBonusDate, ovrduerec.ptdate)) {
			return ;
		}
		initialize(hdivdrec.dividendRec);
		hdivdrec.chdrChdrcoy.set(ovrduerec.chdrcoy);
		hdivdrec.chdrChdrnum.set(ovrduerec.chdrnum);
		hdivdrec.lifeLife.set(ovrduerec.life);
		hdivdrec.covrCoverage.set(ovrduerec.coverage);
		hdivdrec.covrRider.set(ovrduerec.rider);
		hdivdrec.plnsfx.set(ovrduerec.planSuffix);
		hdivdrec.cntcurr.set(ovrduerec.cntcurr);
		hdivdrec.cnttype.set(ovrduerec.cnttype);
		hdivdrec.transcd.set(ovrduerec.trancode);
		hdivdrec.premStatus.set(covrmjaIO.getPstatcode());
		hdivdrec.crtable.set(ovrduerec.crtable);
		hdivdrec.sumin.set(covrmjaIO.getSumins());
		hdivdrec.effectiveDate.set(ovrduerec.effdate);
		hdivdrec.ptdate.set(ovrduerec.ptdate);
		hdivdrec.allocMethod.set(covrmjaIO.getBonusInd());
		hdivdrec.mortcls.set(covrmjaIO.getMortcls());
		hdivdrec.sex.set(covrmjaIO.getSex());
		hdivdrec.issuedAge.set(covrmjaIO.getAnbAtCcd());
		hdivdrec.tranno.set(ovrduerec.tranno);
		hdivdrec.divdCalcMethod.set(hcsdIO.getZcshdivmth());
		hdivdrec.periodFrom.set(covrmjaIO.getUnitStatementDate());
		hdivdrec.periodTo.set(wsaaNewBonusDate);
		hdivdrec.crrcd.set(covrmjaIO.getCrrcd());
		hdivdrec.cessDate.set(covrmjaIO.getRiskCessDate());
		wsaaBillfreq.set(ovrduerec.billfreq);
		compute(hdivdrec.annualisedPrem, 3).setRounded(mult(covrmjaIO.getInstprem(), wsaaBillfreq9));
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		hdivdrec.divdRate.set(ZERO);
		hdivdrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Dividend Allocation Subroutine*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t6639);
		wsaaT6639Divdmth.set(hcsdIO.getZcshdivmth());
		wsaaT6639Pstcd.set(covrmjaIO.getPstatcode());
		itdmIO.setItemitem(wsaaT6639Item);
		itdmIO.setItmfrm(wsaaNewBonusDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		itdmIO.setFormat(lifeatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6639)
		|| isNE(itdmIO.getItemitem(), wsaaT6639Item)) {
			itdmIO.setStatuz(varcom.endp);
			return ;
		}
		t6639rec.t6639Rec.set(itdmIO.getGenarea());
		if (isEQ(t6639rec.revBonusProg, SPACES)) {
			return ;
		}
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz, varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			systemError9000();
		}
		zrdecplrec.amountIn.set(hdivdrec.divdAmount);
		a000CallRounding();
		hdivdrec.divdAmount.set(zrdecplrec.amountOut);
		/*    When the dividend amount from allocation is not zeroes,*/
		/*    add the amount to WSAA-DVD for dividend total*/
		if (isNE(hdivdrec.divdAmount, 0)) {
			wsaaValuesInner.wsaaBasicDvd.set(hdivdrec.divdAmount);
			wsaaValuesInner.wsaaDivdRate.set(hdivdrec.divdRate);
			wsaaValuesInner.wsaaRateDate.set(hdivdrec.rateDate);
			wsaaValuesInner.wsaaNewDvd.add(hdivdrec.divdAmount);
			wsaaValuesInner.wsaaDvd.add(hdivdrec.divdAmount);
			wsaaValuesInner.wsaaTotSv.add(hdivdrec.divdAmount);
		}
		/*    Read HPUA and allocate dividend from these paid-up addition*/
		hpuaIO.setChdrcoy(hdivdrec.chdrChdrcoy);
		hpuaIO.setChdrnum(hdivdrec.chdrChdrnum);
		hpuaIO.setLife(hdivdrec.lifeLife);
		hpuaIO.setCoverage(hdivdrec.covrCoverage);
		hpuaIO.setRider(hdivdrec.covrRider);
		hpuaIO.setPlanSuffix(hdivdrec.plnsfx);
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		hpuaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)
		&& isNE(hpuaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			dbError9100();
		}
		if (isEQ(hpuaIO.getStatuz(), varcom.endp)
		|| isNE(hpuaIO.getChdrcoy(), hdivdrec.chdrChdrcoy)
		|| isNE(hpuaIO.getChdrnum(), hdivdrec.chdrChdrnum)
		|| isNE(hpuaIO.getLife(), hdivdrec.lifeLife)
		|| isNE(hpuaIO.getCoverage(), hdivdrec.covrCoverage)
		|| isNE(hpuaIO.getRider(), hdivdrec.covrRider)
		|| isNE(hpuaIO.getPlanSuffix(), hdivdrec.plnsfx)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hpuaIO.getStatuz(), varcom.endp))) {
			dividendPaidup4000();
		}
		
	}

protected void checkSurrVal3020()
	{
		/* Check for zero Surrender Value*/
		if (isEQ(wsaaValuesInner.wsaaTotSv, ZERO)) {
			if (isEQ(ovrduerec.function, SPACES)) {
				wsaaException.set(nilSvMsg);
				openPrinter8800();
				writeRh5388300();
			}
			ovrduerec.statuz.set("LAPS");
		}
		/*EXIT*/
	}

protected void dividendPaidup4000()
	{
		dividend4010();
		nextHpua4080();
	}

protected void dividend4010()
	{
		/*    Skip this HPUA if it is not Dividend participant*/
		if (isNE(hpuaIO.getDivdParticipant(), "Y")) {
			return ;
		}
		/*    Find dividend allocation subroutine from T6639.*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t6639);
		wsaaT6639Divdmth.set(hcsdIO.getZcshdivmth());
		wsaaT6639Pstcd.set(hpuaIO.getPstatcode());
		itdmIO.setItemitem(wsaaT6639Item);
		itdmIO.setItmfrm(wsaaNewBonusDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		itdmIO.setFormat(lifeatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6639)
		|| isNE(itdmIO.getItemitem(), wsaaT6639Item)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			return ;
		}
		t6639rec.t6639Rec.set(itdmIO.getGenarea());
		/*    Check if dividend allocation subroutine is defined*/
		if (isEQ(t6639rec.revBonusProg, SPACES)) {
			return ;
		}
		/*    Prepare to call cash dividend allocation subroutine*/
		initialize(hdivdrec.dividendRec);
		hdivdrec.chdrChdrcoy.set(ovrduerec.chdrcoy);
		hdivdrec.chdrChdrnum.set(ovrduerec.chdrnum);
		hdivdrec.lifeLife.set(ovrduerec.life);
		hdivdrec.covrCoverage.set(ovrduerec.coverage);
		hdivdrec.covrRider.set(ovrduerec.rider);
		hdivdrec.plnsfx.set(ovrduerec.planSuffix);
		hdivdrec.cntcurr.set(ovrduerec.cntcurr);
		hdivdrec.cnttype.set(ovrduerec.cnttype);
		hdivdrec.transcd.set(ovrduerec.trancode);
		hdivdrec.premStatus.set(hpuaIO.getPstatcode());
		hdivdrec.crtable.set(hpuaIO.getCrtable());
		hdivdrec.sumin.set(hpuaIO.getSumin());
		hdivdrec.effectiveDate.set(ovrduerec.effdate);
		hdivdrec.ptdate.set(ovrduerec.ptdate);
		hdivdrec.allocMethod.set(covrmjaIO.getBonusInd());
		hdivdrec.mortcls.set(covrmjaIO.getMortcls());
		hdivdrec.sex.set(covrmjaIO.getSex());
		hdivdrec.issuedAge.set(hpuaIO.getAnbAtCcd());
		hdivdrec.tranno.set(ovrduerec.tranno);
		hdivdrec.divdCalcMethod.set(hcsdIO.getZcshdivmth());
		hdivdrec.periodFrom.set(covrmjaIO.getUnitStatementDate());
		hdivdrec.periodTo.set(wsaaNewBonusDate);
		hdivdrec.crrcd.set(hpuaIO.getCrrcd());
		hdivdrec.cessDate.set(hpuaIO.getRiskCessDate());
		hdivdrec.annualisedPrem.set(hpuaIO.getSingp());
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		hdivdrec.divdRate.set(ZERO);
		hdivdrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Dividend Paid-up Allocation Subroutine*/
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz, varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			systemError9000();
		}
		zrdecplrec.amountIn.set(hdivdrec.divdAmount);
		a000CallRounding();
		hdivdrec.divdAmount.set(zrdecplrec.amountOut);
		/*    When the dividend amount from allocation is not zeroes,*/
		/*    write a HDIV (dividend detail) and update HDIS (dividend*/
		/*    history) if exists or create one.*/
		if (isNE(hdivdrec.divdAmount, 0)) {
			wsaaValuesInner.wsaaNewDvd.add(hdivdrec.divdAmount);
			wsaaValuesInner.wsaaDvd.add(hdivdrec.divdAmount);
			wsaaValuesInner.wsaaTotSv.add(hdivdrec.divdAmount);
		}
	}

protected void nextHpua4080()
	{
		hpuaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)
		&& isNE(hpuaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			dbError9100();
		}
		if (isEQ(hpuaIO.getStatuz(), varcom.endp)
		|| isNE(hpuaIO.getChdrcoy(), hdivdrec.chdrChdrcoy)
		|| isNE(hpuaIO.getChdrnum(), hdivdrec.chdrChdrnum)
		|| isNE(hpuaIO.getLife(), hdivdrec.lifeLife)
		|| isNE(hpuaIO.getCoverage(), hdivdrec.covrCoverage)
		|| isNE(hpuaIO.getRider(), hdivdrec.covrRider)
		|| isNE(hpuaIO.getPlanSuffix(), hdivdrec.plnsfx)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void settleLoan5000()
	{
		start5010();
	}

protected void start5010()
	{
		/*  Call HRTOTLON to bring interest up to date on this*/
		/*  contract.*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(ovrduerec.chdrcoy);
		totloanrec.chdrnum.set(ovrduerec.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(ovrduerec.effdate);
		totloanrec.tranno.set(ovrduerec.tranno);
		totloanrec.batchkey.set(wsaaBatckey);
		totloanrec.tranTerm.set(ovrduerec.termid);
		totloanrec.tranDate.set(ovrduerec.tranDate);
		totloanrec.tranTime.set(ovrduerec.tranTime);
		totloanrec.tranUser.set(ovrduerec.user);
		totloanrec.language.set(ovrduerec.language);
		totloanrec.function.set("LOAN");
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError9000();
		}
		compute(wsaaValuesInner.wsaaTotLoan, 2).set(add(totloanrec.principal, totloanrec.interest));
		if (isLTE(wsaaValuesInner.wsaaTotLoan, ZERO)) {
			wsaaValuesInner.wsaaAccumValue.set(wsaaValuesInner.wsaaTotSv);
			return ;
		}
		/* Surrender Value is less than outstanding loan and interest,*/
		/* this is an error.*/
		if (isLTE(wsaaValuesInner.wsaaTotSv, wsaaValuesInner.wsaaTotLoan)) {
			/*        MOVE HL35               TO OVRD-STATUZ*/
			if (isEQ(ovrduerec.function, SPACES)) {
				wsaaException.set(lonErMsg);
				openPrinter8800();
				writeRh5388300();
			}
			ovrduerec.statuz.set("OMIT");
			return ;
		}
		/* If error free, post the loan interest.*/
		if (isNE(ovrduerec.function, "OLNPT")) {
			totloanrec.totloanRec.set(SPACES);
			totloanrec.chdrcoy.set(ovrduerec.chdrcoy);
			totloanrec.chdrnum.set(ovrduerec.chdrnum);
			totloanrec.principal.set(ZERO);
			totloanrec.interest.set(ZERO);
			totloanrec.loanCount.set(ZERO);
			totloanrec.effectiveDate.set(ovrduerec.effdate);
			totloanrec.tranno.set(ovrduerec.tranno);
			totloanrec.batchkey.set(wsaaBatckey);
			totloanrec.tranTerm.set(ovrduerec.termid);
			totloanrec.tranDate.set(ovrduerec.tranDate);
			totloanrec.tranTime.set(ovrduerec.tranTime);
			totloanrec.tranUser.set(ovrduerec.user);
			totloanrec.language.set(ovrduerec.language);
			totloanrec.function.set("LOAN");
			totloanrec.postFlag.set("Y");
			callProgram(Hrtotlon.class, totloanrec.totloanRec);
			if (isNE(totloanrec.statuz, varcom.oK)) {
				syserrrec.params.set(totloanrec.totloanRec);
				syserrrec.statuz.set(totloanrec.statuz);
				systemError9000();
			}
		}
		/*  Loan repayment.*/
		wsaaGlCompany.set(ovrduerec.chdrcoy);
		wsaaGlCurrency.set(ovrduerec.cntcurr);
		wsaaTranTermid.set(ovrduerec.termid);
		wsaaTranUser.set(ovrduerec.user);
		wsaaTranTime.set(ovrduerec.tranTime);
		wsaaTranDate.set(ovrduerec.tranDate);
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(ovrduerec.chdrcoy);
		wsaaTranEntity.set(ovrduerec.chdrnum);
		cashedrec.cashedRec.set(SPACES);
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(wsaaBatckey);
		cashedrec.doctNumber.set(ovrduerec.chdrnum);
		cashedrec.doctCompany.set(ovrduerec.chdrcoy);
		cashedrec.trandate.set(ovrduerec.effdate);
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		wsaaSequenceNo.set(1);
		cashedrec.transeq.set(wsaaSequenceNo);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(ovrduerec.cntcurr);
		cashedrec.dissrate.set(0);
		cashedrec.trandesc.set(SPACES);
		cashedrec.genlCompany.set(ovrduerec.chdrcoy);
		cashedrec.genlCurrency.set(ovrduerec.cntcurr);
		cashedrec.chdrcoy.set(ovrduerec.chdrcoy);
		cashedrec.chdrnum.set(ovrduerec.chdrnum);
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(ovrduerec.chdrcoy);
		wsaaTranEntity.set(ovrduerec.chdrnum);
		cashedrec.trankey.set(wsaaTrankey);
		cashedrec.tranno.set(ovrduerec.tranno);
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.language.set(ovrduerec.language);
		if (isEQ(ovrduerec.function, "OLNPT")) {
			compute(wsaaValuesInner.wsaaAccumValue, 0).set(sub(wsaaValuesInner.wsaaTotSv, wsaaValuesInner.wsaaTotLoan));
		}
		else {
			if (isNE(wsaaValuesInner.wsaaTotLoan, ZERO)) {
				wsaaValuesInner.wsaaAccumValue.set(wsaaValuesInner.wsaaTotSv);
				for (wsaaSub1.set(13); !(isGT(wsaaSub1, 16)
				|| isEQ(wsaaValuesInner.wsaaAccumValue, 0)); wsaaSub1.add(1)){
					loans5500();
				}
				wsaaSequenceNo.set(cashedrec.transeq);
			}
			else {
				wsaaValuesInner.wsaaAccumValue.set(wsaaValuesInner.wsaaTotSv);
			}
		}
		/* Surrender Value should be greater than zero after repaying*/
		/* the loan and interest.*/
		if (isLTE(wsaaValuesInner.wsaaAccumValue, ZERO)) {
			if (isEQ(ovrduerec.function, SPACES)) {
				wsaaException.set(lonErMsg);
				openPrinter8800();
				writeRh5388300();
			}
			ovrduerec.statuz.set("OMIT");
			return ;
		}
	}

protected void loans5500()
	{
		start5510();
	}

protected void start5510()
	{
		/* Line 13 - 16 of T5645 Accounting entry*/
		cashedrec.sign.set(wsaaT5645Sign[wsaaSub1.toInt()]);
		cashedrec.sacscode.set(wsaaT5645Sacscode[wsaaSub1.toInt()]);
		cashedrec.sacstyp.set(wsaaT5645Sacstype[wsaaSub1.toInt()]);
		wsaaGlMap.set(wsaaT5645Glmap[wsaaSub1.toInt()]);
		cashedrec.genlkey.set(wsaaGenlkey);
		cashedrec.origamt.set(wsaaValuesInner.wsaaAccumValue);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			systemError9000();
		}
		wsaaValuesInner.wsaaAccumValue.set(cashedrec.docamt);
	}

protected void callLifacmv6000()
	{
		/*START*/
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError9000();
		}
		/*EXIT*/
	}

protected void calEtiTerm7000()
	{
		/*START*/
		/* If online quotation, exit program.*/
		/* Otherwise, perform posting.*/
		newIssAge7100();
		if (isEQ(ovrduerec.statuz, varcom.oK)) {
			calEtiSi7300();
			if (isEQ(ovrduerec.statuz, varcom.oK)) {
				lookUpTerm7500();
				if (isEQ(ovrduerec.statuz, varcom.oK)) {
					if (isEQ(ovrduerec.function, "OLNPT")) {
						return ;
					}
					else {
						posting7700();
					}
				}
			}
		}
		/*EXIT*/
	}

protected void newIssAge7100()
	{
		start7110();
	}

protected void start7110()
	{
		/* Read T1693 to obtain FSU Company.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(tablesInner.t1693);
		itemIO.setItemitem(ovrduerec.chdrcoy);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(lifeatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError9100();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		/* Read LIFELNB.*/
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(ovrduerec.chdrcoy);
		lifelnbIO.setChdrnum(ovrduerec.chdrnum);
		lifelnbIO.setFormat(lifeatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			dbError9100();
		}
		if (isEQ(lifelnbIO.getStatuz(), varcom.endp)
		|| isNE(lifelnbIO.getChdrcoy(), ovrduerec.chdrcoy)
		|| isNE(lifelnbIO.getChdrnum(), ovrduerec.chdrnum)) {
			ovrduerec.statuz.set(errorsInner.e355);
			return ;
		}
		/* Set up AGECALCREC.*/
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(ovrduerec.language);
		agecalcrec.cnttype.set(ovrduerec.cnttype);
		agecalcrec.intDate1.set(lifelnbIO.getCltdob());
		agecalcrec.intDate2.set(ovrduerec.ptdate);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(agecalcrec.agecalcRec);
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError9000();
		}
		wsaaAnb.set(agecalcrec.agerating);
	}

protected void calEtiSi7300()
	{
		start7310();
	}

protected void start7310()
	{
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			covrmjaIO.setStatuz(errorsInner.f259);
			ovrduerec.statuz.set(errorsInner.f259);
			return ;
		}
		/* Calaulate*/
		compute(wsaaValuesInner.wsaaDvdSi, 0).set(sub(wsaaValuesInner.wsaaDvd, wsaaValuesInner.wsaaTotLoan));
		if (isLT(wsaaValuesInner.wsaaDvdSi, ZERO)) {
			wsaaValuesInner.wsaaDvdSi.set(ZERO);
		}
		/* If setting in TH506 indicates to include dividend in the SI,*/
		/* do so.*/
		if (isEQ(th506rec.confirm, "Y")) {
			if (isEQ(covrmjaIO.getVarSumInsured(), ZERO)) {
				compute(wsaaValuesInner.wsaaEtiSi, 2).set(add(covrmjaIO.getSumins(), wsaaValuesInner.wsaaDvdSi));
			}
			else {
				compute(wsaaValuesInner.wsaaEtiSi, 2).set(add(covrmjaIO.getVarSumInsured(), wsaaValuesInner.wsaaDvdSi));
			}
		}
		else {
			if (isEQ(covrmjaIO.getVarSumInsured(), ZERO)) {
				wsaaValuesInner.wsaaEtiSi.set(covrmjaIO.getSumins());
			}
			else {
				wsaaValuesInner.wsaaEtiSi.set(covrmjaIO.getVarSumInsured());
			}
		}
	}

protected void lookUpTerm7500()
	{
		start7510();
	}

protected void start7510()
	{
		/* Calculate age at ETI for TH536 key*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(lifelnbIO.getCltdob());
		datcon3rec.intDate2.set(ovrduerec.tranDate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		/*  MOVE DTC3-FREQ-FACTOR       TO WSAA-TH536-ETIAGE.            */
		wsaaTh536EtiageNum.set(datcon3rec.freqFactor);
		wsaaTh536Etiage.set(wsaaTh536EtiageNum);
		/* Look up TH536 to obtain the Extended Term*/
		wsaaTh536Found = "N";
		wsaaTh536Exhausted = "N";
		wsaaTh536Crtable.set(covrmjaIO.getCrtable());
		wsaaTh536Issage.set(wsaaAnb);
		/* MOVE COVRMJA-MORTCLS        TO WSAA-TH536-MORTCLS.*/
		/* MOVE COVRMJA-SEX            TO WSAA-TH536-SEX.*/
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.th536);
		itdmIO.setItemitem(wsaaTh536Keys);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		while ( !(isEQ(wsaaTh536Found,"Y")
		|| isEQ(wsaaTh536Exhausted,"Y"))) {
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				dbError9100();
			}
			if (isNE(wsaaTh536Keys, itdmIO.getItemitem())
			|| isNE(covrmjaIO.getChdrcoy(), itdmIO.getItemcoy())
			|| isNE(itdmIO.getItemtabl(), tablesInner.th536)
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				if (isEQ(wsaaTh536Crtable, "****")) {
					wsaaTh536Exhausted = "Y";
				}
				else {
					if (isEQ(wsaaTh536Issage, "**")) {
						wsaaTh536Crtable.set("****");
					}
					else {
						/*                   IF WSAA-TH536-MORTCLS = '*'*/
						/*                       MOVE '**'    TO WSAA-TH536-ISSAGE*/
						/*                   ELSE*/
						/*                       IF WSAA-TH536-SEX = '*'*/
						/*                           MOVE '*'    TO WSAA-TH536-MORTCLS*/
						/*                       ELSE*/
						/*                           MOVE '*'    TO WSAA-TH536-SEX*/
						/*                       END-IF*/
						if (isEQ(wsaaTh536Etiage, "**")) {
							wsaaTh536Issage.set("**");
						}
						else {
							wsaaTh536Etiage.set("**");
						}
					}
				}
				itdmIO.setItempfx("IT");
				itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
				itdmIO.setItemtabl(tablesInner.th536);
				itdmIO.setItemitem(wsaaTh536Keys);
				itdmIO.setItmfrm(covrmjaIO.getCrrcd());
			}
			else {
				wsaaTh536Found = "Y";
			}
		}
		
		if (isEQ(wsaaTh536Found, "N")) {
			ovrduerec.statuz.set(errorsInner.hl37);
			return ;
		}
		th536rec.th536Rec.set(itdmIO.getGenarea());
		/* Based on the SV and the SI, calculate the Actuarial Value which*/
		/* will be used to compare with values in TH536.*/
		compute(wsaaValuesInner.wsaaEtiAct, 1).setRounded(div((mult(mult(wsaaValuesInner.wsaaAccumValue, th536rec.unit), th536rec.premUnit)), wsaaValuesInner.wsaaEtiSi));
		zrdecplrec.amountIn.set(wsaaValuesInner.wsaaEtiAct);
		a000CallRounding();
		wsaaValuesInner.wsaaEtiAct.set(zrdecplrec.amountOut);
		if (isLTE(wsaaValuesInner.wsaaEtiAct, ZERO)) {
			ovrduerec.statuz.set(errorsInner.hl36);
			return ;
		}
		/* Look up TH536*/
		wsaaTh536Found = "N";
		for (wsaaSub.set(1); !(isEQ(wsaaTh536Found, "Y")
		|| isGT(wsaaSub, wsaaMaxOcc)); wsaaSub.add(1)){
			compute(wsaaEtiDiff, 4).setRounded(sub(th536rec.insprm[wsaaSub.toInt()], wsaaValuesInner.wsaaEtiAct));
			if (isEQ(wsaaEtiDiff, ZERO)) {
				wsaaYears.set(wsaaSub);
				wsaaTh536Found = "Y";
			}
			else {
				if (isGT(wsaaEtiDiff, ZERO)) {
					compute(wsaaYears, 0).set(sub(wsaaSub, 1));
					wsaaTh536Found = "Y";
				}
			}
		}
		if (isEQ(wsaaTh536Found, "N")) {
			wsaaYears.set(ZERO);
			wsaaDays.set(ZERO);
			ovrduerec.statuz.set(errorsInner.hl39);
			return ;
		}
		/* Now we have the ETI Years i.e. WSAA-YEARS*/
		compute(wsaaYearsPlus1, 0).set(add(1, wsaaYears));
		/* Now we calculate the ETI Days.*/
		if (isGT(wsaaYears, ZERO)) {
			compute(wsaaDecimalDays, 3).setRounded(div(mult((sub(wsaaValuesInner.wsaaEtiAct, th536rec.insprm[wsaaYears.toInt()])), 365), (sub(th536rec.insprm[wsaaYearsPlus1.toInt()], th536rec.insprm[wsaaYears.toInt()]))));
		}
		else {
			compute(wsaaDecimalDays, 3).setRounded(div(mult(wsaaValuesInner.wsaaEtiAct, 365), th536rec.insprm[wsaaYearsPlus1.toInt()]));
		}
		/* Round up to one day if less than one day.*/
		wsaaDays.set(wsaaDecimalDays);
		compute(wsaaDiff, 2).set(sub(wsaaDecimalDays, wsaaDays));
		if (isGT(wsaaDiff, 0)) {
			wsaaDays.add(1);
		}
		/* Advance the current PTD by the ETI Term to give a new Cess Date*/
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.freqFactor.set(wsaaYears);
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(ovrduerec.ptdate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError9000();
		}
		wsaaCessDate.set(datcon2rec.intDate2);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.freqFactor.set(wsaaDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(wsaaCessDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError9000();
		}
		wsaaCessDate.set(datcon2rec.intDate2);
		/* If the new Cess Date is greater than the original Cess Date,*/
		/* calculate the difference between the current PTD and the*/
		/* original Cessation Date. The difference will be used to work*/
		/* out the cost of Term Coverage up to the original Cess Date.*/
		if (isGT(wsaaCessDate, ovrduerec.riskCessDate)) {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(ovrduerec.ptdate);
			datcon3rec.intDate2.set(ovrduerec.riskCessDate);
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserrrec.params.set(datcon3rec.datcon3Rec);
				systemError9000();
			}
			wsaaDtc3FreqFactor.set(datcon3rec.freqFactor);
			compute(wsaaDtc3Diff, 5).set(sub(datcon3rec.freqFactor, wsaaDtc3FreqFactor));
			/* Whole Year*/
			if (isEQ(wsaaDtc3Diff, ZERO)) {
				compute(wsaaValuesInner.wsaaEtiCost, 1).setRounded(div(div(mult(wsaaValuesInner.wsaaEtiSi, th536rec.insprm[wsaaDtc3FreqFactor.toInt()]), th536rec.unit), th536rec.premUnit));
				wsaaYears.set(datcon3rec.freqFactor);
				wsaaDays.set(ZERO);
			}
			else {
				compute(wsaaValuesInner.wsaaEtiCost, 6).setRounded(div(div(mult(wsaaValuesInner.wsaaEtiSi, (add(th536rec.insprm[wsaaDtc3FreqFactor.toInt()], mult((sub(th536rec.insprm[add(wsaaDtc3FreqFactor, 1).toInt()], th536rec.insprm[wsaaDtc3FreqFactor.toInt()])), wsaaDtc3Diff)))), th536rec.unit), th536rec.premUnit));
				wsaaYears.set(wsaaDtc3FreqFactor);
				compute(wsaaDays, 5).set(add(mult(wsaaDtc3Diff, 365), 0.9999));
			}
			wsaaCessDate.set(ovrduerec.riskCessDate);
		}
		else {
			wsaaValuesInner.wsaaEtiCost.set(wsaaValuesInner.wsaaAccumValue);
		}
		zrdecplrec.amountIn.set(wsaaValuesInner.wsaaEtiCost);
		a000CallRounding();
		wsaaValuesInner.wsaaEtiCost.set(zrdecplrec.amountOut);
		/* Update the Cessation Dates*/
		if (isEQ(wsaaCessDate, ZERO)) {
			wsaaCessDate.set(varcom.vrcmMaxDate);
		}
		ovrduerec.newPremCessDate.set(wsaaCessDate);
		ovrduerec.newRiskCessDate.set(wsaaCessDate);
		ovrduerec.newRerateDate.set(wsaaCessDate);
		ovrduerec.newSumins.set(wsaaValuesInner.wsaaEtiSi);
		ovrduerec.surrenderValue.set(wsaaValuesInner.wsaaAccumValue);
		ovrduerec.etiYears.set(wsaaYears);
		ovrduerec.etiDays.set(wsaaDays);
		ovrduerec.newCrrcd.set(ovrduerec.ptdate);
		ovrduerec.newSingp.set(wsaaValuesInner.wsaaEtiCost);
		ovrduerec.newAnb.set(wsaaAnb);
		ovrduerec.newInstprem.set(ZERO);
		/* Write a report line to indicate there will be a refund.*/
		if (isGT(wsaaValuesInner.wsaaAccumValue, wsaaValuesInner.wsaaEtiCost)) {
			computeRefund8400();
			if (isEQ(ovrduerec.function, SPACES)) {
				wsaaException.set(refundMsg);
				openPrinter8800();
				writeRh5388300();
			}
		}
		else {
			wsaaValuesInner.wsaaRefund.set(ZERO);
		}
		ovrduerec.cvRefund.set(wsaaValuesInner.wsaaRefund);
	}

protected void posting7700()
	{
		/*START*/
		setupLifacmv7720();
		postBasicCv7730();
		postPuaCv7740();
		divAccting7750();
		divIntAccting7760();
		updateHdis7768();
		etiPremium7770();
		refund7780();
		commClawback7790();
		/*EXIT*/
	}

protected void setupLifacmv7720()
	{
		start7720();
	}

protected void start7720()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(ovrduerec.chdrcoy);
		lifacmvrec.batcbrn.set(ovrduerec.batcbrn);
		lifacmvrec.batcactyr.set(ovrduerec.acctyear);
		lifacmvrec.batcactmn.set(ovrduerec.acctmonth);
		lifacmvrec.batctrcde.set(ovrduerec.trancode);
		lifacmvrec.batcbatch.set(ovrduerec.batcbatch);
		lifacmvrec.rldgcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.rdocnum.set(ovrduerec.chdrnum);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(ovrduerec.tranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(ovrduerec.effdate);
		lifacmvrec.tranref.set(ovrduerec.chdrnum);
		wsaaRldgChdrnum.set(ovrduerec.chdrnum);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.termid.set(ovrduerec.termid);
		lifacmvrec.transactionDate.set(ovrduerec.tranDate);
		lifacmvrec.transactionTime.set(ovrduerec.tranTime);
		lifacmvrec.user.set(ovrduerec.user);
		lifacmvrec.function.set("PSTW");
	}

protected void postNewDivdAlloc7725()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start7725();
					basicDvdHdiv7725();
				case puaDvdHdiv7725: 
					puaDvdHdiv7725();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7725()
	{
		/* Firstly, post dividend payable*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaNewDvd);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[25]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[25]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[25]);
			lifacmvrec.glsign.set(wsaaT5645Sign[25]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[25]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[23]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[23]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[23]);
			lifacmvrec.glsign.set(wsaaT5645Sign[23]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[23]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
		/* Then, dividend suspense, where if HDIS sacscode or sacstype is*/
		/* not blank, use them.*/
		if (isNE(hdisIO.getSacscode(), SPACES)
		|| isNE(hdisIO.getSacstyp(), SPACES)) {
			lifacmvrec.sacscode.set(hdisIO.getSacscode());
			lifacmvrec.sacstyp.set(hdisIO.getSacstyp());
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[24]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[24]);
		}
		lifacmvrec.glcode.set(wsaaT5645Glmap[24]);
		lifacmvrec.glsign.set(wsaaT5645Sign[24]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[24]);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(ovrduerec.chdrnum);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void basicDvdHdiv7725()
	{
		/* Write HDIV to denote the new dividend allocated*/
		if (isEQ(wsaaValuesInner.wsaaBasicDvd, 0)) {
			goTo(GotoLabel.puaDvdHdiv7725);
		}
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(ovrduerec.chdrcoy);
		hdivIO.setChdrnum(ovrduerec.chdrnum);
		hdivIO.setLife(ovrduerec.life);
		hdivIO.setCoverage(ovrduerec.coverage);
		hdivIO.setRider(ovrduerec.rider);
		hdivIO.setPlanSuffix(ovrduerec.planSuffix);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setEffdate(ovrduerec.effdate);
		hdivIO.setDivdAllocDate(ovrduerec.effdate);
		hdivIO.setBatccoy(ovrduerec.chdrcoy);
		hdivIO.setBatcbrn(ovrduerec.batcbrn);
		hdivIO.setBatcactyr(ovrduerec.acctyear);
		hdivIO.setBatcactmn(ovrduerec.acctmonth);
		hdivIO.setBatctrcde(ovrduerec.trancode);
		hdivIO.setBatcbatch(ovrduerec.batcbatch);
		hdivIO.setTranno(ovrduerec.tranno);
		hdivIO.setDivdIntCapDate(99999999);
		hdivIO.setCntcurr(ovrduerec.cntcurr);
		hdivIO.setDivdType("D");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		/* Effectively, option processing has been actioned on this*/
		hdivIO.setDivdOptprocTranno(ovrduerec.tranno);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setDivdAmount(wsaaValuesInner.wsaaBasicDvd);
		hdivIO.setDivdRate(wsaaValuesInner.wsaaDivdRate);
		hdivIO.setDivdRtEffdt(wsaaValuesInner.wsaaRateDate);
		hdivIO.setFormat(lifeatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			dbError9100();
		}
	}

protected void puaDvdHdiv7725()
	{
		/*    Read HPUA and allocate dividend from these paid-up addition*/
		hpuaIO.setChdrcoy(ovrduerec.chdrcoy);
		hpuaIO.setChdrnum(ovrduerec.chdrnum);
		hpuaIO.setLife(ovrduerec.life);
		hpuaIO.setCoverage(ovrduerec.coverage);
		hpuaIO.setRider(ovrduerec.rider);
		hpuaIO.setPlanSuffix(ovrduerec.planSuffix);
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		hpuaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)
		&& isNE(hpuaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			dbError9100();
		}
		if (isEQ(hpuaIO.getStatuz(), varcom.endp)
		|| isNE(hpuaIO.getChdrcoy(), ovrduerec.chdrcoy)
		|| isNE(hpuaIO.getChdrnum(), ovrduerec.chdrnum)
		|| isNE(hpuaIO.getLife(), ovrduerec.life)
		|| isNE(hpuaIO.getCoverage(), ovrduerec.coverage)
		|| isNE(hpuaIO.getRider(), ovrduerec.rider)
		|| isNE(hpuaIO.getPlanSuffix(), ovrduerec.planSuffix)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hpuaIO.getStatuz(), varcom.endp))) {
			dividendPaidupUpdate7728();
		}
		
		/* Pass unit statement date in OVRD- to update COVR*/
		ovrduerec.newBonusDate.set(wsaaNewBonusDate);
	}

protected void dividendPaidupUpdate7728()
	{
		dividend7728();
		nextHpua7728();
	}

protected void dividend7728()
	{
		/*    Skip this HPUA if it is not Dividend participant*/
		if (isNE(hpuaIO.getDivdParticipant(), "Y")) {
			return ;
		}
		/*    Find dividend allocation subroutine from T6639.*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t6639);
		wsaaT6639Divdmth.set(hcsdIO.getZcshdivmth());
		wsaaT6639Pstcd.set(hpuaIO.getPstatcode());
		itdmIO.setItemitem(wsaaT6639Item);
		itdmIO.setItmfrm(wsaaNewBonusDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		itdmIO.setFormat(lifeatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6639)
		|| isNE(itdmIO.getItemitem(), wsaaT6639Item)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			return ;
		}
		t6639rec.t6639Rec.set(itdmIO.getGenarea());
		/*    Check if dividend allocation subroutine is defined*/
		if (isEQ(t6639rec.revBonusProg, SPACES)) {
			return ;
		}
		/*    Prepare to call cash dividend allocation subroutine*/
		initialize(hdivdrec.dividendRec);
		hdivdrec.chdrChdrcoy.set(ovrduerec.chdrcoy);
		hdivdrec.chdrChdrnum.set(ovrduerec.chdrnum);
		hdivdrec.lifeLife.set(ovrduerec.life);
		hdivdrec.covrCoverage.set(ovrduerec.coverage);
		hdivdrec.covrRider.set(ovrduerec.rider);
		hdivdrec.plnsfx.set(ovrduerec.planSuffix);
		hdivdrec.cntcurr.set(ovrduerec.cntcurr);
		hdivdrec.cnttype.set(ovrduerec.cnttype);
		hdivdrec.transcd.set(ovrduerec.trancode);
		hdivdrec.premStatus.set(hpuaIO.getPstatcode());
		hdivdrec.crtable.set(hpuaIO.getCrtable());
		hdivdrec.sumin.set(hpuaIO.getSumin());
		hdivdrec.effectiveDate.set(ovrduerec.effdate);
		hdivdrec.ptdate.set(ovrduerec.ptdate);
		hdivdrec.allocMethod.set(covrmjaIO.getBonusInd());
		hdivdrec.mortcls.set(covrmjaIO.getMortcls());
		hdivdrec.sex.set(covrmjaIO.getSex());
		hdivdrec.issuedAge.set(hpuaIO.getAnbAtCcd());
		hdivdrec.tranno.set(ovrduerec.tranno);
		hdivdrec.divdCalcMethod.set(hcsdIO.getZcshdivmth());
		hdivdrec.periodFrom.set(covrmjaIO.getUnitStatementDate());
		hdivdrec.periodTo.set(wsaaNewBonusDate);
		hdivdrec.crrcd.set(hpuaIO.getCrrcd());
		hdivdrec.cessDate.set(hpuaIO.getRiskCessDate());
		hdivdrec.annualisedPrem.set(hpuaIO.getSingp());
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		hdivdrec.divdRate.set(ZERO);
		hdivdrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Dividend Paid-up Allocation Subroutine*/
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz, varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			systemError9000();
		}
		/*    When the dividend amount from allocation is not zeroes,*/
		/*    write a HDIV (dividend detail) and update HDIS (dividend*/
		/*    history) if exists or create one.*/
		if (isEQ(hdivdrec.divdAmount, 0)) {
			return ;
		}
		/* Write HDIV to denote the new dividend allocated*/
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(ovrduerec.chdrcoy);
		hdivIO.setChdrnum(ovrduerec.chdrnum);
		hdivIO.setLife(ovrduerec.life);
		hdivIO.setCoverage(ovrduerec.coverage);
		hdivIO.setRider(ovrduerec.rider);
		hdivIO.setPlanSuffix(ovrduerec.planSuffix);
		hdivIO.setPuAddNbr(hpuaIO.getPuAddNbr());
		hdivIO.setEffdate(ovrduerec.effdate);
		hdivIO.setDivdAllocDate(ovrduerec.effdate);
		hdivIO.setBatccoy(ovrduerec.chdrcoy);
		hdivIO.setBatcbrn(ovrduerec.batcbrn);
		hdivIO.setBatcactyr(ovrduerec.acctyear);
		hdivIO.setBatcactmn(ovrduerec.acctmonth);
		hdivIO.setBatctrcde(ovrduerec.trancode);
		hdivIO.setBatcbatch(ovrduerec.batcbatch);
		hdivIO.setTranno(ovrduerec.tranno);
		hdivIO.setDivdIntCapDate(99999999);
		hdivIO.setCntcurr(ovrduerec.cntcurr);
		hdivIO.setDivdRate(hdivdrec.divdRate);
		hdivIO.setDivdRtEffdt(hdivdrec.rateDate);
		hdivIO.setDivdType("D");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		/* Effectively, option processing has been actioned on this*/
		hdivIO.setDivdOptprocTranno(ovrduerec.tranno);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setDivdAmount(hdivdrec.divdAmount);
		hdivIO.setFormat(lifeatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			dbError9100();
		}
	}

protected void nextHpua7728()
	{
		hpuaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)
		&& isNE(hpuaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			dbError9100();
		}
		if (isEQ(hpuaIO.getStatuz(), varcom.endp)
		|| isNE(hpuaIO.getChdrcoy(), ovrduerec.chdrcoy)
		|| isNE(hpuaIO.getChdrnum(), ovrduerec.chdrnum)
		|| isNE(hpuaIO.getLife(), ovrduerec.life)
		|| isNE(hpuaIO.getCoverage(), ovrduerec.coverage)
		|| isNE(hpuaIO.getRider(), ovrduerec.rider)
		|| isNE(hpuaIO.getPlanSuffix(), ovrduerec.planSuffix)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void postBasicCv7730()
	{
		start7731();
	}

protected void start7731()
	{
		/*    DR Cash Value of Component.*/
		if (isEQ(wsaaValuesInner.wsaaBasicCv, ZERO)) {
			return ;
		}
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaBasicCv);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[2]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[2]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[2]);
			lifacmvrec.glsign.set(wsaaT5645Sign[2]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[2]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			/*        MOVE WSAA-RLDGACCT      TO LIFA-RLDGACCT                 */
			lifacmvrec.tranref.set(wsaaRldgacct);
			lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[1]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[1]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[1]);
			lifacmvrec.glsign.set(wsaaT5645Sign[1]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[1]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void postPuaCv7740()
	{
		start7741();
	}

protected void start7741()
	{
		/*  DR Cash Value of PUA*/
		if (isEQ(wsaaValuesInner.wsaaPuaCv, ZERO)) {
			return ;
		}
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaPuaCv);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[4]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[4]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[4]);
			lifacmvrec.glsign.set(wsaaT5645Sign[4]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[4]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[3]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[3]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[3]);
			lifacmvrec.glsign.set(wsaaT5645Sign[3]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[3]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void divAccting7750()
	{
		start7751();
	}

protected void start7751()
	{
		/* Update the Cash Dividend Summary File to denote withdrawal*/
		hdisIO.setDataKey(SPACES);
		hdisIO.setChdrcoy(ovrduerec.chdrcoy);
		hdisIO.setChdrnum(ovrduerec.chdrnum);
		hdisIO.setLife(ovrduerec.life);
		hdisIO.setCoverage(ovrduerec.coverage);
		hdisIO.setRider(ovrduerec.rider);
		hdisIO.setPlanSuffix(ovrduerec.planSuffix);
		hdisIO.setFormat(lifeatsInner.hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)
		&& isNE(hdisIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hdisIO.getParams());
			dbError9100();
		}
		if (isEQ(hdisIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		wsaaValuesInner.wsaaHdisOsInt.set(hdisIO.getOsInterest());
		if (isGT(wsaaValuesInner.wsaaNewDvd, 0)) {
			postNewDivdAlloc7725();
		}
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			dbError9100();
		}
		hdisIO.setFunction(varcom.writs);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			dbError9100();
		}
		if (isEQ(wsaaValuesInner.wsaaDvd, ZERO)) {
			return ;
		}
		/* Write HDIV to denote the dividend withdrawal has been taken*/
		/* place.*/
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(ovrduerec.chdrcoy);
		hdivIO.setChdrnum(ovrduerec.chdrnum);
		hdivIO.setLife(ovrduerec.life);
		hdivIO.setCoverage(ovrduerec.coverage);
		hdivIO.setRider(ovrduerec.rider);
		hdivIO.setPlanSuffix(ovrduerec.planSuffix);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setEffdate(ovrduerec.effdate);
		hdivIO.setDivdAllocDate(ovrduerec.effdate);
		hdivIO.setBatccoy(ovrduerec.chdrcoy);
		hdivIO.setBatcbrn(ovrduerec.batcbrn);
		hdivIO.setBatcactyr(ovrduerec.acctyear);
		hdivIO.setBatcactmn(ovrduerec.acctmonth);
		hdivIO.setBatctrcde(ovrduerec.trancode);
		hdivIO.setBatcbatch(ovrduerec.batcbatch);
		hdivIO.setTranno(ovrduerec.tranno);
		hdivIO.setDivdIntCapDate(99999999);
		hdivIO.setCntcurr(ovrduerec.cntcurr);
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(99999999);
		hdivIO.setDivdType("C");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		setPrecision(hdivIO.getDivdAmount(), 0);
		hdivIO.setDivdAmount(mult(wsaaValuesInner.wsaaDvd, -1));
		hdivIO.setFormat(lifeatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			dbError9100();
		}
	}

protected void divIntAccting7760()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start7761();
				case dividendWithdrawal7763: 
					dividendWithdrawal7763();
				case osInt7764: 
					osInt7764();
				case bookNewInterest7765: 
					bookNewInterest7765();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7761()
	{
		if (isEQ(wsaaValuesInner.wsaaInt, ZERO)) {
			goTo(GotoLabel.dividendWithdrawal7763);
		}
		/* Write HDIV to denote the dividend int withdrawal has been taken*/
		/* place.*/
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(ovrduerec.chdrcoy);
		hdivIO.setChdrnum(ovrduerec.chdrnum);
		hdivIO.setLife(ovrduerec.life);
		hdivIO.setCoverage(ovrduerec.coverage);
		hdivIO.setRider(ovrduerec.rider);
		hdivIO.setPlanSuffix(ovrduerec.planSuffix);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setEffdate(ovrduerec.effdate);
		hdivIO.setDivdAllocDate(ovrduerec.effdate);
		hdivIO.setBatccoy(ovrduerec.chdrcoy);
		hdivIO.setBatcbrn(ovrduerec.batcbrn);
		hdivIO.setBatcactyr(ovrduerec.acctyear);
		hdivIO.setBatcactmn(ovrduerec.acctmonth);
		hdivIO.setBatctrcde(ovrduerec.trancode);
		hdivIO.setBatcbatch(ovrduerec.batcbatch);
		hdivIO.setTranno(ovrduerec.tranno);
		hdivIO.setDivdIntCapDate(99999999);
		hdivIO.setCntcurr(ovrduerec.cntcurr);
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(99999999);
		hdivIO.setDivdType("I");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		setPrecision(hdivIO.getDivdAmount(), 0);
		hdivIO.setDivdAmount(mult(wsaaValuesInner.wsaaInt, -1));
		hdivIO.setFormat(lifeatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		if (isNE(hdivIO.getDivdAmount(), ZERO)) {
			SmartFileCode.execute(appVars, hdivIO);
			if (isNE(hdivIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(hdivIO.getParams());
				dbError9100();
			}
		}
	}

protected void dividendWithdrawal7763()
	{
		/*  DR Accumulated Dividend*/
		if (isEQ(wsaaValuesInner.wsaaDvd, ZERO)) {
			goTo(GotoLabel.osInt7764);
		}
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaDvd);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[5]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[5]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[5]);
		lifacmvrec.glsign.set(wsaaT5645Sign[5]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[5]);
		if (isNE(hdisIO.getSacscode(), SPACES)) {
			lifacmvrec.sacscode.set(hdisIO.getSacscode());
		}
		if (isNE(hdisIO.getSacstyp(), SPACES)) {
			lifacmvrec.sacstyp.set(hdisIO.getSacstyp());
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		callLifacmv6000();
		compute(wsaaValuesInner.wsaaNewInt, 0).set(sub(wsaaValuesInner.wsaaInt, wsaaValuesInner.wsaaHdisOsInt));
	}

protected void osInt7764()
	{
		/*    IF WSAA-HDIS-OS-INT         = ZERO*/
		if (isEQ(wsaaValuesInner.wsaaInt, ZERO)) {
			goTo(GotoLabel.bookNewInterest7765);
		}
		/*    MOVE WSAA-HDIS-OS-INT       TO LIFA-ORIGAMT.*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaInt);
		/* DR Dividend Interest*/
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[7]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[7]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[7]);
			lifacmvrec.glsign.set(wsaaT5645Sign[7]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[7]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[6]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[6]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[6]);
			lifacmvrec.glsign.set(wsaaT5645Sign[6]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[6]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void bookNewInterest7765()
	{
		/*  DR Outstanding Interest*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaNewInt);
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[9]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[9]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[9]);
			lifacmvrec.glsign.set(wsaaT5645Sign[9]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[9]);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[8]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[8]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[8]);
			lifacmvrec.glsign.set(wsaaT5645Sign[8]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[8]);
		}
		callLifacmv6000();
		/* DR Interest Suspense (Withdrawal)*/
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[27]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[27]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[27]);
			lifacmvrec.glsign.set(wsaaT5645Sign[27]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[27]);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[26]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[26]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[26]);
			lifacmvrec.glsign.set(wsaaT5645Sign[26]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[26]);
		}
		callLifacmv6000();
		/* Write HDIV to denote the declaration of the Outstanding*/
		/* Interest.*/
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(ovrduerec.chdrcoy);
		hdivIO.setChdrnum(ovrduerec.chdrnum);
		hdivIO.setLife(ovrduerec.life);
		hdivIO.setCoverage(ovrduerec.coverage);
		hdivIO.setRider(ovrduerec.rider);
		hdivIO.setPlanSuffix(ovrduerec.planSuffix);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setEffdate(ovrduerec.effdate);
		hdivIO.setDivdAllocDate(ovrduerec.effdate);
		hdivIO.setBatccoy(ovrduerec.chdrcoy);
		hdivIO.setBatcbrn(ovrduerec.batcbrn);
		hdivIO.setBatcactyr(ovrduerec.acctyear);
		hdivIO.setBatcactmn(ovrduerec.acctmonth);
		hdivIO.setBatctrcde(ovrduerec.trancode);
		hdivIO.setBatcbatch(ovrduerec.batcbatch);
		hdivIO.setTranno(ovrduerec.tranno);
		hdivIO.setDivdIntCapDate(99999999);
		hdivIO.setCntcurr(ovrduerec.cntcurr);
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(99999999);
		hdivIO.setDivdType("I");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setDivdAmount(wsaaValuesInner.wsaaNewInt);
		hdivIO.setFormat(lifeatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			dbError9100();
		}
	}

protected void updateHdis7768()
	{
		start7768();
	}

protected void start7768()
	{
		/* If the previous read is not OK, skip updating of HDIS.*/
		if (isNE(hdisIO.getStatuz(), varcom.oK)) {
			return ;
		}
		setNextInterestDate7769a();
		hdisIO.setNextIntDate(wsaaNextIntDate);
		hdisIO.setLastIntDate(ovrduerec.ptdate);
		hdisIO.setValidflag("1");
		hdisIO.setTranno(ovrduerec.tranno);
		hdisIO.setOsInterest(ZERO);
		if (isGT(wsaaValuesInner.wsaaNewDvd, 0)) {
			setPrecision(hdisIO.getBalAtLastDivd(), 2);
			hdisIO.setBalAtLastDivd(add(hdisIO.getBalAtLastDivd(), wsaaValuesInner.wsaaNewDvd));
			setPrecision(hdisIO.getBalSinceLastCap(), 2);
			hdisIO.setBalSinceLastCap(add(hdisIO.getBalSinceLastCap(), wsaaValuesInner.wsaaNewDvd));
			hdisIO.setLastDivdDate(wsaaNewBonusDate);
		}
		hdisIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			dbError9100();
		}
	}

protected void setNextInterestDate7769a()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start7769a();
				case datcon27769a: 
					datcon27769a();
				case exit7769a: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7769a()
	{
		/* This paragraph sets the Next Dividend Interest Date*/
		wsaaNextIntDate.set(hdisIO.getLastIntDate());
		/* First, try using DATCON4, if not O-K, use DATCON2.*/
		datcon4rec.intDate1.set(wsaaNextIntDate);
		datcon4rec.frequency.set(th501rec.freqcy02);
		datcon4rec.freqFactor.set(1);
		datcon4rec.billmonth.set(hdisIO.getIntDueMm());
		datcon4rec.billday.set(hdisIO.getIntDueDd());
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			goTo(GotoLabel.datcon27769a);
		}
		wsaaNextIntDate.set(datcon4rec.intDate2);
		/* If not yet greater than the PTD, use DATCON4 to advance the*/
		/* next interest date.*/
		if (isLTE(wsaaNextIntDate, ovrduerec.ptdate)) {
			while ( !(isGT(wsaaNextIntDate, ovrduerec.ptdate))) {
				datcon4rec.intDate1.set(wsaaNextIntDate);
				datcon4rec.frequency.set(th501rec.freqcy02);
				datcon4rec.freqFactor.set(1);
				datcon4rec.billmonth.set(hdisIO.getIntDueMm());
				datcon4rec.billday.set(hdisIO.getIntDueDd());
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				if (isNE(datcon4rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon4rec.datcon4Rec);
					syserrrec.statuz.set(datcon4rec.statuz);
					systemError9000();
				}
				wsaaNextIntDate.set(datcon4rec.intDate2);
			}
			
		}
		/* If DATCON4 is used, the WSAA-NEXT-INT-DATE should be valid.*/
		/* Therefore, exit this paragraph now.*/
		goTo(GotoLabel.exit7769a);
	}

protected void datcon27769a()
	{
		datcon2rec.intDate1.set(hdisIO.getLastIntDate());
		datcon2rec.frequency.set(th501rec.freqcy02);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError9000();
		}
		wsaaNextIntDate.set(datcon2rec.intDate2);
		/* If not yet greater than the PTD, use DATCON2 to advance the*/
		/* next interest date.*/
		if (isLTE(wsaaNextIntDate, ovrduerec.ptdate)) {
			while ( !(isGT(wsaaNextIntDate, ovrduerec.ptdate))) {
				datcon2rec.intDate1.set(wsaaNextIntDate);
				datcon2rec.frequency.set(th501rec.freqcy02);
				datcon2rec.freqFactor.set(1);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					systemError9000();
				}
				wsaaNextIntDate.set(datcon2rec.intDate2);
			}
			
		}
		if (isNE(th501rec.hfixdd02, SPACES)) {
			wsaaNextintDay.set(th501rec.hfixdd02);
		}
		if (isNE(th501rec.hfixmm02, SPACES)) {
			if (isGT(th501rec.hfixmm02, wsaaNextintMth)) {
				wsaaNextintYear.subtract(1);
			}
			wsaaNextintMth.set(th501rec.hfixmm02);
		}
		/*    Ensure this Next Interest Allocation date is valid*/
		datcon1rec.intDate.set(wsaaNextIntDate);
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			wsaaNextintDay.set(wsaaLastdd[wsaaNextintMth.toInt()]);
			if (isEQ(wsaaNextintMth, 2)
			&& (setPrecision(wsaaNextintYear, 0)
			&& isEQ((mult((div(wsaaNextintYear, 4)), 4)), wsaaNextintYear))
			&& isNE(wsaaNextintYear, 1900)) {
				wsaaNextintDay.add(1);
			}
		}
	}

protected void etiPremium7770()
	{
		start7771();
	}

protected void start7771()
	{
		/*  CR Single Premium*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaEtiCost);
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[11]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[11]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[11]);
			lifacmvrec.glsign.set(wsaaT5645Sign[11]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[11]);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[10]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[10]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[10]);
			lifacmvrec.glsign.set(wsaaT5645Sign[10]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[10]);
		}
		callLifacmv6000();
	}

protected void refund7780()
	{
		/*START*/
		/*  CR Suspense for Refund*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaRefund);
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[12]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[12]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[12]);
		lifacmvrec.glsign.set(wsaaT5645Sign[12]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[12]);
		callLifacmv6000();
		/*EXIT*/
	}

protected void commClawback7790()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start7791();
				case callAgcmio7794: 
					callAgcmio7794();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7791()
	{
		/* Read the first AGCM record for this contract.*/
		agcmIO.setParams(SPACES);
		agcmIO.setChdrnum(ovrduerec.chdrnum);
		agcmIO.setChdrcoy(ovrduerec.chdrcoy);
		agcmIO.setPlanSuffix(ZERO);
		agcmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		agcmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
 
		agcmIO.setFormat(lifeatsInner.agcmrec);
	}

protected void callAgcmio7794()
	{
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), varcom.oK)
		&& isNE(agcmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			dbError9100();
		}
		/* If the end of the AGCM file has been reached or an AGCM*/
		/* record was read but not for the contract being processed,*/
		/* then exit the section.*/
		if (isEQ(agcmIO.getStatuz(), varcom.endp)
		|| isNE(agcmIO.getChdrcoy(), ovrduerec.chdrcoy)
		|| isNE(agcmIO.getChdrnum(), ovrduerec.chdrnum)) {
			return ;
		}
		/* for the AGCM record just read.*/
		if (isEQ(agcmIO.getChdrcoy(), ovrduerec.chdrcoy)
		&& isEQ(agcmIO.getChdrnum(), ovrduerec.chdrnum)
		&& isEQ(agcmIO.getLife(), ovrduerec.life)
		&& isEQ(agcmIO.getCoverage(), ovrduerec.coverage)
		&& isEQ(agcmIO.getRider(), ovrduerec.rider)
		&& isEQ(agcmIO.getPlanSuffix(), ovrduerec.planSuffix)
		&& isNE(agcmIO.getCompay(), agcmIO.getComern())) {
			updateAgcmFile8000();
		}
		/* Loop round to see if there any other AGCM records to*/
		/* process for the contract.*/
		agcmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callAgcmio7794);
	}

protected void updateAgcmFile8000()
	{
		readh8010();
		rewrt8020();
		writr8030();
	}

	/**
	* <pre>
	* previous processing loop on AGCM is not upset.
	* created with commission paid set to earned, valid-flag of "1"
	* and correct tranno. Also create accounts records (ACMV).
	* </pre>
	*/
protected void readh8010()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmIO.getAgntnum());
		agcmdbcIO.setLife(agcmIO.getLife());
		agcmdbcIO.setCoverage(agcmIO.getCoverage());
		agcmdbcIO.setRider(agcmIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		agcmdbcIO.setFormat(lifeatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError9100();
		}
	}

protected void rewrt8020()
	{
		agcmdbcIO.setValidflag("2");
		agcmdbcIO.setCurrto(ovrduerec.effdate);
		agcmdbcIO.setFunction(varcom.rewrt);
		agcmdbcIO.setFormat(lifeatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError9100();
		}
	}

protected void writr8030()
	{
		/* correct tranno, and new commission paid.*/
		compute(wsaaValuesInner.wsaaCommRecovered, 2).set(sub(agcmIO.getCompay(), agcmIO.getComern()));
		if (isEQ(agcmIO.getOvrdcat(), "O")) {
			overrideCommAcmv8200();
		}
		else {
			commClawbackAcmv8100();
		}
		agcmdbcIO.setCompay(agcmIO.getComern());
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setTranno(ovrduerec.tranno);
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writr);
		agcmdbcIO.setFormat(lifeatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError9100();
		}
	}

protected void commClawbackAcmv8100()
	{
		para8110();
	}

protected void para8110()
	{
		if (isEQ(wsaaValuesInner.wsaaCommRecovered, 0)) {
			return ;
		}
		/* Write a ACMV for the clawback commission.*/
		lifacmvrec.rdocnum.set(ovrduerec.chdrnum);
		lifacmvrec.batccoy.set(ovrduerec.chdrcoy);
		lifacmvrec.rldgcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.batcactyr.set(ovrduerec.acctyear);
		lifacmvrec.batctrcde.set(ovrduerec.trancode);
		lifacmvrec.batcactmn.set(ovrduerec.acctmonth);
		lifacmvrec.batcbatch.set(ovrduerec.batcbatch);
		lifacmvrec.batcbrn.set(ovrduerec.batcbrn);
		lifacmvrec.tranno.set(ovrduerec.tranno);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(ovrduerec.ptdate);
		lifacmvrec.tranref.set(ovrduerec.chdrnum);
		lifacmvrec.user.set(ovrduerec.user);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
		lifacmvrec.transactionDate.set(ovrduerec.tranDate);
		lifacmvrec.transactionTime.set(ovrduerec.tranTime);
		lifacmvrec.termid.set(ovrduerec.termid);
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaCommRecovered);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[17]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[17]);
		lifacmvrec.glsign.set(wsaaT5645Sign[17]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[17]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[17]);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.function.set("PSTW");
		callLifacmv6000();
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[21]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[21]);
			lifacmvrec.glsign.set(wsaaT5645Sign[21]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[21]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[21]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlansuff.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[18]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[18]);
			lifacmvrec.glsign.set(wsaaT5645Sign[18]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[18]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[18]);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(ovrduerec.chdrnum);
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void overrideCommAcmv8200()
	{
		para8210();
	}

protected void para8210()
	{
		if (isEQ(wsaaValuesInner.wsaaCommRecovered, 0)) {
			return ;
		}
		/* Write a ACMV for the clawback commission.*/
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rdocnum.set(ovrduerec.chdrnum);
		lifacmvrec.batccoy.set(ovrduerec.chdrcoy);
		lifacmvrec.rldgcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.batcactyr.set(ovrduerec.acctyear);
		lifacmvrec.batctrcde.set(ovrduerec.trancode);
		lifacmvrec.batcactmn.set(ovrduerec.acctmonth);
		lifacmvrec.batcbatch.set(ovrduerec.batcbatch);
		lifacmvrec.batcbrn.set(ovrduerec.batcbrn);
		lifacmvrec.tranno.set(ovrduerec.tranno);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(ovrduerec.ptdate);
		lifacmvrec.tranref.set(ovrduerec.chdrnum);
		lifacmvrec.user.set(ovrduerec.user);
		lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
		lifacmvrec.transactionDate.set(ovrduerec.tranDate);
		lifacmvrec.transactionTime.set(ovrduerec.tranTime);
		lifacmvrec.termid.set(ovrduerec.termid);
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaCommRecovered);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[19]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[19]);
		lifacmvrec.glsign.set(wsaaT5645Sign[19]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[19]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[19]);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.function.set("PSTW");
		callLifacmv6000();
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[22]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[22]);
			lifacmvrec.glsign.set(wsaaT5645Sign[22]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[22]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[22]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlansuff.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[20]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[20]);
			lifacmvrec.glsign.set(wsaaT5645Sign[20]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[20]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[20]);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(ovrduerec.chdrnum);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.function.set("PSTW");
		callLifacmv6000();
	}

protected void writeRh5388300()
	{
		start8310();
	}

protected void start8310()
	{
		if (newPageReq.isTrue()) {
			/*        MOVE SPACES             TO RH538H01-O*/
			printerFile.printRh538h01(rh538h01O, indicArea);
			wsaaOverflow.set("N");
		}
		rh538d01O.set(SPACES);
		/* Format print line.*/
		rd01Chdrnum.set(ovrduerec.chdrnum);
		rd01Cnttype.set(ovrduerec.cnttype);
		rd01Cntcurr.set(ovrduerec.cntcurr);
		rd01Surrval.set(wsaaValuesInner.wsaaTotSv);
		rd01Loanorigam.set(wsaaValuesInner.wsaaTotLoan);
		rd01Refundfe.set(wsaaValuesInner.wsaaRefund);
		rd01Descrip.set(wsaaException);
		printerFile.printRh538d01(rh538d01O, indicArea);
	}

protected void computeRefund8400()
	{
		start8400();
	}

protected void start8400()
	{
		/*  Read T6642 to get actuarial basis to read TT562 with           */
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t6642);
		itdmIO.setItemitem(ovrduerec.crtable);
		itdmIO.setItmfrm(ovrduerec.effdate);
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6642)
		|| isNE(itdmIO.getItemitem(), ovrduerec.crtable)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(ovrduerec.crtable);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.g579);
			dbError9100();
		}
		else {
			t6642rec.t6642Rec.set(itdmIO.getGenarea());
		}
		/*  Read TT562 using T6642 actuarial basis value.                  */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItmfrm(varcom.vrcmMaxDate);
		itemIO.setItemtabl(tablesInner.tt562);
		itemIO.setItemitem(t6642rec.slsumass);
		itemIO.setFunction(varcom.readr);
		itemIO.setStatuz(SPACES);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		if (isEQ(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getDataKey());
			syserrrec.statuz.set(errorsInner.tl25);
			dbError9100();
		}
		else {
			tt562rec.tt562Rec.set(itemIO.getGenarea());
		}
		/*  Now determine refund multiplier factor*/
		compute(wsaaTage, 0).set(add(covrmjaIO.getRiskCessTerm(), covrmjaIO.getAnbAtCcd()));
		compute(wsaaAnbp1, 0).set(add(wsaaAnb, 1));
		compute(wsaaTagep1, 0).set(add(wsaaTage, 1));
		wsaaTt562ValuesInner.wsaaFindAge.set(wsaaAnb);
		tt562AgevaluePointer8500();
		wsaaTt562ValuesInner.wsaaNa.set(wsaaTt562ValuesInner.wsaaTt562Valwithdp);
		wsaaTt562ValuesInner.wsaaFindAge.set(wsaaAnbp1);
		tt562AgevaluePointer8500();
		wsaaTt562ValuesInner.wsaaNaplus1.set(wsaaTt562ValuesInner.wsaaTt562Valwithdp);
		wsaaTt562ValuesInner.wsaaFindAge.set(wsaaTage);
		tt562AgevaluePointer8500();
		wsaaTt562ValuesInner.wsaaNb.set(wsaaTt562ValuesInner.wsaaTt562Valwithdp);
		wsaaTt562ValuesInner.wsaaFindAge.set(wsaaTagep1);
		tt562AgevaluePointer8500();
		wsaaTt562ValuesInner.wsaaNbplus1.set(wsaaTt562ValuesInner.wsaaTt562Valwithdp);
		compute(wsaaTt562ValuesInner.wsaaMultfact, 0).set(div((sub(wsaaTt562ValuesInner.wsaaNa, wsaaTt562ValuesInner.wsaaNaplus1)), (sub(wsaaTt562ValuesInner.wsaaNb, wsaaTt562ValuesInner.wsaaNbplus1))));
		/*  Compute refund*/
		compute(wsaaValuesInner.wsaaRefund, 0).set(mult((sub(wsaaValuesInner.wsaaAccumValue, wsaaValuesInner.wsaaEtiCost)), wsaaTt562ValuesInner.wsaaMultfact));
		zrdecplrec.amountIn.set(wsaaValuesInner.wsaaRefund);
		a000CallRounding();
		wsaaValuesInner.wsaaRefund.set(zrdecplrec.amountOut);
	}

protected void tt562AgevaluePointer8500()
	{
		start8500();
	}

protected void start8500()
	{
		/* Get Age value from table*/
		/* Table TT562 has 103 value flds. Corresponding to ages 00 to 102*/
		/*   TT562-NXMONEY               = Age   00*/
		/*   TT562-NXMON occurs 99 times = Ages  01 to  99*/
		/*   TT562-TNXMON-01 to -03      = Ages 100 to 102*/
		wsaaTt562ValuesInner.wsaaTt562Rawval.set(ZERO);
		/*  calculate index into the TT562 field array to pick the TT562*/
		/*  value corresponding to the age passed*/
		if ((isLT(wsaaTt562ValuesInner.wsaaFindAge, wsaaMinimumAge)
		|| isGT(wsaaTt562ValuesInner.wsaaFindAge, wsaaMaximumAge))) {
			wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(ZERO);
			return ;
		}
		if ((isEQ(wsaaTt562ValuesInner.wsaaFindAge, wsaaMinimumAge))) {
			wsaaTt562ValuesInner.wsaaTt562Rawval.set(tt562rec.nxmoney);
		}
		else {
			if ((isGT(wsaaTt562ValuesInner.wsaaFindAge, wsaaMinimumAge)
			&& isLT(wsaaTt562ValuesInner.wsaaFindAge, 100))) {
				wsaaTt562ValuesInner.wsaaTt562Rawval.set(tt562rec.nxmon[wsaaTt562ValuesInner.wsaaFindAge.toInt()]);
			}
			else {
				/*           IF WSAA-FIND-AGE = 100                                */
				/*              MOVE TT562-TNXMON-01 TO WSAA-TT562-RAWVAL          */
				/*           ELSE                                                  */
				/*              IF WSAA-FIND-AGE = 101                             */
				/*                 MOVE TT562-TNXMON-02 TO WSAA-TT562-RAWVAL       */
				/*              ELSE                                               */
				/*                 MOVE TT562-TNXMON-03 TO WSAA-TT562-RAWVAL       */
				compute(wsaaTt562ValuesInner.wsaaTt562Rawval, 0).set(tt562rec.tnxmon[sub(wsaaTt562ValuesInner.wsaaFindAge, 99).toInt()]);
			}
		}
		/* Find out what row the age value required is on*/
		wsaaAgerowPtrInner.wsaaAgerowPtr.set(wsaaTt562ValuesInner.wsaaFindAge);
		if (wsaaAgerowPtrInner.wsaaAgerow1.isTrue()) {
			wsaaTt562ValuesInner.wsaaAgerow.set(1);
		}
		else {
			if (wsaaAgerowPtrInner.wsaaAgerow2.isTrue()) {
				wsaaTt562ValuesInner.wsaaAgerow.set(2);
			}
			else {
				if (wsaaAgerowPtrInner.wsaaAgerow3.isTrue()) {
					wsaaTt562ValuesInner.wsaaAgerow.set(3);
				}
				else {
					if (wsaaAgerowPtrInner.wsaaAgerow4.isTrue()) {
						wsaaTt562ValuesInner.wsaaAgerow.set(4);
					}
					else {
						if (wsaaAgerowPtrInner.wsaaAgerow5.isTrue()) {
							wsaaTt562ValuesInner.wsaaAgerow.set(5);
						}
						else {
							if (wsaaAgerowPtrInner.wsaaAgerow6.isTrue()) {
								wsaaTt562ValuesInner.wsaaAgerow.set(6);
							}
							else {
								if (wsaaAgerowPtrInner.wsaaAgerow7.isTrue()) {
									wsaaTt562ValuesInner.wsaaAgerow.set(7);
								}
								else {
									if (wsaaAgerowPtrInner.wsaaAgerow8.isTrue()) {
										wsaaTt562ValuesInner.wsaaAgerow.set(8);
									}
									else {
										if (wsaaAgerowPtrInner.wsaaAgerow9.isTrue()) {
											wsaaTt562ValuesInner.wsaaAgerow.set(9);
										}
										else {
											if (wsaaAgerowPtrInner.wsaaAgerow10.isTrue()) {
												wsaaTt562ValuesInner.wsaaAgerow.set(10);
											}
											else {
												if (wsaaAgerowPtrInner.wsaaAgerow11.isTrue()) {
													wsaaTt562ValuesInner.wsaaAgerow.set(11);
												}
												else {
													if (wsaaAgerowPtrInner.wsaaAgerow12.isTrue()) {
														wsaaTt562ValuesInner.wsaaAgerow.set(12);
													}
													else {
														if (wsaaAgerowPtrInner.wsaaAgerow13.isTrue()) {
															wsaaTt562ValuesInner.wsaaAgerow.set(13);
														}
														else {
															if (wsaaAgerowPtrInner.wsaaAgerow14.isTrue()) {
																wsaaTt562ValuesInner.wsaaAgerow.set(14);
															}
															else {
																if (wsaaAgerowPtrInner.wsaaAgerow15.isTrue()) {
																	wsaaTt562ValuesInner.wsaaAgerow.set(15);
																}
																else {
																	if (wsaaAgerowPtrInner.wsaaAgerow16.isTrue()) {
																		wsaaTt562ValuesInner.wsaaAgerow.set(16);
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(ZERO);
		/* Depending on the number of decimal places ( from TT562 )*/
		/* compute TT562 value into WS.*/
		if ((isEQ(tt562rec.nxdp[wsaaTt562ValuesInner.wsaaAgerow.toInt()], ZERO))) {
			wsaaTt562ValuesInner.wsaaZeroDp.set(wsaaTt562ValuesInner.wsaaTt562Rawval);
			wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(wsaaTt562ValuesInner.wsaaZeroDp);
		}
		else {
			if ((isEQ(tt562rec.nxdp[wsaaTt562ValuesInner.wsaaAgerow.toInt()], 1))) {
				compute(wsaaTt562ValuesInner.wsaaOneDp, 0).set(div(wsaaTt562ValuesInner.wsaaTt562Rawval, 10));
				wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(wsaaTt562ValuesInner.wsaaOneDp);
			}
			else {
				if ((isEQ(tt562rec.nxdp[wsaaTt562ValuesInner.wsaaAgerow.toInt()], 2))) {
					compute(wsaaTt562ValuesInner.wsaaTwoDp, 0).set(div(wsaaTt562ValuesInner.wsaaTt562Rawval, 100));
					wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(wsaaTt562ValuesInner.wsaaTwoDp);
				}
				else {
					if ((isEQ(tt562rec.nxdp[wsaaTt562ValuesInner.wsaaAgerow.toInt()], 3))) {
						compute(wsaaTt562ValuesInner.wsaaThreeDp, 0).set(div(wsaaTt562ValuesInner.wsaaTt562Rawval, 1000));
						wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(wsaaTt562ValuesInner.wsaaThreeDp);
					}
					else {
						if ((isEQ(tt562rec.nxdp[wsaaTt562ValuesInner.wsaaAgerow.toInt()], 4))) {
							compute(wsaaTt562ValuesInner.wsaaFourDp, 0).set(div(wsaaTt562ValuesInner.wsaaTt562Rawval, 10000));
							wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(wsaaTt562ValuesInner.wsaaFourDp);
						}
						else {
							if ((isEQ(tt562rec.nxdp[wsaaTt562ValuesInner.wsaaAgerow.toInt()], 5))) {
								compute(wsaaTt562ValuesInner.wsaaFiveDp, 0).set(div(wsaaTt562ValuesInner.wsaaTt562Rawval, 100000));
								wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(wsaaTt562ValuesInner.wsaaFiveDp);
							}
							else {
								if ((isEQ(tt562rec.nxdp[wsaaTt562ValuesInner.wsaaAgerow.toInt()], 6))) {
									compute(wsaaTt562ValuesInner.wsaaSixDp, 0).set(div(wsaaTt562ValuesInner.wsaaTt562Rawval, 1000000));
									wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(wsaaTt562ValuesInner.wsaaSixDp);
								}
								else {
									if ((isEQ(tt562rec.nxdp[wsaaTt562ValuesInner.wsaaAgerow.toInt()], 7))) {
										compute(wsaaTt562ValuesInner.wsaaSevenDp, 0).set(div(wsaaTt562ValuesInner.wsaaTt562Rawval, 10000000));
										wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(wsaaTt562ValuesInner.wsaaSevenDp);
									}
									else {
										if ((isEQ(tt562rec.nxdp[wsaaTt562ValuesInner.wsaaAgerow.toInt()], 8))) {
											compute(wsaaTt562ValuesInner.wsaaEightDp, 0).set(div(wsaaTt562ValuesInner.wsaaTt562Rawval, 100000000));
											wsaaTt562ValuesInner.wsaaTt562Valwithdp.set(wsaaTt562ValuesInner.wsaaEightDp);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

protected void openPrinter8800()
	{
		try {
			openFiles8810();
			setUpHeadingCompany8820();
			setUpHeadingBranch8830();
			setUpHeadingDates8840();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void openFiles8810()
	{
		if (isEQ(wsaaPrtOpen, "Y")) {
			goTo(GotoLabel.exit8890);
		}
		/* Open required files.*/
		printerFile.openOutput();
	}

protected void setUpHeadingCompany8820()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(tablesInner.t1693);
		descIO.setDescitem(ovrduerec.chdrcoy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(ovrduerec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(lifeatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			dbError9100();
		}
		rh01Company.set(ovrduerec.chdrcoy);
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch8830()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ovrduerec.chdrcoy);
		descIO.setDesctabl(tablesInner.t1692);
		descIO.setDescitem(ovrduerec.batcbrn);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(ovrduerec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(lifeatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			dbError9100();
		}
		rh01Branch.set(ovrduerec.batcbrn);
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates8840()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(ovrduerec.effdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
		printerFile.printRh538h01(rh538h01O, indicArea);
		wsaaOverflow.set("N");
		/* Flag that the printer has been opened.*/
		wsaaPrtOpen = "Y";
	}

protected void close8900()
	{
		/*CLOSE-FILES*/
		/*  Close any open files.*/
		/*    CLOSE PRINTER-FILE.*/
		ovrduerec.statuz.set(varcom.oK);
		/*EXIT*/
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
				case seExit9090: 
					seExit9090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		ovrduerec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9100();
				case dbExit9190: 
					dbExit9190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		ovrduerec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(ovrduerec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(ovrduerec.cntcurr);
		zrdecplrec.batctrcde.set(ovrduerec.trancode);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError9000();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-TT562-VALUES--INNER
 */
private static final class WsaaTt562ValuesInner { 

	private FixedLengthStringData wsaaTt562Values = new FixedLengthStringData(115);
	private PackedDecimalData wsaaZeroDp = new PackedDecimalData(8, 0).isAPartOf(wsaaTt562Values, 0);
	private PackedDecimalData wsaaOneDp = new PackedDecimalData(8, 1).isAPartOf(wsaaTt562Values, 5);
	private PackedDecimalData wsaaTwoDp = new PackedDecimalData(8, 2).isAPartOf(wsaaTt562Values, 10);
	private PackedDecimalData wsaaThreeDp = new PackedDecimalData(8, 3).isAPartOf(wsaaTt562Values, 15);
	private PackedDecimalData wsaaFourDp = new PackedDecimalData(8, 4).isAPartOf(wsaaTt562Values, 20);
	private PackedDecimalData wsaaFiveDp = new PackedDecimalData(8, 5).isAPartOf(wsaaTt562Values, 25);
	private PackedDecimalData wsaaSixDp = new PackedDecimalData(8, 6).isAPartOf(wsaaTt562Values, 30);
	private PackedDecimalData wsaaSevenDp = new PackedDecimalData(8, 7).isAPartOf(wsaaTt562Values, 35);
	private PackedDecimalData wsaaEightDp = new PackedDecimalData(9, 8).isAPartOf(wsaaTt562Values, 40);
	private PackedDecimalData wsaaMultfact = new PackedDecimalData(18, 9).isAPartOf(wsaaTt562Values, 45);
	private PackedDecimalData wsaaNa = new PackedDecimalData(18, 9).isAPartOf(wsaaTt562Values, 55);
	private PackedDecimalData wsaaNaplus1 = new PackedDecimalData(18, 9).isAPartOf(wsaaTt562Values, 65);
	private PackedDecimalData wsaaNb = new PackedDecimalData(18, 9).isAPartOf(wsaaTt562Values, 75);
	private PackedDecimalData wsaaNbplus1 = new PackedDecimalData(18, 9).isAPartOf(wsaaTt562Values, 85);
	private PackedDecimalData wsaaTt562Valwithdp = new PackedDecimalData(18, 9).isAPartOf(wsaaTt562Values, 95);
	private PackedDecimalData wsaaTt562Rawval = new PackedDecimalData(8, 0).isAPartOf(wsaaTt562Values, 105);
	private ZonedDecimalData wsaaAgerow = new ZonedDecimalData(2, 0).isAPartOf(wsaaTt562Values, 110).setUnsigned();
	private ZonedDecimalData wsaaFindAge = new ZonedDecimalData(3, 0).isAPartOf(wsaaTt562Values, 112).setUnsigned();
}
/*
 * Class transformed  from Data Structure WSAA-AGEROW-PTR--INNER
 */
private static final class WsaaAgerowPtrInner { 

	private ZonedDecimalData wsaaAgerowPtr = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private Validator wsaaAgerow1 = new Validator(wsaaAgerowPtr, 0,1,2,3,4,5,6);
	private Validator wsaaAgerow2 = new Validator(wsaaAgerowPtr, 7,8,9,10,11,12,13);
	private Validator wsaaAgerow3 = new Validator(wsaaAgerowPtr, 14,15,16,17,18,19,20);
	private Validator wsaaAgerow4 = new Validator(wsaaAgerowPtr, 21,22,23,24,25,26,27);
	private Validator wsaaAgerow5 = new Validator(wsaaAgerowPtr, 28,29,30,31,32,33,34);
	private Validator wsaaAgerow6 = new Validator(wsaaAgerowPtr, 35,36,37,38,39,40,41);
	private Validator wsaaAgerow7 = new Validator(wsaaAgerowPtr, 42,43,44,45,46,47,48);
	private Validator wsaaAgerow8 = new Validator(wsaaAgerowPtr, 49,50,51,52,53,54,55);
	private Validator wsaaAgerow9 = new Validator(wsaaAgerowPtr, 56,57,58,59,60,61,62);
	private Validator wsaaAgerow10 = new Validator(wsaaAgerowPtr, 63,64,65,66,67,68,69);
	private Validator wsaaAgerow11 = new Validator(wsaaAgerowPtr, 70,71,72,73,74,75,76);
	private Validator wsaaAgerow12 = new Validator(wsaaAgerowPtr, 77,78,79,80,81,82,83);
	private Validator wsaaAgerow13 = new Validator(wsaaAgerowPtr, 84,85,86,87,88,89,90);
	private Validator wsaaAgerow14 = new Validator(wsaaAgerowPtr, 91,92,93,94,95,96,97);
	private Validator wsaaAgerow15 = new Validator(wsaaAgerowPtr, 98,99,100,101,102,103,104);
	private Validator wsaaAgerow16 = new Validator(wsaaAgerowPtr, 105,106,107,108,109,110);
}
/*
 * Class transformed  from Data Structure WSAA-VALUES--INNER
 */
private static final class WsaaValuesInner { 
		/* WSAA-VALUES */
	private PackedDecimalData wsaaBasicCv = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaPuaCv = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaDvd = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaInt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaNewInt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaHdisOsInt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaTotSv = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaTotLoan = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaRefund = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaCommRecovered = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaEtiCost = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaEtiSi = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaEtiAct = new PackedDecimalData(18, 3).setUnsigned();
	private PackedDecimalData wsaaDvdSi = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAccumValue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaNewDvd = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaBasicDvd = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaDivdRate = new PackedDecimalData(8, 5);
	private PackedDecimalData wsaaRateDate = new PackedDecimalData(8, 0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t1688 = new FixedLengthStringData(6).init("T1688");
	private FixedLengthStringData t1692 = new FixedLengthStringData(6).init("T1692");
	private FixedLengthStringData t1693 = new FixedLengthStringData(6).init("T1693");
	private FixedLengthStringData t5645 = new FixedLengthStringData(6).init("T5645");
	private FixedLengthStringData t5687 = new FixedLengthStringData(6).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(6).init("T5688");
	private FixedLengthStringData t6598 = new FixedLengthStringData(6).init("T6598");
	private FixedLengthStringData t6639 = new FixedLengthStringData(6).init("T6639");
	private FixedLengthStringData th506 = new FixedLengthStringData(6).init("TH506");
	private FixedLengthStringData th536 = new FixedLengthStringData(6).init("TH536");
	private FixedLengthStringData th501 = new FixedLengthStringData(6).init("TH501");
	private FixedLengthStringData t6642 = new FixedLengthStringData(6).init("T6642");
	private FixedLengthStringData tt562 = new FixedLengthStringData(6).init("TT562");
}
/*
 * Class transformed  from Data Structure LIFEATS--INNER
 */
private static final class LifeatsInner { 
		/* LIFEATS */
	private FixedLengthStringData agcmdbcrec = new FixedLengthStringData(10).init("AGCMDBCREC");
	private FixedLengthStringData agcmrec = new FixedLengthStringData(10).init("AGCMREC   ");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData hdisrec = new FixedLengthStringData(10).init("HDISREC");
	private FixedLengthStringData hdivrec = new FixedLengthStringData(10).init("HDIVREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData f259 = new FixedLengthStringData(4).init("F259");
	private FixedLengthStringData e076 = new FixedLengthStringData(4).init("E076");
	private FixedLengthStringData e308 = new FixedLengthStringData(4).init("E308");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData h053 = new FixedLengthStringData(4).init("H053");
	private FixedLengthStringData h791 = new FixedLengthStringData(4).init("H791");
	private FixedLengthStringData hl36 = new FixedLengthStringData(4).init("HL36");
	private FixedLengthStringData hl37 = new FixedLengthStringData(4).init("HL37");
	private FixedLengthStringData hl39 = new FixedLengthStringData(4).init("HL39");
	private FixedLengthStringData z039 = new FixedLengthStringData(4).init("Z039");
	private FixedLengthStringData g579 = new FixedLengthStringData(4).init("G579");
	private FixedLengthStringData tl25 = new FixedLengthStringData(4).init("TL25");
}
}
