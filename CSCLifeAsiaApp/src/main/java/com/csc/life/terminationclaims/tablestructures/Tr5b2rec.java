package com.csc.life.terminationclaims.tablestructures;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: vdivisala
 * @version
 * Creation Date: 20/02/2017 03:16:22
 * Description:
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr5b2rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr5b2Rec = new FixedLengthStringData(7);
  	public FixedLengthStringData nlg = DD.keya.copy().isAPartOf(tr5b2Rec,0);
	public ZonedDecimalData nlgczag = new ZonedDecimalData(3, 0).isAPartOf(tr5b2Rec,1);
	public ZonedDecimalData maxnlgyrs = new ZonedDecimalData(3, 0).isAPartOf(tr5b2Rec,4);

	public void initialize() {
		COBOLFunctions.initialize(tr5b2Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr5b2Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}