package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5235
 * @version 1.0 generated on 30/08/09 06:38
 * @author Quipoz
 */
public class S5235ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(494);
	public FixedLengthStringData dataFields = new FixedLengthStringData(174).isAPartOf(dataArea, 0);
	public FixedLengthStringData advance = DD.advance.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData arrears = DD.arrears.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData capcont = DD.capcont.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,27);
	public ZonedDecimalData dthpercn = DD.dthpercn.copyToZonedDecimal().isAPartOf(dataFields,29);
	public ZonedDecimalData dthperco = DD.dthperco.copyToZonedDecimal().isAPartOf(dataFields,34);
	public FixedLengthStringData freqann = DD.freqann.copy().isAPartOf(dataFields,39);
	public ZonedDecimalData guarperd = DD.guarperd.copyToZonedDecimal().isAPartOf(dataFields,41);
	public ZonedDecimalData intanny = DD.intanny.copyToZonedDecimal().isAPartOf(dataFields,44);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,112);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData nomlife = DD.nomlife.copy().isAPartOf(dataFields,161);
	public FixedLengthStringData ppind = DD.ppind.copy().isAPartOf(dataFields,169);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,170);
	public FixedLengthStringData withoprop = DD.withoprop.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData withprop = DD.withprop.copy().isAPartOf(dataFields,173);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(80).isAPartOf(dataArea, 174);
	public FixedLengthStringData advanceErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData arrearsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData capcontErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData dthpercnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData dthpercoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData freqannErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData guarperdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData intannyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData nomlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ppindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData withopropErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData withpropErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(240).isAPartOf(dataArea, 254);
	public FixedLengthStringData[] advanceOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] arrearsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] capcontOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] dthpercnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] dthpercoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] freqannOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] guarperdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] intannyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] nomlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ppindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] withopropOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] withpropOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5235screenWritten = new LongData(0);
	public LongData S5235protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5235ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(advanceOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(arrearsOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(intannyOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(capcontOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(withpropOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(withopropOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ppindOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dthpercnOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dthpercoOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(guarperdOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(nomlifeOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqannOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {advance, arrears, intanny, capcont, withprop, withoprop, ppind, dthpercn, dthperco, guarperd, linsname, chdrnum, life, coverage, rider, lifcnum, nomlife, freqann, jlifcnum, jlinsname};
		screenOutFields = new BaseData[][] {advanceOut, arrearsOut, intannyOut, capcontOut, withpropOut, withopropOut, ppindOut, dthpercnOut, dthpercoOut, guarperdOut, linsnameOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, nomlifeOut, freqannOut, jlifcnumOut, jlinsnameOut};
		screenErrFields = new BaseData[] {advanceErr, arrearsErr, intannyErr, capcontErr, withpropErr, withopropErr, ppindErr, dthpercnErr, dthpercoErr, guarperdErr, linsnameErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, nomlifeErr, freqannErr, jlifcnumErr, jlinsnameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5235screen.class;
		protectRecord = S5235protect.class;
	}

}
