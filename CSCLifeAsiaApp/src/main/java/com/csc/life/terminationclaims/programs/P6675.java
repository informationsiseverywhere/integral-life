/*
 * File: P6675.java
 * Date: 30 August 2009 0:51:08
 * Author: Quipoz Limited
 * 
 * Class transformed from P6675.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.general.tablestructures.T5611rec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrtpmTableDAM; //IJS-58
import com.csc.life.terminationclaims.dataaccess.LifesurTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SvltsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.recordstructures.SurtaxRec;
import com.csc.life.terminationclaims.recordstructures.UnexpiredPrmrec;
import com.csc.life.terminationclaims.screens.S6675ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Tr691rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Full Surrender Enquiry
*
* This   transaction,  Full  Surrender, is  selected  from  the
* surrender sub-menu  S5245/P5245.
* The transaction allows enquiry on an entire plan or one or
* many policies. A whole plan surrender is not permitted if
* any of the policies have been broken out.
*
*Initialise
*----------
*
* Read the  Contract  header  (function  RETRV)  and  read  the
* relevant data necessary for obtaining the status description,
* the Owner,  the  Life-assured,  Joint-life, CCD, Paid-to-date
* and the Bill-to-date.
*
* RETRV the "Plan" or  the  policy  to  be  worked  on from the
* COVRSUR  I/O  module,  it was selected from program P5015 (or
* defaulted if only one  policy present). If the Plan suffix in
* the key "kept" is zero,  the whole Plan is to be surrendered,
* otherwise, a single policy is to be surrendered.
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*      - READR  the   life  details  using  LIFESUR  (for  this
*           contract  number,  joint life number '00').  Format
*           the name accordingly.
*
*      - READR the  joint-life  details using LIFESUR (for this
*           contract  number,  joint life number '01').  Format
*           the name accordingly.
*
* Obtain the  necessary  descriptions  by  calling 'DESCIO' and
* output the descriptions of the statuses to the screen.
*
* Format Name
*
*      Read the  client  details  record  and  use the relevant
*           copybook in order to format the required names.
*
* Build the Coverage/Rider review details
*
*      If the  entire  Plan is being surrendered, then read all
*           the  coverage/riders  for  each  policy  within the
*           Plan.  The  literal  "Policy  no."  and  the policy
*           number  field  are non-display for the surrender of
*           an entire Plan.
*
*      If only one policy  within  a Plan is being surrendered,
*           read only the revelant coverage/riders.
*
*      N.B. the currency is decided by the surrender subroutine
*           and passed back.
*
*      If all the returned  details  are  of  the same currency
*           throughout, then default  the currency field at the
*           bottom of the screen to the same currency.
*
*      For each coverage/rider attached to the policy
*
*           - Check that the component has a valid status for
*                surrendering by reading T5679. Invalid
*                status components will not call the surrender
*                calculation routine and will display a
*                blank line in the subfile. Valid status
*                components will further have their risk
*                cessation dates checked against the
*                effective date. Any components which have
*                risk cessation dates prior to the effective
*                date and which are still in force, will prevent
*                the transaction from continuing.
*
*           - call  the  surrender  calculation  subroutine  as
*                defined on  T5687,  this  method  is  used  to
*                access  T6598,  which contains the subroutines
*                necessary   for   the  surrender  calculation,
*                processing.
*
* Linkage area passed to the surrender calculation subroutine:-
*
*        - company
*        - contract header number
*        - suffix
*        - life number
*        - joint-life number
*        - coverage
*        - rider
*        - crtable
*        - language
*        - estimated value
*        - actual value
*        - currency
*        - element code
*        - description
*        - type code
*        - status
*
*      DOWHILE
*         surrender calculation subroutine status not = ENDP
*            - load the subfile record
*            - accumulate single currency if applicable
*            - call surrender calculation subroutine
*      ENDDO
*
*      Surrender claim subroutines  may  work at Plan or policy
*           level,  therefore   the   DOWHILE  process  may  be
*           processed for end of  COVR  for a Plan or a Policy,
*           determine the type of surrender being processed.
*
*      If the  policy  selected is part of a summary, then, the
*           amount  returned  must be divided by "n" ("n" being
*           the  number of policies in the plan). Note:- If the
*           policy  selected  is not only part of a summary but
*           also  the  first policy from the summary, then, the
*           value  returned  is calculated as:  n - (n-1), this
*           caters for any rounding problems that may occur.
*
* Policy Loans
*
*      this amount is the total value of loans held against
*           the contract. It is obtained by calling TOTLOAN
*           using this contract header number.
*           Once returned, this figure will be deducted from
*           the Surrender Value. The figure remaining will be
*           displayed in the Net of Surrender Value Debt field.
*
* Net of Surrender Value Debt
*
*      this is the value of loans outstanding after this
*           policy has been surrendered.
*
* Total Estimated value
*
*      this amount  is  the  total of the estimated values from
*           the  coverage/rider  subfile review. This amount is
*           the  sum  of the individual amounts returned by the
*           subroutine. (If all the amounts returned are in the
*           same currency).
*
* Total Actual value
*
*      this amount  is  the total of the actual values from the
*           coverage/rider  subfile review (i.e. the sum of the
*           individual amounts returned  by  the subroutine, if
*           all the amounts returned are  in the same currency)
*           together with the policy loans and manually entered
*           adjustments, initially zero.
*
* Letter Print Request Flag
*
*      Set this field to 'Y' if a letter is required to evidence
*           the enquiry.
*
*      Do not allow full surrenders when no premiums have been
*           paid.
*
*Validation
*----------
*
* If  KILL   was entered, then skip the remainder of the
* validation and exit from the program.
*
* If a letter print was requested, then create a LETC record contain-
* ing the  Letter  Type  and hold the Contract Number and Surrender
* Value on the record which will be used in the creation of a letter.
* Letter to be customised on site.
* Additionally create a SVLT letter detail record which will be
* picked up by the Query in the subsequent letter creation.
*
*Next Program
*------------
*
* For KILL or 'Enter', add 1 to the program pointer and exit.
*
*
*Modifications
*-------------
*****************************************************************
* </pre>
*/
public class P6675 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6675");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	protected ZonedDecimalData wsaaSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator wholePlan = new Validator(wsaaSwitch, "1");
	private Validator partPlan = new Validator(wsaaSwitch, "2");
	protected Validator summaryPartPlan = new Validator(wsaaSwitch, "3");

	protected ZonedDecimalData wsaaSwitch2 = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator firstTime = new Validator(wsaaSwitch2, "1");
	protected ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(0);
	private ZonedDecimalData wsaaPlanSuffStore = new ZonedDecimalData(4, 0).init(0);
	protected FixedLengthStringData wsaaStoredLife = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);

	protected ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator detailsSameCurrency = new Validator(wsaaCurrencySwitch, "0");
	private Validator detailsDifferent = new Validator(wsaaCurrencySwitch, "1");
	private Validator totalCurrDifferent = new Validator(wsaaCurrencySwitch, "2");
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).init(SPACES);
	protected PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaCurrcd = new FixedLengthStringData(3).init(SPACES);
	protected FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsaaTaxAmt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaFeeTax = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotalFee = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(31);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaLetokeys, 0);
	private ZonedDecimalData wsaaSvTotal = new ZonedDecimalData(18, 2).isAPartOf(wsaaLetokeys, 8).setPattern("Z999999999999999.99");
	protected ZonedDecimalData wsaaSvltSub = new ZonedDecimalData(2, 0).setUnsigned();

		/*01  WSAA-TRANSACTION-REC.                                        */
	protected FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(206);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 0);
	private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 10);
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData filler4 = new FixedLengthStringData(184).isAPartOf(wsaaTransactionRec, 22, FILLER).init(SPACES);
		/*                                                         <D96NUM>*/
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	protected FixedLengthStringData wsaaErrorIndicators = new FixedLengthStringData(100);
	protected String wsaaValidStatus = "";
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	protected FixedLengthStringData wsaaMaturityFlag = new FixedLengthStringData(1);
	protected FixedLengthStringData wsaaSumFlag = new FixedLengthStringData(1).init(SPACES);
	protected FixedLengthStringData wsaaSurrenderRecStore = new FixedLengthStringData(150);

	protected FixedLengthStringData wsaaT5611Item = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT5611Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5611Item, 0);
	protected FixedLengthStringData wsaaT5611Pstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 4);
	private FixedLengthStringData filler5 = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 6, FILLER).init(SPACES);
	protected String wsaaNoPrice = "";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	protected ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	protected CltsTableDAM cltsIO = new CltsTableDAM();
	protected CovrtpmTableDAM covrclmIO = new CovrtpmTableDAM(); //IJS-58
	protected CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected LifesurTableDAM lifesurIO = new LifesurTableDAM();
	protected PayrTableDAM payrIO = new PayrTableDAM();
	protected SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	protected SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	protected SvltsurTableDAM svltsurIO = new SvltsurTableDAM();
	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	private Letrqstrec letrqstrec = new Letrqstrec();
	protected Srcalcpy srcalcpy = new Srcalcpy();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Tr384rec tr384rec = new Tr384rec();
	protected T5687rec t5687rec = new T5687rec();
	protected T6598rec t6598rec = new T6598rec();
	private T5679rec t5679rec = new T5679rec();
	protected T5611rec t5611rec = new T5611rec();
	private Tr691rec tr691rec = new Tr691rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Totloanrec totloanrec = new Totloanrec();
	protected Txcalcrec txcalcrec = new Txcalcrec();
	protected Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6675ScreenVars sv = getLScreenVars();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	protected WsaaStoredLetterDetailInner wsaaStoredLetterDetailInner = new WsaaStoredLetterDetailInner();
	protected ExternalisedRules er = new ExternalisedRules();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End

	protected boolean CTENQ003Permission  = false;
	private String PD5E3="PD5E3";
	
	boolean susur013Permission  = false;
	private T5645rec t5645rec = new T5645rec();
	private T3695rec t3695rec = new T3695rec();
	private Itempf itempf = null;
	private Acblpf acblpf = null;
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private FixedLengthStringData wsaaSurrenderMethod = new FixedLengthStringData(4).init("SC14");
	
/**
 * Contains all possible labels used by goTo action.
 */
	public enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue1030, 
		exit1640, 
		exit1840, 
		exit2090, 
		updateErrorIndicators2270
	}

	public P6675() {
		super();
		screenVars = sv;
		new ScreenModel("S6675", AppVars.getInstance(), sv);
	}
	protected S6675ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6675ScreenVars.class);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case continue1030: 
					continue1030();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		CTENQ003Permission  = FeaConfg.isFeatureExist("2", "CTENQ003", appVars, "IT");
		/* Skip this section if returning from an optional selection,*/
		/* i.e. check the stack action flag equals '*'.*/
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaSwitch.set(ZERO);
		wsaaPlanSuffix.set(ZERO);
		wsaaCurrencySwitch.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		wsaaSwitch2.set(1);
		wsaaStoredLife.set(SPACES);
		wsaaStoredCoverage.set(SPACES);
		wsaaStoredRider.set(SPACES);
		wsaaStoredCurrency.set(SPACES);
		wsaaErrorIndicators.set(SPACES);
		wsaaMaturityFlag.set(SPACES);
		wsaaSumFlag.set(SPACES);
		wsaaCrtable.set(SPACES);
		wsaaNoPrice = "N";
		wsaaStoredLetterDetailInner.wsaaStoredLetterDetail.set(SPACES);
		/*  Initialise table numeric items in SVLT rec.                    */
		for (wsaaSvltSub.set(1); !(isGT(wsaaSvltSub, 10)); wsaaSvltSub.add(1)){
			svltsurIO.setEmv(wsaaSvltSub, 0);
			svltsurIO.setActvalue(wsaaSvltSub, 0);
			wsaaStoredLetterDetailInner.wsaaSvltEmv[wsaaSvltSub.toInt()].set(0);
			wsaaStoredLetterDetailInner.wsaaSvltActvalue[wsaaSvltSub.toInt()].set(0);
		}
		wsaaSvltSub.set(ZERO);
		wsaaTransactionRec.set(wsspcomn.tranid);
		/* retrieve the coverage in order to find out if a whole*/
		/* plan (plan suffix = 0 ) or and individual policy is being*/
		/* surrendered*/
		covrsurIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			syserrrec.statuz.set(covrsurIO.getStatuz());
			fatalError600();
		}
		chdrsurIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			syserrrec.statuz.set(chdrsurIO.getStatuz());
			fatalError600();
		}
		srcalcpy.ptdate.set(chdrsurIO.getPtdate());
		/* Read the PAYR file to obtain the Contract Billing frequency     */
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrsurIO.getChdrcoy());
		payrIO.setChdrnum(chdrsurIO.getChdrnum());
		payrIO.setPayrseqno(covrsurIO.getPayrseqno());
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* decide which part of the plan is being surrended*/
		/* POLSUM = 1 doesn't imply there is a summarised record.*/
		srcalcpy.planSuffix.set(covrsurIO.getPlanSuffix());
		if (isEQ(covrsurIO.getPlanSuffix(), ZERO)) {
			wsaaSwitch.set(1);
		}
		else {
			if (isGT(covrsurIO.getPlanSuffix(), chdrsurIO.getPolsum())
			|| isEQ(chdrsurIO.getPolsum(), 1)) {
				wsaaSwitch.set(2);
			}
			else {
				wsaaSwitch.set(3);
			}
		}
		surhclmIO.setPlanSuffix(covrsurIO.getPlanSuffix());
		/* MOVE CHDRSUR-BILLFREQ       TO SURC-BILLFREQ.                */
		srcalcpy.chdrCurr.set(chdrsurIO.getCntcurr());
		initialiseCpybuk();
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		/* Dummy subfile initalisation for prototype - replace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("S6675", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Dummy field initilisation for prototype version.*/
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		susur013Permission  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR013", appVars, "IT");
		if(!susur013Permission) {
			sv.unexpiredprmOut[varcom.nd.toInt()].set("Y");
			sv.suspenseamtOut[varcom.nd.toInt()].set("Y");
		}
		if(isNE(chdrsurIO.getCnttype(), "TEN") && isNE(chdrsurIO.getCnttype(), "TWL") 
				&& isNE(chdrsurIO.getCnttype(), "JEA") && isNE(chdrsurIO.getCnttype(), "LCE")) {
			susur013Permission = false;
		}
		sv.estimateTotalValue.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.policyloan.set(ZERO);
		sv.zrcshamt.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.clamant.set(ZERO);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(chdrsurIO.getOccdate());
		sv.chdrnum.set(chdrsurIO.getChdrnum());
		sv.cnttype.set(chdrsurIO.getCnttype());
		descIO.setDescitem(chdrsurIO.getCnttype());
		sv.effdate.set(wsspcomn.currfrom);
		descIO.setDesctabl(tablesInner.t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrsurIO.getCownnum());
		cltsIO.setClntnum(chdrsurIO.getCownnum());
		getClientDetails1200();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		/* MOVE CHDRSUR-BTDATE           TO S6675-BTDATE.               */
		/* MOVE CHDRSUR-PTDATE           TO S6675-PTDATE.               */
		sv.btdate.set(chdrsurIO.getBtdate());
		sv.ptdate.set(chdrsurIO.getPtdate());
		if (isNE(sv.ptdate, sv.btdate)) {
			sv.ptdateErr.set(errorsInner.g008);
			sv.btdateErr.set(errorsInner.g008);
		}
		if (isEQ(chdrsurIO.getPtdate(), chdrsurIO.getCcdate())) {
			sv.ptdateErr.set(errorsInner.t065);
		}
		/*    Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrsurIO.getStatcode());
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrsurIO.getPstatcode());
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/* read the life details and the joint life details if they exist*/
		/* and format the names*/
		lifesurIO.setDataArea(SPACES);
		lifesurIO.setChdrcoy(chdrsurIO.getChdrcoy());
		lifesurIO.setChdrnum(chdrsurIO.getChdrnum());
		lifesurIO.setLife(covrsurIO.getLife());
		lifesurIO.setJlife("00");
		lifesurIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, lifesurIO);
		if (isNE(lifesurIO.getStatuz(), varcom.oK)
		&& isNE(lifesurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifesurIO.getParams());
			syserrrec.statuz.set(lifesurIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsspcomn.company, lifesurIO.getChdrcoy())
		|| isNE(chdrsurIO.getChdrnum(), lifesurIO.getChdrnum())
		|| isNE(lifesurIO.getJlife(), "00")
		&& isNE(lifesurIO.getJlife(), "  ")
		|| isEQ(lifesurIO.getStatuz(), "ENDP")) {
			syserrrec.params.set(lifesurIO.getParams());
			syserrrec.statuz.set(lifesurIO.getStatuz());
			fatalError600();
		}
		/* Move the last record read to the screen and get the*/
		/* description.*/
		sv.lifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1200();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.linsnameErr.set(errorsInner.e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		/*    look for joint life.*/
		lifesurIO.setJlife("01");
		lifesurIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifesurIO);
		if ((isNE(lifesurIO.getStatuz(), varcom.oK))
		&& (isNE(lifesurIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(lifesurIO.getParams());
			syserrrec.statuz.set(lifesurIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifesurIO.getStatuz(), varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.continue1030);
		}
		sv.jlifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1200();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.jlinsnameErr.set(errorsInner.e304);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
	}

protected void initialiseCpybuk() {
	
}
protected void continue1030()
	{
		/*  Build the coverage and rider details*/
		/*  If the entire plan is being surrendered, then read all the*/
		/*  coverage/riders for each policy within the plan.*/
		/*  If only one policy within  a plan is being surrender read only*/
		/*  the relevent coverage/riders.*/
		if (isEQ(covrsurIO.getPlanSuffix(), ZERO)) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.planSuffix.set(covrsurIO.getPlanSuffix());
		}
		if (wholePlan.isTrue()) {
			wholePlan1300();
		}
		else {
			partPlan1700();
		}
		/* Write blank subfile if its empty. This is to initialise the     */
		/* the subfile area.                                               */
		if (isEQ(scrnparams.subfileRrn, 0)) {
			blankSubfile1900x();
		}
		sv.policyloan.set(ZERO);
		/*  Computation of tax amount to be imposed.                       */
		setTaxamtCustomerSpecific();
		/*  Get the value of any Loans held against this component.        */
		getLoanDetails1900();
		getPolicyDebt1990();
		if(susur013Permission) {		
			wsaaBatckey.set(wsspcomn.batchkey);
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl("T5645");
			itempf.setItemitem(wsaaProg.toString());
		    itempf.setItemseq("  ");
			itempf = itemDao.getItemRecordByItemkey(itempf);
			
			if (itempf == null) {
				syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5645").concat(wsaaProg.toString()));
				fatalError600();
			}
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			
		    itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl("T3695");
			itempf.setItemitem(t5645rec.sacstype01.toString().trim());
			itempf = itemDao.getItemRecordByItemkey(itempf);
			
			if (itempf==null) {
				syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T3695").concat(t5645rec.sacstype01.toString()));
				fatalError600();
			}
			t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				
			getContractSuspense013();
			getUnexpiredPremium();
		}
		if (detailsSameCurrency.isTrue()) {
			sv.estimateTotalValue.set(wsaaEstimateTot);
			/*         COMPUTE S6675-CLAMANT = WSAA-ACTUAL-TOT +               */
			/*            S6675-POLICYLOAN                                     */
			/*      COMPUTE S6675-CLAMANT = WSAA-ACTUAL-TOT -         <CAS1.*/
			/*         S6675-POLICYLOAN                               <CAS1.*/
			if(susur013Permission) {
				compute(sv.clamant, 2).set(add(add(sub(sub(add(sub(wsaaActualTot, sv.policyloan), sv.zrcshamt), sv.tdbtamt), sv.taxamt), sv.unexpiredprm), sv.suspenseamt));
			}else {
				compute(sv.clamant, 2).set(sub(sub(add(sub(wsaaActualTot, sv.policyloan), sv.zrcshamt), sv.tdbtamt), sv.taxamt));
			}
			/* In the case of SUM products with premium adjustments add the    */
			/* premium adjustments to the total value displayed on screen...   */
			if (isNE(wsaaSumFlag, SPACES)
			&& isNE(sv.otheradjst, ZERO)) {
				sv.clamant.add(sv.otheradjst);
			}
			/*         MOVE CHDRSUR-CNTCURR   TO S6675-CURRCD.                 */
			sv.currcd.set(chdrsurIO.getCntcurr());
		}
		else {
			wsaaStoredCurrency.set(SPACES);
		}
		/* COMPUTE S6675-NET-OF-SV-DEBT = S6675-POLICYLOAN -      <CAS1.*/
		/*                                WSAA-ACTUAL-TOT.        <CAS1.*/
		compute(sv.netOfSvDebt, 2).set(sub(sub(sv.policyloan, sv.zrcshamt), wsaaActualTot));
		
		customerSpecificnetofSvDebt();
		/*  If Surrender Value totalled from the detail records is         */
		/*  =< 0 then put out message to this effect and prevent further   */
		/*  processing                                                     */
		if (isLTE(wsaaActualTot, 0)
		&& isLTE(wsaaEstimateTot, 0) && !susur013Permission) {
			sv.clamantErr.set(errorsInner.h377);
		}
		else {
			if (isLT(sv.netOfSvDebt, 0)) {
				sv.netOfSvDebt.set(0);
			}
			else {
				if (isLT(sv.clamant, 0)) {
					sv.clamant.set(0);
				}
			}
		}
		/*  Store any errors that have been encountered.                   */
		wsaaErrorIndicators.set(sv.errorIndicators);
	}
protected void setTaxamtCustomerSpecific() {
		/*IVE-705 Surrender Tax Calc Started*/
		//getTaxAmount1880();
		//sv.taxamt.set(wsaaTaxAmt);
		/*ILIFE-2397 Start */
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("SURTAX") && er.isExternalized(sv.cnttype.toString(), covrsurIO.crtable.toString())))//ILIFE-4833
		{
			getTaxAmount1880();
			sv.taxamt.set(wsaaTaxAmt);
		}
		else
		{
			SurtaxRec surtaxRec = new SurtaxRec();	
			surtaxRec.cnttype.set(sv.cnttype);
			/* Ticket #ILIFE-3747 [Surrender Value Enquiry Not Working_S5245] */
			//surtaxRec.cntcurr.set(sv.cnstcur);
			if (isEQ(sv.currcd, SPACES)) {
				surtaxRec.cntcurr.set(chdrsurIO.getCntcurr());
			}
			else {
				surtaxRec.cntcurr.set(sv.currcd);
			}
			/* Ticket #ILIFE-3747 end */
			surtaxRec.effectiveDate.set(sv.effdate);
			surtaxRec.occDate.set(sv.effdate);
			surtaxRec.actualAmount.set(wsaaActualTot);						
			callProgram("SURTAX", surtaxRec.surtaxRec);			
			sv.taxamt.set(surtaxRec.taxAmount);
			sv.htype.set(surtaxRec.amountType);
		}
		/*ILIFE-2397 End */
		/*IVE-705 Surrender Tax Calc end*/
		
		checkCalcTax5100();
	}
private void getUnexpiredPremium() {
	if(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("UNEXPIREDPRM"))
	{
		UnexpiredPrmrec unexpiredPrmRec = new UnexpiredPrmrec();
		unexpiredPrmRec.cnttype.set(sv.cnttype);
		String isPrmCollected;
		if(isGT(payrIO.getPtdate(), sv.effdate) && isEQ(payrIO.getPtdate() , payrIO.getBtdate())) {
			isPrmCollected = "Y";
		}else {
			isPrmCollected = "N";
		}
		unexpiredPrmRec.prmCollectedFlag.set(isPrmCollected);
		
		ZonedDecimalData calcPremium = new ZonedDecimalData(17,2);
		if(isGT(payrIO.getEffdate(),sv.effdate)) {
			List<Payrpf> payrpflist = payrpfDAO.getPayrListByCoyAndNumAndSeq(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString(), payrIO.getPayrseqno().toInt(), "2");
			for(Payrpf payrpf1: payrpflist) {
				if(isLT(payrpf1.getEffdate(),sv.effdate)) {
					calcPremium.set(payrpf1.getSinstamt06());
					unexpiredPrmRec.frequency.set(payrpf1.getBillfreq());
				}
			}
		}else {
			calcPremium.set(payrIO.getSinstamt06());
			unexpiredPrmRec.frequency.set(payrIO.getBillfreq());
		}

		unexpiredPrmRec.calcPremium.set(calcPremium);
		unexpiredPrmRec.sccDate.set(sv.occdate);
		unexpiredPrmRec.sEffDate.set(sv.effdate);
		unexpiredPrmRec.sptDate.set(sv.ptdate);//ibplife-3582
		callProgram("UNEXPIREDPRM",unexpiredPrmRec.unexpiredPrmRec);
		sv.unexpiredprm.set(unexpiredPrmRec.unexpiredPrmOut);
	}
}
protected void getContractSuspense013()
{

	acblpf = acblpfDAO.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode01.toString(), chdrsurIO.getChdrnum().toString(), payrIO.getBillcurr().toString(), t5645rec.sacstype01.toString());
	
	if (acblpf == null) {
		sv.suspenseamt.set(ZERO);
	}
	else {
		if (isEQ(t3695rec.sign,"-")) {
			compute(sv.suspenseamt, 2).set(mult(acblpf.getSacscurbal(),-1));
		}
		else {
			sv.suspenseamt.set(acblpf.getSacscurbal());
		}
	}
}


protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void wholePlan1300()
	{
		read1310();
	}

protected void read1310()
	{
		/* Begin on the coverage/rider record*/
		covrclmIO.setDataArea(SPACES);
		covrclmIO.setChdrcoy(covrsurIO.getChdrcoy());
		covrclmIO.setChdrnum(covrsurIO.getChdrnum());
		//Start--IJS-58
		/*covrclmIO.setLife(covrsurIO.getLife());
		covrclmIO.setCoverage(covrsurIO.getCoverage());
		covrclmIO.setRider(covrsurIO.getRider());*/
		//End--IJS-58
		covrclmIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		covrclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(), varcom.oK)
		&& isNE(covrclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrclmIO.getParams());
			syserrrec.statuz.set(covrclmIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrclmIO.getChdrcoy(), covrsurIO.getChdrcoy())
		|| isNE(covrclmIO.getChdrnum(), covrsurIO.getChdrnum())
		//Start--IJS-58
		/*|| isNE(covrclmIO.getLife(), covrsurIO.getLife())
		|| isNE(covrclmIO.getCoverage(), covrsurIO.getCoverage())
		|| isNE(covrclmIO.getRider(), covrsurIO.getRider())*/
		//End--IJS-58
		|| isEQ(covrclmIO.getStatuz(), varcom.endp)) {
			covrclmIO.setStatuz(varcom.endp);
		}
		wsaaStoredCoverage.set(covrclmIO.getCoverage());
		wsaaStoredRider.set(covrclmIO.getRider());
		wsaaStoredLife.set(covrclmIO.getLife());
		wsaaStoredCurrency.set(covrclmIO.getPremCurrency());
		while ( !(isEQ(covrclmIO.getStatuz(), varcom.endp))) {
			processComponents1350();
		}
		
	}

protected void processComponents1350()
	{
		read1351();
	}

protected void read1351()
	{
		/* Read table T5687 to find the surrender calculation subroutine*/
		wsaaCrtable.set(covrclmIO.getCrtable());
		obtainSurrenderCalc1400();
		/* add fields to subfile*/
		sv.coverage.set(covrclmIO.getCoverage());
		sv.hcover.set(covrclmIO.getCoverage());
		sv.hlife.set(covrclmIO.getLife());
		if (isEQ(covrclmIO.getRider(), "00")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(covrclmIO.getRider());
		}
		if (isNE(covrclmIO.getRider(), "00")
		&& isNE(covrclmIO.getRider(), "  ")) {
			sv.coverage.set(SPACES);
		}
		sv.cnstcur.set(covrclmIO.getPremCurrency());
		sv.hcnstcur.set(covrclmIO.getPremCurrency());
		srcalcpy.currcode.set(covrclmIO.getPremCurrency());
		validateStatusesWp1650();
		if (isEQ(wsaaValidStatus, "Y")
		&& isLT(covrclmIO.getRiskCessDate(), sv.effdate)) {
			wsaaMaturityFlag.set("Y");
			if (isEQ(covrclmIO.getRider(), "00")) {
				sv.chdrnumErr.set(errorsInner.t062);
			}
			else {
				sv.chdrnumErr.set(errorsInner.t062);
			}
		}
		/* check the reinsurance file at a later date and move*/
		/* 'y' to this field if they are present*/
		/* This Check never actually took place, but does now.             */
		/*  MOVE SPACES                    TO S6675-RIIND.               */
		/*                                                       <R96REA>*/
		/*  MOVE COVRCLM-CHDRCOY        TO WSAA-RACD-CHDRCOY.    <R96REA>*/
		/*  MOVE COVRCLM-CHDRNUM        TO WSAA-RACD-CHDRNUM.    <R96REA>*/
		/*  MOVE COVRCLM-LIFE           TO WSAA-RACD-LIFE.       <R96REA>*/
		/*  MOVE COVRCLM-COVERAGE       TO WSAA-RACD-COVERAGE.   <R96REA>*/
		/*  MOVE COVRCLM-RIDER          TO WSAA-RACD-RIDER.      <R96REA>*/
		/*                                                       <R96REA>*/
		/*  PERFORM 1980-READ-RACD.                              <R96REA>*/
		/*                                                       <R96REA>*/
		/*  IF RACDMJA-STATUZ            = O-K                   <R96REA>*/
		/*      MOVE 'Y'                TO S6675-RIIND           <R96REA>*/
		/*  END-IF.                                              <R96REA>*/
		srcalcpy.endf.set(SPACES);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrclmIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrclmIO.getChdrnum());
		srcalcpy.lifeLife.set(covrclmIO.getLife());
		srcalcpy.lifeJlife.set(covrclmIO.getJlife());
		srcalcpy.covrCoverage.set(covrclmIO.getCoverage());
		srcalcpy.covrRider.set(covrclmIO.getRider());
		srcalcpy.crtable.set(covrclmIO.getCrtable());
		srcalcpy.crrcd.set(covrclmIO.getCrrcd());
		srcalcpy.convUnits.set(covrclmIO.getConvertInitialUnits());
		srcalcpy.pstatcode.set(covrclmIO.getPstatcode());
		srcalcpy.status.set(SPACES);
		srcalcpy.polsum.set(chdrsurIO.getPolsum());
		srcalcpy.language.set(wsspcomn.language);
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(payrIO.getBillfreq());
		}
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			callSurMethodWhole1600();
		}
		
		while ( !((isNE(wsaaStoredRider, covrclmIO.getRider()))
		|| (isNE(wsaaStoredCoverage, covrclmIO.getCoverage()))
		|| (isNE(wsaaStoredLife, covrclmIO.getLife()))
		|| isEQ(covrclmIO.getStatuz(), varcom.endp))) {
			findNextComponent1550();
		}
		
		wsaaStoredLife.set(covrclmIO.getLife());
		wsaaStoredCoverage.set(covrclmIO.getCoverage());
		wsaaStoredRider.set(covrclmIO.getRider());
		wsaaStoredCurrency.set(covrclmIO.getPremCurrency());
	}

protected void obtainSurrenderCalc1400()
	{
		read1410();
	}

protected void read1410()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(chdrsurIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), wsaaCrtable)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*     MOVE F294                   TO SCRN-ERROR-CODE            */
			itdmIO.setItemitem(wsaaCrtable);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.f294);
			fatalError600();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t6598);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
	}

protected void findNextComponent1550()
	{
		/*READ*/
		/* Read the next coverage/rider record.*/
		covrclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(), varcom.oK)
		&& isNE(covrclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrclmIO.getParams());
			syserrrec.statuz.set(covrclmIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrclmIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(covrclmIO.getChdrnum(), chdrsurIO.getChdrnum())) {
			/* OR  COVRCLM-LIFE            NOT = WSAA-LIFE                  */
			covrclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void callSurMethodWhole1600()
	{
		try {
			read1610();
			addToSubfile1630();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void read1610()
	{
		srcalcpy.effdate.set(sv.effdate);
		srcalcpy.type.set("F");
		/* IF COVRCLM-INSTPREM         > ZERO                           */
		srcalcpy.singp.set(covrclmIO.getInstprem());
		/* ELSE                                                         */
		/*    MOVE COVRCLM-SINGP       TO SURC-SINGP.                   */
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		/* if no surrender method found print zeros as surrender value*/
		if (isEQ(t6598rec.calcprog, SPACES)
		|| isEQ(wsaaValidStatus, "N")) {
			sv.estMatValue.set(ZERO);
			sv.hemv.set(ZERO);
			sv.actvalue.set(ZERO);
			sv.hactval.set(ZERO);
			sv.fieldType.set(SPACES);
			sv.shortds.set(SPACES);
			sv.fund.set(SPACES);
			sv.cnstcur.set(SPACES);
			srcalcpy.status.set(varcom.endp);
			/*     GO TO 1630-ADD-TO-SUBFILE.                               */
			goTo(GotoLabel.exit1640);
		}
		/*IVE-797 RUL Product - Full Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		/*ILIFE-7548 start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())&& er.isExternalized(chdrsurIO.getCnttype().toString(), srcalcpy.crtable.toString()))) 
		{
			srcalcpy.surrCalcMeth.set(t5687rec.svMethod);
			srcalcpy.cnttype.set(chdrsurIO.getCnttype());
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
			callProgramX(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		/*ILIFE-7548 end*/
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			
			
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrsurIO);//VPMS call
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.endf.set("Y");
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
			/* Ticket #ILIFE-3747 [Surrender Value Enquiry Not Working_S5245] */
			if(vpxsurcrec.statuz.equals(varcom.endp))
				srcalcpy.status.set(varcom.endp);
			/* Ticket #ILIFE-3747 end */
		}
		
		/*IVE-797 RUL Product - Full Surrender Calculation end*/
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.statuz.set(srcalcpy.status);
			syserrrec.params.set(srcalcpy.surrenderRec);
			fatalError600();
		}
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)
		&& isNE(srcalcpy.status, "NOPR")) {
			syserrrec.statuz.set(srcalcpy.status);
			syserrrec.params.set(srcalcpy.surrenderRec);
			fatalError600();
		}
		if (isEQ(srcalcpy.status, "NOPR")) {
			wsaaNoPrice = "Y";
			sv.effdateErr.set(errorsInner.g094);
		}
		if (isEQ(srcalcpy.status, varcom.oK)
		|| isEQ(srcalcpy.status, varcom.endp)) {
			if (isEQ(srcalcpy.currcode, SPACES)) {
				srcalcpy.currcode.set(chdrsurIO.getCntcurr());
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			a000CallRounding();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			a000CallRounding();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		if (isEQ(srcalcpy.type, "J")) {
			compute(srcalcpy.actualVal, 2).set(mult(srcalcpy.actualVal, -1));
		}
		/* Check the amount returned for being negative. In the case of    */
		/* SUM products this is possible and so set these values to zero.  */
		/* Note SUM products do not have to have the same PT & BT dates.   */
		checkT56112700();
		if (isNE(wsaaSumFlag, SPACES)) {
			if (isNE(sv.ptdate, sv.btdate)) {
				sv.ptdateErr.set(SPACES);
				sv.btdateErr.set(SPACES);
			}
			if (isLT(srcalcpy.actualVal, ZERO)) {
				srcalcpy.actualVal.set(ZERO);
			}
		}
		/*    IF SURC-ENDF  = 'Y'                                          */
		/*       GO TO 1640-EXIT.                                          */
		if (isEQ(srcalcpy.estimatedVal, ZERO)
		&& isEQ(srcalcpy.actualVal, ZERO)) {
			goTo(GotoLabel.exit1640);
		}
		if (isEQ(srcalcpy.currcode, SPACES)) {
			sv.cnstcur.set(chdrsurIO.getCntcurr());
			sv.hcnstcur.set(chdrsurIO.getCntcurr());
		}
		else {
			sv.cnstcur.set(srcalcpy.currcode);
			sv.hcnstcur.set(srcalcpy.currcode);
		}
		sv.hcrtable.set(covrclmIO.getCrtable());
		sv.htype.set(srcalcpy.type);
		sv.fieldType.set(srcalcpy.type);
		sv.estMatValue.set(srcalcpy.estimatedVal);
		sv.hemv.set(srcalcpy.estimatedVal);
		sv.actvalue.set(srcalcpy.actualVal);
		sv.hactval.set(srcalcpy.actualVal);
		sv.fund.set(srcalcpy.fund);
		sv.shortds.set(srcalcpy.description);
		sv.effdate.set(srcalcpy.effdate);
		/*  IF S6675-SHORTDS         NOT = COVRCLM-CRTABLE       <R96REA>*/
		/*      MOVE SPACES             TO S6675-RIIND           <R96REA>*/
		/*  END-IF.                                              <R96REA>*/
		if (firstTime.isTrue()) {
			wsaaSwitch2.set(0);
			wsaaStoredCurrency.set(sv.cnstcur);
			wsaaEstimateTot.add(srcalcpy.estimatedVal);
			wsaaActualTot.add(srcalcpy.actualVal);
		}
		else {
			/*    IF SURC-CURRCODE            = WSAA-STORED-CURRENCY           */
			if (isEQ(sv.cnstcur, wsaaStoredCurrency)) {
				wsaaEstimateTot.add(srcalcpy.estimatedVal);
				wsaaActualTot.add(srcalcpy.actualVal);
			}
			else {
				wsaaEstimateTot.set(ZERO);
				wsaaActualTot.set(ZERO);
				wsaaCurrencySwitch.set(1);
			}
		}
		/* Check the effective date of the surrender against the paid to   */
		/* date of the contract. If there are to be premiums paid or       */
		/* refunded then call the appropriate subroutine held on T5611.    */
		/* Note that if the item does not exist then the coverage does not */
		/* use the SUM calculation package & is hence not applicable....   */
		if (isNE(srcalcpy.effdate, srcalcpy.ptdate)
		&& isNE(wsaaSumFlag, SPACES)
		&& isNE(t5611rec.calcprog, SPACES)) {
			checkPremadj2600();
		}
		if (isEQ(srcalcpy.type, "P") && isEQ(t5687rec.svMethod, wsaaSurrenderMethod)) {
			sv.hcover.set(SPACES);
			sv.hcrtable.set(SPACES);
			sv.coverage.set(SPACES);
			sv.hlife.set(SPACES);
			sv.rider.set(SPACES);
		}
	}

	/**
	* <pre>
	*  add record to subfile
	* </pre>
	*/
protected void addToSubfile1630()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S6675", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*  Add subfile record infromation to SVLT file.                   */
		addToSvlt5000();
	}

protected void validateStatusesWp1650()
	{
		start1650();
	}

	/**
	* <pre>
	*  Read T5679 to establish whether this component has a           
	*  valid status for this transaction.                             
	* </pre>
	*/
protected void start1650()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			riskStatusCheckWp1660();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				premStatusCheckWp1670();
			}
		}
	}

protected void riskStatusCheckWp1660()
	{
		/*START*/
		if (isEQ(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrclmIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrclmIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void premStatusCheckWp1670()
	{
		/*START*/
		if (isEQ(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrclmIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrclmIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void partPlan1700()
	{
		partPlan1710();
	}

protected void partPlan1710()
	{
		/* Begin on the coverage/rider record*/
		/*    MOVE 'RETRV'                TO COVRSUR-FUNCTION.             */
		covrsurIO.setFunction(varcom.begn);
		/*  Store the actual plan suffix for this transaction.             */
		wsaaPlanSuffStore.set(covrsurIO.getPlanSuffix());
		if (summaryPartPlan.isTrue()) {
			covrsurIO.setPlanSuffix(0);
		}
		wsaaStoredLife.set(covrsurIO.getLife());
		wsaaStoredCoverage.set(covrsurIO.getCoverage());
		wsaaStoredRider.set(covrsurIO.getRider());
		wsaaPlanSuffix.set(covrsurIO.getPlanSuffix());
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			syserrrec.statuz.set(covrsurIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrsurIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(covrsurIO.getChdrnum(), chdrsurIO.getChdrnum())
		|| isNE(covrsurIO.getPlanSuffix(), wsaaPlanSuffix)
		|| isNE(covrclmIO.getLife(), wsaaStoredLife)
		|| isNE(covrclmIO.getCoverage(), wsaaStoredCoverage)
		|| isNE(covrclmIO.getRider(), wsaaStoredRider)
		|| isEQ(covrclmIO.getStatuz(), varcom.endp)) {
			covrclmIO.setStatuz(varcom.endp);
		}
		/*    MOVE COVRSUR-COVERAGE          TO WSAA-STORED-COVERAGE.      */
		/*    MOVE COVRSUR-RIDER             TO WSAA-STORED-RIDER.         */
		wsaaStoredCurrency.set(covrsurIO.getPremCurrency());
		/*    MOVE COVRSUR-PLAN-SUFFIX       TO WSAA-PLAN-SUFFIX.          */
		while ( !(isEQ(covrsurIO.getStatuz(), varcom.endp))) {
			processPartComponents1750();
		}
		
		wsaaPlanSuffix.set(wsaaPlanSuffStore);
	}

protected void processPartComponents1750()
	{
		read1751();
	}

protected void read1751()
	{
		/* Read table T5687 to find the surrender calculation subroutine*/
		wsaaCrtable.set(covrsurIO.getCrtable());
		obtainSurrenderCalc1400();
		/* add fields to subfile*/
		sv.coverage.set(covrsurIO.getCoverage());
		sv.hcover.set(covrsurIO.getCoverage());
		if (isEQ(covrsurIO.getRider(), "00")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(covrsurIO.getRider());
		}
		if (isNE(covrsurIO.getRider(), "00")
		&& isNE(covrsurIO.getRider(), "  ")) {
			sv.coverage.set(SPACES);
		}
		sv.cnstcur.set(covrsurIO.getPremCurrency());
		sv.hcnstcur.set(covrsurIO.getPremCurrency());
		srcalcpy.currcode.set(covrsurIO.getPremCurrency());
		validateStatusesPp1950();
		if (isEQ(wsaaValidStatus, "Y")
		&& isLT(covrsurIO.getRiskCessDate(), sv.effdate)) {
			wsaaMaturityFlag.set("Y");
			if (isEQ(covrclmIO.getRider(), "00")) {
				sv.chdrnumErr.set(errorsInner.t062);
			}
			else {
				sv.chdrnumErr.set(errorsInner.t062);
			}
		}
		/* find description for crtable.*/
		/* check the reinsurance file at a later date and move*/
		/* 'y' to this field if they are present*/
		/*   MOVE SPACES                 TO S6675-RIIND.                  */
		/*                                                        <R96REA>*/
		/*   MOVE COVRSUR-CHDRCOY        TO WSAA-RACD-CHDRCOY.    <R96REA>*/
		/*   MOVE COVRSUR-CHDRNUM        TO WSAA-RACD-CHDRNUM.    <R96REA>*/
		/*   MOVE COVRSUR-LIFE           TO WSAA-RACD-LIFE.       <R96REA>*/
		/*   MOVE COVRSUR-COVERAGE       TO WSAA-RACD-COVERAGE.   <R96REA>*/
		/*   MOVE COVRSUR-RIDER          TO WSAA-RACD-RIDER.      <R96REA>*/
		/*                                                        <R96REA>*/
		/*   PERFORM 1980-READ-RACD.                              <R96REA>*/
		/*                                                        <R96REA>*/
		/*   IF RACDMJA-STATUZ            = O-K                   <R96REA>*/
		/*       MOVE 'Y'                TO S6675-RIIND           <R96REA>*/
		/*   END-IF.                                              <R96REA>*/
		/*                                                        <R96REA>*/
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrsurIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrsurIO.getChdrnum());
		srcalcpy.lifeLife.set(covrsurIO.getLife());
		srcalcpy.lifeJlife.set(covrsurIO.getJlife());
		srcalcpy.covrCoverage.set(covrsurIO.getCoverage());
		srcalcpy.covrRider.set(covrsurIO.getRider());
		srcalcpy.crtable.set(covrsurIO.getCrtable());
		srcalcpy.crrcd.set(covrsurIO.getCrrcd());
		srcalcpy.convUnits.set(covrsurIO.getConvertInitialUnits());
		srcalcpy.status.set(SPACES);
		srcalcpy.pstatcode.set(covrsurIO.getPstatcode());
		srcalcpy.polsum.set(chdrsurIO.getPolsum());
		srcalcpy.language.set(wsspcomn.language);
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(payrIO.getBillfreq());
		}
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			callSurMethodPart1800();
		}
		
		/* Read the next coverage/rider record.*/
		covrsurIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)
		&& isNE(covrsurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			syserrrec.statuz.set(covrsurIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrsurIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(covrsurIO.getChdrnum(), chdrsurIO.getChdrnum())
		|| isNE(covrsurIO.getPlanSuffix(), wsaaPlanSuffix)) {
			covrsurIO.setStatuz(varcom.endp);
		}
	}

protected void callSurMethodPart1800()
	{
		try {
			read1810();
			addToSubfile1830();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void read1810()
	{
		srcalcpy.effdate.set(sv.effdate);
		srcalcpy.type.set("F");
		/* IF COVRSUR-INSTPREM          > ZERO                          */
		srcalcpy.singp.set(covrsurIO.getInstprem());
		/* ELSE                                                         */
		/*    MOVE COVRSUR-SINGP        TO SURC-SINGP.                  */
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		/* if no surrender method found print zeros as surrender value*/
		if (isEQ(t6598rec.calcprog, SPACES)
		|| isEQ(wsaaValidStatus, "N")) {
			sv.estMatValue.set(ZERO);
			sv.hemv.set(ZERO);
			sv.actvalue.set(ZERO);
			sv.hactval.set(ZERO);
			sv.fieldType.set(SPACES);
			sv.shortds.set(SPACES);
			sv.fund.set(SPACES);
			sv.cnstcur.set(SPACES);
			srcalcpy.status.set(varcom.endp);
			goTo(GotoLabel.exit1840);
		}
		/*     GO TO 1830-ADD-TO-SUBFILE.                               */
		/*IVE-797 RUL Product - Full Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		/*ILIFE-7548 start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())&& er.isExternalized(chdrsurIO.getCnttype().toString(), srcalcpy.crtable.toString()))) 
		{
			srcalcpy.surrCalcMeth.set(t5687rec.svMethod);
			srcalcpy.cnttype.set(chdrsurIO.getCnttype());
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
			callProgramX(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		/*ILIFE-7548 end*/
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrsurIO);//VPMS call
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.endf.set("Y");
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
			/* Ticket #ILIFE-3747 [Surrender Value Enquiry Not Working_S5245] */
			if(vpxsurcrec.statuz.equals(varcom.endp))
				srcalcpy.status.set(varcom.endp);
			/* Ticket #ILIFE-3747 end */
		}
		
		/*IVE-797 RUL Product - Full Surrender Calculation end*/
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.statuz.set(srcalcpy.status);
			syserrrec.params.set(srcalcpy.surrenderRec);
			fatalError600();
		}
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)) {
			syserrrec.statuz.set(srcalcpy.status);
			syserrrec.params.set(srcalcpy.surrenderRec);
			fatalError600();
		}
		if (isEQ(srcalcpy.status, varcom.oK)
		|| isEQ(srcalcpy.status, varcom.endp)) {
			if (isEQ(srcalcpy.currcode, SPACES)) {
				srcalcpy.currcode.set(chdrsurIO.getCntcurr());
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			a000CallRounding();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			zrdecplrec.currency.set(srcalcpy.currcode);
			a000CallRounding();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		if (isEQ(srcalcpy.type, "J")) {
			compute(srcalcpy.actualVal, 2).set(mult(srcalcpy.actualVal, -1));
		}
		/* Check the amount returned for being negative. In the case of    */
		/* SUM products this is possible and so set these values to zero.  */
		/* Note SUM products do not have to have the same PT & BT dates.   */
		checkT56112700();
		if (isNE(wsaaSumFlag, SPACES)) {
			if (isNE(sv.ptdate, sv.btdate)) {
				sv.ptdateErr.set(SPACES);
				sv.btdateErr.set(SPACES);
			}
			if (isLT(srcalcpy.actualVal, ZERO)) {
				srcalcpy.actualVal.set(ZERO);
			}
		}
		/*    IF SURC-ENDF  = 'Y'                                          */
		/*       GO TO 1840-EXIT.                                          */
		if (isEQ(srcalcpy.estimatedVal, ZERO)
		&& isEQ(srcalcpy.actualVal, ZERO)) {
			goTo(GotoLabel.exit1840);
		}
		sv.hcrtable.set(covrsurIO.getCrtable());
		if (isEQ(srcalcpy.currcode, SPACES)) {
			sv.cnstcur.set(chdrsurIO.getCntcurr());
			sv.hcnstcur.set(chdrsurIO.getCntcurr());
		}
		else {
			sv.cnstcur.set(srcalcpy.currcode);
			sv.hcnstcur.set(srcalcpy.currcode);
		}
		sv.htype.set(srcalcpy.type);
		sv.fieldType.set(srcalcpy.type);
		sv.estMatValue.set(srcalcpy.estimatedVal);
		sv.hemv.set(srcalcpy.estimatedVal);
		sv.actvalue.set(srcalcpy.actualVal);
		sv.hactval.set(srcalcpy.actualVal);
		/* 'IF SURC-ACTUAL-VAL' should also be part of the nested 'IF*/
		/*  SUMMARY-PART-PLAN'.*/
		if (summaryPartPlan.isTrue()) {
			if (isNE(srcalcpy.estimatedVal, ZERO)) {
				compute(sv.estMatValue, 2).set(div(srcalcpy.estimatedVal, chdrsurIO.getPolsum()));
				compute(sv.hemv, 2).set(div(srcalcpy.estimatedVal, chdrsurIO.getPolsum()));
			}
		}
		zrdecplrec.amountIn.set(sv.estMatValue);
		zrdecplrec.currency.set(srcalcpy.currcode);
		a000CallRounding();
		sv.estMatValue.set(zrdecplrec.amountOut);
		sv.hemv.set(zrdecplrec.amountOut);
		if (summaryPartPlan.isTrue()) {
			if (isNE(srcalcpy.actualVal, ZERO)) {
				compute(sv.actvalue, 2).set(div(srcalcpy.actualVal, chdrsurIO.getPolsum()));
				compute(sv.hactval, 2).set(div(srcalcpy.actualVal, chdrsurIO.getPolsum()));
			}
		}
		zrdecplrec.amountIn.set(sv.actvalue);
		zrdecplrec.currency.set(srcalcpy.currcode);
		a000CallRounding();
		sv.actvalue.set(zrdecplrec.amountOut);
		sv.fund.set(srcalcpy.fund);
		sv.shortds.set(srcalcpy.description);
		if (firstTime.isTrue()) {
			wsaaSwitch2.set(0);
			wsaaStoredCurrency.set(sv.cnstcur);
			wsaaEstimateTot.add(sv.estMatValue);
			wsaaActualTot.add(sv.actvalue);
		}
		else {
			/*    IF SURC-CURRCODE            = WSAA-STORED-CURRENCY           */
			if (isEQ(sv.cnstcur, wsaaStoredCurrency)) {
				wsaaEstimateTot.add(sv.estMatValue);
				wsaaActualTot.add(sv.actvalue);
			}
			else {
				wsaaEstimateTot.set(ZERO);
				wsaaActualTot.set(ZERO);
				wsaaCurrencySwitch.set(1);
			}
		}
		/*   IF S6675-SHORTDS         NOT = COVRSUR-CRTABLE       <R96REA>*/
		/*       MOVE SPACES             TO S6675-RIIND           <R96REA>*/
		/*   END-IF.                                              <R96REA>*/
		/* Check the effective date of the surrender against the paid to   */
		/* date of the contract. If there are to be premiums paid or       */
		/* refunded then call the appropriate subroutine held on T5611.    */
		/* Note that if the item does not exist then the coverage does not */
		/* use the SUM calculation package & is hence not applicable....   */
		if (isNE(srcalcpy.effdate, srcalcpy.ptdate)
		&& isNE(wsaaSumFlag, SPACES)
		&& isNE(t5611rec.calcprog, SPACES)) {
			checkPremadj2600();
		}
	}

	/**
	* <pre>
	*  add record to subfile
	* </pre>
	*/
protected void addToSubfile1830()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S6675", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*  Add subfile record infromation to SVLT file.                   */
		addToSvlt5000();
	}

protected void getTaxAmount1880()
	{
		getTaxAmountPara1880();
	}

protected void getTaxAmountPara1880()
	{
		wsaaTaxAmt.set(0);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr691);
		/* MOVE CHDRSUR-CNTTYPE        TO ITEM-ITEMITEM.        <LA2924>*/
		if (isEQ(sv.currcd, SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrsurIO.getCnttype());
			stringVariable1.addExpression(chdrsurIO.getCntcurr());
			stringVariable1.setStringInto(itemIO.getItemitem());
		}
		else {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(chdrsurIO.getCnttype());
			stringVariable2.addExpression(sv.currcd);
			stringVariable2.setStringInto(itemIO.getItemitem());
		}
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setParams(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr691);
			/*    MOVE '***'               TO ITEM-ITEMITEM         <LA2924>*/
			if (isEQ(sv.currcd, SPACES)) {
				StringUtil stringVariable3 = new StringUtil();
				stringVariable3.addExpression("***");
				stringVariable3.addExpression(chdrsurIO.getCntcurr());
				stringVariable3.setStringInto(itemIO.getItemitem());
			}
			else {
				StringUtil stringVariable4 = new StringUtil();
				stringVariable4.addExpression("***");
				stringVariable4.addExpression(sv.currcd);
				stringVariable4.setStringInto(itemIO.getItemitem());
			}
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				return ;
			}
		}
		tr691rec.tr691Rec.set(itemIO.getGenarea());
		datcon3rec.intDate2.set(sv.effdate);
		datcon3rec.intDate1.set(sv.occdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isGTE(datcon3rec.freqFactor, tr691rec.tyearno)) {
			return ;
		}
		if (isNE(tr691rec.pcnt, 0)) {
			//MIBT-111
//			compute(wsaaTaxAmt, 3).setRounded(mult(wsaaActualTot, (div(tr691rec.pcnt, 100))));
			compute(wsaaTaxAmt, 3).setRounded(div(mult(wsaaActualTot,tr691rec.pcnt), 100));
		}
		if (isNE(tr691rec.flatrate, 0)) {
			wsaaTaxAmt.set(tr691rec.flatrate);
		}
		/*MIBT-111::wsaaTAxAmount is negative value.So that Tax deducted instead of Addition into Service tax  */
		if (isLT(wsaaTaxAmt, 0)) {
			compute(wsaaTaxAmt, 2).set(mult(wsaaTaxAmt, -1));
		}
		else {
			wsaaTaxAmt.set(wsaaTaxAmt);
		}		
		zrdecplrec.amountIn.set(wsaaTaxAmt);
		if (isEQ(sv.currcd, SPACES)) {
			zrdecplrec.currency.set(chdrsurIO.getCntcurr());
		}
		else {
			zrdecplrec.currency.set(sv.currcd);
		}
		a000CallRounding();
		wsaaTaxAmt.set(zrdecplrec.amountOut);
	}

protected void getLoanDetails1900()
	{
		start1900();
	}

	/**
	* <pre>
	*  Get the details of all loans currently held against this       
	*  Contract.                                                      
	*  Call TOTLOAN to get the current loan value.                    
	*  Note also that TOTLOAN always returns details in the CHDR      
	*  currency.                                                      
	* </pre>
	*/
protected void start1900()
	{
		/*  Read TOTLOAN to get value of loans for this contract.          */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrsurIO.getChdrcoy());
		totloanrec.chdrnum.set(chdrsurIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.function.set("LOAN");
		totloanrec.language.set(wsspcomn.language);
		/* CALL 'TOTLOAN' USING TOTL-TOTLOAN-REC.                       */
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal, totloanrec.interest));
		sv.policyloan.set(wsaaHeldCurrLoans);
		/* Calculate Cash Deposit accounts related to the Contract.     */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrsurIO.getChdrcoy());
		totloanrec.chdrnum.set(chdrsurIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(sv.effdate);
		totloanrec.function.set("CASH");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(sv.zrcshamt, 2).set(add(totloanrec.principal, totloanrec.interest));
		/* Change the sign to reflect the Client's context.             */
		if (isLT(sv.zrcshamt, 0)) {
			compute(sv.zrcshamt, 2).set(mult(sv.zrcshamt, (-1)));
		}
	}

protected void validateStatusesPp1950()
	{
		start1950();
	}

	/**
	* <pre>
	*  Read T5679 to establish whether this component has a           
	*  valid status for this transaction.                             
	* </pre>
	*/
protected void start1950()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			riskStatusCheckPp1960();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				premStatusCheckPp1970();
			}
		}
	}

protected void riskStatusCheckPp1960()
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrsurIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrsurIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void premStatusCheckPp1970()
	{
		/*START*/
		if (isEQ(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrsurIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrsurIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrsurIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*1980-READ-RACD SECTION.                                  <R96REA>
	**************************                                <R96REA>
	*                                                         <R96REA>
	*1971-REASSURANCE.                                        <R96REA>
	*                                                         <R96REA>
	*    MOVE SPACES                 TO RACDMJA-PARAMS.       <R96REA>
	*                                                         <R96REA>
	*    MOVE WSAA-RACD-CHDRCOY      TO RACDMJA-CHDRCOY.      <R96REA>
	*    MOVE WSAA-RACD-CHDRNUM      TO RACDMJA-CHDRNUM.      <R96REA>
	*    MOVE WSAA-RACD-LIFE         TO RACDMJA-LIFE.         <R96REA>
	*    MOVE WSAA-RACD-COVERAGE     TO RACDMJA-COVERAGE.     <R96REA>
	*    MOVE WSAA-RACD-RIDER        TO RACDMJA-RIDER.        <R96REA>
	*    MOVE ZEROES                 TO RACDMJA-PLAN-SUFFIX.  <R96REA>
	*    MOVE 99                     TO RACDMJA-SEQNO.        <R96REA>
	*    MOVE 'RACDMJAREC'           TO RACDMJA-FORMAT.               
	*    MOVE BEGN                   TO RACDMJA-FUNCTION.             
	*    CALL 'RACDMJAIO'            USING RACDMJA-PARAMS.            
	*    IF RACDMJA-STATUZ        NOT = O-K AND NOT = 'ENDP'          
	*       MOVE RACDMJA-PARAMS      TO SYSR-PARAMS                   
	*       PERFORM 600-FATAL-ERROR                                   
	*    END-IF.                                                      
	*                                                         <R96REA>
	*    IF RACDMJA-CHDRCOY       NOT = WSAA-RACD-CHDRCOY OR  <R96REA>
	*       RACDMJA-CHDRNUM       NOT = WSAA-RACD-CHDRNUM OR  <R96REA>
	*       RACDMJA-LIFE          NOT = WSAA-RACD-LIFE OR     <R96REA>
	*       RACDMJA-COVERAGE      NOT = WSAA-RACD-COVERAGE OR <R96REA>
	*       RACDMJA-RIDER         NOT = WSAA-RACD-RIDER OR    <R96REA>
	*       RACDMJA-STATUZ            = ENDP                  <R96REA>
	*         MOVE ENDP              TO RACDMJA-STATUZ        <R96REA>
	*    END-IF.                                              <R96REA>
	*                                                         <R96REA>
	*1989-EXIT.                                               <R96REA>
	*     EXIT.                                               <R96REA>
	* </pre>
	*/
protected void getPolicyDebt1990()
	{
		start1990();
		read1991();
	}

protected void start1990()
	{
		sv.tdbtamt.set(ZERO);
		tpoldbtIO.setRecKeyData(SPACES);
		tpoldbtIO.setChdrcoy(chdrsurIO.getChdrcoy());
		tpoldbtIO.setChdrnum(chdrsurIO.getChdrnum());
		tpoldbtIO.setTranno(ZERO);
		tpoldbtIO.setFormat(formatsInner.tpoldbtrec);
		tpoldbtIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		tpoldbtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		tpoldbtIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	}

protected void read1991()
	{
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
		&& isNE(tpoldbtIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(tpoldbtIO.getParams());
			syserrrec.statuz.set(tpoldbtIO.getStatuz());
			fatalError600();
		}
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
		|| isNE(tpoldbtIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(tpoldbtIO.getChdrnum(), chdrsurIO.getChdrnum())) {
			return ;
		}
		sv.tdbtamt.add(tpoldbtIO.getTdbtamt());
		tpoldbtIO.setFunction(varcom.nextr);
		read1991();
		return ;
	}

protected void blankSubfile1900x()
	{
		/*X-START*/
		sv.cnstcur.set(SPACES);
		sv.coverage.set(SPACES);
		sv.hcnstcur.set(SPACES);
		sv.hcover.set(SPACES);
		sv.hcrtable.set(SPACES);
		sv.hlife.set(SPACES);
		sv.htype.set(SPACES);
		sv.rider.set(SPACES);
		sv.shortds.set(SPACES);
		sv.fieldType.set(SPACES);
		sv.fund.set(SPACES);
		sv.actvalue.set(ZERO);
		sv.estMatValue.set(ZERO);
		sv.hactval.set(ZERO);
		sv.hemv.set(ZERO);
		scrnparams.function.set(varcom.sadd);
		processScreen("S6675", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*X-EXIT*/
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isGT(sv.clamant, 0)
		|| isGT(sv.estimateTotalValue, 0)) {
			sv.clamantOut[varcom.hi.toInt()].set("Y");
		}
		/*    PERFORM 5100-CHECK-CALC-TAX.                                 */
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			validateSelectionFields2070();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			exit2090();
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S6675IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6675-DATA-AREA                         */
		/*                         S6675-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		sv.errorIndicators.set(wsaaErrorIndicators);
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.currcd, SPACES)) {
			sv.currcd.set(chdrsurIO.getCntcurr());
		}
		/*    IF S6675-PTDATE NOT = S6675-BTDATE                      <004>*/
		/*        MOVE G008               TO S6675-PTDATE-ERR         <004>*/
		/*                                   S6675-BTDATE-ERR         <004>*/
		/*                                   S6675-BTDATE-ERR.        <004>*/
		/*        MOVE 'Y'                TO WSSP-EDTERROR.           <004>*/
		/*  If currency has changed, don't update stored currency          */
		/*  until after currency conversion.                               */
		/*  Additionally, subtract the policy loan amount rather than      */
		/*  add it to the total.                                           */
		if (isNE(sv.currcd, wsaaStoredCurrency)) {
			/*        MOVE S6675-CURRCD TO WSAA-STORED-CURRENCY                */
			wsaaCurrencySwitch.set(0);
			wsspcomn.edterror.set("Y");
			readjustCurrencies2100();
			wsaaStoredCurrency.set(sv.currcd);
			sv.estimateTotalValue.set(wsaaEstimateTot);
			/*     COMPUTE S6675-CLAMANT = WSAA-ACTUAL-TOT +                */
			/*            S6675-OTHERADJST +                             <CAS1.*/
			/*         S6675-OTHERADJST -                             <CAS1.*/
			/*             S6675-POLICYLOAN.                                */
			compute(sv.clamant, 2).set(sub(sub(sub(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), sv.tdbtamt), sv.taxamt), sv.policyloan));
		}
		/*       effective date must be entered - should be not less than*/
		/*       the ccd and not less than date of death.*/
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)) {
			sv.effdateErr.set(errorsInner.e186);
		}
		else {
			if (isLT(sv.effdate, chdrsurIO.getOccdate())) {
				sv.effdateErr.set(errorsInner.f616);
			}
		}
		/* COMPUTE S6675-CLAMANT = WSAA-ACTUAL-TOT +                    */
		/*            S6675-OTHERADJST +                             <CAS1.*/
		/*         S6675-OTHERADJST -                             <CAS1.*/
		/*         S6675-POLICYLOAN.                                    */
		if(susur013Permission)
		{
			compute(sv.clamant, 2).set(add(add(sub(sub(sub(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), sv.tdbtamt), sv.taxamt), sv.policyloan), sv.suspenseamt), sv.unexpiredprm));
		}else {
			compute(sv.clamant, 2).set(sub(sub(sub(add(add(wsaaActualTot, sv.zrcshamt), sv.otheradjst), sv.tdbtamt), sv.taxamt), sv.policyloan));
		}
		/*  Recalculate the Net of Surr Val Debt in the new Currency       */
		/* COMPUTE S6675-NET-OF-SV-DEBT = S6675-POLICYLOAN -      <CAS1.*/
		/*                                WSAA-ACTUAL-TOT -       <CAS1.*/
		/*                                S6675-OTHERADJST.       <CAS1.*/
		compute(sv.netOfSvDebt, 2).set(sub(sub(sub(sv.policyloan, sv.zrcshamt), sv.otheradjst), wsaaActualTot));
	
		customerSpecificnetofSvDebt();
		if (isLT(sv.netOfSvDebt, 0)) {
			sv.netOfSvDebt.set(0);
		}
		if (isLT(sv.clamant, 0)) {
			sv.clamant.set(0);
		}
		/* IF  S6675-LETTER-PRINT-FLAG NOT = 'Y'                   <002>*/
		/* AND                         NOT = 'y'                   <002>*/
		/* AND                         NOT = 'N'                   <002>*/
		/* AND                         NOT = 'n'                   <002>*/
		/* AND                         NOT = SPACES                <002>*/
		/*     MOVE G941                   TO S6675-LETPRTFLAG-ERR.<002>*/
		/*                                                         <002>*/
		/* IF  S6675-LETTER-PRINT-FLAG     = 'Y'                   <002>*/
		/* OR                              = 'y'                   <002>*/
		/*     PERFORM 2400-READ-T6634.                            <002>*/
		if (isNE(sv.letterPrintFlag, "Y")
		&& isNE(sv.letterPrintFlag, "N")
		&& isNE(sv.letterPrintFlag, SPACES)) {
			sv.letprtflagErr.set(errorsInner.g941);
		}
		if (isEQ(sv.letterPrintFlag, "Y")) {
			readT66342400();
		}
		sv.ptdateErr.set(SPACES);
		sv.btdateErr.set(SPACES);
		/* The WSSP-FLAG is set as 'M' in the sub menu. To make sure the sc*/
		/* is redisplayed with the calculated value, move CALC to the statu*/
		/* and change the WSSP-FLAG to 'X'.*/
		/*    IF S6675-ERROR-INDICATORS       NOT = SPACES                 */
		/*        MOVE 'Y'                    TO WSSP-EDTERROR             */
		/*        MOVE 'M'                    TO WSSP-FLAG                 */
		/*        GO TO 2090-EXIT.                                         */
		if (isEQ(wsaaNoPrice, "Y")) {
			sv.effdateErr.set(errorsInner.g094);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			if (isNE(sv.ptdate, sv.btdate)) {
				sv.ptdateErr.set(errorsInner.g008);
				sv.btdateErr.set(errorsInner.g008);
			}
			wsspcomn.edterror.set("Y");
			wsspcomn.flag.set("M");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsspcomn.flag, "M")) {
			scrnparams.statuz.set("CALC");
			wsspcomn.flag.set("X");
		}
	}

protected void validateSelectionFields2070()
	{
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaMaturityFlag, "Y")) {
			wsspcomn.edterror.set("Y");
		}
		/*CHECK-FOR-ERRORS*/
		/* If an error has been returned we want to make sure that the scre*/
		/* is redisplayed again rather than returning to the Sub menu, henc*/
		/* the use of WSSP-FLAG.*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.flag.set("M");
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.ptdate, sv.btdate)) {
			sv.ptdateErr.set(errorsInner.g008);
			sv.btdateErr.set(errorsInner.g008);
		}
	}

protected void exit2090()
	{
		if (isEQ(scrnparams.deviceInd, "*RMT")||(wsspcomn.secProgs.toString().contains(PD5E3) && CTENQ003Permission)) {
			wsspcomn.edterror.set(varcom.oK);
		}
	}

protected void readjustCurrencies2100()
	{
		g02150();
	}

protected void g02150()
	{
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6675", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaSvltSub.set(ZERO);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile2200();
		}
		
		getTaxAmount1880();
		sv.taxamt.set(wsaaTaxAmt);
		checkCalcTax5100();
		convertPolicyloan2400();
		if (isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readSubfile2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go2250();
				case updateErrorIndicators2270: 
					updateErrorIndicators2270();
					readNextRecord2280();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go2250()
	{
		/*convert the estimated value if it is non zero.*/
		if (isEQ(sv.currcd, sv.hcnstcur)
		|| isEQ(sv.currcd, SPACES)) {
			/*    IF S6675-CURRCD   = S6675-CNSTCUR*/
			sv.cnstcur.set(sv.hcnstcur);
			sv.estMatValue.set(sv.hemv);
			wsaaEstimateTot.add(sv.estMatValue);
			sv.actvalue.set(sv.hactval);
			wsaaActualTot.add(sv.actvalue);
			goTo(GotoLabel.updateErrorIndicators2270);
		}
		/*convert the estimated value if it is non zero.*/
		if (isNE(sv.hemv, ZERO)) {
			conlinkrec.amountIn.set(sv.hemv);
			conlinkrec.function.set("CVRT");
			callXcvrt2300();
			sv.estMatValue.set(conlinkrec.amountOut);
			wsaaEstimateTot.add(conlinkrec.amountOut);
		}
		/*convert the actual value if it is non zero.*/
		if (isNE(sv.hactval, ZERO)) {
			conlinkrec.amountIn.set(sv.hactval);
			if (isEQ(sv.fieldType, "C")) {
				conlinkrec.function.set("SURR");
			}
			else {
				conlinkrec.function.set("CVRT");
			}
			callXcvrt2300();
			sv.actvalue.set(conlinkrec.amountOut);
			wsaaActualTot.add(conlinkrec.amountOut);
		}
		sv.cnstcur.set(conlinkrec.currOut);
		/*  Need to update SVLT details with changed currency values.      */
		wsaaSvltSub.add(1);
		wsaaStoredLetterDetailInner.wsaaSvltCnstcur[wsaaSvltSub.toInt()].set(sv.cnstcur);
		wsaaStoredLetterDetailInner.wsaaSvltEmv[wsaaSvltSub.toInt()].set(sv.estMatValue);
		/* MOVE S6675-ACTVALUE TO WSAA-SVLT-ACTVALUE(WSAA-SVLT-SUB).<002*/
		if (isEQ(sv.fieldType, "C")) {
			compute(wsaaStoredLetterDetailInner.wsaaSvltActvalue[wsaaSvltSub.toInt()], 2).set(mult(-1, sv.actvalue));
		}
		else {
			wsaaStoredLetterDetailInner.wsaaSvltActvalue[wsaaSvltSub.toInt()].set(sv.actvalue);
		}
	}

protected void updateErrorIndicators2270()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6675", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextRecord2280()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S6675", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callXcvrt2300()
	{
		g02350();
	}

protected void g02350()
	{
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(sv.hcnstcur);
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrsurIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, "****")) {
			/*        MOVE CLNK-STATUZ        TO SCRN-ERROR-CODE               */
			/*        MOVE 'Y'                TO WSSP-EDTERROR.                */
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(sv.currcd);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void readT66342400()
	{
		start2400();
	}

protected void start2400()
	{
		/*  Get the Letter type from T6634.                                */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		/* MOVE T6634                      TO ITEM-ITEMTABL.    <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		/*  Build key to T6634 from contract type & transaction code.      */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrsurIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		/* IF  ITEM-STATUZ             NOT = O-K                <LA4540>*/
		/*     MOVE ITEM-PARAMS            TO SYSR-PARAMS       <LA4540>*/
		/*     MOVE G437                   TO SYSR-STATUZ       <LA4540>*/
		/*     PERFORM 600-FATAL-ERROR.                         <LA4540>*/
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("***");
			stringVariable2.addExpression(wsaaBatckey.batcBatctrcde);
			stringVariable2.setStringInto(itemIO.getItemitem());
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				sv.letprtflagErr.set(errorsInner.rl36);
				return ;
			}
		}
		/* MOVE ITEM-GENAREA               TO T6634-T6634-REC.  <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType, SPACES)) {
			sv.letprtflagErr.set(errorsInner.rl36);
		}
	}

protected void convertPolicyloan2400()
	{
		start12400();
	}

protected void start12400()
	{
		/*  Convert the Policy Loan into the requested Currency.           */
		/*  Note, the LOAN value as returned from TOTLOAN will always      */
		/*  be in the CHDR currency. Therefore always use that value       */
		/*  and that currency to convert from.                             */
		/* MOVE 'CVRT'                 TO CLNK-FUNCTION.           <CAS1*/
		conlinkrec.function.set("SURR");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(chdrsurIO.getCntcurr());
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(wsaaHeldCurrLoans);
		conlinkrec.company.set(chdrsurIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		conlinkrec.currOut.set(sv.currcd);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		sv.policyloan.set(conlinkrec.amountOut);
	}

protected void checkPremadj2600()
	{
		start2610();
	}

protected void start2610()
	{
		/* Call the surrender routine for premium adjustments. Note that   */
		/* for the moment this uses the same copy-book as the surrender    */
		/* routine, which is stored and then re-instated .........         */
		wsaaSurrenderRecStore.set(srcalcpy.surrenderRec);
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(payrIO.getBillfreq());
		}
		/* MOVE CHDRSUR-BILLFREQ       TO WSAA-BILLFREQ.        <A05691>*/
		srcalcpy.estimatedVal.set(covrsurIO.getInstprem());
		srcalcpy.actualVal.set(ZERO);
		callProgram(t5611rec.calcprog, srcalcpy.surrenderRec);
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		else {
			if (isNE(srcalcpy.status, varcom.oK)
			&& isNE(srcalcpy.status, varcom.endp)) {
				syserrrec.params.set(srcalcpy.surrenderRec);
				syserrrec.statuz.set(srcalcpy.status);
				fatalError600();
			}
		}
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		zrdecplrec.currency.set(sv.cnstcur);
		a000CallRounding();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		/* Note adjustments are subtracted from the total.                 */
		/* The actual value here is the premium adjustment returned...     */
		sv.otheradjst.add(srcalcpy.actualVal);
		srcalcpy.surrenderRec.set(wsaaSurrenderRecStore);
	}

protected void checkT56112700()
	{
		start2710();
	}

protected void start2710()
	{
		/* Read the Coverage Surrender/Paid-Up/Bonus parameter table.      */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(tablesInner.t5611);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		wsaaT5611Crtable.set(srcalcpy.crtable);
		wsaaT5611Pstatcode.set(srcalcpy.pstatcode);
		itdmIO.setItemitem(wsaaT5611Item);
		itdmIO.setItmfrm(srcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			if (isNE(itdmIO.getStatuz(), varcom.endp)
			&& isEQ(itdmIO.getItemtabl(), tablesInner.t5611)
			&& isEQ(itdmIO.getItemcoy(), srcalcpy.chdrChdrcoy)
			&& isEQ(itdmIO.getItemitem(), wsaaT5611Item)) {
				t5611rec.t5611Rec.set(itdmIO.getGenarea());
				wsaaSumFlag.set("Y");
			}
			else {
				wsaaSumFlag.set(SPACES);
			}
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		/*    IF SCRN-STATUZ                = 'NOTE'*/
		if (isEQ(sv.letterPrintFlag, "Y")
		|| isEQ(sv.letterPrintFlag, "y")) {
			/*NEXT_SENTENCE*/
		}
		else {
			return ;
		}
		svltsurIO.setChdrcoy(wsspcomn.company);
		svltsurIO.setChdrnum(sv.chdrnum);
		svltAlreadyExists3100();
		/*  Move details stored in Working Storage to SVLT Record.         */
		svltsurIO.setCoverages(wsaaStoredLetterDetailInner.wsaaSvltCoverages);
		svltsurIO.setRiders(wsaaStoredLetterDetailInner.wsaaSvltRiders);
		svltsurIO.setShortdss(wsaaStoredLetterDetailInner.wsaaSvltShortdss);
		svltsurIO.setTypes(wsaaStoredLetterDetailInner.wsaaSvltTypes);
		/*  MOVE WSAA-SVLT-RIINDS           TO SVLTSUR-RIINDS.      <002>*/
		svltsurIO.setVirtfunds(wsaaStoredLetterDetailInner.wsaaSvltVirtfunds);
		svltsurIO.setCnstcurs(wsaaStoredLetterDetailInner.wsaaSvltCnstcurs);
		svltsurIO.setEmvs(wsaaStoredLetterDetailInner.wsaaSvltEmvs);
		svltsurIO.setActvalues(wsaaStoredLetterDetailInner.wsaaSvltActvalues);
		/*  Store general Contract details in SVLT record area.            */
		svltsurIO.setPlanSuffix(sv.planSuffix);
		svltsurIO.setCnttype(sv.cnttype);
		svltsurIO.setCownnum(sv.cownnum);
		svltsurIO.setCtypedes(sv.ctypedes);
		svltsurIO.setLifcnum(sv.lifcnum);
		svltsurIO.setLinsname(sv.linsname);
		svltsurIO.setJlifcnum(sv.jlifcnum);
		svltsurIO.setJlinsname(sv.jlinsname);
		svltsurIO.setOccdate(sv.occdate);
		svltsurIO.setOwnername(sv.ownername);
		svltsurIO.setPstate(sv.pstate);
		svltsurIO.setPtdate(sv.ptdate);
		svltsurIO.setRstate(sv.rstate);
		svltsurIO.setTransactionDate(wsaaTransactionDate);
		svltsurIO.setTransactionTime(wsaaTransactionTime);
		svltsurIO.setUser(wsaaUser);
		svltsurIO.setTermid(wsaaTermid);
		svltsurIO.setValidflag("1");
		svltsurIO.setFunction(varcom.writr);
		svltsurIO.setFormat(formatsInner.svltsurrec);
		SmartFileCode.execute(appVars, svltsurIO);
		if (isNE(svltsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(svltsurIO.getParams());
			syserrrec.statuz.set(svltsurIO.getStatuz());
			fatalError600();
		}
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrsurIO.getChdrcoy());
		/*    MOVE 'SURRVALU'             TO LETRQST-LETTER-TYPE.          */
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(sv.effdate);
		/*    MOVE CHDRSUR-CHDRCOY        TO LETRQST-CLNTCOY.              */
		letrqstrec.clntcoy.set(chdrsurIO.getCowncoy());
		letrqstrec.clntnum.set(chdrsurIO.getCownnum());
		letrqstrec.otherKeys.set(SPACES);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(chdrsurIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrsurIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrsurIO.getChdrnum());
		letrqstrec.chdrnum.set(chdrsurIO.getChdrnum());
		letrqstrec.branch.set(wsspcomn.branch);
		/*  MOVE CHDRSUR-CHDRNUM        TO WSAA-CHDRNUM.                 */
		/*    IF   WSAA-ACTUAL-TOT         > WSAA-ESTIMATE-TOT             */
		/*         MOVE WSAA-ACTUAL-TOT   TO WSAA-SV-TOTAL                 */
		/*    ELSE                                                         */
		/*         MOVE WSAA-ESTIMATE-TOT TO WSAA-SV-TOTAL.                */
		/*  MOVE WSAA-LETOKEYS          TO LETRQST-OTHER-KEYS.           */
		letrqstrec.function.set("ADD");
		/*        CALL 'LETRQST' USING LETRQST-PARAMS.                     */
		/* IF  T6634-LETTER-TYPE       NOT = SPACES             <PCPPRT>*/
		if (isNE(tr384rec.letterType, SPACES)) {
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz, varcom.oK)) {
				syserrrec.params.set(letrqstrec.params);
				syserrrec.statuz.set(letrqstrec.statuz);
				fatalError600();
			}
		}
	}

protected void svltAlreadyExists3100()
	{
		start3100();
	}

protected void start3100()
	{
		/*  Check to see if SVLT record already exists and if so mark it   */
		/*  as valid flag 2 and rewrite.                                   */
		svltsurIO.setFunction(varcom.readr);
		svltsurIO.setFormat(formatsInner.svltsurrec);
		SmartFileCode.execute(appVars, svltsurIO);
		if (isNE(svltsurIO.getStatuz(), varcom.oK)
		&& isNE(svltsurIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(svltsurIO.getParams());
			syserrrec.statuz.set(svltsurIO.getStatuz());
			fatalError600();
		}
		if (isEQ(svltsurIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		/*  Record already exists, READH, update valid flag and rewrite.   */
		svltsurIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, svltsurIO);
		if (isNE(svltsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(svltsurIO.getParams());
			syserrrec.statuz.set(svltsurIO.getStatuz());
			fatalError600();
		}
		svltsurIO.setValidflag("2");
		svltsurIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, svltsurIO);
		if (isNE(svltsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(svltsurIO.getParams());
			syserrrec.statuz.set(svltsurIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void addToSvlt5000()
	{
		start5000();
	}

	/**
	* <pre>
	*  Add to variable portion of SVLT record.                        
	* </pre>
	*/
protected void start5000()
	{
		wsaaSvltSub.add(1);
		if (isLTE(wsaaSvltSub, 10)) {
			wsaaStoredLetterDetailInner.wsaaSvltCoverage[wsaaSvltSub.toInt()].set(sv.coverage);
			wsaaStoredLetterDetailInner.wsaaSvltRider[wsaaSvltSub.toInt()].set(sv.rider);
			wsaaStoredLetterDetailInner.wsaaSvltShortds[wsaaSvltSub.toInt()].set(sv.shortds);
			wsaaStoredLetterDetailInner.wsaaSvltType[wsaaSvltSub.toInt()].set(sv.fieldType);
			/*     MOVE S6675-RIIND    TO WSAA-SVLT-RIIND (WSAA-SVLT-SUB)<002*/
			wsaaStoredLetterDetailInner.wsaaSvltVirtfund[wsaaSvltSub.toInt()].set(sv.fund);
			wsaaStoredLetterDetailInner.wsaaSvltCnstcur[wsaaSvltSub.toInt()].set(sv.cnstcur);
			wsaaStoredLetterDetailInner.wsaaSvltEmv[wsaaSvltSub.toInt()].set(sv.estMatValue);
			/*     MOVE S6675-ACTVALUE TO WSAA-SVLT-ACTVALUE (WSAA-SVLT-SUB) */
			if (isEQ(sv.fieldType, "C")) {
				compute(wsaaStoredLetterDetailInner.wsaaSvltActvalue[wsaaSvltSub.toInt()], 2).set(mult(-1, sv.actvalue));
			}
			else {
				wsaaStoredLetterDetailInner.wsaaSvltActvalue[wsaaSvltSub.toInt()].set(sv.actvalue);
			}
		}
	}

protected void checkCalcTax5100()
	{
		start5110();
		readTr52d5120();
		readSubFile5130();
	}

protected void start5110()
	{
		wsaaFeeTax.set(ZERO);
		/* Read CHDRENQ                                                    */
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		chdrenqIO.setChdrcoy(chdrsurIO.getChdrcoy());
		chdrenqIO.setChdrnum(chdrsurIO.getChdrnum());
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
	}

protected void readTr52d5120()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrenqIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrenqIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrenqIO.getChdrcoy());
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void readSubFile5130()
	{
		/* Accumulate TOTAL FEE                                            */
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6675", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			if (isEQ(sv.fieldType, "C")
			|| isEQ(sv.fieldType, "J")) {
				callTaxsubr5200();
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("S6675", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		sv.taxamt.add(wsaaFeeTax);
	}

protected void callTaxsubr5200()
	{
		start5200();
	}

protected void start5200()
	{
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
		wsaaTr52eCrtable.set(sv.hcrtable);
		readTr52e5300();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrenqIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e5300();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e5300();
		}
		/* IF TR52E-TAXIND-05          NOT = 'Y'                        */
		if (isNE(tr52erec.taxind03, "Y")) {
			/*     GO TO 5190-EXIT                                   <S19FIX>*/
			return ;
		}
		/* Call tax subroutine                                             */
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrenqIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrenqIO.getChdrnum());
		txcalcrec.life.set(sv.hlife);
		txcalcrec.coverage.set(sv.coverage);
		txcalcrec.rider.set(sv.rider);
		txcalcrec.crtable.set(sv.hcrtable);
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrenqIO.getCnttype());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.register.set(chdrenqIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		if (isEQ(sv.currcd, SPACES)) {
			txcalcrec.ccy.set(chdrenqIO.getCntcurr());
			wsaaCntCurr.set(chdrenqIO.getCntcurr());
		}
		else {
			txcalcrec.ccy.set(sv.currcd);
			wsaaCntCurr.set(sv.currcd);
		}
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.cntTaxInd.set(SPACES);
		/*   MOVE WSAA-TOTAL-FEE         TO TXCL-AMOUNT-IN.               */
		txcalcrec.amountIn.set(sv.actvalue);
		if (isLT(sv.actvalue, 0)) {
			compute(txcalcrec.amountIn, 2).set(mult(sv.actvalue, -1));
		}
		else {
			txcalcrec.amountIn.set(sv.actvalue);
		}
		txcalcrec.transType.set("SURF");
		txcalcrec.effdate.set(sv.effdate);
		txcalcrec.tranno.set(ZERO);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaFeeTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void readTr52e5300()
	{
		start5300();
	}

protected void start5300()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(chdrenqIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			/*        MOVE WSAA-TR52E-KEY     TO SYSR-PARAMS           <LA4895>*/
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), chdrenqIO.getChdrcoy()))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-STORED-LETTER-DETAIL--INNER
 */
public static final class WsaaStoredLetterDetailInner { 

	public FixedLengthStringData wsaaStoredLetterDetail = new FixedLengthStringData(400);
	private FixedLengthStringData wsaaSvltCoverages = new FixedLengthStringData(20).isAPartOf(wsaaStoredLetterDetail, 0);
	private FixedLengthStringData[] wsaaSvltCoverage = FLSArrayPartOfStructure(10, 2, wsaaSvltCoverages, 0);
	private FixedLengthStringData wsaaSvltRiders = new FixedLengthStringData(20).isAPartOf(wsaaStoredLetterDetail, 20);
	private FixedLengthStringData[] wsaaSvltRider = FLSArrayPartOfStructure(10, 2, wsaaSvltRiders, 0);
	private FixedLengthStringData wsaaSvltShortdss = new FixedLengthStringData(100).isAPartOf(wsaaStoredLetterDetail, 40);
	private FixedLengthStringData[] wsaaSvltShortds = FLSArrayPartOfStructure(10, 10, wsaaSvltShortdss, 0);
	private FixedLengthStringData wsaaSvltTypes = new FixedLengthStringData(10).isAPartOf(wsaaStoredLetterDetail, 140);
	private FixedLengthStringData[] wsaaSvltType = FLSArrayPartOfStructure(10, 1, wsaaSvltTypes, 0);
		/* 03  WSAA-SVLT-RIINDS.                                   <002>*/
	private FixedLengthStringData wsaaSvltVirtfunds = new FixedLengthStringData(40).isAPartOf(wsaaStoredLetterDetail, 150);
	private FixedLengthStringData[] wsaaSvltVirtfund = FLSArrayPartOfStructure(10, 4, wsaaSvltVirtfunds, 0);
	private FixedLengthStringData wsaaSvltCnstcurs = new FixedLengthStringData(30).isAPartOf(wsaaStoredLetterDetail, 190);
	private FixedLengthStringData[] wsaaSvltCnstcur = FLSArrayPartOfStructure(10, 3, wsaaSvltCnstcurs, 0);
		/* 03  WSAA-SVLT-EMVS.                                  <D96NUM>
		 03  WSAA-SVLT-ACTVALUES.                             <D96NUM>*/
	private FixedLengthStringData wsaaSvltEmvs = new FixedLengthStringData(90).isAPartOf(wsaaStoredLetterDetail, 220);
	public PackedDecimalData[] wsaaSvltEmv = PDArrayPartOfStructure(10, 17, 2, wsaaSvltEmvs, 0);
	private FixedLengthStringData wsaaSvltActvalues = new FixedLengthStringData(90).isAPartOf(wsaaStoredLetterDetail, 310);
	public PackedDecimalData[] wsaaSvltActvalue = PDArrayPartOfStructure(10, 17, 2, wsaaSvltActvalues, 0);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData t062 = new FixedLengthStringData(4).init("T062");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData g941 = new FixedLengthStringData(4).init("G941");
	private FixedLengthStringData h377 = new FixedLengthStringData(4).init("H377");
	private FixedLengthStringData g008 = new FixedLengthStringData(4).init("G008");
	private FixedLengthStringData t065 = new FixedLengthStringData(4).init("T065");
	private FixedLengthStringData g094 = new FixedLengthStringData(4).init("G094");
	private FixedLengthStringData rl36 = new FixedLengthStringData(4).init("RL36");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t6598 = new FixedLengthStringData(5).init("T6598");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
	private FixedLengthStringData t5611 = new FixedLengthStringData(5).init("T5611");
	private FixedLengthStringData tr691 = new FixedLengthStringData(5).init("TR691");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData svltsurrec = new FixedLengthStringData(10).init("SVLTSURREC");
	private FixedLengthStringData tpoldbtrec = new FixedLengthStringData(10).init("TPOLDBTREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
}
protected void customerSpecificnetofSvDebt(){
	
}
}
