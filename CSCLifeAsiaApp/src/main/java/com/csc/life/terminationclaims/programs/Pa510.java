/**
 * Pa510 —— Notification Notes from Sa508 hyperlink
 */
package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.dao.ClnnpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.csc.life.terminationclaims.screens.Sa510ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The details of  the  contract  being  enquired  upon  will be
*     stored in the  CHDRENQ  I/O  module. Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*     The PAYR file is read for the contract,followed by T3620.
*     If the contract requires bank details(i.e Direct Debit with
****  T3620-DDIND NOT = SPACES),then the Mandate file is read
****  to obtain the Mandate detalis.
*     T3620-DDIND NOT = SPACES or Credit Card with T3620-CRCIND
*     NOT = SPACES),then the Mandate file is read to obtain the
*     Mandate detalis.
*
*     Set up the  Factoring  House, (CHDR-FACTHOUS), on the screen.
*     Valid Factoring House  codes  are held on T3684. (See GETDESC
*     for an example of how to obtain the long description from the
*     DESC data-set. Do  not  call  GETDESC,  the  appropriate code
*     should be moved into the mainline.
*
*     Set up the  Bank  Code,  (CHDR-BANKKEY),  on  the  screen and
*     obtain the  corresponding  description  from the Bank Details
*     data-set BABR.  Read  BABR  with  a  key  of CHDR-BANKKEY and
*     obtain BABR-BANKDESC.
*
*     Set up the  Account  Number, (CHDR-BANKACCKEY), on the screen
*     and  obtain  the  correeponding  description  from  the  CLBL
*     data-set.  Read  CLBL   with   a   key  of  CHDR-BANKKEY  and
*     CHDR-BANKACCKEY and obtain CLBL-BANKADDDSC and CLBL-CURRCODE.
*
*     If the Assignee client, (CHDR-ASGNNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the usual way.
*
*     If the Despatch client, (CHDR-DESPNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the ususal way.
*
*
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The only validation  required  is  of  the  indicator field -
*     CONBEN. This may be space or 'X'.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was  pressed  move  spaces  to  the current program
*     position in the program stack and exit.
*
*     If returning from  processing  a  selection  further down the
*     program stack, (current  stack  action  field  will  be '*'),
*     restore the next  8  programs  from  the  WSSP and remove the
*     asterisk.
*
*     If nothing was  selected move '*' to the current stack action
*     field, add 1 to the program pointer and exit.
*
*     If a selection has  been found use GENSWCH to locate the next
*     program(s)  to process  the  selection,  (up  to  8).  Use  a
*     function of 'A'. Save  the  next  8 programs in the stack and
*     replace them with  the  ones  returned from GENSWCH. Place an
*     asterisk in the  current  stack  action  field,  add 1 to the
*     program pointer and exit.
*
*
* Notes.
* ------
*
*
*****************************************************************
* </pre>
*/
public class Pa510 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final String CLMPREFIX = "CLMNTF";
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA510");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);

	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sa510ScreenVars sv = getPScreenVars() ;
	private Clntpf clntpf;
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	protected static final String h093 = "H093";
	private Subprogrec subprogrec = new Subprogrec();
	//CML001
	private ClnnpfDAO clnnpfDAO= getApplicationContext().getBean("clnnpfDAO",ClnnpfDAO.class);
	protected static final String chdrenqrec = "CHDRENQREC";
	protected ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator planLevel = new Validator(wsaaPlanproc, "Y");
	private List<Clnnpf> clnnpfList = new ArrayList<Clnnpf>();
	private Clnnpf clnnpf = null;
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(4, 0).setUnsigned();
	private Optswchrec optswchrec = new Optswchrec();
	private PackedDecimalData wsaaRrn = new PackedDecimalData(5, 0);
	protected static final String e186 = "E186";
	
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2090,
		exit3090
	}

	public Pa510() {
		super();
		screenVars = sv;
		new ScreenModel("Sa510", AppVars.getInstance(), sv);
	}

	protected Sa510ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sa510ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			e.printStackTrace();
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		}catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			e.printStackTrace();
		}catch (Exception ex){
			ex.printStackTrace();
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*      GO TO 1090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		sv.select.set(SPACES);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		wsaaBatckey.set(wsspcomn.batchkey);	
		getLifeAssSection1020();
		loadSubFile();
		callOptSwitch();
	

}
	
	private void loadSubFile() {
		try{
		clnnpfList =  clnnpfDAO.getClnnpfList(wsspcomn.company.toString(), sv.notifinum.toString().trim().replace(CLMPREFIX, ""),wsspcomn.wsaaclaimno.toString().trim());
		if(clnnpfList!=null && !clnnpfList.isEmpty()){
			wsaaCount.set(clnnpfList.size());
			for(Clnnpf clnnpf : clnnpfList){
				if(wsspcomn.chdrCownnum.toString().trim().equals(clnnpf.getLifenum().trim())) { //IBPLIFE-906
					sv.select.set(ZERO);
					sv.sequence.set(wsaaCount);
					sv.accdesc.set(clnnpf.getNotificationNote().trim());
					Date parse=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(clnnpf.getDatime().substring(0,16));
					String dateString = new SimpleDateFormat("yyyyMMdd").format(parse);
					sv.date.set(dateString);
					dateString = parse.toString().substring(11, 16);
					sv.acctime.set(dateString);
					sv.userID.set(clnnpf.getUsrprf());
					scrnparams.function.set(varcom.sadd);
					screenIo9000();
					wsaaCount.minusminus();
				}
				else {
					sv.sequence.set(wsaaCount);
					wsaaCount.minusminus();
				}
			}
		}
		}
		catch(ParseException e){
			e.printStackTrace();
		}
	}
	
	protected void screenIo9000()
	{
		/*BEGIN*/
		processScreen("SA510", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
				&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
	
	private void callOptSwitch(){
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if(isEQ(wsspcomn.flag,"I")){
			sv.indxflgOut[varcom.pr.toInt()].set("Y");
		}
	}
	private void getLifeAssSection1020() {
		try{
			sv.lifcnum.set(wsspcomn.chdrCownnum);
			if(isNE(wsspcomn.wsaanotificationNum,SPACES))
				sv.notifinum.set(wsspcomn.wsaanotificationNum.toString());
			else
				sv.notifinum.set(SPACES);	
			clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), wsspcomn.chdrCownnum.toString());
			if(clntpf ==null){
				syserrrec.params.set(wsspcomn.fsuco.toString().concat(wsspcomn.chdrCownnum.toString()));
				fatalError600();
			}
			else{
				String Surname = clntpf.getSurname() != null?clntpf.getSurname().trim(): " ";
	            String GivenName = clntpf.getGivname() != null?clntpf.getGivname().trim(): " ";
				sv.lifename.set(Surname+","+GivenName);
			}
			sv.relation.set(wsspcomn.wsaarelationship);
			sv.claimant.set(wsspcomn.wsaaclaimant);
			clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), sv.claimant.toString());
			if(clntpf ==null){
				syserrrec.params.set(wsspcomn.fsuco.toString().concat(wsspcomn.chdrCownnum.toString()));
				fatalError600();
			}
			else{
				String Surname = clntpf.getSurname() != null?clntpf.getSurname().trim(): " ";
	            String GivenName = clntpf.getGivname() != null?clntpf.getGivname().trim(): " ";
	            sv.clamnme.set(Surname+","+GivenName);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* IF   WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                  */
		/*      MOVE O-K               TO WSSP-EDTERROR                 */
		/*      GO TO 2090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();					
				case exit2090: 
					exit2090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	
	}

	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/

protected void screenIo2010()
	{	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			return;
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			exit2090();
		}
		 if (isEQ(scrnparams.statuz, varcom.calc)) {
				wsspcomn.edterror.set("Y");
		}
		
	}

protected void exit2090(){
	
	if (isNE(sv.errorSubfile, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}


protected void validate2020()
	{
	 if (isEQ(scrnparams.statuz, varcom.kill)) {
         return;
     }
	 scrnparams.function.set(varcom.srnch);
	 screenIo9000();
     if (isEQ(sv.indxflg, "Y") && isNE(sv.select,ZERO)) {
    	 scrnparams.errorCode.set(errorsInner.RRSL);
    	 sv.select.set(ZERO);
    	 wsspcomn.edterror.set("Y");
    	 scrnparams.function.set(varcom.supd);
      	 screenIo9000();
    	 return;
     }
	 if(isEQ(sv.select,3) || isEQ(sv.select,4)){
		 checkUserSanc();
	 }
     if (isEQ(scrnparams.statuz, varcom.calc)) {
         wsspcomn.edterror.set("Y");
     }
	}
	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void checkUserSanc(){
	
	 if(clnnpfList!=null && !clnnpfList.isEmpty()){
		 wsaaIndex.set(sub(clnnpfList.size(),sv.sequence.toInt()));
		 if(isNE(wsspcomn.userid,clnnpfList.get(wsaaIndex.toInt()).getUsrprf())){
			 scrnparams.errorCode.set(errorsInner.RRSM);
			 sv.select.set(ZERO);
			 wsspcomn.edterror.set("Y");
			 scrnparams.function.set(varcom.supd);
		  	 screenIo9000();
		 }
	 }
}

protected void update3000()
	{	
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
		return;
	}
	if(isEQ(sv.indxflg,"Y") && isEQ(sv.select,ZERO)){
		sv.select.set(2);
	 }
	 if(clnnpfList!=null && !clnnpfList.isEmpty()){
		wsaaIndex.set(sub(clnnpfList.size(),sv.sequence.toInt()));
		clnnpf = clnnpfList.get(wsaaIndex.toInt());
		clnnpfDAO.setCacheObject(clnnpf);
	}	
	
}
/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
{
	nextProgram4010();
}

protected void saveProgram4100()
{
	/*SAVE*/
	wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
} 
protected void restoreProgram4100()
{
	/*PARA*/
	wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
	sub1.add(1);
	sub2.add(1);
	/*EXIT*/
}

protected void restoreProgram4200()
{
	/*RESTORE*/
	wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
}

protected void loadProgram4400()
{
	/*RESTORE1*/
	wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT1*/
}
	protected void nextProgram4010(){
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.nextprog.set(wsaaProg);
		}
		optswchCall4500();
	}
	protected void optswchCall4500()
	{
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set(SPACES);
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelType.set("L");
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		sv.select.set(ZERO);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if ((isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp))) {
			optswchrec.optsItemCompany.set(wsspcomn.company);
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz, varcom.endp)) {
			sv.select.set(ZERO);
			scrnparams.function.set(varcom.supd);
			screenIo9000();
			scrnparams.function.set(varcom.sclr);
			screenIo9000();
			loadSubFile();
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);		
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */

	protected void prepareName(String firstName, String lastName) {
		String fullName = "";
		if (isNE(firstName, SPACES)) {
			fullName = getStringUtil().plainName(firstName, lastName, ",");
			 
		} else {
			fullName = lastName;
		}
		wsspcomn.longconfname.set(fullName);
	}

	/**
	 * can be overriden to change the its behaviour
	 * @return
	 */
	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}
	
	public Subprogrec getSubprogrec() {
		return subprogrec;
	}
	public void setSubprogrec(Subprogrec subprogrec) {
		this.subprogrec = subprogrec;
	}
	
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 
		 private FixedLengthStringData RRNJ = new FixedLengthStringData(4).init("RRNJ");
		 private FixedLengthStringData RRSL = new FixedLengthStringData(4).init("RRSL");
		 private FixedLengthStringData RRSM = new FixedLengthStringData(4).init("RRSM");
	}

}


