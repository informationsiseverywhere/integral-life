package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SD5JP
 * @version 1.0 generated on 30/08/09 07:05
 * @author Quipoz
 */
public class Sd5jpScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(475);
	public FixedLengthStringData dataFields = new FixedLengthStringData(133).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,8);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,38);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData ownersel = DD.ownersel.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData premStatDesc = DD.pstatdsc.copy().isAPartOf(dataFields,103);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,113);
    public FixedLengthStringData statdsc = DD.statdsc.copy().isAPartOf(dataFields,121);
	public FixedLengthStringData rfundflgs = new FixedLengthStringData(2).isAPartOf(dataFields, 131);
	public FixedLengthStringData[] rfundflg = FLSArrayPartOfStructure(2, 1, rfundflgs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(rfundflgs, 0, FILLER_REDEFINE);
	public FixedLengthStringData rfundflg01 = DD.rfundflg.copy().isAPartOf(filler,0);
	public FixedLengthStringData rfundflg02 = DD.rfundflg.copy().isAPartOf(filler,1);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 133);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ownerselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData pstatdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData statdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData rfundflgsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] rfundflgErr = FLSArrayPartOfStructure(2, 4, rfundflgsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(rfundflgsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rfundflg01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData rfundflg02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 173);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ownerselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] pstatdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] statdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData rfundflgsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] rfundflgOut = FLSArrayPartOfStructure(2, 12, rfundflgsOut, 0);
	public FixedLengthStringData[][] rfundflgO = FLSDArrayPartOfArrayStructure(12, 1, rfundflgOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(rfundflgsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rfundflg01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] rfundflg02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public LongData Sd5jpscreenWritten = new LongData(0);
	public LongData Sd5jpprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5jpScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(rfundflg02Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, ctypedes, statdsc, premStatDesc, ownersel, ownername, occdate, ptdate, rfundflg01, rfundflg02};
		screenOutFields = new BaseData[][] {chdrnumOut, ctypedesOut, statdscOut, pstatdscOut, ownerselOut, ownernameOut, occdateOut, ptdateOut, rfundflg01Out, rfundflg02Out};
		screenErrFields = new BaseData[] {chdrnumErr, ctypedesErr, statdscErr, pstatdscErr, ownerselErr, ownernameErr, occdateErr, ptdateErr, rfundflg01Err, rfundflg02Err};
		screenDateFields = new BaseData[] {occdate, ptdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5jpscreen.class;
		protectRecord = Sd5jpprotect.class;
	}

}
