/*
 * File: P6676.java
 * Date: 30 August 2009 0:51:27
 * Author: Quipoz Limited
 * 
 * Class transformed from P6676.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5677rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.FluprgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpenqTableDAM;
import com.csc.life.terminationclaims.screens.S6676ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgNonCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  P6676 - Work With Regular Payments.
*  -----------------------------------
*
*  Overview.
*  ---------
*
*  This program  is  the  sub-menu  for  the  Regular  Payments
*  sub-system.  It  uses  the  'Work  With' approach to provide
*  easy access to  all  the  functions  necessary  to  maintain
*  regular payment details against components.
*
*  The  user  will  enter  the  contract number of the contract
*  that is to be worked on and the subfile part of  the  screen
*  will   display  all  of  the  components  of  the  contract,
*  including  any  existing  payment  details.  These  will  be
*  interspersed   between  the  components  so  that  they  are
*  visually  associated  with  the  components  to  which  they
*  relate.  The  usual  indentation  method will be employed by
*  the  subfile  portion  of  the  screen  to   represent   the
*  structure  of  the  contract  and the interdependance of the
*  coverages and riders within the lives assured.
*
*  The user will control the work to be carried out by  placing
*  certain  actions  against  the  components  or  the existing
*  payment details.
*
*  The program  switching  that  follows  will  be  unlike  the
*  standard  SMART  sub-menu  switching  in  that  it  will  be
*  generically  driven.  Depending  on   the   component   type
*  selected  the  transaction  will  switch  the user to either
*  Regular  Withdrawals  or  Regular  Benefit  Claims.  Regular
*  Withdrawals  will  be  set up for Unit Linked components and
*  Regular  Claims  will  be  set  up   for   Regular   Benefit
*  components such as disability or waiver of premium riders.
*
*  This  means  that  it  will not be possible to set up a Unit
*  Linked Regular Benefit component if it will be necessary  to
*  create Regular Withdrawals against it. A contract containing
*  this sort of combination  is likely to be  set up as  a Unit
*  Linked coverage with a Regular Benefit rider.
*
*  Each  selection  will  be  treated  as  a  separate business
*  transaction and so there will be a commit issued after  each
*  update   type  function.  If  the  user  has  made  multiple
*  selections and a selection other than the last one  requires
*  AT  to  be  invoked then the control of the softlock will be
*  passed to AT and this program will be unable to  handle  the
*  subsequent  selections. A message will therefore be provided
*  to the effect that the previous selection is incomplete  and
*  the other selections cannot yet be processed.
*
*  Initialise.
*  -----------
*
*  If  returning from processing a Batch enquiry, SCRN-STATUZ =
*  'BACH', skip this section.
*
*  Initialise all  Working  Storage  variables  and  clear  the
*  subfile ready for loading.
*
*  Display and Validation. (2000 Section).
*  ---------------------------------------
*
*  Page  up  and  Page  down  should  never  be received as the
*  subfile load will load all of the  components  and  existing
*  payment  details  regardless  of  how  many there are. There
*  should never be so many that it impairs system response.
*
*  The user may opt to change the contract number at any  time.
*  If  this  happens  then  no  selections are to be processed.
*  Compare the Contract  Number  with  the  store,  if  it  has
*  changed then proceed as from Initial Screen Load.
*
*       Initial Screen Load.
*       --------------------
*
*       Store  the new contract number, initialise any relevant
*       variables, clear the subfile ready for loading.
*
*       Ensure that the Contract Number  entered  is  valid  by
*       checking it against CHDR.
*
*       If  it  is,  store  the record in the I/O module with a
*       KEEPS, display its Contract Type and  long  description
*       from  table  T5688  and  read all of the components and
*       write one line to the  subfile  for  each.  The  normal
*       indentation  should  be  employed in the display of the
*       Life, Coverage and Rider  sequence  numbers.  For  each
*       life  read  the LIFE file and obtain the Life Assured's
*       Client Number. Place this on the screen and  read  CLTS
*       to  obtain  the client's name. Format this for display.
*       The selection field should be protected  for  the  line
*       displaying the life details.
*
*       On  the  coverage  and rider lines display the coverage
*       code, (CRTABLE) and  read  T5687  to  obtain  the  long
*       description for display.
*
*       The  component  lines  should  be interspersed with the
*       details of any existing  payment  detail.  So  for  any
*       component  read the Regular Payment Details file, REGP,
*       and if corresponding  records  are  found  display  the
*       payment details as follows:
*
*            .  In  the  same  place  as  the client number and
*            coverage  code  are  displayed  show   the   short
*            description from T6691, keyed on the Payment Type.
*
*            . Display the payment status.
*
*            .  Display  the  First  Payment Date and the Final
*            Payment Date if it is not Max Date.
*
*            .  If  the  amount  is   non-zero   display   this
*            otherwise display the percentage.
*
*            . Display the currency code.
*
*       These  details  are all displayed on one long field and
*       so will require formatting within the program.
*
*       Also store the Regular Payment  Sequence  Number  in  a
*       hidden  field  on  the  subfile for later processing if
*       the line is selected for further action.
*
*       For all component and  Regular  Payment  subfile  lines
*       set  the  Life,  Coverage  and  Rider  numbers  in  the
*       appropriate fields but non-display where  necessary  to
*       provide  the  indentation  and  also  the values of the
*       fields for later processing. Also store the CRTABLE  in
*       a  hidden  field  on  each  subfile  record  for  later
*       processing.
*
*
*  Converse with the screen using the screen I/O module.
*
*  Validation.
*  -----------
*
*  Processing after a screen I/O will be one of the following:
*
*  . Changed Contract Number
*
*       If the Contract Number  has  changed  proceed  as  from
*       Initial  Screen Load by moving 'Y' to WSSP-EDTERROR and
*       going to exit.
*
*  . Selections to Process
*       Two passes will  be  made  over  the  subfile,  one  to
*       validate   the   selections   and   a  second,  if  the
*       selections are all valid, to process them.  The  second
*       pass will be carried out in the 3000 section.
*
*       For  the  first  pass perform a loop using Subfile Read
*       Next Changed to pick up all of the selected lines.  For
*       each  one  check  that it is valid by reading T6693 and
*       ensuring that the selected action is a valid  entry  on
*       the Extra Data Screen.
*
*       If  the selected line is itself a Coverage or Rider use
*       a key of  '**'  concatenated  with  the  Coverage/Rider
*       Code.  If  no  entry  is  found  then  Regular  Payment
*       details may not be set up against  this  component.  If
*       an  entry  is  found  then  check  the Transaction Code
*       obtained from T1690 against the Allowable  Transactions
*       on  the  Extra  Data  Screen. If no match is found then
*       the action is invalid against that component.
*
*       If the selected line is a  Payment  Details  line  then
*       read  T6693  with  a  key of the Regular Payment Status
*       concatenated with the associated Component Code. If  no
*       entry  is found then read again using four asterisks in
*       place of the Component  Code.  If  there  is  still  no
*       entry  then there is no possible action for the status.
*       If  an  entry  is  found  then  check  the  transaction
*       against  the  Allowable  Transactions on the Extra Data
*       Screen.
*
*       If no selections were made re-set the subfile RRN to  1
*       and re-display the screen.
*
*
*  . Batch Enquiry Required
*
*       A  Batch  Enquiry  may  be  requested  and as this is a
*       subfile submenu where multiple actions may  be  entered
*       at  once  it  is  essential that the program is able to
*       set the batch enquiry  processing  in  motion  for  the
*       correct   action.  In  order  to  avoid  confusion  and
*       provide the user  with  the  ability  to  indicate  for
*       which  action he requires the batch enquiry the program
*       will take the first non-blank  selection  field  to  be
*       the one for which batch enquiry is required.
*
*       Therefore  if the screen statuz is 'BACH' use Read Next
*       Changed  Subfile  to   locate   the   first   non-blank
*       selection  record. When you have located it sucessfully
*       set the screen status back to 'BACH'. If  no  selection
*       has  been  found  and  the  end  of the subfile reached
*       simply re-display the  screen  with  an  error  message
*       indicating that no selection has been made.
*
*       Access  T1690  using  the  subroutine 'SUBPROG' and the
*       action entered on  the  subfile.  If  batching  is  not
*       required,  SUBP-BCHRQD  not  =  'Y', then give an error
*       message, (E073). Otherwise retrieve the  batch  enquiry
*       programs as follows:
*
*       Call  the subroutine BCBPROG using the transaction code
*       from T1690  and  the  signon  company.  If  the  return
*       statuz  is  not O-K then use it as an error code on the
*       selection  field.  Otherwise  load  the  four  returned
*       programs moving BCBP-NXTPROG1 to WSSP-NEXT1PROG etc.
*
*  Updating.
*  ---------
*
*  If  batch  enquiry  was  selected, (screen statuz = 'BACH'),
*  skip this section.
*
*
*  The first time through after selections have  been  made  it
*  will  be  necessary  to  perform  a  start on the subfile to
*  re-position at the beginning after the validation pass.
*
*  Therefore if WSAA-SELECTION is space perform a start on  the
*  subfile.  Thereafter  whether WSAA-SELECTION is blank or not
*  continue to read until  the  first  non-blank  selection  is
*  located.
*
*  If  there  are no more selections and end of file is reached
*  set WSAA-SELECTION to space, set WSSP-EDTERROR to  'Y',  set
*  the  screen  RRN  to  1  and go to exit. This will cause the
*  screen to be re-displayed.
*
*  Each selection will be processed  one  at  a  time  and  the
*  appropriate  transaction programs loaded and switched to for
*  each action.
*
*  For each entry selected attempt to  perform  a  Softlock  on
*  the  Contract  Number.  Any  softlock on the contract from a
*  previous  selection  should  have  been  released   by   the
*  transaction  screen  that processed the selection. If it has
*  not released it then we must  assume  that  the  transaction
*  screen  passed  control  to  AT  and  therefore we must wait
*  unitl AT has completed before proceeding.  If  the  contract
*  is  found  to be locked and this is the first selection then
*  highlight the action and simply inform  the  user  that  the
*  contract  is in use. If this is not the first selection then
*  highlight the action and inform the user that  the  previous
*  transaction is incomplete.
*
*  If  the  softlock  is successful then locate the appropriate
*  transaction to use when reading T5671 in the following way:
*
*       Access T1690 by calling the subroutine SUBPROG  passing
*       the  signon  company,  program  id. and the action from
*       the subfile. If the returned Statuz  is  not  O-K  then
*       use it as an error code on the selection field.
*
*       If  the  returned  code is O-K then check that the user
*       is authorised to invoke the transaction by calling  the
*       subroutine SANCTN. Pass the following parameters:
*
*            . Function     -    'SUBM'
*            . Password     \
*            . Company       >   from WSSP
*            . Branch       /
*            . Transcode    -    SUBP-TRANSCD, (from T1690).
*
*       If  the  returned  Status  is 'BOMB' then perform fatal
*       error processing. If it is not O-K then use  it  as  an
*       error code on the selection field.
*
*  Set WSSP-FLAG accordingly:
*
*       . Create  - set WSSP-FLAG to 'C',
*       . Modify  -  "      "     "  'M',
*       . Delete  -  "      "     "  'D',
*       . Enquire -  "      "     "  'I'.
*
*  Treat  Approval  as  an  Enquiry and set WSSP-FLAG to 'I' so
*  that any screens windowed to  via  tha  indicators  will  be
*  protected.
*
*
*  If  the  selection  has  been made against a regular payment
*  line then store the REGP record with a READS.
*
*  If the selection is a Create  then  set  up  an  initialised
*  REGP record with the following:
*
*       CHDRCOY   -    Signon Company
*       CHDRNUM   -    Contract Number
*       PLNSFX    -    Zeroes
*       LIFE      -    Life Sequence Number
*       COVERAGE  -    Coverage Sequence Number
*       RIDER     -    Rider Sequence Number
*       CRTABLE   -    Coverage/Rider Code
*
*  and  the  remaining  fields  spaces and zeroes and perform a
*  KEEPS on REGP.
*
*  In Register Mode, Default  Follow  Ups will be created  if a
*  default  follow  up  method  exists on  T5688. T5677 is read
*  using the  follow up method  as  the  key and  up to 16 FLUP
*  records will be created.
*
*  Set the current selection in  the  field  WSAA-SELECTION  in
*  Working  Storage  to  indicate  that  a  selection  is to be
*  processed.  This  will  be  used  upon   return   from   the
*  transaction  so  that  this  program  can track its progress
*  through the selections.
*
*  Blank out  the  selection  field  and  rewrite  the  subfile
*  record.
*
*  Where Next.
*  -----------
*
*  If  batch  enquiry  was  selected, (screen statuz = 'BACH'),
*  then move zero to  WSSP-PROGRAM-PTR  and  WSSP-NEXT1PROG  to
*  WSSP-NEXTPROG. Skip the rest of this section.
*
*
*  This  section  will only be reached if a valid selection has
*  been made. If there was no selection then  the  screen  will
*  have  been  re-displayed. As this is a sub-menu the user may
*  only leave by using a function key.
*
*  The current line has  already  been  selected  in  the  3000
*  section  and this section will load the appropriate programs
*  and set the transaction in motion.
*
*  As the processing from here will be  generically  based  the
*  appropriate  programs  will  be  found  on T5671. Read T5671
*  using the Transaction Code concatenated with  the  Component
*  Code,  CRTABLE,  and  load  the programs into the first four
*  entries on the program stack.
*
*
*  Notes.
*  ------
*
*  Tables Used.
*  ------------
*
*  . T5661 - Follow Up Codes
*            Key: T5677-Follow up Code
*
*  . T5671 - Generic program Switching
*            Key: Transaction Code || CRTABLE
*
*  . T5677 - Default Follow Ups
*            Key: Transaction Code || Default Follow Up Method
*
*  . T5687 - Coverage / Rider Details
*            Key: CRTABLE
*
*  . T5688 - Contract Definition
*            Key: Contract Type
*
*  . T1690 - Submenu Switching
*            Key: WSAA-PROG and Submenu Action
*
*  . T6691 - Regular Payment Type
*            Key: Regular Payment Type
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be '****' and for selections against
*            a Component set Payment Status to '**'.
*
*****************************************************************
* </pre>
*/
public class P6676 extends ScreenProgNonCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6676");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	protected FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	protected FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
		/* TABLES */
	protected static final String t5661 = "T5661";
	private static final String t5671 = "T5671";
	protected static final String t5677 = "T5677";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t6691 = "T6691";
	private static final String t6693 = "T6693";

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Transcd = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);

	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);

	protected FixedLengthStringData wsaaT5677Key = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT5677Tranno = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 0);
	protected FixedLengthStringData wsaaT5677FollowUp = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 4);

	private FixedLengthStringData wsaaPaymentDetails = new FixedLengthStringData(49);
	private FixedLengthStringData wsaaPaystat = new FixedLengthStringData(2).isAPartOf(wsaaPaymentDetails, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 2, FILLER).init(SPACES);
	private FixedLengthStringData wsaaFirstPaydate = new FixedLengthStringData(10).isAPartOf(wsaaPaymentDetails, 3);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 13, FILLER).init(SPACES);
	private FixedLengthStringData wsaaFinalPaydate = new FixedLengthStringData(10).isAPartOf(wsaaPaymentDetails, 14).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 24, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaPaymentDetails, 25);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 28, FILLER).init(SPACES);
	private ZonedDecimalData wsaaAmtPrcntVal = new ZonedDecimalData(17, 2).isAPartOf(wsaaPaymentDetails, 29).setPattern("ZZZZZZZZZZZZZZ9.99");
	private FixedLengthStringData wsaaPercentSign = new FixedLengthStringData(2).isAPartOf(wsaaPaymentDetails, 47);
	private FixedLengthStringData wsaaChdrnumStore = new FixedLengthStringData(8);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaContractStatuzCheck = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).isAPartOf(wsaaContractStatuzCheck, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 1);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 3);
	protected PackedDecimalData wsaaX = new PackedDecimalData(2, 0).init(0).setUnsigned();
	protected ZonedDecimalData wsaaNextClamnum = new ZonedDecimalData(8, 0).setUnsigned();
	protected PackedDecimalData wsaaFupno = new PackedDecimalData(2, 0);
	protected ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	protected FluprgpTableDAM fluprgpIO = new FluprgpTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private RegpenqTableDAM regpenqIO = new RegpenqTableDAM();
	protected Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected T5661rec t5661rec = new T5661rec();
	private T5671rec t5671rec = new T5671rec();
	private T5677rec t5677rec = new T5677rec();
	private T5679rec t5679rec = new T5679rec();
	protected T5688rec t5688rec = new T5688rec();
	private T6693rec t6693rec = new T6693rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Wssplife wssplife = new Wssplife();
	private S6676ScreenVars sv = ScreenProgram.getScreenVars( S6676ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	protected FormatsInner formatsInner = new FormatsInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();

/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2100, 
		getRegPayment2310, 
		exit2300, 
		updateErrorIndicators2670, 
		checkActionsSanctions3030, 
		exit3090, 
		exit3490, 
		exit3590
	}

	public P6676() {
		super();
		screenVars = sv;
		new ScreenModel("S6676", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		/*  Check if a selection is being processed*/
		if (isNE(wsaaMiscellaneousInner.wsaaSelection, SPACES)) {
			return ;
		}
		scrnparams.subfileRrn.set(1);
		/*  Initialise the working storage variables*/
		wsaaT5671Key.set(SPACES);
		wsaaT6693Key.set(SPACES);
		wsaaPaymentDetails.set(SPACES);
		wsaaChdrnumStore.set(SPACES);
		wsaaMiscellaneousInner.wsaaFirstLifeRead.set(SPACES);
		wsaaMiscellaneousInner.wsaaFirstCovrRead.set(SPACES);
		wsaaMiscellaneousInner.wsaaFirstPaymRead.set(SPACES);
		wsaaMiscellaneousInner.wsaaRedisplay.set(SPACES);
		wsaaContractStatuzCheck.set(SPACES);
		wsaaMiscellaneousInner.wsaaSelection.set(SPACES);
		wsaaSub.set(ZERO);
		wsaaAmtPrcntVal.set(ZERO);
		wsaaMiscellaneousInner.wsaaEffdate.set(ZERO);
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
	}

	/**
	* <pre>
	*2010-VALIDATE-SCREEN.                                            
	* </pre>
	*/
protected void screenIo2010()
	{
		/*  Check if a selection is being processed*/
		/*  If WSAA-SELECTION = 'B' control is returning from*/
		/*   Batching and processing continues accordingly*/
		if (isEQ(wsaaMiscellaneousInner.wsaaSelection, "B")) {
			wsspcomn.edterror.set("Y");
			validation2040();
			return ;
			/******   GO TO 2040-CHECK-FOR-ERRORS                       <D509CS>*/
		}
		if ((isNE(wsaaMiscellaneousInner.wsaaSelection, SPACES))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			wsspcomn.edterror.set(varcom.oK);
			return ;
		}
		/*  Get todays date*/
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaMiscellaneousInner.wsaaEffdate.set(datcon1rec.intDate);
		/*  Check if it is the first time through or if the contract no. ha*/
		/*   changed.*/
		if ((isNE(sv.chdrsel, wsaaChdrnumStore))
		|| (isEQ(wsaaChdrnumStore, SPACES))
		|| (isEQ(wsaaMiscellaneousInner.wsaaRedisplay, "Y"))) {
			/*  Store the original contract number.*/
			wsaaChdrnumStore.set(sv.chdrsel);
			/*  Clear out the subfile*/
			wsaaMiscellaneousInner.wsaaRedisplay.set(SPACES);
			sv.dataArea.set(SPACES);
			sv.subfileArea.set(SPACES);
			scrnparams.function.set(varcom.sclr);
			processScreen("S6676", sv);
			if (isNE(scrnparams.statuz, varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			scrnparams.subfileRrn.set(1);
			/*  Move the stored number back out.*/
			sv.chdrsel.set(wsaaChdrnumStore);
			/*  Check that the contract number entered exists*/
			if ((isNE(sv.chdrsel, SPACES))) {
				chdrrgpIO.setParams(SPACES);
				chdrrgpIO.setChdrcoy(wsspcomn.company);
				chdrrgpIO.setChdrnum(sv.chdrsel);
				chdrrgpIO.setFormat(formatsInner.chdrrgprec);
				chdrrgpIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, chdrrgpIO);
				if ((isNE(chdrrgpIO.getStatuz(), varcom.oK))
				&& (isNE(chdrrgpIO.getStatuz(), varcom.mrnf))) {
					syserrrec.params.set(chdrrgpIO.getParams());
					fatalError600();
				}
				if (isEQ(chdrrgpIO.getStatuz(), varcom.mrnf)) {
					sv.chdrselErr.set(errorsInner.e544);
					wsaaMiscellaneousInner.wsaaRedisplay.set("Y");
					validation2040();
					return ;
					/******         GO TO 2040-CHECK-FOR-ERRORS                 <D509CS>*/
				}
				if (isNE(chdrrgpIO.getValidflag(), "1")) {
					sv.chdrselErr.set(errorsInner.e704);
					wsaaMiscellaneousInner.wsaaRedisplay.set("Y");
					validation2040();
					return ;
					/******         GO TO 2040-CHECK-FOR-ERRORS                 <D509CS>*/
				}
				/* Contract is valid so obtain the contract type and description.*/
				descIO.setDataKey(SPACES);
				descIO.setDescpfx("IT");
				descIO.setDesccoy(wsspcomn.company);
				descIO.setDesctabl(t5688);
				descIO.setDescitem(chdrrgpIO.getCnttype());
				descIO.setLanguage(wsspcomn.language);
				descIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, descIO);
				if ((isNE(descIO.getStatuz(), varcom.oK))
				&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
					syserrrec.params.set(descIO.getParams());
					fatalError600();
				}
				if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
					sv.ctypedes.set(SPACES);
				}
				else {
					sv.ctypedes.set(descIO.getLongdesc());
				}
				sv.cnttype.set(chdrrgpIO.getCnttype());
				/* Read in the details of the Life Assured.*/
				lifeIO.setParams(SPACES);
				lifeIO.setChdrcoy(wsspcomn.company);
				lifeIO.setChdrnum(chdrrgpIO.getChdrnum());
				lifeIO.setCurrfrom(ZERO);
				lifeIO.setFormat(formatsInner.liferec);
				wsaaMiscellaneousInner.wsaaFirstLifeRead.set("Y");
				while ( !(isEQ(lifeIO.getStatuz(), varcom.endp))) {
					getPolicyDetails2100();
				}
				
				scrnparams.subfileRrn.set(1);
			}
		}
		/*                                                         <D509CS>*/
		validation2040();
	}

	/**
	* <pre>
	*                                                         <D509CS>
	* </pre>
	*/
protected void validation2040()
	{
		checkForErrors2040();
		displayScreen2050();
		validateScreen2060();
	}

	/**
	* <pre>
	*************************                                 <D509CS>
	* </pre>
	*/
protected void checkForErrors2040()
	{
		/*                                                         <D509CS>*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*                                                         <D509CS>
	* </pre>
	*/
protected void displayScreen2050()
	{
		/*                                                         <D509CS>*/
		/* Set screen function                                     <D509CS>*/
		/*                                                         <D509CS>*/
		if ((isEQ(varcom.vrcmInit, " "))
		|| (isNE(wsaaMiscellaneousInner.wsaaSelection, SPACES))) {
			scrnparams.function.set(varcom.init);
			varcom.vrcmInit.set("Y");
			wsaaMiscellaneousInner.wsaaSelection.set(SPACES);
		}
		else {
			scrnparams.function.set(varcom.norml);
		}
		scrnparams.subfileRrn.set(1);
		processScreen("S6676", sv);
		screenErrors200();
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.subfileRrn.set(1);
	}

	/**
	* <pre>
	*                                                         <D509CS>
	* </pre>
	*/
protected void validateScreen2060()
	{
		/*                                                         <D509CS>*/
		/* Check if the contract number has changed, if so repeat p<D509CS>*/
		/*  for new number.                                        <D509CS>*/
		/*                                                         <D509CS>*/
		if ((isNE(sv.chdrsel, wsaaChdrnumStore))
		|| (isEQ(wsaaMiscellaneousInner.wsaaRedisplay, "Y"))) {
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*                                                         <D509CS>*/
		/*  Check if Batch Enquiry invoked                         <D509CS>*/
		/*                                                         <D509CS>*/
		if ((isEQ(scrnparams.statuz, "BACH"))) {
			processBatchRequest2800();
			scrnparams.subfileRrn.set(1);
			return ;
		}
		/*                                                         <D509CS>*/
		/*  Get the first changed subfile record                   <D509CS>*/
		/*                                                         <D509CS>*/
		if (isEQ(wsaaMiscellaneousInner.wsaaSelection, SPACES)) {
			scrnparams.function.set(varcom.srnch);
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		processScreen("S6676", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			scrnparams.subfileRrn.set(1);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*                                                         <D509CS>*/
		/* Validate any actions entered against subfile records    <D509CS>*/
		/*                                                         <D509CS>*/
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
		scrnparams.subfileRrn.set(1);
		wsaaMiscellaneousInner.wsaaFirstRecord.set("Y");
	}

protected void getPolicyDetails2100()
	{
		try {
			getClientNumber2110();
			getClientName2120();
			getCompPayDetails2130();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void getClientNumber2110()
	{
		if (isEQ(wsaaMiscellaneousInner.wsaaFirstLifeRead, "Y")) {
			lifeIO.setFunction(varcom.begn);
			wsaaMiscellaneousInner.wsaaFirstLifeRead.set("N");
		}
		else {
			lifeIO.setFunction(varcom.nextr);
		}
		SmartFileCode.execute(appVars, lifeIO);
		if ((isNE(lifeIO.getStatuz(), varcom.oK))
		&& (isNE(lifeIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if ((isEQ(lifeIO.getStatuz(), varcom.endp))
		|| (isNE(lifeIO.getChdrcoy(), wsspcomn.company))
		|| (isNE(lifeIO.getChdrnum(), chdrrgpIO.getChdrnum()))
		|| (isNE(lifeIO.getJlife(), "00"))
		|| (isNE(lifeIO.getValidflag(), "1"))) {
			lifeIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2100);
		}
	}

protected void getClientName2120()
	{
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifeIO.getLifcnum());
		cltsIO.setFormat(formatsInner.cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		/* Add record to subfile*/
		sv.subfileArea.set(SPACES);
		sv.life.set(lifeIO.getLife());
		sv.shortdesc.set(lifeIO.getLifcnum());
		sv.elemdesc.set(wsspcomn.longconfname);
		sv.actionOut[varcom.pr.toInt()].set("Y");
		sv.actionOut[varcom.nd.toInt()].set("Y");
		sv.coverageOut[varcom.nd.toInt()].set("Y");
		sv.riderOut[varcom.nd.toInt()].set("Y");
		addToSubfile2500();
	}

protected void getCompPayDetails2130()
	{
		/* Read in the details of the Plan Components and any relevant Paym*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(lifeIO.getChdrcoy());
		covrIO.setChdrnum(lifeIO.getChdrnum());
		covrIO.setLife(lifeIO.getLife());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		wsaaMiscellaneousInner.wsaaFirstCovrRead.set("Y");
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			getComponentDetails2300();
		}
		
	}

protected void getComponentDetails2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					getCovrDetails2310();
				case getRegPayment2310: 
					getRegPayment2310();
				case exit2300: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void getCovrDetails2310()
	{
		/* Search for relevant Coverages / Riders*/
		if (isEQ(wsaaMiscellaneousInner.wsaaFirstCovrRead, "Y")) {
			covrIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi 
			covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
			wsaaMiscellaneousInner.wsaaFirstCovrRead.set("N");
		}
		else {
			covrIO.setFunction(varcom.nextr);
		}
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(), varcom.oK))
		&& (isNE(covrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if ((isEQ(covrIO.getStatuz(), varcom.endp))
		|| (isNE(covrIO.getChdrcoy(), lifeIO.getChdrcoy()))
		|| (isNE(covrIO.getChdrnum(), lifeIO.getChdrnum()))
		|| (isNE(covrIO.getLife(), lifeIO.getLife()))) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2300);
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			goTo(GotoLabel.exit2300);
		}
		/*IF (COVR-STATUZ              = ENDP)         OR              */
		/*   (COVR-CHDRCOY         NOT = LIFE-CHDRCOY) OR              */
		/*   (COVR-CHDRNUM         NOT = LIFE-CHDRNUM) OR              */
		/*   (COVR-LIFE            NOT = LIFE-LIFE)    OR              */
		/*   (COVR-VALIDFLAG       NOT = '1')                          */
		/*   MOVE ENDP                TO COVR-STATUZ                   */
		/*   GO TO 2300-EXIT                                           */
		/*END-IF                                                       */
		/*  Check if the record is a broken out part of a component otherwi*/
		/*   set up indentation for display of component hierarchy*/
		if ((isEQ(covrIO.getLife(), sv.life))
		&& (isEQ(covrIO.getCoverage(), sv.coverage))
		&& (isEQ(covrIO.getRider(), sv.rider))) {
			goTo(GotoLabel.exit2300);
		}
		else {
			sv.subfileArea.set(SPACES);
			sv.life.set(covrIO.getLife());
			sv.rider.set(covrIO.getRider());
			sv.coverage.set(covrIO.getCoverage());
			sv.lifeOut[varcom.nd.toInt()].set("Y");
			if ((isEQ(covrIO.getRider(), ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
			}
			/*  Display the component name and obtain its description from T568*/
			descIO.setDataKey(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(t5687);
			descIO.setDescitem(covrIO.getCrtable());
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if ((isNE(descIO.getStatuz(), varcom.oK))
			&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			/* Add record to subfile*/
			if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
				sv.elemdesc.set(SPACES);
			}
			else {
				sv.elemdesc.set(descIO.getLongdesc());
			}
			sv.shortdesc.set(covrIO.getCrtable());
			sv.hcrtable.set(covrIO.getCrtable());
			addToSubfile2500();
			/* Get the Payment Details (if any)*/
			goTo(GotoLabel.getRegPayment2310);
		}
	}

	/**
	* <pre>
	* Read in the details of any Payments against the components
	* </pre>
	*/
protected void getRegPayment2310()
	{
		regpIO.setParams(SPACES);
		regpIO.setChdrcoy(covrIO.getChdrcoy());
		regpIO.setChdrnum(covrIO.getChdrnum());
		regpIO.setLife(covrIO.getLife());
		regpIO.setCoverage(covrIO.getCoverage());
		regpIO.setRider(covrIO.getRider());
		regpIO.setRgpynum(ZERO);
		regpIO.setFormat(formatsInner.regprec);
		wsaaMiscellaneousInner.wsaaFirstPaymRead.set("Y");
		while ( !(isEQ(regpIO.getStatuz(), varcom.endp))) {
			getPaymentDetails2400();
		}
		
	}

protected void getPaymentDetails2400()
	{
		readPaymentDetails2410();
	}

protected void readPaymentDetails2410()
	{
		if (isEQ(wsaaMiscellaneousInner.wsaaFirstPaymRead, "Y")) {
			regpIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi 
			regpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			regpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
			wsaaMiscellaneousInner.wsaaFirstPaymRead.set("N");
		}
		else {
			regpIO.setFunction(varcom.nextr);
		}
		SmartFileCode.execute(appVars, regpIO);
		if ((isNE(regpIO.getStatuz(), varcom.oK))
		&& (isNE(regpIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		if ((isEQ(regpIO.getStatuz(), varcom.endp))
		|| (isNE(regpIO.getChdrcoy(), covrIO.getChdrcoy()))
		|| (isNE(regpIO.getChdrnum(), covrIO.getChdrnum()))
		|| (isNE(regpIO.getLife(), covrIO.getLife()))
		|| (isNE(regpIO.getCoverage(), covrIO.getCoverage()))
		|| (isNE(regpIO.getRider(), covrIO.getRider()))) {
			regpIO.setStatuz(varcom.endp);
			return ;
		}
		/* Obtain the payment type description from T6691*/
		descIO.setDataArea(SPACES);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t6691);
		descIO.setDescitem(regpIO.getRgpytype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/* Format details for output to screen*/
		datcon1rec.intDate.set(regpIO.getFirstPaydate());
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaFirstPaydate.set(datcon1rec.extDate);
		if (isNE(regpIO.getFinalPaydate(), varcom.vrcmMaxDate)) {
			datcon1rec.intDate.set(regpIO.getFinalPaydate());
			datcon1rec.function.set("CONV");
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				fatalError600();
			}
			wsaaFinalPaydate.set(datcon1rec.extDate);
		}
		else {
			wsaaFinalPaydate.set(SPACES);
		}
		wsaaPaystat.set(regpIO.getRgpystat());
		wsaaCurrcode.set(regpIO.getCurrcd());
		if (isNE(regpIO.getPymt(), ZERO)) {
			/*MOVE REGP-CURRCD         TO WSAA-CURRCODE                 */
			wsaaAmtPrcntVal.set(regpIO.getPymt());
			wsaaPercentSign.set(SPACES);
		}
		else {
			/*MOVE SPACES              TO WSAA-CURRCODE                 */
			wsaaAmtPrcntVal.set(regpIO.getPrcnt());
			wsaaPercentSign.set(" %");
		}
		/* Add record to subfile*/
		sv.subfileArea.set(SPACES);
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.shortdesc.set(SPACES);
		}
		else {
			sv.shortdesc.set(descIO.getShortdesc());
		}
		sv.life.set(regpIO.getLife());
		sv.coverage.set(regpIO.getCoverage());
		sv.rider.set(regpIO.getRider());
		sv.lifeOut[varcom.nd.toInt()].set("Y");
		sv.coverageOut[varcom.nd.toInt()].set("Y");
		sv.riderOut[varcom.nd.toInt()].set("Y");
		sv.hcrtable.set(covrIO.getCrtable());
		sv.hrgpynum.set(regpIO.getRgpynum());
		sv.elemdesc.set(wsaaPaymentDetails);
		addToSubfile2500();
	}

protected void addToSubfile2500()
	{
		/*ADD-LINE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6676", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case updateErrorIndicators2670: 
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (isEQ(sv.action, SPACES)) {
			sv.actionErr.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/*  Determine what type of record has been selected (C/R or Payment*/
		if ((isNE(sv.hrgpynum, SPACES))) {
			itdmIO.setDataArea(SPACES);
			wsaaPaymentDetails.set(sv.elemdesc);
			wsaaT6693Paystat.set(wsaaPaystat);
			wsaaT6693Crtable.set(sv.hcrtable);
			readT6693Table2700();
			if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
				itdmIO.setDataArea(SPACES);
				wsaaT6693Crtable.set("****");
				readT6693Table2700();
			}
		}
		else {
			itdmIO.setDataArea(SPACES);
			wsaaT6693Crtable.set(sv.hcrtable);
			wsaaT6693Paystat.set("**");
			readT6693Table2700();
		}
		/* If no item found, then there is no allowable transaction*/
		if (isNE(itdmIO.getStatuz(), varcom.endp)) {
			t6693rec.t6693Rec.set(itdmIO.getGenarea());
		}
		else {
			sv.actionErr.set(errorsInner.e005);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Call SUBPROG with the entered action to obtain the transaction c*/
		subprogrec.action.set(sv.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/*  Read through array of allowable transactions to determine if*/
		/*   transaction is valid*/
		wsaaSub.set(1);
		while ( !((isGT(wsaaSub, 12))
		|| (isEQ(subprogrec.transcd, t6693rec.trcode[wsaaSub.toInt()])))) {
			if (isNE(subprogrec.transcd, t6693rec.trcode[wsaaSub.toInt()])) {
				wsaaSub.add(1);
			}
		}
		
		if (isGT(wsaaSub, 12)) {
			sv.actionErr.set(errorsInner.e005);
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.hrgpynum, SPACES)) {
			if ((isEQ(sv.rider, ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
				sv.coverageOut[varcom.nd.toInt()].set(" ");
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
				sv.riderOut[varcom.nd.toInt()].set(" ");
			}
		}
		else {
			sv.coverageOut[varcom.nd.toInt()].set("Y");
			sv.riderOut[varcom.nd.toInt()].set("Y");
			sv.lifeOut[varcom.nd.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6676", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		if (isEQ(wsaaMiscellaneousInner.wsaaSelection, SPACES)) {
			scrnparams.function.set(varcom.srnch);
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		processScreen("S6676", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	* </pre>
	*/
protected void readT6693Table2700()
	{
		readTable2710();
	}

protected void readTable2710()
	{
		/*  Read T6693 to check if the selected record has a valid action*/
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6693);
		itdmIO.setItemitem(wsaaT6693Key);
		itdmIO.setItmfrm(wsaaMiscellaneousInner.wsaaEffdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), t6693))
		|| (isNE(itdmIO.getItemitem(), wsaaT6693Key))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setItemitem(wsaaT6693Key);
		}
	}

protected void processBatchRequest2800()
	{
		checkForSelectedRecord2810();
	}

protected void checkForSelectedRecord2810()
	{
		/* Get the first selected record on the subfile*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("S6676", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !((isEQ(scrnparams.statuz, varcom.endp))
		|| (isNE(sv.action, SPACES)))) {
			scrnparams.function.set(varcom.srdn);
			processScreen("S6676", sv);
			if ((isNE(scrnparams.statuz, varcom.oK))
			&& (isNE(scrnparams.statuz, varcom.endp))) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			scrnparams.subfileRrn.set(1);
			scrnparams.errorCode.set(errorsInner.h127);
			wsspcomn.edterror.set("Y");
			return ;
		}
		else {
			scrnparams.statuz.set("BACH");
		}
		/* Validate action and check if Batch Enquiry is allowed*/
		subprogrec.action.set(sv.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			wsspcomn.edterror.set("Y");
			batchScreenUpdate2900();
			return ;
		}
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(errorsInner.e073);
			wsspcomn.edterror.set("Y");
			batchScreenUpdate2900();
			return ;
		}
		/*  Call BCBPROG to obtain programs for processing batch request.*/
		sv.action.set(" ");
		batchScreenUpdate2900();
		scrnparams.statuz.set("BACH");
		wsaaMiscellaneousInner.wsaaSelection.set("B");
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			wsspcomn.edterror.set("Y");
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void batchScreenUpdate2900()
	{
		update2910();
	}

protected void update2910()
	{
		if (isEQ(sv.hrgpynum, SPACES)) {
			if ((isEQ(sv.rider, ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
				sv.coverageOut[varcom.nd.toInt()].set(" ");
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
				sv.riderOut[varcom.nd.toInt()].set(" ");
			}
		}
		else {
			sv.coverageOut[varcom.nd.toInt()].set("Y");
			sv.riderOut[varcom.nd.toInt()].set("Y");
			sv.lifeOut[varcom.nd.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6676", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					processSelections3010();
					findRecord3020();
				case checkActionsSanctions3030: 
					checkActionsSanctions3030();
					setActionFlag3040();
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void processSelections3010()
	{
		/*  If Batch Enquiry invoked skip this section.*/
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		/*  Store the CHDRRGP record in the I/O module using KEEPS, if no e*/
		/*   have been detected*/
		chdrrgpIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		/* If first time reposition the subfile to the beginning*/
		if ((isEQ(wsaaMiscellaneousInner.wsaaSelection, SPACES))
		&& (isEQ(wsaaMiscellaneousInner.wsaaFirstRecord, "Y"))) {
			scrnparams.function.set(varcom.sstrt);
			converseWithScreen3200();
		}
		scrnparams.function.set(varcom.srdn);
		while ( !((isNE(sv.action, SPACES))
		|| (isEQ(scrnparams.statuz, varcom.endp)))) {
			converseWithScreen3200();
		}
		
	}

protected void findRecord3020()
	{
		if ((isEQ(scrnparams.statuz, varcom.endp))) {
			wsspcomn.edterror.set("Y");
			wsaaMiscellaneousInner.wsaaRedisplay.set("Y");
			goTo(GotoLabel.exit3090);
		}
		/* Softlock the contract*/
		if (isEQ(sv.action, "6")) {
			goTo(GotoLabel.checkActionsSanctions3030);
		}
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(sv.chdrsel);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if ((isNE(sftlockrec.statuz, varcom.oK))
		&& (isNE(sftlockrec.statuz, "LOCK"))) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			if (isNE(wsaaMiscellaneousInner.wsaaFirstRecord, "Y")) {
				sv.actionErr.set(errorsInner.h128);
				wsspcomn.edterror.set("Y");
				scrnparams.function.set(varcom.supd);
				converseWithScreen3200();
				scrnparams.subfileRrn.set(1);
				goTo(GotoLabel.exit3090);
			}
			else {
				sv.actionErr.set(errorsInner.f910);
				wsspcomn.edterror.set("Y");
				scrnparams.function.set(varcom.supd);
				converseWithScreen3200();
				scrnparams.subfileRrn.set(1);
				goTo(GotoLabel.exit3090);
			}
		}
	}

protected void checkActionsSanctions3030()
	{
		/* Obtain correct action and check sanctions*/
		wsaaMiscellaneousInner.wsaaFirstRecord.set("N");
		subprogrec.action.set(sv.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			wsspcomn.edterror.set("Y");
			scrnparams.function.set(varcom.supd);
			converseWithScreen3200();
			scrnparams.subfileRrn.set(1);
			if (isNE(sv.action, "6")) {
				unlockContract3300();
			}
			goTo(GotoLabel.exit3090);
		}
		/* Check if sanctioned to actions*/
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			wsspcomn.edterror.set("Y");
			scrnparams.function.set(varcom.supd);
			converseWithScreen3200();
			scrnparams.subfileRrn.set(1);
			if (isNE(sv.action, "6")) {
				unlockContract3300();
			}
			goTo(GotoLabel.exit3090);
		}
		/* Check contract status against T5679*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatuz.set("N");
		/*MOVE 1                      TO WSAA-SUB.                     */
		wsaaStatcode.set(chdrrgpIO.getStatcode());
		wsaaPstcde.set(chdrrgpIO.getPstatcode());
		/*PERFORM UNTIL WSAA-SUB       > 12                            */
		/*   IF T5679-CN-RISK-STAT(WSAA-SUB) NOT = SPACE               */
		/*      IF (T5679-CN-RISK-STAT(WSAA-SUB) = WSAA-STATCODE) AND  */
		/*         (T5679-CN-PREM-STAT(WSAA-SUB) = WSAA-PSTCDE)        */
		/*          MOVE 13           TO WSAA-SUB                      */
		/*          MOVE 'Y'          TO WSAA-VALID-STATUZ             */
		/*      END-IF                                                 */
		/*   END-IF                                                    */
		/*   ADD 1                    TO WSAA-SUB                      */
		/*END-PERFORM.                                                 */
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
				for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
				|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()], wsaaPstcde)) {
						wsaaValidStatuz.set("Y");
					}
				}
			}
		}
		if (isEQ(wsaaValidStatuz, "N")) {
			sv.actionErr.set(errorsInner.h137);
			wsspcomn.edterror.set("Y");
			if (isNE(sv.action, "6")) {
				unlockContract3300();
			}
			scrnparams.function.set(varcom.supd);
			converseWithScreen3200();
			scrnparams.subfileRrn.set(1);
			goTo(GotoLabel.exit3090);
		}
		/* Set up the batchkey for the subsequent programs to be processed*/
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
	}

protected void setActionFlag3040()
	{
		/* Set the WSSP-FLAG according to the transaction to be invoked*/
		if (isEQ(sv.action, "1")) {
			wsspcomn.flag.set("C");
			createFollowUps3400();
		}
		if ((isEQ(sv.action, "2"))) {
			wsspcomn.flag.set("M");
		}
		if ((isEQ(sv.action, "3"))
		|| (isEQ(sv.action, "6"))) {
			wsspcomn.flag.set("I");
		}
		if ((isEQ(sv.action, "4"))
		|| (isEQ(sv.action, "5"))) {
			wsspcomn.flag.set("D");
		}
		/* Do a KEEPS on the key of a REGP record if one does not already e*/
		/*  otherwise do a READS.*/
		regpIO.setNonKey(SPACES);
		regpIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		regpIO.setChdrnum(chdrrgpIO.getChdrnum());
		regpIO.setLife(sv.life);
		regpIO.setCoverage(sv.coverage);
		regpIO.setRider(sv.rider);
		regpIO.setRgpynum(ZERO);
		regpIO.setFormat(formatsInner.regprec);
		if (isEQ(sv.hrgpynum, SPACES)) {
			regpIO.setCrtable(sv.hcrtable);
			regpIO.setPrcnt(ZERO);
			regpIO.setPymt(ZERO);
			regpIO.setTotamnt(ZERO);
			regpIO.setPlanSuffix(ZERO);
			regpIO.setTranno(ZERO);
			regpIO.setCrtdate(ZERO);
			regpIO.setAprvdate(ZERO);
			regpIO.setFirstPaydate(ZERO);
			regpIO.setNextPaydate(ZERO);
			regpIO.setRevdte(ZERO);
			regpIO.setLastPaydate(ZERO);
			regpIO.setFinalPaydate(ZERO);
			regpIO.setAnvdate(ZERO);
			regpIO.setCancelDate(ZERO);
			regpIO.setValidflag("1");
			regpIO.setFunction(varcom.keeps);
			regpIO.setUser(varcom.vrcmUser);
			regpIO.setTransactionTime(varcom.vrcmTime);
			regpIO.setTransactionDate(wsaaMiscellaneousInner.wsaaEffdate);
			regpIO.setTermid(varcom.vrcmTermid);
		}
		else {
			regpIO.setRgpynum(sv.hrgpynum);
			regpIO.setFunction(varcom.reads);
		}
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/*  Set the WSAA-SELECTION field to the current selection value to*/
		/*   indicate that a selection is to be processed; then blank out*/
		/*   the screen field and rewrite the subfile record.*/
		wsaaMiscellaneousInner.wsaaSelection.set(sv.action);
		sv.action.set(" ");
		scrnparams.function.set(varcom.supd);
		converseWithScreen3200();
	}

protected void batching3080()
	{
		if ((isEQ(subprogrec.bchrqd, "Y"))
		&& (isEQ(sv.errorIndicators, SPACES))) {
			batcdorrec.function.set("AUTO");
			batcdorrec.tranid.set(wsspcomn.tranid);
			batcdorrec.batchkey.set(wsspcomn.batchkey);
			callProgram(Batcdor.class, batcdorrec.batcdorRec);
			if (isNE(batcdorrec.statuz, varcom.oK)) {
				sv.actionErr.set(batcdorrec.statuz);
			}
			wsspcomn.batchkey.set(batcdorrec.batchkey);
		}
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	* </pre>
	*/
protected void converseWithScreen3200()
	{
		getSelectedRecord3210();
	}

protected void getSelectedRecord3210()
	{
		if (isEQ(sv.hrgpynum, SPACES)) {
			if ((isEQ(sv.rider, ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
				sv.coverageOut[varcom.nd.toInt()].set(" ");
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
				sv.riderOut[varcom.nd.toInt()].set(" ");
			}
		}
		else {
			sv.coverageOut[varcom.nd.toInt()].set("Y");
			sv.riderOut[varcom.nd.toInt()].set("Y");
			sv.lifeOut[varcom.nd.toInt()].set("Y");
		}
		processScreen("S6676", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void unlockContract3300()
	{
		unlock3310();
	}

protected void unlock3310()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrrgpIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void createFollowUps3400()
	{
		try {
			chekIfRequired3410();
			readDefaultsTable3420();
			writeFollowUps3430();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void chekIfRequired3410()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5688);
		/* CHDRRGP has already been read before this.                      */
		itdmIO.setItemitem(chdrrgpIO.getCnttype());
		itdmIO.setItmfrm(wsaaMiscellaneousInner.wsaaEffdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrrgpIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrrgpIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.e308);
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		if (isEQ(t5688rec.defFupMeth, SPACES)) {
			goTo(GotoLabel.exit3490);
		}
	}

protected void readDefaultsTable3420()
	{
		wsaaT5677Tranno.set(wsaaBatchkey.batcBatctrcde);
		wsaaT5677FollowUp.set(t5688rec.defFupMeth);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5677);
		itemIO.setItemitem(wsaaT5677Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5677rec.t5677Rec.set(itemIO.getGenarea());
	}

protected void writeFollowUps3430()
	{
		wsaaFupno.set(0);
		for (wsaaX.set(1); !(isGT(wsaaX, 16)); wsaaX.add(1)){
			writeFollowUp3500();
		}
	}

protected void writeFollowUp3500()
	{
		try {
			lookUpStatus3510();
			lookUpDescription3520();
			writeRecord3530();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void lookUpStatus3510()
	{
		/*    IF T5677-FUPCODE (WSAA-X)    = SPACES                <V72L11>*/
		if (isEQ(t5677rec.fupcdes[wsaaX.toInt()], SPACES)) {
			goTo(GotoLabel.exit3590);
			/*    END-IF.                                                      */
			/* UNREACHABLE CODE
			itemIO.setDataKey(SPACES);
			 */
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5661);
		/*    MOVE T5677-FUPCODE (WSAA-X) TO ITEM-ITEMITEM.        <V72L11>*/
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(t5677rec.fupcdes[wsaaX.toInt()]);
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		fluprgpIO.setFupstat(t5661rec.fupstat);
	}

protected void lookUpDescription3520()
	{
		descIO.setDataKey(SPACES);
		descIO.setNonKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5661);
		/*    MOVE T5677-FUPCODE (WSAA-X) TO DESC-DESCITEM.        <V72L11>*/
		descIO.setDescitem(t5677rec.fupcdes[wsaaX.toInt()]);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void writeRecord3530()
	{
		fluprgpIO.setParams(SPACES);
		fluprgpIO.setChdrcoy(wsspcomn.company);
		fluprgpIO.setChdrnum(chdrrgpIO.getChdrnum());
		/* Determine WHAT the highest claim number was on this contract    */
		/* then add 1 to it to get the next CLAMNUM. This needs only to    */
		/* be done for the first time through. Subsequent times will       */
		/* keep the same Claim Number.                                     */
		if (isEQ(wsaaFupno, 0)) {
			begnRegp3600();
		}
		fluprgpIO.setClamnum(wsaaNextClamnum);
		wsaaFupno.add(1);
		fluprgpIO.setFupno(wsaaFupno);
		fluprgpIO.setTranno(chdrrgpIO.getTranno());
		fluprgpIO.setFupremdt(datcon1rec.intDate);
		/*    MOVE T5677-FUPCODE (WSAA-X) TO FLUPRGP-FUPCODE.      <V72L11>*/
		fluprgpIO.setFupcode(t5677rec.fupcdes[wsaaX.toInt()]);
		fluprgpIO.setFuptype("P");
		fluprgpIO.setFupstat(t5661rec.fupstat);
		fluprgpIO.setFupremk(descIO.getLongdesc());
		fluprgpIO.setLife("01");
		fluprgpIO.setJlife("00");
		fluprgpIO.setFormat(formatsInner.fluprgprec);
		fluprgpIO.setTermid(varcom.vrcmTermid);
		fluprgpIO.setTransactionDate(varcom.vrcmDate);
		fluprgpIO.setTransactionTime(varcom.vrcmTime);
		fluprgpIO.setUser(varcom.vrcmUser);
		fluprgpIO.setEffdate(datcon1rec.intDate);
		fluprgpIO.setCrtdate(datcon1rec.intDate);
		fluprgpIO.setFuprcvd(varcom.vrcmMaxDate);
		fluprgpIO.setExprdate(varcom.vrcmMaxDate);
		fluprgpIO.setZlstupdt(varcom.vrcmMaxDate);
		fluprgpIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fluprgpIO);
		if (isNE(fluprgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
	}

protected void begnRegp3600()
	{
		find3610();
	}

protected void find3610()
	{
		wsaaNextClamnum.set(ZERO);
		regpenqIO.setParams(SPACES);
		regpenqIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		regpenqIO.setChdrnum(chdrrgpIO.getChdrnum());
		regpenqIO.setLife(ZERO);
		regpenqIO.setCoverage(ZERO);
		regpenqIO.setRider(ZERO);
		regpenqIO.setRgpynum(99999);
		regpenqIO.setPlanSuffix(ZERO);
		regpenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		regpenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		while ( !(isEQ(regpenqIO.getStatuz(), varcom.endp))) {
			findNextRegp3700();
		}
		
		compute(wsaaNextClamnum, 0).set(add(wsaaNextClamnum, 1));
	}

protected void findNextRegp3700()
	{
		find3710();
	}

protected void find3710()
	{
		SmartFileCode.execute(appVars, regpenqIO);
		if ((isNE(regpenqIO.getStatuz(), varcom.oK))
		&& (isNE(regpenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(regpenqIO.getParams());
			fatalError600();
		}
		if (isEQ(regpenqIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if ((isEQ(chdrrgpIO.getChdrcoy(), regpenqIO.getChdrcoy()))
		&& (isEQ(chdrrgpIO.getChdrnum(), regpenqIO.getChdrnum()))) {
			if (isGT(regpenqIO.getRgpynum(), wsaaNextClamnum)) {
				wsaaNextClamnum.set(regpenqIO.getRgpynum());
			}
		}
		else {
			regpenqIO.setStatuz(varcom.endp);
		}
		regpenqIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		/* Check if batch enquiry invoked*/
		if (isEQ(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(ZERO);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
			return ;
		}
		/* Read T5671 to obtain the appropriate programs for processing the*/
		/*  selected record*/
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Transcd.set(subprogrec.transcd);
		wsaaT5671Crtable.set(sv.hcrtable);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* If the item is there load in the programs to the program stack.*/
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsspcomn.secProg[1].set(t5671rec.pgm01);
		wsspcomn.secProg[2].set(t5671rec.pgm02);
		wsspcomn.secProg[3].set(t5671rec.pgm03);
		wsspcomn.secProg[4].set(t5671rec.pgm04);
		wsspcomn.programPtr.set(1);
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e005 = new FixedLengthStringData(4).init("E005");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e308 = new FixedLengthStringData(4).init("E308");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData e704 = new FixedLengthStringData(4).init("E704");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData h127 = new FixedLengthStringData(4).init("H127");
	private FixedLengthStringData h128 = new FixedLengthStringData(4).init("H128");
	private FixedLengthStringData h137 = new FixedLengthStringData(4).init("H137");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
public static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrrgprec = new FixedLengthStringData(10).init("CHDRRGPREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
	private FixedLengthStringData regprec = new FixedLengthStringData(10).init("REGPREC");
	public FixedLengthStringData fluprgprec = new FixedLengthStringData(10).init("FLUPRGPREC");
}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 
		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaFirstLifeRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFirstCovrRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFirstPaymRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSelection = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaRedisplay = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaCompare = new FixedLengthStringData(2);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaCompare, 0, FILLER).init("0");
	private FixedLengthStringData wsaaFirstRecord = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0);
}
}
