/*
 * File: Dccan4.java
 * Date: 29 August 2009 22:45:33
 * Author: Quipoz Limited
 * 
 * Class transformed from DCCAN4.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* DCCAN4
* ======
* OVERVIEW
* ~~~~~~~~
* This is a new subroutine which forms part of the 9405
* Annuities Development.  It is the subroutine which will
* calculate the amount payable on a death claim of a benefit
* paying annuity.
*
* It will be called during the on-line registration of a death
* claim from T6598, which is read using the Death Claim method
* from T5687 for the component.
*
* This subroutine performs the following calculations:
*
* If within Guraranteed Period
* ============================
* i.e. The effective date of the death claim is earlier
* than the review date on the Regular Payment records.
*
*      If 'Return of Purchase Price' on ANNY is Yes (non-blank)
*      ========================================================
*      N.B. This is only applicable to Immediate Annuities.
*      The Purchase Price is the SINGP from the COVR.
*
*      Obtain the value of payments made to date from the REGP
*      TOTAMNT.
*
*      Calculate the value of remaining payments due between the
*      Effective Date and the end on the Guaranteed Period (i.e.
*      the Review Date on the REGP record)
*
*      Subtract Payments made to date (REGP-TOTAMNT) from the
*      Purchase Price (COVR-SINGP).
*
*           If this figure is less than or equal to the value of
*           the remaining payments due, the death value will be
*           zero, the Final Payment Date will be set to the
*           end of the Guaranteed Period (i.e. the Review Date)
*           and the Regular Payments will continue.
*
*           If this figure is greater than the value of the
*           remaining payments due the death claim value will be
*           the Purchase price less the Payments made to date
*           and the Regular Payment records will be terminated.
*
*      If 'Return of Purchase Price' on ANNY is No (blank)
*      ===================================================
*      Set Final Payment Date to the end of the Guaranteed
*      Period (i.e. the Review Date)
*      Death Value = 0
*
* If outside Guaranteed Period OR If no Guaranteed Period
* =======================================================
* i.e. Effective date is greater than or equal to the
* Review Date OR the Guaranteed Period field is blank.
*
*      If 'Return of Purchase Price' on ANNY is Yes (non-blank)
*      ========================================================
*      Obtain Purchase Price from COVR-SINGP.
*      Obtain total payments made to date from REGP-TOTAMNT.
*
*      If the Purchase Price less the Total payments made to
*      date is greater than zero, this is the death value and
*      the regular payment records will be terminated.
*
*      If the Purchase Price less the Total payments made to
*      date is less than or equal to zero, the death value is
*      zero and the regular payment records will be terminated.
*
*      If 'Return of Purchase Price' on ANNY is No (blank)
*      ===================================================
*
*           If 'with proportion' on ANNY is Yes (non-blank)
*           ===============================================
*           Calculate the number of days between the Date of
*           Death and the last Payment Date, from REGP.
*
*           Calculate this number of days as a proportion of the
*           number of days between Last Payment Date and Next
*           Payment Date, both from the REGP.
*
*           The Death Value is the payment amount multiplied by
*           the proportion of days figure calculated above.
*           The regular payment records are terminated.
*
*           If 'With proportion' on ANNY is No (blank)
*           ==========================================
*           Death value = 0
*           Regular payment records are terminated.
* ____________________________________________________________
*
*****************************************************************
* </pre>
*/
public class Dccan4 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "DCCAN4";

	private FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaSwitch, " ");
		/* WSAA-PLNSFX */
	private ZonedDecimalData wsaaOrigPlnsfx = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaRedfPlansfx = new FixedLengthStringData(4).isAPartOf(wsaaOrigPlnsfx, 0, REDEFINE);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(2, 0).isAPartOf(wsaaRedfPlansfx, 2).setUnsigned();
	private PackedDecimalData wsaaRunningTotal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaDval = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotamnt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRempay1 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotrempay = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaNodays1 = new PackedDecimalData(7, 0).setUnsigned();
	private PackedDecimalData wsaaNodays2 = new PackedDecimalData(7, 0).setUnsigned();
	private PackedDecimalData wsaaProp = new PackedDecimalData(5, 2).setUnsigned();
		/* FORMATS */
	private String annyrec = "ANNYREC";
	private String covrrec = "COVRREC";
	private String descrec = "DESCREC";
	private String regpclmrec = "REGPCLMREC";
		/* TABLES */
	private String t5687 = "T5687";
		/*Annuity Details*/
	private AnnyTableDAM annyIO = new AnnyTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Deathrec deathrec = new Deathrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Regular Payments File*/
	private RegpclmTableDAM regpclmIO = new RegpclmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit1090, 
		exit2090, 
		exit3290, 
		exit3229, 
		exit3239, 
		exit602
	}

	public Dccan4() {
		super();
	}

public void mainline(Object... parmArray)
	{
		deathrec.deathRec = convertAndSetParam(deathrec.deathRec, parmArray, 0);
		try {
			init000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void init000()
	{
		try {
			para010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void para010()
	{
		deathrec.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		deathrec.estimatedVal.set(ZERO);
		deathrec.actualVal.set(ZERO);
		deathrec.element.set(SPACES);
		deathrec.description.set(SPACES);
		wsaaDval.set(ZERO);
		wsaaTotamnt.set(ZERO);
		wsaaRempay1.set(ZERO);
		wsaaTotrempay.set(ZERO);
		wsaaNodays1.set(ZERO);
		wsaaNodays2.set(ZERO);
		wsaaProp.set(ZERO);
		if (firstTime.isTrue()) {
			begnCovr1000();
		}
		else {
			nextrCovr2000();
		}
		wsaaSwitch.set("N");
		if (isEQ(covrIO.getStatuz(),varcom.endp)) {
			deathrec.status.set(varcom.endp);
			wsaaSwitch.set(SPACES);
			goTo(GotoLabel.exit090);
		}
		deathrec.processInd.set(SPACES);
		startSubr3000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void begnCovr1000()
	{
		try {
			para1010();
		}
		catch (GOTOException e){
		}
	}

protected void para1010()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(deathrec.chdrChdrcoy);
		covrIO.setChdrnum(deathrec.chdrChdrnum);
		covrIO.setLife(deathrec.lifeLife);
		covrIO.setCoverage(deathrec.covrCoverage);
		covrIO.setRider(deathrec.covrRider);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		

		//performance improvement --  atiwari23 
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if ((isNE(covrIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(covrIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(covrIO.getLife(),deathrec.lifeLife)
		|| isNE(covrIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(covrIO.getRider(),deathrec.covrRider)
		|| isEQ(covrIO.getStatuz(),varcom.endp))) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1090);
		}
		deathrec.fieldType.set("S");//ILIFE-4885		
	}

protected void nextrCovr2000()
	{
		try {
			para2010();
		}
		catch (GOTOException e){
		}
	}

protected void para2010()
	{
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if ((isNE(covrIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(covrIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(covrIO.getLife(),deathrec.lifeLife)
		|| isNE(covrIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(covrIO.getRider(),deathrec.covrRider)
		|| isEQ(covrIO.getStatuz(),varcom.endp))) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
	}

protected void startSubr3000()
	{
		para3010();
	}

protected void para3010()
	{
		descIO.setDescitem(deathrec.crtable);
		descIO.setDesctabl(t5687);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(deathrec.chdrChdrcoy);
		descIO.setLanguage(deathrec.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			deathrec.description.set(descIO.getShortdesc());
		}
		else {
			deathrec.description.fill("?");
		}
		readrAnny3100();
		if (isEQ(annyIO.getGuarperd(),ZERO)) {
			deathrec.processInd.set("A");
		}
		else {
			accumulate3200();
			if (isGT(regpclmIO.getRevdte(),deathrec.effdate)) {
				deathrec.processInd.set("B");
			}
			else {
				deathrec.processInd.set("C");
			}
		}
		if (isEQ(deathrec.processInd,"B")) {
			if (isNE(annyIO.getPpind(),SPACES)) {
				withinGuarperdP13300();
			}
			else {
				withinGuarperdP23400();
			}
		}
		else {
			if ((isEQ(deathrec.processInd,"A")
			|| isEQ(deathrec.processInd,"C"))) {
				outsideGuarperd3500();
			}
		}
		deathrec.chdrChdrcoy.set(covrIO.getChdrcoy());
		deathrec.chdrChdrnum.set(covrIO.getChdrcoy());
		deathrec.lifeLife.set(covrIO.getLife());
		deathrec.covrCoverage.set(covrIO.getCoverage());
		deathrec.covrRider.set(covrIO.getRider());
		wsaaOrigPlnsfx.set(covrIO.getPlanSuffix());
		deathrec.element.set(wsaaPlanSuffix);
	}

protected void withinGuarperdP13300()
	{
		/*PARA*/
		compute(wsaaDval, 3).setRounded((sub(covrIO.getSingp(),wsaaTotamnt)));
		if ((isLTE(wsaaDval,wsaaTotrempay))) {
			deathrec.actualVal.set(ZERO);
			deathrec.processInd.set("D");
		}
		else {
			deathrec.actualVal.set(wsaaDval);
			deathrec.processInd.set("E");
		}
		/*EXIT*/
	}

protected void withinGuarperdP23400()
	{
		/*PARA*/
		deathrec.actualVal.set(ZERO);
		deathrec.processInd.set("D");
		/*EXIT*/
	}

protected void outsideGuarperd3500()
	{
		para3510();
	}

protected void para3510()
	{
		if (isNE(annyIO.getPpind(),SPACES)) {
			if(isEQ(deathrec.processInd,'C'))
			{
				wsaaTotamnt.set(ZERO);
				wsaaTotrempay.set(wsaaTotamnt);
			}
			accumulate3200();
			compute(wsaaDval, 3).setRounded((sub(covrIO.getSingp(),wsaaTotamnt)));
			if (isGT(wsaaDval,0)) {
				deathrec.actualVal.set(wsaaDval);
			}
			else {
				deathrec.actualVal.set(0);
			}
		}
		else {
			if (isNE(annyIO.getWithprop(),SPACES)) {
				deathrec.actualVal.set(ZERO);
				wsaaRunningTotal.set(ZERO);
				calculateProportion3520();
			}
			else {
				deathrec.actualVal.set(ZERO);
			}
		}
		deathrec.processInd.set("E");
	}

protected void readrAnny3100()
	{
		para3110();
	}

protected void para3110()
	{
		annyIO.setChdrcoy(deathrec.chdrChdrcoy);
		annyIO.setChdrnum(deathrec.chdrChdrnum);
		annyIO.setLife(deathrec.lifeLife);
		annyIO.setCoverage(deathrec.covrCoverage);
		annyIO.setRider(deathrec.covrRider);
		annyIO.setPlanSuffix(covrIO.getPlanSuffix());
		annyIO.setFormat(annyrec);
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
	}

protected void accumulate3200()
	{
		try {
			para3210();
		}
		catch (GOTOException e){
		}
	}

protected void para3210()
	{
		regpclmIO.setChdrcoy(deathrec.chdrChdrcoy);
		regpclmIO.setChdrnum(deathrec.chdrChdrnum);
		regpclmIO.setLife(deathrec.lifeLife);
		regpclmIO.setCoverage(deathrec.covrCoverage);
		regpclmIO.setRider(deathrec.covrRider);
		regpclmIO.setPlanSuffix(covrIO.getPlanSuffix());
		regpclmIO.setRgpynum(ZERO);
		regpclmIO.setFormat(regpclmrec);
		regpclmIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		regpclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");



		SmartFileCode.execute(appVars, regpclmIO);
		if ((isNE(regpclmIO.getStatuz(),varcom.oK)
		&& isNE(regpclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regpclmIO.getParams());
			fatalError600();
		}
		if ((isEQ(regpclmIO.getStatuz(),varcom.endp)
		|| isNE(regpclmIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(regpclmIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(regpclmIO.getLife(),deathrec.lifeLife)
		|| isNE(regpclmIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(regpclmIO.getRider(),deathrec.covrRider)
		|| isNE(regpclmIO.getPlanSuffix(),covrIO.getPlanSuffix()))) {
			regpclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3290);
		}
		if (isLTE(regpclmIO.getFirstPaydate() ,deathrec.effdate))
		{
			wsaaTotamnt.add(regpclmIO.getPymt());//ILIFE-4885
		}
		accumulateRempay3230();
		while ( !(isEQ(regpclmIO.getStatuz(),varcom.endp))) {
			nextrRegp3220();
		}
		
	}

protected void calculateProportion3520()
	{
		starts3520();
	}

protected void starts3520()
	{
		regpclmIO.setChdrcoy(deathrec.chdrChdrcoy);
		regpclmIO.setChdrnum(deathrec.chdrChdrnum);
		regpclmIO.setLife(deathrec.lifeLife);
		regpclmIO.setCoverage(deathrec.covrCoverage);
		regpclmIO.setRider(deathrec.covrRider);
		regpclmIO.setPlanSuffix(covrIO.getPlanSuffix());
		regpclmIO.setRgpynum(ZERO);
		regpclmIO.setFormat(regpclmrec);
		regpclmIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		regpclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");



		SmartFileCode.execute(appVars, regpclmIO);
		if ((isNE(regpclmIO.getStatuz(),varcom.oK)
		&& isNE(regpclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regpclmIO.getParams());
			fatalError600();
		}
		if ((isNE(regpclmIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(regpclmIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(regpclmIO.getLife(),deathrec.lifeLife)
		|| isNE(regpclmIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(regpclmIO.getRider(),deathrec.covrRider)
		|| isNE(regpclmIO.getPlanSuffix(),covrIO.getPlanSuffix()))) {
			regpclmIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(regpclmIO.getStatuz(),varcom.endp))) {
			checkRegp3530();
		}
		
	}

protected void checkRegp3530()
	{
		starts3530();
	}

protected void starts3530()
	{
		if ((isNE(regpclmIO.getLastPaydate(),varcom.vrcmMaxDate)
		&& isNE(regpclmIO.getNextPaydate(),varcom.vrcmMaxDate))) {
			calcNodaysProp3540();
		}
		compute(wsaaRunningTotal, 3).setRounded((mult(regpclmIO.getPymt(),wsaaProp)));
		deathrec.actualVal.add(wsaaRunningTotal);
		regpclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, regpclmIO);
		if ((isNE(regpclmIO.getStatuz(),varcom.oK)
		&& isNE(regpclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regpclmIO.getParams());
			fatalError600();
		}
		if ((isNE(regpclmIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(regpclmIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(regpclmIO.getLife(),deathrec.lifeLife)
		|| isNE(regpclmIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(regpclmIO.getRider(),deathrec.covrRider)
		|| isNE(regpclmIO.getPlanSuffix(),covrIO.getPlanSuffix())
		|| isEQ(regpclmIO.getStatuz(),varcom.endp))) //ILIFE-4885
		{
			regpclmIO.setStatuz(varcom.endp);
		}
	}

protected void nextrRegp3220()
	{
		try {
			para3220();
		}
		catch (GOTOException e){
		}
	}

protected void para3220()
	{
		regpclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, regpclmIO);
		if ((isNE(regpclmIO.getStatuz(),varcom.oK)
		&& isNE(regpclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(regpclmIO.getParams());
			fatalError600();
		}
		if ((isNE(regpclmIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(regpclmIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(regpclmIO.getLife(),deathrec.lifeLife)
		|| isNE(regpclmIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(regpclmIO.getRider(),deathrec.covrRider)
		|| isNE(regpclmIO.getPlanSuffix(),covrIO.getPlanSuffix())
		|| isEQ(regpclmIO.getStatuz(),varcom.endp))) //ILIFE-4885
		{
			regpclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3229);
		}
		if (isLTE(regpclmIO.getFirstPaydate() ,deathrec.effdate)) 
		{
			wsaaTotamnt.add(regpclmIO.getPymt());//ILIFE-4885
		}
		accumulateRempay3230();
	}

protected void accumulateRempay3230()
	{
		try {
			para3230();
		}
		catch (GOTOException e){
		}
	}

protected void para3230()
	{
		if (isEQ(regpclmIO.getRevdte(),varcom.vrcmMaxDate)) {
			goTo(GotoLabel.exit3239);
		}
		datcon3rec.intDate1.set(deathrec.effdate);
		datcon3rec.intDate2.set(regpclmIO.getRevdte());
		datcon3rec.frequency.set(regpclmIO.getRegpayfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz,varcom.oK)) {
			compute(wsaaRempay1, 5).set((mult(datcon3rec.freqFactor,regpclmIO.getPymt())));
			wsaaTotrempay.add(wsaaRempay1);
		}
		else {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
	}

protected void calcNodaysProp3540()
	{
		para3540();
	}

protected void para3540()
	{
		datcon3rec.intDate2.set(regpclmIO.getLastPaydate());
		datcon3rec.intDate1.set(deathrec.effdate);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz,varcom.oK)) {
			wsaaNodays1.set(datcon3rec.freqFactor);
		}
		else {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		datcon3rec.intDate1.set(regpclmIO.getLastPaydate());
		datcon3rec.intDate2.set(regpclmIO.getNextPaydate());
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isEQ(datcon3rec.statuz,varcom.oK)) {
			wsaaNodays2.set(datcon3rec.freqFactor);
		}
		else {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		compute(wsaaProp, 3).setRounded((div(wsaaNodays1,wsaaNodays2)));
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error600();
				}
				case exit602: {
					exit602();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error600()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit602);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit602()
	{
		deathrec.status.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
