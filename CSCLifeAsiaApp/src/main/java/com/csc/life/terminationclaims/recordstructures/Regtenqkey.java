package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:47
 * Description:
 * Copybook name: REGTENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regtenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regtenqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData regtenqKey = new FixedLengthStringData(64).isAPartOf(regtenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData regtenqChdrcoy = new FixedLengthStringData(1).isAPartOf(regtenqKey, 0);
  	public FixedLengthStringData regtenqChdrnum = new FixedLengthStringData(8).isAPartOf(regtenqKey, 1);
  	public FixedLengthStringData regtenqLife = new FixedLengthStringData(2).isAPartOf(regtenqKey, 9);
  	public FixedLengthStringData regtenqCoverage = new FixedLengthStringData(2).isAPartOf(regtenqKey, 11);
  	public FixedLengthStringData regtenqRider = new FixedLengthStringData(2).isAPartOf(regtenqKey, 13);
  	public PackedDecimalData regtenqSeqnbr = new PackedDecimalData(3, 0).isAPartOf(regtenqKey, 15);
  	public PackedDecimalData regtenqRgpynum = new PackedDecimalData(5, 0).isAPartOf(regtenqKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(regtenqKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regtenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regtenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}