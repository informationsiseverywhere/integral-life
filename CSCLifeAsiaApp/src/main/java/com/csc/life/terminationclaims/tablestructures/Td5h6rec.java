package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Td5h6rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData td5h6Rec = new FixedLengthStringData(503);
  	public ZonedDecimalData fullsurauthlim = new ZonedDecimalData(10, 2).isAPartOf(td5h6Rec, 0).setUnsigned();
  	public FixedLengthStringData filler = new FixedLengthStringData(493).isAPartOf(td5h6Rec, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(td5h6Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			td5h6Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}