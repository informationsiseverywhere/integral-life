package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6691
 * @version 1.0 generated on 30/08/09 06:58
 * @author Quipoz
 */
public class S6691ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(770);
	public FixedLengthStringData dataFields = new FixedLengthStringData(338).isAPartOf(dataArea, 0);
	public ZonedDecimalData bonusDecDate = DD.bondecdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData bonusReserveValue = DD.bonresval.copyToZonedDecimal().isAPartOf(dataFields,8);
	public ZonedDecimalData bonusValue = DD.bonusvalue.copyToZonedDecimal().isAPartOf(dataFields,25);
	public ZonedDecimalData bonusValueSurrender = DD.bonvalsurr.copyToZonedDecimal().isAPartOf(dataFields,42);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,59);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData cntdesc = DD.cntdesc.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,108);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,111);
	public FixedLengthStringData cownum = DD.cownum.copy().isAPartOf(dataFields,113);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,121);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,129);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,176);
	public FixedLengthStringData letterPrintFlag = DD.letprtflag.copy().isAPartOf(dataFields,184);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,185);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,187);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,234);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,242);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,246);
	public FixedLengthStringData paycurr = DD.paycurr.copy().isAPartOf(dataFields,293);
	public ZonedDecimalData plansfx = DD.plansfx.copyToZonedDecimal().isAPartOf(dataFields,296);
	public FixedLengthStringData premStatDesc = DD.pstatdsc.copy().isAPartOf(dataFields,300);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,310);
	public ZonedDecimalData rcdate = DD.rcdate.copyToZonedDecimal().isAPartOf(dataFields,318);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,326);
	public FixedLengthStringData rstatdesc = DD.rstatdsc.copy().isAPartOf(dataFields,328);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 338);
	public FixedLengthStringData bondecdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bonresvalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bonusvalueErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bonvalsurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cntdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cownumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData letprtflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData paycurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData plansfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData pstatdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData rcdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData rstatdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(324).isAPartOf(dataArea, 446);
	public FixedLengthStringData[] bondecdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bonresvalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bonusvalueOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bonvalsurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cntdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cownumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] letprtflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] paycurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] plansfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] pstatdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] rcdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] rstatdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData bonusDecDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rcdateDisp = new FixedLengthStringData(10);

	public LongData S6691screenWritten = new LongData(0);
	public LongData S6691protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6691ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(plansfxOut,new String[] {null, null, "01","-01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bonvalsurrOut,new String[] {"02","04","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paycurrOut,new String[] {"03","04","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(letprtflagOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {cnttype, cntdesc, cownum, lifenum, jlife, chdrnum, rstatdesc, premStatDesc, rcdate, ownername, lifename, jlifename, ptdate, btdate, plansfx, cntcurr, bonusDecDate, bonusValue, life, coverage, rider, effdate, bonusValueSurrender, bonusReserveValue, paycurr, numpols, letterPrintFlag};
		screenOutFields = new BaseData[][] {cnttypeOut, cntdescOut, cownumOut, lifenumOut, jlifenumOut, chdrnumOut, rstatdscOut, pstatdscOut, rcdateOut, ownernameOut, lifenameOut, jlifenameOut, ptdateOut, btdateOut, plansfxOut, cntcurrOut, bondecdateOut, bonusvalueOut, lifeOut, coverageOut, riderOut, effdateOut, bonvalsurrOut, bonresvalOut, paycurrOut, numpolsOut, letprtflagOut};
		screenErrFields = new BaseData[] {cnttypeErr, cntdescErr, cownumErr, lifenumErr, jlifenumErr, chdrnumErr, rstatdscErr, pstatdscErr, rcdateErr, ownernameErr, lifenameErr, jlifenameErr, ptdateErr, btdateErr, plansfxErr, cntcurrErr, bondecdateErr, bonusvalueErr, lifeErr, coverageErr, riderErr, effdateErr, bonvalsurrErr, bonresvalErr, paycurrErr, numpolsErr, letprtflagErr};
		screenDateFields = new BaseData[] {rcdate, ptdate, btdate, bonusDecDate, effdate};
		screenDateErrFields = new BaseData[] {rcdateErr, ptdateErr, btdateErr, bondecdateErr, effdateErr};
		screenDateDispFields = new BaseData[] {rcdateDisp, ptdateDisp, btdateDisp, bonusDecDateDisp, effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6691screen.class;
		protectRecord = S6691protect.class;
	}

}
