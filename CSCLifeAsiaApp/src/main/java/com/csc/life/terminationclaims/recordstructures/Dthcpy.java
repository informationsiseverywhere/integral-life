package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:15
 * Description:
 * Copybook name: DTHCPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Dthcpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData deathRec = new FixedLengthStringData(getDeathRecSize());
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(deathRec, 0);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(deathRec, 1);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(deathRec, 9);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(deathRec, 11);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(deathRec, 13);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(deathRec, 15);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(deathRec, 17);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(deathRec, 21);
  	public PackedDecimalData estimatedVal = new PackedDecimalData(17, 2).isAPartOf(deathRec, 29);
  	public PackedDecimalData actualVal = new PackedDecimalData(17, 2).isAPartOf(deathRec, 38);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(deathRec, 47);
  	public FixedLengthStringData contractCurr = new FixedLengthStringData(3).isAPartOf(deathRec, 50);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(deathRec, 53);
  	public FixedLengthStringData element = new FixedLengthStringData(4).isAPartOf(deathRec, 56);
  	public FixedLengthStringData riind = new FixedLengthStringData(1).isAPartOf(deathRec, 60);
  	public FixedLengthStringData fieldType = new FixedLengthStringData(1).isAPartOf(deathRec, 61);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(deathRec, 62);
  	public FixedLengthStringData batckey = new FixedLengthStringData(64).isAPartOf(deathRec, 66);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(deathRec, 130);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(deathRec, 133);
  	public ZonedDecimalData date_var = new ZonedDecimalData(6, 0).isAPartOf(deathRec, 137).setUnsigned();
  	public ZonedDecimalData time = new ZonedDecimalData(6, 0).isAPartOf(deathRec, 143).setUnsigned();
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(deathRec, 149).setUnsigned();
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(deathRec, 155);


	public void initialize() {
		COBOLFunctions.initialize(deathRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		deathRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public int getDeathRecSize()
	{
		return 156;
	}


}