package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sh5c2ScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,51);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,61);
	public ZonedDecimalData riskcommdte = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,69);	
	
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,77);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,85);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,140);
	public FixedLengthStringData claimnumber = DD.claimnumber.copy().isAPartOf(dataFields,187);
	public FixedLengthStringData claimTyp = DD.clamtyp.copy().isAPartOf(dataFields,196);
	
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,198);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,206);
	
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,214);
	public FixedLengthStringData rsncde = DD.rsncde.copy().isAPartOf(dataFields,222);
	public ZonedDecimalData totalPrem = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,226);
	public ZonedDecimalData tCashval = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,243);
	public ZonedDecimalData tPolicyReserve = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,260);
	public ZonedDecimalData tFundVal = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,277);
	public ZonedDecimalData apl = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,294);
	public ZonedDecimalData aplInterest = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,311);
	public ZonedDecimalData policyloan = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,328);
	public ZonedDecimalData policyloanInterest = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,345);
	public ZonedDecimalData susamt = DD.susamt.copyToZonedDecimal().isAPartOf(dataFields,362);
	public ZonedDecimalData unexpiredprm = DD.unexpiredprm.copyToZonedDecimal().isAPartOf(dataFields,379);
	public ZonedDecimalData total = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,396);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,413);
	public FixedLengthStringData refundOption = DD.refundOption.copy().isAPartOf(dataFields,463);
	public ZonedDecimalData advPrm = DD.tprem.copyToZonedDecimal().isAPartOf(dataFields,467);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData riskcommdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData claimnumberErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData claimTypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData rsncdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData totalPremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData tCashvalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData tPolicyReserveErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData tFundValErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData aplErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData aplInterestErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData policyloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);  
	public FixedLengthStringData policyloanInterestErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);  
	public FixedLengthStringData susamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);  
	public FixedLengthStringData unexpiredprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);  
	public FixedLengthStringData totalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);  
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData refundOptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData advPrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	
	 public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	 public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	 public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
   	 public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	 public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	 public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	 public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	 public FixedLengthStringData[] riskcommdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] claimnumberOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] claimTypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] rsncdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] totalPremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] tCashvalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] tPolicyReserveOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);	
	public FixedLengthStringData[] tFundValOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240); 
	public FixedLengthStringData[] aplOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252); 
	public FixedLengthStringData[] aplInterestOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] policyloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276); 
	public FixedLengthStringData[] policyloanInterestOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288); 
	public FixedLengthStringData[] susamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300); 
	public FixedLengthStringData[] unexpiredprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312); 
	public FixedLengthStringData[] totalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324); 
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336); 
	public FixedLengthStringData[] refundOptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);	
	public FixedLengthStringData[] advPrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);	

	public FixedLengthStringData subfileArea = new FixedLengthStringData(getSubfileAreaSize());
	public FixedLengthStringData subfileFields = new FixedLengthStringData(getSubfileFieldsSize()).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,2);
	public FixedLengthStringData component = DD.component.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData compdesc = DD.cmpntdesc.copy().isAPartOf(subfileFields,12);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(subfileFields,54);
	public ZonedDecimalData cashvalare = DD.cashvalare.copyToZonedDecimal().isAPartOf(subfileFields,71);
	public ZonedDecimalData fundVal = DD.cashvalare.copyToZonedDecimal().isAPartOf(subfileFields,88);
	public ZonedDecimalData polReserve = DD.cashvalare.copyToZonedDecimal().isAPartOf(subfileFields,105);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(getErrorSubfileSize()).isAPartOf(subfileArea, getSubfileFieldsSize());
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData componentErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData compdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData instPremErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData cashvalareErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData fundValErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData polReserveErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);

	public FixedLengthStringData outputSubfile = new FixedLengthStringData(getOutputSubfileSize()).isAPartOf(subfileArea, getErrorSubfileSize()+getSubfileFieldsSize());
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] componentOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] compdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] instPremOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] cashvalareOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] fundValOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] polReserveOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, getSubfileFieldsSize()+getErrorSubfileSize()+getOutputSubfileSize());

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	

	 public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	 public FixedLengthStringData riskcommdteDisp = new FixedLengthStringData(10);
	 public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	 public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	 public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	 
	 public LongData Sh5c2screensflWritten = new LongData(0);
	 public LongData Sh5c2screenctlWritten = new LongData(0);
	 public LongData Sh5c2screenWritten = new LongData(0);
	 public LongData Sh5c2protectWritten = new LongData(0);
	 public GeneralTable sh5c2screensfl = new GeneralTable(AppVars.getInstance());
	 

	 public boolean hasSubfile() {
		 return true;
	 }

	 public GeneralTable getScreenSubfileTable() {
		 return sh5c2screensfl;
	 }

	 public Sh5c2ScreenVars() {
		 super();
		 initialiseScreenVars();
	 }

	 {
		 screenIndicArea = DD.indicarea.copy();
	 }
	 
	 protected void initialiseScreenVars() {
		 
		 	fieldIndMap.put(rsncdeOut,new String[] {"05","10","-05",null, null, null, null, null, null, null, null, null});
			fieldIndMap.put(resndescOut,new String[] {"06","12","-06",null, null, null, null, null, null, null, null, null});
			fieldIndMap.put(refundOptionOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
			fieldIndMap.put(tPolicyReserveOut,new String[] {"07","11","-07",null, null, null, null, null, null, null, null, null});
			
			screenSflFields = getscreenLSflFields();
			screenSflOutFields = getscreenSflOutFields();
			screenSflErrFields = getscreenSflErrFields();
			screenSflDateFields = new BaseData[] {};
			screenSflDateErrFields = new BaseData[] {};
			screenSflDateDispFields = new BaseData[] {};
	
			screenFields = getscreenFields();
			screenOutFields = getscreenOutFields();
			screenErrFields = getscreenErrFields();
			screenDateFields = getscreenDateFields();
			screenDateErrFields = getscreenDateErrFields();
			screenDateDispFields = getscreenDateDispFields();
			
			screenDataArea = dataArea;
			screenSubfileArea = subfileArea;
			screenSflIndicators = screenIndicArea;
			errorInds = errorIndicators;
			errorSflInds = errorSubfile;
			screenRecord = Sh5c2screen.class;
			screenSflRecord = Sh5c2screensfl.class;
			screenCtlRecord = Sh5c2screenctl.class;
			initialiseSubfileArea();
			protectRecord = Sh5c2protect.class;
	 }
	 
	 
	 public void initialiseSubfileArea() {
			initialize(screenSubfileArea);
			subfilePage.set(Sh5c2screenctl.lrec.pageSubfile);
		}
	 
	 

	public int getDataAreaSize()
	{
		return getDataFieldsSize()+getErrorIndicatorSize()+getOutputFieldSize();	
	}
	public int getDataFieldsSize()
	{
		return 484;
	}
	public int getErrorIndicatorSize()
	{
		return 124;
	}
	public int getOutputFieldSize()
	{
		return 372;
	}
	
	
	public BaseData[] getscreenFields()
	{
		
		return new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate,riskcommdte,cownnum, ownername, lifcnum, linsname, claimnumber, claimTyp, ptdate, btdate, effdate, rsncde, totalPrem, tCashval, tPolicyReserve, tFundVal, apl,aplInterest, policyloan , policyloanInterest, susamt, unexpiredprm, total, resndesc,refundOption, advPrm};
		
	}
	
	public BaseData[][] getscreenOutFields()
	{
		
		return new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut,riskcommdteOut,cownnumOut, ownernameOut, lifcnumOut, linsnameOut, claimnumberOut, claimTypOut, ptdateOut, btdateOut, effdateOut, rsncdeOut, totalPremOut, tCashvalOut, tPolicyReserveOut, tFundValOut, aplOut,aplInterestOut, policyloanOut, policyloanInterestOut, susamtOut, unexpiredprmOut, totalOut, resndescOut, refundOptionOut, advPrmOut};
		
	}
	
	public BaseData[] getscreenErrFields()
	{
		
		return new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, riskcommdteErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, claimnumberErr, claimTypErr, ptdateErr, btdateErr,effdateErr, rsncdeErr, totalPremErr, tCashvalErr, tPolicyReserveErr, tFundValErr, aplErr,aplInterestErr, policyloanErr, policyloanInterestErr, susamtErr, unexpiredprmErr, totalErr, resndescErr, refundOptionErr, advPrmErr};
		
	}	
	
	public int getSubfileAreaSize()
	{
		return 255;
	}
	
	public int getSubfileFieldsSize()
	{
		return 122;
	}

	public int getErrorSubfileSize()
	{
		return 32;
	}

	public int getOutputSubfileSize()
	{
		return 96;
	}

	public BaseData[] getscreenLSflFields()
	{
		return new BaseData[] {coverage, rider, component, compdesc, instPrem, cashvalare, fundVal, polReserve};
	}
	
	public BaseData[][] getscreenSflOutFields()
	{
		return new BaseData[][] {coverageOut, riderOut, componentOut, compdescOut, instPremOut, cashvalareOut, fundValOut, polReserveOut};
	}
	
	
	public BaseData[] getscreenSflErrFields()
	{
		return new BaseData[] {coverageErr, riderErr, componentErr, compdescErr, instPremErr, cashvalareErr, fundValErr, polReserveErr};
	}
	

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {occdate,riskcommdte,ptdate, btdate, effdate};
	}
	
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {occdateDisp,riskcommdteDisp, ptdateDisp, btdateDisp, effdateDisp};
	}
	

	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {occdateErr, riskcommdteErr,ptdateErr, btdateErr, effdateErr};
	}
	
}
