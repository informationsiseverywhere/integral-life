/*
 * File: Regpayis.java
 * Date: 30 August 2009 2:04:54
 * Author: Quipoz Limited
 * 
 * Class transformed from REGPAYIS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.terminationclaims.dataaccess.RegplnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegtsfxTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
* This subroutine forms part of the 9405 Annuities Development.
* It is called from T5671 during the AT contract issue processing.
*
* The purpose of this subroutine is to replace all Regular Payment
* Temporary records (REGT) with Regular Payment records (REGP)
* - one REGP record for each REGT record.
* It will also set the Regular Payment Status based on the entries
* on the Regular Payment allowable actions by status table, T6693.
*
* The key to this table will be ** plus the component code.
*
*****************************************************************
* </pre>
*/
public class Regpayis extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "REGPAYIS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaItemstat = new FixedLengthStringData(2).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaItemtabl = new FixedLengthStringData(4).isAPartOf(wsaaItem, 2);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
		/* ERRORS */
	private String h144 = "H144";
	private String h157 = "H157";
		/* FORMATS */
	private String regtsfxrec = "REGTSFXREC";
	private String regplnbrec = "REGPLNBREC";
		/* TABLES */
	private String t6693 = "T6693";
	private String t6625 = "T6625";
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Isuallrec isuallrec = new Isuallrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Regular Payments File*/
	private RegplnbTableDAM regplnbIO = new RegplnbTableDAM();
		/*Regular Payments Temporary Data*/
	private RegtsfxTableDAM regtsfxIO = new RegtsfxTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6625rec t6625rec = new T6625rec();
	private T6693rec t6693rec = new T6693rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Regpayis() {
		super();
	}

public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		/*START*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		syserrrec.subrname.set(wsaaSubr);
		regtsfxIO.setChdrcoy(isuallrec.company);
		regtsfxIO.setChdrnum(isuallrec.chdrnum);
		regtsfxIO.setLife(isuallrec.life);
		regtsfxIO.setCoverage(isuallrec.coverage);
		regtsfxIO.setRider(isuallrec.rider);
		regtsfxIO.setPlanSuffix(isuallrec.planSuffix);
		regtsfxIO.setSeqnbr(ZERO);
		regtsfxIO.setRgpynum(ZERO);
		regtsfxIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		regtsfxIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtsfxIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		regtsfxIO.setFormat(regtsfxrec);
		SmartFileCode.execute(appVars, regtsfxIO);
		if (isNE(regtsfxIO.getStatuz(),varcom.oK)
		&& isNE(regtsfxIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regtsfxIO.getParams());
			syserrrec.statuz.set(regtsfxIO.getStatuz());
			fatalError600();
		}
		while ( !(isEQ(regtsfxIO.getStatuz(),varcom.endp)
		|| isNE(regtsfxIO.getChdrcoy(),isuallrec.company)
		|| isNE(regtsfxIO.getChdrnum(),isuallrec.chdrnum)
		|| isNE(regtsfxIO.getLife(),isuallrec.life)
		|| isNE(regtsfxIO.getCoverage(),isuallrec.coverage)
		|| isNE(regtsfxIO.getRider(),isuallrec.rider)
		|| isNE(regtsfxIO.getPlanSuffix(),isuallrec.planSuffix))) {
			convertRegtRegp2000();
		}
		
		exitProgram();
	}

protected void convertRegtRegp2000()
	{
		start2000();
	}

protected void start2000()
	{
		regplnbIO.setChdrcoy(regtsfxIO.getChdrcoy());
		regplnbIO.setChdrnum(regtsfxIO.getChdrnum());
		regplnbIO.setLife(regtsfxIO.getLife());
		regplnbIO.setCoverage(regtsfxIO.getCoverage());
		regplnbIO.setRider(regtsfxIO.getRider());
		regplnbIO.setRgpynum(regtsfxIO.getRgpynum());
		regplnbIO.setPlanSuffix(regtsfxIO.getPlanSuffix());
		regplnbIO.setFirstPaydate(regtsfxIO.getFirstPaydate());
		regplnbIO.setNextPaydate(regtsfxIO.getFirstPaydate());
		regplnbIO.setSacscode(regtsfxIO.getSacscode());
		regplnbIO.setSacstype(regtsfxIO.getSacstype());
		regplnbIO.setGlact(regtsfxIO.getGlact());
		regplnbIO.setDebcred(regtsfxIO.getDebcred());
		regplnbIO.setDestkey(regtsfxIO.getDestkey());
		regplnbIO.setPaycoy(regtsfxIO.getPaycoy());
		regplnbIO.setPayclt(regtsfxIO.getPayclt());
		regplnbIO.setRgpymop(regtsfxIO.getRgpymop());
		regplnbIO.setRegpayfreq(regtsfxIO.getRegpayfreq());
		regplnbIO.setCurrcd(regtsfxIO.getCurrcd());
		regplnbIO.setPymt(regtsfxIO.getPymt());
		regplnbIO.setPrcnt(regtsfxIO.getPrcnt());
		regplnbIO.setPayreason(regtsfxIO.getPayreason());
		regplnbIO.setClaimevd(regtsfxIO.getClaimevd());
		regplnbIO.setBankkey(regtsfxIO.getBankkey());
		regplnbIO.setBankacckey(regtsfxIO.getBankacckey());
		regplnbIO.setCrtdate(regtsfxIO.getCrtdate());
		regplnbIO.setRevdte(regtsfxIO.getRevdte());
		regplnbIO.setFinalPaydate(regtsfxIO.getFinalPaydate());
		regplnbIO.setAnvdate(regtsfxIO.getAnvdate());
		regplnbIO.setRgpytype(regtsfxIO.getRgpytype());
		regplnbIO.setCrtable(regtsfxIO.getCrtable());
		regplnbIO.setTotamnt(regtsfxIO.getTotamnt());
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(regtsfxIO.getCrtable());
		itdmIO.setItmfrm(regtsfxIO.getCrtdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(),t6625)
		|| isNE(itdmIO.getItemitem(),regtsfxIO.getCrtable())) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t6625rec.t6625Rec.set(itdmIO.getGenarea());
		datcon2rec.intDate1.set(regtsfxIO.getCrtdate());
		if (isNE(t6625rec.frequency,SPACES)) {
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(t6625rec.frequency);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			regplnbIO.setCertdate(datcon2rec.intDate2);
		}
		else {
			regplnbIO.setCertdate(varcom.vrcmMaxDate);
		}
		if (isLT(regplnbIO.getCertdate(),regplnbIO.getRevdte())
		&& isNE(regplnbIO.getRevdte(),varcom.vrcmMaxDate)) {
			regplnbIO.setCertdate(regplnbIO.getRevdte());
		}
		authoriseRecord3000();
		regplnbIO.setAprvdate(varcom.vrcmMaxDate);
		regplnbIO.setLastPaydate(varcom.vrcmMaxDate);
		regplnbIO.setCancelDate(varcom.vrcmMaxDate);
		regplnbIO.setRecvdDate(varcom.vrcmMaxDate);
		regplnbIO.setIncurdt(varcom.vrcmMaxDate);
		regplnbIO.setValidflag("1");
		regplnbIO.setTermid(isuallrec.termid);
		regplnbIO.setTransactionDate(isuallrec.transactionDate);
		regplnbIO.setTransactionTime(isuallrec.transactionTime);
		regplnbIO.setUser(isuallrec.user);
		regplnbIO.setTranno(chdrlnbIO.getTranno());
		regplnbIO.setFunction(varcom.writr);
		regplnbIO.setFormat(regplnbrec);
		SmartFileCode.execute(appVars, regplnbIO);
		if (isNE(regplnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regplnbIO.getParams());
			syserrrec.statuz.set(regplnbIO.getStatuz());
			fatalError600();
		}
		regtsfxIO.setFunction(varcom.deltd);
		regtsfxIO.setFormat(regtsfxrec);
		SmartFileCode.execute(appVars, regtsfxIO);
		if (isNE(regtsfxIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtsfxIO.getParams());
			syserrrec.statuz.set(regtsfxIO.getStatuz());
			fatalError600();
		}
		regtsfxIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, regtsfxIO);
		if (isNE(regtsfxIO.getStatuz(),varcom.oK)
		&& isNE(regtsfxIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regtsfxIO.getParams());
			syserrrec.statuz.set(regtsfxIO.getStatuz());
			fatalError600();
		}
	}

protected void authoriseRecord3000()
	{
		start3000();
	}

protected void start3000()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(t6693);
		wsaaItemstat.set("**");
		wsaaItemtabl.set(regtsfxIO.getCrtable());
		itdmIO.setItemitem(wsaaItem);
		itdmIO.setItmfrm(isuallrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(),t6693)
		|| isNE(itdmIO.getItemitem(),wsaaItem)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(wsaaItem);
			syserrrec.statuz.set(h144);
			fatalError600();
		}
		else {
			t6693rec.t6693Rec.set(itdmIO.getGenarea());
		}
		regplnbIO.setRgpystat(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			findStatus4000();
		}
		if (isEQ(regplnbIO.getRgpystat(),SPACES)) {
			syserrrec.params.set(isuallrec.batctrcde);
			syserrrec.statuz.set(h157);
			fatalError600();
		}
	}

protected void findStatus4000()
	{
		/*START*/
		if (isEQ(t6693rec.trcode[wsaaIndex.toInt()],isuallrec.batctrcde)) {
			regplnbIO.setRgpystat(t6693rec.rgpystat[wsaaIndex.toInt()]);
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		isuallrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
