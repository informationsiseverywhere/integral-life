package com.csc.life.terminationclaims.dataaccess.dao;

import com.csc.life.terminationclaims.dataaccess.model.Laptpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface LaptpfDAO extends BaseDAO<Laptpf> {
	public Laptpf getLaptByKey(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx);
	public boolean updtOrInstLaptByKey(Laptpf laptpf);
	public int updatLaptByKey(Laptpf laptpf);
	public boolean insertLaptpf(Laptpf laptpf);
}
