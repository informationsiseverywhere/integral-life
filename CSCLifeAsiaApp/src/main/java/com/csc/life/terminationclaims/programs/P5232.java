/*
 * File: P5232.java
 * Date: 30 August 2009 0:21:06
 * Author: Quipoz Limited
 * 
 * Class transformed from P5232.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.dataaccess.AnbkTableDAM;
import com.csc.life.annuities.dataaccess.AnnyvstTableDAM;
import com.csc.life.annuities.recordstructures.Vcalcpy;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.screens.S5220ScreenVars;
import com.csc.life.terminationclaims.screens.S5232ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.dip.jvpms.web.ExternalisedRules;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This  program  is  part of  the 9405 Annuities Development.  It
* forms part of the Registration of Vesting on a Deferred annuity
* contract.
*
* This program allows the user to  modify or enquire upon Annuity
* Details  data held on  the  Annuity Details File, ANNY.  If the
* user is enquiring on Annuities data, the Annuity Details Screen
* is displayed  with  all fields protected, otherwise all annuity
* detail fields are amendable and the screen is redisplayed until
* it is error free or the user exits from the transaction.
*
* If the details are changed, the original Annuity Details record
* (ANNY), will  be  unaltered  but  a  new  Validflag  '1' record
* will be written with the new details. This Validflag '1' record
* is only temporary as its  coverage key  will  be changed in the
* AT module and the appropriate new details recorded on this  new
* ANNY.  The program then calls the Vesting  Calculation  program
* on T6598 which recalculates the Benefit  payable  based  on the
* commutation factors on T6623.
*
*****************************************************************
* </pre>
*/
public class P5232 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5232");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaBreakout = "";
	private ZonedDecimalData wsaaGuarperd = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaAdvance = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaArrears = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFreqann = new FixedLengthStringData(2);
	private PackedDecimalData wsaaBirthDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBirthDatej = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSexj = new FixedLengthStringData(1);
		/* ERRORS */
	private static final String e925 = "E925";
	private static final String f045 = "F045";
	private static final String f046 = "F046";
	private static final String f047 = "F047";
	private static final String f048 = "F048";
	private static final String f049 = "F049";
	private static final String f064 = "F064";
	private static final String f090 = "F090";
	private static final String h347 = "H347";
		/* TABLES */
	private static final String t6625 = "T6625";
	private static final String t6598 = "T6598";
		/* FORMATS */
	private static final String annyvstrec = "ANNYVSTREC";
	protected static final String chdrmjarec = "CHDRMJAREC";
	private static final String anbkrec = "ANBKREC";
	private static final String cltsrec = "CLTSREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String itemrec = "ITEMREC";
	protected static final String covrmjarec = "COVRMJAREC";			//ILIFE-4605
	private AnbkTableDAM anbkIO = new AnbkTableDAM();
	private AnnyvstTableDAM annyvstIO = new AnnyvstTableDAM();
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	protected CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	protected CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);		//ILIFE=4605
	protected Covrpf covrpf = new Covrpf();			//ILIFE=4605
	private Batckey wsaaBatchkey = new Batckey();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private T6625rec t6625rec = new T6625rec();
	private T6598rec t6598rec = new T6598rec();
	private Vcalcpy vcalcpy = new Vcalcpy();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wssplife wssplife = new Wssplife();
	private S5232ScreenVars sv = getLScreenVars();
	ExternalisedRules er=new ExternalisedRules();
 	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
 	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	boolean isFeatureConfig = false;	public P5232() {
		super();
		screenVars = sv;
		new ScreenModel("S5232", AppVars.getInstance(), sv);
	}

	protected S5232ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5232ScreenVars.class);
	}

	
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Initialisation of the screen area and working storage.*/
		sv.dataArea.set(SPACES);
		wsaaBreakout = "N";
		wsaaGuarperd.set(ZERO);
		wsaaAdvance.set(SPACES);
		wsaaArrears.set(SPACES);
		wsaaFreqann.set(SPACES);
		wsaaBirthDate.set(ZERO);
		wsaaBirthDatej.set(ZERO);
		wsaaSex.set(SPACES);
		wsaaSexj.set(SPACES);
		wsaaBatchkey.set(wsspcomn.batchkey);
		
		readCovrChdrCustomerSpecific1011();	
	/* Set up screen fields.*/
		sv.chdrnum.set(covrmjaIO.getChdrnum());
		sv.life.set(covrmjaIO.getLife());
		sv.coverage.set(covrmjaIO.getCoverage());
		sv.rider.set(covrmjaIO.getRider());
		/* Read (READR) the LIFELNB file.*/
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setChdrnum(covrmjaIO.getChdrnum());
		lifelnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifelnbIO.setLife(covrmjaIO.getLife());
		lifelnbIO.setJlife(ZERO);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		/* Read (READR) the CLTS file.*/
		cltsIO.setFunction(varcom.readr);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		wsaaBirthDate.set(cltsIO.getCltdob());
		wsaaSex.set(cltsIO.getCltsex());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		/* Read (READR) the LIFELNB file for joint life details.*/
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setChdrnum(covrmjaIO.getChdrnum());
		lifelnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifelnbIO.setLife(covrmjaIO.getLife());
		lifelnbIO.setJlife("01");
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
		}
		else {
			sv.jlifcnum.set(lifelnbIO.getLifcnum());
			cltsIO.setFunction(varcom.readr);
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setClntnum(lifelnbIO.getLifcnum());
			cltsIO.setFormat(cltsrec);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			wsaaBirthDatej.set(cltsIO.getCltdob());
			wsaaSexj.set(cltsIO.getCltsex());
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
		/* If the user is vesting the whole plan, the plan suffix will*/
		/* equal zero and the ANNY file is read to obtain the annuities*/
		/* data.  If the user is vesting part of the plan, this part may*/
		/* be summarised or not.  When the part is not summarised, the*/
		/* plan suffix is greater than the summarised policies and the*/
		/* ANNY file is read.  Otherwise the part is summarised and the*/
		/* ANBK file is read.  If the record is not found on the ANBK,*/
		/* read the ANNY file.*/
		if (isNE(covrmjaIO.getPlanSuffix(), 0)
		&& isLTE(covrmjaIO.getPlanSuffix(), chdrmjaIO.getPolsum())) {
			readAnbk1200();
			if (isEQ(anbkIO.getStatuz(), varcom.mrnf)) {
				annyvstIO.setPlanSuffix(0);
				readAnny1100();
			}
		}
		else {
			annyvstIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
			readAnny1100();
		}
		isFeatureConfig  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUOTR006", appVars, "IT");	
		if (isFeatureConfig){
			sv.ddindOut[varcom.pr.toInt()].set("Y"); 
			}
		else
			sv.ddindOut[varcom.nd.toInt()].set("Y");	
	}

	protected void readCovrChdrCustomerSpecific1011() {
		
		/* Retrieve the COVRMJA record stored previously.*/	
		//ILIFE-4605 changes started
		covrpf = covrDao.getCacheObject(covrpf);
		if(covrpf != null){
			if (covrpf.getUniqueNumber() <= 0) {
				fatalError600();
		}
		covrmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covrmjaIO.setChdrnum(covrpf.getChdrnum());
		covrmjaIO.setLife(covrpf.getLife());
		covrmjaIO.setCoverage(covrpf.getCoverage());
		covrmjaIO.setRider(covrpf.getRider());
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.readr);
		}else{
			covrmjaIO.setFunction(varcom.retrv);
		}
		
		//ILIFE-4605 changes ended
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		
		/* If Vesting, retrieve the CHDRMJA record stored previously.*/
		/* Otherwise read the CHDRMJA as it will not have been stored.*/
		/* Previous program P5069 sets WSSP-FLAG to 'N'/'Y' for Enquiry.*/
		if (isNE(wsspcomn.flag, "I")
		&& isNE(wsspcomn.flag, "N")
		&& isNE(wsspcomn.flag, "Y")) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			
		}
		else {
			chdrmjaIO.setChdrcoy(covrmjaIO.getChdrcoy());
			chdrmjaIO.setChdrnum(covrmjaIO.getChdrnum());
			chdrmjaIO.setFormat(chdrmjarec);
			chdrmjaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
		}
		
		
	}

	/**
	* <pre>
	**** Read the ANNYVST file if whole of plan being processed.
	* </pre>
	*/
protected void readAnny1100()
	{
		startRead1110();
	}

protected void startRead1110()
	{
		/* Read ANNYVST (READR) in order to obtain the Annuity*/
		/* information.*/
		annyvstIO.setChdrcoy(covrmjaIO.getChdrcoy());
		annyvstIO.setChdrnum(covrmjaIO.getChdrnum());
		annyvstIO.setLife(covrmjaIO.getLife());
		annyvstIO.setCoverage(covrmjaIO.getCoverage());
		annyvstIO.setRider(covrmjaIO.getRider());
		annyvstIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyvstIO);
		if (isNE(annyvstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(annyvstIO.getParams());
			fatalError600();
		}
		/* Set up screen values (and working storage for later use).*/
		sv.freqann.set(annyvstIO.getFreqann());
		wsaaFreqann.set(annyvstIO.getFreqann());
		sv.guarperd.set(annyvstIO.getGuarperd());
		wsaaGuarperd.set(annyvstIO.getGuarperd());
		sv.dthpercn.set(annyvstIO.getDthpercn());
		sv.dthperco.set(annyvstIO.getDthperco());
		sv.arrears.set(annyvstIO.getArrears());
		wsaaArrears.set(annyvstIO.getArrears());
		sv.advance.set(annyvstIO.getAdvance());
		wsaaAdvance.set(annyvstIO.getAdvance());
		sv.ppind.set(annyvstIO.getPpind());
		sv.withprop.set(annyvstIO.getWithprop());
		sv.withoprop.set(annyvstIO.getWithoprop());
		sv.nomlife.set(annyvstIO.getNomlife());
		sv.intanny.set(annyvstIO.getIntanny());
		if (isEQ(wsaaBreakout, "Y")) {
			compute(sv.capcont, 3).setRounded(div(annyvstIO.getCapcont(), chdrmjaIO.getPolsum()));
		}
		else {
			sv.capcont.set(annyvstIO.getCapcont());
		}
		zrdecplrec.amountIn.set(sv.capcont);
		callRounding5000();
		sv.capcont.set(zrdecplrec.amountOut);
	}

	/**
	* <pre>
	**** Read the ANBK file if part of plan being processed.
	* </pre>
	*/
protected void readAnbk1200()
	{
		startRead1210();
	}

protected void startRead1210()
	{
		/* Read ANBK (READR) in order to obtain the Annuity information.*/
		wsaaBreakout = "Y";
		anbkIO.setChdrcoy(covrmjaIO.getChdrcoy());
		anbkIO.setChdrnum(covrmjaIO.getChdrnum());
		anbkIO.setLife(covrmjaIO.getLife());
		anbkIO.setCoverage(covrmjaIO.getCoverage());
		anbkIO.setRider(covrmjaIO.getRider());
		anbkIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		anbkIO.setTranno(wsspcomn.tranno);
		anbkIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, anbkIO);
		if ((isNE(anbkIO.getStatuz(), varcom.oK)
		&& isNE(anbkIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(anbkIO.getParams());
			fatalError600();
		}
		/* Leave the section if no ANBK record is found.*/
		if (isEQ(anbkIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		/* Set up screen values (and working storage for later use).*/
		sv.freqann.set(anbkIO.getFreqann());
		wsaaFreqann.set(anbkIO.getFreqann());
		sv.guarperd.set(anbkIO.getGuarperd());
		wsaaGuarperd.set(anbkIO.getGuarperd());
		sv.dthpercn.set(anbkIO.getDthpercn());
		sv.dthperco.set(anbkIO.getDthperco());
		sv.arrears.set(anbkIO.getArrears());
		wsaaArrears.set(anbkIO.getArrears());
		sv.advance.set(anbkIO.getAdvance());
		wsaaAdvance.set(anbkIO.getAdvance());
		sv.ppind.set(anbkIO.getPpind());
		sv.withprop.set(anbkIO.getWithprop());
		sv.withoprop.set(anbkIO.getWithoprop());
		sv.nomlife.set(anbkIO.getNomlife());
		sv.intanny.set(anbkIO.getIntanny());
		sv.capcont.set(anbkIO.getCapcont());
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* If the user is enquiring about annuities data, protect the   */
		/* screen.                                                      */
		/* Previous program P5069 sets WSSP-FLAG to 'N'/'Y' for Enquiry.*/
		if (isEQ(wsspcomn.flag, "I")
		|| isEQ(wsspcomn.flag, "N")
		|| isEQ(wsspcomn.flag, "Y")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S5232IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S5232-DATA-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/* If the user is enquiring about annuities data, no validation*/
		/* is required.  Otherwise each field must be validated.*/
		if (isNE(wsspcomn.flag, "I")
		&& isNE(wsspcomn.flag, "N")
		&& isNE(wsspcomn.flag, "Y")) {
			validateReqd2100();
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	**** If the user is modifying the annuities data, this section
	**** validates the input fields.
	* </pre>
	*/
protected void validateReqd2100()
	{
		valStart2110();
	}

protected void valStart2110()
	{
		if (isEQ(sv.freqann, SPACES)) {
			sv.freqannErr.set(e925);
		}
		if (isEQ(sv.advance, SPACES)
		&& isEQ(sv.arrears, SPACES)) {
			sv.arrearsErr.set(f045);
			sv.advanceErr.set(f045);
		}
		else {
			if (isNE(sv.advance, SPACES)
			&& isNE(sv.arrears, SPACES)) {
				sv.arrearsErr.set(f047);
				sv.advanceErr.set(f047);
			}
		}
		if (isEQ(sv.withprop, SPACES)
		&& isEQ(sv.withoprop, SPACES)) {
			sv.withpropErr.set(f046);
			sv.withopropErr.set(f046);
		}
		else {
			if (isNE(sv.withprop, SPACES)
			&& isNE(sv.withoprop, SPACES)) {
				sv.withpropErr.set(f047);
				sv.withopropErr.set(f047);
			}
		}
		if (isNE(sv.advance, SPACES)
		&& isNE(sv.withprop, SPACES)) {
			sv.advanceErr.set(f048);
			sv.withpropErr.set(f048);
		}
		if (isGT(sv.capcont, covrmjaIO.getSumins())) {
			sv.capcontErr.set(f090);
		}
		if (isNE(sv.nomlife, SPACES)) {
			begnLifelnb2200();
		}
		if (isGT(sv.dthpercn, 100)
		|| isGT(sv.dthperco, 100)) {
			if (isGT(sv.dthpercn, 100)) {
				sv.dthpercnErr.set(h347);
			}
			else {
				sv.dthpercoErr.set(h347);
			}
		}
		if ((isGT(sv.dthpercn, 0)
		|| isGT(sv.dthperco, 0))
		&& isEQ(sv.nomlife, SPACES)) {
			sv.nomlifeErr.set(f064);
		}
	}

	/**
	* <pre>
	**** Section to read (BEGN) the LIFELNB file.
	* </pre>
	*/
protected void begnLifelnb2200()
	{
		begnStart2210();
	}

protected void begnStart2210()
	{
		lifelnbIO.setFunction(varcom.begn);
		lifelnbIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifelnbIO.setChdrnum(covrmjaIO.getChdrnum());
		lifelnbIO.setLife(covrmjaIO.getLife());
		lifelnbIO.setJlife(ZERO);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		//performance improvement --  Niharika Modi 
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		while ( !(isEQ(lifelnbIO.getLifcnum(), sv.nomlife)
		|| isEQ(lifelnbIO.getStatuz(), varcom.endp)
		|| isNE(lifelnbIO.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(), covrmjaIO.getChdrnum())
		|| isNE(lifelnbIO.getLife(), covrmjaIO.getLife()))) {
			nextrLifelnb2300();
		}
		
		if (isEQ(lifelnbIO.getStatuz(), varcom.endp)
		|| isNE(lifelnbIO.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(), covrmjaIO.getChdrnum())
		|| isNE(lifelnbIO.getLife(), covrmjaIO.getLife())) {
			sv.nomlifeErr.set(f049);
		}
	}

	/**
	* <pre>
	**** Section to read the next record (NEXTR) in the LIFELNB file.
	* </pre>
	*/
protected void nextrLifelnb2300()
	{
		/*NEXTR-START*/
		lifelnbIO.setFunction(varcom.nextr);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	**** Update Database if required and log transaction.
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/* The database is not updated if the user is enquiring on data.*/
		if (isEQ(wsspcomn.flag, "I")
		|| isEQ(wsspcomn.flag, "N")
		|| isEQ(wsspcomn.flag, "Y")) {
			return ;
		}
		if (isEQ(wsaaBreakout, "N")) {
			updateReqd3100();
		}
		else {
			breakoutReqd3200();
		}
		if (isNE(sv.freqann, wsaaFreqann)
		|| isNE(sv.guarperd, wsaaGuarperd)
		|| isNE(sv.arrears, wsaaArrears)
		|| isNE(sv.advance, wsaaAdvance)) {
			calculateNewAmount3300();
		}
		/*EXIT*/
	}

protected void updateReqd3100()
	{
		updateStart3110();
		createAnnyRecord3130();
		writeAnnyRecord3140();
	}

protected void updateStart3110()
	{
		/* Read and hold (READH) the ANNYVST record being processed.*/
		annyvstIO.setFunction(varcom.readh);
		annyvstIO.setChdrcoy(covrmjaIO.getChdrcoy());
		annyvstIO.setChdrnum(covrmjaIO.getChdrnum());
		annyvstIO.setLife(covrmjaIO.getLife());
		annyvstIO.setCoverage(covrmjaIO.getCoverage());
		annyvstIO.setRider(covrmjaIO.getRider());
		annyvstIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		annyvstIO.setFormat(annyvstrec);
		SmartFileCode.execute(appVars, annyvstIO);
		if (isNE(annyvstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(annyvstIO.getParams());
			syserrrec.statuz.set(annyvstIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	**** Move in the fields ready to write the ANNYVST record.
	* </pre>
	*/
protected void createAnnyRecord3130()
	{
		annyvstIO.setFreqann(sv.freqann);
		annyvstIO.setGuarperd(sv.guarperd);
		annyvstIO.setDthpercn(sv.dthpercn);
		annyvstIO.setDthperco(sv.dthperco);
		annyvstIO.setArrears(sv.arrears);
		annyvstIO.setAdvance(sv.advance);
		annyvstIO.setPpind(sv.ppind);
		annyvstIO.setWithprop(sv.withprop);
		annyvstIO.setWithoprop(sv.withoprop);
		annyvstIO.setNomlife(sv.nomlife);
		annyvstIO.setIntanny(sv.intanny);
		annyvstIO.setCapcont(sv.capcont);
		annyvstIO.setValidflag("1");
	}

	/**
	* <pre>
	**** Insert a record in ANNYVST file.  If one has already been
	**** created through this screen, the held ANNYVST record will
	**** have the same transaction number as the tranno in working
	**** storage.  In this case, the ANNYVST record should be updated.
	**** This has a key of the initial ANNY record but this is only
	**** temporary as a new coverage number is calculated in the AT
	**** module and a new record written with the correct key. This
	**** Validflag '1' record is then deleted in the AT.
	* </pre>
	*/
protected void writeAnnyRecord3140()
	{
		if (isEQ(wsspcomn.tranno, annyvstIO.getTranno())) {
			annyvstIO.setFunction(varcom.rewrt);
		}
		else {
			annyvstIO.setFunction(varcom.writr);
		}
		annyvstIO.setTranno(wsspcomn.tranno);
		annyvstIO.setFormat(annyvstrec);
		SmartFileCode.execute(appVars, annyvstIO);
		if (isNE(annyvstIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(annyvstIO.getParams());
			syserrrec.statuz.set(annyvstIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void breakoutReqd3200()
	{
		setUpRecord3210();
		writeAnbkRecord3220();
	}

protected void setUpRecord3210()
	{
		/* Move in the fields ready to write the ANBK record.*/
		anbkIO.setFreqann(sv.freqann);
		anbkIO.setGuarperd(sv.guarperd);
		anbkIO.setDthpercn(sv.dthpercn);
		anbkIO.setDthperco(sv.dthperco);
		anbkIO.setArrears(sv.arrears);
		anbkIO.setAdvance(sv.advance);
		anbkIO.setPpind(sv.ppind);
		anbkIO.setWithprop(sv.withprop);
		anbkIO.setWithoprop(sv.withoprop);
		anbkIO.setNomlife(sv.nomlife);
		anbkIO.setIntanny(sv.intanny);
		anbkIO.setCapcont(sv.capcont);
		anbkIO.setValidflag("1");
	}

	/**
	* <pre>
	**** Insert a record in ANBK file.  The UPDAT function is used as
	**** an ANBK record may already have been written using this
	**** screen, in which case the existing record will be rewritten.
	* </pre>
	*/
protected void writeAnbkRecord3220()
	{
		anbkIO.setChdrcoy(covrmjaIO.getChdrcoy());
		anbkIO.setChdrnum(covrmjaIO.getChdrnum());
		anbkIO.setLife(covrmjaIO.getLife());
		anbkIO.setCoverage(covrmjaIO.getCoverage());
		anbkIO.setRider(covrmjaIO.getRider());
		anbkIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		anbkIO.setTranno(wsspcomn.tranno);
		anbkIO.setFunction(varcom.updat);
		anbkIO.setFormat(anbkrec);
		SmartFileCode.execute(appVars, anbkIO);
		if (isNE(anbkIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(anbkIO.getParams());
			syserrrec.statuz.set(anbkIO.getStatuz());
			fatalError600();
		}
	}

protected void calculateNewAmount3300()
	{
		setUpRecord3310();
	}

protected void setUpRecord3310()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		//itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t6625rec.t6625Rec.set(itdmIO.getGenarea());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(itemrec);
		itemIO.setItemtabl(t6598);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(t6625rec.vcalcmth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		if (isEQ(t6598rec.calcprog, SPACES)) {
			wssplife.updateFlag.set("N");
			return ;
		}
		/* Get age of Life Assured.*/
		/*    MOVE WSAA-BIRTH-DATE        TO DTC3-INT-DATE-1.              */
		/*    MOVE WSSP-CURRFROM          TO DTC3-INT-DATE-2.              */
		/*    MOVE '01'                   TO DTC3-FREQUENCY.               */
		/*    CALL 'DATCON3'              USING DTC3-DATCON3-REC.          */
		/*    IF DTC3-STATUZ               = O-K                           */
		/*        MOVE DTC3-FREQ-FACTOR   TO VCAL-AGE                      */
		/*    ELSE                                                         */
		/*        MOVE DTC3-DATCON3-REC   TO SYSR-PARAMS                   */
		/*        PERFORM 600-FATAL-ERROR                                  */
		/*    END-IF.                                                      */
		/* Routine to calculate Age next/nearest/last birthday             */
		/* replaces above.                                                 */
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrmjaIO.getCnttype());
		agecalcrec.intDate1.set(wsaaBirthDate);
		agecalcrec.intDate2.set(wsspcomn.currfrom);
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		vcalcpy.age.set(agecalcrec.agerating);
		/* Get age of JOINT Life.*/
		/*    IF WSAA-BIRTH-DATEJ      NOT = ZEROES                        */
		/*       MOVE WSAA-BIRTH-DATEJ    TO DTC3-INT-DATE-1               */
		/*       MOVE WSSP-CURRFROM       TO DTC3-INT-DATE-2               */
		/*       MOVE '01'                TO DTC3-FREQUENCY                */
		/*       CALL 'DATCON3'           USING DTC3-DATCON3-REC           */
		/*       IF DTC3-STATUZ            = O-K                           */
		/*           MOVE DTC3-FREQ-FACTOR   TO VCAL-AGEJ                  */
		/*       ELSE                                                      */
		/*           MOVE DTC3-DATCON3-REC   TO SYSR-PARAMS                */
		/*           PERFORM 600-FATAL-ERROR                               */
		/*       END-IF                                                    */
		/*    END-IF.                                                      */
		if (isNE(wsaaBirthDatej, ZERO)) {
			initialize(agecalcrec.agecalcRec);
			agecalcrec.function.set("CALCP");
			agecalcrec.cnttype.set(chdrmjaIO.getCnttype());
			agecalcrec.intDate1.set(wsaaBirthDatej);
			agecalcrec.intDate2.set(wsspcomn.currfrom);
			agecalcrec.company.set(wsspcomn.fsuco);
			agecalcrec.language.set(wsspcomn.language);
			callProgram(Agecalc.class, agecalcrec.agecalcRec);
			if (isNE(agecalcrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(agecalcrec.statuz);
				fatalError600();
			}
			vcalcpy.agej.set(agecalcrec.agerating);
		}
		vcalcpy.oldGuarperd.set(wsaaGuarperd);
		vcalcpy.oldAdvance.set(wsaaAdvance);
		vcalcpy.oldArrears.set(wsaaArrears);
		vcalcpy.oldFreqann.set(wsaaFreqann);
		vcalcpy.newGuarperd.set(sv.guarperd);
		vcalcpy.newAdvance.set(sv.advance);
		vcalcpy.newArrears.set(sv.arrears);
		vcalcpy.newFreqann.set(sv.freqann);
		vcalcpy.sex.set(wsaaSex);
		vcalcpy.sexj.set(wsaaSexj);
		vcalcpy.crtable.set(covrmjaIO.getCrtable());
		vcalcpy.chdrcoy.set(covrmjaIO.getChdrcoy());
		vcalcpy.oldAmount.set(wssplife.bigAmt);
		vcalcpy.newAmount.set(ZERO);
		vcalcpy.statuz.set(varcom.oK);
		/*IVE-910 LIFE Annuties DAN Product - Vesting Calcualtion - Integration with latest PA compatible started*/
		//callProgram(t6598rec.calcprog, vcalcpy.rec);

        if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))
		{
			callProgram(t6598rec.calcprog, vcalcpy.rec);
		}
		else
		{
			callProgram(t6598rec.calcprog, vcalcpy.rec, chdrmjaIO); 
			if(isEQ(vcalcpy.statuz,SPACES))
				vcalcpy.statuz.set(Varcom.oK);
		}
        
	/*IVE-910 LIFE Annuties DAN Product - Vesting Calcualtion - Integration with latest PA compatible end*/
		if (isEQ(vcalcpy.statuz, varcom.bomb)) {
			syserrrec.statuz.set(vcalcpy.statuz);
			syserrrec.params.set(vcalcpy.rec);
			fatalError600();
		}
		if (isEQ(vcalcpy.statuz, varcom.mrnf)) {
			wssplife.updateFlag.set("N");
		}
		else {
			wssplife.bigAmt.set(vcalcpy.newAmount);
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
	if (isEQ(scrnparams.statuz,Varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			return;
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*") && isNE(sv.ddind,"X")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return;
		}
		popUp4010();
		genssw4020();
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
			/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrmjaIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void genssw4020() {
	if ((isEQ(sv.ddind, "?"))) {
	if (isEQ(gensswrec.function, SPACES)) {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
		return;
	}
	
	gensswrec.company.set(wsspcomn.company);
	gensswrec.progIn.set(wsaaProg);
	gensswrec.transact.set("T504");
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1) {
		saveProgramStack4200();
	}
	sv.ddind.set("?");
	gensswrec.function.set("A");
	callProgram(Genssw.class, gensswrec.gensswRec);
	if ((isNE(gensswrec.statuz, varcom.oK))
			&& (isNE(gensswrec.statuz, varcom.mrnf))) {
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		wsspcomn.nextprog.set(scrnparams.scrname);
	}
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1) {
		loadProgramStack4300();
	}
	}
}
protected void popUp4010() {
	gensswrec.function.set(SPACES);
	if ((isEQ(sv.ddind, "X"))) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1) {
			saveProgramStack4200();
		}
		sv.ddind.set("?");
		gensswrec.function.set("A");
	}
}


protected void restoreProgram4100() {
	/* PARA */
	wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
	sub1.add(1);
	sub2.add(1);
	/* EXIT */
}

protected void saveProgramStack4200() {
	/* PARA */
	wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
	sub1.add(1);
	sub2.add(1);
	/* EXIT */
}

protected void loadProgramStack4300() {
	/* PARA */
	wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
	sub1.add(1);
	sub2.add(1);
	/* EXIT */
}

}
