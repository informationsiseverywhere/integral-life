package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for PROTECT
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sa509protect extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa509ScreenVars sv = (Sa509ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa509protectWritten, null, av, null, ind2, ind3);
		
		
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sa509ScreenVars screenVars = (Sa509ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.notifinum.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.optdsc.setClassString("");
		screenVars.claimant.setClassString("");
		screenVars.clamnme.setClassString("");
		screenVars.relation.setClassString("");
	}

/**
 * Clear all the variables in Sa509protect
 */
	public static void clear(VarModel pv) {
		Sa509ScreenVars screenVars = (Sa509ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.notifinum.clear();
		screenVars.lifcnum.clear();
		screenVars.lifename.clear();
		screenVars.optdsc.clear();
		screenVars.claimant.clear();
		screenVars.clamnme.clear();
		screenVars.relation.clear();
	}
}
