package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Zlreinpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Bd5e4DAO extends BaseDAO<Zlreinpf> { 
	public List<Zlreinpf> loadDataByBatch(int batchID);
	public int populateBd5e4Temp(int batchExtractSize, String payxtempTable,String wsaaChdrnumFrom,String wsaaChdrnumTo);
	public void populateBIReportTmp(List<Zlreinpf> zlreinpfList, int Sno);
	public void populateFailBIRptTmp(String payxtempTable,String wsaaChdrnumFrom,String wsaaChdrnumTo,int Sno) ;

}
