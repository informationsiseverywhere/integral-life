/*
 * File: Pr689.java
 * Date: 30 August 2009 1:56:38
 * Author: Quipoz Limited
 * 
 * Class transformed from PR689.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Wsspdocs;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.terminationclaims.dataaccess.HclabanTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclabltTableDAM;
import com.csc.life.terminationclaims.dataaccess.HcldTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclhTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.screens.Sr688ScreenVars;
import com.csc.life.terminationclaims.screens.Sr689ScreenVars;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
***********************************************************************
* </pre>
*/
public class Pr689 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR689");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr386Language = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386ProgId = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private PackedDecimalData wsaaCompYear = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaClmlvlA = new PackedDecimalData(10, 0).setUnsigned();
	private PackedDecimalData wsaaClmlvlB = new PackedDecimalData(10, 0).setUnsigned();
	private PackedDecimalData wsaaClmlvlC = new PackedDecimalData(7, 2);
	protected PackedDecimalData wsaaClmlvlAmt = new PackedDecimalData(14, 2);
	private PackedDecimalData wsaaClmlvlAmtsav = new PackedDecimalData(14, 2);
	protected PackedDecimalData wsaaAnllvlA = new PackedDecimalData(10, 0).setUnsigned();
	protected PackedDecimalData wsaaLftlvlA = new PackedDecimalData(10, 0).setUnsigned();
	private PackedDecimalData wsaaDatefrom = new PackedDecimalData(10, 0).setUnsigned();
	private PackedDecimalData wsaaDateto = new PackedDecimalData(10, 0).setUnsigned();
	private PackedDecimalData wsaaGincurr = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaDaclaim = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaGcnetpy = new PackedDecimalData(14, 2);
	private PackedDecimalData wsaaCopayAmt = new PackedDecimalData(14, 2);
	protected PackedDecimalData wsaaAnlBal = new PackedDecimalData(14, 2);
	protected PackedDecimalData wsaaLifeBal = new PackedDecimalData(14, 2);
	protected PackedDecimalData wsaaMaxBen = new PackedDecimalData(14, 2);
		/* TABLES */
	private static final String tr386 = "TR386";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String hcldrec = "HCLDREC";
	private static final String hclabanrec = "HCLABANREC";
	private static final String hclabltrec = "HCLABLTREC";
	protected HclabanTableDAM hclabanIO = new HclabanTableDAM();
	protected HclabltTableDAM hclabltIO = new HclabltTableDAM();
	private HcldTableDAM hcldIO = new HcldTableDAM();
	protected HclhTableDAM hclhIO = new HclhTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private Tr386rec tr386rec = new Tr386rec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected Datcon3rec datcon3rec = new Datcon3rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspdocs wsspdocs = new Wsspdocs();
	protected Sr688ScreenVars sv1 = ScreenProgram.getScreenVars( Sr688ScreenVars.class);
	protected Sr689ScreenVars sv = ScreenProgram.getScreenVars( Sr689ScreenVars.class);
	protected ErrorsInner errorsInner = new ErrorsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090
	}

	public Pr689() {
		super();
		screenVars = sv;
		new ScreenModel("Sr688", AppVars.getInstance(), sv1);
		new ScreenModel("Sr689", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspdocs.userArea = convertAndSetParam(wsspdocs.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspdocs.userArea = convertAndSetParam(wsspdocs.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		retrieveHclh1020();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.amtlife.set(ZERO);
		sv.amtyear.set(ZERO);
		sv.benlmt.set(ZERO);
		sv.copay.set(ZERO);
		sv.zdaycov.set(ZERO);
		sv.daclaim.set(ZERO);
		wsaaDaclaim.set(ZERO);
		sv.gdeduct.set(ZERO);
		sv.gincurr.set(ZERO);
		sv.gtotinc.set(ZERO);
		sv.gcnetpy.set(ZERO);
		wsaaGcnetpy.set(ZERO);
		sv.nofday.set(ZERO);
		sv.tamtyear.set(ZERO);
		sv.tclmamt.set(ZERO);
		sv.amtfld.set(ZERO);
		wsaaCompYear.set(ZERO);
		sv.zunit.set(ZERO);
		sv.datefrom.set(varcom.maxdate);
		wsaaDatefrom.set(varcom.maxdate);
		wsaaDateto.set(varcom.maxdate);
		sv.dateto.set(varcom.maxdate);
		sv.benefits.set(SPACES);
		sv.comt.set(SPACES);
		sv.inqopt.set(SPACES);
		sv.hosben.set(SPACES);
		a100ReadTr386();
		if (isEQ(wsspcomn.flag,"1")){
			sv.inqopt.set(tr386rec.progdesc01);
		}
		else if (isEQ(wsspcomn.flag,"2")){
			sv.inqopt.set(tr386rec.progdesc02);
		}
		else if (isEQ(wsspcomn.flag,"3")){
			sv.inqopt.set(tr386rec.progdesc03);
		}
		else if (isEQ(wsspcomn.flag,"4")){
			sv.inqopt.set(tr386rec.progdesc04);
		}
	}

	/**
	* <pre>
	*    Set screen fields
	* </pre>
	*/
protected void retrieveHclh1020()
	{
		/* Retrieve from HCLH....*/
		/*    IF WSSP-FLAG             NOT = '1'*/
		hclhIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, hclhIO);
		if (isNE(hclhIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hclhIO.getParams());
			fatalError600();
		}
		/*    END-IF.*/
		/* Retrieve from REGP....                                          */
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			syserrrec.statuz.set(regpIO.getStatuz());
			fatalError600();
		}
		/* Get details from PR688*/
		scrnparams.subfileRrn.set(wsspdocs.subfileRrn);
		scrnparams.subfileEnd.set(wsspdocs.subfileEnd);
		scrnparams.function.set(varcom.sread);
		processScreen("SR688", sv1);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* Calculate the component year*/
		datcon3rec.intDate1.set(sv1.crrcd);
		datcon3rec.intDate2.set(sv1.crtdate);
		datcon3rec.frequency.set("01");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		compute(wsaaCompYear, 5).set(add(0.99999,datcon3rec.freqFactor));
		/*  Get Annual Paid amount*/
		hclabanIO.setChdrcoy(hclhIO.getChdrcoy());
		hclabanIO.setChdrnum(hclhIO.getChdrnum());
		hclabanIO.setLife(hclhIO.getLife());
		hclabanIO.setCoverage(hclhIO.getCoverage());
		hclabanIO.setRider(hclhIO.getRider());
		hclabanIO.setHosben(sv1.hosben);
		hclabanIO.setAcumactyr(wsaaCompYear);
		hclabanIO.setFunction(varcom.readr);
		hclabanIO.setFormat(hclabanrec);
		SmartFileCode.execute(appVars, hclabanIO);
		if (isNE(hclabanIO.getStatuz(),varcom.oK)
		&& isNE(hclabanIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hclabanIO.getStatuz());
			syserrrec.params.set(hclabanIO.getParams());
			fatalError600();
		}
		if (isEQ(hclabanIO.getStatuz(),varcom.mrnf)) {
			hclabanIO.setClmpaid(ZERO);
			sv.tamtyear.set(ZERO);
		}
		else {
			sv.tamtyear.set(hclabanIO.getClmpaid());
		}
		/*  Get Life Time Paid amount*/
		hclabltIO.setChdrcoy(hclhIO.getChdrcoy());
		hclabltIO.setChdrnum(hclhIO.getChdrnum());
		hclabltIO.setLife(hclhIO.getLife());
		hclabltIO.setCoverage(hclhIO.getCoverage());
		hclabltIO.setRider(hclhIO.getRider());
		hclabltIO.setHosben(sv1.hosben);
		hclabltIO.setFunction(varcom.readr);
		hclabltIO.setFormat(hclabltrec);
		SmartFileCode.execute(appVars, hclabltIO);
		if (isNE(hclabltIO.getStatuz(),varcom.oK)
		&& isNE(hclabltIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hclabltIO.getStatuz());
			syserrrec.params.set(hclabltIO.getParams());
			fatalError600();
		}
		if (isEQ(hclabltIO.getStatuz(),varcom.mrnf)) {
			hclabltIO.setClmpaid(ZERO);
			sv.tclmamt.set(ZERO);
		}
		else {
			sv.tclmamt.set(hclabltIO.getClmpaid());
		}
		moveToSr6891500();
		if (isNE(wsspcomn.flag,"1")) {
			readHcld1600();
		}
	}

protected void moveToSr6891500()
	{
		/*MOVE-TO-SR689*/
		sv.hosben.set(sv1.hosben);
		sv.benlmt.set(sv1.benfamt);
		sv.nofday.set(sv1.accday);
		sv.gdeduct.set(sv1.mbrdeduc);
		sv.copay.set(sv1.mbrcopay);
		sv.amtyear.set(sv1.bnflmtb);
		sv.amtlife.set(sv1.bnflmta);
		sv.benefits.set(sv1.benefits);
		sv.zunit.set(sv1.coverc);
		/*EXIT*/
	}

protected void readHcld1600()
	{
		begn1610();
	}

protected void begn1610()
	{
		hcldIO.setChdrcoy(hclhIO.getChdrcoy());
		hcldIO.setChdrnum(hclhIO.getChdrnum());
		hcldIO.setLife(hclhIO.getLife());
		hcldIO.setCoverage(hclhIO.getCoverage());
		hcldIO.setRider(hclhIO.getRider());
		hcldIO.setRgpynum(hclhIO.getRgpynum());
		hcldIO.setHosben(sv1.hosben);
		hcldIO.setFunction(varcom.readr);
		hcldIO.setFormat(hcldrec);
		SmartFileCode.execute(appVars, hcldIO);
		if (isNE(hcldIO.getStatuz(),varcom.oK)
		&& isNE(hcldIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hcldIO.getParams());
			syserrrec.statuz.set(hcldIO.getStatuz());
			fatalError600();
		}
		if (isEQ(hcldIO.getStatuz(),varcom.oK)) {
			sv.gincurr.set(hcldIO.getActexp());
			sv.daclaim.set(hcldIO.getNofday());
			wsaaDaclaim.set(hcldIO.getNofday());
			sv.datefrom.set(hcldIO.getDatefrm());
			sv.dateto.set(hcldIO.getDateto());
			sv.gcnetpy.set(hcldIO.getGcnetpy());
			wsaaGcnetpy.set(hcldIO.getGcnetpy());
			sv.amtfld.set(hcldIO.getAmtfld());
			sv.gdeduct.set(hcldIO.getGdeduct());
		}
	}

protected void a100ReadTr386()
	{
		a100Begin();
	}

protected void a100Begin()
	{
		initialize(wsaaTr386Key);
		wsaaTr386Language.set(wsspcomn.language);
		wsaaTr386ProgId.set(wsaaProg);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag,"3")
		|| isEQ(wsspcomn.flag,"4")) {
			protectScr2100();
		}
		if (isEQ(sv1.accday,ZERO)) {
			sv.daclaimOut[varcom.pr.toInt()].set("Y");
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		/*    Validate fields*/
		if (isEQ(wsspcomn.flag,"3")
		|| isEQ(wsspcomn.flag,"4")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.datefrom,ZERO)
		|| isEQ(sv.datefrom,99999999)) {
			sv.datefromErr.set(errorsInner.f665);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.dateto,ZERO)
		|| isEQ(sv.dateto,99999999)) {
			sv.datetoErr.set(errorsInner.f665);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isGT(sv.datefrom,sv.dateto)) {
			sv.datefromErr.set(errorsInner.e017);
		}
		/* Get Todays Date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,"****")) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		if (isGT(sv.datefrom,datcon1rec.intDate)) {
			sv.datefromErr.set(errorsInner.f073);
		}
		if (isGT(sv.dateto,datcon1rec.intDate)) {
			sv.datetoErr.set(errorsInner.f073);
		}
		if (isLT(sv.datefrom,hclhIO.getIncurdt())) {
			sv.datefromErr.set(errorsInner.rphx);
		}
		if (isLT(sv.dateto,hclhIO.getIncurdt())) {
			sv.datetoErr.set(errorsInner.rphx);
		}
		if (isNE(hclhIO.getGcadmdt(),ZERO)
		&& isNE(hclhIO.getGcadmdt(),99999999)) {
			if (isLT(sv.datefrom,hclhIO.getGcadmdt())) {
				sv.datefromErr.set(errorsInner.rphy);
			}
			if (isLT(sv.dateto,hclhIO.getGcadmdt())) {
				sv.datetoErr.set(errorsInner.rphy);
			}
		}
		if (isNE(hclhIO.getDischdt(),ZERO)
		&& isNE(hclhIO.getDischdt(),99999999)) {
			/*          IF SR689-DATEFROM            < HCLH-DISCHDT            */
			if (isGT(sv.datefrom,hclhIO.getDischdt())) {
				sv.datefromErr.set(errorsInner.rphz);
			}
			/*          IF SR689-DATETO              < HCLH-DISCHDT            */
			if (isGT(sv.dateto,hclhIO.getDischdt())) {
				sv.datetoErr.set(errorsInner.rphz);
			}
		}
		if (isEQ(sv.gincurr,ZERO)) {
			sv.gincurrErr.set(errorsInner.w321);
		}
		if (isNE(sv1.accday,ZERO)) {
			if (isEQ(sv.daclaim,ZERO)) {
				sv.daclaimErr.set(errorsInner.e186);
			}
		}
		/* Per-claim level payable ..*/
		claimLevel2200();
		/* Annual level payable ..*/
		annualLevel2300();
		/* Life time level payable ..*/
		lifetimeLevel2400();
		if (isNE(sv.gcnetpy,ZERO)) {
			compute(wsaaMaxBen, 2).set(mult(sv.benlmt,sv.zdaycov));
			if (isNE(wsaaMaxBen,ZERO)) {
				if (isGT(sv.gcnetpy,wsaaMaxBen)) {
					sv.gcnetpyErr.set(errorsInner.rpia);
				}
			}
			if (isNE(wsaaAnlBal,ZERO)) {
				if (isGT(sv.gcnetpy,wsaaAnlBal)) {
					sv.gcnetpyErr.set(errorsInner.rpib);
				}
			}
			if (isNE(wsaaLifeBal,ZERO)) {
				if (isGT(sv.gcnetpy,wsaaLifeBal)) {
					sv.gcnetpyErr.set(errorsInner.rpic);
				}
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			return ;
		}
		/* Check for screen changes ..*/
		scrnchg2500();
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	* </pre>
	*/
protected void protectScr2100()
	{
		/*PROTECT*/
		sv.datefromOut[varcom.pr.toInt()].set("Y");
		sv.datetoOut[varcom.pr.toInt()].set("Y");
		sv.gincurrOut[varcom.pr.toInt()].set("Y");
		sv.gcnetpyOut[varcom.pr.toInt()].set("Y");
		sv.daclaimOut[varcom.pr.toInt()].set("Y");
		/*EXIT*/
	}

protected void claimLevel2200()
	{
		claim2210();
	}

protected void claim2210()
	{
		/* Covered Days..*/
		if (isNE(sv.nofday,ZERO)) {
			if (isNE(sv.daclaim,ZERO)
			&& isGT(sv.daclaim,sv.nofday)) {
				sv.zdaycov.set(sv.nofday);
			}
			else {
				sv.zdaycov.set(sv.daclaim);
			}
		}
		else {
			sv.zdaycov.set(sv.daclaim);
		}
		/* Covered amount ..*/
		if (isNE(sv.benlmt,ZERO)) {
			if (isGT(sv.gincurr,sv.benlmt)) {
				sv.gtotinc.set(sv.benlmt);
			}
			else {
				sv.gtotinc.set(sv.gincurr);
			}
		}
		if (isNE(sv.zdaycov,ZERO)) {
			compute(wsaaClmlvlA, 2).set(mult(sv.zdaycov,sv.gtotinc));
		}
		else {
			wsaaClmlvlA.set(sv.gtotinc);
		}
		compute(wsaaClmlvlB, 0).set(sub(wsaaClmlvlA,sv.gdeduct));
		compute(wsaaClmlvlC, 2).set(div(sv.copay,100));
		compute(wsaaCopayAmt, 2).set(mult(wsaaClmlvlB,wsaaClmlvlC));
		compute(wsaaClmlvlAmt, 2).set(sub(wsaaClmlvlB,wsaaCopayAmt));
		zrdecplrec.amountIn.set(wsaaClmlvlAmt);
		a000CallRounding();
		wsaaClmlvlAmt.set(zrdecplrec.amountOut);
		sv.amtfld.set(wsaaClmlvlAmt);
		if (isEQ(sv.gcnetpy,ZERO)
		|| isNE(wsaaClmlvlAmt,wsaaClmlvlAmtsav)) {
			sv.gcnetpy.set(wsaaClmlvlAmt);
			wsaaClmlvlAmtsav.set(wsaaClmlvlAmt);
		}
	}

protected void annualLevel2300()
	{
		/*CLAIM*/
		if (isNE(hclabanIO.getClmpaid(),ZERO)) {
			compute(wsaaAnllvlA, 2).set(add(hclabanIO.getClmpaid(),wsaaClmlvlAmt));
			if (isGT(wsaaAnllvlA,sv.amtyear)) {
				compute(sv.tamtyear, 2).set(sub(sv.amtyear,hclabanIO.getClmpaid()));
			}
			else {
				sv.tamtyear.set(wsaaClmlvlAmt);
			}
			compute(wsaaAnlBal, 2).set(sub(sv.amtyear,hclabanIO.getClmpaid()));
		}
		else {
			sv.tamtyear.set(ZERO);
			wsaaAnlBal.set(ZERO);
		}
		/*EXIT*/
	}

protected void lifetimeLevel2400()
	{
		/*CLAIM*/
		if (isNE(hclabltIO.getClmpaid(),ZERO)) {
			compute(wsaaLftlvlA, 2).set(add(hclabltIO.getClmpaid(),sv.tamtyear));
			if (isGT(wsaaLftlvlA,sv.amtlife)) {
				/*       COMPUTE SR689-AMTFLD        = SR689-AMTLIFE*/
				compute(sv.tclmamt, 2).set(sub(sv.amtlife,hclabltIO.getClmpaid()));
			}
			else {
				sv.tclmamt.set(sv.tamtyear);
			}
			compute(wsaaLifeBal, 2).set(sub(sv.amtlife,hclabltIO.getClmpaid()));
		}
		else {
			sv.tclmamt.set(ZERO);
			wsaaLifeBal.set(ZERO);
		}
		/*EXIT*/
	}

protected void scrnchg2500()
	{
		scrn2510();
	}

protected void scrn2510()
	{
		if (isNE(sv.datefrom,wsaaDatefrom)) {
			wsaaDatefrom.set(sv.datefrom);
		}
		if (isNE(sv.dateto,wsaaDateto)) {
			wsaaDateto.set(sv.dateto);
		}
		if (isNE(sv.gincurr,wsaaGincurr)) {
			wsspcomn.edterror.set("Y");
			wsaaGincurr.set(sv.gincurr);
		}
		if (isNE(sv.daclaim,wsaaDaclaim)) {
			wsspcomn.edterror.set("Y");
			wsaaDaclaim.set(sv.daclaim);
		}
		if (isNE(sv.gcnetpy,wsaaGcnetpy)) {
			wsspcomn.edterror.set("Y");
			wsaaGcnetpy.set(sv.gcnetpy);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		if (isEQ(wsspcomn.flag,"4")) {
			return ;
		}
		if (isEQ(wsspcomn.flag,"3")) {
			hcldIO.setFunction(varcom.deltd);
			hcldIO.setFormat(hcldrec);
			SmartFileCode.execute(appVars, hcldIO);
			if (isNE(hcldIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hcldIO.getParams());
				syserrrec.statuz.set(hcldIO.getStatuz());
				fatalError600();
			}
			return ;
		}
		hcldIO.setChdrcoy(hclhIO.getChdrcoy());
		hcldIO.setChdrnum(hclhIO.getChdrnum());
		hcldIO.setLife(hclhIO.getLife());
		hcldIO.setCoverage(hclhIO.getCoverage());
		hcldIO.setRider(hclhIO.getRider());
		hcldIO.setCrtable(hclhIO.getCrtable());
		hcldIO.setRgpynum(hclhIO.getRgpynum());
		hcldIO.setBenpln(hclhIO.getBenpln());
		hcldIO.setHosben(sv.hosben);
		hcldIO.setDatefrm(sv.datefrom);
		hcldIO.setDateto(sv.dateto);
		hcldIO.setActexp(sv.gincurr);
		hcldIO.setNofday(sv.daclaim);
		hcldIO.setBenfamt(sv.benlmt);
		hcldIO.setZdaycov(sv.zdaycov);
		hcldIO.setGdeduct(sv.gdeduct);
		hcldIO.setCoiamt(wsaaCopayAmt);
		hcldIO.setGcnetpy(sv.gcnetpy);
		hcldIO.setAmtfld(sv.amtfld);
		if (isNE(sv.amtyear,0)) {
			hcldIO.setLmtyear("Y");
		}
		else {
			hcldIO.setLmtyear("N");
		}
		if (isNE(sv.amtlife,0)) {
			hcldIO.setLmtlife("Y");
		}
		else {
			hcldIO.setLmtlife("N");
		}
		if (isEQ(wsspcomn.flag,"1")) {
			hcldIO.setFunction(varcom.writr);
		}
		else {
			hcldIO.setFunction(varcom.writd);
		}
		hcldIO.setFormat(hcldrec);
		SmartFileCode.execute(appVars, hcldIO);
		if (isNE(hcldIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcldIO.getParams());
			syserrrec.statuz.set(hcldIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		scrnparams.function.set("HIDEW");
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		/* MOVE SR689-GINCURR          TO ZRDP-CURRENCY.                */
		zrdecplrec.currency.set(regpIO.getCurrcd());
		zrdecplrec.batctrcde.set("****");
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
public static final class ErrorsInner { 
	private FixedLengthStringData e017 = new FixedLengthStringData(4).init("E017");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData f665 = new FixedLengthStringData(4).init("F665");
	private FixedLengthStringData rphx = new FixedLengthStringData(4).init("RPHX");
	private FixedLengthStringData rphy = new FixedLengthStringData(4).init("RPHY");
	private FixedLengthStringData rphz = new FixedLengthStringData(4).init("RPHZ");
	public FixedLengthStringData rpia = new FixedLengthStringData(4).init("RPIA");
	public FixedLengthStringData rpib = new FixedLengthStringData(4).init("RPIB");
	public FixedLengthStringData rpic = new FixedLengthStringData(4).init("RPIC");
	private FixedLengthStringData w321 = new FixedLengthStringData(4).init("W321");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
}
}
