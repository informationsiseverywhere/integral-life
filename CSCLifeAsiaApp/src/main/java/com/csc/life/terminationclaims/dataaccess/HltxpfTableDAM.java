package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HltxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:31
 * Class transformed from HLTXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HltxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 469;
	public FixedLengthStringData hltxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hltxpfRecord = hltxrec;
	
	public FixedLengthStringData clntcoy = DD.clntcoy.copy().isAPartOf(hltxrec);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(hltxrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hltxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hltxrec);
	public FixedLengthStringData requestCompany = DD.reqcoy.copy().isAPartOf(hltxrec);
	public FixedLengthStringData letterType = DD.lettype.copy().isAPartOf(hltxrec);
	public PackedDecimalData letterSeqno = DD.letseqno.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hlngchar01 = DD.hlngchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hlngchar02 = DD.hlngchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hlngchar03 = DD.hlngchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hlngchar04 = DD.hlngchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hlngchar05 = DD.hlngchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hlngchar06 = DD.hlngchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hlngchar07 = DD.hlngchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hlngchar08 = DD.hlngchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hshtchar01 = DD.hshtchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hshtchar02 = DD.hshtchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hshtchar03 = DD.hshtchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hshtchar04 = DD.hshtchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hshtchar05 = DD.hshtchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hshtchar06 = DD.hshtchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hshtchar07 = DD.hshtchar.copy().isAPartOf(hltxrec);
	public FixedLengthStringData hshtchar08 = DD.hshtchar.copy().isAPartOf(hltxrec);
	public PackedDecimalData hnumfld01 = DD.hnumfld.copy().isAPartOf(hltxrec);
	public PackedDecimalData hnumfld02 = DD.hnumfld.copy().isAPartOf(hltxrec);
	public PackedDecimalData hnumfld03 = DD.hnumfld.copy().isAPartOf(hltxrec);
	public PackedDecimalData hnumfld04 = DD.hnumfld.copy().isAPartOf(hltxrec);
	public PackedDecimalData hnumfld05 = DD.hnumfld.copy().isAPartOf(hltxrec);
	public PackedDecimalData hnumfld06 = DD.hnumfld.copy().isAPartOf(hltxrec);
	public PackedDecimalData hnumfld07 = DD.hnumfld.copy().isAPartOf(hltxrec);
	public PackedDecimalData hnumfld08 = DD.hnumfld.copy().isAPartOf(hltxrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hltxrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hltxrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hltxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HltxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HltxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HltxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HltxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HltxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HltxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HltxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HLTXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CLNTCOY, " +
							"CLNTNUM, " +
							"CHDRCOY, " +
							"CHDRNUM, " +
							"REQCOY, " +
							"LETTYPE, " +
							"LETSEQNO, " +
							"HLNGCHAR01, " +
							"HLNGCHAR02, " +
							"HLNGCHAR03, " +
							"HLNGCHAR04, " +
							"HLNGCHAR05, " +
							"HLNGCHAR06, " +
							"HLNGCHAR07, " +
							"HLNGCHAR08, " +
							"HSHTCHAR01, " +
							"HSHTCHAR02, " +
							"HSHTCHAR03, " +
							"HSHTCHAR04, " +
							"HSHTCHAR05, " +
							"HSHTCHAR06, " +
							"HSHTCHAR07, " +
							"HSHTCHAR08, " +
							"HNUMFLD01, " +
							"HNUMFLD02, " +
							"HNUMFLD03, " +
							"HNUMFLD04, " +
							"HNUMFLD05, " +
							"HNUMFLD06, " +
							"HNUMFLD07, " +
							"HNUMFLD08, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     clntcoy,
                                     clntnum,
                                     chdrcoy,
                                     chdrnum,
                                     requestCompany,
                                     letterType,
                                     letterSeqno,
                                     hlngchar01,
                                     hlngchar02,
                                     hlngchar03,
                                     hlngchar04,
                                     hlngchar05,
                                     hlngchar06,
                                     hlngchar07,
                                     hlngchar08,
                                     hshtchar01,
                                     hshtchar02,
                                     hshtchar03,
                                     hshtchar04,
                                     hshtchar05,
                                     hshtchar06,
                                     hshtchar07,
                                     hshtchar08,
                                     hnumfld01,
                                     hnumfld02,
                                     hnumfld03,
                                     hnumfld04,
                                     hnumfld05,
                                     hnumfld06,
                                     hnumfld07,
                                     hnumfld08,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		clntcoy.clear();
  		clntnum.clear();
  		chdrcoy.clear();
  		chdrnum.clear();
  		requestCompany.clear();
  		letterType.clear();
  		letterSeqno.clear();
  		hlngchar01.clear();
  		hlngchar02.clear();
  		hlngchar03.clear();
  		hlngchar04.clear();
  		hlngchar05.clear();
  		hlngchar06.clear();
  		hlngchar07.clear();
  		hlngchar08.clear();
  		hshtchar01.clear();
  		hshtchar02.clear();
  		hshtchar03.clear();
  		hshtchar04.clear();
  		hshtchar05.clear();
  		hshtchar06.clear();
  		hshtchar07.clear();
  		hshtchar08.clear();
  		hnumfld01.clear();
  		hnumfld02.clear();
  		hnumfld03.clear();
  		hnumfld04.clear();
  		hnumfld05.clear();
  		hnumfld06.clear();
  		hnumfld07.clear();
  		hnumfld08.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHltxrec() {
  		return hltxrec;
	}

	public FixedLengthStringData getHltxpfRecord() {
  		return hltxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHltxrec(what);
	}

	public void setHltxrec(Object what) {
  		this.hltxrec.set(what);
	}

	public void setHltxpfRecord(Object what) {
  		this.hltxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hltxrec.getLength());
		result.set(hltxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}