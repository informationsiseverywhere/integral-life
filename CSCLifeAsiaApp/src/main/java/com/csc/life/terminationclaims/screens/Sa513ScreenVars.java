package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.StringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.datadictionarydatatype.FLSDDObj;
import com.quipoz.framework.datatype.*;

/**
 * Screen variables for SR50D
 * @version 1.0 generated on 30/08/09 07:14
 * @author Quipoz
 */
public class Sa513ScreenVars extends SmartVarModel { 


		//upper part
		public FixedLengthStringData dataArea = new FixedLengthStringData(214);		
		public FixedLengthStringData dataFields = new FixedLengthStringData(98).isAPartOf(dataArea, 0);
		//Notification Number
		public FixedLengthStringData notifnum = DD.aacct.copy().isAPartOf(dataFields,0);
		public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,14);
		public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,22);
		public FixedLengthStringData claimant = DD.claimant.copy().isAPartOf(dataFields,69);
		public FixedLengthStringData clamnme = DD.clamnme.copy().isAPartOf(dataFields,77);
		public FixedLengthStringData relation = DD.relation.copy().isAPartOf(dataFields,96);
			
		public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 98);
		public FixedLengthStringData notifnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
		public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
		public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
		public FixedLengthStringData claimantErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
		public FixedLengthStringData clamnmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
		public FixedLengthStringData relationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
		
		public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 122);
		public FixedLengthStringData[] notifnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
		public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
		public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
		public FixedLengthStringData[] claimantOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
		public FixedLengthStringData[] clamnmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
		public FixedLengthStringData[] relationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
		//lower part
		public FixedLengthStringData subfileArea = new FixedLengthStringData(198);
		public FixedLengthStringData subfileFields = new FixedLengthStringData(83).isAPartOf(subfileArea, 0);
		//Select
		public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,0);
		//Transaction Number
		public FixedLengthStringData tranno = DD.transno.copy().isAPartOf(subfileFields,1);
		//Transaction Date
		public ZonedDecimalData trdate = DD.effdates.copyToZonedDecimal().isAPartOf(subfileFields,6);
		//Effective Date
		public ZonedDecimalData effdates = DD.effdates.copyToZonedDecimal().isAPartOf(subfileFields,16);
		//Transaction Code
		public FixedLengthStringData trancd = DD.trancd.copy().isAPartOf(subfileFields,26);
		//Transaction Description
		public FixedLengthStringData trandes = DD.trandes.copy().isAPartOf(subfileFields,30);
		//UserID
		public FixedLengthStringData userid = DD.userid.copy().isAPartOf(subfileFields,73);
		
		public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 83);
		public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
		public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
		public FixedLengthStringData trdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
		public FixedLengthStringData effdatesErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
		public FixedLengthStringData trancdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
		public FixedLengthStringData trandesErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
		public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
		
		public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 111);
		public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
		public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
		public FixedLengthStringData[] trdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
		public FixedLengthStringData[] effdatesOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
		public FixedLengthStringData[] trancdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
		public FixedLengthStringData[] trandesOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
		public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
		
		public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 195);
			
		public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
		public FixedLengthStringData transDateDisp = new FixedLengthStringData(10);
		public FixedLengthStringData effDateDisp = new FixedLengthStringData(10);
		
		public LongData Sa513screensflWritten = new LongData(0);
		public LongData Sa513screenctlWritten = new LongData(0);
		public LongData Sa513screenWritten = new LongData(0);
		public LongData Sa513protectWritten = new LongData(0);
		public GeneralTable sa513screensfl = new GeneralTable(AppVars.getInstance());


		public boolean hasSubfile() {
			return false;
		}


		public Sa513ScreenVars() {
			super();
			initialiseScreenVars();
		}

		protected void initialiseScreenVars() {
			fieldIndMap.put(selectOut,new String[] {"02","04","-02","03",null, null, null, null, null, null, null, null});
			fieldIndMap.put(trannoOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null, null});
			fieldIndMap.put(trdateOut,new String[] {null, null, null, "06", null, null, null, null, null, null, null, null});
			fieldIndMap.put(effdatesOut,new String[] {null, null, null, "07", null, null, null, null, null, null, null, null});
			fieldIndMap.put(trancdOut,new String[] {null, null, null, "08", null, null, null, null, null, null, null, null});
			fieldIndMap.put(trandesOut,new String[] {"09",null, "09",null, null, null, null, null, null, null, null, null});
			fieldIndMap.put(useridOut,new String[] {"10",null, "10",null, null, null, null, null, null, null, null, null});
			//ILIFE-1138 STARTS
			screenSflFields = new BaseData[] {select, tranno, trdate, effdates, trancd, trandes, userid};
			screenSflOutFields = new BaseData[][] {selectOut, trannoOut, trdateOut, effdatesOut, trancdOut, trandesOut, useridOut};
			screenSflErrFields = new BaseData[] {selectErr, trannoErr, trdateErr, effdatesErr, trancdErr, trandesErr, useridErr};
			//ILIFE-1138 ENDS
			screenSflDateFields = new BaseData[] {trdate,effdates};
			screenSflDateErrFields = new BaseData[] {trdateErr,effdatesErr};
			screenSflDateDispFields = new BaseData[] {transDateDisp,effDateDisp};
			
			screenFields = new BaseData[] {notifnum,lifcnum, lifename,claimant,clamnme,relation};
			screenOutFields = new BaseData[][] {notifnumOut,lifcnumOut, lifenameOut,claimantOut,clamnmeOut,relationOut};
			screenErrFields = new BaseData[] {notifnumErr,lifcnumErr, lifenameErr,claimantErr,clamnmeErr,relationErr};
			screenDateFields = new BaseData[] {};
			screenDateErrFields = new BaseData[] {};
			screenDateDispFields = new BaseData[] {};
			
			screenDataArea = dataArea;
			screenSubfileArea = subfileArea;
			screenSflIndicators = screenIndicArea;
			errorInds = errorIndicators;
			errorSflInds = errorSubfile;
			screenRecord = Sa513screen.class;
			screenSflRecord = Sa513screensfl.class;
			screenCtlRecord = Sa513screenctl.class;
			initialiseSubfileArea();
			protectRecord = Sa513protect.class;
			
		}
		public void initialiseSubfileArea() {
			initialize(screenSubfileArea);
			subfilePage.set(Sa513screenctl.lrec.pageSubfile);
		}
		
}
