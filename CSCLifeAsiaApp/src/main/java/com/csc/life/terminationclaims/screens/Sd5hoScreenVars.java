package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sd5hoScreenVars  extends SmartVarModel {
	public FixedLengthStringData dataArea = new FixedLengthStringData(968);
	public FixedLengthStringData dataFields = new FixedLengthStringData(224).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData remamons = new FixedLengthStringData(60).isAPartOf(dataFields, 44);
  	public ZonedDecimalData[] remamon = ZDArrayPartOfStructure(20, 3, 0, remamons, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(remamons, 0, FILLER_REDEFINE);
  	public ZonedDecimalData remamon01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData remamon02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData remamon03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData remamon04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData remamon05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData remamon06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData remamon07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData remamon08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData remamon09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData remamon10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public ZonedDecimalData remamon11 = new ZonedDecimalData(3, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData remamon12 = new ZonedDecimalData(3, 0).isAPartOf(filler, 33);
  	public ZonedDecimalData remamon13 = new ZonedDecimalData(3, 0).isAPartOf(filler, 36);
  	public ZonedDecimalData remamon14 = new ZonedDecimalData(3, 0).isAPartOf(filler, 39);
  	public ZonedDecimalData remamon15 = new ZonedDecimalData(3, 0).isAPartOf(filler, 42);
  	public ZonedDecimalData remamon16 = new ZonedDecimalData(3, 0).isAPartOf(filler, 45);
  	public ZonedDecimalData remamon17 = new ZonedDecimalData(3, 0).isAPartOf(filler, 48);
  	public ZonedDecimalData remamon18 = new ZonedDecimalData(3, 0).isAPartOf(filler, 51);
  	public ZonedDecimalData remamon19 = new ZonedDecimalData(3, 0).isAPartOf(filler, 54);
  	public ZonedDecimalData remamon20 = new ZonedDecimalData(3, 0).isAPartOf(filler, 57);
  	public FixedLengthStringData rates = new FixedLengthStringData(120).isAPartOf(dataFields, 104);
  	public ZonedDecimalData[] rate = ZDArrayPartOfStructure(20, 6, 4, rates, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(120).isAPartOf(rates, 0, FILLER_REDEFINE);
	public ZonedDecimalData rate01 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 0);
	public ZonedDecimalData rate02 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 6);
	public ZonedDecimalData rate03 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 12);
	public ZonedDecimalData rate04 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 18);
	public ZonedDecimalData rate05 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 24);
	public ZonedDecimalData rate06 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 30);
	public ZonedDecimalData rate07 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 36);
	public ZonedDecimalData rate08 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 42);
	public ZonedDecimalData rate09 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 48);
	public ZonedDecimalData rate10 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 54);
	public ZonedDecimalData rate11 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 60);
	public ZonedDecimalData rate12 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 66);
	public ZonedDecimalData rate13 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 72);
	public ZonedDecimalData rate14 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 78);
	public ZonedDecimalData rate15 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 84);
	public ZonedDecimalData rate16 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 90);
	public ZonedDecimalData rate17 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 96);
	public ZonedDecimalData rate18 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 102);
	public ZonedDecimalData rate19 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 108);
	public ZonedDecimalData rate20 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 114);
  	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(176).isAPartOf(dataArea, 224);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData remamonsErr = new FixedLengthStringData(80).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] remamonErr = FLSArrayPartOfStructure(20, 4, remamonsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(80).isAPartOf(remamonsErr, 0, FILLER_REDEFINE);
  	public FixedLengthStringData remamon01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
  	public FixedLengthStringData remamon02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
  	public FixedLengthStringData remamon03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
  	public FixedLengthStringData remamon04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
  	public FixedLengthStringData remamon05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
  	public FixedLengthStringData remamon06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
  	public FixedLengthStringData remamon07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
  	public FixedLengthStringData remamon08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
  	public FixedLengthStringData remamon09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
  	public FixedLengthStringData remamon10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
  	public FixedLengthStringData remamon11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
  	public FixedLengthStringData remamon12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
  	public FixedLengthStringData remamon13Err = new FixedLengthStringData(4).isAPartOf(filler2, 48);
  	public FixedLengthStringData remamon14Err = new FixedLengthStringData(4).isAPartOf(filler2, 52);
  	public FixedLengthStringData remamon15Err = new FixedLengthStringData(4).isAPartOf(filler2, 56);
  	public FixedLengthStringData remamon16Err = new FixedLengthStringData(4).isAPartOf(filler2, 60);
  	public FixedLengthStringData remamon17Err = new FixedLengthStringData(4).isAPartOf(filler2, 64);
  	public FixedLengthStringData remamon18Err = new FixedLengthStringData(4).isAPartOf(filler2, 68);
  	public FixedLengthStringData remamon19Err = new FixedLengthStringData(4).isAPartOf(filler2, 72);
  	public FixedLengthStringData remamon20Err = new FixedLengthStringData(4).isAPartOf(filler2, 76);
  	public FixedLengthStringData ratesErr = new FixedLengthStringData(80).isAPartOf(errorIndicators, 96);
  	public FixedLengthStringData[] rateErr = FLSArrayPartOfStructure(20, 4, ratesErr, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(80).isAPartOf(ratesErr, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rate01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
  	public FixedLengthStringData rate02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
  	public FixedLengthStringData rate03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
  	public FixedLengthStringData rate04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
  	public FixedLengthStringData rate05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData rate06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
  	public FixedLengthStringData rate07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
  	public FixedLengthStringData rate08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
  	public FixedLengthStringData rate09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
  	public FixedLengthStringData rate10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData rate11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
  	public FixedLengthStringData rate12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
  	public FixedLengthStringData rate13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
  	public FixedLengthStringData rate14Err = new FixedLengthStringData(4).isAPartOf(filler3, 52);
  	public FixedLengthStringData rate15Err = new FixedLengthStringData(4).isAPartOf(filler3, 56);
	public FixedLengthStringData rate16Err = new FixedLengthStringData(4).isAPartOf(filler3, 60);
  	public FixedLengthStringData rate17Err = new FixedLengthStringData(4).isAPartOf(filler3, 64);
  	public FixedLengthStringData rate18Err = new FixedLengthStringData(4).isAPartOf(filler3, 68);
  	public FixedLengthStringData rate19Err = new FixedLengthStringData(4).isAPartOf(filler3, 72);
  	public FixedLengthStringData rate20Err = new FixedLengthStringData(4).isAPartOf(filler3, 76);
  	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(528).isAPartOf(dataArea, 400);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData remamonsOut = new FixedLengthStringData(240).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] remamonOut = FLSArrayPartOfStructure(20, 12, remamonsOut, 0);
	public FixedLengthStringData[][] remamonO = FLSDArrayPartOfArrayStructure(12, 1, remamonOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(240).isAPartOf(remamonsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] remamon01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] remamon02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] remamon03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] remamon04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] remamon05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] remamon06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] remamon07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] remamon08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] remamon09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] remamon10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] remamon11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] remamon12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] remamon13Out = FLSArrayPartOfStructure(12, 1, filler4, 144);
	public FixedLengthStringData[] remamon14Out = FLSArrayPartOfStructure(12, 1, filler4, 156);
	public FixedLengthStringData[] remamon15Out = FLSArrayPartOfStructure(12, 1, filler4, 168);
	public FixedLengthStringData[] remamon16Out = FLSArrayPartOfStructure(12, 1, filler4, 180);
	public FixedLengthStringData[] remamon17Out = FLSArrayPartOfStructure(12, 1, filler4, 192);
	public FixedLengthStringData[] remamon18Out = FLSArrayPartOfStructure(12, 1, filler4, 204);
	public FixedLengthStringData[] remamon19Out = FLSArrayPartOfStructure(12, 1, filler4, 216);
	public FixedLengthStringData[] remamon20Out = FLSArrayPartOfStructure(12, 1, filler4, 228);
	public FixedLengthStringData ratesOut = new FixedLengthStringData(240).isAPartOf(outputIndicators, 288);
	public FixedLengthStringData[] rateOut = FLSArrayPartOfStructure(20, 12, ratesOut, 0);
	public FixedLengthStringData[][] rateO = FLSDArrayPartOfArrayStructure(12, 1, rateOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(240).isAPartOf(ratesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rate01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] rate02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] rate03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] rate04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] rate05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] rate06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] rate07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] rate08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] rate09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] rate10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] rate11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] rate12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] rate13Out = FLSArrayPartOfStructure(12, 1, filler5, 144);
	public FixedLengthStringData[] rate14Out = FLSArrayPartOfStructure(12, 1, filler5, 156);
	public FixedLengthStringData[] rate15Out = FLSArrayPartOfStructure(12, 1, filler5, 168);
	public FixedLengthStringData[] rate16Out = FLSArrayPartOfStructure(12, 1, filler5, 180);
	public FixedLengthStringData[] rate17Out = FLSArrayPartOfStructure(12, 1, filler5, 192);
	public FixedLengthStringData[] rate18Out = FLSArrayPartOfStructure(12, 1, filler5, 204);
	public FixedLengthStringData[] rate19Out = FLSArrayPartOfStructure(12, 1, filler5, 216);
	public FixedLengthStringData[] rate20Out = FLSArrayPartOfStructure(12, 1, filler5, 228);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sd5hoscreenWritten = new LongData(0);
	public LongData Sd5hoprotectWritten = new LongData(0);


	public boolean hasSubfile() {
		return false;
	}
	


	public Sd5hoScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {


		screenFields = new BaseData[] {company, item, tabl, longdesc,remamon01,remamon02,remamon03,remamon04,remamon05,remamon06,remamon07,remamon08,remamon09,remamon10,
				remamon11,remamon12,remamon13,remamon14,remamon15,remamon16,remamon17,remamon18,remamon19,remamon20,rate01,rate02,rate03,rate04,rate05,rate06,rate07,rate08,
				rate09,rate10,rate11,rate12,rate13,rate14,rate15,rate16,rate17,rate18,rate19,rate20};
		screenOutFields = new BaseData[][] {companyOut, itemOut, tablOut, longdescOut,remamon01Out,remamon02Out,remamon03Out,remamon04Out,remamon05Out,remamon06Out,
			remamon07Out,remamon08Out,remamon09Out,remamon10Out,remamon11Out,remamon12Out,remamon13Out,remamon14Out,remamon15Out,remamon16Out,remamon17Out,remamon18Out,
			remamon19Out,remamon20Out,rate01Out,rate02Out,rate03Out,rate04Out,rate05Out,rate06Out,rate07Out,rate08Out,rate09Out,rate10Out,rate11Out,rate12Out,rate13Out,
			rate14Out,rate15Out,rate16Out,rate17Out,rate18Out,rate19Out,rate20Out};
		screenErrFields = new BaseData[] {companyErr, itemErr, tablErr, longdescErr, remamon01Err,remamon02Err,remamon03Err,remamon04Err,remamon05Err,remamon06Err,remamon07Err,
				remamon08Err,remamon09Err,remamon10Err,remamon11Err,remamon12Err,remamon13Err,remamon14Err,remamon15Err,remamon16Err,remamon17Err,remamon18Err,remamon19Err,
				remamon20Err,rate01Err,rate02Err,rate03Err,rate04Err,rate05Err,rate06Err,rate07Err,rate08Err,rate09Err,rate10Err,rate11Err,rate12Err,rate13Err,rate14Err,
				rate15Err,rate16Err,rate17Err,rate18Err,rate19Err,rate20Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5hoscreen.class;
		protectRecord = Sd5hoprotect.class;
	}
	

}
