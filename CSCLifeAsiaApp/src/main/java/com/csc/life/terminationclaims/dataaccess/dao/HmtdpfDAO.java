package com.csc.life.terminationclaims.dataaccess.dao;
import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Hmtdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HmtdpfDAO extends BaseDAO<Hmtdpf> {
	
	public boolean insertHmtdpf(List<Hmtdpf> hmtdpfList);
	public Hmtdpf getHmtdData(String chdrcoy,String chdrnum,String life,String coverage,String crtable, String type);

}
