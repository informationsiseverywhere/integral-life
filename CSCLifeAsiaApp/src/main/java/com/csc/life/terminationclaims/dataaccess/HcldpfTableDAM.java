package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HcldpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:25
 * Class transformed from HCLDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HcldpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 129;
	public FixedLengthStringData hcldrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hcldpfRecord = hcldrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hcldrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hcldrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(hcldrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(hcldrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(hcldrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(hcldrec);
	public PackedDecimalData rgpynum = DD.rgpynum.copy().isAPartOf(hcldrec);
	public FixedLengthStringData benpln = DD.benpln.copy().isAPartOf(hcldrec);
	public FixedLengthStringData hosben = DD.hosben.copy().isAPartOf(hcldrec);
	public PackedDecimalData datefrm = DD.datefrm.copy().isAPartOf(hcldrec);
	public PackedDecimalData dateto = DD.dateto.copy().isAPartOf(hcldrec);
	public PackedDecimalData actexp = DD.actexp.copy().isAPartOf(hcldrec);
	public PackedDecimalData nofday = DD.nofday.copy().isAPartOf(hcldrec);
	public PackedDecimalData benfamt = DD.benfamt.copy().isAPartOf(hcldrec);
	public PackedDecimalData zdaycov = DD.zdaycov.copy().isAPartOf(hcldrec);
	public PackedDecimalData gdeduct = DD.gdeduct.copy().isAPartOf(hcldrec);
	public PackedDecimalData coiamt = DD.coiamt.copy().isAPartOf(hcldrec);
	public PackedDecimalData gcnetpy = DD.gcnetpy.copy().isAPartOf(hcldrec);
	public PackedDecimalData amtfld = DD.amtfld.copy().isAPartOf(hcldrec);
	public FixedLengthStringData lmtyear = DD.lmtyear.copy().isAPartOf(hcldrec);
	public FixedLengthStringData lmtlife = DD.lmtlife.copy().isAPartOf(hcldrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hcldrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hcldrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hcldrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HcldpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HcldpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HcldpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HcldpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HcldpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HcldpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HcldpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HCLDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"RGPYNUM, " +
							"BENPLN, " +
							"HOSBEN, " +
							"DATEFRM, " +
							"DATETO, " +
							"ACTEXP, " +
							"NOFDAY, " +
							"BENFAMT, " +
							"ZDAYCOV, " +
							"GDEDUCT, " +
							"COIAMT, " +
							"GCNETPY, " +
							"AMTFLD, " +
							"LMTYEAR, " +
							"LMTLIFE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     crtable,
                                     rgpynum,
                                     benpln,
                                     hosben,
                                     datefrm,
                                     dateto,
                                     actexp,
                                     nofday,
                                     benfamt,
                                     zdaycov,
                                     gdeduct,
                                     coiamt,
                                     gcnetpy,
                                     amtfld,
                                     lmtyear,
                                     lmtlife,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		rgpynum.clear();
  		benpln.clear();
  		hosben.clear();
  		datefrm.clear();
  		dateto.clear();
  		actexp.clear();
  		nofday.clear();
  		benfamt.clear();
  		zdaycov.clear();
  		gdeduct.clear();
  		coiamt.clear();
  		gcnetpy.clear();
  		amtfld.clear();
  		lmtyear.clear();
  		lmtlife.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHcldrec() {
  		return hcldrec;
	}

	public FixedLengthStringData getHcldpfRecord() {
  		return hcldpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHcldrec(what);
	}

	public void setHcldrec(Object what) {
  		this.hcldrec.set(what);
	}

	public void setHcldpfRecord(Object what) {
  		this.hcldpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hcldrec.getLength());
		result.set(hcldrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}