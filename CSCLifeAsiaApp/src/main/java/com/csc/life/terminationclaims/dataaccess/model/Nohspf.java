package com.csc.life.terminationclaims.dataaccess.model;

import java.io.Serializable;

/**
 * 
 * @author ehyrat related databaase table NOHSPF
 *
 */
public class Nohspf implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long uniqueNumber;
	// NOTIHYCOY
	private String notihycoy;
	// NOTIHSNUM
	private String notihsnum;
	// TRANSNO
	private String transno;
	// TRANSCODE
	private String transcode;
	// TRANSDATE
	private String transdate;
	// TRANSDESC
	private String transdesc;
	// EFFECTDATE
	private int effectdate;
	// USRPRF
	private String usrprf;
	//JOBNM
	private String jobnm;
	//DATIME
	private String datime;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getNotihycoy() {
		return notihycoy;
	}
	public void setNotihycoy(String notihycoy) {
		this.notihycoy = notihycoy;
	}
	public String getTransno() {
		return transno;
	}
	public void setTransno(String transno) {
		this.transno = transno;
	}
	public String getTranscode() {
		return transcode;
	}
	public void setTranscode(String transcode) {
		this.transcode = transcode;
	}
	public String getTransdate() {
		return transdate;
	}
	public void setTransdate(String transdate) {
		this.transdate = transdate;
	}
	public String getTransdesc() {
		return transdesc;
	}
	public void setTransdesc(String transdesc) {
		this.transdesc = transdesc;
	}
	public int getEffectdate() {
		return effectdate;
	}
	public void setEffectdate(int effectdate) {
		this.effectdate = effectdate;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public String getNotihsnum() {
		return notihsnum;
	}
	public void setNotihsnum(String notihsnum) {
		this.notihsnum = notihsnum;
	}
	
}
