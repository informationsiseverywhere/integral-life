package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:48
 * Description:
 * Copybook name: REGTSFXKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regtsfxkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regtsfxFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData regtsfxKey = new FixedLengthStringData(64).isAPartOf(regtsfxFileKey, 0, REDEFINE);
  	public FixedLengthStringData regtsfxChdrcoy = new FixedLengthStringData(1).isAPartOf(regtsfxKey, 0);
  	public FixedLengthStringData regtsfxChdrnum = new FixedLengthStringData(8).isAPartOf(regtsfxKey, 1);
  	public FixedLengthStringData regtsfxLife = new FixedLengthStringData(2).isAPartOf(regtsfxKey, 9);
  	public FixedLengthStringData regtsfxCoverage = new FixedLengthStringData(2).isAPartOf(regtsfxKey, 11);
  	public FixedLengthStringData regtsfxRider = new FixedLengthStringData(2).isAPartOf(regtsfxKey, 13);
  	public PackedDecimalData regtsfxPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(regtsfxKey, 15);
  	public PackedDecimalData regtsfxRgpynum = new PackedDecimalData(5, 0).isAPartOf(regtsfxKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(regtsfxKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regtsfxFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regtsfxFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}