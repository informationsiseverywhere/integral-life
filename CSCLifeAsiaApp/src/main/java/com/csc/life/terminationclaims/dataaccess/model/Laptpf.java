package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;

public class Laptpf {
	
	private long unique_number;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int plnsfx;
	private BigDecimal newsuma;
	private String termid;
	private int trdt;
	private int trtm;
	private int user_t;
	private String usrprf;
	private String jobnm;
	
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public BigDecimal getNewsuma() {
		return newsuma;
	}
	public void setNewsuma(BigDecimal newsuma) {
		this.newsuma = newsuma;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public int getTrdt() {
		return trdt;
	}
	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}
	public int getTrtm() {
		return trtm;
	}
	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}
	public int getUser_t() {
		return user_t;
	}
	public void setUser_t(int user_t) {
		this.user_t = user_t;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}


}
