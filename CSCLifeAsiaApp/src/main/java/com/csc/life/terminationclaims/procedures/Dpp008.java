package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.terminationclaims.dataaccess.ClmddppTableDAM;
import com.csc.life.terminationclaims.recordstructures.Dthcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZutrpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  PROCESSING FOR BID VALUES + SUM ASSURED
*  This program is called for each detail record (from the online
*  screen.
*  If the passed record is for a FUND i.e. DTHP-FIELD-TYPE = 'I' or
*  'A' then write a UTRN for 100% Surrender. The UTRN to contain
*  trigger module DTHADD4.
*  If the ACTUAL amount is not 0 post from O/S CLAIMS to
*  DEATHCLAIMS for the amount. For the record to have any positive
*  amount in the ACTUAL figure suggests no Unit Holdings exist
*  (processed or unprocessed).
*
* ORIGINAL PROGRAM SPECIFICATION
* ------------------------------
*
*       SPECIFICATION FOR DEATH PROCESSING METHOD No. 8
*       -----------------------------------------------
*
*   This program is an item entry on T6598, the death claim
*  subroutine method table. This method is used in order to
*  create a UTRN which, at a later date, will update the
*  sub-accounts.
*
*    The following is the linkage information passed to this
*  subroutine:-
*
*             - company
*             - contract header number
*             - life number
*             - joint-life number
*             - coverage
*             - rider
*             - crtable
*             - effective date
*             - estimated value
*             - actual value
*             - currency
*             - element code
*             - type code
*             - status
*             - batch key
*
*   PROCESSING.
*   ----------
*    Access T6647, this indicates whether Now or Deferred
*  processing is required, check the Deallocation field for
*  'N' or 'D'.
*
*    From the data passed in the linkage section, check
*  whether it is a type 'F' or a type 'S' for fund.
*
*    For type 'F', then perform the following:
*
*    If the estimated amount is zero, do not write UTRN
*  records.
*
*   Write a UTRN record with the following data, (this UTRN
*  will be used to update the outstanding claims (O/S)
*  sub-account):-
*
*        Processing Seq No. -  from T6647
*        All Indicator      -  'Y'
*        Amount (signed)    -  0
*        Effective date     -  Effective-date of the claim
*        Bid/Offer Ind      -  'B'
*        Now/Deferred Ind   -  T6647 entry
*        Price              -  0
*        Fund               -  linkage (element code)
*        Number of Units    -  0
*        Contract No.       -  linkage
*        Contract Type      -  linkage
*        Sub-Account Code   -  T5645 entry for this sub-account
*        Sub-Account Type   -  ditto
*        G/L key            -  ditto
*        Trigger module     -  T5687/T6598 entry
*        Trigger key        -  Contract no./Life/Coverage/Rider
*
*
*   For type 'S', then perform the following:
*
*    Read all the CLMD records for that coverage/rider and
*  accumulate the Estimated amounts and accumulate the Actual
*  amounts of 'F' type records.
*
*  If the sum of the estimated amounts is not zero, then skip
*  all further processing.
*
*  If the accumulated estimated amount is equal to zero, then
*  the claim amount is calculated as follows (read the COVR
*  records and accumulate the Sum Insured (SI) amount), the SI
*  amount.
*
*  If the claim amount is less than zero, then set the claim
*  amount to zero.
*
*  Update the 'S' type record with the Estimate amount equal
*  to zero and set the Actual amount to the claim amount.
*
*  if the claim amount is not zero, then post to the Death
*  Claim and to the O/S Claim sub-accounts.
*
*   The sub-account entries are found on T5645, accessed by
*  program number. 01 and 02 are posted.
*
*  If the amounts are not zero, then call LIFACMV ("cash"
*  posting subroutine) to post to the correct account.  The
*  posting required is defined in the appropriate line no.  on
*  the T5645 table entry.  Set up and pass the linkage area as
*  follows:
*
*             Function               - PSTW
*             Batch key              - from linkage
*             Document number        - contract number
*             Sequence number        - transaction no.
*             Sub-account code       - from applicable T5645
*                                      entry
*             Sub-account type       - ditto
*             Sub-account GL map     - ditto
*             Sub-account GL sign    - ditto
*             S/acct control total   - ditto
*             Cmpy code (sub ledger) - batch company
*             Cmpy code (GL)         - batch company
*             Subsidiary ledger      - contract number
*             Original currency code - currency payable in
*             Original currency amt  - actual value from
*                                      linkage
*             Accounting curr code   - blank (handled by
*                                      subroutine)
*             Accounting curr amount - zero (handled by
*                                      subroutine)
*             Exchange rate          - zero (handled by
*                                      subroutine)
*             Trans reference        - contract transaction
*                                      number
*             Trans description      - from transaction code
*                                      description
*             Posting month and year - defaulted
*             Effective date         - Death Claim
*                                      effective-date
*             Reconciliation amount  - zero
*             Reconciliation date    - Max date
*             Transaction Id         - from linkage
*             Substitution code 1    - contract type
*
*     This Subroutine now also checks the value of the Unit
*     Statement Method on T6647 and uses it to access T6659.
*     The appropriate Subroutine will be called if set up.
*
*
/
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dpp008 extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Dpp008.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "DPP008";
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String f294 = "F294";
	private static final String h072 = "H072";
	private static final String g029 = "G029";
	private static final String h115 = "H115";

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTabBatckey = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);

	private FixedLengthStringData wsaaTrigger = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaTriggerChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTrigger, 0);
	private FixedLengthStringData wsaaTriggerChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTrigger, 1);
	private FixedLengthStringData wsaaTriggerLife = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 9);
	private FixedLengthStringData wsaaTriggerCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 11);
	private FixedLengthStringData wsaaTriggerRider = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 13);
	private FixedLengthStringData filler = new FixedLengthStringData(5).isAPartOf(wsaaTrigger, 15, FILLER).init(SPACES);
		/* TABLES */
	private static final String t6647 = "T6647";
	private static final String t5687 = "T5687";
	private static final String t6598 = "T6598";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t6659 = "T6659";
	private static final String utrnrec = "UTRNREC";
	private static final String hitrrec = "HITRREC";
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredCrtable = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaClaimAmount = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaActual = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaEstimate = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaFirstPost = new PackedDecimalData(3, 0).init(2);
	private PackedDecimalData wsaaSecondPost = new PackedDecimalData(3, 0).init(3);
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private ClmddppTableDAM clmddppIO = new ClmddppTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Varcom varcom = new Varcom();
	private T6647rec t6647rec = new T6647rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T6659rec t6659rec = new T6659rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Dthcpy dthcpy = new Dthcpy();
	private Zutrpf zutrpf = new Zutrpf();//ILIFE-5462
	private ZutrpfDAO zutrpfDAO = getApplicationContext().getBean("zutrpfDAO", ZutrpfDAO.class);//ILIFE-5462
	private List<Zutrpf> zutrpfList = null; //ILIFE-5462
	private ExternalisedRules er = new ExternalisedRules();//ILIFE-5462
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class); //IBPLIFE-7497
	private List<Covrpf> covrpfList = null; //IBPLIFE-7497
	private Covrpf covrpf = new Covrpf(); //IBPLIFE-7497

	public Dpp008() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dthcpy.deathRec = convertAndSetParam(dthcpy.deathRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		dthcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaBatckey.set(dthcpy.batckey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaTime.set(getCobolTime());
		readTabT6647200();
		if (isNE(t6647rec.unitStatMethod, SPACES)) {
			a00ReadTabT6659();
		}
		readTabT5688250();
		getTrigger300();
		readTabT5645350();
		if (isEQ(dthcpy.fieldType, "D")) {
			a100WriteHitr();
		}
		else {
			
			if (isEQ(dthcpy.fieldType, "I")
			|| isEQ(dthcpy.fieldType, "A")) {
				writeUtrn400();
				//IBPLIFE-7497 starts
				checkForSinglePremium();
				//IBPLIFE-7497 ends
			}
			else {
				if (isEQ(dthcpy.fieldType, "S")) {
					processClmd500();
					if (isEQ(wsaaEstimate, ZERO)) {
						calcClaimAmount700();
					}
				}
			}
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTabT6647200()
	{
		read231();
	}

protected void read231()
	{
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaTabBatckey.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(dthcpy.cnttype);
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaT6647Key, itdmIO.getItemitem())
		|| isNE(dthcpy.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t6647)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT6647Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h115);
			fatalError9000();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void a00ReadTabT6659()
	{
		a00Read();
		a10CheckT6659Details();
	}

protected void a00Read()
	{
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(t6647rec.unitStatMethod, itdmIO.getItemitem())
		|| isNE(dthcpy.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t6659)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g029);
			fatalError9000();
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

protected void a10CheckT6659Details()
	{
		/* Check the details and call the generic subroutine from T6659.*/
		if (isEQ(t6659rec.subprog, SPACES)
		|| isNE(t6659rec.annOrPayInd, "P")
		|| isNE(t6659rec.osUtrnInd, "Y")) {
			return ;
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(dthcpy.chdrChdrcoy);
		annprocrec.chdrnum.set(dthcpy.chdrChdrnum);
		annprocrec.life.set(SPACES);
		annprocrec.coverage.set(SPACES);
		annprocrec.rider.set(SPACES);
		annprocrec.planSuffix.set(0);
		annprocrec.effdate.set(dthcpy.effdate);
		annprocrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		annprocrec.crdate.set(0);
		annprocrec.crtable.set(SPACES);
		annprocrec.user.set(dthcpy.user);
		annprocrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		annprocrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		annprocrec.batccoy.set(wsaaBatckey.batcBatccoy);
		annprocrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		annprocrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		annprocrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			fatalError9000();
		}
	}

protected void readTabT5688250()
	{
		read251();
	}

protected void read251()
	{
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(dthcpy.cnttype);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), dthcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), dthcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(dthcpy.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void getTrigger300()
	{
		read331();
	}

protected void read331()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(dthcpy.crtable);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), dthcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), dthcpy.crtable)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(dthcpy.crtable);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6598);
		itdmIO.setItemitem(t5687rec.dcmeth);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), dthcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t6598)
		|| isNE(itdmIO.getItemitem(), t5687rec.dcmeth)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(t5687rec.dcmeth);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h072);
			fatalError9000();
		}
		else {
			t6598rec.t6598Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTabT5645350()
	{
		read351();
	}

protected void read351()
	{
		itemIO.setItemcoy(dthcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void writeUtrn400()
	{
		write431();
	}

protected void write431()
	{
		utrnIO.setParams(SPACES);
		utrnIO.setTranno(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setPlanSuffix(ZERO);
		utrnIO.setBatcactyr(ZERO);
		utrnIO.setBatcactmn(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setContractAmount(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setProcSeqNo(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setSvp(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setCrComDate(ZERO);
		utrnIO.setChdrcoy(dthcpy.chdrChdrcoy);
		wsaaTriggerChdrcoy.set(dthcpy.chdrChdrcoy);
		utrnIO.setChdrnum(dthcpy.chdrChdrnum);
		wsaaTriggerChdrnum.set(dthcpy.chdrChdrnum);
		utrnIO.setLife(dthcpy.lifeLife);
		wsaaTriggerLife.set(dthcpy.lifeLife);
		utrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		utrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		utrnIO.setUser(dthcpy.user);
		utrnIO.setCoverage(dthcpy.covrCoverage);
		wsaaTriggerCoverage.set(dthcpy.covrCoverage);
		utrnIO.setRider(dthcpy.covrRider);
		wsaaTriggerRider.set(dthcpy.covrRider);
		utrnIO.setCrtable(dthcpy.crtable);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setMoniesDate(dthcpy.effdate);
		utrnIO.setSurrenderPercent(100);
		utrnIO.setFundAmount(0);
		utrnIO.setUnitVirtualFund(dthcpy.element);
		utrnIO.setContractType(dthcpy.cnttype);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setNowDeferInd(t6647rec.dealin);
		
		readReservePricing();//ILIFE-5462
		
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode04);
			utrnIO.setSacstyp(t5645rec.sacstype04);
			utrnIO.setGenlcde(t5645rec.glmap04);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		utrnIO.setTriggerModule(t6598rec.addprocess);
		utrnIO.setTriggerKey(wsaaTrigger);
		utrnIO.setNofUnits(0);
		utrnIO.setTransactionDate(wsaaToday);
		utrnIO.setTransactionTime(wsaaTime);
		utrnIO.setTranno(dthcpy.tranno);
		utrnIO.setSvp(1);
		utrnIO.setFundCurrency(dthcpy.currcode);
		utrnIO.setCntcurr(dthcpy.contractCurr);
		utrnIO.setUnitType(dthcpy.fieldType);
		utrnIO.setTermid(SPACES);
		utrnIO.setFormat(utrnrec);
		utrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(utrnIO.getStatuz());
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void processClmd500()
	{
		/*READ*/
		/*  find each coverage or rider attached to the death*/
		/*  by reading the CLMDDPP file*/
		clmddppIO.setDataArea(SPACES);
		clmddppIO.setChdrcoy(dthcpy.chdrChdrcoy);
		clmddppIO.setChdrnum(dthcpy.chdrChdrnum);
		clmddppIO.setCoverage(dthcpy.covrCoverage);
		clmddppIO.setRider(dthcpy.covrRider);
		if (isEQ(clmddppIO.getRider(), SPACES)) {
			clmddppIO.setRider("00");
		}
		clmddppIO.setCrtable(dthcpy.crtable);
		clmddppIO.setFunction(varcom.begn);
		while ( !(isEQ(clmddppIO.getStatuz(), varcom.endp))) {
			processClmddpp600();
		}
		
		/*EXIT*/
	}

protected void processClmddpp600()
	{
		/*READ*/
		SmartFileCode.execute(appVars, clmddppIO);
		if (isNE(clmddppIO.getStatuz(), varcom.endp)
		&& isNE(clmddppIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clmddppIO.getStatuz());
			syserrrec.params.set(clmddppIO.getParams());
			fatalError9000();
		}
		if (isNE(dthcpy.chdrChdrcoy, clmddppIO.getChdrcoy())
		|| isNE(dthcpy.chdrChdrnum, clmddppIO.getChdrnum())
		|| isNE(dthcpy.covrCoverage, clmddppIO.getCoverage())
		|| isNE(dthcpy.covrRider, clmddppIO.getRider())
		|| isEQ(clmddppIO.getStatuz(), varcom.endp)) {
			clmddppIO.setStatuz(varcom.endp);
		}
		else {
			wsaaEstimate.add(clmddppIO.getEstMatValue());
			wsaaActual.add(clmddppIO.getActvalue());
		}
		clmddppIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void calcClaimAmount700()
	{
		read710();
	}

protected void read710()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrnum(dthcpy.chdrChdrnum);
		covrIO.setChdrcoy(dthcpy.chdrChdrcoy);
		covrIO.setLife(dthcpy.lifeLife);
		covrIO.setCoverage(dthcpy.covrCoverage);
		covrIO.setRider(dthcpy.covrRider);
		covrIO.setPlanSuffix(0);
		covrIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		if (isNE(covrIO.getChdrnum(), dthcpy.chdrChdrnum)
		|| isNE(covrIO.getChdrcoy(), dthcpy.chdrChdrcoy)
		|| isNE(covrIO.getLife(), dthcpy.lifeLife)
		|| isNE(covrIO.getCoverage(), dthcpy.covrCoverage)
		|| isNE(covrIO.getRider(), dthcpy.covrRider)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		/*  store coverage and rider and currency*/
		wsaaStoredCoverage.set(covrIO.getCoverage());
		wsaaStoredRider.set(covrIO.getRider());
		wsaaStoredCrtable.set(covrIO.getCrtable());
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			processComponents800();
		}
		
		compute(wsaaClaimAmount, 2).set(sub(wsaaSumins, wsaaActual));
		if (isLT(wsaaClaimAmount, ZERO)) {
			wsaaClaimAmount.set(ZERO);
		}
		dthcpy.estimatedVal.set(0);
		dthcpy.actualVal.set(wsaaClaimAmount);
		if (isNE(wsaaClaimAmount, ZERO)) {
			postToDeathClaim850();
			postToOutstandClaim900();
		}
	}

protected void processComponents800()
	{
		/*READ*/
		wsaaSumins.add(covrIO.getSumins());
		while ( !((isNE(wsaaStoredRider, covrIO.getRider()))
		|| (isNE(wsaaStoredCoverage, covrIO.getCoverage()))
		|| isEQ(covrIO.getStatuz(), varcom.endp))) {
			findNextComponent950();
		}
		
		wsaaStoredCoverage.set(covrIO.getCoverage());
		wsaaStoredRider.set(covrIO.getRider());
		/*EXIT*/
	}

protected void postToDeathClaim850()
	{
		read860();
	}

protected void read860()
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(dthcpy.chdrChdrnum);
		lifacmvrec.tranno.set(dthcpy.tranno);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode[6]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[6]);
			lifacmvrec.glcode.set(t5645rec.glmap[6]);
			lifacmvrec.glsign.set(t5645rec.sign[6]);
			lifacmvrec.contot.set(t5645rec.cnttot[6]);
			wsaaRldgChdrnum.set(dthcpy.chdrChdrnum);
			wsaaRldgLife.set(dthcpy.lifeLife);
			wsaaRldgCoverage.set(dthcpy.covrCoverage);
			wsaaRldgRider.set(dthcpy.covrRider);
			wsaaRldgPlanSuffix.set("00");
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(wsaaStoredCrtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaFirstPost.toInt()]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaFirstPost.toInt()]);
			lifacmvrec.glcode.set(t5645rec.glmap[wsaaFirstPost.toInt()]);
			lifacmvrec.glsign.set(t5645rec.sign[wsaaFirstPost.toInt()]);
			lifacmvrec.contot.set(t5645rec.cnttot[wsaaFirstPost.toInt()]);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(SPACES);
			lifacmvrec.rldgacct.set(dthcpy.chdrChdrnum);
		}
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec.genlcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec.origcurr.set(dthcpy.currcode);
		lifacmvrec.origamt.set(dthcpy.actualVal);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		wsaaTranno.set(dthcpy.tranno);
		lifacmvrec.tranref.set(wsaaTranno);
		descIO.setDescitem(lifacmvrec.batctrcde);
		getDescription1000();
		lifacmvrec.effdate.set(dthcpy.effdate);
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(dthcpy.cnttype);
		lifacmvrec.termid.set(dthcpy.termid);
		lifacmvrec.user.set(dthcpy.user);
		lifacmvrec.transactionTime.set(dthcpy.time);
		lifacmvrec.transactionDate.set(dthcpy.date_var);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void postToOutstandClaim900()
	{
		read910();
	}

protected void read910()
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(dthcpy.chdrChdrnum);
		lifacmvrec.tranno.set(dthcpy.tranno);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode[5]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[5]);
			lifacmvrec.glcode.set(t5645rec.glmap[5]);
			lifacmvrec.glsign.set(t5645rec.sign[5]);
			lifacmvrec.contot.set(t5645rec.cnttot[5]);
			wsaaRldgChdrnum.set(dthcpy.chdrChdrnum);
			wsaaRldgLife.set(dthcpy.lifeLife);
			wsaaRldgCoverage.set(dthcpy.covrCoverage);
			wsaaRldgRider.set(dthcpy.covrRider);
			wsaaRldgPlanSuffix.set("00");
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(wsaaStoredCrtable);
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaSecondPost.toInt()]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaSecondPost.toInt()]);
			lifacmvrec.glcode.set(t5645rec.glmap[wsaaSecondPost.toInt()]);
			lifacmvrec.glsign.set(t5645rec.sign[wsaaSecondPost.toInt()]);
			lifacmvrec.contot.set(t5645rec.cnttot[wsaaSecondPost.toInt()]);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(dthcpy.chdrChdrnum);
		}
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec.genlcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec.origcurr.set(dthcpy.currcode);
		lifacmvrec.origamt.set(dthcpy.actualVal);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		wsaaTranno.set(dthcpy.tranno);
		lifacmvrec.tranref.set(wsaaTranno);
		descIO.setDescitem(lifacmvrec.batctrcde);
		getDescription1000();
		lifacmvrec.effdate.set(dthcpy.effdate);
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(dthcpy.cnttype);
		lifacmvrec.termid.set(dthcpy.termid);
		lifacmvrec.user.set(dthcpy.user);
		lifacmvrec.transactionTime.set(dthcpy.time);
		lifacmvrec.transactionDate.set(dthcpy.date_var);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void findNextComponent950()
	{
		/*NEXT*/
		/* Read the next coverage/rider record.*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		if (isNE(covrIO.getChdrcoy(), dthcpy.chdrChdrcoy)
		|| isNE(covrIO.getChdrnum(), dthcpy.chdrChdrnum)
		|| isNE(covrIO.getLife(), dthcpy.lifeLife)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void getDescription1000()
	{
		/*PARA*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(dthcpy.chdrChdrcoy);
		descIO.setLanguage(dthcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		lifacmvrec.trandesc.set(descIO.getShortdesc());
		/*EXIT*/
	}

protected void a100WriteHitr()
	{
		a110Hitr();
	}

protected void a110Hitr()
	{
		hitrIO.setParams(SPACES);
		hitrIO.setTranno(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setPlanSuffix(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setContractAmount(ZERO);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setProcSeqNo(ZERO);
		hitrIO.setSvp(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setChdrcoy(dthcpy.chdrChdrcoy);
		wsaaTriggerChdrcoy.set(dthcpy.chdrChdrcoy);
		hitrIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hitrIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hitrIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hitrIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hitrIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hitrIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		hitrIO.setChdrnum(dthcpy.chdrChdrnum);
		wsaaTriggerChdrnum.set(dthcpy.chdrChdrnum);
		hitrIO.setLife(dthcpy.lifeLife);
		wsaaTriggerLife.set(dthcpy.lifeLife);
		hitrIO.setCoverage(dthcpy.covrCoverage);
		wsaaTriggerCoverage.set(dthcpy.covrCoverage);
		hitrIO.setRider(dthcpy.covrRider);
		wsaaTriggerRider.set(dthcpy.covrRider);
		hitrIO.setCrtable(dthcpy.crtable);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setSurrenderPercent(100);
		hitrIO.setFundAmount(0);
		hitrIO.setZintbfnd(dthcpy.element);
		hitrIO.setCnttyp(dthcpy.cnttype);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			hitrIO.setSacscode(t5645rec.sacscode04);
			hitrIO.setSacstyp(t5645rec.sacstype04);
			hitrIO.setGenlcde(t5645rec.glmap04);
		}
		else {
			hitrIO.setSacscode(t5645rec.sacscode01);
			hitrIO.setSacstyp(t5645rec.sacstype01);
			hitrIO.setGenlcde(t5645rec.glmap01);
		}
		hitrIO.setTriggerModule(t6598rec.addprocess);
		hitrIO.setTriggerKey(wsaaTrigger);
		hitrIO.setTranno(dthcpy.tranno);
		hitrIO.setSvp(1);
		hitrIO.setZrectyp("P");
		hitrIO.setFundCurrency(dthcpy.currcode);
		hitrIO.setCntcurr(dthcpy.contractCurr);
		hitrIO.setEffdate(dthcpy.effdate);
		hitrIO.setZintalloc("N");
		hitrIO.setZintrate(ZERO);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setFormat(hitrrec);
		hitrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hitrIO.getStatuz());
			syserrrec.params.set(hitrIO.getParams());
			fatalError9000();
		}
	}
//ILIFE-5462 Start
protected void readReservePricing(){
	
	zutrpf.setChdrcoy(dthcpy.chdrChdrcoy.toString().trim());
	zutrpf.setChdrnum(dthcpy.chdrChdrnum.toString().trim());
	zutrpf.setTranno(dthcpy.tranno.toInt());
	
	try {
		zutrpfList = zutrpfDAO.readReservePricing(zutrpf);
	} catch (Exception e) {
		LOGGER.error("Exception occured in readReservePricing()",e);
		fatalError9000();
	}
	
	if(zutrpfList.size()>0) {
		for(Zutrpf zutrpf1 : zutrpfList){
			utrnIO.setMoniesDate(zutrpf1.getReserveUnitsDate()); 
			utrnIO.setNowDeferInd('N');
		}
	}	
	//ILIFE-5459 End
	
}
protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		dthcpy.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

//IBPLIFE-7497 starts
protected void checkForSinglePremium() 
	{
		covrpfList = covrpfDAO.searchCovrrnlRecord(dthcpy.chdrChdrcoy.toString(), dthcpy.chdrChdrnum.toString(), dthcpy.lifeLife.toString(), dthcpy.covrCoverage.toString(), dthcpy.covrRider.toString(), 0);
		if(!(covrpfList.isEmpty())) {
			covrpf = covrpfList.get(0);
			if(isNE(covrpf.getSingp(), 0)) {
				writeUtrn400();
			}
		}
	}
//IBPLIFE-7497 ends

}
