package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.KjodpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Kjodpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class KjodpfDAOImpl extends BaseDAOImpl<Kjodpf> implements KjodpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(KjodpfDAOImpl.class);

	@Override
	public void insertKjodpfList(List<Kjodpf> pfList) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO KJODPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,TRANNO,EFFDATE,CURRCD,CRTABLE,ACTVALUE,CLAIM,TERMID,TRDT,TRTM,USER_T,USRPRF,JOBNM,DATIME) ");
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for (Kjodpf pf : pfList) {
				int i = 1;
				ps.setString(i++, pf.getChdrcoy());
				ps.setString(i++, pf.getChdrnum());
				ps.setString(i++, pf.getLife());
				ps.setString(i++, pf.getCoverage());
				ps.setString(i++, pf.getRider());
				ps.setInt(i++, pf.getTranno());
				ps.setInt(i++, pf.getEffdate());
				ps.setString(i++, pf.getCurrcd());
				ps.setString(i++, pf.getCrtable());
				ps.setBigDecimal(i++, pf.getActvalue());
				ps.setString(i++, pf.getClaim());
				ps.setString(i++, pf.getTermid());
				ps.setInt(i++, pf.getTrdt());
				ps.setInt(i++, pf.getTrtm());
				ps.setInt(i++, pf.getuserid());				
				ps.setString(i++, this.getUsrprf());
				ps.setString(i++, this.getJobnm());
				ps.setTimestamp(i++, this.getDatime());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertKjodpfList()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

	}
	
	@Override
	public Kjodpf getKjodpfRecord(String chdrnum, String coverage, String rider) {
	
      	
      	StringBuilder sql = new StringBuilder("SELECT * FROM KJODPF");
      	sql.append(" WHERE CHDRNUM=? or CLAIM=? and VALIDFLAG='1' ");
      	PreparedStatement ps=null;
      	ResultSet rs=null;
      	
      	Kjodpf kjodpf= null;
      	
      	try {
      		ps=getPrepareStatement(sql.toString());
      		ps.setString(1, chdrnum);
      		ps.setString(2, coverage);
      		ps.setString(3, rider);
      		rs=ps.executeQuery();
        while (rs.next()) {
        	kjodpf= new Kjodpf();
        	
        	kjodpf.setChdrnum(rs.getString("CHDRNUM"));
        	kjodpf.setChdrcoy(rs.getString("CHDRCOY"));
        	kjodpf.setCoverage(rs.getString("COVERAGE"));
        	kjodpf.setRider(rs.getString("RIDER"));
        	kjodpf.setCrtable(rs.getString("CRTABLE"));
        	kjodpf.setActvalue(rs.getBigDecimal("ACTVALUE"));
        	
            }
        } catch (SQLException e) {
        LOGGER.error("getKjodpfRecord()", e);
        throw new SQLRuntimeException(e);
    } finally {
        close(ps, rs);
    }
    return kjodpf;
	}
	
}
