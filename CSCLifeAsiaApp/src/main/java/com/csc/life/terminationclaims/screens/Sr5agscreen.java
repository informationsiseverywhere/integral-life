package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sr5agscreen extends ScreenRecord{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}
	public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
			Sr5agScreenVars sv = (Sr5agScreenVars) pv;
			clearInds(av, pfInds);
			write(lrec, sv.Sr5agscreenWritten, null, av, null, ind2, ind3);
		}

		public static void read(Indicator ind2, Indicator ind3) {}

		public static void clearClassString(VarModel pv) {
			Sr5agScreenVars screenVars = (Sr5agScreenVars)pv;
			screenVars.screenRow.setClassString("");
			screenVars.screenColumn.setClassString("");
			screenVars.company.setClassString("");
			screenVars.tabl.setClassString("");
			screenVars.item.setClassString("");
			screenVars.longdesc.setClassString("");
			screenVars.progdesc01.setClassString("");
			screenVars.progdesc02.setClassString("");
			screenVars.progdesc03.setClassString("");
			screenVars.progdesc04.setClassString("");
			screenVars.progdesc05.setClassString("");
			screenVars.excltxt.setClassString("");
				}

	/**
	 * Clear all the variables in Sr5agscreen
	 */
		public static void clear(VarModel pv) {
			Sr5agScreenVars screenVars = (Sr5agScreenVars) pv;
			screenVars.screenRow.clear();
			screenVars.screenColumn.clear();
			screenVars.company.clear();
			screenVars.tabl.clear();
			screenVars.item.clear();
			screenVars.longdesc.clear();
			screenVars.progdesc01.clear();
			screenVars.progdesc02.clear();
			screenVars.progdesc03.clear();
			screenVars.progdesc04.clear();
			screenVars.progdesc05.clear();
			screenVars.excltxt.clear();
				}

}
