/*
 * File: P6625.java
 * Date: 30 August 2009 0:47:10
 * Author: Quipoz Limited
 * 
 * Class transformed from P6625.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.procedures.T6625pt;
import com.csc.life.terminationclaims.screens.S6625ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P6625 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6625");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	protected T6625rec t6625rec = getT6625rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6625ScreenVars sv = getLScreenVars(); // ScreenProgram.getScreenVars( S6625ScreenVars.class);

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P6625() {
		super();
		screenVars = sv;
		new ScreenModel("S6625", AppVars.getInstance(), sv);
	}
	
	protected S6625ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6625ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t6625rec.t6625Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t6625rec.comtperc.set(ZERO);
		t6625rec.dthpercn.set(ZERO);
		t6625rec.dthperco.set(ZERO);
		t6625rec.evstperd.set(ZERO);
		t6625rec.guarperd.set(ZERO);
		t6625rec.lvstperd.set(ZERO);
		t6625rec.annuty.set(SPACE);//ILIFE-3595
	}

protected void generalArea1045()
	{
		sv.bonalloc.set(t6625rec.bonalloc);
		sv.comtmeth.set(t6625rec.comtmeth);
		sv.comtmthj.set(t6625rec.comtmthj);
		sv.comtperc.set(t6625rec.comtperc);
		sv.dthpercn.set(t6625rec.dthpercn);
		sv.dthperco.set(t6625rec.dthperco);
		sv.evstperd.set(t6625rec.evstperd);
		sv.freqann.set(t6625rec.freqann);
		sv.frequency.set(t6625rec.frequency);
		sv.guarperd.set(t6625rec.guarperd);
		sv.annuty.set(t6625rec.annuty);//ILIFE-3595
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.lvstperd.set(t6625rec.lvstperd);
		sv.ncovvest.set(t6625rec.ncovvest);
		sv.revBonusMeth.set(t6625rec.revBonusMeth);
		sv.vcalcmth.set(t6625rec.vcalcmth);
		sv.vclcmthj.set(t6625rec.vclcmthj);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t6625rec.t6625Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.bonalloc,t6625rec.bonalloc)) {
			t6625rec.bonalloc.set(sv.bonalloc);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.comtmeth,t6625rec.comtmeth)) {
			t6625rec.comtmeth.set(sv.comtmeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.comtmthj,t6625rec.comtmthj)) {
			t6625rec.comtmthj.set(sv.comtmthj);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.comtperc,t6625rec.comtperc)) {
			t6625rec.comtperc.set(sv.comtperc);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.dthpercn,t6625rec.dthpercn)) {
			t6625rec.dthpercn.set(sv.dthpercn);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.dthperco,t6625rec.dthperco)) {
			t6625rec.dthperco.set(sv.dthperco);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.evstperd,t6625rec.evstperd)) {
			t6625rec.evstperd.set(sv.evstperd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.freqann,t6625rec.freqann)) {
			t6625rec.freqann.set(sv.freqann);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.frequency,t6625rec.frequency)) {
			t6625rec.frequency.set(sv.frequency);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.guarperd,t6625rec.guarperd)) {
			t6625rec.guarperd.set(sv.guarperd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.lvstperd,t6625rec.lvstperd)) {
			t6625rec.lvstperd.set(sv.lvstperd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ncovvest,t6625rec.ncovvest)) {
			t6625rec.ncovvest.set(sv.ncovvest);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.revBonusMeth,t6625rec.revBonusMeth)) {
			t6625rec.revBonusMeth.set(sv.revBonusMeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.vcalcmth,t6625rec.vcalcmth)) {
			t6625rec.vcalcmth.set(sv.vcalcmth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.vclcmthj,t6625rec.vclcmthj)) {
			t6625rec.vclcmthj.set(sv.vclcmthj);
			wsaaUpdateFlag = "Y";
		}
		//ILIFE-3595-STARTS
		if (isNE(sv.annuty,t6625rec.annuty)) {
			t6625rec.annuty.set(sv.annuty);
			wsaaUpdateFlag = "Y";
		}
		//ILIFE-3595-ENDS
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T6625pt.class, wsaaTablistrec);
		/*EXIT*/
	}
public T6625rec getT6625rec() {
	return new T6625rec();
} 

public String getWsaaUpdateFlag() {
	return wsaaUpdateFlag;
}

public void setWsaaUpdateFlag(String wsaaUpdateFlag) {
	this.wsaaUpdateFlag = wsaaUpdateFlag;
}


}
