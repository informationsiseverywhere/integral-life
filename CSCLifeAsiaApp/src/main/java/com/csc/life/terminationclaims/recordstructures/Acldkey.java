package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:39
 * Description:
 * Copybook name: ACLDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acldkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acldFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData acldKey = new FixedLengthStringData(256).isAPartOf(acldFileKey, 0, REDEFINE);
  	public FixedLengthStringData acldChdrcoy = new FixedLengthStringData(1).isAPartOf(acldKey, 0);
  	public FixedLengthStringData acldChdrnum = new FixedLengthStringData(8).isAPartOf(acldKey, 1);
  	public FixedLengthStringData acldLife = new FixedLengthStringData(2).isAPartOf(acldKey, 9);
  	public FixedLengthStringData acldCoverage = new FixedLengthStringData(2).isAPartOf(acldKey, 11);
  	public FixedLengthStringData acldRider = new FixedLengthStringData(2).isAPartOf(acldKey, 13);
  	public FixedLengthStringData acldAcdben = new FixedLengthStringData(5).isAPartOf(acldKey, 15);
  	public PackedDecimalData acldRgpynum = new PackedDecimalData(5, 0).isAPartOf(acldKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(233).isAPartOf(acldKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acldFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acldFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}