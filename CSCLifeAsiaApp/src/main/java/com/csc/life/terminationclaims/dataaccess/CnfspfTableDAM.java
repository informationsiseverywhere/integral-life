package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CnfspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:30
 * Class transformed from CNFSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CnfspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 61;
	public FixedLengthStringData cnfsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData cnfspfRecord = cnfsrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(cnfsrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(cnfsrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(cnfsrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(cnfsrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(cnfsrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(cnfsrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(cnfsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CnfspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for CnfspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CnfspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CnfspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CnfspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CnfspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CnfspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CNFSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"VALIDFLAG, " +
							"EFFDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     validflag,
                                     effdate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		validflag.clear();
  		effdate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCnfsrec() {
  		return cnfsrec;
	}

	public FixedLengthStringData getCnfspfRecord() {
  		return cnfspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCnfsrec(what);
	}

	public void setCnfsrec(Object what) {
  		this.cnfsrec.set(what);
	}

	public void setCnfspfRecord(Object what) {
  		this.cnfspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(cnfsrec.getLength());
		result.set(cnfsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}