package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl54screenctl extends ScreenRecord{

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 29, 30}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sjl54screensfl";
		lrec.subfileClass = Sjl54screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 101;
		lrec.pageSubfile = 100;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 6, 1, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl54ScreenVars sv = (Sjl54ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl54screenctlWritten, sv.Sjl54screensflWritten, av, sv.sjl54screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl54ScreenVars screenVars = (Sjl54ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.payNum.setClassString("");
		screenVars.claimNum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.payDateDisp.setClassString("");
		screenVars.reqntype.setClassString("");
		screenVars.totalPayAmt.setClassString("");
		screenVars.clntName.setClassString("");
		screenVars.clientnum.setClassString("");
	}

/**
 * Clear all the variables in Sjl54screenctl
 */
	public static void clear(VarModel pv) {
		Sjl54ScreenVars screenVars = (Sjl54ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.payNum.clear();
		screenVars.claimNum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.payDateDisp.clear();
		screenVars.reqntype.clear();
		screenVars.totalPayAmt.clear();
		screenVars.clntName.clear();
		screenVars.clientnum.clear();
	}
}
