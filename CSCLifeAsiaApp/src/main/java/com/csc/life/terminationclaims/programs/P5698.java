/*
 * File: P5698.java
 * Date: 30 August 2009 0:34:50
 * Author: Quipoz Limited
 * 
 * Class transformed from P5698.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;
import com.csc.life.terminationclaims.screens.S5698ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Death Claim - Registered Dead Life Selection.
*
*  This transaction, Life Selection, is selected from the
*  Claims sub-menu S6319/P6319. It is entered into first, in
*  order that the correct registered life may be  selected, if
*  there is only one life, then it will default to this life.
*
*
*     Initialise
*     ----------
*
*  Clear the subfile ready for loading.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To obtain the relevant life  (i.e.  that  lucky  one  to
*  die) BEGN using LIFECLM  for  this  contract and only lives
*  with a valid flag of '1' are selected (defined in logical
*  view) and a status of 'RD' are selected.
*
*  - one line is written to  the subfile for each life read
*    from the LIFECLM file.
*
*  - check the status of  each life against the status code
*    applicable to it on  T5679.  If  the  codes  do not
*    match, then protect  the  selection  field  on that
*    life. I.E. this life may not be returned.
*
*  - if there is only  one life which can be selected, then
*    return this life  as  the  selected life and do not
*    display the life selection screen S5698.
*
*  Obtain the  necessary  descriptions  by  calling 'DESCIO'
*  and output the descriptions of the statuses to the screen.
*
*  Only read the  Contract header (function RETRV) if the
*  screen is going to be returned. Read the relevant data
*  necessary for obtaining   the   status   description,
*  the   Owner,   CCD, Paid-to-date and the Bill-to-date.
*
*  Read the client  details record and use the relevant
*  copybook in order to format the required names.
*
*  In all cases, load all  pages required in the subfile and
*  set the subfile indicator to no.
*
*  The above details are returned to the user.
*
*
*     Validation
*     ----------
*
*  If  CF11  (KILL)  was entered, then skip the remainder of
*  the validation and blank out the correct program on the
*  stack and exit from the program.
*
*  Read all modified  records and if more than one selection
*  was made return the  screen  to the user requesting that
*  only one life be selected.  Likewise, return a message if
*  no selection was made, as one life must be selected.
*
*
*     Updating
*     --------
*
*  If the  CF11  (KILL)  function  key  was  pressed,  skip
*  the updating  and  blank out the correct program on the
*  stack and exit from the program.
*
*  For the life selected do a KEEPS on this the CLMHCLM file
*
*  Next Program
*  ------------
*
*  For CF11 or 'Enter', add 1 to the program pointer and exit.
*
*
*  Modifications
*  -------------
*
*
*  Include the following logical views:
*
*            - CHDRCLM
*            - LIFECLM
*            - CLMHCLM
*****************************************************************
* </pre>
*/
public class P5698 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5698");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaActualWritten = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaNoOfSelect = new PackedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaRrn = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0);
		/* ERRORS */
	private static final String e304 = "E304";
	private static final String h070 = "H070";
	private static final String h071 = "H071";
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t5680 = "T5680";
	private static final String t5679 = "T5679";
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
		/* FORMATS */
	private static final String chdrclmrec = "CHDRCLMREC";
	private static final String clmhclmrec = "CLMHCLMREC";
	private static final String lifeclmrec = "LIFECLMREC";
		/*Claims Contract Header*/
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
		/*Claim Header file*/
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Claims Life Record*/
	private LifeclmTableDAM lifeclmIO = new LifeclmTableDAM();
	private T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5698ScreenVars sv = ScreenProgram.getScreenVars( S5698ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
	}

	public P5698() {
		super();
		screenVars = sv;
		new ScreenModel("S5698", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			beginLifeFile1020();
			loadSubfile1030();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaActualWritten.set(ZERO);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		scrnparams.function.set(varcom.sclr);
		processScreen("S5698", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		chdrclmIO.setFunction(varcom.retrv);
		chdrclmIO.setFormat(chdrclmrec);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	* Begin reading the life file (LIFECLM) for this contract
	* </pre>
	*/
protected void beginLifeFile1020()
	{
		lifeclmIO.setDataArea(SPACES);
		lifeclmIO.setChdrcoy(wsspcomn.company);
		lifeclmIO.setChdrnum(chdrclmIO.getChdrnum());
		lifeclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		lifeclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, lifeclmIO);
		if ((isNE(lifeclmIO.getStatuz(),varcom.oK))
		&& (isNE(lifeclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,lifeclmIO.getChdrcoy())
		|| isNE(chdrclmIO.getChdrnum(),lifeclmIO.getChdrnum())
		|| isEQ(lifeclmIO.getStatuz(),varcom.endp)) {
			lifeclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1090);
		}
	}

protected void loadSubfile1030()
	{
		while ( !(isEQ(lifeclmIO.getStatuz(),varcom.endp))) {
			loadSubfile1100();
		}
		
		if (isGT(wsaaActualWritten,1)) {
			fillScreen1200();
		}
	}

protected void loadSubfile1100()
	{
		lifeDetails1110();
		readNextLife1500();
	}

protected void lifeDetails1110()
	{
		if (isNE(lifeclmIO.getValidflag(),"1")) {
			return ;
		}
		/* Move the last record read to the screen and get the*/
		/* description.*/
		if (isEQ(lifeclmIO.getJlife(),"00")) {
			sv.jlife.set(SPACES);
		}
		else {
			sv.jlife.set(lifeclmIO.getJlife());
		}
		sv.life.set(lifeclmIO.getLife());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		sv.lifcnum.set(lifeclmIO.getLifcnum());
		cltsIO.setClntnum(lifeclmIO.getLifcnum());
		/* Get the confirmation name.*/
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		checkStatus1400();
		descIO.setDescitem(lifeclmIO.getStatcode());
		descIO.setDesctabl(t5680);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.stdescsh.set(descIO.getLongdesc());
		}
		else {
			sv.stdescsh.fill("?");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5698", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*    maintain count of actual records written*/
		/*        (not in the case of records which are not valid to*/
		/*         be selected i.e. invalid status code)*/
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			wsaaRrn.set(scrnparams.subfileRrn);
			wsaaActualWritten.add(1);
		}
	}

protected void readNextLife1500()
	{
		/*    look for next life*/
		lifeclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifeclmIO);
		if ((isNE(lifeclmIO.getStatuz(),varcom.oK))
		&& (isNE(lifeclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,lifeclmIO.getChdrcoy())
		|| isNE(chdrclmIO.getChdrnum(),lifeclmIO.getChdrnum())) {
			lifeclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void fillScreen1200()
	{
		headings1210();
	}

protected void headings1210()
	{
		sv.occdate.set(chdrclmIO.getOccdate());
		sv.chdrnum.set(chdrclmIO.getChdrnum());
		sv.cnttype.set(chdrclmIO.getCnttype());
		descIO.setDescitem(chdrclmIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrclmIO.getCownnum());
		getClientDetails1700();
		sv.btdate.set(chdrclmIO.getBtdate());
		sv.ptdate.set(chdrclmIO.getPtdate());
		/*    Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrclmIO.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrclmIO.getPstatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
	}

protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkStatus1400()
	{
		readStatusTable1410();
	}

protected void readStatusTable1410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaSub.set(ZERO);
		lookForStat1500();
	}

protected void lookForStat1500()
	{
		para1505();
		search1510();
	}

protected void para1505()
	{
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
	}

protected void search1510()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,6)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isEQ(lifeclmIO.getJlife(),"01")) {
			if (isNE(lifeclmIO.getStatcode(),t5679rec.jlifeStat[wsaaSub.toInt()])) {
				search1510();
				return ;
			}
		}
		else {
			if (isNE(lifeclmIO.getStatcode(),t5679rec.lifeStat[wsaaSub.toInt()])) {
				search1510();
				return ;
			}
		}
	}

protected void getClientDetails1700()
	{
		read1710();
	}

protected void read1710()
	{
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrclmIO.getCownnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernumErr.set(e304);
			sv.ownernum.set(SPACES);
		}
		else {
			plainname();
			sv.ownernum.set(wsspcomn.longconfname);
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		wsaaNoOfSelect.set(0);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsaaActualWritten,1)) {
			wsspcomn.edterror.set(varcom.oK);
			positionToLifeSelected2100();
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
	}

protected void screenEdit2000()
	{
		screenIo2010();
		startSubfile2060();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void startSubfile2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5698", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaRrn.set(scrnparams.subfileRrn);
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		if (isGT(wsaaNoOfSelect,1)) {
			scrnparams.errorCode.set(h070);
			wsspcomn.edterror.set("Y");
			scrnparams.subfileRrn.set(1);
			return ;
		}
		if (isLT(wsaaNoOfSelect,1)) {
			scrnparams.errorCode.set(h071);
			scrnparams.subfileRrn.set(1);
			wsspcomn.edterror.set("Y");
			return ;
		}
		wsspcomn.edterror.set(varcom.oK);
		positionToLifeSelected2100();
	}

protected void positionToLifeSelected2100()
	{
		/*POSITION-TO-LIFE-SELECTED*/
		/*                                                         <D509CS>*/
		/* reposition subfile on life record to be passed to next s<D509CS>*/
		/*                                                         <D509CS>*/
		scrnparams.subfileRrn.set(wsaaRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S5698", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		/*VALIDATION*/
		if (isNE(sv.select,SPACES)) {
			wsaaNoOfSelect.add(1);
			wsaaRrn.set(scrnparams.subfileRrn);
		}
		/*READ-NEXT-RECORD*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5698", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		lifeclmIO.setDataArea(SPACES);
		lifeclmIO.setChdrcoy(chdrclmIO.getChdrcoy());
		lifeclmIO.setChdrnum(chdrclmIO.getChdrnum());
		lifeclmIO.setFormat(lifeclmrec);
		lifeclmIO.setLife(sv.life);
		if (isEQ(sv.jlife,SPACES)) {
			lifeclmIO.setJlife("00");
		}
		else {
			lifeclmIO.setJlife(sv.jlife);
		}
		lifeclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		lifeclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, lifeclmIO);
		if ((isNE(lifeclmIO.getStatuz(),varcom.oK))
		&& (isNE(lifeclmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,lifeclmIO.getChdrcoy())
		|| isNE(chdrclmIO.getChdrnum(),lifeclmIO.getChdrnum())
		|| isEQ(lifeclmIO.getStatuz(),varcom.endp)) {
			lifeclmIO.setStatuz(varcom.endp);
			return ;
		}
		clmhclmIO.setDataKey(SPACES);
		clmhclmIO.setChdrcoy(chdrclmIO.getChdrcoy());
		clmhclmIO.setChdrnum(chdrclmIO.getChdrnum());
		clmhclmIO.setLife(lifeclmIO.getLife());
		clmhclmIO.setFormat(clmhclmrec);
		clmhclmIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
