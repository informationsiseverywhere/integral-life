package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.PyoupfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Pyoupf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;


public class PyoupfDAOImpl extends BaseDAOImpl<Pyoupf> implements PyoupfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(PyoupfDAOImpl.class);

    
    @Override
	public void insertPyoupfRecord(Pyoupf pyoupf) {	
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO PYOUPF (CHDRPFX,CHDRCOY,CHDRNUM,TRANNO,BATCTRCDE,PAYRNUM,REQNTYPE,BANKACCKEY,BANKKEY,BANKDESC,PAYMENTFLAG,REQNNO,RLDGACCT,USRPRF,JOBNM,DATIME)");
		sql.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		try {
				int i = 1;
				ps.setString(i++, pyoupf.getChdrpfx());
				ps.setString(i++, pyoupf.getChdrcoy());
				ps.setString(i++, pyoupf.getChdrnum());
				ps.setInt(i++, pyoupf.getTranno());
				ps.setString(i++, pyoupf.getBatctrcde());
				ps.setString(i++, pyoupf.getPayrnum());
				ps.setString(i++, pyoupf.getReqntype());
				ps.setString(i++, pyoupf.getBankacckey());
				ps.setString(i++, pyoupf.getBankkey());
				ps.setString(i++, pyoupf.getBankdesc());
				ps.setString(i++, pyoupf.getPaymentflag());
				ps.setString(i++, pyoupf.getReqnno());
				ps.setString(i++, pyoupf.getRldgacct());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.executeUpdate();
				//ps.getConnection().commit();
		} catch (SQLException e) {
			LOGGER.error("insertPyoupfRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
    
    
    
	@Override
	public void deletePyoupfRecord(List<String> delist) {
		StringBuilder sqlDelete = new StringBuilder();
		sqlDelete.append("DELETE FROM PYOUPF ");
		sqlDelete.append(" WHERE CHDRNUM in ('");
		String delete =  String.join("','", delist);
		sqlDelete.append(delete);
		sqlDelete.append("')");
		PreparedStatement ps = getPrepareStatement(sqlDelete.toString());
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("Error occured while deleting PYOUPF: ", e);//IJTI-1485
			throw new SQLRuntimeException (e);
		} finally {
			close(ps, null);
		}
	}

	@Override
	public Pyoupf getItemByContractNum(String chdrnum) {
        	Pyoupf pyoupf = new Pyoupf();
          	
          	StringBuilder sql = new StringBuilder("SELECT * FROM PYOUPF");
          	sql.append(" WHERE CHDRNUM=? ");
          	PreparedStatement ps=null;
          	ResultSet rs=null;
          	
          	try {
          		ps=getPrepareStatement(sql.toString());
          		ps.setString(1, chdrnum);
          		rs=ps.executeQuery();
            while (rs.next()) {
            	pyoupf.setChdrpfx(rs.getString(2));
            	pyoupf.setChdrcoy(rs.getString(3));
            	pyoupf.setChdrnum(rs.getString(4));
            	pyoupf.setTranno(rs.getInt(5));
               	pyoupf.setBatctrcde(rs.getString(6));
            	pyoupf.setPayrnum(rs.getString(7));
            	pyoupf.setReqntype(rs.getString(8));
            	pyoupf.setBankacckey(rs.getString(9));
            	pyoupf.setBankkey(rs.getString(10));
            	pyoupf.setBankdesc(rs.getString(11));
            	pyoupf.setPaymentflag(rs.getString(12));
            	pyoupf.setReqnno(rs.getString(13));
                }
            } catch (SQLException e) {
            LOGGER.error("getItemByContractNum()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return pyoupf;
	}
	
	public List<Pyoupf> getAllItemitem(String chdrnum) {
		long ts = System.currentTimeMillis();
		String sql = "select * from pyoupf where CHDRNUM = ? "; //IAF-2479

		List<Pyoupf> list = new ArrayList<Pyoupf>();

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = getConnection().prepareStatement(sql);/* IJTI-1523 */
			stmt.setString(1, chdrnum);
			rs = stmt.executeQuery();

			Pyoupf pyoupf = null;
			while (rs.next()) {
				pyoupf = new Pyoupf();
				pyoupf.setChdrpfx(rs.getString(2));
            	pyoupf.setChdrcoy(rs.getString(3));
            	pyoupf.setChdrnum(rs.getString(4));
            	pyoupf.setTranno(rs.getInt(5));
               	pyoupf.setBatctrcde(rs.getString(6));
            	pyoupf.setPayrnum(rs.getString(7));
            	pyoupf.setReqntype(rs.getString(8));
            	pyoupf.setBankacckey(rs.getString(9));
            	pyoupf.setBankkey(rs.getString(10));
            	pyoupf.setBankdesc(rs.getString(11));
            	pyoupf.setPaymentflag(rs.getString(12));
            	pyoupf.setReqnno(rs.getString(13));
				
				list.add(pyoupf);
			}
		} catch (SQLException e) {
			LOGGER.error("getAllItemitem(): ", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
			if((System.currentTimeMillis() - ts)>=5000){
				LOGGER.info("SQL Call took longer than 5000 msec");
			}
		}

		return list;
	}



	@Override
	public void deleteCashDividendRecord(String chdrnum, String tranno, String trancode, String chdrcoy) {

		 StringBuilder sql_delete = new StringBuilder("DELETE FROM PYOUPF WHERE CHDRNUM=? AND TRANNO=? AND BATCTRCDE=? AND CHDRCOY=?");

	        PreparedStatement psDelete = getPrepareStatement(sql_delete.toString());
	        ResultSet rs = null;
	        try {
	        	psDelete.setString(1, chdrnum.trim());
	        	psDelete.setString(2, tranno.trim());
	        	psDelete.setString(3, trancode.trim());
	        	psDelete.setString(4, chdrcoy.trim());
	          
	        	psDelete.execute();
	          
	        } catch (SQLException e) {
	            LOGGER.error("delete peoutPf()", e);//IJTI-1485
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psDelete, rs);
	        }
		
	
	}
	
	
		
	public List<Pyoupf> deleterecord(String chdrnum) {
		long ts = System.currentTimeMillis();
		String sql = "delete  from PYOUPF where CHDRNUM = ? "; 

		List<Pyoupf> list = new ArrayList<Pyoupf>();

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = getConnection().prepareStatement(sql);/* IJTI-1523 */
			stmt.setString(1, chdrnum);
			rs = stmt.executeQuery();

			Pyoupf pyoupf = null;
			while (rs.next()) {
				pyoupf = new Pyoupf();
				pyoupf.setChdrnum(rs.getString(1));
				list.add(pyoupf);
			}
		} catch (SQLException e) {
			LOGGER.error("deleterecord(): ", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
			if((System.currentTimeMillis() - ts)>=5000){
				LOGGER.info("SQL Call took longer than 5000 msec");
			}
		}

		return list;
	}
	
	public void updatePayflg(Pyoupf pyoupf){		
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE PYOUPF SET PAYMENTFLAG=?,REQNNO=? ");
		sb.append("WHERE CHDRNUM=? ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	

				ps.setString(1, pyoupf.getPaymentflag());			    
			    ps.setString(2, pyoupf.getReqnno());
			    ps.setString(3, pyoupf.getChdrnum());
			    
			    ps.executeUpdate();				
				
			
		}catch (SQLException e) {
			LOGGER.error("updatePayflgflag()",e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
		
	}
	
	public List<Pyoupf> getPyoupfList(String chdrcoy, int minRecord, int maxRecord) {
		StringBuilder sb = new StringBuilder("select * from (");
		sb.append("SELECT row_number()over(order by CHDRCOY, CHDRNUM) ROWNM, UNIQUE_NUMBER, CHDRPFX, CHDRCOY, CHDRNUM, TRANNO, BATCTRCDE, PAYRNUM, REQNTYPE, BANKKEY, BANKACCKEY, BANKDESC, PAYMENTFLAG, REQNNO, RLDGACCT ");
		sb.append("FROM PYOUPF WHERE PAYMENTFLAG !='Y' AND CHDRCOY=?) temp where ROWNM >=? AND ROWNM <?  ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		Pyoupf pyoupf = null;
		List<Pyoupf> list = new ArrayList();
		try {
			   ps = getPrepareStatement(sb.toString());
			   ps.setString(1, chdrcoy);			    
			   ps.setInt(2, minRecord);
			   ps.setInt(3, maxRecord);

				rs = ps.executeQuery();
				
				while(rs.next()) {
					pyoupf = new Pyoupf();
					pyoupf.setChdrpfx(rs.getString("CHDRPFX"));
					pyoupf.setChdrcoy(rs.getString("CHDRCOY"));
					pyoupf.setChdrnum(rs.getString("CHDRNUM"));
					pyoupf.setTranno(rs.getInt("TRANNO"));
					pyoupf.setBatctrcde(rs.getString("BATCTRCDE"));
					pyoupf.setPayrnum(rs.getString("PAYRNUM"));
					pyoupf.setReqntype(rs.getString("REQNTYPE"));
					pyoupf.setBankkey(rs.getString("BANKKEY"));
					pyoupf.setBankacckey(rs.getString("BANKACCKEY"));
					pyoupf.setBankdesc(rs.getString("BANKDESC"));
					pyoupf.setPaymentflag(rs.getString("PAYMENTFLAG"));
					pyoupf.setReqnno(rs.getString("REQNNO"));
					pyoupf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					pyoupf.setRldgacct(rs.getString("RLDGACCT"));
				  
				  list.add(pyoupf);
				}
	}catch (SQLException e) {
		LOGGER.error("getPyoupf()",e);//IJTI-1485
		throw new SQLRuntimeException(e);
	} finally {
		close(ps, rs);
	}
    
		return list;
	}
	
	public void updatepayflag(long uniqueNumber){
	     
        StringBuilder updateSql = new StringBuilder(" UPDATE PYOUPF SET PAYMENTFLAG='Y' WHERE UNIQUE_NUMBER=? ");
        PreparedStatement ps = getPrepareStatement(updateSql.toString());
        try {
                ps.setLong(1, uniqueNumber);
                ps.execute();
        } catch (SQLException e) {
            LOGGER.error("updatepayflag()", e);//IJTI-1485
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    }

}