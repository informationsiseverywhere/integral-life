/*
 * File: P6001at.java
 * Date: 30 August 2009 0:35:42
 * Author: Quipoz Limited
 *
 * Class transformed from P6001AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.reassurance.procedures.Actvres;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.PuplTableDAM;
import com.csc.life.terminationclaims.dataaccess.PuptTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.life.terminationclaims.tablestructures.T6651rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  P6001AT - Paid Up Processing AT Processing.
*  -------------------------------------------
*
*  This program will be run under AT and will perform the
*  general functions for the finalisation of Paid Up
*  Processing.
*
*  The program will carry out the following functions:
*
*  1.  Increment the TRANNO field on the Contract Header.
*
*  2.  Call the Breakout routine if required.
*
*  3.  Validflag '2' the old COVRMJA record.
*
*  4.  Create a new COVRMJA record.
*
*
*  Processing:
*  -----------
*
*  Contract Header
*  ---------------
*  The ATMOD-PRIMARY-KEY will contain the Company and Contract
*  number of the contract being processed.  Use these to
*  perform a READH on CHDRMJA. Increment the TRANNO field by 1.
*
*
*  Breakout Processing
*  -------------------
*  Read all the PUPT for the contract.
*  If the Plan Suffix on the first record equals zeros, no
*  physical breakout is required
*  else
*  if the lowest Plan Suffix on PUPT is less than or equal to
*  CHDRMJA-POLSUM, a breakout is required.  Set the
*  OLD-SUMMARY to the value of CHDRMJA-POLSUM and the
*  NEW-SUMMARY to the value of the lowest PUPT-PLNSFX, then
*  call 'BRKOUT'.
*
*  Read T6647 (Unit Linked Contract Details) for the contract,
*  accessing the table with TRANCODE||CHDRMJA-CNTTYPE as the
*  item key.
*  Read T5645 (Transaction Accounting Rules) for the program,
*  accessing the table with the program name as the item key.
*  Read T5679 (Component Status) for the transaction,
*  accessing the table with the transaction name as the item
*  key.
*
*  BEGNH and NEXTR all the PUPT records again.  For each PUPT
*  record read, update the COVRMJA record(s) and INCR records
*  with processing dependant on field values as below:-
*
*  If PUPT-RIDER is non-zero, update the corresponding COVRMJA
*  record as follows:
*
*       READH the COVRMJA file, with the key set as follows:
*          Company - PUPT-CHDRCOY
*          Contract Number - PUPT-CHDRNUM
*          Life - PUPT-LIFE
*          Coverage - PUPT-COVERAGE
*          Rider - PUPT-RIDER
*                    Plan Suffix - PUPT-PLNSFX.
*        Perform the UPDAT-COVR section.
*
*  else
*
*  the PUPT-RIDER is zero, so update the corresponding COVRMJA
*  records as follows (i.e. paying up the coverage and lapsing
*  the riders that are not benefit billed):
*
*  BEGN and NEXTR the COVRMJA file, with the key set as follows:
*         Company - PUPT-CHDRCOY
*         Contract Number - PUPT-CHDRNUM
*         Life - PUPT-LIFE
*         Coverage - PUPT-COVERAGE
*         Rider - PUPT-RIDER
*         Plan Suffix - PUPT-PLNSFX.
*       READH the COVRMJA file.
*       Perform the UPDAT-COVR section.
*
*       Repeat this process until the cover and all attached
*       riders have been processed.
*
*  READH the INCR file, with the key set as follows:
*         Company - PUPT-CHDRCOY
*         Contract Number - PUPT-CHDRNUM
*         Life - PUPT-LIFE
*         Coverage - PUPT-COVERAGE
*         Rider - PUPT-RIDER
*         Plan Suffix - PUPT-PLNSFX.
*  UPDATE the INCR record with Validflag '2' and P/Statcode.
*
*  When all processing has been performed for the PUPT record,
*  delete the PUPT record.
*
*  On a change of contract header number or end of file:
*    If a breakout was performed, update the POLSUM field
*    with the new number of summarised policies.
*    Check for the contract being Paid Up:-
*
*       Read all the COVRMJA records for the contract,
*       checking the coverage premium status (PSTATCODE)
*       against the appropriate T5679-SET-COV-PREM-STAT, and
*       coverage risk status (STATCODE) against the appropriate
*       T5679-SET-COV-RISK-STAT.  If the statuses do not match,
*       rewrite the CHDRMJA record and exit; else if the
*       statuses match for all COVRMJA records, subtract 1 from
*       CHDRMJA-TRANNO, rewrite the CHDRMJA record with a
*       validflag of '2', and write a new CHDRMJA record
*       setting CHDRMJA-TRANNO to CHDRMJA-TRANNO + 1,
*       PSTATCODE to T5679-SET-CN-PREM-STAT and STATCODE to
*       T5679-SET-CN-RISK-STAT.
*
*       Note:
*       - BEGN and NEXTR the coverage records as COVRMJA has
*         omit criteria to exclude validflag '2' records.
*
*       - when a contract is paid up, the status is changed -
*         'billing' should check the status to ensure that
*          billing does not occur once a contract is paid up.
*
*       - re-rating of the contract header premium amount is
*         not performed.  Renewals will do this, so the user
*         should be aware that the premium amount may not show
*         the correct value until renewals is run.
*
*     Write a batch header record, and release the softlock
*     on the contract header record.
*
*  UPDAT-COVR Processing
*  ---------------------
*  Rewrite the existing COVRMJA with the validflag set to '2'.
*
*  Create a new COVRMJA record with fields set as follows:
*
*       TRANNO - CHDRMJA-TRANNO
*       INSTPREM - zeros
*       SUMINS - new sum-assured from PUPT
*       PREM-CESS-DATE - CHDRMJA-BTDATE
*
*       if looking at a rider
*          if the rider is not benefit billed
*            call T6597 non forfeiture subroutine
*              if return statuz is okay
*                 set the risk status and prem status =
*                 status from T6597
*              else
*                leave status as before
*            else
*              get the next COVR record
*       else
*            call PUP method from T6598
*            if statuz is okay
*               update statuses with T5679 statuses.
*
*
*       NOTE:  If any  of  the 'T5679 STAT'  fields  above  are
*              spaces,  do not change the corresponding COVRMJA
*              status field(s);  this means that 'paid up' will
*              be determined by the COVR dates being set to the
*              CHDR paid to date - i.e.  checking  the statuses
*              is not sufficient.
*
*
*
*  NOTES
*  -----
*  Tables:
*  -------
*  T5515 - Unit Linked Fund                   Key: Fund
*  T5645 - Transaction Accounting Rules       Key: Program Id.
*  T5679 - Component Status                   Key: Trancode
*  T6647 - Unit Linked Contract Details       Key:
*                                             Trancode||CNTTYPE
*  T6651 - Paid Up Processing Rules           Key: Paid Up
*                                             Method||Currency
*
*  T6651 - Paid Up Processing Rules
*  --------------------------------
*  A new table to hold parameter values used to determine the
*  fees to be charged against a paid up component.
*
*  The table will be keyed on paid up calculation method
*  concatenated with currency.
*
*  The table will be a dated table.
*
*  Parameters are as follows, in a matrix with five
*  occurrences by billing frequency:
*
*       - minimum fee chargeable
*            PUFEEMIN       PIC S9(07)V99
*       - maximum fee chargeable
*            PUFEEMAX       PIC S9(07)V99
*       - percent value to charge
*            FEEPC          PIC S9(03)V99
*       - flat amount to charge
*            PUFFAMT        PIC S9(07)V99
*       - flag to use either bid or offer price
*            BIDOFFER       PIC X
*       - flag to adjust initial units or not
*            ADJUSTIU       PIC X
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are COVRSTS and COVRSTA.
* COVRSTS allow only validflag 1 records, and COVRSTA
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through its life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as its item name, the transaction code and
* the contract status of a contract, e.g. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, e.g. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, e.g. AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to COVRSTA records, and
* current categories refer to COVRSTS records. AGCMSTS
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the AGCMSTS, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out up to and including the TRANNOR number.
*
*
*****************************************************
* </pre>
*/
public class P6001at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P6001AT");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(22);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private PackedDecimalData wsaaLetterDate = new PackedDecimalData(8, 0).isAPartOf(wsaaTransArea, 16);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 21);
	private PackedDecimalData wsaaBusinessDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaTotCovrpuInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaNewsumaTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFundamntTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPupfeeTot = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaRider = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaOldSumins = new PackedDecimalData(13, 2).init(ZERO);
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaEndOfPupt = new FixedLengthStringData(1).init("N");
	private Validator endOfPupt = new Validator(wsaaEndOfPupt, "Y");

	private FixedLengthStringData wsaaEndOfCovr = new FixedLengthStringData(1).init("N");
	private Validator endOfCovr = new Validator(wsaaEndOfCovr, "Y");

	private FixedLengthStringData wsaaEndOfAgcmpup = new FixedLengthStringData(1).init("N");
	private Validator endOfAgcmpup = new Validator(wsaaEndOfAgcmpup, "Y");

	private FixedLengthStringData wsaaChdrPaidup = new FixedLengthStringData(1).init("N");
	protected Validator chdrPaidup = new Validator(wsaaChdrPaidup, "Y");

	private FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).init("N");
	private Validator validStatuz = new Validator(wsaaValidStatuz, "Y");

	private FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1).init("N");
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");

	private FixedLengthStringData wsaaBenefitBilled = new FixedLengthStringData(1).init("N");
	private Validator benefitBilled = new Validator(wsaaBenefitBilled, "Y");

	private FixedLengthStringData wsaaWopRider = new FixedLengthStringData(1).init("N");
	private Validator wopRider = new Validator(wsaaWopRider, "Y");

	private FixedLengthStringData wsaaT6651Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6651Pumeth = new FixedLengthStringData(4).isAPartOf(wsaaT6651Item, 0);
	private FixedLengthStringData wsaaT6651Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT6651Item, 4);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/*01  WSAA-PRO-RATA-TABLE.
		    03  WSAA-PRO-RATA-ITEM      OCCURS 40.                       */
	private PackedDecimalData wsaaOccdate = new PackedDecimalData(8, 0);
		/* ERRORS */
	private static final String h114 = "H114";
	private static final String h021 = "H021";
	private static final String f294 = "F294";
	private static final String g381 = "G381";
	private static final String t035 = "T035";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t6651 = "T6651";
	private static final String t5687 = "T5687";
	private static final String t6597 = "T6597";
	private static final String t6598 = "T6598";
	private static final String tr384 = "TR384";
	private static final String t1693 = "T1693";
	private static final String t7508 = "T7508";
	private static final String tr517 = "TR517";
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Automatic Increase File*/
	private IncrTableDAM incrIO = new IncrTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	protected PayrTableDAM payrIO = new PayrTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
		/*Paid Up Details File*/
	private PuplTableDAM puplIO = new PuplTableDAM();
		/*Paid Up Processing Temporary Logical Fil*/
	private PuptTableDAM puptIO = new PuptTableDAM();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Batckey wsaaBatckey = new Batckey();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	protected Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Actvresrec actvresrec = new Actvresrec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	protected T5679rec t5679rec = new T5679rec();
	private T6598rec t6598rec = new T6598rec();
	private T6651rec t6651rec = new T6651rec();
	private T5687rec t5687rec = new T5687rec();
	private T6597rec t6597rec = new T6597rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T1693rec t1693rec = new T1693rec();
	private T7508rec t7508rec = new T7508rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT,
		checkBrkoutReqd1150,
		exit1199,
		rider2050,
		deletePupt2080,
		rider2330,
		writeCovr2340,
		break8500,
		exit3290,
		writeNewContractHeader3420,
		writeRecord3430,
		exit3599
	}

	public P6001at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
			mainline0001();
			exit0009();
		}

protected void mainline0001()
	{
		initialise1000();
		if (endOfPupt.isTrue()) {
			/*Check whether or not require update of batch header here...   */
			batchHeader3500();
			releaseSoftlock3600();
			return ;
		}
		/*    Begin the PUPT file again, to enable record processing*/
		puptIO.setDataArea(SPACES);
		puptIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		puptIO.setChdrnum(chdrmjaIO.getChdrnum());
		puptIO.setPlanSuffix(ZERO);
		puptIO.setFunction(varcom.begnh);
		puptIO.setFormat(formatsInner.puptrec);
		SmartFileCode.execute(appVars, puptIO);
		if (isNE(puptIO.getStatuz(),varcom.oK)
		&& isNE(puptIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(puptIO.getParams());
			fatalError9000();
		}
		if (isEQ(puptIO.getStatuz(),varcom.endp)
		|| isNE(chdrmjaIO.getChdrnum(),puptIO.getChdrnum())) {
			/*Check whether or not require update of batch header here...   */
			batchHeader3500();
			releaseSoftlock3600();
			return ;
		}
		while ( !(endOfPupt.isTrue())) {
			processPupt2000();
		}

		finalise3000();
		a000Statistics();
	}

protected void exit0009()
	{
		exitProgram();
	}

protected void initialise1000()
	{
		start1010();
	}

protected void start1010()
	{
		/*    Retrieve the contract header and store the transaction number*/
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		wsaaNewsumaTot.set(ZERO);
		wsaaFundamntTot.set(ZERO);
		wsaaOldSumins.set(ZERO);
		wsaaPupfeeTot.set(ZERO);
		/*    Get today's date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError9000();
		}
		wsaaBusinessDate.set(datcon1rec.intDate);
		wsaaTotCovrpuInstprem.set(ZERO);
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		/*MOVE READH                  TO CHDRMJA-FUNCTION.             */
		chdrmjaIO.setFunction(varcom.readr);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		/*MOVE CHDRMJA-TRANNO         TO WSAA-TRANNO.                  */
		/* Read the Unit Linked Contract Details Table (T6647)          */
		/* for the contract.                                            */
		/* PERFORM 4500-READ-T6647.                                     */
		/* DON'T READ T5645 ANYMORE                                     */
		/* Read the Transaction Accounting Rules Table (T5645)          */
		/* for the program.                                             */
		/* PERFORM 4200-READ-T5645.                                     */
		/*    Read the Component Status Table (T5679) for the transaction.*/
		readT56794000();
		/*  MOVE CHDRMJA-CURRFROM       TO WSAA-CURRFROM.                */
		wsaaEndOfPupt.set("N");
		wsaaPlanSuffix.set(9999);
		checkIfBreakoutReqd1100();
	}

protected void checkIfBreakoutReqd1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1110();
					nextr1130();
				case checkBrkoutReqd1150:
					checkBrkoutReqd1150();
					performBrkout1170();
				case exit1199:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1110()
	{
		/*    Read all the PUPT for the contract.*/
		/*    If the Plan Suffix on the first record equals zeros, no*/
		/*    physical breakout is required*/
		/*    else*/
		/*    if the lowest Plan Suffix on PUPT is less than or equal to*/
		/*    CHDRMJA-POLSUM, a breakout is required.*/
		/*    Set the OLD-SUMMARY to the value of CHDRMJA-POLSUM*/
		/*    and the NEW-SUMMARY to the value of the lowest PUPT-PLAN-SUFF*/
		/*    then call 'BRKOUT'.*/
		puptIO.setDataArea(SPACES);
		puptIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		puptIO.setChdrnum(chdrmjaIO.getChdrnum());
		puptIO.setPlanSuffix(ZERO);
		puptIO.setFunction(varcom.begn);

		//performance improvement --  atiwari23
		puptIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		puptIO.setFitKeysSearch("CHDRNUM");

		puptIO.setFormat(formatsInner.puptrec);
		SmartFileCode.execute(appVars, puptIO);
		if (isNE(puptIO.getStatuz(),varcom.oK)
		&& isNE(puptIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(puptIO.getParams());
			fatalError9000();
		}
		if (isEQ(puptIO.getStatuz(),varcom.endp)
		|| isNE(chdrmjaIO.getChdrnum(),puptIO.getChdrnum())) {
			wsaaEndOfPupt.set("Y");
			goTo(GotoLabel.exit1199);
		}
		if (isEQ(puptIO.getPlanSuffix(),ZERO)) {
			wsaaPlanSuffix.set(ZERO);
			goTo(GotoLabel.exit1199);
		}
		/* If policies summarised = 1 then BRKOUT will not be needed       */
		if (isEQ(chdrmjaIO.getPolsum(),1)) {
			wsaaPlanSuffix.set(puptIO.getPlanSuffix());
			goTo(GotoLabel.exit1199);
		}
		puptIO.setFunction(varcom.nextr);
	}

protected void nextr1130()
	{
		/* This is to make sure that if the PUP is required on the last    */
		/* policy in a summarised record that BRKOUT will still be called  */
		/*IF CHDRMJA-POLSUM            > PUPT-PLAN-SUFFIX              */
		if ((setPrecision(chdrmjaIO.getPolsum(), 0)
		&& isGT(chdrmjaIO.getPolsum(),sub(puptIO.getPlanSuffix(),1)))) {
			if (isLT(puptIO.getPlanSuffix(),wsaaPlanSuffix)) {
				wsaaPlanSuffix.set(puptIO.getPlanSuffix());
			}
		}
		SmartFileCode.execute(appVars, puptIO);
		if (isNE(puptIO.getStatuz(),varcom.oK)
		&& isNE(puptIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(puptIO.getParams());
			fatalError9000();
		}
		if (isEQ(puptIO.getStatuz(),varcom.endp)
		|| isNE(chdrmjaIO.getChdrnum(),puptIO.getChdrnum())) {
			goTo(GotoLabel.checkBrkoutReqd1150);
		}
		nextr1130();
		return ;
	}

protected void checkBrkoutReqd1150()
	{
		/* This is to make sure that if the PUP is required on the last    */
		/* policy in a summarised record that BRKOUT will still be called  */
		/*IF CHDRMJA-POLSUM            > WSAA-PLAN-SUFFIX              */
		if ((setPrecision(chdrmjaIO.getPolsum(), 0)
		&& isGT(chdrmjaIO.getPolsum(),sub(wsaaPlanSuffix,1)))) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.exit1199);
		}
	}

protected void performBrkout1170()
	{
		brkoutrec.brkOldSummary.set(chdrmjaIO.getPolsum());
		/* The 'BRKOUT' subroutine was being called with the wrong value   */
		/* in BRK-NEW-SUMMARY                                              */
		/*MOVE WSAA-PLAN-SUFFIX         TO BRK-NEW-SUMMARY.            */
		compute(brkoutrec.brkNewSummary, 0).set(sub(wsaaPlanSuffix,1));
		brkoutrec.brkChdrnum.set(chdrmjaIO.getChdrnum());
		brkoutrec.brkChdrcoy.set(chdrmjaIO.getChdrcoy());
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		brkoutrec.brkStatuz.set(varcom.oK);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz,varcom.oK)) {
			syserrrec.params.set(brkoutrec.outRec);
			syserrrec.statuz.set(brkoutrec.brkStatuz);
			fatalError9000();
		}
	}

protected void processPupt2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2010();
				case rider2050:
					rider2050();
				case deletePupt2080:
					deletePupt2080();
					continue2085();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2010()
	{
		wsaaNewsumaTot.add(puptIO.getNewsuma());
		wsaaFundamntTot.add(puptIO.getFundAmount());
		wsaaPupfeeTot.add(puptIO.getPupfee());
		wsaaBenefitBilled.set("N");
		incrCheck7000();
		/*    If PUPT-RIDER is zeros, pay up the coverage and lapse all*/
		/*    the associated riders*/
		if (isEQ(puptIO.getRider(),"00")) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.rider2050);
		}
		wsaaRider.set(ZERO);
		covrmjaIO.setStatuz(varcom.oK);
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(puptIO.getChdrcoy());
		covrmjaIO.setChdrnum(puptIO.getChdrnum());
		covrmjaIO.setLife(puptIO.getLife());
		covrmjaIO.setCoverage(puptIO.getCoverage());
		covrmjaIO.setRider(puptIO.getRider());
		covrmjaIO.setPlanSuffix(puptIO.getPlanSuffix());
		covrmjaIO.setFunction(varcom.begn);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		/*    PERFORM 2100-PROCESS-COVRS  UNTIL COVRMJA-STATUZ = MRNF.     */
		while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
			processCovrs2100();
		}

		a100DelPolUnderwritting();
		puptIO.setRider("00");
		goTo(GotoLabel.deletePupt2080);
	}

protected void rider2050()
	{
		/*    The rider is non-zero, so only the coverage selected is to*/
		/*    be paid up*/
		wsaaRider.set(puptIO.getRider());
		readhCovr2200();
		/*    IF COVRMJA-STATUZ           = MRNF                           */
		/*       MOVE COVRMJA-PARAMS      TO SYSR-PARAMS                   */
		/*       PERFORM 9000-FATAL-ERROR.                                 */
		wsaaBenefitBilled.set("N");
		checkIfBenefitBilled2400();
		/* If no benefit billing method is found on T5687 for the rider    */
		/* then this rider should be made to lapse                         */
		if (isNE(t5687rec.bbmeth,SPACES)
		&& !wopRider.isTrue()) {
			wsaaBenefitBilled.set("Y");
		}
		else {
			/*    PERFORM 2300-UPDATE-COVR.                         <V42005>*/
			updateCovr2300();
			a200DelCovUnderwritting();
		}
	}

protected void deletePupt2080()
	{
		/*    The temporary PUPT record has been processed, so delete the*/
		/*    record.*/
		puptIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, puptIO);
		if (isNE(puptIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(puptIO.getParams());
			fatalError9000();
		}
		puptIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, puptIO);
		if ((isNE(puptIO.getStatuz(),varcom.oK))
		&& (isNE(puptIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(puptIO.getParams());
			fatalError9000();
		}
		/*    Read the next PUPT record*/
		puptIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, puptIO);
		if (isNE(puptIO.getStatuz(),varcom.oK)
		&& isNE(puptIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(puptIO.getParams());
			fatalError9000();
		}
	}

protected void continue2085()
	{
		if (isEQ(puptIO.getStatuz(),varcom.endp)
		|| isNE(chdrmjaIO.getChdrcoy(),puptIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),puptIO.getChdrnum())) {
			wsaaEndOfPupt.set("Y");
		}
		/*EXIT*/
	}

protected void processCovrs2100()
	{
		/*START*/
		findCovr2250();
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			return ;
		}
		readhCovr2200();
		/*    IF COVRMJA-STATUZ           NOT = O-K                        */
		/* AND COVRMJA-STATUZ          NOT = MRNF                       */
		/*       MOVE COVRMJA-PARAMS      TO SYSR-PARAMS                   */
		/*       PERFORM 9000-FATAL-ERROR.                                 */
		/* IF COVRMJA-STATUZ           = MRNF                           */
		/*    NEXT SENTENCE                                             */
		/* ELSE                                                         */
		/*    PERFORM 2300-UPDATE-COVR                                  */
		/*    ADD 1                    TO WSAA-RIDER                    */
		/*    MOVE WSAA-RIDER          TO PUPT-RIDER.                   */
		/*                                                      <LA4810>*/
		/* PERFORM 2500-ACTIVATE-REASSURANCE.                   <LA4810>*/
		/*                                                      <LA4810>*/
		updateCovr2300();
		activateReassurance2500();
		if (isNE(puptIO.getPlanSuffix(),0)) {
			covrmjaIO.setStatuz(varcom.endp);
			return ;
		}
		covrmjaIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void readhCovr2200()
	{
		start2210();
	}

protected void start2210()
	{
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(puptIO.getChdrcoy());
		covrmjaIO.setChdrnum(puptIO.getChdrnum());
		covrmjaIO.setLife(puptIO.getLife());
		covrmjaIO.setCoverage(puptIO.getCoverage());
		/* MOVE PUPT-RIDER             TO COVRMJA-RIDER.                */
		covrmjaIO.setRider(wsaaRider);
		covrmjaIO.setPlanSuffix(puptIO.getPlanSuffix());
		covrmjaIO.setFunction(varcom.readh);
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
	}

protected void findCovr2250()
	{
		/*START*/
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(puptIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		|| isNE(puptIO.getChdrnum(),covrmjaIO.getChdrnum())
		|| isNE(puptIO.getLife(),covrmjaIO.getLife())
		|| isNE(puptIO.getCoverage(),covrmjaIO.getCoverage())) {
			covrmjaIO.setStatuz(varcom.endp);
		}
		else {
			wsaaRider.set(covrmjaIO.getRider());
		}
		/*EXIT*/
	}

protected void updateCovr2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2310();
				case rider2330:
					rider2330();
				case writeCovr2340:
					writeCovr2340();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2310()
	{
		/*    Rewrite the existing COVRMJA with the validflag set to '2'*/
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(chdrmjaIO.getBtdate());
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
		/*    Create a new COVRMJA record with new fields*/
		wsaaTotCovrpuInstprem.add(covrmjaIO.getInstprem());
		covrmjaIO.setTransactionDate(wsaaTransactionDate);
		covrmjaIO.setTransactionTime(wsaaTransactionTime);
		covrmjaIO.setUser(wsaaUser);
		covrmjaIO.setTermid(wsaaTermid);
		covrmjaIO.setTranno(chdrmjaIO.getTranno());
		covrmjaIO.setValidflag("1");
		/* Do not zeroise INSTPREM as this was required under the wrong    */
		/* assumption that RENEWALS will recalculate CHDR-INSTAMT06 by     */
		/* adding up the COVR's INSTPREM values.                           */
		/*MOVE ZEROS                  TO COVRMJA-INSTPREM.             */
		/* The following updates should only be done if the rider          */
		/* is not benefit billed.                                          */
		/*MOVE PUPT-NEWSUMA           TO COVRMJA-SUMINS.               */
		/*MOVE CHDRMJA-BTDATE         TO COVRMJA-PREM-CESS-DATE.       */
		/* If a coverage has been selected and it has                      */
		/* rider(s) - check the riders for benefit billing                 */
		/*    IF (PUPT-RIDER          NOT = '00') AND                 <020>*/
		if ((isEQ(puptIO.getRider(),"00"))
		&& (isNE(covrmjaIO.getRider(),"00"))) {
			wsaaBenefitBilled.set("N");
			checkIfBenefitBilled2400();
			if (isNE(t5687rec.bbmeth,SPACES)
			&& !wopRider.isTrue()) {
				wsaaBenefitBilled.set("Y");
				goTo(GotoLabel.writeCovr2340);
			}
		}
		/*   NEXT LINES ARE COMMENTED OUT FOR EASE OF READING  **         */
		/*   MODIFICATIONS <037>                               **         */
		/* IF BENEFIT-BILLED                                          <0*/
		/*    NEXT SENTENCE                                           <0*/
		/* ELSE                                                       <0*/
		/*    MOVE PUPT-NEWSUMA        TO COVRMJA-SUMINS              <0*/
		/*    MOVE CHDRMJA-BTDATE      TO COVRMJA-PREM-CESS-DATE.     <0*/
		/* Check COVRMJA-SUMINS - value differs if a rider record          */
		wsaaOldSumins.set(covrmjaIO.getSumins());
		if (benefitBilled.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			/*       IF  PUPT-RIDER       NOT  = '00'                     <037>*/
			if (isNE(covrmjaIO.getRider(),"00")) {
				covrmjaIO.setSumins(ZERO);
			}
			else {
				covrmjaIO.setSumins(puptIO.getNewsuma());
			}
			covrmjaIO.setPremCessDate(chdrmjaIO.getBtdate());
		}
		/* If PUPT-RIDER not = '00' then we are processing a rider         */
		/*IF PUPT-RIDER               = '00'                           */
		if (isNE(puptIO.getRider(),"00")) {
			goTo(GotoLabel.rider2330);
		}
		callPupMethod5000();
		/* Do not reset the statii if the coverage has been lapsed in      */
		/* section 5000-....                                               */
		if (isEQ(t5687rec.pumeth,SPACES)) {
			goTo(GotoLabel.writeCovr2340);
		}
		if (isNE(t5679rec.setCovPremStat,SPACES)) {
			covrmjaIO.setPstatcode(t5679rec.setCovPremStat);
		}
		if (isNE(t5679rec.setCovRiskStat,SPACES)) {
			covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
		}
		goTo(GotoLabel.writeCovr2340);
	}

protected void rider2330()
	{
		callPupMethod5000();
		/* Do not reset the statii if the rider has been lapsed in         */
		/* section 5000-....                                               */
		if (isEQ(t5687rec.pumeth,SPACES)) {
			goTo(GotoLabel.writeCovr2340);
		}
		if (isNE(t5679rec.setRidPremStat,SPACES)) {
			covrmjaIO.setPstatcode(t5679rec.setRidPremStat);
		}
		if (isNE(t5679rec.setRidRiskStat,SPACES)) {
			covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
		}
	}

	/**
	* <pre>
	**** PERFORM 6000-LAPSE-METHOD.                              <027>
	*    IF T5679-SET-RID-PREM-STAT NOT = SPACES
	*       MOVE T5679-SET-RID-PREM-STAT TO COVRMJA-PSTATCODE.
	*    IF T5679-SET-RID-RISK-STAT NOT = SPACES
	*       MOVE T5679-SET-RID-RISK-STAT TO COVRMJA-STATCODE.
	* </pre>
	*/
protected void writeCovr2340()
	{
		covrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void checkIfBenefitBilled2400()
	{
		start2410();
	}

protected void start2410()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");


		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if ((isNE(itdmIO.getItemcoy(),atmodrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5687))
		|| (isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemitem(covrmjaIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			itdmIO.setStatuz(f294);
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		/*    If it is benefit bill rider, here we also need to check      */
		/*    whether it is WOP rider. Need to lapse WOP rider when the    */
		/*    contract status turns to Paid-Up.                            */
		wsaaWopRider.set("N");
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tr517);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(),tr517)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaWopRider.set("N");
		}
		else {
			wsaaWopRider.set("Y");
		}
	}

	/**
	* <pre>
	*2500-PRO-RATA SECTION.
	*2510-START.
	*    Read T6651 to obtain the paid up processing fee rules
	*    PERFORM 4100-READ-T6651.
	*    MOVE ZEROS                  TO WSAA-TOTAL-COMPONENTS-FUNDS
	*                                   WSAA-TOTAL-REAL-FUNDS.
	*    PERFORM 2600-INIT-TABLE     VARYING WSAA-SUB
	*                                FROM 1 BY 1
	*                                UNTIL WSAA-SUB > 40.
	*    MOVE ZEROS                  TO WSAA-SUB.
	*    Begin reading the UTRS file
	*    MOVE SPACES                 TO UTRS-DATA-KEY.
	*    MOVE PUPT-CHDRCOY           TO UTRS-CHDRCOY.
	*    MOVE PUPT-CHDRNUM           TO UTRS-CHDRNUM.
	*    MOVE PUPT-LIFE              TO UTRS-LIFE.
	*    MOVE PUPT-COVERAGE          TO UTRS-COVERAGE.
	*    MOVE PUPT-RIDER             TO UTRS-RIDER.
	*    MOVE PUPT-PLAN-SUFFIX       TO UTRS-PLAN-SUFFIX.
	*    MOVE BEGN                   TO UTRS-FUNCTION.
	*    MOVE UTRSREC                TO UTRS-FORMAT.
	*    CALL 'UTRSIO'               USING UTRS-PARAMS.
	*    IF UTRS-STATUZ              NOT = O-K
	*    AND UTRS-STATUZ             NOT = ENDP
	*       MOVE UTRS-PARAMS         TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*    IF PUPT-CHDRCOY             NOT = UTRS-CHDRCOY
	*    OR PUPT-CHDRNUM             NOT = UTRS-CHDRNUM
	*    OR PUPT-LIFE                NOT = UTRS-LIFE
	*    OR PUPT-COVERAGE            NOT = UTRS-COVERAGE
	*    OR PUPT-RIDER               NOT = UTRS-RIDER
	*    OR PUPT-PLAN-SUFFIX         NOT = UTRS-PLAN-SUFFIX
	*    OR UTRS-STATUZ              = ENDP
	*       GO 2508-FINAL-CHECK.
	*    PERFORM 4600-READ-T5515.
	*    PERFORM 4800-GET-FUND-PRICE.
	*    MOVE 'N'                    TO WSAA-END-OF-FUND.
	*    PERFORM 2530-PROCESS-FUNDS  UNTIL END-OF-FUND.
	*2508-FINAL-CHECK.
	*    PERFORM 2550-PROCESS-FUND.
	*    Check that the total-components-funds field is the same as th
	*    PUPT fund value field; if not, abort the program.
	*    Temporarily commented out because test fails through
	*    rounding - pending further study.
	*    IF WSAA-TOTAL-COMPONENTS-FUNDS = PUPT-FUND-AMOUNT
	*       NEXT SENTENCE
	*    ELSE
	*       MOVE H349                TO SYSR-STATUZ
	*       PERFORM 9000-FATAL-ERROR.
	*    Write UTRN records for each entry in the table.
	*    PERFORM 4900-SETUP-COMMON-UTRN.
	*    PERFORM 2590-UTRN-FROM-TABLE VARYING WSAA-SUB
	*                                 FROM 1 BY 1
	*                                 UNTIL WSAA-SUB > 40.
	*2509-EXIT.
	*    EXIT.
	*2530-PROCESS-FUNDS SECTION.
	*2531-START.
	*    IF UTRS-CURRENT-UNIT-BAL    = ZEROS
	*       GO 2535-NEXTR.
	*    IF UTRS-UNIT-VIRTUAL-FUND   NOT = WSAA-UNIT-VIRTUAL-FUND
	*       PERFORM 2550-PROCESS-FUND
	*       PERFORM 4600-READ-T5515
	*       PERFORM 4800-GET-FUND-PRICE.
	*    IF UTRS-UNIT-TYPE           = 'I'
	*       IF T6651-ADJUSTIU        = 'Y'
	*          ADD UTRS-CURRENT-UNIT-BAL TO WSAA-TOTAL-REAL-FUNDS
	*          PERFORM 2570-WRITE-ADJUST-UTRNS
	*       ELSE
	*          GO 2535-NEXTR
	*    ELSE
	*       ADD UTRS-CURRENT-UNIT-BAL TO WSAA-TOTAL-REAL-FUNDS.
	*2535-NEXTR.
	*    MOVE NEXTR                  TO UTRS-FUNCTION.
	*    CALL 'UTRSIO'               USING UTRS-PARAMS.
	*    IF UTRS-STATUZ              NOT = O-K
	*    AND UTRS-STATUZ             NOT = ENDP
	*       MOVE UTRS-PARAMS         TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*    IF PUPT-CHDRCOY             NOT = UTRS-CHDRCOY
	*    OR PUPT-CHDRNUM             NOT = UTRS-CHDRNUM
	*    OR PUPT-LIFE                NOT = UTRS-LIFE
	*    OR PUPT-COVERAGE            NOT = UTRS-COVERAGE
	*    OR PUPT-RIDER               NOT = UTRS-RIDER
	*    OR PUPT-PLAN-SUFFIX         NOT = UTRS-PLAN-SUFFIX
	*    OR UTRS-STATUZ              = ENDP
	*       MOVE 'Y'                 TO WSAA-END-OF-FUND.
	*2539-EXIT.
	*    EXIT.
	*2550-PROCESS-FUND SECTION.
	*2551-START.
	*    IF WSAA-TOTAL-REAL-FUNDS   = ZEROS
	*       GO 2559-EXIT.
	*    COMPUTE WSAA-FUND-VALUE    = WSAA-TOTAL-REAL-FUNDS
	*                               * WSAA-BIDOFFER-PRICE.
	*    IF T5515-CURRCODE          = CHDRMJA-CNTCURR
	*       GO 2555-WRITE-TABLE-ENTRY.
	*    As the payment currency is different to the fund currency,
	*    a conversion is required, resulting in an amount in the
	*    payment currency
	*    MOVE SPACES                TO CLNK-CLNK002-REC.
	*    MOVE ATMD-COMPANY          TO CLNK-COMPANY.
	*    MOVE CHDRMJA-BTDATE        TO CLNK-CASHDATE.
	*    MOVE T5515-CURRCODE        TO CLNK-CURR-IN.
	*    MOVE CHDRMJA-CNTCURR       TO CLNK-CURR-OUT.
	*    MOVE WSAA-FUND-VALUE       TO CLNK-AMOUNT-IN.
	*    MOVE ZEROS                 TO CLNK-AMOUNT-OUT.
	**** MOVE 'CVRT'                TO CLNK-FUNCTION.
	*    MOVE 'SURR'                TO CLNK-FUNCTION.
	*    CALL 'XCVRT'               USING CLNK-CLNK002-REC.
	*    IF  CLNK-STATUZ            = BOMB
	*        MOVE CLNK-CLNK002-REC  TO SYSR-PARAMS
	*        MOVE CLNK-STATUZ       TO SYSR-STATUZ
	*        PERFORM 9000-FATAL-ERROR.
	*    MOVE CLNK-AMOUNT-OUT       TO WSAA-FUND-VALUE.
	*2555-WRITE-TABLE-ENTRY.
	*    ADD 1                      TO WSAA-SUB.
	*    IF WSAA-SUB                > 41
	*       MOVE H351               TO SYSR-STATUZ
	*       PERFORM 9000-FATAL-ERROR.
	*    MOVE WSAA-UNIT-VIRTUAL-FUND TO WSAA-FUND(WSAA-SUB).
	*    MOVE CHDRMJA-CNTCURR       TO WSAA-FUND-CURR(WSAA-SUB).
	*    MOVE WSAA-FUND-VALUE       TO WSAA-FUND-AMOUNT(WSAA-SUB).
	*    Add the fund value to the total fund value
	*    ADD WSAA-FUND-VALUE        TO WSAA-TOTAL-COMPONENTS-FUNDS.
	*    Initialise the fund accumulator
	*    MOVE ZEROS                 TO WSAA-TOTAL-REAL-FUNDS.
	*2559-EXIT.
	*    EXIT.
	*2570-WRITE-ADJUST-UTRNS SECTION.
	*2571-START.
	*    PERFORM 4900-SETUP-COMMON-UTRN.
	*    MOVE WSAA-UNIT-VIRTUAL-FUND TO UTRN-UNIT-VIRTUAL-FUND.
	*    MOVE 'I'                    TO UTRN-UNIT-TYPE.
	*    MOVE 'INIU'                 TO UTRN-UNIT-SUB-ACCOUNT.
	*    COMPUTE UTRN-NOF-UNITS      = UTRS-CURRENT-UNIT-BAL * -1.
	*    COMPUTE UTRN-NOF-DUNITS     = UTRS-CURRENT-DUNIT-BAL * -1.
	*    MOVE T5515-CURRCODE         TO UTRN-FUND-CURRENCY.
	*    MOVE WRITR                  TO UTRN-FUNCTION.
	*    MOVE UTRNREC                TO UTRN-FORMAT.
	*    CALL 'UTRNIO'               USING UTRN-PARAMS.
	*    IF UTRN-STATUZ              NOT = O-K
	*       MOVE UTRN-PARAMS         TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*    MOVE 'A'                    TO UTRN-UNIT-TYPE.
	*    MOVE 'ACMU'                 TO UTRN-UNIT-SUB-ACCOUNT.
	*    MOVE UTRS-CURRENT-UNIT-BAL  TO UTRN-NOF-UNITS
	*                                   UTRN-NOF-DUNITS.
	*    MOVE VRCM-TERMID            TO UTRN-TERMID.
	*    MOVE 1                      TO UTRN-SVP.
	*    MOVE WRITR                  TO UTRN-FUNCTION.
	*    MOVE UTRNREC                TO UTRN-FORMAT.
	*    CALL 'UTRNIO'               USING UTRN-PARAMS.
	*    IF UTRN-STATUZ              NOT = O-K
	*       MOVE UTRN-PARAMS         TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*2579-EXIT.
	*    EXIT.
	*2590-UTRN-FROM-TABLE SECTION.
	*2591-START.
	*    IF WSAA-FUND(WSAA-SUB)      = SPACES
	*       MOVE 41                  TO WSAA-SUB
	*       GO 2599-EXIT.
	*    MOVE WSAA-FUND(WSAA-SUB)    TO UTRN-UNIT-VIRTUAL-FUND.
	*    MOVE 'A'                    TO UTRN-UNIT-TYPE.
	*    MOVE 'ACMU'                 TO UTRN-UNIT-SUB-ACCOUNT.
	*    MOVE WSAA-FUND-CURR(WSAA-SUB) TO UTRN-FUND-CURRENCY.
	*    COMPUTE UTRN-CONTRACT-AMOUNT = PUPT-PUPFEE
	*                                 * WSAA-FUND-AMOUNT(WSAA-SUB)
	*                                 / WSAA-TOTAL-COMPONENTS-FUNDS
	*                                 * -1.
	*    MOVE WRITR                  TO UTRN-FUNCTION.
	*    MOVE UTRNREC                TO UTRN-FORMAT.
	*    CALL 'UTRNIO'               USING UTRN-PARAMS.
	*    IF UTRN-STATUZ              NOT = O-K
	*       MOVE UTRN-PARAMS         TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*2599-EXIT.
	*    EXIT.
	*2600-INIT-TABLE SECTION.
	*2610-START.
	*    MOVE SPACES                 TO WSAA-FUND(WSAA-SUB)
	*                                   WSAA-FUND-CURR(WSAA-SUB).
	*    MOVE ZEROS                  TO WSAA-FUND-AMOUNT(WSAA-SUB).
	*2690-EXIT.
	*    EXIT.
	*2700-COMMISSION-CLAWBACK SECTION.
	*2710-START.
	*    MOVE SPACES                 TO AGCMPUP-DATA-AREA.
	*    MOVE PUPT-CHDRCOY           TO AGCMPUP-CHDRCOY.
	*    MOVE PUPT-CHDRNUM           TO AGCMPUP-CHDRNUM.
	*    MOVE PUPT-LIFE              TO AGCMPUP-LIFE.
	*    MOVE PUPT-COVERAGE          TO AGCMPUP-COVERAGE.
	*    MOVE PUPT-RIDER             TO AGCMPUP-RIDER.
	*    MOVE PUPT-PLAN-SUFFIX       TO AGCMPUP-PLAN-SUFFIX.
	*    MOVE BEGN                   TO AGCMPUP-FUNCTION.
	*    MOVE AGCMPUPREC             TO AGCMPUP-FORMAT.
	*    CALL 'AGCMPUPIO'            USING AGCMPUP-PARAMS.
	*    IF AGCMPUP-STATUZ           NOT = O-K
	*    AND AGCMPUP-STATUZ          NOT = ENDP
	*       MOVE AGCMPUP-PARAMS      TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*    IF PUPT-CHDRCOY             NOT = AGCMPUP-CHDRCOY
	*    OR PUPT-CHDRNUM             NOT = AGCMPUP-CHDRNUM
	*    OR PUPT-LIFE                NOT = AGCMPUP-LIFE
	*    OR PUPT-COVERAGE            NOT = AGCMPUP-COVERAGE
	*    OR PUPT-RIDER               NOT = AGCMPUP-RIDER
	*    OR PUPT-PLAN-SUFFIX         NOT = AGCMPUP-PLAN-SUFFIX
	*    OR AGCMPUP-STATUZ           = ENDP
	*       GO 2790-EXIT.
	*    MOVE AGCMPUP-AGNTNUM        TO WSAA-STORED-AGNTNUM.
	*    MOVE ZEROS                  TO WSAA-ACCUMULATE-EARNED
	*                                   WSAA-ACCUMULATE-PAID
	*                                   WSAA-ACCUM-OVRD-EARNED
	*                                   WSAA-ACCUM-OVRD-PAID.
	*    MOVE 'N'                    TO WSAA-END-OF-AGCMPUP.
	*    PERFORM 2800-PROCESS-AGCMPUP UNTIL END-OF-AGCMPUP.
	*    PERFORM 2870-POST-CLAWBACK.
	*2790-EXIT.
	*    EXIT.
	*2800-PROCESS-AGCMPUP SECTION.
	*2801-GO.
	*    IF WSAA-STORED-AGNTNUM      = AGCMPUP-AGNTNUM
	*       GO 2805-NEXTR.
	*    PERFORM 2870-POST-CLAWBACK.
	*    MOVE ZEROS                  TO WSAA-ACCUMULATE-EARNED
	*                                   WSAA-ACCUMULATE-PAID
	*                                   WSAA-ACCUM-OVRD-EARNED
	*                                   WSAA-ACCUM-OVRD-PAID.
	*    MOVE AGCMPUP-AGNTNUM        TO WSAA-STORED-AGNTNUM.
	*2805-NEXTR.
	*    IF AGCMPUP-OVRDCAT          = 'O'
	*       ADD AGCMPUP-COMERN          TO WSAA-ACCUM-OVRD-EARNED
	*       ADD AGCMPUP-COMPAY          TO WSAA-ACCUM-OVRD-PAID
	*    ELSE
	*       ADD AGCMPUP-COMERN          TO WSAA-ACCUMULATE-EARNED
	*       ADD AGCMPUP-COMPAY          TO WSAA-ACCUMULATE-PAID
	*    END-IF.
	*    ADD AGCMPUP-COMERN          TO WSAA-ACCUMULATE-EARNED.
	*    ADD AGCMPUP-COMPAY          TO WSAA-ACCUMULATE-PAID.
	*    MOVE NEXTR                  TO AGCMPUP-FUNCTION.
	*    CALL 'AGCMPUPIO'            USING AGCMPUP-PARAMS.
	*    IF AGCMPUP-STATUZ           = ENDP
	*       MOVE 'Y'                 TO WSAA-END-OF-AGCMPUP
	*       GO 2809-EXIT.
	*    IF AGCMPUP-STATUZ           NOT = O-K
	*       MOVE AGCMPUP-PARAMS      TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*    IF PUPT-CHDRCOY             NOT = AGCMPUP-CHDRCOY
	*    OR PUPT-CHDRNUM             NOT = AGCMPUP-CHDRNUM
	*    OR PUPT-LIFE                NOT = AGCMPUP-LIFE
	*    OR PUPT-COVERAGE            NOT = AGCMPUP-COVERAGE
	*    OR PUPT-RIDER               NOT = AGCMPUP-RIDER
	*    OR PUPT-PLAN-SUFFIX         NOT = AGCMPUP-PLAN-SUFFIX
	*       MOVE 'Y'                 TO WSAA-END-OF-AGCMPUP.
	*2809-EXIT.
	*    EXIT.
	*2870-POST-CLAWBACK SECTION.
	*2871-START.
	*    COMPUTE WSAA-OVRD-COMM-CLAWBACK = WSAA-ACCUM-OVRD-EARNED
	*                                - WSAA-ACCUM-OVRD-PAID.
	*    COMPUTE WSAA-COMM-CLAWBACK  = WSAA-ACCUMULATE-EARNED
	*                                - WSAA-ACCUMULATE-PAID.
	*    PERFORM 2900-SETUP-COMMON-ACMV.
	*    IF   WSAA-COMM-CLAWBACK     = 0
	*         GO TO 2875-OVERRIDE-COMM.
	*    MOVE WSAA-COMM-CLAWBACK     TO LIFA-ORIGAMT.
	*    MOVE T5645-SACSCODE-01      TO LIFA-SACSCODE.
	*    MOVE T5645-SACSTYPE-01      TO LIFA-SACSTYP.
	*    MOVE T5645-GLMAP-01         TO LIFA-GLCODE.
	*    MOVE T5645-SIGN-01          TO LIFA-GLSIGN.
	*    MOVE T5645-CNTTOT-01        TO LIFA-CONTOT.
	*    CALL 'LIFACMV'              USING LIFA-LIFACMV-REC.
	*    IF LIFA-STATUZ              NOT = O-K
	*         MOVE LIFA-LIFACMV-REC  TO SYSR-PARAMS
	*         PERFORM 9000-FATAL-ERROR.
	*    MOVE WSAA-COMM-CLAWBACK     TO LIFA-ORIGAMT.
	*    MOVE T5645-SACSCODE-02      TO LIFA-SACSCODE.
	*    MOVE T5645-SACSTYPE-02      TO LIFA-SACSTYP.
	*    MOVE T5645-GLMAP-02         TO LIFA-GLCODE.
	*    MOVE T5645-SIGN-02          TO LIFA-GLSIGN.
	*    MOVE T5645-CNTTOT-02        TO LIFA-CONTOT.
	*    CALL 'LIFACMV'              USING LIFA-LIFACMV-REC.
	*    IF LIFA-STATUZ              NOT = O-K
	*         MOVE LIFA-LIFACMV-REC  TO SYSR-PARAMS
	*         PERFORM 9000-FATAL-ERROR.
	*2875-OVERRIDE-COMM.
	*    IF WSAA-OVRD-COMM-CLAWBACK  = ZEROES
	*         GO TO 2879-EXIT
	*    END-IF.
	*    MOVE WSAA-OVRD-COMM-CLAWBACK TO LIFA-ORIGAMT.
	*    MOVE AGCMPUP-CEDAGENT       TO LIFA-TRANREF.
	*    MOVE T5645-SACSCODE-04      TO LIFA-SACSCODE.
	*    MOVE T5645-SACSTYPE-04      TO LIFA-SACSTYP.
	*    MOVE T5645-GLMAP-04         TO LIFA-GLCODE.
	*    MOVE T5645-SIGN-04          TO LIFA-GLSIGN.
	*    MOVE T5645-CNTTOT-04        TO LIFA-CONTOT.
	*    CALL 'LIFACMV'              USING LIFA-LIFACMV-REC.
	*    IF LIFA-STATUZ              NOT = O-K
	*         MOVE LIFA-LIFACMV-REC  TO SYSR-PARAMS
	*         PERFORM 9000-FATAL-ERROR.
	*    MOVE WSAA-OVRD-COMM-CLAWBACK TO LIFA-ORIGAMT.
	*    MOVE AGCMPUP-CEDAGENT       TO LIFA-TRANREF.
	*    MOVE T5645-SACSCODE-05      TO LIFA-SACSCODE.
	*    MOVE T5645-SACSTYPE-05      TO LIFA-SACSTYP.
	*    MOVE T5645-GLMAP-05         TO LIFA-GLCODE.
	*    MOVE T5645-SIGN-05          TO LIFA-GLSIGN.
	*    MOVE T5645-CNTTOT-05        TO LIFA-CONTOT.
	*    CALL 'LIFACMV'              USING LIFA-LIFACMV-REC.
	*    IF LIFA-STATUZ              NOT = O-K
	*         MOVE LIFA-LIFACMV-REC  TO SYSR-PARAMS
	*         PERFORM 9000-FATAL-ERROR.
	*2879-EXIT.
	*    EXIT.
	*2900-SETUP-COMMON-ACMV SECTION.
	*2910-START.
	*    MOVE SPACES                 TO LIFA-LIFACMV-REC.
	*    MOVE 'PSTW'                 TO LIFA-FUNCTION.
	*    MOVE WSAA-BATCKEY           TO LIFA-BATCKEY.
	*    MOVE CHDRMJA-CHDRNUM        TO LIFA-RDOCNUM.
	*    MOVE CHDRMJA-TRANNO         TO LIFA-TRANNO.
	*    MOVE CHDRMJA-CHDRCOY        TO LIFA-RLDGCOY.
	*    MOVE CHDRMJA-CHDRCOY        TO LIFA-GENLCOY.
	*    MOVE AGCMPUP-AGNTNUM        TO LIFA-RLDGACCT.
	*    MOVE CHDRMJA-CNTCURR        TO LIFA-ORIGCURR.
	*    MOVE WSAA-COMM-CLAWBACK     TO LIFA-ORIGAMT.
	*    MOVE CHDRMJA-TRANNO         TO LIFA-TRANREF.
	*    MOVE DESC-LONGDESC          TO LIFA-TRANDESC.
	*    MOVE CHDRMJA-BTDATE         TO LIFA-EFFDATE.
	*    MOVE WSAA-TERMID            TO LIFA-TERMID.
	*    MOVE WSAA-USER              TO LIFA-USER.
	*    MOVE WSAA-TRANSACTION-TIME  TO LIFA-TRANSACTION-TIME.
	*    MOVE WSAA-TRANSACTION-DATE  TO LIFA-TRANSACTION-DATE.
	*    MOVE CHDRMJA-CNTTYPE        TO LIFA-SUBSTITUTE-CODE(01).
	*    MOVE VRCM-MAX-DATE          TO LIFA-FRCDATE.
	*    MOVE ZEROS                  TO LIFA-RCAMT
	*                                   LIFA-ACCTAMT
	*                                   LIFA-CRATE
	*                                   LIFA-POSTYEAR
	*                                   LIFA-POSTMONTH
	*                                   LIFA-JRNSEQ.
	*2990-EXIT.
	*    EXIT.
	***********************************                       <R96REA>
	* </pre>
	*/
protected void activateReassurance2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					activation2501();
				case break8500:
					break8500();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	***********************************                       <R96REA>
	* </pre>
	*/
protected void activation2501()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(covrmjaIO.getChdrcoy());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifeenqIO.setChdrnum(covrmjaIO.getChdrnum());
		lifeenqIO.setLife(covrmjaIO.getLife());
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(formatsInner.lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError9000();
		}
		wsaaL1Clntnum.set(lifeenqIO.getLifcnum());
		if (isNE(covrmjaIO.getJlife(),"01")) {
			goTo(GotoLabel.break8500);
		}
		lifeenqIO.setParams(SPACES);
		lifeenqIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifeenqIO.setChdrnum(covrmjaIO.getChdrnum());
		lifeenqIO.setLife(covrmjaIO.getLife());
		lifeenqIO.setJlife("01");
		lifeenqIO.setFunction(varcom.readr);
		lifeenqIO.setFormat(formatsInner.lifeenqrec);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(),varcom.oK)
		&& isNE(lifeenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError9000();
		}
		if (isEQ(lifeenqIO.getStatuz(),varcom.oK)) {
			wsaaL2Clntnum.set(lifeenqIO.getLifcnum());
		}
		else {
			wsaaL2Clntnum.set(SPACES);
		}
	}

protected void break8500()
	{
		/*                                                      <R96REA>*/
		/* Call to ACTVRES Subroutine.                          <R96REA>*/
		/*                                                      <R96REA>*/
		actvresrec.actvresRec.set(SPACES);
		actvresrec.function.set("ACT8");
		actvresrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		actvresrec.chdrnum.set(chdrmjaIO.getChdrnum());
		actvresrec.life.set(covrmjaIO.getLife());
		actvresrec.coverage.set(covrmjaIO.getCoverage());
		actvresrec.rider.set(covrmjaIO.getRider());
		actvresrec.planSuffix.set(covrmjaIO.getPlanSuffix());
		actvresrec.crtable.set(covrmjaIO.getCrtable());
		actvresrec.clntcoy.set(t1693rec.fsuco);
		actvresrec.l1Clntnum.set(wsaaL1Clntnum);
		actvresrec.l2Clntnum.set(wsaaL2Clntnum);
		actvresrec.jlife.set(covrmjaIO.getJlife());
		actvresrec.currency.set(chdrmjaIO.getCntcurr());
		actvresrec.oldSumins.set(wsaaOldSumins);
		actvresrec.newSumins.set(covrmjaIO.getSumins());
		actvresrec.effdate.set(covrmjaIO.getCrrcd());
		actvresrec.cnttype.set(chdrmjaIO.getCnttype());
		actvresrec.tranno.set(chdrmjaIO.getTranno());
		actvresrec.crrcd.set(covrmjaIO.getCrrcd());
		actvresrec.language.set(atmodrec.language);
		actvresrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Actvres.class, actvresrec.actvresRec);
		if (isNE(actvresrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(actvresrec.statuz);
			syserrrec.params.set(actvresrec.actvresRec);
			fatalError9000();
		}
	}

protected void finalise3000()
	{
		start3010();
	}

protected void start3010()
	{
		writePtrn3100();
		wsaaEndOfCovr.set("N");
		wsaaValidStatuz.set("N");
		wsaaChdrPaidup.set("N");
		wsaaValidPstatcode.set("Y");
		while ( !(endOfCovr.isTrue())) {
			checkChdrPaidup3200();
		}

		if (validStatuz.isTrue()) {
			wsaaChdrPaidup.set("Y");
		}
		updateChdr3400();
		batchHeader3500();
		processLetter3550();
		dryProcessing8000();
		releaseSoftlock3600();
	}

protected void writePtrn3100()
	{
		start3110();
	}

protected void start3110()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setPtrneff(wsaaBusinessDate);
		ptrnIO.setDatesub(wsaaBusinessDate);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setValidflag("1");
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError9000();
		}
	}

protected void checkChdrPaidup3200()
	{
			try {
					start3210();
					checkStatuz3250();
			}
			catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start3210()
	{
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFunction(varcom.begn);

		//performance improvement --  atiwari23
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		covrmjaIO.setFormat(formatsInner.covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)
		|| isNE(chdrmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum())) {
			wsaaEndOfCovr.set("Y");
			goTo(GotoLabel.exit3290);
		}
	}

protected void checkStatuz3250()
	{
		/*MOVE 'N'                    TO WSAA-VALID-STATUZ.            */
		/*IF (T5679-SET-COV-RISK-STAT = COVRMJA-STATCODE) OR*/
		/*(T5679-SET-COV-RISK-STAT = SPACE )*/
		/*IF (T5679-SET-COV-PREM-STAT = COVRMJA-PSTATCODE) OR*/
		/*(T5679-SET-COV-PREM-STAT = SPACE )*/
		/*MOVE 'Y'              TO WSAA-VALID-STATUZ.*/
		if ((isNE(covrmjaIO.getRider(),"00"))) {
			wsaaBenefitBilled.set("N");
			checkIfBenefitBilled3400();
			if (isNE(t5687rec.bbmeth,SPACES)
			&& !wopRider.isTrue()) {
				wsaaBenefitBilled.set("Y");
			}
			else {
				wsaaBenefitBilled.set("N");
			}
		}
		if ((isNE(covrmjaIO.getRider(),"00"))) {
			if (benefitBilled.isTrue()) {
				wsaaValidStatuz.set("Y");
			}
			else if(isEQ(t5687rec.pumeth,SPACES)){
				checkRiderStatuz3350();
			}else{
				checkCoverageStatuz3300();
			}
		}
		else {
			checkCoverageStatuz3300();
		}
		/*PERFORM 3300-COVR-STATUZ-CHECK  VARYING WSAA-SUB*/
		/*                                FROM 1 BY 1*/
		/*                                UNTIL WSAA-SUB > 12*/
		/*                                OR VALID-STATUZ.*/
		/*PERFORM 3300-COVR-PSTATUS-CHECK VARYING WSAA-SUB             */
		/*                                FROM 1 BY 1                  */
		/*                                  UNTIL WSAA-SUB > 12          */
		/*                                  OR NOT VALID-PSTATCODE.      */
		/* IF VALID-STATUZ                                              */
		/*    GO 3290-EXIT.                                             */
		if (validStatuz.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaEndOfCovr.set("Y");
			return ;
		}


		//performance improvement --  atiwari23
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError9000();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)
		|| isNE(chdrmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum())) {
			wsaaEndOfCovr.set("Y");
		}
		else {
			checkStatuz3250();
			return ;
		}
	}

protected void checkCoverageStatuz3300()
	{
		/*START*/
		if ((isNE(t5679rec.setCovRiskStat,SPACES))
		&& (isEQ(t5679rec.setCovRiskStat,covrmjaIO.getStatcode()))) {
			if ((isNE(t5679rec.setCovPremStat,SPACES))
			&& (isEQ(t5679rec.setCovPremStat,covrmjaIO.getPstatcode()))) {
				wsaaValidStatuz.set("Y");
			}
			else {
				wsaaValidStatuz.set("N");
			}
		}
		else {
			wsaaValidStatuz.set("N");
		}
		/*EXIT*/
	}

protected void checkRiderStatuz3350()
	{
		/*START*/
		if ((isNE(t5679rec.setRidRiskStat,SPACES))
		&& (isEQ(t5679rec.setRidRiskStat,covrmjaIO.getStatcode()))) {
			if ((isNE(t5679rec.setRidPremStat,SPACES))
			&& (isEQ(t5679rec.setRidPremStat,covrmjaIO.getPstatcode()))) {
				wsaaValidStatuz.set("Y");
			}
			else {
				wsaaValidStatuz.set("N");
			}
		}
		else {
			wsaaValidStatuz.set("N");
		}
		/*EXIT*/
	}

protected void checkIfBenefitBilled3400()
	{
		start3410();
	}

protected void start3410()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if ((isNE(itdmIO.getItemcoy(),atmodrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5687))
		|| (isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemitem(covrmjaIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			itdmIO.setStatuz(f294);
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		/*    If it is benefit bill rider, here we also need to check      */
		/*    whether it is WOP rider. Need to lapse WOP rider when the    */
		/*    contract status turn into Paid-Up.                           */
		wsaaWopRider.set("N");
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tr517);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(),tr517)
		|| isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaWopRider.set("N");
		}
		else {
			wsaaWopRider.set("Y");
		}
	}

	/**
	* <pre>
	*3300-COVR-STATUZ-CHECK SECTION.
	*3310-COVR-STATUZ-CHECK.
	*    IF T5679-COV-RISK-STAT(WSAA-SUB) NOT = SPACE
	*        IF T5679-COV-RISK-STAT(WSAA-SUB) = COVRMJA-STATCODE
	*        AND T5679-COV-PREM-STAT(WSAA-SUB) = COVRMJA-PSTATCODE
	*            MOVE 'Y'            TO WSAA-VALID-STATUZ.
	*3390-EXIT.
	*     EXIT.
	*3300-COVR-PSTATUS-CHECK SECTION.
	*3310-COVR-PSTATUS-CHECK.
	*    IF  T5679-COV-PREM-STAT(WSAA-SUB) NOT = SPACE
	*        IF T5679-COV-PREM-STAT(WSAA-SUB) NOT = COVRMJA-PSTATCODE
	*            MOVE 'N'            TO WSAA-VALID-PSTATCODE.
	*    IF  T5679-COV-RISK-STAT(WSAA-SUB) NOT = SPACE
	*        IF T5679-COV-RISK-STAT(WSAA-SUB) NOT = COVRMJA-STATCODE
	*            MOVE 'N'            TO WSAA-VALID-PSTATCODE.
	*3390-EXIT.
	*     EXIT.
	* THIS SECTION HAS BEEN REWRITTEN - <018> *************
	*3400-UPDATE-CHDR SECTION.
	*3410-START.
	* Read and update contract header before update
	*    MOVE SPACES                 TO CHDRMJA-DATA-AREA.
	*    MOVE ATMD-COMPANY           TO CHDRMJA-CHDRCOY.
	*    MOVE WSAA-PRIMARY-CHDRNUM   TO CHDRMJA-CHDRNUM.
	*    MOVE READH                  TO CHDRMJA-FUNCTION.
	*    MOVE CHDRMJAREC             TO CHDRMJA-FORMAT.
	*    CALL 'CHDRMJAIO'            USING CHDRMJA-PARAMS.
	*    IF CHDRMJA-STATUZ           NOT = O-K
	*       MOVE CHDRMJA-PARAMS      TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*    MOVE WSAA-TRANNO            TO CHDRMJA-TRANNO.
	* Make sure the CHDR SINSTAMT01 and SINSTAMT06 are reduced by
	* the instalment amount of the coverages being made PUP.
	*    IF CHDRMJA-BILLFREQ         = '00' OR '  '
	*       GO TO 3420-CHECK-IF-ALL-PUP.
	*    IF CHDR-PAIDUP
	*       MOVE ZERO                TO CHDRMJA-SINSTAMT01
	*                                   CHDRMJA-SINSTAMT02
	*                                   CHDRMJA-SINSTAMT03
	*                                   CHDRMJA-SINSTAMT04
	*                                   CHDRMJA-SINSTAMT05
	*                                   CHDRMJA-SINSTAMT06
	*       GO TO 3420-CHECK-IF-ALL-PUP.
	*    IF CHDRMJA-SINSTAMT01   NOT = WSAA-TOT-COVRPU-INSTPREM
	*       NEXT SENTENCE
	*    ELSE
	*       MOVE ZERO                TO CHDRMJA-SINSTAMT02.
	*    COMPUTE CHDRMJA-SINSTAMT01  = CHDRMJA-SINSTAMT01
	*                                - WSAA-TOT-COVRPU-INSTPREM.
	*    COMPUTE CHDRMJA-SINSTAMT06  = CHDRMJA-SINSTAMT01 +
	*                                  CHDRMJA-SINSTAMT02 +
	*                                  CHDRMJA-SINSTAMT03 +
	*                                  CHDRMJA-SINSTAMT04 +
	*                                  CHDRMJA-SINSTAMT05.
	*       MOVE 0                   TO CHDRMJA-INSTTOT01
	*                                   CHDRMJA-INSTTOT02
	*                                   CHDRMJA-INSTTOT03
	*                                   CHDRMJA-INSTTOT04
	*                                   CHDRMJA-INSTTOT05
	*                                   CHDRMJA-INSTTOT06.
	*3420-CHECK-IF-ALL-PUP.
	*    IF CHDR-PAIDUP
	*       NEXT SENTENCE
	*    ELSE
	*       GO 3430-REWRITE-CHDR.
	*    SUBTRACT 1                  FROM CHDRMJA-TRANNO.
	*    MOVE '2'                    TO CHDRMJA-VALIDFLAG.
	*    MOVE REWRT                  TO CHDRMJA-FUNCTION.
	*    MOVE CHDRMJAREC             TO CHDRMJA-FORMAT.
	*    CALL 'CHDRMJAIO'            USING CHDRMJA-PARAMS.
	*    IF CHDRMJA-STATUZ           NOT = O-K
	*       MOVE CHDRMJA-PARAMS      TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*    ADD 1                       TO CHDRMJA-TRANNO.
	*    MOVE '1'                    TO CHDRMJA-VALIDFLAG.
	*****IF VALID-PSTATCODE
	*******IF T5679-SET-CN-PREM-STAT   NOT = SPACES
	**********MOVE T5679-SET-CN-PREM-STAT TO CHDRMJA-PSTATCODE.
	**********MOVE T5679-SET-CN-PREM-STAT TO CHDRMJA-PSTATCODE
	*******IF T5679-SET-CN-RISK-STAT   NOT = SPACES
	**********MOVE T5679-SET-CN-RISK-STAT TO CHDRMJA-STATCODE.
	*    IF WSAA-PLAN-SUFFIX         NOT = ZERO
	*    AND WSAA-PLAN-SUFFIX        NOT > CHDRMJA-POLSUM
	*       COMPUTE CHDRMJA-POLSUM   = WSAA-PLAN-SUFFIX - 1.
	*    IF T5679-SET-CN-PREM-STAT   NOT = SPACES
	*       MOVE T5679-SET-CN-PREM-STAT TO CHDRMJA-PSTATCODE.
	*    IF T5679-SET-CN-RISK-STAT   NOT = SPACES
	*       MOVE T5679-SET-CN-RISK-STAT TO CHDRMJA-STATCODE.
	*    MOVE WRITR                  TO CHDRMJA-FUNCTION.
	*    MOVE CHDRMJAREC             TO CHDRMJA-FORMAT.
	*    CALL 'CHDRMJAIO'            USING CHDRMJA-PARAMS.
	*    IF CHDRMJA-STATUZ           NOT = O-K
	*       MOVE CHDRMJA-PARAMS      TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*3430-REWRITE-CHDR.
	*    IF WSAA-PLAN-SUFFIX         NOT = ZERO
	*    AND WSAA-PLAN-SUFFIX        NOT > CHDRMJA-POLSUM
	*       COMPUTE CHDRMJA-POLSUM   = WSAA-PLAN-SUFFIX - 1.
	*****MOVE T5679-SET-CN-PREM-STAT TO CHDRMJA-PSTATCODE.
	*****IF VALID-PSTATCODE
	*******IF T5679-SET-CN-PREM-STAT   NOT = SPACES
	**********MOVE T5679-SET-CN-PREM-STAT TO CHDRMJA-PSTATCODE
	*******IF T5679-SET-CN-RISK-STAT   NOT = SPACES
	**********MOVE T5679-SET-CN-RISK-STAT TO CHDRMJA-STATCODE.
	*  We must now move updat to the chdrmja function as updat
	*  reads and holds the record prior to updating the record
	*    MOVE REWRT                  TO CHDRMJA-FUNCTION.
	*    MOVE UPDAT                  TO CHDRMJA-FUNCTION.
	*    MOVE CHDRMJAREC             TO CHDRMJA-FORMAT.
	*    CALL 'CHDRMJAIO'            USING CHDRMJA-PARAMS.
	*    IF CHDRMJA-STATUZ           NOT = O-K
	*       MOVE CHDRMJA-PARAMS      TO SYSR-PARAMS
	*       PERFORM 9000-FATAL-ERROR.
	*3490-EXIT.
	*    EXIT.
	* THE SECTION TO BE REWRITTEN ENDS HERE - <018> *******
	* REWRITE STARTS HERE - AMENDMENT <018> ***************
	* </pre>
	*/
protected void updateChdr3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start13410();
					updatePayr3410();
				case writeNewContractHeader3420:
					writeNewContractHeader3420();
				case writeRecord3430:
					writeRecord3430();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start13410()
	{
		/*  Since a READH of the CHDR is done at this point the TRANNO     */
		/*  at this point will be correct and will not be advanced by      */
		/*  1.                                                             */
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(atmodrec.company);
		chdrmjaIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrmjaIO.setFunction(varcom.readh);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
		/*  Rewrite the original contract header for historical purposes.  */
		chdrmjaIO.setValidflag("2");
		/* Write the CURRTO and CURRFROM dates for valid flag '2'          */
		/* records on the CHDR File                                        */
		/*  MOVE WSAA-BUSINESS-DATE     TO CHDRMJA-CURRTO.               */
		/*  IF WSAA-OCCDATE             > WSAA-CURRFROM                  */
		/*     MOVE WSAA-OCCDATE        TO CHDRMJA-CURRFROM              */
		/*  ELSE                                                         */
		/*     MOVE WSAA-CURRFROM       TO CHDRMJA-CURRFROM.             */
		chdrmjaIO.setCurrto(chdrmjaIO.getBtdate());
		chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
	}

	/**
	* <pre>
	* Update current PAYR record with valid flag = 2 and write a
	* new PAYR record with valid flag = '1' and effective date is
	* CURRFROM date.
	* </pre>
	*/
protected void updatePayr3410()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(atmodrec.company);
		payrIO.setChdrnum(wsaaPrimaryChdrnum);
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError9000();
		}
		payrIO.setValidflag("2");
		/* MOVE CHDRMJA-CURRFROM       TO PAYR-EFFDATE.         <D9703> */
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError9000();
		}
		if (isEQ(chdrmjaIO.getBillfreq(),"00")
		|| isEQ(chdrmjaIO.getBillfreq(), "  ")) {
			goTo(GotoLabel.writeNewContractHeader3420);
		}
		if (chdrPaidup.isTrue()) {
			chdrmjaIO.setSinstamt01(ZERO);
			chdrmjaIO.setSinstamt02(ZERO);
			chdrmjaIO.setSinstamt03(ZERO);
			chdrmjaIO.setSinstamt04(ZERO);
			chdrmjaIO.setSinstamt05(ZERO);
			chdrmjaIO.setSinstamt06(ZERO);
			payrIO.setSinstamt01(ZERO);
			payrIO.setSinstamt02(ZERO);
			payrIO.setSinstamt03(ZERO);
			payrIO.setSinstamt04(ZERO);
			payrIO.setSinstamt05(ZERO);
			payrIO.setSinstamt06(ZERO);
			goTo(GotoLabel.writeNewContractHeader3420);
		}
		if (isNE(chdrmjaIO.getSinstamt01(),wsaaTotCovrpuInstprem)) {
			/*NEXT_SENTENCE*/
		}
		else {
			chdrmjaIO.setSinstamt02(ZERO);
		}
		if (isNE(payrIO.getSinstamt01(),wsaaTotCovrpuInstprem)) {
			/*NEXT_SENTENCE*/
		}
		else {
			payrIO.setSinstamt02(ZERO);
		}
		setPrecision(chdrmjaIO.getSinstamt01(), 2);
		chdrmjaIO.setSinstamt01(sub(chdrmjaIO.getSinstamt01(),wsaaTotCovrpuInstprem));
		setPrecision(payrIO.getSinstamt01(), 2);
		payrIO.setSinstamt01(sub(payrIO.getSinstamt01(),wsaaTotCovrpuInstprem));
		setPrecision(chdrmjaIO.getSinstamt06(), 2);
		chdrmjaIO.setSinstamt06(add(add(add(add(chdrmjaIO.getSinstamt01(),chdrmjaIO.getSinstamt02()),chdrmjaIO.getSinstamt03()),chdrmjaIO.getSinstamt04()),chdrmjaIO.getSinstamt05()));
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(add(add(add(add(payrIO.getSinstamt01(),payrIO.getSinstamt02()),payrIO.getSinstamt03()),payrIO.getSinstamt04()),payrIO.getSinstamt05()));
	}

	/**
	* <pre>
	**** MOVE 0                      TO CHDRMJA-INSTTOT01        <018>
	****                                CHDRMJA-INSTTOT02        <018>
	****                                CHDRMJA-INSTTOT03        <018>
	****                                CHDRMJA-INSTTOT04        <018>
	****                                CHDRMJA-INSTTOT05        <018>
	****                                CHDRMJA-INSTTOT06.       <018>
	* </pre>
	*/
protected void writeNewContractHeader3420()
	{
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		chdrmjaIO.setValidflag("1");
		/* Write the CURRTO and CURRFROM dates for valid flag '1'          */
		/* records on the CHDR File                                        */
		/*  MOVE 99999999               TO CHDRMJA-CURRTO.               */
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		/*  MOVE 1                      TO DTC2-FREQ-FACTOR.             */
		/*  MOVE 'DY'                   TO DTC2-FREQUENCY.               */
		/*  MOVE WSAA-BUSINESS-DATE     TO DTC2-INT-DATE-1.              */
		/*  CALL 'DATCON2'              USING DTC2-DATCON2-REC.          */
		/*  IF DTC2-STATUZ               NOT = O-K                       */
		/*     MOVE DTC2-DATCON2-REC     TO SYSR-PARAMS                  */
		/*     MOVE DTC2-STATUZ          TO SYSR-STATUZ                  */
		/*     PERFORM 9000-FATAL-ERROR.                                 */
		/*  MOVE DTC2-INT-DATE-2        TO CHDRMJA-CURRFROM.             */
		chdrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		payrIO.setValidflag("1");
		payrIO.setTranno(chdrmjaIO.getTranno());
		payrIO.setEffdate(chdrmjaIO.getCurrfrom());
		if (chdrPaidup.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.writeRecord3430);
		}
		if (isNE(t5679rec.setCnPremStat,SPACES)) {
			chdrmjaIO.setPstatcode(t5679rec.setCnPremStat);
		}
		if (isNE(t5679rec.setCnRiskStat,SPACES)) {
			chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		}
	}

protected void writeRecord3430()
	{
		chdrmjaIO.setFunction(varcom.writr);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError9000();
		}
		/*  Write a new PAYR record with valid flag = 1.                   */
		payrIO.setPstatcode(chdrmjaIO.getPstatcode());
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError9000();
		}
	}

	/**
	* <pre>
	* REWRITE ENDS HERE - AMENDMENT <018> *****************
	* </pre>
	*/
protected void batchHeader3500()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void processLetter3550()
	{
		try {
			chkUpdPuplRecord3550();
			readTableT66343560();
			writeLetcRecord3570();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void chkUpdPuplRecord3550()
	{
		puplIO.setDataKey(SPACES);
		puplIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		puplIO.setChdrnum(chdrmjaIO.getChdrnum());
		puplIO.setFormat(formatsInner.puplrec);
		puplIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, puplIO);
		if (isNE(puplIO.getStatuz(),varcom.oK)
		&& isNE(puplIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(puplIO.getParams());
			fatalError9000();
		}
		if (isEQ(puplIO.getStatuz(),varcom.mrnf)) {
			puplIO.setParams(SPACES);
			puplIO.setChdrcoy(chdrmjaIO.getChdrcoy());
			puplIO.setChdrnum(chdrmjaIO.getChdrnum());
			puplIO.setJlife("00");
			puplIO.setCowncoy(chdrmjaIO.getCowncoy());
			puplIO.setCownnum(chdrmjaIO.getCownnum());
			puplIO.setFunction(varcom.writr);
		}
		else {
			puplIO.setFunction(varcom.rewrt);
		}
		puplIO.setTransactionDate(wsaaTransactionDate);
		puplIO.setTransactionTime(wsaaTransactionTime);
		puplIO.setNewsuma(wsaaNewsumaTot);
		puplIO.setFundAmount(wsaaFundamntTot);
		puplIO.setPupfee(wsaaPupfeeTot);
		puplIO.setTermid(wsaaTermid);
		puplIO.setFormat(formatsInner.puplrec);
		SmartFileCode.execute(appVars, puplIO);
		if (isNE(puplIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(puplIO.getParams());
			fatalError9000();
		}
	}

	/**
	* <pre>
	*                                                         <P008>
	*  The ITEM-ITEMITEM key is made up of CHDRMJA-CNTTYPE and
	*  the transaction code (T575).
	* </pre>
	*/
protected void readTableT66343560()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		/* MOVE T6634                  TO ITEM-ITEMTABL.        <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from contract type & transaction code.      */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			/*       GO TO                   3599-EXIT.                       */
			readT6634Again3700();
		}
		else {
			/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
		/* IF   T6634-LETTER-TYPE          = SPACE              <PCPPRT>*/
		if (isEQ(tr384rec.letterType,SPACES)) {
			goTo(GotoLabel.exit3599);
		}
	}

	/**
	* <pre>
	*3565-GET-ENDORSE.                                          <P008>
	*    MOVE 'NEXT '                TO ALNO-FUNCTION.          <P008>
	*    MOVE 'EN'                   TO ALNO-PREFIX.            <P008>
	*    MOVE WSKY-BATC-BATCBRN      TO ALNO-GENKEY.            <P008>
	*    MOVE ATMD-COMPANY           TO ALNO-COMPANY.           <P008>
	*    CALL 'ALOCNO' USING ALNO-ALOCNO-REC.                   <P008>
	*    IF ALNO-STATUZ              NOT = O-K                  <P008>
	*       MOVE ALNO-STATUZ         TO SYSR-STATUZ             <P008>
	*       MOVE 'ALOCNO'            TO SYSR-PARAMS             <P008>
	*       PERFORM 9000-FATAL-ERROR.                           <P008>
	*  The LETOKEYS field is the contract header number CHDRNUM.
	* </pre>
	*/
protected void writeLetcRecord3570()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		/*MOVE WSAA-TRANSACTION-DATE  TO LETRQST-LETTER-REQUEST-DATE.<030>*/
		letrqstrec.letterRequestDate.set(wsaaLetterDate);
		/*    MOVE 'EN'                   TO LETRQST-RDOCPFX.        <P008>*/
		/*    MOVE ATMD-COMPANY           TO LETRQST-RDOCCOY.        <P008>*/
		/*    MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.        <P008>*/
		letrqstrec.rdocpfx.set(chdrmjaIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		/* MOVE CHDRMJA-CHDRNUM        TO LETRQST-OTHER-KEYS.           */
		letrqstrec.otherKeys.set(atmodrec.language);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.function.set("ADD");
		/*    CALL 'HLETRQS'              USING LETRQST-PARAMS.      <P008>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError9000();
		}
	}

protected void releaseSoftlock3600()
	{
		start3610();
	}

protected void start3610()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError9000();
		}
	}

protected void readT6634Again3700()
	{
		start3710();
	}

protected void start3710()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
		/* MOVE T6634                  TO ITEM-ITEMTABL.        <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from '***' & transaction code.              */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("***", SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.      <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
	}

protected void readT56794000()
	{
		start4010();
	}

protected void start4010()
	{
		/*    Read the Component Status table*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readT66514100()
	{
		start4110();
	}

protected void start4110()
	{
		/*    Read the Paid Up Processing Rules Table*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t6651);
		wsaaT6651Pumeth.set(puptIO.getPumeth());
		wsaaT6651Cntcurr.set(chdrmjaIO.getCntcurr());
		itdmIO.setItemitem(wsaaT6651Item);
		itdmIO.setItmfrm(chdrmjaIO.getBtdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");



		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.params.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if ((isNE(itdmIO.getItemcoy(),atmodrec.company))
		|| (isNE(itdmIO.getItemtabl(),t6651))
		|| (isNE(itdmIO.getItemitem(),wsaaT6651Item))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemitem(wsaaT6651Item);
			syserrrec.params.set(itdmIO.getParams());
			itdmIO.setStatuz(h114);
			fatalError9000();
		}
		else {
			t6651rec.t6651Rec.set(itdmIO.getGenarea());
		}
	}

	/**
	* <pre>
	*4200-READ-T5645 SECTION.
	*4210-START.
	**** Read the Transaction Accounting Rules Table
	**** MOVE SPACES                 TO ITEM-DATA-AREA.
	**** MOVE 'IT'                   TO ITEM-ITEMPFX.
	**** MOVE ATMD-COMPANY           TO ITEM-ITEMCOY.
	**** MOVE T5645                  TO ITEM-ITEMTABL.
	**** MOVE WSAA-TABLE-PROG        TO ITEM-ITEMITEM.
	**** MOVE ITEMREC                TO ITEM-FORMAT.
	**** MOVE  READR                 TO ITEM-FUNCTION.
	**** CALL 'ITEMIO' USING ITEM-PARAMS.
	**** IF ITEM-STATUZ              NOT = O-K
	****     MOVE ITEM-PARAMS        TO SYSR-PARAMS
	****     PERFORM 9000-FATAL-ERROR.
	**** MOVE ITEM-GENAREA           TO T5645-T5645-REC.
	**** MOVE 'IT'                   TO DESC-DESCPFX.
	**** MOVE CHDRMJA-CHDRCOY        TO DESC-DESCCOY.
	**** MOVE T5645                  TO DESC-DESCTABL.
	**** MOVE ATMD-LANGUAGE          TO DESC-LANGUAGE.
	**** MOVE WSAA-TABLE-PROG        TO DESC-DESCITEM.
	**** MOVE READR                  TO DESC-FUNCTION.
	**** CALL 'DESCIO'               USING DESC-PARAMS.
	**** IF DESC-STATUZ              NOT = O-K
	****     MOVE DESC-PARAMS        TO SYSR-PARAMS
	****     PERFORM 9000-FATAL-ERROR.
	*4299-EXIT.
	**** EXIT.
	*4500-READ-T6647 SECTION.
	*4510-START.
	**** Read the Unit Linked Contract Details Table
	**** MOVE SPACES                 TO ITDM-DATA-AREA.
	**** MOVE ATMD-COMPANY           TO ITDM-ITEMCOY.
	**** MOVE T6647                  TO ITDM-ITEMTABL.
	**** MOVE WSKY-BATC-BATCTRCDE    TO WSAA-T6647-BATCTRCDE.
	**** MOVE CHDRMJA-CNTTYPE        TO WSAA-T6647-CNTTYPE.
	**** MOVE WSAA-T6647-ITEM        TO ITDM-ITEMITEM.
	**** MOVE CHDRMJA-OCCDATE        TO ITDM-ITMFRM.
	**** MOVE BEGN                   TO ITDM-FUNCTION.
	**** CALL 'ITDMIO'               USING ITDM-PARAMS.
	**** IF (ITDM-STATUZ         NOT = O-K) AND
	****    (ITDM-STATUZ         NOT = ENDP)
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS
	****    MOVE ITDM-STATUZ         TO SYSR-STATUZ
	****    PERFORM 9000-FATAL-ERROR.
	**** IF (ITDM-ITEMCOY        NOT = ATMD-COMPANY)    OR
	****    (ITDM-ITEMTABL       NOT = T6647)           OR
	****    (ITDM-ITEMITEM       NOT = WSAA-T6647-ITEM) OR
	****    (ITDM-STATUZ             = ENDP)
	****    MOVE WSAA-T6647-ITEM     TO ITDM-ITEMITEM
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS
	****    MOVE H115                TO ITDM-STATUZ              <028>
	****    PERFORM 9000-FATAL-ERROR
	**** ELSE
	****    MOVE ITDM-GENAREA        TO T6647-T6647-REC.
	*4599-EXIT.
	**** EXIT.
	*4600-READ-T5515 SECTION.
	*4610-START.
	*    Read the Virtual Fund Details Table
	**** MOVE SPACES                 TO ITDM-DATA-AREA.
	**** MOVE ATMD-COMPANY           TO ITDM-ITEMCOY.
	**** MOVE T5515                  TO ITDM-ITEMTABL.
	**** MOVE UTRS-UNIT-VIRTUAL-FUND TO ITDM-ITEMITEM.
	**** MOVE CHDRMJA-OCCDATE        TO ITDM-ITMFRM.
	**** MOVE BEGN                   TO ITDM-FUNCTION.
	**** CALL 'ITDMIO' USING ITDM-PARAMS.
	**** IF ITDM-STATUZ              NOT = O-K
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS
	****    MOVE ITDM-STATUZ         TO SYSR-STATUZ
	****    PERFORM 9000-FATAL-ERROR.
	**** IF (ITDM-ITEMCOY        NOT = ATMD-COMPANY)           OR
	****    (ITDM-ITEMTABL       NOT = T5515)                  OR
	****    (ITDM-ITEMITEM       NOT = UTRS-UNIT-VIRTUAL-FUND) OR
	****    (ITDM-STATUZ             = ENDP)
	****    MOVE UTRS-UNIT-VIRTUAL-FUND TO ITDM-ITEMITEM
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS
	****    MOVE H116                TO ITDM-STATUZ              <028>
	****    PERFORM 9000-FATAL-ERROR
	**** ELSE
	****    MOVE ITDM-GENAREA        TO T5515-T5515-REC.
	*4699-EXIT.
	**** EXIT.
	*4800-GET-FUND-PRICE SECTION.
	*4810-START.
	*    Read the Virtual Price File to obtain the fund price
	**** MOVE ZEROS                  TO WSAA-BIDOFFER-PRICE
	****                                WSAA-TOTAL-REAL-FUNDS.
	**** MOVE SPACES                 TO VPRC-DATA-AREA.
	**** MOVE UTRS-UNIT-VIRTUAL-FUND TO VPRC-UNIT-VIRTUAL-FUND.
	**** MOVE 'A'                    TO VPRC-UNIT-TYPE.         <017>
	**** MOVE UTRS-UNIT-TYPE         TO VPRC-UNIT-TYPE.         <017>
	**** MOVE CHDRMJA-BTDATE         TO VPRC-EFFDATE.
	**** MOVE ZEROS                  TO VPRC-JOBNO.
	**** MOVE VPRCREC                TO VPRC-FORMAT.
	**** MOVE BEGN                   TO VPRC-FUNCTION.
	**** CALL 'VPRCIO'               USING VPRC-PARAMS.
	**** IF VPRC-STATUZ              NOT = O-K
	**** AND VPRC-STATUZ             NOT = ENDP
	****     MOVE VPRC-PARAMS        TO SYSR-PARAMS
	****     PERFORM 9000-FATAL-ERROR.
	**** IF VPRC-UNIT-VIRTUAL-FUND   NOT = UTRS-UNIT-VIRTUAL-FUND
	***  OR VPRC-UNIT-TYPE           NOT = 'A'
	**** OR VPRC-UNIT-TYPE           NOT = UTRS-UNIT-TYPE        <017>
	****     MOVE MRNF               TO SYSR-STATUZ
	****     MOVE VPRC-PARAMS        TO SYSR-PARAMS
	****     PERFORM 9000-FATAL-ERROR.
	**** MOVE UTRS-UNIT-VIRTUAL-FUND TO WSAA-UNIT-VIRTUAL-FUND.
	**** IF T6651-BIDOFFER           = 'B'
	****    MOVE VPRC-UNIT-BID-PRICE TO WSAA-BIDOFFER-PRICE
	**** ELSE
	**** IF T6651-BIDOFFER           = 'O'
	****    MOVE VPRC-UNIT-OFFER-PRICE TO WSAA-BIDOFFER-PRICE.
	*4899-EXIT.
	**** EXIT.
	*4900-SETUP-COMMON-UTRN SECTION.
	*4910-START.
	**** MOVE SPACES                 TO UTRN-DATA-KEY.
	**** MOVE PUPT-CHDRCOY           TO UTRN-CHDRCOY.
	**** MOVE PUPT-CHDRNUM           TO UTRN-CHDRNUM.
	**** MOVE PUPT-LIFE              TO UTRN-LIFE.
	**** MOVE PUPT-COVERAGE          TO UTRN-COVERAGE.
	**** MOVE PUPT-RIDER             TO UTRN-RIDER.
	**** MOVE PUPT-PLAN-SUFFIX       TO UTRN-PLAN-SUFFIX.
	**** MOVE CHDRMJA-TRANNO         TO UTRN-TRANNO.
	* As Vrcm variables only serve to initialise Utrn variables we
	* must initialise vrcm variables first.
	**** MOVE ZEROES                 TO VRCM-TRANID-N.           <004>
	**** MOVE VRCM-TERMID            TO UTRN-TERMID.
	***  MOVE VRCM-USER              TO UTRN-USER.
	**** MOVE WSAA-USER              TO UTRN-USER.               <005>
	**** MOVE WSAA-TRANSACTION-DATE  TO UTRN-TRANSACTION-DATE.
	**** MOVE WSAA-TRANSACTION-TIME  TO UTRN-TRANSACTION-TIME.
	**** MOVE WSKY-BATC-BATCCOY      TO UTRN-BATCCOY.
	**** MOVE WSKY-BATC-BATCBRN      TO UTRN-BATCBRN.
	**** MOVE WSKY-BATC-BATCACTYR    TO UTRN-BATCACTYR.
	**** MOVE WSKY-BATC-BATCACTMN    TO UTRN-BATCACTMN.
	**** MOVE WSKY-BATC-BATCTRCDE    TO UTRN-BATCTRCDE.
	**** MOVE WSKY-BATC-BATCBATCH    TO UTRN-BATCBATCH.
	**** MOVE T6647-ALOIND           TO UTRN-NOW-DEFER-IND.
	**** IF   T6647-EFDCODE          = 'BD'
	****      MOVE WSAA-BUSINESS-DATE TO UTRN-MONIES-DATE
	**** ELSE
	**** IF   T6647-EFDCODE          = 'DD'
	****      MOVE CHDRMJA-BTDATE    TO UTRN-MONIES-DATE
	**** ELSE
	**** IF   T6647-EFDCODE          = 'LO'
	****      MOVE CHDRMJA-BTDATE    TO UTRN-MONIES-DATE
	**** ELSE
	****      MOVE ZEROES            TO UTRN-MONIES-DATE.
	**** IF   T6647-EFDCODE          = 'LO'
	**** AND  WSAA-BUSINESS-DATE     > CHDRMJA-BTDATE
	****      MOVE WSAA-BUSINESS-DATE TO UTRN-MONIES-DATE.
	**** MOVE COVRMJA-CRTABLE        TO UTRN-CRTABLE.
	**** MOVE CHDRMJA-CNTCURR        TO UTRN-CNTCURR.
	**** MOVE T5645-SACSCODE-03      TO UTRN-SACSCODE.
	**** MOVE T5645-SACSTYPE-03      TO UTRN-SACSTYP.
	**** MOVE T5645-GLMAP-03         TO UTRN-GENLCDE.
	**** MOVE CHDRMJA-CNTTYPE        TO UTRN-CONTRACT-TYPE.
	**** MOVE T6647-PROC-SEQ-NO      TO UTRN-PROC-SEQ-NO.
	**** MOVE COVRMJA-CRRCD          TO UTRN-CR-COM-DATE.
	**** MOVE ZEROS                  TO UTRN-INCI-PERD01
	****                                UTRN-INCI-PERD02
	****                                UTRN-INCIPRM01
	****                                UTRN-INCIPRM02
	****                                UTRN-JOBNO-PRICE
	****                                UTRN-FUND-RATE
	****                                UTRN-STRPDATE
	****                                UTRN-NOF-UNITS
	****                                UTRN-NOF-DUNITS
	****                                UTRN-PRICE-DATE-USED
	****                                UTRN-PRICE-USED
	****                                UTRN-UNIT-BARE-PRICE
	****                                UTRN-INCI-NUM
	****                                UTRN-USTMNO
	****                                UTRN-CONTRACT-AMOUNT
	****                                UTRN-FUND-AMOUNT
	****                                UTRN-DISCOUNT-FACTOR
	****                                UTRN-SURRENDER-PERCENT.
	**** MOVE 1                      TO UTRN-SVP.
	*4990-EXIT.
	**** EXIT.
	* </pre>
	*/
protected void callPupMethod5000()
	{
			start5005();
		}

protected void start5005()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if ((isNE(itdmIO.getItemcoy(),atmodrec.company))
		|| (isNE(itdmIO.getItemtabl(),t5687))
		|| (isNE(itdmIO.getItemitem(),covrmjaIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemitem(covrmjaIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			itdmIO.setStatuz(f294);
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5687rec.pumeth,SPACES)) {
			lapseMethod6000();
			return ;
		}
		/* Read T6598 to get the Calculation subroutine name               */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemitem(t5687rec.pumeth);
		itemIO.setItemtabl(t6598);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setStatuz(h021);
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
		/* IF T6598-CALCPROG           = SPACES                    <027>*/
		if (isEQ(t6598rec.procesprog,SPACES)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(t035);
			fatalError9000();
		}
		ovrduerec.ovrdueRec.set(SPACES);
		ovrduerec.company.set(atmodrec.company);
		ovrduerec.language.set(atmodrec.language);
		ovrduerec.trancode.set(wsaaBatckey.batcBatctrcde);
		ovrduerec.acctyear.set(wsaaBatckey.batcBatcactyr);
		ovrduerec.acctmonth.set(wsaaBatckey.batcBatcactmn);
		ovrduerec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ovrduerec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		ovrduerec.user.set(wsaaUser);
		ovrduerec.tranDate.set(wsaaTransactionDate);
		ovrduerec.tranTime.set(wsaaTransactionTime);
		ovrduerec.chdrnum.set(puptIO.getChdrnum());
		ovrduerec.chdrcoy.set(puptIO.getChdrcoy());
		ovrduerec.cntcurr.set(chdrmjaIO.getCntcurr());
		ovrduerec.effdate.set(chdrmjaIO.getCcdate());
		ovrduerec.outstamt.set(chdrmjaIO.getOutstamt());
		ovrduerec.btdate.set(chdrmjaIO.getBtdate());
		ovrduerec.ptdate.set(chdrmjaIO.getPtdate());
		ovrduerec.agntnum.set(chdrmjaIO.getAgntnum());
		ovrduerec.cownnum.set(chdrmjaIO.getCownnum());
		ovrduerec.tranno.set(chdrmjaIO.getTranno());
		ovrduerec.ovrdueDays.set(ZERO);
		ovrduerec.life.set(puptIO.getLife());
		ovrduerec.coverage.set(puptIO.getCoverage());
		ovrduerec.rider.set(puptIO.getRider());
		ovrduerec.planSuffix.set(puptIO.getPlanSuffix());
		ovrduerec.crtable.set(covrmjaIO.getCrtable());
		ovrduerec.pumeth.set(t5687rec.pumeth);
		ovrduerec.billfreq.set(chdrmjaIO.getBillfreq());
		ovrduerec.instprem.set(covrmjaIO.getInstprem());
		/* MOVE COVRMJA-SUMINS         TO OVRD-SUMINS.          <R96REA>*/
		ovrduerec.sumins.set(wsaaOldSumins);
		ovrduerec.newSumins.set(covrmjaIO.getSumins());
		ovrduerec.crrcd.set(covrmjaIO.getCrrcd());
		ovrduerec.premCessDate.set(covrmjaIO.getPremCessDate());
		ovrduerec.pupfee.set(puptIO.getPupfee());
		ovrduerec.cnttype.set(chdrmjaIO.getCnttype());
		ovrduerec.occdate.set(chdrmjaIO.getOccdate());
		/* MOVE T6647-ALOIND           TO OVRD-ALOIND.             <027>*/
		/* MOVE T6647-EFDCODE          TO OVRD-EFDCODE.            <027>*/
		/* MOVE T6647-PROC-SEQ-NO      TO OVRD-PROC-SEQ-NO.        <027>*/
		ovrduerec.procSeqNo.set(ZERO);
		ovrduerec.polsum.set(ZERO);
		ovrduerec.actualVal.set(ZERO);
		ovrduerec.runDate.set(wsaaBusinessDate);
		ovrduerec.description.set(SPACES);
		ovrduerec.function.set("CMDF");
		/* CALL  T6598-CALCPROG        USING OVRD-OVRDUE-REC.      <027>*/
		callProgram(t6598rec.procesprog, ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz,varcom.oK)) {
			syserrrec.statuz.set(ovrduerec.statuz);
			syserrrec.params.set(ovrduerec.ovrdueRec);
			fatalError9000();
		}
	}

protected void lapseMethod6000()
	{
			para6010();
		}

protected void para6010()
	{
		/* Read T5687 for non-forfieture method.                           */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(covrmjaIO.getCrtable(),itdmIO.getItemitem())
		|| isNE(atmodrec.company,itdmIO.getItemcoy())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covrmjaIO.getCrtable());
			/*     MOVE ITDM-STATUZ         TO SYSR-STATUZ                   */
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			fatalError9000();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		/* Read T6597 for non-forfieture subroutine.                       */
		if (isEQ(t5687rec.nonForfeitMethod,SPACES)) {
			return ;
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t6597);
		itdmIO.setItemitem(t5687rec.nonForfeitMethod);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemtabl(),t6597)
		|| isNE(t5687rec.nonForfeitMethod,itdmIO.getItemitem())
		|| isNE(atmodrec.company,itdmIO.getItemcoy())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(t5687rec.nonForfeitMethod);
			/*     MOVE ITDM-STATUZ         TO SYSR-STATUZ                   */
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g381);
			fatalError9000();
		}
		t6597rec.t6597Rec.set(itdmIO.getGenarea());
		if (isEQ(t6597rec.premsubr04,SPACES)) {
			return ;
		}
		ovrduerec.ovrdueRec.set(SPACES);
		ovrduerec.company.set(atmodrec.company);
		ovrduerec.language.set(atmodrec.language);
		ovrduerec.trancode.set(wsaaBatckey.batcBatctrcde);
		ovrduerec.acctyear.set(wsaaBatckey.batcBatcactyr);
		ovrduerec.acctmonth.set(wsaaBatckey.batcBatcactmn);
		ovrduerec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ovrduerec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		ovrduerec.user.set(wsaaUser);
		ovrduerec.tranDate.set(wsaaTransactionDate);
		ovrduerec.tranTime.set(wsaaTransactionTime);
		ovrduerec.chdrnum.set(chdrmjaIO.getChdrnum());
		ovrduerec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		ovrduerec.cntcurr.set(chdrmjaIO.getCntcurr());
		ovrduerec.effdate.set(chdrmjaIO.getCcdate());
		ovrduerec.outstamt.set(chdrmjaIO.getOutstamt());
		ovrduerec.btdate.set(chdrmjaIO.getBtdate());
		ovrduerec.ptdate.set(chdrmjaIO.getPtdate());
		ovrduerec.agntnum.set(chdrmjaIO.getAgntnum());
		ovrduerec.cownnum.set(chdrmjaIO.getCownnum());
		ovrduerec.tranno.set(chdrmjaIO.getTranno());
		ovrduerec.ovrdueDays.set(ZERO);
		ovrduerec.life.set(covrmjaIO.getLife());
		ovrduerec.coverage.set(covrmjaIO.getCoverage());
		ovrduerec.rider.set(covrmjaIO.getRider());
		ovrduerec.planSuffix.set(covrmjaIO.getPlanSuffix());
		ovrduerec.crtable.set(covrmjaIO.getCrtable());
		ovrduerec.pumeth.set(SPACES);
		ovrduerec.billfreq.set(chdrmjaIO.getBillfreq());
		ovrduerec.instprem.set(covrmjaIO.getInstprem());
		/* MOVE COVRMJA-SUMINS         TO OVRD-SUMINS.          <R96REA>*/
		ovrduerec.sumins.set(wsaaOldSumins);
		ovrduerec.crrcd.set(covrmjaIO.getCrrcd());
		ovrduerec.premCessDate.set(covrmjaIO.getPremCessDate());
		ovrduerec.pupfee.set(ZERO);
		ovrduerec.cnttype.set(chdrmjaIO.getCnttype());
		ovrduerec.occdate.set(chdrmjaIO.getOccdate());
		ovrduerec.aloind.set(SPACES);
		ovrduerec.efdcode.set(SPACES);
		ovrduerec.procSeqNo.set(ZERO);
		ovrduerec.actualVal.set(ZERO);
		ovrduerec.polsum.set(ZERO);
		callProgram(t6597rec.premsubr04, ovrduerec.ovrdueRec);
		if (isEQ(ovrduerec.statuz,varcom.oK)) {
			if (isNE(t6597rec.cpstat04,SPACES)) {
				if (isNE(t6597rec.crstat04,SPACES)) {
					covrmjaIO.setPstatcode(t6597rec.cpstat04);
					covrmjaIO.setStatcode(t6597rec.crstat04);
				}
			}
		}
		else {
			syserrrec.statuz.set(ovrduerec.statuz);
			syserrrec.params.set(ovrduerec.ovrdueRec);
			fatalError9000();
		}
	}

protected void incrCheck7000()
	{
			para7010();
		}

protected void para7010()
	{
		/* Instead of updating the INCR record in situ, write a new     */
		/* INCR record for this transaction to allow reversals to       */
		/* reinstate the previous record.                               */
		incrIO.setDataArea(SPACES);
		incrIO.setChdrcoy(puptIO.getChdrcoy());
		incrIO.setChdrnum(puptIO.getChdrnum());
		incrIO.setLife(puptIO.getLife());
		incrIO.setCoverage(puptIO.getCoverage());
		incrIO.setRider(puptIO.getRider());
		incrIO.setPlanSuffix(puptIO.getPlanSuffix());
		/* MOVE READH                  TO INCR-FUNCTION.        <A06275>*/
		incrIO.setFunction(varcom.readr);
		incrIO.setFormat(formatsInner.incrrec);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(),varcom.oK)
		&& isNE(incrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(incrIO.getParams());
			fatalError9000();
		}
		if (isEQ(incrIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		incrIO.setValidflag("2");
		if (isNE(t5679rec.setCovPremStat,SPACES)) {
			incrIO.setPstatcode(t5679rec.setCovPremStat);
		}
		if (isNE(t5679rec.setCovRiskStat,SPACES)) {
			incrIO.setStatcode(t5679rec.setCovRiskStat);
		}
		/* MOVE REWRT                  TO INCR-FUNCTION.        <A06275>*/
		incrIO.setTransactionDate(wsaaTransactionDate);
		incrIO.setTransactionTime(wsaaTransactionTime);
		incrIO.setUser(wsaaUser);
		incrIO.setTermid(wsaaTermid);
		incrIO.setTranno(chdrmjaIO.getTranno());
		incrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(incrIO.getParams());
			fatalError9000();
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
				}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypAplsupto.set(payrIO.getAplspto());
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError9000();
				}
				}

protected void readT75088100()
	{
		start8110();
			}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
			}
		}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void fatalError9000()
	{
		start9010();
		errorProg9050();
	}

protected void start9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg9050()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrmjaIO.getChdrnum());
		lifsttrrec.tranno.set(covrmjaIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError9000();
		}
	}

protected void a100DelPolUnderwritting()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(wsaaL1Clntnum);
		crtundwrec.coy.set(chdrmjaIO.getChdrcoy());
		crtundwrec.currcode.set(chdrmjaIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrmjaIO.getChdrnum());
		crtundwrec.cnttyp.set(chdrmjaIO.getCnttype());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError9000();
		}
	}

protected void a200DelCovUnderwritting()
	{
		a200Ctrl();
	}

protected void a200Ctrl()
	{
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(wsaaL1Clntnum);
		crtundwrec.coy.set(chdrmjaIO.getChdrcoy());
		crtundwrec.currcode.set(chdrmjaIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrmjaIO.getChdrnum());
		crtundwrec.cnttyp.set(chdrmjaIO.getCnttype());
		crtundwrec.crtable.set(covrmjaIO.getCrtable());
		crtundwrec.life.set(covrmjaIO.getLife());
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError9000();
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData puptrec = new FixedLengthStringData(10).init("PUPTREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData puplrec = new FixedLengthStringData(10).init("PUPLREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData incrrec = new FixedLengthStringData(10).init("INCRREC");
	private FixedLengthStringData lifeenqrec = new FixedLengthStringData(10).init("LIFEENQREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
