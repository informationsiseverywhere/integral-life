package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PtshpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:10
 * Class transformed from PTSHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PtshpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 89;
	public FixedLengthStringData ptshrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ptshpfRecord = ptshrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ptshrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ptshrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(ptshrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(ptshrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(ptshrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(ptshrec);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(ptshrec);
	public PackedDecimalData totalamt = DD.totalamt.copy().isAPartOf(ptshrec);
	public PackedDecimalData prcnt = DD.prcnt.copy().isAPartOf(ptshrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(ptshrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(ptshrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ptshrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ptshrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ptshrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PtshpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PtshpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PtshpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PtshpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PtshpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PtshpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PtshpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PTSHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"LIFE, " +
							"JLIFE, " +
							"EFFDATE, " +
							"CURRCD, " +
							"TOTALAMT, " +
							"PRCNT, " +
							"PLNSFX, " +
							"CNTTYPE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     life,
                                     jlife,
                                     effdate,
                                     currcd,
                                     totalamt,
                                     prcnt,
                                     planSuffix,
                                     cnttype,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		life.clear();
  		jlife.clear();
  		effdate.clear();
  		currcd.clear();
  		totalamt.clear();
  		prcnt.clear();
  		planSuffix.clear();
  		cnttype.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPtshrec() {
  		return ptshrec;
	}

	public FixedLengthStringData getPtshpfRecord() {
  		return ptshpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPtshrec(what);
	}

	public void setPtshrec(Object what) {
  		this.ptshrec.set(what);
	}

	public void setPtshpfRecord(Object what) {
  		this.ptshpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ptshrec.getLength());
		result.set(ptshrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}