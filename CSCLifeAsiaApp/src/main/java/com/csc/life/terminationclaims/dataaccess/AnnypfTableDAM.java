package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AnnypfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:54
 * Class transformed from ANNYPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AnnypfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 103;
	public FixedLengthStringData annyrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData annypfRecord = annyrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(annyrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(annyrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(annyrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(annyrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(annyrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(annyrec);
	public PackedDecimalData guarperd = DD.guarperd.copy().isAPartOf(annyrec);
	public FixedLengthStringData freqann = DD.freqann.copy().isAPartOf(annyrec);
	public FixedLengthStringData arrears = DD.arrears.copy().isAPartOf(annyrec);
	public FixedLengthStringData advance = DD.advance.copy().isAPartOf(annyrec);
	public PackedDecimalData dthpercn = DD.dthpercn.copy().isAPartOf(annyrec);
	public PackedDecimalData dthperco = DD.dthperco.copy().isAPartOf(annyrec);
	public PackedDecimalData intanny = DD.intanny.copy().isAPartOf(annyrec);
	public FixedLengthStringData withprop = DD.withprop.copy().isAPartOf(annyrec);
	public FixedLengthStringData withoprop = DD.withoprop.copy().isAPartOf(annyrec);
	public FixedLengthStringData ppind = DD.ppind.copy().isAPartOf(annyrec);
	public PackedDecimalData capcont = DD.capcont.copy().isAPartOf(annyrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(annyrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(annyrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(annyrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(annyrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(annyrec);
	public FixedLengthStringData nomlife = DD.nomlife.copy().isAPartOf(annyrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AnnypfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AnnypfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AnnypfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AnnypfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AnnypfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AnnypfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AnnypfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ANNYPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"GUARPERD, " +
							"FREQANN, " +
							"ARREARS, " +
							"ADVANCE, " +
							"DTHPERCN, " +
							"DTHPERCO, " +
							"INTANNY, " +
							"WITHPROP, " +
							"WITHOPROP, " +
							"PPIND, " +
							"CAPCONT, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"NOMLIFE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     guarperd,
                                     freqann,
                                     arrears,
                                     advance,
                                     dthpercn,
                                     dthperco,
                                     intanny,
                                     withprop,
                                     withoprop,
                                     ppind,
                                     capcont,
                                     validflag,
                                     tranno,
                                     userProfile,
                                     jobName,
                                     datime,
                                     nomlife,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		guarperd.clear();
  		freqann.clear();
  		arrears.clear();
  		advance.clear();
  		dthpercn.clear();
  		dthperco.clear();
  		intanny.clear();
  		withprop.clear();
  		withoprop.clear();
  		ppind.clear();
  		capcont.clear();
  		validflag.clear();
  		tranno.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		nomlife.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAnnyrec() {
  		return annyrec;
	}

	public FixedLengthStringData getAnnypfRecord() {
  		return annypfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAnnyrec(what);
	}

	public void setAnnyrec(Object what) {
  		this.annyrec.set(what);
	}

	public void setAnnypfRecord(Object what) {
  		this.annypfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(annyrec.getLength());
		result.set(annyrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}