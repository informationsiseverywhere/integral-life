/*
 * File: Pupm002.java
 * Date: 30 August 2009 2:02:44
 * Author: Quipoz Limited
 * 
 * Class transformed from PUPM002.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  This subroutine calculates a paid-up Sum Assured equal to the
*  ratio of  the  number of months' premiums paid and the number
*  of months'  premiums  payable  applied  to  the  present  Sum
*  Assured.
*
*
****************************************************************
* </pre>
*/
public class Pupm002 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "PUPM002";
		/* WSAA-MONTHS-FIELDS */
	private PackedDecimalData wsaaMonthsPaid = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaMonthsDue = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaPupProportion = new PackedDecimalData(13, 8);
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit199
	}

	public Pupm002() {
		super();
	}

public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		ovrduerec.statuz.set(varcom.oK);
		wsaaMonthsPaid.set(ZERO);
		wsaaMonthsDue.set(ZERO);
		calcSumAssured100();
		/*EXIT*/
		exitProgram();
	}

protected void calcSumAssured100()
	{
		try {
			para100();
		}
		catch (GOTOException e){
		}
	}

protected void para100()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.function.set("CMDF");
		datcon3rec.frequency.set(ovrduerec.billfreq);
		datcon3rec.intDate1.set(ovrduerec.crrcd);
		datcon3rec.intDate2.set(ovrduerec.btdate);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			databaseError500();
			goTo(GotoLabel.exit199);
		}
		wsaaMonthsPaid.set(datcon3rec.freqFactor);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.function.set("CMDF");
		datcon3rec.frequency.set(ovrduerec.billfreq);
		datcon3rec.intDate1.set(ovrduerec.crrcd);
		datcon3rec.intDate2.set(ovrduerec.premCessDate);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			databaseError500();
			goTo(GotoLabel.exit199);
		}
		compute(wsaaMonthsDue, 6).setRounded(mult(datcon3rec.freqFactor,1));
		compute(wsaaPupProportion, 8).set(div(wsaaMonthsPaid,wsaaMonthsDue));
		compute(ovrduerec.newSumins, 9).setRounded(mult(wsaaPupProportion,ovrduerec.sumins));
	}

protected void databaseError500()
	{
		/*START*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		ovrduerec.statuz.set(syserrrec.statuz);
		/*EXIT*/
	}
}
