package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:40
 * Description:
 * Copybook name: MATDTRGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Matdtrgkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData matdtrgFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData matdtrgKey = new FixedLengthStringData(64).isAPartOf(matdtrgFileKey, 0, REDEFINE);
  	public FixedLengthStringData matdtrgChdrcoy = new FixedLengthStringData(1).isAPartOf(matdtrgKey, 0);
  	public FixedLengthStringData matdtrgChdrnum = new FixedLengthStringData(8).isAPartOf(matdtrgKey, 1);
  	public PackedDecimalData matdtrgTranno = new PackedDecimalData(5, 0).isAPartOf(matdtrgKey, 9);
  	public PackedDecimalData matdtrgPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(matdtrgKey, 12);
  	public FixedLengthStringData matdtrgCoverage = new FixedLengthStringData(2).isAPartOf(matdtrgKey, 15);
  	public FixedLengthStringData matdtrgRider = new FixedLengthStringData(2).isAPartOf(matdtrgKey, 17);
  	public FixedLengthStringData matdtrgVirtualFund = new FixedLengthStringData(4).isAPartOf(matdtrgKey, 19);
  	public FixedLengthStringData matdtrgFieldType = new FixedLengthStringData(1).isAPartOf(matdtrgKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(matdtrgKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(matdtrgFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		matdtrgFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}