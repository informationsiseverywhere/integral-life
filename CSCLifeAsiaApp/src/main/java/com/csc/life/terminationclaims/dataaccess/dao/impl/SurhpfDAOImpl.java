package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.SurhpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Surhpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class SurhpfDAOImpl extends BaseDAOImpl<Surhpf> implements SurhpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(SurhpfDAOImpl.class);
	@Override
	public void insertSurhpfList(List<Surhpf> pfList) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO SURHPF(CHDRCOY,CHDRNUM,PLNSFX,LIFE,JLIFE,TRANNO,TERMID,TRDT,USER_T,EFFDATE,CURRCD,POLICYLOAN,TDBTAMT,TAXAMT,OTHERADJST,REASONCD,RESNDESC,CNTTYPE,TRTM,ZRCSHAMT,USRPRF,JOBNM,DATIME,SUSPENSEAMT, UNEXPIREDPRM) ");
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for (Surhpf pf : pfList) {
				int i = 1;
				ps.setString(i++, pf.getChdrcoy());
				ps.setString(i++, pf.getChdrnum());
				ps.setInt(i++, pf.getPlanSuffix());
				ps.setString(i++, pf.getLife());
				ps.setString(i++, pf.getJlife());
				ps.setInt(i++, pf.getTranno());
				ps.setString(i++, pf.getTermid());//
				ps.setInt(i++, pf.getTransactionDate());//
				ps.setInt(i++, pf.getUser());//
				ps.setInt(i++, pf.getEffdate());
				ps.setString(i++, pf.getCurrcd());
				ps.setBigDecimal(i++, pf.getPolicyloan());
				ps.setBigDecimal(i++, pf.getTdbtamt());
				ps.setBigDecimal(i++, pf.getTaxamt());
				ps.setBigDecimal(i++, pf.getOtheradjst());
				ps.setString(i++, pf.getReasoncd());
				ps.setString(i++, pf.getResndesc());
				ps.setString(i++, pf.getCnttype());
				ps.setInt(i++, pf.getTransactionTime());
				ps.setBigDecimal(i++, pf.getZrcshamt());
				ps.setString(i++, this.getUsrprf());
				ps.setString(i++, this.getJobnm());
				ps.setTimestamp(i++, this.getDatime());
				ps.setBigDecimal(i++, pf.getSuspenseamt());
				ps.setBigDecimal(i++, pf.getUnexpiredPremium());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertSurhpfList()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

    @Override
    public List<Surhpf> getSurhpfRecord(String chdrcoy, String chdrnum) {
        
        List<Surhpf> surhpfList = new ArrayList<Surhpf>();
        StringBuilder sb = new StringBuilder("");
        sb.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PLNSFX, LIFE, JLIFE, TRANNO, TERMID, TRDT, USER_T, EFFDATE, CURRCD, POLICYLOAN, "
                + "TDBTAMT, TAXAMT, OTHERADJST, REASONCD, RESNDESC, CNTTYPE, TRTM, ZRCSHAMT, USRPRF, JOBNM, DATIME, SUSPENSEAMT, "
                + "UNEXPIREDPRM ");
        sb.append("FROM VM1DTA.SURHPF WHERE CHDRCOY=? AND CHDRNUM=?  ");
        sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC");
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        try {
            ps.setString(1, chdrcoy);
            ps.setString(2, chdrnum);
            rs = executeQuery(ps);
            Surhpf surhpf = null;
            while (rs.next()) {
                surhpf = new Surhpf();
                surhpf.setChdrcoy(rs.getString("CHDRCOY"));
                surhpf.setChdrnum(rs.getString("CHDRNUM"));
                surhpf.setPlanSuffix(rs.getInt("PLNSFX"));
                surhpf.setLife(rs.getString("LIFE"));
                surhpf.setJlife(rs.getString("JLIFE"));
                surhpf.setTranno(rs.getInt("TRANNO"));
                surhpf.setTermid(rs.getString("TERMID"));
                surhpf.setTransactionDate(rs.getInt("TRDT"));
                surhpf.setUser(rs.getInt("USER_T"));
                surhpf.setEffdate(rs.getInt("EFFDATE"));
                surhpf.setCurrcd(rs.getString("CURRCD"));
                surhpf.setPolicyloan(rs.getBigDecimal("POLICYLOAN"));
                surhpf.setTdbtamt(rs.getBigDecimal("TDBTAMT"));
                surhpf.setTaxamt(rs.getBigDecimal("TAXAMT"));
                surhpf.setOtheradjst(rs.getBigDecimal("OTHERADJST"));
                surhpf.setReasoncd(rs.getString("REASONCD"));
                surhpf.setResndesc(rs.getString("RESNDESC"));
                surhpf.setCnttype(rs.getString("CNTTYPE"));
                surhpf.setTransactionTime(rs.getInt("TRTM"));  
                surhpf.setZrcshamt(rs.getBigDecimal("ZRCSHAMT"));
                surhpf.setUserProfile(rs.getString("USRPRF"));
                surhpf.setJobName(rs.getString("JOBNM"));
                surhpf.setDatime(rs.getString("DATIME"));
                surhpf.setSuspenseamt(rs.getBigDecimal("SUSPENSEAMT"));    
                surhpf.setDatime(rs.getString("DATIME"));
                surhpf.setUnexpiredPremium(rs.getBigDecimal("UNEXPIREDPRM"));
                surhpfList.add(surhpf);
            }
        } catch (SQLException e) {
            LOGGER.error("getSurhpfRecord()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return surhpfList;
    }
    
}

