package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6762screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6762ScreenVars sv = (S6762ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6762screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6762ScreenVars screenVars = (S6762ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.regpayExcpCsfl.setClassString("");
		screenVars.regpayExcpIchs.setClassString("");
		screenVars.regpayExcpIcos.setClassString("");
		screenVars.regpayExcpIpst.setClassString("");
		screenVars.regpayExcpInrv.setClassString("");
		screenVars.regpayExcpExpd.setClassString("");
		screenVars.regpayExcpInsf.setClassString("");
		screenVars.regpayExcpLtma.setClassString("");
		screenVars.regpayExcpNopr.setClassString("");
		screenVars.regpayExcpTinf.setClassString("");
	}

/**
 * Clear all the variables in S6762screen
 */
	public static void clear(VarModel pv) {
		S6762ScreenVars screenVars = (S6762ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.regpayExcpCsfl.clear();
		screenVars.regpayExcpIchs.clear();
		screenVars.regpayExcpIcos.clear();
		screenVars.regpayExcpIpst.clear();
		screenVars.regpayExcpInrv.clear();
		screenVars.regpayExcpExpd.clear();
		screenVars.regpayExcpInsf.clear();
		screenVars.regpayExcpLtma.clear();
		screenVars.regpayExcpNopr.clear();
		screenVars.regpayExcpTinf.clear();
	}
}
