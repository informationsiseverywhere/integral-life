/*
 * File: Ph546.java
 * Date: 30 August 2009 1:07:44
 * Author: Quipoz Limited
 * 
 * Class transformed from PH546.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.procedures.Th546pt;
import com.csc.life.terminationclaims.screens.Sh546ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Th546rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Ph546 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH546");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Th546rec th546rec = new Th546rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sh546ScreenVars sv = ScreenProgram.getScreenVars( Sh546ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		other3080, 
		exit3090
	}

	public Ph546() {
		super();
		screenVars = sv;
		new ScreenModel("Sh546", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		th546rec.th546Rec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-01                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-02                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-03                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-04                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-05                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-06                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-07                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-08                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-09                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-10                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-11                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-12                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-13                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-14                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-15                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-16                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-17                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-18                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-19                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-20                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-21                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-22                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-23                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-24                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-25                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-26                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-27                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-28                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-29                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-30                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-31                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-32                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-33                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-34                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-35                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-36                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-37                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-38                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-39                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-40                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-41                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-42                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-43                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-44                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-45                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-46                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-47                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-48                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-49                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-50                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-51                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-52                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-53                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-54                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-55                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-56                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-57                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-58                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-59                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-60                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-61                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-62                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-63                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-64                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-65                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-66                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-67                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-68                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-69                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-70                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-71                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-72                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-73                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-74                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-75                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-76                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-77                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-78                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-79                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-80                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-81                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-82                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-83                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-84                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-85                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-86                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-87                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-88                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-89                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-90                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-91                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-92                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-93                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-94                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-95                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-96                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-97                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-98                .*/
		/*    MOVE ZERO*/
		/*      TO TH546-INSPRM-99                .*/
		/*    MOVE ZERO                                                    */
		/*      TO TH546-INSTPR                   .                        */
		wsaaSub1.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 11); loopVar1 += 1){
			initInstprs1500();
		}
		th546rec.insprem.set(ZERO);
		th546rec.premUnit.set(ZERO);
		th546rec.unit.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 99)); wsaaSub.add(1)){
			th546rec.insprm[wsaaSub.toInt()].set(ZERO);
		}
	}

protected void generalArea1045()
	{
		/*    MOVE TH546-INSPRMS*/
		/*      TO SH546-INSPRMS                 .*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 99)); wsaaSub.add(1)){
			sv.insprm[wsaaSub.toInt()].set(th546rec.insprm[wsaaSub.toInt()]);
		}
		/*    MOVE TH546-INSTPR                                            */
		/*      TO SH546-INSTPR                  .                         */
		wsaaSub1.set(0);
		for (int loopVar2 = 0; !(loopVar2 == 11); loopVar2 += 1){
			moveInstprs1600();
		}
		sv.insprem.set(th546rec.insprem);
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.premUnit.set(th546rec.premUnit);
		sv.unit.set(th546rec.unit);
	}

protected void initInstprs1500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		th546rec.instpr[wsaaSub1.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void moveInstprs1600()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isEQ(th546rec.instpr[wsaaSub1.toInt()], NUMERIC)) {
			sv.instpr[wsaaSub1.toInt()].set(th546rec.instpr[wsaaSub1.toInt()]);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		/*    CALL 'SH546IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SH546-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(th546rec.th546Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.insprms, th546rec.insprms)) {
			/*        MOVE SH546-INSPRMS*/
			/*          TO TH546-INSPRMS*/
			for (wsaaSub.set(1); !(isGT(wsaaSub, 99)); wsaaSub.add(1)){
				th546rec.insprm[wsaaSub.toInt()].set(sv.insprm[wsaaSub.toInt()]);
			}
			wsaaUpdateFlag = "Y";
		}
		/*    IF SH546-INSTPR                  NOT =                       */
		/*       TH546-INSTPR                                              */
		/*        MOVE SH546-INSTPR                                        */
		/*          TO TH546-INSTPR                                        */
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                            */
		wsaaSub1.set(0);
		for (int loopVar3 = 0; !(loopVar3 == 11); loopVar3 += 1){
			updateInstprs3500();
		}
		if (isNE(sv.insprem, th546rec.insprem)) {
			th546rec.insprem.set(sv.insprem);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premUnit, th546rec.premUnit)) {
			th546rec.premUnit.set(sv.premUnit);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.unit, th546rec.unit)) {
			th546rec.unit.set(sv.unit);
			wsaaUpdateFlag = "Y";
		}
	}

protected void updateInstprs3500()
	{
		/*PARA*/
		wsaaSub1.add(1);
		if (isNE(sv.instpr[wsaaSub1.toInt()], th546rec.instpr[wsaaSub1.toInt()])) {
			th546rec.instpr[wsaaSub1.toInt()].set(sv.instpr[wsaaSub1.toInt()]);
			wsaaUpdateFlag = "Y";
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Th546pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
