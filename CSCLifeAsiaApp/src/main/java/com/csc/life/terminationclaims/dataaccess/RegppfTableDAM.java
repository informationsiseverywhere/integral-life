package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RegppfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:16
 * Class transformed from REGPPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RegppfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 349+28+18+17;
	public FixedLengthStringData regprec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData regppfRecord = regprec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(regprec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(regprec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(regprec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(regprec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(regprec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(regprec);
	public PackedDecimalData rgpynum = DD.rgpynum.copy().isAPartOf(regprec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(regprec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(regprec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(regprec);
	public FixedLengthStringData sacstype = DD.sacstype.copy().isAPartOf(regprec);
	public FixedLengthStringData glact = DD.glact.copy().isAPartOf(regprec);
	public FixedLengthStringData debcred = DD.debcred.copy().isAPartOf(regprec);
	public FixedLengthStringData destkey = DD.destkey.copy().isAPartOf(regprec);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(regprec);
	public FixedLengthStringData rgpymop = DD.rgpymop.copy().isAPartOf(regprec);
	public FixedLengthStringData regpayfreq = DD.regpayfreq.copy().isAPartOf(regprec);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(regprec);
	public PackedDecimalData pymt = DD.pymt.copy().isAPartOf(regprec);
	public PackedDecimalData prcnt = DD.prcnt.copy().isAPartOf(regprec);
	public PackedDecimalData totamnt = DD.totamnt.copy().isAPartOf(regprec);
	public FixedLengthStringData rgpystat = DD.rgpystat.copy().isAPartOf(regprec);
	public FixedLengthStringData payreason = DD.payreason.copy().isAPartOf(regprec);
	public FixedLengthStringData claimevd = DD.claimevd.copy().isAPartOf(regprec);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(regprec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(regprec);
	public PackedDecimalData crtdate = DD.crtdate.copy().isAPartOf(regprec);
	public PackedDecimalData aprvdate = DD.aprvdate.copy().isAPartOf(regprec);
	public PackedDecimalData firstPaydate = DD.fpaydate.copy().isAPartOf(regprec);
	public PackedDecimalData nextPaydate = DD.npaydate.copy().isAPartOf(regprec);
	public PackedDecimalData revdte = DD.revdte.copy().isAPartOf(regprec);
	public PackedDecimalData lastPaydate = DD.lpaydate.copy().isAPartOf(regprec);
	public PackedDecimalData finalPaydate = DD.epaydate.copy().isAPartOf(regprec);
	public PackedDecimalData anvdate = DD.anvdate.copy().isAPartOf(regprec);
	public PackedDecimalData cancelDate = DD.canceldate.copy().isAPartOf(regprec);
	public FixedLengthStringData rgpytype = DD.rgpytype.copy().isAPartOf(regprec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(regprec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(regprec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(regprec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(regprec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(regprec);
	public FixedLengthStringData paycoy = DD.paycoy.copy().isAPartOf(regprec);
	public PackedDecimalData certdate = DD.certdate.copy().isAPartOf(regprec);
	public FixedLengthStringData clamparty = DD.clamparty.copy().isAPartOf(regprec);
	public PackedDecimalData recvdDate = DD.zclmrecd.copy().isAPartOf(regprec);
	public PackedDecimalData incurdt = DD.incurdt.copy().isAPartOf(regprec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(regprec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(regprec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(regprec);
	//TODO add adjustment amount
	public PackedDecimalData adjamt = DD.adjamt.copy().isAPartOf(regprec);
	//TODO add adjustment amount reason code
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(regprec);
	//TODO add net claim amount
	public PackedDecimalData netamt = DD.netamt.copy().isAPartOf(regprec);
	//TODO add adjustment amount reason description
	public FixedLengthStringData reason = DD.reason.copy().isAPartOf(regprec);
	/*
	 * CLM004
	 */
	public PackedDecimalData interestdays = DD.itstdays.copy().isAPartOf(regprec);
	public PackedDecimalData interestrate = DD.intrat.copy().isAPartOf(regprec);
	public PackedDecimalData interestamt = DD.itstamt.copy().isAPartOf(regprec);
	
		public FixedLengthStringData ovrpermently = DD.ovrpermently.copy().isAPartOf(regprec);//ILIFE-8299
	public PackedDecimalData ovrsumin = DD.ovrsumin.copy().isAPartOf(regprec);//ILIFE-8299
	
	
	public FixedLengthStringData getOvrpermently() {
		return ovrpermently;
	}

	public void setOvrpermently(FixedLengthStringData ovrpermently) {
		this.ovrpermently = ovrpermently;
	}

	public PackedDecimalData getOvrsumin() {
		return ovrsumin;
	}

	public void setOvrsumin(PackedDecimalData ovrsumin) {
		this.ovrsumin = ovrsumin;
	}
	
	public FixedLengthStringData claimno = DD.claimnumber.copy().isAPartOf(regprec); 
	public FixedLengthStringData claimnotifino = DD.accno.copy().isAPartOf(regprec); 
	
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public RegppfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for RegppfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public RegppfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for RegppfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public RegppfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for RegppfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public RegppfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("REGPPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {

		QUALIFIEDCOLUMNS =
				"CHDRCOY, " +
						"CHDRNUM, " +
						"PLNSFX, " +
						"LIFE, " +
						"COVERAGE, " +
						"RIDER, " +
						"RGPYNUM, " +
						"VALIDFLAG, " +
						"TRANNO, " +
						"SACSCODE, " +
						"SACSTYPE, " +
						"GLACT, " +
						"DEBCRED, " +
						"DESTKEY, " +
						"PAYCLT, " +
						"RGPYMOP, " +
						"REGPAYFREQ, " +
						"CURRCD, " +
						"PYMT, " +
						"PRCNT, " +
						"TOTAMNT, " +
						"RGPYSTAT, " +
						"PAYREASON, " +
						"CLAIMEVD, " +
						"BANKKEY, " +
						"BANKACCKEY, " +
						"CRTDATE, " +
						"APRVDATE, " +
						"FPAYDATE, " +
						"NPAYDATE, " +
						"REVDTE, " +
						"LPAYDATE, " +
						"EPAYDATE, " +
						"ANVDATE, " +
						"CANCELDATE, " +
						"RGPYTYPE, " +
						"CRTABLE, " +
						"TERMID, " +
						"TRDT, " +
						"TRTM, " +
						"USER_T, " +
						"PAYCOY, " +
						"CERTDATE, " +
						"CLAMPARTY, " +
						"ZCLMRECD, " +
						"INCURDT, " +
						"OVRPERMENTLY, " +//ILIFE-8299
			            "OVRSUMIN, " + //ILIFE-8299
						"USRPRF, " +
						"JOBNM, " +
						"ADJAMT, " +
						"REASONCD, " +
						"NETAMT, " +
						"REASON, " +
						"DATIME, " +
						"INTERESTDAYS, " +//CLM004
						"INTERESTRATE, " +
						"INTERESTAMT, " +
						"CLAIMNO, " +
						"CLAIMNOTIFINO, " +
						"UNIQUE_NUMBER";
	}

	public void setColumns() {

		qualifiedColumns = new BaseData[]{
				chdrcoy,
				chdrnum,
				planSuffix,
				life,
				coverage,
				rider,
				rgpynum,
				validflag,
				tranno,
				sacscode,
				sacstype,
				glact,
				debcred,
				destkey,
				payclt,
				rgpymop,
				regpayfreq,
				currcd,
				pymt,
				prcnt,
				totamnt,
				rgpystat,
				payreason,
				claimevd,
				bankkey,
				bankacckey,
				crtdate,
				aprvdate,
				firstPaydate,
				nextPaydate,
				revdte,
				lastPaydate,
				finalPaydate,
				anvdate,
				cancelDate,
				rgpytype,
				crtable,
				termid,
				transactionDate,
				transactionTime,
				user,
				paycoy,
				certdate,
				clamparty,
				recvdDate,
				incurdt,
				ovrpermently,
                ovrsumin,
				userProfile,
				jobName,
				adjamt,
				reasoncd,
				netamt,
				reason,
				datime,
				interestdays,
				interestrate,
				interestamt,
				claimno,
				claimnotifino,
				unique_number
		};
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		planSuffix.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		rgpynum.clear();
  		validflag.clear();
  		tranno.clear();
  		sacscode.clear();
  		sacstype.clear();
  		glact.clear();
  		debcred.clear();
  		destkey.clear();
  		payclt.clear();
  		rgpymop.clear();
  		regpayfreq.clear();
  		currcd.clear();
  		pymt.clear();
  		prcnt.clear();
  		totamnt.clear();
  		rgpystat.clear();
  		payreason.clear();
  		claimevd.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		crtdate.clear();
  		aprvdate.clear();
  		firstPaydate.clear();
  		nextPaydate.clear();
  		revdte.clear();
  		lastPaydate.clear();
  		finalPaydate.clear();
  		anvdate.clear();
  		cancelDate.clear();
  		rgpytype.clear();
  		crtable.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		paycoy.clear();
  		certdate.clear();
  		clamparty.clear();
  		recvdDate.clear();
  		incurdt.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		adjamt.clear();
  		reasoncd.clear();
  		netamt.clear();
		reason.clear();
		interestdays.clear();
		interestrate.clear();
		interestamt.clear();
		ovrpermently.clear();
     	ovrsumin.clear();
		claimno.clear();
		claimnotifino.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getRegprec() {
  		return regprec;
	}

	public FixedLengthStringData getRegppfRecord() {
  		return regppfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setRegprec(what);
	}

	public void setRegprec(Object what) {
  		this.regprec.set(what);
	}

	public void setRegppfRecord(Object what) {
  		this.regppfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(regprec.getLength());
		result.set(regprec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}
