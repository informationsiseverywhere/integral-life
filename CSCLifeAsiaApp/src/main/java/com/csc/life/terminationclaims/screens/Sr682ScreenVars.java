package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR682
 * @version 1.0 generated on 30/08/09 07:23
 * @author Quipoz
 */
public class Sr682ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1814);
	public FixedLengthStringData dataFields = new FixedLengthStringData(774).isAPartOf(dataArea, 0);
	public FixedLengthStringData activeInd = DD.actind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData anntind = DD.anntind.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData anvdate = DD.anvdate.copyToZonedDecimal().isAPartOf(dataFields,2);
	public ZonedDecimalData aprvdate = DD.aprvdate.copyToZonedDecimal().isAPartOf(dataFields,10);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,18);
	public ZonedDecimalData cancelDate = DD.canceldate.copyToZonedDecimal().isAPartOf(dataFields,26);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData claimcur = DD.claimcur.copy().isAPartOf(dataFields,42);
	public FixedLengthStringData claimevd = DD.claimevd.copy().isAPartOf(dataFields,45);
	public FixedLengthStringData clamparty = DD.clamparty.copy().isAPartOf(dataFields,63);
	public ZonedDecimalData clmamt = DD.clmamt.copyToZonedDecimal().isAPartOf(dataFields,71);
	public FixedLengthStringData clmcurdsc = DD.clmcurdsc.copy().isAPartOf(dataFields,86);
	public FixedLengthStringData clmdesc = DD.clmdesc.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData cltype = DD.cltype.copy().isAPartOf(dataFields,126);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,128);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,139);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(dataFields,143);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,151);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,181);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,184);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,194);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,195);
	public FixedLengthStringData destkey = DD.destkey.copy().isAPartOf(dataFields,225);
	public ZonedDecimalData finalPaydate = DD.epaydate.copyToZonedDecimal().isAPartOf(dataFields,235);
	public ZonedDecimalData firstPaydate = DD.fpaydate.copyToZonedDecimal().isAPartOf(dataFields,243);
	public FixedLengthStringData frqdesc = DD.frqdesc.copy().isAPartOf(dataFields,251);
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,261);
	public ZonedDecimalData incurdt = DD.incurdt.copyToZonedDecimal().isAPartOf(dataFields,262);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,270);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,278);
	public ZonedDecimalData lastPaydate = DD.lpaydate.copyToZonedDecimal().isAPartOf(dataFields,325);
	public FixedLengthStringData msgclaim = DD.msgclaim.copy().isAPartOf(dataFields,333);
	public ZonedDecimalData nextPaydate = DD.npaydate.copyToZonedDecimal().isAPartOf(dataFields,373);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,381);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,389);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(dataFields,436);
	public FixedLengthStringData payenme = DD.payenme.copy().isAPartOf(dataFields,444);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,491);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,501);
	public FixedLengthStringData regpayfreq = DD.regpayfreq.copy().isAPartOf(dataFields,509);
	public ZonedDecimalData revdte = DD.revdte.copyToZonedDecimal().isAPartOf(dataFields,511);
	public FixedLengthStringData rgpymop = DD.rgpymop.copy().isAPartOf(dataFields,519);
	public ZonedDecimalData rgpynum = DD.rgpynum.copyToZonedDecimal().isAPartOf(dataFields,520);
	public FixedLengthStringData rgpyshort = DD.rgpyshort.copy().isAPartOf(dataFields,525);
	public FixedLengthStringData rgpystat = DD.rgpystat.copy().isAPartOf(dataFields,535);
	public FixedLengthStringData rgpytypesd = DD.rgpytypesd.copy().isAPartOf(dataFields,537);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,547);
	public FixedLengthStringData statdsc = DD.statdsc.copy().isAPartOf(dataFields,557);
	public ZonedDecimalData totalamt = DD.totalamt.copyToZonedDecimal().isAPartOf(dataFields,567);
	public ZonedDecimalData totamnt = DD.totamnt.copyToZonedDecimal().isAPartOf(dataFields,585);
	public ZonedDecimalData recvdDate = DD.zclmrecd.copyToZonedDecimal().isAPartOf(dataFields,603);
	
	// CML-009
	public ZonedDecimalData pymtAdj = DD.pymt.copyToZonedDecimal().isAPartOf(
			dataFields, 611);
	public ZonedDecimalData adjustamt = DD.adjamt.copyToZonedDecimal()
			.isAPartOf(dataFields, 628);
	public ZonedDecimalData netclaimamt = DD.netamt.copyToZonedDecimal()
			.isAPartOf(dataFields, 640);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(
			dataFields, 657);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(
			dataFields, 661);
	
	// CML-004
	public ZonedDecimalData itstdays = DD.itstdays.copyToZonedDecimal().isAPartOf(dataFields, 712);
	public ZonedDecimalData itstrate = DD.intrat.copyToZonedDecimal().isAPartOf(dataFields, 715);
	public ZonedDecimalData itstamt = DD.itstamt.copyToZonedDecimal().isAPartOf(dataFields, 723); 
	
	public FixedLengthStringData claimno = DD.claimnumber.copy().isAPartOf(dataFields, 740);
	public FixedLengthStringData notifino = DD.notifino.copy().isAPartOf(dataFields,749);
	public FixedLengthStringData investres = DD.investres.copy().isAPartOf(dataFields,763);
	public FixedLengthStringData claimnotes = DD.claimnotes.copy().isAPartOf(dataFields,764);
	public FixedLengthStringData showFlag = DD.action.copy().isAPartOf(dataFields,765);
	public ZonedDecimalData riskcommdte = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,766);	//ILJ-48
	
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(260).isAPartOf(dataArea, 774);
	public FixedLengthStringData actindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData anntindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData anvdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData aprvdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData canceldateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData claimcurErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData claimevdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData clampartyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData clmamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData clmcurdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData clmdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData cltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData destkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData epaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData fpaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData frqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData incurdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData lpaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData msgclaimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData npaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData paycltErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData payenmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData regpayfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData revdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData rgpymopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData rgpynumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData rgpyshortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData rgpystatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData rgpytypesdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData statdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData totalamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData totamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData zclmrecdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	// CML-009
	public FixedLengthStringData adjustamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData netClaimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4)
			.isAPartOf(errorIndicators, 220);
	public FixedLengthStringData pymtAdjErr = new FixedLengthStringData(4)
			.isAPartOf(errorIndicators, 224);
	
	
	// CML-004
	public FixedLengthStringData itstdaysErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);
	public FixedLengthStringData itstrateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData itstamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);
	
	public FixedLengthStringData claimnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);
	public FixedLengthStringData notifinoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData investresErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);
	public FixedLengthStringData claimnotesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 252);
	public FixedLengthStringData riskcommdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 256);	//ILJ-48
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(780).isAPartOf(dataArea, 1034);
	public FixedLengthStringData[] actindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] anntindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] anvdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] aprvdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] canceldateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] claimcurOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] claimevdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] clampartyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] clmamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] clmcurdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] clmdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] cltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] destkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] epaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] fpaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] frqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] incurdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] lpaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] msgclaimOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] npaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] paycltOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] payenmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] regpayfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] revdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] rgpymopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] rgpynumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] rgpyshortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] rgpystatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] rgpytypesdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] statdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] totalamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] totamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[] zclmrecdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	// CML-009
	public FixedLengthStringData[] adjustamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[] netclaimamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1,
			outputIndicators, 660);
	public FixedLengthStringData[] pymtAdjOut = FLSArrayPartOfStructure(12, 1,
			outputIndicators, 672);
	
	// CML-004
	public FixedLengthStringData[] itstdaysOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	public FixedLengthStringData[] itstrateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	public FixedLengthStringData[] itstamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);
	//fwang3
	public FixedLengthStringData[] claimnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);
	public FixedLengthStringData[] notifinoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[] investresOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);
	public FixedLengthStringData[] claimnotesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 756);
	public FixedLengthStringData[] riskcommdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 768);	//ILJ-48
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData anvdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData aprvdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData cancelDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData finalPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData firstPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData incurdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lastPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData revdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData recvdDateDisp = new FixedLengthStringData(10);
	//ILJ-48 Starts
	public FixedLengthStringData riskcommdteDisp = new FixedLengthStringData(10);
	//ILJ-48 End

	public LongData Sr682screenWritten = new LongData(0);
	public LongData Sr682protectWritten = new LongData(0);
	public FixedLengthStringData iljCntDteFlag = new FixedLengthStringData(1);  //ILJ-48

	public boolean hasSubfile() {
		return false;
	}


	public Sr682ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(cltypeOut,new String[] {"02","32","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimevdOut,new String[] {"03","33","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpymopOut,new String[] {"04","34","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(regpayfreqOut,new String[] {"05","35","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paycltOut,new String[] {"01","36","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(destkeyOut,new String[] {"11","37","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimcurOut,new String[] {"75","39","-75",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtdateOut,new String[] {"13","44","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(revdteOut,new String[] {"16","41","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fpaydateOut,new String[] {"15","23","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(npaydateOut,new String[] {"40","76","-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(anvdateOut,new String[] {"18","42","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(epaydateOut,new String[] {"17","43","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(canceldateOut,new String[] {null, null, null, "31",null, null, null, null, null, null, null, null});
		fieldIndMap.put(totalamtOut,new String[] {"-70",null, "70","70",null, null, null, null, null, null, null, null});
		fieldIndMap.put(msgclaimOut,new String[] {null, null, null, "70",null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupflgOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(anntindOut,new String[] {"45","46","-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clampartyOut,new String[] {"50","51","-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actindOut,new String[] {"55","58","-55",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zclmrecdOut,new String[] {"06","38","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(incurdtOut,new String[] {"06","38","-06",null, null, null, null, null, null, null, null, null});
		// CML-009
		fieldIndMap.put(reasoncdOut, new String[] { "07", "49", "-07", "77",null, null, null, null, null, null, null, null });
		fieldIndMap.put(netclaimamtOut, new String[] { "12", "68", "-12", "78",null, null, null, null, null, null, null, null });
		fieldIndMap.put(adjustamtOut, new String[] { "08", "69", "-08", "79",null, null, null, null, null, null, null, null });
		fieldIndMap.put(resndescOut, new String[] { "10", "66", "-10", "80",null,null, null, null, null, null, null, null });
		fieldIndMap.put(pymtAdjOut, new String[] { "44", "67", "-44", null, null, null, null, null, null, null, null, null });
		//clm-004
		fieldIndMap.put(itstdaysOut,new String[] {"96","90","-96","91", null, null, null, null, null, null, null, null});
		fieldIndMap.put(itstrateOut,new String[] {"97","92","-97","93", null, null, null, null, null, null, null, null});
		fieldIndMap.put(itstamtOut,new String[] {"98","94","-98","95", null, null, null, null, null, null, null, null});
		//fwang3
		fieldIndMap.put(investresOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimnotesOut,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(notifinoOut,new String[] {"52", "71", "-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"81", null, null, null, null, null, null, null, null});//ILJ-49
		
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, lifcnum, linsname, cownnum, ownername, occdate, rstate, pstate, ptdate, btdate, currcd, currds, rgpynum, rgpytypesd, rgpystat, statdsc, cltype, clmdesc, claimevd, rgpymop, rgpyshort, regpayfreq, frqdesc, payclt, payenme, destkey, totamnt, claimcur, clmcurdsc, aprvdate, crtdate, revdte, firstPaydate, lastPaydate, nextPaydate, anvdate, finalPaydate, cancelDate, totalamt, msgclaim, fupflg, ddind, crtable, descrip, anntind, clamparty, activeInd, clmamt, recvdDate, incurdt, adjustamt, netclaimamt,
				reasoncd, resndesc, pymtAdj, itstdays,itstrate,itstamt,claimno,notifino,investres,claimnotes, riskcommdte};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, lifcnumOut, linsnameOut, cownnumOut, ownernameOut, occdateOut, rstateOut, pstateOut, ptdateOut, btdateOut, currcdOut, currdsOut, rgpynumOut, rgpytypesdOut, rgpystatOut, statdscOut, cltypeOut, clmdescOut, claimevdOut, rgpymopOut, rgpyshortOut, regpayfreqOut, frqdescOut, paycltOut, payenmeOut, destkeyOut, totamntOut, claimcurOut, clmcurdscOut, aprvdateOut, crtdateOut, revdteOut, fpaydateOut, lpaydateOut, npaydateOut, anvdateOut, epaydateOut, canceldateOut, totalamtOut, msgclaimOut, fupflgOut, ddindOut, crtableOut, descripOut, anntindOut, clampartyOut, actindOut, clmamtOut, zclmrecdOut, incurdtOut, adjustamtOut, netclaimamtOut,
				reasoncdOut, resndescOut, pymtAdjOut, itstdaysOut, itstrateOut, itstamtOut,claimnoOut,notifinoOut,investresOut,claimnotesOut, riskcommdteOut };
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, lifcnumErr, linsnameErr, cownnumErr, ownernameErr, occdateErr, rstateErr, pstateErr, ptdateErr, btdateErr, currcdErr, currdsErr, rgpynumErr, rgpytypesdErr, rgpystatErr, statdscErr, cltypeErr, clmdescErr, claimevdErr, rgpymopErr, rgpyshortErr, regpayfreqErr, frqdescErr, paycltErr, payenmeErr, destkeyErr, totamntErr, claimcurErr, clmcurdscErr, aprvdateErr, crtdateErr, revdteErr, fpaydateErr, lpaydateErr, npaydateErr, anvdateErr, epaydateErr, canceldateErr, totalamtErr, msgclaimErr, fupflgErr, ddindErr, crtableErr, descripErr, anntindErr, clampartyErr, actindErr, clmamtErr, zclmrecdErr, incurdtErr, adjustamtErr, netClaimErr, reasoncdErr,
				resndescErr, pymtAdjErr, itstdaysErr, itstrateErr, itstamtErr,claimnoErr,notifinoErr,investresErr,claimnotesErr , riskcommdteErr };
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, aprvdate, crtdate, revdte, firstPaydate, lastPaydate, nextPaydate, anvdate, finalPaydate, cancelDate, recvdDate, incurdt ,riskcommdte};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, aprvdateErr, crtdateErr, revdteErr, fpaydateErr, lpaydateErr, npaydateErr, anvdateErr, epaydateErr, canceldateErr, zclmrecdErr, incurdtErr , riskcommdteErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, aprvdateDisp, crtdateDisp, revdteDisp, firstPaydateDisp, lastPaydateDisp, nextPaydateDisp, anvdateDisp, finalPaydateDisp, cancelDateDisp, recvdDateDisp, incurdtDisp , riskcommdteDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr682screen.class;
		protectRecord = Sr682protect.class;
	}

}
