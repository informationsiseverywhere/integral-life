package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5238
 * @version 1.0 generated on 30/08/09 06:38
 * @author Quipoz
 */
public class S5238ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(357);
	public FixedLengthStringData dataFields = new FixedLengthStringData(181).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData dob = DD.dob.copyToZonedDecimal().isAPartOf(dataFields,8);
	public ZonedDecimalData dtofdeath = DD.dtofdeath.copyToZonedDecimal().isAPartOf(dataFields,16);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,26);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,83);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,87);
	public FixedLengthStringData sex = DD.sex.copy().isAPartOf(dataFields,137);
	public FixedLengthStringData trandes = DD.trandes.copy().isAPartOf(dataFields,138);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(44).isAPartOf(dataArea, 181);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData dobErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData dtofdeathErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData sexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData trandesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 225);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] dobOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] dtofdeathOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] sexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] trandesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dobDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dtofdeathDisp = new FixedLengthStringData(10);

	public LongData S5238screenWritten = new LongData(0);
	public LongData S5238protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5238ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(trandesOut,new String[] {null, null, null, "04",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, life, jlife, lifenum, lifename, sex, dob, trandes, dtofdeath, reasoncd, resndesc};
		screenOutFields = new BaseData[][] {chdrnumOut, lifeOut, jlifeOut, lifenumOut, lifenameOut, sexOut, dobOut, trandesOut, dtofdeathOut, reasoncdOut, resndescOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifeErr, jlifeErr, lifenumErr, lifenameErr, sexErr, dobErr, trandesErr, dtofdeathErr, reasoncdErr, resndescErr};
		screenDateFields = new BaseData[] {dob, dtofdeath};
		screenDateErrFields = new BaseData[] {dobErr, dtofdeathErr};
		screenDateDispFields = new BaseData[] {dobDisp, dtofdeathDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5238screen.class;
		protectRecord = S5238protect.class;
	}

}
