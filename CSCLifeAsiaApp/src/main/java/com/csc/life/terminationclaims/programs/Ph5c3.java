package com.csc.life.terminationclaims.programs;


import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.anticipatedendowment.dataaccess.dao.LoanpfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Loanpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.KjohpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Kjohpf;
import com.csc.life.terminationclaims.screens.Sh5c3ScreenVars;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcchkrec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


public class Ph5c3 extends ScreenProgCS{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Ph5c3");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Batckey wsaaBatchkey = new Batckey();
	private FixedLengthStringData wsaaTrancde = new FixedLengthStringData(4);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Batcchkrec batcchkrec = new Batcchkrec();
	private ErrorsInner errorsInner = new ErrorsInner();
	private static final String t5679 = "T5679";
	private T5679rec t5679rec = new T5679rec();
	private Sh5c3ScreenVars sv = ScreenProgram.getScreenVars( Sh5c3ScreenVars.class);
	private Chdrpf chdrpf;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	private Cattpf cattpf;
	private CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	private KjohpfDAO kjohpfDAO = getApplicationContext().getBean("kjohpfDAO", KjohpfDAO.class);
	Kjohpf kjohpf= new Kjohpf();
	private LoanpfDAO loanpfDao = getApplicationContext().getBean("loanpfDAO",LoanpfDAO.class);
	private List<Loanpf> loanpfList = null;
	
	public Ph5c3() {
		super();
		screenVars = sv;
		new ScreenModel("Sh5c3", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		wsaaTrancde.set(wsaaBatchkey.batcBatctrcde);
		wsspcomn.submenu.set(wsaaProg);
		if(isEQ(sv.efdate, SPACES)){
			sv.efdate.set(varcom.vrcmMaxDate);
		}
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		if (isEQ(wsaaToday, ZERO)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*    CALL 'Sh5c3IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                Sh5c3-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if(validateAction2100()){
			validate2200();
			
		}else{
			verifyBatchControl2300();
		}
		
		if (isNE(sv.errorIndicators, SPACES))
            wsspcomn.edterror.set("Y");
        if (isEQ(scrnparams.statuz, "CALC")) {
            wsspcomn.edterror.set("Y");
        }
		
	}

protected void verifyBatchControl2300()
{
	bcbprogrec.company.set(wsspcomn.company);
	callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
	if (isNE(bcbprogrec.statuz, varcom.oK)) {
		sv.actionErr.set(bcbprogrec.statuz);
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*       GO TO 2900-EXIT                                           */
		return ;
	}
	wsspcomn.secProg[1].set(bcbprogrec.nxtprog1);
	wsspcomn.secProg[2].set(bcbprogrec.nxtprog2);
	wsspcomn.secProg[3].set(bcbprogrec.nxtprog3);
	wsspcomn.secProg[4].set(bcbprogrec.nxtprog4);
}

private void validateClaimNumber2220() {
	/*cattpf = cattpfDAO.approvedClaimRecord(sv.claimnmber.toString());
	if(null == cattpf && isNE(sv.claimnmber,SPACES)) {
		sv.claimnmberErr.set("jl71");
	}*/
	
	kjohpf=kjohpfDAO.getKjohpfRecord(" ", sv.claimnmber.toString());
	if(null == kjohpf && isNE(sv.claimnmber,SPACES)) {
		sv.claimnmberErr.set("jl71");
	}
	if(isEQ(sv.action, "I")){
		
		if(isEQ(sv.claimnmber, SPACES)){
			sv.claimnmberErr.set(errorsInner.f649);
		}
		else{
			String claimStat=kjohpf.getClamstat();
			if((null!=kjohpf) && !claimStat.equals("RG") && !claimStat.equals("AP")){
				sv.claimnmberErr.set(errorsInner.e767);
			}
		}
		
	}
}

protected boolean validateAction2100()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			return true;
		}
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			return true;
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.statuz, "BACH")) {
			return false;
		}
		return true;
	}

protected void validate2200()
	{
	
		try {
			
			
			if(isEQ(sv.action, "A")){
				
				if (isNE(sv.claimnmber, SPACES) && isEQ(sv.chdrsel, SPACES) && isEQ(sv.efdateDisp, SPACES)){
					sv.claimnmberErr.set(errorsInner.e844);
				}
				if(isEQ(sv.chdrsel, SPACES)){
					sv.chdrselErr.set(errorsInner.e696);
				}
			}
			
			if (isEQ(sv.chdrselErr, SPACES)) {
				validateChdrsel2210();
			}
			if (isNE(sv.chdrselErr, SPACES)) {
				return ;
			}
			if (isEQ(sv.claimnmberErr, SPACES)) {
				validateClaimNumber2220();
			}
			if(isNE(sv.action, "I")){
				validateEffdate2230();
			}
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
		
		                                     
	}

protected void validateChdrsel2210()
{
	
	
		if (isEQ(sv.action, "I")) {
			if (isNE(sv.chdrsel, SPACES)) {
				sv.chdrselErr.set(errorsInner.e844);
			}
		} else {
			chdrpf = new Chdrpf();
			chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsspcomn.company.toString(),
					sv.chdrsel.toString().trim());
			
			if (null == chdrpf)
				sv.chdrselErr.set(errorsInner.e544);
			else{
				kjohpf = kjohpfDAO.getKjohpfRecord(chdrpf.getChdrnum(), " ");
				if ((null!=kjohpf) && (kjohpf.getClamstat().equals("RG"))) {
				sv.chdrselErr.set(errorsInner.rumo);
				} 
				else if ((null!=kjohpf) && (kjohpf.getClamstat().equals("AP"))) {
				sv.chdrselErr.set(errorsInner.rump);
				}
			}
			if (isEQ(sv.chdrselErr, SPACES)) {
				checkStatus2500();
			}
		}
}

protected void checkStatus2500()
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(t5679);
	itempf.setItemitem(subprogrec.transcd.toString());
	itempf = itempfDAO. getItempfRecord(itempf);
	if(null==itempf){
		sv.chdrselErr.set(errorsInner.h205);
	}
	else
	{
	t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	lookForStat2510();
	}
}

protected void lookForStat2510()
{
		try {
				initialRisk2510();
				loopRisk2512();
				initialPrem2515();
				loopPrem2517();
		}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}

protected void initialRisk2510()
{
	wsaaSub.set(ZERO);
}

protected void loopRisk2512()
{
	wsaaSub.add(1);
	if (isGT(wsaaSub, 12)) {
		sv.chdrselErr.set(errorsInner.e767);
		wsspcomn.edterror.set("Y");
	}
	if (isNE(chdrpf.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
		loopRisk2512();
		return ;
	}
	}

protected void initialPrem2515()
{
	wsaaSub.set(ZERO);
}

protected void loopPrem2517()
{
	wsaaSub.add(1);
	if (isGT(wsaaSub, 12)) {
		sv.chdrselErr.set(errorsInner.e767);
		return ;
	}
	if (isNE(chdrpf.getPstcde(), t5679rec.cnPremStat[wsaaSub.toInt()])) {
		loopPrem2517();
		return ;
	}
}

protected void validateEffdate2230()
{
    loanpfList = loanpfDao.loadDataByLoanNum(wsspcomn.company.toString(), sv.chdrsel.toString(), "0");
	Loanpf loanpf= new Loanpf();
	
	if(null != loanpfList && loanpfList.size() > 0){
		loanpf = loanpfList.get(0);
	}
	if(null==chdrpf){
		chdrpf = new Chdrpf();
		chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsspcomn.company.toString(),
				sv.chdrsel.toString().trim());
	}
	Calendar previousPtd = Calendar.getInstance();
	SimpleDateFormat originalFormat = new SimpleDateFormat("yyyyMMdd");
	try {
		previousPtd.setTime(originalFormat.parse(chdrpf.getPtdate().toString()));
	
		previousPtd.add(Calendar.MONTH, -12/Integer.parseInt(chdrpf.getBillfreq()));
	} catch (ParseException e) {
		
		e.printStackTrace();
	}
	if (isEQ(sv.efdate, varcom.vrcmMaxDate)) {
		sv.efdate.set(wsaaToday);
	}
	else {
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(sv.efdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			sv.efdateErr.set(errorsInner.e032);
			sv.efdateOut[varcom.ri.toInt()].set("Y");
			return ;
		}
	}
	if (isGT(sv.efdate, wsaaToday)) {
		sv.efdateErr.set(errorsInner.f073);
	}
	else if (isLT(sv.efdate, chdrpf.getOccdate())) {
			sv.efdateErr.set(errorsInner.f616);
		
	}
	else if(null!=loanpf && isGT(loanpf.getLoansdate(),sv.efdate)){
		sv.efdateErr.set(errorsInner.ruml);
	}
	else if (isLT(sv.efdate, Integer.parseInt(originalFormat.format(previousPtd.getTime())))){
		sv.efdateErr.set(errorsInner.rumm);
	}
		
}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
{
updateWssp3010();
}	

protected void updateWssp3010()
{
	wsspcomn.sbmaction.set(scrnparams.action);
	wsaaBatchkey.set(wsspcomn.batchkey);
	wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
	wsspcomn.batchkey.set(wsaaBatchkey);
	wsspcomn.submenu.set(wsaaProg);
	
	if (isEQ(sv.action, "A")) {
		wsspcomn.flag.set("A");
	}
	if (isEQ(sv.action, "I")) {
		wsspcomn.flag.set("I");
	}

if (isEQ(scrnparams.statuz, "BACH")) {
	return;
}
wsspcomn.secProg[1].set(subprogrec.nxt1prog);
wsspcomn.secProg[2].set(subprogrec.nxt2prog);
wsspcomn.secProg[3].set(subprogrec.nxt3prog);
wsspcomn.secProg[4].set(subprogrec.nxt4prog);
/*    Soft lock contract.*/
if(isNE(sv.action, "I")){
	sftlockrec.function.set("LOCK");
	sftlockrec.company.set(wsspcomn.company);
	sftlockrec.enttyp.set("CH");
	/* MOVE S5245-CHDRSEL          TO SFTL-ENTITY                   */
	sftlockrec.entity.set(sv.chdrsel.toString());
	sftlockrec.transaction.set(subprogrec.transcd);
	sftlockrec.user.set(varcom.vrcmUser);
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz, varcom.oK)
	&& isNE(sftlockrec.statuz, "LOCK")) {
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
	if (isEQ(sftlockrec.statuz, "LOCK")) {
		sv.chdrselErr.set(errorsInner.f910);
		wsspcomn.edterror.set("Y");
		return;
	}
}
}


@Override
protected void whereNext4000()
{

wsspcomn.chdrChdrnum.set(sv.chdrsel);

/*NEXT-PROGRAM*/
	wsspcomn.programPtr.set(1);
	wsspcomn.nextprog.set(subprogrec.nxt1prog);
/*EXIT*/
}


/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData rumm = new FixedLengthStringData(4).init("RUMM");
	private FixedLengthStringData h205 = new FixedLengthStringData(4).init("H205");
	private FixedLengthStringData e032 = new FixedLengthStringData(4).init("E032");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData e844 = new FixedLengthStringData(4).init("E844");
	private FixedLengthStringData e696 = new FixedLengthStringData(4).init("E696");
	private FixedLengthStringData rumo = new FixedLengthStringData(4).init("RUMO");
	private FixedLengthStringData rump = new FixedLengthStringData(4).init("RUMP");
	private FixedLengthStringData f649 = new FixedLengthStringData(4).init("F649");
	private FixedLengthStringData ruml = new FixedLengthStringData(4).init("RUML");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
}


}
