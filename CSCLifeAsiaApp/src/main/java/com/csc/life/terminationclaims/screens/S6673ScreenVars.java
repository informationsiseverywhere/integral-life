package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6673
 * @version 1.0 generated on 30/08/09 06:57
 * @author Quipoz
 */
public class S6673ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(820);
	public FixedLengthStringData dataFields = new FixedLengthStringData(356).isAPartOf(dataArea, 0);
	public FixedLengthStringData bilfrqdesc = DD.bilfrqdesc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,30);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,32);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,64);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,66);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,108);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,138);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,185);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,193);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,195);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,242);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,250);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,254);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,258);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(dataFields,268);
	public FixedLengthStringData pstatdesc = DD.pstatdesc.copy().isAPartOf(dataFields,270);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,300);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,308);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,311);
	public FixedLengthStringData ridnum = DD.ridnum.copy().isAPartOf(dataFields,313);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(dataFields,315);
	public FixedLengthStringData statdesc = DD.statdesc.copy().isAPartOf(dataFields,317);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,341);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(116).isAPartOf(dataArea, 356);
	public FixedLengthStringData bilfrqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData pstatdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData ridnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData statcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData statdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(348).isAPartOf(dataArea, 472);
	public FixedLengthStringData[] bilfrqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] pstatdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] ridnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] statcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] statdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S6673screenWritten = new LongData(0);
	public LongData S6673protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6673ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
	/*ilife-2648...started by vjain60*/
		fieldIndMap.put(ridnumOut,new String[] {null, null, null, null,null, null, null, null, null, null, null, null});
	/*	<!-- ilife-2648...ended by vjain60 -->*/
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, life, coverage, rider, crtable, crtabdesc, ptdate, btdate, crrcd, pstatdesc, billfreq, statcode, pstatcode, bilfrqdesc, statdesc, numpols, planSuffix, sumin, ridnum};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, crtableOut, crtabdescOut, ptdateOut, btdateOut, crrcdOut, pstatdescOut, billfreqOut, statcodeOut, pstatcodeOut, bilfrqdescOut, statdescOut, numpolsOut, plnsfxOut, suminOut, ridnumOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, crtableErr, crtabdescErr, ptdateErr, btdateErr, crrcdErr, pstatdescErr, billfreqErr, statcodeErr, pstatcodeErr, bilfrqdescErr, statdescErr, numpolsErr, plnsfxErr, suminErr, ridnumErr};
		screenDateFields = new BaseData[] {ptdate, btdate, crrcd};
		screenDateErrFields = new BaseData[] {ptdateErr, btdateErr, crrcdErr};
		screenDateDispFields = new BaseData[] {ptdateDisp, btdateDisp, crrcdDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6673screen.class;
		protectRecord = S6673protect.class;
	}

}