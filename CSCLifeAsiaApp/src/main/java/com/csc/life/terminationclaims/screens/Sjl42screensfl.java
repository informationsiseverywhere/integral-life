package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 10/03/20 05:41
 * @author Sparikh8
 */
public class Sjl42screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 7;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 16, 2, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl42ScreenVars sv = (Sjl42ScreenVars) pv;
		
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sjl42screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sjl42screensfl, 
			sv.Sjl42screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sjl42ScreenVars sv = (Sjl42ScreenVars) pv;
		
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sjl42screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sjl42ScreenVars sv = (Sjl42ScreenVars) pv;

		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sjl42screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sjl42screensflWritten.gt(0))
		{
			sv.sjl42screensfl.setCurrentIndex(0);
			sv.Sjl42screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sjl42ScreenVars sv = (Sjl42ScreenVars) pv;
	
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sjl42screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl42ScreenVars screenVars = (Sjl42ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.htype.setFieldName("htype");
				screenVars.hcover.setFieldName("hcover");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.hcnstcur.setFieldName("hcnstcur");
				screenVars.hemv.setFieldName("hemv");
				screenVars.hactval.setFieldName("hactval");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.liencd.setFieldName("liencd");
				screenVars.actvalue.setFieldName("actvalue");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.shortds.setFieldName("shortds");
				screenVars.vfund.setFieldName("vfund");
				screenVars.fieldType.setFieldName("fieldType");
				screenVars.cnstcur.setFieldName("cnstcur");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.htype.set(dm.getField("htype"));
			screenVars.hcover.set(dm.getField("hcover"));
			screenVars.hcrtable.set(dm.getField("hcrtable"));
			screenVars.hcnstcur.set(dm.getField("hcnstcur"));
			screenVars.hemv.set(dm.getField("hemv"));
			screenVars.hactval.set(dm.getField("hactval"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.liencd.set(dm.getField("liencd"));
			screenVars.actvalue.set(dm.getField("actvalue"));
			screenVars.estMatValue.set(dm.getField("estMatValue"));
			screenVars.shortds.set(dm.getField("shortds"));
			screenVars.vfund.set(dm.getField("vfund"));
			screenVars.fieldType.set(dm.getField("fieldType"));
			screenVars.cnstcur.set(dm.getField("cnstcur"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl42ScreenVars screenVars = (Sjl42ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.htype.setFieldName("htype");
				screenVars.hcover.setFieldName("hcover");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.hcnstcur.setFieldName("hcnstcur");
				screenVars.hemv.setFieldName("hemv");
				screenVars.hactval.setFieldName("hactval");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.liencd.setFieldName("liencd");
				screenVars.actvalue.setFieldName("actvalue");
				screenVars.estMatValue.setFieldName("estMatValue");
				screenVars.shortds.setFieldName("shortds");
				screenVars.vfund.setFieldName("vfund");
				screenVars.fieldType.setFieldName("fieldType");
				screenVars.cnstcur.setFieldName("cnstcur");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("htype").set(screenVars.htype);
			dm.getField("hcover").set(screenVars.hcover);
			dm.getField("hcrtable").set(screenVars.hcrtable);
			dm.getField("hcnstcur").set(screenVars.hcnstcur);
			dm.getField("hemv").set(screenVars.hemv);
			dm.getField("hactval").set(screenVars.hactval);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("liencd").set(screenVars.liencd);
			dm.getField("actvalue").set(screenVars.actvalue);
			dm.getField("estMatValue").set(screenVars.estMatValue);
			dm.getField("shortds").set(screenVars.shortds);
			dm.getField("vfund").set(screenVars.vfund);
			dm.getField("fieldType").set(screenVars.fieldType);
			dm.getField("cnstcur").set(screenVars.cnstcur);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sjl42screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		
		Sjl42ScreenVars screenVars = (Sjl42ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.htype.clearFormatting();
		screenVars.hcover.clearFormatting();
		screenVars.hcrtable.clearFormatting();
		screenVars.hcnstcur.clearFormatting();
		screenVars.hemv.clearFormatting();
		screenVars.hactval.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.liencd.clearFormatting();
		screenVars.actvalue.clearFormatting();
		screenVars.estMatValue.clearFormatting();
		screenVars.shortds.clearFormatting();
		screenVars.vfund.clearFormatting();
		screenVars.fieldType.clearFormatting();
		screenVars.cnstcur.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sjl42ScreenVars screenVars = (Sjl42ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.htype.setClassString("");
		screenVars.hcover.setClassString("");
		screenVars.hcrtable.setClassString("");
		screenVars.hcnstcur.setClassString("");
		screenVars.hemv.setClassString("");
		screenVars.hactval.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.liencd.setClassString("");
		screenVars.actvalue.setClassString("");
		screenVars.estMatValue.setClassString("");
		screenVars.shortds.setClassString("");
		screenVars.vfund.setClassString("");
		screenVars.fieldType.setClassString("");
		screenVars.cnstcur.setClassString("");
	}

/**
 * Clear all the variables in SJL42screensfl
 */
	public static void clear(VarModel pv) {
		Sjl42ScreenVars screenVars = (Sjl42ScreenVars) pv;
		
		screenVars.screenIndicArea.clear();
		screenVars.htype.clear();
		screenVars.hcover.clear();
		screenVars.hcrtable.clear();
		screenVars.hcnstcur.clear();
		screenVars.hemv.clear();
		screenVars.hactval.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.liencd.clear();
		screenVars.actvalue.clear();
		screenVars.estMatValue.clear();
		screenVars.shortds.clear();
		screenVars.vfund.clear();
		screenVars.fieldType.clear();
		screenVars.cnstcur.clear();
	}
}
