package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sjl55ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData payNum = DD.reqno.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData claimNum =  DD.claimnumber.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData contractNum =  DD.chdrnum.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 26);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 29);
	public ZonedDecimalData payDate = DD.paydte.copyToZonedDecimal().isAPartOf(dataFields,59);
	public FixedLengthStringData payMethod =  DD.paymthd.copy().isAPartOf(dataFields,67);
	public ZonedDecimalData totalPayAmt = DD.qpaymnt.copyToZonedDecimal().isAPartOf(dataFields,97);
	public FixedLengthStringData clientnum = DD.lifcnum.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData clntName = DD.linsname.copy().isAPartOf(dataFields,122);
	public FixedLengthStringData createdUser = DD.username.copy().isAPartOf(dataFields,169);
	public FixedLengthStringData modifiedUser = DD.username.copy().isAPartOf(dataFields,193);
	public FixedLengthStringData approvedUser = DD.username.copy().isAPartOf(dataFields,217);
	public FixedLengthStringData authorizedUser = DD.username.copy().isAPartOf(dataFields,241);
	public ZonedDecimalData createdDate = DD.paydte.copyToZonedDecimal().isAPartOf(dataFields,265);
	public ZonedDecimalData modifiedDate = DD.paydte.copyToZonedDecimal().isAPartOf(dataFields,273);
	public ZonedDecimalData approvedDate = DD.paydte.copyToZonedDecimal().isAPartOf(dataFields,281);
	public ZonedDecimalData authorizedDate = DD.paydte.copyToZonedDecimal().isAPartOf(dataFields,289);
	public FixedLengthStringData payStatus =  DD.paymthd.copy().isAPartOf(dataFields,297);
	public FixedLengthStringData scrndesc = DD.scrndesc.copy().isAPartOf(dataFields,327);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData payNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData claimNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData contractNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData payDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData payMethodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData totalPayAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData clientnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData clntNameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData createdUserErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData modifiedUserErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData approvedUserErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData authorizedUserErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData createdDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData modifiedDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData approvedDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData authorizedDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData payStatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] payNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] claimNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] contractNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] payDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] payMethodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] totalPayAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] clientnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] clntNameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] createdUserOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] modifiedUserOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] approvedUserOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] authorizedUserOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] createdDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] modifiedDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] approvedDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] authorizedDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] payStatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(366);//MLIF-111
	public FixedLengthStringData subfileFields = new FixedLengthStringData(219).isAPartOf(subfileArea, 0);
	public ZonedDecimalData seqenum = DD.seqno.copyToZonedDecimal().isAPartOf(subfileArea,0);
	public FixedLengthStringData clttwo = DD.clttwo.copy().isAPartOf(subfileArea,2);
	public FixedLengthStringData clntID = DD.linsname.copy().isAPartOf(subfileArea,10);
	public FixedLengthStringData babrdc = DD.babrdc.copy().isAPartOf(subfileArea,57);
	public FixedLengthStringData accdesc = DD.accdesc.copy().isAPartOf(subfileArea,117);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(subfileArea,147);
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(subfileArea,167);
	public ZonedDecimalData prcent = DD.prcent.copyToZonedDecimal().isAPartOf(subfileArea,197);
	public ZonedDecimalData pymt = DD.pymt.copyToZonedDecimal().isAPartOf(subfileArea,202);
	/*<!-- SGS006 END -->*/
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 219);
	public FixedLengthStringData seqenumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData clttwoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData clntIDErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData babrdcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData accdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData prcentErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData pymtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	/*<!-- SGS006 END -->*/
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 255);
	public FixedLengthStringData[] seqenumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] clttwoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] clntIDOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] babrdcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] accdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] prcentOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] pymtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea,363);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData createdDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData modifiedDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData approvedDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData authorizedDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData payDateDisp = new FixedLengthStringData(10);
	
	public FixedLengthStringData pmpmt020Flag = new FixedLengthStringData(1); //IFSU-3795

	public LongData Sjl55screensflWritten = new LongData(0);
	public LongData Sjl55screenctlWritten = new LongData(0);
	public LongData Sjl55screenWritten = new LongData(0);
	public LongData Sjl55protectWritten = new LongData(0);
	public GeneralTable sjl55screensfl = new GeneralTable(AppVars.getInstance());
	
	public static int[] screenSflPfInds = new int[] {1, 2, 3, 4, 5, 7, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24};  
	public static int[] screenSflAffectedInds = new int[] { 2,  3,  4,  6,  7, 20,  21,  22,  23,  24,  25,  26,  27, 28, 29, 33,  50,  55}; 

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sjl55screensfl;
	}

	public Sjl55ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {

		fieldIndMap.put(seqenumOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clttwoOut,new String[] {"13","23", "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntIDOut,new String[] {"14","24", "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(babrdcOut,new String[] {"15","25", "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(accdescOut,new String[] {"16","26", "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankacckeyOut,new String[] {"17","27", "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankaccdscOut,new String[] {"18","28", "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prcentOut,new String[] {"19","21", "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pymtOut,new String[] {"20","22", "-20",null, null, null, null, null, null, null, null, null});
				
		screenSflFields=getscreenLSflFields();
		screenSflOutFields= getscreenSflOutFields();
		screenSflErrFields= getscreenSflErrFields();
		screenSflDateFields=getscreenSflDateFields();
		screenSflDateErrFields=getscreenSflDateErrFields();
		screenSflDateDispFields = getscreenSflDateDispFields();	

		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sjl55screen.class;
		screenSflRecord = Sjl55screensfl.class;
		screenCtlRecord = Sjl55screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sjl55protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl55screenctl.lrec.pageSubfile);
	}
	
	public int getDataAreaSize()
	{
		return 661;
	}
	
	public int getDataFieldsSize()
	{
		return 357;
	}
	public int getErrorIndicatorSize()
	{
		return 76;
	}
	public int getOutputFieldSize()
	{
		return 228;
	}
	public int getSubfileAreaSize()
	{
		return 449;
	}	
	
	public int getSubfileFieldsSize()
	{
		return 178;
	}	
	
	public int getErrorSubfileSize()
	{
		return 64;
	}	
	
	public int getOutputSubfileSize()
	{
		return 204;
	}
	
	public BaseData[] getscreenFields()
	{
		return new BaseData[] {payNum, claimNum, contractNum, cnttype, ctypedes, payDate, payMethod, totalPayAmt, clientnum, clntName, createdUser, modifiedUser, approvedUser, authorizedUser, createdDate, modifiedDate, approvedDate, authorizedDate, payStatus}; 
		
	}	
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {payNumOut, claimNumOut, contractNumOut, cnttypeOut, ctypedesOut, payDateOut, payMethodOut, totalPayAmtOut, clientnumOut, clntNameOut, createdUserOut, modifiedUserOut, approvedUserOut, authorizedUserOut, createdDateOut, modifiedDateOut, approvedDateOut, authorizedDateOut, payStatusOut};
		
	}	
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {payNumErr, claimNumErr, contractNumErr, cnttypeErr, ctypedesErr, payDateErr, payMethodErr, totalPayAmtErr, clientnumErr, clntNameErr, createdUserErr, modifiedUserErr, approvedUserErr, authorizedUserErr, createdDateErr, modifiedDateErr, approvedDateErr, authorizedDateErr, payStatusErr};
	}
	
	public BaseData[] getscreenLSflFields()
	{
		return new BaseData[] {seqenum, clttwo, clntID, babrdc, accdesc, bankacckey, bankaccdsc, prcent, pymt};
		
	}
	
	public BaseData[][] getscreenSflOutFields()
	{
		return new BaseData[][] {seqenumOut, clttwoOut, clntIDOut, babrdcOut, accdescOut, bankacckeyOut, bankaccdscOut, prcentOut, pymtOut};
	}	
	
	public BaseData[] getscreenSflErrFields()
	{
		return new BaseData[] {seqenumErr, clttwoErr, clntIDErr,babrdcErr, accdescErr, bankacckeyErr, bankaccdscErr, prcentErr, pymtErr};
	}
	
	public BaseData[] getscreenSflDateFields()
	{
		return new BaseData[] {};
	}
	
	public BaseData[] getscreenSflDateErrFields()
	{
		return new BaseData[] {};
	}
	
	public BaseData[] getscreenSflDateDispFields()
	{
		return new BaseData[] {};
	}	
	
	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {payDate, createdDate, modifiedDate, approvedDate, authorizedDate};
	}	
	
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {payDateDisp, createdDateDisp, modifiedDateDisp, approvedDateDisp, authorizedDateDisp};
	}	
	
	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {payDateErr, createdDateErr, modifiedDateErr, approvedDateErr, authorizedDateErr};
	}	
	
	public static int[] getScreenSflPfInds()
	{
		return screenSflPfInds;
	}	
	
	public static int[] getScreenSflAffectedInds()
	{
		return screenSflAffectedInds;
	}
}

