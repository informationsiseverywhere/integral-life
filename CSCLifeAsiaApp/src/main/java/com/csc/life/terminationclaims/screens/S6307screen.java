package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6307screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {18, 22, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6307ScreenVars sv = (S6307ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6307screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6307ScreenVars screenVars = (S6307ScreenVars)pv;
		screenVars.effdateDisp.setClassString("");
		screenVars.clamant.setClassString("");
		screenVars.estimateTotalValue.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.totalfee.setClassString("");
		screenVars.totalamt.setClassString("");
		screenVars.prcnt.setClassString("");
		screenVars.taxamt.setClassString("");
		screenVars.reserveUnitsInd.setClassString("");
		screenVars.reserveUnitsDateDisp.setClassString("");
		screenVars.reqntype.setClassString("");
		screenVars.payee.setClassString(""); //ICIL-254
		screenVars.payeename.setClassString(""); //ICIL-254
		screenVars.bankacckey.setClassString(""); //ICIL-254
        screenVars.bankdesc.setClassString(""); //ICIL-254
        screenVars.bankkey.setClassString(""); //ICIL-254
        screenVars.crdtcrd.setClassString("");
	}

/**
 * Clear all the variables in S6307screen
 */
	public static void clear(VarModel pv) {
		S6307ScreenVars screenVars = (S6307ScreenVars) pv;
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.clamant.clear();
		screenVars.estimateTotalValue.clear();
		screenVars.currcd.clear();
		screenVars.totalfee.clear();
		screenVars.totalamt.clear();
		screenVars.prcnt.clear();
		screenVars.taxamt.clear();
		screenVars.reserveUnitsInd.clear();
		screenVars.reserveUnitsDateDisp.clear();
		screenVars.reqntype.clear();
		screenVars.payee.clear(); //ICIL-254
		screenVars.payeename.clear(); //ICIL-254
		screenVars.bankacckey.clear(); //ICIL-254
        screenVars.bankdesc.clear(); //ICIL-254
        screenVars.bankkey.clear(); //ICIL-254
        screenVars.crdtcrd.clear();
	}
}
