package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Phuc Vuu
 */
public class Sr57tscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 6;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 16, 2, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr57tScreenVars sv = (Sr57tScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr57tscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr57tscreensfl, 
			sv.Sr57tscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr57tScreenVars sv = (Sr57tScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr57tscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr57tScreenVars sv = (Sr57tScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr57tscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr57tscreensflWritten.gt(0))
		{
			sv.sr57tscreensfl.setCurrentIndex(0);
			sv.Sr57tscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr57tScreenVars sv = (Sr57tScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr57tscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr57tScreenVars screenVars = (Sr57tScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.life.setFieldName("life");
				screenVars.crtable.setFieldName("crtable");
				screenVars.lcendesc.setFieldName("lcendesc");
				screenVars.actvalue.setFieldName("actvalue");
				screenVars.zclmadjst.setFieldName("zclmadjst");
				screenVars.zhldclmv.setFieldName("zhldclmv");
				screenVars.zhldclma.setFieldName("zhldclma");
				
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.life.set(dm.getField("life"));
			screenVars.crtable.set(dm.getField("crtable"));
			screenVars.lcendesc.set(dm.getField("lcendesc"));
			screenVars.actvalue.set(dm.getField("actvalue"));
			screenVars.zclmadjst.set(dm.getField("zclmadjst"));
			screenVars.zhldclmv.set(dm.getField("zhldclmv"));
			screenVars.zhldclma.set(dm.getField("zhldclma"));
			
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr57tScreenVars screenVars = (Sr57tScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.life.setFieldName("life");
				screenVars.crtable.setFieldName("crtable");
				screenVars.lcendesc.setFieldName("lcendesc");
				screenVars.actvalue.setFieldName("actvalue");
				screenVars.zclmadjst.setFieldName("zclmadjst");
				screenVars.zhldclmv.setFieldName("zhldclmv");
				screenVars.zhldclma.setFieldName("zhldclma");

			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("life").set(screenVars.life);
			dm.getField("crtable").set(screenVars.crtable);
			dm.getField("lcendesc").set(screenVars.lcendesc);
			dm.getField("actvalue").set(screenVars.actvalue);
			dm.getField("zclmadjst").set(screenVars.zclmadjst);
			dm.getField("zhldclmv").set(screenVars.zhldclmv);
			dm.getField("zhldclma").set(screenVars.zhldclma);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr57tscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr57tScreenVars screenVars = (Sr57tScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.crtable.clearFormatting();
		screenVars.lcendesc.clearFormatting();
		screenVars.actvalue.clearFormatting();
		screenVars.zclmadjst.clearFormatting();
		screenVars.zhldclmv.clearFormatting();
		screenVars.zhldclma.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr57tScreenVars screenVars = (Sr57tScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.life.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.lcendesc.setClassString("");
		screenVars.actvalue.setClassString("");
		screenVars.zclmadjst.setClassString("");
		screenVars.zhldclmv.setClassString("");
		screenVars.zhldclma.setClassString("");
	}

/**
 * Clear all the variables in Sr57tscreensfl
 */
	public static void clear(VarModel pv) {
		Sr57tScreenVars screenVars = (Sr57tScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.life.clear();
		screenVars.crtable.clear();
		screenVars.lcendesc.clear();
		screenVars.actvalue.clear();
		screenVars.zclmadjst.clear();
		screenVars.zhldclmv.clear();
		screenVars.zhldclma.clear();
	}
}
