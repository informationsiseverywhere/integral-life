package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.productdefinition.dataaccess.dao.ActxDAO;
import com.csc.life.productdefinition.dataaccess.model.Br523DTO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.terminationclaims.dataaccess.dao.ClmdpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Clmdpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmhpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ClmdpfDAOImpl extends BaseDAOImpl<Clmdpf> implements ClmdpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClmdpfDAOImpl.class);

    public List<Clmdpf> selectClmdpfRecord(Clmdpf clmdpf){
    	List<Clmdpf> covrResult = new ArrayList<Clmdpf>();
      	 StringBuilder sb = new StringBuilder("");
      	 
      	sb.append("SELECT CHDRCOY,CHDRNUM,COVERAGE,CRTABLE,JLIFE,LIFE,RIDER,ACTVALUE,EMV,LIENCD,CNSTCUR,ANNYPIND,UNIQUE_NUMBER,CLAIMNO,CLAIMNOTIFINO,TYPE_T ");
      	sb.append("FROM CLMDPF WHERE CHDRCOY=? AND CHDRNUM=? ");
      	sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, COVERAGE ASC, RIDER ASC, CRTABLE ASC, UNIQUE_NUMBER DESC");
      	
      	PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet sql1rs = null;
        try {
       	
       	 ps.setInt(1, Integer.parseInt(clmdpf.getChdrcoy()));
       	 ps.setString(2, clmdpf.getChdrnum());
       	 
       	 sql1rs = executeQuery(ps);

       	 while (sql1rs.next()) {  
	        	clmdpf = new Clmdpf();
	        	
	        	clmdpf.setChdrcoy(sql1rs.getString(1));
	        	clmdpf.setChdrnum(sql1rs.getString(2));        	
	        	clmdpf.setCoverage(sql1rs.getString(3));           
	        	clmdpf.setCrtable(sql1rs.getString(4));
	        	clmdpf.setJlife(sql1rs.getString(5));
	        	clmdpf.setLife(sql1rs.getString(6));
	        	clmdpf.setRider(sql1rs.getString(7));
	            clmdpf.setActvalue(sql1rs.getDouble(8));
	            clmdpf.setEstMatValue(sql1rs.getDouble(9));
	            clmdpf.setLiencd(sql1rs.getString(10));
	            clmdpf.setCnstcur(sql1rs.getString(11));
	            clmdpf.setAnnypind(sql1rs.getString(12));
	        	clmdpf.setUniqueNumber(sql1rs.getLong(13));  
	        	clmdpf.setClaimno(sql1rs.getString(14));
	        	clmdpf.setClaimnotifino(sql1rs.getString(15));
	        	clmdpf.setFieldType(sql1rs.getString("TYPE_T"));	        	
	            covrResult.add(clmdpf);
       	 	}
       	 
        }
        
        catch (SQLException e) {
            LOGGER.error("searchCovrRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, sql1rs);
        }
      	return covrResult;
    }
    public void insertClmdRecord(List<Clmdpf> clmdpfList){
		StringBuilder sql = new StringBuilder("INSERT INTO CLMDCLM (CHDRCOY,CHDRNUM,TRANNO,LIFE,JLIFE,COVERAGE,RIDER,VALIDFLAG,CRTABLE,SHORTDS,LIENCD,CNSTCUR,EMV,ACTVALUE");
				sql.append(",VIRTFND,TYPE_T,ANNYPIND,USRPRF,JOBNM,DATIME,CLAIMNO,CLAIMNOTIFINO)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		try {
			for(Clmdpf clmdpf :clmdpfList){
				ps.setString(1, clmdpf.getChdrcoy());
				ps.setString(2, clmdpf.getChdrnum());
				ps.setInt(3, clmdpf.getTranno());
				ps.setString(4, clmdpf.getLife());
				ps.setString(5, clmdpf.getJlife());
				ps.setString(6, clmdpf.getCoverage());
				ps.setString(7, clmdpf.getRider());
				ps.setString(8, clmdpf.getValidflag());
				ps.setString(9, clmdpf.getCrtable());
				ps.setString(10, clmdpf.getShortds());
				ps.setString(11, clmdpf.getLiencd());
				ps.setString(12, clmdpf.getCnstcur());
				ps.setDouble(13, clmdpf.getEstMatValue());
				ps.setDouble(14, clmdpf.getActvalue());
				ps.setString(15, clmdpf.getVirtualFund());
				ps.setString(16, clmdpf.getFieldType());
				ps.setString(17, clmdpf.getAnnypind());
				ps.setString(18, getUsrprf());
				ps.setString(19, getJobnm());
				ps.setTimestamp(20, new Timestamp(System.currentTimeMillis()));
				ps.setString(21, clmdpf.getClaimno());
				ps.setString(22, clmdpf.getClaimnotifino());
				ps.addBatch();
			}
			ps.executeBatch();
        } catch (SQLException e) {
            LOGGER.error("insertClmhRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    }
    
    public void updateCnstCurActValue(Clmdpf clmdpf)
    {
    	String sql = "UPDATE CLMDPF SET CNSTCUR = ? , ACTVALUE = ? , USRPRF = ? , JOBNM = ? , DATIME = ? WHERE UNIQUE_NUMBER = ?";    	
    	try(PreparedStatement ps = getPrepareStatement(sql);)
    	{
    		ps.setString(1, clmdpf.getCnstcur());
    		ps.setDouble(2, clmdpf.getActvalue());    		
    		ps.setString(3, getUsrprf());
    		ps.setString(4, getJobnm());
    		ps.setTimestamp(5, getDatime());
    		ps.setLong(6, clmdpf.getUniqueNumber());
    		ps.executeUpdate();
    	} catch (SQLException e) {
            LOGGER.error("updateCnstCurActValue(){ }", e);
            throw new SQLRuntimeException(e);
        }
    }
	
	 public void deleteRcdByChdrnum(String chdrcoy,String chdrnum) {
		 StringBuilder sb = new StringBuilder("");
			sb.append("DELETE FROM CLMDPF");
			sb.append(" WHERE CHDRCOY=?  AND CHDRNUM=? AND VALIDFLAG = '1' ");
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());	

				    ps.setString(1, chdrcoy);
				    ps.setString(2, chdrnum);
			    
				    ps.executeUpdate();				
					
				
			}catch (SQLException e) {
				LOGGER.error("deleteRcdByChdrnum()", e);	
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}				
	 }		    
}
