package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr687screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr687ScreenVars sv = (Sr687ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr687screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr687ScreenVars screenVars = (Sr687ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.benfamt01.setClassString("");
		screenVars.benfamt02.setClassString("");
		screenVars.benfamt03.setClassString("");
		screenVars.benfamt04.setClassString("");
		screenVars.benfamt05.setClassString("");
		screenVars.benfamt06.setClassString("");
		screenVars.nofday01.setClassString("");
		screenVars.nofday02.setClassString("");
		screenVars.nofday03.setClassString("");
		screenVars.nofday04.setClassString("");
		screenVars.nofday05.setClassString("");
		screenVars.nofday06.setClassString("");
		screenVars.copay01.setClassString("");
		screenVars.copay02.setClassString("");
		screenVars.copay03.setClassString("");
		screenVars.copay04.setClassString("");
		screenVars.copay05.setClassString("");
		screenVars.copay06.setClassString("");
		screenVars.hosben01.setClassString("");
		screenVars.hosben02.setClassString("");
		screenVars.hosben03.setClassString("");
		screenVars.hosben04.setClassString("");
		screenVars.hosben05.setClassString("");
		screenVars.hosben06.setClassString("");
		screenVars.amtlife01.setClassString("");
		screenVars.amtlife02.setClassString("");
		screenVars.amtlife03.setClassString("");
		screenVars.amtlife04.setClassString("");
		screenVars.amtlife05.setClassString("");
		screenVars.amtlife06.setClassString("");
		screenVars.amtyear01.setClassString("");
		screenVars.amtyear02.setClassString("");
		screenVars.amtyear03.setClassString("");
		screenVars.amtyear04.setClassString("");
		screenVars.amtyear05.setClassString("");
		screenVars.amtyear06.setClassString("");
		screenVars.gdeduct01.setClassString("");
		screenVars.gdeduct02.setClassString("");
		screenVars.gdeduct03.setClassString("");
		screenVars.gdeduct04.setClassString("");
		screenVars.gdeduct05.setClassString("");
		screenVars.gdeduct06.setClassString("");
		screenVars.aad.setClassString("");
		screenVars.premunit.setClassString("");
		screenVars.zssi.setClassString("");
		screenVars.factor.setClassString("");
		screenVars.contitem.setClassString("");
		screenVars.benefits01.setClassString("");
		screenVars.benefits02.setClassString("");
		screenVars.benefits03.setClassString("");
		screenVars.benefits04.setClassString("");
		screenVars.benefits05.setClassString("");
		screenVars.benefits06.setClassString("");
	}

/**
 * Clear all the variables in Sr687screen
 */
	public static void clear(VarModel pv) {
		Sr687ScreenVars screenVars = (Sr687ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.benfamt01.clear();
		screenVars.benfamt02.clear();
		screenVars.benfamt03.clear();
		screenVars.benfamt04.clear();
		screenVars.benfamt05.clear();
		screenVars.benfamt06.clear();
		screenVars.nofday01.clear();
		screenVars.nofday02.clear();
		screenVars.nofday03.clear();
		screenVars.nofday04.clear();
		screenVars.nofday05.clear();
		screenVars.nofday06.clear();
		screenVars.copay01.clear();
		screenVars.copay02.clear();
		screenVars.copay03.clear();
		screenVars.copay04.clear();
		screenVars.copay05.clear();
		screenVars.copay06.clear();
		screenVars.hosben01.clear();
		screenVars.hosben02.clear();
		screenVars.hosben03.clear();
		screenVars.hosben04.clear();
		screenVars.hosben05.clear();
		screenVars.hosben06.clear();
		screenVars.amtlife01.clear();
		screenVars.amtlife02.clear();
		screenVars.amtlife03.clear();
		screenVars.amtlife04.clear();
		screenVars.amtlife05.clear();
		screenVars.amtlife06.clear();
		screenVars.amtyear01.clear();
		screenVars.amtyear02.clear();
		screenVars.amtyear03.clear();
		screenVars.amtyear04.clear();
		screenVars.amtyear05.clear();
		screenVars.amtyear06.clear();
		screenVars.gdeduct01.clear();
		screenVars.gdeduct02.clear();
		screenVars.gdeduct03.clear();
		screenVars.gdeduct04.clear();
		screenVars.gdeduct05.clear();
		screenVars.gdeduct06.clear();
		screenVars.aad.clear();
		screenVars.premunit.clear();
		screenVars.zssi.clear();
		screenVars.factor.clear();
		screenVars.contitem.clear();
		screenVars.benefits01.clear();
		screenVars.benefits02.clear();
		screenVars.benefits03.clear();
		screenVars.benefits04.clear();
		screenVars.benefits05.clear();
		screenVars.benefits06.clear();
	}
}
