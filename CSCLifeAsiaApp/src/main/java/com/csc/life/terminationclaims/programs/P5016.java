/*
 * File: P5016.java
 * Date: 29 August 2009 23:55:27
 * Author: Quipoz Limited
 * 
 * Class transformed from P5016.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;//ILIFE-7688
import com.csc.fsu.clients.dataaccess.model.Clntpf;//ILIFE-7688
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;//ILIFE-7688
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.contractservicing.dataaccess.FupeTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HxclTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.FlupclmTableDAM;
//import com.csc.life.terminationclaims.programs.P5256.FormatsInner;//ILIFE-7688
import com.csc.life.terminationclaims.screens.S5016ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILIFE-7688
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
* Retrieve the contract header information from the CHDRCLM I/O
* module.
*
* Look up the following:
*
*      -the contract type description from T5688.
*
*      Any number of follow-ups are allowed per contract. These may
*      optionally bE attached to a life. A subfile, with a program
*      controlled   rolling,   will  be  used to  maintain  the
*      records. Clear  the  subfile and load any existing follow-ups
*      (FLUPCLM), up to a maximum of one page full.  for each record
*      read:
*
*          - add a record to the subfile, initialising all the
*               fields as retrieved from the follow-ups file. Use a
*               "hidden" field to hold the sequence number from the
*                key.
*
*          - set a  flag  in a "hidden" field to indicate that the
*              record was loaded from the database (and so is not
*              new - required during updating).
*
*  If the first page  is not  full, add the number of blank
*  records required to  the subfile in order to display one full
*  page. Set the subfile more indicator to 'Y'. Set the "hidden"
*  follow-up sequence number to the next available number (i.e.
*  one after the last one added to the subfile).
*
*  Retrieve todays date (DATCON1) and store for later use.
*
*  Set a flag to indicate that this is the first page (roll down
*  not allowed).
*
* Validation
* ----------
*
* If in enquiry  mode  (WSSP-FLAG  =  '1') protect  the entire
* screen prior to output.
*
* If first page of subfile is shown and RollDown key is pressed
* display error message saying RollDown not permitted.
*
* Where next
* ----------
*
* If the RollUp key is pressed, show the next 12 Follow-up
* records. If no more records available, show a screen
* consisting of 12 blank lines.
*
* If the RollDown key is pressed, show the previous 12 Follow-up
* records.
*
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Validation has been added to detect the presence of a follow
* up letter type for printing of either standard follow up
* letter or a reminder. If follow up reminder date is left
* blank for standard letter, it will be automatically assigned
* with consideration of the elapsed period of the letter's type.
*
*****************************************************************
* </pre>
*/
public class P5016 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5016");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).setUnsigned();
	protected String wsaaFirstPage = "";
	private String wsaaNoMoreFlups = "";
	private PackedDecimalData wsaaFupno = new PackedDecimalData(2, 0);
	protected PackedDecimalData wsaaRrn = new PackedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);

		/* WSBB-STACK-ARRAY */
	private FixedLengthStringData wsbbFlupkeyArray = new FixedLengthStringData(10);
	private PackedDecimalData[] wsbbFlupkey = PDArrayPartOfStructure(5, 2, 0, wsbbFlupkeyArray, 0);

	private FixedLengthStringData wsbbChangeArray = new FixedLengthStringData(5);
	private FixedLengthStringData[] wsbbChange = FLSArrayPartOfStructure(5, 1, wsbbChangeArray, 0);

	private FixedLengthStringData wsaaFirstFlupKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaFlupKeyChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaFirstFlupKey, 0);
	private FixedLengthStringData wsaaFlupKeyChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstFlupKey, 1);
	private PackedDecimalData wsaaFlupKeyFupno = new PackedDecimalData(2, 0).isAPartOf(wsaaFirstFlupKey, 9);
	private FixedLengthStringData filler = new FixedLengthStringData(53).isAPartOf(wsaaFirstFlupKey, 11, FILLER).init(SPACES);

		/* WSAA-ROLL-FLGS */
	private FixedLengthStringData wsaaRolu = new FixedLengthStringData(1);
	private Validator rollup = new Validator(wsaaRolu, "Y");

	protected FixedLengthStringData wsaaRold = new FixedLengthStringData(1);
	private Validator rolldown = new Validator(wsaaRold, "Y");

	private FixedLengthStringData wsaaLastFlupKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaLastFlupKeyChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaLastFlupKey, 0);
	private FixedLengthStringData wsaaLastFlupKeyChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaLastFlupKey, 1);
	private PackedDecimalData wsaaLastFlupKeyFupno = new PackedDecimalData(2, 0).isAPartOf(wsaaLastFlupKey, 9);
	private FixedLengthStringData filler1 = new FixedLengthStringData(53).isAPartOf(wsaaLastFlupKey, 11, FILLER).init(SPACES);
	private String wsaaAllBlankLines = "";

	private FixedLengthStringData wsaaKeysArray = new FixedLengthStringData(320);
	private FixedLengthStringData[] wsaaRecordKey = FLSArrayPartOfStructure(5, 64, wsaaKeysArray, 0);
	private FixedLengthStringData[] wsaaRecordKeyChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaRecordKey, 0);
	private FixedLengthStringData[] wsaaRecordKeyChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaRecordKey, 1);
	private PackedDecimalData[] wsaaRecordKeyFupno = PDArrayPartOfArrayStructure(2, 0, wsaaRecordKey, 9);
	private FixedLengthStringData[] filler2 = FLSDArrayPartOfArrayStructure(53, wsaaRecordKey, 11, SPACES);

	private FixedLengthStringData wsaaCheckBox = new FixedLengthStringData(1);
	private Validator checkBox = new Validator(wsaaCheckBox, "Y");
	private PackedDecimalData ix = new PackedDecimalData(2, 0);
	private ZonedDecimalData iy = new ZonedDecimalData(2, 0);
	private PackedDecimalData wsaaDocseq = new PackedDecimalData(2, 0);

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
		/* ERRORS */
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t5661 = "T5661";
	private static final String t5660 = "T5660";
	private static final String flupclmrec = "FLUPCLMREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String fuperec = "FUPEREC";
	private static final String itemrec = "ITEMREC";
	private static final String hxclrec = "HXCLREC";
		/*Claims Contract Header*/
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Follow Ups for Death Claims*/
	private FlupclmTableDAM flupclmIO = new FlupclmTableDAM();
		/*Follow-up Extended Text File*/
	private FupeTableDAM fupeIO = new FupeTableDAM();
		/*Exclusion Clauses Logical File*/
	private HxclTableDAM hxclIO = new HxclTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details - Contract Enquiry.*/
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private T5661rec t5661rec = new T5661rec();
	private Optswchrec optswchrec = new Optswchrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	protected S5016ScreenVars sv = getLScreenVars();//ScreenProgram.getScreenVars( S5016ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	
	//ILIFE-7688 start
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	protected FormatsInner formatsInner = new FormatsInner();
	private Clntpf cltspf = new Clntpf();
	private ClntpfDAO clntpfDao = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	protected static final String e040 = "E040";
	//ILIFE-7688 end
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		continue2620, 
		updateErrorIndicators2670, 
		switch4170, 
		addSubfileRecord4120, 
		updateSubfile4670
	}

	public P5016() {
		super();
		screenVars = sv;
		new ScreenModel("S5016", AppVars.getInstance(), sv);
	}
	protected S5016ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5016ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void largename()
	{
		/*LGNM-100*/
		//ILIFE-7688
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltspf.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltspf.getSurname());//ILIFE-7688
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		//ILIFE-7688
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltspf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltspf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltspf.getSurname(), "  ");//ILIFE-7688
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltspf.getGivname(), "  ");//ILIFE-7688
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltspf.getSurname());//ILIFE-7688
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		//ILIFE-7688
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltspf.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltspf.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltspf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltspf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltspf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltspf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltspf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltspf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		//ILIFE-7688
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltspf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltspf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
			initialise1010();
		}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		//ILIFE-7688 start
		/*chdrclmIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}*/
				chdrpf = chdrpfDAO.getCacheObject(chdrpf);
				if(null==chdrpf) {
					chdrclmIO.setFunction(varcom.retrv);
					chdrclmIO.setFormat(formatsInner.chdrclmrec);
					SmartFileCode.execute(appVars, chdrclmIO);
					if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
						syserrrec.params.set(chdrclmIO.getParams());
						fatalError600();
					}
					else {
						chdrpf = chdrpfDAO.getChdrpf(chdrclmIO.getChdrcoy().toString(), chdrclmIO.getChdrnum().toString());
						if(null==chdrpf) {
							fatalError600();
						}
						else {
							chdrpfDAO.setCacheObject(chdrpf);
						}
					}
				}
				//ILIFE-7688 end
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(chdrpf.getCnttype().trim());/* IJTI-1523 */
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrpf.getChdrnum());/* IJTI-1523 */
		sv.cnttype.set(chdrpf.getCnttype());/* IJTI-1523 */
		sv.cownnum.set(chdrpf.getCownnum());//ILIFE-7688
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fupremk.set(SPACES);
		}
		else {
			sv.fupremk.set(descIO.getLongdesc());
		}
		//ILIFE-7688 start
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		/*cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrpf.getCownnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernameErr.set(errorsInner.e040);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}*/
		cltspf = clntpfDao.searchClntRecord("CN", wsspcomn.fsuco.toString(), chdrpf.getCownnum());/* IJTI-1523 */
		if (cltspf == null
				|| isNE(cltspf.getValidflag(), 1)) {
					sv.ownernameErr.set(e040);
					sv.ownername.set(SPACES);
				}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		//ILIFE-7688 end
		flupclmIO.setParams(SPACES);
		flupclmIO.setChdrcoy(chdrpf.getChdrcoy());//ILIFE-7688
		flupclmIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7688
		flupclmIO.setFupno(ZERO);
		flupclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		flupclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		flupclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		wsaaFirstFlupKey.set(flupclmIO.getDataKey());
		/*    Store the last subfile record key for later use if a         */
		/*    ROLU is pressed.                                             */
		wsaaLastFlupKey.set(flupclmIO.getDataKey());
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		/* If no records to show, we display a screen consisting of blank  */
		/* lines. To indicate if there are only blank lines, we set the    */
		/* all-blank-lines indicator to yes.                               */
		//ILIFE-7688
		if (isEQ(flupclmIO.getChdrcoy(),chdrpf.getChdrcoy())
		&& isEQ(flupclmIO.getChdrnum(),chdrpf.getChdrnum())
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			wsaaAllBlankLines = "N";
		}
		else {
			wsaaAllBlankLines = "Y";
		}
		/* We initialize the array containing the record keys of the       */
		/* subfile records.                                                */
		/* Then load the first subfile page.                               */
		wsaaKeysArray.set(SPACES);
		initialize(wsaaKeysArray);
		wsaaX.set(1);
		while ( !(isGT(wsaaX,5))) {
			roluSubfileLoad4100();
		}
		
		/*            UNTIL WSAA-X > 12.                           <V72L11>*/
		/* Set the screen to signify it is the first page of data. We can  */
		/*     then validate against a ROLD request when there are no recs */
		/*     to load.                                                    */
		/* Set the screen to signify more records can be loaded. Note that */
		/*     this may NOT really be the case, but we must trick the      */
		/*     the system to enable us to add records.                     */
		wsaaFirstPage = "Y";
		scrnparams.subfileMore.set("Y");
		initOptswch1200();
	}

protected void initOptswch1200()
	{
		start1210();
	}

protected void start1210()
	{
		optswchrec.optsFunction.set(varcom.init);
		optswchrec.optsLanguage.set(wsspcomn.language);
		optswchrec.optsCompany.set(wsspcomn.company);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(datcon1rec.intDate);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.params.set(optswchrec.rec);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     Load subfile                                                
	* </pre>
	*/
protected void loadSubfile1100()
	{
					loadSubfile1110();
					addSubfileRecord1120();
				}

protected void loadSubfile1110()
	{
		wsaaX.add(1);
		//ILIFE-7688
		if (isNE(flupclmIO.getChdrcoy(),chdrpf.getChdrcoy())
		|| isNE(flupclmIO.getChdrnum(),chdrpf.getChdrnum())
		|| isEQ(flupclmIO.getStatuz(),varcom.endp)) {
			sv.subfileFields.set(SPACES);
			sv.fupremdt.set(99999999);
			sv.lifeno.set(ZERO);
			sv.jlife.set(SPACES);
			sv.language.set(wsspcomn.language);
			wsaaFupno.add(1);
			sv.fupno.set(wsaaFupno);
			sv.crtdate.set(varcom.vrcmMaxDate);
			sv.fuprcvd.set(99999999);
			sv.exprdate.set(99999999);
			sv.fuptype.set("C");
			sv.fuptypOut[varcom.nd.toInt()].set("Y");
			return ;
		}
		sv.fupcdes.set(flupclmIO.getFupcode());
		sv.fupremdt.set(flupclmIO.getFupremdt());
		sv.fupno.set(flupclmIO.getFupno());
		wsaaFupno.set(flupclmIO.getFupno());
		sv.lifeno.set(flupclmIO.getLife());
		sv.jlife.set(flupclmIO.getJlife());
		sv.fupremk.set(flupclmIO.getFupremk());
		sv.fuptype.set(flupclmIO.getFuptype());
		sv.fupstat.set(flupclmIO.getFupstat());
		sv.zitem.set(flupclmIO.getFupcode());
		sv.fuprcvd.set(flupclmIO.getFuprcvd());
		if (isEQ(sv.fuprcvd,ZERO)) {
			sv.fuprcvd.set(varcom.vrcmMaxDate);
		}
		sv.exprdate.set(flupclmIO.getExprdate());
		if (isEQ(sv.exprdate,ZERO)) {
			sv.exprdate.set(varcom.vrcmMaxDate);
		}
		readFupe1600();
		if (isEQ(fupeIO.getStatuz(),varcom.oK)) {
			sv.sfflg.set("+");
			sv.indic.set("+");
		}
		else {
			sv.sfflg.set(SPACES);
			sv.indic.set(SPACES);
		}
		sv.crtuser.set(flupclmIO.getCrtuser());
		sv.language.set(wsspcomn.language);
		sv.crtdate.set(flupclmIO.getCrtdate());
		if (isEQ(flupclmIO.getCrtdate(),ZERO)) {
			sv.crtdate.set(varcom.vrcmMaxDate);
		}
		checkHxcl1400();
		flupclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		/* To protect all fields except "action" field when enquiry        */
		if (isEQ(wsspcomn.flag, "I")) {
			sv.fuprcvdOut[varcom.pr.toInt()].set("Y");
			sv.lifenoOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void addSubfileRecord1120()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkHxcl1400()
	{
			hxcl1410();
		}

protected void hxcl1410()
	{
		hxclIO.setParams(SPACES);
		hxclIO.setChdrcoy(flupclmIO.getChdrcoy());
		hxclIO.setChdrnum(flupclmIO.getChdrnum());
		hxclIO.setFupno(wsaaFupno);
		hxclIO.setHxclseqno(ZERO);
		hxclIO.setFormat(hxclrec);
		hxclIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(),varcom.oK)
		&& isNE(hxclIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		if (isEQ(hxclIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(hxclIO.getChdrcoy(),flupclmIO.getChdrcoy())
		|| isNE(hxclIO.getChdrnum(),flupclmIO.getChdrnum())
		|| isNE(hxclIO.getFupno(),flupclmIO.getFupno())) {
			return ;
		}
		sv.sfflg.set("+");
		sv.indic.set("+");
	}

	/**
	* <pre>
	* Read FUPE to determine whether any record exists                
	* </pre>
	*/
protected void readFupe1600()
	{
		readFupe1610();
	}

protected void readFupe1610()
	{
		fupeIO.setParams(SPACES);
		fupeIO.setChdrcoy(flupclmIO.getChdrcoy());
		fupeIO.setChdrnum(flupclmIO.getChdrnum());
		fupeIO.setFupno(flupclmIO.getFupno());
		fupeIO.setClamnum(SPACES);
		fupeIO.setTranno(flupclmIO.getTranno());
		fupeIO.setDocseq(ZERO);
		fupeIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		fupeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fupeIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO","FUPNO");
		fupeIO.setFormat(fuperec);
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(),varcom.oK)
		&& isNE(fupeIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(fupeIO.getStatuz());
			syserrrec.params.set(fupeIO.getParams());
			fatalError600();
		}
		if (isNE(fupeIO.getChdrcoy(),flupclmIO.getChdrcoy())
		|| isNE(fupeIO.getChdrnum(),flupclmIO.getChdrnum())
		|| isNE(fupeIO.getFupno(),flupclmIO.getFupno())
		|| isNE(fupeIO.getClamnum(),flupclmIO.getClamnum())
		|| isNE(fupeIO.getTranno(),flupclmIO.getTranno())) {
			fupeIO.setStatuz(varcom.endp);
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    IF WSSP-FLAG                = 'I'                    <V72L11>*/
		/*       MOVE PROT                TO SCRN-FUNCTION.        <V72L11>*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2050();
			checkStatuz2070();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			/* OR WSSP-FLAG                = 'I'                            */
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.rold)
		&& isEQ(wsaaFirstPage,"Y")) {
			/*     MOVE F498               TO S5016-UPRFLAG                 */
			scrnparams.errorCode.set(errorsInner.f498);
			wsspcomn.edterror.set("Y");
		}
		wsaaRolu.set("N");
		wsaaRold.set("N");
		if (isEQ(scrnparams.statuz,varcom.rold)) {
			wsaaRold.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaRolu.set("Y");
		}
	}

	/**
	* <pre>
	*    AND WSAA-NO-MORE-FLUPS      = 'Y'
	*        MOVE F499               TO S5016-UPRFLAG
	*        MOVE 'Y'                TO WSSP-EDTERROR.
	*    Validate fields
	* </pre>
	*/
protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void checkStatuz2070()
	{
		/*  Do not allow any action on the screen if an error was found    */
		/*  in the above VALIDATE section.                                 */
		if (isNE(wsspcomn.edterror,"Y")) {
			if (isEQ("Y",wsaaRolu)
			|| isEQ("Y",wsaaRold)) {
				checkRoluRold2100();
			}
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void checkRoluRold2100()
	{
					check2110();
					checkForErrors2180();
				}

protected void check2110()
	{
		if (isEQ(wsaaRold,"Y")) {
			if (isEQ(wsaaFirstPage,"Y")) {
				scrnparams.errorCode.set(errorsInner.f498);
			}
			return ;
		}
		/* If this is a ROLU.                                              */
		if (isEQ(wsspcomn.flag,"I")) {
			if (isEQ(scrnparams.subfileMore,"N")) {
				scrnparams.errorCode.set(errorsInner.f499);
			}
			return ;
		}
		for (wsaaCount.set(1); !(isGT(wsaaCount,5)); wsaaCount.add(1)){
			checkSubfile2110();
		}
	}

protected void checkForErrors2180()
	{
		if (isNE(scrnparams.errorCode,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void checkSubfile2110()
	{
		/*CHECK*/
		scrnparams.subfileRrn.set(wsaaCount);
		scrnparams.function.set(varcom.sread);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(sv.uprflag,SPACES)) {
			scrnparams.errorCode.set(errorsInner.f499);
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case continue2620: 
					continue2620();
				case updateErrorIndicators2670: 
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		/* Invalid action for in blank line                                */
		if ((isEQ(sv.fupcdes,SPACES))
		&& (isEQ(sv.fupremk,SPACES))
		&& (isEQ(sv.fupstat,SPACES))
		&& (isEQ(sv.fupremdt,ZERO)
		|| isEQ(sv.fupremdt,99999999)
		|| isEQ(sv.fupremdt,SPACES))
		&& (isEQ(sv.uprflag, " ")
		|| isEQ(sv.uprflag,"C"))
		&& isNE(sv.sfflg,SPACES)) {
			sv.sfflgErr.set(errorsInner.e005);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Only check blank line for enquiry mode                          */
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		vldt2610CustomerSpecific();
		/* Check if fields have been blanked out i.e. deleted.*/
		/*    IF  S5016-FUPCODE           = SPACES                         */
		if (isEQ(sv.fupcdes,SPACES)
		&& isEQ(sv.fupremk,SPACES)
		&& isEQ(sv.fupstat,SPACES)
		&& (isEQ(sv.fupremdt,ZERO)
		|| isEQ(sv.fupremdt,99999999)
		|| isEQ(sv.fupremdt,SPACES))
		&& isEQ(sv.lifeno,ZERO)
		&& isEQ(sv.jlife,SPACES)
		&& isEQ(sv.uprflag,"N")) {
			sv.uprflag.set("D");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Fields have been blanked out for a new record*/
		/*    IF  S5016-FUPCODE           = SPACES                         */
		if (isEQ(sv.fupcdes,SPACES)
		&& isEQ(sv.fupremk,SPACES)
		&& isEQ(sv.fupstat,SPACES)
		&& (isEQ(sv.fupremdt,ZERO)
		|| isEQ(sv.fupremdt,99999999)
		|| isEQ(sv.fupremdt,SPACES))
		&& isEQ(sv.lifeno,ZERO)
		&& isEQ(sv.jlife,SPACES)
		&& isEQ(sv.uprflag, " ")) {
			sv.uprflag.set("X");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Check follow up status.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5660);
		itemIO.setItemitem(sv.fupstat);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.fupstsErr.set(errorsInner.e558);
		}
		/* Check follow up code.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5661);
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			/*     MOVE U000                TO S5016-FUPCDE-ERR              */
			/*       MOVE E557                TO S5016-FUPCDE-ERR      <V72L11>*/
			sv.fupcdesErr.set(errorsInner.e557);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Check presence of letter type in case of printing of either     */
		/* standard follow up letter or reminder.                          */
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		if (isEQ(sv.fupstat,"L")) {
			if (isEQ(t5661rec.zlettype,SPACES)) {
				sv.fupcdesErr.set(errorsInner.hl50);
			}
		}
		if (isEQ(sv.fupstat,"M")) {
			if (isEQ(t5661rec.zlettyper,SPACES)) {
				sv.fupcdesErr.set(errorsInner.hl50);
			}
		}
		if (isNE(sv.zitem,SPACES)
		&& isNE(sv.fupcdes,SPACES)
		&& isNE(sv.fupcdes,sv.zitem)) {
			sv.fupcdesErr.set(errorsInner.w431);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* If the remark is blank then default to desc from T5661.*/
		if (isNE(sv.fupremk,SPACES)) {
			goTo(GotoLabel.continue2620);
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5661);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(wsaaT5661Key);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.fuprmkErr.set(errorsInner.e557);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		else {
			sv.fupremk.set(descIO.getLongdesc());
		}
	}

protected void vldt2610CustomerSpecific(){
	
}

protected void continue2620()
	{
		/* If the date is blank the use 'todays' date.*/
		if (isEQ(sv.fupremdt,ZERO)
		|| isEQ(sv.fupremdt,SPACES)
		|| isEQ(sv.fupremdt,99999999)) {
			if (isEQ(sv.fupstat,"L")) {
				setReminderDate6000();
			}
			else {
				sv.fupremdt.set(datcon1rec.intDate);
			}
		}
		//ILIFE-7688
		if (isLT(sv.fupremdt,chdrpf.getOccdate())) {
			sv.fupdtErr.set(errorsInner.f574);
		}
		/* Validate that the life number is not ZEROES.                    */
		if (isEQ(sv.lifeno,ZERO)) {
			sv.lifenoErr.set(errorsInner.h031);
		}
		/* Check Received Date and Expiry Date                             */
		if (isNE(sv.fuprcvd,varcom.vrcmMaxDate)
		&& isGT(sv.fuprcvd,datcon1rec.intDate)) {
			sv.fuprcvdErr.set(errorsInner.f073);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isNE(sv.exprdate,varcom.vrcmMaxDate)
		&& isLT(sv.exprdate,datcon1rec.intDate)) {
			sv.exprdateErr.set(errorsInner.g914);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.fuprcvd,varcom.vrcmMaxDate)) {
			for (ix.set(1); !(isGT(ix,10)
			|| isEQ(t5661rec.fuposs[ix.toInt()],SPACES)); ix.add(1)){
				if (isEQ(t5661rec.fuposs[ix.toInt()],sv.fupstat)) {
					sv.fuprcvdErr.set(errorsInner.e186);
				}
			}
		}
		/* Validate joint life, if entered.                                */
		if (isNE(sv.jlife,SPACES)) {
			lifeenqIO.setParams(SPACES);
			lifeenqIO.setChdrcoy(chdrpf.getChdrcoy());//ILIFE-7688
			lifeenqIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7688
			lifeenqIO.setLife(sv.lifeno);
			lifeenqIO.setJlife(sv.jlife);
			lifeenqIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, lifeenqIO);
			if (isNE(lifeenqIO.getStatuz(),varcom.oK)
			&& isNE(lifeenqIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(lifeenqIO.getParams());
				fatalError600();
			}
			if (isEQ(lifeenqIO.getStatuz(),varcom.mrnf)) {
				sv.jlifeErr.set(errorsInner.e350);
			}
		}
		/* Check option switching for follow-up extended text screen       */
		if (isNE(sv.sfflgErr,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Cannot switch to Follow-up Extended Text if no follow-up code   */
		if (isNE(sv.sfflg,SPACES)
		&& isNE(sv.sfflg,"+")
		&& isEQ(sv.fupcdes,SPACES)) {
			sv.sfflgErr.set(errorsInner.rf00);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.zitem,SPACES)) {
			if (isEQ(sv.fupcdes,SPACES)) {
				sv.sfflg.set(SPACES);
				sv.indic.set(SPACES);
			}
			else {
				if (isNE(t5661rec.messages,SPACES)) {
					sv.indic.set("+");
				}
			}
		}
		if (isEQ(sv.sfflg,SPACES)
		&& isNE(sv.indic,SPACES)) {
			sv.sfflg.set(sv.indic);
		}
		/* The subfile record has been changed so update the hidden        */
		/* flag.                                                           */
		if (isEQ(sv.uprflag,"N")) {
			sv.uprflag.set("U");
		}
		if (isEQ(sv.uprflag,SPACES)) {
			sv.crtdate.set(datcon1rec.intDate);
			sv.fuptype.set("C");
			sv.fuptypOut[varcom.nd.toInt()].set("N");
			sv.uprflag.set("A");
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		for (wsaaRrn.set(1); !(isGT(wsaaRrn,5)); wsaaRrn.add(1)){
			updateFollowUp3100();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DO THE FOLLOW UP FILE UPDATING
	* </pre>
	*/
protected void updateFollowUp3100()
	{
			updateFollowUp3110();
		}

protected void updateFollowUp3110()
	{
		/* Get the subfile record using the relative record number.*/
		scrnparams.subfileRrn.set(wsaaRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* The subfile record has not been changed.*/
		if (isEQ(sv.uprflag,SPACES)
		|| isEQ(sv.uprflag,"N")) {
			return ;
		}
		/* Set up key.*/
		flupclmIO.setParams(SPACES);
		flupclmIO.setChdrcoy(chdrpf.getChdrcoy());//ILIFE-7688
		flupclmIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7688
		flupclmIO.setFupno(sv.fupno);
		/* If the update is a delete.*/
		if (isEQ(sv.uprflag,"D")) {
			flupclmIO.setFunction(varcom.readh);
			flupFile3200();
			deleteFupe3400();
			flupclmIO.setFormat(flupclmrec);
			flupclmIO.setFunction(varcom.delet);
			flupFile3200();
		}
		/* If the update is an addition.*/
		if (isEQ(sv.uprflag,"A")) {
			flupclmIO.setFuptype("C");
			flupclmIO.setTranno(chdrpf.getTranno());//ILIFE-7688
			flupclmIO.setLife(sv.lifeno);
			if (isEQ(sv.jlife,SPACES)) {
				flupclmIO.setJlife("00");
			}
			else {
				flupclmIO.setJlife(sv.jlife);
			}
			flupclmIO.setFupcode(sv.fupcdes);
			flupclmIO.setFupstat(sv.fupstat);
			flupclmIO.setFupremdt(sv.fupremdt);
			flupclmIO.setFupremk(sv.fupremk);
			flupclmIO.setFuptype(sv.fuptype);
			flupclmIO.setTermid(varcom.vrcmTermid);
			flupclmIO.setUser(varcom.vrcmUser);
			flupclmIO.setTransactionDate(varcom.vrcmDate);
			flupclmIO.setTransactionTime(varcom.vrcmTime);
			flupclmIO.setCrtuser(sv.crtuser);
			flupclmIO.setCrtdate(sv.crtdate);
			flupclmIO.setEffdate(datcon1rec.intDate);
			flupclmIO.setFormat(flupclmrec);
			flupclmIO.setFuprcvd(sv.fuprcvd);
			flupclmIO.setExprdate(sv.exprdate);
			/*    MOVE VRCM-MAX-DATE       TO FLUPCLM-EFFDATE       <LA4592>*/
			flupclmIO.setZlstupdt(varcom.vrcmMaxDate);
			writeFupe3500();
			flupclmIO.setFunction(varcom.writr);
			flupFile3200();
		}
		/* If the update is an update.*/
		if (isEQ(sv.uprflag,"U")) {
			flupclmIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, flupclmIO);
			if (isNE(flupclmIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(flupclmIO.getParams());
				fatalError600();
			}
			flupclmIO.setFunction(varcom.readh);
			flupFile3200();
			flupclmIO.setLife(sv.lifeno);
			if (isEQ(sv.jlife,SPACES)) {
				flupclmIO.setJlife("00");
			}
			else {
				flupclmIO.setJlife(sv.jlife);
			}
			flupclmIO.setFupcode(sv.fupcdes);
			flupclmIO.setFupstat(sv.fupstat);
			flupclmIO.setFupremdt(sv.fupremdt);
			flupclmIO.setFupremk(sv.fupremk);
			flupclmIO.setFuprcvd(sv.fuprcvd);
			flupclmIO.setExprdate(sv.exprdate);
			flupclmIO.setFuptype(sv.fuptype);
			flupclmIO.setTermid(varcom.vrcmTermid);
			flupclmIO.setUser(varcom.vrcmUser);
			flupclmIO.setTransactionDate(varcom.vrcmDate);
			flupclmIO.setTransactionTime(varcom.vrcmTime);
			flupclmIO.setLstupuser(wsspcomn.userid);
			flupclmIO.setZlstupdt(datcon1rec.intDate);
			flupclmIO.setFormat(flupclmrec);
			flupclmIO.setFunction(varcom.rewrt);
			flupFile3200();
		}
		/* Update the array containing the record keys of the subfile      */
		/* records.                                                        */
		if (isEQ(sv.uprflag,"A")){
			wsaaRecordKeyChdrcoy[wsaaRrn.toInt()].set(flupclmIO.getChdrcoy());
			wsaaRecordKeyChdrnum[wsaaRrn.toInt()].set(flupclmIO.getChdrnum());
			wsaaRecordKeyFupno[wsaaRrn.toInt()].set(flupclmIO.getFupno());
		}
		else if (isEQ(sv.uprflag,"D")){
			wsaaRecordKey[wsaaRrn.toInt()].set(SPACES);
			initialize(wsaaRecordKey[wsaaRrn.toInt()]);
		}
	}

	/**
	* <pre>
	*     ACCESS THE FLUPCLM FILE
	* </pre>
	*/
protected void flupFile3200()
	{
		/*FLUP-FILE*/
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(flupclmIO.getStatuz());
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void deleteFupe3400()
	{
		start3410();
		callFupeio3420();
	}

protected void start3410()
	{
		fupeIO.setParams(SPACES);
		fupeIO.setStatuz(varcom.oK);
		fupeIO.setChdrcoy(flupclmIO.getChdrcoy());
		fupeIO.setChdrnum(flupclmIO.getChdrnum());
		fupeIO.setFupno(sv.fupno);
		fupeIO.setClamnum(SPACES);
		fupeIO.setTranno(flupclmIO.getTranno());
		fupeIO.setDocseq(ZERO);
		fupeIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		fupeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fupeIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO","FUPNO");
		fupeIO.setFormat(fuperec);
	}

protected void callFupeio3420()
	{
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(),varcom.oK)
		&& isNE(fupeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(fupeIO.getParams());
			syserrrec.statuz.set(fupeIO.getStatuz());
			fatalError600();
		}
		if (isEQ(fupeIO.getStatuz(),varcom.endp)
		|| isNE(fupeIO.getChdrcoy(),flupclmIO.getChdrcoy())
		|| isNE(fupeIO.getChdrnum(),flupclmIO.getChdrnum())
		|| isNE(fupeIO.getFupno(),sv.fupno)
		|| isNE(fupeIO.getClamnum(),flupclmIO.getClamnum())
		|| isNE(fupeIO.getTranno(),flupclmIO.getTranno())) {
			return ;
		}
		fupeIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fupeIO.getParams());
			syserrrec.statuz.set(fupeIO.getStatuz());
			fatalError600();
		}
		fupeIO.setFunction(varcom.nextr);
		callFupeio3420();
		return ;
	}

protected void writeFupe3500()
	{
		start3510();
		nextExtendedText3530();
	}

protected void start3510()
	{
		wsaaDocseq.set(ZERO);
		itemIO.setParams(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5661);
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		iy.set(ZERO);
		for (ix.set(5); !(isEQ(ix,ZERO)
		|| isNE(iy,ZERO)); ix.add(-1)){
			if (isNE(t5661rec.message[ix.toInt()],SPACES)) {
				iy.set(ix);
			}
		}
		ix.set(ZERO);
	}

protected void nextExtendedText3530()
	{
		ix.add(1);
		if (isGT(ix,5)
		|| isGT(ix,iy)) {
			return ;
		}
		wsaaDocseq.add(1);
		fupeIO.setParams(SPACES);
		fupeIO.setStatuz(varcom.oK);
		fupeIO.setChdrcoy(flupclmIO.getChdrcoy());
		fupeIO.setChdrnum(flupclmIO.getChdrnum());
		fupeIO.setFupno(sv.fupno);
		fupeIO.setClamnum(SPACES);
		fupeIO.setTranno(flupclmIO.getTranno());
		fupeIO.setDocseq(wsaaDocseq);
		fupeIO.setMessage(t5661rec.message[ix.toInt()]);
		fupeIO.setFunction(varcom.writr);
		fupeIO.setFormat(fuperec);
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fupeIO.getParams());
			syserrrec.statuz.set(fupeIO.getStatuz());
			fatalError600();
		}
		nextExtendedText3530();
		return ;
	}

protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		/* IF ROLLUP                                                    */
		/*    MOVE 'N'                 TO WSAA-FIRST-PAGE               */
		/*    IF FLUPCLM-CHDRCOY       = CHDRCLM-CHDRCOY                */
		/*    OR FLUPCLM-CHDRNUM       = CHDRCLM-CHDRNUM                */
		/*    OR FLUPCLM-STATUZ        NOT = ENDP                       */
		/*       PERFORM 5000-CLEAR-SUBFILE                             */
		/*       PERFORM 4100-ROLU-SUBFILE-LOAD 12 TIMES.               */
		/* IF ROLLDOWN                                                  */
		/*    MOVE SPACES              TO FLUPCLM-PARAMS                */
		/*    MOVE WSAA-FIRST-FLUP-KEY TO FLUPCLM-DATA-KEY              */
		/*    MOVE BEGN                TO FLUPCLM-FUNCTION              */
		/*    CALL 'FLUPCLMIO'         USING FLUPCLM-PARAMS             */
		/*    IF FLUPCLM-STATUZ        NOT = O-K                        */
		/*                             AND NOT = ENDP                   */
		/*       MOVE FLUPCLM-PARAMS   TO SYSR-PARAMS                   */
		/*       PERFORM 600-FATAL-ERROR                                */
		/*    ELSE                                                      */
		/*       PERFORM 5000-CLEAR-SUBFILE                             */
		/*       MOVE 12                  TO WSAA-RRN                   */
		/*                                   SCRN-SUBFILE-RRN           */
		/*       PERFORM 4200-ROLD-SUBFILE-LOAD                         */
		/*               UNTIL FLUPCLM-STATUZ      = ENDP               */
		/*               OR FLUPCLM-CHDRCOY NOT = CHDRCLM-CHDRCOY       */
		/*               OR FLUPCLM-CHDRNUM NOT = CHDRCLM-CHDRNUM       */
		/*               OR WSAA-RRN            = 0.                    */
		/* If roll up requested, we locate the key of the first record     */
		/*     on the screen.                                              */
		/*     If there are any records on the screen, we update the       */
		/*     first-subfile record key.                                   */
		/*     We locate the key of the last record on the screen.         */
		/*     If there are any records on the screen, we update the       */
		/*     last-subfile record key.                                    */
		/*     If it is the first page and the screen is empty, we do      */
		/*     not permit a rollup. Therefore we only set first-page to    */
		/*     to 'N' if there are any records on the screen.              */
		/*     We position the file on the last-subfile-record key.        */
		/*     Then we read the next record in the file.                   */
		/*     If no records to show, we display a screen consisting of    */
		/*     blank lines. To indicate if there are only blank lines, we  */
		/*     set the all-blank-lines indicator to yes.                   */
		/*     If there are records to show, we store the first subfile    */
		/*     record key for later use if a roll down is pressed.         */
		/*     We clear the subfile, initialize the array containing the   */
		/*     record keys of the subfile records and load a new page of   */
		/*     the subfile.                                                */
		if (rollup.isTrue()) {
			rollup4400();
		}
		/* If roll down requested move the key of the first record in the  */
		/*     subfile page to the key of the file and BEGN in order       */
		/*     to position the file correctly.                             */
		/*     If the last screen shown consisted of blank lines only,     */
		/*     we do not need to read backwards through the file, because  */
		/*     we still have the correct key for the first record in the   */
		/*     subfile page.                                               */
		/*     Otherwise we will read backwards through the file in order  */
		/*     to find the previous 12 records.                            */
		/*     Then we have to determine whether or not we are displaying  */
		/*     first page.                                                 */
		/*     We clear the subfile, initialize the array containing the   */
		/*     record keys of the subfile records and load a new page of   */
		/*     the subfile.                                                */
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.function.set("HIDEW");
			return ;
		}
		if (rolldown.isTrue()) {
			rolldown4500();
		}
		if (rollup.isTrue()
		|| rolldown.isTrue()) {
			wsaaRolu.set("N");
			wsaaRold.set("N");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.nextprog.set("P5016");
			scrnparams.function.set(varcom.sstrt);
			a100CallS5016io();
			optswchCall4100();
		}
	}

protected void optswchCall4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call4110();
					lineSwitching4120();
				case switch4170: 
					switch4170();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call4110()
	{
		wsspcomn.nextprog.set(wsaaProg);
	}

	/**
	* <pre>
	* Check for Line Switching                                        
	* </pre>
	*/
protected void lineSwitching4120()
	{
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			scrnparams.subfileRrn.set(ZERO);
		}
		else {
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isNE(sv.sfflg,SPACES)
		|| isEQ(scrnparams.statuz,varcom.endp))) {
			if (isNE(sv.fupno,SPACES)
			&& isNE(sv.fupcdes,SPACES)) {
				fupeIO.setParams(SPACES);
				fupeIO.setChdrcoy(flupclmIO.getChdrcoy());
				fupeIO.setChdrnum(flupclmIO.getChdrnum());
				fupeIO.setFupno(sv.fupno);
				fupeIO.setClamnum(SPACES);
				fupeIO.setTranno(ZERO);
				fupeIO.setDocseq(ZERO);
				fupeIO.setFunction(varcom.begn);
				//performance improvement --  Niharika Modi 
				fupeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				fupeIO.setFitKeysSearch("CHDRCOY","CHDRNUM","FUPNO");
				fupeIO.setFormat(fuperec);
				SmartFileCode.execute(appVars, fupeIO);
				if (isNE(fupeIO.getStatuz(),varcom.oK)
				&& isNE(fupeIO.getStatuz(),varcom.endp)) {
					syserrrec.statuz.set(fupeIO.getStatuz());
					syserrrec.params.set(fupeIO.getParams());
					fatalError600();
				}
				if (isNE(fupeIO.getChdrcoy(),flupclmIO.getChdrcoy())
				|| isNE(fupeIO.getChdrnum(),flupclmIO.getChdrnum())
				|| isNE(fupeIO.getFupno(),sv.fupno)) {
					fupeIO.setStatuz(varcom.endp);
				}
				if (isEQ(fupeIO.getStatuz(),varcom.endp)) {
					sv.sfflg.set(SPACES);
					sv.indic.set(SPACES);
				}
				else {
					sv.sfflg.set("+");
					sv.indic.set("+");
				}
				checkHxcl1400();
			}
			sv.zitem.set(sv.fupcdes);
			scrnparams.function.set(varcom.supd);
			processScreen("S5016", sv);
			if (isNE(scrnparams.statuz,varcom.oK)
			&& isNE(scrnparams.statuz,varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("S5016", sv);
			if (isNE(scrnparams.statuz,varcom.oK)
			&& isNE(scrnparams.statuz,varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.switch4170);
		}
		/* ...if found, "select" the subfile line to switch to PopUp       */
		/* program.                                                        */
		/* READR and KEEPS the FLUP record for the next program            */
		keepsFlupclm4700();
		keepsChdrlnb4800();
		wsaaRrn.set(scrnparams.subfileRrn);
		optswchrec.optsSelType.set("L");
		if (isEQ(sv.sfflg,"1")
		|| isEQ(sv.sfflg,"2")) {
			optswchrec.optsSelOptno.set(sv.sfflg);
		}
		optswchrec.optsSelCode.set(SPACES);
		/* Update the subfile line.                                        */
		sv.sfflg.set(SPACES);
		sv.zitem.set(sv.fupcdes);
		sv.uprflag.set("N");
		scrnparams.function.set(varcom.supd);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void switch4170()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.params.set(optswchrec.rec);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.oK)) {
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void roluSubfileLoad4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					roluSubfileLoad4110();
					rolu4115();
				case addSubfileRecord4120: 
					addSubfileRecord4120();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void roluSubfileLoad4110()
	{
	//ILIFE-7688
		if (isNE(flupclmIO.getChdrcoy(),chdrpf.getChdrcoy())
		|| isNE(flupclmIO.getChdrnum(),chdrpf.getChdrnum())
		|| isEQ(flupclmIO.getStatuz(),varcom.endp)) {
			sv.subfileFields.set(SPACES);
			sv.fupremdt.set(99999999);
			sv.lifeno.set(ZERO);
			sv.jlife.set(SPACES);
			sv.language.set(wsspcomn.language);
			wsaaFupno.add(1);
			sv.fupno.set(wsaaFupno);
			sv.zitem.set(SPACES);
			sv.sfflg.set(SPACES);
			sv.indic.set(SPACES);
			sv.crtuser.set(wsspcomn.userid);
			sv.crtdate.set(varcom.vrcmMaxDate);
			sv.fuptype.set("C");
			sv.fuprcvd.set(99999999);
			sv.exprdate.set(99999999);
			sv.fuptypOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.addSubfileRecord4120);
		}
	}

protected void rolu4115()
	{
		sv.fupcdes.set(flupclmIO.getFupcode());
		sv.fupremdt.set(flupclmIO.getFupremdt());
		sv.fupno.set(flupclmIO.getFupno());
		wsaaFupno.set(flupclmIO.getFupno());
		sv.lifeno.set(flupclmIO.getLife());
		sv.jlife.set(flupclmIO.getJlife());
		sv.fupremk.set(flupclmIO.getFupremk());
		sv.fupstat.set(flupclmIO.getFupstat());
		sv.uprflag.set("N");
		sv.zitem.set(flupclmIO.getFupcode());
		if (isNE(flupclmIO.getFuprcvd(),NUMERIC)) {
			sv.fuprcvd.set(varcom.vrcmMaxDate);
		}
		else {
			sv.fuprcvd.set(flupclmIO.getFuprcvd());
		}
		if (isNE(flupclmIO.getExprdate(),NUMERIC)) {
			sv.exprdate.set(varcom.vrcmMaxDate);
		}
		else {
			sv.exprdate.set(flupclmIO.getExprdate());
		}
		readFupe1600();
		if (isEQ(fupeIO.getStatuz(),varcom.oK)) {
			sv.sfflg.set("+");
			sv.indic.set("+");
		}
		else {
			sv.sfflg.set(SPACES);
			sv.indic.set(SPACES);
		}
		sv.crtuser.set(flupclmIO.getCrtuser());
		sv.language.set(wsspcomn.language);
		if (isNE(flupclmIO.getCrtdate(),NUMERIC)) {
			sv.crtdate.set(varcom.vrcmMaxDate);
		}
		else {
			sv.crtdate.set(flupclmIO.getCrtdate());
		}
		sv.fuptype.set(flupclmIO.getFuptype());
		/* Update the array containing the record keys of the subfile      */
		/* records.                                                        */
		wsaaRecordKeyChdrcoy[wsaaX.toInt()].set(flupclmIO.getChdrcoy());
		wsaaRecordKeyChdrnum[wsaaX.toInt()].set(flupclmIO.getChdrnum());
		wsaaRecordKeyFupno[wsaaX.toInt()].set(flupclmIO.getFupno());
		/*    Store the last subfile record key for later use if a         */
		/*    ROLU is pressed.                                             */
		wsaaLastFlupKey.set(flupclmIO.getDataKey());
		flupclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
	}

protected void addSubfileRecord4120()
	{
		/*  MOVE 'Y'                    TO S5016-FUPRMK-OUT(PR)  <LA4592>*/
		if (isEQ(wsspcomn.flag,"I")) {
			sv.fupcdesOut[varcom.pr.toInt()].set("Y");
			sv.fuprcvdOut[varcom.pr.toInt()].set("Y");
			sv.lifenoOut[varcom.pr.toInt()].set("Y");
		}
		addSfl4120CustomerSpecific();
		scrnparams.function.set(varcom.sadd);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaX.add(1);
		/*EXIT*/
	}

protected void addSfl4120CustomerSpecific(){
	
}

protected void roldSubfileLoad4200()
	{
		/*ROLD-SUBFILE-LOAD*/
		flupclmIO.setFunction(varcom.nextp);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		//ILIFE-7688
		if (isEQ(flupclmIO.getChdrcoy(),chdrpf.getChdrcoy())
		&& isEQ(flupclmIO.getChdrnum(),chdrpf.getChdrnum())
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			wsaaFirstFlupKey.set(flupclmIO.getDataKey());
		}
		wsaaX.add(1);
		/*EXIT*/
	}

protected void checkFirstPage4300()
	{
		checkFirstPage4310();
	}

protected void checkFirstPage4310()
	{
		if (isNE(flupclmIO.getStatuz(),varcom.endp)) {
			flupclmIO.setFunction(varcom.nextp);
			SmartFileCode.execute(appVars, flupclmIO);
			if (isNE(flupclmIO.getStatuz(),varcom.oK)
			&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(flupclmIO.getParams());
				fatalError600();
			}
		}
		if (isNE(flupclmIO.getChdrcoy(),chdrpf.getChdrcoy())
		|| isNE(flupclmIO.getChdrnum(),chdrpf.getChdrnum())
		|| isEQ(flupclmIO.getStatuz(),varcom.endp)) {
			wsaaFirstPage = "Y";
		}
		flupclmIO.setParams(SPACES);
		flupclmIO.setDataKey(wsaaFirstFlupKey);
		flupclmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
	}

protected void rollup4400()
	{
		start4400();
	}

protected void start4400()
	{
		wsaaX.set(1);
		while ( !(isGT(wsaaX,5)
		|| isNE(wsaaRecordKeyChdrcoy[wsaaX.toInt()],SPACES))) {
			wsaaX.add(1);
		}
		
		if (isLTE(wsaaX,5)) {
			wsaaFirstFlupKey.set(wsaaRecordKey[wsaaX.toInt()]);
		}
		wsaaX.set(5);
		while ( !(isLT(wsaaX,1)
		|| isNE(wsaaRecordKeyChdrcoy[wsaaX.toInt()],SPACES))) {
			wsaaX.subtract(1);
		}
		
		if (isGTE(wsaaX,1)) {
			wsaaLastFlupKey.set(wsaaRecordKey[wsaaX.toInt()]);
		}
		if (isEQ(wsaaFirstPage,"Y")
		&& isGTE(wsaaX,1)) {
			wsaaFirstPage = "N";
		}
		flupclmIO.setParams(SPACES);
		flupclmIO.setDataKey(wsaaLastFlupKey);
		flupclmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		//ILIFE-7688
		if (isEQ(flupclmIO.getChdrcoy(),chdrpf.getChdrcoy())
		&& isEQ(flupclmIO.getChdrnum(),chdrpf.getChdrnum())
		&& isEQ(flupclmIO.getFupno(),wsaaLastFlupKeyFupno)
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			flupclmIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, flupclmIO);
			if (isNE(flupclmIO.getStatuz(),varcom.oK)
			&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(flupclmIO.getParams());
				fatalError600();
			}
		}
		if (isEQ(flupclmIO.getChdrcoy(),chdrpf.getChdrcoy())
		&& isEQ(flupclmIO.getChdrnum(),chdrpf.getChdrnum())
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			wsaaAllBlankLines = "N";
			wsaaFirstFlupKey.set(flupclmIO.getDataKey());
		}
		else {
			wsaaAllBlankLines = "Y";
		}
		clearSubfile5000();
		wsaaKeysArray.set(SPACES);
		initialize(wsaaKeysArray);
		wsaaX.set(1);
		while ( !(isGT(wsaaX,5))) {
			roluSubfileLoad4100();
		}
		
	}

protected void rolldown4500()
	{
		start4500();
	}

protected void start4500()
	{
		flupclmIO.setParams(SPACES);
		flupclmIO.setDataKey(wsaaFirstFlupKey);
		flupclmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)
		&& isNE(flupclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		if (isEQ(wsaaAllBlankLines,"Y")) {
			wsaaAllBlankLines = "N";
		}
		else {
			wsaaX.set(1);
			roldSubfileLoad4200();
			//ILIFE-7688
			while ( !(isEQ(flupclmIO.getStatuz(),varcom.endp)
			|| isNE(flupclmIO.getChdrcoy(),chdrpf.getChdrcoy())
			|| isNE(flupclmIO.getChdrnum(),chdrpf.getChdrnum())
			|| isGT(wsaaX,5))) {
				roldSubfileLoad4200();
			}
			
		}
		checkFirstPage4300();
		clearSubfile5000();
		wsaaKeysArray.set(SPACES);
		initialize(wsaaKeysArray);
		wsaaX.set(1);
		while ( !(isGT(wsaaX,5))) {
			roluSubfileLoad4100();
		}
		
	}

protected void optswchCall4600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start4610();
				case updateSubfile4670: 
					updateSubfile4670();
					readNextModifiedRecord4680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start4610()
	{
		if (isEQ(sv.sfflg,SPACES)
		|| isEQ(sv.sfflg,"+")
		|| isEQ(sv.sfflg,"?")) {
			goTo(GotoLabel.updateSubfile4670);
		}
		optswchrec.optsInd[1].set(sv.sfflg);
		wsaaCheckBox.set("Y");
	}

protected void updateSubfile4670()
	{
		if (isEQ(sv.sfflg,"?")) {
			readFupe1600();
			if (isEQ(fupeIO.getStatuz(),varcom.endp)) {
				sv.sfflg.set(SPACES);
				sv.indic.set(SPACES);
			}
			else {
				sv.sfflg.set("+");
				sv.indic.set("+");
			}
		}
		if (isNE(sv.sfflg,SPACES)
		&& isNE(sv.sfflg,"+")
		&& isNE(sv.sfflg,"?")) {
			sv.sfflg.set("?");
		}
		sv.zitem.set(sv.fupcdes);
		if (isNE(sv.uprflag, " ")
		&& isNE(sv.fupcdes, " ")) {
			sv.uprflag.set("N");
		}
		scrnparams.function.set(varcom.supd);
		a100CallS5016io();
	}

protected void readNextModifiedRecord4680()
	{
		scrnparams.function.set(varcom.srdn);
		a100CallS5016io();
		/*EXIT*/
	}

protected void keepsFlupclm4700()
	{
		start4710();
	}

protected void start4710()
	{
		flupclmIO.setParams(SPACES);
		flupclmIO.setStatuz(varcom.oK);
		flupclmIO.setChdrcoy(chdrpf.getChdrcoy());//ILIFE-7688
		flupclmIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7688
		flupclmIO.setFupno(sv.fupno);
		flupclmIO.setFormat(flupclmrec);
		flupclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		flupclmIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
	}

protected void keepsChdrlnb4800()
	{
		start4810();
	}

protected void start4810()
	{
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setStatuz(varcom.oK);
		chdrlnbIO.setChdrcoy(chdrpf.getChdrcoy());//ILIFE-7688
		chdrlnbIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7688
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void a100CallS5016io()
	{
		/*A110-START*/
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*A190-EXIT*/
	}

protected void clearSubfile5000()
	{
		/*CLEAR-SUBFILE*/
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5016", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

protected void setReminderDate6000()
	{
		setRemdt6010();
	}

protected void setRemdt6010()
	{
		/*  Calculate the date of issuing reminder letter                  */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5661);
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcdes);
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(datcon1rec.intDate);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(t5661rec.zelpdays);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		sv.fupremdt.set(datcon2rec.intDate2);
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e005 = new FixedLengthStringData(4).init("E005");
	private FixedLengthStringData e040 = new FixedLengthStringData(4).init("E040");
	private FixedLengthStringData f498 = new FixedLengthStringData(4).init("F498");
	private FixedLengthStringData f499 = new FixedLengthStringData(4).init("F499");
	private FixedLengthStringData e558 = new FixedLengthStringData(4).init("E558");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	private FixedLengthStringData h031 = new FixedLengthStringData(4).init("H031");
	private FixedLengthStringData e350 = new FixedLengthStringData(4).init("E350");
	private FixedLengthStringData f574 = new FixedLengthStringData(4).init("F574");
	private FixedLengthStringData hl50 = new FixedLengthStringData(4).init("HL50");
	private FixedLengthStringData rf00 = new FixedLengthStringData(4).init("RF00");
	private FixedLengthStringData w431 = new FixedLengthStringData(4).init("W431");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData g914 = new FixedLengthStringData(4).init("G914");
}
//ILIFE-7688 start
private static final class FormatsInner {
	/* FORMATS */
	private FixedLengthStringData chdrclmrec = new FixedLengthStringData(10).init("CHDRCLMREC");//ILB-459
}
//ILIFE-7688 end
}

