package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Ptshpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface PtshpfDAO extends BaseDAO<Ptshpf>{
	
	public List<Ptshpf> serachPtshRecord(Ptshpf ptshObj);
	public void insertPtshRecord(Ptshpf ptshpf);//IBL-462
}
