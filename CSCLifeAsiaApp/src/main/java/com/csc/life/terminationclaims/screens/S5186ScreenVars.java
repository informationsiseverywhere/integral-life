package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5186
 * @version 1.0 generated on 30/08/09 06:37
 * @author Quipoz
 */
public class S5186ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(425);
	public FixedLengthStringData dataFields = new FixedLengthStringData(201).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData entity = DD.entity.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData jlifeno = DD.jlifeno.copy().isAPartOf(dataFields,117);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,172);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,180);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,184);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,198);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 201);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData entityErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifenoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 257);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] entityOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifenoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	//ILIFE-1530 STARTS
	//ILIFE-1138 STARTS
	public FixedLengthStringData subfileArea = new FixedLengthStringData(314);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(104).isAPartOf(subfileArea, 0);
	//ILIFE-1138 ENDS
	public FixedLengthStringData asterisk = DD.asterisk.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData elemdesc = DD.compdesc.copy().isAPartOf(subfileFields,3);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData hrgpynum = DD.hrgpynum.copy().isAPartOf(subfileFields,60);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(subfileFields,65);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,67);
	public ZonedDecimalData plansuff = DD.plansuff.copyToZonedDecimal().isAPartOf(subfileFields,69);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,71);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,73);
	public FixedLengthStringData shortdesc = DD.shrtdesc.copy().isAPartOf(subfileFields,74);
	//ILIFE-1138 STARTS
	public FixedLengthStringData covrriskstat = DD.shrtdesc.copy().isAPartOf(subfileFields,84);
	public FixedLengthStringData covrpremstat = DD.shrtdesc.copy().isAPartOf(subfileFields,94);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(52).isAPartOf(subfileArea, 104);
	//ILIFE-1138 ENDS
	public FixedLengthStringData asteriskErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData elemdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hrgpynumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData plansuffErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData shrtdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	//ILIFE-1138 STARTS
	public FixedLengthStringData covrriskstatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData covrpremstatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(156).isAPartOf(subfileArea, 156);
	//ILIFE-1138 ENDS
	public FixedLengthStringData[] asteriskOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] elemdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hrgpynumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] plansuffOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] shrtdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	//ILIFE-1138 STARTS
	public FixedLengthStringData[] covrriskstatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] covrpremstatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 312);
	//ILIFE-1138 ENDS
	//ILIFE-1530 ENDS
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5186screensflWritten = new LongData(0);
	public LongData S5186screenctlWritten = new LongData(0);
	public LongData S5186screenWritten = new LongData(0);
	public LongData S5186protectWritten = new LongData(0);
	public GeneralTable s5186screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5186screensfl;
	}

	public S5186ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"02","04","-02","03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(coverageOut,new String[] {null, null, null, "06",null, null, null, null, null, null, null, null});
		fieldIndMap.put(riderOut,new String[] {null, null, null, "07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(plansuffOut,new String[] {null, null, null, "08",null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrstatusOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(premstatusOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(entityOut,new String[] {null, null, "11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, null, "11",null, null, null, null, null, null, null, null});
		//ILIFE-1138 STARTS
		screenSflFields = new BaseData[] {hrgpynum, hcrtable, select, asterisk, life, jlife, coverage, rider, plansuff, shortdesc, elemdesc, covrriskstat, covrpremstat};
		screenSflOutFields = new BaseData[][] {hrgpynumOut, hcrtableOut, selectOut, asteriskOut, lifeOut, jlifeOut, coverageOut, riderOut, plansuffOut, shrtdescOut, elemdescOut,covrriskstatOut,covrpremstatOut};
		screenSflErrFields = new BaseData[] {hrgpynumErr, hcrtableErr, selectErr, asteriskErr, lifeErr, jlifeErr, coverageErr, riderErr, plansuffErr, shrtdescErr, elemdescErr,covrriskstatErr,covrpremstatErr};
		//ILIFE-1138 ENDS
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlifeno, jlifename, numpols, entity, planSuffix};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenoOut, jlifenameOut, numpolsOut, entityOut, plnsfxOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenoErr, jlifenameErr, numpolsErr, entityErr, plnsfxErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5186screen.class;
		screenSflRecord = S5186screensfl.class;
		screenCtlRecord = S5186screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5186protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5186screenctl.lrec.pageSubfile);
	}
}
