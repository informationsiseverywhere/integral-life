package com.csc.life.terminationclaims.dataaccess.dao;

import com.csc.life.terminationclaims.dataaccess.model.Clmhpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ClmhpfDAO extends BaseDAO<Clmhpf> {

	public Clmhpf getClmhpfH(String chdrCoy, String chdrNum);
	public int updateClmhpfValidFlag(long uniqueNumber, String validFlag);
	public void deleteRcdByChdrnum(String chdrcoy,String chdrnum);
	public int updateClaimStatus(String chdrcoy,String chdrnum,String clamstat);
}
