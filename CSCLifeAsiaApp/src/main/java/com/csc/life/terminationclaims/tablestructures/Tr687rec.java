package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:21
 * Description:
 * Copybook name: TR687REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr687rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr687Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData aad = new ZonedDecimalData(10, 0).isAPartOf(tr687Rec, 0);
  	public FixedLengthStringData amtlifes = new FixedLengthStringData(60).isAPartOf(tr687Rec, 10);
  	public ZonedDecimalData[] amtlife = ZDArrayPartOfStructure(6, 10, 0, amtlifes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(amtlifes, 0, FILLER_REDEFINE);
  	public ZonedDecimalData amtlife01 = new ZonedDecimalData(10, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData amtlife02 = new ZonedDecimalData(10, 0).isAPartOf(filler, 10);
  	public ZonedDecimalData amtlife03 = new ZonedDecimalData(10, 0).isAPartOf(filler, 20);
  	public ZonedDecimalData amtlife04 = new ZonedDecimalData(10, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData amtlife05 = new ZonedDecimalData(10, 0).isAPartOf(filler, 40);
  	public ZonedDecimalData amtlife06 = new ZonedDecimalData(10, 0).isAPartOf(filler, 50);
  	public FixedLengthStringData amtyears = new FixedLengthStringData(60).isAPartOf(tr687Rec, 70);
  	public ZonedDecimalData[] amtyear = ZDArrayPartOfStructure(6, 10, 0, amtyears, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(amtyears, 0, FILLER_REDEFINE);
  	public ZonedDecimalData amtyear01 = new ZonedDecimalData(10, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData amtyear02 = new ZonedDecimalData(10, 0).isAPartOf(filler1, 10);
  	public ZonedDecimalData amtyear03 = new ZonedDecimalData(10, 0).isAPartOf(filler1, 20);
  	public ZonedDecimalData amtyear04 = new ZonedDecimalData(10, 0).isAPartOf(filler1, 30);
  	public ZonedDecimalData amtyear05 = new ZonedDecimalData(10, 0).isAPartOf(filler1, 40);
  	public ZonedDecimalData amtyear06 = new ZonedDecimalData(10, 0).isAPartOf(filler1, 50);
  	public FixedLengthStringData benfamts = new FixedLengthStringData(42).isAPartOf(tr687Rec, 130);
  	public ZonedDecimalData[] benfamt = ZDArrayPartOfStructure(6, 7, 0, benfamts, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(42).isAPartOf(benfamts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData benfamt01 = new ZonedDecimalData(7, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData benfamt02 = new ZonedDecimalData(7, 0).isAPartOf(filler2, 7);
  	public ZonedDecimalData benfamt03 = new ZonedDecimalData(7, 0).isAPartOf(filler2, 14);
  	public ZonedDecimalData benfamt04 = new ZonedDecimalData(7, 0).isAPartOf(filler2, 21);
  	public ZonedDecimalData benfamt05 = new ZonedDecimalData(7, 0).isAPartOf(filler2, 28);
  	public ZonedDecimalData benfamt06 = new ZonedDecimalData(7, 0).isAPartOf(filler2, 35);
  	public FixedLengthStringData contitem = new FixedLengthStringData(8).isAPartOf(tr687Rec, 172);
  	public FixedLengthStringData copays = new FixedLengthStringData(18).isAPartOf(tr687Rec, 180);
  	public ZonedDecimalData[] copay = ZDArrayPartOfStructure(6, 3, 0, copays, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(18).isAPartOf(copays, 0, FILLER_REDEFINE);
  	public ZonedDecimalData copay01 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData copay02 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 3);
  	public ZonedDecimalData copay03 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 6);
  	public ZonedDecimalData copay04 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 9);
  	public ZonedDecimalData copay05 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 12);
  	public ZonedDecimalData copay06 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 15);
  	public ZonedDecimalData factor = new ZonedDecimalData(2, 0).isAPartOf(tr687Rec, 198);
  	public FixedLengthStringData gdeducts = new FixedLengthStringData(60).isAPartOf(tr687Rec, 200);
  	public ZonedDecimalData[] gdeduct = ZDArrayPartOfStructure(6, 10, 0, gdeducts, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(60).isAPartOf(gdeducts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData gdeduct01 = new ZonedDecimalData(10, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData gdeduct02 = new ZonedDecimalData(10, 0).isAPartOf(filler4, 10);
  	public ZonedDecimalData gdeduct03 = new ZonedDecimalData(10, 0).isAPartOf(filler4, 20);
  	public ZonedDecimalData gdeduct04 = new ZonedDecimalData(10, 0).isAPartOf(filler4, 30);
  	public ZonedDecimalData gdeduct05 = new ZonedDecimalData(10, 0).isAPartOf(filler4, 40);
  	public ZonedDecimalData gdeduct06 = new ZonedDecimalData(10, 0).isAPartOf(filler4, 50);
  	public FixedLengthStringData hosbens = new FixedLengthStringData(30).isAPartOf(tr687Rec, 260);
  	public FixedLengthStringData[] hosben = FLSArrayPartOfStructure(6, 5, hosbens, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(30).isAPartOf(hosbens, 0, FILLER_REDEFINE);
  	public FixedLengthStringData hosben01 = new FixedLengthStringData(5).isAPartOf(filler5, 0);
  	public FixedLengthStringData hosben02 = new FixedLengthStringData(5).isAPartOf(filler5, 5);
  	public FixedLengthStringData hosben03 = new FixedLengthStringData(5).isAPartOf(filler5, 10);
  	public FixedLengthStringData hosben04 = new FixedLengthStringData(5).isAPartOf(filler5, 15);
  	public FixedLengthStringData hosben05 = new FixedLengthStringData(5).isAPartOf(filler5, 20);
  	public FixedLengthStringData hosben06 = new FixedLengthStringData(5).isAPartOf(filler5, 25);
  	public FixedLengthStringData nofdays = new FixedLengthStringData(18).isAPartOf(tr687Rec, 290);
  	public ZonedDecimalData[] nofday = ZDArrayPartOfStructure(6, 3, 0, nofdays, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(18).isAPartOf(nofdays, 0, FILLER_REDEFINE);
  	public ZonedDecimalData nofday01 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 0);
  	public ZonedDecimalData nofday02 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 3);
  	public ZonedDecimalData nofday03 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 6);
  	public ZonedDecimalData nofday04 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 9);
  	public ZonedDecimalData nofday05 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 12);
  	public ZonedDecimalData nofday06 = new ZonedDecimalData(3, 0).isAPartOf(filler6, 15);
  	public ZonedDecimalData premunit = new ZonedDecimalData(6, 0).isAPartOf(tr687Rec, 308);
  	public ZonedDecimalData zssi = new ZonedDecimalData(17, 0).isAPartOf(tr687Rec, 314);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(169).isAPartOf(tr687Rec, 331, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr687Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr687Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}