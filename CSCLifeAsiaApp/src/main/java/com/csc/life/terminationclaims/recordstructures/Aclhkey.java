package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:40
 * Description:
 * Copybook name: ACLHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Aclhkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData aclhFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData aclhKey = new FixedLengthStringData(256).isAPartOf(aclhFileKey, 0, REDEFINE);
  	public FixedLengthStringData aclhChdrcoy = new FixedLengthStringData(1).isAPartOf(aclhKey, 0);
  	public FixedLengthStringData aclhChdrnum = new FixedLengthStringData(8).isAPartOf(aclhKey, 1);
  	public FixedLengthStringData aclhLife = new FixedLengthStringData(2).isAPartOf(aclhKey, 9);
  	public FixedLengthStringData aclhCoverage = new FixedLengthStringData(2).isAPartOf(aclhKey, 11);
  	public FixedLengthStringData aclhRider = new FixedLengthStringData(2).isAPartOf(aclhKey, 13);
  	public PackedDecimalData aclhRgpynum = new PackedDecimalData(5, 0).isAPartOf(aclhKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(aclhKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(aclhFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		aclhFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}