/**
 * Pa511 —— Life Assured list of Claim Notification Enquiry
 */
package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import com.csc.smart400framework.dataaccess.dao.DescDAO;

import java.util.List;
import com.csc.smart400framework.dataaccess.model.Descpf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart.procedures.Genssw;

import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.life.terminationclaims.screens.Sa511ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;

import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;

import com.csc.fsu.clients.dataaccess.model.Clntpf;

import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.procedures.Optswch;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The details of  the  contract  being  enquired  upon  will be
*     stored in the  CHDRENQ  I/O  module. Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*     The PAYR file is read for the contract,followed by T3620.
*     If the contract requires bank details(i.e Direct Debit with
****  T3620-DDIND NOT = SPACES),then the Mandate file is read
****  to obtain the Mandate detalis.
*     T3620-DDIND NOT = SPACES or Credit Card with T3620-CRCIND
*     NOT = SPACES),then the Mandate file is read to obtain the
*     Mandate detalis.
*
*     Set up the  Factoring  House, (CHDR-FACTHOUS), on the screen.
*     Valid Factoring House  codes  are held on T3684. (See GETDESC
*     for an example of how to obtain the long description from the
*     DESC data-set. Do  not  call  GETDESC,  the  appropriate code
*     should be moved into the mainline.
*
*     Set up the  Bank  Code,  (CHDR-BANKKEY),  on  the  screen and
*     obtain the  corresponding  description  from the Bank Details
*     data-set BABR.  Read  BABR  with  a  key  of CHDR-BANKKEY and
*     obtain BABR-BANKDESC.
*
*     Set up the  Account  Number, (CHDR-BANKACCKEY), on the screen
*     and  obtain  the  correeponding  description  from  the  CLBL
*     data-set.  Read  CLBL   with   a   key  of  CHDR-BANKKEY  and
*     CHDR-BANKACCKEY and obtain CLBL-BANKADDDSC and CLBL-CURRCODE.
*
*     If the Assignee client, (CHDR-ASGNNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the usual way.
*
*     If the Despatch client, (CHDR-DESPNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the ususal way.
*
*
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The only validation  required  is  of  the  indicator field -
*     CONBEN. This may be space or 'X'.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was  pressed  move  spaces  to  the current program
*     position in the program stack and exit.
*
*     If returning from  processing  a  selection  further down the
*     program stack, (current  stack  action  field  will  be '*'),
*     restore the next  8  programs  from  the  WSSP and remove the
*     asterisk.
*
*     If nothing was  selected move '*' to the current stack action
*     field, add 1 to the program pointer and exit.
*
*     If a selection has  been found use GENSWCH to locate the next
*     program(s)  to process  the  selection,  (up  to  8).  Use  a
*     function of 'A'. Save  the  next  8 programs in the stack and
*     replace them with  the  ones  returned from GENSWCH. Place an
*     asterisk in the  current  stack  action  field,  add 1 to the
*     program pointer and exit.
*
*
*
*****************************************************************
* </pre>
*/
public class Pa511 extends ScreenProgCS {
	private static final Logger LOGGER = LoggerFactory.getLogger(Pa511.class);
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final String CLMPREFIX = "CLMNTF";
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA511");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);

	
	private Chdrpf chdrpf = new Chdrpf();
	
	

	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sa511ScreenVars sv = getPScreenVars() ;
	
	private Clntpf clntpf;

	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();

	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private Fluppf fluppf = null ;
	/* WSAA-MISCELLANEOUS-FLAGS */
	private ZonedDecimalData wsaaSelectedlife = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSelected = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSelectFlagLife = new FixedLengthStringData(1);
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");
	boolean rpuConfig = false;
	private static final String g931 = "G931";
	protected static final String h093 = "H093";
	protected static final String rrng = "RRNG";
	private Batckey wsaaBatchkey = new Batckey();
	private Subprogrec subprogrec = new Subprogrec();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Descpf descpf = null;
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	protected static final String chdrenqrec = "CHDRENQREC";
	protected ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	boolean rpuMetNotFund;
	int subFileCount;

	 protected static final String e186 = "E186";
	 private String notifistatus="Closed";
	 private String CLMNTF="CLMNTF";
	private String tA522="TA522";
	 String inctype="DC";
	 String inctype1="RP";
	 String rgpytype="HS";
	 private static final String CN = "CN";
	 private String expiredFlag="2";
	 private String effectiveFlag="1";
	 private Optswchrec optswchrec = new Optswchrec();
	 protected static final String tapl = "TAPL";
	 protected static final String tapm = "TAPM";
	 protected static final String tapn = "TAPN";
	 protected static final String tapo = "TAPO";

	
	public Pa511() {
		super();
		screenVars = sv;
		new ScreenModel("Sa511", AppVars.getInstance(), sv);
	}

	protected Sa511ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sa511ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
			LOGGER.info(e.getMessage());
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
			LOGGER.info(e.getMessage());
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
			try {
			initialise1010();	
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
	LOGGER.info(e.getMessage());
		}
	}

protected void initialise1010()
	{
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*      GO TO 1090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		/* Clear the WORKING STORAGE.*/
		wsaaImmexitFlag.set(SPACES);
		wsaaSelectFlag.set(SPACES);
		wsaaSelected.set(ZERO);
		wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
		/* Clear the Subfile.*/
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaMiscellaneousInner.wsaaPlansuff.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("Sa511", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
		wsaaSelectFlagLife.set(SPACES);
		scrnparams.subfileRrn.set(1);
		
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
	
		getLifeAssSection();
		//CLM001 set Life Assured List Notification
		getLifeAssList();
		if(isNE(wsaaBatckey.batcBatctrcde,tapl) && isNE(wsaaBatckey.batcBatctrcde,tapm) && isNE(wsaaBatckey.batcBatctrcde,tapn) && isNE(wsaaBatckey.batcBatctrcde,tapo)){
		init1100();
		}
	}
	

	private void getLifeAssSection() {
		try {
			sv.lifcnum.set(wsspcomn.chdrCownnum);
			clntpf = clntpfDAO.searchClntRecord(CN, wsspcomn.fsuco.toString(),wsspcomn.chdrCownnum.toString());

			if (clntpf != null) {

				String Surname = clntpf.getSurname() != null ? clntpf
						.getSurname().trim() : " ";
				String GivenName = clntpf.getGivname() != null ? clntpf
						.getGivname().trim() : " ";
				sv.lifename.set(Surname + "," + GivenName);
				// CML001 set Client Status
				sv.cltstat.set(clntpf.getCltstat() != null ? clntpf
						.getCltstat().trim() : " ");
			}
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
	}
	
		public void getLifeAssList(){
		//get Life Assured List record and display
		List<Notipf> notifiRecord = notipfDAO.getNotifiRecordByValidFlag(wsspcomn.chdrCownnum.toString(),effectiveFlag);
		if(notifiRecord != null && !notifiRecord.isEmpty() && notifiRecord.size()!=0){
			for(Notipf notipf : notifiRecord){
			sv.subfileArea.set(SPACES);
			sv.subfileFields.set(SPACES);
	//If status is closed, then radio is disabled,but we need to check if the action is enquiry
			sv.ntfstat.set(notipf.getNotifistatus());
			if(!isEQ(wsspcomn.flag, "I")){
				if(isEQ(sv.ntfstat.trim(),notifistatus )){
					sv.selectOut[varcom.pr.toInt()].set("Y");
				}else{
					sv.selectOut[varcom.pr.toInt()].set("N");
				}
			}
			//need to use longdesc
			descpf = descDAO.getdescData("IT", tA522, notipf.getIncidentt().trim(), wsspcomn.company.toString(), wsspcomn.language.toString());
			if(descpf==null){
				sv.longdesc.set(notipf.getIncidentt().trim());
			}else{
				sv.longdesc.set(descpf.getLongdesc().trim());
			}
			sv.notifnum.set(CLMPREFIX+notipf.getNotificnum());
			sv.notifcdate.set(notipf.getNotifidate());
			//currently set null value
			String datetime = notipf.getDatime().replace("-", "");
			datetime=datetime.substring(0, 8);
			sv.notiflupdate.set(datetime);

			sv.userid.set(notipf.getUsrprf());
			//addSubfile
			addToSubfile1500();
			}
		}else{
	       		scrnparams.errorCode.set(rrng);
			wsspcomn.edterror.set("Y");
		}
	}
	
	protected void addToSubfile1500()
	{
		/*ADD-LINE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("Sa511", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
		syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
	protected void init1100()
	{
		/*  Initialize the Switching Options.*/
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
	}
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* IF   WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                  */
		/*      MOVE O-K               TO WSSP-EDTERROR                 */
		/*      GO TO 2090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		scrnparams.subfileRrn.set(1);
		return ;
	}

protected void screenEdit2000()
	{

		try{
			screenIo2010();
			validate2020();
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
		
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			return;
		
		}
		wsspcomn.edterror.set(varcom.oK);
		/*    Catering for F11.                                    <V4L011>*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.edterror.set(varcom.oK);
				return;
		}
			scrnparams.subfileRrn.set(1);
			validate2020();
	}

protected void validate2020()
	{
	 if (isEQ(scrnparams.statuz, varcom.kill)) {
         return;
     }
     
     if (isEQ(scrnparams.statuz, varcom.calc)) {
         wsspcomn.edterror.set("Y");
         return;
     }
     validateSubfile2030();
 	
 	}

public void validateSubfile2030(){
	subfileStart();	
	if(isNE(wsaaSelectFlagLife,"Y")){
		wsaaSelectedlife.set(ZERO);
	}
	subFileCount=0;
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
		validateSubfile2600();
		if(rpuMetNotFund) break;
		scrnparams.function.set(varcom.supd);
		subfileIo4400();
		scrnparams.function.set(varcom.srdn);
		subfileIo4400();
	} 		
    //g931 if there is no selection
    if(!rpuMetNotFund){
    if(isNE(wsaaBatckey.batcBatctrcde,tapl) && isNE(wsaaBatckey.batcBatctrcde,tapm) && isNE(wsaaBatckey.batcBatctrcde,tapn) && isNE(wsaaBatckey.batcBatctrcde,tapo))
    	return;	
    if (isEQ(wsaaMiscellaneousInner.wsaaSelected,0) && isEQ(rpuConfig,false)) {
    	scrnparams.errorCode.set(g931);
    	wsspcomn.edterror.set("Y");
    	}	
    }
 }
     
protected void subfileIo4400() {
	processScreen("Sa511", sv);
	if (isNE(scrnparams.statuz, varcom.oK) && isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}

protected void validateSubfile2600()
{

	try{
		readNextRecord2610();		
		updateErrorIndicators2650();
	}catch(Exception e){
		e.printStackTrace();
	}

}

protected void updateErrorIndicators2650()
{
if (isNE(sv.errorSubfile,SPACES)) {
	wsspcomn.edterror.set("Y");
}
else {
	wsspcomn.edterror.set(varcom.oK);
	return;
}
	sv.select.set(SPACES);
	sv.selectOut[varcom.pr.toInt()].set("Y");		
	scrnparams.function.set(varcom.supd);
	processScreen("Sa511", sv);	
}

protected void readNextRecord2610()
{

	/* If record has space in the select AND it has already*/
	/* been processed then count it as a valid selection then*/
	/* exit. This is to force the user to only have a single*/
	/* selection given subsequent program limitations.*/
	if (isNE(sv.select,SPACES)) {
		wsaaMiscellaneousInner.wsaaSelected.add(1);
		wsspcomn.chdrChdrnum.set(sv.notifnum.toString().replace(CLMNTF,""));
		
	}
}

protected void subfileStart()	
{
	/*VALIDATE-SUBFILE*/
	scrnparams.function.set(varcom.sstrt);
	processScreen("Sa511", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateWssp3010();
		return ;	
	}

protected void updateWssp3010()
{
	wsspcomn.sbmaction.set(scrnparams.action);
	wsaaBatchkey.set(wsspcomn.batchkey);
	wsspcomn.batchkey.set(wsaaBatchkey);
	if (isEQ(scrnparams.statuz, "BACH")) {
		return;

	}
	

}

protected void keeps3070()
{
	/*   Store the contract header for use by the transaction programs*/
//ILB-459 start
	chdrpfDAO.setCacheObject(chdrpf);
	//ILB-459 end
}


	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
{
	nextProgram4010();
	}

protected void switch4020()
{
	if (isEQ(sv.select, "1")) {
		optswchrec.optsSelOptno.set(1);
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelCode.set(SPACES);
		if(isNE(sv.select, SPACES)) {
			wsspwindow.value.set(sv.notifnum);
		}else {
			wsspwindow.value.set(SPACES);
		} 
		wssplife.userArea.set(wsspwindow.toString());
	}
	programStack4300();
	if (isEQ(optswchrec.optsStatuz, varcom.endp)) {
		sv.select.set(SPACES);
		wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
		scrnparams.function.set(varcom.supd);
		processScreen("Sa511", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.sclr);
		processScreen("Sa511", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		getLifeAssList();
		wsspcomn.nextprog.set(scrnparams.scrname);
	}
	else{
		wsspcomn.programPtr.add(1);
	}
	return;
	
}

protected void programStack4300()
{
	/*STCK*/
	/* The 'STCK' function saves the original switching stack and*/
	/* replaces it with the stack specified on T1661.*/
	optswchrec.optsCallingProg.set(wsaaProg);
	optswchrec.optsCompany.set(wsspcomn.company);
	optswchrec.optsItemCompany.set(wsspcomn.company);
	optswchrec.optsLanguage.set(wsspcomn.language);
	varcom.vrcmTranid.set(wsspcomn.tranid);
	optswchrec.optsUser.set(varcom.vrcmUser);
	optswchrec.optsFunction.set("STCK");
	sv.select.set(SPACES);
	wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
	callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
	if (isNE(optswchrec.optsStatuz, varcom.oK)
	&& isNE(optswchrec.optsStatuz, varcom.endp)) {
		syserrrec.function.set("STCK");
		syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
		syserrrec.statuz.set(optswchrec.optsStatuz);
		syserrrec.iomod.set("OPTSWCH");
		fatalError600();
	}
	/*EXIT*/
}

protected void continue4080()
{
	/* Set up for execution of the component specific programs to be*/
	/* executed. Note that this includes a set of follow up records*/
	/* for each payment created according to table settings.*/
	/* Read the  Program  table T5671 for  the components programs*/
	/* to be executed next.*/
	programTables4400();
	/*UPDATE-SCREEN*/
	screenUpdate4600();
}

protected void screenUpdate4600()
{
	/*PARA*/
	scrnparams.function.set(varcom.supd);
	processScreen("Sa511", sv);		
}

protected void programTables4400()
{
		try {
			readProgramTable4410();
		}
		catch (GOTOException e){
			LOGGER.info(e.getMessage());
		}
}

protected void readProgramTable4410()
{
	/* Read the  Program  table T5671 for  the components programs to*/
	/* be executed next.*/
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
	stringVariable1.addExpression(wsaaMiscellaneousInner.wsaaCrtable);
	
	/* Reset the Action to '*' signifying action desired to the next*/
	/* program.*/
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
	wsspcomn.nextprog.set(wsaaProg);
}

protected void wayout4800()
{
	/*PROGRAM-EXIT*/
	/* If control is passed to this  part of the 4000 section on the*/
	/*   way out of the program, i.e. after  screen  I/O,  then  the*/
	/*   current stack position action  flag will be  blank.  If the*/
	/*   4000-section  is  being   performed  after  returning  from*/
	/*   processing another program then  the current stack position*/
	/*   action flag will be '*'.*/
	/* If 'KILL' has been requested, (CF11), then move spaces to the*/
	/*   current program entry in the program stack and exit.*/
	if (isEQ(scrnparams.statuz, "KILL")) {
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		return ;
	}
	/* Add 1 to the program pointer and exit.*/
	wsspcomn.programPtr.add(1);
	/*EXIT*/
}

protected void nextProgram4010()
{
	
	if (isEQ(scrnparams.statuz,"KILL")) {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsaaMiscellaneousInner.wsaaSub.set(1);
		wsspcomn.secActn[wsaaMiscellaneousInner.wsaaSub.toInt()].set(SPACES);
	
		return;
	}
	if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
		initSelection4100();
	}
	else {
		scrnparams.function.set(varcom.srdn);
	}
	if(isNE(wsaaBatckey.batcBatctrcde,tapl) && isNE(wsaaBatckey.batcBatctrcde,tapm) && isNE(wsaaBatckey.batcBatctrcde,tapn) && isNE(wsaaBatckey.batcBatctrcde,tapo)){
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACE);
		wsspcomn.nextprog.set(SPACE);
		wsspcomn.programPtr.add(1);
		nextRec4210();
		switch4020();
		return;
	}
	
	wsaaSelectFlag.set("N");
	wsaaImmexitFlag.set("N");
	while ( !(wsaaSelection.isTrue()
	|| wsaaImmExit.isTrue())) {
		next4200();
	}
	
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
		if (wsaaImmExit.isTrue()) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		return;
		}
	}

		if(isEQ(wsaaSelectFlag,"N")){
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set("PA506");
		return;
		}else{
			if (wsaaImmExit.isTrue()) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				wsspcomn.programPtr.add(1);
				scrnparams.subfileRrn.set(1);
				wsspcomn.nextprog.set(wsaaProg);
			return;
			}
		}
	continue4080();
	
}

protected void next4200()
{
	try {
		nextRec4210();
		ifSubfileEndp4220();
	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
		LOGGER.info(e.getMessage());
	}
}

protected void nextRec4210()
{
	/* Read next subfile record sequentially.*/
	processScreen("Sa511", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}

protected void ifSubfileEndp4220()
{
	/* Check for the end of the Subfile.*/
	/* If end of Subfile re-load the Saved four programs to Return*/
	/*   to initial stack order.*/
	if (isEQ(scrnparams.statuz, varcom.endp)) {
		reloadProgsWssp4230();
		return;

	}
	/* Exit if a selection has been found. NOTE that we space*/
	/* and protect the selection as the subsequent transactions*/
	/* cannot handle multiple entry.*/
	if (isNE(sv.select, SPACES)) {
		sv.select.set(SPACES);
		wsaaSelectFlagLife.set("Y");
		wsaaSelectFlag.set("Y");
	}
	scrnparams.function.set(varcom.srdn);
	return;
}


protected void reloadProgsWssp4230()
{
	/* Set Flag for immediate exit as the next action is to go to the*/
	/* next program in the stack.*/
	/* Set Subscripts and call Loop.*/
	wsaaImmexitFlag.set("Y");
	compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
	wsaaMiscellaneousInner.wsaaSub2.set(1);
	for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
		loop34240();
    }
}

/*protected void programStack4300()
	{
		STCK
		 The 'STCK' function saves the original switching stack and
		 replaces it with the stack specified on T1661.
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		sv.select.set(SPACES);
		wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)
		&& isNE(optswchrec.optsStatuz, varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		EXIT
}*/

protected void loop34240()
{
	/* Re-load the Saved four programs from Working Storage to the*/
	/* WSSP Program Stack.*/
	wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()].set(wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()]);
	wsaaMiscellaneousInner.wsaaSub1.add(1);
	wsaaMiscellaneousInner.wsaaSub2.add(1);
}

protected void initSelection4100()
{
	try {
		saveNextProgs4110();
	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
		LOGGER.info(e.getMessage());
	}
}

protected void saveNextProgs4110()
{
	/* Save the  next  four  programs  after  P  into  Working*/
	/* Storage for re-instatement at the end of the Subfile.*/
	compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
	wsaaMiscellaneousInner.wsaaSub2.set(1);
	for (int loopVar1 = 0; !(loopVar1 == 4); loopVar1 += 1){
		loop14120();
	}
	/* First Read of the Subfile for a Selection.*/
	scrnparams.function.set(varcom.sstrt);
	/* Go to the first  record  in  the Subfile to  find the First*/
	/* Selection.*/
}

protected void loop14120()
{
	/* This loop will load the next four  programs from WSSP Stack*/
	/* into a Working Storage Save area.*/
	wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()].set(wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()]);
	wsaaMiscellaneousInner.wsaaSub1.add(1);
	wsaaMiscellaneousInner.wsaaSub2.add(1);
}

protected void saveProgram4100()
{
	/*SAVE*/
	wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
} 
protected void restoreProgram4100()
{
	/*PARA*/
	wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
	sub1.add(1);
	sub2.add(1);
	/*EXIT*/
}

protected void callGenssw4300()
{
	callSubroutine4310();
}

protected void callSubroutine4310()
{
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)
	&& isNE(gensswrec.statuz, varcom.mrnf)) {
		syserrrec.params.set(gensswrec.gensswRec);
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	/* If an entry on T1675 was not found by genswch redisplay the scre*/
	/* with an error and the options and extras indicator*/
	/* with its initial load value*/
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*     MOVE V045                TO SCRN-ERROR-CODE               */
		scrnparams.errorCode.set(h093);//ILB-459
		wsspcomn.nextprog.set(scrnparams.scrname);
		return ;
	}
	/*    load from gensw to wssp*/
	compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
	wsaaY.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		loadProgram4400();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
}
protected void checkFollowups1850() {
	//ILIFE-6296 by wli31
	String chdrnum = (chdrpf.getChdrnum()!= null?chdrpf.getChdrnum(): " ");//ILB-459
	String chdrcoy = (chdrpf.getChdrcoy()!=null?chdrpf.getChdrcoy().toString():" ");//ILB-459
	boolean recFound = fluppfDAO.checkFluppfRecordByChdrnum(chdrcoy, chdrnum);

}
protected void restoreProgram4200()
{
	/*RESTORE*/
	wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
}

protected void loadProgram4400()
{
	/*RESTORE1*/
	wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT1*/
}
public Chdrpf getChdrpf() {
		return chdrpf;
	}

	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}




	protected void prepareName(String firstName, String lastName) {
		String fullName = "";
		if (isNE(firstName, SPACES)) {
			fullName = getStringUtil().plainName(firstName, lastName, ",");
			 
		} else {
			fullName = lastName;
		}
		
		wsspcomn.longconfname.set(fullName);

	}

	/**
	 * can be overriden to change the its behaviour
	 * @return
	 */
	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}
	
	public Subprogrec getSubprogrec() {
		return subprogrec;
	}
	public void setSubprogrec(Subprogrec subprogrec) {
		this.subprogrec = subprogrec;
	}
	
	/*
	 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
	 */
	private static final class WsaaMiscellaneousInner { 

			/* WSAA-MISCELLANEOUS */
		private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).init(1);
		private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
		private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
		private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(4, 0).setUnsigned();
		private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).setUnsigned();
		private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	
		private ZonedDecimalData wsaaSelected = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	}
	


}


