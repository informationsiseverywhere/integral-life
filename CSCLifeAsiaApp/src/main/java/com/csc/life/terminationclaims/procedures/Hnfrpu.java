/*
 * File: Hnfrpu.java
 * Date: 29 August 2009 22:54:10
 * Author: Quipoz Limited
 * 
 * Class transformed from HNFRPU.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuadopTableDAM;
import com.csc.life.cashdividends.recordstructures.Hdivdrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.reports.Rh547Report;
import com.csc.life.terminationclaims.tablestructures.Th546rec;
import com.csc.life.terminationclaims.tablestructures.Th548rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        RPU PROCESSING SUBROUTINE.
*
*   This is called from various programs via T6597.
*
*   This program will first calculate the Surrender Value of the
*   contract, which will include the Basic CV, the CV of PUA, any
*   accumulated dividend plus outstanding interest. After repaying
*   any outstanding loan plus any outstanding interest, the remain
*   of the Surrender Value will be used as a single premium to
*   purchase a Paid-Up coverage with a cover period same as the
*   original Basic Plan. The amount of the Coverage will be deter
*   -mined by factors like the attained age, the remaining years
*   to be covered. When called in batch mode, exception will be
*   reported in printed output.
*
*   Initialise
*     - read various tables.
*
*   Processing
*     - calculate the Surrender Value.
*     - if Surrender Value is zero, exit program with a LAPSE
*       status and write a line to the exception report.
*     - repay any outstanding loan and loan interest.
*     - if the outstanding loan plus interest is greater than
*       the Surrender Value, write a line to the exception report
*       and exit subroutine with an 'SKIP' stautuz.
*     - calculate the new issue age.
*     - calculate the new RPU SI.
*     - if it is an online quotation, exit program. Otherwise,
*       perform various accounting posting.
*
*****************************************************************
* </pre>
*/
public class Hnfrpu extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rh547Report printerFile = new Rh547Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(500);
	private ZonedDecimalData wsaaInsprm = new ZonedDecimalData(6, 0);
	private String wsaaCashDividend = "";
	private final String wsaaSubr = "HNFRPU";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGlCompany = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGlCurrency = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlMap = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaTranTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaTranUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
	private String wsaaTh546Found = "";
	private String wsaaTh546Exhausted = "";

	private FixedLengthStringData wsaaTh546Keys = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh546Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh546Keys, 0);
	private FixedLengthStringData wsaaTh546MatYr = new FixedLengthStringData(2).isAPartOf(wsaaTh546Keys, 4);
	private FixedLengthStringData wsaaTh546Mortcls = new FixedLengthStringData(1).isAPartOf(wsaaTh546Keys, 6);
	private FixedLengthStringData wsaaTh546Sex = new FixedLengthStringData(1).isAPartOf(wsaaTh546Keys, 7);

	private FixedLengthStringData wsaaT6639Item = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6639Divdmth = new FixedLengthStringData(4).isAPartOf(wsaaT6639Item, 0);
	private FixedLengthStringData wsaaT6639Pstcd = new FixedLengthStringData(2).isAPartOf(wsaaT6639Item, 4);
	private PackedDecimalData wsaaNewBonusDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0, REDEFINE).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaT5645Sub = new ZonedDecimalData(2, 0).setUnsigned();
	//private static final int wsaaT5645Size = 27;
		//ILIFE-2628 fixed--Array size increased
		private static final int wsaaT5645Size = 1000;
		/* WSAA-T5645 */
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);

	private FixedLengthStringData wsaaMaturityYr = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaMaturityYears = new ZonedDecimalData(2, 0).isAPartOf(wsaaMaturityYr, 0).setUnsigned();
	private PackedDecimalData wsaaNewPuaNo = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private String wsaaPrtOpen = "N";
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaException = new FixedLengthStringData(30);
		/* WSAA-EXCEPTION-MSG */
	private static final String nilSvMsg = "Lapsed. No surrender value.";
	private static final String refundMsg = "Refund is required.";
	private static final String lonErMsg = "Insufficient surrender value.";
	private ZonedDecimalData wsaaNextIntDate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaNextIntdate = new FixedLengthStringData(8).isAPartOf(wsaaNextIntDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNextintYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNextIntdate, 0).setUnsigned();
	private ZonedDecimalData wsaaNextintMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextIntdate, 4).setUnsigned();
	private ZonedDecimalData wsaaNextintDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextIntdate, 6).setUnsigned();
	private FixedLengthStringData wsaaLastdayOfMonths = new FixedLengthStringData(24).init("312831303130313130313031");

	private FixedLengthStringData wsaaLastddOfMonths = new FixedLengthStringData(24).isAPartOf(wsaaLastdayOfMonths, 0, REDEFINE);
	private ZonedDecimalData[] wsaaLastdd = ZDArrayPartOfStructure(12, 2, 0, wsaaLastddOfMonths, 0, UNSIGNED_TRUE);
	private ZonedDecimalData wsaaDtc3FreqFactor = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaDtc3Diff = new ZonedDecimalData(5, 5);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/* RH547-H01 */
	private FixedLengthStringData rh547h01O = new FixedLengthStringData(83);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rh547h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rh547h01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rh547h01O, 11);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rh547h01O, 41);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rh547h01O, 51);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rh547h01O, 53);
    
		/* RH547-D01 */
	private FixedLengthStringData rh547d01O = new FixedLengthStringData(95);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rh547d01O, 0);
	private FixedLengthStringData rd01Cnttype = new FixedLengthStringData(3).isAPartOf(rh547d01O, 8);
	private FixedLengthStringData rd01Cntcurr = new FixedLengthStringData(3).isAPartOf(rh547d01O, 11);
	private ZonedDecimalData rd01Surrval = new ZonedDecimalData(17, 2).isAPartOf(rh547d01O, 14);
	private ZonedDecimalData rd01Loanorigam = new ZonedDecimalData(17, 2).isAPartOf(rh547d01O, 31);
	private ZonedDecimalData rd01Refundfe = new ZonedDecimalData(17, 2).isAPartOf(rh547d01O, 48);
	private FixedLengthStringData rd01Descrip = new FixedLengthStringData(30).isAPartOf(rh547d01O, 65);
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private HdivTableDAM hdivIO = new HdivTableDAM();
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
	private HpuadopTableDAM hpuadopIO = new HpuadopTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Cashedrec cashedrec = new Cashedrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T1693rec t1693rec = new T1693rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T6598rec t6598rec = new T6598rec();
	private T6639rec t6639rec = new T6639rec();
	private Th506rec th506rec = new Th506rec();
	private Th546rec th546rec = new Th546rec();
	private Th548rec th548rec = new Th548rec();
	private Th501rec th501rec = new Th501rec();
	private Th605rec th605rec = new Th605rec();
	private Totloanrec totloanrec = new Totloanrec();
	private Hdivdrec hdivdrec = new Hdivdrec();
	private Varcom varcom = new Varcom();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaValuesInner wsaaValuesInner = new WsaaValuesInner();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ExternalisedRules er = new ExternalisedRules();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readT56451020, 
		tableLoop1030, 
		loopCheck1040, 
		puaDvdHdiv7725, 
		dividendWithdrawal7763, 
		osInt7764, 
		bookNewInterest7765, 
		datcon27769a, 
		exit7769a, 
		callAgcmio7794, 
		exit8890, 
		seExit9090, 
		dbExit9190
	}

	public Hnfrpu() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			main0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main0000()
	{
		/*START*/
		/* Main logic*/
		initialize1000();
		calSurrVal3000();
		if (isEQ(ovrduerec.statuz,varcom.oK)) {
			settleLoan5000();
			if (isEQ(ovrduerec.statuz,varcom.oK)) {
				calRpuCover7000();
				if (isEQ(ovrduerec.statuz,varcom.oK)) {
					close8900();
				}
			}
		}
		/*EXIT*/
		exitProgram();
	}

protected void initialize1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case readT56451020: 
					readT56451020();
				case tableLoop1030: 
					tableLoop1030();
				case loopCheck1040: 
					loopCheck1040();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		wsaaValuesInner.wsaaBasicCv.set(ZERO);
		wsaaValuesInner.wsaaPuaCv.set(ZERO);
		wsaaValuesInner.wsaaDvd.set(ZERO);
		wsaaValuesInner.wsaaNewDvd.set(ZERO);
		wsaaValuesInner.wsaaBasicDvd.set(ZERO);
		wsaaValuesInner.wsaaInt.set(ZERO);
		wsaaValuesInner.wsaaNewInt.set(ZERO);
		wsaaValuesInner.wsaaHdisOsInt.set(ZERO);
		wsaaValuesInner.wsaaTotSv.set(ZERO);
		wsaaValuesInner.wsaaTotLoan.set(ZERO);
		wsaaValuesInner.wsaaAccumValue.set(ZERO);
		wsaaValuesInner.wsaaCommRecovered.set(ZERO);
		wsaaValuesInner.wsaaRefund.set(ZERO);
		wsaaValuesInner.wsaaRpuCost.set(ZERO);
		wsaaValuesInner.wsaaRpuSi.set(ZERO);
		wsaaSequenceNo.set(0);
		wsaaCashDividend = "Y";
		varcom.vrcmTime.set(getCobolTime());
		wsaaBatckey.batcBatcpfx.set(smtpfxcpy.batc);
		wsaaBatckey.batcBatcbrn.set(ovrduerec.batcbrn);
		wsaaBatckey.batcBatccoy.set(ovrduerec.chdrcoy);
		wsaaBatckey.batcBatcactyr.set(ovrduerec.acctyear);
		wsaaBatckey.batcBatcactmn.set(ovrduerec.acctmonth);
		wsaaBatckey.batcBatctrcde.set(ovrduerec.trancode);
		wsaaBatckey.batcBatcbatch.set(ovrduerec.batcbatch);
		ovrduerec.statuz.set(varcom.oK);
		/* Obtain transaction description from T1688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(ovrduerec.chdrcoy);
		descIO.setDesctabl(tablesInner.t1688);
		descIO.setDescitem(ovrduerec.trancode);
		descIO.setLanguage(ovrduerec.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError9100();
		}
		wsaaTransDesc.set(descIO.getLongdesc());
		/* Read T5645 for accounting details. This call will read the 2nd*/
		/* page of the table item(this item has more than one page of*/
		/* accounting details). This first page will be read in individual*/
		/* section. The details read here will be stored away in working*/
		/* storage and to be used later on.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		wsaaT5645Sub.set(1);
	}

protected void readT56451020()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			dbError9100();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItemcoy(),ovrduerec.chdrcoy)
		|| isNE(itemIO.getItemtabl(), tablesInner.t5645)
		|| isNE(itemIO.getItemitem(),wsaaSubr)) {
			itemIO.setGenarea(SPACES);
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaSub1.set(1);
	}

protected void tableLoop1030()
	{
		if (isEQ(t5645rec.sacscode[wsaaSub1.toInt()],SPACES)
		&& isEQ(t5645rec.sacstype[wsaaSub1.toInt()],SPACES)
		&& isEQ(t5645rec.glmap[wsaaSub1.toInt()],SPACES)) {
			wsaaSub1.set(17);
			goTo(GotoLabel.loopCheck1040);
		}
		if (isGT(wsaaT5645Sub,wsaaT5645Size)) {
			syserrrec.statuz.set(errorsInner.h791);
			systemError9000();
		}
		wsaaT5645Cnttot[wsaaT5645Sub.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
		wsaaT5645Glmap[wsaaT5645Sub.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
		wsaaT5645Sacscode[wsaaT5645Sub.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
		wsaaT5645Sacstype[wsaaT5645Sub.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
		wsaaT5645Sign[wsaaT5645Sub.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		wsaaSub1.add(1);
		wsaaT5645Sub.add(1);
	}

protected void loopCheck1040()
	{
		/* Next Line*/
		if (isLT(wsaaSub1,16)) {
			goTo(GotoLabel.tableLoop1030);
		}
		/* Next Page*/
		if (isEQ(wsaaSub1,16)) {
			itemIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readT56451020);
		}
		/* Read TH506*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(tablesInner.th506);
		itemIO.setItemitem(ovrduerec.cnttype);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		/* Read T5679*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(ovrduerec.trancode);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Read T5687*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(ovrduerec.crtable);
		itdmIO.setItmfrm(ovrduerec.crrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(ovrduerec.crtable,itdmIO.getItemitem())
		|| isNE(ovrduerec.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			ovrduerec.statuz.set(errorsInner.h053);
			return ;
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t5687rec.svMethod,SPACES)) {
			ovrduerec.statuz.set(errorsInner.e076);
			return ;
		}
		/* Read T6598*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(tablesInner.t6598);
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		/* Read T5688*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(ovrduerec.cnttype);
		itdmIO.setItmfrm(ovrduerec.crrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(ovrduerec.cnttype,itdmIO.getItemitem())
		|| isNE(ovrduerec.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			ovrduerec.statuz.set(errorsInner.e308);
			return ;
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		/* Read HCSD to obtain the Cash Div Method.*/
		hcsdIO.setChdrcoy(ovrduerec.chdrcoy);
		hcsdIO.setChdrnum(ovrduerec.chdrnum);
		hcsdIO.setLife(ovrduerec.life);
		hcsdIO.setCoverage(ovrduerec.coverage);
		hcsdIO.setRider(ovrduerec.rider);
		hcsdIO.setPlanSuffix(ovrduerec.planSuffix);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isEQ(hcsdIO.getStatuz(),varcom.mrnf)) {
			wsaaCashDividend = "N";
			initialize(th501rec.th501Rec);
			return ;
		}
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			dbError9100();
		}
		/* Read TH501.*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.th501);
		itdmIO.setItemitem(hcsdIO.getZcshdivmth());
		itdmIO.setItmfrm(ovrduerec.crrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.th501)
		|| isNE(itdmIO.getItemitem(),hcsdIO.getZcshdivmth())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(errorsInner.z039);
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		th501rec.th501Rec.set(itdmIO.getGenarea());
		readTableTh6051400();
	}

protected void readTableTh6051400()
	{
		start1410();
	}

protected void start1410()
	{
		/* Read TH605 to see whether system is "Override based on Agent    */
		/* Details"                                                        */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(ovrduerec.chdrcoy);
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(ovrduerec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError9100();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

protected void calSurrVal3000()
	{
					start3010();
					checkSurrVal3020();
				}

protected void start3010()
	{
		/* Set up SRCALCPY from linkage*/
		initialize(srcalcpy.surrenderRec);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(ovrduerec.chdrcoy);
		srcalcpy.chdrChdrnum.set(ovrduerec.chdrnum);
		srcalcpy.planSuffix.set(ovrduerec.planSuffix);
		srcalcpy.polsum.set(ovrduerec.polsum);
		srcalcpy.lifeLife.set(ovrduerec.life);
		srcalcpy.covrCoverage.set(ovrduerec.coverage);
		srcalcpy.covrRider.set(ovrduerec.rider);
		srcalcpy.crtable.set(ovrduerec.crtable);
		srcalcpy.crrcd.set(ovrduerec.crrcd);
		srcalcpy.ptdate.set(ovrduerec.ptdate);
		srcalcpy.effdate.set(ovrduerec.effdate);
		srcalcpy.language.set(ovrduerec.language);
		srcalcpy.chdrCurr.set(ovrduerec.cntcurr);
		srcalcpy.pstatcode.set(ovrduerec.pstatcode);
		srcalcpy.billfreq.set(ovrduerec.billfreq);
		while ( !(isEQ(srcalcpy.status,varcom.endp)
		|| isEQ(srcalcpy.status,varcom.bomb))) {
			/*IVE-796 RUL Product - Partial Surrender Calculation started*/
			//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))
			{
				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			}
			else
			{
		 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
				Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
				vpxsurcrec.function.set("INIT");
				callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
				vpmfmtrec.initialize();
				vpmfmtrec.amount02.set(wsaaEstimateTot);
				chdrlnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
				chdrlnbIO.setChdrnum(srcalcpy.chdrChdrnum);
				chdrlnbIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, chdrlnbIO);

				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrlnbIO);//VPMS call
				
				
				if(isEQ(srcalcpy.type,"L"))
				{
					vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
					callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
					srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
					srcalcpy.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpy.type,"C"))
				{
					srcalcpy.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
				{
					srcalcpy.status.set(varcom.endp);
				}
				else
				{
					srcalcpy.status.set(varcom.oK);
				}
			}

			/*IVE-796 RUL Product - Partial Surrender Calculation end*/
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			a200CallRounding();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			if (isEQ(srcalcpy.type,"S")){
				wsaaValuesInner.wsaaBasicCv.set(srcalcpy.actualVal);
			}
			else if (isEQ(srcalcpy.type,"P")){
				wsaaValuesInner.wsaaPuaCv.set(srcalcpy.actualVal);
			}
			else if (isEQ(srcalcpy.type,"D")){
				wsaaValuesInner.wsaaDvd.set(srcalcpy.actualVal);
			}
			else if (isEQ(srcalcpy.type,"I")){
				wsaaValuesInner.wsaaInt.set(srcalcpy.actualVal);
			}
			wsaaValuesInner.wsaaTotSv.add(srcalcpy.actualVal);
			srcalcpy.actualVal.set(ZERO);
		}
		
		/* Set up to call dividend allocation if premium is fully paid*/
		hcsdIO.setDataKey(SPACES);
		hcsdIO.setChdrcoy(ovrduerec.chdrcoy);
		hcsdIO.setChdrnum(ovrduerec.chdrnum);
		hcsdIO.setLife(ovrduerec.life);
		hcsdIO.setCoverage(ovrduerec.coverage);
		hcsdIO.setRider(ovrduerec.rider);
		hcsdIO.setPlanSuffix(ovrduerec.planSuffix);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		/*    IF HCSD-STATUZ          NOT = O-K                            */
		if (isNE(hcsdIO.getStatuz(),varcom.oK)
		&& isNE(hcsdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			dbError9100();
		}
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setStatuz(varcom.oK);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE");
		covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setChdrcoy(ovrduerec.chdrcoy);
		covrmjaIO.setChdrnum(ovrduerec.chdrnum);
		covrmjaIO.setLife(ovrduerec.life);
		covrmjaIO.setCoverage(ovrduerec.coverage);
		covrmjaIO.setRider(ovrduerec.rider);
		covrmjaIO.setPlanSuffix(ovrduerec.planSuffix);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			dbError9100();
		}
		if (isNE(covrmjaIO.getChdrcoy(),ovrduerec.chdrcoy)
		|| isNE(covrmjaIO.getChdrnum(),ovrduerec.chdrnum)
		|| isNE(covrmjaIO.getLife(),ovrduerec.life)
		|| isNE(covrmjaIO.getCoverage(),ovrduerec.coverage)
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			covrmjaIO.setStatuz(errorsInner.f259);
			ovrduerec.statuz.set(errorsInner.f259);
			return ;
		}
		if (isEQ(wsaaCashDividend,"N")) {
			return ;
		}
		datcon2rec.intDate1.set(covrmjaIO.getUnitStatementDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaNewBonusDate.set(datcon2rec.intDate2);
		if (isGT(wsaaNewBonusDate,ovrduerec.ptdate)) {
			return ;
		}
		initialize(hdivdrec.dividendRec);
		hdivdrec.chdrChdrcoy.set(ovrduerec.chdrcoy);
		hdivdrec.chdrChdrnum.set(ovrduerec.chdrnum);
		hdivdrec.lifeLife.set(ovrduerec.life);
		hdivdrec.covrCoverage.set(ovrduerec.coverage);
		hdivdrec.covrRider.set(ovrduerec.rider);
		hdivdrec.plnsfx.set(ovrduerec.planSuffix);
		hdivdrec.cntcurr.set(ovrduerec.cntcurr);
		hdivdrec.cnttype.set(ovrduerec.cnttype);
		hdivdrec.transcd.set(ovrduerec.trancode);
		hdivdrec.premStatus.set(covrmjaIO.getPstatcode());
		hdivdrec.crtable.set(ovrduerec.crtable);
		hdivdrec.sumin.set(covrmjaIO.getSumins());
		hdivdrec.effectiveDate.set(ovrduerec.effdate);
		hdivdrec.ptdate.set(ovrduerec.ptdate);
		hdivdrec.allocMethod.set(covrmjaIO.getBonusInd());
		hdivdrec.mortcls.set(covrmjaIO.getMortcls());
		hdivdrec.sex.set(covrmjaIO.getSex());
		hdivdrec.issuedAge.set(covrmjaIO.getAnbAtCcd());
		hdivdrec.tranno.set(ovrduerec.tranno);
		hdivdrec.divdCalcMethod.set(hcsdIO.getZcshdivmth());
		hdivdrec.periodFrom.set(covrmjaIO.getUnitStatementDate());
		hdivdrec.periodTo.set(wsaaNewBonusDate);
		hdivdrec.crrcd.set(covrmjaIO.getCrrcd());
		hdivdrec.cessDate.set(covrmjaIO.getRiskCessDate());
		wsaaBillfreq.set(ovrduerec.billfreq);
		compute(hdivdrec.annualisedPrem, 3).setRounded(mult(covrmjaIO.getInstprem(),wsaaBillfreq9));
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		hdivdrec.divdRate.set(ZERO);
		hdivdrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Dividend Allocation Subroutine*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t6639);
		wsaaT6639Divdmth.set(hcsdIO.getZcshdivmth());
		wsaaT6639Pstcd.set(covrmjaIO.getPstatcode());
		itdmIO.setItemitem(wsaaT6639Item);
		itdmIO.setItmfrm(wsaaNewBonusDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6639)
		|| isNE(itdmIO.getItemitem(),wsaaT6639Item)) {
			itdmIO.setStatuz(varcom.endp);
			return ;
		}
		t6639rec.t6639Rec.set(itdmIO.getGenarea());
		if (isEQ(t6639rec.revBonusProg,SPACES)) {
			return ;
		}
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz,varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			systemError9000();
		}
		zrdecplrec.amountIn.set(hdivdrec.divdAmount);
		a200CallRounding();
		hdivdrec.divdAmount.set(zrdecplrec.amountOut);
		/*    When the dividend amount from allocation is not zeroes,*/
		/*    add the amount to WSAA-DVD for dividend total*/
		if (isNE(hdivdrec.divdAmount,0)) {
			wsaaValuesInner.wsaaBasicDvd.set(hdivdrec.divdAmount);
			wsaaValuesInner.wsaaDivdRate.set(hdivdrec.divdRate);
			wsaaValuesInner.wsaaRateDate.set(hdivdrec.rateDate);
			wsaaValuesInner.wsaaNewDvd.add(hdivdrec.divdAmount);
			wsaaValuesInner.wsaaDvd.add(hdivdrec.divdAmount);
			wsaaValuesInner.wsaaTotSv.add(hdivdrec.divdAmount);
		}
		/*    Read HPUA and allocate dividend from these paid-up addition*/
		hpuaIO.setChdrcoy(hdivdrec.chdrChdrcoy);
		hpuaIO.setChdrnum(hdivdrec.chdrChdrnum);
		hpuaIO.setLife(hdivdrec.lifeLife);
		hpuaIO.setCoverage(hdivdrec.covrCoverage);
		hpuaIO.setRider(hdivdrec.covrRider);
		hpuaIO.setPlanSuffix(hdivdrec.plnsfx);
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hpuaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)
		&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			dbError9100();
		}
		if (isEQ(hpuaIO.getStatuz(),varcom.endp)
		|| isNE(hpuaIO.getChdrcoy(),hdivdrec.chdrChdrcoy)
		|| isNE(hpuaIO.getChdrnum(),hdivdrec.chdrChdrnum)
		|| isNE(hpuaIO.getLife(),hdivdrec.lifeLife)
		|| isNE(hpuaIO.getCoverage(),hdivdrec.covrCoverage)
		|| isNE(hpuaIO.getRider(),hdivdrec.covrRider)
		|| isNE(hpuaIO.getPlanSuffix(),hdivdrec.plnsfx)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hpuaIO.getStatuz(),varcom.endp))) {
			dividendPaidup4000();
		}
		
	}

protected void checkSurrVal3020()
	{
		/* Check for zero Surrender Value*/
		if (isEQ(wsaaValuesInner.wsaaTotSv, ZERO)) {
			if (isEQ(ovrduerec.function,SPACES)) {
				wsaaException.set(nilSvMsg);
				openPrinter8800();
				writeRh5478300();
			}
			ovrduerec.statuz.set("LAPS");
		}
		/*EXIT*/
	}

protected void dividendPaidup4000()
	{
					dividend4010();
					nextHpua4080();
				}

protected void dividend4010()
	{
		/*    Skip this HPUA if it is not Dividend participant*/
		if (isNE(hpuaIO.getDivdParticipant(),"Y")) {
			return ;
		}
		/*    Find dividend allocation subroutine from T6639.*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t6639);
		wsaaT6639Divdmth.set(hcsdIO.getZcshdivmth());
		wsaaT6639Pstcd.set(hpuaIO.getPstatcode());
		itdmIO.setItemitem(wsaaT6639Item);
		itdmIO.setItmfrm(wsaaNewBonusDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6639)
		|| isNE(itdmIO.getItemitem(),wsaaT6639Item)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			return ;
		}
		t6639rec.t6639Rec.set(itdmIO.getGenarea());
		/*    Check if dividend allocation subroutine is defined*/
		if (isEQ(t6639rec.revBonusProg,SPACES)) {
			return ;
		}
		/*    Prepare to call cash dividend allocation subroutine*/
		initialize(hdivdrec.dividendRec);
		hdivdrec.chdrChdrcoy.set(ovrduerec.chdrcoy);
		hdivdrec.chdrChdrnum.set(ovrduerec.chdrnum);
		hdivdrec.lifeLife.set(ovrduerec.life);
		hdivdrec.covrCoverage.set(ovrduerec.coverage);
		hdivdrec.covrRider.set(ovrduerec.rider);
		hdivdrec.plnsfx.set(ovrduerec.planSuffix);
		hdivdrec.cntcurr.set(ovrduerec.cntcurr);
		hdivdrec.cnttype.set(ovrduerec.cnttype);
		hdivdrec.transcd.set(ovrduerec.trancode);
		hdivdrec.premStatus.set(hpuaIO.getPstatcode());
		hdivdrec.crtable.set(hpuaIO.getCrtable());
		hdivdrec.sumin.set(hpuaIO.getSumin());
		hdivdrec.effectiveDate.set(ovrduerec.effdate);
		hdivdrec.ptdate.set(ovrduerec.ptdate);
		hdivdrec.allocMethod.set(covrmjaIO.getBonusInd());
		hdivdrec.mortcls.set(covrmjaIO.getMortcls());
		hdivdrec.sex.set(covrmjaIO.getSex());
		hdivdrec.issuedAge.set(hpuaIO.getAnbAtCcd());
		hdivdrec.tranno.set(ovrduerec.tranno);
		hdivdrec.divdCalcMethod.set(hcsdIO.getZcshdivmth());
		hdivdrec.periodFrom.set(covrmjaIO.getUnitStatementDate());
		hdivdrec.periodTo.set(wsaaNewBonusDate);
		hdivdrec.crrcd.set(hpuaIO.getCrrcd());
		hdivdrec.cessDate.set(hpuaIO.getRiskCessDate());
		hdivdrec.annualisedPrem.set(hpuaIO.getSingp());
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		hdivdrec.divdRate.set(ZERO);
		hdivdrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Dividend Paid-up Allocation Subroutine*/
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz,varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			systemError9000();
		}
		zrdecplrec.amountIn.set(hdivdrec.divdAmount);
		a200CallRounding();
		hdivdrec.divdAmount.set(zrdecplrec.amountOut);
		/*    When the dividend amount from allocation is not zeroes,*/
		/*    write a HDIV (dividend detail) and update HDIS (dividend*/
		/*    history) if exists or create one.*/
		if (isNE(hdivdrec.divdAmount,0)) {
			wsaaValuesInner.wsaaNewDvd.add(hdivdrec.divdAmount);
			wsaaValuesInner.wsaaDvd.add(hdivdrec.divdAmount);
			wsaaValuesInner.wsaaTotSv.add(hdivdrec.divdAmount);
		}
	}

protected void nextHpua4080()
	{
		hpuaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)
		&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			dbError9100();
		}
		if (isEQ(hpuaIO.getStatuz(),varcom.endp)
		|| isNE(hpuaIO.getChdrcoy(),hdivdrec.chdrChdrcoy)
		|| isNE(hpuaIO.getChdrnum(),hdivdrec.chdrChdrnum)
		|| isNE(hpuaIO.getLife(),hdivdrec.lifeLife)
		|| isNE(hpuaIO.getCoverage(),hdivdrec.covrCoverage)
		|| isNE(hpuaIO.getRider(),hdivdrec.covrRider)
		|| isNE(hpuaIO.getPlanSuffix(),hdivdrec.plnsfx)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void settleLoan5000()
	{
			start5010();
		}

protected void start5010()
	{
		/*  Call HRTOTLON to bring interest up to date on this*/
		/*  contract.*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(ovrduerec.chdrcoy);
		totloanrec.chdrnum.set(ovrduerec.chdrnum);
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(ovrduerec.effdate);
		totloanrec.tranno.set(ovrduerec.tranno);
		totloanrec.batchkey.set(wsaaBatckey);
		totloanrec.tranTerm.set(ovrduerec.termid);
		totloanrec.tranDate.set(ovrduerec.tranDate);
		totloanrec.tranTime.set(ovrduerec.tranTime);
		totloanrec.tranUser.set(ovrduerec.user);
		totloanrec.language.set(ovrduerec.language);
		totloanrec.function.set("LOAN");
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			systemError9000();
		}
		compute(wsaaValuesInner.wsaaTotLoan, 2).set(add(totloanrec.principal, totloanrec.interest));
		if (isLTE(wsaaValuesInner.wsaaTotLoan, ZERO)) {
			wsaaValuesInner.wsaaAccumValue.set(wsaaValuesInner.wsaaTotSv);
			return ;
		}
		/* Surrender Value is less than outstanding loan and interest,*/
		/* this is an error.*/
		if (isLTE(wsaaValuesInner.wsaaTotSv, wsaaValuesInner.wsaaTotLoan)) {
			if (isEQ(ovrduerec.function,SPACES)) {
				wsaaException.set(lonErMsg);
				openPrinter8800();
				writeRh5478300();
			}
			/*        MOVE 'OMIT'             TO OVRD-STATUZ*/
			ovrduerec.statuz.set("SKIP");
			return ;
		}
		/* If error free, post the loan interest.*/
		if (isNE(ovrduerec.function,"OLNPT")) {
			totloanrec.totloanRec.set(SPACES);
			totloanrec.chdrcoy.set(ovrduerec.chdrcoy);
			totloanrec.chdrnum.set(ovrduerec.chdrnum);
			totloanrec.principal.set(ZERO);
			totloanrec.interest.set(ZERO);
			totloanrec.loanCount.set(ZERO);
			totloanrec.effectiveDate.set(ovrduerec.effdate);
			totloanrec.tranno.set(ovrduerec.tranno);
			totloanrec.batchkey.set(wsaaBatckey);
			totloanrec.tranTerm.set(ovrduerec.termid);
			totloanrec.tranDate.set(ovrduerec.tranDate);
			totloanrec.tranTime.set(ovrduerec.tranTime);
			totloanrec.tranUser.set(ovrduerec.user);
			totloanrec.language.set(ovrduerec.language);
			totloanrec.function.set("LOAN");
			totloanrec.postFlag.set("Y");
			callProgram(Hrtotlon.class, totloanrec.totloanRec);
			if (isNE(totloanrec.statuz,varcom.oK)) {
				syserrrec.params.set(totloanrec.totloanRec);
				syserrrec.statuz.set(totloanrec.statuz);
				systemError9000();
			}
		}
		/*  Loan repayment.*/
		wsaaGlCompany.set(ovrduerec.chdrcoy);
		wsaaGlCurrency.set(ovrduerec.cntcurr);
		wsaaTranTermid.set(ovrduerec.termid);
		wsaaTranUser.set(ovrduerec.user);
		wsaaTranTime.set(ovrduerec.tranTime);
		wsaaTranDate.set(ovrduerec.tranDate);
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(ovrduerec.chdrcoy);
		wsaaTranEntity.set(ovrduerec.chdrnum);
		cashedrec.cashedRec.set(SPACES);
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(wsaaBatckey);
		cashedrec.doctNumber.set(ovrduerec.chdrnum);
		cashedrec.doctCompany.set(ovrduerec.chdrcoy);
		cashedrec.trandate.set(ovrduerec.effdate);
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		wsaaSequenceNo.set(1);
		cashedrec.transeq.set(wsaaSequenceNo);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(ovrduerec.cntcurr);
		cashedrec.dissrate.set(0);
		cashedrec.trandesc.set(SPACES);
		cashedrec.genlCompany.set(ovrduerec.chdrcoy);
		cashedrec.genlCurrency.set(ovrduerec.cntcurr);
		cashedrec.chdrcoy.set(ovrduerec.chdrcoy);
		cashedrec.chdrnum.set(ovrduerec.chdrnum);
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(ovrduerec.chdrcoy);
		wsaaTranEntity.set(ovrduerec.chdrnum);
		cashedrec.trankey.set(wsaaTrankey);
		cashedrec.tranno.set(ovrduerec.tranno);
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.language.set(ovrduerec.language);
		if (isEQ(ovrduerec.function,"OLNPT")) {
			compute(wsaaValuesInner.wsaaAccumValue, 0).set(sub(wsaaValuesInner.wsaaTotSv, wsaaValuesInner.wsaaTotLoan));
		}
		else {
			if (isNE(wsaaValuesInner.wsaaTotLoan, ZERO)) {
				wsaaValuesInner.wsaaAccumValue.set(wsaaValuesInner.wsaaTotSv);
				for (wsaaSub1.set(13); !(isGT(wsaaSub1,16)
				|| isEQ(wsaaValuesInner.wsaaAccumValue, 0)); wsaaSub1.add(1)){
					loans5500();
				}
				wsaaSequenceNo.set(cashedrec.transeq);
			}
			else {
				wsaaValuesInner.wsaaAccumValue.set(wsaaValuesInner.wsaaTotSv);
			}
		}
		/* Surrender Value should be greater than zero after repaying*/
		/* the loan and interest.*/
		if (isLTE(wsaaValuesInner.wsaaAccumValue, ZERO)) {
			if (isEQ(ovrduerec.function,SPACES)) {
				wsaaException.set(lonErMsg);
				openPrinter8800();
				writeRh5478300();
			}
			/*        MOVE 'OMIT'             TO OVRD-STATUZ*/
			ovrduerec.statuz.set("SKIP");
			return ;
		}
	}

protected void loans5500()
	{
		start5510();
	}

protected void start5510()
	{
		/* Line 13 - 16 of T5645 Accounting entry*/
		cashedrec.sign.set(wsaaT5645Sign[wsaaSub1.toInt()]);
		cashedrec.sacscode.set(wsaaT5645Sacscode[wsaaSub1.toInt()]);
		cashedrec.sacstyp.set(wsaaT5645Sacstype[wsaaSub1.toInt()]);
		wsaaGlMap.set(wsaaT5645Glmap[wsaaSub1.toInt()]);
		cashedrec.genlkey.set(wsaaGenlkey);
		cashedrec.origamt.set(wsaaValuesInner.wsaaAccumValue);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			systemError9000();
		}
		wsaaValuesInner.wsaaAccumValue.set(cashedrec.docamt);
	}

protected void callLifacmv6000()
	{
		/*START*/
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError9000();
		}
		/*EXIT*/
	}

protected void calRpuCover7000()
	{
		/*START*/
		/* If online quotation, exit program.*/
		/* Otherwise, perform posting.*/
		newIssAge7100();
		if (isEQ(ovrduerec.statuz,varcom.oK)) {
			calRpuSi7300();
			if (isEQ(ovrduerec.statuz,varcom.oK)) {
				lookUpTerm7500();
				if (isEQ(ovrduerec.statuz,varcom.oK)) {
					if (isEQ(ovrduerec.function,"OLNPT")) {
						return ;
					}
					else {
						writeHpua7600();
						posting7700();
					}
				}
			}
		}
		/*EXIT*/
	}

protected void newIssAge7100()
	{
			start7110();
		}

protected void start7110()
	{
		/* Read T1693 to obtain FSU Company.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(tablesInner.t1693);
		itemIO.setItemitem(ovrduerec.chdrcoy);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError9100();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		/* Read LIFELNB.*/
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(ovrduerec.chdrcoy);
		lifelnbIO.setChdrnum(ovrduerec.chdrnum);
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			dbError9100();
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.endp)
		|| isNE(lifelnbIO.getChdrcoy(),ovrduerec.chdrcoy)
		|| isNE(lifelnbIO.getChdrnum(),ovrduerec.chdrnum)) {
			ovrduerec.statuz.set(errorsInner.e355);
			return ;
		}
		/* Set up AGECALCREC.*/
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(ovrduerec.language);
		agecalcrec.cnttype.set(ovrduerec.cnttype);
		agecalcrec.intDate1.set(lifelnbIO.getCltdob());
		agecalcrec.intDate2.set(ovrduerec.ptdate);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.params.set(agecalcrec.agecalcRec);
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError9000();
		}
		wsaaAnb.set(agecalcrec.agerating);
	}

protected void calRpuSi7300()
	{
			start7310();
		}

protected void start7310()
	{
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			covrmjaIO.setStatuz(errorsInner.f259);
			ovrduerec.statuz.set(errorsInner.f259);
			return ;
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(ovrduerec.ptdate);
		datcon3rec.intDate2.set(ovrduerec.riskCessDate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			systemError9000();
		}
		wsaaDtc3FreqFactor.set(datcon3rec.freqFactor);
		compute(wsaaDtc3Diff, 5).set(sub(datcon3rec.freqFactor,wsaaDtc3FreqFactor));
		if (isGT(wsaaDtc3Diff,ZERO)) {
			compute(wsaaMaturityYears, 5).set(add(1,datcon3rec.freqFactor));
		}
		else {
			wsaaMaturityYears.set(datcon3rec.freqFactor);
		}
	}

protected void lookUpTerm7500()
	{
			start7510();
		}

protected void start7510()
	{
		/* Look up TH546 to obtain the*/
		wsaaTh546Found = "N";
		wsaaTh546Exhausted = "N";
		wsaaTh546Crtable.set(covrmjaIO.getCrtable());
		wsaaTh546MatYr.set(wsaaMaturityYr);
		wsaaTh546Mortcls.set(covrmjaIO.getMortcls());
		wsaaTh546Sex.set(covrmjaIO.getSex());
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.th546);
		itdmIO.setItemitem(wsaaTh546Keys);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		while ( !(isEQ(wsaaTh546Found,"Y")
		|| isEQ(wsaaTh546Exhausted,"Y"))) {
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				dbError9100();
			}
			if (isNE(wsaaTh546Keys,itdmIO.getItemitem())
			|| isNE(covrmjaIO.getChdrcoy(),itdmIO.getItemcoy())
			|| isNE(itdmIO.getItemtabl(), tablesInner.th546)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				if (isEQ(wsaaTh546Crtable,"****")) {
					wsaaTh546Exhausted = "Y";
				}
				else {
					if (isEQ(wsaaTh546MatYr,"**")) {
						wsaaTh546Crtable.set("****");
					}
					else {
						if (isEQ(wsaaTh546Mortcls,"*")) {
							wsaaTh546MatYr.set("**");
						}
						else {
							if (isEQ(wsaaTh546Sex,"*")) {
								wsaaTh546Mortcls.set("*");
							}
							else {
								wsaaTh546Sex.set("*");
							}
						}
					}
				}
				itdmIO.setItempfx("IT");
				itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
				itdmIO.setItemtabl(tablesInner.th546);
				itdmIO.setItemitem(wsaaTh546Keys);
				itdmIO.setItmfrm(covrmjaIO.getCrrcd());
			}
			else {
				wsaaTh546Found = "Y";
			}
		}
		
		if (isEQ(wsaaTh546Found,"N")) {
			ovrduerec.statuz.set(errorsInner.hl37);
			return ;
		}
		th546rec.th546Rec.set(itdmIO.getGenarea());
		/* Calculate the RPU SI.*/
		if (isEQ(wsaaAnb,0)) {
			wsaaInsprm.set(th546rec.insprem);
		}
		else {
			/*       IF WSAA-ANB   = 100                               <V73L03>*/
			/*          MOVE TH546-INSTPR       TO WSAA-INSPRM         <V73L03>*/
			if (isGTE(wsaaAnb, 100)
			&& isLTE(wsaaAnb, 110)) {
				compute(wsaaIndex, 0).set(sub(wsaaAnb, 99));
				wsaaInsprm.set(th546rec.instpr[wsaaIndex.toInt()]);
			}
			else {
				wsaaInsprm.set(th546rec.insprm[wsaaAnb.toInt()]);
			}
		}
		compute(wsaaValuesInner.wsaaRpuSi, 1).setRounded(mult((div((mult(th546rec.unit, th546rec.premUnit)), wsaaInsprm)), wsaaValuesInner.wsaaAccumValue));
		zrdecplrec.amountIn.set(wsaaValuesInner.wsaaRpuSi);
		a200CallRounding();
		wsaaValuesInner.wsaaRpuSi.set(zrdecplrec.amountOut);
		wsaaValuesInner.wsaaRefund.set(ZERO);
		wsaaValuesInner.wsaaRpuCost.set(wsaaValuesInner.wsaaAccumValue);
		if (isGT(wsaaValuesInner.wsaaRpuSi, ZERO)) {
			if (isGT(covrmjaIO.getVarSumInsured(),ZERO)) {
				if (isGT(wsaaValuesInner.wsaaRpuSi, covrmjaIO.getVarSumInsured())) {
					wsaaValuesInner.wsaaRpuSi.set(covrmjaIO.getVarSumInsured());
					compute(wsaaValuesInner.wsaaRpuCost, 1).setRounded(div(wsaaValuesInner.wsaaRpuSi, (div((mult(th546rec.unit, th546rec.premUnit)), wsaaInsprm))));
					zrdecplrec.amountIn.set(wsaaValuesInner.wsaaRpuCost);
					a200CallRounding();
					wsaaValuesInner.wsaaRpuCost.set(zrdecplrec.amountOut);
					compute(wsaaValuesInner.wsaaRefund, 1).setRounded(sub(wsaaValuesInner.wsaaAccumValue, wsaaValuesInner.wsaaRpuCost));
					if (isEQ(ovrduerec.function,SPACES)) {
						wsaaException.set(refundMsg);
						openPrinter8800();
						writeRh5478300();
					}
				}
			}
			else {
				if (isGT(wsaaValuesInner.wsaaRpuSi, covrmjaIO.getSumins())) {
					wsaaValuesInner.wsaaRpuSi.set(covrmjaIO.getSumins());
					compute(wsaaValuesInner.wsaaRpuCost, 1).setRounded(div(wsaaValuesInner.wsaaRpuSi, (div((mult(th546rec.unit, th546rec.premUnit)), wsaaInsprm))));
					zrdecplrec.amountIn.set(wsaaValuesInner.wsaaRpuCost);
					a200CallRounding();
					wsaaValuesInner.wsaaRpuCost.set(zrdecplrec.amountOut);
					compute(wsaaValuesInner.wsaaRefund, 1).setRounded(sub(wsaaValuesInner.wsaaAccumValue, wsaaValuesInner.wsaaRpuCost));
					if (isEQ(ovrduerec.function,SPACES)) {
						wsaaException.set(refundMsg);
						openPrinter8800();
						writeRh5478300();
					}
				}
			}
		}
		/* Update the return values*/
		ovrduerec.newSumins.set(ZERO);
		ovrduerec.surrenderValue.set(wsaaValuesInner.wsaaTotSv);
		ovrduerec.newInstprem.set(ZERO);
		ovrduerec.cvRefund.set(wsaaValuesInner.wsaaRefund);
		ovrduerec.newPua.set(wsaaValuesInner.wsaaRpuSi);
		ovrduerec.newAnb.set(wsaaAnb);
		ovrduerec.newSingp.set(wsaaValuesInner.wsaaRpuCost);
		ovrduerec.newPremCessDate.set(ovrduerec.ptdate);
	}

protected void writeHpua7600()
	{
			start7610();
		}

protected void start7610()
	{
		/* Obtain the last PUA Number*/
		hpuadopIO.setParams(SPACES);
		hpuadopIO.setChdrcoy(ovrduerec.chdrcoy);
		hpuadopIO.setChdrnum(ovrduerec.chdrnum);
		hpuadopIO.setLife(ovrduerec.life);
		hpuadopIO.setCoverage(ovrduerec.coverage);
		hpuadopIO.setRider(ovrduerec.rider);
		hpuadopIO.setPlanSuffix(ovrduerec.planSuffix);
		hpuadopIO.setPuAddNbr(999);
		hpuadopIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hpuadopIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuadopIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX");
		hpuadopIO.setFormat(formatsInner.hpuadoprec);
		SmartFileCode.execute(appVars, hpuadopIO);
		if (isNE(hpuadopIO.getStatuz(),varcom.oK)
		&& isNE(hpuadopIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuadopIO.getParams());
			dbError9100();
		}
		if (isNE(ovrduerec.chdrcoy,hpuadopIO.getChdrcoy())
		|| isNE(ovrduerec.chdrnum,hpuadopIO.getChdrnum())
		|| isNE(ovrduerec.life,hpuadopIO.getLife())
		|| isNE(ovrduerec.coverage,hpuadopIO.getCoverage())
		|| isNE(ovrduerec.rider,hpuadopIO.getRider())
		|| isNE(ovrduerec.planSuffix,hpuadopIO.getPlanSuffix())
		|| isEQ(hpuadopIO.getStatuz(),varcom.endp)) {
			wsaaNewPuaNo.set(1);
		}
		else {
			compute(wsaaNewPuaNo, 0).set(add(1,hpuadopIO.getPuAddNbr()));
		}
		/* Read TH548*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.th548);
		itdmIO.setItemitem(ovrduerec.crtable);
		itdmIO.setItmfrm(ovrduerec.crrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(ovrduerec.crtable,itdmIO.getItemitem())
		|| isNE(ovrduerec.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.th548)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			/*       MOVE H053                TO OVRD-STATUZ                   */
			return ;
		}
		else {
			th548rec.th548Rec.set(itdmIO.getGenarea());
		}
		/* Setup and write HPUA*/
		hpuaIO.setParams(SPACES);
		hpuaIO.setChdrcoy(ovrduerec.chdrcoy);
		hpuaIO.setChdrnum(ovrduerec.chdrnum);
		hpuaIO.setLife(ovrduerec.life);
		hpuaIO.setJlife(covrmjaIO.getJlife());
		hpuaIO.setCoverage(ovrduerec.coverage);
		hpuaIO.setRider(ovrduerec.rider);
		hpuaIO.setPlanSuffix(ovrduerec.planSuffix);
		hpuaIO.setPuAddNbr(wsaaNewPuaNo);
		hpuaIO.setValidflag("1");
		hpuaIO.setTranno(ovrduerec.tranno);
		hpuaIO.setAnbAtCcd(wsaaAnb);
		hpuaIO.setSumin(wsaaValuesInner.wsaaRpuSi);
		hpuaIO.setSingp(ovrduerec.newSingp);
		hpuaIO.setCrrcd(ovrduerec.ptdate);
		hpuaIO.setRstatcode(t5679rec.setCovRiskStat);
		hpuaIO.setPstatcode(t5679rec.setCovPremStat);
		hpuaIO.setRiskCessDate(ovrduerec.riskCessDate);
		hpuaIO.setCrtable(th548rec.puCvCode);
		hpuaIO.setDivdParticipant(th548rec.divdParticipant);
		hpuaIO.setFunction(varcom.writr);
		hpuaIO.setFormat(formatsInner.hpuarec);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hpuaIO.getParams());
			dbError9100();
		}
		/* Update HCSD if there is a Dividend Option*/
		if (isNE(th548rec.zdivopt,SPACES)) {
			hcsdIO.setChdrcoy(ovrduerec.chdrcoy);
			hcsdIO.setChdrnum(ovrduerec.chdrnum);
			hcsdIO.setLife(ovrduerec.life);
			hcsdIO.setCoverage(ovrduerec.coverage);
			hcsdIO.setRider(ovrduerec.rider);
			hcsdIO.setPlanSuffix(ovrduerec.planSuffix);
			hcsdIO.setFormat(formatsInner.hcsdrec);
			hcsdIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, hcsdIO);
			if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hcsdIO.getParams());
				dbError9100();
			}
			hcsdIO.setValidflag("2");
			hcsdIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, hcsdIO);
			if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hcsdIO.getParams());
				dbError9100();
			}
			hcsdIO.setValidflag("1");
			hcsdIO.setTranno(ovrduerec.tranno);
			hcsdIO.setZdivopt(th548rec.zdivopt);
			hcsdIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hcsdIO);
			if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hcsdIO.getParams());
				dbError9100();
			}
		}
	}

protected void posting7700()
	{
		/*START*/
		setupLifacmv7720();
		postBasicCv7730();
		postPuaCv7740();
		divAccting7750();
		divIntAccting7760();
		updateHdis7768();
		rpuPremium7770();
		refund7780();
		commClawback7790();
		/*EXIT*/
	}

protected void setupLifacmv7720()
	{
		start7720();
	}

protected void start7720()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.batccoy.set(ovrduerec.chdrcoy);
		lifacmvrec.batcbrn.set(ovrduerec.batcbrn);
		lifacmvrec.batcactyr.set(ovrduerec.acctyear);
		lifacmvrec.batcactmn.set(ovrduerec.acctmonth);
		lifacmvrec.batctrcde.set(ovrduerec.trancode);
		lifacmvrec.batcbatch.set(ovrduerec.batcbatch);
		lifacmvrec.rldgcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.rdocnum.set(ovrduerec.chdrnum);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.tranno.set(ovrduerec.tranno);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(ovrduerec.effdate);
		lifacmvrec.tranref.set(ovrduerec.chdrnum);
		wsaaRldgChdrnum.set(ovrduerec.chdrnum);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.termid.set(ovrduerec.termid);
		lifacmvrec.transactionDate.set(ovrduerec.tranDate);
		lifacmvrec.transactionTime.set(ovrduerec.tranTime);
		lifacmvrec.user.set(ovrduerec.user);
		lifacmvrec.function.set("PSTW");
	}

protected void postNewDivdAlloc7725()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start7725();
					basicDvdHdiv7725();
				case puaDvdHdiv7725: 
					puaDvdHdiv7725();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7725()
	{
		/* Firstly, post dividend payable*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaNewDvd);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[25]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[25]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[25]);
			lifacmvrec.glsign.set(wsaaT5645Sign[25]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[25]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[23]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[23]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[23]);
			lifacmvrec.glsign.set(wsaaT5645Sign[23]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[23]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
		/* Then, dividend suspense, where if HDIS sacscode or sacstype is*/
		/* not blank, use them.*/
		if (isNE(hdisIO.getSacscode(),SPACES)
		|| isNE(hdisIO.getSacstyp(),SPACES)) {
			lifacmvrec.sacscode.set(hdisIO.getSacscode());
			lifacmvrec.sacstyp.set(hdisIO.getSacstyp());
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[24]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[24]);
		}
		lifacmvrec.glcode.set(wsaaT5645Glmap[24]);
		lifacmvrec.glsign.set(wsaaT5645Sign[24]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[24]);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rldgacct.set(ovrduerec.chdrnum);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void basicDvdHdiv7725()
	{
		/* Write HDIV to denote the new dividend allocated*/
		if (isEQ(wsaaValuesInner.wsaaBasicDvd, 0)) {
			goTo(GotoLabel.puaDvdHdiv7725);
		}
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(ovrduerec.chdrcoy);
		hdivIO.setChdrnum(ovrduerec.chdrnum);
		hdivIO.setLife(ovrduerec.life);
		hdivIO.setCoverage(ovrduerec.coverage);
		hdivIO.setRider(ovrduerec.rider);
		hdivIO.setPlanSuffix(ovrduerec.planSuffix);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setEffdate(ovrduerec.effdate);
		hdivIO.setDivdAllocDate(ovrduerec.effdate);
		hdivIO.setBatccoy(ovrduerec.chdrcoy);
		hdivIO.setBatcbrn(ovrduerec.batcbrn);
		hdivIO.setBatcactyr(ovrduerec.acctyear);
		hdivIO.setBatcactmn(ovrduerec.acctmonth);
		hdivIO.setBatctrcde(ovrduerec.trancode);
		hdivIO.setBatcbatch(ovrduerec.batcbatch);
		hdivIO.setTranno(ovrduerec.tranno);
		hdivIO.setDivdIntCapDate(99999999);
		hdivIO.setCntcurr(ovrduerec.cntcurr);
		hdivIO.setDivdType("D");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		/* Effectively, option processing has been actioned on this*/
		hdivIO.setDivdOptprocTranno(ovrduerec.tranno);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setDivdAmount(wsaaValuesInner.wsaaBasicDvd);
		hdivIO.setDivdRate(wsaaValuesInner.wsaaDivdRate);
		hdivIO.setDivdRtEffdt(wsaaValuesInner.wsaaRateDate);
		hdivIO.setFormat(formatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			dbError9100();
		}
	}

protected void puaDvdHdiv7725()
	{
		/*    Read HPUA and allocate dividend from these paid-up addition*/
		hpuaIO.setChdrcoy(ovrduerec.chdrcoy);
		hpuaIO.setChdrnum(ovrduerec.chdrnum);
		hpuaIO.setLife(ovrduerec.life);
		hpuaIO.setCoverage(ovrduerec.coverage);
		hpuaIO.setRider(ovrduerec.rider);
		hpuaIO.setPlanSuffix(ovrduerec.planSuffix);
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)
		&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			dbError9100();
		}
		if (isEQ(hpuaIO.getStatuz(),varcom.endp)
		|| isNE(hpuaIO.getChdrcoy(),ovrduerec.chdrcoy)
		|| isNE(hpuaIO.getChdrnum(),ovrduerec.chdrnum)
		|| isNE(hpuaIO.getLife(),ovrduerec.life)
		|| isNE(hpuaIO.getCoverage(),ovrduerec.coverage)
		|| isNE(hpuaIO.getRider(),ovrduerec.rider)
		|| isNE(hpuaIO.getPlanSuffix(),ovrduerec.planSuffix)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hpuaIO.getStatuz(),varcom.endp))) {
			dividendPaidupUpdate7728();
		}
		
		/* Pass unit statement date in OVRD- to update COVR*/
		ovrduerec.newBonusDate.set(wsaaNewBonusDate);
	}

protected void dividendPaidupUpdate7728()
	{
					dividend7728();
					nextHpua7728();
				}

protected void dividend7728()
	{
		/*    Skip this HPUA if it is not Dividend participant*/
		if (isNE(hpuaIO.getDivdParticipant(),"Y")) {
			return ;
		}
		/*    Skip this HPUA if it is the RPU of the current transaction*/
		if (isEQ(hpuaIO.getTranno(),ovrduerec.tranno)) {
			return ;
		}
		/*    Find dividend allocation subroutine from T6639.*/
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(tablesInner.t6639);
		wsaaT6639Divdmth.set(hcsdIO.getZcshdivmth());
		wsaaT6639Pstcd.set(hpuaIO.getPstatcode());
		itdmIO.setItemitem(wsaaT6639Item);
		itdmIO.setItmfrm(wsaaNewBonusDate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6639)
		|| isNE(itdmIO.getItemitem(),wsaaT6639Item)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			return ;
		}
		t6639rec.t6639Rec.set(itdmIO.getGenarea());
		/*    Check if dividend allocation subroutine is defined*/
		if (isEQ(t6639rec.revBonusProg,SPACES)) {
			return ;
		}
		/*    Prepare to call cash dividend allocation subroutine*/
		initialize(hdivdrec.dividendRec);
		hdivdrec.chdrChdrcoy.set(ovrduerec.chdrcoy);
		hdivdrec.chdrChdrnum.set(ovrduerec.chdrnum);
		hdivdrec.lifeLife.set(ovrduerec.life);
		hdivdrec.covrCoverage.set(ovrduerec.coverage);
		hdivdrec.covrRider.set(ovrduerec.rider);
		hdivdrec.plnsfx.set(ovrduerec.planSuffix);
		hdivdrec.cntcurr.set(ovrduerec.cntcurr);
		hdivdrec.cnttype.set(ovrduerec.cnttype);
		hdivdrec.transcd.set(ovrduerec.trancode);
		hdivdrec.premStatus.set(hpuaIO.getPstatcode());
		hdivdrec.crtable.set(hpuaIO.getCrtable());
		hdivdrec.sumin.set(hpuaIO.getSumin());
		hdivdrec.effectiveDate.set(ovrduerec.effdate);
		hdivdrec.ptdate.set(ovrduerec.ptdate);
		hdivdrec.allocMethod.set(covrmjaIO.getBonusInd());
		hdivdrec.mortcls.set(covrmjaIO.getMortcls());
		hdivdrec.sex.set(covrmjaIO.getSex());
		hdivdrec.issuedAge.set(hpuaIO.getAnbAtCcd());
		hdivdrec.tranno.set(ovrduerec.tranno);
		hdivdrec.divdCalcMethod.set(hcsdIO.getZcshdivmth());
		hdivdrec.periodFrom.set(covrmjaIO.getUnitStatementDate());
		hdivdrec.periodTo.set(wsaaNewBonusDate);
		hdivdrec.crrcd.set(hpuaIO.getCrrcd());
		hdivdrec.cessDate.set(hpuaIO.getRiskCessDate());
		hdivdrec.annualisedPrem.set(hpuaIO.getSingp());
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		hdivdrec.divdRate.set(ZERO);
		hdivdrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Dividend Paid-up Allocation Subroutine*/
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz,varcom.oK)) {
			syserrrec.params.set(hdivdrec.dividendRec);
			syserrrec.statuz.set(hdivdrec.statuz);
			systemError9000();
		}
		zrdecplrec.amountIn.set(hdivdrec.divdAmount);
		a200CallRounding();
		hdivdrec.divdAmount.set(zrdecplrec.amountOut);
		/*    When the dividend amount from allocation is not zeroes,*/
		/*    write a HDIV (dividend detail) and update HDIS (dividend*/
		/*    history) if exists or create one.*/
		if (isEQ(hdivdrec.divdAmount,0)) {
			return ;
		}
		/* Write HDIV to denote the new dividend allocated*/
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(ovrduerec.chdrcoy);
		hdivIO.setChdrnum(ovrduerec.chdrnum);
		hdivIO.setLife(ovrduerec.life);
		hdivIO.setCoverage(ovrduerec.coverage);
		hdivIO.setRider(ovrduerec.rider);
		hdivIO.setPlanSuffix(ovrduerec.planSuffix);
		hdivIO.setPuAddNbr(hpuaIO.getPuAddNbr());
		hdivIO.setEffdate(ovrduerec.effdate);
		hdivIO.setDivdAllocDate(ovrduerec.effdate);
		hdivIO.setBatccoy(ovrduerec.chdrcoy);
		hdivIO.setBatcbrn(ovrduerec.batcbrn);
		hdivIO.setBatcactyr(ovrduerec.acctyear);
		hdivIO.setBatcactmn(ovrduerec.acctmonth);
		hdivIO.setBatctrcde(ovrduerec.trancode);
		hdivIO.setBatcbatch(ovrduerec.batcbatch);
		hdivIO.setTranno(ovrduerec.tranno);
		hdivIO.setDivdIntCapDate(99999999);
		hdivIO.setCntcurr(ovrduerec.cntcurr);
		hdivIO.setDivdRate(hdivdrec.divdRate);
		hdivIO.setDivdRtEffdt(hdivdrec.rateDate);
		hdivIO.setDivdType("D");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		/* Effectively, option processing has been actioned on this*/
		hdivIO.setDivdOptprocTranno(ovrduerec.tranno);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setDivdAmount(hdivdrec.divdAmount);
		hdivIO.setFormat(formatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			dbError9100();
		}
	}

protected void nextHpua7728()
	{
		hpuaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(),varcom.oK)
		&& isNE(hpuaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			dbError9100();
		}
		if (isEQ(hpuaIO.getStatuz(),varcom.endp)
		|| isNE(hpuaIO.getChdrcoy(),ovrduerec.chdrcoy)
		|| isNE(hpuaIO.getChdrnum(),ovrduerec.chdrnum)
		|| isNE(hpuaIO.getLife(),ovrduerec.life)
		|| isNE(hpuaIO.getCoverage(),ovrduerec.coverage)
		|| isNE(hpuaIO.getRider(),ovrduerec.rider)
		|| isNE(hpuaIO.getPlanSuffix(),ovrduerec.planSuffix)) {
			hpuaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void postBasicCv7730()
	{
		start7731();
	}

protected void start7731()
	{
		/*    DR Cash Value of Component.*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaBasicCv);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[2]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[2]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[2]);
			lifacmvrec.glsign.set(wsaaT5645Sign[2]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[2]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[1]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[1]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[1]);
			lifacmvrec.glsign.set(wsaaT5645Sign[1]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[1]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void postPuaCv7740()
	{
			start7741();
		}

protected void start7741()
	{
		/*  DR Cash Value of PUA*/
		if (isEQ(wsaaValuesInner.wsaaPuaCv, ZERO)) {
			return ;
		}
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaPuaCv);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[4]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[4]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[4]);
			lifacmvrec.glsign.set(wsaaT5645Sign[4]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[4]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[3]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[3]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[3]);
			lifacmvrec.glsign.set(wsaaT5645Sign[3]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[3]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void divAccting7750()
	{
			start7751();
		}

protected void start7751()
	{
		/* Update the Cash Dividend Summary File to denote withdrawal*/
		hdisIO.setDataKey(SPACES);
		hdisIO.setChdrcoy(ovrduerec.chdrcoy);
		hdisIO.setChdrnum(ovrduerec.chdrnum);
		hdisIO.setLife(ovrduerec.life);
		hdisIO.setCoverage(ovrduerec.coverage);
		hdisIO.setRider(ovrduerec.rider);
		hdisIO.setPlanSuffix(ovrduerec.planSuffix);
		hdisIO.setFormat(formatsInner.hdisrec);
		hdisIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)
		&& isNE(hdisIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hdisIO.getParams());
			dbError9100();
		}
		if (isEQ(hdisIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		wsaaValuesInner.wsaaHdisOsInt.set(hdisIO.getOsInterest());
		if (isGT(wsaaValuesInner.wsaaNewDvd, 0)) {
			postNewDivdAlloc7725();
		}
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			dbError9100();
		}
		hdisIO.setFunction(varcom.writs);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			dbError9100();
		}
		if (isEQ(wsaaValuesInner.wsaaDvd, ZERO)) {
			return ;
		}
		/* Write HDIV to denote the dividend withdrawal has been taken*/
		/* place.*/
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(ovrduerec.chdrcoy);
		hdivIO.setChdrnum(ovrduerec.chdrnum);
		hdivIO.setLife(ovrduerec.life);
		hdivIO.setCoverage(ovrduerec.coverage);
		hdivIO.setRider(ovrduerec.rider);
		hdivIO.setPlanSuffix(ovrduerec.planSuffix);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setEffdate(ovrduerec.effdate);
		hdivIO.setDivdAllocDate(ovrduerec.effdate);
		/*    MOVE OVRD-COMPANY           TO HDIV-BATCCOY.*/
		hdivIO.setBatccoy(ovrduerec.chdrcoy);
		hdivIO.setBatcbrn(ovrduerec.batcbrn);
		hdivIO.setBatcactyr(ovrduerec.acctyear);
		hdivIO.setBatcactmn(ovrduerec.acctmonth);
		hdivIO.setBatctrcde(ovrduerec.trancode);
		hdivIO.setBatcbatch(ovrduerec.batcbatch);
		hdivIO.setTranno(ovrduerec.tranno);
		hdivIO.setDivdIntCapDate(99999999);
		hdivIO.setCntcurr(ovrduerec.cntcurr);
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(99999999);
		hdivIO.setDivdType("C");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		setPrecision(hdivIO.getDivdAmount(), 0);
		hdivIO.setDivdAmount(mult(wsaaValuesInner.wsaaDvd, -1));
		hdivIO.setFormat(formatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			dbError9100();
		}
	}

protected void divIntAccting7760()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start7761();
				case dividendWithdrawal7763: 
					dividendWithdrawal7763();
				case osInt7764: 
					osInt7764();
				case bookNewInterest7765: 
					bookNewInterest7765();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7761()
	{
		if (isEQ(wsaaValuesInner.wsaaInt, ZERO)) {
			goTo(GotoLabel.dividendWithdrawal7763);
		}
		/* Write HDIV to denote the dividend int withdrawal has been taken*/
		/* place.*/
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(ovrduerec.chdrcoy);
		hdivIO.setChdrnum(ovrduerec.chdrnum);
		hdivIO.setLife(ovrduerec.life);
		hdivIO.setCoverage(ovrduerec.coverage);
		hdivIO.setRider(ovrduerec.rider);
		hdivIO.setPlanSuffix(ovrduerec.planSuffix);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setEffdate(ovrduerec.effdate);
		hdivIO.setDivdAllocDate(ovrduerec.effdate);
		hdivIO.setBatccoy(ovrduerec.chdrcoy);
		hdivIO.setBatcbrn(ovrduerec.batcbrn);
		hdivIO.setBatcactyr(ovrduerec.acctyear);
		hdivIO.setBatcactmn(ovrduerec.acctmonth);
		hdivIO.setBatctrcde(ovrduerec.trancode);
		hdivIO.setBatcbatch(ovrduerec.batcbatch);
		hdivIO.setTranno(ovrduerec.tranno);
		hdivIO.setDivdIntCapDate(99999999);
		hdivIO.setCntcurr(ovrduerec.cntcurr);
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(99999999);
		hdivIO.setDivdType("I");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		setPrecision(hdivIO.getDivdAmount(), 0);
		hdivIO.setDivdAmount(mult(wsaaValuesInner.wsaaInt, -1));
		hdivIO.setFormat(formatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		if (isNE(hdivIO.getDivdAmount(),ZERO)) {
			SmartFileCode.execute(appVars, hdivIO);
			if (isNE(hdivIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(hdivIO.getParams());
				dbError9100();
			}
		}
	}

protected void dividendWithdrawal7763()
	{
		/*  DR Accumulated Dividend*/
		if (isEQ(wsaaValuesInner.wsaaDvd, ZERO)) {
			goTo(GotoLabel.osInt7764);
		}
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaDvd);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[5]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[5]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[5]);
		lifacmvrec.glsign.set(wsaaT5645Sign[5]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[5]);
		if (isNE(hdisIO.getSacscode(),SPACES)) {
			lifacmvrec.sacscode.set(hdisIO.getSacscode());
		}
		if (isNE(hdisIO.getSacstyp(),SPACES)) {
			lifacmvrec.sacstyp.set(hdisIO.getSacstyp());
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		callLifacmv6000();
		compute(wsaaValuesInner.wsaaNewInt, 0).set(sub(wsaaValuesInner.wsaaInt, wsaaValuesInner.wsaaHdisOsInt));
	}

protected void osInt7764()
	{
		/*    IF WSAA-HDIS-OS-INT         = ZERO*/
		if (isEQ(wsaaValuesInner.wsaaInt, ZERO)) {
			goTo(GotoLabel.bookNewInterest7765);
		}
		/*    MOVE WSAA-HDIS-OS-INT       TO LIFA-ORIGAMT.*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaInt);
		/*  DR Dividend Interest*/
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[7]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[7]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[7]);
			lifacmvrec.glsign.set(wsaaT5645Sign[7]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[7]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[6]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[6]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[6]);
			lifacmvrec.glsign.set(wsaaT5645Sign[6]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[6]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void bookNewInterest7765()
	{
		/*  DR Outstanding Interest*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaNewInt);
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[9]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[9]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[9]);
			lifacmvrec.glsign.set(wsaaT5645Sign[9]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[9]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[8]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[8]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[8]);
			lifacmvrec.glsign.set(wsaaT5645Sign[8]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[8]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
		/* DR Interest Suspense (Withdrawal)*/
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[27]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[27]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[27]);
			lifacmvrec.glsign.set(wsaaT5645Sign[27]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[27]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[26]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[26]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[26]);
			lifacmvrec.glsign.set(wsaaT5645Sign[26]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[26]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
		/* Write HDIV to denote the declaration of the Outstanding*/
		/* Interest.*/
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(ovrduerec.chdrcoy);
		hdivIO.setChdrnum(ovrduerec.chdrnum);
		hdivIO.setLife(ovrduerec.life);
		hdivIO.setCoverage(ovrduerec.coverage);
		hdivIO.setRider(ovrduerec.rider);
		hdivIO.setPlanSuffix(ovrduerec.planSuffix);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setEffdate(ovrduerec.effdate);
		hdivIO.setDivdAllocDate(ovrduerec.effdate);
		/*    MOVE OVRD-COMPANY           TO HDIV-BATCCOY.*/
		hdivIO.setBatccoy(ovrduerec.chdrcoy);
		hdivIO.setBatcbrn(ovrduerec.batcbrn);
		hdivIO.setBatcactyr(ovrduerec.acctyear);
		hdivIO.setBatcactmn(ovrduerec.acctmonth);
		hdivIO.setBatctrcde(ovrduerec.trancode);
		hdivIO.setBatcbatch(ovrduerec.batcbatch);
		hdivIO.setTranno(ovrduerec.tranno);
		hdivIO.setDivdIntCapDate(99999999);
		hdivIO.setCntcurr(ovrduerec.cntcurr);
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(99999999);
		hdivIO.setDivdType("I");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setDivdAmount(wsaaValuesInner.wsaaNewInt);
		hdivIO.setFormat(formatsInner.hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			dbError9100();
		}
	}

protected void updateHdis7768()
	{
			start7768();
		}

protected void start7768()
	{
		/* if the previous read is not OK, skipping updating               */
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			return ;
		}
		setNextInterestDate7769a();
		hdisIO.setNextIntDate(wsaaNextIntDate);
		hdisIO.setLastIntDate(ovrduerec.ptdate);
		hdisIO.setValidflag("1");
		hdisIO.setTranno(ovrduerec.tranno);
		hdisIO.setOsInterest(ZERO);
		if (isGT(wsaaValuesInner.wsaaNewDvd, 0)) {
			setPrecision(hdisIO.getBalAtLastDivd(), 2);
			hdisIO.setBalAtLastDivd(add(hdisIO.getBalAtLastDivd(), wsaaValuesInner.wsaaNewDvd));
			setPrecision(hdisIO.getBalSinceLastCap(), 2);
			hdisIO.setBalSinceLastCap(add(hdisIO.getBalSinceLastCap(), wsaaValuesInner.wsaaNewDvd));
			hdisIO.setLastDivdDate(wsaaNewBonusDate);
		}
		hdisIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			dbError9100();
		}
	}

protected void setNextInterestDate7769a()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start7769a();
				case datcon27769a: 
					datcon27769a();
				case exit7769a: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7769a()
	{
		/* This paragraph sets the Next Dividend Interest Date*/
		wsaaNextIntDate.set(hdisIO.getLastIntDate());
		/* First, try using DATCON4, if not O-K, use DATCON2.*/
		datcon4rec.intDate1.set(wsaaNextIntDate);
		datcon4rec.frequency.set(th501rec.freqcy02);
		datcon4rec.freqFactor.set(1);
		datcon4rec.billmonth.set(hdisIO.getIntDueMm());
		datcon4rec.billday.set(hdisIO.getIntDueDd());
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			goTo(GotoLabel.datcon27769a);
		}
		wsaaNextIntDate.set(datcon4rec.intDate2);
		/* If not yet greater than the PTD, use DATCON4 to advance the*/
		/* next interest date.*/
		if (isLTE(wsaaNextIntDate,ovrduerec.ptdate)) {
			while ( !(isGT(wsaaNextIntDate,ovrduerec.ptdate))) {
				datcon4rec.intDate1.set(wsaaNextIntDate);
				datcon4rec.frequency.set(th501rec.freqcy02);
				datcon4rec.freqFactor.set(1);
				datcon4rec.billmonth.set(hdisIO.getIntDueMm());
				datcon4rec.billday.set(hdisIO.getIntDueDd());
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				if (isNE(datcon4rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon4rec.datcon4Rec);
					syserrrec.statuz.set(datcon4rec.statuz);
					systemError9000();
				}
				wsaaNextIntDate.set(datcon4rec.intDate2);
			}
			
		}
		/* If DATCON4 is used, the WSAA-NEXT-INT-DATE should be valid.*/
		/* Therefore, exit this paragraph now.*/
		goTo(GotoLabel.exit7769a);
	}

protected void datcon27769a()
	{
		datcon2rec.intDate1.set(hdisIO.getLastIntDate());
		datcon2rec.frequency.set(th501rec.freqcy02);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError9000();
		}
		wsaaNextIntDate.set(datcon2rec.intDate2);
		/* If not yet greater than the PTD, use DATCON2 to advance the*/
		/* next interest date.*/
		if (isLTE(wsaaNextIntDate,ovrduerec.ptdate)) {
			while ( !(isGT(wsaaNextIntDate,ovrduerec.ptdate))) {
				datcon2rec.intDate1.set(wsaaNextIntDate);
				datcon2rec.frequency.set(th501rec.freqcy02);
				datcon2rec.freqFactor.set(1);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					systemError9000();
				}
				wsaaNextIntDate.set(datcon2rec.intDate2);
			}
			
		}
		if (isNE(th501rec.hfixdd02,SPACES)) {
			wsaaNextintDay.set(th501rec.hfixdd02);
		}
		if (isNE(th501rec.hfixmm02,SPACES)) {
			if (isGT(th501rec.hfixmm02,wsaaNextintMth)) {
				wsaaNextintYear.subtract(1);
			}
			wsaaNextintMth.set(th501rec.hfixmm02);
		}
		/*    Ensure this Next Interest Allocation date is valid*/
		datcon1rec.intDate.set(wsaaNextIntDate);
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			wsaaNextintDay.set(wsaaLastdd[wsaaNextintMth.toInt()]);
			if (isEQ(wsaaNextintMth,2)
			&& (setPrecision(wsaaNextintYear, 0)
			&& isEQ((mult((div(wsaaNextintYear,4)),4)),wsaaNextintYear))
			&& isNE(wsaaNextintYear,1900)) {
				wsaaNextintDay.add(1);
			}
		}
	}

protected void rpuPremium7770()
	{
			start7771();
		}

protected void start7771()
	{
		/*  CR Single Premium*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaRpuCost);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[11]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[11]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[11]);
			lifacmvrec.glsign.set(wsaaT5645Sign[11]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[11]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlan.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[10]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[10]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[10]);
			lifacmvrec.glsign.set(wsaaT5645Sign[10]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[10]);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void refund7780()
	{
		/*START*/
		/*  CR Suspense for Refund*/
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaRefund);
		if (isEQ(lifacmvrec.origamt,ZERO)) {
			return ;
		}
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[12]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[12]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[12]);
		lifacmvrec.glsign.set(wsaaT5645Sign[12]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[12]);
		callLifacmv6000();
		/*EXIT*/
	}

protected void commClawback7790()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start7791();
				case callAgcmio7794: 
					callAgcmio7794();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start7791()
	{
		/* Read the first AGCM record for this contract.*/
		agcmIO.setParams(SPACES);
		agcmIO.setChdrnum(ovrduerec.chdrnum);
		agcmIO.setChdrcoy(ovrduerec.chdrcoy);
		agcmIO.setPlanSuffix(ZERO);
		agcmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		agcmIO.setFormat(formatsInner.agcmrec);
	}

protected void callAgcmio7794()
	{
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(),varcom.oK)
		&& isNE(agcmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			dbError9100();
		}
		/* If the end of the AGCM file has been reached or an AGCM*/
		/* record was read but not for the contract being processed,*/
		/* then exit the section.*/
		if (isEQ(agcmIO.getStatuz(),varcom.endp)
		|| isNE(agcmIO.getChdrcoy(),ovrduerec.chdrcoy)
		|| isNE(agcmIO.getChdrnum(),ovrduerec.chdrnum)) {
			return ;
		}
		/* for the AGCM record just read.*/
		if (isEQ(agcmIO.getChdrcoy(),ovrduerec.chdrcoy)
		&& isEQ(agcmIO.getChdrnum(),ovrduerec.chdrnum)
		&& isEQ(agcmIO.getLife(),ovrduerec.life)
		&& isEQ(agcmIO.getCoverage(),ovrduerec.coverage)
		&& isEQ(agcmIO.getRider(),ovrduerec.rider)
		&& isEQ(agcmIO.getPlanSuffix(),ovrduerec.planSuffix)
		&& isNE(agcmIO.getCompay(),agcmIO.getComern())) {
			updateAgcmFile8000();
		}
		/* Loop round to see if there any other AGCM records to*/
		/* process for the contract.*/
		agcmIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callAgcmio7794);
	}

protected void updateAgcmFile8000()
	{
		readh8010();
		rewrt8020();
		writr8030();
	}

	/**
	* <pre>
	* previous processing loop on AGCM is not upset.
	* created with commission paid set to earned, valid-flag of "1"
	* and correct tranno. Also create accounts records (ACMV).
	* </pre>
	*/
protected void readh8010()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmIO.getAgntnum());
		agcmdbcIO.setLife(agcmIO.getLife());
		agcmdbcIO.setCoverage(agcmIO.getCoverage());
		agcmdbcIO.setRider(agcmIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError9100();
		}
	}

protected void rewrt8020()
	{
		agcmdbcIO.setValidflag("2");
		agcmdbcIO.setCurrto(ovrduerec.effdate);
		agcmdbcIO.setFunction(varcom.rewrt);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError9100();
		}
	}

protected void writr8030()
	{
		/* correct tranno, and new commission paid.*/
		compute(wsaaValuesInner.wsaaCommRecovered, 2).set(sub(agcmIO.getCompay(), agcmIO.getComern()));
		if (isEQ(agcmIO.getOvrdcat(),"O")) {
			overrideCommAcmv8200();
		}
		else {
			commClawbackAcmv8100();
		}
		agcmdbcIO.setCompay(agcmIO.getComern());
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setTranno(ovrduerec.tranno);
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writr);
		agcmdbcIO.setFormat(formatsInner.agcmdbcrec);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			dbError9100();
		}
	}

protected void commClawbackAcmv8100()
	{
			para8110();
		}

protected void para8110()
	{
		if (isEQ(wsaaValuesInner.wsaaCommRecovered, 0)) {
			return ;
		}
		/* Write a ACMV for the clawback commission.*/
		lifacmvrec.rdocnum.set(ovrduerec.chdrnum);
		lifacmvrec.batccoy.set(ovrduerec.chdrcoy);
		lifacmvrec.rldgcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.batcactyr.set(ovrduerec.acctyear);
		lifacmvrec.batctrcde.set(ovrduerec.trancode);
		lifacmvrec.batcactmn.set(ovrduerec.acctmonth);
		lifacmvrec.batcbatch.set(ovrduerec.batcbatch);
		lifacmvrec.batcbrn.set(ovrduerec.batcbrn);
		lifacmvrec.tranno.set(ovrduerec.tranno);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(ovrduerec.ptdate);
		lifacmvrec.tranref.set(ovrduerec.chdrnum);
		lifacmvrec.user.set(ovrduerec.user);
		lifacmvrec.trandesc.set(wsaaTransDesc);
		lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
		lifacmvrec.transactionDate.set(ovrduerec.tranDate);
		lifacmvrec.transactionTime.set(ovrduerec.tranTime);
		lifacmvrec.termid.set(ovrduerec.termid);
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaCommRecovered);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[17]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[17]);
		lifacmvrec.glsign.set(wsaaT5645Sign[17]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[17]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[17]);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.function.set("PSTW");
		callLifacmv6000();
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmIO.getAnnprem());
			zorlnkrec.effdate.set(agcmIO.getEfdate());
			a100CallZorcompy();
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[20]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[20]);
			lifacmvrec.glsign.set(wsaaT5645Sign[20]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[20]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[20]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlansuff.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[18]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[18]);
			lifacmvrec.glsign.set(wsaaT5645Sign[18]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[18]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[18]);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(ovrduerec.chdrnum);
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		callLifacmv6000();
	}

protected void overrideCommAcmv8200()
	{
			para8210();
		}

protected void para8210()
	{
		if (isEQ(wsaaValuesInner.wsaaCommRecovered, 0)) {
			return ;
		}
		/* Write a ACMV for the clawback commission.*/
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.rdocnum.set(ovrduerec.chdrnum);
		lifacmvrec.batccoy.set(ovrduerec.chdrcoy);
		lifacmvrec.rldgcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.genlcoy.set(ovrduerec.chdrcoy);
		lifacmvrec.batcactyr.set(ovrduerec.acctyear);
		lifacmvrec.batctrcde.set(ovrduerec.trancode);
		lifacmvrec.batcactmn.set(ovrduerec.acctmonth);
		lifacmvrec.batcbatch.set(ovrduerec.batcbatch);
		lifacmvrec.batcbrn.set(ovrduerec.batcbrn);
		lifacmvrec.tranno.set(ovrduerec.tranno);
		lifacmvrec.origcurr.set(ovrduerec.cntcurr);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.frcdate.set(99999999);
		lifacmvrec.effdate.set(ovrduerec.ptdate);
		lifacmvrec.tranref.set(ovrduerec.chdrnum);
		lifacmvrec.user.set(ovrduerec.user);
		lifacmvrec.rldgacct.set(agcmIO.getAgntnum());
		lifacmvrec.transactionDate.set(ovrduerec.tranDate);
		lifacmvrec.transactionTime.set(ovrduerec.tranTime);
		lifacmvrec.termid.set(ovrduerec.termid);
		lifacmvrec.origamt.set(wsaaValuesInner.wsaaCommRecovered);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[19]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[19]);
		lifacmvrec.glsign.set(wsaaT5645Sign[19]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[19]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[19]);
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.function.set("PSTW");
		callLifacmv6000();
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[22]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[22]);
			lifacmvrec.glsign.set(wsaaT5645Sign[22]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[22]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[22]);
			wsaaRldgChdrnum.set(ovrduerec.chdrnum);
			wsaaRldgLife.set(ovrduerec.life);
			wsaaRldgCoverage.set(ovrduerec.coverage);
			wsaaRldgRider.set(ovrduerec.rider);
			wsaaPlansuff.set(ovrduerec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(ovrduerec.crtable);
		}
		else {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[20]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[20]);
			lifacmvrec.glsign.set(wsaaT5645Sign[20]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[20]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[20]);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(ovrduerec.chdrnum);
		}
		lifacmvrec.substituteCode[1].set(ovrduerec.cnttype);
		lifacmvrec.function.set("PSTW");
		callLifacmv6000();
	}

protected void writeRh5478300()
	{
		start8310();
	}

protected void start8310()
	{
		if (newPageReq.isTrue()) {
			/*        MOVE SPACES             TO RH547H01-O*/
			printerFile.printRh547h01(rh547h01O, indicArea);
			wsaaOverflow.set("N");
		}
		rh547d01O.set(SPACES);
		/* Format print line.*/
		rd01Chdrnum.set(ovrduerec.chdrnum);
		rd01Cnttype.set(ovrduerec.cnttype);
		rd01Cntcurr.set(ovrduerec.cntcurr);
		rd01Surrval.set(wsaaValuesInner.wsaaTotSv);
		rd01Loanorigam.set(wsaaValuesInner.wsaaTotLoan);
		rd01Refundfe.set(wsaaValuesInner.wsaaRefund);
		rd01Descrip.set(wsaaException);
		printerFile.printRh547d01(rh547d01O, indicArea);
	}

protected void openPrinter8800()
	{
		try {
			openFiles8810();
			setUpHeadingCompany8820();
			setUpHeadingBranch8830();
			setUpHeadingDates8840();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void openFiles8810()
	{
		if (isEQ(wsaaPrtOpen,"Y")) {
			goTo(GotoLabel.exit8890);
		}
		/* Open required files.*/
		printerFile.openOutput();
	}

protected void setUpHeadingCompany8820()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(tablesInner.t1693);
		descIO.setDescitem(ovrduerec.chdrcoy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(ovrduerec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			dbError9100();
		}
		rh01Company.set(ovrduerec.chdrcoy);
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch8830()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ovrduerec.chdrcoy);
		descIO.setDesctabl(tablesInner.t1692);
		descIO.setDescitem(ovrduerec.batcbrn);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(ovrduerec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			dbError9100();
		}
		rh01Branch.set(ovrduerec.batcbrn);
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates8840()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(ovrduerec.effdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
		printerFile.printRh547h01(rh547h01O, indicArea);
		wsaaOverflow.set("N");
		/* Flag that the printer has been opened.*/
		wsaaPrtOpen = "Y";
	}

protected void close8900()
	{
		/*CLOSE-FILES*/
		/*  Close any open files.*/
		/*    CLOSE PRINTER-FILE.*/
		ovrduerec.statuz.set(varcom.oK);
		/*EXIT*/
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
				case seExit9090: 
					seExit9090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		ovrduerec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9100();
				case dbExit9190: 
					dbExit9190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		ovrduerec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void a100CallZorcompy()
	{
		a110Start();
	}

protected void a110Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.ptdate.set(ovrduerec.ptdate);
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(ovrduerec.tranno);
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set("Y");
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			systemError9000();
		}
	}

protected void a200CallRounding()
	{
		/*A210-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(ovrduerec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(ovrduerec.cntcurr);
		zrdecplrec.batctrcde.set(ovrduerec.trancode);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError9000();
		}
		/*A290-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-VALUES--INNER
 */
private static final class WsaaValuesInner { 
		/* WSAA-VALUES */
	private PackedDecimalData wsaaBasicCv = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaPuaCv = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaDvd = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaInt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaNewInt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaHdisOsInt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaTotSv = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaTotLoan = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaCommRecovered = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaRpuSi = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaRpuCost = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaRefund = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaAccumValue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaNewDvd = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaBasicDvd = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaDivdRate = new PackedDecimalData(8, 5);
	private PackedDecimalData wsaaRateDate = new PackedDecimalData(8, 0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t1688 = new FixedLengthStringData(6).init("T1688");
	private FixedLengthStringData t1692 = new FixedLengthStringData(6).init("T1692");
	private FixedLengthStringData t1693 = new FixedLengthStringData(6).init("T1693");
	private FixedLengthStringData t5645 = new FixedLengthStringData(6).init("T5645");
	private FixedLengthStringData t5679 = new FixedLengthStringData(6).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(6).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(6).init("T5688");
	private FixedLengthStringData t6598 = new FixedLengthStringData(6).init("T6598");
	private FixedLengthStringData t6639 = new FixedLengthStringData(6).init("T6639");
	private FixedLengthStringData th506 = new FixedLengthStringData(6).init("TH506");
	private FixedLengthStringData th546 = new FixedLengthStringData(6).init("TH546");
	private FixedLengthStringData th548 = new FixedLengthStringData(6).init("TH548");
	private FixedLengthStringData th605 = new FixedLengthStringData(6).init("TH605");
	private FixedLengthStringData th501 = new FixedLengthStringData(6).init("TH501");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData agcmdbcrec = new FixedLengthStringData(10).init("AGCMDBCREC");
	private FixedLengthStringData agcmrec = new FixedLengthStringData(10).init("AGCMREC   ");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData hdisrec = new FixedLengthStringData(10).init("HDISREC");
	private FixedLengthStringData hdivrec = new FixedLengthStringData(10).init("HDIVREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData hpuarec = new FixedLengthStringData(10).init("HPUAREC");
	private FixedLengthStringData hpuadoprec = new FixedLengthStringData(10).init("HPUADOPREC");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData f259 = new FixedLengthStringData(4).init("F259");
	private FixedLengthStringData e076 = new FixedLengthStringData(4).init("E076");
	private FixedLengthStringData e308 = new FixedLengthStringData(4).init("E308");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData h053 = new FixedLengthStringData(4).init("H053");
	private FixedLengthStringData h791 = new FixedLengthStringData(4).init("H791");
	private FixedLengthStringData hl37 = new FixedLengthStringData(4).init("HL37");
	private FixedLengthStringData z039 = new FixedLengthStringData(4).init("Z039");
}
}
