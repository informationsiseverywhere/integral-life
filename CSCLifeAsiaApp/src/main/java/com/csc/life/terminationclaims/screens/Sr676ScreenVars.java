package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR676
 * @version 1.0 generated on 30/08/09 07:23
 * @author Quipoz
 */
public class Sr676ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1498);
	public FixedLengthStringData dataFields = new FixedLengthStringData(494).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData anntind = DD.anntind.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,4);
	public ZonedDecimalData benCessDate = DD.bcesdte.copyToZonedDecimal().isAPartOf(dataFields,8);
	public ZonedDecimalData benCessAge = DD.bcessage.copyToZonedDecimal().isAPartOf(dataFields,16);
	public ZonedDecimalData benCessTerm = DD.bcesstrm.copyToZonedDecimal().isAPartOf(dataFields,19);
	public FixedLengthStringData benpln = DD.benpln.copy().isAPartOf(dataFields,22);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData coverdtl = DD.coverdtl.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,35);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData frqdesc = DD.frqdesc.copy().isAPartOf(dataFields,68);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,78);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,95);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,103);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,150);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,152);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,160);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,162);
	public FixedLengthStringData livesno = DD.livesno.copy().isAPartOf(dataFields,209);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,210);
	public ZonedDecimalData numapp = DD.numapp.copyToZonedDecimal().isAPartOf(dataFields,211);
	public ZonedDecimalData numavail = DD.numavail.copyToZonedDecimal().isAPartOf(dataFields,215);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,219);
	public FixedLengthStringData pbind = DD.pbind.copy().isAPartOf(dataFields,220);
	public ZonedDecimalData premCessDate = DD.pcesdte.copyToZonedDecimal().isAPartOf(dataFields,221);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,229);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,232);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,235);
	public FixedLengthStringData ratypind = DD.ratypind.copy().isAPartOf(dataFields,239);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,240);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,248);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,251);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,254);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,256);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,257);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,258);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,260);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,264);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,279);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,296);
	public FixedLengthStringData viewplan = DD.viewplan.copy().isAPartOf(dataFields,297);
	public FixedLengthStringData waiverprem = DD.waiverprem.copy().isAPartOf(dataFields,298);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,299);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,312);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,329);
	public ZonedDecimalData zunit = DD.zunit.copyToZonedDecimal().isAPartOf(dataFields,346);
	/*BRD-306 START */
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 348);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 351);
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 381);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 398);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 415);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 432);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 449);

	/*BRD-306 END */
	public FixedLengthStringData waitperiod = DD.waitperiod.copy().isAPartOf(dataFields,466);
	public FixedLengthStringData bentrm = DD.bentrm.copy().isAPartOf(dataFields,469);
	public FixedLengthStringData poltyp = DD.zpoltyp.copy().isAPartOf(dataFields,471);
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,472);
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,473);
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,490);//BRD-NBP-011
	public FixedLengthStringData exclind = DD.optextind.copy().isAPartOf(dataFields,493);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(248).isAPartOf(dataArea, 494);
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData anntindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData benplnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData coverdtlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData frqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData livesnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData numappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData numavailErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData pbindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData pcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData ratypindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData viewplanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData waiverpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData zunitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	/*BRD-306 START */
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);

	/*BRD-306 END */
	
	public FixedLengthStringData waitperiodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData bentrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData poltypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData zstpduty01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);//BRD-NBP-011
	public FixedLengthStringData exclindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(756).isAPartOf(dataArea, 742);
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] anntindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] benplnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] coverdtlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] frqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] livesnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] numappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] numavailOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] pbindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] pcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] ratypindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] viewplanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] waiverpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] zunitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	/*BRD-306 START */
	public FixedLengthStringData[]cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[]ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);

	/*BRD-306 END */
	
	public FixedLengthStringData[]waitperiodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[]bentrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	public FixedLengthStringData[]poltypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	public FixedLengthStringData[]zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);//BRD-NBP-011
	public FixedLengthStringData[] exclindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[] riskCessAgeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);//ILJ-43
	//public FixedLengthStringData[] riskCessDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 756);//ILJ-43
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData benCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);

	public LongData Sr676screenWritten = new LongData(0);
	public LongData Sr676protectWritten = new LongData(0);
	public FixedLengthStringData contDtCalcScreenflag = new FixedLengthStringData(1); 

	public boolean hasSubfile() {
		return false;
	}


	public Sr676ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {null, null, "42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqdescOut,new String[] {null, null, null, "55",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcessageOut,new String[] {"15","03","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesstrmOut,new String[] {"16","04","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesdteOut,new String[] {"29","28","-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcessageOut,new String[] {"17","24","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesdteOut,new String[] {"30","28","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcessageOut,new String[] {"17","24","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcesdteOut,new String[] {"30","28","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"19","06","-19","05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencdOut,new String[] {"20","08","-20","07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bappmethOut,new String[] {"39","41","-39","41",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"21","09","-21","09",null, null, null, null, null, null, null, null});
		fieldIndMap.put(instprmOut,new String[] {"22","31","-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ratypindOut,new String[] {"23","40","-01","40",null, null, null, null, null, null, null, null});
		fieldIndMap.put(anntindOut,new String[] {"34","35","-34","33",null, null, null, null, null, null, null, null});
		fieldIndMap.put(polincOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"23","26","-23","27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(numavailOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(numappOut,new String[] {"36","11","-36","10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(pbindOut,new String[] {"37","38","-37","37",null, null, null, null, null, null, null, null});
		fieldIndMap.put(livesnoOut,new String[] {"52","31","-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(coverdtlOut,new String[] {"51",null, "-51",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(waiverpremOut,new String[] {"53","31","-53",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zunitOut,new String[] {"14","02","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(benplnOut,new String[] {"32","02","-32",null, null, null, null, null, null, null, null, null});
		//ILIFE-1223 STARTS
		fieldIndMap.put(taxamtOut,new String[] {null, "46", null, "44",null, null, null, null, null, null, null, null});
		//ILIFE-1223 ENDS
		fieldIndMap.put(taxindOut,new String[] {"43","42","-43","42",null, null, null, null, null, null, null, null});
		/*BRD-306 START */
		fieldIndMap.put(cnttypeOut,new String[] {null, null, null, "47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypedesOut,new String[] {null, null, null, "48", null, null, null, null, null, null, null, null});
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "56", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "57", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "58", null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "59", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "60", null, null, null, null, null, null, null, null});
		fieldIndMap.put(waitperiodOut,new String[] {"71", "65", "-71", "75", null, null, null, null, null, null, null, null});
		fieldIndMap.put(bentrmOut,new String[] {"72", "66", "-72", "76", null, null, null, null, null, null, null, null});
		fieldIndMap.put(poltypOut,new String[] {"73", "67", "-73", "77", null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmbasisOut,new String[] {"74", "68", "-74", "78", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zstpduty01Out,new String[] {"75", "69", "-75", "79", null, null, null, null, null, null, null, null});
	/*BRD-306 END */
		/*BRD-306 END */
		fieldIndMap.put(dialdownoptionOut,new String[] {"82", "80", "-82", "81", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(exclindOut,new String[] {null, null, null, "83", null, null, null, null, null, null, null, null});
		//fieldIndMap.put(riskCessAgeOut,new String[] {null, null, null, "123", null, null, null, null, null, null, null, null});//ILJ-43
		//fieldIndMap.put(riskCessDateOut,new String[] {null, null, null, "124", null, null, null, null, null, null, null, null});//ILJ-43

		screenFields = new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifcnum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, frqdesc, currcd, riskCessAge, riskCessTerm, riskCessDate, premCessAge, premCessTerm, premCessDate, benCessAge, benCessTerm, benCessDate, mortcls, liencd, bappmeth, optextind, instPrem, ratypind, anntind, polinc, select, numavail, numapp, pbind, zlinstprem, livesno, coverdtl, viewplan, waiverprem, sumin, zunit, benpln, taxamt, taxind, loadper, rateadj, fltmort, premadj, cnttype, ctypedes,adjustageamt,waitperiod,bentrm,poltyp,prmbasis,dialdownoption,exclind,zstpduty01};
		screenOutFields = new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, frqdescOut, currcdOut, rcessageOut, rcesstrmOut, rcesdteOut, pcessageOut, pcesstrmOut, pcesdteOut, bcessageOut, bcesstrmOut, bcesdteOut, mortclsOut, liencdOut, bappmethOut, optextindOut, instprmOut, ratypindOut, anntindOut, polincOut, selectOut, numavailOut, numappOut, pbindOut, zlinstpremOut, livesnoOut, coverdtlOut, viewplanOut, waiverpremOut, suminOut, zunitOut, benplnOut, taxamtOut, taxindOut,loadperOut, rateadjOut, rateadjOut, premadjOut, cnttypeOut , ctypedesOut,adjustageamtOut,waitperiodOut,bentrmOut,poltypOut,prmbasisOut,dialdownoptionOut,exclindOut,zstpduty01Out,riskCessAgeOut};
		screenErrFields = new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, frqdescErr, currcdErr, rcessageErr, rcesstrmErr, rcesdteErr, pcessageErr, pcesstrmErr, pcesdteErr, bcessageErr, bcesstrmErr, bcesdteErr, mortclsErr, liencdErr, bappmethErr, optextindErr, instprmErr, ratypindErr, anntindErr, polincErr, selectErr, numavailErr, numappErr, pbindErr, zlinstpremErr, livesnoErr, coverdtlErr, viewplanErr, waiverpremErr, suminErr, zunitErr, benplnErr, taxamtErr, taxindErr,loadperErr, rateadjErr, fltmortErr, premadjErr, cnttypeErr, ctypedesErr,adjustageamtErr,waitperiodErr,bentrmErr,poltypErr,prmbasisErr,dialdownoptionErr,exclindErr,zstpduty01Err};
		screenDateFields = new BaseData[] {riskCessDate, premCessDate, benCessDate};
		screenDateErrFields = new BaseData[] {rcesdteErr, pcesdteErr, bcesdteErr};
		screenDateDispFields = new BaseData[] {riskCessDateDisp, premCessDateDisp, benCessDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr676screen.class;
		protectRecord = Sr676protect.class;
	}

}
