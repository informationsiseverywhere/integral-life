package com.csc.life.terminationclaims.dataaccess.dao;


import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Crsvpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CrsvpfDAO extends BaseDAO<Crsvpf> {
	
	public void insertCrsvRecord(Crsvpf clmrpf);
	
	public void updateValidFlgCrsvRecord(Crsvpf clmrpf, String validflag);
	
	public List<Crsvpf> getCrsvpfRecord(String chdrnum, String wclmcoy, String validflag, String trcode);
	
	public List<Crsvpf> getCrsvpfRecordByTranno(String chdrnum, String wclmcoy, String validflag, String trcode,String tranno);
	
	public void deleteCrsvpfRecord(long UniqueNumber);
	
	public Crsvpf getRecord(String chdrnum);//ILJ-798
	
	public void updateCrsvRecord(Crsvpf crsvpfItem);//ILJ-798
}
