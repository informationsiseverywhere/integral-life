package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.terminationclaims.dataaccess.model.Surdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface SurdpfDAO extends BaseDAO<Surdpf> {

	Map<String, List<Surdpf>> getSurdpfMap(String coy, List<String> chdrnumList);

	void insertSurdpfList(List<Surdpf> pfList);
	//ILIFE-4406
    public List<Surdpf> searchSurdclmRecord(String chdrcoy,String chdrnum,String life,String coverage,String rider,int planSuffix,int tranno);
    public void insertSurdclmpfDataBulk(List<Surdpf> surdpfList);
    public void insertSurdpfRecord(Surdpf s);
    public List<Surdpf> searchSurdclmRecordList(String chdrcoy,String chdrnum,int tranno);

}
