package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.csc.life.terminationclaims.dataaccess.dao.ClmhpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Clmhpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ClmhpfDAOImpl extends BaseDAOImpl<Clmhpf> implements ClmhpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClmhpfDAOImpl.class);
	
	private static final String SQLGETCLMHPDATA = "select * from Clmhpf t where t.CHDRCOY = ? and t.CHDRNUM = ?";
	private List<Clmhpf> getClmhpfData(String chdrCoy, String chdrNum,
			String validFlag) {
		List<Clmhpf> result = new ArrayList<Clmhpf>();
		StringBuilder sqlBuilder = new StringBuilder(SQLGETCLMHPDATA);
		if (validFlag != null && !"".equals(validFlag)) {
			sqlBuilder.append(" and t.VALIDFLAG = ?");
		}
		PreparedStatement ps = getPrepareStatement(sqlBuilder.toString());
		ResultSet sql1rs = null;
		try {
			ps.setString(1, chdrCoy);
			ps.setString(2, chdrNum);
			if (validFlag != null && !"".equals(validFlag)) {
				ps.setString(3, validFlag);
			}
			sql1rs = ps.executeQuery();
			BeanPropertyRowMapper<Clmhpf> mapper = BeanPropertyRowMapper.newInstance(Clmhpf.class);
			int i = 1;
			while (sql1rs.next()) {
				Clmhpf clmhpf = mapper.mapRow(sql1rs, i);
				result.add(clmhpf);
				i++;
			}
		} catch (SQLException e) {
            LOGGER.error("getClmhpfData()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, sql1rs);
        }
        
		return result;
	}

	@Override
	public Clmhpf getClmhpfH(String chdrCoy, String chdrNum) {
		List<Clmhpf> result = getClmhpfData(chdrCoy, chdrNum, "1");
		if (result.size() >= 1) {
			return result.get(0);
		}
		return null;
	}

	private static final String UpdateClmhpfValidFlagSQL = "update Clmhpf set VALIDFLAG = ? where UNIQUE_NUMBER = ?";
	@Override
	public int updateClmhpfValidFlag(long uniqueNumber, String validFlag) {
		PreparedStatement ps = getPrepareStatement(UpdateClmhpfValidFlagSQL);
		int num = 0;
		try {
			ps.setString(1, validFlag);
			ps.setLong(2, uniqueNumber);
			num = ps.executeUpdate();
		} catch (SQLException e) {
            LOGGER.error("updateClmhpfValidFlag()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
		return num;
	}
	 public void deleteRcdByChdrnum(String chdrcoy,String chdrnum) {
		 StringBuilder sb = new StringBuilder("");
			sb.append("DELETE FROM CLMHPF");
			sb.append(" WHERE CHDRCOY=?  AND CHDRNUM=? AND VALIDFLAG = '1' ");
			LOGGER.info(sb.toString());
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sb.toString());	

				    ps.setString(1, chdrcoy);
				    ps.setString(2, chdrnum);
				    ps.executeUpdate();				
			}catch (SQLException e) {
				LOGGER.error("deleteRcdByChdrnum()", e);	
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}				
	 }
	public int updateClaimStatus(String chdrcoy,String chdrnum,String clamstat) {

			String SQL_CLMH_UPDATE = "UPDATE CLMHPF SET CLAMSTAT=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE CHDRCOY=? AND CHDRNUM = ? AND VALIDFLAG = '1'";

			PreparedStatement psClmhpdate = getPrepareStatement(SQL_CLMH_UPDATE);
			int count;
			try {
				psClmhpdate.setString(1, clamstat);
				psClmhpdate.setString(2, getJobnm());
				psClmhpdate.setString(3, getUsrprf());
				psClmhpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				psClmhpdate.setString(5, chdrcoy);
				psClmhpdate.setString(6, chdrnum);	

				count = psClmhpdate.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error("updateClaimStatus()",e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psClmhpdate, null);
			}
			return count;
		}
}
