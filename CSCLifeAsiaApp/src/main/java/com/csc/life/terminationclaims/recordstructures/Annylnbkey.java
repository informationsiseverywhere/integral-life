package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:16
 * Description:
 * Copybook name: ANNYLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Annylnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData annylnbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData annylnbKey = new FixedLengthStringData(64).isAPartOf(annylnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData annylnbChdrcoy = new FixedLengthStringData(1).isAPartOf(annylnbKey, 0);
  	public FixedLengthStringData annylnbChdrnum = new FixedLengthStringData(8).isAPartOf(annylnbKey, 1);
  	public FixedLengthStringData annylnbLife = new FixedLengthStringData(2).isAPartOf(annylnbKey, 9);
  	public FixedLengthStringData annylnbCoverage = new FixedLengthStringData(2).isAPartOf(annylnbKey, 11);
  	public FixedLengthStringData annylnbRider = new FixedLengthStringData(2).isAPartOf(annylnbKey, 13);
  	public PackedDecimalData annylnbPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(annylnbKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(annylnbKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(annylnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		annylnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}