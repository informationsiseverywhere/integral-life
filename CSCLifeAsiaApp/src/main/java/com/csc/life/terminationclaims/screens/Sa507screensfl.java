package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class Sa507screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {8, 4, 5, 6, 7, 2, 3, 10, 9, 11}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa507ScreenVars sv = (Sa507ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sa507screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sa507screensfl, 
			sv.Sa507screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sa507ScreenVars sv = (Sa507ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sa507screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sa507ScreenVars sv = (Sa507ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sa507screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sa507screensflWritten.gt(0))
		{
			sv.sa507screensfl.setCurrentIndex(0);
			sv.Sa507screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sa507ScreenVars sv = (Sa507ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sa507screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa507ScreenVars screenVars = (Sa507ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");				
				screenVars.select.setFieldName("select");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.cnttype.setFieldName("cnttype");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.cltype.setFieldName("cltype");
				screenVars.riskCommDate.setFieldName("riskCommDate");
				screenVars.riskCessDate.setFieldName("riskCessDate");
				screenVars.riskCommDateDisp.setFieldName("riskCommDateDisp");
				screenVars.riskCessDateDisp.setFieldName("riskCessDateDisp");
				screenVars.slt.setFieldName("slt");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.chdrnum.set(dm.getField("chdrnum"));
			screenVars.cnttype.set(dm.getField("cnttype"));
			screenVars.statcode.set(dm.getField("statcode"));
			screenVars.pstatcode.set(dm.getField("pstatcode"));
			screenVars.longdesc.set(dm.getField("longdesc"));
			screenVars.cltype.set(dm.getField("cltype"));
			screenVars.riskCommDate.set(dm.getField("riskCommDate"));
			screenVars.riskCessDate.set(dm.getField("riskCessDate"));
			screenVars.riskCommDateDisp.set(dm.getField("riskCommDateDisp"));
			screenVars.riskCessDateDisp.set(dm.getField("riskCessDateDisp"));
			screenVars.slt.set(dm.getField("slt"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa507ScreenVars screenVars = (Sa507ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.chdrnum.setFieldName("chdrnum");
				screenVars.cnttype.setFieldName("cnttype");
				screenVars.statcode.setFieldName("statcode");
				screenVars.pstatcode.setFieldName("pstatcode");
				screenVars.longdesc.setFieldName("longdesc");
				screenVars.cltype.setFieldName("cltype");
				screenVars.riskCommDate.setFieldName("riskCommDate");
				screenVars.riskCessDate.setFieldName("riskCessDate");
				screenVars.riskCommDateDisp.setFieldName("riskCommDateDisp");
				screenVars.riskCessDateDisp.setFieldName("riskCessDateDisp");
				screenVars.slt.setFieldName("slt");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("chdrnum").set(screenVars.chdrnum);
			dm.getField("cnttype").set(screenVars.cnttype);
			dm.getField("statcode").set(screenVars.statcode);
			dm.getField("pstatcode").set(screenVars.pstatcode);
			dm.getField("longdesc").set(screenVars.longdesc);
			dm.getField("cltype").set(screenVars.cltype);
			dm.getField("riskCommDate").set(screenVars.riskCommDate);
			dm.getField("riskCessDate").set(screenVars.riskCessDate);
			dm.getField("riskCommDateDisp").set(screenVars.riskCommDateDisp);
			dm.getField("riskCessDateDisp").set(screenVars.riskCessDateDisp);
			dm.getField("slt").set(screenVars.slt);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sa507screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sa507ScreenVars screenVars = (Sa507ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.chdrnum.clearFormatting();
		screenVars.cnttype.clearFormatting();
		screenVars.statcode.clearFormatting();
		screenVars.pstatcode.clearFormatting();
		screenVars.longdesc.clearFormatting();
		screenVars.cltype.clearFormatting();
		screenVars.riskCommDate.clearFormatting();
		screenVars.riskCessDate.clearFormatting();
		screenVars.riskCommDateDisp.clearFormatting();
		screenVars.riskCessDateDisp.clearFormatting();
		screenVars.slt.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sa507ScreenVars screenVars = (Sa507ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.statcode.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.cltype.setClassString("");
		screenVars.riskCommDate.setClassString("");
		screenVars.riskCessDate.setClassString("");
		screenVars.riskCommDateDisp.setClassString("");
		screenVars.riskCessDateDisp.setClassString("");
		screenVars.slt.setClassString("");
	}

/**
 * Clear all the variables in S5186screensfl
 */
	public static void clear(VarModel pv) {
		Sa507ScreenVars screenVars = (Sa507ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.statcode.clear();
		screenVars.pstatcode.clear();
		screenVars.longdesc.clear();
		screenVars.cltype.clear();
		screenVars.riskCommDate.clear();
		screenVars.riskCessDate.clear();
		screenVars.riskCommDateDisp.clear();
		screenVars.riskCessDateDisp.clear();
		screenVars.slt.clear();
	}
}
