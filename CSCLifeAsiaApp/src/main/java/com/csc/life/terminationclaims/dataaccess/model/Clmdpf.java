package com.csc.life.terminationclaims.dataaccess.model;



public class Clmdpf {
   
    private String chdrcoy;
    private String chdrnum;
    private int tranno;
    private String life;
    private String jlife;
    private String coverage;
    private String rider;
    private String crtable;
    private String shortds;
    private String liencd;
    private String cnstcur;
    private double estMatValue;
    private double actvalue;
    private String virtualFund;
    private String fieldType;
    private String annypind;
    private String userProfile;
    private String jobName;
    private String datime;
    private String claimno;
    private String claimnotifino;
    private long unique_number;
    private String validflag;
       public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public long getUniqueNumber() {
        return unique_number;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.unique_number = uniqueNumber;
    }
    public String getAnnypind() {
        return annypind;
    }
    public void setAnnypind(String annypind) {
        this.annypind = annypind;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public String getLife() {
        return life;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public String getCoverage() {
        return coverage;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public String getRider() {
        return rider;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
   
    public double getEstMatValue() {
        return estMatValue;
    }
    public void setEstMatValue(double estMatValue) {
        this.estMatValue = estMatValue;
    }
    
    public double getActvalue() {
        return actvalue;
    }
    public void setActvalue(double actvalue) {
        this.actvalue = actvalue;
    }
    
    public String getFieldType() {
        return fieldType;
    }
    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }
    public String getVirtualFund() {
        return virtualFund;
    }
    public void setVirtualFund(String virtualFund) {
        this.virtualFund = virtualFund;
    }
    public String getCnstcur() {
        return cnstcur;
    }
    public void setCnstcur(String cnstcur) {
        this.cnstcur = cnstcur;
    }
    
   
    
    public String getLiencd() {
        return liencd;
    }
    public void setLiencd(String liencd) {
        this.liencd = liencd;
    }
    public String getShortds() {
        return shortds;
    }
    public void setShortds(String shortds) {
        this.shortds = shortds;
    }
    public String getCrtable() {
        return crtable;
    }
    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }
    
    public String getJlife() {
        return jlife;
    }
    public void setJlife(String jlife) {
        this.jlife = jlife;
    }
    
    public int getTranno() {
        return tranno;
    }
    public void setTranno(int tranno) {
        this.tranno = tranno;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public String getJobName() {
        return jobName;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public String getDatime() {
        return datime;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
    public String getClaimno() {
        return claimno;
    }
    public void setClaimno(String claimno) {
        this.claimno = claimno;
    }
    public String getClaimnotifino() {
        return claimnotifino;
    }
    public void setClaimnotifino(String claimnotifino) {
        this.claimnotifino = claimnotifino;
    }
}