/*
 * File: P5184.java
 * Date: 30 August 2009 0:17:31
 * Author: Quipoz Limited
 * 
 * Class transformed from P5184.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.screens.S5184ScreenVars;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcchk;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcchkrec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P5184 - Regular Payments Sub-Menu.
*
* This is the Regular Payments Sub Menu Program.
* It performs all the tasks necessary of a Sub Menu.
*
* 1000-
*
* Initialise the Screen fields and all the appropriate Working
* Storage Variables.
*
* 2000-
*
* Display the Screen by Calling the S5184IO Module.
*
* Call the SUBPROG Subroutine.
*
* Call the SANCTN Subroutine.
*
* Load up the next four Programs in the Stack.
*
* Validate the Screen fields. Both fields are mandatory.
*
* Read the CHDR Record and validate it against Table T5679.
*
*
* 3000-
*
* Softlock and perform a KEEPS on the the Contract.
*
* Update the WSSP fields.
*
* Open a new BATCH.
*
* 4000-
*
* Move 1 to the WSSP-PROGRAM-PTR.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class P5184 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5184");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaContractStatuzCheck = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).isAPartOf(wsaaContractStatuzCheck, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 1);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 3);
		/* ERRORS */
	private static final String e717 = "E717";
	protected static final String f259 = "F259";
	private static final String f910 = "F910";
	private static final String g667 = "G667";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrrgprec = "CHDRRGPREC";
	private static final String itemrec = "ITEMREC";
	protected ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcchkrec batcchkrec = new Batcchkrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	protected S5184ScreenVars sv = getLScreenVars();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		validate2200, 
		verifyBatchControl2300, 
		exit2090, 
		keepRecord3100, 
		continue3400, 
		exit3900
	}

	public P5184() {
		super();
		screenVars = sv;
		new ScreenModel("S5184", AppVars.getInstance(), sv);
	}

	protected S5184ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5184ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		wsaaContractStatuzCheck.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*    CALL 'S5184IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                S5184-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if(validateAction2100()){
			validate2200();
		}else{
			verifyBatchControl2300();
		}
	}


protected boolean validateAction2100()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			return true;
		}
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			return true;
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.statuz, "BACH")) {
			return false;
		}
		return true;
	}

protected void validate2200()
	{
		if (isEQ(sv.chdrsel, SPACES)) {
			sv.chdrselErr.set(g667);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT                                           */
			return;
		}
		/* Read the contract header details.*/
		chdrrgpIO.setParams(SPACES);
		chdrrgpIO.setChdrcoy(wsspcomn.company);
		chdrrgpIO.setChdrnum(sv.chdrsel);
		chdrrgpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)
		&& isNE(chdrrgpIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrrgpIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set(f259);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT                                           */
			return;
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			wsspcomn.edterror.set("Y");
			return;
		}
		/* Read the valid statii from table T5679.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatuz.set("N");
		wsaaStatcode.set(chdrrgpIO.getStatcode());
		wsaaPstcde.set(chdrrgpIO.getPstatcode());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
				for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
				|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()], wsaaPstcde)) {
						wsaaValidStatuz.set("Y");
					}
				}
			}
		}
		if (isEQ(wsaaValidStatuz, "N")) {
			sv.chdrselErr.set(e717);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*    GO TO 2900-EXIT.                                             */
	}

protected void verifyBatchControl2300()
	{
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT                                           */
			return ;
		}
		wsspcomn.secProg[1].set(bcbprogrec.nxtprog1);
		wsspcomn.secProg[2].set(bcbprogrec.nxtprog2);
		wsspcomn.secProg[3].set(bcbprogrec.nxtprog3);
		wsspcomn.secProg[4].set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		if(updateWssp3010()){
			if(keepRecord3100()){
				if(updateBatchControl3200()){
					batcdorrec.function.set("AUTO");
					continue3400();
				}
			}
		}
	}

protected boolean updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		/*  Set the mode indicator WSSP-FLAG according to action.*/
		wsspcomn.flag.set("I");
		if (isEQ(scrnparams.action, "A")) {
			wsspcomn.flag.set("C");
		}
		if (isEQ(scrnparams.action, "B")) {
			wsspcomn.flag.set("M");
		}
		if (isEQ(scrnparams.action, "D")
		|| isEQ(scrnparams.action, "E")) {
			wsspcomn.flag.set("D");
		}
		updateWssp3011CustomerSpecific();
		/* Softlock the contract header record for update modes, unless*/
		/* we are approving the payment when we lock it but set the flag*/
		/* to 'I'.*/
		if (isEQ(wsspcomn.flag, "I")
		&& isNE(scrnparams.action, "C")) {
			return true;
		}
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrrgpIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if ((isNE(sftlockrec.statuz, varcom.oK))
		&& (isNE(sftlockrec.statuz, "LOCK"))) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(f910);
			wsspcomn.edterror.set("Y");
			return false;
		}
		return true;
	}

protected boolean keepRecord3100()
	{
		/*  Keep the contract header record.*/
		chdrrgpIO.setFormat(chdrrgprec);
		chdrrgpIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)
		&& isNE(chdrrgpIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, "BACH")) {
			return false;
		}
		if (isNE(subprogrec.bchrqd, "Y")) {
			return false;
		}
		return true;
	}

protected boolean updateBatchControl3200()
	{
		batcchkrec.batcchkRec.set(SPACES);
		batcchkrec.function.set("CHECK");
		batcchkrec.batchkey.set(wsspcomn.batchkey);
		batcchkrec.tranid.set(wsspcomn.tranid);
		callProgram(Batcchk.class, batcchkrec.batcchkRec);
		if (isEQ(batcchkrec.statuz, varcom.oK)) {
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			return false;
		}
		if (isEQ(batcchkrec.statuz, "INAC")) {
			batcdorrec.function.set("ACTIV");
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			continue3400();
			return false;
		}
		if (isNE(batcchkrec.statuz, varcom.mrnf)) {
			sv.actionErr.set(batcchkrec.statuz);
			wsspcomn.edterror.set("Y");
			return false;
		}
		return true;
	}

protected void continue3400()
	{
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
			wsspcomn.edterror.set("Y");
			rollback();
			return ;
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.set(1);
		/*EXIT*/
	}

protected void updateWssp3011CustomerSpecific(){
	
}
}
