package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Calprpmrec extends ExternalData {

	//*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData calprpmRec = new FixedLengthStringData(233);//ILIFE-8509
	  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(calprpmRec, 0);
	  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(calprpmRec, 5);
		public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(calprpmRec, 9);
		public FixedLengthStringData chdrnum =new FixedLengthStringData(8).isAPartOf(calprpmRec, 10);
		public FixedLengthStringData transcode =new FixedLengthStringData(4).isAPartOf(calprpmRec, 18);
		public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(calprpmRec, 22);
	    public ZonedDecimalData prem = new ZonedDecimalData(17,2).isAPartOf(calprpmRec, 23);
	    public ZonedDecimalData totlprem = new ZonedDecimalData(17,2).isAPartOf(calprpmRec, 40);
	    public ZonedDecimalData rerateprem = new ZonedDecimalData(17,2).isAPartOf(calprpmRec, 57);
	    public ZonedDecimalData zlinstprem = new ZonedDecimalData(17,2).isAPartOf(calprpmRec, 74);
	    public ZonedDecimalData uniqueno = new ZonedDecimalData(18).isAPartOf(calprpmRec, 91);
		public ZonedDecimalData lastPTDate = new ZonedDecimalData(8, 0).isAPartOf(calprpmRec, 109).setUnsigned();
		public ZonedDecimalData ptdlapse = new ZonedDecimalData(8, 0).isAPartOf(calprpmRec, 117).setUnsigned();
		public ZonedDecimalData reinstDate = new ZonedDecimalData(8, 0).isAPartOf(calprpmRec, 125).setUnsigned();
		public ZonedDecimalData nextPTDate = new ZonedDecimalData(8, 0).isAPartOf(calprpmRec, 133).setUnsigned();
		public FixedLengthStringData mode = new FixedLengthStringData(1).isAPartOf(calprpmRec, 141);
		public ZonedDecimalData increasePrem = new ZonedDecimalData(17,2).isAPartOf(calprpmRec, 142);
		public ZonedDecimalData zbinstprem = new ZonedDecimalData(17,2).isAPartOf(calprpmRec, 159);//ILIFE-8509
		public ZonedDecimalData increaseSum = new ZonedDecimalData(17,2).isAPartOf(calprpmRec, 176);
		public ZonedDecimalData zstpduty01 = new ZonedDecimalData(17,2).isAPartOf(calprpmRec, 193);
		public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(calprpmRec, 210);
		public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(calprpmRec, 213);
		public ZonedDecimalData cntfee = new ZonedDecimalData(17,2).isAPartOf(calprpmRec, 216);
	  
		public void initialize() {
			COBOLFunctions.initialize(calprpmRec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			calprpmRec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}

}
