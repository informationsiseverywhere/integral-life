package com.csc.life.terminationclaims.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Rr59hReport extends SMARTReportLayout {
	
	private FixedLengthStringData clntnm = new FixedLengthStringData(40);
	private FixedLengthStringData clntnum = new FixedLengthStringData(8);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private FixedLengthStringData issuedte = new FixedLengthStringData(10);
	private FixedLengthStringData rcessterm = new FixedLengthStringData(2);
	private FixedLengthStringData rcesdte = new FixedLengthStringData(10);
	private ZonedDecimalData sumin = new ZonedDecimalData(15, 0);
	private FixedLengthStringData curr = new FixedLengthStringData(3);
	private FixedLengthStringData vrtfund = new FixedLengthStringData(4);
	private FixedLengthStringData unitval = new FixedLengthStringData(4);
	private FixedLengthStringData matamt = new FixedLengthStringData(17);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private RPGTimeData time = new RPGTimeData();
	

	/**
	 * Constructors
	 */

	public Rr59hReport() {
		super();
	}


	/**
	 * Print the XML for Rr59hd01
	 */
	public void printRr59hd01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 1, 40));
		clntnum.setFieldName("clntnum");
		clntnum.setInternal(subString(recordData, 41, 8));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 49, 8));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 57, 30));
		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 87, 4));
		issuedte.setFieldName("issuedte");
		issuedte.setInternal(subString(recordData, 91, 10));
		rcessterm.setFieldName("rcessterm");
		rcessterm.setInternal(subString(recordData, 101, 2));
		rcesdte.setFieldName("rcesdte");
		rcesdte.setInternal(subString(recordData, 103, 10));
		sumin.setFieldName("sumin");
		sumin.setInternal(subString(recordData, 113, 15));
		curr.setFieldName("curr");
		curr.setInternal(subString(recordData, 128, 3));
		vrtfund.setFieldName("vrtfund");
		vrtfund.setInternal(subString(recordData, 131, 4));
		unitval.setFieldName("unitval");
		unitval.setInternal(subString(recordData, 135, 4));
		matamt.setFieldName("matamt");
		matamt.setInternal(subString(recordData, 139, 17));
		printLayout("Rr59hd01",			// Record name
			new BaseData[]{	                      // Fields:
				
				clntnm,
				clntnum,
				chdrnum,
				descrip,
				crtable,
				issuedte,
				rcessterm,
				rcesdte,
				sumin,
				curr,
				vrtfund,
				unitval,
				matamt
				
			}
		);

		currentPrintLine.add(2);
	}


	/**
	 * Print the XML for Rr59hh01
	 */
	public void printRr59hh01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 1, 2));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 3, 1));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 4, 10));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 14, 30)); //ILIFE-3362
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 44, 30)); //ILIFE-3362
		time.setFieldName("time");
		time.set(getTime());
		printLayout("Rr59hh01",			// Record name
			new BaseData[]{			// Fields:
					
				branch,
				company,
				sdate,
				branchnm,
				companynm,
				time
			}
		);

		currentPrintLine.set(12);
	}



}
