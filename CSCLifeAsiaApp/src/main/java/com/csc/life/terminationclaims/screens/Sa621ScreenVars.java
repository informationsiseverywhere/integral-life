package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.life.contractservicing.screens.S5082protect;
import com.csc.life.contractservicing.screens.S5082screen;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sa621ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(3052);//Creating method for Data area size
	public FixedLengthStringData dataFields = new FixedLengthStringData(3040).isAPartOf(dataArea, 0);
	public FixedLengthStringData notifinum = DD.aacct.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData notes = DD.accdesc1.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData datime = DD.crtdatime.copy().isAPartOf(dataFields,3014);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 3040);
	public FixedLengthStringData notifinumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData notesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData datimeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 82);
	public FixedLengthStringData[] notifinumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] notesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] datimeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public LongData Sa621screenWritten = new LongData(0);
	public LongData Sa621protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sa621ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {notifinum, notes,datime};
		screenOutFields = new BaseData[][] {notifinumOut, notesOut,datimeOut};
		screenErrFields = new BaseData[] {notifinumErr, notesErr,datimeErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sa621screen.class;
		protectRecord = Sa621protect.class;
	}
}
