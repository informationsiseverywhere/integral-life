/*
 * File: Pa545.java
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.AnntlnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegplnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegtenqTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegtlnbTableDAM;
import com.csc.life.terminationclaims.screens.Sa545ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.terminationclaims.tablestructures.T6690rec;
import com.csc.life.terminationclaims.tablestructures.T6692rec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.life.terminationclaims.tablestructures.T6696rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
* This program is part of the 9405 Annuities Development.  It is
* called via T5671 switching when a user is creating, modifying
* or enquiring upon an Immediate Annuity proposal.
*
* This program controls the Immediate Annuity Payment Details
* screen, which is used to modify or enquire upon annuity
* payment details held on the Regular Payments Temporary file,
* REGT.
*
* If in enquiry mode all fields except the window select fields
* will be protected.
*
* Display the REGT details on the screen  looking  up  all  of
* the descriptions from DESC where appropriate.
*
* If  the  bank details on REGT are non-blank set a '+' in the
* Bank Details indicator field.
*
* Validation rules.
* =================
*
* Reason  Code.
*  This is a mandatory field. The item must exist on the Regular
*  Payment Reason Codes  table,  T6692.  A  window  facility  is
*  available for this field.
*
* Evidence.
*  This field is free format and optional.
*
* Payment Method.
*  This is a mandatory field.  If the entry on  Regular  Payment
*  Methods  table,  T6694,  for  the  method of payment entered,
*  indicates that bank details are required an X will be  forced
*  into  the  Bank  Details selection field to ensure that  bank
*  details will be set up.
*
* Payment Currency.
*  This  is  a mandatory field.  It will  be  held  on the  REGT
*  created  by  P5329.  It can be overidden and must be a  valid
*  item on T3629.
*
* Frequency Code.
*  This  is a mandatory field.  It will be held on the REGT file
*  created  by P5329.  It can  only be overridden if the Regular
*  Claim  Detail  rules  held  on  table  T6696  allow frequency
*  override. If the Frequency  Code has  changed then the  Total
*  Sum  Assured  field  must  also be  re-calculated to bring it
*  into line with the new frequency.
*
* Payee  Client.
*  If the entry on the Regular Payment Methods of Payment table,
*  T6694,  for  the  method of  payment entered indicates that a
*  payee is required this field  is  mandatory,  otherwise it is
*  not required and will cause an error if completed.
*
* Percentage.
*  If the  percentage and amount  fields  are  blank, obtain the
*  default  percentage  from  table  T6696  and  calculate   the
*  amount from this.  If the  percentage is changed, recalculate
*  the amount as a  percentage of the sum assured on the screen.
*  If both  the percentage  and amount  fields are  changed, the
*  amount takes precedence and the percentage is recalculated.
*
* Destination  Key.
*  If  the  'Contract  Details  Required'  indicator   on  T6694
*  is 'Y' then a valid contract  number must  be  entered  here.
*  Otherwise the field must be blank.
*
* Payment Amount.
*  This is the amount to be paid for the regular payment record.
*  The total  payment amount for  all Immediate annuity payments
*  payable  during the  same period can not be  greater than the
*  sum assured for the Immediate Annuity component.
*
* Effective date.
*  This  is  the  Effective  Date  of  Annuity  payments  and is
*  shown here for information only.
*
* First Payment Date.
*  This  is obtained from the REGT record and is shown here  for
*  information only.  It is recalculated if the frequency changes
*  and the payment is made in arrears.
*
* Review Date.
*  This is obtained from the REGT record and is protected if the
*  coverage has a guaranteed payment period.
*
* Final Payment Date.
*  This is optional  and  if  left  blank will  be  set  to  Max
*  Date. If entered it must not be less than  the First  Payment
*  date.
*
* Anniversary Date.
*  This may be entered by the user  but  if not  then  calculate
*  it from the effective Date and the Indexation Frequency  from
*  T6696. The default is Max Date.
*
* Tables Used.
*      T3629 - Currency Code Details
*      Key: CURRCD
*      T5606 - Regular Benefit Edit Rules
*      Key: Validation Item Key (from T5671) +  Currency Code
*      T5671 - Generic Program Switching
*      Key: Transaction Code +  CRTABLE
*      T5688 - Contract Definition
*      Key: Contract Type
*      T6692 - Regular Payment Reason Codes
*      Key: Regular Payment Reason Code
*      T6694 - Regular Payment Method of Payment
*      Key: Regular Payment MOP
*      T6696 - Regular Claim Detail Rules
*      Key: CRTABLE +  Reason Code
*
*****************************************************************
* </pre>
*/
public class Pa545 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA545");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT6696Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6696Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6696Key, 0);
	private FixedLengthStringData wsaaT6696Cltype = new FixedLengthStringData(2).isAPartOf(wsaaT6696Key, 4);

	private FixedLengthStringData wsaaTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscd = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 4);

	private FixedLengthStringData wsaaTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTran = new FixedLengthStringData(5).isAPartOf(wsaaTranCurrency, 0);
	private FixedLengthStringData wsaaCurrency = new FixedLengthStringData(3).isAPartOf(wsaaTranCurrency, 5);
	private FixedLengthStringData wsaaPayclt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLastFreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaFreq2 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPayrseqno = new ZonedDecimalData(1, 0).setUnsigned();
	private String wsaaTotalUsed = "";
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaSumins2 = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaPrcnt = new PackedDecimalData(5, 2).setUnsigned();
	private PackedDecimalData wsaaLastPrcnt = new PackedDecimalData(5, 2).setUnsigned();
	private PackedDecimalData wsaaInterimPrcnt = new PackedDecimalData(12, 9).setUnsigned();
	private PackedDecimalData wsaaPymt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaLastPymt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaPymt2 = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData index2 = new PackedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
		/* TABLES */
	private static final String t3590 = "T3590";
	private static final String t3629 = "T3629";
	private static final String t5688 = "T5688";
	private static final String t6690 = "T6690";
	private static final String t6691 = "T6691";
	private static final String t6692 = "T6692";
	private static final String t6694 = "T6694";
	private static final String t5606 = "T5606";
	private static final String t5671 = "T5671";
	
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/rider transactions - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
//	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class); //fwang3
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Regular Payment Temporary Record New Bus*/
	private RegtlnbTableDAM regtlnbIO = new RegtlnbTableDAM();
	private RegplnbTableDAM regplnbIO = new RegplnbTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T6690rec t6690rec = new T6690rec();
	private T6694rec t6694rec = new T6694rec();
	private T5606rec t5606rec = new T5606rec();
	private T5671rec t5671rec = new T5671rec();
	private Wssplife wssplife = new Wssplife();
	private Sa545ScreenVars sv = ScreenProgram.getScreenVars( Sa545ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private boolean isBankDirect= false;
	private CovrpfDAO covrDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Covrpf covrObj = null;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrDAO",ChdrpfDAO.class);
	protected Chdrpf chdrpf = new Chdrpf();
	
	

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		checkDestination2060, 
		checkForErrors2080, 
		exit2090, 
		exit4090
	}

	public Pa545() {
		super();
		screenVars = sv;
		new ScreenModel("Sa545", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	****   INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1010();
			create1060();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		wsaaPayclt.set(SPACES);
		wsaaLastFreq.set(SPACES);
		sv.anvdate.set(varcom.vrcmMaxDate);
		sv.crtdate.set(varcom.vrcmMaxDate);
		sv.finalPaydate.set(varcom.vrcmMaxDate);
		sv.firstPaydate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.revdate.set(varcom.vrcmMaxDate);
		wsaaSumins.set(ZERO);
		wsaaSumins2.set(ZERO);
		wsaaPayrseqno.set(ZERO);
		wsaaPymt.set(ZERO);
		wsaaLastPymt.set(ZERO);
		wsaaPymt2.set(ZERO);
		wsaaPrcnt.set(ZERO);
		wsaaLastPrcnt.set(ZERO);
		wsaaInterimPrcnt.set(ZERO);
		wsaaFreq.set(ZERO);
		wsaaFreq2.set(ZERO);
		sv.pymt.set(ZERO);
		sv.prcnt.set(ZERO);
		sv.sumins.set(ZERO);
		sv.totalamt.set(ZERO);
		sv.totalamtOut[varcom.nd.toInt()].set("Y");
		
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrlnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		} 
		/*chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}*/
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill(" ");
		}
		descIO.setDescitem(chdrpf.getCntcurr());
		descIO.setDesctabl(t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.currds.set(descIO.getShortdesc());
		}
		else {
			sv.currds.fill(" ");
		}
		cltsIO.setClntnum(chdrpf.getCownnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),1))) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cownnum.set(chdrpf.getCownnum());
		sv.occdate.set(chdrpf.getOccdate());
		sv.currcd.set(chdrpf.getCntcurr());
		regtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regtlnbIO);
		
		if (isNE(regtlnbIO.getStatuz(),varcom.oK) && isNE(regtlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(regtlnbIO.getParams());
			fatalError600();
		}
		
		if(isEQ(regtlnbIO.getStatuz(),varcom.mrnf)) {
			regplnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, regplnbIO);
			regplnbIO.getStatuz();
		}
		
		covtlnbIO.setChdrcoy(chdrpf.getChdrcoy());
		covtlnbIO.setChdrnum(chdrpf.getChdrnum());
		if (isEQ(regtlnbIO.getStatuz(), varcom.mrnf)) {
			covtlnbIO.setLife(regplnbIO.getLife());
			covtlnbIO.setCoverage(regplnbIO.getCoverage());
			covtlnbIO.setRider(regplnbIO.getRider());
		}else {
			covtlnbIO.setLife(regtlnbIO.getLife());
			covtlnbIO.setCoverage(regtlnbIO.getCoverage());
			covtlnbIO.setRider(regtlnbIO.getRider());
		}
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setStatuz(varcom.oK);
		covtlnbIO.setFunction(varcom.begn);
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK) && isNE(covtlnbIO.getStatuz(), varcom.endp)
				|| isNE(chdrpf.getChdrcoy(),covtlnbIO.getChdrcoy())
				|| isNE(chdrpf.getChdrnum(),covtlnbIO.getChdrnum())) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if(isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
			if(isNE(regtlnbIO.getStatuz(),varcom.mrnf)) {
				covrObj = covrDAO.getCovrRecord(chdrpf.getChdrcoy().toString(), sv.chdrnum.toString(), regtlnbIO.getLife().toString(), regtlnbIO.getCoverage().toString(), regtlnbIO.getRider().toString(), regtlnbIO.getPlanSuffix().toInt(), "1");
			}else {
				covrObj = covrDAO.getCovrRecord(chdrpf.getChdrcoy().toString(), sv.chdrnum.toString(), regplnbIO.getLife().toString(), regplnbIO.getCoverage().toString(), regplnbIO.getRider().toString(), regplnbIO.getPlanSuffix().toInt(), regplnbIO.getValidflag().toString());      
			}
			
			if (isGT(covrObj.getPayrseqno(),0)) {
				wsaaPayrseqno.set(covrObj.getPayrseqno());
			}
			else {
				wsaaPayrseqno.set(1);
			}
			wsaaSumins.set(covrObj.getSumins());
			lifelnbIO.setFunction(varcom.readr);
			lifelnbIO.setChdrnum(covrObj.getChdrnum());
			lifelnbIO.setChdrcoy(covrObj.getChdrcoy());
			lifelnbIO.setLife(covrObj.getLife());
		}else {
			if (isGT(covtlnbIO.getPayrseqno(),0)) {
				wsaaPayrseqno.set(covtlnbIO.getPayrseqno());
			}
			else {
				wsaaPayrseqno.set(1);
			}
			wsaaSumins.set(covtlnbIO.getSumins());
			lifelnbIO.setFunction(varcom.readr);
			lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
			lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
			lifelnbIO.setLife(covtlnbIO.getLife());
		}
		lifelnbIO.setJlife(ZERO);
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),1))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
	}

protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))
		&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void create1060()
{
	setScreenDetails1300();
	
	/* Read PAYR record to obtain contract currreny.*/
	payrIO.setDataArea(SPACES);
	payrIO.setChdrcoy(chdrpf.getChdrcoy());
	payrIO.setChdrnum(chdrpf.getChdrnum());
	payrIO.setPayrseqno(wsaaPayrseqno);
	payrIO.setFormat(formatsInner.payrrec);
	payrIO.setFunction(varcom.readh);
	SmartFileCode.execute(appVars, payrIO);
	if (isNE(payrIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(payrIO.getStatuz());
		fatalError600();
	}
	
	wsaaTranscd.set(wsaaBatckey.batcBatctrcde);
	if(isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
		wsaaCrtable.set(covrObj.getCrtable());
	}else {
		wsaaCrtable.set(covtlnbIO.getCrtable());
	}
	loadT56714100();
	
	for (index2.set(1); !(isGT(index2,4)); index2.add(1)){
		if (isEQ(subString(t5671rec.pgm[index2.toInt()], 2, 4),subString(wsspcomn.lastprog, 2, 4))) {
			wsaaTran.set(t5671rec.edtitm[index2.toInt()]);
			index2.set(5);
		}
	} 
	
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(t5606);
	wsaaCurrency.set(payrIO.getCntcurr());
	itempf.setItemitem(wsaaTranCurrency.toString());
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate()));
	itempf.setItemseq("  ");
	itempf = itempfDAO.getItemRecordByItemkey(itempf);

	if (itempf == null) {
		syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat(t5606).concat(wsaaProg.toString()));
		fatalError600();
	}
	t5606rec.t5606Rec.set(StringUtil.rawToString(itempf.getGenarea())); 
	
	if(isEQ(regtlnbIO.getStatuz(),varcom.mrnf)) {
		if (isNE(regplnbIO.getRegpayfreq(),t5606rec.benfreq)) {
			wsaaFreq.set(t5606rec.benfreq);
			wsaaFreq2.set(regplnbIO.getRegpayfreq());
			compute(wsaaSumins2, 3).setRounded((div((mult(wsaaSumins,wsaaFreq)),wsaaFreq2)));
		}
		else {
			wsaaSumins2.set(wsaaSumins);
		}
	}else {
		if (isNE(regtlnbIO.getRegpayfreq(),t5606rec.benfreq)) {
			wsaaFreq.set(t5606rec.benfreq);
			wsaaFreq2.set(regtlnbIO.getRegpayfreq());
			compute(wsaaSumins2, 3).setRounded((div((mult(wsaaSumins,wsaaFreq)),wsaaFreq2)));
		}
		else {
			wsaaSumins2.set(wsaaSumins);
		}
	}
	zrdecplrec.amountIn.set(wsaaSumins2);
	callRounding5000();
	wsaaSumins2.set(zrdecplrec.amountOut);
	sv.sumins.set(wsaaSumins2);
	wsaaLastPymt.set(regtlnbIO.getPymt());
	wsaaLastPrcnt.set(regtlnbIO.getPrcnt());
}

protected void setScreenDetails1300()
	{
		if(isEQ(regtlnbIO.getStatuz(),varcom.mrnf)) {
			read1320();
		}else {
			read1310();
		}
	}

protected void read1310()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(t6690);
		itemIO.setItempfx("IT");
		if(covrObj != null && isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
			itemIO.setItemitem(covrObj.getCrtable());
		}else {
			itemIO.setItemitem(covtlnbIO.getCrtable());
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6690rec.t6690Rec.set(itemIO.getGenarea());
		descIO.setDescitem(t6690rec.rgpytype);
		descIO.setDesctabl(t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		}
		else {
			sv.rgpytypesd.fill(" ");
		}
		descIO.setDescitem(regtlnbIO.getRegpayfreq());
		descIO.setDesctabl(t3590);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.frqdesc.set(descIO.getShortdesc());
		}
		else {
			sv.frqdesc.fill(" ");
		}
		descIO.setDescitem(regtlnbIO.getCurrcd());
		descIO.setDesctabl(t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		}
		else {
			sv.clmcurdsc.fill(" ");
		}

		sv.rgpynum.set(regtlnbIO.getRgpynum());
		sv.regpayfreq.set(regtlnbIO.getRegpayfreq());
		wsaaFreq.set(regtlnbIO.getRegpayfreq());
		wsaaLastFreq.set(regtlnbIO.getRegpayfreq());
		sv.claimcur.set(regtlnbIO.getCurrcd());
		sv.crtdate.set(regtlnbIO.getCrtdate());
		sv.revdate.set(regtlnbIO.getRevdte());
		sv.firstPaydate.set(regtlnbIO.getFirstPaydate());
		sv.cltype.set(regtlnbIO.getPayreason());
		sv.payclt.set(regtlnbIO.getPayclt());
		sv.claimevd.set(regtlnbIO.getClaimevd());
		sv.rgpymop.set(regtlnbIO.getRgpymop());
		
		isBankDirect = FeaConfg.isFeatureExist(regtlnbIO.getChdrcoy().toString().trim(), "SUOTR006", appVars, "IT");//BSD-ICIL-261
		if(regtlnbIO.getRgpymop().toString().equals(" ") || regtlnbIO.getRgpymop().toString() == null){
			if(isBankDirect){
				sv.rgpymop.set("B");
			}
		}else{
			sv.rgpymop.set(regtlnbIO.getRgpymop());
		}
		sv.prcnt.set(regtlnbIO.getPrcnt());
		sv.destkey.set(regtlnbIO.getDestkey());
		sv.pymt.set(regtlnbIO.getPymt());
		sv.totalamt.set(regtlnbIO.getTotamnt());
		sv.anvdate.set(regtlnbIO.getAnvdate());
		sv.finalPaydate.set(regtlnbIO.getFinalPaydate());
		if (isNE(regtlnbIO.getPayclt(),SPACES)) {
			wsaaPayclt.set(regtlnbIO.getPayclt());
		}
		if (isNE(regtlnbIO.getPayclt(),SPACES)) {
			cltsIO.setClntnum(regtlnbIO.getPayclt());
			getClientDetails1200();
			if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
			|| (isNE(cltsIO.getValidflag(),1))) {
				sv.payenmeErr.set(errorsInner.e335);
				sv.payenme.set(SPACES);
			}
			else {
				plainname();
				sv.payenme.set(wsspcomn.longconfname);
			}
		}
		if (isNE(regtlnbIO.getPayreason(),SPACES)) {
			descIO.setDescitem(regtlnbIO.getPayreason());
			descIO.setDesctabl(t6692);
			findDesc1100();
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				sv.clmdesc.set(descIO.getLongdesc());
			}
			else {
				sv.clmdesc.set(SPACES);
			}
		}
		if (isNE(regtlnbIO.getRgpymop(),SPACES)) {
			descIO.setDescitem(regtlnbIO.getRgpymop());
			descIO.setDesctabl(t6694);
			findDesc1100();
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				sv.rgpyshort.set(descIO.getShortdesc());
			}
			else {
				sv.rgpyshort.set(SPACES);
			}
		}
		if (isNE(regtlnbIO.getBankkey(),SPACES)
		|| isNE(regtlnbIO.getBankacckey(),SPACES)) {
			sv.ddind.set("+");
		}
//		calculateSumin();
	}

protected void read1320()
{
	itemIO.setDataKey(SPACES);
	itemIO.setItemcoy(wsspcomn.company);
	itemIO.setFormat(formatsInner.itemrec);
	itemIO.setItemtabl(t6690);
	itemIO.setItempfx("IT");
	itemIO.setItemitem(covrObj.getCrtable());
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if ((isNE(itemIO.getStatuz(),varcom.oK))) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	t6690rec.t6690Rec.set(itemIO.getGenarea());
	descIO.setDescitem(t6690rec.rgpytype);
	descIO.setDesctabl(t6691);
	findDesc1100();
	if (isEQ(descIO.getStatuz(),varcom.oK)) {
		sv.rgpytypesd.set(descIO.getShortdesc());
	}
	else {
		sv.rgpytypesd.fill(" ");
	}
	descIO.setDescitem(regplnbIO.getRegpayfreq());
	descIO.setDesctabl(t3590);
	findDesc1100();
	if (isEQ(descIO.getStatuz(),varcom.oK)) {
		sv.frqdesc.set(descIO.getShortdesc());
	}
	else {
		sv.frqdesc.fill(" ");
	}
	descIO.setDescitem(regplnbIO.getCurrcd());
	descIO.setDesctabl(t3629);
	findDesc1100();
	if (isEQ(descIO.getStatuz(),varcom.oK)) {
		sv.clmcurdsc.set(descIO.getShortdesc());
	}
	else {
		sv.clmcurdsc.fill(" ");
	}

	sv.rgpynum.set(regplnbIO.getRgpynum());
	sv.regpayfreq.set(regplnbIO.getRegpayfreq());
	wsaaFreq.set(regplnbIO.getRegpayfreq());
	wsaaLastFreq.set(regplnbIO.getRegpayfreq());
	sv.claimcur.set(regplnbIO.getCurrcd());
	sv.crtdate.set(regplnbIO.getCrtdate());
	sv.revdate.set(regplnbIO.getRevdte());
	sv.firstPaydate.set(regplnbIO.getFirstPaydate());
	sv.cltype.set(regplnbIO.getPayreason());
	sv.payclt.set(regplnbIO.getPayclt());
	sv.claimevd.set(regplnbIO.getClaimevd());
	sv.rgpymop.set(regplnbIO.getRgpymop());
	
	isBankDirect = FeaConfg.isFeatureExist(regplnbIO.getChdrcoy().toString().trim(), "SUOTR006", appVars, "IT");//BSD-ICIL-261
	if(regplnbIO.getRgpymop().toString().equals(" ") || regplnbIO.getRgpymop().toString() == null){
		if(isBankDirect){
			sv.rgpymop.set("B");
		}
	}else{
		sv.rgpymop.set(regplnbIO.getRgpymop());
	}
	sv.prcnt.set(regplnbIO.getPrcnt());
	sv.destkey.set(regplnbIO.getDestkey());
	sv.pymt.set(regplnbIO.getPymt());
	sv.totalamt.set(regplnbIO.getTotamnt());
	sv.anvdate.set(regplnbIO.getAnvdate());
	sv.finalPaydate.set(regplnbIO.getFinalPaydate());
	if (isNE(regplnbIO.getPayclt(),SPACES)) {
		wsaaPayclt.set(regplnbIO.getPayclt());
	}
	if (isNE(regplnbIO.getPayclt(),SPACES)) {
		cltsIO.setClntnum(regplnbIO.getPayclt());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(),varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(),1))) {
			sv.payenmeErr.set(errorsInner.e335);
			sv.payenme.set(SPACES);
		}
		else {
			plainname();
			sv.payenme.set(wsspcomn.longconfname);
		}
	}
	if (isNE(regplnbIO.getPayreason(),SPACES)) {
		descIO.setDescitem(regplnbIO.getPayreason());
		descIO.setDesctabl(t6692);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.clmdesc.set(descIO.getLongdesc());
		}
		else {
			sv.clmdesc.set(SPACES);
		}
	}
	if (isNE(regplnbIO.getRgpymop(),SPACES)) {
		descIO.setDescitem(regplnbIO.getRgpymop());
		descIO.setDesctabl(t6694);
		findDesc1100();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rgpyshort.set(descIO.getShortdesc());
		}
		else {
			sv.rgpyshort.set(SPACES);
		}
	}
	if (isNE(regplnbIO.getBankkey(),SPACES)
	|| isNE(regplnbIO.getBankacckey(),SPACES)) {
		sv.ddind.set("+");
	}
	
//	calculateSumin();
	
}


protected void calculateSumin() {
	wsaaFreq2.set(sv.regpayfreq);
	wsaaLastFreq.set(sv.regpayfreq);
	wsaaFreq.set(t5606rec.benfreq);
	wsaaSumins2.set(ZERO);
	compute(wsaaSumins2, 3).setRounded(div((mult(wsaaSumins,wsaaFreq)),wsaaFreq2));
	zrdecplrec.amountIn.set(wsaaSumins2);
	callRounding5000();
	wsaaSumins2.set(zrdecplrec.amountOut);
	sv.sumins.set(wsaaSumins2);
}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsspcomn.flag,"I")) {
			protectScr2100();
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
					checkBankDetails2035();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/* Validate fields*/
		if (isNE(scrnparams.statuz,varcom.oK)) {
			wsspcomn.edterror.set("Y");
			if (isNE(scrnparams.statuz,varcom.calc)) {
				scrnparams.errorCode.set(errorsInner.curs);
				goTo(GotoLabel.exit2090);
			}
		}
		/* We must validate the bank indicator here since it can be*/
		/* altered in enquiry mode to access bank details.*/
		if ((isNE(sv.ddind,"+")
		&& isNE(sv.ddind,"X")
		&& isNE(sv.ddind,SPACES))) {
			sv.ddindErr.set(errorsInner.h118);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag,"I")) {
			if(isEQ(regtlnbIO.getStatuz(),varcom.mrnf)) {
				if ((isEQ(sv.ddind,"X")
						&& isEQ(regplnbIO.getBankkey(),SPACES))) {
					sv.ddindErr.set(errorsInner.e493);
					wsspcomn.edterror.set("Y");
					sv.ddind.set(SPACES);
				}
			}else {
				if ((isEQ(sv.ddind,"X")
						&& isEQ(regtlnbIO.getBankkey(),SPACES))) {
					sv.ddindErr.set(errorsInner.e493);
					wsspcomn.edterror.set("Y");
					sv.ddind.set(SPACES);
				}
			}
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkBankDetails2035()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.rgpymop);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company,itdmIO.getItemcoy()))
		|| (isNE(t6694,itdmIO.getItemtabl()))
		|| (isNE(sv.rgpymop,itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.rgpymopErr.set(errorsInner.g529);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		else {
			descIO.setDescitem(sv.rgpymop);
			descIO.setDesctabl(t6694);
			findDesc1100();
			sv.rgpyshort.set(descIO.getShortdesc());
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		setBankIndicator2200();
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void protectScr2100()
	{
		/*PROTECT*/
		sv.paycltOut[varcom.pr.toInt()].set("Y");
		sv.cltypeOut[varcom.pr.toInt()].set("Y");
		sv.claimevdOut[varcom.pr.toInt()].set("Y");
		sv.rgpymopOut[varcom.pr.toInt()].set("Y");
		sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
		sv.destkeyOut[varcom.pr.toInt()].set("Y");
		sv.pymtOut[varcom.pr.toInt()].set("Y");
		sv.claimcurOut[varcom.pr.toInt()].set("Y");
		sv.prcntOut[varcom.pr.toInt()].set("Y");
		sv.anvdateOut[varcom.pr.toInt()].set("Y");
		sv.epaydateOut[varcom.pr.toInt()].set("Y");
		/*EXIT*/
	}

protected void setBankIndicator2200()
	{
			set2210();
		}

protected void set2210()
	{
		if (isNE(sv.ddindErr,SPACES)) {
			return ;
		}
		if (isEQ(t6694rec.bankreq,"Y")) {
			if ((isEQ(regtlnbIO.getBankkey(),SPACES))
			|| (isEQ(regtlnbIO.getBankacckey(),SPACES))) {
				sv.ddind.set("X");
			}
			else {
				if (isNE(sv.ddind,"X")) {
					sv.ddind.set("+");
				}
			}
		}
		else {
			
			if (isEQ(sv.ddind,"X") && isEQ(regtlnbIO.getBankkey(),SPACES)) {
				sv.ddindErr.set(errorsInner.e570);
				sv.ddind.set(SPACES);
			}
			else if ((isEQ(sv.ddind,"X") || isEQ(sv.ddind,"+")) && isNE(regtlnbIO.getBankkey(),SPACES)){
					sv.ddind.set(SPACES);
				}
			else {
				sv.ddind.set(SPACES);
			}
		}
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		/*  No Update on an Inquiry.*/
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		try {
			nextProgram4000();
			popUp4010();
			genssw4020();
			nextProgram4030();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4000()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}
		if (isEQ(sv.ddind,"X")) {
			if(isEQ(regtlnbIO.getStatuz(),varcom.mrnf)) {
				regtlnbIO.setChdrcoy(regplnbIO.getChdrcoy());
				regtlnbIO.setChdrnum(regplnbIO.getChdrnum());
				regtlnbIO.setPayclt(regplnbIO.getPayclt());
				regtlnbIO.setBankkey(regplnbIO.getBankkey());
				regtlnbIO.setBankacckey(regplnbIO.getBankacckey());
				regtlnbIO.setCurrcd(regplnbIO.getCurrcd());
				
			}
			regtlnbIO.setFunction(varcom.keeps);
			regtlnbIO.setFormat(formatsInner.regtlnbrec);
			SmartFileCode.execute(appVars, regtlnbIO);
			if (isNE(regtlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(regtlnbIO.getParams());
				fatalError600();
			}
		}
		else {
			if (isEQ(sv.ddind,"?")) {
				checkBankDetails4400();
			}
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		&& isNE(sv.ddind,"X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
	}

protected void popUp4010()
	{
		gensswrec.function.set(SPACES);
		if ((isEQ(sv.ddind,"X"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				saveProgramStack4200();
			}
			sv.ddind.set("?");
			gensswrec.function.set("A");
		}
	}

protected void genssw4020()
	{
		if (isEQ(gensswrec.function,SPACES)) {
			
			if(isNE(sv.ddind,"+")){
				if (isNE(wsspcomn.flag,"I")) {  // ILIFE-7860
				regtlnbIO.setBankkey(SPACES);
				regtlnbIO.setBankacckey(SPACES);
				regtlnbIO.setFunction(varcom.rewrt);
				regtlnbIO.setFormat(formatsInner.regtlnbrec);
				SmartFileCode.execute(appVars, regtlnbIO);
				if (isNE(regtlnbIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(regtlnbIO.getParams());
					fatalError600();
				}
			}
			}
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4090);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz,varcom.oK))
		&& (isNE(gensswrec.statuz,varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
		}
	}

protected void nextProgram4030()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void checkBankDetails4400()
	{
		bank4400();
	}

protected void bank4400()
	{
		regtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regtlnbIO);
		if (isNE(regtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtlnbIO.getParams());
			fatalError600();
		}
		regtlnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regtlnbIO);
		if (isNE(regtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtlnbIO.getParams());
			fatalError600();
		}
		if ((isEQ(regtlnbIO.getBankkey(),SPACES))
		&& (isEQ(regtlnbIO.getBankacckey(),SPACES))) {
			sv.ddind.set(SPACES);
		}
		else {
			sv.ddind.set("+");
			wsaaPayclt.set(sv.payclt);
		}
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadT56714100() {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(t5671);
	itempf.setItemitem(wsaaTranCrtable.toString());
	itempf = itempfDAO.getItemRecordByItemkey(itempf);
	if (itempf == null) {
		syserrrec.statuz.set("MRNF");
		fatalError600();
	}
	t5671rec.t5671Rec.set(StringUtil.rawToString(itempf.getGenarea()));
} 

/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData e493 = new FixedLengthStringData(4).init("E493");
	private FixedLengthStringData e570 = new FixedLengthStringData(4).init("E570");
	private FixedLengthStringData g529 = new FixedLengthStringData(4).init("G529");
	private FixedLengthStringData h118 = new FixedLengthStringData(4).init("H118");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData regtlnbrec = new FixedLengthStringData(10).init("REGTLNBREC");
}
}
