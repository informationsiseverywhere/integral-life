package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:17
 * Description:
 * Copybook name: PUPLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Puplkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData puplFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData puplKey = new FixedLengthStringData(64).isAPartOf(puplFileKey, 0, REDEFINE);
  	public FixedLengthStringData puplChdrcoy = new FixedLengthStringData(1).isAPartOf(puplKey, 0);
  	public FixedLengthStringData puplChdrnum = new FixedLengthStringData(8).isAPartOf(puplKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(puplKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(puplFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		puplFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}