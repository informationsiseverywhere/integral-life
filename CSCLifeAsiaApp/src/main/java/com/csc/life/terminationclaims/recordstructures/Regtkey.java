package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:47
 * Description:
 * Copybook name: REGTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regtFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData regtKey = new FixedLengthStringData(64).isAPartOf(regtFileKey, 0, REDEFINE);
  	public FixedLengthStringData regtChdrcoy = new FixedLengthStringData(1).isAPartOf(regtKey, 0);
  	public FixedLengthStringData regtChdrnum = new FixedLengthStringData(8).isAPartOf(regtKey, 1);
  	public FixedLengthStringData regtLife = new FixedLengthStringData(2).isAPartOf(regtKey, 9);
  	public FixedLengthStringData regtCoverage = new FixedLengthStringData(2).isAPartOf(regtKey, 11);
  	public FixedLengthStringData regtRider = new FixedLengthStringData(2).isAPartOf(regtKey, 13);
  	public PackedDecimalData regtSeqnbr = new PackedDecimalData(3, 0).isAPartOf(regtKey, 15);
  	public PackedDecimalData regtRgpynum = new PackedDecimalData(5, 0).isAPartOf(regtKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(regtKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}