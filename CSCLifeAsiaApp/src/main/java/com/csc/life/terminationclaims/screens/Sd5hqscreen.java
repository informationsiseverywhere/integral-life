package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sd5hqscreen extends ScreenRecord{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}
	public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
			Sd5hqScreenVars sv = (Sd5hqScreenVars) pv;
			clearInds(av, pfInds);
			write(lrec, sv.Sd5hqscreenWritten, null, av, null, ind2, ind3);
		}

		public static void read(Indicator ind2, Indicator ind3) {}

		public static void clearClassString(VarModel pv) {
			Sd5hqScreenVars screenVars = (Sd5hqScreenVars)pv;
			screenVars.screenRow.setClassString("");
			screenVars.screenColumn.setClassString("");
			screenVars.company.setClassString("");
			screenVars.tabl.setClassString("");
			screenVars.item.setClassString("");
			screenVars.longdesc.setClassString("");
			screenVars.calBasis.setClassString("");
		}

	/**
	 * Clear all the variables in S6696screen
	 */
		public static void clear(VarModel pv) {
			Sd5hqScreenVars screenVars = (Sd5hqScreenVars)pv;
			screenVars.screenRow.clear();
			screenVars.screenColumn.clear();
			screenVars.company.clear();
			screenVars.tabl.clear();
			screenVars.item.clear();
			screenVars.longdesc.clear();
			screenVars.calBasis.clear();
		}
}
