package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SH580
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh580ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1081);
	public FixedLengthStringData dataFields = new FixedLengthStringData(553).isAPartOf(dataArea, 0);
	public ZonedDecimalData clamant = DD.clamant.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData estimateTotalValue = DD.estimtotal.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData instprems = new FixedLengthStringData(34).isAPartOf(dataFields, 34);
	public ZonedDecimalData[] instprem = ZDArrayPartOfStructure(2, 17, 2, instprems, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(34).isAPartOf(instprems, 0, FILLER_REDEFINE);
	public ZonedDecimalData instprem01 = DD.instprem.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData instprem02 = DD.instprem.copyToZonedDecimal().isAPartOf(filler,17);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,68);
	public FixedLengthStringData suminss = new FixedLengthStringData(34).isAPartOf(dataFields, 85);
	public ZonedDecimalData[] sumins = ZDArrayPartOfStructure(2, 17, 2, suminss, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(34).isAPartOf(suminss, 0, FILLER_REDEFINE);
	public ZonedDecimalData sumins01 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData sumins02 = DD.sumins.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,119);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,127);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,138);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,146);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,150);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,180);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,235);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,243);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,290);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,298);
	public ZonedDecimalData policyloan = DD.policyloan.copyToZonedDecimal().isAPartOf(dataFields,345);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,362);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,372);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,380);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,390);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,407);
	//ICIL-254
	
	public FixedLengthStringData payee = DD.payeesel.copy().isAPartOf(dataFields, 424);
	public FixedLengthStringData payeename = DD.payeename.copy().isAPartOf(dataFields, 434);
	public FixedLengthStringData reqntype = DD.reqntype.copy().isAPartOf(dataFields,464);
	public FixedLengthStringData bankacckey = DD.bankacckey1.copy().isAPartOf(dataFields,465);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,485);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,515);
	public FixedLengthStringData crdtcrd = DD.crdtcrd.copy().isAPartOf(dataFields,525);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,545);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 553);
	public FixedLengthStringData clamantErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData estimtotalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData instpremsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] instpremErr = FLSArrayPartOfStructure(2, 4, instpremsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(instpremsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData instprem01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData instprem02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData suminssErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] suminsErr = FLSArrayPartOfStructure(2, 4, suminssErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(suminssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumins01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData sumins02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData policyloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	

	public FixedLengthStringData payeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100); //ICIL-254
	public FixedLengthStringData payeenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104); //ICIL-254
	public FixedLengthStringData reqntypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData crdtcrdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128); //ICIL-254
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(396).isAPartOf(dataArea, 685);
	public FixedLengthStringData[] clamantOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] estimtotalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData instpremsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] instpremOut = FLSArrayPartOfStructure(2, 12, instpremsOut, 0);
	public FixedLengthStringData[][] instpremO = FLSDArrayPartOfArrayStructure(12, 1, instpremOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(instpremsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] instprem01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] instprem02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData suminssOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] suminsOut = FLSArrayPartOfStructure(2, 12, suminssOut, 0);
	public FixedLengthStringData[][] suminsO = FLSDArrayPartOfArrayStructure(12, 1, suminsOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(suminssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumins01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] sumins02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] policyloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	
	
	public FixedLengthStringData[] payeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300); //ICIL-254
	public FixedLengthStringData[] payeenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312); //ICIL-254
	public FixedLengthStringData[] reqntypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] crdtcrdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384); //ICIL-254

	public FixedLengthStringData subfileArea = new FixedLengthStringData(216);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(102).isAPartOf(subfileArea, 0);
	public ZonedDecimalData actvalue = DD.actvalue.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData estMatValue = DD.emv.copyToZonedDecimal().isAPartOf(subfileFields,50);
	public ZonedDecimalData hactval = DD.hactval.copyToZonedDecimal().isAPartOf(subfileFields,67);
	public ZonedDecimalData hemv = DD.hemv.copyToZonedDecimal().isAPartOf(subfileFields,84);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(subfileFields,101);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 102);
	public FixedLengthStringData actvalueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData emvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hactvalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hemvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData typeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 130);
	public FixedLengthStringData[] actvalueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] emvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hactvalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hemvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] typeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 214);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sh580screensflWritten = new LongData(0);
	public LongData Sh580screenctlWritten = new LongData(0);
	public LongData Sh580screenWritten = new LongData(0);
	public LongData Sh580protectWritten = new LongData(0);
	public GeneralTable sh580screensfl = new GeneralTable(AppVars.getInstance());
	
	public FixedLengthStringData susur002flag = new FixedLengthStringData(1); 

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sh580screensfl;
	}

	public Sh580ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(policyloanOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sumins02Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(estimtotalOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(instprem02Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankacckeyOut,new String[] {"55", "56", "55", "57", null, null, null, null, null, null, null, null});
		fieldIndMap.put(crdtcrdOut,new String[] {"60", "61", "60", "62", null, null, null, null, null, null, null, null});
		fieldIndMap.put(reqntypeOut,new String[] {"80", "81", "80", "82", null, null, null, null, null, null, null, null});
		fieldIndMap.put(payeeOut,new String[] {null, null, null, "88", null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenSflFields = new BaseData[] {estMatValue, hemv, hactval, fieldType, descrip, cnstcur, actvalue};
		screenSflOutFields = new BaseData[][] {emvOut, hemvOut, hactvalOut, typeOut, descripOut, cnstcurOut, actvalueOut};
		screenSflErrFields = new BaseData[] {emvErr, hemvErr, hactvalErr, typeErr, descripErr, cnstcurErr, actvalueErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {zbinstprem, zlinstprem, chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, crtable, policyloan, sumins01, otheradjst, sumins02, estimateTotalValue, instprem01, clamant, instprem02, payee, payeename, reqntype,bankacckey,bankdesc,bankkey,crdtcrd, effdate};
		screenOutFields = new BaseData[][] {zbinstpremOut, zlinstpremOut, chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, crtableOut, policyloanOut, sumins01Out, otheradjstOut, sumins02Out, estimtotalOut, instprem01Out, clamantOut, instprem02Out, payeeOut, payeenameOut, reqntypeOut,bankacckeyOut,bankdescOut,bankkeyOut,crdtcrdOut, effdateOut};
		screenErrFields = new BaseData[] {zbinstpremErr, zlinstpremErr, chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, crtableErr, policyloanErr, sumins01Err, otheradjstErr, sumins02Err, estimtotalErr, instprem01Err, clamantErr, instprem02Err, payeeErr, payeenameErr, reqntypeErr,bankacckeyErr,bankdescErr,bankkeyErr,crdtcrdErr, effdateErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, effdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, effdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sh580screen.class;
		screenSflRecord = Sh580screensfl.class;
		screenCtlRecord = Sh580screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sh580protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sh580screenctl.lrec.pageSubfile);
	}
}
