package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AnbkpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:53
 * Class transformed from ANBKPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AnbkpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 103;
	public FixedLengthStringData anbkrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData anbkpfRecord = anbkrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(anbkrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(anbkrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(anbkrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(anbkrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(anbkrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(anbkrec);
	public PackedDecimalData guarperd = DD.guarperd.copy().isAPartOf(anbkrec);
	public FixedLengthStringData freqann = DD.freqann.copy().isAPartOf(anbkrec);
	public FixedLengthStringData arrears = DD.arrears.copy().isAPartOf(anbkrec);
	public FixedLengthStringData advance = DD.advance.copy().isAPartOf(anbkrec);
	public PackedDecimalData dthpercn = DD.dthpercn.copy().isAPartOf(anbkrec);
	public PackedDecimalData dthperco = DD.dthperco.copy().isAPartOf(anbkrec);
	public PackedDecimalData intanny = DD.intanny.copy().isAPartOf(anbkrec);
	public FixedLengthStringData withprop = DD.withprop.copy().isAPartOf(anbkrec);
	public FixedLengthStringData withoprop = DD.withoprop.copy().isAPartOf(anbkrec);
	public FixedLengthStringData ppind = DD.ppind.copy().isAPartOf(anbkrec);
	public PackedDecimalData capcont = DD.capcont.copy().isAPartOf(anbkrec);
	public FixedLengthStringData nomlife = DD.nomlife.copy().isAPartOf(anbkrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(anbkrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(anbkrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(anbkrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(anbkrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(anbkrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AnbkpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AnbkpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AnbkpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AnbkpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AnbkpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AnbkpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AnbkpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ANBKPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"GUARPERD, " +
							"FREQANN, " +
							"ARREARS, " +
							"ADVANCE, " +
							"DTHPERCN, " +
							"DTHPERCO, " +
							"INTANNY, " +
							"WITHPROP, " +
							"WITHOPROP, " +
							"PPIND, " +
							"CAPCONT, " +
							"NOMLIFE, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     guarperd,
                                     freqann,
                                     arrears,
                                     advance,
                                     dthpercn,
                                     dthperco,
                                     intanny,
                                     withprop,
                                     withoprop,
                                     ppind,
                                     capcont,
                                     nomlife,
                                     validflag,
                                     tranno,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		guarperd.clear();
  		freqann.clear();
  		arrears.clear();
  		advance.clear();
  		dthpercn.clear();
  		dthperco.clear();
  		intanny.clear();
  		withprop.clear();
  		withoprop.clear();
  		ppind.clear();
  		capcont.clear();
  		nomlife.clear();
  		validflag.clear();
  		tranno.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAnbkrec() {
  		return anbkrec;
	}

	public FixedLengthStringData getAnbkpfRecord() {
  		return anbkpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAnbkrec(what);
	}

	public void setAnbkrec(Object what) {
  		this.anbkrec.set(what);
	}

	public void setAnbkpfRecord(Object what) {
  		this.anbkpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(anbkrec.getLength());
		result.set(anbkrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}