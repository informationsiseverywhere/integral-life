package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:13
 * Description:
 * Copybook name: T6691REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6691rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6691Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData earningCap = new ZonedDecimalData(11, 0).isAPartOf(t6691Rec, 0);
  	public FixedLengthStringData pcFundingLimits = new FixedLengthStringData(50).isAPartOf(t6691Rec, 11);
  	public ZonedDecimalData[] pcFundingLimit = ZDArrayPartOfStructure(10, 5, 2, pcFundingLimits, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(pcFundingLimits, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pcFundingLimit01 = new ZonedDecimalData(5, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData pcFundingLimit02 = new ZonedDecimalData(5, 2).isAPartOf(filler, 5);
  	public ZonedDecimalData pcFundingLimit03 = new ZonedDecimalData(5, 2).isAPartOf(filler, 10);
  	public ZonedDecimalData pcFundingLimit04 = new ZonedDecimalData(5, 2).isAPartOf(filler, 15);
  	public ZonedDecimalData pcFundingLimit05 = new ZonedDecimalData(5, 2).isAPartOf(filler, 20);
  	public ZonedDecimalData pcFundingLimit06 = new ZonedDecimalData(5, 2).isAPartOf(filler, 25);
  	public ZonedDecimalData pcFundingLimit07 = new ZonedDecimalData(5, 2).isAPartOf(filler, 30);
  	public ZonedDecimalData pcFundingLimit08 = new ZonedDecimalData(5, 2).isAPartOf(filler, 35);
  	public ZonedDecimalData pcFundingLimit09 = new ZonedDecimalData(5, 2).isAPartOf(filler, 40);
  	public ZonedDecimalData pcFundingLimit10 = new ZonedDecimalData(5, 2).isAPartOf(filler, 45);
  	public ZonedDecimalData pcReleif = new ZonedDecimalData(5, 2).isAPartOf(t6691Rec, 61);
  	public FixedLengthStringData toages = new FixedLengthStringData(20).isAPartOf(t6691Rec, 66);
  	public ZonedDecimalData[] toage = ZDArrayPartOfStructure(10, 2, 0, toages, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(toages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData toage01 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData toage02 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
  	public ZonedDecimalData toage03 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData toage04 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData toage05 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData toage06 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 10);
  	public ZonedDecimalData toage07 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData toage08 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 14);
  	public ZonedDecimalData toage09 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 16);
  	public ZonedDecimalData toage10 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 18);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(414).isAPartOf(t6691Rec, 86, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6691Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6691Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}