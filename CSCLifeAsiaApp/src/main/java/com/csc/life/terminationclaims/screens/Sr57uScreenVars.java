package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;

import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sr57u
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class Sr57uScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(958);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(382).isAPartOf(dataArea, 0);
	public FixedLengthStringData causeofdth = DD.causeofdth.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,4);
	public ZonedDecimalData dtofdeath = DD.dtofdeath.copyToZonedDecimal().isAPartOf(dataFields,7);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,15);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,23);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,53);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,70);
	public ZonedDecimalData totclaim = DD.totclaim.copyToZonedDecimal().isAPartOf(dataFields,74);
	public FixedLengthStringData asterisk = DD.asterisk.copy().isAPartOf(dataFields,91);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,92);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,100);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,108);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,111);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,119);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,149);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,157);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,204);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,212);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,259);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,269);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,277);
	public FixedLengthStringData astrsk = DD.astrsk.copy().isAPartOf(dataFields,287);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,288);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,296);
	public FixedLengthStringData causeofdthdsc = DD.causeofdthdsc.copy().isAPartOf(dataFields,343);
	public FixedLengthStringData claimnumber = DD.claimnumber.copy().isAPartOf(dataFields,373);
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 382);
	public FixedLengthStringData causeofdthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData dtofdeathErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData totclaimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData asteriskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData causeofdthdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData claimnumberErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(432).isAPartOf(dataArea, 526);
	public FixedLengthStringData[] causeofdthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] dtofdeathOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] totclaimOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] asteriskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] causeofdthdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] claimnumberOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(242);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(128).isAPartOf(subfileArea, 0);
	public FixedLengthStringData bnyclt = DD.bnyclt.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData bnynam = DD.bnynam.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData bnypc = DD.bnypc.copyToZonedDecimal().isAPartOf(subfileFields,55);	
	public ZonedDecimalData actvalue = DD.actvalue.copyToZonedDecimal().isAPartOf(subfileFields,60);
	public ZonedDecimalData zclmadjst = DD.zclmadjst.copyToZonedDecimal().isAPartOf(subfileFields,77);
	public ZonedDecimalData zhldclmv = DD.zhldclmv.copyToZonedDecimal().isAPartOf(subfileFields,94);
	public ZonedDecimalData zhldclma = DD.zhldclma.copyToZonedDecimal().isAPartOf(subfileFields,111);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 128);
	public FixedLengthStringData bnycltErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData bnynamErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData bnypcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);	
	public FixedLengthStringData actvalueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData zclmadjstErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData zhldclmvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData zhldclmaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 156);
	public FixedLengthStringData[] bnycltOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] bnynamOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] bnypcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);	
	public FixedLengthStringData[] actvalueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] zclmadjstOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] zhldclmvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] zhldclmaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 240);
	
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData dtofdeathDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sr57uscreensflWritten = new LongData(0);
	public LongData Sr57uscreenctlWritten = new LongData(0);
	public LongData Sr57uscreenWritten = new LongData(0);
	public LongData Sr57uprotectWritten = new LongData(0);
	public GeneralTable sr57uscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr57uscreensfl;
	}

	public Sr57uScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dtofdeathOut,new String[] {"01","09","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"02","10","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(causeofdthOut,new String[] {"03","09","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(otheradjstOut,new String[] {"07","09","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"05","09","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(longdescOut,new String[] {"06","09","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zhldclmvOut,new String[] {"08","09","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zhldclmaOut,new String[] {"11","09","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenSflFields = new BaseData[] {bnyclt, bnynam, bnypc, actvalue, zclmadjst, zhldclmv, zhldclma};
		screenSflOutFields = new BaseData[][] {bnycltOut, bnynamOut, bnypcOut, actvalueOut, zclmadjstOut, zhldclmvOut, zhldclmaOut};
		screenSflErrFields = new BaseData[] {bnycltErr, bnynamErr, bnypcErr, actvalueErr, zclmadjstErr, zhldclmvErr, zhldclmaErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, ptdate, btdate, asterisk, dtofdeath, effdate, causeofdth, otheradjst, reasoncd, longdesc, currcd, totclaim, claimnumber};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, ptdateOut, btdateOut, asteriskOut, dtofdeathOut, effdateOut, causeofdthOut, otheradjstOut, reasoncdOut, longdescOut, currcdOut, totclaimOut, claimnumberOut };
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, ptdateErr, btdateErr, asteriskErr, dtofdeathErr, effdateErr, causeofdthErr, otheradjstErr, reasoncdErr, longdescErr, currcdErr, totclaimErr, claimnumberErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, dtofdeath, effdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, dtofdeathErr, effdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, dtofdeathDisp, effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr57uscreen.class;
		screenSflRecord = Sr57uscreensfl.class;
		screenCtlRecord = Sr57uscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr57uprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr57uscreenctl.lrec.pageSubfile);
	}
}
