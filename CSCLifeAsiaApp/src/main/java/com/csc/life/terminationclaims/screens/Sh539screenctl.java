package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class Sh539screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sh539screensfl";
		lrec.subfileClass = Sh539screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 4;
		lrec.pageSubfile = 3;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 14, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh539ScreenVars sv = (Sh539ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh539screenctlWritten, sv.Sh539screensflWritten, av, sv.sh539screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh539ScreenVars screenVars = (Sh539ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.bilfrqdesc.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.flag.setClassString("");
	}

/**
 * Clear all the variables in Sh539screenctl
 */
	public static void clear(VarModel pv) {
		Sh539ScreenVars screenVars = (Sh539ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.billfreq.clear();
		screenVars.bilfrqdesc.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.mop.clear();
		screenVars.flag.clear();
	}
}
