package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 10/03/20 05:41
 * @author Sparikh8
 */
public class Sjl42screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {17, 23, 2, 79}); 
	}

	/**
	 * Writes a record to the screen.
	 * @param errorInd - will be set on if an error occurs
	 * @param noRecordFoundInd - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
		Sjl42ScreenVars sv = (Sjl42ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl42screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl42ScreenVars screenVars = (Sjl42ScreenVars)pv;
		screenVars.effdateDisp.setClassString("");
		screenVars.causeofdth.setClassString("");
		screenVars.dtofdeathDisp.setClassString("");
		screenVars.claim.setClassString("");
	}

	/**
	 * Clear all the variables in S5256screen
	 */
	public static void clear(VarModel pv) {
		Sjl42ScreenVars screenVars = (Sjl42ScreenVars) pv;
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.causeofdth.clear();
		screenVars.dtofdeathDisp.clear();
		screenVars.dtofdeath.clear();
		screenVars.claim.clear();
	}
}
