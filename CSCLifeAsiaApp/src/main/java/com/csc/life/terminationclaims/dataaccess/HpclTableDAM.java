package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HpclTableDAM.java
 * Date: Sun, 30 Aug 2009 03:41:14
 * Class transformed from HPCL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HpclTableDAM extends HpclpfTableDAM {

	public HpclTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HPCL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", RGPYNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "RGPYNUM, " +
		            "ITMFRM, " +
		            "ITMTO, " +
		            "BENEFITS01, " +
		            "BENEFITS02, " +
		            "BENEFITS03, " +
		            "BENEFITS04, " +
		            "BENEFITS05, " +
		            "BENEFITS06, " +
		            "BENEFITS07, " +
		            "BENEFITS08, " +
		            "BENEFITS09, " +
		            "BENEFITS10, " +
		            "BENEFITS11, " +
		            "BENEFITS12, " +
		            "BENEFITS13, " +
		            "BENFAMT01, " +
		            "BENFAMT02, " +
		            "BENFAMT03, " +
		            "BENFAMT04, " +
		            "BENFAMT05, " +
		            "BENFAMT06, " +
		            "BENFAMT07, " +
		            "BENFAMT08, " +
		            "BENFAMT09, " +
		            "BENFAMT10, " +
		            "BENFAMT11, " +
		            "BENFAMT12, " +
		            "BENFAMT13, " +
		            "DACLAIM01, " +
		            "DACLAIM02, " +
		            "DACLAIM03, " +
		            "DACLAIM04, " +
		            "DACLAIM05, " +
		            "DACLAIM06, " +
		            "DACLAIM07, " +
		            "DACLAIM08, " +
		            "DACLAIM09, " +
		            "DACLAIM10, " +
		            "DACLAIM11, " +
		            "DACLAIM12, " +
		            "DACLAIM13, " +
		            "CLAIMTOT, " +
		            "PREMUNIT, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "RGPYNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "RGPYNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               rgpynum,
                               itmfrm,
                               itmto,
                               benefits01,
                               benefits02,
                               benefits03,
                               benefits04,
                               benefits05,
                               benefits06,
                               benefits07,
                               benefits08,
                               benefits09,
                               benefits10,
                               benefits11,
                               benefits12,
                               benefits13,
                               benfamt01,
                               benfamt02,
                               benfamt03,
                               benfamt04,
                               benfamt05,
                               benfamt06,
                               benfamt07,
                               benfamt08,
                               benfamt09,
                               benfamt10,
                               benfamt11,
                               benfamt12,
                               benfamt13,
                               daclaim01,
                               daclaim02,
                               daclaim03,
                               daclaim04,
                               daclaim05,
                               daclaim06,
                               daclaim07,
                               daclaim08,
                               daclaim09,
                               daclaim10,
                               daclaim11,
                               daclaim12,
                               daclaim13,
                               claimtot,
                               premunit,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(46);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getRgpynum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, rgpynum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(rgpynum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(538);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ getItmfrm().toInternal()
					+ getItmto().toInternal()
					+ getBenefits01().toInternal()
					+ getBenefits02().toInternal()
					+ getBenefits03().toInternal()
					+ getBenefits04().toInternal()
					+ getBenefits05().toInternal()
					+ getBenefits06().toInternal()
					+ getBenefits07().toInternal()
					+ getBenefits08().toInternal()
					+ getBenefits09().toInternal()
					+ getBenefits10().toInternal()
					+ getBenefits11().toInternal()
					+ getBenefits12().toInternal()
					+ getBenefits13().toInternal()
					+ getBenfamt01().toInternal()
					+ getBenfamt02().toInternal()
					+ getBenfamt03().toInternal()
					+ getBenfamt04().toInternal()
					+ getBenfamt05().toInternal()
					+ getBenfamt06().toInternal()
					+ getBenfamt07().toInternal()
					+ getBenfamt08().toInternal()
					+ getBenfamt09().toInternal()
					+ getBenfamt10().toInternal()
					+ getBenfamt11().toInternal()
					+ getBenfamt12().toInternal()
					+ getBenfamt13().toInternal()
					+ getDaclaim01().toInternal()
					+ getDaclaim02().toInternal()
					+ getDaclaim03().toInternal()
					+ getDaclaim04().toInternal()
					+ getDaclaim05().toInternal()
					+ getDaclaim06().toInternal()
					+ getDaclaim07().toInternal()
					+ getDaclaim08().toInternal()
					+ getDaclaim09().toInternal()
					+ getDaclaim10().toInternal()
					+ getDaclaim11().toInternal()
					+ getDaclaim12().toInternal()
					+ getDaclaim13().toInternal()
					+ getClaimtot().toInternal()
					+ getPremunit().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, itmfrm);
			what = ExternalData.chop(what, itmto);
			what = ExternalData.chop(what, benefits01);
			what = ExternalData.chop(what, benefits02);
			what = ExternalData.chop(what, benefits03);
			what = ExternalData.chop(what, benefits04);
			what = ExternalData.chop(what, benefits05);
			what = ExternalData.chop(what, benefits06);
			what = ExternalData.chop(what, benefits07);
			what = ExternalData.chop(what, benefits08);
			what = ExternalData.chop(what, benefits09);
			what = ExternalData.chop(what, benefits10);
			what = ExternalData.chop(what, benefits11);
			what = ExternalData.chop(what, benefits12);
			what = ExternalData.chop(what, benefits13);
			what = ExternalData.chop(what, benfamt01);
			what = ExternalData.chop(what, benfamt02);
			what = ExternalData.chop(what, benfamt03);
			what = ExternalData.chop(what, benfamt04);
			what = ExternalData.chop(what, benfamt05);
			what = ExternalData.chop(what, benfamt06);
			what = ExternalData.chop(what, benfamt07);
			what = ExternalData.chop(what, benfamt08);
			what = ExternalData.chop(what, benfamt09);
			what = ExternalData.chop(what, benfamt10);
			what = ExternalData.chop(what, benfamt11);
			what = ExternalData.chop(what, benfamt12);
			what = ExternalData.chop(what, benfamt13);
			what = ExternalData.chop(what, daclaim01);
			what = ExternalData.chop(what, daclaim02);
			what = ExternalData.chop(what, daclaim03);
			what = ExternalData.chop(what, daclaim04);
			what = ExternalData.chop(what, daclaim05);
			what = ExternalData.chop(what, daclaim06);
			what = ExternalData.chop(what, daclaim07);
			what = ExternalData.chop(what, daclaim08);
			what = ExternalData.chop(what, daclaim09);
			what = ExternalData.chop(what, daclaim10);
			what = ExternalData.chop(what, daclaim11);
			what = ExternalData.chop(what, daclaim12);
			what = ExternalData.chop(what, daclaim13);
			what = ExternalData.chop(what, claimtot);
			what = ExternalData.chop(what, premunit);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getRgpynum() {
		return rgpynum;
	}
	public void setRgpynum(Object what) {
		setRgpynum(what, false);
	}
	public void setRgpynum(Object what, boolean rounded) {
		if (rounded)
			rgpynum.setRounded(what);
		else
			rgpynum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getItmfrm() {
		return itmfrm;
	}
	public void setItmfrm(Object what) {
		setItmfrm(what, false);
	}
	public void setItmfrm(Object what, boolean rounded) {
		if (rounded)
			itmfrm.setRounded(what);
		else
			itmfrm.set(what);
	}	
	public PackedDecimalData getItmto() {
		return itmto;
	}
	public void setItmto(Object what) {
		setItmto(what, false);
	}
	public void setItmto(Object what, boolean rounded) {
		if (rounded)
			itmto.setRounded(what);
		else
			itmto.set(what);
	}	
	public FixedLengthStringData getBenefits01() {
		return benefits01;
	}
	public void setBenefits01(Object what) {
		benefits01.set(what);
	}	
	public FixedLengthStringData getBenefits02() {
		return benefits02;
	}
	public void setBenefits02(Object what) {
		benefits02.set(what);
	}	
	public FixedLengthStringData getBenefits03() {
		return benefits03;
	}
	public void setBenefits03(Object what) {
		benefits03.set(what);
	}	
	public FixedLengthStringData getBenefits04() {
		return benefits04;
	}
	public void setBenefits04(Object what) {
		benefits04.set(what);
	}	
	public FixedLengthStringData getBenefits05() {
		return benefits05;
	}
	public void setBenefits05(Object what) {
		benefits05.set(what);
	}	
	public FixedLengthStringData getBenefits06() {
		return benefits06;
	}
	public void setBenefits06(Object what) {
		benefits06.set(what);
	}	
	public FixedLengthStringData getBenefits07() {
		return benefits07;
	}
	public void setBenefits07(Object what) {
		benefits07.set(what);
	}	
	public FixedLengthStringData getBenefits08() {
		return benefits08;
	}
	public void setBenefits08(Object what) {
		benefits08.set(what);
	}	
	public FixedLengthStringData getBenefits09() {
		return benefits09;
	}
	public void setBenefits09(Object what) {
		benefits09.set(what);
	}	
	public FixedLengthStringData getBenefits10() {
		return benefits10;
	}
	public void setBenefits10(Object what) {
		benefits10.set(what);
	}	
	public FixedLengthStringData getBenefits11() {
		return benefits11;
	}
	public void setBenefits11(Object what) {
		benefits11.set(what);
	}	
	public FixedLengthStringData getBenefits12() {
		return benefits12;
	}
	public void setBenefits12(Object what) {
		benefits12.set(what);
	}	
	public FixedLengthStringData getBenefits13() {
		return benefits13;
	}
	public void setBenefits13(Object what) {
		benefits13.set(what);
	}	
	public PackedDecimalData getBenfamt01() {
		return benfamt01;
	}
	public void setBenfamt01(Object what) {
		setBenfamt01(what, false);
	}
	public void setBenfamt01(Object what, boolean rounded) {
		if (rounded)
			benfamt01.setRounded(what);
		else
			benfamt01.set(what);
	}	
	public PackedDecimalData getBenfamt02() {
		return benfamt02;
	}
	public void setBenfamt02(Object what) {
		setBenfamt02(what, false);
	}
	public void setBenfamt02(Object what, boolean rounded) {
		if (rounded)
			benfamt02.setRounded(what);
		else
			benfamt02.set(what);
	}	
	public PackedDecimalData getBenfamt03() {
		return benfamt03;
	}
	public void setBenfamt03(Object what) {
		setBenfamt03(what, false);
	}
	public void setBenfamt03(Object what, boolean rounded) {
		if (rounded)
			benfamt03.setRounded(what);
		else
			benfamt03.set(what);
	}	
	public PackedDecimalData getBenfamt04() {
		return benfamt04;
	}
	public void setBenfamt04(Object what) {
		setBenfamt04(what, false);
	}
	public void setBenfamt04(Object what, boolean rounded) {
		if (rounded)
			benfamt04.setRounded(what);
		else
			benfamt04.set(what);
	}	
	public PackedDecimalData getBenfamt05() {
		return benfamt05;
	}
	public void setBenfamt05(Object what) {
		setBenfamt05(what, false);
	}
	public void setBenfamt05(Object what, boolean rounded) {
		if (rounded)
			benfamt05.setRounded(what);
		else
			benfamt05.set(what);
	}	
	public PackedDecimalData getBenfamt06() {
		return benfamt06;
	}
	public void setBenfamt06(Object what) {
		setBenfamt06(what, false);
	}
	public void setBenfamt06(Object what, boolean rounded) {
		if (rounded)
			benfamt06.setRounded(what);
		else
			benfamt06.set(what);
	}	
	public PackedDecimalData getBenfamt07() {
		return benfamt07;
	}
	public void setBenfamt07(Object what) {
		setBenfamt07(what, false);
	}
	public void setBenfamt07(Object what, boolean rounded) {
		if (rounded)
			benfamt07.setRounded(what);
		else
			benfamt07.set(what);
	}	
	public PackedDecimalData getBenfamt08() {
		return benfamt08;
	}
	public void setBenfamt08(Object what) {
		setBenfamt08(what, false);
	}
	public void setBenfamt08(Object what, boolean rounded) {
		if (rounded)
			benfamt08.setRounded(what);
		else
			benfamt08.set(what);
	}	
	public PackedDecimalData getBenfamt09() {
		return benfamt09;
	}
	public void setBenfamt09(Object what) {
		setBenfamt09(what, false);
	}
	public void setBenfamt09(Object what, boolean rounded) {
		if (rounded)
			benfamt09.setRounded(what);
		else
			benfamt09.set(what);
	}	
	public PackedDecimalData getBenfamt10() {
		return benfamt10;
	}
	public void setBenfamt10(Object what) {
		setBenfamt10(what, false);
	}
	public void setBenfamt10(Object what, boolean rounded) {
		if (rounded)
			benfamt10.setRounded(what);
		else
			benfamt10.set(what);
	}	
	public PackedDecimalData getBenfamt11() {
		return benfamt11;
	}
	public void setBenfamt11(Object what) {
		setBenfamt11(what, false);
	}
	public void setBenfamt11(Object what, boolean rounded) {
		if (rounded)
			benfamt11.setRounded(what);
		else
			benfamt11.set(what);
	}	
	public PackedDecimalData getBenfamt12() {
		return benfamt12;
	}
	public void setBenfamt12(Object what) {
		setBenfamt12(what, false);
	}
	public void setBenfamt12(Object what, boolean rounded) {
		if (rounded)
			benfamt12.setRounded(what);
		else
			benfamt12.set(what);
	}	
	public PackedDecimalData getBenfamt13() {
		return benfamt13;
	}
	public void setBenfamt13(Object what) {
		setBenfamt13(what, false);
	}
	public void setBenfamt13(Object what, boolean rounded) {
		if (rounded)
			benfamt13.setRounded(what);
		else
			benfamt13.set(what);
	}	
	public PackedDecimalData getDaclaim01() {
		return daclaim01;
	}
	public void setDaclaim01(Object what) {
		setDaclaim01(what, false);
	}
	public void setDaclaim01(Object what, boolean rounded) {
		if (rounded)
			daclaim01.setRounded(what);
		else
			daclaim01.set(what);
	}	
	public PackedDecimalData getDaclaim02() {
		return daclaim02;
	}
	public void setDaclaim02(Object what) {
		setDaclaim02(what, false);
	}
	public void setDaclaim02(Object what, boolean rounded) {
		if (rounded)
			daclaim02.setRounded(what);
		else
			daclaim02.set(what);
	}	
	public PackedDecimalData getDaclaim03() {
		return daclaim03;
	}
	public void setDaclaim03(Object what) {
		setDaclaim03(what, false);
	}
	public void setDaclaim03(Object what, boolean rounded) {
		if (rounded)
			daclaim03.setRounded(what);
		else
			daclaim03.set(what);
	}	
	public PackedDecimalData getDaclaim04() {
		return daclaim04;
	}
	public void setDaclaim04(Object what) {
		setDaclaim04(what, false);
	}
	public void setDaclaim04(Object what, boolean rounded) {
		if (rounded)
			daclaim04.setRounded(what);
		else
			daclaim04.set(what);
	}	
	public PackedDecimalData getDaclaim05() {
		return daclaim05;
	}
	public void setDaclaim05(Object what) {
		setDaclaim05(what, false);
	}
	public void setDaclaim05(Object what, boolean rounded) {
		if (rounded)
			daclaim05.setRounded(what);
		else
			daclaim05.set(what);
	}	
	public PackedDecimalData getDaclaim06() {
		return daclaim06;
	}
	public void setDaclaim06(Object what) {
		setDaclaim06(what, false);
	}
	public void setDaclaim06(Object what, boolean rounded) {
		if (rounded)
			daclaim06.setRounded(what);
		else
			daclaim06.set(what);
	}	
	public PackedDecimalData getDaclaim07() {
		return daclaim07;
	}
	public void setDaclaim07(Object what) {
		setDaclaim07(what, false);
	}
	public void setDaclaim07(Object what, boolean rounded) {
		if (rounded)
			daclaim07.setRounded(what);
		else
			daclaim07.set(what);
	}	
	public PackedDecimalData getDaclaim08() {
		return daclaim08;
	}
	public void setDaclaim08(Object what) {
		setDaclaim08(what, false);
	}
	public void setDaclaim08(Object what, boolean rounded) {
		if (rounded)
			daclaim08.setRounded(what);
		else
			daclaim08.set(what);
	}	
	public PackedDecimalData getDaclaim09() {
		return daclaim09;
	}
	public void setDaclaim09(Object what) {
		setDaclaim09(what, false);
	}
	public void setDaclaim09(Object what, boolean rounded) {
		if (rounded)
			daclaim09.setRounded(what);
		else
			daclaim09.set(what);
	}	
	public PackedDecimalData getDaclaim10() {
		return daclaim10;
	}
	public void setDaclaim10(Object what) {
		setDaclaim10(what, false);
	}
	public void setDaclaim10(Object what, boolean rounded) {
		if (rounded)
			daclaim10.setRounded(what);
		else
			daclaim10.set(what);
	}	
	public PackedDecimalData getDaclaim11() {
		return daclaim11;
	}
	public void setDaclaim11(Object what) {
		setDaclaim11(what, false);
	}
	public void setDaclaim11(Object what, boolean rounded) {
		if (rounded)
			daclaim11.setRounded(what);
		else
			daclaim11.set(what);
	}	
	public PackedDecimalData getDaclaim12() {
		return daclaim12;
	}
	public void setDaclaim12(Object what) {
		setDaclaim12(what, false);
	}
	public void setDaclaim12(Object what, boolean rounded) {
		if (rounded)
			daclaim12.setRounded(what);
		else
			daclaim12.set(what);
	}	
	public PackedDecimalData getDaclaim13() {
		return daclaim13;
	}
	public void setDaclaim13(Object what) {
		setDaclaim13(what, false);
	}
	public void setDaclaim13(Object what, boolean rounded) {
		if (rounded)
			daclaim13.setRounded(what);
		else
			daclaim13.set(what);
	}	
	public PackedDecimalData getClaimtot() {
		return claimtot;
	}
	public void setClaimtot(Object what) {
		setClaimtot(what, false);
	}
	public void setClaimtot(Object what, boolean rounded) {
		if (rounded)
			claimtot.setRounded(what);
		else
			claimtot.set(what);
	}	
	public PackedDecimalData getPremunit() {
		return premunit;
	}
	public void setPremunit(Object what) {
		setPremunit(what, false);
	}
	public void setPremunit(Object what, boolean rounded) {
		if (rounded)
			premunit.setRounded(what);
		else
			premunit.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getDaclaims() {
		return new FixedLengthStringData(daclaim01.toInternal()
										+ daclaim02.toInternal()
										+ daclaim03.toInternal()
										+ daclaim04.toInternal()
										+ daclaim05.toInternal()
										+ daclaim06.toInternal()
										+ daclaim07.toInternal()
										+ daclaim08.toInternal()
										+ daclaim09.toInternal()
										+ daclaim10.toInternal()
										+ daclaim11.toInternal()
										+ daclaim12.toInternal()
										+ daclaim13.toInternal());
	}
	public void setDaclaims(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getDaclaims().getLength()).init(obj);
	
		what = ExternalData.chop(what, daclaim01);
		what = ExternalData.chop(what, daclaim02);
		what = ExternalData.chop(what, daclaim03);
		what = ExternalData.chop(what, daclaim04);
		what = ExternalData.chop(what, daclaim05);
		what = ExternalData.chop(what, daclaim06);
		what = ExternalData.chop(what, daclaim07);
		what = ExternalData.chop(what, daclaim08);
		what = ExternalData.chop(what, daclaim09);
		what = ExternalData.chop(what, daclaim10);
		what = ExternalData.chop(what, daclaim11);
		what = ExternalData.chop(what, daclaim12);
		what = ExternalData.chop(what, daclaim13);
	}
	public PackedDecimalData getDaclaim(BaseData indx) {
		return getDaclaim(indx.toInt());
	}
	public PackedDecimalData getDaclaim(int indx) {

		switch (indx) {
			case 1 : return daclaim01;
			case 2 : return daclaim02;
			case 3 : return daclaim03;
			case 4 : return daclaim04;
			case 5 : return daclaim05;
			case 6 : return daclaim06;
			case 7 : return daclaim07;
			case 8 : return daclaim08;
			case 9 : return daclaim09;
			case 10 : return daclaim10;
			case 11 : return daclaim11;
			case 12 : return daclaim12;
			case 13 : return daclaim13;
			default: return null; // Throw error instead?
		}
	
	}
	public void setDaclaim(BaseData indx, Object what) {
		setDaclaim(indx, what, false);
	}
	public void setDaclaim(BaseData indx, Object what, boolean rounded) {
		setDaclaim(indx.toInt(), what, rounded);
	}
	public void setDaclaim(int indx, Object what) {
		setDaclaim(indx, what, false);
	}
	public void setDaclaim(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setDaclaim01(what, rounded);
					 break;
			case 2 : setDaclaim02(what, rounded);
					 break;
			case 3 : setDaclaim03(what, rounded);
					 break;
			case 4 : setDaclaim04(what, rounded);
					 break;
			case 5 : setDaclaim05(what, rounded);
					 break;
			case 6 : setDaclaim06(what, rounded);
					 break;
			case 7 : setDaclaim07(what, rounded);
					 break;
			case 8 : setDaclaim08(what, rounded);
					 break;
			case 9 : setDaclaim09(what, rounded);
					 break;
			case 10 : setDaclaim10(what, rounded);
					 break;
			case 11 : setDaclaim11(what, rounded);
					 break;
			case 12 : setDaclaim12(what, rounded);
					 break;
			case 13 : setDaclaim13(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBenfamts() {
		return new FixedLengthStringData(benfamt01.toInternal()
										+ benfamt02.toInternal()
										+ benfamt03.toInternal()
										+ benfamt04.toInternal()
										+ benfamt05.toInternal()
										+ benfamt06.toInternal()
										+ benfamt07.toInternal()
										+ benfamt08.toInternal()
										+ benfamt09.toInternal()
										+ benfamt10.toInternal()
										+ benfamt11.toInternal()
										+ benfamt12.toInternal()
										+ benfamt13.toInternal());
	}
	public void setBenfamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBenfamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, benfamt01);
		what = ExternalData.chop(what, benfamt02);
		what = ExternalData.chop(what, benfamt03);
		what = ExternalData.chop(what, benfamt04);
		what = ExternalData.chop(what, benfamt05);
		what = ExternalData.chop(what, benfamt06);
		what = ExternalData.chop(what, benfamt07);
		what = ExternalData.chop(what, benfamt08);
		what = ExternalData.chop(what, benfamt09);
		what = ExternalData.chop(what, benfamt10);
		what = ExternalData.chop(what, benfamt11);
		what = ExternalData.chop(what, benfamt12);
		what = ExternalData.chop(what, benfamt13);
	}
	public PackedDecimalData getBenfamt(BaseData indx) {
		return getBenfamt(indx.toInt());
	}
	public PackedDecimalData getBenfamt(int indx) {

		switch (indx) {
			case 1 : return benfamt01;
			case 2 : return benfamt02;
			case 3 : return benfamt03;
			case 4 : return benfamt04;
			case 5 : return benfamt05;
			case 6 : return benfamt06;
			case 7 : return benfamt07;
			case 8 : return benfamt08;
			case 9 : return benfamt09;
			case 10 : return benfamt10;
			case 11 : return benfamt11;
			case 12 : return benfamt12;
			case 13 : return benfamt13;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBenfamt(BaseData indx, Object what) {
		setBenfamt(indx, what, false);
	}
	public void setBenfamt(BaseData indx, Object what, boolean rounded) {
		setBenfamt(indx.toInt(), what, rounded);
	}
	public void setBenfamt(int indx, Object what) {
		setBenfamt(indx, what, false);
	}
	public void setBenfamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBenfamt01(what, rounded);
					 break;
			case 2 : setBenfamt02(what, rounded);
					 break;
			case 3 : setBenfamt03(what, rounded);
					 break;
			case 4 : setBenfamt04(what, rounded);
					 break;
			case 5 : setBenfamt05(what, rounded);
					 break;
			case 6 : setBenfamt06(what, rounded);
					 break;
			case 7 : setBenfamt07(what, rounded);
					 break;
			case 8 : setBenfamt08(what, rounded);
					 break;
			case 9 : setBenfamt09(what, rounded);
					 break;
			case 10 : setBenfamt10(what, rounded);
					 break;
			case 11 : setBenfamt11(what, rounded);
					 break;
			case 12 : setBenfamt12(what, rounded);
					 break;
			case 13 : setBenfamt13(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBenefitss() {
		return new FixedLengthStringData(benefits01.toInternal()
										+ benefits02.toInternal()
										+ benefits03.toInternal()
										+ benefits04.toInternal()
										+ benefits05.toInternal()
										+ benefits06.toInternal()
										+ benefits07.toInternal()
										+ benefits08.toInternal()
										+ benefits09.toInternal()
										+ benefits10.toInternal()
										+ benefits11.toInternal()
										+ benefits12.toInternal()
										+ benefits13.toInternal());
	}
	public void setBenefitss(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBenefitss().getLength()).init(obj);
	
		what = ExternalData.chop(what, benefits01);
		what = ExternalData.chop(what, benefits02);
		what = ExternalData.chop(what, benefits03);
		what = ExternalData.chop(what, benefits04);
		what = ExternalData.chop(what, benefits05);
		what = ExternalData.chop(what, benefits06);
		what = ExternalData.chop(what, benefits07);
		what = ExternalData.chop(what, benefits08);
		what = ExternalData.chop(what, benefits09);
		what = ExternalData.chop(what, benefits10);
		what = ExternalData.chop(what, benefits11);
		what = ExternalData.chop(what, benefits12);
		what = ExternalData.chop(what, benefits13);
	}
	public FixedLengthStringData getBenefits(BaseData indx) {
		return getBenefits(indx.toInt());
	}
	public FixedLengthStringData getBenefits(int indx) {

		switch (indx) {
			case 1 : return benefits01;
			case 2 : return benefits02;
			case 3 : return benefits03;
			case 4 : return benefits04;
			case 5 : return benefits05;
			case 6 : return benefits06;
			case 7 : return benefits07;
			case 8 : return benefits08;
			case 9 : return benefits09;
			case 10 : return benefits10;
			case 11 : return benefits11;
			case 12 : return benefits12;
			case 13 : return benefits13;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBenefits(BaseData indx, Object what) {
		setBenefits(indx.toInt(), what);
	}
	public void setBenefits(int indx, Object what) {

		switch (indx) {
			case 1 : setBenefits01(what);
					 break;
			case 2 : setBenefits02(what);
					 break;
			case 3 : setBenefits03(what);
					 break;
			case 4 : setBenefits04(what);
					 break;
			case 5 : setBenefits05(what);
					 break;
			case 6 : setBenefits06(what);
					 break;
			case 7 : setBenefits07(what);
					 break;
			case 8 : setBenefits08(what);
					 break;
			case 9 : setBenefits09(what);
					 break;
			case 10 : setBenefits10(what);
					 break;
			case 11 : setBenefits11(what);
					 break;
			case 12 : setBenefits12(what);
					 break;
			case 13 : setBenefits13(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		rgpynum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		itmfrm.clear();
		itmto.clear();
		benefits01.clear();
		benefits02.clear();
		benefits03.clear();
		benefits04.clear();
		benefits05.clear();
		benefits06.clear();
		benefits07.clear();
		benefits08.clear();
		benefits09.clear();
		benefits10.clear();
		benefits11.clear();
		benefits12.clear();
		benefits13.clear();
		benfamt01.clear();
		benfamt02.clear();
		benfamt03.clear();
		benfamt04.clear();
		benfamt05.clear();
		benfamt06.clear();
		benfamt07.clear();
		benfamt08.clear();
		benfamt09.clear();
		benfamt10.clear();
		benfamt11.clear();
		benfamt12.clear();
		benfamt13.clear();
		daclaim01.clear();
		daclaim02.clear();
		daclaim03.clear();
		daclaim04.clear();
		daclaim05.clear();
		daclaim06.clear();
		daclaim07.clear();
		daclaim08.clear();
		daclaim09.clear();
		daclaim10.clear();
		daclaim11.clear();
		daclaim12.clear();
		daclaim13.clear();
		claimtot.clear();
		premunit.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}