package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Nohspf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

/**
 * 
 * @author ehyrat
 * related database table NOHSPF DAO
 */

public interface NohspfDAO extends BaseDAO<Nohspf>{
	public boolean insertNohspf(Nohspf nohspf);
//	public int getCountByLifeAssNum(String notihscnum);
	public String getMaxTransnum(String notihsnum);
	public List<Nohspf> getNotificationHistoryRecord(String notihscnum);
	//ICIL-1502
	public List<Nohspf> getCombainedNotificationHistoryRecord(String notihscnum);
}
