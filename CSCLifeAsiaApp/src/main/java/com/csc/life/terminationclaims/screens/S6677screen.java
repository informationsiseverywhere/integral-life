package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6677screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 21, 20, 10}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6677ScreenVars sv = (S6677ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6677screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6677ScreenVars screenVars = (S6677ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.currds.setClassString("");
		screenVars.rgpynum.setClassString("");
		screenVars.rgpytypesd.setClassString("");
		screenVars.rgpystat.setClassString("");
		screenVars.statdsc.setClassString("");
		screenVars.cltype.setClassString("");
		screenVars.clmdesc.setClassString("");
		screenVars.claimevd.setClassString("");
		screenVars.rgpymop.setClassString("");
		screenVars.rgpyshort.setClassString("");
		screenVars.regpayfreq.setClassString("");
		screenVars.frqdesc.setClassString("");
		screenVars.payclt.setClassString("");
		screenVars.payenme.setClassString("");
		screenVars.prcnt.setClassString("");
		screenVars.destkey.setClassString("");
		screenVars.pymt.setClassString("");
		screenVars.totamnt.setClassString("");
		//CML-009
		screenVars.adjustamt.setClassString("");
		screenVars.netclaimamt.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.resndesc.setClassString("");
		screenVars.pymtAdj.setClassString("");
		screenVars.claimcur.setClassString("");
		screenVars.clmcurdsc.setClassString("");
		screenVars.aprvdateDisp.setClassString("");
		screenVars.crtdateDisp.setClassString("");
		screenVars.revdteDisp.setClassString("");
		screenVars.firstPaydateDisp.setClassString("");
		screenVars.lastPaydateDisp.setClassString("");
		screenVars.nextPaydateDisp.setClassString("");
		screenVars.anvdateDisp.setClassString("");
		screenVars.finalPaydateDisp.setClassString("");
		screenVars.cancelDateDisp.setClassString("");
		screenVars.totalamt.setClassString("");
		screenVars.msgclaim.setClassString("");
		screenVars.fupflg.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.zrsumin.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.anntind.setClassString("");
		screenVars.agclmstz.setClassString("");
		screenVars.recvdDateDisp.setClassString("");
		screenVars.incurdtDisp.setClassString("");
		// CML-004
		screenVars.intstdays.setClassString("");
		screenVars.intstrate.setClassString("");
		screenVars.intstamt.setClassString("");
		screenVars.claimno.setClassString("");
		screenVars.notifino.setClassString("");
		screenVars.investres.setClassString("");
		screenVars.claimnotes.setClassString("");
		screenVars.riskcommdteDisp.setClassString("");	//ILJ-48
	}

/**
 * Clear all the variables in S6677screen
 */
	public static void clear(VarModel pv) {
		S6677ScreenVars screenVars = (S6677ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.currcd.clear();
		screenVars.currds.clear();
		screenVars.rgpynum.clear();
		screenVars.rgpytypesd.clear();
		screenVars.rgpystat.clear();
		screenVars.statdsc.clear();
		screenVars.cltype.clear();
		screenVars.clmdesc.clear();
		screenVars.claimevd.clear();
		screenVars.rgpymop.clear();
		screenVars.rgpyshort.clear();
		screenVars.regpayfreq.clear();
		screenVars.frqdesc.clear();
		screenVars.payclt.clear();
		screenVars.payenme.clear();
		screenVars.prcnt.clear();
		screenVars.destkey.clear();
		screenVars.pymt.clear();
		screenVars.totamnt.clear();
		//CML-009
		screenVars.adjustamt.clear();
		screenVars.netclaimamt.clear();
		screenVars.reasoncd.clear();
		screenVars.resndesc.clear();
		screenVars.pymtAdj.clear();
		screenVars.claimcur.clear();
		screenVars.clmcurdsc.clear();
		screenVars.aprvdateDisp.clear();
		screenVars.aprvdate.clear();
		screenVars.crtdateDisp.clear();
		screenVars.crtdate.clear();
		screenVars.revdteDisp.clear();
		screenVars.revdte.clear();
		screenVars.firstPaydateDisp.clear();
		screenVars.firstPaydate.clear();
		screenVars.lastPaydateDisp.clear();
		screenVars.lastPaydate.clear();
		screenVars.nextPaydateDisp.clear();
		screenVars.nextPaydate.clear();
		screenVars.anvdateDisp.clear();
		screenVars.anvdate.clear();
		screenVars.finalPaydateDisp.clear();
		screenVars.finalPaydate.clear();
		screenVars.cancelDateDisp.clear();
		screenVars.cancelDate.clear();
		screenVars.totalamt.clear();
		screenVars.msgclaim.clear();
		screenVars.fupflg.clear();
		screenVars.ddind.clear();
		screenVars.crtable.clear();
		screenVars.zrsumin.clear();
		screenVars.descrip.clear();
		screenVars.anntind.clear();
		screenVars.agclmstz.clear();
		screenVars.recvdDateDisp.clear();
		screenVars.recvdDate.clear();
		screenVars.incurdtDisp.clear();
		screenVars.incurdt.clear();
		
		// CML-004
		screenVars.intstdays.clear();
		screenVars.intstrate.clear();
		screenVars.intstamt.clear();
		screenVars.claimno.clear();
		screenVars.notifino.clear();
		screenVars.investres.clear();
		screenVars.claimnotes.clear();
		screenVars.riskcommdteDisp.clear();	//ILJ-48
		screenVars.riskcommdte.clear();		//ILJ-48
	}
}
