package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:44
 * Description:
 * Copybook name: REGPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regpFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData regpKey = new FixedLengthStringData(256).isAPartOf(regpFileKey, 0, REDEFINE);
  	public FixedLengthStringData regpChdrcoy = new FixedLengthStringData(1).isAPartOf(regpKey, 0);
  	public FixedLengthStringData regpChdrnum = new FixedLengthStringData(8).isAPartOf(regpKey, 1);
  	public FixedLengthStringData regpLife = new FixedLengthStringData(2).isAPartOf(regpKey, 9);
  	public FixedLengthStringData regpCoverage = new FixedLengthStringData(2).isAPartOf(regpKey, 11);
  	public FixedLengthStringData regpRider = new FixedLengthStringData(2).isAPartOf(regpKey, 13);
  	public PackedDecimalData regpRgpynum = new PackedDecimalData(5, 0).isAPartOf(regpKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(regpKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}