package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:15
 * Description:
 * Copybook name: ANNTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Anntkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData anntFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData anntKey = new FixedLengthStringData(64).isAPartOf(anntFileKey, 0, REDEFINE);
  	public FixedLengthStringData anntChdrcoy = new FixedLengthStringData(1).isAPartOf(anntKey, 0);
  	public FixedLengthStringData anntChdrnum = new FixedLengthStringData(8).isAPartOf(anntKey, 1);
  	public FixedLengthStringData anntLife = new FixedLengthStringData(2).isAPartOf(anntKey, 9);
  	public FixedLengthStringData anntCoverage = new FixedLengthStringData(2).isAPartOf(anntKey, 11);
  	public FixedLengthStringData anntRider = new FixedLengthStringData(2).isAPartOf(anntKey, 13);
  	public PackedDecimalData anntSeqnbr = new PackedDecimalData(3, 0).isAPartOf(anntKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(anntKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(anntFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		anntFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}