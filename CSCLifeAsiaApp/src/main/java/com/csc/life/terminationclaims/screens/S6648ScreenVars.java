package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6648
 * @version 1.0 generated on 30/08/09 06:55
 * @author Quipoz
 */
public class S6648ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(786);
	public FixedLengthStringData dataFields = new FixedLengthStringData(290).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData minipups = new FixedLengthStringData(65).isAPartOf(dataFields, 55);
	public ZonedDecimalData[] minipup = ZDArrayPartOfStructure(5, 13, 2, minipups, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(65).isAPartOf(minipups, 0, FILLER_REDEFINE);
	public ZonedDecimalData minipup01 = DD.minipup.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData minipup02 = DD.minipup.copyToZonedDecimal().isAPartOf(filler,13);
	public ZonedDecimalData minipup03 = DD.minipup.copyToZonedDecimal().isAPartOf(filler,26);
	public ZonedDecimalData minipup04 = DD.minipup.copyToZonedDecimal().isAPartOf(filler,39);
	public ZonedDecimalData minipup05 = DD.minipup.copyToZonedDecimal().isAPartOf(filler,52);
	public FixedLengthStringData minisumas = new FixedLengthStringData(65).isAPartOf(dataFields, 120);
	public ZonedDecimalData[] minisuma = ZDArrayPartOfStructure(5, 13, 2, minisumas, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(65).isAPartOf(minisumas, 0, FILLER_REDEFINE);
	public ZonedDecimalData minisuma01 = DD.minisuma.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData minisuma02 = DD.minisuma.copyToZonedDecimal().isAPartOf(filler1,13);
	public ZonedDecimalData minisuma03 = DD.minisuma.copyToZonedDecimal().isAPartOf(filler1,26);
	public ZonedDecimalData minisuma04 = DD.minisuma.copyToZonedDecimal().isAPartOf(filler1,39);
	public ZonedDecimalData minisuma05 = DD.minisuma.copyToZonedDecimal().isAPartOf(filler1,52);
	public FixedLengthStringData minmthifs = new FixedLengthStringData(15).isAPartOf(dataFields, 185);
	public ZonedDecimalData[] minmthif = ZDArrayPartOfStructure(5, 3, 0, minmthifs, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(15).isAPartOf(minmthifs, 0, FILLER_REDEFINE);
	public ZonedDecimalData minmthif01 = DD.minmthif.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData minmthif02 = DD.minmthif.copyToZonedDecimal().isAPartOf(filler2,3);
	public ZonedDecimalData minmthif03 = DD.minmthif.copyToZonedDecimal().isAPartOf(filler2,6);
	public ZonedDecimalData minmthif04 = DD.minmthif.copyToZonedDecimal().isAPartOf(filler2,9);
	public ZonedDecimalData minmthif05 = DD.minmthif.copyToZonedDecimal().isAPartOf(filler2,12);
	public FixedLengthStringData minpusas = new FixedLengthStringData(65).isAPartOf(dataFields, 200);
	public ZonedDecimalData[] minpusa = ZDArrayPartOfStructure(5, 13, 2, minpusas, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(65).isAPartOf(minpusas, 0, FILLER_REDEFINE);
	public ZonedDecimalData minpusa01 = DD.minpusa.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData minpusa02 = DD.minpusa.copyToZonedDecimal().isAPartOf(filler3,13);
	public ZonedDecimalData minpusa03 = DD.minpusa.copyToZonedDecimal().isAPartOf(filler3,26);
	public ZonedDecimalData minpusa04 = DD.minpusa.copyToZonedDecimal().isAPartOf(filler3,39);
	public ZonedDecimalData minpusa05 = DD.minpusa.copyToZonedDecimal().isAPartOf(filler3,52);
	public FixedLengthStringData minterms = new FixedLengthStringData(20).isAPartOf(dataFields, 265);
	public ZonedDecimalData[] minterm = ZDArrayPartOfStructure(5, 4, 0, minterms, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(20).isAPartOf(minterms, 0, FILLER_REDEFINE);
	public ZonedDecimalData minterm01 = DD.minterm.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData minterm02 = DD.minterm.copyToZonedDecimal().isAPartOf(filler4,4);
	public ZonedDecimalData minterm03 = DD.minterm.copyToZonedDecimal().isAPartOf(filler4,8);
	public ZonedDecimalData minterm04 = DD.minterm.copyToZonedDecimal().isAPartOf(filler4,12);
	public ZonedDecimalData minterm05 = DD.minterm.copyToZonedDecimal().isAPartOf(filler4,16);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,285);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(124).isAPartOf(dataArea, 290);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData minipupsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] minipupErr = FLSArrayPartOfStructure(5, 4, minipupsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(20).isAPartOf(minipupsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData minipup01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData minipup02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData minipup03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData minipup04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData minipup05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData minisumasErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] minisumaErr = FLSArrayPartOfStructure(5, 4, minisumasErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(20).isAPartOf(minisumasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData minisuma01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData minisuma02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData minisuma03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData minisuma04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData minisuma05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData minmthifsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] minmthifErr = FLSArrayPartOfStructure(5, 4, minmthifsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(20).isAPartOf(minmthifsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData minmthif01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData minmthif02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData minmthif03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData minmthif04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData minmthif05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData minpusasErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData[] minpusaErr = FLSArrayPartOfStructure(5, 4, minpusasErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(20).isAPartOf(minpusasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData minpusa01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData minpusa02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData minpusa03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData minpusa04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData minpusa05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData mintermsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData[] mintermErr = FLSArrayPartOfStructure(5, 4, mintermsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(20).isAPartOf(mintermsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData minterm01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData minterm02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData minterm03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData minterm04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData minterm05Err = new FixedLengthStringData(4).isAPartOf(filler9, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(372).isAPartOf(dataArea, 414);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData minipupsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] minipupOut = FLSArrayPartOfStructure(5, 12, minipupsOut, 0);
	public FixedLengthStringData[][] minipupO = FLSDArrayPartOfArrayStructure(12, 1, minipupOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(60).isAPartOf(minipupsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] minipup01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] minipup02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] minipup03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] minipup04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] minipup05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData minisumasOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] minisumaOut = FLSArrayPartOfStructure(5, 12, minisumasOut, 0);
	public FixedLengthStringData[][] minisumaO = FLSDArrayPartOfArrayStructure(12, 1, minisumaOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(60).isAPartOf(minisumasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] minisuma01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] minisuma02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] minisuma03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] minisuma04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] minisuma05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData minmthifsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] minmthifOut = FLSArrayPartOfStructure(5, 12, minmthifsOut, 0);
	public FixedLengthStringData[][] minmthifO = FLSDArrayPartOfArrayStructure(12, 1, minmthifOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(60).isAPartOf(minmthifsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] minmthif01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] minmthif02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] minmthif03Out = FLSArrayPartOfStructure(12, 1, filler12, 24);
	public FixedLengthStringData[] minmthif04Out = FLSArrayPartOfStructure(12, 1, filler12, 36);
	public FixedLengthStringData[] minmthif05Out = FLSArrayPartOfStructure(12, 1, filler12, 48);
	public FixedLengthStringData minpusasOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 240);
	public FixedLengthStringData[] minpusaOut = FLSArrayPartOfStructure(5, 12, minpusasOut, 0);
	public FixedLengthStringData[][] minpusaO = FLSDArrayPartOfArrayStructure(12, 1, minpusaOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(60).isAPartOf(minpusasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] minpusa01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] minpusa02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] minpusa03Out = FLSArrayPartOfStructure(12, 1, filler13, 24);
	public FixedLengthStringData[] minpusa04Out = FLSArrayPartOfStructure(12, 1, filler13, 36);
	public FixedLengthStringData[] minpusa05Out = FLSArrayPartOfStructure(12, 1, filler13, 48);
	public FixedLengthStringData mintermsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 300);
	public FixedLengthStringData[] mintermOut = FLSArrayPartOfStructure(5, 12, mintermsOut, 0);
	public FixedLengthStringData[][] mintermO = FLSDArrayPartOfArrayStructure(12, 1, mintermOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(60).isAPartOf(mintermsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] minterm01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] minterm02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] minterm03Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] minterm04Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] minterm05Out = FLSArrayPartOfStructure(12, 1, filler14, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6648screenWritten = new LongData(0);
	public LongData S6648protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6648ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(minisuma01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minterm01Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minmthif01Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minpusa01Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minipup01Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minisuma02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minterm02Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minmthif02Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minpusa02Out,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minipup02Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minisuma03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minterm03Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minmthif03Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minpusa03Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minipup03Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minisuma04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minterm04Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minmthif04Out,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minpusa04Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minipup04Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minisuma05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minterm05Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minmthif05Out,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minpusa05Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minipup05Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, minisuma01, minterm01, minmthif01, minpusa01, minipup01, minisuma02, minterm02, minmthif02, minpusa02, minipup02, minisuma03, minterm03, minmthif03, minpusa03, minipup03, minisuma04, minterm04, minmthif04, minpusa04, minipup04, minisuma05, minterm05, minmthif05, minpusa05, minipup05};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, minisuma01Out, minterm01Out, minmthif01Out, minpusa01Out, minipup01Out, minisuma02Out, minterm02Out, minmthif02Out, minpusa02Out, minipup02Out, minisuma03Out, minterm03Out, minmthif03Out, minpusa03Out, minipup03Out, minisuma04Out, minterm04Out, minmthif04Out, minpusa04Out, minipup04Out, minisuma05Out, minterm05Out, minmthif05Out, minpusa05Out, minipup05Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, minisuma01Err, minterm01Err, minmthif01Err, minpusa01Err, minipup01Err, minisuma02Err, minterm02Err, minmthif02Err, minpusa02Err, minipup02Err, minisuma03Err, minterm03Err, minmthif03Err, minpusa03Err, minipup03Err, minisuma04Err, minterm04Err, minmthif04Err, minpusa04Err, minipup04Err, minisuma05Err, minterm05Err, minmthif05Err, minpusa05Err, minipup05Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6648screen.class;
		protectRecord = S6648protect.class;
	}

}
