package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6696
 * @version 1.0 generated on 30/08/09 06:58
 * @author Quipoz
 */
public class S6696ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(494);
	public FixedLengthStringData dataFields = new FixedLengthStringData(126).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData dfclmpct = DD.dfclmpct.copyToZonedDecimal().isAPartOf(dataFields,1);
	public ZonedDecimalData dfdefprd = DD.dfdefprd.copyToZonedDecimal().isAPartOf(dataFields,6);
	public FixedLengthStringData dissmeth = DD.dissmeth.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData freqcys = new FixedLengthStringData(6).isAPartOf(dataFields, 13);
	public FixedLengthStringData[] freqcy = FLSArrayPartOfStructure(3, 2, freqcys, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(freqcys, 0, FILLER_REDEFINE);
	public FixedLengthStringData freqcy01 = DD.freqcy.copy().isAPartOf(filler,0);
	public FixedLengthStringData freqcy02 = DD.freqcy.copy().isAPartOf(filler,2);
	public FixedLengthStringData freqcy03 = DD.freqcy.copy().isAPartOf(filler,4);
	public FixedLengthStringData frqoride = DD.frqoride.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData glact = DD.glact.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData inxfrq = DD.inxfrq.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData inxsbm = DD.inxsbm.copy().isAPartOf(dataFields,36);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,46);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,54);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,62);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,70);
	public ZonedDecimalData mndefprd = DD.mndefprd.copyToZonedDecimal().isAPartOf(dataFields,100);
	public ZonedDecimalData mnovrpct = DD.mnovrpct.copyToZonedDecimal().isAPartOf(dataFields,103);
	public ZonedDecimalData mxovrpct = DD.mxovrpct.copyToZonedDecimal().isAPartOf(dataFields,108);
	public ZonedDecimalData revitrm = DD.revitrm.copyToZonedDecimal().isAPartOf(dataFields,113);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(dataFields,116);
	public FixedLengthStringData sacstype = DD.sacstype.copy().isAPartOf(dataFields,118);
	public FixedLengthStringData sign = DD.sign.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,121);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(92).isAPartOf(dataArea, 126);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData dfclmpctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData dfdefprdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData dissmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData freqcysErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] freqcyErr = FLSArrayPartOfStructure(3, 4, freqcysErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(freqcysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData freqcy01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData freqcy02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData freqcy03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData frqorideErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData glactErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData inxfrqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData inxsbmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mndefprdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData mnovrpctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData mxovrpctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData revitrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData sacscodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData sacstypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData signErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(276).isAPartOf(dataArea, 218);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] dfclmpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] dfdefprdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] dissmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData freqcysOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] freqcyOut = FLSArrayPartOfStructure(3, 12, freqcysOut, 0);
	public FixedLengthStringData[][] freqcyO = FLSDArrayPartOfArrayStructure(12, 1, freqcyOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(freqcysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] freqcy01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] freqcy02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] freqcy03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] frqorideOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] glactOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] inxfrqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] inxsbmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mndefprdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] mnovrpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] mxovrpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] revitrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] sacscodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] sacstypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] signOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6696screenWritten = new LongData(0);
	public LongData S6696protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6696ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dfclmpctOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mxovrpctOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mnovrpctOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dfdefprdOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy01Out,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mndefprdOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy02Out,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(revitrmOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcy03Out,new String[] {"43",null, "-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frqorideOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(inxfrqOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(inxsbmOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacscodeOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sacstypeOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(glactOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(signOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dissmethOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, dfclmpct, mxovrpct, mnovrpct, dfdefprd, freqcy01, mndefprd, freqcy02, revitrm, freqcy03, frqoride, inxfrq, inxsbm, sacscode, sacstype, glact, sign, dissmeth};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, dfclmpctOut, mxovrpctOut, mnovrpctOut, dfdefprdOut, freqcy01Out, mndefprdOut, freqcy02Out, revitrmOut, freqcy03Out, frqorideOut, inxfrqOut, inxsbmOut, sacscodeOut, sacstypeOut, glactOut, signOut, dissmethOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, dfclmpctErr, mxovrpctErr, mnovrpctErr, dfdefprdErr, freqcy01Err, mndefprdErr, freqcy02Err, revitrmErr, freqcy03Err, frqorideErr, inxfrqErr, inxsbmErr, sacscodeErr, sacstypeErr, glactErr, signErr, dissmethErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6696screen.class;
		protectRecord = S6696protect.class;
	}

}
