package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:23
 * Description:
 * Copybook name: CHDRSURKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrsurkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrsurFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrsurKey = new FixedLengthStringData(64).isAPartOf(chdrsurFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrsurChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrsurKey, 0);
  	public FixedLengthStringData chdrsurChdrnum = new FixedLengthStringData(8).isAPartOf(chdrsurKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrsurKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrsurFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrsurFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}