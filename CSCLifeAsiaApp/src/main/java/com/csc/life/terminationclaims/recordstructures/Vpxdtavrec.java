package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Vpxdtavrec extends ExternalData {

	private static final long serialVersionUID = 1L;

	/*
	 *  01  VPXDTAV-REC.
     *      03 VPXDTAV-FUNCTION             PIC X(05).
     *      03 VPXDTAV-STATUZ               PIC X(04).
     *      03 VPXDTAV-COPYBOOK             PIC X(12).
     *      03 VPXDTAV-RESULT.
     *         05 VPXDTAV-RESULT-FIELD      PIC X(35).
     *         05 VPXDTAV-RESULT-VALUE      PIC X(80).
     *      03 VPXDTAV-DATA-REC.
     *         05 VPXDTAV-CURDUNTBAL        PIC S9(13)V9(05).
     *         05 VPXDTAV-UNPROCESSED       PIC S9(13)V9(05).
     *         05 VPXDTAV-DTH-MTH           PIC X(04).
     *         05 VPXDTAV-CURRCODE          PIC X(03).
	 */
  	public FixedLengthStringData vpxdtavrec = new FixedLengthStringData(176);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxdtavrec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxdtavrec, 5);
  	public FixedLengthStringData copyBook = new FixedLengthStringData(12).isAPartOf(vpxdtavrec, 9);
  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxdtavrec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);
  	public FixedLengthStringData dataRec = new FixedLengthStringData(40).isAPartOf(vpxdtavrec, 136);
  	public PackedDecimalData curduntbal = new PackedDecimalData(16, 5).isAPartOf(dataRec, 0);
  	public PackedDecimalData unprocessed = new PackedDecimalData(17, 2).isAPartOf(dataRec,16);
  	public FixedLengthStringData dthMth = new FixedLengthStringData(4).isAPartOf(dataRec, 33);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(dataRec, 37);



	@Override
	public void initialize() {
		COBOLFunctions.initialize(vpxdtavrec);
	}

	@Override
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxdtavrec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

}
