 
package com.csc.life.terminationclaims.recordstructures; 


import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Wed, 7 OCT 2015
 * Description:
 * Copybook name: SURTAXREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class SurtaxRec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData surtaxRec = new FixedLengthStringData(51);
	  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(surtaxRec, 0);
	  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(surtaxRec, 5);
	  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(surtaxRec, 9);
	  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(surtaxRec, 10);
	  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(surtaxRec, 13);
	  	public ZonedDecimalData effectiveDate = new ZonedDecimalData(8, 0).isAPartOf(surtaxRec, 16);
	  	public ZonedDecimalData occDate = new ZonedDecimalData(8, 0).isAPartOf(surtaxRec, 24);
	  	public PackedDecimalData actualAmount = new PackedDecimalData(17, 2).isAPartOf(surtaxRec, 32).setUnsigned();
	  	public PackedDecimalData taxAmount = new PackedDecimalData(17, 2).isAPartOf(surtaxRec, 41).setUnsigned();
	  	public FixedLengthStringData amountType = new FixedLengthStringData(1).isAPartOf(surtaxRec, 50);
	  		  	
		public void initialize() {
			COBOLFunctions.initialize(surtaxRec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	    		surtaxRec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}
	
}