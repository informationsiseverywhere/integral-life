/*
 * File: Dmrtdd3.java
 * Date: 29 August 2009 22:47:19
 * Author: Quipoz Limited
 * 
 * Class transformed from Dmrtdd3.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;

import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;//TKPOC1
import com.csc.fsu.general.recordstructures.Datcon3rec; //TKPOC1
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM; //TKPOC1

import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.interestbearing.dataaccess.HitrclmTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.ClmdaddTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  ADDITIONAL PROCESSING 3 FOR DEATH CLAIMS.
*
*                   DEATH CLAIM ADDITIONAL PROCESSING METHOD 2.
*                   -------------------------------------------
*
*           This program is an item entry on T6598, the death clai 
*          subroutine method table. This method is used in order t 
*          update the relevant sub-accounts. The trigger module an 
*          key were passed with other details with the UTRN record 
*
*            PROCESSING.
*            ----------
*
*            This routine is called once for each UTRN.
*
*            Read the Claim detail record (CLMDCLM) for this trigg r
*          key and set the estimated amount to zero. Set the Actua 
*          amount to the trigger-amount passed and re-write the CL D
*          record.
*
*            Read all the CLMD records for this coverage/rider and sum
*          the estimates and actuals.
*
*          If the sum of the estimated amounts is not zero, then s ip
*          all further processing.
*
*          If the accumulated estimated amount is equal to zero, t en
*          the claim amount is calculated as follows:- (read the C VR
*          records and accumulate the Sum Insured (SI) amounts), t e
*          claim amount is the SI amount minus the Actual amount.
*
*          If the claim amount is less than zero, then set the cla m
*          amount to zero.
*
*          Update the 'S' type record with the Estimate amount equ l
*          to zero and set the Actual amount to the claim amount.
*
*          Post the Claim amount to the following sub-accounts,
*          Outstanding Claims (01) and the Death Claims (02).
*
*            CONTRACT ACCOUNTING FOR DEATH CLAIMS
*
*          Read the transaction accounting rules from table T5645
*          (keyed on program no P5256) this contains the rules
*          necessary for posting the monies to the correct account .
*
*          Read the description of this table entry as well. This  s
*          used as narrative description on all the postings gener ted.
*
*          If the amounts are not zero, then call LIFACMV ("non-ca h"
*          posting subroutine) to post to the correct suspense acc unt.
*          The posting required is defined in the appropriate line no.
*          on the T5645 table entry.  Set up and pass the linkage  rea
*          as follows:
*
*                     Function               - PSTW
*                     Batch key              - AT linkage
*                     Document number        - contract number
*                     Sequence number        - transaction no.
*                     Sub-account code       - from applicable T56 5
*                                              entry
*                     Sub-account type       - ditto
*                     Sub-account GL map     - ditto
*                     Sub-account GL sign    - ditto
*                     S/acct control total   - ditto
*                     Cmpy code (sub ledger) - batch company
*                     Cmpy code (GL)         - batch company
*                     Subsidiary ledger      - contract number
*                     Original currency code - currency payable in
*                     Original currency amt  - claim amount
*                     Accounting curr code   - blank (handled by
*                                              subroutine)
*                     Accounting curr amount - zero (handled by
*                                              subroutine)
*                     Exchange rate          - zero (handled by
*                                              subroutine)
*                     Trans reference        - contract transactio 
*                                              number
*                     Trans description      - from transaction co e
*                                              description
*                     Posting month and year - defaulted
*                     Effective date         - Death Claim
*                                              effective-date
*                     Reconciliation amount  - zero
*                     Reconciliation date    - Max date
*                     Transaction Id         - AT linkage
*                     Substitution code 1    - contract type
*
*            Read all the CLMD records for that coverage/rider and
*          accumulate the Estimated amounts and accumulate the Act al
*          amounts of 'F' type records.
*
*            If the accumulated estimated amount is equal to zero,
*          then the claim amount is calculated as the Sum Insured  SI)
*          minus the Actual amount.
*
*            If the claim amount is less than zero, then set the c aim
*          amount to zero.
*
*            Update the 'S' type record with the Estimated amount
*          equal to zero and set the Actual amount to the claim am unt.
*
*            If the claim amount is not zero, then post to the O/S
*          Claim (01) and to the Death Claim (02) sub-accounts.
*
*            If the amounts are not zero, then call LIFRTRN ("cash 
*          posting subroutine) to post to the correct suspense
*          account.  The posting required is defined in the
*          appropriate line no.  on the T5645 table entry.  Set up and
*          pass the linkage area as follows:
*
*                     Function               - PSTW
*                     Batch key              - from AT linkage
*                     Document number        - contract number
*                     Sequence number        - transaction no.
*                     Sub-account code       - from applicable T56 5
*                     Sub-account type       - ditto
*                     Sub-account GL map     - ditto
*                     Sub-account GL sign    - ditto
*                     S/acct control total   - ditto
*                     Cmpy code (sub ledger) - batch company
*                     Cmpy code (GL)         - batch company
*                     Subsidiary ledger      - contract number
*                     Original currency code - currency payable in
*                     Original currency amt  - claim amount
*                     Accounting curr code   - blank (handled by
*                                              subroutine)
*                     Accounting curr amount - zero (handled by
*                                              subroutine)
*                     Exchange rate          - Zero (handled by
*                                              subroutine)
*                     Trans reference        - contract transactio 
*                                              number
*                     Trans description      - from transaction co e
*                                              description
*                     Posting month and year - defaulted
*                     Effective date         - Death Claim
*                                              effective-date
*                     Reconciliation amount  - zero
*                     Reconciliation date    - Max date
*                     Transaction Id         - from AT linkage
*                     Substitution code 1    - contract type
*
*****************************************************************
* </pre>
*/
public class Dmrtdd3 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "DMRTDD3";
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String clmdaddrec = "CLMDADDREC";
	private static final String hitrclmrec = "HITRCLMREC";
	private String chdrlifrec = "CHDRLIFREC";
	private String mbnsrec ="MBNSREC";
	
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaActvalue = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaDeemCharge = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaGrossVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaGrossRound = new PackedDecimalData(17, 5).init(0);
	private PackedDecimalData wsaaAcumGrossVal = new PackedDecimalData(17, 2).init(0);
	private String wsaaInitialUnit = "";
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaClaimAmount = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).init(ZERO);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaContractType = new FixedLengthStringData(4).init(SPACES);
	private ClmdaddTableDAM clmdaddIO = new ClmdaddTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrclmTableDAM hitrclmIO = new HitrclmTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Udtrigrec udtrigrec = new Udtrigrec();

	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM(); //TKPOC1
	private Datcon3rec datcon3rec = new Datcon3rec(); //TKPOC1
	private MbnsTableDAM mbnsIO = new MbnsTableDAM(); //TKPOC1


	public Dmrtdd3() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		udtrigrec.udtrigrecRec = convertAndSetParam(udtrigrec.udtrigrecRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		readUtrn1000();
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			a200ReadAndUpdateClaim();
		}
		else {
			readAndUpdateClaim2000();
		}
		mainProcClaimDetails3000();
		/*EXIT*/
		exitProgram();
	}

protected void readTabT5645350()
	{
		/*READ*/
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO ITEM-ITEMCOY.                 */
		itemIO.setItemcoy(udtrigrec.tk3Chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		/*  MOVE 'P5256'                TO ITEM-ITEMITEM.                */
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void readTabT5688400()
	{
		read410();
	}

protected void read410()
	{
		/* MOVE PARM-COMPANY           TO ITDM-ITEMCOY.            <011>*/
		itdmIO.setItemcoy(udtrigrec.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaContractType);
		/* MOVE PARM-EFFDATE           TO ITDM-ITMFRM.             <011>*/
		itdmIO.setItmfrm(udtrigrec.effdate);
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		/* IF ITDM-ITEMCOY           NOT = PARM-COMPANY            <011>*/
		if (isNE(itdmIO.getItemcoy(), udtrigrec.company)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), wsaaContractType)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaContractType);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readUtrn1000()
	{
		readUtrnPara1000();
		go1010();
	}

protected void readUtrnPara1000()
	{
		varcom.vrcmTermid.set(SPACES);
	}

protected void go1010()
	{
		utrnIO.setParams(SPACES);
		/* MOVE UTRN-LINKAGE           TO UTRN-DATA-KEY.                */
		utrnIO.setChdrcoy(udtrigrec.chdrcoy);
		utrnIO.setChdrnum(udtrigrec.chdrnum);
		utrnIO.setLife(udtrigrec.life);
		utrnIO.setCoverage(udtrigrec.coverage);
		utrnIO.setRider(udtrigrec.rider);
		utrnIO.setPlanSuffix(udtrigrec.planSuffix);
		utrnIO.setUnitVirtualFund(udtrigrec.unitVirtualFund);
		utrnIO.setUnitType(udtrigrec.unitType);
		utrnIO.setTranno(udtrigrec.tranno);
		/* MOVE UDTG-PROC-SEQ-NO       TO UTRN-PROC-SEQ-NO              */
		utrnIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnIO);
		/*IF UTRN-PARAMS              NOT = O-K                        */
		if (isNE(utrnIO.getStatuz(), varcom.oK)
		&& isNE(utrnIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			a100ReadHitr();
			return ;
		}
		wsaaContractType.set(utrnIO.getContractType());
	}

protected void readAndUpdateClaim2000()
	{
		go2010();
	}

protected void go2010()
	{
		clmdaddIO.setParams(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO CLMDADD-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO CLMDADD-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-LIFE      TO CLMDADD-LIFE.            <002>*/
		/* MOVE WSAA-TRIGGER-COVERAGE  TO CLMDADD-COVERAGE.             */
		/* MOVE WSAA-TRIGGER-RIDER     TO CLMDADD-RIDER.                */
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setLife(udtrigrec.tk3Life);
		clmdaddIO.setCoverage(udtrigrec.tk3Coverage);
		clmdaddIO.setRider(udtrigrec.tk3Rider);
		clmdaddIO.setFieldType(utrnIO.getUnitType());
		clmdaddIO.setVirtualFund(utrnIO.getUnitVirtualFund());
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdaddIO.getStatuz());
			fatalError9000();
		}
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setEstMatValue(ZERO);
		/*  MOVE UTRN-CONTRACT-AMOUNT TO CLMDADD-ACTVALUE.               */
		wsaaDeemCharge.set(ZERO);
		wsaaGrossVal.set(ZERO);
		wsaaGrossRound.set(ZERO);
		/*Check UTRN-UNIT-TYPE.                                            */
		if (isEQ(utrnIO.getUnitType(), "I")
		&& isNE(utrnIO.getNofDunits(), utrnIO.getNofUnits())) {
			wsaaInitialUnit = "Y";
		}
		else {
			wsaaInitialUnit = " ";
		}
		/* Calculate PEND-CLAIM amount for 'A' units.                      */
		/* Round the amount to 2 decimal.                                  */
		if (isEQ(wsaaInitialUnit, " ")) {
			compute(wsaaGrossRound, 5).set(mult(utrnIO.getPriceUsed(), utrnIO.getNofDunits()));
			compute(wsaaGrossVal, 6).setRounded(mult(wsaaGrossRound, 1));
			zrdecplrec.amountIn.set(wsaaGrossVal);
			a000CallRounding();
			wsaaGrossVal.set(zrdecplrec.amountOut);
			wsaaAcumGrossVal.set(wsaaGrossVal);
		}
		/* Calculate deemed amount for 'I' units.                          */
		if (isEQ(wsaaInitialUnit, "Y")) {
			compute(wsaaGrossRound, 5).set(mult(utrnIO.getPriceUsed(), utrnIO.getNofDunits()));
			/* Round up to 2 decimal.                                          */
			if (isEQ(wsaaGrossRound, ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				compute(wsaaGrossVal, 6).setRounded(mult(wsaaGrossRound, 1));
			}
			if (isEQ(wsaaGrossVal, ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				compute(wsaaDeemCharge, 2).set(sub(wsaaGrossVal, utrnIO.getContractAmount()));
			}
		}
		/*    If initial units, than move deem amount to                   */
		/*    Claim detail file, else add utrn-cont-amt.                   */
		if (isEQ(wsaaInitialUnit, "Y")) {
			clmdaddIO.setActvalue(wsaaGrossVal);
		}
		else {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(add(clmdaddIO.getActvalue(), utrnIO.getContractAmount()));
		}
		if (isGT(clmdaddIO.getActvalue(), ZERO)) {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), 1));
		}
		else {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), -1));
		}
		clmdaddIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
	}

	/**
	* <pre>
	*   read all the claim details per coverage/rider
	* </pre>
	*/
protected void mainProcClaimDetails3000()
	{
		/*GO*/
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		clmdaddIO.setParams(SPACES);
		/* MOVE BEGNH                  TO CLMDADD-FUNCTION.             */
		clmdaddIO.setFunction(varcom.begn);
		/*    MOVE WSAA-TRIGGER-CHDRCOY   TO CLMDADD-CHDRCOY.              */
		/*    MOVE WSAA-TRIGGER-CHDRNUM   TO CLMDADD-CHDRNUM.              */
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setFormat(clmdaddrec);
		while ( !(isEQ(clmdaddIO.getStatuz(), varcom.endp))) {
			readClaims3500();
		}
		
		/*  if the accumulated estimated amount is not zero then*/
		/*  skip all further processing*/
		if (isNE(wsaaEstimateTot, ZERO)) {
			return ;
		}
		readCovr3600();
		/*EXIT*/
	}

protected void readClaims3500()
	{
		go3510();
	}

protected void go3510()
	{
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)
		&& isNE(clmdaddIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmdaddIO.getStatuz());
			fatalError9000();
		}
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = CLMDADD-CHDRCOY OR         */
		/*    WSAA-TRIGGER-CHDRNUM     NOT =  CLMDADD-CHDRNUM OR        */
		/*    WSAA-TRIGGER-COVERAGE    NOT =  CLMDADD-COVERAGE OR       */
		/*    WSAA-TRIGGER-RIDER       NOT =  CLMDADD-RIDER OR     <004>*/
		if (isNE(udtrigrec.tk3Chdrcoy, clmdaddIO.getChdrcoy())
		|| isNE(udtrigrec.tk3Chdrnum, clmdaddIO.getChdrnum())
		|| isNE(udtrigrec.tk3Coverage, clmdaddIO.getCoverage())
		|| isNE(udtrigrec.tk3Rider, clmdaddIO.getRider())
		|| isEQ(clmdaddIO.getStatuz(), varcom.endp)) {
			clmdaddIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(clmdaddIO.getFieldType(), "I")
		|| isEQ(clmdaddIO.getFieldType(), "A")
		|| isEQ(clmdaddIO.getFieldType(), "D")) {
			wsaaEstimateTot.add(clmdaddIO.getEstMatValue());
			wsaaActualTot.add(clmdaddIO.getActvalue());
		}
		if (isNE(wsaaEstimateTot, ZERO)) {
			clmdaddIO.setStatuz(varcom.endp);
		}
		else {
			clmdaddIO.setFunction(varcom.nextr);
		}
	}

protected void readCovr3600()
	{
		go3610();
	}

protected void go3610()
	{
		/*  MOVE SPACES                 TO COVRCLM-PARAMS.               */
		/*  MOVE BEGNH                  TO COVRCLM-FUNCTION.             */
		/*  MOVE WSAA-TRIGGER-CHDRCOY   TO COVRCLM-CHDRCOY.              */
		/*  MOVE WSAA-TRIGGER-CHDRNUM   TO COVRCLM-CHDRNUM.              */
		/*  MOVE WSAA-TRIGGER-COVERAGE  TO COVRCLM-COVERAGE.             */
		/*  MOVE WSAA-TRIGGER-RIDER     TO COVRCLM-RIDER.                */
		/*  MOVE WSAA-TRIGGER-LIFE      TO COVRCLM-LIFE.                 */
		/*  MOVE COVRCLMREC             TO COVRCLM-FORMAT.               */
		/*  MOVE READR                  TO COVRCLM-FUNCTION.             */
		wsaaSumins.set(ZERO);
		wsaaClaimAmount.set(ZERO);
		wsaaStoredCoverage.set(SPACES);
		wsaaStoredRider.set(SPACES);
		covrIO.setParams(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO COVR-CHDRCOY.            <003>*/
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO COVR-CHDRNUM.            <003>*/
		/* MOVE WSAA-TRIGGER-COVERAGE  TO COVR-COVERAGE.           <003>*/
		/* MOVE WSAA-TRIGGER-RIDER     TO COVR-RIDER.              <003>*/
		/* MOVE WSAA-TRIGGER-LIFE      TO COVR-LIFE.               <003>*/
		covrIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		covrIO.setChdrnum(udtrigrec.tk3Chdrnum);
		covrIO.setCoverage(udtrigrec.tk3Coverage);
		covrIO.setRider(udtrigrec.tk3Rider);
		covrIO.setLife(udtrigrec.tk3Life);
		covrIO.setPlanSuffix(0);
		covrIO.setFunction(varcom.begn);


		//performance improvement --  atiwari23 
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		/* IF COVR-CHDRNUM    NOT =  WSAA-TRIGGER-CHDRNUM OR       <003>*/
		/*      COVR-CHDRCOY  NOT =  WSAA-TRIGGER-CHDRCOY OR       <003>*/
		/*      COVR-LIFE     NOT =  WSAA-TRIGGER-LIFE OR          <003>*/
		/*      COVR-COVERAGE NOT =  WSAA-TRIGGER-COVERAGE OR      <003>*/
		/*      COVR-RIDER    NOT =  WSAA-TRIGGER-RIDER OR         <003>*/
		if (isNE(covrIO.getChdrnum(), udtrigrec.tk3Chdrnum)
		|| isNE(covrIO.getChdrcoy(), udtrigrec.tk3Chdrcoy)
		|| isNE(covrIO.getLife(), udtrigrec.tk3Life)
		|| isNE(covrIO.getCoverage(), udtrigrec.tk3Coverage)
		|| isNE(covrIO.getRider(), udtrigrec.tk3Rider)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		/*  store coverage and rider and currency                          */
		wsaaStoredCoverage.set(covrIO.getCoverage());
		wsaaStoredRider.set(covrIO.getRider());
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			/*  If actual amount is equal or greater than total sum insured    */
			/*  no posting is needed.                                          */
			processComponents3700();
		}
		readMbnsFileA300(); //TKPOC1
		if (isGTE(wsaaActualTot, wsaaSumins)) {
			return ;
		}
		compute(wsaaClaimAmount, 2).set(sub(wsaaSumins, wsaaActualTot));
		/* IF WSAA-CLAIM-AMOUNT        < ZERO                      <003>*/
		/*     MOVE ZERO TO WSAA-CLAIM-AMOUNT.                     <003>*/
		/* PERFORM 6000-READ-AND-UPDATE-SA-CLAIM.                  <003>*/
		if (isLTE(wsaaClaimAmount, ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			readAndUpdateSaClaim6000();
		}
		if (isNE(wsaaClaimAmount, ZERO)) {
			readTabT5645350();
			readTabT5688400();
			postToDeathClaim3800();
		}
	}

protected void processComponents3700()
	{
		/*READ*/
	//	wsaaSumins.add(covrIO.getSumins()); //TKPOC1 Comment this line very important

		while ( !((isNE(wsaaStoredRider, covrIO.getRider()))
		|| (isNE(wsaaStoredCoverage, covrIO.getCoverage()))
		|| isEQ(covrIO.getStatuz(), varcom.endp))) {
			findNextComponent4000();
		}
		
		if ((isNE(wsaaStoredRider, covrIO.getRider()))
		|| (isNE(wsaaStoredCoverage, covrIO.getCoverage()))) {
			covrIO.setStatuz(varcom.endp);
		}
		wsaaStoredCoverage.set(covrIO.getCoverage());
		wsaaStoredRider.set(covrIO.getRider());
		/*EXIT*/
	}

protected void postToDeathClaim3800()
	{
		read3810();
	}

protected void read3810()
	{
		batcdorrec.function.set("AUTO");
		varcom.vrcmTermid.set(SPACES);
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmUser.set(ZERO);
		batcdorrec.tranid.set(varcom.vrcmTranid);
		/*   MOVE UTRN-BATCCOY           TO LIFA-BATCCOY.         <LA4524>*/
		/*   MOVE UTRN-BATCBRN           TO LIFA-BATCBRN.         <LA4524>*/
		/*   MOVE UTRN-BATCACTYR         TO LIFA-BATCACTYR.       <LA4524>*/
		/*   MOVE UTRN-BATCACTMN         TO LIFA-BATCACTMN.       <LA4524>*/
		lifacmvrec1.batckey.set(udtrigrec.batchkey);
		lifacmvrec1.batctrcde.set(utrnIO.getBatctrcde());
		/*   MOVE UTRN-BATCBATCH         TO LIFA-BATCBATCH.       <LA4524>*/
		batcdorrec.batchkey.set(lifacmvrec1.batckey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
		lifacmvrec1.batckey.set(batcdorrec.batchkey);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.rdocnum.set(udtrigrec.tk3Chdrnum);
		lifacmvrec1.tranno.set(utrnIO.getTranno());
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.rldgcoy.set(udtrigrec.tk3Chdrcoy);
		lifacmvrec1.genlcoy.set(udtrigrec.tk3Chdrcoy);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode01);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec1.glcode.set(t5645rec.glmap01);
			lifacmvrec1.glsign.set(t5645rec.sign01);
			lifacmvrec1.contot.set(t5645rec.cnttot01);
			lifacmvrec1.rldgacct.set(SPACES);
			wsaaRldgChdrnum.set(udtrigrec.chdrnum);
			wsaaRldgLife.set(udtrigrec.life);
			wsaaRldgCoverage.set(udtrigrec.coverage);
			wsaaRldgRider.set(udtrigrec.rider);
			wsaaPlansuff.set(udtrigrec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.substituteCode[1].set(SPACES);
			lifacmvrec1.substituteCode[6].set(utrnIO.getCrtable());
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode03);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec1.glcode.set(t5645rec.glmap03);
			lifacmvrec1.glsign.set(t5645rec.sign03);
			lifacmvrec1.contot.set(t5645rec.cnttot03);
			lifacmvrec1.substituteCode[1].set(wsaaContractType);
			lifacmvrec1.substituteCode[6].set(SPACES);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(udtrigrec.tk3Chdrnum);
		}
		lifacmvrec1.origcurr.set(utrnIO.getCntcurr());
		lifacmvrec1.origamt.set(wsaaClaimAmount);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.tranref.set(utrnIO.getTranno());
		descIO.setDescitem(lifacmvrec1.batctrcde);
		getDescription5000();
		lifacmvrec1.effdate.set(utrnIO.getMoniesDate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.termid.set(SPACES);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.transactionDate.set(getCobolDate());
		lifacmvrec1.transactionTime.set(getCobolTime());
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError9000();
		}
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode04);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec1.glcode.set(t5645rec.glmap04);
			lifacmvrec1.glsign.set(t5645rec.sign04);
			lifacmvrec1.contot.set(t5645rec.cnttot04);
			wsaaRldgChdrnum.set(udtrigrec.chdrnum);
			wsaaRldgLife.set(udtrigrec.life);
			wsaaRldgCoverage.set(udtrigrec.coverage);
			wsaaRldgRider.set(udtrigrec.rider);
			wsaaPlansuff.set(udtrigrec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.substituteCode[1].set(SPACES);
			lifacmvrec1.substituteCode[6].set(utrnIO.getCrtable());
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode02);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec1.glcode.set(t5645rec.glmap02);
			lifacmvrec1.glsign.set(t5645rec.sign02);
			lifacmvrec1.contot.set(t5645rec.cnttot02);
			lifacmvrec1.substituteCode[1].set(wsaaContractType);
			lifacmvrec1.substituteCode[6].set(SPACES);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(udtrigrec.tk3Chdrnum);
		}
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError9000();
		}
	}

protected void postingsToDeathClaim3900()
	{
		read3910();
	}

protected void read3910()
	{
		/* MOVE 'AUTO'                 TO BATD-FUNCTION.        <Q25>   */
		/* MOVE PARM-TRANID            TO BATD-TRANID.          <Q25>   */
		/* MOVE SPACES                 TO VRCM-TERMID           <Q25>   */
		/* ACCEPT VRCM-DATE            FROM DATE                <Q25>   */
		/* ACCEPT VRCM-TIME            FROM TIME                <Q25>   */
		/* MOVE ZERO                   TO VRCM-USER             <Q25>   */
		/* MOVE VRCM-TRANID            TO BATD-TRANID.          <Q25>   */
		/* MOVE UTRN-BATCCOY           TO LIFA-BATCCOY.         <Q25>   */
		/* MOVE UTRN-BATCBRN           TO LIFA-BATCBRN.         <Q25>   */
		/* MOVE UTRN-BATCACTYR         TO LIFA-BATCACTYR.       <Q25>   */
		/* MOVE UTRN-BATCACTMN         TO LIFA-BATCACTMN.       <Q25>   */
		/* MOVE UTRN-BATCTRCDE         TO LIFA-BATCTRCDE.       <Q25>   */
		/* MOVE UTRN-BATCBATCH         TO LIFA-BATCBATCH.       <Q25>   */
		/* MOVE LIFA-BATCKEY           TO BATD-BATCHKEY.        <Q25>   */
		/* MOVE 'BA'                   TO BATD-PREFIX.          <Q25>   */
		/*                                                      <Q25>   */
		/* CALL 'BATCDOR' USING BATD-BATCDOR-REC.               <Q25>   */
		/* IF BATD-STATUZ              NOT = O-K                <Q25>   */
		/*     MOVE BATD-BATCDOR-REC   TO SYSR-PARAMS           <Q25>   */
		/*     PERFORM 9000-FATAL-ERROR.                        <Q25>   */
		/*                                                      <Q25>   */
		/* MOVE BATD-BATCHKEY          TO LIFA-BATCKEY.         <Q25>   */
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(ZERO);
		/* Posting at contract level.  Total of accumulation and initial   */
		/* units paid to the client.                                       */
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.rldgacct.set(SPACES);
		/*    MOVE WSAA-TRIGGER-CHDRNUM   TO LIFA-RDOCNUM.            <013>*/
		lifacmvrec1.rdocnum.set(udtrigrec.tk3Chdrnum);
		lifacmvrec1.tranno.set(utrnIO.getTranno());
		lifacmvrec1.sacscode.set(t5645rec.sacscode02);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec1.glcode.set(t5645rec.glmap02);
		lifacmvrec1.glsign.set(t5645rec.sign02);
		lifacmvrec1.contot.set(t5645rec.cnttot02);
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO LIFA-RLDGACCT.           <013>*/
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO LIFA-RLDGCOY.            <013>*/
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO LIFA-GENLCOY.            <013>*/
		lifacmvrec1.rldgacct.set(udtrigrec.tk3Chdrnum);
		lifacmvrec1.rldgcoy.set(udtrigrec.tk3Chdrcoy);
		lifacmvrec1.genlcoy.set(udtrigrec.tk3Chdrcoy);
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.origcurr.set(utrnIO.getCntcurr());
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.tranref.set(utrnIO.getTranno());
		descIO.setDescitem(lifacmvrec1.batctrcde);
		getDescription5000();
		lifacmvrec1.effdate.set(utrnIO.getMoniesDate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(utrnIO.getContractType());
		/* MOVE PARM-TRANID                TO VRCM-TRANID.         <013>*/
		/* MOVE VRCM-TERMID             TO LIFA-TERMID.            <013>*/
		/* MOVE VRCM-USER               TO LIFA-USER.              <013>*/
		/* MOVE VRCM-TIME               TO LIFA-TRANSACTION-TIME.  <013>*/
		/* MOVE VRCM-DATE               TO LIFA-TRANSACTION-DATE.  <013>*/
		lifacmvrec1.transactionDate.set(getCobolDate());
		lifacmvrec1.transactionTime.set(getCobolTime());
		lifacmvrec1.termid.set(SPACES);
		wsaaGrossVal.add(wsaaAcumGrossVal);
		lifacmvrec1.origamt.set(wsaaGrossVal);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError9000();
		}
		if (isEQ(wsaaDeemCharge, ZERO)) {
			return ;
		}
		lifacmvrec1.function.set("PSTW");
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			wsaaRldgChdrnum.set(utrnIO.getChdrnum());
			wsaaRldgLife.set(utrnIO.getLife());
			wsaaRldgCoverage.set(utrnIO.getCoverage());
			wsaaRldgRider.set(utrnIO.getRider());
			wsaaPlansuff.set(utrnIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.substituteCode[6].set(SPACES);
			lifacmvrec1.substituteCode[1].set(utrnIO.getCoverage());
			lifacmvrec1.substituteCode[2].set(utrnIO.getUnitVirtualFund());
			lifacmvrec1.sacscode.set(t5645rec.sacscode05);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec1.glcode.set(t5645rec.glmap05);
			lifacmvrec1.glsign.set(t5645rec.sign05);
			lifacmvrec1.contot.set(t5645rec.cnttot05);
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode06);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec1.glcode.set(t5645rec.glmap06);
			lifacmvrec1.glsign.set(t5645rec.sign06);
			lifacmvrec1.contot.set(t5645rec.cnttot06);
			lifacmvrec1.substituteCode[1].set(utrnIO.getCoverage());
			lifacmvrec1.substituteCode[2].set(utrnIO.getUnitVirtualFund());
			lifacmvrec1.substituteCode[6].set(SPACES);
			lifacmvrec1.rldgacct.set(SPACES);
			/*       MOVE WSAA-TRIGGER-CHDRNUM   TO LIFA-RLDGACCT         <013>*/
			lifacmvrec1.rldgacct.set(udtrigrec.tk3Chdrnum);
			lifacmvrec1.origamt.set(wsaaDeemCharge);
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec1.lifacmvRec);
				fatalError9000();
			}
		}
	}

protected void findNextComponent4000()
	{
		/*NEXT*/
		/* Read the next coverage/rider record.                            */
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		/*    IF  COVR-CHDRCOY         NOT = WSAA-TRIGGER-CHDRCOY     <003>*/
		/*    OR  COVR-CHDRNUM         NOT = WSAA-TRIGGER-CHDRNUM     <003>*/
		/*    OR  COVR-LIFE            NOT = WSAA-TRIGGER-LIFE        <003>*/
		if (isNE(covrIO.getChdrcoy(), udtrigrec.tk3Chdrcoy)
		|| isNE(covrIO.getChdrnum(), udtrigrec.tk3Chdrcoy)
		|| isNE(covrIO.getLife(), udtrigrec.tk3Life)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void getDescription5000()
	{
		/*PARA*/
		descIO.setDescpfx("IT");
		/*    MOVE WSAA-TRIGGER-CHDRCOY   TO DESC-DESCCOY.            <003>*/
		descIO.setDesccoy(udtrigrec.tk3Chdrcoy);
		/* MOVE 'E'                    TO DESC-LANGUAGE.        <LA3998>*/
		descIO.setLanguage(udtrigrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		lifacmvrec1.trandesc.set(descIO.getShortdesc());
		/*EXIT*/
	}

protected void readAndUpdateSaClaim6000()
	{
		go6010();
	}

protected void go6010()
	{
		clmdaddIO.setParams(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO CLMDADD-CHDRCOY.         <003>*/
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO CLMDADD-CHDRNUM.         <003>*/
		/* MOVE WSAA-TRIGGER-LIFE      TO CLMDADD-LIFE.            <003>*/
		/* MOVE WSAA-TRIGGER-COVERAGE  TO CLMDADD-COVERAGE.        <003>*/
		/* MOVE WSAA-TRIGGER-RIDER     TO CLMDADD-RIDER.           <003>*/
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setLife(udtrigrec.tk3Life);
		clmdaddIO.setCoverage(udtrigrec.tk3Coverage);
		clmdaddIO.setRider(udtrigrec.tk3Rider);
		clmdaddIO.setFieldType("S");
		clmdaddIO.setVirtualFund(SPACES);
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			/*    MOVE CLMDADD-STATUZ      TO SYSR-PARAMS              <003>*/
			syserrrec.params.set(clmdaddIO.getParams());
			syserrrec.statuz.set(clmdaddIO.getStatuz());
			fatalError9000();
		}
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setEstMatValue(ZERO);
		clmdaddIO.setActvalue(wsaaClaimAmount);
		clmdaddIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(udtrigrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(clmdaddIO.getCnstcur());
		zrdecplrec.batctrcde.set(udtrigrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError9000();
		}
		/*A090-EXIT*/
	}

protected void a100ReadHitr()
	{
		a110Hitr();
	}

protected void a110Hitr()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrcoy(udtrigrec.chdrcoy);
		hitrclmIO.setChdrnum(udtrigrec.chdrnum);
		hitrclmIO.setLife(udtrigrec.life);
		hitrclmIO.setCoverage(udtrigrec.coverage);
		hitrclmIO.setRider(udtrigrec.rider);
		hitrclmIO.setPlanSuffix(udtrigrec.planSuffix);
		hitrclmIO.setZintbfnd(udtrigrec.unitVirtualFund);
		hitrclmIO.setTranno(udtrigrec.tranno);
		hitrclmIO.setFormat(hitrclmrec);
		hitrclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
		/* Set up the UTRN fields required for posting ACMVs.              */
		wsaaContractType.set(hitrclmIO.getCnttyp());
		utrnIO.setBatccoy(hitrclmIO.getBatccoy());
		utrnIO.setBatcbrn(hitrclmIO.getBatcbrn());
		utrnIO.setBatcactyr(hitrclmIO.getBatcactyr());
		utrnIO.setBatcactmn(hitrclmIO.getBatcactmn());
		utrnIO.setBatctrcde(hitrclmIO.getBatctrcde());
		utrnIO.setBatcbatch(hitrclmIO.getBatcbatch());
		utrnIO.setTranno(hitrclmIO.getTranno());
		utrnIO.setCntcurr(hitrclmIO.getCntcurr());
		utrnIO.setMoniesDate(hitrclmIO.getEffdate());
		utrnIO.setCrtable(hitrclmIO.getCrtable());
	}

protected void a200ReadAndUpdateClaim()
	{
		a210Go();
	}

protected void a210Go()
	{
		clmdaddIO.setParams(SPACES);
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setLife(udtrigrec.tk3Life);
		clmdaddIO.setCoverage(udtrigrec.tk3Coverage);
		clmdaddIO.setRider(udtrigrec.tk3Rider);
		clmdaddIO.setFieldType("D");
		clmdaddIO.setVirtualFund(hitrclmIO.getZintbfnd());
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setEstMatValue(ZERO);
		setPrecision(clmdaddIO.getActvalue(), 2);
		clmdaddIO.setActvalue(add(clmdaddIO.getActvalue(), hitrclmIO.getContractAmount()));
		if (isGT(clmdaddIO.getActvalue(), ZERO)) {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), 1));
		}
		else {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), -1));
		}
		clmdaddIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
	}

protected void readMbnsFileA300() { //TKPOC1

clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setLife(udtrigrec.tk3Life);
		clmdaddIO.setCoverage(udtrigrec.tk3Coverage);
		clmdaddIO.setRider(udtrigrec.tk3Rider);

		mbnsIO.setParams(SPACES);  //TKPOC1
		chdrlifIO.setParams(SPACES); //TKPOC1
		chdrlifIO.setChdrcoy(udtrigrec.tk3Chdrcoy); //TKPOC1
		chdrlifIO.setChdrnum(udtrigrec.tk3Chdrnum); //TKPOC1
		chdrlifIO.setFormat(chdrlifrec); //TKPOC1
		chdrlifIO.setFunction(varcom.readr); //TKPOC1
		SmartFileCode.execute(appVars, chdrlifIO);//TKPOC1
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError9000();
		}

		datcon3rec.function.set(SPACES); //TKPOC1
		datcon3rec.intDate1.set(chdrlifIO.getOccdate()); //TKPOC1
		datcon3rec.intDate2.set(udtrigrec.effdate); //TKPOC1 
		datcon3rec.frequency.set("01"); //TKPOC1
		callProgram(Datcon3.class, datcon3rec.datcon3Rec); //TKPOC1
		if (isNE(datcon3rec.statuz,varcom.oK)) { //TKPOC1
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		compute(datcon3rec.freqFactor, 2).set(add(datcon3rec.freqFactor, 1)); //TKPOC1
		mbnsIO.setYrsinf(datcon3rec.freqFactor); //TKPOC1

		mbnsIO.setChdrcoy(udtrigrec.tk3Chdrcoy); //TKPOC1
		mbnsIO.setChdrnum(udtrigrec.tk3Chdrnum); //TKPOC1
		mbnsIO.setLife(udtrigrec.tk3Life); //TKPOC1
		mbnsIO.setCoverage(udtrigrec.tk3Coverage); //TKPOC1
		mbnsIO.setRider(udtrigrec.tk3Rider); //TKPOC1
		mbnsIO.setFormat(mbnsrec); //TKPOC1
		mbnsIO.setFunction(varcom.readr); //TKPOC1
		//performance improvement -- Anjali
	
		SmartFileCode.execute(appVars, mbnsIO); //TKPOC1
		if (isNE(mbnsIO.getStatuz(),varcom.oK)) { //TKPOC1
			syserrrec.params.set(mbnsIO.getParams());
			fatalError9000();
		}
		wsaaSumins.set(mbnsIO.getSumins()); //TKPOC1

}


protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
{
	udtrigrec.statuz.set(varcom.bomb);
	/*EXIT*/
	exitProgram();
}
}
