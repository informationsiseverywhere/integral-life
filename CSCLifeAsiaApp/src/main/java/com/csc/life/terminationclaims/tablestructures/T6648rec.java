package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:52
 * Description:
 * Copybook name: T6648REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6648rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6648Rec = new FixedLengthStringData(560);
  	public FixedLengthStringData minisumas = new FixedLengthStringData(65).isAPartOf(t6648Rec, 0);
  	public ZonedDecimalData[] minisuma = ZDArrayPartOfStructure(5, 13, 2, minisumas, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(65).isAPartOf(minisumas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData minisuma01 = new ZonedDecimalData(13, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData minisuma02 = new ZonedDecimalData(13, 2).isAPartOf(filler, 13);
  	public ZonedDecimalData minisuma03 = new ZonedDecimalData(13, 2).isAPartOf(filler, 26);
  	public ZonedDecimalData minisuma04 = new ZonedDecimalData(13, 2).isAPartOf(filler, 39);
  	public ZonedDecimalData minisuma05 = new ZonedDecimalData(13, 2).isAPartOf(filler, 52);
  	public FixedLengthStringData minmthifs = new FixedLengthStringData(15).isAPartOf(t6648Rec, 65);
  	public ZonedDecimalData[] minmthif = ZDArrayPartOfStructure(5, 3, 0, minmthifs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(15).isAPartOf(minmthifs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData minmthif01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData minmthif02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
  	public ZonedDecimalData minmthif03 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData minmthif04 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 9);
  	public ZonedDecimalData minmthif05 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 12);
  	public FixedLengthStringData minterms = new FixedLengthStringData(20).isAPartOf(t6648Rec, 80);
  	public ZonedDecimalData[] minterm = ZDArrayPartOfStructure(5, 4, 0, minterms, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(minterms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData minterm01 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData minterm02 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 4);
  	public ZonedDecimalData minterm03 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 8);
  	public ZonedDecimalData minterm04 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData minterm05 = new ZonedDecimalData(4, 0).isAPartOf(filler2, 16);
  	public FixedLengthStringData minpusas = new FixedLengthStringData(65).isAPartOf(t6648Rec, 100);
  	public ZonedDecimalData[] minpusa = ZDArrayPartOfStructure(5, 13, 2, minpusas, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(65).isAPartOf(minpusas, 0, FILLER_REDEFINE);
  	public ZonedDecimalData minpusa01 = new ZonedDecimalData(13, 2).isAPartOf(filler3, 0);
  	public ZonedDecimalData minpusa02 = new ZonedDecimalData(13, 2).isAPartOf(filler3, 13);
  	public ZonedDecimalData minpusa03 = new ZonedDecimalData(13, 2).isAPartOf(filler3, 26);
  	public ZonedDecimalData minpusa04 = new ZonedDecimalData(13, 2).isAPartOf(filler3, 39);
  	public ZonedDecimalData minpusa05 = new ZonedDecimalData(13, 2).isAPartOf(filler3, 52);
  	public FixedLengthStringData minipups = new FixedLengthStringData(65).isAPartOf(t6648Rec, 165);
  	public ZonedDecimalData[] minipup = ZDArrayPartOfStructure(5, 13, 2, minipups, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(65).isAPartOf(minipups, 0, FILLER_REDEFINE);
  	public ZonedDecimalData minipup01 = new ZonedDecimalData(13, 2).isAPartOf(filler4, 0);
  	public ZonedDecimalData minipup02 = new ZonedDecimalData(13, 2).isAPartOf(filler4, 13);
  	public ZonedDecimalData minipup03 = new ZonedDecimalData(13, 2).isAPartOf(filler4, 26);
  	public ZonedDecimalData minipup04 = new ZonedDecimalData(13, 2).isAPartOf(filler4, 39);
  	public ZonedDecimalData minipup05 = new ZonedDecimalData(13, 2).isAPartOf(filler4, 52);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(330).isAPartOf(t6648Rec, 230, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6648Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6648Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}