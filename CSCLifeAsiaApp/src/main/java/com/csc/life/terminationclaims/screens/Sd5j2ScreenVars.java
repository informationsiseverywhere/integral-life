package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Sd5j2ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(142);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(46).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9); 
	public FixedLengthStringData shortTerm = DD.shortTerm.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData longTerm = DD.longTerm.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,41);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 46);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData shortTermErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longTermErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 70);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] shortTermOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longTermOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sd5j2screenWritten = new LongData(0);
	public LongData Sd5j2protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5j2ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, shortTerm,longTerm};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, shortTermOut,longTermOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, shortTermErr,longTermErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5j2screen.class;
		protectRecord = Sd5j2protect.class;
	}

}
