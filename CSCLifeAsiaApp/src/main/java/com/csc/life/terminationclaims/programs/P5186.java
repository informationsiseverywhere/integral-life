/*
 * File: P5186.java
 * Date: 30 August 2009 0:17:50
 * Author: Quipoz Limited
 * 
 * Class transformed from P5186.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.util.List;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5677rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Ty501rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.screens.S5186ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrrgpTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.SlckpfDAO;
import com.csc.smart400framework.dataaccess.dao.impl.SlckpfDAOImpl;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Slckpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*             P5186 - REGULAR PAYMENTS SELECTION
*             u001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001au001a
*Initialise
*----------
*
*  Skip  this  section  if  returning from an optional selection
*  (current stack position action flag = '*').
*
*  Clear the subfile ready for loading.
*
*  The  details  of  the  contract  being  enquired upon will be
*  stored  in  the  CHDRRGP I/O module. Retrieve the details and
*  set up the header portion of the screen. Some fields are not
*  present and the CHDRMJA I/O module will need to be read to
*  obtain these fields.
*
*  Look up the following descriptions and names:
*
*       Contract Type, (CNTTYPE) - long description from T5688,
*
*       Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*
*       Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*
*       The owner's client (CLTS) details.
*
*       The joint  owner's  client (CLTS) details if they exist.
*
*  Load the subfile as follows:
*
*  Retrieve the COVRRGP file (RETRV), if the selected plan has a
*  plan suffix  which  is  contained  within the summarised plan
*  record:  less than  or  equal  to  the  number  of summarised
*  policies (CHDR-POLSUM):  then the policy must be 'broken out'
*  with each life,  coverage  and rider for an associated policy
*  written to the  subfile  along  with  the  description of the
*  cover/rider.
*
*       Initial Screen Load.
*       --------------------
*
*       After the contract we read all of the components and
*       write one line to the  subfile  for  each.  The  normal
*       indentation  should  be  employed in the display of the
*       Life, Coverage and Rider  sequence  numbers.  For  each
*       life  read  the LIFE file and obtain the Life Assured's
*       Client Number. Place this on the screen and  read  CLTS
*       to  obtain  the client's name. Format this for display.
*       The selection field should be protected  for  the  line
*       displaying the life details.
*
*       On  the  coverage  and rider lines display the coverage
*       code, (CRTABLE) and  read  T5687  to  obtain  the  long
*       description for display.
*
*       The  component  lines  should  be interspersed with the
*       details of any existing  payment  detail.  So  for  any
*       component  read the Regular Payment Details file, REGP,
*       and if corresponding  records  are  found  display  the
*       payment details as follows:
*
*            .  In  the  same  place  as  the client number and
*            coverage  code  are  displayed  show   the   short
*            description from T6691, keyed on the Payment Type.
*
*            . Display the payment status.
*
*            .  Display  the  First  Payment Date and the Final
*            Payment Date if it is not Max Date.
*
*            .  If  the  amount  is   non-zero   display   this
*            otherwise display the percentage.
*
*            . Display the currency code.
*
*       These  details  are all displayed on one long field and
*       so will require formatting within the program.
*
*       Also store the Regular Payment  Sequence  Number  in  a
*       hidden  field  on  the  subfile for later processing if
*       the line is selected for further action.
*
*       For all component and  Regular  Payment  subfile  lines
*       set  the  Life,  Coverage  and  Rider  numbers  in  the
*       appropriate fields but non-display where  necessary  to
*       provide  the  indentation  and  also  the values of the
*       fields for later processing. Also store the CRTABLE  in
*       a  hidden  field  on  each  subfile  record  for  later
*       processing.
*
*  Load all pages  required  in  the subfile and set the subfile
*  more indicator to no.
*
*  Page  up  and  Page  down  should  never  be received as the
*  subfile load will load all of the  components  and  existing
*  payment  details  regardless  of  how  many there are. There
*  should never be so many that it impairs system response.
*
*
*Validation
*----------
*
*  . Selections to Process
*       Two passes will  be  made  over  the  subfile,  one  to
*       validate   the   selections   and   a  second,  if  the
*       selections are all valid, to process them.  The  second
*       pass will be carried out in the 4000 section.
*
*       For  the  first  pass perform a loop using Subfile Read
*       Next Changed to pick up all of the selected lines.  For
*       each  one  check  that it is valid by reading T6693 and
*       ensuring that the selected action is a valid  entry  on
*       the Extra Data Screen.
*
*       If  the selected line is itself a Coverage or Rider use
*       a key of  '**'  concatenated  with  the  Coverage/Rider
*       Code.  If  no  entry  is  found  then  Regular  Payment
*       details may not be set up against  this  component.  If
*       an  entry  is  found  then  check  the Transaction Code
*       obtained from T1690 against the Allowable  Transactions
*       on  the  Extra  Data  Screen. If no match is found then
*       the action is invalid against that component.
*
*       If the selected line is a  Payment  Details  line  then
*       read  T6693  with  a  key of the Regular Payment Status
*       concatenated with the associated Component Code. If  no
*       entry  is found then read again using four asterisks in
*       place of the Component  Code.  If  there  is  still  no
*       entry  then there is no possible action for the status.
*       If  an  entry  is  found  then  check  the  transaction
*       against  the  Allowable  Transactions on the Extra Data
*       Screen.
*
*       If no selections were made re-set the subfile RRN to  1
*       and re-display the screen.
*
*
*Updating
*--------
*
*  There is no validation in this program.
*
*
*Next Program
*------------
*
*  If "KILL" was requested move spaces to  the  current  program
*  position and action field, add 1 to the  program  pointer and
*  exit.
*
*  At this point the program will be either  searching  for  the
*  FIRST  selected  record  in  order  to pass  control  to  the
*  appropriate  generic  enquiry  program   for   the   selected
*  component  or it will be returning from one  of  the  Enquiry
*  screens after displaying some details and  searching  for the
*  NEXT selected record.
*
*  It will be able  to determine which of these two states it is
*  in by examining the Stack Action Flag.
*
*  If not returning  from  a  component (stack action is blank),
*  save the next four  programs currently on the stack. Read the
*  first record from the subfile. If this is not  selected, read
*  the next one and so on, until a selected  record is found, or
*  the end of the subfile is reached.
*
*  If a subfile  record  has been selected, look up the programs
*  required to  be  processed  from  the coverage/rider programs
*  table (T5671  -  accessed  by transaction number concatenated
*  with coverage/rider code from the subfile record). Move these
*  four programs into  the  program  stack  and  set the current
*  stack action to '*'  (so  that the system will return to this
*  program to process the next one).
*
*  If  the  selection  has  been made against a regular payment
*  line then store the REGP record with a READS.
*
*  If the selection is a Create  then  set  up  an  initialised
*  REGP record with the following:
*
*       CHDRCOY   -    Signon Company
*       CHDRNUM   -    Contract Number
*       PLNSFX    -    Zeroes
*       LIFE      -    Life Sequence Number
*       COVERAGE  -    Coverage Sequence Number
*       RIDER     -    Rider Sequence Number
*       CRTABLE   -    Coverage/Rider Code
*
*  and  the  remaining  fields  spaces and zeroes and perform a
*  KEEPS on REGP.
*
*  In Register Mode, Default  Follow  Ups will be created  if a
*  default  follow  up  method  exists on  T5688. T5677 is read
*  using the  follow up method  as  the  key and  up to 16 FLUP
*  records will be created.
*
*  Add one to  the  program  pointer  and  exit  to  process the
*  required generic component.
*
*  If nothing was selected or there are no  more  selections  to
*  process,  continue  by  moving blanks to  the  current  Stack
*  Action  field  reload  the  saved  four programs and exit.
*
*Notes.
*------
*
*  Tables Used:
*
*  . T5661 - Follow Up Codes
*            Key: T5677-Follow up Code
*
*  . T5671 - Generic program Switching
*            Key: Transaction Code || CRTABLE
*
*  . T5677 - Default Follow Ups
*            Key: Transaction Code || Default Follow Up Method
*
*  . T5687 - Coverage / Rider Details
*            Key: CRTABLE
*
*  . T5688 - Contract Definition
*            Key: Contract Type
*
*  . T1690 - Submenu Switching
*            Key: WSAA-PROG and Submenu Action
*
*  . T6691 - Regular Payment Type
*            Key: Regular Payment Type
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be '****' and for selections against
*            a Component set Payment Status to '**'.
*
*
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class P5186 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5186");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/* WSAA-MISCELLANEOUS-FLAGS */
	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");

	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");
	private String wsaaFirstRead = "";
	
	//ILIFE-1530 STARTS
	private FixedLengthStringData wsaaPaymentDetails = new FixedLengthStringData(48);
	//ILIFE-1530 ENDS
	private FixedLengthStringData wsaaPaystat = new FixedLengthStringData(2).isAPartOf(wsaaPaymentDetails, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 2, FILLER).init(SPACES);
	private FixedLengthStringData wsaaFirstPaydate = new FixedLengthStringData(10).isAPartOf(wsaaPaymentDetails, 3);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 13, FILLER).init(SPACES);
	private FixedLengthStringData wsaaFinalPaydate = new FixedLengthStringData(10).isAPartOf(wsaaPaymentDetails, 14).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 24, FILLER).init(SPACES);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3).isAPartOf(wsaaPaymentDetails, 25);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaPaymentDetails, 28, FILLER).init(SPACES);
	//ILIFE-1530 STARTS
	private ZonedDecimalData wsaaAmtPrcntVal = new ZonedDecimalData(13, 2).isAPartOf(wsaaPaymentDetails, 29).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData wsaaPercentSign = new FixedLengthStringData(2).isAPartOf(wsaaPaymentDetails, 46);
	//ILIFE-1530 ENDS
	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);

	private FixedLengthStringData wsaaT5677Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5677Tranno = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 0);
	private FixedLengthStringData wsaaT5677FollowUp = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 4);

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);
		/* WSAA-PROGRAM-SAVE */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(4, 5);

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
		/* ERRORS */
	private static final String e005 = "E005";
	private static final String e308 = "E308";
	private static final String f388 = "F388";
	private static final String g433 = "G433";
	private static final String g931 = "G931";
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5661 = "T5661";
	private static final String t5671 = "T5671";
	private static final String t5677 = "T5677";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t6691 = "T6691";
	private static final String t6693 = "T6693";
	private static final String tr386 = "TR386";
	protected ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CovrrgpTableDAM covrrgpIO = new CovrrgpTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5661rec t5661rec = new T5661rec();
	private T5671rec t5671rec = new T5671rec();
	private T5677rec t5677rec = new T5677rec();
	private T5688rec t5688rec = new T5688rec();
	private T6693rec t6693rec = new T6693rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5186ScreenVars sv = getLScreenVars();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	
	private FixedLengthStringData wsaaCoverageStatuzCheck = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaCovrValidStatuz = new FixedLengthStringData(1).isAPartOf(wsaaCoverageStatuzCheck, 0);
	//ILIFE-1138 STARTS
	private FixedLengthStringData wsaaContractStatuzCheck = new FixedLengthStringData(5);	
	private FixedLengthStringData wsaaContStatcode = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 1);
	private FixedLengthStringData wsaaContPstcde = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 3);
	/*ILIFE-1242 */
	private static final String h912  = "H912";
	private static final String ty501 = "TY501";
	private Ty501rec ty501rec = new Ty501rec();
	//ILIFE-1138 ENDS
	private ChdrpfDAO chdrpfDAO=getApplicationContext().getBean("chdrDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf;
	private Clntpf clntpf;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	protected List<Lifepf> lifeList;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrList;
	private Covrpf covrpf;
	private RegppfDAO regppfDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private List<Regppf> regpList;
	private Fluppf fluppf=new Fluppf();
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private Slckpf slckpf=new Slckpf();
	private SlckpfDAO slckpfDAO = new SlckpfDAOImpl();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit1100, 
		getRegPayment1310, 
		exit1300, 
		exit2090, 
		updateErrorIndicators2650, 
		exit2690, 
		exit4090, 
		exit4190, 
		exit4290, 
		readProgramTable4410, 
		exit4490, 
		exit4590, 
		exit5090, 
		exit5390
	}

	public P5186() {
		super();
		screenVars = sv;
		new ScreenModel("S5186", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

protected void plainname() {
	/* PLAIN-100 */
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(clntpf.getClttype(), "C")) {
		corpname();

	} else if (isNE(clntpf.getGivname(), SPACES)) {
		String firstName = clntpf.getGivname();/* IJTI-1523 */
		String lastName = clntpf.getSurname();/* IJTI-1523 */
		String delimiter = ",";

		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);

	} else {
		wsspcomn.longconfname.set(clntpf.getSurname());
	}
	/* PLAIN-EXIT */
}
protected void corpname() {
	/* PAYEE-1001 */
	wsspcomn.longconfname.set(SPACES);
	/* STRING CLTS-SURNAME DELIMITED SIZE */
	/* CLTS-GIVNAME DELIMITED ' ' */
	String firstName = clntpf.getLgivname();/* IJTI-1523 */
	String lastName = clntpf.getLsurname();/* IJTI-1523 */
	String delimiter = "";

	// this way we can override StringUtil behaviour in formatName()
	String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
	wsspcomn.longconfname.set(fullName);
	/* CORP-EXIT */
}
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1010();
			retrvContract1020();
			headerDetail1030();
			retrvCoverage1040();
			readLifeDetails1050();
			jointLifeDetails1060();
			contractTypeStatus1070();
			subfileLoad1080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialise1010()
	{
		/* If returning from a multiple selection then go to exit.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		/* Clear the WORKING STORAGE.*/
		wsaaImmexitFlag.set(SPACES);
		wsaaSelectFlag.set(SPACES);
		//ILIFE-1138 STARTS
		wsaaCovrValidStatuz.set("N");
		//ILIFE-1138 ENDS
		wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
		/* Set up basic data. Get todays date.*/
		wsaaBatckey.set(wsspcomn.batchkey);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaMiscellaneousInner.wsaaEffdate.set(datcon1rec.intDate);
		/* Clear the Subfile.*/
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.planSuffix.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlansuff.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5186", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrvContract1020()
	{
		/* Read CHDRRGP (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		chdrrgpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		/* Read CHDRMJA to obtain header info not present on CHDRRGP.*/
		chdrpf=chdrpfDAO.readChdrpfForLetter(chdrrgpIO.getChdrcoy().toString(), chdrrgpIO.getChdrnum().toString());
	}

protected void headerDetail1030()
	{
		/* Move Contract details to the screen header.*/
		//ILIFE-1138 STARTS
		wsaaContractStatuzCheck.set(SPACES);
		//ILIFE-1138 ENDS
		sv.chdrnum.set(chdrrgpIO.getChdrnum());
		sv.cnttype.set(chdrrgpIO.getCnttype());
		sv.cntcurr.set(chdrrgpIO.getCntcurr());
		sv.register.set(chdrpf.getReg());
		sv.numpols.set(chdrrgpIO.getPolinc());
		//ILIFE-1138 STARTS
		wsaaContStatcode.set(chdrrgpIO.getStatcode());
		wsaaContPstcde.set(chdrrgpIO.getPstatcode());
		//ILIFE-1138 ENDS
	}

protected void retrvCoverage1040()
	{
		/* Retrieve the coverage in order to obtain policy selected.*/
		covrrgpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrrgpIO);
		if (isNE(covrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrgpIO.getParams());
			fatalError600();
		}
		sv.planSuffix.set(covrrgpIO.getPlanSuffix());
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(covrrgpIO.getPlanSuffix());
		/* Release the coverage.*/
		covrrgpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrrgpIO);
		if (isNE(covrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrgpIO.getParams());
			fatalError600();
		}
		/*--- Read TR386 table to get screen literals                      */
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		wsaaTr386Id.set(SPACES);
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),tr386, wsaaTr386Key.toString());
		tr386rec.tr386Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		/* If Covr plan suffix is zero then we have selected accross the*/
		/*    whole plan.*/
		/* If Covr plan suffix is NOT zero then we have selected a partic-*/
		/*    ular Policy.*/
		if (isEQ(sv.planSuffix, ZERO)) {
			wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set("L");
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			/*      MOVE 'Whole Plan      '  TO S5186-ENTITY                  */
			/*      MOVE TR386-PROGDESC-1    TO S5186-ENTITY                  */
			sv.entity.set(tr386rec.progdesc01);
			sv.entityOut[varcom.hi.toInt()].set("Y");
		}
		else {
			wsaaMiscellaneousInner.wsaaPlanPolicyFlag.set("O");
			sv.plnsfxOut[varcom.nd.toInt()].set(" ");
			/*      MOVE 'Policy Number  :'  TO S5186-ENTITY                  */
			/*      MOVE TR386-PROGDESC-2    TO S5186-ENTITY                  */
			sv.entity.set(tr386rec.progdesc02);
			sv.entityOut[varcom.hi.toInt()].set(" ");
		}
		/* If the policy selected is a summarised policy then to read the*/
		/* Coverage/Rider records we must zeroise the Plan Suffix.*/
		if (isLTE(covrrgpIO.getPlanSuffix(), chdrrgpIO.getPolsum())
		&& isNE(covrrgpIO.getPlanSuffix(), ZERO)
		&& isNE(chdrrgpIO.getPolsum(), 1)) {
			wsaaMiscellaneousInner.wsaaPlanSuffix.set(ZERO);
		}
		/* If a COVR record has been retrieved then the program must have*/
		/* been invoked from the Policy Selection screen. The record is*/
		/* then released and processing should continue as for a Plan. The*/
		/* following BEGN will position this program at the first COVR*/
		/* record for the selected policy.*/
		covrrgpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, covrrgpIO);
		if (isNE(covrrgpIO.getStatuz(), varcom.oK)
		&& isNE(covrrgpIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrrgpIO.getParams());
			fatalError600();
		}
	}

protected void readLifeDetails1050()
	{
		/* Obtain the Life Assured and Joint Life Assured, if they exist.*/
		/* The BEGN function is used to retrieve the first Life for the*/
		/* contract in case life '01' has been deleted.*/
		lifeList = lifepfDAO.getLifeData(chdrrgpIO.getChdrcoy().toString(), chdrrgpIO.getChdrnum().toString(), "01", "00");
		/* Get the client name.*/
		sv.lifenum.set(lifeList.get(0).getLifcnum());
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifeList.get(0).getLifcnum());
		/* Format the Name in Plain format.*/
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void jointLifeDetails1060()
	{
		/* Check for the existence of Joint Life details.*/
		lifeList = lifepfDAO.getLifeData(chdrrgpIO.getChdrcoy().toString(), chdrrgpIO.getChdrnum().toString(), "01", "01");
		
		if (lifeList.size()>0) {
			sv.jlife.set(lifeList.get(0).getLifcnum());
			clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifeList.get(0).getLifcnum());
			plainname();
			sv.jlifename.set(wsspcomn.longconfname);
			
		}
	}

protected void contractTypeStatus1070()
	{
		/* Obtain the Contract Type description from T5688.*/
		descpf=descDAO.getdescData("IT", t5688, chdrrgpIO.getCnttype().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		/* Obtain the Contract Status description from T3623.*/
		descpf=descDAO.getdescData("IT", t3623, chdrrgpIO.getStatcode().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descpf.getShortdesc());
		}
		/* Obtain the Premuim Status description from T3588.*/
		descpf=descDAO.getdescData("IT", t3588, chdrrgpIO.getPstatcode().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descpf.getShortdesc());
		}
	}

protected void subfileLoad1080()
	{
		/* Read the first LIFE details on the contract for the Load of the*/
		/* Subfile.*/
		lifeList=lifepfDAO.getLfRecords(wsspcomn.company.toString(), chdrrgpIO.getChdrnum().toString(), "00");
		
		for (Lifepf lifepf:lifeList) {
			if(lifepf.getValidflag().trim().equals("1"))	//IJS-590
				getPolicyDetails1100(lifepf);
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void getPolicyDetails1100(Lifepf lifepf)
	{
		try {
			getClientName1120(lifepf);
			getCompPayDetails1130(lifepf);
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

		protected void getClientName1120(Lifepf lifepf)
	{
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifepf.getLifcnum());
		plainname();
		/* Add record to subfile*/
		sv.subfileArea.set(SPACES);
		sv.plansuff.set(ZERO);
		sv.life.set(lifepf.getLife());
		sv.shortdesc.set(lifepf.getLifcnum());
		sv.elemdesc.set(wsspcomn.longconfname);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		sv.selectOut[varcom.nd.toInt()].set("Y");
		sv.coverageOut[varcom.nd.toInt()].set("Y");
		sv.riderOut[varcom.nd.toInt()].set("Y");
		sv.plansuffOut[varcom.nd.toInt()].set("Y");
		addToSubfile1500();
	}

protected void getCompPayDetails1130(Lifepf lifepf)
	{
		/* Read in the details of the Plan Components and any relevant Paym*/
	    covrpf=new Covrpf();
		covrpf.setChdrcoy(lifepf.getChdrcoy());
		covrpf.setChdrnum(lifepf.getChdrnum());
		covrpf.setLife(lifepf.getLife());
		covrList=covrpfDAO.selectCoverage(covrpf);
		for ( Covrpf covr:covrList) {
			getComponentDetails1300(covr);
		}
		
	}

protected void getComponentDetails1300(Covrpf covr)
	{
		
		/* Search for relevant Coverages / Riders*/
		/*  Check if the record is a broken out part of a component otherwi*/
		/*   set up indentation for display of component hierarchy*/
		if ((isEQ(covr.getLife(), sv.life))
		&& (isEQ(covr.getCoverage(), sv.coverage))
		&& (isEQ(covr.getRider(), sv.rider))) {
			return;
		}
		else {
			sv.subfileArea.set(SPACES);
			sv.life.set(covr.getLife());
			sv.rider.set(covr.getRider());
			sv.coverage.set(covr.getCoverage());
			//ILIFE-1138 STARTS
			sv.covrriskstat.set(covr.getStatcode());
			sv.covrpremstat.set(covr.getPstatcode());
			//ILIFE-1138 ENDS
			sv.plansuff.set(ZERO);
			sv.lifeOut[varcom.nd.toInt()].set("Y");
			if ((isEQ(covr.getRider(), ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
			}
			sv.plansuffOut[varcom.nd.toInt()].set("Y");
			/*  Display the component name and obtain its description from T568*/
			descpf=descDAO.getdescData("IT", t5687, covr.getCrtable(), wsspcomn.company.toString(), wsspcomn.language.toString());
			/* Add record to subfile*/
			if (descpf==null) { 
				sv.elemdesc.set(SPACES);
			}
			else {
				sv.elemdesc.set(descpf.getLongdesc());
			}
			sv.shortdesc.set(covr.getCrtable());
			sv.hcrtable.set(covr.getCrtable());
			/* If not in a create mode (WSSP-FLAG not = 'C') then we cannot*/
			/* process components, only payments, so protect the selection.*/
			if (isNE(wsspcomn.flag, "C")) {
				sv.selectOut[varcom.pr.toInt()].set("Y");
				sv.selectOut[varcom.nd.toInt()].set("Y");
			}
			addToSubfile1500();
			/* Get the Payment Details (if any)*/
			getRegPayment1310(covr);
		}
	}

	/**
	* <pre>
	* Read in the details of any Payments against the components
	* </pre>
	*/
protected void getRegPayment1310(Covrpf covr)
	{
		
		regpList=regppfDAO.readRecord(covr.getChdrcoy(), covr.getChdrnum(), covr.getLife(), covr.getCoverage(), covr.getRider());
		for(Regppf regppf:regpList) {
			getPaymentDetails1400(covr,regppf);
		}
		
	}

protected void getPaymentDetails1400(Covrpf covr,Regppf regppf)
	{
		
		if ((isNE(wsaaMiscellaneousInner.wsaaPlanSuffix, ZERO))
		&& (isNE(wsaaMiscellaneousInner.wsaaPlanSuffix, regppf.getPlanSuffix()))) {
			return ;
		}
		/* Obtain the payment type description from T6691*/
		descpf=descDAO.getdescData("IT", t6691, regppf.getRgpytype(), wsspcomn.company.toString(), wsspcomn.language.toString());
		/* Format details for output to screen*/
		datcon1rec.intDate.set(regppf.getFirstPaydate());
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaFirstPaydate.set(datcon1rec.extDate);
		if (isNE(regppf.getFinalPaydate(), varcom.vrcmMaxDate)) {
			datcon1rec.intDate.set(regppf.getFinalPaydate());
			datcon1rec.function.set("CONV");
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				fatalError600();
			}
			wsaaFinalPaydate.set(datcon1rec.extDate);
		}
		else {
			wsaaFinalPaydate.set(SPACES);
		}
		wsaaPaystat.set(regppf.getRgpystat());
		wsaaCurrcode.set(regppf.getCurrcd());
		if (isNE(regppf.getPymt(), ZERO)) {
			wsaaAmtPrcntVal.set(regppf.getPymt());
			wsaaPercentSign.set(SPACES);
		}
		else {
			wsaaAmtPrcntVal.set(regppf.getPrcnt());
			wsaaPercentSign.set(" %");
		}
		/* Add record to subfile*/
		sv.subfileArea.set(SPACES);
		if (descpf==null) {
			sv.shortdesc.set(SPACES);
		}
		else {
			sv.shortdesc.set(descpf.getShortdesc());
		}
		sv.life.set(regppf.getLife());
		sv.coverage.set(regppf.getCoverage());
		sv.rider.set(regppf.getRider());
		sv.plansuff.set(regppf.getPlanSuffix());
		sv.lifeOut[varcom.nd.toInt()].set("Y");
		sv.coverageOut[varcom.nd.toInt()].set("Y");
		sv.riderOut[varcom.nd.toInt()].set("Y");
		if (isEQ(regppf.getPlanSuffix(), ZERO)
		|| isEQ(regppf.getPlanSuffix(), wsaaMiscellaneousInner.wsaaPlansuff)) {
			sv.plansuffOut[varcom.nd.toInt()].set("Y");
		}
		sv.hcrtable.set(covr.getCrtable());
		sv.hrgpynum.set(regppf.getRgpynum());
		sv.elemdesc.set(wsaaPaymentDetails);
		wsaaMiscellaneousInner.wsaaPlansuff.set(regppf.getPlanSuffix());
		/* If in a create mode (WSSP-FLAG = 'C') then we can only process*/
		/* components, not payments, so protect the selection.*/
		if (isEQ(wsspcomn.flag, "C")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.nd.toInt()].set("Y");
		}
		addToSubfile1500();
	}

protected void addToSubfile1500()
	{
		/*ADD-LINE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5186", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		scrnparams.subfileRrn.set(1);
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5186IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5186-DATA-AREA                         */
		/*                         S5186-SUBFILE-AREA.                     */
		/* If user has abandoned the transaction, then prevent*/
		/* program from continuing.*/
		if (isEQ(scrnparams.statuz, "SUBM ")) {
			wsspcomn.flag.set("K");
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If termination of processing then go to exit. P6350 will*/
		/*  release the soft lock.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSubfile2030()
	{
		/* Validate the selections within the subfile.*/
		wsaaFirstRead = "Y";
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
		/* If no selections.*/
		if(isEQ(wsspcomn.flag, "C") && isEQ(wsaaMiscellaneousInner.wsaaSelected, 0))
		{
				scrnparams.errorCode.set(g931);
				wsspcomn.edterror.set("Y");
		}
		/* If more than one selection.*/
		if (isGT(wsaaMiscellaneousInner.wsaaSelected, 1)) {
            //ILIFE-1251 starts		
			if(isNE(wsspcomn.flag, "I")){
			  scrnparams.errorCode.set(f388);
			  wsspcomn.edterror.set("Y");
			}//ILIFE-1251 ends
		}
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readNextRecord2610();
					validation2630();
				case updateErrorIndicators2650: 
					updateErrorIndicators2650();
				case exit2690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readNextRecord2610()
	{
		if (isEQ(wsaaFirstRead, "Y")) {
			wsaaFirstRead = "N";
			wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
			scrnparams.function.set(varcom.sstrt);
			scrnparams.subfileRrn.set(1);
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		processScreen("S5186", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if ((isEQ(scrnparams.statuz, varcom.endp))) {
			goTo(GotoLabel.exit2690);
		}
		/* If record has space in the select AND it has already*/
		/* been processed then count it as a valid selection then*/
		/* exit. This is to force the user to only have a single*/
		/* selection given subsequent program limitations.*/
		if (isEQ(sv.select, SPACES)
		&& isNE(sv.asterisk, SPACES)) {
			wsaaMiscellaneousInner.wsaaSelected.add(1);
			goTo(GotoLabel.exit2690);
		}
		/* If record has space in the select then bypass it.*/
		if (isEQ(sv.select, SPACES)) {
			goTo(GotoLabel.exit2690);
		}
	}

protected void validation2630()
	{
		/*  Determine what type of record has been selected (C/R or Payment*/
		//ILIFE-1138 STARTS
		if((isEQ(wsaaContStatcode, "RD") || isEQ(wsaaContStatcode, "DH")) && isEQ(wsaaContPstcde, "DH")) {
			readTy501Covrstat();
		}
		//ILIFE-1138 ENDS
		if ((isNE(sv.hrgpynum, SPACES))) {		
			wsaaPaymentDetails.set(sv.elemdesc);
			wsaaT6693Paystat.set(wsaaPaystat);
			wsaaT6693Crtable.set(sv.hcrtable);
			readT6693Table2700();
			if (itempfList.size()==0) {
				wsaaT6693Crtable.set("****");
				readT6693Table2700();
			}
		}
		else {
			wsaaT6693Crtable.set(sv.hcrtable);
			wsaaT6693Paystat.set("**");
			readT6693Table2700();
		}
		/* If no item found, then there is no allowable transaction*/
		if (itempfList.size()>0) {
			t6693rec.t6693Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		else {
			sv.selectErr.set(e005);
			goTo(GotoLabel.updateErrorIndicators2650);
		}
		/*  Read through array of allowable transactions to determine if*/
		/*   transaction is valid*/
		wsaaMiscellaneousInner.wsaaSub.set(1);
		while ( !((isGT(wsaaMiscellaneousInner.wsaaSub, 12))
		|| (isEQ(wsaaBatckey.batcBatctrcde, t6693rec.trcode[wsaaMiscellaneousInner.wsaaSub.toInt()])))) {
			if (isNE(wsaaBatckey.batcBatctrcde, t6693rec.trcode[wsaaMiscellaneousInner.wsaaSub.toInt()])) {
				wsaaMiscellaneousInner.wsaaSub.add(1);
			}
		}
		
		if (isGT(wsaaMiscellaneousInner.wsaaSub, 12)) {
			sv.selectErr.set(e005);
			goTo(GotoLabel.updateErrorIndicators2650);
		}
		/* If record selected, and valid, update selection counter as*/
		/* we can only have single selections at present due to*/
		/* limitations in the subsequent programs.*/
		wsaaMiscellaneousInner.wsaaSelected.add(1);
		goTo(GotoLabel.exit2690);
	}

//ILIFE-1138 STARTS
protected void  readTy501Covrstat() {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sv.hcrtable);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),ty501, stringVariable1.toString());
		if(itempfList.size()>0)
		{
			ty501rec.ty501Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
					|| isEQ(wsaaCovrValidStatuz, "Y")); wsaaSub.add(1)){
						if (isEQ(ty501rec.covRiskStat[wsaaSub.toInt()], sv.covrriskstat)) {
							for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
							|| isEQ(wsaaCovrValidStatuz, "Y")); wsaaSub.add(1)){
								if (isEQ(ty501rec.covPremStat[wsaaSub.toInt()], sv.covrpremstat)) {
									wsaaCovrValidStatuz.set("Y");
								}
							}
						}
			}
		}
		if (isEQ(wsaaCovrValidStatuz, "N")) {
			/*ILIFE-1242*/
			/*sv.selectErr.set(e717);*/
			sv.selectErr.set(h912);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.updateErrorIndicators2650);
		}
}
//ILIFE-1138 ENDS

protected void updateErrorIndicators2650()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* NOTE that we space and protect the invalid selection to*/
		/* stop a user re-selecting it.*/
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set("Y");
		if (isEQ(sv.hrgpynum, SPACES)) {
			if ((isEQ(sv.rider, ZERO))) {
				sv.riderOut[varcom.nd.toInt()].set("Y");
				sv.coverageOut[varcom.nd.toInt()].set(" ");
			}
			else {
				sv.coverageOut[varcom.nd.toInt()].set("Y");
				sv.riderOut[varcom.nd.toInt()].set(" ");
			}
		}
		else {
			sv.coverageOut[varcom.nd.toInt()].set("Y");
			sv.riderOut[varcom.nd.toInt()].set("Y");
			sv.lifeOut[varcom.nd.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5186", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	* </pre>
	*/
protected void readT6693Table2700()
	{
		readTable2710();
	}

protected void readTable2710()
	{
		/*  Read T6693 to check if the selected record has a valid action*/
	itempf=new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(t6693);
	itempf.setItemitem(wsaaT6693Key.toString());
	itempf.setItmfrm(wsaaMiscellaneousInner.wsaaEffdate.getbigdata());
	itempf.setItmto(wsaaMiscellaneousInner.wsaaEffdate.getbigdata());
	itempfList = itempfDAO.findByItemDates(itempf);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/* Update database files as required*/
		return ;
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		try {
			nextProgram4010();
			continue4080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void nextProgram4010()
	{
		/* If selection terminated clear program and action stacks.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			releaseSftlck4800();
			wsaaMiscellaneousInner.wsaaSub.set(1);
			wsspcomn.secActn[wsaaMiscellaneousInner.wsaaSub.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/* If not returning from a selection ACTION = ' ' then we must*/
		/*   store the next four Programs in the stack and load in the*/
		/*   generic programs according to Table T5671.*/
		/* If returning from a selection ACTION = '*' then sequentially*/
		/*   read down the subfile to find the next selected record.*/
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			initSelection4100();
		}
		else {
			scrnparams.function.set(varcom.srdn);
		}
		/* Read the Subfile until a Selection has been made.*/
		wsaaSelectFlag.set("N");
		wsaaImmexitFlag.set("N");
		while ( !(wsaaSelection.isTrue()
		|| wsaaImmExit.isTrue())) {
			next4200();
		}
		
		/* Set action flag to ' ' in order that the program will resume*/
		/* initial stack order and go to the next program.*/
		/* NOTE  With the limitations in subsequent transactions we have*/
		/* a problem with creation of poayments. They call AT which then*/
		/* fail because we still hold records. As a temp measure lets*/
		/* exit immediatly rather than display info to the user if in a*/
		/* create mode.*/
		
//		ILIFE-734 start-sgadkari
//		if (isNE(wsspcomn.flag, "C")) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				if (wsaaImmExit.isTrue()) {
					wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
					wsspcomn.nextprog.set(scrnparams.scrname);
					goTo(GotoLabel.exit4090);
				}
			}
//		}
//		ILIFE-734 end
		/* If the end of the subfile, exit to next initial program.*/
		/* Set action flag to ' ' in order that the program will resume*/
		/* initial stack order and go to the next program.*/
		if (wsaaImmExit.isTrue()) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(1);
			wsspcomn.nextprog.set(wsaaProg);
			goTo(GotoLabel.exit4090);
		}
	}

protected void continue4080()
	{
		/* Set up for execution of the component specific programs to be*/
		/* executed. Note that this includes a set of follow up records*/
		/* for each payment created according to table settings.*/
		setUpForComponents4300();
		/* Read the  Program  table T5671 for  the components programs*/
		/* to be executed next.*/
		programTables4400();
		/*UPDATE-SCREEN*/
		screenUpdate4600();
	}

protected void initSelection4100()
	{
		try {
			saveNextProgs4110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void saveNextProgs4110()
	{
		/* Save the  next  four  programs  after  P5186  into  Working*/
		/* Storage for re-instatement at the end of the Subfile.*/
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 4); loopVar1 += 1){
			loop14120();
		}
		/* First Read of the Subfile for a Selection.*/
		scrnparams.function.set(varcom.sstrt);
		/* Go to the first  record  in  the Subfile to  find the First*/
		/* Selection.*/
		goTo(GotoLabel.exit4190);
	}

protected void loop14120()
	{
		/* This loop will load the next four  programs from WSSP Stack*/
		/* into a Working Storage Save area.*/
		wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()].set(wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void next4200()
	{
		try {
			nextRec4210();
			ifSubfileEndp4220();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void nextRec4210()
	{
		/* Read next subfile record sequentially.*/
		processScreen("S5186", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void ifSubfileEndp4220()
	{
		/* Check for the end of the Subfile.*/
		/* If end of Subfile re-load the Saved four programs to Return*/
		/*   to initial stack order.*/
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			reloadProgsWssp4230();
			goTo(GotoLabel.exit4290);
		}
		/* Exit if a selection has been found. NOTE that we space*/
		/* and protect the selection as the subsequent transactions*/
		/* cannot handle multiple entry.*/
		if (isNE(sv.select, SPACES)) {
			sv.asterisk.set("*");
			sv.select.set(SPACES);
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaSelectFlag.set("Y");
		}
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.exit4290);
	}

protected void reloadProgsWssp4230()
	{
		/* Set Flag for immediate exit as the next action is to go to the*/
		/* next program in the stack.*/
		/* Set Subscripts and call Loop.*/
		wsaaImmexitFlag.set("Y");
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			loop34240();
		}
		goTo(GotoLabel.exit4290);
	}

protected void loop34240()
	{
		/* Re-load the Saved four programs from Working Storage to the*/
		/* WSSP Program Stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()].set(wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void setUpForComponents4300()
	{
		setupRegp4310();
	}

protected void setupRegp4310()
	{
		/* Do a KEEPS on the key of a REGP record if one does not already e*/
		/*  otherwise do a READS.*/
		regpIO.setNonKey(SPACES);
		regpIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		regpIO.setChdrnum(chdrrgpIO.getChdrnum());
		regpIO.setLife(sv.life);
		regpIO.setCoverage(sv.coverage);
		regpIO.setRider(sv.rider);
		regpIO.setRgpynum(ZERO);
		regpIO.setFormat(formatsInner.regprec);
		wsaaMiscellaneousInner.wsaaCrtable.set(sv.hcrtable);
		if (isEQ(sv.hrgpynum, SPACES)) {
			regpIO.setCrtable(sv.hcrtable);
			regpIO.setPlanSuffix(covrrgpIO.getPlanSuffix());
			regpIO.setPrcnt(ZERO);
			regpIO.setPymt(ZERO);
			regpIO.setTotamnt(ZERO);
			regpIO.setTranno(ZERO);
			regpIO.setCrtdate(ZERO);
			regpIO.setAprvdate(ZERO);
			regpIO.setFirstPaydate(ZERO);
			regpIO.setNextPaydate(ZERO);
			regpIO.setRevdte(ZERO);
			regpIO.setLastPaydate(ZERO);
			regpIO.setFinalPaydate(ZERO);
			regpIO.setAnvdate(ZERO);
			regpIO.setCancelDate(ZERO);
			regpIO.setValidflag("1");
			regpIO.setFunction(varcom.keeps);
			regpIO.setUser(varcom.vrcmUser);
			regpIO.setTransactionTime(varcom.vrcmTime);
			regpIO.setTransactionDate(wsaaMiscellaneousInner.wsaaEffdate);
			regpIO.setTermid(varcom.vrcmTermid);
		}
		else {
			regpIO.setRgpynum(sv.hrgpynum.toInt());
			regpIO.setFunction(varcom.reads);
		}
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* If in create mode we must be creating a payment. As such*/
		/* we create any default follow ups for the payment. The*/
		/* earlier validation and screen set up prevent existing*/
		/* payments being selected in the create mode.*/
		if (isEQ(wsspcomn.flag, "C")) {
			createFollowUps5000();
		}
	}

protected void programTables4400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case readProgramTable4410: 
					readProgramTable4410();
					loadProgsToWssp4420();
				case exit4490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readProgramTable4410()
	{
		/* Read the  Program  table T5671 for  the components programs to*/
		/* be executed next.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.addExpression(wsaaMiscellaneousInner.wsaaCrtable);
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),t5671, stringVariable1.toString());
		if (itempfList.size()==0) {
			if (isEQ(wsaaMiscellaneousInner.wsaaCrtable, "****")) {
				sv.selectErr.set(g433);
				wsspcomn.edterror.set("Y");
				wsspcomn.nextprog.set(scrnparams.scrname);
				planReload4500();
				wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
				goTo(GotoLabel.exit4490);
			}
			else {
				wsaaMiscellaneousInner.wsaaCrtable.set("****");
				goTo(GotoLabel.readProgramTable4410);
			}
		}
		else {
			t5671rec.t5671Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}

protected void loadProgsToWssp4420()
	{
		/* Move the component programs to the WSSP stack.*/
		wsaaMiscellaneousInner.wsaaSub1.set(1);
		compute(wsaaMiscellaneousInner.wsaaSub2, 0).set(add(1, wsspcomn.programPtr));
		for (int loopVar3 = 0; !(loopVar3 == 4); loopVar3 += 1){
			loop24430();
		}
		/* Reset the Action to '*' signifying action desired to the next*/
		/* program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		wsspcomn.nextprog.set(wsaaProg);
		goTo(GotoLabel.exit4490);
	}

protected void loop24430()
	{
		/* This loop will load four programs from table T5671 to the WSSP*/
		/* stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub2.toInt()].set(t5671rec.pgm[wsaaMiscellaneousInner.wsaaSub1.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void planReload4500()
	{
		try {
			reloadProgsWssp4510();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void reloadProgsWssp4510()
	{
		/* Set Flag for immediate exit as the next action is to go to the*/
		/* next program in the stack.*/
		/* Set Subscripts and call Loop.*/
		compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
		wsaaMiscellaneousInner.wsaaSub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 4); loopVar4 += 1){
			loop34520();
		}
		/* Set action flag to ' ' in order that the program will resume*/
		/* initial stack order and go to the next program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		goTo(GotoLabel.exit4590);
	}

protected void loop34520()
	{
		/* Re-load the Saved four programs from Working Storage to the*/
		/* WSSP Program Stack.*/
		wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()].set(wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()]);
		wsaaMiscellaneousInner.wsaaSub1.add(1);
		wsaaMiscellaneousInner.wsaaSub2.add(1);
	}

protected void screenUpdate4600()
	{
		/*PARA*/
		scrnparams.function.set(varcom.supd);
		processScreen("S5186", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void releaseSftlck4800()
	{
		unlockContract4810();
	}

protected void unlockContract4810()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		slckpf.setEnttyp("CH");
		slckpf.setCompany(wsspcomn.company.toString());
		slckpf.setEntity(chdrrgpIO.getChdrnum().toString());
		Slckpf result = slckpfDAO.readHSlckData(slckpf);
		if (result != null) {
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrrgpIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		}
	}

protected void createFollowUps5000()
	{
		try {
			chekIfRequired5010();
			readDefaultsTable5020();
			writeFollowUps5030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void chekIfRequired5010()
	{
	    itempf=new Itempf();
	    itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5688);
		itempf.setItemitem(chdrrgpIO.getCnttype().toString());
		itempf.setItmfrm(wsaaMiscellaneousInner.wsaaEffdate.getbigdata());
		itempf.setItmto(wsaaMiscellaneousInner.wsaaEffdate.getbigdata());
		itempfList = itempfDAO.findByItemDates(itempf);
		if (itempfList.size()==0) {
			syserrrec.statuz.set(e308);
			fatalError600();
		}
		t5688rec.t5688Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		if (isEQ(t5688rec.defFupMeth, SPACES)) {
			goTo(GotoLabel.exit5090);
		}
	}

protected void readDefaultsTable5020()
	{
		wsaaT5677Tranno.set(wsaaBatckey.batcBatctrcde);
		wsaaT5677FollowUp.set(t5688rec.defFupMeth);
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),t5677, wsaaT5677Key.toString());
		t5677rec.t5677Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}

protected void writeFollowUps5030()
	{
		wsaaMiscellaneousInner.wsaaFupno.set(0);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 16)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			writeFollowUp5300();
		}
	}

protected void writeFollowUp5300()
	{
		try {
			lookUpStatus5310();
			lookUpDescription5320();
			writeRecord5330();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void lookUpStatus5310()
	{
		/*    IF T5677-FUPCODE (WSAA-SUB)  = SPACES                        */
		if (isEQ(t5677rec.fupcdes[wsaaMiscellaneousInner.wsaaSub.toInt()], SPACES)) {
			goTo(GotoLabel.exit5390);
			/*    END-IF.*/
			/* UNREACHABLE CODE
			itemIO.setDataKey(SPACES);
			 */
		}
		/*    MOVE T5677-FUPCODE (WSAA-SUB)                                */
		/*                                TO ITEM-ITEMITEM.                */
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(t5677rec.fupcdes[wsaaMiscellaneousInner.wsaaSub.toInt()]);
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(),t5661, wsaaT5661Key.toString());
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}

protected void lookUpDescription5320()
	{
		
		/*    MOVE T5677-FUPCODE (WSAA-SUB)                                */
		/*                                TO DESC-DESCITEM.                */
		/*    MOVE T5677-FUPCDES (WSAA-SUB)                        <LA4314>*/
		/*                                TO DESC-DESCITEM.        <LA4314>*/
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(t5677rec.fupcdes[wsaaMiscellaneousInner.wsaaSub.toInt()]);
		descpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		
	}

protected void writeRecord5330()
	{
		
		fluppf.setChdrcoy(wsspcomn.company.charat(0));
		fluppf.setChdrnum(chdrrgpIO.getChdrnum().toString());
		/* Determine WHAT the highest claim number was on this contract*/
		/* then add 1 to it to get the next CLAMNUM. This needs only to*/
		/* be done for the first time through. Subsequent times will*/
		/* keep the same Claim Number.*/
		if (isEQ(wsaaMiscellaneousInner.wsaaFupno, 0)) {
			begnRegp5500();
		}
		fluppf.setClamNum(wsaaMiscellaneousInner.wsaaNextClamnum.toString());
		wsaaMiscellaneousInner.wsaaFupno.add(1);
		fluppf.setFupNo(wsaaMiscellaneousInner.wsaaFupno.toInt());
		fluppf.setTranNo(chdrrgpIO.getTranno().toInt());
		fluppf.setFupDt(datcon1rec.intDate.toInt());
		/*    MOVE T5677-FUPCODE (WSAA-SUB)                                */
		/*                                TO FLUPRGP-FUPCODE.              */
		fluppf.setFupCde(t5677rec.fupcdes[wsaaMiscellaneousInner.wsaaSub.toInt()].toString());
		fluppf.setFupTyp('P');
		fluppf.setFupSts(t5661rec.fupstat.charat(0));
		fluppf.setFupRmk(descpf.getLongdesc());
		/* MOVE '01'                   TO FLUPRGP-LIFE.         <LA1307>*/
		/* MOVE '00'                   TO FLUPRGP-JLIFE.        <LA1307>*/
		fluppf.setLife(sv.life.toString());
		fluppf.setjLife(sv.jlife.toString());
		fluppf.setTermId(varcom.vrcmTermid.toString());
		fluppf.setTrdt(varcom.vrcmDate.toInt());
		fluppf.setTrtm(varcom.vrcmTime.toInt());
		fluppf.setUserT(varcom.vrcmUser.toInt());
		fluppf.setEffDate(datcon1rec.intDate.toInt());
		fluppf.setCrtDate(datcon1rec.intDate.toInt());
		fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
		fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
		fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
		fluppfDAO.insertFlupRecord(fluppf);
	}

protected void begnRegp5500()
	{
		wsaaMiscellaneousInner.wsaaNextClamnum.set(ZERO);
		regpList=regppfDAO.readRegpRecord(chdrrgpIO.getChdrcoy().toString(), chdrrgpIO.getChdrnum().toString());
		for (Regppf regppf:regpList) {
			if (isGT(regppf.getRgpynum(), wsaaMiscellaneousInner.wsaaNextClamnum)) {
				wsaaMiscellaneousInner.wsaaNextClamnum.set(regppf.getRgpynum());
			}
		}
		compute(wsaaMiscellaneousInner.wsaaNextClamnum, 0).set(add(wsaaMiscellaneousInner.wsaaNextClamnum, 1));
	}

/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 

		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaaPlanPolicyFlag = new FixedLengthStringData(1);
	private Validator planLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "L");
	private Validator policyLevelEnquiry = new Validator(wsaaPlanPolicyFlag, "O");
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaNextClamnum = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaFupno = new PackedDecimalData(2, 0);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaSelected = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData regprec = new FixedLengthStringData(10).init("REGPREC");
}

protected S5186ScreenVars getLScreenVars() {
	return ScreenProgram.getScreenVars(S5186ScreenVars.class);
}

public StringUtil getStringUtil() {
	return stringUtil;
}
}
