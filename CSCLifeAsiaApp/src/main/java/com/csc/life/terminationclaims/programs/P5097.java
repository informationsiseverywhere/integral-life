/*
 * File: P5097.java
 * Date: 30 August 2009 0:06:36
 * Author: Quipoz Limited
 * 
 * Class transformed from P5097.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.terminationclaims.dataaccess.PuptTableDAM;
import com.csc.life.terminationclaims.screens.S5097ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6648rec;
import com.csc.life.terminationclaims.tablestructures.T6651rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*  P5097 - Traditional Paid Up Processing Selection    (LTRBPUP)
*  ------------------------------------
*
*  This function will be used to perform Paid Up processing  on
*  a component within a contract.
*
*  The  user  may  have  selected to 'pay-up' either individual
*  policies within the plan or the whole plan.  After this  the
*  actual  component  will be selected.  If individual policies
*  are being paid-up then this screen will be invoked once  for
*  each selected component.
*
*
*  Initialise
*  ----------
*
*  Skip this section if returning from a program  further  down
*  the stack, (current stack position action flag = '*').
*
*  Perform  a RETRV on the CHDRMJA I/O module. This will return
*  the CHDRMJA record on the selected contract number.
*
*  Perform  a READR on the PAYR I/O module to retrieve the
*  billing information which are not longer in the contract
*  header file.
*
*  Perform  a RETRV on the COVRMJA I/O module. This will return
*  the COVRMJA record on the selected policy.
*
*  If COVRMJA-PLAN-SUFFIX = 0000 then the whole plan  is  being
*  processed  and every COVRMJA record whose key matches on the
*  passed Company, Contract Number, Life,  Coverage  and  Rider
*  will be processed but only the first coverage is displayed on
*  the screen.
*
*  If  the COVRMJA-PLAN-SUFFIX is not greater than the value of
*  CHDRMJA-POLSUM then a 'NOTIONAL' policy is being  processed,
*  i.e. a  policy  has been selected that is at present part of
*  the summarised portion of the contract. If this is the  case
*  then  the COVRMJA summary record will have been saved by the
*  KEEPS command and the values from here  will  be  displayed.
*  The  details  of the contract being processed will be stored
*  in the CHDRMJA I/O  module.    Retrieve  the  details  using
*  RETRV.
*
*  Set  up  the header portion of the screen with the following
*  fields, descriptions and names:
*
*    -  Contract number (CHDRMJA-CHDRNUM)
*    -  Contract  Type  (CHDRMJA-CNTTYPE)  -  long  description
*       from T5688
*    -  Contract  Status (CHDRMJA-STATCODE) - short description
*       from T3623
*    -  Premium      Status   (CHDRMJA-PSTATCODE)    -    short
*       description from T3588
*    -  Paid to Date (CHDRMJA-PTDATE)
*    -  Billed to Date (CHDRMJA-BTDATE)
*    -  Billing Frequency (CHDRMJA-BILLFREQ)
*    -  The owner's client (CLTS) details.
*    -  The first Life Assured details from the LIFE dataset.
*    -  The  Joint  Life  Assured details from the LIFE dataset
*       if they exist.
*    -  Existing Sum Assured (COVRMJA-SUMINS)
*    -  New Sum Assured - Subroutine from table T6598
*    -  Component Risk Status (COVRMJA-STATCODE)
*    -  Component Premium Status (COVRMJA-PSTATCODE)
*    -  Paid Up Method - from table T5687
*       All  the  client  numbers  should  have   their   names
*       displayed  using  CONFNAME.    Obtain  the confirmation
*       names.
*
*  Validation
*  ----------
*
*  Read  the PUPT file, checking for an existing record for the
*  component selected.  If so, proceed to the  'Field  Display'
*  section, displaying the following message:
*   'PUP Already Requested'.
*
*  Check   that   the  coverage/rider  exists  on  the  General
*  Coverage/Rider Details Table:-
*       Read T5687  for  the  coverage/rider  (COVRMJA-CRTABLE)
*       selected.      If  no  table  item  exists,  abort  the
*       program.  If T5687-PUMETH is equal to spaces, issue  an
*       error and disallow pay-up.
*
*  Check  that  the  component  has  been in force at least the
*  number of months specified on the Paid Up  Validation  Rules
*  table:-
*       Read  T6648 using T5687-PUMETH||CHDRMJA-CNTCURR.  If no
*       table item exists, abort the program.    Otherwise  use
*       DATCON3  to  calculate  the  number  of  months between
*       COVRMJA-CRRCD and  CHDRMJA-BTDATE;  if  the  number  of
*       months  calculated is less than the number of months on
*       the  table  (T6648-MINMTHIF)  for   the   corresponding
*       billing  frequency  (CHDRMJA-BILLFREQ)  entry, issue an
*       error and disallow pay-up.
*
*  Check that the sum assured is greater than or equal  to  the
*  minimum  sum assured  required  on  the  Paid  Up Validation
*  Rules table:-
*       Use T6648.
*       If the sum assured is  less  than  that  on  the  table
*       (T6648-MINISUMA)    for   the   corresponding   billing
*       frequency (CHDRMJA-BILLFREQ) entry, issue an error  and
*       disallow pay-up.
*
*  Check  that  the  remaining policy term for the component is
*  at least the number of  months  specified  on  the  Paid  Up
*  Validation Rules table:-
*       Use T6648.
*       Perform  DATCON3  to  calculate  the  number  of months
*       between CHDRMJA-PTDATE and COVRMJA-RISK-CESS-DATE.   If
*       the  number  of  months  calculated  is  less  than the
*       number of months on the table (T6648-MINTERM)  for  the
*       corresponding   billing   frequency  (CHDRMJA-BILLFREQ)
*       entry, issue an error and disallow pay-up.
*
*  Check that the coverage/rider  status  matches  one  of  the
*  pairs on the Component Status Table:-
*       Read  T5679 using the program's transaction code as the
*       item key.
*       If COVRMJA-RIDER = '00',  check  against  the  coverage
*       status, else check against the rider status.
*       Use    the    fields    T5679-RID(COV)-PREM-STAT    and
*       T5679-RID(COV)-RISK-STAT against COVRMJA-PSTATCODE  and
*       COVRMJA-STATCODE.
*       If  the statuses do  not  match one of the pairs on the
*       table entry, issue an error and disallow pay-up.
*
*  Check that the new sum-assured is greater than or equal to
*  the minimum sum-assured required on the  Paid Up Validation
*  Rules table:-
*       Use T6648.
*       If the new sum assured is less than that on the table
*       (T6648-MINSUMA)    for   the   corresponding   billing
*       frequency (CHDRMJA-BILLFREQ) entry, issue an error  and
*       disallow pay-up.
*
*
*  The component instalment premium  (COVRMJA-INSTPREM)  cannot
*  equal zero; if it does, issue an error and disallow pay-up.
*
*  Read  T6651  using  a  key  of  paid  up  processing  method
*  concatenated with CNTCURR, giving  the  paid  up  processing
*  rules for the item.
*
*  Check that new sum assured field is allowed to be changed,
*  from T6651-OVRSUMA field. If not allowed to change, then
*  protect the field else unprotect it to allow changes.
*
*
*  Read  T6598  using  PUPT-PUMETH  to  find  the   Calculation
*  Subroutine Name.
*  If the  Paid  Up  Processing Calculation Subroutine is not
*  spaces, then call the subroutine with the following linkage
*  fields:
*       Company - CHDRCOY
*       Contract Number - CHDRNUM
*       Life - LIFE
*       Coverage - COVERAGE
*       Rider - RIDER
*       Coverage/Rider Type - COVRMJA-CRTABLE
*       Paid Up Method - T5687-PUMETH
*       Contract Currency - CHDRMJA-CNTCURR
*       Billing Frequency - CHDRMJA-BILLFREQ
*       Instalment Premium - COVRMJA-INSTPREM
*       Sum Assured - COVRMJA-SUMINS
*       Billed to Date - CHDRMJA-BTDATE
*       Coverage/Rider Risk Commencement Date - COVRMJA-CRRCD
*       Premium Cessation Date - COVRMJA-PREM-CESS-DATE
*       New Sum Assured - set to zeros (will be returned)
*
*  Field Display
*  -------------
*  Display the following component/rider level fields:
*       Existing  Sum  Assured  (COVRMJA-SUMINS)  - if the plan
*       suffix is  zeros,  the  sum-assured  will  need  to  be
*       divided  by the number of policies currently summarised
*       (CHDRMJA-POLSUM)
*       Risk Commencement Date (COVRMJA-CRRCD)
*       Paid  Up  Method (T5687-PUMETH) - long description from
*       T6598
*       Component  Risk  Status   (COVRMJA-STATCODE)   -   long
*       description from T5682
*       Component  Premium  Status  (COVRMJA-PSTATCODE)  - long
*       description from T5681
*
*  Screen Display
*  --------------
*
*  Skip this section if returning from a program  further  down
*  the stack, (current stack position action flag = '*').
*
*
*  If 'KILL' is requested, exit this section.
*
*  Display the screen, with all fields protected except new
*  sum assured depending on T6651.
*
*  Note:
*  If the new sum assured is unprotected, validate that the new
*  sum assured entered is greater than on table T6648-MINSUMA
*  for the corresponding billing frequency.
*
*  Updating
*  --------
*  Skip  this  section if returning from a program further down
*  the stack (current stack position action  flag  =  '*'),  or
*  'KILL' was requested.
*
*  Write  one  PUPT  record  for  the  component to be paid up,
*  using the WRITR function.
*
*  Set the following fields on the PUPT:
*
*  CHDRCOY   -|
*  CHDRNUM    |
*  LIFE       |
*  COVERAGE   |---key from COVRMJA record
*  RIDER      |
*  PLNSFX    -|
*  Paid Up Processing Method - T5687-PUMETH
*  New Sum Assured
*  Termid
*  Transaction Date
*  Transaction Time
*  User
*
*
*  Next Program
*  ------------
*  If 'KILL' was requested move spaces  to  the  current  stack
*  action  and  program  fields and do not add 1 to the program
*  pointer.  Then exit.
*
*  If the current stack action field is '*'  then  re-load  the
*  next  8  positions in the program stack from the saved area,
*  and move spaces to WSSP-SEC-ACTION(current position).
*
*  If processing the whole Plan, (COVRMJA-PLAN-SUFFIX =  0000),
*  then  the  program will read the next COVRMJA record for the
*  matching Company, Contract Number, Life, Coverage and  Rider
*  and  loop back around to the 2000 section and re-display the
*  details from the next COVRMJA record as they were first  set
*  up  by  the  first  pass through the 1000 section. This will
*  continue until all the matching COVRMJA  records  have  been
*  processed.
*
*  Add 1 to the current program pointer and exit.
*
*
*  Tables Used
*  -----------
*
*  T3623 - Contract Risk Status           Key: Risk Code
*  T3588 - Contract Premium Status        Key: Premium Code
*  T5679 - Component Status               Key: Transaction Code
*  T5681 - Component Premium Status       Key: Premium Code
*  T5682 - Component Risk Status          Key: Risk Code
*  T5687 - General Coverage/Rider Details Key: Coverage/Rider
*                                              Code
*  T5688 - Contract Structure             Key: Contract Type
*  T6598 - Paid Up Calculation Method     Key: Paid Up Method
*  T6648 - Paid Up Validation Rules       Key: Paid Up
*                                              Method||Currency
*  T6651 - Paid Up Processing Rules       Key: Paid Up
*                                              Method||Currency
*
*  T6648 - Paid Up Processing Validation
*  -------------------------------------
*  A  new  table  to  hold  parameter  values used to determine
*  whether the component is able to be paid up.
*
*  The table will  be  keyed  on  paid  up  calculation  method
*  concatenated with currency.
*
*  The table will be a dated table.
*
*  Parameters   are   as   follows,   in  a  matrix  with  five
*  occurrences by billing frequency:
*       - minimum number of months in force
*            MINMTHIF       PIC 9(3)
*       - minimum sum assured amount
*            MINISUMA       PIC S9(11)V99
*       - minimum remaining policy term (in months)
*            MINTERM        PIC 9(4)
*       - minimum paid up sum assured amount
*            MINPUSA        PIC S9(11)V99
*       - minimum paid up cash option amount
*            MINIPUP        PIC S9(11)V99
*
*
*
*****************************************************************
* </pre>
*/
public class P5097 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5097");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaNoPolicy = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaMinpusaTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMinisumaTot = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaShortdesc = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLongconfname = new FixedLengthStringData(48);
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaDesctabl = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaDescitem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaErrorIndicators = new FixedLengthStringData(128);

	private FixedLengthStringData wsaaValidRisk = new FixedLengthStringData(1).init("Y");
	private Validator validRisk = new Validator(wsaaValidRisk, "Y");
	private Validator invalidRisk = new Validator(wsaaValidRisk, "N");

	private FixedLengthStringData wsaaValidPremium = new FixedLengthStringData(1).init("Y");
	private Validator validPremium = new Validator(wsaaValidPremium, "Y");
	private Validator invalidPremium = new Validator(wsaaValidPremium, "N");

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, 1);
	private Validator partPlan = new Validator(wsaaPlanSwitch, 2);
	private Validator summaryPartPlan = new Validator(wsaaPlanSwitch, 3);

	private FixedLengthStringData wsaaKey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5687Pumeth = new FixedLengthStringData(4).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaaChdrCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaKey, 4);
		/* WSAA-COVR-KEY */
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaPaidupProcessed = new FixedLengthStringData(1).init(" ");
	private Validator pendingPaidup = new Validator(wsaaPaidupProcessed, "1");
	private Validator alreadyPaidup = new Validator(wsaaPaidupProcessed, "2");
		/* FORMATS */
	private static final String itemrec = "ITEMREC   ";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String descrec = "DESCREC   ";
	private static final String payrrec = "PAYRREC   ";
	private static final String lifemjarec = "LIFEMJAREC";
	private static final String cltsrec = "CLTSREC   ";
	private static final String puptrec = "PUPTREC   ";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PuptTableDAM puptIO = new PuptTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T6648rec t6648rec = new T6648rec();
	private T6651rec t6651rec = new T6651rec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5097ScreenVars sv = ScreenProgram.getScreenVars( S5097ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private TablesInner tablesInner = new TablesInner();
	//ILIFE-8137 
    private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpfList;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue1004, 
		continue1008, 
		checkExistingPup1009, 
		chkBillfreq1010, 
		storeIndicators1017, 
		exit1019, 
		exit1509, 
		exit1559, 
		loopRid1614, 
		loopCov1615, 
		exit1619, 
		loopRid1622, 
		loopCov1625, 
		exit1629
	}

	public P5097() {
		super();
		screenVars = sv;
		new ScreenModel("S5097", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					init1000();
					setScreen1001();
				case continue1004: 
					continue1004();
					validateRiskPremium1005();
					validatePremium1007();
				case continue1008: 
					continue1008();
				case checkExistingPup1009: 
					checkExistingPup1009();
				case chkBillfreq1010: 
					chkBillfreq1010();
					validateNewSumAssured1012();
				case storeIndicators1017: 
					storeIndicators1017();
				case exit1019: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init1000()
	{
		wsaaBatckey.batcKey.set(wsspcomn.batchkey);
		/* Skip this section if returning from a program further down the*/
		/* stack, (current stack position action flag = '*')*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1019);
		}
		if (isNE(wsaaBatckey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatckey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		sv.dataArea.set(SPACES);
		sv.btdate.set(ZERO);
		sv.ptdate.set(ZERO);
		sv.sumins.set(ZERO);
		sv.crrcd.set(ZERO);
		sv.numpols.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.newinsval.set(ZERO);
		sv.hnewinsval.set(ZERO);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		wsaaPaidupProcessed.set(SPACES);
		/* Retrieve Contract Header details which have been stored in*/
		/* the CHDRMJA I/O MODULE and load the corresponding screen fields*/
		//ILIFE-8137
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		/* Read the PAYR file to retrieve the Billing information*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrpf.getChdrcoy());
		payrIO.setChdrnum(chdrpf.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* Retrieve Contract Header details which have been stored in*/
		/* the COVRMJA I/O MODULE and load the corresponding screen fields*/
		//ILIFE-8137
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
				else {
					covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		/*covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		/* If COVRMJA-PLAN-SUFFIX is zeroes then the whole plan is to be*/
		/* processed*/
		wsaaNoPolicy.set(1);
		if (isEQ(covrpf.getPlanSuffix(), ZERO)) {
			wsaaPlanSwitch.set(1);
			wsaaNoPolicy.set(chdrpf.getPolinc());
		}
		else {
			if (isGT(covrpf.getPlanSuffix(), chdrpf.getPolsum())
			|| isEQ(chdrpf.getPolsum(), 1)) {
				wsaaPlanSwitch.set(2);
			}
			else {
				wsaaPlanSwitch.set(3);
			}
		}
	}

protected void setScreen1001()
	{
		/*    Obtain the Contract Type description from T5688.*/
		wsaaDesctabl.set(tablesInner.t5688);
		wsaaDescitem.set(chdrpf.getCnttype());
		getDescription1900();
		sv.ctypedes.set(wsaaLongdesc);
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cntcurr.set(chdrpf.getCntcurr());
		/*    Obtain the Contract Status description from T3623.*/
		wsaaDesctabl.set(tablesInner.t3623);
		wsaaDescitem.set(chdrpf.getStatcode());
		getDescription1900();
		sv.chdrstatus.set(wsaaShortdesc);
		/*    Obtain the Premuim Status description from T3588.*/
		wsaaDesctabl.set(tablesInner.t3588);
		wsaaDescitem.set(chdrpf.getPstcde());
		getDescription1900();
		sv.premstatus.set(wsaaShortdesc);
		sv.register.set(chdrpf.getReg());
		/* Load the First-Life details to screen*/
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		wsaaClntnum.set(lifemjaIO.getLifcnum());
		getClientName1800();
		sv.lifename.set(wsaaLongconfname);
		sv.lifenum.set(lifemjaIO.getLifcnum());
		/* Check for existance of JOINT-LIFE Details.*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(SPACES);
			sv.jlifename.set(SPACES);
			goTo(GotoLabel.continue1004);
		}
		wsaaClntnum.set(lifemjaIO.getLifcnum());
		getClientName1800();
		sv.jlifename.set(wsaaLongconfname);
		sv.jlife.set(lifemjaIO.getLifcnum());
	}

protected void continue1004()
	{
		/* Get Coverage description  from T5687*/
		wsaaDesctabl.set(tablesInner.t5687);
		wsaaDescitem.set(covrpf.getCrtable());
		getDescription1900();
		sv.crtabdesc.set(wsaaLongdesc);
		sv.crtable.set(covrpf.getCrtable());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.btdate.set(chdrpf.getBtdate());
		/* Get Billing Frequency description from T3590*/
		wsaaDesctabl.set(tablesInner.t3590);
		wsaaDescitem.set(chdrpf.getBillfreq());
		getDescription1900();
		sv.bilfrqdesc.set(wsaaLongdesc);
		sv.billfreq.set(chdrpf.getBillfreq());
		sv.crrcd.set(covrpf.getCrrcd());
		sv.planSuffix.set(covrpf.getPlanSuffix());
		sv.numpols.set(chdrpf.getPolinc());
		sv.life.set(covrpf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		/* Read table T5687 to get the Paid Up calculation method.*/
		readTableT56871700();
		if (isEQ(wsspcomn.edterror, "Y")) {
			goTo(GotoLabel.storeIndicators1017);
		}
		/* Display Paid-up method and get the*/
		/*     Paid-up Method Description from T5698*/
		wsaaDesctabl.set(tablesInner.t6598);
		wsaaDescitem.set(t5687rec.pumeth);
		getDescription1900();
		sv.pumethdesc.set(wsaaLongdesc);
		sv.pumeth.set(t5687rec.pumeth);
	}

	/**
	* <pre>
	* Display Risk-status and get the
	*     Component Risk-status-Desc from T5682
	* </pre>
	*/
protected void validateRiskPremium1005()
	{
		readTableT56791600();
		if (isEQ(sv.coverageErr, "Y")) {
			goTo(GotoLabel.checkExistingPup1009);
		}
		validateRiskCovrRidr1610();
		if (invalidRisk.isTrue()) {
			sv.statcodeErr.set(errorsInner.i028);
			wsspcomn.edterror.set("Y");
		}
		wsaaDesctabl.set(tablesInner.t5682);
		wsaaDescitem.set(covrpf.getStatcode());
		getDescription1900();
		sv.statdesc.set(wsaaLongdesc);
		sv.statcode.set(covrpf.getStatcode());
	}

protected void validatePremium1007()
	{
		validatePremCovrRidr1620();
		if (invalidPremium.isTrue()) {
			sv.pstatcodeErr.set(errorsInner.i028);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.continue1008);
		}
		/* Display the Prem-status and get the*/
		/*     Component PREM-STATUS-DESC from T5681*/
		wsaaDesctabl.set(tablesInner.t5681);
		wsaaDescitem.set(covrpf.getPstatcode());
		getDescription1900();
		sv.pstatdesc.set(wsaaLongdesc);
		sv.pstatcode.set(covrpf.getPstatcode());
	}

protected void continue1008()
	{
		if ((isNE(t5679rec.setCnPremStat, SPACES)
		&& isEQ(t5679rec.setCnPremStat, covrpf.getPstatcode()))) {
			if ((isNE(t5679rec.setCnRiskStat, SPACES)
			&& isEQ(t5679rec.setCnRiskStat, covrpf.getStatcode()))) {
				wsaaPaidupProcessed.set("2");
				goTo(GotoLabel.chkBillfreq1010);
			}
		}
	}

protected void checkExistingPup1009()
	{
		checkExistingPup1400();
		if (isEQ(puptIO.getStatuz(), varcom.oK)) {
			sv.sumins.set(covrpf.getSumins());
			sv.newinsval.set(puptIO.getNewsuma());
			sv.hnewinsval.set(puptIO.getNewsuma());
			goTo(GotoLabel.storeIndicators1017);
		}
		/* Single premium contract is not allowed to PUP.*/
		if (isEQ(chdrpf.getBillfreq(), "00")) {
			sv.billfreqErr.set(errorsInner.h014);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.storeIndicators1017);
		}
	}

protected void chkBillfreq1010()
	{
		if (isEQ(chdrpf.getBillfreq(), "01")) {
			wsaaIndex.set(2);
		}
		else {
			if (isEQ(chdrpf.getBillfreq(), "02")) {
				wsaaIndex.set(3);
			}
			else {
				if (isEQ(chdrpf.getBillfreq(), "04")) {
					wsaaIndex.set(4);
				}
				else {
					wsaaIndex.set(5);
				}
			}
		}
		/*VALIDATE-SUM-ASSURED*/
		validateSumAssured1500();
		if (isEQ(wsspcomn.edterror, "Y")) {
			goTo(GotoLabel.storeIndicators1017);
		}
	}

protected void validateNewSumAssured1012()
	{
		calculateNewSumAssured1550();
		if (isEQ(wsspcomn.edterror, "Y")) {
			goTo(GotoLabel.storeIndicators1017);
		}
		/*CHK-OVR-NEW-SUM-ASSURED*/
		chkOvrNewSumAssured1300();
	}

	/**
	* <pre>
	* Store the original errors in a working storage so in section
	* 2000-... we can redisplay the error messages, if there is
	* any. NOTE: if new field is added to screen S5097 then
	* WSAA-ERROR-INDICATORS should increase accordingly.kd
	* </pre>
	*/
protected void storeIndicators1017()
	{
		wsaaErrorIndicators.set(sv.errorIndicators);
	}

	/**
	* <pre>
	* Get the Paid-Up processing rules from T6651
	* </pre>
	*/
protected void chkOvrNewSumAssured1300()
	{
		readTableT66511300();
	}

protected void readTableT66511300()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		wsaaT5687Pumeth.set(t5687rec.pumeth);
		wsaaChdrCntcurr.set(chdrpf.getCntcurr());
		itdmIO.setItemitem(wsaaKey);
		itdmIO.setItemtabl(tablesInner.t6651);
		itdmIO.setFormat(itemrec);
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6651)
		|| isNE(itdmIO.getItemitem(), wsaaKey)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sv.chdrnumErr.set(errorsInner.h020);
			wsspcomn.edterror.set("Y");
			sv.newinsvalOut[varcom.pr.toInt()].set("Y");
			return ;
		}
		t6651rec.t6651Rec.set(itdmIO.getGenarea());
		if (isNE(t6651rec.ovrsuma, "Y")) {
			sv.newinsvalOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkExistingPup1400()
	{
		readPupt1400();
	}

protected void readPupt1400()
	{
		puptIO.setChdrcoy(covrpf.getChdrcoy());
		puptIO.setChdrnum(covrpf.getChdrnum());
		puptIO.setLife(covrpf.getLife());
		puptIO.setCoverage(covrpf.getCoverage());
		puptIO.setRider(covrpf.getRider());
		puptIO.setPlanSuffix(covrpf.getPlanSuffix());
		puptIO.setFormat(puptrec);
		puptIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, puptIO);
		if (isEQ(puptIO.getStatuz(), varcom.oK)) {
			wsaaPaidupProcessed.set("1");
		}
	}

protected void validateSumAssured1500()
	{
		try {
			readT66481500();
			validateMinAmount1502();
			validateMinTerm1504();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	* Read table T6648 tp get the Paid-Up Component values.
	* </pre>
	*/
protected void readT66481500()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		wsaaT5687Pumeth.set(t5687rec.pumeth);
		wsaaChdrCntcurr.set(chdrpf.getCntcurr());
		itdmIO.setItemitem(wsaaKey);
		itdmIO.setItemtabl(tablesInner.t6648);
		itdmIO.setFormat(itemrec);
		itdmIO.setItmfrm(chdrpf.getPtdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6648)
		|| isNE(itdmIO.getItemitem(), wsaaKey)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sv.chdrnumErr.set(errorsInner.g203);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit1509);
		}
		t6648rec.t6648Rec.set(itdmIO.getGenarea());
	}

	/**
	* <pre>
	* Check that the Component has been in-force at least the number
	* of months specified on T6648. If the number of months between
	* COVRMJA-CRRD and PAYR-BTDATE is less then T6648-MINMYHIF
	* for the corresponding CHDRMJA-BILLFREQ pay-up is disallowed.
	* </pre>
	*/
protected void validateMinAmount1502()
	{
		/* Check that Sum-Assured is >= Min-Sum-Assured on T6648*/
		compute(wsaaMinisumaTot, 3).setRounded(mult(t6648rec.minisuma[wsaaIndex.toInt()], wsaaNoPolicy));
		if (isLT(covrpf.getSumins(), wsaaMinisumaTot)) {
			sv.suminsErr.set(errorsInner.h011);
			wsspcomn.edterror.set("Y");
		}
		/*  Check that frequency factor between risk commencement date*/
		/*  and bill to date >= minimum months in force in T6648.*/
		datcon3rec.function.set("CMDF");
		datcon3rec.frequency.set("12");
		datcon3rec.intDate1.set(covrpf.getCrrcd());
		datcon3rec.intDate2.set(payrIO.getBtdate());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		if (isLT(datcon3rec.freqFactor, t6648rec.minmthif[wsaaIndex.toInt()])) {
			sv.crrcdErr.set(errorsInner.h010);
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	* Check that the remaining policy term is at least as long as
	* specified on T6648
	* </pre>
	*/
protected void validateMinTerm1504()
	{
		datcon3rec.function.set("CMDF");
		datcon3rec.frequency.set("12");
		datcon3rec.intDate1.set(payrIO.getPtdate());
		datcon3rec.intDate2.set(covrpf.getRiskCessDate());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		if (isLT(datcon3rec.freqFactor, t6648rec.minterm[wsaaIndex.toInt()])) {
			sv.ptdateErr.set(errorsInner.h012);
			wsspcomn.edterror.set("Y");
		}
		if (wholePlan.isTrue()
		|| partPlan.isTrue()) {
			sv.sumins.set(covrpf.getSumins());
		}
		else {
			compute(sv.sumins, 2).set(div(covrpf.getSumins(), chdrpf.getPolsum()));
		}
		zrdecplrec.amountIn.set(sv.sumins);
		callRounding5000();
		sv.sumins.set(zrdecplrec.amountOut);
	}

	/**
	* <pre>
	* Read T6598 to get the Calculation subroutine name
	* </pre>
	*/
protected void calculateNewSumAssured1550()
	{
		try {
			readT65981550();
			initializePara1552();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readT65981550()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemitem(t5687rec.pumeth);
		itemIO.setItemtabl(tablesInner.t6598);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.pumethErr.set(errorsInner.h021);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit1559);
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		if (isEQ(t6598rec.calcprog, SPACES)) {
			sv.pumethErr.set(errorsInner.h021);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit1559);
		}
	}

protected void initializePara1552()
	{
		ovrduerec.ovrdueRec.set(SPACES);
		ovrduerec.description.set(SPACES);
		ovrduerec.crrcd.set(ZERO);
		ovrduerec.ptdate.set(ZERO);
		ovrduerec.polsum.set(ZERO);
		ovrduerec.actualVal.set(ZERO);
		ovrduerec.chdrcoy.set(covrpf.getChdrcoy());
		ovrduerec.chdrnum.set(covrpf.getChdrnum());
		ovrduerec.life.set(covrpf.getLife());
		ovrduerec.coverage.set(covrpf.getCoverage());
		ovrduerec.rider.set(covrpf.getRider());
		ovrduerec.planSuffix.set(covrpf.getPlanSuffix());
		ovrduerec.polsum.set(chdrpf.getPolsum());
		ovrduerec.crtable.set(covrpf.getCrtable());
		ovrduerec.language.set(wsspcomn.language);
		ovrduerec.effdate.set(wsspcomn.currfrom);
		ovrduerec.runDate.set(wsaaToday);
		ovrduerec.acctyear.set(wsaaBatckey.batcBatcactyr);
		ovrduerec.acctmonth.set(wsaaBatckey.batcBatcactmn);
		ovrduerec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ovrduerec.trancode.set(wsaaBatckey.batcBatctrcde);
		ovrduerec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		ovrduerec.cnttype.set(chdrpf.getCnttype());
		ovrduerec.tranno.set(chdrpf.getTranno());
		ovrduerec.billfreq.set(payrIO.getBillfreq());
		ovrduerec.cntcurr.set(payrIO.getCntcurr());
		ovrduerec.ptdate.set(payrIO.getPtdate());
		ovrduerec.user.set(payrIO.getUser());
		ovrduerec.crrcd.set(covrpf.getCrrcd());
		ovrduerec.pstatcode.set(covrpf.getPstatcode());
		ovrduerec.riskCessDate.set(covrpf.getRiskCessDate());
		ovrduerec.tranTime.set(varcom.vrcmTime);
		ovrduerec.tranDate.set(varcom.vrcmDate);
		ovrduerec.termid.set(varcom.vrcmTermid);
		ovrduerec.statuz.set(varcom.oK);
		callProgram(t6598rec.calcprog, ovrduerec.ovrdueRec);
		if (isEQ(ovrduerec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(ovrduerec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(ovrduerec.actualVal);
		callRounding5000();
		ovrduerec.actualVal.set(zrdecplrec.amountOut);
		sv.newinsval.set(ovrduerec.actualVal);
		sv.hnewinsval.set(ovrduerec.actualVal);
		/* MOVE SPACES                 TO SURC-SURRENDER-REC.           */
		/* MOVE ZEROES                 TO SURC-CRRCD                    */
		/*                                SURC-PTDATE                   */
		/*                                SURC-CONV-UNITS               */
		/*                                SURC-ESTIMATED-VAL            */
		/*                                SURC-ACTUAL-VAL               */
		/*                                SURC-SINGP.                   */
		/* MOVE COVRMJA-CHDRCOY        TO SURC-CHDR-CHDRCOY.            */
		/* MOVE COVRMJA-CHDRNUM        TO SURC-CHDR-CHDRNUM.            */
		/* MOVE COVRMJA-LIFE           TO SURC-LIFE-LIFE.               */
		/* MOVE COVRMJA-COVERAGE       TO SURC-COVR-COVERAGE.           */
		/* MOVE COVRMJA-RIDER          TO SURC-COVR-RIDER.              */
		/* MOVE COVRMJA-PLAN-SUFFIX    TO SURC-PLAN-SUFFIX.             */
		/* MOVE CHDRMJA-POLSUM         TO SURC-POLSUM.                  */
		/* MOVE COVRMJA-CRTABLE        TO SURC-CRTABLE.                 */
		/* MOVE WSSP-LANGUAGE          TO SURC-LANGUAGE.                */
		/* MOVE WSSP-CURRFROM          TO SURC-EFFDATE.                 */
		/* MOVE O-K                    TO SURC-STATUS.                  */
		/* CALL T6598-CALCPROG         USING SURC-SURRENDER-REC.        */
		/* IF SURC-STATUS              = BOMB                           */
		/*     MOVE SURC-STATUS        TO SYSR-STATUZ                   */
		/*     PERFORM 600-FATAL-ERROR.                                 */
		/* MOVE SURC-ACTUAL-VAL        TO S5097-NEWINSVAL               */
		/*                                S5097-HNEWINSVAL.             */
		validateNewSumAssured1570();
	}

protected void validateNewSumAssured1570()
	{
		/*VALIDATE-MIN-PUP-AMOUNT*/
		/* Check that New Sum-Assured is >= Minimum Paid Up Sum Assured*/
		/* on T6648*/
		compute(wsaaMinpusaTot, 3).setRounded(mult(t6648rec.minpusa[wsaaIndex.toInt()], wsaaNoPolicy));
		if (isLT(sv.newinsval, wsaaMinpusaTot)) {
			sv.newinsvalErr.set(errorsInner.t023);
			wsspcomn.edterror.set("Y");
		}
		else {
			sv.newinsvalErr.set(SPACES);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Read table T5679 to get the status codes.
	* </pre>
	*/
protected void readTableT56791600()
	{
		readT56791600();
	}

protected void readT56791600()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.coverageErr.set(errorsInner.f050);
			wsspcomn.edterror.set("Y");
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
	}

protected void validateRiskCovrRidr1610()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initial1610();
				case loopRid1614: 
					loopRid1614();
				case loopCov1615: 
					loopCov1615();
				case exit1619: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* Validate the risk status
	* </pre>
	*/
protected void initial1610()
	{
		wsaaIndex.set(ZERO);
		if (isEQ(covrpf.getRider(), "00")) {
			goTo(GotoLabel.loopCov1615);
		}
	}

	/**
	* <pre>
	* Check the rider level
	* </pre>
	*/
protected void loopRid1614()
	{
		wsaaIndex.add(1);
		if (isGT(wsaaIndex, 12)) {
			wsaaValidRisk.set("N");
			goTo(GotoLabel.exit1619);
		}
		if (isEQ(covrpf.getStatcode(), t5679rec.ridRiskStat[wsaaIndex.toInt()])) {
			wsaaValidRisk.set("Y");
			goTo(GotoLabel.exit1619);
		}
		goTo(GotoLabel.loopRid1614);
	}

	/**
	* <pre>
	* check the coverage level
	* </pre>
	*/
protected void loopCov1615()
	{
		wsaaIndex.add(1);
		if (isGT(wsaaIndex, 12)) {
			wsaaValidRisk.set("N");
			return ;
		}
		if (isEQ(covrpf.getStatcode(), t5679rec.covRiskStat[wsaaIndex.toInt()])) {
			wsaaValidRisk.set("Y");
			return ;
		}
		goTo(GotoLabel.loopCov1615);
	}

protected void validatePremCovrRidr1620()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initPrem1620();
				case loopRid1622: 
					loopRid1622();
				case loopCov1625: 
					loopCov1625();
				case exit1629: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	* Validate the premium status
	* </pre>
	*/
protected void initPrem1620()
	{
		wsaaIndex.set(ZERO);
		if (isEQ(covrpf.getRider(), "00")) {
			goTo(GotoLabel.loopCov1625);
		}
	}

	/**
	* <pre>
	* Check on rider level.
	* </pre>
	*/
protected void loopRid1622()
	{
		wsaaIndex.add(1);
		if (isGT(wsaaIndex, 12)) {
			wsaaValidPremium.set("N");
			goTo(GotoLabel.exit1629);
		}
		if (isEQ(covrpf.getPstatcode(), t5679rec.ridPremStat[wsaaIndex.toInt()])) {
			wsaaValidPremium.set("Y");
			goTo(GotoLabel.exit1629);
		}
		goTo(GotoLabel.loopRid1622);
	}

	/**
	* <pre>
	* Check on coverage level
	* </pre>
	*/
protected void loopCov1625()
	{
		wsaaIndex.add(1);
		if (isGT(wsaaIndex, 12)) {
			wsaaValidPremium.set("N");
			return ;
		}
		if (isEQ(covrpf.getPstatcode(), t5679rec.covPremStat[wsaaIndex.toInt()])) {
			wsaaValidPremium.set("Y");
			return ;
		}
		goTo(GotoLabel.loopCov1625);
	}

protected void readTableT56871700()
	{
		callItdmT56871700();
	}

	/**
	* <pre>
	* Check that the Coverage/Rider exists on T5687
	* </pre>
	*/
protected void callItdmT56871700()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(), covrpf.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sv.riderErr.set(errorsInner.f050);
			wsspcomn.edterror.set("Y");
			return ;
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		/* If paid up method not found in T5687 then display error*/
		/* message and abort program.*/
		if (isEQ(t5687rec.pumeth, SPACES)) {
			sv.pumethErr.set(errorsInner.i027);
			wsspcomn.edterror.set("Y");
		}
	}

protected void getClientName1800()
	{
		callCltsio1800();
	}

protected void callCltsio1800()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(wsaaClntnum);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		wsaaLongconfname.set(wsspcomn.longconfname);
	}

protected void getDescription1900()
	{
		callDescio1900();
	}

protected void callDescio1900()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaDesctabl);
		descIO.setDescitem(wsaaDescitem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaLongdesc.fill("?");
			wsaaShortdesc.fill("?");
		}
		else {
			wsaaShortdesc.set(descIO.getShortdesc());
			wsaaLongdesc.set(descIO.getLongdesc());
		}
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section.
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* Skip this section if returning from a program further down the  */
		/* stack, (current stack position action flag = '*')               */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/* Catering for F11                                                */
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isNE(wsaaPaidupProcessed, SPACES)) {
			sv.chdrstatusErr.set(errorsInner.h009);
			wsspcomn.edterror.set("Y");
		}
		return ;
	}

protected void screenEdit2000()
	{
		start2000();
		continue2004();
	}

protected void start2000()
	{
		/*    CALL 'S5097IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S5097-DATA-AREA.           */
		/* IO module cleared the error indicators so re initialize*/
		/* the error indicators from the WSAA-ERROR-INDICATORS*/
		sv.errorIndicators.set(wsaaErrorIndicators);
		/*VALIDATE*/
		/*  Validate the new sum assured if it is not proctect and is*/
		/*  difference from previous new sum assured.*/
		if (isEQ(sv.newinsvalOut[varcom.pr.toInt()], "Y")) {
			return ;
		}
		if (isNE(sv.newinsval, sv.hnewinsval)) {
			sv.hnewinsval.set(sv.newinsval);
			validateNewSumAssured1570();
			wsaaErrorIndicators.set(sv.errorIndicators);
		}
	}

protected void continue2004()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag, "I")) {
			wsspcomn.edterror.set(varcom.oK);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3100();
	}

protected void updateDatabase3100()
	{
		/* Skip this section if returning from a program further down the*/
		/* stack, (current stack position action flag = '*')*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		/* Enquiry on Paid Up Quotation                                    */
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/* Catering for F11*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.edterror.set(varcom.oK);
			return ;
		}
		/* Do not write a PUPT record, if already processed that is*/
		/* statii found in table T5679.*/
		if (alreadyPaidup.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			return ;
		}
		/*  Write a PUPT record for the component to be paid-up*/
		if (pendingPaidup.isTrue()) {
			puptIO.setFunction(varcom.rewrt);
		}
		else {
			puptIO.setFunction(varcom.writr);
		}
		puptIO.setChdrcoy(covrpf.getChdrcoy());
		puptIO.setChdrnum(covrpf.getChdrnum());
		puptIO.setLife(covrpf.getLife());
		puptIO.setCoverage(covrpf.getCoverage());
		puptIO.setRider(covrpf.getRider());
		puptIO.setPlanSuffix(covrpf.getPlanSuffix());
		puptIO.setPumeth(t5687rec.pumeth);
		puptIO.setNewsuma(sv.newinsval);
		puptIO.setFundAmount(ZERO);
		puptIO.setPupfee(ZERO);
		/* MOVE VRCM-TERM              TO PUPT-TERMID.                  */
		puptIO.setTermid(varcom.vrcmTermid);
		puptIO.setTransactionDate(varcom.vrcmDate);
		puptIO.setTransactionTime(varcom.vrcmTime);
		puptIO.setUser(varcom.vrcmUser);
		puptIO.setFormat(puptrec);
		SmartFileCode.execute(appVars, puptIO);
		if (isNE(puptIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(puptIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.cntcurr);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData f050 = new FixedLengthStringData(4).init("F050");
	private FixedLengthStringData g203 = new FixedLengthStringData(4).init("G203");
	private FixedLengthStringData h009 = new FixedLengthStringData(4).init("H009");
	private FixedLengthStringData h010 = new FixedLengthStringData(4).init("H010");
	private FixedLengthStringData h011 = new FixedLengthStringData(4).init("H011");
	private FixedLengthStringData h012 = new FixedLengthStringData(4).init("H012");
	private FixedLengthStringData h014 = new FixedLengthStringData(4).init("H014");
	private FixedLengthStringData h020 = new FixedLengthStringData(4).init("H020");
	private FixedLengthStringData h021 = new FixedLengthStringData(4).init("H021");
	private FixedLengthStringData i027 = new FixedLengthStringData(4).init("I027");
	private FixedLengthStringData i028 = new FixedLengthStringData(4).init("I028");
	private FixedLengthStringData t023 = new FixedLengthStringData(4).init("T023");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5681 = new FixedLengthStringData(5).init("T5681");
	private FixedLengthStringData t5682 = new FixedLengthStringData(5).init("T5682");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6598 = new FixedLengthStringData(5).init("T6598");
	private FixedLengthStringData t6648 = new FixedLengthStringData(5).init("T6648");
	private FixedLengthStringData t6651 = new FixedLengthStringData(5).init("T6651");
}
}
