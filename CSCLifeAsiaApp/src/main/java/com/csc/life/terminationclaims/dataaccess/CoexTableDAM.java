package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CoexTableDAM.java
 * Date: Sun, 30 Aug 2009 03:34:25
 * Class transformed from COEX.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CoexTableDAM extends CoexpfTableDAM {

	public CoexTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("COEX");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "ARACDE"
		             + ", AGNTNUM"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "ARACDE, " +
		            "AGNTNUM, " +
		            "CHDRNUM, " +
		            "CNTCURR, " +
		            "BILLFREQ, " +
		            "CRTABLE, " +
		            "EFFDATE, " +
		            "GROSPRE, " +
		            "TYEARNO, " +
		            "PAYPERD, " +
		            "TRANDATE, " +
		            "SEXRAT, " +
		            "TRANCD, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "ARACDE ASC, " +
		            "AGNTNUM ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "ARACDE DESC, " +
		            "AGNTNUM DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               aracde,
                               agntnum,
                               chdrnum,
                               cntcurr,
                               billfreq,
                               crtable,
                               effdate,
                               grospre,
                               tyearno,
                               payperd,
                               trandate,
                               sexrat,
                               trancd,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(45);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getAracde().toInternal()
					+ getAgntnum().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, aracde);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(aracde.toInternal());
	nonKeyFiller20.setInternal(agntnum.toInternal());
	nonKeyFiller30.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(106);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getCntcurr().toInternal()
					+ getBillfreq().toInternal()
					+ getCrtable().toInternal()
					+ getEffdate().toInternal()
					+ getGrospre().toInternal()
					+ getTyearno().toInternal()
					+ getPayperd().toInternal()
					+ getTrandate().toInternal()
					+ getSexrat().toInternal()
					+ getTrancd().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, grospre);
			what = ExternalData.chop(what, tyearno);
			what = ExternalData.chop(what, payperd);
			what = ExternalData.chop(what, trandate);
			what = ExternalData.chop(what, sexrat);
			what = ExternalData.chop(what, trancd);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getAracde() {
		return aracde;
	}
	public void setAracde(Object what) {
		aracde.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getGrospre() {
		return grospre;
	}
	public void setGrospre(Object what) {
		setGrospre(what, false);
	}
	public void setGrospre(Object what, boolean rounded) {
		if (rounded)
			grospre.setRounded(what);
		else
			grospre.set(what);
	}	
	public PackedDecimalData getTyearno() {
		return tyearno;
	}
	public void setTyearno(Object what) {
		setTyearno(what, false);
	}
	public void setTyearno(Object what, boolean rounded) {
		if (rounded)
			tyearno.setRounded(what);
		else
			tyearno.set(what);
	}	
	public PackedDecimalData getPayperd() {
		return payperd;
	}
	public void setPayperd(Object what) {
		setPayperd(what, false);
	}
	public void setPayperd(Object what, boolean rounded) {
		if (rounded)
			payperd.setRounded(what);
		else
			payperd.set(what);
	}	
	public PackedDecimalData getTrandate() {
		return trandate;
	}
	public void setTrandate(Object what) {
		setTrandate(what, false);
	}
	public void setTrandate(Object what, boolean rounded) {
		if (rounded)
			trandate.setRounded(what);
		else
			trandate.set(what);
	}	
	public PackedDecimalData getSexrat() {
		return sexrat;
	}
	public void setSexrat(Object what) {
		setSexrat(what, false);
	}
	public void setSexrat(Object what, boolean rounded) {
		if (rounded)
			sexrat.setRounded(what);
		else
			sexrat.set(what);
	}	
	public FixedLengthStringData getTrancd() {
		return trancd;
	}
	public void setTrancd(Object what) {
		trancd.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		aracde.clear();
		agntnum.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		cntcurr.clear();
		billfreq.clear();
		crtable.clear();
		effdate.clear();
		grospre.clear();
		tyearno.clear();
		payperd.clear();
		trandate.clear();
		sexrat.clear();
		trancd.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}