/*
 * File: P6319.java
 * Date: 30 August 2009 0:43:09
 * Author: Quipoz Limited
 * 
 * Class transformed from P6319.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;


import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;//ILB-459
import com.csc.fsu.accounting.dataaccess.model.Acblpf;//ILB-459
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;//ILB-459
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.dataaccess.dao.VstdpfDAO;
import com.csc.life.annuities.dataaccess.model.Vstdpf;
import com.csc.life.contractservicing.dataaccess.dao.RplnpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rplnpf;
//import com.csc.life.enquiries.programs.P6353.FormatsInner;//ILB-459
//import com.csc.life.enquiries.programs.P6363.GotoLabel;//ILB-459
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5677rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ZhlbpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ZhlcpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.ZhlbpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.ZhlcpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Zhlbpf;
import com.csc.life.terminationclaims.dataaccess.model.Zhlcpf;
import com.csc.life.terminationclaims.screens.S6319ScreenVars;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILB-459
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;



/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*     Claims sub-menu.
*
* Validation
* ----------
*
*  Key 1 - Contract number (CHDRCLM)
*
*       Y = mandatory, must exist on file.
*            - CHDRCLM  -  Life  new  business  contract  header
*                 logical view.
*            - must be correct status for transaction (T5679).
*            - MUST BE CORRECT BRANCH.
*
*       N = IRREVELANT.
*
*       Blank = irrelevant.
*
*
* Updating
* --------
*
*  Set up WSSP-FLAG:
*       - "I" for actions E and I, otherwise "M".
*  For enqury transactions ("I"), nothing else is required.
*
*  In Register Mode, Default  Follow  Ups will be created  if a
*  default  follow  up  method  exists on  T5688. T5677 is read
*  using the  follow up method  as  the  key and  up to 16 FLUP
*  records will be created.
*
*  For maintenance transactions  (WSSP-FLAG  set to  'M' above),
*  soft lock the contract header (call SFTLOCK). If the contract
*  is already locked, display an error message.
*
*****************************************************************
* </pre>
*/
public class P6319 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6319");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private String wsaaValidStatuz = "";
	private String wsaaDthOk = "";
	private ZonedDecimalData wsaaLives = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumChanged = new FixedLengthStringData(1);

	protected FixedLengthStringData wsaaT5677Key = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT5677Tranno = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 0);
	protected FixedLengthStringData wsaaT5677FollowUp = new FixedLengthStringData(4).isAPartOf(wsaaT5677Key, 4);
	protected PackedDecimalData wsaaX = new PackedDecimalData(2, 0).init(0).setUnsigned();
	protected PackedDecimalData wsaaFupno = new PackedDecimalData(2, 0);

	protected FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	protected FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

		/* FORMATS */
	protected static final String chdrenqrec = "CHDRENQREC";
		/* Dummy user wssp. Replace with required version.
		    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private T5677rec t5677rec = new T5677rec();
	protected T5661rec t5661rec = new T5661rec();
	protected T5688rec t5688rec = new T5688rec();
	protected Batckey wsaaBatchkey = new Batckey();
	protected T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	protected Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	protected Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	protected Sdasancrec sdasancrec = new Sdasancrec();
	private S6319ScreenVars sv = getPScreenVars();//ScreenProgram.getScreenVars( S6319ScreenVars.class);
	//private ErrorsInner errorsInner = new ErrorsInner();//ILB-459
	private T5645rec t5645rec = new T5645rec();
	private PackedDecimalData wsaaHoldCovAmt = new PackedDecimalData(17,2);
	private PackedDecimalData wsaaHoldBenAmt = new PackedDecimalData(17,2);
	private PackedDecimalData wsaaClaimRlsCovAmt = new PackedDecimalData(17,2);
	private PackedDecimalData wsaaClaimRlsBenAmt = new PackedDecimalData(17,2);
	private ZhlcpfDAO zhlcpfDAO = new ZhlcpfDAOImpl();
	private Zhlcpf zhlcpf = new Zhlcpf();
	private ZhlbpfDAO zhlbpfDAO = new ZhlbpfDAOImpl();
	private Zhlbpf zhlbpf = new Zhlbpf();
	private PackedDecimalData wsaaHoldCovAdjAmt = new PackedDecimalData(17,2);
	private PackedDecimalData wsaaHoldBenAdjAmt = new PackedDecimalData(17,2);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected List<Covrpf> covrpfDAOList;
	protected Covrpf covrpf = null;
	
	protected List<Itempf> itempfList;
	protected Itempf itempf = null;
	
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private List<Lifepf> LifepfList;
	private Lifepf lifepf = null;
	protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	//ILIFE-6811
	protected DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	protected Descpf descpf;
	protected FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	protected List<Fluppf> fluplnbList = new ArrayList<Fluppf>();
	protected Fluppf fluppf = null;
	protected int fupno = 0;
	//ICIL-14 Start
	boolean isLoanConfig = false;
	private RplnpfDAO rplnpfDAO = getApplicationContext().getBean("rplnpfDAO", RplnpfDAO.class);
	private Rplnpf rplnpf = new Rplnpf();
	//ICIL-14 End
	
	//ILB-459 start
	 protected Chdrpf chdrpf = new Chdrpf();
	 protected ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	 
	 private Acblpf acblpf = new Acblpf();
	 private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class); 
	 
	 protected Vstdpf vstdpf = new Vstdpf();
	 protected VstdpfDAO vstdpfDAO= getApplicationContext().getBean("vstdpfDAO",VstdpfDAO.class);
	 protected CattpfDAO cattpfDAO =getApplicationContext().getBean("cattpfDAO",CattpfDAO.class);
	 protected static final String f918 = "F918";
	 protected static final String e544 = "E544";
	 protected static final String e070 = "E070";
	 protected static final String rrfh = "RRFH";
	 protected static final String h917 = "H917";
	 protected static final String e455 = "E455";
	 protected static final String e659 = "E659";
	 protected static final String h124 = "H124";
	 protected static final String e357 = "E357";
	 protected static final String e767 = "E767";
	 protected static final String rftb = "RFTB";
	 protected static final String rftc = "RFTC";
	 protected static final String rfts = "RFTS";
	 protected static final String rftd = "RFTD";
	 protected static final String f073 = "F073";
	 protected static final String f616 = "F616";
	 protected static final String e073 = "E073";
	 protected static final String h222 = "H222";
	 protected static final String f910 = "F910";
     protected static final String rrlx = "RRLX";
     protected static final String rrlw = "RRLW";
     protected static final String jl59 = "JL59";
     protected static final String jl60 = "JL60";
     protected static final String e005 = "E005";
     protected static final String f917 = "F917";
	 protected static final String e186 = "E186";
	 
	//ILB-459 end
		
    protected boolean CMDTH006Permission  = false;
    boolean CMDTH010Permission = false;
	
    private Acblpf acblenqIO = null;
	
/**
 * Contains all possible labels used by goTo action.
 */
	 protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		
		exit1090,
		exit2190, 
		validateKey22220, 
		exit2290, 
		loopCovrclm2650, 
		exit2690, 
		exit12090, 
		keeps3070, 
		batching3080, 
		exit3090, 
		exit3490, 
		exit3590
	}

	public P6319() {
		super();
		screenVars = sv;
		new ScreenModel("S6319", AppVars.getInstance(), sv);
	}
	protected S6319ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( S6319ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		
	}

protected void initialise1010()
	{
        CMDTH006Permission  = FeaConfg.isFeatureExist("2", "CMDTH006", appVars, "IT");
        CMDTH010Permission  = FeaConfg.isFeatureExist("2", "CMDTH010", appVars, "IT");
		wsaaLastChdrnum.set(ZERO);
		wsaaChdrnumChanged.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		sv.efdate.set(varcom.vrcmMaxDate);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		/* This is initialised to 'N' the first time through only.         */
		wsaaLives.set(ZERO);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);//ILB-459
		}
		/*BRD-34 starts*/
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		
		isLoanConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSLND002", appVars, "IT"); //ICIL-14 
		
		loadT56454500();
		if(!CMDTH010Permission)
			sv.actionOut[varcom.nd.toInt()].set("Y");
	}

protected void loadT56454500() {
	//ILIFE-6811
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5645");
		itempf.setItemitem(wsaaProg.toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(wsspcomn.company.toString().concat("T5645").concat(wsaaProg.toString()));
			fatalError600();
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

/*BRD-34 ends*/

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'S6319IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6319-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isNE(sv.chdrsel, wsaaLastChdrnum)) {
			wsaaChdrnumChanged.set("Y");
			wsaaLastChdrnum.set(sv.chdrsel);
			wsaaLives.set(ZERO);
		}
		else {
			wsaaChdrnumChanged.set("N");
		}
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
				validateEffdate2700();
				chkValidAction2300();
			}
			else {
				verifyBatchControl2900();
			}
		}
		
		//ICIL-14 Start
		if( isEQ(isLoanConfig,true)){
			rplnpf = rplnpfDAO.getRploanRecord(sv.chdrsel.toString().trim(),"P");
			if(rplnpf!=null) {
				if(isEQ(sv.action, "A")){						
					sv.actionErr.set(rrfh);	//ILB-459
				}			
			}
		}
		//ICIL-14 End
				
		
		/* Check that the component is not Vesting Registered by        */
		/* checking to see if there is a VSTD.                          */
		if (isEQ(sv.errorIndicators, SPACES)) {
			checkForVstd2950();
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateKey12210();
				case validateKey22220: 
					validateKey22220();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1, SPACES)) {
			goTo(GotoLabel.validateKey22220);
		}
		//ILB-459 start
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
		if(chdrpf==null && isEQ(subprogrec.key1, "Y")){
			if(isEQ(sv.chdrsel, SPACES)){
				sv.chdrselErr.set(e186);
			}
			else{
			sv.chdrselErr.set(e544);
			}
			if(CMDTH010Permission && (isEQ(sv.action,"K") || isEQ(sv.action,"L"))) {
				sv.chdrselErr.set(f917);
				goTo(GotoLabel.exit2290);
			}
			goTo(GotoLabel.exit2290);
		}
		
		if (!CMDTH010Permission && (isEQ(sv.action, "K") || isEQ(sv.action, "L"))) {
			sv.actionErr.set(e005);
			return;
		}
		
		
		if(chdrpf!=null && isEQ(subprogrec.key1, "N")){
			sv.chdrselErr.set(f918);
		}
		
		//ILB-459 end
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				goTo(GotoLabel.exit2290);
			}
		}
		if (isEQ(subprogrec.key1, "Y")
				&& isEQ(sv.chdrselErr, SPACES)) {
					checkStatus2400();
				}
		//ILB-459 start
		if(chdrpf!=null && isEQ(sv.chdrselErr, SPACES)){
			if (isEQ(chdrpf.getCcdate(), chdrpf.getPtdate())) {
				sv.chdrselErr.set(h917);
			}
		}
		
		//ILB-459 end
		
		/*    For transactions on policies,*/
		/*       check the contract selected is of the correct status*/
		/*       and from correct branch.*/
		
		/*BRD-34 starts*/
		if (isEQ(subprogrec.key1, "Y") && isEQ(sv.chdrselErr, SPACES)
				&& isEQ(sv.action, "F")) {		
			checkClaimHoldCov4100();
			checkClaimHoldBen4200();
		}
		if (isEQ(subprogrec.key1, "Y") && isEQ(sv.chdrselErr, SPACES)
				&& isEQ(sv.action, "G")) {			
	            checkClaimHoldBen4200();
	            checkClaimHoldCov4100();
		}
		
		if (isEQ(subprogrec.key1, "Y") && isEQ(sv.chdrselErr, SPACES)
				&& isEQ(sv.action, "H")) {			
			checkClaimRlsCov4300();
		}
		if (isEQ(subprogrec.key1, "Y") && isEQ(sv.chdrselErr, SPACES)
				&& isEQ(sv.action, "I")) {			
			checkClaimRlsBen4400();
		}
		
		if (!CMDTH010Permission && isEQ(sv.action, "K")) {
			sv.actionErr.set(e005);
		}
		if (isEQ(sv.chdrselErr, SPACES)&& isEQ(sv.action, "K") && CMDTH010Permission) {		
			checkValidPolicyStatus();
		}
		
		if (isEQ(sv.chdrselErr, SPACES)&& isEQ(sv.action, "L") && CMDTH010Permission) {		
			checkPreRegistration();
		}
		if (isEQ(subprogrec.key1, "Y") && isEQ(sv.chdrselErr, SPACES)
				&& isEQ(sv.action, "c")) {		
			checkClaimHoldCov4100();
		}

        if (CMDTH006Permission && isEQ(subprogrec.key1, "Y") && isEQ(sv.chdrselErr, SPACES)
                && isEQ(sv.action, "A")) {
            checkRegHoldCov4500();
            double Lpae = checkRegHoldCov4600();
            double Lpas = checkRegHoldCov4700();
            //ICIL-920
          	//if((Lpae+Lpas)!=0){
			if(((Lpae+Lpas)!=0)
			&&  isEQ(sv.chdrselErr, SPACES)){
                sv.chdrselErr.set(rrlw);
            }
        }
		/*BRD-34 ends*/
		//ILB-459 start
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)
		&& isNE(wsspcomn.branch, chdrpf.getCntbranch())) {
			sv.chdrselErr.set(e455);
		}
		//ILB-459 end
	}
    protected void checkValidPolicyStatus(){
    	if(cattpfDAO.isExists(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum())){
    		sv.chdrselErr.set(jl59);
    	}
   }
    protected void checkRegHoldCov4500() {
    	//IBPLIFE-572
    	acblenqIO = acblDao.getAcblpfRecord(wsspcomn.company.toString(), t5645rec.sacscode03.toString(), sv.chdrsel.toString(), t5645rec.sacstype03.toString(), chdrpf.getCntcurr());
 
       //ICIL-900
          if(acblenqIO !=null && acblenqIO.getSacscurbal()!=null){
         	  if(acblenqIO.getSacscurbal().compareTo(BigDecimal.ZERO)!=0){
               	  sv.chdrselErr.set(rrlx);
              } 
          }


    }
	protected void checkPreRegistration(){
	boolean isExists = 	cattpfDAO.isExists(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());
	if(!isExists){
	sv.chdrselErr.set(jl60);	
	}
	}
    protected double checkRegHoldCov4600() {

        double result=0;
      //IBPLIFE-572
    	acblenqIO = acblDao.getAcblpfRecord(wsspcomn.company.toString(), t5645rec.sacscode04.toString(), sv.chdrsel.toString(), t5645rec.sacstype04.toString(), chdrpf.getCntcurr());
 
        
        //ICIL-900
        if(acblenqIO !=null && acblenqIO.getSacscurbal()!=null){
            result=acblenqIO.getSacscurbal()==null?0:acblenqIO.getSacscurbal().doubleValue();
        }

        return result ;
    }

    protected double checkRegHoldCov4700() {
        double result=0;

        acblenqIO = acblDao.getAcblpfRecord(wsspcomn.company.toString(), t5645rec.sacscode05.toString(), sv.chdrsel.toString(), t5645rec.sacstype05.toString(), chdrpf.getCntcurr());

        //ICIL-900
        if(acblenqIO !=null && acblenqIO.getSacscurbal()!=null){
            result=acblenqIO.getSacscurbal()==null?0:acblenqIO.getSacscurbal().doubleValue();
        }
        return result;
    }
protected void validateKey22220()
	{
		/*if (isEQ(sv.chdrsel, SPACES)) {
			sv.chdrselErr.set(e544);//ILB-459  
			return ;
		}*/
		//htruong25
		validateKey22220CustomerSpecific();
		
		if (isEQ(subprogrec.key2, SPACES)
		|| isEQ(subprogrec.key2, "N")) {
			return ;
		}
		//ILB-459 start
		chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
		if(chdrpf==null){
			sv.chdrselErr.set(e544);
		}
		//ILB-459 end
	}




protected void chkValidAction2300()
	{
		init2310();
	}

protected void init2310()
	{
		if (isNE(sv.chdrselErr, SPACES)
		&& isEQ(wsaaChdrnumChanged, "N")) {
			return ;
		}
		if(chdrpf == null){
			return;
		}
		wsaaDthOk = "N";
		chkValidDth2600();
		if (isEQ(wsaaDthOk, "N") && isEQ(sv.chdrselErr, SPACES)) {
			sv.chdrselErr.set(e659);//ILB-459
		}
		/* Only on the first time through for a Register Death Claim       */
		/* transaction and for every Register First Death transaction,     */
		/* check how many life records are found for this contract.        */
		/* If the WSAA-LIVES flag is greater than one then more than       */
		/* one valid life has been found.  If more than one life is        */
		/* found for a Register Death Claim, set a warning message.        */
		/* Similarly, if ONLY one life is found for a Register First       */
		/* Death, display an error.                                        */
		if (isEQ(sv.action, "A")
		&& isEQ(wsaaLives, ZERO)
		|| isEQ(sv.action, "D")) {
			wsaaLives.set(ZERO);
		}
		else {
			return ;
		}
		lifepf = new Lifepf();
		lifepf.setChdrcoy(chdrpf.getChdrcoy().toString());//ILB-459
		lifepf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
		LifepfList = lifepfDAO.selectLifepfRecord(lifepf);
		
		if(LifepfList!= null && LifepfList.size() > 0)
		{	
			for(Lifepf life : LifepfList)
			{
					if (isEQ(life.getValidflag(), "2")) {						
						continue;
					}
					else {
						if (isEQ(life.getJlife(), ZERO)) {
							for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
							|| isEQ(t5679rec.lifeStat[wsaaSub.toInt()], SPACES)); wsaaSub.add(1)){
								if (isEQ(t5679rec.lifeStat[wsaaSub.toInt()], life.getStatcode())) {
									wsaaLives.add(1);
								}
							}
						}
						else {
							for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
							|| isEQ(t5679rec.jlifeStat[wsaaSub.toInt()], SPACES)); wsaaSub.add(1)){
								if (isEQ(t5679rec.jlifeStat[wsaaSub.toInt()], life.getStatcode())) {
									wsaaLives.add(1);
								}
							}
						}
					}			
			}
		}
		
		if (isGT(wsaaLives, 1)
		&& isEQ(sv.action, "A")) {
			sv.chdrselErr.set(h124);//ILB-459
		}
		else {
			if (isLTE(wsaaLives, 1)
			&& isEQ(sv.action, "D")) {
				sv.chdrselErr.set(e357);//ILB-459
			}
		}
	}

protected void checkStatus2400()
	{
		readStatusTable2410();
	}

protected void readStatusTable2410()
	{
		//ILIFE-6811
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5679");
		itempf.setItemitem(subprogrec.transcd.toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(wsspcomn.company.toString().concat("T5645").concat(subprogrec.transcd.toString()));
			fatalError600();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/*MOVE ZERO TO WSAA-SUB.                                       */
		/*PERFORM 2500-LOOK-FOR-STAT.                                  */
		wsaaValidStatuz = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
			lookForStat2500();
		}
		if (isNE(wsaaValidStatuz, "Y")) {
			sv.chdrselErr.set(e767);//ILB-459
			wsspcomn.edterror.set("Y");
		}
	}

protected void lookForStat2500()
	{
		/*SEARCH*/
		/*ADD 1 TO WSAA-SUB.                                        */
		/*IF WSAA-SUB > 12                                          */
		/*   MOVE E767                TO S6319-CHDRSEL-ERR          */
		/*   MOVE 'Y'                 TO WSSP-EDTERROR              */
		/*ELSE                                                      */
		/*IF CHDRCLM-STATCODE NOT = T5679-CN-RISK-STAT(WSAA-SUB)    */
		/*   OR                                                <007>*/
		/*   CHDRCLM-PSTATCODE NOT = T5679-CN-PREM-STAT(WSAA-SUB)<00*/
		/*   GO TO 2510-SEARCH.                                     */
	//ILB-459 start
		if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()], chdrpf.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
			|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
				if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()], chdrpf.getPstcde())) {
					wsaaValidStatuz = "Y";
				}
			}
		}
		//ILB-459 end
		/*EXIT*/
	}

protected void chkValidDth2600()
	{
		covrpf = new Covrpf();
		covrpf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
		covrpf.setChdrcoy(chdrpf.getChdrcoy().toString());//ILB-459
		covrpfDAOList = covrpfDAO.searchCovrRecordForContract(covrpf);
		if(covrpfDAOList != null && covrpfDAOList.size() > 0)
		{
			for(Covrpf covr : covrpfDAOList)
			{
				itempf = new Itempf();
				itempf.setItempfx("IT");
				itempf.setItemcoy(wsspcomn.company.toString());
				itempf.setItemitem(covr.getCrtable());
				itempf.setItemtabl("T5687");
				itempf.setItmfrm(new BigDecimal(covr.getCrrcd()));
				itempf.setItmto(new BigDecimal(covr.getCrrcd()));
				
				itempfList = itemDAO.findByItemDates(itempf);//ILIFE-3955
				if(itempfList != null && itempfList.size() > 0) {
					for (Itempf it : itempfList) {				
						t5687rec.t5687Rec.set(new String(it.getGenarea()));
						
					}
				} 
				
				if (isNE(t5687rec.dcmeth, SPACES)) {
					wsaaDthOk = "Y";
					return ;
				}				
			}
		}
	}

/*BRD-34 starts*/
protected void checkClaimHoldCov4100() {

        wsaaHoldCovAmt.set(0);
        wsaaHoldCovAdjAmt.set(0);
      //ILB-459 start
        acblpf = acblDao.getAcblpfRecord(wsspcomn.company.toString(), t5645rec.sacscode01.toString(), sv.chdrsel.toString(),t5645rec.sacstype01.toString(), 
        		chdrpf.getCntcurr()); /* IJTI-1479 */
        if(acblpf!=null){
        	wsaaHoldCovAmt.set(acblpf.getSacscurbal());//ILIFE-7701
			compute(wsaaHoldCovAmt, 2).set(mult(wsaaHoldCovAmt,-1));
        }
      //  wsaaHoldCovAmt.set(0);
      //ILB-459 end
		/* BRD-34 */
		zhlcpf.setChdrcoy(chdrpf.getChdrcoy().toString());//ILB-459
		zhlcpf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
		List ls = zhlcpfDAO.readZhlcpf(zhlcpf);
		
		
		
			Iterator  lsIterator =ls.iterator();
						while ( lsIterator.hasNext())
						{
							Zhlcpf tempZhlcpf =		  (Zhlcpf)lsIterator.next();
						
							if(	isGT( (tempZhlcpf.getZhldclma().toBigInteger()) ,0) ){ 
									
								wsaaHoldCovAdjAmt.add(tempZhlcpf.getZhldclma().doubleValue());
								}
						}	

		
		if (isGT(wsaaHoldCovAmt, 0) || isGT(wsaaHoldCovAdjAmt, 0)) {
			if( isEQ(sv.action, 'F') ||  isEQ(sv.action, 'G'))
			sv.chdrselErr.set(rftb);//ILB-459
			else 
				sv.chdrselErr.set (rfts);//ILB-459

		}
		if (isEQ(wsaaHoldCovAmt, 0)  && isEQ(wsaaHoldCovAdjAmt, 0)) {
			if( isEQ(sv.action, 'I'))	
				sv.chdrselErr.set(rftc);//ILB-459
		}
		
}
/* BRD-34 */ 

protected void checkClaimHoldBen4200() {

	wsaaHoldBenAmt.set(0);
	wsaaHoldBenAdjAmt.set(0);
	//ILB-459 start
	acblpf = acblDao.getAcblpfRecord(wsspcomn.company.toString(), t5645rec.sacscode02.toString(), sv.chdrsel.toString(), 
			t5645rec.sacstype02.toString(), chdrpf.getCntcurr()); /* IJTI-1479 */
	
	  if(acblpf!=null){
		  wsaaHoldBenAmt.set(acblpf.getSacscurbal());//ILIFE-7722 
		  compute(wsaaHoldBenAmt, 2).set(mult(wsaaHoldBenAmt,-1));
      }
	  //ILIFE-7722 start 
	/* if(acblpf == null){
	  	wsaaHoldBenAmt.set(0);  //ILIFE-7722 --ends
	 }*/
	 //ILIFE-7722 end
	/*BRD-34*/
	  //ILB-459 end
	
	zhlbpf.setChdrcoy(chdrpf.getChdrcoy().toString());//ILB-459
	zhlbpf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
	List ls = zhlbpfDAO.readZhlbpf(zhlbpf);
	
	
	
		Iterator  lsIterator =ls.iterator();
					while ( lsIterator.hasNext())
					{
						Zhlbpf tempZhlbpf = (Zhlbpf)lsIterator.next();
					
						if(	isGT( (tempZhlbpf.getZhldclma().toBigInteger()) ,0) ){ 
								
							wsaaHoldBenAdjAmt.add(tempZhlbpf.getZhldclma().doubleValue());
							}
					}	

	
	/*BRD-34*/
	
	if( isGT(wsaaHoldBenAmt, 0) || isGT(wsaaHoldBenAdjAmt, 0) )  {
		if( isEQ(sv.action, 'G') ||  isEQ(sv.action, 'F'))	
		sv.chdrselErr.set(rftb);//ILB-459
		else 
			sv.chdrselErr.set (rftd);//ILB-459
	}
	if (isEQ(wsaaHoldBenAmt, 0) && isEQ(wsaaHoldBenAdjAmt, 0)) {
		if( isEQ(sv.action, 'H'))	
			sv.chdrselErr.set(rftd);	//ILB-459
	}
   }


protected void checkClaimRlsCov4300() {

	wsaaClaimRlsCovAmt.set(0);
	//ILB-459 start
	acblpf = acblDao.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode01.toString(), sv.chdrsel.toString(), 
    		chdrpf.getCntcurr(),t5645rec.sacstype01.toString()); /* IJTI-1479 */
	
	  if(acblpf!=null){
		  wsaaClaimRlsCovAmt.set(acblpf.getSacscurbal());

		  compute(wsaaClaimRlsCovAmt, 2).set(mult(wsaaClaimRlsCovAmt,-1));
      }
	  if(acblpf==null){
		  wsaaClaimRlsCovAmt.set(0);  
	  }
	  if (isEQ(wsaaClaimRlsCovAmt, 0)) {
			sv.chdrselErr.set(rftc);
		}
	//ILB-459 end
}


protected void checkClaimRlsBen4400() 
	{

	wsaaClaimRlsBenAmt.set(0);
	//ILB-459 start
	acblpf = acblDao.loadDataByBatch(wsspcomn.company.toString(), t5645rec.sacscode02.toString(), sv.chdrsel.toString().trim(), 
			chdrpf.getCntcurr(),t5645rec.sacstype02.toString()); /* IJTI-1479 */
	if(acblpf!=null){
		wsaaClaimRlsBenAmt.set(acblpf.getSacscurbal());
		compute(wsaaClaimRlsBenAmt, 2).set(mult(wsaaClaimRlsBenAmt,-1));
	}
	
	if(acblpf==null){
		wsaaClaimRlsBenAmt.set(0);
	  }
	if (isEQ(wsaaClaimRlsBenAmt, 0)) {
		sv.chdrselErr.set(rftd);
	}
	//ILB-459 end
 }


protected void validateEffdate2700()
	{
		start2700();
	}

	/**
	* <pre>
	*  Effective Date must be entered for a Death Claim.              
	*  It must not be greater than current date.                      
	*  ie. cannot die tomorrow!                                       
	*  It must also not be prior to the contract commencement date.   
	* </pre>
	*/
protected void start2700()
	{
		if (isNE(sv.chdrselErr, SPACES)) {
			return ;
		}
		if (isNE(sv.efdateErr, SPACES)) {
			return ;
		}
		if (isEQ(sv.efdate, varcom.vrcmMaxDate)) {
			sv.efdate.set(wsaaToday);
		}
		else {
			if (isGT(sv.efdate, wsaaToday)) {
				sv.efdateErr.set(f073);//ILB-459
			}
			else {
				if (isLT(sv.efdate, chdrpf.getOccdate())) {
					sv.efdateErr.set(f616);//ILB-459
				}
			}
		}
		wsspcomn.currfrom.set(sv.efdate);
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			/*     MOVE E073               TO S6319-ACTION-ERR.             */
			sv.actionErr.set(e073);//ILB-459
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void checkForVstd2950()
	{
		starts2950();
	}

protected void starts2950()
	{		
	    int count = vstdpfDAO.getVstdpfList(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum().trim());
	    
	    if(count == 0)
	    	return;	    
	    else 
	    	sv.chdrselErr.set(h222);
	    
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
				case keeps3070: 
					keeps3070();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details*/
		if (isEQ(scrnparams.action, "E")
		|| isEQ(scrnparams.action, "I")
		|| isEQ(scrnparams.action, "J")
		||isEQ(scrnparams.action,"L")) {
			wsspcomn.flag.set("I");
			
			//ILB-459 start 
			
			/*chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
			if(chdrpf==null){
				chdrenqIO.setFormat(formatsInner.chdrenqrec);
				fatalError600();
			}*/
			goTo(GotoLabel.keeps3070);
			
			/*chdrenqIO.setChdrcoy(wsspcomn.company);
			chdrenqIO.setChdrnum(sv.chdrsel);
			chdrenqIO.setFunction(varcom.reads);
			chdrenqIO.setFormat(chdrenqrec);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrclmIO.getStatuz(), varcom.oK)
			&& isNE(chdrclmIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(chdrclmIO.getParams());
				fatalError600();
			}
			else {
				goTo(GotoLabel.keeps3070);
			}*/
			//ILB-459 end
		}
		else {
			wsspcomn.flag.set("M");
		}
		updt3010CustomerSpecific();
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.batching3080);
		}
		/*    Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		/* MOVE S6319-CHDRSEL          TO SFTL-ENTITY                   */
		sftlockrec.entity.set(chdrpf.getChdrnum());//ILB-459
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(f910);//ILB-459
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		/*    For existing proposals, add one to the transaction number.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			setPrecision(chdrpf.getTranno(), 0);//ILB-459
			chdrpf.setTranno(add(chdrpf.getTranno(), 1).toInt());//ILB-459
		}
		if (isEQ(sv.action, "A")) {
			createFollowUps3400();
			//htruong25
			setWsspcomnEtnameCustomerSpecific();
			
		}
	}



protected void keeps3070()
	{
		/*   Store the contract header for use by the transaction programs*/
	//ILB-459 start
		chdrpfDAO.setCacheObject(chdrpf);
		//ILB-459 end
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void createFollowUps3400()
	{
		try {
			checkIfRequired3410();
			readDefaultsTable3420();
			writeFollowUps3430();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkIfRequired3410()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrpf.getCnttype()); /* IJTI-1479 */
		itempf.setItemtabl("T5688");
		itempf.setItmfrm(wsaaToday.getbigdata());
		itempf.setItmto(wsaaToday.getbigdata());
		
		itempfList = itemDAO.findByItemDates(itempf);//ILIFE-3955
		if(itempfList != null && itempfList.size() > 0) {
			for (Itempf it : itempfList) {				
				t5688rec.t5688Rec.set(new String(it.getGenarea()));
				
			}
		} 
		
		if (isEQ(t5688rec.defFupMeth, SPACES)) {
			goTo(GotoLabel.exit3490);
		}
	}

protected void readDefaultsTable3420()
	{
		wsaaT5677Tranno.set(wsaaBatchkey.batcBatctrcde);
		wsaaT5677FollowUp.set(t5688rec.defFupMeth);
		//ILIFE-6811
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5677");
		itempf.setItemitem(wsaaT5677Key.toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(wsspcomn.company.toString().concat("TR386").concat(wsaaT5677Key.toString()));
			fatalError600();
		}
		t5677rec.t5677Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void writeFollowUps3430()
	{
		wsaaFupno.set(0);
		for (wsaaX.set(1); !(isGT(wsaaX, 16)); wsaaX.add(1)){
			writeFollowUp3500();
		}
		if(fluplnbList.size() > 0){
			fluppfDAO.insertFlupRecord(fluplnbList);
			fluplnbList.clear();
		}
		
	}

	protected void writeFollowUp3500() {
		try {
			fluppf = new Fluppf();
			lookUpStatus3510();
			lookUpDescription3520();
			writeRecord3530();
		} catch (GOTOException e) {
			/* Expected exception for control flow purposes. */
		}
	}

protected void lookUpStatus3510()
	{
		/*    IF T5677-FUPCODE (WSAA-X)    = SPACES                <V72L11>*/
		if (isEQ(t5677rec.fupcdes[wsaaX.toInt()], SPACES)) {
			goTo(GotoLabel.exit3590);
		}
		//ILIFE-6811
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl("T5661");
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(t5677rec.fupcdes[wsaaX.toInt()]);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(wsspcomn.company.toString().concat("T5661").concat(wsaaT5661Key.toString()));
			fatalError600();
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}

protected void lookUpDescription3520(){
	//ILIFE-6811
	wsaaT5661Lang.set(wsspcomn.language);
	wsaaT5661Fupcode.set(t5677rec.fupcdes[wsaaX.toInt()]);
	descpf=descDAO.getdescData("IT", "T5661" ,wsaaT5661Key.toString().trim(), wsspcomn.company.toString(), wsspcomn.language.toString());
}

protected void writeRecord3530(){
	//ILIFE-6811
	fupno++;
	fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
	fluppf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
	fluppf.setClamNum("");
	fluppf.setFupNo(fupno);
	/* Default Life is 01, this will be available to change on the     */
	/* Follow Up window.                                               */
	fluppf.setLife("01");
	fluppf.setjLife("00");
	fluppf.setTranNo(chdrpf.getTranno().intValue());//ILB-459
	fluppf.setFupDt(wsaaToday.toInt());
	fluppf.setFupCde(t5677rec.fupcdes[wsaaX.toInt()].toString());
	fluppf.setFupTyp('C');
	fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	fluppf.setFupRmk(descpf.getLongdesc());
	fluppf.setTermId(varcom.vrcmTermid.toString());
	fluppf.setTrdt(varcom.vrcmDate.toInt());
	fluppf.setTrtm(varcom.vrcmTime.toInt());
	fluppf.setEffDate(datcon1rec.intDate.toInt());
	fluppf.setCrtDate(datcon1rec.intDate.toInt());
	fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
	fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
	fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
	fluppf.setUserT(varcom.vrcmUser.toInt());
	fluplnbList.add(fluppf);
}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
protected void validateKey22220CustomerSpecific() {
	
}
public Subprogrec getSubprogrec() {
	return subprogrec;
}
public void setSubprogrec(Subprogrec subprogrec) {
	this.subprogrec = subprogrec;
}
protected void setWsspcomnEtnameCustomerSpecific() {
	
}


//ILB-459 end
protected void updt3010CustomerSpecific(){
}
}
