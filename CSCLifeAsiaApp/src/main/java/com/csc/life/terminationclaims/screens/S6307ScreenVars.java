package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6307
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6307ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(998);
	public FixedLengthStringData dataFields = new FixedLengthStringData(486).isAPartOf(dataArea, 0);
	public ZonedDecimalData clamant = DD.clamant.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,17);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,20);
	public ZonedDecimalData estimateTotalValue = DD.estimtotal.copyToZonedDecimal().isAPartOf(dataFields,28);
	public ZonedDecimalData prcnt = DD.prcnt.copyToZonedDecimal().isAPartOf(dataFields,45);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,50);
	public ZonedDecimalData totalamt = DD.totalamt.copyToZonedDecimal().isAPartOf(dataFields,67);
	public ZonedDecimalData totalfee = DD.totalfee.copyToZonedDecimal().isAPartOf(dataFields,85);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,102);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,110);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,118);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,121);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,129);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,159);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,167);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,214);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,222);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,269);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,277);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,324);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,328);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,338);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,346);
	
	public FixedLengthStringData reserveUnitsInd = DD.rsunin.copy().isAPartOf(dataFields,356); // ILIFE-5459
	public ZonedDecimalData reserveUnitsDate = DD.rundte.copyToZonedDecimal().isAPartOf(dataFields,357); // ILIFE-5459	
	//ICIL-254
	public FixedLengthStringData payee = DD.payeesel.copy().isAPartOf(dataFields, 365);
	public FixedLengthStringData payeename = DD.payeename.copy().isAPartOf(dataFields, 375);
	public FixedLengthStringData reqntype = DD.reqntype.copy().isAPartOf(dataFields,405);
	public FixedLengthStringData bankacckey = DD.bankacckey1.copy().isAPartOf(dataFields,406);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,426);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,456);
	public FixedLengthStringData crdtcrd = DD.crdtcrd.copy().isAPartOf(dataFields,466);
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(128).isAPartOf(dataArea, 486);
	public FixedLengthStringData clamantErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData estimtotalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData prcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData totalamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData totalfeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	
	public FixedLengthStringData rsuninErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92); // ILIFE-5459
	public FixedLengthStringData rundteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96); // ILIFE-5459
	
	public FixedLengthStringData payeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100); //ICIL-254
	public FixedLengthStringData payeenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104); //ICIL-254
	public FixedLengthStringData reqntypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData crdtcrdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(384).isAPartOf(dataArea, 614);
	public FixedLengthStringData[] clamantOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] estimtotalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] prcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] totalamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] totalfeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	
	public FixedLengthStringData[] rsuninOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276); // ILIFE-5459
	public FixedLengthStringData[] rundteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288); // ILIFE-5459
	
	public FixedLengthStringData[] payeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300); //ICIL-254
	public FixedLengthStringData[] payeenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312); //ICIL-254
	public FixedLengthStringData[] reqntypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] crdtcrdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(424);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(118).isAPartOf(subfileArea, 0);
	public ZonedDecimalData actvalue = DD.actvalue.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(subfileFields,22);
	public ZonedDecimalData estMatValue = DD.emv.copyToZonedDecimal().isAPartOf(subfileFields,30);
	public ZonedDecimalData hactval = DD.hactval.copyToZonedDecimal().isAPartOf(subfileFields,47);
	public FixedLengthStringData hcnstcur = DD.hcnstcur.copy().isAPartOf(subfileFields,64);
	public FixedLengthStringData hcover = DD.hcover.copy().isAPartOf(subfileFields,67);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,69);
	public ZonedDecimalData hemv = DD.hemv.copyToZonedDecimal().isAPartOf(subfileFields,73);
	public FixedLengthStringData hjlife = DD.hjlife.copy().isAPartOf(subfileFields,90);
	public FixedLengthStringData hlife = DD.hlife.copy().isAPartOf(subfileFields,92);
	public FixedLengthStringData htype = DD.htype.copy().isAPartOf(subfileFields,94);
	public FixedLengthStringData hupdflg = DD.hupdflg.copy().isAPartOf(subfileFields,95);
	public ZonedDecimalData percreqd = DD.percreqd.copyToZonedDecimal().isAPartOf(subfileFields,96);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,101);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(subfileFields,103);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(subfileFields,113);
	public FixedLengthStringData fund = DD.virtfund.copy().isAPartOf(subfileFields,114);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(76).isAPartOf(subfileArea, 118);
	public FixedLengthStringData actvalueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData emvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hactvalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hcnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hcoverErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData hemvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData hjlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData hlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData htypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData hupdflgErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData percreqdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData shortdsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 64);
	public FixedLengthStringData typeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 68);
	public FixedLengthStringData virtfundErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 72);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(228).isAPartOf(subfileArea, 194);
	public FixedLengthStringData[] actvalueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] emvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hactvalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hcnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hcoverOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] hemvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] hjlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] hlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] htypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] hupdflgOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] percreqdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 192);
	public FixedLengthStringData[] typeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 204);
	public FixedLengthStringData[] virtfundOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 216);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 422);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData reserveUnitsDateDisp = new FixedLengthStringData(10); // ILIFE-5459

	public LongData S6307screensflWritten = new LongData(0);
	public LongData S6307screenctlWritten = new LongData(0);
	public LongData S6307screenWritten = new LongData(0);
	public LongData S6307protectWritten = new LongData(0);
	public GeneralTable s6307screensfl = new GeneralTable(AppVars.getInstance());
	
	public FixedLengthStringData susur002flag = new FixedLengthStringData(1); 

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6307screensfl;
	}

	public S6307ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(percreqdOut,new String[] {"07","60","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actvalueOut,new String[] {"05","60","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "-01","01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currcdOut,new String[] {"08","20","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(totalfeeOut,new String[] {null, null, null, "11",null, null, null, null, null, null, null, null});
		fieldIndMap.put(totalamtOut,new String[] {"03","20","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prcntOut,new String[] {"04","20","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "41",null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(rsuninOut,new String[] {"70","31","-70","59",null, null, null, null, null, null, null, null}); // ILIFE-5459 
		fieldIndMap.put(rundteOut,new String[] {"71","32","-71","69",null, null, null, null, null, null, null, null}); // ILIFE-5459
		fieldIndMap.put(bankacckeyOut,new String[] {"55", "56", "55", "57", null, null, null, null, null, null, null, null});
		fieldIndMap.put(crdtcrdOut,new String[] {"60", "61", "60", "62", null, null, null, null, null, null, null, null});
		fieldIndMap.put(reqntypeOut,new String[] {"80", "81", "80", "82", null, null, null, null, null, null, null, null});
		fieldIndMap.put(payeeOut,new String[] {null, null, null, "88",null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		
		screenSflFields = new BaseData[] {hactval, hemv, htype, hcnstcur, hcover, hcrtable, hlife, hjlife, hupdflg, crrcd, coverage, rider, fund, fieldType, shortds, cnstcur, percreqd, actvalue, estMatValue};
		screenSflOutFields = new BaseData[][] {hactvalOut, hemvOut, htypeOut, hcnstcurOut, hcoverOut, hcrtableOut, hlifeOut, hjlifeOut, hupdflgOut, crrcdOut, coverageOut, riderOut, virtfundOut, typeOut, shortdsOut, cnstcurOut, percreqdOut, actvalueOut, emvOut};
		screenSflErrFields = new BaseData[] {hactvalErr, hemvErr, htypeErr, hcnstcurErr, hcoverErr, hcrtableErr, hlifeErr, hjlifeErr, hupdflgErr, crrcdErr, coverageErr, riderErr, virtfundErr, typeErr, shortdsErr, cnstcurErr, percreqdErr, actvalueErr, emvErr};
		screenSflDateFields = new BaseData[] {crrcd};
		screenSflDateErrFields = new BaseData[] {crrcdErr};
		screenSflDateDispFields = new BaseData[] {crrcdDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, planSuffix, effdate, clamant, estimateTotalValue, currcd, totalfee, totalamt, prcnt, taxamt, reserveUnitsInd, reserveUnitsDate, payee, payeename, reqntype,bankacckey,bankdesc,bankkey,crdtcrd};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, plnsfxOut, effdateOut, clamantOut, estimtotalOut, currcdOut, totalfeeOut, totalamtOut, prcntOut, taxamtOut, rsuninOut, rundteOut,  payeeOut, payeenameOut, reqntypeOut,bankacckeyOut,bankdescOut,bankkeyOut,crdtcrdOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, plnsfxErr, effdateErr, clamantErr, estimtotalErr, currcdErr, totalfeeErr, totalamtErr, prcntErr, taxamtErr, rsuninErr, rundteErr,  payeeErr, payeenameErr, reqntypeErr,bankacckeyErr,bankdescErr,bankkeyErr,crdtcrdErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, effdate, reserveUnitsDate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, effdateErr, rundteErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdateDisp, reserveUnitsDateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6307screen.class;
		screenSflRecord = S6307screensfl.class;
		screenCtlRecord = S6307screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6307protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6307screenctl.lrec.pageSubfile);
	}
}
