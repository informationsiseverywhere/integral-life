package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import java.util.ArrayList;
import java.util.List;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.screens.Sjl52ScreenVars;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.csc.smart.recordstructures.Varcom;

public class Pjl52 extends ScreenProgCS{
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL52");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Sjl52ScreenVars sv = ScreenProgram.getScreenVars( Sjl52ScreenVars.class);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private List<Cattpf> cattpfList = new ArrayList<>();
	private CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private FixedLengthStringData wsaaClaimNumbersearch =  new FixedLengthStringData(9);
	private FixedLengthStringData wsaaContractNumbersearch =  new FixedLengthStringData(8);
	private FixedLengthStringData wsaaOwnersearch =  new FixedLengthStringData(50);
	private FixedLengthStringData wsaaContactDatesearch =  new FixedLengthStringData(8);
	private FixedLengthStringData wsaaClaimTypesearch =  new FixedLengthStringData(2);
	private Cattpf cattpfTemp = new Cattpf(); 
	private static final String SJL52 = "Sjl52";
	private FixedLengthStringData wsaaRecordFoundFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaNoRecordFound = new Validator(wsaaRecordFoundFlag, "N");
	private Validator wsaaRecordFound = new Validator(wsaaRecordFoundFlag, "Y");
	private boolean isFilterSame;
	private String cn = "CN" ;
	private String it = "IT" ;
	private String ta522 = "TA522" ;
	private String zero = "00000000";
	boolean cntCnclFlag  = false;
	
	public Pjl52() {
		super();
		screenVars = sv;
		new ScreenModel(SJL52, AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			throw e;
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			throw e;
		}
	}

	@Override
	protected void initialise1000()
		{
			initialise1001();
		}
	

	protected void initialise1001()
	{	//IBPLIFE-3582
		cntCnclFlag  = FeaConfg.isFeatureExist("2", "SUOTR009", appVars, "IT");
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		
		sv.conStatus.set(SPACES);
		sv.claimFrom.set(SPACES);
		sv.conFrom.set(SPACES);
		sv.conOwner.set(SPACES);
		sv.contactDate.clear();
		scrnparams.function.set(Varcom.sclr);
		processScreen(SJL52, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (!cntCnclFlag) {
			sv.cnclScreenFlag.set("N");
		}
		else {
			sv.cnclScreenFlag.set("Y");
		}
		cattpfList = cattpfDAO.approvedRecords();
		Chdrpf chdrpfObj = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(), cattpfList.get(0).getChdrnum());
		wsaaCount.set(1);
		loadSubfile1002();
		if (cattpfList.isEmpty()) {			
			scrnparams.subfileMore.set(SPACES);
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
		if(null != chdrpfObj) {
			Descpf descpfObjStatus= descDAO.getdescData(it,  "T3623", chdrpfObj.getStatcode(), wsspcomn.company.toString(),
					wsspcomn.language.toString());
			sv.conStatus.set(descpfObjStatus.getDescitem().trim().concat(" - ").concat(descpfObjStatus.getLongdesc().trim()));
		}
		wsaaClaimNumbersearch.set(SPACES);
		wsaaContractNumbersearch.set(SPACES);
		wsaaOwnersearch.set(SPACES);
		wsaaContactDatesearch.set(999999999);
		wsaaClaimTypesearch.set(SPACES);
		sv.conOwnerOut[Varcom.pr.toInt()].set("Y"); 
	}
	
	protected void loadSubfile1002()
	{
		if (isEQ(wsaaCount,10)) {
			wsaaCount.set(20);
			return;
		}
		if(cattpfList.isEmpty()) {
			loadEmptySubfile1003();
		}
		else {
			for(int iy=0 ; iy<cattpfList.size() ; iy++) {
				
						sv.claimNum.set(cattpfList.get(iy).getClaim());
						sv.chdrnum.set(cattpfList.get(iy).getChdrnum());
						Clntpf clntpfObj = clntpfDAO.getClntpf(cn, wsspcomn.fsuco.toString(), cattpfList.get(iy).getLifcnum());
					getDetails(iy, clntpfObj);
					scrnparams.function.set(Varcom.sadd);
					screenIo9000();
				
				wsaaCount.add(1);
			}
		}
	}

	protected void getDetails(int iy, Clntpf clntpfObj) {
		if(null != clntpfObj)
			sv.clntId.set(clntpfObj.getClntnum().concat(",").concat(clntpfObj.getLsurname().concat(" ")
					.concat(clntpfObj.getLgivname())));		
		sv.contactDate.set(cattpfList.get(iy).getCondte());
		Descpf descpfObj = descDAO.getdescData(it, ta522, cattpfList.get(iy).getClamtyp(), 
				wsspcomn.company.toString(),  wsspcomn.language.toString());
		sv.claimType.set(descpfObj.getLongdesc());
		sv.seqenum.set(wsaaCount);
	}

	protected void loadEmptySubfile1003() {
		sv.seqenum.set(SPACES);
		sv.claimNum.set(SPACES);
		sv.chdrnum.set(SPACES);
		sv.clntId.set(SPACES);
		sv.contactDate.set(varcom.maxdate);
		sv.claimType.set(SPACES);
		scrnparams.function.set(Varcom.sadd);
		screenIo9000();
		wsaaCount.add(1);
	}
	
	@Override
	protected void preScreenEdit() {
		//no changes are required
	}
	
	protected void screenIo9000()
	{
		processScreen(SJL52, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	@Override
	protected void screenEdit2000() {
		scrnparams.subfileRrn.set(1); 
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		scrnparams.function.set(Varcom.srnch);
		processScreen(SJL52, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
			&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		checkForRecordSelect2030();
		if (wsaaRecordFound.isTrue() || isFilterSame) {
			isFilterSame = false;
			return;
		}
		filterSearch();
		saveNewFilters2060();
	}
	
	protected void checkForRecordSelect2030()
	{
		wsaaNoRecordFound.setTrue();
		while ( !(wsaaRecordFound.isTrue()
		|| isEQ(scrnparams.statuz, Varcom.endp))) {
			checkForSelection2100();
		}
		
		if (wsaaRecordFound.isTrue()) {
			loadWsspData2200();
			return;
		}
		checkFilterIfSame();
	}
	
	private void checkFilterIfSame() {
		
		boolean isFilterSame1 = false;
		if (isEQ(sv.claimFrom, wsaaClaimNumbersearch)
		&& isEQ(sv.conFrom, wsaaContractNumbersearch)
		&& isEQ(sv.clttwo, wsaaOwnersearch)) {
			isFilterSame1 = true;
		}
		if(isFilterSame1 && isEQ(sv.conDate, wsaaContactDatesearch)
		&& isEQ(sv.claimTypeList, wsaaClaimTypesearch)){
			isFilterSame = true;
		}
	}
	
	protected void checkForSelection2100()
	{
		if (isNE(sv.select, SPACES)) {
			wsaaRecordFound.setTrue();
		}
	}
	
	protected void loadWsspData2200()
	{
		check2200();
	}

	protected void check2200()
	{
		wsspwindow.value.set(sv.claimNum);
		wsspwindow.confirmation.set(sv.clntId);
	}

	protected void filterSearch() {
		scrnparams.function.set(Varcom.sclr);
		screenIo9000();
		
		List<Cattpf> filteredList = new ArrayList<>();
		
		String claimFromValue = sv.claimFrom.toString().trim();
		
		boolean isclaimFromEmpty = false;
		if(("".equalsIgnoreCase(sv.claimFrom.toString().trim()) || (sv.claimFrom.toString().trim().equals("000000000")))) {
			 isclaimFromEmpty = true;
		}
		
		String conFromValue = sv.conFrom.toString().trim();
		
		boolean isconFromEmpty = false;
		if(("".equalsIgnoreCase(sv.conFrom.toString().trim()) || (sv.conFrom.toString().trim().equals("00000000")))) {
			 isconFromEmpty = true;
		}
		
		String clttwoValue = sv.clttwo.toString().trim();
		
		boolean isPayeeEmpty = false;
		if(("".equalsIgnoreCase(sv.clttwo.toString().trim()) || (sv.clttwo.toString().trim().equals(zero)))) {
			isPayeeEmpty = true;
		}
		
		String conDateDispStr = sv.conDate.toString().trim();
		
		boolean isconDateDispEmpty = false;
		if("".equalsIgnoreCase(conDateDispStr) || "99999999".equalsIgnoreCase(conDateDispStr)) {
			isconDateDispEmpty = true;
		}
		
		String claimTypeListStr = sv.claimTypeList.toString().trim();
			
		boolean isclaimTypeListEmpty = "".equalsIgnoreCase(claimTypeListStr);
		boolean combinedfilter = false;
		
		if(isclaimFromEmpty && isconFromEmpty && isconDateDispEmpty) {
			combinedfilter = true;
		} 
		if(combinedfilter && isPayeeEmpty && isclaimTypeListEmpty){
			wsaaCount.set(1);
			while ( !(isGT(wsaaCount,10))) {
				loadSubfile1002();
			}
			return;
		}else{
			for(Cattpf cattpfrec : cattpfList){
				boolean isFilterApplied = false;
					if ((isclaimFromEmpty || (Integer.parseInt(claimFromValue) <= Integer.valueOf(cattpfrec.getClaim().trim()))) 
						&& (isconDateDispEmpty || (cattpfrec.getCondte().equals(Integer.valueOf(conDateDispStr))))) {
						 isFilterApplied = true;
					}
					
					if (isFilterApplied && (isconFromEmpty || (Integer.parseInt(conFromValue) 
							<= Integer.parseInt(cattpfrec.getChdrnum().trim())))) {
						isFilterApplied = true;
					}
					else {
						isFilterApplied = false;
					}
					
					if(isFilterApplied && (isPayeeEmpty || (isEQ(clttwoValue,Integer.parseInt(cattpfrec.getLifcnum().trim()))))) {
						isFilterApplied = true;
					}
					else {
						isFilterApplied = false;
					}
					
					if(isFilterApplied && (isclaimTypeListEmpty || (cattpfrec.getClamtyp().equals(claimTypeListStr))))
					{
						filteredList.add(cattpfrec);
					}
				}
			}
		processCattpf(filteredList);
		wsspcomn.edterror.set("Y");
	}
	
	protected void saveNewFilters2060()
	{
		wsaaClaimNumbersearch.set(sv.claimFrom);
		wsaaContractNumbersearch.set(sv.conFrom);
		wsaaOwnersearch.set(sv.clttwo);
		wsaaContactDatesearch.set(sv.conDate);
		wsaaClaimTypesearch.set(sv.claimTypeList);
		wsspcomn.edterror.set("Y");
		
		if(!sv.clttwo.toString().trim().equals("")) {
			wsspcomn.longconfname.set(SPACES);
			Clntpf clntpfObj = clntpfDAO.getClntpf(cn, wsspcomn.fsuco.toString(), sv.clttwo.toString().trim());
			if(null != clntpfObj)
				wsspcomn.longconfname.set(clntpfObj.getLsurname().trim().concat(" ").concat(clntpfObj.getLgivname().trim()));	
			sv.conOwner.set(wsspcomn.longconfname);
		}
		if(sv.clttwo.toString().trim().equals("")) {
			sv.conOwner.set(SPACES);
		}
	}
	
	protected void processCattpf(List<Cattpf> cattpfTempList) {
		wsaaCount.set(ZERO);
		for(Cattpf catt : cattpfTempList) {
			cattpfTemp = catt;
			processCattpf1100();
		}
	} 
	
	protected void processCattpf1100()
	{
		para1200();
		writeSubfile1300();
	}
	
	protected void para1200() {
		sv.claimNum.set(cattpfTemp.getClaim());
		sv.chdrnum.set(cattpfTemp.getChdrnum());
		Clntpf clntpfObj = clntpfDAO.getClntpf(cn, wsspcomn.fsuco.toString(), cattpfTemp.getLifcnum().trim());
		if(null != clntpfObj) {
			sv.clntId.set(clntpfObj.getClntnum().concat(",").concat(clntpfObj.getLsurname().concat(" ")
					.concat(clntpfObj.getLgivname())));	
		}
		Descpf descpfObj = descDAO.getdescData(it, ta522, cattpfTemp.getClamtyp(), 
				wsspcomn.company.toString(),  wsspcomn.language.toString());
		if(null != descpfObj) {
			sv.claimType.set(descpfObj.getLongdesc());
		}	
		sv.contactDate.set(cattpfTemp.getCondte());
		wsaaCount.add(1);
		sv.seqenum.set(wsaaCount);
	}
	
	protected void writeSubfile1300()
	{
		scrnparams.function.set(Varcom.sadd);
		processScreen(SJL52, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	@Override
	protected void update3000() {
		/*UPDATE-DATABASE*/
		/**  No database updates are required.*/
		/*EXIT*/
	}
	
	@Override
	protected void whereNext4000()	{
		wsspcomn.nextprog.set(SPACES);
	}
	
}