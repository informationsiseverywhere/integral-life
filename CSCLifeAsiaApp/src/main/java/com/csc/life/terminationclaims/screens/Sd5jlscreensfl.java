package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 04/12/18
 * @author Quipoz
 */
public class Sd5jlscreensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 30;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {37,  38,  39,  40,  41,  42,  43,  44,  45,  47, 48, 50, 53, 54,55, 57, 61,  62,  63,  64,  65,  66, 68, 70}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 19, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5jlScreenVars sv = (Sd5jlScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sd5jlscreensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sd5jlscreensfl, 
			sv.Sd5jlscreensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sd5jlScreenVars sv = (Sd5jlScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sd5jlscreensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sd5jlScreenVars sv = (Sd5jlScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sd5jlscreensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sd5jlscreensflWritten.gt(0))
		{
			sv.sd5jlscreensfl.setCurrentIndex(0);
			sv.Sd5jlscreensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sd5jlScreenVars sv = (Sd5jlScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sd5jlscreensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5jlScreenVars screenVars = (Sd5jlScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.cltreln.setFieldName("cltreln");
				screenVars.bnycd.setFieldName("bnycd");
				screenVars.bnypc.setFieldName("bnypc");
				screenVars.bnysel.setFieldName("bnysel");
				screenVars.clntsname.setFieldName("clntsname");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.bnytype.setFieldName("bnytype");
				screenVars.relto.setFieldName("relto");
				screenVars.revcflg.setFieldName("revcflg");
				screenVars.enddateDisp.setFieldName("enddateDisp");/*ILIFE-3581*/
				screenVars.sequence.setFieldName("sequence");/*ICIL-11*/
				screenVars.paymthbf.setFieldName("paymthbf");/*ICIL-11*/
				screenVars.bankkey.setFieldName("bankkey");/*ICIL-11*/
				screenVars.bankacckey.setFieldName("bankacckey");/*ICIL-11*/
				screenVars.bnkcdedsc.setFieldName("bnkcdedsc");/*ICIL-11*/
				screenVars.bkrelackey.setFieldName("bkrelackey");/*ICIL-11*/
				screenVars.amount.setFieldName("amount");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.cltreln.set(dm.getField("cltreln"));
			screenVars.bnycd.set(dm.getField("bnycd"));
			screenVars.bnypc.set(dm.getField("bnypc"));
			screenVars.bnysel.set(dm.getField("bnysel"));
			screenVars.clntsname.set(dm.getField("clntsname"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.bnytype.set(dm.getField("bnytype"));
			screenVars.relto.set(dm.getField("relto"));
			screenVars.revcflg.set(dm.getField("revcflg"));
			screenVars.enddateDisp.set(dm.getField("enddateDisp"));/*ILIFE-3581*/
			screenVars.sequence.set(dm.getField("sequence"));/*ICIL-11*/
			screenVars.paymthbf.set(dm.getField("paymthbf"));/*ICIL-11*/
			screenVars.bankkey.set(dm.getField("bankkey"));/*ICIL-11*/
			screenVars.bankacckey.set(dm.getField("bankacckey"));/*ICIL-11*/
			screenVars.bnkcdedsc.set(dm.getField("bnkcdedsc"));/*ICIL-11*/
			screenVars.bkrelackey.set(dm.getField("bkrelackey"));/*ICIL-11*/
			screenVars.amount.set(dm.getField("amount"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5jlScreenVars screenVars = (Sd5jlScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.cltreln.setFieldName("cltreln");
				screenVars.bnycd.setFieldName("bnycd");
				screenVars.bnypc.setFieldName("bnypc");
				screenVars.bnysel.setFieldName("bnysel");
				screenVars.clntsname.setFieldName("clntsname");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.bnytype.setFieldName("bnytype");
				screenVars.relto.setFieldName("relto");
				screenVars.revcflg.setFieldName("revcflg");
				screenVars.enddateDisp.setFieldName("enddateDisp");/*ILIFE-3581*/
				screenVars.sequence.setFieldName("sequence");/*ICIL-11*/
				screenVars.paymthbf.setFieldName("paymthbf");/*ICIL-11*/
				screenVars.bankkey.setFieldName("bankkey");/*ICIL-11*/
				screenVars.bankacckey.setFieldName("bankacckey");/*ICIL-11*/
				screenVars.bnkcdedsc.setFieldName("bnkcdedsc");/*ICIL-11*/
				screenVars.bkrelackey.setFieldName("bkrelackey");/*ICIL-11*/
				screenVars.amount.setFieldName("amount");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("cltreln").set(screenVars.cltreln);
			dm.getField("bnycd").set(screenVars.bnycd);
			dm.getField("bnypc").set(screenVars.bnypc);
			dm.getField("bnysel").set(screenVars.bnysel);
			dm.getField("clntsname").set(screenVars.clntsname);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("bnytype").set(screenVars.bnytype);
			dm.getField("relto").set(screenVars.relto);
			dm.getField("revcflg").set(screenVars.revcflg);
			dm.getField("enddateDisp").set(screenVars.enddateDisp);/*ILIFE-3581*/
			dm.getField("sequence").set(screenVars.sequence);/*ICIL-11*/
			dm.getField("paymthbf").set(screenVars.paymthbf);/*ICIL-11*/
			dm.getField("bankkey").set(screenVars.bankkey);/*ICIL-11*/
			dm.getField("bankacckey").set(screenVars.bankacckey);/*ICIL-11*/
			dm.getField("bnkcdedsc").set(screenVars.bnkcdedsc);/*ICIL-11*/
			dm.getField("bkrelackey").set(screenVars.bkrelackey);/*ICIL-11*/
			dm.getField("amount").set(screenVars.amount);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sd5jlscreensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sd5jlScreenVars screenVars = (Sd5jlScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.cltreln.clearFormatting();
		screenVars.bnycd.clearFormatting();
		screenVars.bnypc.clearFormatting();
		screenVars.bnysel.clearFormatting();
		screenVars.clntsname.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.bnytype.clearFormatting();
		screenVars.relto.clearFormatting();
		screenVars.revcflg.clearFormatting();
		screenVars.enddateDisp.clearFormatting();/*ILIFE-3581*/
		screenVars.sequence.clearFormatting();/*ICIL-11*/
		screenVars.paymthbf.clearFormatting();/*ICIL-11*/
		screenVars.bankkey.clearFormatting();/*ICIL-11*/
		screenVars.bankacckey.clearFormatting();/*ICIL-11*/
		screenVars.bnkcdedsc.clearFormatting();/*ICIL-11*/
		screenVars.bkrelackey.clearFormatting();/*ICIL-11*/
		screenVars.amount.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sd5jlScreenVars screenVars = (Sd5jlScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.cltreln.setClassString("");
		screenVars.bnycd.setClassString("");
		screenVars.bnypc.setClassString("");
		screenVars.bnysel.setClassString("");
		screenVars.clntsname.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.bnytype.setClassString("");
		screenVars.relto.setClassString("");
		screenVars.revcflg.setClassString("");
		screenVars.enddateDisp.setClassString("");/*ILIFE-3581*/
		screenVars.sequence.setClassString("");/*ICIL-11*/
		screenVars.paymthbf.setClassString("");/*ICIL-11*/
		screenVars.bankkey.setClassString("");/*ICIL-11*/
		screenVars.bankacckey.setClassString("");/*ICIL-11*/
		screenVars.bnkcdedsc.setClassString("");/*ICIL-11*/
		screenVars.bkrelackey.setClassString("");/*ICIL-11*/
		screenVars.amount.setClassString("");
	}

/**
 * Clear all the variables in Sd5jlscreensfl
 */
	public static void clear(VarModel pv) {
		Sd5jlScreenVars screenVars = (Sd5jlScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.cltreln.clear();
		screenVars.bnycd.clear();
		screenVars.bnypc.clear();
		screenVars.bnysel.clear();
		screenVars.clntsname.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.bnytype.clear();
		screenVars.relto.clear();
		screenVars.revcflg.clear();
		screenVars.enddateDisp.clear();/*ILIFE-3581*/
		screenVars.enddate.clear();
		screenVars.sequence.clear();/*ICIL-11*/
		screenVars.paymthbf.clear();/*ICIL-11*/
		screenVars.bankkey.clear();/*ICIL-11*/
		screenVars.bankacckey.clear();/*ICIL-11*/
		screenVars.bnkcdedsc.clear();/*ICIL-11*/
		screenVars.bkrelackey.clear();/*ICIL-11*/
		screenVars.amount.clear();
	}
}
