/*
 * File: P5015.java
 * Date: 29 August 2009 23:55:17
 * Author: Quipoz Limited
 * 
 * Class transformed from P5015.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifesurTableDAM;
import com.csc.life.terminationclaims.screens.S5015ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P5015 - Plan Policy Select.
* ---------------------------
*
* This screen,  S5015  is used to select a  Plan  (i.e.  number  of
* policies  is   greater than one) or one or many policies within a
* Plan.  If  it  is  not  a  Plan,   then    this  screen  will  be
* bypassed  and the policy for selection is defaulted to the one on
* file.
*
* Initialise
* ----------
*
* If the current stack entry is '*', then skip this section.
*
* Clear the subfile ready for loading.
*
* Read CHDRSUR (RETRV)  in  order to  obtain  the  contract  header
* information.   If  the  number of policies in the plan is zero or
* one then  Plan-processing  does not apply. If there is any  other
* numeric    value   present,  this  value will indicate the number
* of    policies    in    the  Plan.  If  Plan  processing  is  not
* applicable, do not load the screen (i.e. skip this section).
*
* For  Plan  processing,    the  number of policies within the Plan
* are  displayed  in  descending  sequence.    The  first  position
* remains    blank  (spaces)  and if this option is selected by the
* user, the entire Plan is selected.
*
* The status/description  for  each of the policies is obtained  as
* follows:-
*
* Read    the    first "COVR" record for this Policy and output the
* coverage  and premium  status  codes.  Look-up    the    coverage
* status  description  for  the  subfile  entry  and  output on the
* screen.
*
* Obtain  the  status codes required by reading table  T5679,  i.e.
* the   'Component Statuses For Transactions'.  This table is keyed
* by transaction number.
*
* Check the status code of the coverage  and  premium  against  the
* code  applicable    to   it  on T5679. If the codes do not match,
* then protect the selection field on that policy.
*
* While  loading  the  subfile,  if it is  only  possible  for  the
* user    to    select 1 policy, skip looking up any other details,
* displaying the screen etc., and process as  if  the  entire  Plan
* was originally selected for processing.
*
* LIFE DETAILS
*
* Obtain the life assured and joint-life details (if any):
*
*      - read  the life details using LIFESUR (life number from
*               CHDRSUR, joint life number '00').  Look up the name
*               from the  client  details  (CLTS)  and  format as a
*               "confirmation name".
*
*      - read the joint life details using LIFESUR (life number
*               from CHDRSUR,  joint  life number '01').  If found,
*               look up the name from the client details (CLTS) and
*               format as a "confirmation name".
*
* Obtain  the    necessary   descriptions  by  calling 'DESCIO' and
* output the descriptions of the statuses to the screen.
*
* Output the remaining relevant  data to  the  screen,  the  status
* descriptions,  the  Owner  number  and name (look-up the CLTS and
* format the name), CCD, Paid-to-date and the Bill-to-date.
*
* In all cases, load all  pages required in  the  subfile  and  set
* the subfile indicator to no.
*
* The above details are returned to the user.
*
* Validation
* ----------
*
* If    Plan    processing  is  not  applicable, or only one policy
* remains  for  selection, the screen is  not  displayed,  so  skip
* this section.
*
* Skip    this    section  if  returning  from  a  Policy selection
* (current stack position action flag = '*').
*
*     If KILL   was   entered,  then  skip  the  remainder  of  the
*     validation and blank out the correct program on the stack and
*     exit from the program.
*
* Read all modified records  and  if this is  a  Plan  (see  above)
* and    the    first  subfile  entry  is selected (i.e. select the
* complete   Plan)  then  no  other  selection  must    be    made.
* Otherwise    this is an error and return the screen and request a
* re-selection.
*
* If  a  Plan  is  being processed and some  of  the  policies  are
* protected    (i.e.  cannot  be selected) and the user selects the
* remaining policies, then this selection is  treated  as  if   the
* entire Plan was selected.
*
* Updating
* --------
*
* This program does no updating.
*
* Next Program
* ------------
*
* If not returning from a policy selection:
*
* If  part  surrendering  over  the entire Plan, "KEEPS" the key of
* the first coverage/rider with a suffix of 0. Call  "GENSSW"  with
* an  action  of  "A"  load  the returned programs into the program
* stack. Add 1 to the program pointer and exit.
*
* Otherwise, one or many policies are being surrendered.
*
* Call "GENSSW" with  an  action  of  "B"  and  load  the  returned
* programs  into the program stack. Set the stack option to '*' and
* start reading the subfile from the beginning.
*
* When a selected policy  is  found,   "KEEPS"  its  COVR  details,
* add 1 to the program pointer and exit.
*
* When  the  end  of  the subfile is reached, call "GENSSW" with an
* action of "E" and load the returned  programs  into  the  program
* stack. Add 1 to the program pointer and exit.
*
* Modifications
* -------------
*
* Include the following logical views:
*
*           - CHDRSUR
*           - LIFESUR
*           - COVRSUR
*
*****************************************************************
* </pre>
*/
public class P5015 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5015");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(ZERO);
	private ZonedDecimalData wsaaSavePlanSuffix = new ZonedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaPoliciesProtected = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaCovrTimes = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaSelected = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0).init(ZERO);
	private ZonedDecimalData wsccSub1 = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaPlan = new FixedLengthStringData(1).init(SPACES);
	private Validator planNotSelected = new Validator(wsaaPlan, " ");
	private Validator planSelected = new Validator(wsaaPlan, "Y");
	private ZonedDecimalData wsccSub2 = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaActualWritten = new PackedDecimalData(3, 0).init(ZERO);
	private String wsaaCovStatus = "";
	private String wsaaPremStatus = "";

	private FixedLengthStringData wsaaTable = new FixedLengthStringData(300);
	private PackedDecimalData[] wsaaPlnSuff = PDArrayPartOfStructure(100, 4, 0, wsaaTable, 0);
	private PackedDecimalData suffSub1 = new PackedDecimalData(4, 0);
	private PackedDecimalData suffSub2 = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsaaValidSuff = new FixedLengthStringData(1).init("Y");
	private Validator validSuffix = new Validator(wsaaValidSuff, "Y");

	private FixedLengthStringData wsaaEofFlag = new FixedLengthStringData(1);
	private Validator wsaaEofExit = new Validator(wsaaEofFlag, "Y");
		/* ERRORS */
	private String g633 = "G633";
	private String g634 = "G634";
	private String e304 = "E304";
	private String h080 = "H080";
	private String h093 = "H093";
		/* TABLES */
	private String t5679 = "T5679";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String t5682 = "T5682";
	private String t5688 = "T5688";
		/* FORMATS */
	private String chdrsurrec = "CHDRSURREC";
		/*Contract Header - Surrenders*/
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage and Rider Details - Full Surren*/
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life logical file - full surrender*/
	private LifesurTableDAM lifesurIO = new LifesurTableDAM();
	private T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5015ScreenVars sv = ScreenProgram.getScreenVars( S5015ScreenVars.class);
	private CovrpfDAO covrSurdao =getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrSur=null;
	List<Covrpf> covrSurList=null;
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private List<Descpf> t5682List;
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		addToSubfile1230, 
		exit1249, 
		suffix1295, 
		exit1295, 
		exit1298, 
		search1410, 
		search1510, 
		readJlife1610, 
		exit1690, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		updateErrorIndicators2670, 
		bypass4020, 
		exit4090, 
		exit4110
	}

	public P5015() {
		super();
		screenVars = sv;
		new ScreenModel("S5015", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		scrnparams.function.set(varcom.sclr);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.planSuffix.set(ZERO);
		initialize(wsaaTable);
		suffSub1.set(ZERO);
		suffSub2.set(ZERO);
		wsaaEofFlag.set(SPACES);
		chdrsurIO.setFunction("RETRV");
		chdrsurIO.setFormat(chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		if (isLTE(chdrsurIO.getPolinc(),1)) {
			goTo(GotoLabel.exit1090);
		}
		checkStatus1100();
		sv.planSuffix.set(0);
		if (isNE(chdrsurIO.getPolsum(),chdrsurIO.getPolinc())) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.set(chdrsurIO.getPolinc());
		/*covrsurIO.setDataArea(SPACES);
		covrsurIO.setChdrcoy(wsspcomn.company);
		covrsurIO.setChdrnum(chdrsurIO.getChdrnum());
		covrsurIO.setPlanSuffix("9999");
		covrsurIO.setLife("01");
		covrsurIO.setCoverage("01");
		covrsurIO.setRider("00");
		covrsurIO.setFunction("ENDR");*/

		t5682List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5682");/* IJTI-1523 */
		covrSur=new Covrpf();
		covrSur.setChdrcoy(chdrsurIO.getChdrcoy().toString().trim());
		covrSur.setChdrnum(chdrsurIO.getChdrnum().toString().trim());
		covrSur.setLife("01");
		covrSur.setCoverage("01");
		covrSur.setRider("00");
		covrSur.setPlanSuffix(9999);
		covrSurList=covrSurdao.selectCovrSurData(covrSur);
		for(int intCounter=covrSurList.size()-1;intCounter>=0;intCounter--){
			covrSur=covrSurList.get(intCounter);
			loadCovrSubfile1200();
		}
		/*while ( !(wsaaEofExit.isTrue())) {
			loadCovrSubfile1200();
		}*/
		
		if (isNE(chdrsurIO.getPolsum(),ZERO)) {
			processSummary1150();
		}
		fillScreen1600();
	}

protected void checkStatus1100()
	{
		readStatusTable1110();
	}

protected void readStatusTable1110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void processSummary1150()
	{
		go1151();
	}

protected void go1151()
	{
		/*covrsurIO.setDataArea(SPACES);
		covrsurIO.setChdrcoy(wsspcomn.company);
		covrsurIO.setChdrnum(chdrsurIO.getChdrnum());
		covrsurIO.setPlanSuffix(ZERO);
		covrsurIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, covrsurIO);
		if ((isNE(covrsurIO.getStatuz(),varcom.oK))
		&& (isNE(covrsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,covrsurIO.getChdrcoy())
		|| isNE(chdrsurIO.getChdrnum(),covrsurIO.getChdrnum())
		|| isEQ(covrsurIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();

		}*/
		covrSur=new Covrpf();
		covrSur.setChdrcoy(chdrsurIO.getChdrcoy().toString().trim());
		covrSur.setChdrnum(chdrsurIO.getChdrnum().toString().trim());
		covrSurList=covrSurdao.selectCovrSurData(covrSur);
		covrSur=covrSurList.get(0);
		wsaaSub.set(ZERO);
		sv.selectOut[varcom.pr.toInt()].set("N");
		checkStatusCoverage1400();
		wsaaSub.set(ZERO);
		sv.selectOut[varcom.pr.toInt()].set("N");
		checkStatusPremium1500();

		
		if (isEQ(sv.selectOut[varcom.pr.toInt()],"Y")) {
			//wsaaSavePlanSuffix.set(covrsurIO.getPlanSuffix());
			wsaaSavePlanSuffix.set(covrSur.getPlanSuffix());
		}
		boolean itemFound = false;
		for (Descpf descItem : t5682List) {
			if (descItem.getDescitem().trim().equals(covrSur.getStatcode().trim())){
				sv.statdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.statdesc.fill("?");
		}
		//descIO.setDescitem(covrsurIO.getStatcode());
		/*descIO.setDesctabl(t5682);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.statdesc.set(descIO.getLongdesc());
		}
		else {
			sv.statdesc.fill("?");

		}*/
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(chdrsurIO.getPolsum());
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			loadSummarySubfile1250();
		}
	}

protected void loadCovrSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					go1210();
				}
				case addToSubfile1230: {
					addToSubfile1230();
				}
				case exit1249: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go1210()
	{
		/*SmartFileCode.execute(appVars, covrsurIO);
		if ((isNE(covrsurIO.getStatuz(),varcom.oK))
		&& (isNE(covrsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,covrsurIO.getChdrcoy())
		|| isNE(chdrsurIO.getChdrnum(),covrsurIO.getChdrnum())) {
			wsaaEofFlag.set("Y");
			goTo(GotoLabel.exit1249);
		}
		if (isEQ(covrsurIO.getPlanSuffix(),ZERO)) {
			covrsurIO.setFunction(varcom.nextp);
			goTo(GotoLabel.exit1249);
		}
		sv.planSuffix.set(covrsurIO.getPlanSuffix());*/
		if(covrSur.getPlanSuffix()==0){
			return;
		}
	    sv.planSuffix.set(covrSur.getPlanSuffix());
		wsaaSub.set(ZERO);
		wsaaCovStatus = "N";
		wsaaPremStatus = "N";
		sv.selectOut[varcom.pr.toInt()].set("N");
		wsaaCovStatus = "N";
		checkStatusCoverage1400();
		wsaaSub.set(ZERO);
		sv.selectOut[varcom.pr.toInt()].set("N");
		wsaaPremStatus = "N";
		checkStatusPremium1500();
		if (isEQ(sv.selectOut[varcom.pr.toInt()],"Y")) {
			wsaaSavePlanSuffix.set(sv.planSuffix);
		}
		boolean itemFound = false;
		for (Descpf descItem : t5682List) {
			if (descItem.getDescitem().trim().equals(covrSur.getStatcode().trim())){
				sv.statdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.statdesc.fill("?");
		}
		/*descIO.setDescitem(covrsurIO.getStatcode());
		descIO.setDesctabl(t5682);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.statdesc.set(descIO.getLongdesc());
		}
		else {
			sv.statdesc.fill("?");

		}*/
		suffSub2.set(ZERO);
		wsaaValidSuff.set("N");
		checkSuffix1295();
		if (isEQ(wsaaCovStatus,"Y")
		&& isEQ(wsaaPremStatus,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set(SPACES);
			if (validSuffix.isTrue()) {
				subfileUpdate1297();
				goTo(GotoLabel.exit1249);
			}
			else {
				goTo(GotoLabel.addToSubfile1230);
			}
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		if (validSuffix.isTrue()) {
			goTo(GotoLabel.exit1249);
		}
	}

protected void addToSubfile1230()
	{
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			wsaaActualWritten.add(1);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.subtract(1);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		suffSub1.add(1);
		/*wsaaPlnSuff[suffSub1.toInt()].set(covrsurIO.getPlanSuffix());
		covrsurIO.setFunction(varcom.nextp);*/
		wsaaPlnSuff[suffSub1.toInt()].set(covrSur.getPlanSuffix());
	}

protected void loadSummarySubfile1250()
	{
		/*GO*/
		sv.planSuffix.set(wsaaPlanSuffix);
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			wsaaSavePlanSuffix.set(sv.planSuffix);
			wsaaActualWritten.add(1);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.subtract(1);
		/*EXIT*/
	}

protected void checkSuffix1295()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case suffix1295: {
					suffix1295();
				}
				case exit1295: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void suffix1295()
	{
		suffSub2.add(1);
		if (isEQ(wsaaPlnSuff[suffSub2.toInt()],ZERO)) {
			goTo(GotoLabel.exit1295);
		}
		//if (isEQ(covrsurIO.getPlanSuffix(),wsaaPlnSuff[suffSub2.toInt()])) {
		if (isEQ(covrSur.getPlanSuffix(),wsaaPlnSuff[suffSub2.toInt()])) {
			wsaaValidSuff.set("Y");
			goTo(GotoLabel.exit1295);
		}
		if (isLT(suffSub2,100)) {
			//goTo(GotoLabel.suffix1295);
			suffix1295();
		}
	}

protected void subfileUpdate1297()
	{
		subfileBegin1297();
	}

protected void subfileBegin1297()
	{
		scrnparams.subfileRrn.set(1);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			readSubfile1298();
		}
		
		scrnparams.function.set(varcom.supd);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readSubfile1298()
	{
		try {
			subfileRead1298();
		}
		catch (GOTOException e){
		}
	}

protected void subfileRead1298()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		//if (isNE(sv.planSuffix,covrsurIO.getPlanSuffix())) {
		if (isNE(sv.planSuffix,covrSur.getPlanSuffix())) {
			goTo(GotoLabel.exit1298);
		}
		scrnparams.statuz.set(varcom.endp);
		/*sv.stycvr.set(covrsurIO.getStatcode());
		sv.pstatcode.set(covrsurIO.getPstatcode());*/
		sv.stycvr.set(covrSur.getStatcode());
		sv.pstatcode.set(covrSur.getPstatcode());
		/*descIO.setDescitem(covrsurIO.getStatcode());
		descIO.setDesctabl(t5682);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.statdesc.set(descIO.getLongdesc());
		}
		else {
			sv.statdesc.fill("?");
		}*/
		boolean itemFound = false;
		for (Descpf descItem : t5682List) {
			if (descItem.getDescitem().trim().equals(covrSur.getStatcode().trim())){
				sv.statdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.statdesc.fill("?");
		}
	}

protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkStatusCoverage1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case search1410: {
					search1410();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void search1410()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			//sv.stycvr.set(covrsurIO.getStatcode());
			sv.stycvr.set(covrSur.getStatcode());
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			//if (isNE(covrsurIO.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
			if (isNE(covrSur.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
				//goTo(GotoLabel.search1410);
				search1410();
			}
			else {
				wsaaCovStatus = "Y";
				//sv.stycvr.set(covrsurIO.getStatcode());
				sv.stycvr.set(covrSur.getStatcode());
			}
		}
		/*EXIT*/
	}

protected void checkStatusPremium1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case search1510: {
					search1510();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void search1510()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			//sv.pstatcode.set(covrsurIO.getPstatcode());
			sv.pstatcode.set(covrSur.getPstatcode());
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			//if (isNE(covrsurIO.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
			if (isNE(covrSur.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
				//goTo(GotoLabel.search1510);
				search1510();
			}
			else {
				wsaaPremStatus = "Y";
				//sv.pstatcode.set(covrsurIO.getPstatcode());
				sv.pstatcode.set(covrSur.getPstatcode());
			}
		}
		/*EXIT*/
	}

protected void fillScreen1600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					headings1610();
				}
				case readJlife1610: {
					readJlife1610();
				}
				case exit1690: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void headings1610()
	{
		sv.occdate.set(chdrsurIO.getOccdate());
		sv.chdrnum.set(chdrsurIO.getChdrnum());
		sv.cnttype.set(chdrsurIO.getCnttype());
		descIO.setDescitem(chdrsurIO.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrsurIO.getCownnum());
		cltsIO.setClntnum(chdrsurIO.getCownnum());
		getClientDetails1700();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernumErr.set(e304);
			sv.ownernum.set(SPACES);
		}
		else {
			plainname();
			sv.ownernum.set(wsspcomn.longconfname);
		}
		sv.btdate.set(chdrsurIO.getBtdate());
		sv.ptdate.set(chdrsurIO.getPtdate());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrsurIO.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrsurIO.getPstatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		lifesurIO.setDataArea(SPACES);
		lifesurIO.setChdrnum(chdrsurIO.getChdrnum());
		lifesurIO.setChdrcoy(chdrsurIO.getChdrcoy());
		//lifesurIO.setLife(covrsurIO.getLife());
		lifesurIO.setLife(covrSur.getLife());
		lifesurIO.setJlife("00");
		lifesurIO.setFunction("READR");
		SmartFileCode.execute(appVars, lifesurIO);
		if (isNE(lifesurIO.getStatuz(),varcom.oK)
		&& isNE(lifesurIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		if (isEQ(lifesurIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.readJlife1610);
		}
		sv.lifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1700();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
	}

protected void readJlife1610()
	{
		lifesurIO.setJlife("01");
		lifesurIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifesurIO);
		if ((isNE(lifesurIO.getStatuz(),varcom.oK))
		&& (isNE(lifesurIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(lifesurIO.getParams());
			fatalError600();
		}
		if (isEQ(lifesurIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.exit1690);
		}
		sv.jlifcnum.set(lifesurIO.getLifcnum());
		cltsIO.setClntnum(lifesurIO.getLifcnum());
		getClientDetails1700();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.jlinsnameErr.set(e304);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
	}

protected void getClientDetails1700()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsaaSelected.set(0);
		wsaaPlan.set(SPACES);
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(wsaaActualWritten,1)
		|| isLTE(chdrsurIO.getPolinc(),1)) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateSubfile2010()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.select,SPACES)) {
			wsaaPlan.set("Y");
		}
		if (isNE(sv.select,SPACES)
		&& isLTE(chdrsurIO.getPolinc(),1)) {
			scrnparams.errorCode.set(h080);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		sv.selectOut[varcom.pr.toInt()].set("Y");
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		scrnparams.subfileRrn.set(1);
		if (planSelected.isTrue()
		&& isGT(wsaaSelected,1)) {
			scrnparams.errorCode.set(g633);
			wsspcomn.edterror.set("Y");
		}
		if (planNotSelected.isTrue()
		&& isEQ(wsaaSelected,0)) {
			scrnparams.errorCode.set(g634);
			wsspcomn.edterror.set("Y");
		}
		compute(wsaaPoliciesProtected, 0).set(sub(chdrsurIO.getPolinc(),wsaaActualWritten));
		if (isGT(chdrsurIO.getPolinc(),1)) {
			if (isEQ(wsaaPoliciesProtected,wsaaActualWritten)) {
				wsaaPlan.set("Y");
			}
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2610();
					updateLineProtection2620();
				}
				case updateErrorIndicators2670: {
					updateErrorIndicators2670();
					readNextRecord2680();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		if (isNE(sv.select,SPACES)) {
			wsaaSelected.add(1);
		}
	}

protected void updateLineProtection2620()
	{
		if (isEQ(sv.planSuffix,"0000")) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub,12)
		|| isEQ(sv.stycvr,t5679rec.covRiskStat[wsaaSub.toInt()]))) {
			wsaaSub.add(1);
		}
		
		if (isGT(wsaaSub,12)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub,12)
		|| isEQ(sv.pstatcode,t5679rec.covPremStat[wsaaSub.toInt()]))) {
			wsaaSub.add(1);
		}
		
		if (isGT(wsaaSub,12)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case bypass4020: {
					bypass4020();
				}
				case exit4090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			goTo(GotoLabel.bypass4020);
		}
		if (isLTE(chdrsurIO.getPolinc(),1)
		|| isLTE(wsaaActualWritten,1)) {
			keepsCovr4400();
			gensswrec.function.set("A");
			callGenssw4100();
			goTo(GotoLabel.exit4090);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.select,SPACES)) {
			keepsCovr4400();
			gensswrec.function.set("A");
			callGenssw4100();
			goTo(GotoLabel.exit4090);
		}
	}

protected void bypass4020()
	{
		if (isEQ(sv.select,SPACES)) {
			while ( !(isNE(sv.select,SPACES)
			|| isEQ(scrnparams.statuz,varcom.endp))) {
				readSubfile4800();
			}
			
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			gensswrec.function.set("E");
			callGenssw4100();
			goTo(GotoLabel.exit4090);
		}
		keepsCovr4400();
		gensswrec.function.set("B");
		callGenssw4100();
		sv.select.set(SPACES);
	}

protected void callGenssw4100()
	{
		try {
			callGenssw4110();
		}
		catch (GOTOException e){
		}
	}

protected void callGenssw4110()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz,varcom.oK))
		&& (isNE(gensswrec.statuz,varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4110);
		}
		compute(wsccSub1, 0).set(add(1,wsspcomn.programPtr));
		wsccSub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			gensToWsspProgs4300();
		}
		if (isEQ(gensswrec.function,"B")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		}
		else {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		}
		wsspcomn.programPtr.add(1);
	}

protected void gensToWsspProgs4300()
	{
		/*GENS-TO-WSSP-PROGS*/
		wsspcomn.secProg[wsccSub1.toInt()].set(gensswrec.progOut[wsccSub2.toInt()]);
		wsccSub1.add(1);
		wsccSub2.add(1);
		/*EXIT*/
	}

protected void keepsCovr4400()
	{
		go4410();
	}

protected void go4410()
	{
		covrsurIO.setDataArea(SPACES);
		if (isLTE(wsaaActualWritten,1)) {
			covrsurIO.setPlanSuffix(wsaaSavePlanSuffix);
		}
		else {
			if (isLTE(sv.planSuffix,chdrsurIO.getPolsum())) {
				covrsurIO.setPlanSuffix(ZERO);
			}
			else {
				covrsurIO.setPlanSuffix(sv.planSuffix);
			}
		}
		covrsurIO.setChdrcoy(wsspcomn.company);
		covrsurIO.setChdrnum(chdrsurIO.getChdrnum());
		covrsurIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY","CHDRNUM");	
		SmartFileCode.execute(appVars, covrsurIO);
		if ((isNE(covrsurIO.getStatuz(),varcom.oK))
		&& (isNE(covrsurIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,covrsurIO.getChdrcoy())
		|| isNE(chdrsurIO.getChdrnum(),covrsurIO.getChdrnum())
		|| isEQ(covrsurIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		covrsurIO.setFunction(varcom.keeps);
		covrsurIO.setPlanSuffix(sv.planSuffix);
		covrsurIO.setFormat("COVRSURREC");
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
	}

protected void readSubfile4800()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5015", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
