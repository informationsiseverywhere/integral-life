/*
 * File: Pr688.java
 * Date: 30 August 2009 1:56:24
 * Author: Quipoz Limited
 * 
 * Class transformed from PR688.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Wsspdocs;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclaanlTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclaltlTableDAM;
import com.csc.life.terminationclaims.dataaccess.HcldTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclhTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.screens.Sr688ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Tr687rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Itdmkey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
***********************************************************************
* </pre>
*/
public class Pr688 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR688");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTr687Itemitem = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr687Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTr687Itemitem, 0);
	private FixedLengthStringData wsaaTr687Benpln = new FixedLengthStringData(2).isAPartOf(wsaaTr687Itemitem, 4);
		/* WSAA-VARIABLES */
	private ZonedDecimalData ix = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaBenDesc = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBenDesclong = new FixedLengthStringData(29);
	private FixedLengthStringData wsaaShrtDesc = new FixedLengthStringData(29);

	private FixedLengthStringData wsaaLadc = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaL = new FixedLengthStringData(1).isAPartOf(wsaaLadc, 0);
	private FixedLengthStringData wsaaA = new FixedLengthStringData(1).isAPartOf(wsaaLadc, 1);
	private FixedLengthStringData wsaaD = new FixedLengthStringData(1).isAPartOf(wsaaLadc, 2);
	private FixedLengthStringData wsaaC = new FixedLengthStringData(1).isAPartOf(wsaaLadc, 3);
	private FixedLengthStringData wsaaSlt = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaStoreFlag = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaCompYear = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaTotExpense = new ZonedDecimalData(12, 2).setUnsigned();
	private ZonedDecimalData wsaaTotClmamt = new ZonedDecimalData(12, 2).setUnsigned();
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaAad = new ZonedDecimalData(12, 2).setUnsigned();
	private ZonedDecimalData wsaaDeductedAad = new ZonedDecimalData(12, 2).setUnsigned();
	private ZonedDecimalData wsaaTcoiamt = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaYearlmtBal = new ZonedDecimalData(12, 2).setUnsigned();
	private ZonedDecimalData wsaaLifelmtBal = new ZonedDecimalData(12, 2).setUnsigned();
	private String wsaaTable = "";
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private static final String wsaaPayeeName = "PYNMN";

	private FixedLengthStringData wsaaLifelmtFlag = new FixedLengthStringData(1);
	private Validator lifelmtYes = new Validator(wsaaLifelmtFlag, "Y");
	private Validator lifelmtNo = new Validator(wsaaLifelmtFlag, "N");

	private FixedLengthStringData wsaaYearlmtFlag = new FixedLengthStringData(1);
	private Validator yearlmtYes = new Validator(wsaaYearlmtFlag, "Y");
	private Validator yearlmtNo = new Validator(wsaaYearlmtFlag, "N");

	private FixedLengthStringData wsaaAadFlag = new FixedLengthStringData(1);
	private Validator aadYes = new Validator(wsaaAadFlag, "Y");
	private Validator aadNo = new Validator(wsaaAadFlag, "N");
	private static final String g624 = "G624";
	private static final String w293 = "W293";
	private static final String w322 = "W322";
	private static final String w121 = "W121";
	private static final String rphu = "RPHU";
	private static final String rphv = "RPHV";
		/* TABLES */
	private static final String tr687 = "TR687";
	private static final String tr50a = "TR50A";
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
	private HclaanlTableDAM hclaanlIO = new HclaanlTableDAM();
	private HclaltlTableDAM hclaltlIO = new HclaltlTableDAM();
	private HcldTableDAM hcldIO = new HcldTableDAM();
	private HclhTableDAM hclhIO = new HclhTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private Itdmkey wsaaItdmkey = new Itdmkey();
	private Optswchrec optswchrec = new Optswchrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Tr687rec tr687rec = new Tr687rec();
	private Wsspdocs wsspdocs = new Wsspdocs();
	private Sr688ScreenVars sv = ScreenProgram.getScreenVars( Sr688ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readHbnfTplan1040, 
		loadSubfile1050, 
		exit1090, 
		lifePaid1120, 
		validateSubfile2060, 
		optswch4080, 
		exit4090
	}

	public Pr688() {
		super();
		screenVars = sv;
		new ScreenModel("Sr688", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspdocs.userArea = convertAndSetParam(wsspdocs.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspdocs.userArea = convertAndSetParam(wsspdocs.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					retrieveCovr1020();
					readHclh1030();
				case readHbnfTplan1040: 
					readHbnfTplan1040();
				case loadSubfile1050: 
					loadSubfile1050();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			/* Perform a RLSE on HCLH file*/
			hclhIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, hclhIO);
			if (isNE(hclhIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(hclhIO.getParams());
				syserrrec.statuz.set(hclhIO.getStatuz());
				fatalError600();
			}
			wsspcomn.flag.set(wsaaStoreFlag);
			if (isEQ(wsaaSlt, "1")
			|| isEQ(wsaaSlt, "2")
			|| isEQ(wsaaSlt, "3")
			|| isEQ(wsaaSlt, "4")) {
				sv.sel.set(SPACES);
				scrnparams.function.set(varcom.sclr);
				processScreen("SR688", sv);
				if (isNE(scrnparams.statuz, varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
				scrnparams.subfileRrn.set(1);
				goTo(GotoLabel.readHbnfTplan1040);
			}
			wsaaSlt.set(SPACES);
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaScrnStatuz.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR688", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.clamamt.set(ZERO);
		sv.taccamt1.set(ZERO);
		sv.taccamt2.set(ZERO);
		sv.tactexp.set(ZERO);
		sv.tclmamt.set(ZERO);
		sv.tdeduct1.set(ZERO);
		sv.tdeduct2.set(ZERO);
		sv.tdeduct3.set(ZERO);
		sv.zunit.set(ZERO);
		sv.amtlife.set(ZERO);
		sv.amtyear.set(ZERO);
		sv.gdeduct.set(ZERO);
		sv.copay.set(ZERO);
		sv.mbrdeduc.set(ZERO);
		sv.mbrcopay.set(ZERO);
		sv.bnflmtb.set(ZERO);
		sv.bnflmta.set(ZERO);
		wsaaTotExpense.set(ZERO);
		wsaaTotClmamt.set(ZERO);
		wsaaAad.set(ZERO);
		wsaaDeductedAad.set(ZERO);
		wsaaTcoiamt.set(ZERO);
		wsaaLifelmtBal.set(ZERO);
		wsaaYearlmtBal.set(ZERO);
		wsaaCompYear.set(ZERO);
		sv.dischdt.set(varcom.vrcmMaxDate);
		sv.gcadmdt.set(varcom.vrcmMaxDate);
		sv.incurdt.set(varcom.vrcmMaxDate);
		sv.itmfrm.set(varcom.vrcmMaxDate);
		sv.crrcd.set(varcom.vrcmMaxDate);
		sv.crtdate.set(varcom.vrcmMaxDate);
		sv.itmto.set(varcom.vrcmMaxDate);
		sv.benefits.set(SPACES);
		wsaaLifelmtFlag.set("N");
		wsaaYearlmtFlag.set("N");
		wsaaAadFlag.set("N");
		screenInitOptswch1700();
	}

	/**
	* <pre>
	*    Set screen fields
	* </pre>
	*/
protected void retrieveCovr1020()
	{
		/* Retrieve from REGP....*/
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Retrieve from COVTRBN....*/
		covrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if (isEQ(covrIO.getStatuz(), varcom.mrnf)) {
			covrIO.setChdrcoy(regpIO.getChdrcoy());
			covrIO.setChdrnum(regpIO.getChdrnum());
			covrIO.setLife(regpIO.getLife());
			covrIO.setCoverage(regpIO.getCoverage());
			covrIO.setRider(regpIO.getRider());
			covrIO.setPlanSuffix(ZERO);
			covrIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, covrIO);
			if (isNE(covrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrIO.getParams());
				fatalError600();
			}
		}
		/* Calculate the component year*/
		datcon3rec.intDate1.set(covrIO.getCrrcd());
		datcon3rec.intDate2.set(regpIO.getIncurdt());
		datcon3rec.frequency.set("01");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		compute(wsaaCompYear, 5).set(add(0.99999, datcon3rec.freqFactor));
	}

protected void readHclh1030()
	{
		hclhIO.setChdrcoy(regpIO.getChdrcoy());
		hclhIO.setChdrnum(regpIO.getChdrnum());
		hclhIO.setLife(regpIO.getLife());
		hclhIO.setCoverage(regpIO.getCoverage());
		hclhIO.setRider(regpIO.getRider());
		hclhIO.setRgpynum(regpIO.getRgpynum());
		hclhIO.setFunction(varcom.readr);
		hclhIO.setFormat(formatsInner.hclhrec);
		SmartFileCode.execute(appVars, hclhIO);
		if (isEQ(hclhIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.readHbnfTplan1040);
		}
		if (isNE(hclhIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hclhIO.getStatuz());
			syserrrec.params.set(hclhIO.getParams());
			fatalError600();
		}
		if (isEQ(hclhIO.getStatuz(), varcom.oK)) {
			sv.gcadmdt.set(hclhIO.getGcadmdt());
			sv.dischdt.set(hclhIO.getDischdt());
			sv.diagcde.set(hclhIO.getDiagcde());
			sv.zdoctor.set(hclhIO.getZdoctor());
			sv.zmedprv.set(hclhIO.getZmedprv());
			sv.clamamt.set(hclhIO.getTclmamt());
			wsaaAad.set(hclhIO.getAad());
			sv.tdeduct2.set(hclhIO.getTcoiamt());
		}
	}

protected void readHbnfTplan1040()
	{
		callHbnf1800();
		if (isEQ(hbnfIO.getStatuz(), varcom.mrnf)) {
			hbnfIO.setRecNonKeyData(SPACES);
		}
		else {
			wsaaItdmkey.itdmItemcoy.set(covrIO.getChdrcoy());
			wsaaTr687Crtable.set(covrIO.getCrtable());
			wsaaTr687Benpln.set(hbnfIO.getBenpln());
			wsaaItdmkey.itdmItemitem.set(wsaaTr687Itemitem);
			goTo(GotoLabel.loadSubfile1050);
		}
	}

protected void loadSubfile1050()
	{
		/*    Load first page of subfile*/
		wsaaItdmkey.itdmItemtabl.set(tr687);
		itdmIO.setDataArea(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsaaItdmkey.itdmItemcoy);
		itdmIO.setItemtabl(wsaaItdmkey.itdmItemtabl);
		itdmIO.setItemitem(wsaaItdmkey.itdmItemitem);
		itdmIO.setItmfrm(regpIO.getCrtdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsaaItdmkey.itdmItemcoy)
		|| isNE(itdmIO.getItemtabl(), tr687)
		|| isNE(itdmIO.getItemitem(), wsaaItdmkey.itdmItemitem)) {
			itdmIO.setStatuz(varcom.endp);
			blankSubfile1400();
			return ;
		}
		tr687rec.tr687Rec.set(itdmIO.getGenarea());
		loadSubfile1800();
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsaaSlt.set(SPACES);
			return ;
		}
	}

protected void loadSubfile1800()
	{
		begn1800();
	}

protected void begn1800()
	{
		setUpNonSubfile1100();
		wsaaTotExpense.set(ZERO);
		wsaaTotClmamt.set(ZERO);
		while ( !(isEQ(itdmIO.getStatuz(), varcom.endp))) {
			loadSubfile1200();
		}
		
		scrnparams.subfileRrn.set(1);
		sv.tactexp.set(wsaaTotExpense);
		sv.tclmamt.set(wsaaTotClmamt);
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		compute(sv.tdeduct2, 2).set(div(mult((sub(sv.tclmamt, sv.tdeduct1)), wsaaTcoiamt), 100));
		/* MOVE SR688-TDEDUCT-2     TO ZRDP-AMOUNT-IN.                  */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT     TO SR688-TDEDUCT-2.                 */
		if (isNE(sv.tdeduct2, 0)) {
			zrdecplrec.amountIn.set(sv.tdeduct2);
			callRounding5000();
			sv.tdeduct2.set(zrdecplrec.amountOut);
		}
		compute(sv.clamamt, 2).set(sub(sub(sub(sv.tclmamt, sv.tdeduct1), sv.tdeduct2), sv.tdeduct3));
		if (isLT(sv.clamamt, 0)) {
			sv.clamamt.set(ZERO);
		}
		if (yearlmtYes.isTrue()
		&& isGT(sv.clamamt, wsaaYearlmtBal)) {
			sv.clamamt.set(wsaaYearlmtBal);
		}
		if (lifelmtYes.isTrue()
		&& isGT(sv.clamamt, wsaaLifelmtBal)) {
			sv.clamamt.set(wsaaLifelmtBal);
		}
	}

protected void setUpNonSubfile1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin1110();
				case lifePaid1120: 
					lifePaid1120();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin1110()
	{
		sv.crtable.set(hbnfIO.getCrtable());
		sv.benpln.set(hbnfIO.getBenpln());
		sv.itmfrm.set(itdmIO.getItmfrm());
		sv.incurdt.set(regpIO.getIncurdt());
		sv.itmto.set(itdmIO.getItmto());
		sv.zunit.set(hbnfIO.getZunit());
		/* Get Diagnosis Description*/
		if (isNE(sv.diagcde, SPACES)) {
			wsaaTable = "TR50H";
			wsaaItem.set(sv.diagcde);
			a100TableDesc();
			sv.shortdesc.set(wsaaShrtDesc);
		}
		/* Get Doctor Description*/
		if (isNE(sv.zdoctor, SPACES)) {
			a200DoctorDesc();
		}
		/* Get Provider Description*/
		if (isNE(sv.zmedprv, SPACES)) {
			a300ProvDesc();
		}
		if (isEQ(tr687rec.hosben[1], SPACES)) {
			if (isNE(tr687rec.amtlife[1], ZERO)) {
				wsaaLifelmtFlag.set("Y");
				compute(sv.amtlife, 0).set(mult(tr687rec.amtlife[1], sv.zunit));
			}
			if (isNE(tr687rec.amtyear[1], ZERO)) {
				wsaaYearlmtFlag.set("Y");
				compute(sv.amtyear, 0).set(mult(tr687rec.amtyear[1], sv.zunit));
			}
			sv.tdeduct1.set(tr687rec.gdeduct[1]);
			wsaaTcoiamt.set(tr687rec.copay01);
		}
		if (isNE(tr687rec.aad, ZERO)) {
			wsaaAadFlag.set("Y");
		}
		/* Check on the HB Claim Annual accum file*/
		if (yearlmtNo.isTrue()
		&& aadNo.isTrue()) {
			goTo(GotoLabel.lifePaid1120);
		}
		hclaanlIO.setChdrcoy(regpIO.getChdrcoy());
		hclaanlIO.setChdrnum(regpIO.getChdrnum());
		hclaanlIO.setLife(regpIO.getLife());
		hclaanlIO.setCoverage(regpIO.getCoverage());
		hclaanlIO.setRider(regpIO.getRider());
		hclaanlIO.setAcumactyr(wsaaCompYear);
		hclaanlIO.setFunction(varcom.readr);
		hclaanlIO.setFormat(formatsInner.hclaanlrec);
		SmartFileCode.execute(appVars, hclaanlIO);
		if (isNE(hclaanlIO.getStatuz(), varcom.oK)
		&& isNE(hclaanlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclaanlIO.getStatuz());
			syserrrec.params.set(hclaanlIO.getParams());
			fatalError600();
		}
		if (isEQ(hclaanlIO.getStatuz(), varcom.oK)) {
			sv.taccamt1.set(hclaanlIO.getClmpaid());
			wsaaDeductedAad.set(hclaanlIO.getAad());
			compute(wsaaYearlmtBal, 2).set(sub(sv.amtyear, sv.taccamt1));
			if (isNE(wsaaAad, ZERO)) {
				sv.tdeduct3.set(wsaaAad);
			}
			else {
				if (isGT(tr687rec.aad, wsaaDeductedAad)) {
					compute(sv.tdeduct3, 2).set(sub(tr687rec.aad, wsaaDeductedAad));
				}
			}
		}
		if (isEQ(hclaanlIO.getStatuz(), varcom.mrnf)
		&& yearlmtYes.isTrue()) {
			wsaaYearlmtBal.set(tr687rec.amtyear01);
		}
		if (isEQ(hclaanlIO.getStatuz(), varcom.mrnf)
		&& aadYes.isTrue()) {
			sv.tdeduct3.set(tr687rec.aad);
		}
	}

protected void lifePaid1120()
	{
		if (lifelmtNo.isTrue()) {
			return ;
		}
		hclaltlIO.setChdrcoy(regpIO.getChdrcoy());
		hclaltlIO.setChdrnum(regpIO.getChdrnum());
		hclaltlIO.setLife(regpIO.getLife());
		hclaltlIO.setCoverage(regpIO.getCoverage());
		hclaltlIO.setRider(regpIO.getRider());
		hclaltlIO.setFunction(varcom.readr);
		hclaltlIO.setFormat(formatsInner.hclaltlrec);
		SmartFileCode.execute(appVars, hclaltlIO);
		if (isNE(hclaltlIO.getStatuz(), varcom.oK)
		&& isNE(hclaltlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclaltlIO.getStatuz());
			syserrrec.params.set(hclaltlIO.getParams());
			fatalError600();
		}
		if (isEQ(hclaltlIO.getStatuz(), varcom.oK)) {
			sv.taccamt2.set(hclaltlIO.getClmpaid());
			compute(wsaaLifelmtBal, 2).set(sub(sv.amtlife, sv.taccamt1));
		}
		if (isEQ(hclaanlIO.getStatuz(), varcom.mrnf)
		&& lifelmtYes.isTrue()) {
			wsaaLifelmtBal.set(tr687rec.amtlife01);
		}
	}

protected void loadSubfile1200()
	{
		begn1210();
		contitem1230();
	}

protected void begn1210()
	{
		for (ix.set(1); !(isGT(ix, 6)); ix.add(1)){
			if (isNE(tr687rec.hosben[ix.toInt()], SPACES)) {
				sv.sel.set(" ");
				sv.hosben.set(tr687rec.hosben[ix.toInt()]);
				benefitDesc1300();
				sv.shortds.set(wsaaBenDesc);
				ladcCode1500();
				sv.limitc.set(wsaaLadc);
				compute(sv.benfamt, 0).set(mult(tr687rec.benfamt[ix.toInt()], sv.zunit));
				sv.accday.set(tr687rec.nofday[ix.toInt()]);
				/* Update hidden fields*/
				sv.mbrdeduc.set(tr687rec.gdeduct[ix.toInt()]);
				sv.mbrcopay.set(tr687rec.copay[ix.toInt()]);
				compute(sv.bnflmtb, 0).set(mult(tr687rec.amtyear[ix.toInt()], sv.zunit));
				compute(sv.bnflmta, 0).set(mult(tr687rec.amtlife[ix.toInt()], sv.zunit));
				sv.benefits.set(wsaaBenDesclong);
				sv.crrcd.set(covrIO.getCrrcd());
				sv.crtdate.set(regpIO.getIncurdt());
				sv.coverc.set(sv.zunit);
				readHcld1600();
				scrnparams.function.set(varcom.sadd);
				processScreen("SR688", sv);
				if (isNE(scrnparams.statuz, varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
		}
	}

protected void contitem1230()
	{
		if (isEQ(tr687rec.contitem, SPACES)) {
			itdmIO.setStatuz(varcom.endp);
			return ;
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsaaItdmkey.itdmItemcoy);
		itdmIO.setItemtabl(wsaaItdmkey.itdmItemtabl);
		itdmIO.setItemitem(tr687rec.contitem);
		itdmIO.setItmfrm(regpIO.getCrtdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), wsaaItdmkey.itdmItemcoy)
		|| isNE(itdmIO.getItemtabl(), tr687)
		|| isNE(itdmIO.getItemitem(), tr687rec.contitem)) {
			itdmIO.setStatuz(varcom.endp);
		}
		tr687rec.tr687Rec.set(itdmIO.getGenarea());
	}

protected void benefitDesc1300()
	{
		begn1310();
	}

protected void begn1310()
	{
		if (isEQ(tr687rec.hosben[ix.toInt()], " ")) {
			wsaaBenDesc.fill(" ");
			return ;
		}
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tr50a);
		descIO.setDescitem(tr687rec.hosben[ix.toInt()]);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			wsaaBenDesc.set(descIO.getShortdesc());
			wsaaBenDesclong.set(descIO.getLongdesc());
		}
		else {
			wsaaBenDesc.fill("?");
			wsaaBenDesclong.fill("?");
		}
	}

protected void blankSubfile1400()
	{
		begin1410();
	}

protected void begin1410()
	{
		sv.crtable.set(wsaaTr687Crtable);
		sv.benpln.set(wsaaTr687Benpln);
		sv.itmfrm.set(varcom.vrcmMaxDate);
		sv.itmto.set(varcom.vrcmMaxDate);
		sv.zunit.set(ZERO);
		sv.amtlife.set(ZERO);
		sv.amtyear.set(ZERO);
		sv.benfamt.set(ZERO);
		sv.accday.set(ZERO);
		sv.actexp.set(ZERO);
		sv.nofday.set(ZERO);
		sv.gcnetpy.set(ZERO);
		sv.zdaycov.set(ZERO);
		sv.incurdt.set(ZERO);
		sv.gdeduct.set(ZERO);
		sv.gcadmdt.set(ZERO);
		sv.dischdt.set(ZERO);
		sv.crrcd.set(ZERO);
		sv.crtdate.set(ZERO);
		sv.copay.set(ZERO);
		sv.mbrdeduc.set(ZERO);
		sv.mbrcopay.set(ZERO);
		sv.bnflmtb.set(ZERO);
		sv.bnflmta.set(ZERO);
		sv.hosben.set(SPACES);
		sv.shortds.set(SPACES);
		sv.limitc.set(SPACES);
		sv.diagcde.set(SPACES);
		sv.shortdesc.set(SPACES);
		sv.zdoctor.set(SPACES);
		sv.givname.set(SPACES);
		sv.zmedprv.set(SPACES);
		sv.cdesc.set(SPACES);
		sv.benefits.set(SPACES);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR688", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void ladcCode1500()
	{
		begn1510();
	}

protected void begn1510()
	{
		wsaaLadc.set("   ");
		if (isNE(tr687rec.amtlife[ix.toInt()], ZERO)) {
			wsaaL.set("Y");
		}
		else {
			wsaaL.set("N");
		}
		if (isNE(tr687rec.amtyear[ix.toInt()], ZERO)) {
			wsaaA.set("Y");
		}
		else {
			wsaaA.set("N");
		}
		if (isNE(tr687rec.gdeduct[ix.toInt()], ZERO)) {
			wsaaD.set("Y");
		}
		else {
			wsaaD.set("N");
		}
		if (isNE(tr687rec.copay[ix.toInt()], ZERO)) {
			wsaaC.set("Y");
		}
		else {
			wsaaC.set("N");
		}
	}

protected void readHcld1600()
	{
		begn1610();
	}

protected void begn1610()
	{
		hcldIO.setChdrcoy(regpIO.getChdrcoy());
		hcldIO.setChdrnum(regpIO.getChdrnum());
		hcldIO.setLife(regpIO.getLife());
		hcldIO.setCoverage(regpIO.getCoverage());
		hcldIO.setRider(regpIO.getRider());
		hcldIO.setRgpynum(regpIO.getRgpynum());
		hcldIO.setHosben(sv.hosben);
		hcldIO.setFunction(varcom.readr);
		hcldIO.setFormat(formatsInner.hcldrec);
		SmartFileCode.execute(appVars, hcldIO);
		if (isNE(hcldIO.getStatuz(), varcom.oK)
		&& isNE(hcldIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hcldIO.getParams());
			syserrrec.statuz.set(hcldIO.getStatuz());
			fatalError600();
		}
		if (isEQ(hcldIO.getStatuz(), varcom.oK)) {
			sv.actexp.set(hcldIO.getActexp());
			sv.nofday.set(hcldIO.getNofday());
			if (isNE(hcldIO.getZdaycov(), ZERO)) {
				compute(sv.gcnetpy, 2).set(div(hcldIO.getGcnetpy(), hcldIO.getZdaycov()));
			}
			else {
				sv.gcnetpy.set(hcldIO.getGcnetpy());
			}
			sv.zdaycov.set(hcldIO.getZdaycov());
			if (isNE(hcldIO.getNofday(), ZERO)) {
				compute(wsaaTotExpense, 2).set(add(wsaaTotExpense, mult(hcldIO.getActexp(), hcldIO.getNofday())));
			}
			else {
				wsaaTotExpense.add(hcldIO.getActexp());
			}
			wsaaTotClmamt.add(hcldIO.getGcnetpy());
		}
		else {
			sv.actexp.set(ZERO);
			sv.nofday.set(ZERO);
			sv.gcnetpy.set(ZERO);
			sv.zdaycov.set(ZERO);
		}
	}

protected void screenInitOptswch1700()
	{
		optswch1700();
	}

protected void optswch1700()
	{
		/* Call OPTSWCH to load OPTDSC*/
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
	}

protected void callHbnf1800()
	{
		readHbnf1800();
	}

protected void readHbnf1800()
	{
		hbnfIO.setRecKeyData(SPACES);
		hbnfIO.setChdrcoy(regpIO.getChdrcoy());
		hbnfIO.setChdrnum(regpIO.getChdrnum());
		hbnfIO.setLife(regpIO.getLife());
		hbnfIO.setCoverage(regpIO.getCoverage());
		hbnfIO.setRider(regpIO.getRider());
		hbnfIO.setFunction(varcom.readr);
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)
		&& isNE(hbnfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			syserrrec.statuz.set(hbnfIO.getStatuz());
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			protectScr2100();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateScreen2010();
					checkForErrors2050();
				case validateSubfile2060: 
					validateSubfile2060();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsaaScrnStatuz.set(scrnparams.statuz);
		wsspcomn.edterror.set(varcom.oK);
		/*F5=CALC*/
		if (isEQ(wsaaScrnStatuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateScreen2010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.validateSubfile2060);
		}
		/*    Validate fields*/
		if (isEQ(sv.diagcde, SPACES)) {
			sv.diagcdeErr.set(w293);
		}
		
		customerSpecificProviderValidation();
		
		/* Get Diagnosis Description*/
		if (isNE(sv.diagcde, SPACES)) {
			wsaaTable = "TR50H";
			wsaaItem.set(sv.diagcde);
			a100TableDesc();
			sv.shortdesc.set(wsaaShrtDesc);
		}
		/* Get Doctor Description*/
		if (isNE(sv.zdoctor, SPACES)) {
			a200DoctorDesc();
		}
		/* Get Provider Description*/
		if (isNE(sv.zmedprv, SPACES)) {
			a300ProvDesc();
		}
		if (isNE(sv.gcadmdt, ZERO)
		&& isNE(sv.dischdt, ZERO)) {
			if (isGT(sv.gcadmdt, sv.dischdt)) {
				sv.gcadmdtErr.set(rphu);
			}
			if (isLT(sv.gcadmdt, covrIO.getCrrcd())) {
				sv.gcadmdtErr.set(rphv);
			}
			if (isLT(sv.dischdt, covrIO.getCrrcd())) {
				sv.dischdtErr.set(rphv);
			}
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("SR688", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaTotExpense.set(ZERO);
		wsaaTotClmamt.set(ZERO);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
		scrnparams.subfileRrn.set(1);
		sv.tactexp.set(wsaaTotExpense);
		sv.tclmamt.set(wsaaTotClmamt);
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		compute(sv.tdeduct2, 2).set(div(mult((sub(sv.tclmamt, sv.tdeduct1)), wsaaTcoiamt), 100));
		/* MOVE SR688-TDEDUCT-2     TO ZRDP-AMOUNT-IN.                  */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT     TO SR688-TDEDUCT-2.                 */
		if (isNE(sv.tdeduct2, 0)) {
			zrdecplrec.amountIn.set(sv.tdeduct2);
			callRounding5000();
			sv.tdeduct2.set(zrdecplrec.amountOut);
		}
		compute(sv.clamamt, 2).set(sub(sub(sub(sv.tclmamt, sv.tdeduct1), sv.tdeduct2), sv.tdeduct3));
		if (isLT(sv.clamamt, 0)) {
			sv.clamamt.set(ZERO);
		}
		if (yearlmtYes.isTrue()
		&& isGT(sv.clamamt, wsaaYearlmtBal)) {
			sv.clamamt.set(wsaaYearlmtBal);
		}
		if (lifelmtYes.isTrue()
		&& isGT(sv.clamamt, wsaaLifelmtBal)) {
			sv.clamamt.set(wsaaLifelmtBal);
		}
		if (isEQ(wsaaSlt, " ")
		&& isEQ(sv.tactexp, ZERO)) {
			sv.tactexpErr.set(g624);
			wsspcomn.edterror.set("Y");
		}
	}

protected void protectScr2100()
	{
		/*PROTECT*/
		sv.gcadmdtOut[varcom.pr.toInt()].set("Y");
		sv.dischdtOut[varcom.pr.toInt()].set("Y");
		sv.diagcdeOut[varcom.pr.toInt()].set("Y");
		sv.zdoctorOut[varcom.pr.toInt()].set("Y");
		sv.zmedprvOut[varcom.pr.toInt()].set("Y");
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		validation2610();
		updateErrorIndicators2670();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/*    Validate subfile fields*/
		if (isEQ(sv.sel, "1")
		|| isEQ(sv.sel, "2")
		|| isEQ(sv.sel, "3")
		|| isEQ(sv.sel, "4")
		|| isEQ(sv.sel, " ")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.selErr.set(w121);
		}
		if (isEQ(wsspcomn.flag, "C")
		|| isEQ(wsspcomn.flag, "M")) {
			if (isNE(sv.actexp, ZERO)
			&& isNE(sv.gcnetpy, ZERO)) {
				if (isEQ(sv.sel, "1")) {
					sv.selErr.set(w121);
				}
			}
			if (isEQ(sv.actexp, ZERO)
			&& isEQ(sv.gcnetpy, ZERO)) {
				if (isEQ(sv.sel, "2")
				|| isEQ(sv.sel, "3")) {
					sv.selErr.set(w121);
				}
			}
		}
		if (isEQ(sv.actexp, ZERO)
		&& isEQ(sv.gcnetpy, ZERO)) {
			if (isEQ(sv.sel, "4")) {
				sv.selErr.set(w121);
			}
		}
		if (isEQ(wsspcomn.flag, "I")) {
			if (isEQ(sv.sel, "1")
			|| isEQ(sv.sel, "2")
			|| isEQ(sv.sel, "3")) {
				sv.selErr.set(w121);
			}
		}
		if (isEQ(sv.errorSubfile, SPACES)) {
			if (isNE(sv.sel, SPACES)
			&& isEQ(sv.sel, "1")
			|| isEQ(sv.sel, "2")
			|| isEQ(sv.sel, "3")
			|| isEQ(sv.sel, "4")) {
				wsaaSlt.set(sv.sel);
				wsspdocs.subfileRrn.set(scrnparams.subfileRrn);
				wsspdocs.subfileEnd.set(scrnparams.subfileEnd);
			}
		}
		if (isNE(sv.nofday, ZERO)) {
			compute(wsaaTotExpense, 2).set(add(wsaaTotExpense, mult(sv.actexp, sv.nofday)));
		}
		else {
			wsaaTotExpense.add(sv.actexp);
		}
		if (isNE(sv.zdaycov, ZERO)) {
			compute(wsaaTotClmamt, 2).set(add(wsaaTotClmamt, mult(sv.gcnetpy, sv.zdaycov)));
		}
		else {
			wsaaTotClmamt.add(sv.gcnetpy);
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR688", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SR688", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/*  Update database files as required*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		if (isEQ(hclhIO.getStatuz(), varcom.mrnf)) {
			hclhIO.setStatuz(varcom.oK);
			hclhIO.setDataKey(SPACES);
			hclhIO.setFormat(formatsInner.hclhrec);
			hclhIO.setFunction(varcom.writr);
		}
		else {
//MIBT-334
			if(isEQ(hclhIO.unique_number,ZERO)){
				hclhIO.setFunction(varcom.writr);
			}
			else{
				hclhIO.setFunction(varcom.writd);
			}
		}
		/* Set up Key*/
		hclhIO.setChdrcoy(regpIO.getChdrcoy());
		hclhIO.setChdrnum(regpIO.getChdrnum());
		hclhIO.setLife(regpIO.getLife());
		hclhIO.setCoverage(regpIO.getCoverage());
		hclhIO.setRider(regpIO.getRider());
		hclhIO.setRgpynum(regpIO.getRgpynum());
		hclhIO.setCrtable(sv.crtable);
		hclhIO.setBenpln(hbnfIO.getBenpln());
		hclhIO.setRecvdDate(regpIO.getRecvdDate());
		hclhIO.setIncurdt(sv.incurdt);
		hclhIO.setGcadmdt(sv.gcadmdt);
		hclhIO.setDischdt(sv.dischdt);
		hclhIO.setDiagcde(sv.diagcde);
		hclhIO.setZdoctor(sv.zdoctor);
		hclhIO.setZmedprv(sv.zmedprv);
		hclhIO.setTactexp(sv.tactexp);
		if (isGT(sv.clamamt, 0)) {
			hclhIO.setTclmamt(sv.clamamt);
		}
		else {
			hclhIO.setTclmamt(ZERO);
		}
		hclhIO.setTdeduct(sv.tdeduct1);
		hclhIO.setTcoiamt(sv.tdeduct2);
		hclhIO.setAad(sv.tdeduct3);
		if (isNE(sv.amtyear, 0)) {
			hclhIO.setLmtyear("Y");
		}
		else {
			hclhIO.setLmtyear("N");
		}
		if (isNE(sv.amtlife, 0)) {
			hclhIO.setLmtlife("Y");
		}
		else {
			hclhIO.setLmtlife("N");
		}
		SmartFileCode.execute(appVars, hclhIO);
		if (isNE(hclhIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hclhIO.getParams());
			syserrrec.statuz.set(hclhIO.getStatuz());
			fatalError600();
		}
		/* keep  HCLH  file ...*/
		hclhIO.setFormat(formatsInner.hclhrec);
		hclhIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, hclhIO);
		if (isNE(hclhIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hclhIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					nextProgram4010();
				case optswch4080: 
					optswch4080();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.programPtr.subtract(1);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsaaSlt, " ")) {
			goTo(GotoLabel.optswch4080);
		}
		/* keep  HCLH  file ...*/
		hclhIO.setFormat(formatsInner.hclhrec);
		hclhIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, hclhIO);
		if (isNE(hclhIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hclhIO.getParams());
			fatalError600();
		}
		/* ...if found, "select" the subfile line to switch to PopUp*/
		/* program.*/
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelOptno.set(wsaaSlt);
		optswchrec.optsSelCode.set(SPACES);
	}

protected void optswch4080()
	{
		/* Switch to next program.*/
		wsspcomn.lastActn.set(SPACES);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)
		&& isNE(optswchrec.optsStatuz, varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz, varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
		if (isEQ(wsaaSlt, " ")) {
			return ;
		}
		wsaaStoreFlag.set(wsspcomn.flag);
		wsspcomn.flag.set(wsaaSlt);
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(regpIO.getCurrcd());
		zrdecplrec.batctrcde.set("****");
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a100TableDesc()
	{
		a100Begn();
	}

protected void a100Begn()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaTable);
		descIO.setDescitem(wsaaItem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			wsaaShrtDesc.set(descIO.getShortdesc());
		}
		else {
			wsaaShrtDesc.fill("?");
		}
	}

protected void a200DoctorDesc()
	{
		a200Begn();
	}

protected void a200Begn()
	{
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.zdoctor);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		else {
			namadrsrec.namadrsRec.set(SPACES);
			namadrsrec.clntkey.set(cltsIO.getDataKey());
			namadrsrec.language.set(wsspcomn.language);
			namadrsrec.function.set(wsaaPayeeName);
			callProgram(Namadrs.class, namadrsrec.namadrsRec);
			if (isNE(namadrsrec.statuz, varcom.oK)) {
				syserrrec.params.set(cltsIO.getDataKey());
				syserrrec.statuz.set(namadrsrec.statuz);
				fatalError600();
			}
			sv.givname.set(namadrsrec.name);
		}
	}

protected void a300ProvDesc()
	{
		a300Begn();
	}

protected void a300Begn()
	{
		zmpvIO.setDataArea(SPACES);
		zmpvIO.setStatuz(varcom.oK);
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.zmedprv);
		zmpvIO.setFormat(formatsInner.zmpvrec);
		zmpvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(), varcom.oK)
		&& isNE(zmpvIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(zmpvIO.getStatuz());
			syserrrec.params.set(zmpvIO.getParams());
			fatalError600();
		}
		if (isEQ(zmpvIO.getStatuz(), varcom.oK)) {
			sv.cdesc.set(zmpvIO.getZmednam());
		}
		else {
			sv.cdesc.fill("?");
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData zmpvrec = new FixedLengthStringData(10).init("ZMPVREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC   ");
	private FixedLengthStringData hbnfrec = new FixedLengthStringData(10).init("HBNFREC   ");
	private FixedLengthStringData hcldrec = new FixedLengthStringData(10).init("HCLDREC   ");
	private FixedLengthStringData hclhrec = new FixedLengthStringData(10).init("HCLHREC   ");
	private FixedLengthStringData hclaanlrec = new FixedLengthStringData(10).init("HCLAANLREC");
	private FixedLengthStringData hclaltlrec = new FixedLengthStringData(10).init("HCLALTLREC");
}

protected void customerSpecificProviderValidation() {
	if (isEQ(sv.zmedprv, SPACES)) {
		sv.zmedprvErr.set(w322);
	}
}
}
