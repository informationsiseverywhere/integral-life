package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:47
 * Description:
 * Copybook name: REGTLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regtlnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regtlnbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData regtlnbKey = new FixedLengthStringData(64).isAPartOf(regtlnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData regtlnbChdrcoy = new FixedLengthStringData(1).isAPartOf(regtlnbKey, 0);
  	public FixedLengthStringData regtlnbChdrnum = new FixedLengthStringData(8).isAPartOf(regtlnbKey, 1);
  	public FixedLengthStringData regtlnbLife = new FixedLengthStringData(2).isAPartOf(regtlnbKey, 9);
  	public FixedLengthStringData regtlnbCoverage = new FixedLengthStringData(2).isAPartOf(regtlnbKey, 11);
  	public FixedLengthStringData regtlnbRider = new FixedLengthStringData(2).isAPartOf(regtlnbKey, 13);
  	public PackedDecimalData regtlnbSeqnbr = new PackedDecimalData(3, 0).isAPartOf(regtlnbKey, 15);
  	public PackedDecimalData regtlnbRgpynum = new PackedDecimalData(5, 0).isAPartOf(regtlnbKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(regtlnbKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regtlnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regtlnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}