package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:22
 * Description:
 * Copybook name: T6597REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6597rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6597Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData cpstats = new FixedLengthStringData(8).isAPartOf(t6597Rec, 0);
  	public FixedLengthStringData[] cpstat = FLSArrayPartOfStructure(4, 2, cpstats, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(cpstats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData cpstat01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData cpstat02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData cpstat03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData cpstat04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData crstats = new FixedLengthStringData(8).isAPartOf(t6597Rec, 8);
  	public FixedLengthStringData[] crstat = FLSArrayPartOfStructure(4, 2, crstats, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(crstats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crstat01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData crstat02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData crstat03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData crstat04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData durmnths = new FixedLengthStringData(9).isAPartOf(t6597Rec, 16);
  	public ZonedDecimalData[] durmnth = ZDArrayPartOfStructure(3, 3, 0, durmnths, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(9).isAPartOf(durmnths, 0, FILLER_REDEFINE);
  	public ZonedDecimalData durmnth01 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData durmnth02 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 3);
  	public ZonedDecimalData durmnth03 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 6);
  	public FixedLengthStringData premsubrs = new FixedLengthStringData(32).isAPartOf(t6597Rec, 25);
  	public FixedLengthStringData[] premsubr = FLSArrayPartOfStructure(4, 8, premsubrs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(32).isAPartOf(premsubrs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData premsubr01 = new FixedLengthStringData(8).isAPartOf(filler3, 0);
  	public FixedLengthStringData premsubr02 = new FixedLengthStringData(8).isAPartOf(filler3, 8);
  	public FixedLengthStringData premsubr03 = new FixedLengthStringData(8).isAPartOf(filler3, 16);
  	public FixedLengthStringData premsubr04 = new FixedLengthStringData(8).isAPartOf(filler3, 24);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(443).isAPartOf(t6597Rec, 57, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6597Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6597Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}