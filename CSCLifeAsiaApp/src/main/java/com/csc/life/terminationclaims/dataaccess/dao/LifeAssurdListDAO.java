package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Lcdpf;
import com.quipoz.framework.datatype.FixedLengthStringData;


public interface LifeAssurdListDAO {
	
	public List<Lcdpf> getLifeAssurdList(String lifeNum,String desctabl,String chdrcoy, FixedLengthStringData[] cnRiskStat,FixedLengthStringData[] covRiskStat,String chdrpfx,String language,String descpfx);	
	

}