package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr585screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr585ScreenVars sv = (Sr585ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr585screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr585ScreenVars screenVars = (Sr585ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.acdben01.setClassString("");
		screenVars.ditdsc01.setClassString("");
		screenVars.dfclmpct01.setClassString("");
		screenVars.benfreq01.setClassString("");
		screenVars.mxbenunt01.setClassString("");
		screenVars.gcdblind01.setClassString("");
		screenVars.acdben02.setClassString("");
		screenVars.ditdsc02.setClassString("");
		screenVars.dfclmpct02.setClassString("");
		screenVars.benfreq02.setClassString("");
		screenVars.mxbenunt02.setClassString("");
		screenVars.gcdblind02.setClassString("");
		screenVars.acdben03.setClassString("");
		screenVars.ditdsc03.setClassString("");
		screenVars.dfclmpct03.setClassString("");
		screenVars.benfreq03.setClassString("");
		screenVars.mxbenunt03.setClassString("");
		screenVars.gcdblind03.setClassString("");
		screenVars.acdben04.setClassString("");
		screenVars.ditdsc04.setClassString("");
		screenVars.dfclmpct04.setClassString("");
		screenVars.benfreq04.setClassString("");
		screenVars.mxbenunt04.setClassString("");
		screenVars.gcdblind04.setClassString("");
		screenVars.acdben05.setClassString("");
		screenVars.ditdsc05.setClassString("");
		screenVars.dfclmpct05.setClassString("");
		screenVars.benfreq05.setClassString("");
		screenVars.mxbenunt05.setClassString("");
		screenVars.gcdblind05.setClassString("");
		screenVars.acdben06.setClassString("");
		screenVars.ditdsc06.setClassString("");
		screenVars.dfclmpct06.setClassString("");
		screenVars.benfreq06.setClassString("");
		screenVars.mxbenunt06.setClassString("");
		screenVars.gcdblind06.setClassString("");
		screenVars.acdben07.setClassString("");
		screenVars.ditdsc07.setClassString("");
		screenVars.dfclmpct07.setClassString("");
		screenVars.benfreq07.setClassString("");
		screenVars.mxbenunt07.setClassString("");
		screenVars.gcdblind07.setClassString("");
		screenVars.acdben08.setClassString("");
		screenVars.ditdsc08.setClassString("");
		screenVars.dfclmpct08.setClassString("");
		screenVars.benfreq08.setClassString("");
		screenVars.mxbenunt08.setClassString("");
		screenVars.gcdblind08.setClassString("");
		screenVars.acdben09.setClassString("");
		screenVars.ditdsc09.setClassString("");
		screenVars.dfclmpct09.setClassString("");
		screenVars.benfreq09.setClassString("");
		screenVars.mxbenunt09.setClassString("");
		screenVars.gcdblind09.setClassString("");
		screenVars.acdben10.setClassString("");
		screenVars.ditdsc10.setClassString("");
		screenVars.dfclmpct10.setClassString("");
		screenVars.benfreq10.setClassString("");
		screenVars.mxbenunt10.setClassString("");
		screenVars.gcdblind10.setClassString("");
		screenVars.acdben11.setClassString("");
		screenVars.ditdsc11.setClassString("");
		screenVars.dfclmpct11.setClassString("");
		screenVars.benfreq11.setClassString("");
		screenVars.mxbenunt11.setClassString("");
		screenVars.gcdblind11.setClassString("");
		screenVars.acdben12.setClassString("");
		screenVars.ditdsc12.setClassString("");
		screenVars.dfclmpct12.setClassString("");
		screenVars.benfreq12.setClassString("");
		screenVars.mxbenunt12.setClassString("");
		screenVars.gcdblind12.setClassString("");
		screenVars.acdben13.setClassString("");
		screenVars.ditdsc13.setClassString("");
		screenVars.dfclmpct13.setClassString("");
		screenVars.benfreq13.setClassString("");
		screenVars.mxbenunt13.setClassString("");
		screenVars.gcdblind13.setClassString("");
		screenVars.acdben14.setClassString("");
		screenVars.ditdsc14.setClassString("");
		screenVars.dfclmpct14.setClassString("");
		screenVars.benfreq14.setClassString("");
		screenVars.mxbenunt14.setClassString("");
		screenVars.gcdblind14.setClassString("");
		screenVars.acdben15.setClassString("");
		screenVars.ditdsc15.setClassString("");
		screenVars.dfclmpct15.setClassString("");
		screenVars.benfreq15.setClassString("");
		screenVars.mxbenunt15.setClassString("");
		screenVars.gcdblind15.setClassString("");
		screenVars.contitem.setClassString("");
		screenVars.amtfld01.setClassString("");
		screenVars.amtfld02.setClassString("");
		screenVars.amtfld03.setClassString("");
		screenVars.amtfld04.setClassString("");
		screenVars.amtfld05.setClassString("");
		screenVars.amtfld06.setClassString("");
		screenVars.amtfld07.setClassString("");
		screenVars.amtfld08.setClassString("");
		screenVars.amtfld09.setClassString("");
		screenVars.amtfld10.setClassString("");
		screenVars.amtfld11.setClassString("");
		screenVars.amtfld12.setClassString("");
		screenVars.amtfld13.setClassString("");
		screenVars.amtfld14.setClassString("");
		screenVars.amtfld15.setClassString("");
	}

/**
 * Clear all the variables in Sr585screen
 */
	public static void clear(VarModel pv) {
		Sr585ScreenVars screenVars = (Sr585ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.acdben01.clear();
		screenVars.ditdsc01.clear();
		screenVars.dfclmpct01.clear();
		screenVars.benfreq01.clear();
		screenVars.mxbenunt01.clear();
		screenVars.gcdblind01.clear();
		screenVars.acdben02.clear();
		screenVars.ditdsc02.clear();
		screenVars.dfclmpct02.clear();
		screenVars.benfreq02.clear();
		screenVars.mxbenunt02.clear();
		screenVars.gcdblind02.clear();
		screenVars.acdben03.clear();
		screenVars.ditdsc03.clear();
		screenVars.dfclmpct03.clear();
		screenVars.benfreq03.clear();
		screenVars.mxbenunt03.clear();
		screenVars.gcdblind03.clear();
		screenVars.acdben04.clear();
		screenVars.ditdsc04.clear();
		screenVars.dfclmpct04.clear();
		screenVars.benfreq04.clear();
		screenVars.mxbenunt04.clear();
		screenVars.gcdblind04.clear();
		screenVars.acdben05.clear();
		screenVars.ditdsc05.clear();
		screenVars.dfclmpct05.clear();
		screenVars.benfreq05.clear();
		screenVars.mxbenunt05.clear();
		screenVars.gcdblind05.clear();
		screenVars.acdben06.clear();
		screenVars.ditdsc06.clear();
		screenVars.dfclmpct06.clear();
		screenVars.benfreq06.clear();
		screenVars.mxbenunt06.clear();
		screenVars.gcdblind06.clear();
		screenVars.acdben07.clear();
		screenVars.ditdsc07.clear();
		screenVars.dfclmpct07.clear();
		screenVars.benfreq07.clear();
		screenVars.mxbenunt07.clear();
		screenVars.gcdblind07.clear();
		screenVars.acdben08.clear();
		screenVars.ditdsc08.clear();
		screenVars.dfclmpct08.clear();
		screenVars.benfreq08.clear();
		screenVars.mxbenunt08.clear();
		screenVars.gcdblind08.clear();
		screenVars.acdben09.clear();
		screenVars.ditdsc09.clear();
		screenVars.dfclmpct09.clear();
		screenVars.benfreq09.clear();
		screenVars.mxbenunt09.clear();
		screenVars.gcdblind09.clear();
		screenVars.acdben10.clear();
		screenVars.ditdsc10.clear();
		screenVars.dfclmpct10.clear();
		screenVars.benfreq10.clear();
		screenVars.mxbenunt10.clear();
		screenVars.gcdblind10.clear();
		screenVars.acdben11.clear();
		screenVars.ditdsc11.clear();
		screenVars.dfclmpct11.clear();
		screenVars.benfreq11.clear();
		screenVars.mxbenunt11.clear();
		screenVars.gcdblind11.clear();
		screenVars.acdben12.clear();
		screenVars.ditdsc12.clear();
		screenVars.dfclmpct12.clear();
		screenVars.benfreq12.clear();
		screenVars.mxbenunt12.clear();
		screenVars.gcdblind12.clear();
		screenVars.acdben13.clear();
		screenVars.ditdsc13.clear();
		screenVars.dfclmpct13.clear();
		screenVars.benfreq13.clear();
		screenVars.mxbenunt13.clear();
		screenVars.gcdblind13.clear();
		screenVars.acdben14.clear();
		screenVars.ditdsc14.clear();
		screenVars.dfclmpct14.clear();
		screenVars.benfreq14.clear();
		screenVars.mxbenunt14.clear();
		screenVars.gcdblind14.clear();
		screenVars.acdben15.clear();
		screenVars.ditdsc15.clear();
		screenVars.dfclmpct15.clear();
		screenVars.benfreq15.clear();
		screenVars.mxbenunt15.clear();
		screenVars.gcdblind15.clear();
		screenVars.contitem.clear();
		screenVars.amtfld01.clear();
		screenVars.amtfld02.clear();
		screenVars.amtfld03.clear();
		screenVars.amtfld04.clear();
		screenVars.amtfld05.clear();
		screenVars.amtfld06.clear();
		screenVars.amtfld07.clear();
		screenVars.amtfld08.clear();
		screenVars.amtfld09.clear();
		screenVars.amtfld10.clear();
		screenVars.amtfld11.clear();
		screenVars.amtfld12.clear();
		screenVars.amtfld13.clear();
		screenVars.amtfld14.clear();
		screenVars.amtfld15.clear();
	}
}
