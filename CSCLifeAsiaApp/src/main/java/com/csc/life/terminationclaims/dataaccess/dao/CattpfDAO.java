package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CattpfDAO extends BaseDAO<Cattpf> {
	public boolean isExists(String chdrcoy,String chdrnum);
	public void insertCattpfRecord(Cattpf cattpf);
	public Cattpf selectRecords(String chdrcoy,String chdrnum);
	public void deleteRcdByChdrnum(String chdrcoy,String chdrnum);
	public int updateCattpf(String chdrcoy,String chdrnum);
	public int updateClaimStatus(String chdrcoy,String chdrnum,String clamstat);
	public int updateCattpfSetValidflag1(String chdrcoy,String chdrnum);
	public Cattpf approvedClaimRecord(String claimNum);
	public List<Cattpf> approvedRecords();	
}