package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5124
 * @version 1.0 generated on 30/08/09 06:35
 * @author Quipoz
 */
public class S5124ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(287);
	public FixedLengthStringData dataFields = new FixedLengthStringData(143).isAPartOf(dataArea, 0);
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,80);
	public FixedLengthStringData branchdesc = DD.branchdesc.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData numsel = DD.numsel.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 143);
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData branchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData facthousErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData numselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 179);
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] branchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] facthousOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] numselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5124screenWritten = new LongData(0);
	public LongData S5124windowWritten = new LongData(0);
	public LongData S5124protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5124ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(bankkeyOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankacckeyOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {bankkey, bankacckey, bankdesc, branchdesc, bankaccdsc, payrnum, currcode, facthous, numsel};
		screenOutFields = new BaseData[][] {bankkeyOut, bankacckeyOut, bankdescOut, branchdescOut, bankaccdscOut, payrnumOut, currcodeOut, facthousOut, numselOut};
		screenErrFields = new BaseData[] {bankkeyErr, bankacckeyErr, bankdescErr, branchdescErr, bankaccdscErr, payrnumErr, currcodeErr, facthousErr, numselErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5124screen.class;
		protectRecord = S5124protect.class;
	}

}
