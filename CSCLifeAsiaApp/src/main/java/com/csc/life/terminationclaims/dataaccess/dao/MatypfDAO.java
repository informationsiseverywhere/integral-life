package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Matypf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MatypfDAO extends BaseDAO<Matypf>{
	
	public void deleteMatypf(String tableName);
	public boolean insertMatyTempRecords(List<Matypf> insertmatypfList, String tempTableName);
	public int getMatyCount(String tableName);
	public List<Matypf> getMatypfList(String tableName);
	public Matypf getMatyRecord(String chdrcoy,String chdrnum,String coverage,String rider,String life,String tableName);

}
