package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5235screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5235ScreenVars sv = (S5235ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5235screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5235ScreenVars screenVars = (S5235ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.advance.setClassString("");
		screenVars.arrears.setClassString("");
		screenVars.intanny.setClassString("");
		screenVars.capcont.setClassString("");
		screenVars.withprop.setClassString("");
		screenVars.withoprop.setClassString("");
		screenVars.ppind.setClassString("");
		screenVars.dthpercn.setClassString("");
		screenVars.dthperco.setClassString("");
		screenVars.guarperd.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.nomlife.setClassString("");
		screenVars.freqann.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
	}

/**
 * Clear all the variables in S5235screen
 */
	public static void clear(VarModel pv) {
		S5235ScreenVars screenVars = (S5235ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.advance.clear();
		screenVars.arrears.clear();
		screenVars.intanny.clear();
		screenVars.capcont.clear();
		screenVars.withprop.clear();
		screenVars.withoprop.clear();
		screenVars.ppind.clear();
		screenVars.dthpercn.clear();
		screenVars.dthperco.clear();
		screenVars.guarperd.clear();
		screenVars.linsname.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.lifcnum.clear();
		screenVars.nomlife.clear();
		screenVars.freqann.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
	}
}
