package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6695
 * @version 1.0 generated on 30/08/09 06:58
 * @author Quipoz
 */
public class S6695ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(660);
	public FixedLengthStringData dataFields = new FixedLengthStringData(164).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData pctincs = new FixedLengthStringData(60).isAPartOf(dataFields, 55);
	public ZonedDecimalData[] pctinc = ZDArrayPartOfStructure(12, 5, 2, pctincs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(pctincs, 0, FILLER_REDEFINE);
	public ZonedDecimalData pctinc01 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData pctinc02 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,5);
	public ZonedDecimalData pctinc03 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,10);
	public ZonedDecimalData pctinc04 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData pctinc05 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,20);
	public ZonedDecimalData pctinc06 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,25);
	public ZonedDecimalData pctinc07 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,30);
	public ZonedDecimalData pctinc08 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,35);
	public ZonedDecimalData pctinc09 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,40);
	public ZonedDecimalData pctinc10 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,45);
	public ZonedDecimalData pctinc11 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,50);
	public ZonedDecimalData pctinc12 = DD.pctinc.copyToZonedDecimal().isAPartOf(filler,55);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,115);
	public FixedLengthStringData tupyrs = new FixedLengthStringData(36).isAPartOf(dataFields, 120);
	public FixedLengthStringData[] tupyr = FLSArrayPartOfStructure(12, 3, tupyrs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(tupyrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData tupyr01 = DD.tupyr.copy().isAPartOf(filler1,0);
	public FixedLengthStringData tupyr02 = DD.tupyr.copy().isAPartOf(filler1,3);
	public FixedLengthStringData tupyr03 = DD.tupyr.copy().isAPartOf(filler1,6);
	public FixedLengthStringData tupyr04 = DD.tupyr.copy().isAPartOf(filler1,9);
	public FixedLengthStringData tupyr05 = DD.tupyr.copy().isAPartOf(filler1,12);
	public FixedLengthStringData tupyr06 = DD.tupyr.copy().isAPartOf(filler1,15);
	public FixedLengthStringData tupyr07 = DD.tupyr.copy().isAPartOf(filler1,18);
	public FixedLengthStringData tupyr08 = DD.tupyr.copy().isAPartOf(filler1,21);
	public FixedLengthStringData tupyr09 = DD.tupyr.copy().isAPartOf(filler1,24);
	public FixedLengthStringData tupyr10 = DD.tupyr.copy().isAPartOf(filler1,27);
	public FixedLengthStringData tupyr11 = DD.tupyr.copy().isAPartOf(filler1,30);
	public FixedLengthStringData tupyr12 = DD.tupyr.copy().isAPartOf(filler1,33);
	public FixedLengthStringData contitem = DD.cont.copy().isAPartOf(dataFields,156);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(124).isAPartOf(dataArea, 164);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData pctincsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] pctincErr = FLSArrayPartOfStructure(12, 4, pctincsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(pctincsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pctinc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData pctinc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData pctinc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData pctinc04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData pctinc05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData pctinc06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData pctinc07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData pctinc08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData pctinc09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData pctinc10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData pctinc11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData pctinc12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData tupyrsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] tupyrErr = FLSArrayPartOfStructure(12, 4, tupyrsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(tupyrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData tupyr01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData tupyr02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData tupyr03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData tupyr04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData tupyr05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData tupyr06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData tupyr07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData tupyr08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData tupyr09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData tupyr10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData tupyr11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData tupyr12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData contitemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(372).isAPartOf(dataArea, 288);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData pctincsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] pctincOut = FLSArrayPartOfStructure(12, 12, pctincsOut, 0);
	public FixedLengthStringData[][] pctincO = FLSDArrayPartOfArrayStructure(12, 1, pctincOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(144).isAPartOf(pctincsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pctinc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] pctinc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] pctinc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] pctinc04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] pctinc05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] pctinc06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] pctinc07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] pctinc08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] pctinc09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] pctinc10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] pctinc11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] pctinc12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData tupyrsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] tupyrOut = FLSArrayPartOfStructure(12, 12, tupyrsOut, 0);
	public FixedLengthStringData[][] tupyrO = FLSDArrayPartOfArrayStructure(12, 1, tupyrOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(144).isAPartOf(tupyrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] tupyr01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] tupyr02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] tupyr03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] tupyr04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] tupyr05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] tupyr06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] tupyr07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] tupyr08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] tupyr09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] tupyr10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] tupyr11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] tupyr12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] contitemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6695screenWritten = new LongData(0);
	public LongData S6695protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6695ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(contitemOut,new String[] {"02", null, "-02","01", null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, tupyr01, pctinc01, tupyr02, pctinc02, tupyr03, pctinc03, tupyr04, pctinc04, tupyr05, pctinc05, tupyr06, pctinc06, tupyr07, pctinc07, tupyr08, pctinc08, tupyr09, pctinc09, tupyr10, pctinc10, tupyr11, pctinc11, tupyr12, pctinc12,contitem};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, tupyr01Out, pctinc01Out, tupyr02Out, pctinc02Out, tupyr03Out, pctinc03Out, tupyr04Out, pctinc04Out, tupyr05Out, pctinc05Out, tupyr06Out, pctinc06Out, tupyr07Out, pctinc07Out, tupyr08Out, pctinc08Out, tupyr09Out, pctinc09Out, tupyr10Out, pctinc10Out, tupyr11Out, pctinc11Out, tupyr12Out, pctinc12Out,contitemOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, tupyr01Err, pctinc01Err, tupyr02Err, pctinc02Err, tupyr03Err, pctinc03Err, tupyr04Err, pctinc04Err, tupyr05Err, pctinc05Err, tupyr06Err, pctinc06Err, tupyr07Err, pctinc07Err, tupyr08Err, pctinc08Err, tupyr09Err, pctinc09Err, tupyr10Err, pctinc10Err, tupyr11Err, pctinc11Err, tupyr12Err, pctinc12Err,contitemErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6695screen.class;
		protectRecord = S6695protect.class;
	}

}
