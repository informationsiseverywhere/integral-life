/*
 * File: Pr677.java
 * Date: 30 August 2009 1:53:35
 * Author: Quipoz Limited
 * 
 * Class transformed from PR677.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.screens.Sr677ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Initialise
* ----------
*
*  Retrieve the contract header information from the CHDRLNB I/O
*  module.
*
*  Default the first client to be the main Life Assured. There is
*  no client creation from here, and no other validation is done
*  except valid client number only.
*  Up  to  ten  Lives Details  are  allowed per contract, and no
*  more,  so  a  fixed  subfile size can be used. Initialise the
*  subfile  with  blank records (INIT function), and then update
*  any  records  with  existing details as required. Details are
*  loaded from the Hospital Benefit file (HBNFPF).For each record
*  read,
*
*       - look up the  client name (CLTS), formatting the client
*       name for confirmation.
*
*
* Validation
* ----------
*
*  If in enquiry  mode  (WSSP-FLAG  =  'I')  protect  the entire
*  Screen prior to output.
*
*  If in enquiry mode, skip all the validation.
*
*  Validate the relation field with the details from T5666.
*
*  Validate each changed  subfile  record according to the rules
*  defined in the  screen and field help. Leave duplicate client
*  number  validation  till  later.
*
* Updating
* --------
*
*  If the 'KILL' function  key  was  pressed,  or  if in enquiry
* mode, skip the updating only.
*
*  Add, modify or delete Hospital Benefit record depending on if
*  details  have  been  keyed in, blanked out or changed.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr677 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR677");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private int wsaaStackPointer = 0;
	private int wsaaScreenPointer = 0;
	protected ZonedDecimalData wsaaIndex = new ZonedDecimalData(4, 0).setUnsigned();
	protected ZonedDecimalData wsaaIndex2 = new ZonedDecimalData(4, 0).setUnsigned();
	protected ZonedDecimalData wsaaNofRead = new ZonedDecimalData(2, 0).setUnsigned();
	private int wsaaStackRecAllow = 0;
	private int wsaaLastSeqNo = 0;
	private ZonedDecimalData wsaaBnyCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaNoDets = new FixedLengthStringData(1);
	protected int wsaaSubfileSize = 10;
	protected ZonedDecimalData wsaaLivesno = new ZonedDecimalData(2, 0).setUnsigned();
	protected FixedLengthStringData wsaaHbnfLivesnoChar = new FixedLengthStringData(1);
	protected ZonedDecimalData wsaaHbnfLivesnoNum = new ZonedDecimalData(1, 0).isAPartOf(wsaaHbnfLivesnoChar, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(3);
	private Validator wsaaAdd = new Validator(wsaaUpdateFlag, "ADD");
	private Validator wsaaDelete = new Validator(wsaaUpdateFlag, "DEL");
	private Validator wsaaModify = new Validator(wsaaUpdateFlag, "MOD");
	private Validator wsaaChange = new Validator(wsaaUpdateFlag, "CHG");

	private FixedLengthStringData wsaaEofFlag = new FixedLengthStringData(1);
	private Validator wsaaEof = new Validator(wsaaEofFlag, "Y");

	private FixedLengthStringData wsaaNoMoreInsertFlag = new FixedLengthStringData(1);
	private Validator wsaaNoMoreInsert = new Validator(wsaaNoMoreInsertFlag, "Y");

	private FixedLengthStringData wsaaActionOk = new FixedLengthStringData(1);
	private Validator actionOk = new Validator(wsaaActionOk, "Y");

	private FixedLengthStringData wsbbStackArray = new FixedLengthStringData(600);
	private FixedLengthStringData wsbbClntnumArray = new FixedLengthStringData(80).isAPartOf(wsbbStackArray, 0);
	protected FixedLengthStringData[] wsbbClntnum = FLSArrayPartOfStructure(10, 8, wsbbClntnumArray, 0);
	private FixedLengthStringData wsbbRelationArray = new FixedLengthStringData(20).isAPartOf(wsbbStackArray, 80);
	protected FixedLengthStringData[] wsbbRelation = FLSArrayPartOfStructure(10, 2, wsbbRelationArray, 0);
	private FixedLengthStringData wsbbClntnameArray = new FixedLengthStringData(500).isAPartOf(wsbbStackArray, 100);
	protected FixedLengthStringData[] wsbbClntname = FLSArrayPartOfStructure(10, 50, wsbbClntnameArray, 0);

	private FixedLengthStringData wsaaClntArray = new FixedLengthStringData(100);
	protected FixedLengthStringData[] wsaaClient = FLSArrayPartOfStructure(10, 10, wsaaClntArray, 0);
	//ILIFE-2036 starts by slakkala
	protected FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	protected FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	//ILIFE-2036 ends
	protected BinaryData wsaaDuprSub = new BinaryData(2, 0);
	protected BinaryData wsaaClntSub = new BinaryData(2, 0);
	protected String e048 = "E048";
	private String f782 = "F782";
	private String e058 = "E058";
	protected String e620 = "E620";
	protected String g286 = "G286";
	protected String rla6 = "RLA6";
	protected String rl77 = "RL77";
		/* TABLES */
	protected String t5666 = "T5666";
	private String hbnfrec = "HBNFREC";
		/*Contract header - life new business*/
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
	protected CltsTableDAM cltsIO = new CltsTableDAM();
	protected Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	protected DescTableDAM descIO = new DescTableDAM();
		/*Hospital Benefit File Logical*/
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr677ScreenVars sv = getPScreenVars();

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1290, 
		preExit, 
		exit2090, 
		updateErrorIndicators2170, 
		exit2190, 
		exit2490, 
		exit3090
	}

	public Pr677() {
		super();
		screenVars = sv;
		new ScreenModel("Sr677", AppVars.getInstance(), sv);
	}
	protected Sr677ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sr677ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		retreiveHeader1030();
		readContractLongdesc1040();
		initialStack1050();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sinit);
		processScreen("SR677", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
	}

protected void retreiveHeader1030()
	{
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		hbnfIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(),varcom.oK)
		&& isNE(hbnfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		//ILIFE-2036 starts by slakkala
		wsaaCoverage.set(hbnfIO.coverage);
		wsaaRider.set(hbnfIO.rider);
		wsaaLife.set(hbnfIO.life);
		//ILIFE-2036 ends

	}

protected void readContractLongdesc1040()
	{
		descIO.setDataArea(SPACES);
		descIO.setDesccoy(chdrlnbIO.getChdrcoy());
		descIO.setDesctabl("T5688");
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		hbnfIO.setDataKey(SPACES);
		hbnfIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hbnfIO.setChdrnum(chdrlnbIO.getChdrnum());
	//ILIFE-2036 starts by slakkala	
		hbnfIO.setLife(wsaaLife);
		hbnfIO.setCoverage(wsaaCoverage);
		hbnfIO.setRider(wsaaRider);
	//ILIFE-2036 ends	
	}

protected void initialStack1050()
	{
		loadStack1200();
		/*LOAD-SUBFILE-FIELDS*/
		moveStackToScreen1400();
		/*EXIT*/
	}

protected void loadStack1200()
	{
		try {
			para1200();
			retreiveClientDetails1240();
		}
		catch (GOTOException e){
		}
	}

protected void para1200()
	{
		wsbbStackArray.set(SPACES);
		wsaaNofRead.set(0);
		hbnfIO.setFunction(varcom.readr);
		/*READ*/
		callHbnfio5000();
		if (isEQ(hbnfIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit1290);
		}
	}

protected void retreiveClientDetails1240()
	{
		for (wsaaNofRead.set(1); !(isGT(wsaaNofRead,wsaaSubfileSize)
		|| isEQ(hbnfIO.getClntnum(wsaaNofRead),SPACES)); wsaaNofRead.add(1)){
			wsbbClntnum[wsaaNofRead.toInt()].set(hbnfIO.getClntnum(wsaaNofRead));
			wsbbRelation[wsaaNofRead.toInt()].set(hbnfIO.getRelation(wsaaNofRead));
			cltsIO.setDataKey(SPACES);
			cltsIO.setDataKey(SPACES);
			cltsIO.setClntpfx("CN");
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntnum(hbnfIO.getClntnum(wsaaNofRead));
			cltsIO.setFunction(varcom.readr);
			cltsio1900();
			plainname();
			wsbbClntname[wsaaNofRead.toInt()].set(wsspcomn.longconfname);
		}
		for (wsaaNofRead.set(wsaaNofRead); !(isGT(wsaaNofRead,wsaaSubfileSize)); wsaaNofRead.add(1)){
			initialize(wsbbClntnum[wsaaNofRead.toInt()]);
			initialize(wsbbRelation[wsaaNofRead.toInt()]);
			initialize(wsbbClntname[wsaaNofRead.toInt()]);
		}
	}

protected void moveStackToScreen1400()
	{
		/*PARA*/
		scrnparams.function.set(varcom.sclr);
		callScreenIo5100();
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,wsaaSubfileSize)); wsaaIndex.add(1)){
			moveToScreen1500();
		}
		/*EXIT*/
	}

protected void moveToScreen1500()
	{
		/*MOVE-TO-SCREEN*/
		sv.clntnum.set(wsbbClntnum[wsaaIndex.toInt()]);
		sv.relation.set(wsbbRelation[wsaaIndex.toInt()]);
		sv.clntname.set(wsbbClntname[wsaaIndex.toInt()]);
		scrnparams.function.set(varcom.sadd);
		callScreenIo5100();
		/*EXIT*/
	}

protected void cltsio1900()
	{
		/*CLTSIO*/
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			cltsIO.setParams(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callHbnfio5000()
	{
		/*PARA*/
		hbnfIO.setFormat(hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(),varcom.oK)
		&& isNE(hbnfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callScreenIo5100()
	{
		/*PARA*/
		processScreen("SR677", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,wsaaSubfileSize)); wsaaIndex.add(1)){
			moveSubfile2200();
		}
		wsaaLivesno.set(0);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,wsaaSubfileSize)); wsaaIndex.add(1)){
			checkSubfile2300();
		}
		wsaaHbnfLivesnoChar.set(hbnfIO.getLivesno());
		if (isEQ(wsaaHbnfLivesnoNum,4)) {
			if (isLTE(wsaaLivesno,3)) {
				sv.chdrnumErr.set(rla6);
			}
		}
		else {
			if (isNE(wsaaLivesno,wsaaHbnfLivesnoNum)) {
				sv.chdrnumErr.set(rla6);
			}
		}
		if (isEQ(sv.errorIndicators,SPACES)
		&& isEQ(sv.errorSubfile,SPACES)) {
			clientDuplicate2500();
		}
		else {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateSubfile2120();
				}
				case updateErrorIndicators2170: {
					updateErrorIndicators2170();
				}
				case exit2190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateSubfile2120()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR677", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)
		|| isEQ(scrnparams.statuz,varcom.oK)) {
			calc2400();
		}
		if (isEQ(sv.relation,SPACES)) {
			goTo(GotoLabel.updateErrorIndicators2170);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5666);
		descIO.setDescitem(sv.relation);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.dbparams.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.relationErr.set(e620);
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateErrorIndicators2170()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("SR677", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
	}

protected void moveSubfile2200()
	{
		/*CHECK*/
		scrnparams.subfileRrn.set(wsaaIndex);
		scrnparams.function.set(varcom.sread);
		callScreenIo5100();
		wsbbClntnum[wsaaIndex.toInt()].set(sv.clntnum);
		wsbbRelation[wsaaIndex.toInt()].set(sv.relation);
		/*EXIT*/
	}

protected void checkSubfile2300()
	{
		check2310();
		updateErrorIndicators2320();
	}

protected void check2310()
	{
		scrnparams.subfileRrn.set(wsaaIndex);
		scrnparams.function.set(varcom.sread);
		callScreenIo5100();
		if (isNE(wsaaIndex,wsaaSubfileSize)) {
			compute(wsaaIndex2, 0).set(add(wsaaIndex,1));
		}
		if (isEQ(sv.clntnum,SPACES)) {
			if (isNE(wsbbClntnum[wsaaIndex2.toInt()],SPACES)) {
				sv.clntnumErr.set(g286);
			}
		}
		else {
			wsaaLivesno.add(1);
		}
		if (isEQ(wsaaIndex,1)) {
			if (isNE(sv.clntnum,hbnfIO.getClntnum01())) {
				sv.clntnumErr.set(rl77);
			}
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateErrorIndicators2320()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("SR677", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void calc2400()
	{
		try {
			calc2420();
		}
		catch (GOTOException e){
		}
	}

protected void calc2420()
	{
		if (isEQ(sv.clntnum,SPACES)) {
			sv.clntname.set(SPACES);
			goTo(GotoLabel.exit2490);
		}
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.clntnum);
		cltsio2900();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.clntnumErr.set(e058);
			wsspcomn.longconfname.set(SPACES);
		}
		else {
			plainname();
			if (isLTE(cltsIO.getCltdod(),chdrlnbIO.getOccdate())) {
				sv.clntnumErr.set(f782);
			}
		}
		sv.clntname.set(wsspcomn.longconfname);
	}

protected void clientDuplicate2500()
	{
		checkForDuplicates2500();
	}

protected void checkForDuplicates2500()
	{
		wsaaClntArray.set(SPACES);
		wsaaDuprSub.set(1);
		scrnparams.function.set(varcom.sstrt);
		processScreen("SR677", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaClient[wsaaDuprSub.toInt()].set(sv.clntnum);
		for (wsaaDuprSub.set(2); !(isGT(wsaaDuprSub,10)); wsaaDuprSub.add(1)){
			checkClientTable2550();
			if (isNE(sv.clntnumErr,SPACES)) {
				wsspcomn.edterror.set("Y");
				scrnparams.function.set(varcom.supd);
				processScreen("SR677", sv);
				if (isNE(scrnparams.statuz,varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
		}
	}

protected void checkClientTable2550()
	{
		/*CHECK-TABLE*/
		scrnparams.function.set(varcom.srdn);
		processScreen("SR677", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaClient[wsaaDuprSub.toInt()].set(sv.clntnum);
		for (wsaaClntSub.set(1); !(isEQ(wsaaClntSub,wsaaDuprSub)); wsaaClntSub.add(1)){
			if (isEQ(sv.clntnum,wsaaClient[wsaaClntSub.toInt()])) {
				if (isNE(sv.clntnum,SPACES)) {
					sv.clntnumErr.set(e048);
				}
			}
		}
		/*EXIT*/
	}

protected void cltsio2900()
	{
		/*CLTSIO*/
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			cltsIO.setParams(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		hbnfIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(),varcom.oK)
		&& isNE(hbnfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}

		//hbnfIO.setParams(SPACES);
		hbnfIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hbnfIO.setChdrnum(chdrlnbIO.getChdrnum());
		hbnfIO.setFunction(varcom.readr);
		callHbnfio5000();
		hbnfIO.setClntnums(wsbbClntnumArray);
		hbnfIO.setRelations(wsbbRelationArray);
		if (isEQ(hbnfIO.getStatuz(),varcom.mrnf)) {
			hbnfIO.setFunction(varcom.writr);
		}
		else {
			hbnfIO.setFunction(varcom.writd);
		}
		callHbnfio5000();
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
