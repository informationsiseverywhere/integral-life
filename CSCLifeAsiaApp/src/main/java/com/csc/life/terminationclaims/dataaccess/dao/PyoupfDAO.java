package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Pyoupf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface PyoupfDAO extends BaseDAO<Pyoupf> {
	
	public void insertPyoupfRecord(Pyoupf pyoupf);
	public void deletePyoupfRecord(List<String> delist);
	public Pyoupf getItemByContractNum(String chdrnum);
	public void updatePayflg(Pyoupf pyoupf);
	public List<Pyoupf> getAllItemitem(String chdrnum);
	public List<Pyoupf> deleterecord(String chdrnum);
	public void deleteCashDividendRecord(String chdrnum, String tranno, String trancode, String chdrcoy);
	public List<Pyoupf> getPyoupfList(String chdrcoy, int minRecord, int maxRecord);//ICIL-254
	public void updatepayflag(long uniqueNumber);//ICIL-254
}