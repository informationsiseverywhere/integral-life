package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author ndhingra9
 *Definition of cancellation header file 
 */
public class Kjohpf {
	private Long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private Integer plnsfx;
	private String life;
	private String jlife;
	private Integer tranno;
	private String termid;
	private Integer userid;
	private Integer effdate;
	private String currcd;
	private String claimType;
	private BigDecimal policyloan;
	private BigDecimal otheradjst;
	private String reasoncd;
	private String resndesc;
	private String cnttype;
	private Integer trtm;
	private BigDecimal plint01;
	private BigDecimal plint02;
	private BigDecimal aplamt01;
	private BigDecimal aplamt02;
	private BigDecimal aplint01;
	private BigDecimal aplint02;
	private BigDecimal premsusp;
	private BigDecimal premadvbal;
	private BigDecimal premadvint;
	private BigDecimal surrtax;
	private BigDecimal interest;
	private Integer intdays;
	private String payclt;
	private String bankkey;
	private String bankacckey;
	private String bankactype;
	private String paymmeth;
	private BigDecimal surrval01;
	private BigDecimal surrval02;
	private BigDecimal surrval03;
	private String clamstat;
	private BigDecimal plamt01;
	private BigDecimal plamt02;
	private BigDecimal clamamt01;
	private BigDecimal clamamt02;
	private BigDecimal ofcharge;
	private String cheqno;
	private Integer paydate;
	private String paycoy;
	private String validflag;
	private String claim;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	private BigDecimal skpmsusp;
	private BigDecimal wuepamt;
	private BigDecimal prempaid;
	private BigDecimal total;
	private String refundoption;
	public Long getUniqueNumber() {
		return uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public Integer getPlnsfx() {
		return plnsfx;
	}

	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public Integer getTranno() {
		return tranno;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getEffdate() {
		return effdate;
	}

	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}

	public String getCurrcd() {
		return currcd;
	}

	public void setCurrcd(String currcd) {
		this.currcd = currcd;
	}

	public BigDecimal getPolicyloan() {
		return policyloan;
	}

	public void setPolicyloan(BigDecimal policyloan) {
		this.policyloan = policyloan;
	}

	public BigDecimal getOtheradjst() {
		return otheradjst;
	}

	public void setOtheradjst(BigDecimal otheradjst) {
		this.otheradjst = otheradjst;
	}

	public String getReasoncd() {
		return reasoncd;
	}

	public void setReasoncd(String reasoncd) {
		this.reasoncd = reasoncd;
	}

	public String getResndesc() {
		return resndesc;
	}

	public void setResndesc(String resndesc) {
		this.resndesc = resndesc;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public Integer getTrtm() {
		return trtm;
	}

	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}

	public BigDecimal getPlint01() {
		return plint01;
	}

	public void setPlint01(BigDecimal plint01) {
		this.plint01 = plint01;
	}

	public BigDecimal getPlint02() {
		return plint02;
	}

	public void setPlint02(BigDecimal plint02) {
		this.plint02 = plint02;
	}

	public BigDecimal getAplamt01() {
		return aplamt01;
	}

	public void setAplamt01(BigDecimal aplamt01) {
		this.aplamt01 = aplamt01;
	}

	public BigDecimal getAplamt02() {
		return aplamt02;
	}

	public void setAplamt02(BigDecimal aplamt02) {
		this.aplamt02 = aplamt02;
	}

	public BigDecimal getAplint01() {
		return aplint01;
	}

	public void setAplint01(BigDecimal aplint01) {
		this.aplint01 = aplint01;
	}

	public BigDecimal getAplint02() {
		return aplint02;
	}

	public void setAplint02(BigDecimal aplint02) {
		this.aplint02 = aplint02;
	}

	public BigDecimal getPremsusp() {
		return premsusp;
	}

	public void setPremsusp(BigDecimal premsusp) {
		this.premsusp = premsusp;
	}

	public BigDecimal getPremadvbal() {
		return premadvbal;
	}

	public void setPremadvbal(BigDecimal premadvbal) {
		this.premadvbal = premadvbal;
	}

	public BigDecimal getPremadvint() {
		return premadvint;
	}

	public void setPremadvint(BigDecimal premadvint) {
		this.premadvint = premadvint;
	}

	public BigDecimal getSurrtax() {
		return surrtax;
	}

	public void setSurrtax(BigDecimal surrtax) {
		this.surrtax = surrtax;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	public Integer getIntdays() {
		return intdays;
	}

	public void setIntdays(Integer intdays) {
		this.intdays = intdays;
	}

	public String getPayclt() {
		return payclt;
	}

	public void setPayclt(String payclt) {
		this.payclt = payclt;
	}

	public String getBankkey() {
		return bankkey;
	}

	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}

	public String getBankacckey() {
		return bankacckey;
	}

	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}

	public String getBankactype() {
		return bankactype;
	}

	public void setBankactype(String bankactype) {
		this.bankactype = bankactype;
	}

	public String getPaymmeth() {
		return paymmeth;
	}

	public void setPaymmeth(String paymmeth) {
		this.paymmeth = paymmeth;
	}

	public BigDecimal getSurrval01() {
		return surrval01;
	}

	public void setSurrval01(BigDecimal surrval01) {
		this.surrval01 = surrval01;
	}

	public BigDecimal getSurrval02() {
		return surrval02;
	}

	public void setSurrval02(BigDecimal surrval02) {
		this.surrval02 = surrval02;
	}

	public BigDecimal getSurrval03() {
		return surrval03;
	}

	public void setSurrval03(BigDecimal surrval03) {
		this.surrval03 = surrval03;
	}

	public String getClamstat() {
		return clamstat;
	}

	public void setClamstat(String clamstat) {
		this.clamstat = clamstat;
	}

	public BigDecimal getPlamt01() {
		return plamt01;
	}

	public void setPlamt01(BigDecimal plamt01) {
		this.plamt01 = plamt01;
	}

	public BigDecimal getPlamt02() {
		return plamt02;
	}

	public void setPlamt02(BigDecimal plamt02) {
		this.plamt02 = plamt02;
	}

	public BigDecimal getClamamt01() {
		return clamamt01;
	}

	public void setClamamt01(BigDecimal clamamt01) {
		this.clamamt01 = clamamt01;
	}

	public BigDecimal getClamamt02() {
		return clamamt02;
	}

	public void setClamamt02(BigDecimal clamamt02) {
		this.clamamt02 = clamamt02;
	}

	public BigDecimal getOfcharge() {
		return ofcharge;
	}

	public void setOfcharge(BigDecimal ofcharge) {
		this.ofcharge = ofcharge;
	}

	public String getCheqno() {
		return cheqno;
	}

	public void setCheqno(String cheqno) {
		this.cheqno = cheqno;
	}

	public Integer getPaydate() {
		return paydate;
	}

	public void setPaydate(Integer paydate) {
		this.paydate = paydate;
	}

	public String getPaycoy() {
		return paycoy;
	}

	public void setPaycoy(String paycoy) {
		this.paycoy = paycoy;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public String getClaim() {
		return claim;
	}

	public void setClaim(String claim) {
		this.claim = claim;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public Timestamp getDatime() {
		return datime;
	}

	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}

	public BigDecimal getSkpmsusp() {
		return skpmsusp;
	}

	public void setSkpmsusp(BigDecimal skpmsusp) {
		this.skpmsusp = skpmsusp;
	}

	public BigDecimal getWuepamt() {
		return wuepamt;
	}

	public void setWuepamt(BigDecimal wuepamt) {
		this.wuepamt = wuepamt;
	}

	public BigDecimal getPrempaid() {
		return prempaid;
	}

	public void setPrempaid(BigDecimal prempaid) {
		this.prempaid = prempaid;
	}

	public void setUniqueNumber(Long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getRefundoption() {
		return refundoption;
	}

	public void setRefundoption(String refundoption) {
		this.refundoption = refundoption;
	}
 
	
	
	
	
	
	
	
}
