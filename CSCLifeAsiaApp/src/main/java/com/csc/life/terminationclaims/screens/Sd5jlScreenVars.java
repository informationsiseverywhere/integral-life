package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sd5jl
 * @version 1.0 generated on 05/12/18
 * @author Quipoz
 */
public class Sd5jlScreenVars extends SmartVarModel { 


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public FixedLengthStringData dataArea = new FixedLengthStringData(330);
	public FixedLengthStringData dataFields = new FixedLengthStringData(170).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData accInd = new FixedLengthStringData(1).isAPartOf(dataFields, 151);
	public FixedLengthStringData disabledFlag = DD.flag.copy().isAPartOf(dataFields,152);
	public ZonedDecimalData totalAmount = DD.amt.copyToZonedDecimal().isAPartOf(dataFields,153);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 170);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData accIndErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData disabledFlagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData totalAmountErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 210);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] accIndOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] disabledFlagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] totalAmountOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(445);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(170).isAPartOf(subfileArea, 0);
	
	public FixedLengthStringData bnycd = DD.bnycd.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData bnypc = DD.bnypc.copyToZonedDecimal().isAPartOf(subfileFields,1);
	public FixedLengthStringData bnysel = DD.bnysel.copy().isAPartOf(subfileFields,6);
	public FixedLengthStringData bnytype = DD.bnytype.copy().isAPartOf(subfileFields,16);
	public FixedLengthStringData clntsname = DD.clntsname.copy().isAPartOf(subfileFields,18);
	public FixedLengthStringData cltreln = DD.cltreln.copy().isAPartOf(subfileFields,48);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,52);
	public FixedLengthStringData relto = DD.relto.copy().isAPartOf(subfileFields,60);
	public FixedLengthStringData revcflg = DD.revcflg.copy().isAPartOf(subfileFields,61);
	public ZonedDecimalData enddate = DD.effdate.copyToZonedDecimal().isAPartOf(subfileFields,62);/*ILIFE-3581*/
	public FixedLengthStringData sequence = DD.sequence.copy().isAPartOf(subfileFields, 70);/*ICIL-11*/
	public FixedLengthStringData paymthbf = DD.paymthbf.copy().isAPartOf(subfileFields, 72);/*ICIL-11*/
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(subfileFields,73);/*ICIL-11*/
	public FixedLengthStringData bankacckey = DD.bankacc.copy().isAPartOf(subfileFields, 83);/*ICIL-11*/
	public FixedLengthStringData bkrelackey = DD.bkrelac.copy().isAPartOf(subfileFields, 103);/*ICIL-11*/
	public FixedLengthStringData bnkcdedsc = DD.bnkcdedsc.copy().isAPartOf(subfileFields, 123);/*ICIL-11*/
	
	public ZonedDecimalData amount = DD.amt.copyToZonedDecimal().isAPartOf(subfileFields,153);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(68).isAPartOf(subfileArea, 170);
	
	public FixedLengthStringData bnycdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData bnypcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData bnyselErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData bnytypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData clntsnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData cltrelnErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData reltoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData revcflgErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData enddateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);/*ILIFE-3581*/
	public FixedLengthStringData sequenceErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);/*ICIL-11*/
	public FixedLengthStringData paymthbfErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);/*ICIL-11*/
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);/*ICIL-11*/
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);/*ICIL-11*/
	public FixedLengthStringData bkrelackeyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);/*ICIL-11*/
	public FixedLengthStringData bnkcdedscErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);/*ICIL-11*/
	public FixedLengthStringData amountErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 64);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(204).isAPartOf(subfileArea, 238);
	
	public FixedLengthStringData[] bnycdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] bnypcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] bnyselOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] bnytypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] clntsnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] cltrelnOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] reltoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] revcflgOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] enddateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);/*ILIFE-3581*/
	public FixedLengthStringData[] sequenceOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);/*ICIL-11*/
	public FixedLengthStringData[] paymthbfOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);/*ICIL-11*/
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);/*ICIL-11*/
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);/*ICIL-11*/
	public FixedLengthStringData[] bkrelackeyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);/*ICIL-11*/
	public FixedLengthStringData[] bnkcdedscOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);/*ICIL-11*/
	public FixedLengthStringData[] amountOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 192);
	
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 442);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData enddateDisp = new FixedLengthStringData(10);
	public LongData Sd5jlscreensflWritten = new LongData(0);
	public LongData Sd5jlscreenctlWritten = new LongData(0);
	public LongData Sd5jlscreenWritten = new LongData(0);
	public LongData Sd5jlprotectWritten = new LongData(0);
	public GeneralTable sd5jlscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sd5jlscreensfl;
	}

	public Sd5jlScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(cltrelnOut,new String[] {"37","61","-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnycdOut,new String[] {"38","62","-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnypcOut,new String[] {"39","63","-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnyselOut,new String[] {"40","64","-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntsnameOut,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"42","65","-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnytypeOut,new String[] {"43","66","-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reltoOut,new String[] {"44",null, "-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(revcflgOut,new String[] {"45",null, "-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumOut,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(enddateOut,new String[] {"51","52", "-51",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sequenceOut,new String[] {"47", "48", "-47",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paymthbfOut, new String[] {"53", "54", "-53",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankkeyOut,new String[] {"55","68","-55",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankacckeyOut,new String[] {"57","70","-57",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(amountOut,new String[] {"60","77","-70",null, null, null, null, null, null, null, null, null});
		
		screenSflFields = new BaseData[] {cltreln, bnycd, bnypc, bnysel, clntsname, effdate, bnytype, relto, revcflg, enddate, sequence, paymthbf, bankkey, bankacckey, bkrelackey, bnkcdedsc,amount};
		screenSflOutFields = new BaseData[][] {cltrelnOut, bnycdOut, bnypcOut, bnyselOut, clntsnameOut, effdateOut, bnytypeOut, reltoOut, revcflgOut, enddateOut, sequenceOut, paymthbfOut, bankkeyOut, bankacckeyOut, bkrelackeyOut, bnkcdedscOut,amountOut};
		screenSflErrFields = new BaseData[] {cltrelnErr, bnycdErr, bnypcErr, bnyselErr, clntsnameErr, effdateErr, bnytypeErr, reltoErr, revcflgErr, enddateErr, sequenceErr, paymthbfErr, bankkeyErr, bankacckeyErr, bkrelackeyErr, bnkcdedscErr,amountErr};
		screenSflDateFields = new BaseData[] {effdate, enddate};
		screenSflDateErrFields = new BaseData[] {effdateErr,enddateErr};
		screenSflDateDispFields = new BaseData[] {effdateDisp,enddateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cownnum, ownername, lifcnum, linsname, accInd, totalAmount};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, accIndOut, totalAmountOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, accIndErr, totalAmountErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sd5jlscreen.class;
		screenSflRecord = Sd5jlscreensfl.class;
		screenCtlRecord = Sd5jlscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sd5jlprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sd5jlscreenctl.lrec.pageSubfile);
	}
}
