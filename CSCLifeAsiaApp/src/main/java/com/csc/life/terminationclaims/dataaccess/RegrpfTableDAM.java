package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RegrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:17
 * Class transformed from REGRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RegrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 122;
	public FixedLengthStringData regrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData regrpfRecord = regrrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(regrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(regrrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(regrrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(regrrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(regrrec);
	public PackedDecimalData rgpynum = DD.rgpynum.copy().isAPartOf(regrrec);
	public PackedDecimalData pymt = DD.pymt.copy().isAPartOf(regrrec);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(regrrec);
	public PackedDecimalData prcnt = DD.prcnt.copy().isAPartOf(regrrec);
	public FixedLengthStringData rgpytype = DD.rgpytype.copy().isAPartOf(regrrec);
	public FixedLengthStringData payreason = DD.payreason.copy().isAPartOf(regrrec);
	public FixedLengthStringData rgpystat = DD.rgpystat.copy().isAPartOf(regrrec);
	public PackedDecimalData revdte = DD.revdte.copy().isAPartOf(regrrec);
	public PackedDecimalData firstPaydate = DD.fpaydate.copy().isAPartOf(regrrec);
	public FixedLengthStringData progname = DD.progname.copy().isAPartOf(regrrec);
	public FixedLengthStringData excode = DD.excode.copy().isAPartOf(regrrec);
	public FixedLengthStringData exreport = DD.exreport.copy().isAPartOf(regrrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(regrrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(regrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(regrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(regrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(regrrec);
	public PackedDecimalData lastPaydate = DD.lpaydate.copy().isAPartOf(regrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public RegrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for RegrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public RegrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for RegrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public RegrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for RegrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public RegrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("REGRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"RGPYNUM, " +
							"PYMT, " +
							"CURRCD, " +
							"PRCNT, " +
							"RGPYTYPE, " +
							"PAYREASON, " +
							"RGPYSTAT, " +
							"REVDTE, " +
							"FPAYDATE, " +
							"PROGNAME, " +
							"EXCODE, " +
							"EXREPORT, " +
							"CRTABLE, " +
							"TRANNO, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"LPAYDATE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     rgpynum,
                                     pymt,
                                     currcd,
                                     prcnt,
                                     rgpytype,
                                     payreason,
                                     rgpystat,
                                     revdte,
                                     firstPaydate,
                                     progname,
                                     excode,
                                     exreport,
                                     crtable,
                                     tranno,
                                     userProfile,
                                     jobName,
                                     datime,
                                     lastPaydate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		rgpynum.clear();
  		pymt.clear();
  		currcd.clear();
  		prcnt.clear();
  		rgpytype.clear();
  		payreason.clear();
  		rgpystat.clear();
  		revdte.clear();
  		firstPaydate.clear();
  		progname.clear();
  		excode.clear();
  		exreport.clear();
  		crtable.clear();
  		tranno.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		lastPaydate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getRegrrec() {
  		return regrrec;
	}

	public FixedLengthStringData getRegrpfRecord() {
  		return regrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setRegrrec(what);
	}

	public void setRegrrec(Object what) {
  		this.regrrec.set(what);
	}

	public void setRegrpfRecord(Object what) {
  		this.regrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(regrrec.getLength());
		result.set(regrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}