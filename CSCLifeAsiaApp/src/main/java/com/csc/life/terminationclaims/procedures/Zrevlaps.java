/*
 * File: Zrevlaps.java
 * Date: 30 August 2009 2:56:02
 * Author: Quipoz Limited
 * 
 * Class transformed from ZREVLAPS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmdbcTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrvsTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;

import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*  ZREVLAPS - Auto Lapse/Paid-up Reversal (Clone from REVLAPS)
*  -----------------------------------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*  The following transactions are reversed :
*
*  - CHDR
*
*       READH on  the  Contract  Header record (CHDRLIF) for the
*            relevant contract.
*       DELET this record.
*       NEXTR on  the  Contract  Header record (CHDRLIF) for the
*            same contract. This should read a record with
*            valid flag of '2'. If not, then this is an error.
*       REWRT this   Contract   Header  record  (CHDRLIF)  after
*            altering  the  valid  flag from '2' to '1'.
*
*  - COVRs
*
*       Only reverse COVR records whose TRANNO is equal to the
*           Transaction number (of the PTRN) being reversed.
*       READH on  the  COVR  records for the relevant contract.
*       DELET this record.
*       NEXTR on  the  COVR  file should read a record with valid
*            flag  of  '2'. If not, then this is an error.
*       REWRT this  COVR  record after altering the valid flag
*            from  '2'  to  '1' . This will reinstate the
*            coverage prior to the LAPSE/PUP.
*
*  - UTRNs
*
*       Call the T5671 generic subroutine to reverse Unit
*       transactions for the TRANNO being reversed.
*       The subroutine GREVUTRN should be entered in the
*       relevant table entry.
*
*  - ACMVs
*
*       Do a BEGNH on the ACMV file for this transaction no. and
*       call LIFACMV to post the reversal to the sub-account.
*
*  - AGCMs
*
*       Read the first AGCM record (AGCMRVS).
*       Delete this record (via AGCMDBC).
*       Read the next AGCM looking for valid flag 2 records.
*       Update these AGCM records with valid flag 1.
*
*  - PAYR
*
*       READH on the Payor record (PAYRLIF) for the relevant
*            contract.
*       DELET this record.
*       NEXTR on the Payor record (PAYRLIF) for the same
*            contract. This should read a record with valid
*            flag of '2'. If not, then this is an error.
*       REWRT this Payor record (PAYRLIF) after altering the
*            valid flag from '2' to '1'.
*
* AGENT/GOVERNMENT STATISTICS RECORDS REVERSAL.
*
* All of these records are reversed when REVGENAT, the AT module,
*  calls the statistical subroutine LIFSTTR.
*
* NO processing is therefore required in this subroutine.
*
*
****************************************************************
* </pre>
*/
public class Zrevlaps extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zrevlaps.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZREVLAPS";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaRevTrandesc = new FixedLengthStringData(30);

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* TABLES */
	private static final String t5671 = "T5671";
	private static final String t1688 = "T1688";
		/* FORMATS */
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String covrrec = "COVRREC";
	private static final String acmvrevrec = "ACMVREVREC";
	private static final String agcmrvsrec = "AGCMRVSREC";
	private static final String payrlifrec = "PAYRLIFREC";
	private static final String arcmrec = "ARCMREC";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AgcmdbcTableDAM agcmdbcIO = new AgcmdbcTableDAM();
	private AgcmrvsTableDAM agcmrvsIO = new AgcmrvsTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Reverserec reverserec = new Reverserec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		processComponent3050, 
		exit3090, 
		findRecordToReverse5120
	}

	public Zrevlaps() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		initialise1001();
	}

protected void initialise1001()
	{
		syserrrec.subrname.set(wsaaSubr);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		wsaaRevTrandesc.set(descIO.getLongdesc());
	}

protected void process2000()
	{
		/*PROCESS*/
		processChdrs2100();
		processCovrs3000();
		processAcmvs4000();
		processAcmvOptical4010();
		processAgcms5000();
		/*EXIT*/
	}

protected void processChdrs2100()
	{
		start2101();
	}

protected void start2101()
	{
		/*  delete valid flag 1 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getValidflag(),1)) {
			syserrrec.params.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		chdrlifIO.setFunction(varcom.delet);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*    Select the next record which will be the valid flag 2 record*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		/* MOVE BEGNH                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		chdrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrlifIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(chdrlifIO.getChdrcoy(),reverserec.company)
		|| isNE(chdrlifIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(chdrlifIO.getValidflag(),2)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		chdrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.writd);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processCovrs3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					covers3001();
				case processComponent3050: 
					processComponent3050();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void covers3001()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void processComponent3050()
	{
		/*    If this component has a TRANNO equal to the TRANNO we are*/
		/*    reversing, then it has been Lapsed/Paid-up and must now be*/
		/*    reversed.  If not, then find the next component.*/
		wsaaCovrKey.set(covrIO.getDataKey());
		if (isEQ(covrIO.getTranno(),reverserec.tranno)) {
			deleteAndUpdatComp3200();
			processGenericSubr3300();
		}
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getDataKey(),wsaaCovrKey))) {
			getNextCovr3500();
		}
		
		if (isNE(covrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.processComponent3050);
		}
	}

protected void deleteAndUpdatComp3200()
	{
		deleteRecs3201();
	}

protected void deleteRecs3201()
	{
		if (isNE(covrIO.getValidflag(),1)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/* valid flag equal to 1*/
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.delet);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		/*    As we have just deleted the VALIDFLAG '1' record there*/
		/*    must be a VALIDFLAG '2' record.*/
		if (isNE(covrIO.getValidflag(),2)
		|| isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		covrIO.setValidflag("1");
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setBenBillDate(reverserec.bbldat);
		covrIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void processGenericSubr3300()
	{
			readGenericTable3301();
		}

protected void readGenericTable3301()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Trancode.set(reverserec.oldBatctrcde);
		wsaaT5671Crtable.set(covrIO.getCrtable());
		itemIO.setItemitem(wsaaT5671Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		greversrec.chdrcoy.set(covrIO.getChdrcoy());
		greversrec.chdrnum.set(covrIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		/* MOVE ZEROES                 TO GREV-PLAN-SUFFIX.*/
		greversrec.planSuffix.set(covrIO.getPlanSuffix());
		greversrec.life.set(covrIO.getLife());
		greversrec.coverage.set(covrIO.getCoverage());
		greversrec.rider.set(covrIO.getRider());
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub,4)); wsaaSub.add(1)){
			callSubprog3400();
		}
	}

protected void callSubprog3400()
	{
		/*GO*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
		}
		if (isNE(greversrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(greversrec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void getNextCovr3500()
	{
		/*GET-RECS*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(covrIO.getChdrcoy(),reverserec.company)
		|| isNE(covrIO.getChdrnum(),reverserec.chdrnum)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processAcmvs4000()
	{
		readSubAcctTable4001();
	}

protected void readSubAcctTable4001()
	{
		/*    UNITDEAL will reverse all the ACMVs created by B5463, so*/
		/*    here we reverse all other ACMVs.*/
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvs4100();
		}
		
	}

protected void processAcmvOptical4010()
	{
		start4010();
	}

protected void start4010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			xxxxFatalError();
		}
		/* If the PTRN being processed has an Accounting Period Greater*/
		/* than the last period Archived then there is no need to*/
		/* attempt to read the Optical Device.*/
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
				}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			xxxxFatalError();
				}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
				}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
				}
		
		/* If the next policy is found then we must call the COLD API*/
		/* again with a function of CLOSE.*/
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				xxxxFatalError();
			}
			}
		}

protected void reverseAcmvs4100()
	{
		start4100();
		readNextAcmv4180();
	}

protected void start4100()
	{
		if (isNE(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(),-1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.trandesc.set(wsaaRevTrandesc);
		lifacmvrec.effdate.set(wsaaToday);
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.*/
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			xxxxFatalError();
		}
	}

protected void readNextAcmv4180()
	{
		/*  Read the next ACMV record.*/
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.*/
		if (isEQ(wsaaIoCall,"CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(),varcom.oK)
		&& isNE(acmvrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			xxxxFatalError();
		}
		if (isNE(reverserec.company,acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum,acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno,acmvrevIO.getTranno())) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processAgcms5000()
	{
		agcms5001();
	}

protected void agcms5001()
	{
		agcmrvsIO.setParams(SPACES);
		agcmrvsIO.setChdrcoy(reverserec.company);
		agcmrvsIO.setChdrnum(reverserec.chdrnum);
		agcmrvsIO.setTranno(reverserec.tranno);
		agcmrvsIO.setFormat(agcmrvsrec);
		agcmrvsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(),varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmrvsIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(agcmrvsIO.getChdrcoy(),reverserec.company)
		|| isNE(agcmrvsIO.getTranno(),reverserec.tranno)) {
			agcmrvsIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmrvsIO.getStatuz(),varcom.endp))) {
			reverseAgcm5100();
		}
		
	}

protected void reverseAgcm5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					deleteRecs5101();
				case findRecordToReverse5120: 
					findRecordToReverse5120();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void deleteRecs5101()
	{
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		/*    Delete this record*/
		agcmdbcIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		/*    valid flag 2 record that was changed by the original*/
		/*    Lapse/PUP transaction.  If this contract has had single*/
		/*    premium top-ups or lump sums at contract issue, we may find*/
		/*    a valid flag 1 record for the key we are reading.  Ignore*/
		/*    these records and continue the quest for the valid flag 2*/
		/*    record.*/
		agcmdbcIO.setParams(SPACES);
		agcmdbcIO.setChdrcoy(agcmrvsIO.getChdrcoy());
		agcmdbcIO.setChdrnum(agcmrvsIO.getChdrnum());
		agcmdbcIO.setAgntnum(agcmrvsIO.getAgntnum());
		agcmdbcIO.setLife(agcmrvsIO.getLife());
		agcmdbcIO.setCoverage(agcmrvsIO.getCoverage());
		agcmdbcIO.setRider(agcmrvsIO.getRider());
		agcmdbcIO.setPlanSuffix(agcmrvsIO.getPlanSuffix());
		agcmdbcIO.setSeqno(agcmrvsIO.getSeqno());
		agcmdbcIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		agcmdbcIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void findRecordToReverse5120()
	{
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getChdrcoy(),agcmrvsIO.getChdrcoy())
		|| isNE(agcmdbcIO.getChdrnum(),agcmrvsIO.getChdrnum())
		|| isNE(agcmdbcIO.getAgntnum(),agcmrvsIO.getAgntnum())
		|| isNE(agcmdbcIO.getLife(),agcmrvsIO.getLife())
		|| isNE(agcmdbcIO.getCoverage(),agcmrvsIO.getCoverage())
		|| isNE(agcmdbcIO.getRider(),agcmrvsIO.getRider())
		|| isNE(agcmdbcIO.getPlanSuffix(),agcmrvsIO.getPlanSuffix())
		|| isNE(agcmdbcIO.getSeqno(),agcmrvsIO.getSeqno())) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(agcmdbcIO.getValidflag(),"2")) {
			agcmdbcIO.setFunction(varcom.nextr);
			goTo(GotoLabel.findRecordToReverse5120);
		}
		/*  Update The Selected Record on the Coverage File.*/
		agcmdbcIO.setValidflag("1");
		agcmdbcIO.setCurrto(varcom.vrcmMaxDate);
		agcmdbcIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmdbcIO);
		if (isNE(agcmdbcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmdbcIO.getParams());
			syserrrec.statuz.set(agcmdbcIO.getStatuz());
			xxxxFatalError();
		}
		agcmrvsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmrvsIO);
		if (isNE(agcmrvsIO.getStatuz(),varcom.oK)
		&& isNE(agcmrvsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmrvsIO.getParams());
			syserrrec.statuz.set(agcmrvsIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(agcmrvsIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isNE(agcmrvsIO.getChdrcoy(),reverserec.company)
		|| isNE(agcmrvsIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(agcmrvsIO.getTranno(),reverserec.tranno)) {
			agcmrvsIO.setStatuz(varcom.endp);
		}
	}

protected void processPayrs6000()
	{
		start6001();
	}

protected void start6001()
	{
		/*  delete valid flag 1 record*/
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getValidflag(),1)) {
			syserrrec.params.set(payrlifIO.getParams());
			xxxxFatalError();
		}
		payrlifIO.setFunction(varcom.delet);
		payrlifIO.setFormat(payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		/*    Select the next record which will be the valid flag 2 record*/
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(1);
		/* MOVE BEGNH                  TO PAYRLIF-FUNCTION              */
		payrlifIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(payrlifIO.getChdrcoy(),reverserec.company)
		|| isNE(payrlifIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(payrlifIO.getValidflag(),2)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
		payrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO PAYRLIF-FUNCTION              */
		payrlifIO.setFunction(varcom.writd);
		payrlifIO.setFormat(payrlifrec);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			syserrrec.statuz.set(payrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxxxErrorBomb();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorBomb()
	{
		reverserec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}
}
