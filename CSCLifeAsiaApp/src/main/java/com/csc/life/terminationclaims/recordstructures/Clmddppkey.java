package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:44
 * Description:
 * Copybook name: CLMDDPPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Clmddppkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData clmddppFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData clmddppKey = new FixedLengthStringData(64).isAPartOf(clmddppFileKey, 0, REDEFINE);
  	public FixedLengthStringData clmddppChdrcoy = new FixedLengthStringData(1).isAPartOf(clmddppKey, 0);
  	public FixedLengthStringData clmddppChdrnum = new FixedLengthStringData(8).isAPartOf(clmddppKey, 1);
  	public FixedLengthStringData clmddppCoverage = new FixedLengthStringData(2).isAPartOf(clmddppKey, 9);
  	public FixedLengthStringData clmddppRider = new FixedLengthStringData(2).isAPartOf(clmddppKey, 11);
  	public FixedLengthStringData clmddppCrtable = new FixedLengthStringData(4).isAPartOf(clmddppKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(clmddppKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(clmddppFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		clmddppFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}