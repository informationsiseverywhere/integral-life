/*
 * File: T6597pt.java
 * Date: 30 August 2009 2:27:21
 * Author: Quipoz Limited
 * 
 * Class transformed from T6597PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6597.
*
*
*****************************************************************
* </pre>
*/
public class T6597pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 27, FILLER).init("Non Forfeiture Parameters                   S6597");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid From:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 16);
	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine003, 26, FILLER).init("   To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(66);
	private FixedLengthStringData filler9 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine004, 48, FILLER).init("Risk          Prem");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(67);
	private FixedLengthStringData filler11 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine005, 13, FILLER).init("Duration        Subroutine        Status        Status");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(21);
	private FixedLengthStringData filler13 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 13, FILLER).init("(Months)");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(65);
	private FixedLengthStringData filler15 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 15).setPattern("ZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 30);
	private FixedLengthStringData filler17 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine007, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 49);
	private FixedLengthStringData filler18 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 63);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(65);
	private FixedLengthStringData filler19 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 15).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 30);
	private FixedLengthStringData filler21 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine008, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 49);
	private FixedLengthStringData filler22 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 63);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(65);
	private FixedLengthStringData filler23 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 15).setPattern("ZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 18, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 30);
	private FixedLengthStringData filler25 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine009, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 49);
	private FixedLengthStringData filler26 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 63);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(65);
	private FixedLengthStringData filler27 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler28 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 15, FILLER).init("Lapse");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 30);
	private FixedLengthStringData filler29 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine010, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 49);
	private FixedLengthStringData filler30 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 63);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6597rec t6597rec = new T6597rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6597pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6597rec.t6597Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t6597rec.durmnth01);
		fieldNo010.set(t6597rec.cpstat01);
		fieldNo009.set(t6597rec.crstat01);
		fieldNo008.set(t6597rec.premsubr01);
		fieldNo011.set(t6597rec.durmnth02);
		fieldNo012.set(t6597rec.premsubr02);
		fieldNo013.set(t6597rec.crstat02);
		fieldNo014.set(t6597rec.cpstat02);
		fieldNo015.set(t6597rec.durmnth03);
		fieldNo016.set(t6597rec.premsubr03);
		fieldNo017.set(t6597rec.crstat03);
		fieldNo018.set(t6597rec.cpstat03);
		fieldNo019.set(t6597rec.premsubr04);
		fieldNo020.set(t6597rec.crstat04);
		fieldNo021.set(t6597rec.cpstat04);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
