package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:28
 * Description:
 * Copybook name: HCLHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hclhkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hclhFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hclhKey = new FixedLengthStringData(256).isAPartOf(hclhFileKey, 0, REDEFINE);
  	public FixedLengthStringData hclhChdrcoy = new FixedLengthStringData(1).isAPartOf(hclhKey, 0);
  	public FixedLengthStringData hclhChdrnum = new FixedLengthStringData(8).isAPartOf(hclhKey, 1);
  	public FixedLengthStringData hclhLife = new FixedLengthStringData(2).isAPartOf(hclhKey, 9);
  	public FixedLengthStringData hclhCoverage = new FixedLengthStringData(2).isAPartOf(hclhKey, 11);
  	public FixedLengthStringData hclhRider = new FixedLengthStringData(2).isAPartOf(hclhKey, 13);
  	public PackedDecimalData hclhRgpynum = new PackedDecimalData(5, 0).isAPartOf(hclhKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(hclhKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hclhFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hclhFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}