package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sa510screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sa510screensfl";
		lrec.subfileClass = Sa510screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 10;
		lrec.pageSubfile = 9;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 12, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa510ScreenVars sv = (Sa510ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa510screenctlWritten, sv.Sa510screensflWritten, av, sv.sa510screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sa510ScreenVars screenVars = (Sa510ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.notifinum.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.claimant.setClassString("");
		screenVars.clamnme.setClassString("");
		screenVars.relation.setClassString("");
		screenVars.indxflg.setClassString("");
	}

/**
 * Clear all the variables in Sa510screenctl
 */
	public static void clear(VarModel pv) {
		Sa510ScreenVars screenVars = (Sa510ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.notifinum.clear();
		screenVars.lifcnum.clear();
		screenVars.lifename.clear();
		screenVars.claimant.clear();
		screenVars.clamnme.clear();
		screenVars.relation.clear();
		screenVars.indxflg.clear();
	}
}
