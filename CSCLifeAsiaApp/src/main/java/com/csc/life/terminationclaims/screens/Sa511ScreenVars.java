package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.StringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.datadictionarydatatype.FLSDDObj;
import com.quipoz.framework.datatype.*;

/**
 * Screen variables for SR50D
 * @version 1.0 generated on 30/08/09 07:14
 * @author Quipoz
 */
public class Sa511ScreenVars extends SmartVarModel { 


	//dataArea
		public FixedLengthStringData dataArea = new FixedLengthStringData(105);
		
		public FixedLengthStringData dataFields = new FixedLengthStringData(57).isAPartOf(dataArea, 0);
		public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,0);
		public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,8);
		//CML001 Client Status
		public FixedLengthStringData cltstat = DD.cltstat.copy().isAPartOf(dataFields,55);
			
		
		public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 57);
		public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
		public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
		public FixedLengthStringData cltstatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
		
		public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 69);
		public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
		public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
		//CML001 Client Status
		public FixedLengthStringData[] cltstatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
		
		//subfileArea
		public FixedLengthStringData subfileArea = new FixedLengthStringData(216);
		public FixedLengthStringData subfileFields = new FixedLengthStringData(101).isAPartOf(subfileArea, 0);
		//Select
		public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,0);
		//Notification Number
		public FixedLengthStringData notifnum = DD.aacct.copy().isAPartOf(subfileFields,1);
		//Incident Type
		public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(subfileFields,15);
		//Notification Creation Date
		public ZonedDecimalData notifcdate = DD.notifcdate.copyToZonedDecimal().isAPartOf(subfileFields,45);
		//Notification Latest Update Date
		public ZonedDecimalData notiflupdate = DD.notiflupdate.copyToZonedDecimal().isAPartOf(subfileFields,53);
		//Notification Status
		public FixedLengthStringData ntfstat = DD.ntfstat.copy().isAPartOf(subfileFields,61);
		//UserID
		public FixedLengthStringData userid = DD.userid.copy().isAPartOf(subfileFields,91);
		
		public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 101);
		public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
		public FixedLengthStringData notifnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
		public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
		public FixedLengthStringData notifcdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
		public FixedLengthStringData notiflupdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
		public FixedLengthStringData ntfstatErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
		public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
		
		public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 129);
		public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
		public FixedLengthStringData[] notifnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
		public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
		public FixedLengthStringData[] notifcdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
		public FixedLengthStringData[] notiflupdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
		public FixedLengthStringData[] ntfstatOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
		public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
		
		public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 213);
			
		public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
		public FixedLengthStringData notifcdateDisp = new FixedLengthStringData(10);
		public FixedLengthStringData notiflupdateDisp = new FixedLengthStringData(10);
		
		public LongData Sa511screensflWritten = new LongData(0);
		public LongData Sa511screenctlWritten = new LongData(0);
		public LongData Sa511screenWritten = new LongData(0);
		public LongData Sa511protectWritten = new LongData(0);
		public GeneralTable sa511screensfl = new GeneralTable(AppVars.getInstance());


		public boolean hasSubfile() {
			return false;
		}


		public Sa511ScreenVars() {
			super();
			initialiseScreenVars();
		}

		protected void initialiseScreenVars() {
			fieldIndMap.put(selectOut,new String[] {"01","02","-01","13",null, null, null, null, null, null, null, null});
			fieldIndMap.put(notifnumOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null, null});
			fieldIndMap.put(longdescOut,new String[] {null, null, null, "06", null, null, null, null, null, null, null, null});
			fieldIndMap.put(notifcdateOut,new String[] {null, null, null, "07", null, null, null, null, null, null, null, null});
			fieldIndMap.put(notiflupdateOut,new String[] {null, null, null, "08", null, null, null, null, null, null, null, null});
			fieldIndMap.put(ntfstatOut,new String[] {"09",null, "09",null, null, null, null, null, null, null, null, null});
			fieldIndMap.put(useridOut,new String[] {"10",null, "10",null, null, null, null, null, null, null, null, null});
			//ILIFE-1138 STARTS
			screenSflFields = new BaseData[] {select, notifnum, longdesc, notifcdate, notiflupdate, ntfstat, userid};
			screenSflOutFields = new BaseData[][] {selectOut, notifnumOut, longdescOut, notifcdateOut, notiflupdateOut, ntfstatOut, useridOut};
			screenSflErrFields = new BaseData[] {selectErr, notifnumErr, longdescErr, notifcdateErr, notiflupdateErr, ntfstatErr, useridErr};
			//ILIFE-1138 ENDS
			screenSflDateFields = new BaseData[] {notifcdate,notiflupdate};
			screenSflDateErrFields = new BaseData[] {notifcdateErr,notiflupdateErr};
			screenSflDateDispFields = new BaseData[] {notifcdateDisp,notiflupdateDisp};
			
			screenFields = new BaseData[] {lifcnum, lifename};
			screenOutFields = new BaseData[][] {lifcnumOut, lifenameOut};
			screenErrFields = new BaseData[] {lifcnumErr, lifenameErr};
			screenDateFields = new BaseData[] {};
			screenDateErrFields = new BaseData[] {};
			screenDateDispFields = new BaseData[] {};
			
			screenDataArea = dataArea;
			screenSubfileArea = subfileArea;
			screenSflIndicators = screenIndicArea;
			errorInds = errorIndicators;
			errorSflInds = errorSubfile;
			screenRecord = Sa511screen.class;
			screenSflRecord = Sa511screensfl.class;
			screenCtlRecord = Sa511screenctl.class;
			initialiseSubfileArea();
			protectRecord = Sa511protect.class;
			
		}
		public void initialiseSubfileArea() {
			initialize(screenSubfileArea);
			subfilePage.set(Sa511screenctl.lrec.pageSubfile);
		}
		
}
