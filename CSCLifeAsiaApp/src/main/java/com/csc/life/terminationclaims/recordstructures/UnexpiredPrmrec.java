package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class UnexpiredPrmrec extends ExternalData {

	public FixedLengthStringData unexpiredPrmRec = new FixedLengthStringData(72);

	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(unexpiredPrmRec, 0);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(unexpiredPrmRec, 4);
  	public FixedLengthStringData prmCollectedFlag = new FixedLengthStringData(1).isAPartOf(unexpiredPrmRec, 7);
  	public FixedLengthStringData frequency = new FixedLengthStringData(2).isAPartOf(unexpiredPrmRec, 8);
  	public ZonedDecimalData calcPremium = new ZonedDecimalData(17,2).isAPartOf(unexpiredPrmRec, 10);
  	public ZonedDecimalData sccDate = new ZonedDecimalData(8, 0).isAPartOf(unexpiredPrmRec, 27);	// contract date
  	public ZonedDecimalData sEffDate = new ZonedDecimalData(8, 0).isAPartOf(unexpiredPrmRec, 35);	//surrender date
  	public ZonedDecimalData sptDate = new ZonedDecimalData(8, 0).isAPartOf(unexpiredPrmRec, 43);	//paid to date IBPLIFE-3582
	public ZonedDecimalData unexpiredPrmOut = new ZonedDecimalData(17,2).isAPartOf(unexpiredPrmRec, 51);
	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(unexpiredPrmRec, 68);
  	
	
	@Override
	public void initialize() {
		COBOLFunctions.initialize(unexpiredPrmRec);
		
	}

	@Override
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			unexpiredPrmRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

}
