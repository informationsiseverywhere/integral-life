package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:26
 * Description:
 * Copybook name: TH584REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th584rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th584Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData nonForfeitMethod = new FixedLengthStringData(4).isAPartOf(th584Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(496).isAPartOf(th584Rec, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th584Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th584Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}