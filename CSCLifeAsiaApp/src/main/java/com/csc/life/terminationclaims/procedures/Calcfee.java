/*
 * File: Calcfee.java
 * Date: 29 August 2009 22:38:02
 * Author: Quipoz Limited
 * 
 * Class transformed from CALCFEE.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.terminationclaims.tablestructures.T6651rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Calculation for Paid Up Processing      (CALCFEE)
*  ----------------------------------
*  This program is the calculation process for obtaining the
*  fee incurred by the pay up.
*
*  The linkage information passed to this subroutine:-
*       - OVRDUEREC
*
*  The linkage field passed back to the calling program:-
*       - fee charged
*
*  Processing
*  ----------
*  Set the fee charged to zeros.
*
*  Calc Fee Charged
*  ----------------
*  Read T6651, using T5687-PUMETH||CNTCURR  as  the  item  key,
*  and  using  the  field  values  for  the appropriate billing
*  frequency.
*
*  If T6651-PUFFAMT is non-zero
*     set the fee to T6651-PUFFAMT
*  else
*     calculate the fee as follows:-
*           multiply the instalment premium by the billing
*           frequency to give the annualised premium;
*           then calculate the fee as a percentage
*           (T6651-FEEPC) of this annualised premium.
*
*  If the fee is less than T6651-PUFEEMIN
*     set the fee to T6651-PUFEEMIN
*  else
*     if the fee is greater than T6651-PUFEEMAX
*        set the fee to T6651-PUFEEMAX.
*
****************************************************************
* </pre>
*/
public class Calcfee extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "CALCFEE";

	private FixedLengthStringData wsaaT6651Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6651Pumeth = new FixedLengthStringData(4).isAPartOf(wsaaT6651Item, 0);
	private FixedLengthStringData wsaaT6651Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT6651Item, 4);

	private FixedLengthStringData wsaaGotFreq = new FixedLengthStringData(1);
	private Validator gotFreq = new Validator(wsaaGotFreq, "Y");
	private ZonedDecimalData wsaaInd = new ZonedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreqNum = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaFrequencies = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFreq1 = new FixedLengthStringData(10).isAPartOf(wsaaFrequencies, 0).init("0001020412");
	private FixedLengthStringData[] wsaaFreq = FLSArrayPartOfStructure(5, wsaaFreq1, 0, REDEFINE);
		/* TABLES */
	private String t6651 = "T6651";
		/* ERRORS */
	private String h114 = "H114";
	private String i036 = "I036";
	private String i037 = "I037";
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Syserrrec syserrrec = new Syserrrec();
	private T6651rec t6651rec = new T6651rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit000, 
		exit199
	}

	public Calcfee() {
		super();
	}

public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			start000();
		}
		catch (GOTOException e){
		}
		finally{
			exit000();
		}
	}

protected void start000()
	{
		syserrrec.subrname.set(wsaaSubr);
		ovrduerec.statuz.set(varcom.oK);
		ovrduerec.pupfee.set(ZERO);
		if (isEQ(ovrduerec.pumeth,SPACES)) {
			ovrduerec.pupfee.set(ZERO);
			goTo(GotoLabel.exit000);
		}
		calcFee100();
	}

protected void exit000()
	{
		exitProgram();
	}

protected void calcFee100()
	{
		try {
			para100();
			calc120();
			minMax150();
		}
		catch (GOTOException e){
		}
	}

protected void para100()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(t6651);
		wsaaT6651Pumeth.set(ovrduerec.pumeth);
		wsaaT6651Cntcurr.set(ovrduerec.cntcurr);
		itdmIO.setItemitem(wsaaT6651Item);
		itdmIO.setItmfrm(ovrduerec.btdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(),varcom.oK))
		&& (isNE(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError500();
			goTo(GotoLabel.exit199);
		}
		if ((isNE(itdmIO.getItemcoy(),ovrduerec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(),t6651))
		|| (isNE(itdmIO.getItemitem(),wsaaT6651Item))
		|| (isEQ(itdmIO.getStatuz(),varcom.endp))) {
			itdmIO.setItemitem(wsaaT6651Item);
			itdmIO.setStatuz(h114);
			itdmIO.setItemtabl(t6651);
			syserrrec.params.set(itdmIO.getParams());
			databaseError500();
			goTo(GotoLabel.exit199);
		}
		t6651rec.t6651Rec.set(itdmIO.getGenarea());
		wsaaInd.set(ZERO);
		wsaaGotFreq.set("N");
		while ( !(gotFreq.isTrue()
		|| isEQ(wsaaInd,5))) {
			getFee200();
		}
		
		if ((!gotFreq.isTrue())) {
			syserrrec.statuz.set(i036);
			syserrrec.syserrType.set("2");
			databaseError500();
			goTo(GotoLabel.exit199);
		}
		if ((isEQ(t6651rec.puffamt[wsaaInd.toInt()],ZERO))
		&& (isEQ(t6651rec.feepc[wsaaInd.toInt()],ZERO))) {
			goTo(GotoLabel.exit199);
		}
		if ((isNE(t6651rec.puffamt[wsaaInd.toInt()],ZERO))) {
			ovrduerec.pupfee.set(t6651rec.puffamt[wsaaInd.toInt()]);
			goTo(GotoLabel.exit199);
		}
	}

protected void calc120()
	{
		if (isNE(ovrduerec.billfreq,NUMERIC)) {
			syserrrec.statuz.set(i037);
			syserrrec.syserrType.set("2");
			databaseError500();
			goTo(GotoLabel.exit199);
		}
		wsaaBillfreq.set(ovrduerec.billfreq);
		compute(ovrduerec.pupfee, 2).set(div(mult(mult(ovrduerec.instprem,wsaaBillfreqNum),t6651rec.feepc[wsaaInd.toInt()]),100));
	}

protected void minMax150()
	{
		if (isEQ(t6651rec.pufeemin[wsaaInd.toInt()],ZERO)
		&& isEQ(t6651rec.pufeemax[wsaaInd.toInt()],ZERO)) {
			goTo(GotoLabel.exit199);
		}
		if (isLT(ovrduerec.pupfee,t6651rec.pufeemin[wsaaInd.toInt()])) {
			ovrduerec.pupfee.set(t6651rec.pufeemin[wsaaInd.toInt()]);
		}
		else {
			if (isGT(ovrduerec.pupfee,t6651rec.pufeemax[wsaaInd.toInt()])) {
				ovrduerec.pupfee.set(t6651rec.pufeemax[wsaaInd.toInt()]);
			}
		}
	}

protected void getFee200()
	{
		/*START*/
		wsaaInd.add(1);
		if (isEQ(wsaaFreq[wsaaInd.toInt()],ovrduerec.billfreq)) {
			wsaaGotFreq.set("Y");
		}
		/*EXIT*/
	}

protected void databaseError500()
	{
		/*START*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		ovrduerec.statuz.set(syserrrec.statuz);
		/*EXIT*/
	}
}
