package com.csc.life.terminationclaims.dataaccess.dao;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;

import com.csc.life.terminationclaims.dataaccess.model.Kjohpf;

public interface KjohpfDAO extends BaseDAO<Kjohpf>{
	void insertKjohpf(Kjohpf pf);
	public Kjohpf getKjohpfRecord(String chdrnum, String claimNum);
	
}
