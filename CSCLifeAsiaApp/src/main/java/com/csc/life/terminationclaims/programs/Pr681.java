/*
 * File: Pr681.java
 * Date: 30 August 2009 1:55:00
 * Author: Quipoz Limited
 * 
 * Class transformed from PR681.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.screens.Sr681ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Initialise
* ----------
*
* This is  the  window  scroll  inquiry  program  for covered
* Lives for contract type - HBNF.
*
* Retrieve the contract header information from the CHDRRGP I/O
* module.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr681 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR681");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private int wsaaSubfileSize = 10;
	private int wsaaStackPointer = 0;
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaNofRead = new ZonedDecimalData(2, 0).setUnsigned();
	private int wsaaStackRecAllow = 0;
	private int wsaaLastSeqNo = 0;

	private FixedLengthStringData wsbbStackArray = new FixedLengthStringData(600);
	private FixedLengthStringData wsbbClntnumArray = new FixedLengthStringData(80).isAPartOf(wsbbStackArray, 0);
	private FixedLengthStringData[] wsbbClntnum = FLSArrayPartOfStructure(10, 8, wsbbClntnumArray, 0);
	private FixedLengthStringData wsbbRelationArray = new FixedLengthStringData(20).isAPartOf(wsbbStackArray, 80);
	private FixedLengthStringData[] wsbbRelation = FLSArrayPartOfStructure(10, 2, wsbbRelationArray, 0);
	private FixedLengthStringData wsbbClntnameArray = new FixedLengthStringData(500).isAPartOf(wsbbStackArray, 100);
	private FixedLengthStringData[] wsbbClntname = FLSArrayPartOfStructure(10, 50, wsbbClntnameArray, 0);
	private String hbnfrec = "HBNFREC";
		/*Regular Payments View.*/
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Hospital Benefit File Logical*/
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private Sr681ScreenVars sv = ScreenProgram.getScreenVars( Sr681ScreenVars.class);
	private CovrTableDAM covrIO = new CovrTableDAM();//ILIFE-8116

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1290, 
		exit2090, 
		preExit
	}

	public Pr681() {
		super();
		screenVars = sv;
		new ScreenModel("Sr681", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		retreiveHeader1030();
		readContractLongdesc1040();
		initialStack1050();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sinit);
		processScreen("SR681", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
	}

protected void retreiveHeader1030()
	{
		chdrrgpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(),varcom.oK)
		&& isNE(chdrrgpIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		/*ILIFE-8116 STARTS*/
		covrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		/*ILIFE-8116 ENDS*/
	}

protected void readContractLongdesc1040()
	{
		descIO.setDataArea(SPACES);
		descIO.setDesccoy(chdrrgpIO.getChdrcoy());
		descIO.setDesctabl("T5688");
		descIO.setDescitem(chdrrgpIO.getCnttype());
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		sv.chdrnum.set(chdrrgpIO.getChdrnum());
		sv.cnttype.set(chdrrgpIO.getCnttype());
		//hbnfIO.setDataKey(SPACES);
		hbnfIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(),varcom.oK)
		&& isNE(hbnfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}

		hbnfIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		hbnfIO.setChdrnum(chdrrgpIO.getChdrnum());
	}

protected void initialStack1050()
	{
		loadStack1200();
		/*LOAD-SUBFILE-FIELDS*/
		moveStackToScreen1400();
		/*EXIT*/
	}

protected void loadStack1200()
	{
		try {
			para1200();
			retreiveClientDetails1240();
		}
		catch (GOTOException e){
		}
	}

protected void para1200()
	{
		wsbbStackArray.set(SPACES);
		wsaaNofRead.set(0);
		
		hbnfIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		hbnfIO.setChdrnum(chdrrgpIO.getChdrnum());
		hbnfIO.setLife(covrIO.getLife());//ILIFE-8116 
		hbnfIO.setCoverage(covrIO.getCoverage());//ILIFE-8116 
		hbnfIO.setRider(covrIO.getRider());//ILIFE-8116 
		hbnfIO.setFunction(varcom.readr);
		/*READ*/
		callHbnfio5000();
		if (isEQ(hbnfIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit1290);
		}
	}

protected void retreiveClientDetails1240()
	{
		for (wsaaNofRead.set(1); !(isGT(wsaaNofRead,wsaaSubfileSize)
		|| isEQ(hbnfIO.getClntnum(wsaaNofRead),SPACES)); wsaaNofRead.add(1)){
			wsbbClntnum[wsaaNofRead.toInt()].set(hbnfIO.getClntnum(wsaaNofRead));
			wsbbRelation[wsaaNofRead.toInt()].set(hbnfIO.getRelation(wsaaNofRead));
			cltsIO.setDataKey(SPACES);
			cltsIO.setDataKey(SPACES);
			cltsIO.setClntpfx("CN");
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntnum(hbnfIO.getClntnum(wsaaNofRead));
			cltsIO.setFunction(varcom.readr);
			cltsio1900();
			plainname();
			wsbbClntname[wsaaNofRead.toInt()].set(wsspcomn.longconfname);
		}
		for (wsaaNofRead.set(wsaaNofRead); !(isGT(wsaaNofRead,wsaaSubfileSize)); wsaaNofRead.add(1)){
			initialize(wsbbClntnum[wsaaNofRead.toInt()]);
			initialize(wsbbRelation[wsaaNofRead.toInt()]);
			initialize(wsbbClntname[wsaaNofRead.toInt()]);
		}
	}

protected void moveStackToScreen1400()
	{
		/*PARA*/
		scrnparams.function.set(varcom.sclr);
		callScreenIo5100();
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,wsaaSubfileSize)); wsaaIndex.add(1)){
			moveToScreen1500();
		}
		/*EXIT*/
	}

protected void moveToScreen1500()
	{
		/*MOVE-TO-SCREEN*/
		sv.clntnum.set(wsbbClntnum[wsaaIndex.toInt()]);
		sv.relation.set(wsbbRelation[wsaaIndex.toInt()]);
		sv.clntname.set(wsbbClntname[wsaaIndex.toInt()]);
		scrnparams.function.set(varcom.sadd);
		callScreenIo5100();
		/*EXIT*/
	}

protected void cltsio1900()
	{
		/*CLTSIO*/
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			cltsIO.setParams(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callHbnfio5000()
	{
		/*PARA*/
		hbnfIO.setFormat(hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(),varcom.oK)
		&& isNE(hbnfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callScreenIo5100()
	{
		/*PARA*/
		processScreen("SR681", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		readNextChange2100();
		if (isNE(sv.slt,SPACES)) {
			wsspwindow.value.set(sv.clntnum);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readNextChange2100()
	{
		/*READ-NEXT*/
		scrnparams.subfileRrn.set(1);
		scrnparams.function.set(varcom.srnch);
		sv.subfileArea.set(SPACES);
		processScreen("SR681", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.nextprog.set(SPACES);
		/*EXIT*/
	}
}
