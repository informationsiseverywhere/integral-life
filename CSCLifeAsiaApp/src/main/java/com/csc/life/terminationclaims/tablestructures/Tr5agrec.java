package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.StringData;

/**
 * 	
 * @author: pmujavadiya
 * @version
 * Creation Date: Sun, 31 Jan 2017 13:28:10
 * Description:
 * Copybook name: TR5AGREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */

public class Tr5agrec extends ExternalData{
	//*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData tr5agRec = new FixedLengthStringData(500);
	  	public FixedLengthStringData progdescs = new FixedLengthStringData(500).isAPartOf(tr5agRec, 0);
	  	public FixedLengthStringData[] progdesc = FLSArrayPartOfStructure(5, 100, progdescs, 0);
	  	public FixedLengthStringData filler = new FixedLengthStringData(500).isAPartOf(progdescs, 0, FILLER_REDEFINE);
	  	public FixedLengthStringData progdesc01 = new FixedLengthStringData(100).isAPartOf(filler, 0);
	  	public FixedLengthStringData progdesc02 = new FixedLengthStringData(100).isAPartOf(filler, 100);
	  	public FixedLengthStringData progdesc03 = new FixedLengthStringData(100).isAPartOf(filler, 200);
	  	public FixedLengthStringData progdesc04 = new FixedLengthStringData(100).isAPartOf(filler, 300);
	  	public FixedLengthStringData progdesc05 = new FixedLengthStringData(100).isAPartOf(filler, 400);
	  	
public void initialize() {
			COBOLFunctions.initialize(tr5agRec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	    		tr5agRec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}



}
