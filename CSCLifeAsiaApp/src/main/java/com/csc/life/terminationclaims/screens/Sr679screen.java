package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr679screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr679ScreenVars sv = (Sr679ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr679screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr679ScreenVars screenVars = (Sr679ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.liencd.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.anbAtCcd.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.statSubsect.setClassString("");
		screenVars.sumin.setClassString("");
		screenVars.frqdesc.setClassString("");
		screenVars.singlePremium.setClassString("");
		screenVars.premcessDisp.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.riskCessDateDisp.setClassString("");
		screenVars.annivProcDateDisp.setClassString("");
		screenVars.rerateDateDisp.setClassString("");
		screenVars.rerateFromDateDisp.setClassString("");
		screenVars.benBillDateDisp.setClassString("");
		screenVars.benpln.setClassString("");
		screenVars.optdsc03.setClassString("");
		screenVars.optind03.setClassString("");
		screenVars.optdsc01.setClassString("");
		screenVars.optind01.setClassString("");
		screenVars.optdsc02.setClassString("");
		screenVars.optind02.setClassString("");
		screenVars.bappmeth.setClassString("");
		screenVars.zdesc.setClassString("");
		screenVars.zlinstprem.setClassString("");
		screenVars.livesno.setClassString("");
		screenVars.waiverprem.setClassString("");
		screenVars.optdsc05.setClassString("");
		screenVars.optind05.setClassString("");
		screenVars.optdsc04.setClassString("");
		screenVars.optind04.setClassString("");
		screenVars.optdsc06.setClassString("");
		screenVars.optind06.setClassString("");
		screenVars.taxamt.setClassString("");
		screenVars.optdsc07.setClassString("");
		screenVars.optind07.setClassString("");
		/*BRD-306 START*/
		screenVars.loadper.setClassString("");
		screenVars.rateadj.setClassString("");
		screenVars.fltmort.setClassString("");
		screenVars.premadj.setClassString("");
		screenVars.adjustageamt.setClassString("");
		screenVars.select.setClassString("");
		screenVars.riskCessAge.setClassString("");
		screenVars.riskCessTerm.setClassString("");
		screenVars.premCessAge.setClassString("");
		screenVars.premCessTerm.setClassString("");
		/*BRD-306 END*/
		//ILIFE-3421-STARTS
		screenVars.waitperiod.setClassString("");
		screenVars.bentrm.setClassString("");
		screenVars.poltyp.setClassString("");
		screenVars.prmbasis.setClassString("");
		//ILIFE-3421-ENDS
		screenVars.dialdownoption.setClassString("");//BRD-NBP-011
		screenVars.optdsc08.setClassString("");
		screenVars.optind08.setClassString("");
		
	}

/**
 * Clear all the variables in Sr679screen
 */
	public static void clear(VarModel pv) {
		Sr679ScreenVars screenVars = (Sr679ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.crtabdesc.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.numpols.clear();
		screenVars.planSuffix.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.liencd.clear();
		screenVars.zagelit.clear();
		screenVars.anbAtCcd.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.statSubsect.clear();
		screenVars.sumin.clear();
		screenVars.frqdesc.clear();
		screenVars.singlePremium.clear();
		screenVars.premcessDisp.clear();
		screenVars.premcess.clear();
		screenVars.instPrem.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.riskCessDateDisp.clear();
		screenVars.riskCessDate.clear();
		screenVars.annivProcDateDisp.clear();
		screenVars.annivProcDate.clear();
		screenVars.rerateDateDisp.clear();
		screenVars.rerateDate.clear();
		screenVars.rerateFromDateDisp.clear();
		screenVars.rerateFromDate.clear();
		screenVars.benBillDateDisp.clear();
		screenVars.benBillDate.clear();
		screenVars.benpln.clear();
		screenVars.optdsc03.clear();
		screenVars.optind03.clear();
		screenVars.optdsc01.clear();
		screenVars.optind01.clear();
		screenVars.optdsc02.clear();
		screenVars.optind02.clear();
		screenVars.bappmeth.clear();
		screenVars.zdesc.clear();
		screenVars.zlinstprem.clear();
		screenVars.livesno.clear();
		screenVars.waiverprem.clear();
		screenVars.optdsc05.clear();
		screenVars.optind05.clear();
		screenVars.optdsc04.clear();
		screenVars.optind04.clear();
		screenVars.optdsc06.clear();
		screenVars.optind06.clear();
		screenVars.taxamt.clear();
		screenVars.optdsc07.clear();
		screenVars.optind07.clear();
		/*BRD-306 START*/
		screenVars.loadper.clear();
		screenVars.rateadj.clear();
		screenVars.fltmort.clear();
		screenVars.premadj.clear();
		screenVars.adjustageamt.clear();
		screenVars.select.clear();
		screenVars.riskCessAge.clear();
		screenVars.riskCessTerm.clear();
		screenVars.premCessAge.clear();
		screenVars.premCessTerm.clear();
		/*BRD-306 START*/
		//ILIFE-3421-STARTS
		screenVars.waitperiod.clear();
		screenVars.bentrm.clear();
		screenVars.poltyp.clear();
		screenVars.prmbasis.clear();
		//ILIFE-3421-STARTS-ENDS
		screenVars.dialdownoption.clear(); //BRD-NBP-011
		screenVars.optdsc08.clear();
		screenVars.optind08.clear();
		
	}
}
