package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MtlhpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:54
 * Class transformed from MTLHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MtlhpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 1750+DD.cltaddr.length*5;//pmujavadiya
	public FixedLengthStringData mtlhrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData mtlhpfRecord = mtlhrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData cowncoy = DD.cowncoy.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(mtlhrec);
	public PackedDecimalData trandate = DD.trandate.copy().isAPartOf(mtlhrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData salut = DD.salut.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData surname = DD.surname.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData givname = DD.givname.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData initials = DD.initials.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData cltaddr01 = DD.cltaddr.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData cltaddr02 = DD.cltaddr.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData cltaddr03 = DD.cltaddr.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData cltaddr04 = DD.cltaddr.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData cltaddr05 = DD.cltaddr.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData cltpcode = DD.cltpcode.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable01 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable02 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable03 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable04 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable05 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable06 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable07 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable08 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable09 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable10 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable11 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable12 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable13 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable14 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable15 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable16 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable17 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable18 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable19 = DD.crtable.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData crtable20 = DD.crtable.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt01 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt02 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt03 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt04 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt05 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt06 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt07 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt08 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt09 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt10 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt11 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt12 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt13 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt14 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt15 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt16 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt17 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt18 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt19 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt20 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData bonusAmt21 = DD.bonusamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins01 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins02 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins03 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins04 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins05 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins06 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins07 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins08 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins09 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins10 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins11 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins12 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins13 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins14 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins15 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins16 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins17 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins18 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins19 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins20 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData sumins21 = DD.sumins.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt01 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt02 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt03 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt04 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt05 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt06 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt07 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt08 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt09 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt10 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt11 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt12 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt13 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt14 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt15 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt16 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt17 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt18 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt19 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt20 = DD.matamt.copy().isAPartOf(mtlhrec);
	public PackedDecimalData matamt21 = DD.matamt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund01 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund02 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund03 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund04 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund05 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund06 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund07 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund08 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund09 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund10 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund11 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund12 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund13 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund14 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund15 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund16 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund17 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund18 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund19 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData virtualFund20 = DD.vrtfund.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip01 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip02 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip03 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip04 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip05 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip06 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip07 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip08 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip09 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip10 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip11 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip12 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip13 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip14 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip15 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip16 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip17 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip18 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip19 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData descrip20 = DD.descrip.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd01 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd02 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd03 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd04 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd05 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd06 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd07 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd08 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd09 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd10 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd11 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd12 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd13 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd14 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd15 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd16 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd17 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd18 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd19 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData currcd20 = DD.currcd.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType01 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType02 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType03 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType04 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType05 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType06 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType07 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType08 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType09 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType10 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType11 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType12 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType13 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType14 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType15 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType16 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType17 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType18 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType19 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData fieldType20 = DD.type.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt01 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt02 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt03 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt04 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt05 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt06 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt07 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt08 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt09 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt10 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt11 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt12 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt13 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt14 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt15 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt16 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt17 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt18 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt19 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData rcesdt20 = DD.rcesdt.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(mtlhrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(mtlhrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public MtlhpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for MtlhpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MtlhpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MtlhpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MtlhpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MtlhpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MtlhpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MTLHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"COWNCOY, " +
							"COWNNUM, " +
							"TRANDATE, " +
							"TRTM, " +
							"JLIFE, " +
							"SALUT, " +
							"SURNAME, " +
							"GIVNAME, " +
							"INITIALS, " +
							"CLTADDR01, " +
							"CLTADDR02, " +
							"CLTADDR03, " +
							"CLTADDR04, " +
							"CLTADDR05, " +
							"CLTPCODE, " +
							"CRTABLE01, " +
							"CRTABLE02, " +
							"CRTABLE03, " +
							"CRTABLE04, " +
							"CRTABLE05, " +
							"CRTABLE06, " +
							"CRTABLE07, " +
							"CRTABLE08, " +
							"CRTABLE09, " +
							"CRTABLE10, " +
							"CRTABLE11, " +
							"CRTABLE12, " +
							"CRTABLE13, " +
							"CRTABLE14, " +
							"CRTABLE15, " +
							"CRTABLE16, " +
							"CRTABLE17, " +
							"CRTABLE18, " +
							"CRTABLE19, " +
							"CRTABLE20, " +
							"BONUSAMT01, " +
							"BONUSAMT02, " +
							"BONUSAMT03, " +
							"BONUSAMT04, " +
							"BONUSAMT05, " +
							"BONUSAMT06, " +
							"BONUSAMT07, " +
							"BONUSAMT08, " +
							"BONUSAMT09, " +
							"BONUSAMT10, " +
							"BONUSAMT11, " +
							"BONUSAMT12, " +
							"BONUSAMT13, " +
							"BONUSAMT14, " +
							"BONUSAMT15, " +
							"BONUSAMT16, " +
							"BONUSAMT17, " +
							"BONUSAMT18, " +
							"BONUSAMT19, " +
							"BONUSAMT20, " +
							"BONUSAMT21, " +
							"SUMINS01, " +
							"SUMINS02, " +
							"SUMINS03, " +
							"SUMINS04, " +
							"SUMINS05, " +
							"SUMINS06, " +
							"SUMINS07, " +
							"SUMINS08, " +
							"SUMINS09, " +
							"SUMINS10, " +
							"SUMINS11, " +
							"SUMINS12, " +
							"SUMINS13, " +
							"SUMINS14, " +
							"SUMINS15, " +
							"SUMINS16, " +
							"SUMINS17, " +
							"SUMINS18, " +
							"SUMINS19, " +
							"SUMINS20, " +
							"SUMINS21, " +
							"MATAMT01, " +
							"MATAMT02, " +
							"MATAMT03, " +
							"MATAMT04, " +
							"MATAMT05, " +
							"MATAMT06, " +
							"MATAMT07, " +
							"MATAMT08, " +
							"MATAMT09, " +
							"MATAMT10, " +
							"MATAMT11, " +
							"MATAMT12, " +
							"MATAMT13, " +
							"MATAMT14, " +
							"MATAMT15, " +
							"MATAMT16, " +
							"MATAMT17, " +
							"MATAMT18, " +
							"MATAMT19, " +
							"MATAMT20, " +
							"MATAMT21, " +
							"VRTFUND01, " +
							"VRTFUND02, " +
							"VRTFUND03, " +
							"VRTFUND04, " +
							"VRTFUND05, " +
							"VRTFUND06, " +
							"VRTFUND07, " +
							"VRTFUND08, " +
							"VRTFUND09, " +
							"VRTFUND10, " +
							"VRTFUND11, " +
							"VRTFUND12, " +
							"VRTFUND13, " +
							"VRTFUND14, " +
							"VRTFUND15, " +
							"VRTFUND16, " +
							"VRTFUND17, " +
							"VRTFUND18, " +
							"VRTFUND19, " +
							"VRTFUND20, " +
							"DESCRIP01, " +
							"DESCRIP02, " +
							"DESCRIP03, " +
							"DESCRIP04, " +
							"DESCRIP05, " +
							"DESCRIP06, " +
							"DESCRIP07, " +
							"DESCRIP08, " +
							"DESCRIP09, " +
							"DESCRIP10, " +
							"DESCRIP11, " +
							"DESCRIP12, " +
							"DESCRIP13, " +
							"DESCRIP14, " +
							"DESCRIP15, " +
							"DESCRIP16, " +
							"DESCRIP17, " +
							"DESCRIP18, " +
							"DESCRIP19, " +
							"DESCRIP20, " +
							"CURRCD01, " +
							"CURRCD02, " +
							"CURRCD03, " +
							"CURRCD04, " +
							"CURRCD05, " +
							"CURRCD06, " +
							"CURRCD07, " +
							"CURRCD08, " +
							"CURRCD09, " +
							"CURRCD10, " +
							"CURRCD11, " +
							"CURRCD12, " +
							"CURRCD13, " +
							"CURRCD14, " +
							"CURRCD15, " +
							"CURRCD16, " +
							"CURRCD17, " +
							"CURRCD18, " +
							"CURRCD19, " +
							"CURRCD20, " +
							"TYPE01, " +
							"TYPE02, " +
							"TYPE03, " +
							"TYPE04, " +
							"TYPE05, " +
							"TYPE06, " +
							"TYPE07, " +
							"TYPE08, " +
							"TYPE09, " +
							"TYPE10, " +
							"TYPE11, " +
							"TYPE12, " +
							"TYPE13, " +
							"TYPE14, " +
							"TYPE15, " +
							"TYPE16, " +
							"TYPE17, " +
							"TYPE18, " +
							"TYPE19, " +
							"TYPE20, " +
							"RCESDT01, " +
							"RCESDT02, " +
							"RCESDT03, " +
							"RCESDT04, " +
							"RCESDT05, " +
							"RCESDT06, " +
							"RCESDT07, " +
							"RCESDT08, " +
							"RCESDT09, " +
							"RCESDT10, " +
							"RCESDT11, " +
							"RCESDT12, " +
							"RCESDT13, " +
							"RCESDT14, " +
							"RCESDT15, " +
							"RCESDT16, " +
							"RCESDT17, " +
							"RCESDT18, " +
							"RCESDT19, " +
							"RCESDT20, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     cowncoy,
                                     cownnum,
                                     trandate,
                                     transactionTime,
                                     jlife,
                                     salut,
                                     surname,
                                     givname,
                                     initials,
                                     cltaddr01,
                                     cltaddr02,
                                     cltaddr03,
                                     cltaddr04,
                                     cltaddr05,
                                     cltpcode,
                                     crtable01,
                                     crtable02,
                                     crtable03,
                                     crtable04,
                                     crtable05,
                                     crtable06,
                                     crtable07,
                                     crtable08,
                                     crtable09,
                                     crtable10,
                                     crtable11,
                                     crtable12,
                                     crtable13,
                                     crtable14,
                                     crtable15,
                                     crtable16,
                                     crtable17,
                                     crtable18,
                                     crtable19,
                                     crtable20,
                                     bonusAmt01,
                                     bonusAmt02,
                                     bonusAmt03,
                                     bonusAmt04,
                                     bonusAmt05,
                                     bonusAmt06,
                                     bonusAmt07,
                                     bonusAmt08,
                                     bonusAmt09,
                                     bonusAmt10,
                                     bonusAmt11,
                                     bonusAmt12,
                                     bonusAmt13,
                                     bonusAmt14,
                                     bonusAmt15,
                                     bonusAmt16,
                                     bonusAmt17,
                                     bonusAmt18,
                                     bonusAmt19,
                                     bonusAmt20,
                                     bonusAmt21,
                                     sumins01,
                                     sumins02,
                                     sumins03,
                                     sumins04,
                                     sumins05,
                                     sumins06,
                                     sumins07,
                                     sumins08,
                                     sumins09,
                                     sumins10,
                                     sumins11,
                                     sumins12,
                                     sumins13,
                                     sumins14,
                                     sumins15,
                                     sumins16,
                                     sumins17,
                                     sumins18,
                                     sumins19,
                                     sumins20,
                                     sumins21,
                                     matamt01,
                                     matamt02,
                                     matamt03,
                                     matamt04,
                                     matamt05,
                                     matamt06,
                                     matamt07,
                                     matamt08,
                                     matamt09,
                                     matamt10,
                                     matamt11,
                                     matamt12,
                                     matamt13,
                                     matamt14,
                                     matamt15,
                                     matamt16,
                                     matamt17,
                                     matamt18,
                                     matamt19,
                                     matamt20,
                                     matamt21,
                                     virtualFund01,
                                     virtualFund02,
                                     virtualFund03,
                                     virtualFund04,
                                     virtualFund05,
                                     virtualFund06,
                                     virtualFund07,
                                     virtualFund08,
                                     virtualFund09,
                                     virtualFund10,
                                     virtualFund11,
                                     virtualFund12,
                                     virtualFund13,
                                     virtualFund14,
                                     virtualFund15,
                                     virtualFund16,
                                     virtualFund17,
                                     virtualFund18,
                                     virtualFund19,
                                     virtualFund20,
                                     descrip01,
                                     descrip02,
                                     descrip03,
                                     descrip04,
                                     descrip05,
                                     descrip06,
                                     descrip07,
                                     descrip08,
                                     descrip09,
                                     descrip10,
                                     descrip11,
                                     descrip12,
                                     descrip13,
                                     descrip14,
                                     descrip15,
                                     descrip16,
                                     descrip17,
                                     descrip18,
                                     descrip19,
                                     descrip20,
                                     currcd01,
                                     currcd02,
                                     currcd03,
                                     currcd04,
                                     currcd05,
                                     currcd06,
                                     currcd07,
                                     currcd08,
                                     currcd09,
                                     currcd10,
                                     currcd11,
                                     currcd12,
                                     currcd13,
                                     currcd14,
                                     currcd15,
                                     currcd16,
                                     currcd17,
                                     currcd18,
                                     currcd19,
                                     currcd20,
                                     fieldType01,
                                     fieldType02,
                                     fieldType03,
                                     fieldType04,
                                     fieldType05,
                                     fieldType06,
                                     fieldType07,
                                     fieldType08,
                                     fieldType09,
                                     fieldType10,
                                     fieldType11,
                                     fieldType12,
                                     fieldType13,
                                     fieldType14,
                                     fieldType15,
                                     fieldType16,
                                     fieldType17,
                                     fieldType18,
                                     fieldType19,
                                     fieldType20,
                                     rcesdt01,
                                     rcesdt02,
                                     rcesdt03,
                                     rcesdt04,
                                     rcesdt05,
                                     rcesdt06,
                                     rcesdt07,
                                     rcesdt08,
                                     rcesdt09,
                                     rcesdt10,
                                     rcesdt11,
                                     rcesdt12,
                                     rcesdt13,
                                     rcesdt14,
                                     rcesdt15,
                                     rcesdt16,
                                     rcesdt17,
                                     rcesdt18,
                                     rcesdt19,
                                     rcesdt20,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		cowncoy.clear();
  		cownnum.clear();
  		trandate.clear();
  		transactionTime.clear();
  		jlife.clear();
  		salut.clear();
  		surname.clear();
  		givname.clear();
  		initials.clear();
  		cltaddr01.clear();
  		cltaddr02.clear();
  		cltaddr03.clear();
  		cltaddr04.clear();
  		cltaddr05.clear();
  		cltpcode.clear();
  		crtable01.clear();
  		crtable02.clear();
  		crtable03.clear();
  		crtable04.clear();
  		crtable05.clear();
  		crtable06.clear();
  		crtable07.clear();
  		crtable08.clear();
  		crtable09.clear();
  		crtable10.clear();
  		crtable11.clear();
  		crtable12.clear();
  		crtable13.clear();
  		crtable14.clear();
  		crtable15.clear();
  		crtable16.clear();
  		crtable17.clear();
  		crtable18.clear();
  		crtable19.clear();
  		crtable20.clear();
  		bonusAmt01.clear();
  		bonusAmt02.clear();
  		bonusAmt03.clear();
  		bonusAmt04.clear();
  		bonusAmt05.clear();
  		bonusAmt06.clear();
  		bonusAmt07.clear();
  		bonusAmt08.clear();
  		bonusAmt09.clear();
  		bonusAmt10.clear();
  		bonusAmt11.clear();
  		bonusAmt12.clear();
  		bonusAmt13.clear();
  		bonusAmt14.clear();
  		bonusAmt15.clear();
  		bonusAmt16.clear();
  		bonusAmt17.clear();
  		bonusAmt18.clear();
  		bonusAmt19.clear();
  		bonusAmt20.clear();
  		bonusAmt21.clear();
  		sumins01.clear();
  		sumins02.clear();
  		sumins03.clear();
  		sumins04.clear();
  		sumins05.clear();
  		sumins06.clear();
  		sumins07.clear();
  		sumins08.clear();
  		sumins09.clear();
  		sumins10.clear();
  		sumins11.clear();
  		sumins12.clear();
  		sumins13.clear();
  		sumins14.clear();
  		sumins15.clear();
  		sumins16.clear();
  		sumins17.clear();
  		sumins18.clear();
  		sumins19.clear();
  		sumins20.clear();
  		sumins21.clear();
  		matamt01.clear();
  		matamt02.clear();
  		matamt03.clear();
  		matamt04.clear();
  		matamt05.clear();
  		matamt06.clear();
  		matamt07.clear();
  		matamt08.clear();
  		matamt09.clear();
  		matamt10.clear();
  		matamt11.clear();
  		matamt12.clear();
  		matamt13.clear();
  		matamt14.clear();
  		matamt15.clear();
  		matamt16.clear();
  		matamt17.clear();
  		matamt18.clear();
  		matamt19.clear();
  		matamt20.clear();
  		matamt21.clear();
  		virtualFund01.clear();
  		virtualFund02.clear();
  		virtualFund03.clear();
  		virtualFund04.clear();
  		virtualFund05.clear();
  		virtualFund06.clear();
  		virtualFund07.clear();
  		virtualFund08.clear();
  		virtualFund09.clear();
  		virtualFund10.clear();
  		virtualFund11.clear();
  		virtualFund12.clear();
  		virtualFund13.clear();
  		virtualFund14.clear();
  		virtualFund15.clear();
  		virtualFund16.clear();
  		virtualFund17.clear();
  		virtualFund18.clear();
  		virtualFund19.clear();
  		virtualFund20.clear();
  		descrip01.clear();
  		descrip02.clear();
  		descrip03.clear();
  		descrip04.clear();
  		descrip05.clear();
  		descrip06.clear();
  		descrip07.clear();
  		descrip08.clear();
  		descrip09.clear();
  		descrip10.clear();
  		descrip11.clear();
  		descrip12.clear();
  		descrip13.clear();
  		descrip14.clear();
  		descrip15.clear();
  		descrip16.clear();
  		descrip17.clear();
  		descrip18.clear();
  		descrip19.clear();
  		descrip20.clear();
  		currcd01.clear();
  		currcd02.clear();
  		currcd03.clear();
  		currcd04.clear();
  		currcd05.clear();
  		currcd06.clear();
  		currcd07.clear();
  		currcd08.clear();
  		currcd09.clear();
  		currcd10.clear();
  		currcd11.clear();
  		currcd12.clear();
  		currcd13.clear();
  		currcd14.clear();
  		currcd15.clear();
  		currcd16.clear();
  		currcd17.clear();
  		currcd18.clear();
  		currcd19.clear();
  		currcd20.clear();
  		fieldType01.clear();
  		fieldType02.clear();
  		fieldType03.clear();
  		fieldType04.clear();
  		fieldType05.clear();
  		fieldType06.clear();
  		fieldType07.clear();
  		fieldType08.clear();
  		fieldType09.clear();
  		fieldType10.clear();
  		fieldType11.clear();
  		fieldType12.clear();
  		fieldType13.clear();
  		fieldType14.clear();
  		fieldType15.clear();
  		fieldType16.clear();
  		fieldType17.clear();
  		fieldType18.clear();
  		fieldType19.clear();
  		fieldType20.clear();
  		rcesdt01.clear();
  		rcesdt02.clear();
  		rcesdt03.clear();
  		rcesdt04.clear();
  		rcesdt05.clear();
  		rcesdt06.clear();
  		rcesdt07.clear();
  		rcesdt08.clear();
  		rcesdt09.clear();
  		rcesdt10.clear();
  		rcesdt11.clear();
  		rcesdt12.clear();
  		rcesdt13.clear();
  		rcesdt14.clear();
  		rcesdt15.clear();
  		rcesdt16.clear();
  		rcesdt17.clear();
  		rcesdt18.clear();
  		rcesdt19.clear();
  		rcesdt20.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMtlhrec() {
  		return mtlhrec;
	}

	public FixedLengthStringData getMtlhpfRecord() {
  		return mtlhpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMtlhrec(what);
	}

	public void setMtlhrec(Object what) {
  		this.mtlhrec.set(what);
	}

	public void setMtlhpfRecord(Object what) {
  		this.mtlhpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(mtlhrec.getLength());
		result.set(mtlhrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}