package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AnntpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:53
 * Class transformed from ANNTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AnntpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 101;
	public FixedLengthStringData anntrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData anntpfRecord = anntrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(anntrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(anntrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(anntrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(anntrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(anntrec);
	public PackedDecimalData seqnbr = DD.seqnbr.copy().isAPartOf(anntrec);
	public PackedDecimalData guarperd = DD.guarperd.copy().isAPartOf(anntrec);
	public FixedLengthStringData freqann = DD.freqann.copy().isAPartOf(anntrec);
	public FixedLengthStringData arrears = DD.arrears.copy().isAPartOf(anntrec);
	public FixedLengthStringData advance = DD.advance.copy().isAPartOf(anntrec);
	public PackedDecimalData dthpercn = DD.dthpercn.copy().isAPartOf(anntrec);
	public PackedDecimalData dthperco = DD.dthperco.copy().isAPartOf(anntrec);
	public PackedDecimalData intanny = DD.intanny.copy().isAPartOf(anntrec);
	public FixedLengthStringData withprop = DD.withprop.copy().isAPartOf(anntrec);
	public FixedLengthStringData withoprop = DD.withoprop.copy().isAPartOf(anntrec);
	public FixedLengthStringData ppind = DD.ppind.copy().isAPartOf(anntrec);
	public PackedDecimalData capcont = DD.capcont.copy().isAPartOf(anntrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(anntrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(anntrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(anntrec);
	public FixedLengthStringData nomlife = DD.nomlife.copy().isAPartOf(anntrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(anntrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AnntpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AnntpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AnntpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AnntpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AnntpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AnntpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AnntpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ANNTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"SEQNBR, " +
							"GUARPERD, " +
							"FREQANN, " +
							"ARREARS, " +
							"ADVANCE, " +
							"DTHPERCN, " +
							"DTHPERCO, " +
							"INTANNY, " +
							"WITHPROP, " +
							"WITHOPROP, " +
							"PPIND, " +
							"CAPCONT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"NOMLIFE, " +
							"PLNSFX, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     seqnbr,
                                     guarperd,
                                     freqann,
                                     arrears,
                                     advance,
                                     dthpercn,
                                     dthperco,
                                     intanny,
                                     withprop,
                                     withoprop,
                                     ppind,
                                     capcont,
                                     userProfile,
                                     jobName,
                                     datime,
                                     nomlife,
                                     planSuffix,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		seqnbr.clear();
  		guarperd.clear();
  		freqann.clear();
  		arrears.clear();
  		advance.clear();
  		dthpercn.clear();
  		dthperco.clear();
  		intanny.clear();
  		withprop.clear();
  		withoprop.clear();
  		ppind.clear();
  		capcont.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		nomlife.clear();
  		planSuffix.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAnntrec() {
  		return anntrec;
	}

	public FixedLengthStringData getAnntpfRecord() {
  		return anntpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAnntrec(what);
	}

	public void setAnntrec(Object what) {
  		this.anntrec.set(what);
	}

	public void setAnntpfRecord(Object what) {
  		this.anntpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(anntrec.getLength());
		result.set(anntrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}