package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.CrsvpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Crsvpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CrsvpfDAOImpl extends BaseDAOImpl<Crsvpf> implements CrsvpfDAO{
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(CrsvpfDAOImpl.class);
	
	 @Override
	public void insertCrsvRecord(Crsvpf crsvpf) {
		
		 StringBuilder sb = new StringBuilder("");
		 
		 sb.append("INSERT INTO CRSVPF ");
		 sb.append("(WCLMPFX, WCLMCOY, CHDRNUM, TRANNO, TRCODE, CONDTE, CLAIM, BALO, WPAID, WPRCL");
		 sb.append(", WREQAMT, WRSCD, TERMID, VALIDFLAG, TRDT, TRTM, USER_T, USRPRF, JOBNM, DATIME)");
		 sb.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		 
			PreparedStatement ps = getPrepareStatement(sb.toString());
	        
	         try {
	      	
	       	 ps.setString(1, crsvpf.getWclmpfx());
	       	 ps.setString(2, crsvpf.getWclmcoy());
	       	 ps.setString(3, crsvpf.getChdrnum());
	       	 ps.setInt(4, crsvpf.getTranno());
	       	 ps.setString(5, crsvpf.getTrcode()!= null ? crsvpf.getTrcode() :" ");
	       	 ps.setInt(6, crsvpf.getCondte());
	       	 ps.setString(7,crsvpf.getClaim());
	       	 ps.setBigDecimal(8, crsvpf.getBalo());
	       	 ps.setBigDecimal(9, crsvpf.getWpaid()!= null ? crsvpf.getWpaid() :  BigDecimal.ZERO );
	       	 ps.setString(10,crsvpf.getWprcl());
	       	 ps.setBigDecimal(11, crsvpf.getWreqamt()!= null ? crsvpf.getWreqamt() :  BigDecimal.ZERO);
	       	 ps.setString(12, crsvpf.getWrscd());
	       	 ps.setString(13,crsvpf.getTermid()!= null ? crsvpf.getTermid() : " ");
	       	 ps.setString(14, crsvpf.getValidflag());
	       	 ps.setInt(15, crsvpf.getTrdt());
	       	 ps.setInt(16, crsvpf.getTrtm()!= null ? crsvpf.getTrtm() :  0);
	       	 ps.setInt(17, crsvpf.getUserT()!= null ? crsvpf.getUserT() :  0);
	       	 ps.setString(18, getUsrprf());
	       	 ps.setString(19, getJobnm());
	       	 ps.setTimestamp(20, new Timestamp(System.currentTimeMillis()));
	       	 
	       	 
	       	 ps.executeUpdate();
	         }catch (SQLException e) {
					LOGGER.error("insertCrsvRecord()",e);	
					throw new SQLRuntimeException(e);
				} finally {
					close(ps, null);
				}		

	}

	@Override
	public void updateValidFlgCrsvRecord(Crsvpf crsvpf,String validflag) {

		
		 StringBuilder sb = new StringBuilder("");
		 sb.append("UPDATE CRSVPF SET VALIDFLAG=?,TRDT=?,TRTM=?,JOBNM=?,USRPRF=?,DATIME=? WHERE WCLMCOY=? AND CHDRNUM=? AND VALIDFLAG =? AND TRCODE = ?  ");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		
		try {
			ps.setString(1,validflag);
			ps.setInt(2, crsvpf.getTrdt()!= null ? crsvpf.getTrdt() : 0);
			ps.setInt(3, crsvpf.getTrtm()!= null ? crsvpf.getTrtm() : 0);
			ps.setString(4, getUsrprf());
	        ps.setString(5, getJobnm());
	        ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
	        ps.setString(7, crsvpf.getWclmcoy());
	        ps.setString(8, crsvpf.getChdrnum());
	        ps.setString(9, crsvpf.getValidflag());
	        ps.setString(10, crsvpf.getTrcode());
			
	        ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("invalidateCrsvRecord()" , e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	@Override
	public List<Crsvpf> getCrsvpfRecord(String chdrnum, String wclmcoy, String validflag, String trcode) {
		
		  List<Crsvpf> result = new ArrayList<>();
		  ResultSet rs =null;
		  PreparedStatement ps = null;
		  StringBuilder sb = new StringBuilder();
		   
		   sb.append("SELECT UNIQUE_NUMBER, WCLMPFX, WCLMCOY, CHDRNUM, TRANNO, TRCODE, CONDTE, CLAIM, BALO");
		   sb.append(", WPAID, WPRCL, WREQAMT, WRSCD, TERMID, VALIDFLAG, TRDT, TRTM ");
		   sb.append("FROM CRSVPF WHERE CHDRNUM = ? AND WCLMCOY = ? AND VALIDFLAG = ? AND TRCODE = ? ORDER BY TRDT DESC");
		  
		   try{
			   ps = getPrepareStatement(sb.toString());
			   ps.setString(1,chdrnum);
			   ps.setString(2, wclmcoy);
			   ps.setString(3, validflag);
			   ps.setString(4, trcode);
			    rs = ps.executeQuery();
			   
			   while (rs.next()) {
					Crsvpf crsvpf = new Crsvpf();
					
					crsvpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					crsvpf.setWclmpfx(rs.getString("WCLMPFX"));
					crsvpf.setWclmcoy(rs.getString("WCLMCOY"));
					crsvpf.setChdrnum(rs.getString("CHDRNUM"));
					crsvpf.setTranno(rs.getInt("TRANNO"));
					crsvpf.setTrcode(rs.getString("TRCODE") != null ? rs.getString("TRCODE") : " " );
					crsvpf.setCondte(rs.getInt("CONDTE"));
					crsvpf.setClaim(rs.getString("CLAIM"));
					crsvpf.setBalo(rs.getBigDecimal("BALO"));
					crsvpf.setWpaid(rs.getBigDecimal("WPAID"));
					crsvpf.setWprcl(rs.getString("WPRCL"));
					crsvpf.setWreqamt(rs.getBigDecimal("WREQAMT"));
					crsvpf.setWrscd(rs.getString("WRSCD"));
					crsvpf.setTermid(rs.getString("TERMID"));
					crsvpf.setValidflag(rs.getString("VALIDFLAG"));
					crsvpf.setTrdt(rs.getInt("TRDT"));
					crsvpf.setTrtm(rs.getInt("TRTM"));
					
					result.add(crsvpf);
				
			   }
			} catch (SQLException e) {
				LOGGER.error("error has occured in getCrsvpfRecord", e);
				throw new SQLRuntimeException(e);
		   }
		   finally {
				close(ps,rs);
			}	
		return result;
	}
	
	@Override
	public  List<Crsvpf> getCrsvpfRecordByTranno(String chdrnum, String wclmcoy, String validflag, String trcode,String tranno){
		
		  List<Crsvpf> result = new ArrayList<>();
		  ResultSet rs =null;
		  PreparedStatement ps = null;
		  StringBuilder sb = new StringBuilder();
		   
		   sb.append("SELECT UNIQUE_NUMBER, WCLMPFX, WCLMCOY, CHDRNUM, TRANNO, TRCODE, CONDTE, CLAIM, BALO");
		   sb.append(", WPAID, WPRCL, WREQAMT, WRSCD, TERMID, VALIDFLAG, TRDT, TRTM ");
		   sb.append("FROM CRSVPF WHERE CHDRNUM = ? AND VALIDFLAG = ? AND TRCODE = ? AND TRANNO=? ORDER BY TRDT DESC");
		  
		   try{
			   ps = getPrepareStatement(sb.toString());
			   ps.setString(1,chdrnum);
			   ps.setString(2, validflag);
			   ps.setString(3, trcode);
			   ps.setString(4, tranno);
			    rs = ps.executeQuery();
			   
			   while (rs.next()) {
					Crsvpf crsvpf = new Crsvpf();
					
					crsvpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
					crsvpf.setWclmpfx(rs.getString("WCLMPFX"));
					crsvpf.setWclmcoy(rs.getString("WCLMCOY"));
					crsvpf.setChdrnum(rs.getString("CHDRNUM"));
					crsvpf.setTranno(rs.getInt("TRANNO"));
					crsvpf.setTrcode(rs.getString("TRCODE") != null ? rs.getString("TRCODE") : " " );
					crsvpf.setCondte(rs.getInt("CONDTE"));
					crsvpf.setClaim(rs.getString("CLAIM"));
					crsvpf.setBalo(rs.getBigDecimal("BALO"));
					crsvpf.setWpaid(rs.getBigDecimal("WPAID"));
					crsvpf.setWprcl(rs.getString("WPRCL"));
					crsvpf.setWreqamt(rs.getBigDecimal("WREQAMT"));
					crsvpf.setWrscd(rs.getString("WRSCD"));
					crsvpf.setTermid(rs.getString("TERMID"));
					crsvpf.setValidflag(rs.getString("VALIDFLAG"));
					crsvpf.setTrdt(rs.getInt("TRDT"));
					crsvpf.setTrtm(rs.getInt("TRTM"));
					
					result.add(crsvpf);
				
			   }
			} catch (SQLException e) {
				LOGGER.error("error has occured in getCrsvpfRecord", e);
				throw new SQLRuntimeException(e);
		   }
		   finally {
				close(ps,rs);
			}	
		return result;
	}

	@Override
	public void deleteCrsvpfRecord(long uniqueNumber) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM CRSVPF WHERE UNIQUE_NUMBER = ?");
		
		PreparedStatement ps = getPrepareStatement(sb.toString());
		
		   try {
			   ps.setLong(1,uniqueNumber);
			   ps.executeUpdate();
		   }catch (SQLException e) {
				LOGGER.error("deleteCrsvpfRecord()",e);	
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}		
	}
	
	@Override
	public Crsvpf getRecord(String chdrnum) {
		
		  Crsvpf crsvpf = null;
		  ResultSet rs =null;
		  PreparedStatement ps = null; 
		  StringBuilder sb = new StringBuilder();
		   
		  sb.append("SELECT * FROM CRSVPF WHERE CHDRNUM = ? AND VALIDFLAG = '1' ;");
		  
		  try{
			  ps = getPrepareStatement(sb.toString());
		      ps.setString(1,chdrnum);
		      rs = ps.executeQuery(); 
		      
			  while (rs.next()) {
				crsvpf = new Crsvpf();
					
				crsvpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				crsvpf.setWclmcoy(rs.getString("WCLMCOY"));
				crsvpf.setWclmpfx(rs.getString("WCLMPFX"));
				crsvpf.setChdrnum(rs.getString("CHDRNUM"));
				crsvpf.setTranno(rs.getInt("TRANNO"));
				crsvpf.setTrcode(rs.getString("TRCODE") != null ? rs.getString("TRCODE") : " " );
				crsvpf.setCondte(rs.getInt("CONDTE"));
				crsvpf.setClaim(rs.getString("CLAIM"));
				crsvpf.setBalo(rs.getBigDecimal("BALO"));
				crsvpf.setWpaid(rs.getBigDecimal("WPAID"));
				crsvpf.setWprcl(rs.getString("WPRCL"));
				crsvpf.setWreqamt(rs.getBigDecimal("WREQAMT"));
				crsvpf.setWrscd(rs.getString("WRSCD"));
				crsvpf.setPaydate(rs.getInt("PAYDATE"));
				crsvpf.setTermid(rs.getString("TERMID"));
				crsvpf.setTrdt(rs.getInt("TRDT"));
				crsvpf.setTrtm(rs.getInt("TRTM"));
				crsvpf.setValidflag(rs.getString("VALIDFLAG"));
				crsvpf.setUserT(rs.getInt("USER_T"));
				crsvpf.setUsrprf(rs.getString("USRPRF"));
			   }
			} 
		    catch (SQLException e) {
				LOGGER.error("error has occured in getCrsvpfRecord", e);
				throw new SQLRuntimeException(e);
			}
		    finally {
				close(ps,rs);
			}	
		  return crsvpf;
		}
	
	@Override
	public void updateCrsvRecord(Crsvpf crsvpfItem) {

		StringBuilder sb = new StringBuilder("");
        sb.append("UPDATE CRSVPF SET BALO=?, WREQAMT= ?, PAYDATE=?, JOBNM=?, USRPRF=?, "
        		+ "DATIME=?, WPAID = ? WHERE CHDRNUM=? AND VALIDFLAG = 1");
        PreparedStatement ps = getPrepareStatement(sb.toString());
        
        try {
            ps.setBigDecimal(1,crsvpfItem.getBalo());
            ps.setBigDecimal(2, crsvpfItem.getWreqamt());
            ps.setInt(3, crsvpfItem.getPaydate());
            ps.setString(4, getJobnm());
            ps.setString(5, getUsrprf());
	        ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
	        if(crsvpfItem.getWpaid() == null) {
	        	ps.setBigDecimal(7, BigDecimal.ZERO);
	        }else {
	        	ps.setBigDecimal(7, crsvpfItem.getWpaid());
	        }
            ps.setString(8, crsvpfItem.getChdrnum());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("invalidateCrsvRecord()" , e);
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    } 

}
