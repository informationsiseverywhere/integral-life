package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;

public class Ptshpf {
	private long uniqueNumber;
	private String chdrcoy;
    private String chdrnum;
    private String life;
    private String jlife;
    private String currcd;
    private Long effdate;
    private int tranno;
    private BigDecimal totalamt;
    private BigDecimal prcnt;
    private BigDecimal planSuffix;
    private String cnttype;
    private String userProfile;
    private String jobName;
    private String datime;
    
	public String getCurrcd() {
		return currcd;
	}
	public void setCurrcd(String currcd) {
		this.currcd = currcd;
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public Long getEffdate() {
		return effdate;
	}
	public void setEffdate(Long effdate) {
		this.effdate = effdate;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public BigDecimal getTotalamt() {
		return totalamt;
	}
	public void setTotalamt(BigDecimal totalamt) {
		this.totalamt = totalamt;
	}
	public BigDecimal getPrcnt() {
		return prcnt;
	}
	public void setPrcnt(BigDecimal prcnt) {
		this.prcnt = prcnt;
	}
	public BigDecimal getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(BigDecimal planSuffix) {
		this.planSuffix = planSuffix;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}

    
    
}
