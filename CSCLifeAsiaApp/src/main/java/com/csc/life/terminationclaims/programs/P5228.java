/*
 * File: P5228.java
 * Date: 30 August 2009 0:20:36
 * Author: Quipoz Limited
 * 
 * Class transformed from P5228.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILB-459
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;//ILB-459
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;//ILB-459
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;//ILB-459
import com.csc.life.productdefinition.dataaccess.model.Lifepf;//ILB-459
import com.csc.life.terminationclaims.screens.S5228ScreenVars;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                DEATH DETAILS PROGRAM.
*
* This is  a new  program which  forms part of the 9405  Annuities
* Development.  It is  a  component of  the processing to register
* the  death  of  one  life  on  a joint life contract, and is NOT
* restricted to annuity contracts.
*
* The screen  displays details of the life assured  selected and a
* date  of death and  reason for  death must be input.  The reason
* for death is validated against the Reason Codes Table, T5500.
*
* The CLNT  file will be updated  with the date of  death and then
* the AT request will be submitted to do all other processing. The
* AT  module  will  be  able  to  identify  which  LIFE record was
* selected  from  the  CLMHCLM  (Claim Header) record written from
* this program.
*
* All LIFE file updating will be performed in the AT Module.
*
*****************************************************************
* </pre>
*/
public class P5228 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5228");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData filler1 = new FixedLengthStringData(183).isAPartOf(wsaaTransactionRec, 17, FILLER).init(SPACES);
		/* ERRORS */
	private static final String f910 = "F910";
	private static final String h073 = "H073";
	private static final String h102 = "H102";
	private static final String t064 = "T064";
		/* FORMATS */
	private static final String chdrclmrec = "CHDRCLMREC";
	private static final String lifeclmrec = "LIFECLMREC";
	private static final String cltsrec = "CLTSREC";
	private static final String clmhclmrec = "CLMHCLMREC";
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
		/*Claim Header file*/
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private LifeclmTableDAM lifeclmIO = new LifeclmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Atreqrec atreqrec = new Atreqrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wssplife wssplife = new Wssplife();
	private S5228ScreenVars sv = ScreenProgram.getScreenVars( S5228ScreenVars.class);
	//ILB-459
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private List<Lifepf> LifepfList;
	private Lifepf lifepf = new Lifepf();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090, 
		exit3090
	}

	public P5228() {
		super();
		screenVars = sv;
		new ScreenModel("S5228", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.dtofdeath.set(varcom.maxdate);
		sv.dob.set(varcom.maxdate);
		sv.reasoncd.set(SPACES);
		sv.resndesc.set(SPACES);
		/* Get Todays Date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		/* Retrieve CHDRCLM stored previously.*/
		//ILB-459 starts
		/*chdrclmIO.setFunction(varcom.retrv);
		chdrclmIO.setFormat(chdrclmrec);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			fatalError600();
		}*/
		
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrclmIO.setFunction(varcom.retrv);
			chdrclmIO.setFormat(chdrclmrec);
			SmartFileCode.execute(appVars, chdrclmIO);
			if (isNE(chdrclmIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrclmIO.getParams());
				syserrrec.statuz.set(chdrclmIO.getStatuz());
				fatalError600();
			}
			
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrclmIO.getChdrcoy().toString(), chdrclmIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		
		/* Retrieve LIFECLM stored previously and set up screen fields.*/
		
		/*lifeclmIO.setFunction(varcom.retrv);
		lifeclmIO.setFormat(lifeclmrec);
		SmartFileCode.execute(appVars, lifeclmIO);
		if (isNE(lifeclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeclmIO.getParams());
			syserrrec.statuz.set(lifeclmIO.getStatuz());
			fatalError600();
		}*/
		
		lifepf = lifepfDAO.getCacheObject(lifepf);
		if(null==lifepf) {
			lifeclmIO.setFunction(varcom.retrv);
			lifeclmIO.setFormat(lifeclmrec);
			SmartFileCode.execute(appVars, lifeclmIO);
			if (isNE(lifeclmIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(lifeclmIO.getParams());
				syserrrec.statuz.set(lifeclmIO.getStatuz());
				fatalError600();
			}
			else {
				lifepf = lifepfDAO.getLifeRecord(lifeclmIO.getChdrcoy().toString(), lifeclmIO.getChdrnum().toString(),lifeclmIO.getLife().toString(),lifeclmIO.getJlife().toString());
				if(null==lifepf) {
					fatalError600();
				}
				else {
					lifepfDAO.setCacheObject(lifepf);
				}
			}
		}
		//ILB-459 end
		sv.chdrnum.set(lifepf.getChdrnum());
		sv.life.set(lifepf.getLife());
		sv.jlife.set(lifepf.getJlife());
		sv.sex.set(lifepf.getCltsex());
		sv.dob.set(lifepf.getCltdob());
		sv.lifenum.set(lifepf.getLifcnum());
		/* Get life assured name and format for screen use.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifepf.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5228IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S5228-DATA-AREA.           */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/* Validate Date of Death field.*/
		if (isNE(sv.dtofdeathErr,SPACES)) {
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.dtofdeath,varcom.vrcmMaxDate)
		|| isEQ(sv.dtofdeath,ZERO)) {
			sv.dtofdeathErr.set(h102);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isGT(sv.dtofdeath,wsaaToday)) {
			sv.dtofdeathErr.set(t064);
		}
		else {
			if (isLT(sv.dtofdeath,chdrpf.getOccdate())) {
				sv.dtofdeathErr.set(h073);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			skipOnKill3010();
			updateDatabase3020();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void skipOnKill3010()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void updateDatabase3020()
	{
		cltsIO.setFunction(varcom.readh);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		cltsIO.setCltdod(sv.dtofdeath);
		/*bug #ILIFE-1008 start*/
		cltsIO.setCltstat("DN");
		/*bug #ILIFE-1008 end*/
		cltsIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		clmhclmIO.setDataArea(SPACES);
		clmhclmIO.setChdrcoy(lifepf.getChdrcoy());
		clmhclmIO.setChdrnum(lifepf.getChdrnum());
		/* TRANNO has already been incremented by 1 in the Sub-menu.*/
		clmhclmIO.setTranno(chdrpf.getTranno()); //ILIFE-8261
		clmhclmIO.setLife(lifepf.getLife());
		clmhclmIO.setJlife(lifepf.getJlife());
		clmhclmIO.setDtofdeath(sv.dtofdeath);
		clmhclmIO.setEffdate(wsaaToday);
		clmhclmIO.setReasoncd(sv.reasoncd);
		clmhclmIO.setResndesc(sv.resndesc);
		clmhclmIO.setCnttype(chdrpf.getCnttype());
		clmhclmIO.setValidflag("1");
		clmhclmIO.setPolicyloan(ZERO);
		clmhclmIO.setOtheradjst(ZERO);
		clmhclmIO.setZrcshamt(ZERO);
		clmhclmIO.setInterest(ZERO);
		clmhclmIO.setIntdays(ZERO);
		clmhclmIO.setOfcharge(ZERO);
		clmhclmIO.setFunction(varcom.writr);
		clmhclmIO.setFormat(clmhclmrec);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(clmhclmIO.getStatuz());
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
		/* Transfer the softlock on the contract.*/
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.enttyp.set(chdrpf.getChdrpfx());
		sftlockrec.company.set(chdrpf.getChdrcoy());
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/* CALL the AT module.*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5228AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrpf.getChdrnum());
		wsaaFsuco.set(wsspcomn.fsuco);
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
