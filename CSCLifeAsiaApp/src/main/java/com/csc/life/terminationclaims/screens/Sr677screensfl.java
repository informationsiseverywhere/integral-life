package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr677screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 12, 3, 21}; 
	public static int maxRecords = 10;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2, 13}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr677ScreenVars sv = (Sr677ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr677screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr677screensfl, 
			sv.Sr677screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr677ScreenVars sv = (Sr677ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr677screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr677ScreenVars sv = (Sr677ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr677screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr677screensflWritten.gt(0))
		{
			sv.sr677screensfl.setCurrentIndex(0);
			sv.Sr677screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr677ScreenVars sv = (Sr677ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr677screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr677ScreenVars screenVars = (Sr677ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.relation.setFieldName("relation");
				screenVars.clntnum.setFieldName("clntnum");
				screenVars.clntname.setFieldName("clntname");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.relation.set(dm.getField("relation"));
			screenVars.clntnum.set(dm.getField("clntnum"));
			screenVars.clntname.set(dm.getField("clntname"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr677ScreenVars screenVars = (Sr677ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.relation.setFieldName("relation");
				screenVars.clntnum.setFieldName("clntnum");
				screenVars.clntname.setFieldName("clntname");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("relation").set(screenVars.relation);
			dm.getField("clntnum").set(screenVars.clntnum);
			dm.getField("clntname").set(screenVars.clntname);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr677screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr677ScreenVars screenVars = (Sr677ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.relation.clearFormatting();
		screenVars.clntnum.clearFormatting();
		screenVars.clntname.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr677ScreenVars screenVars = (Sr677ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.relation.setClassString("");
		screenVars.clntnum.setClassString("");
		screenVars.clntname.setClassString("");
	}

/**
 * Clear all the variables in Sr677screensfl
 */
	public static void clear(VarModel pv) {
		Sr677ScreenVars screenVars = (Sr677ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.relation.clear();
		screenVars.clntnum.clear();
		screenVars.clntname.clear();
	}
}
