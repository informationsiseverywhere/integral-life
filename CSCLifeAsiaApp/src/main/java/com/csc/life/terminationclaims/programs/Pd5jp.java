/*
 * File: Pd5jp.java
 * Date: 07 Dec 2018 1:10:03
 * Author: Quipoz Limited
 *
 * Class transformed from PD5JP.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;


import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.screens.Sd5jpScreenVars;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pd5jp extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5JP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 8);
		/* ERRORS */
	private static final String e186 = "E186";
	private static final String e304 = "E304";
	private static final String rlas = "RLAS";
	private static final String hl57 = "HL57";
		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5688 = "T5688";
	private static final String t5500 = "T5500";
	private static final String tr384 = "TR384";
	private static final String th584 = "TH584";
	private static final String ptrnrec = "PTRNREC";
	private static final String resnrec = "RESNREC";
	private static final String chdrmnarec = "CHDRMNAREC";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private Hpadpf hpadIO = new Hpadpf();
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private Batckey wsaaBatcKey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Tr384rec tr384rec = new Tr384rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wssplife wssplife = new Wssplife();
	private Sd5jpScreenVars sv = ScreenProgram.getScreenVars( Sd5jpScreenVars.class);
	private DescDAO descdao =new DescDAOImpl();/*ILIFE-4036*/
	Descpf descpf=null;
	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Ptrnpf ptrnpf = null;
	private List<Ptrnpf> ptrnpfList = null;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		reascd2050,
		checkForErrors2080
	}

	public Pd5jp() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5jp", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		blankOutFields1020();
	}

protected void initialise1010()
	{
		wsaaBatcKey.set(wsspcomn.batchkey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}

protected void blankOutFields1020()
	{
		sv.dataArea.set(SPACES);
		/* Retrieve contract fields from I/O module*/
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		descpf= descdao.getdescData("IT", t5688, chdrmnaIO.getCnttype().toString(), wsspcomn.company.toString(),wsspcomn.language.toString());
		if(descpf != null)
		{
			sv.ctypedes.set(descpf.getLongdesc());
		}else{
			sv.ctypedes.set(SPACE); 	
		}

		/*  Load all fields from the Contract Header to the Screen*/
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.ownersel.set(chdrmnaIO.getCownnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.ptdate.set(chdrmnaIO.getPtdate());
		
		descpf= descdao.getdescData("IT", t3588, chdrmnaIO.getPstatcode().toString(), wsspcomn.company.toString(),wsspcomn.language.toString());
		if(descpf != null)
		{
			sv.premStatDesc.set(descpf.getShortdesc());
		}else{
			sv.premStatDesc.set(SPACE); 	
		}
		descpf= descdao.getdescData("IT", t3623, chdrmnaIO.getStatcode().toString(), wsspcomn.company.toString(),wsspcomn.language.toString());
		if(descpf != null)
		{
			sv.statdsc.set(descpf.getShortdesc());
		}else{
			sv.statdsc.fill("?");
		}
		
		
		/*   Look up all other descriptions*/
		/*       - owner name*/
		if (isNE(sv.ownersel, SPACES)) {
			formatClientName1700();
		}
		formatNfftDetails5000();
	}

	/**
	* <pre>
	*      RETRIEVE OWNER DETAILS AND SET SCREEN
	* </pre>
	*/
protected void formatClientName1700()
	{
		readClientRecord1710();
	}

protected void readClientRecord1710()
	{
		clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),sv.ownersel.toString());
		
		if(clntpf==null||isNE(clntpf.getValidflag(), 1)){
			sv.ownerselErr.set(e304);
			sv.ownername.set(SPACES);
		}else{
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		
	
		
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}
protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(sv.rfundflg01, SPACES)) {
			sv.rfundflg01.set("N");
		}
		if (isEQ(sv.rfundflg02, sv.rfundflg01)
		||  isEQ(sv.rfundflg02, SPACES)	) {
			sv.rfundflg02Err.set(rlas);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			checkForErrors2080();
		}
	}

protected void checkForErrors2080()
	{
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
		relsfl3070();
	}

protected void updateDatabase3010()
	{
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmnaIO.setTranid(varcom.vrcmCompTranid);
		/*  Update contract header fields as follows*/
		chdrmnaIO.setFunction(varcom.keeps);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setFunction(varcom.writs);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		/* Re-write the HPAD record for this contract.*/
		hpadIO = hpadpfDAO.getHpadData(chdrmnaIO.getChdrcoy().toString(), chdrmnaIO.getChdrnum().toString());
		hpadIO.setRefundOverpay(sv.rfundflg02.toString());
		hpadpfDAO.updateRecord(hpadIO);
		wsaaBatcKey.set(wsspcomn.batchkey);
		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcactmn(wsaaBatcKey.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatcKey.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatcKey.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatcKey.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatcKey.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatcKey.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatcKey.batcBatctrcde.toString());
		ptrnpf.setTranno(chdrmnaIO.getTranno().toInt());
		ptrnpf.setPtrneff(wsaaToday.toInt());
		ptrnpf.setChdrcoy(chdrmnaIO.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrmnaIO.getChdrnum().toString());
		ptrnpf.setValidflag("1");
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setDatesub(wsaaToday.toInt());
		ptrnpf.setChdrpfx("  ");
		ptrnpf.setPrtflg(" ");
		ptrnpf.setCrtuser(" ");
		ptrnpf.setRecode(" ");
		ptrnpfList = new ArrayList<Ptrnpf>();
		ptrnpfList.add(ptrnpf);
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
		
		
		/* Update the batch header using BATCUP.*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(wsspcomn.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.statuz.set(ZERO);
		batcuprec.function.set(SPACES);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError600();
		}
	}

protected void relsfl3070()
	{
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void formatNfftDetails5000()
	{
		start5010();
	}

protected void start5010()
	{
		hpadIO = hpadpfDAO.getHpadData(chdrmnaIO.getChdrcoy().toString(), chdrmnaIO.getChdrnum().toString());

		if(hpadIO == null){
			fatalError600();
		} 
		else 
		{
			if (null == hpadIO.getRefundOverpay() || isEQ(hpadIO.getRefundOverpay(), SPACES)) {
				sv.rfundflg01.set("N");
			} 
			else 
			{
				sv.rfundflg01.set(hpadIO.getRefundOverpay());	
			}
		}
	}
}

