package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl53screensfl extends Subfile{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {6, 7, 8, 9, 10};
	public static int maxRecords = 12;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {6, 7, 8, 9, 10}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 16, 1, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl53ScreenVars sv = (Sjl53ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.Sjl53screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.Sjl53screensfl, 
			sv.Sjl53screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sjl53ScreenVars sv = (Sjl53ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.Sjl53screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sjl53ScreenVars sv = (Sjl53ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.Sjl53screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		if (ind3.isOn() && sv.Sjl53screensflWritten.gt(0))
		{
			sv.Sjl53screensfl.setCurrentIndex(0);
			sv.Sjl53screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sjl53ScreenVars sv = (Sjl53ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.Sjl53screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl53ScreenVars screenVars = (Sjl53ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.paymentNum.setFieldName("paymentNum");
				screenVars.paymtDateDisp.setFieldName("paymtDateDisp");
				screenVars.clntId.setFieldName("clntId");
				screenVars.amount.setFieldName("amount");
				screenVars.paymtMethod.setFieldName("paymtMethod");
				screenVars.bankAccount.setFieldName("bankAccount");
				screenVars.paymtStatus.setFieldName("paymtStatus");
				screenVars.seqenum.setFieldName("seqenum");
				screenVars.select.setFieldName("select");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.paymentNum.set(dm.getField("paymentNum"));
			screenVars.paymtDateDisp.set(dm.getField("paymtDateDisp"));
			screenVars.clntId.set(dm.getField("clntId"));
			screenVars.amount.set(dm.getField("amount"));
			screenVars.paymtMethod.set(dm.getField("paymtMethod"));
			screenVars.bankAccount.set(dm.getField("bankAccount"));
			screenVars.paymtStatus.set(dm.getField("paymtStatus"));
			screenVars.seqenum.set(dm.getField("seqenum"));
			screenVars.select.set(dm.getField("select"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl53ScreenVars screenVars = (Sjl53ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.paymentNum.setFieldName("paymentNum");
				screenVars.paymtDateDisp.setFieldName("paymtDateDisp");
				screenVars.clntId.setFieldName("clntId");
				screenVars.amount.setFieldName("amount");
				screenVars.paymtMethod.setFieldName("paymtMethod");
				screenVars.bankAccount.setFieldName("bankAccount");
				screenVars.paymtStatus.setFieldName("paymtStatus");
				screenVars.seqenum.setFieldName("seqenum");
				screenVars.select.setFieldName("select");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("paymentNum").set(screenVars.paymentNum);
			dm.getField("paymtDateDisp").set(screenVars.paymtDateDisp);
			dm.getField("clntId").set(screenVars.clntId);
			dm.getField("amount").set(screenVars.amount);
			dm.getField("paymtMethod").set(screenVars.paymtMethod);
			dm.getField("bankAccount").set(screenVars.bankAccount);
			dm.getField("paymtStatus").set(screenVars.paymtStatus);
			dm.getField("seqenum").set(screenVars.seqenum);
			dm.getField("select").set(screenVars.select);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sjl53screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sjl53ScreenVars screenVars = (Sjl53ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.paymentNum.clearFormatting();
		screenVars.paymtDateDisp.clearFormatting();
		screenVars.clntId.clearFormatting();
		screenVars.amount.clearFormatting();
		screenVars.paymtMethod.clearFormatting();
		screenVars.bankAccount.clearFormatting();
		screenVars.paymtStatus.clearFormatting();
		screenVars.seqenum.clearFormatting();
		screenVars.select.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sjl53ScreenVars screenVars = (Sjl53ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.paymentNum.setClassString("");
		screenVars.paymtDateDisp.setClassString("");
		screenVars.clntId.setClassString("");
		screenVars.amount.setClassString("");
		screenVars.paymtMethod.setClassString("");
		screenVars.bankAccount.setClassString("");
		screenVars.paymtStatus.setClassString("");
		screenVars.seqenum.setClassString("");
		screenVars.select.setClassString("");
	}

/**
 * Clear all the variables in Sjl53screensfl
 */
	public static void clear(VarModel pv) {
		Sjl53ScreenVars screenVars = (Sjl53ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.paymentNum.clear();
		screenVars.paymtDateDisp.clear();
		screenVars.clntId.clear();
		screenVars.amount.clear();
		screenVars.paymtMethod.clear();
		screenVars.bankAccount.clear();
		screenVars.paymtStatus.clear();
		screenVars.seqenum.clear();
		screenVars.select.clear();
	}

}
