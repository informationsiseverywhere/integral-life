package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RegtpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:17
 * Class transformed from REGTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RegtpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 216+DD.payoutopt.length;
	public FixedLengthStringData regtrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData regtpfRecord = regtrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(regtrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(regtrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(regtrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(regtrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(regtrec);
	public PackedDecimalData seqnbr = DD.seqnbr.copy().isAPartOf(regtrec);
	public PackedDecimalData rgpynum = DD.rgpynum.copy().isAPartOf(regtrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(regtrec);
	public FixedLengthStringData sacstype = DD.sacstype.copy().isAPartOf(regtrec);
	public FixedLengthStringData glact = DD.glact.copy().isAPartOf(regtrec);
	public FixedLengthStringData debcred = DD.debcred.copy().isAPartOf(regtrec);
	public FixedLengthStringData destkey = DD.destkey.copy().isAPartOf(regtrec);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(regtrec);
	public FixedLengthStringData rgpymop = DD.rgpymop.copy().isAPartOf(regtrec);
	public FixedLengthStringData regpayfreq = DD.regpayfreq.copy().isAPartOf(regtrec);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(regtrec);
	public PackedDecimalData pymt = DD.pymt.copy().isAPartOf(regtrec);
	public PackedDecimalData totamnt = DD.totamnt.copy().isAPartOf(regtrec);
	public FixedLengthStringData payreason = DD.payreason.copy().isAPartOf(regtrec);
	public FixedLengthStringData claimevd = DD.claimevd.copy().isAPartOf(regtrec);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(regtrec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(regtrec);
	public PackedDecimalData crtdate = DD.crtdate.copy().isAPartOf(regtrec);
	public PackedDecimalData firstPaydate = DD.fpaydate.copy().isAPartOf(regtrec);
	public PackedDecimalData revdte = DD.revdte.copy().isAPartOf(regtrec);
	public PackedDecimalData finalPaydate = DD.epaydate.copy().isAPartOf(regtrec);
	public PackedDecimalData anvdate = DD.anvdate.copy().isAPartOf(regtrec);
	public FixedLengthStringData rgpytype = DD.rgpytype.copy().isAPartOf(regtrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(regtrec);
	public FixedLengthStringData paycoy = DD.paycoy.copy().isAPartOf(regtrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(regtrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(regtrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(regtrec);
	public PackedDecimalData prcnt = DD.prcnt.copy().isAPartOf(regtrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(regtrec);
	public FixedLengthStringData payoutopt = DD.payoutopt.copy().isAPartOf(regtrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public RegtpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for RegtpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public RegtpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for RegtpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public RegtpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for RegtpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public RegtpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("REGTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"SEQNBR, " +
							"RGPYNUM, " +
							"SACSCODE, " +
							"SACSTYPE, " +
							"GLACT, " +
							"DEBCRED, " +
							"DESTKEY, " +
							"PAYCLT, " +
							"RGPYMOP, " +
							"REGPAYFREQ, " +
							"CURRCD, " +
							"PYMT, " +
							"TOTAMNT, " +
							"PAYREASON, " +
							"CLAIMEVD, " +
							"BANKKEY, " +
							"BANKACCKEY, " +
							"CRTDATE, " +
							"FPAYDATE, " +
							"REVDTE, " +
							"EPAYDATE, " +
							"ANVDATE, " +
							"RGPYTYPE, " +
							"CRTABLE, " +
							"PAYCOY, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"PRCNT, " +
							"PLNSFX, " +
							"PAYOUTOPT, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     seqnbr,
                                     rgpynum,
                                     sacscode,
                                     sacstype,
                                     glact,
                                     debcred,
                                     destkey,
                                     payclt,
                                     rgpymop,
                                     regpayfreq,
                                     currcd,
                                     pymt,
                                     totamnt,
                                     payreason,
                                     claimevd,
                                     bankkey,
                                     bankacckey,
                                     crtdate,
                                     firstPaydate,
                                     revdte,
                                     finalPaydate,
                                     anvdate,
                                     rgpytype,
                                     crtable,
                                     paycoy,
                                     userProfile,
                                     jobName,
                                     datime,
                                     prcnt,
                                     planSuffix,
                                     payoutopt,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		seqnbr.clear();
  		rgpynum.clear();
  		sacscode.clear();
  		sacstype.clear();
  		glact.clear();
  		debcred.clear();
  		destkey.clear();
  		payclt.clear();
  		rgpymop.clear();
  		regpayfreq.clear();
  		currcd.clear();
  		pymt.clear();
  		totamnt.clear();
  		payreason.clear();
  		claimevd.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		crtdate.clear();
  		firstPaydate.clear();
  		revdte.clear();
  		finalPaydate.clear();
  		anvdate.clear();
  		rgpytype.clear();
  		crtable.clear();
  		paycoy.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		prcnt.clear();
  		planSuffix.clear();
  		payoutopt.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getRegtrec() {
  		return regtrec;
	}

	public FixedLengthStringData getRegtpfRecord() {
  		return regtpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setRegtrec(what);
	}

	public void setRegtrec(Object what) {
  		this.regtrec.set(what);
	}

	public void setRegtpfRecord(Object what) {
  		this.regtpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(regtrec.getLength());
		result.set(regtrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}