package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR681
 * @version 1.0 generated on 30/08/09 07:23
 * @author Quipoz
 */
public class Sr681ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(89);
	public FixedLengthStringData dataFields = new FixedLengthStringData(41).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 41);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 53);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(127);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(61).isAPartOf(subfileArea, 0);
	public FixedLengthStringData clntname = DD.clntname.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData relation = DD.relation.copy().isAPartOf(subfileFields,58);
	public FixedLengthStringData slt = DD.slt.copy().isAPartOf(subfileFields,60);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(16).isAPartOf(subfileArea, 61);
	public FixedLengthStringData clntnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData relationErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData sltErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 77);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] relationOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] sltOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 125);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr681screensflWritten = new LongData(0);
	public LongData Sr681screenctlWritten = new LongData(0);
	public LongData Sr681screenWritten = new LongData(0);
	public LongData Sr681protectWritten = new LongData(0);
	public GeneralTable sr681screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr681screensfl;
	}

	public Sr681ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(sltOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {clntname, slt, clntnum, relation};
		screenSflOutFields = new BaseData[][] {clntnameOut, sltOut, clntnumOut, relationOut};
		screenSflErrFields = new BaseData[] {clntnameErr, sltErr, clntnumErr, relationErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr681screen.class;
		screenSflRecord = Sr681screensfl.class;
		screenCtlRecord = Sr681screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr681protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr681screenctl.lrec.pageSubfile);
	}
}
