package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6319
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class S6319ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData efdate = DD.efdate.copyToZonedDecimal().isAPartOf(dataFields,11);
	public FixedLengthStringData submnuprog = DD.submnuprog.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData efdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData submnuprogErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] efdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] submnuprogOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData efdateDisp = new FixedLengthStringData(10);

	public LongData S6319screenWritten = new LongData(0);
	public LongData S6319protectWritten = new LongData(0);
	
	public static int[] screenPfInds = new int[] {4, 22, 17, 23, 18, 15, 6, 24, 16, 1, 2, 3, 21};
	
	public boolean hasSubfile() {
		return false;
	}


	public S6319ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"04", null, "-04", "03", null, null, null, null, null, null, null, null});
		fieldIndMap.put(efdateOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S6319screen.class;
		protectRecord = S6319protect.class;
	}
	
	public int getDataAreaSize()
	{
		return 88;
	}
	

	public int getDataFieldsSize()
	{
		return 24;
	}
	

	public int getErrorIndicatorSize()
	{
		return 16;
	}
	
	public int getOutputFieldSize()
	{
		return 48;
	}
	

	public BaseData[] getscreenFields()
	{
		return new BaseData[] {submnuprog, chdrsel, action, efdate};
	}
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {submnuprogOut, chdrselOut, actionOut, efdateOut};
	}
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {submnuprogErr, chdrselErr, actionErr, efdateErr};
	}
	

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {efdate};
	}
	

	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {efdateDisp};
	}
	

	public BaseData[] getscreenDateErrFields()
	{
		return  new BaseData[] {efdateErr};
	}
	

	public static int[] getScreenPfInds()
	{
		return screenPfInds;
	}
	
}
