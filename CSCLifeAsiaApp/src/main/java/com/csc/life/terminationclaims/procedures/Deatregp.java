/*
 * File: Deatregp.java
 * Date: 29 August 2009 22:45:45
 * Author: Quipoz Limited
 * 
 * Class transformed from DEATREGP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Clmallrec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*
*  This is a new Subroutine & forms part of the 9405 Annuities
* Development. It is the Generic Processing required on the
* registration of a death claim of a Joint Life, last survivor
* annuity component where the annuity is in payment.
*
* All regular payments associated solely with the dead life will
* be terminated. The existing record will be made VALIDFLAG '2'
* and a new record written with a VALIDFLAG '1' and a terminated
* status based on the entries on T6693.
*
* On joint life annuity contracts, the payment amount must be
* reduced in accordance with the Percentage Reduction on Death
* of the appropriate life from the REGPCLM record. Again, the
* existing REGPCLM records will be written with a VALIDFLAG of '2'
* and new REGPCLM records will be written with a VALIDFLAG of '1'
* and a status of 'In Review' from T6693, to ensure that they are
* reported to the Regular Payments batch job as requiring
* attention. The review date should be set to 'today'.
*
*****************************************************************
* </pre>
*/
public class Deatregp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaItemstat = new FixedLengthStringData(2).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaItemtabl = new FixedLengthStringData(4).isAPartOf(wsaaItem, 2);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaPercentage = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaInter1 = new PackedDecimalData(15, 4);
	private PackedDecimalData wsaaInter2 = new PackedDecimalData(10, 5);
	private FixedLengthStringData wsaaNominatedLife = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaStatusFound = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaRgpystat = new FixedLengthStringData(2);
		/* ERRORS */
	private static final String h144 = "H144";
		/* TABLES */
	private static final String t6693 = "T6693";
		/* FORMATS */
	private static final String regpclmrec = "REGPCLMREC";
	private static final String regprec = "REGPREC";
	private static final String annyrec = "ANNYREC";
	private static final String liferec = "LIFEREC";
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private RegpclmTableDAM regpclmIO = new RegpclmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T6693rec t6693rec = new T6693rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Clmallrec clmallrec = new Clmallrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Deatregp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		clmallrec.clmallRec = convertAndSetParam(clmallrec.clmallRec, parmArray, 0);
		try {
			control0090();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control0090()
	{
		/*STARTS*/
		initialise100();
		firstReading200();
		if (isNE(regpclmIO.getStatuz(), varcom.endp)) {
			while ( !(isEQ(regpclmIO.getStatuz(), varcom.endp))) {
				updateRegpclmRecords300();
			}
			
		}
		exitProgram();
	}

protected void initialise100()
	{
		/*START*/
		/* Initialisation of Working-Storage values.*/
		wsaaItemstat.set(SPACES);
		wsaaItemtabl.set(SPACES);
		wsaaNominatedLife.set(SPACES);
		wsaaStatusFound.set(SPACES);
		wsaaIndex.set(ZERO);
		wsaaInter1.set(ZERO);
		wsaaInter2.set(ZERO);
		wsaaPercentage.set(ZERO);
		varcom.vrcmUser.set(clmallrec.user); //ILIFE-2714 slakkala
		/*EXIT*/
	}

protected void firstReading200()
	{
		reading201();
	}

protected void reading201()
	{
		/* Initial moves in advance of First Read (BEGN) on the*/
		/* REGPCLM.*/
		regpclmIO.setChdrcoy(clmallrec.company);
		regpclmIO.setChdrnum(clmallrec.chdrnum);
		regpclmIO.setLife(clmallrec.life);
		regpclmIO.setCoverage(clmallrec.coverage);
		regpclmIO.setRider(clmallrec.rider);
		regpclmIO.setPlanSuffix(clmallrec.planSuffix);
		regpclmIO.setRgpynum(ZERO);
		regpclmIO.setFormat(regpclmrec);
		/* MOVE BEGNH                  TO REGPCLM-FUNCTION.             */
		regpclmIO.setFunction(varcom.begn);



		//performance improvement --  atiwari23 
		regpclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, regpclmIO);
		if (isNE(regpclmIO.getStatuz(), varcom.oK)
		&& isNE(regpclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(regpclmIO.getParams());
			syserrrec.statuz.set(regpclmIO.getStatuz());
			fatalError600();
		}
		if (isNE(regpclmIO.getChdrnum(), clmallrec.chdrnum)
		|| isNE(regpclmIO.getChdrcoy(), clmallrec.company)
		|| isNE(regpclmIO.getLife(), clmallrec.life)
		|| isNE(regpclmIO.getCoverage(), clmallrec.coverage)
		|| isNE(regpclmIO.getRider(), clmallrec.rider)) {
			regpclmIO.setStatuz(varcom.endp);
			return ;
		}
		/* Read the ANNY file appropriate for this component.*/
		annyIO.setChdrcoy(clmallrec.company);
		annyIO.setChdrnum(clmallrec.chdrnum);
		annyIO.setLife(clmallrec.life);
		annyIO.setCoverage(clmallrec.coverage);
		annyIO.setRider(clmallrec.rider);
		annyIO.setPlanSuffix(clmallrec.planSuffix);
		annyIO.setFunction(varcom.readr);
		annyIO.setFormat(annyrec);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		/* If nominated life not found, exit Program.*/
		if (isEQ(annyIO.getNomlife(), SPACES)) {
			return ;
		}
		/* Moves to LIFE before a call to the IO Module.*/
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(clmallrec.company);
		lifeIO.setChdrnum(clmallrec.chdrnum);
		lifeIO.setLife(clmallrec.life);
		lifeIO.setJlife(clmallrec.jlife);
		lifeIO.setCurrfrom(clmallrec.effdate);
		lifeIO.setFunction(varcom.readr);
		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)
		&& (isNE(lifeIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if (isNE(lifeIO.getChdrnum(), clmallrec.chdrnum)
		|| isNE(lifeIO.getChdrcoy(), clmallrec.company)
		|| isNE(lifeIO.getLife(), clmallrec.life)
		|| isNE(lifeIO.getJlife(), clmallrec.jlife)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		if (isEQ(annyIO.getNomlife(), lifeIO.getLifcnum())) {
			wsaaNominatedLife.set("Y");
		}
		else {
			wsaaNominatedLife.set("N");
		}
	}

protected void updateRegpclmRecords300()
	{
		update301();
		readNextRegpclm390();
	}

protected void update301()
	{
		/* The first read of T6693 is done with a Value of REGPCLM-CRTAB*/
		/* in WSAA-ITEMTABL; if this read of the Table is unsuccessful,*/
		/* the same read is done with a value of **** in WSAA=ITEMTABL.*/
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(clmallrec.company);
		itdmIO.setItemtabl(t6693);
		wsaaItemstat.set(regpclmIO.getRgpystat());
		wsaaItemtabl.set(regpclmIO.getCrtable());
		itdmIO.setItemitem(wsaaItem);
		itdmIO.setItmfrm(clmallrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), clmallrec.company)
		|| isNE(itdmIO.getItemtabl(), t6693)
		|| isNE(itdmIO.getItemitem(), wsaaItem)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(clmallrec.company);
			itdmIO.setItemtabl(t6693);
			wsaaItemstat.set(regpclmIO.getRgpystat());
			wsaaItemtabl.set("****");
			itdmIO.setItemitem(wsaaItem);
			itdmIO.setItmfrm(clmallrec.effdate);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
			if (isNE(itdmIO.getItemcoy(), clmallrec.company)
			|| isNE(itdmIO.getItemtabl(), t6693)
			|| isNE(itdmIO.getItemitem(), wsaaItem)
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(h144);
				fatalError600();
			}
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		wsaaStatusFound.set("N");
		/*Initialisation of WSAA-RGPYSTAT value.*/
		wsaaRgpystat.set(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaStatusFound, "Y")); wsaaIndex.add(1)){
			findStatus500();
		}
		/* Error if the Transaction Code not on T6693:*/
		if (isEQ(wsaaStatusFound, "N")) {
			/*    MOVE REWRT               TO REGPCLM-FUNCTION              */
			regpclmIO.setFunction(varcom.writd);
			regpclmIO.setFormat(regpclmrec);
			SmartFileCode.execute(appVars, regpclmIO);
			if (isNE(regpclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpclmIO.getParams());
				fatalError600();
			}
			return ;
		}
		/* Update the REGPCLM record with valid flag of '2'*/
		regpclmIO.setValidflag("2");
		/* MOVE REWRT                  TO REGPCLM-FUNCTION.             */
		regpclmIO.setFunction(varcom.writd);
		regpclmIO.setFormat(regpclmrec);
		SmartFileCode.execute(appVars, regpclmIO);
		if (isNE(regpclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpclmIO.getParams());
			fatalError600();
		}
		recalcRegpclm400();
		wsaaTranno.set(clmallrec.tranno);
		/* At this point, it is necessary to introduce a different*/
		/* Logical, REGP, in order to facilitate the use of a loop*/
		/* within a loop. It is therefore necessary to move all*/
		/* values from REGPCLM to REGP, and to change the Validflag*/
		/* back to '1', before calling REGPIO, and thus Inserting*/
		/* the new Validflag '1' Record.*/
		regpIO.setChdrcoy(regpclmIO.getChdrcoy());
		regpIO.setChdrnum(regpclmIO.getChdrnum());
		regpIO.setLife(regpclmIO.getLife());
		regpIO.setCoverage(regpclmIO.getCoverage());
		regpIO.setRider(regpclmIO.getRider());
		regpIO.setRgpynum(regpclmIO.getRgpynum());
		regpIO.setPlanSuffix(regpclmIO.getPlanSuffix());
		regpIO.setValidflag(regpclmIO.getValidflag());
		regpIO.setSacscode(regpclmIO.getSacscode());
		regpIO.setSacstype(regpclmIO.getSacstype());
		regpIO.setGlact(regpclmIO.getGlact());
		regpIO.setDebcred(regpclmIO.getDebcred());
		regpIO.setDestkey(regpclmIO.getDestkey());
		regpIO.setPaycoy(regpclmIO.getPaycoy());
		regpIO.setPayclt(regpclmIO.getPayclt());
		regpIO.setRgpymop(regpclmIO.getRgpymop());
		regpIO.setRegpayfreq(regpclmIO.getRegpayfreq());
		regpIO.setCurrcd(regpclmIO.getCurrcd());
		regpIO.setPymt(regpclmIO.getPymt());
		compute(wsaaInter2, 6).setRounded(div((sub(100, wsaaPercentage)), 100));
		setPrecision(regpIO.getPrcnt(), 6);
		regpIO.setPrcnt(mult(regpclmIO.getPrcnt(), wsaaInter2), true);
		regpIO.setTotamnt(regpclmIO.getTotamnt());
		regpIO.setPayreason(regpclmIO.getPayreason());
		regpIO.setClaimevd(regpclmIO.getClaimevd());
		regpIO.setBankkey(regpclmIO.getBankkey());
		regpIO.setBankacckey(regpclmIO.getBankacckey());
		regpIO.setCrtdate(regpclmIO.getCrtdate());
		regpIO.setAprvdate(regpclmIO.getAprvdate());
		regpIO.setFirstPaydate(regpclmIO.getFirstPaydate());
		regpIO.setNextPaydate(regpclmIO.getNextPaydate());
		regpIO.setRevdte(regpclmIO.getRevdte());
		regpIO.setLastPaydate(regpclmIO.getLastPaydate());
		regpIO.setFinalPaydate(regpclmIO.getFinalPaydate());
		regpIO.setAnvdate(regpclmIO.getAnvdate());
		regpIO.setCancelDate(regpclmIO.getCancelDate());
		regpIO.setRgpytype(regpclmIO.getRgpytype());
		regpIO.setCrtable(regpclmIO.getCrtable());
		regpIO.setCertdate(regpclmIO.getCertdate());
		regpIO.setRecvdDate(varcom.vrcmMaxDate);
		regpIO.setIncurdt(varcom.vrcmMaxDate);
		regpIO.setTermid(regpclmIO.getTermid());
		regpIO.setTranno(wsaaTranno);
		regpIO.setRgpystat(wsaaRgpystat);
		regpIO.setTransactionDate(varcom.vrcmDate);
		regpIO.setTransactionTime(varcom.vrcmTime);
		regpIO.setUser(varcom.vrcmUser);
		regpIO.setValidflag("1");
		regpIO.setFormat(regprec);
		regpIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	**** Perform subsequent read on REGPCLM.
	* </pre>
	*/
protected void readNextRegpclm390()
	{//performance improvement --  atiwari23 
	regpclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	regpclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		regpclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, regpclmIO);
		if (isNE(regpclmIO.getStatuz(), varcom.oK)
		&& isNE(regpclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(regpclmIO.getParams());
			syserrrec.statuz.set(regpclmIO.getStatuz());
			fatalError600();
		}
		if (isNE(regpclmIO.getChdrnum(), clmallrec.chdrnum)
		|| isNE(regpclmIO.getChdrcoy(), clmallrec.company)
		|| isNE(regpclmIO.getLife(), clmallrec.life)
		|| isNE(regpclmIO.getCoverage(), clmallrec.coverage)
		|| isNE(regpclmIO.getRider(), clmallrec.rider)
		|| isNE(regpclmIO.getPlanSuffix(), clmallrec.planSuffix)) {
			regpclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void recalcRegpclm400()
	{
		/*START*/
		/* The Percentage the payment should be recalculated to*/
		/* is determined by the life that has died. If the Nominated*/
		/* life has died (as captured at New Business) then that*/
		/* percentage is used. Otherwise the Percentage Death of the*/
		/* other life is used.*/
		if (isEQ(wsaaNominatedLife, "Y")) {
			wsaaPercentage.set(annyIO.getDthpercn());
		}
		else {
			wsaaPercentage.set(annyIO.getDthperco());
		}
		compute(wsaaInter1, 5).setRounded(mult((div(regpclmIO.getPymt(), 100)), wsaaPercentage));
		zrdecplrec.amountIn.set(wsaaInter1);
		a000CallRounding();
		wsaaInter1.set(zrdecplrec.amountOut);
		setPrecision(regpclmIO.getPymt(), 5);
		regpclmIO.setPymt(sub(regpclmIO.getPymt(), wsaaInter1), true);
		/*EXIT*/
	}

protected void findStatus500()
	{
		/*START*/
		if (isEQ(t6693rec.trcode[wsaaIndex.toInt()], clmallrec.batctrcde)) {
			wsaaRgpystat.set(t6693rec.rgpystat[wsaaIndex.toInt()]);
			wsaaStatusFound.set("Y");
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fatalError601();
				case errorProg610: 
					errorProg610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		clmallrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(clmallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(regpclmIO.getCurrcd());
		zrdecplrec.batctrcde.set(clmallrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
}
