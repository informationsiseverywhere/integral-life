package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MathclmTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:25
 * Class transformed from MATHCLM.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MathclmTableDAM extends MathpfTableDAM {

	public MathclmTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("MATHCLM");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "TRANNO, " +
		            "PLNSFX, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "EFFDATE, " +
		            "CURRCD, " +
		            "POLICYLOAN, " +
		            "TDBTAMT, " +
		            "OTHERADJST, " +
		            "REASONCD, " +
		            "RESNDESC, " +
		            "CNTTYPE, " +
		            "ESTIMTOTAL, " +
		            "CLAMAMT, " +
		            "ZRCSHAMT, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               tranno,
                               planSuffix,
                               life,
                               jlife,
                               effdate,
                               currcd,
                               policyloan,
                               tdbtamt,
                               otheradjst,
                               reasoncd,
                               resndesc,
                               cnttype,
                               estimateTotalValue,
                               clamamt,
                               zrcshamt,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(49);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(tranno.toInternal());
	nonKeyFiller40.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(198);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getLife().toInternal()
					+ getJlife().toInternal()
					+ getEffdate().toInternal()
					+ getCurrcd().toInternal()
					+ getPolicyloan().toInternal()
					+ getTdbtamt().toInternal()
					+ getOtheradjst().toInternal()
					+ getReasoncd().toInternal()
					+ getResndesc().toInternal()
					+ getCnttype().toInternal()
					+ getEstimateTotalValue().toInternal()
					+ getClamamt().toInternal()
					+ getZrcshamt().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, currcd);
			what = ExternalData.chop(what, policyloan);
			what = ExternalData.chop(what, tdbtamt);
			what = ExternalData.chop(what, otheradjst);
			what = ExternalData.chop(what, reasoncd);
			what = ExternalData.chop(what, resndesc);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, estimateTotalValue);
			what = ExternalData.chop(what, clamamt);
			what = ExternalData.chop(what, zrcshamt);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getCurrcd() {
		return currcd;
	}
	public void setCurrcd(Object what) {
		currcd.set(what);
	}	
	public PackedDecimalData getPolicyloan() {
		return policyloan;
	}
	public void setPolicyloan(Object what) {
		setPolicyloan(what, false);
	}
	public void setPolicyloan(Object what, boolean rounded) {
		if (rounded)
			policyloan.setRounded(what);
		else
			policyloan.set(what);
	}	
	public PackedDecimalData getTdbtamt() {
		return tdbtamt;
	}
	public void setTdbtamt(Object what) {
		setTdbtamt(what, false);
	}
	public void setTdbtamt(Object what, boolean rounded) {
		if (rounded)
			tdbtamt.setRounded(what);
		else
			tdbtamt.set(what);
	}	
	public PackedDecimalData getOtheradjst() {
		return otheradjst;
	}
	public void setOtheradjst(Object what) {
		setOtheradjst(what, false);
	}
	public void setOtheradjst(Object what, boolean rounded) {
		if (rounded)
			otheradjst.setRounded(what);
		else
			otheradjst.set(what);
	}	
	public FixedLengthStringData getReasoncd() {
		return reasoncd;
	}
	public void setReasoncd(Object what) {
		reasoncd.set(what);
	}	
	public FixedLengthStringData getResndesc() {
		return resndesc;
	}
	public void setResndesc(Object what) {
		resndesc.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public PackedDecimalData getEstimateTotalValue() {
		return estimateTotalValue;
	}
	public void setEstimateTotalValue(Object what) {
		setEstimateTotalValue(what, false);
	}
	public void setEstimateTotalValue(Object what, boolean rounded) {
		if (rounded)
			estimateTotalValue.setRounded(what);
		else
			estimateTotalValue.set(what);
	}	
	public PackedDecimalData getClamamt() {
		return clamamt;
	}
	public void setClamamt(Object what) {
		setClamamt(what, false);
	}
	public void setClamamt(Object what, boolean rounded) {
		if (rounded)
			clamamt.setRounded(what);
		else
			clamamt.set(what);
	}	
	public PackedDecimalData getZrcshamt() {
		return zrcshamt;
	}
	public void setZrcshamt(Object what) {
		setZrcshamt(what, false);
	}
	public void setZrcshamt(Object what, boolean rounded) {
		if (rounded)
			zrcshamt.setRounded(what);
		else
			zrcshamt.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		life.clear();
		jlife.clear();
		effdate.clear();
		currcd.clear();
		policyloan.clear();
		tdbtamt.clear();
		otheradjst.clear();
		reasoncd.clear();
		resndesc.clear();
		cnttype.clear();
		estimateTotalValue.clear();
		clamamt.clear();
		zrcshamt.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}