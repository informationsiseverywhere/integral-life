package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;

public class Regrpf {

	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int rgpynum;
	private BigDecimal pymt;
	private String currcd;
	private BigDecimal prcnt;
	private String rgpytype;
	private String payreason;
	private String rgpystat;
	private int revdte;
	private int firstPaydate;
	private String progname;
	private String excode;
	private String exreport;
	private String crtable;
	private int tranno;
	private String userProfile;
	private String jobName;
	private String datime;
	private int lastPaydate;
	private long uniqueNumber;

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public int getRgpynum() {
		return rgpynum;
	}

	public void setRgpynum(int rgpynum) {
		this.rgpynum = rgpynum;
	}

	public BigDecimal getPymt() {
		return pymt;
	}

	public void setPymt(BigDecimal pymt) {
		this.pymt = pymt;
	}

	public String getCurrcd() {
		return currcd;
	}

	public void setCurrcd(String currcd) {
		this.currcd = currcd;
	}

	public BigDecimal getPrcnt() {
		return prcnt;
	}

	public void setPrcnt(BigDecimal prcnt) {
		this.prcnt = prcnt;
	}

	public String getRgpytype() {
		return rgpytype;
	}

	public void setRgpytype(String rgpytype) {
		this.rgpytype = rgpytype;
	}

	public String getPayreason() {
		return payreason;
	}

	public void setPayreason(String payreason) {
		this.payreason = payreason;
	}

	public String getRgpystat() {
		return rgpystat;
	}

	public void setRgpystat(String rgpystat) {
		this.rgpystat = rgpystat;
	}

	public int getRevdte() {
		return revdte;
	}

	public void setRevdte(int revdte) {
		this.revdte = revdte;
	}

	public int getFirstPaydate() {
		return firstPaydate;
	}

	public void setFirstPaydate(int firstPaydate) {
		this.firstPaydate = firstPaydate;
	}

	public String getProgname() {
		return progname;
	}

	public void setProgname(String progname) {
		this.progname = progname;
	}

	public String getExcode() {
		return excode;
	}

	public void setExcode(String excode) {
		this.excode = excode;
	}

	public String getExreport() {
		return exreport;
	}

	public void setExreport(String exreport) {
		this.exreport = exreport;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}

	public int getLastPaydate() {
		return lastPaydate;
	}

	public void setLastPaydate(int lastPaydate) {
		this.lastPaydate = lastPaydate;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

}
