package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;
/**
 * Screen variables for Sa508
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class Sa512ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(4603); //ILIFE-2472 
	
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(3915).isAPartOf(dataArea, 0); //ILIFE-2472
	
	public FixedLengthStringData addrdescs = new FixedLengthStringData(50).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] addrdesc = FLSArrayPartOfStructure(5, 10, addrdescs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(addrdescs, 0, FILLER_REDEFINE);
	public FixedLengthStringData addrdesc01 = DD.addrdesc.copy().isAPartOf(filler,0);
	public FixedLengthStringData addrdesc02 = DD.addrdesc.copy().isAPartOf(filler,10);
	public FixedLengthStringData addrdesc03 = DD.addrdesc.copy().isAPartOf(filler,20);
	public FixedLengthStringData addrdesc04 = DD.addrdesc.copy().isAPartOf(filler,30);
	public FixedLengthStringData addrdesc05 = DD.addrdesc.copy().isAPartOf(filler,40);
	
	//The first part
	public FixedLengthStringData notifinum = DD.aacct.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,72);
	public FixedLengthStringData lidtype = DD.idtype.copy().isAPartOf(dataFields,119);
	public FixedLengthStringData lsecuityno = DD.secuityno.copy().isAPartOf(dataFields,121);
	public FixedLengthStringData lcltsex = DD.cltsex.copy().isAPartOf(dataFields,145);
	
	//The second part
	public FixedLengthStringData clntsel = DD.clttwo.copy().isAPartOf(dataFields,146);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,154);
	public FixedLengthStringData cltreln = DD.relationwithowner.copy().isAPartOf(dataFields,201);
	
	public FixedLengthStringData cidtype = DD.idtype.copy().isAPartOf(dataFields,205);
	public FixedLengthStringData csecuityno = DD.secuityno.copy().isAPartOf(dataFields,207);
	public FixedLengthStringData ccltsex = DD.cltsex.copy().isAPartOf(dataFields,231);
	public FixedLengthStringData cltpcode = DD.cltpcode.copy().isAPartOf(dataFields,232);
	public FixedLengthStringData rinternet = DD.rinternet.copy().isAPartOf(dataFields,242);
	public FixedLengthStringData cltphoneidd=DD.cltphoneidd.copy().isAPartOf(dataFields,292);
	
	//The third part
	public ZonedDecimalData notifiDate = DD.srdate.copyToZonedDecimal().isAPartOf(dataFields,295);
	public ZonedDecimalData incurdt = DD.incurdt.copyToZonedDecimal().isAPartOf(dataFields,303);
	public FixedLengthStringData location = DD.location.copy().isAPartOf(dataFields,311);
	//inctype
	public FixedLengthStringData inctype = DD.inctype.copy().isAPartOf(dataFields,511);
	public ZonedDecimalData cltdodx = DD.cltdodx.copyToZonedDecimal().isAPartOf(dataFields,513);
	public FixedLengthStringData causedeath = DD.rinternet.copy().isAPartOf(dataFields,521);
	
	//Regular claim reason
	public FixedLengthStringData rgpytype = DD.rgpytype.copy().isAPartOf(dataFields,571);
	//clamamt
	public ZonedDecimalData clamamt = DD.clamamt.copyToZonedDecimal().isAPartOf(dataFields,573);
	//doctor
	public FixedLengthStringData zdoctor = DD.zdoctor.copy().isAPartOf(dataFields,590);
	
	//Medical provider
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(dataFields,598);
	
	//Hospital Level
	public FixedLengthStringData hospitalLevel = DD.hospitalLevel.copy().isAPartOf(dataFields,608);
	//Diagnosis
	public FixedLengthStringData diagcde = DD.diagcde.copy().isAPartOf(dataFields,610);
	public ZonedDecimalData admiDate = DD.admitDate.copyToZonedDecimal().isAPartOf(dataFields,615);
	public ZonedDecimalData dischargeDate = DD.dischargeDate.copyToZonedDecimal().isAPartOf(dataFields,623);
	//accdesc
	public FixedLengthStringData accdesc = DD.accdesc1.copy().isAPartOf(dataFields,631);
	public FixedLengthStringData cltaddrs = new FixedLengthStringData(DD.cltaddr.length*5).isAPartOf(dataFields, 3631);
	public FixedLengthStringData[] cltaddr = FLSArrayPartOfStructure(5, DD.cltaddr.length, cltaddrs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(cltaddrs.length()).isAPartOf(cltaddrs, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltaddr01 = DD.cltaddr.copy().isAPartOf(filler1,0);
	public FixedLengthStringData cltaddr02 = DD.cltaddr.copy().isAPartOf(filler1,DD.cltaddr.length*1);
	public FixedLengthStringData cltaddr03 = DD.cltaddr.copy().isAPartOf(filler1,DD.cltaddr.length*2);
	public FixedLengthStringData cltaddr04 = DD.cltaddr.copy().isAPartOf(filler1,DD.cltaddr.length*3);
	public FixedLengthStringData cltaddr05 = DD.cltaddr.copy().isAPartOf(filler1,DD.cltaddr.length*4);
	
	public FixedLengthStringData cltreldsc = DD.cltreldsc.copy().isAPartOf(dataFields,3881);
	
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,3911);
	
	//CML001
	public FixedLengthStringData invresults = DD.invresults.copy().isAPartOf(dataFields,3912);
	public FixedLengthStringData notifnotes = DD.notifnotes.copy().isAPartOf(dataFields,3913);
	public FixedLengthStringData notifhis = DD.notifhis.copy().isAPartOf(dataFields,3914);
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(172).isAPartOf(dataArea, 3915);
	public FixedLengthStringData addrdescsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] addrdescErr = FLSArrayPartOfStructure(5, 4, addrdescsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(addrdescsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData addrdesc01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData addrdesc02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData addrdesc03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData addrdesc04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData addrdesc05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	
	public FixedLengthStringData notifinumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lidtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lsecuitynoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lcltsexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData cltrelnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData cidtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData csecuitynoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ccltsexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData cltpcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData rinternetErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData cltphoneiddErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData notifiDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData incurdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData locationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData inctypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData cltdodxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData causedeathErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData rgpytypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData clamamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData zdoctorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData zmedprvErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData hospitalLevelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData diagcdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData admiDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData dischargeDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	
	public FixedLengthStringData cltaddrsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData[] cltaddrErr = FLSArrayPartOfStructure(5, 4, cltaddrsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(20).isAPartOf(cltaddrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData cltaddr01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData cltaddr02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData cltaddr03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData cltaddr04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData cltaddr05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	
	public FixedLengthStringData cltreldscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	
	public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	//CML001
	public FixedLengthStringData invresultErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData notifnotesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(516).isAPartOf(dataArea, 4087);
	public FixedLengthStringData addrdescsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] addrdescOut = FLSArrayPartOfStructure(5, 12, addrdescsOut, 0);
	public FixedLengthStringData[][] addrdescO = FLSDArrayPartOfArrayStructure(12, 1, addrdescOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(60).isAPartOf(addrdescsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] addrdesc01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] addrdesc02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] addrdesc03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] addrdesc04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] addrdesc05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	
	public FixedLengthStringData[] notifinumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lidtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lsecuitynoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lcltsexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] cltrelnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] cidtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] csecuitynoOut = FLSArrayPartOfStructure(12, 1, outputIndicators,180 );
	public FixedLengthStringData[] ccltsexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] cltpcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	
	public FixedLengthStringData[] rinternetOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] cltphoneiddOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	
	public FixedLengthStringData[] notifiDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] incurdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] locationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] inctypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] cltdodxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] causedeathOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] rgpytypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] clamamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] zdoctorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] zmedprvOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] hospitalLevelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] diagcdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators,372 );
	public FixedLengthStringData[] admiDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators,384 );
	public FixedLengthStringData[] dischargeDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	
	public FixedLengthStringData cltaddrsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 408);
	public FixedLengthStringData[] cltaddrOut = FLSArrayPartOfStructure(5, 12, cltaddrsOut, 0);
	public FixedLengthStringData[][] cltaddrO = FLSDArrayPartOfArrayStructure(12, 1, cltaddrOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(60).isAPartOf(cltaddrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] cltaddr01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] cltaddr02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] cltaddr03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] cltaddr04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] cltaddr05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] cltreldscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	
	public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	//CML001
	public FixedLengthStringData[] invresultOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] notifnotesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	
	
	
	
	
	
		
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	//date disp
	public FixedLengthStringData incurdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData cltdodxDisp = new FixedLengthStringData(10);
	public FixedLengthStringData notifiDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData admiDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dischargeDateDisp = new FixedLengthStringData(10);
	


	public LongData Sa512screenWritten = new LongData(0);
	public LongData Sa512protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sa512ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		fieldIndMap.put(cltdodxOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(cltrelnOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(cltaddr01Out,new String[] {"08","42","-08","46","95", null, null, null, null, null, null, null});//IFSU-812
		fieldIndMap.put(cltaddr02Out,new String[] {"08","42","-08","46",null, null, null, null, null, null, null, null});//IFSU-812
		fieldIndMap.put(cltaddr03Out,new String[] {"12","71","-12","46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltaddr04Out,new String[] {"14","71","-14","46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltaddr05Out,new String[] {"24","71","-24","46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(addrdesc01Out,new String[] {null, null, null, "46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(addrdesc02Out,new String[] {null, null, null, "46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(addrdesc03Out,new String[] {null, null, null, "46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(addrdesc04Out,new String[] {null, null, null, "46",null, null, null, null, null, null, null, null});
		fieldIndMap.put(addrdesc05Out,new String[] {null, null, null, "46",null, null, null, null, null, null, null, null});
		
		
		fieldIndMap.put(notifinumOut,new String[] {"96","40","-96","44", "90", null, null, null, null, null, null, null});	
		fieldIndMap.put(lidtypeOut,new String[] {"97","41","-97","45", "91", null, null, null, null, null, null, null});		
		fieldIndMap.put(lsecuitynoOut,new String[] {"98","42","-97","45", "92", null, null, null, null, null, null, null});		
		fieldIndMap.put(lcltsexOut,new String[] {"99","43","-98","46", "93", null, null, null, null, null, null, null});		
//		fieldIndMap.put(clntselOut,new String[] {"01","25","-01",null, null, null, null, null, null, null, null, null});	
		fieldIndMap.put(cltnameOut,new String[] {"101","45","-100","48", "95", null, null, null, null, null, null, null});		
		
		fieldIndMap.put(cidtypeOut,new String[] {"103","47","-102","50", "97", null, null, null, null, null, null, null});		
		fieldIndMap.put(csecuitynoOut,new String[] {"104","48","-103","51", "98", null, null, null, null, null, null, null});		
		fieldIndMap.put(ccltsexOut,new String[] {"105","49","-104","52", "99", null, null, null, null, null, null, null});		
		fieldIndMap.put(cltpcodeOut,new String[] {"106","50","-105","53", "100", null, null, null, null, null, null, null});		
		fieldIndMap.put(rinternetOut,new String[] {"107","51","-106","54", "101", null, null, null, null, null, null, null});		
		fieldIndMap.put(cltphoneiddOut,new String[] {"108","52","-107","55", "102", null, null, null, null, null, null, null});		
		fieldIndMap.put(notifiDateOut,new String[] {"109","53","-108","56", "103", null, null, null, null, null, null, null});		
		fieldIndMap.put(incurdtOut,new String[] {"110","54","-109","57", "104", null, null, null, null, null, null, null});		
		fieldIndMap.put(locationOut,new String[] {"111","55","-110","58", "105", null, null, null, null, null, null, null});		
		fieldIndMap.put(inctypeOut,new String[] {"112","56","-111","59", "106", null, null, null, null, null, null, null});			
		fieldIndMap.put(causedeathOut,new String[] {"114","58","-113","61", "108", null, null, null, null, null, null, null});		
		fieldIndMap.put(rgpytypeOut,new String[] {"115","59","-114","62", "109", null, null, null, null, null, null, null});		
		fieldIndMap.put(clamamtOut,new String[] {"116","60","-115","63", "110", null, null, null, null, null, null, null});		
		fieldIndMap.put(zdoctorOut,new String[] {"117","61","-116","64", "111", null, null, null, null, null, null, null});		
		fieldIndMap.put(zmedprvOut,new String[] {"118","62","-117","65", "112", null, null, null, null, null, null, null});		
		fieldIndMap.put(hospitalLevelOut,new String[] {"119","63","-118","66", "113", null, null, null, null, null, null, null});		
		fieldIndMap.put(diagcdeOut,new String[] {"120","64","-119","67", "114", null, null, null, null, null, null, null});		
		fieldIndMap.put(dischargeDateOut,new String[] {"121","65","-120","68", "115", null, null, null, null, null, null, null});
		
		fieldIndMap.put(admiDateOut,new String[] {"122","65","-120","68", "115", null, null, null, null, null, null, null});

		fieldIndMap.put(fupflgOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		//CML001
		fieldIndMap.put(invresultOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(notifnotesOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		
		
		

		screenFields = new BaseData[] {addrdesc01, addrdesc02, addrdesc03, addrdesc04, addrdesc05,notifinum,lifcnum,lifename,lidtype,lsecuityno,lcltsex,clntsel,cltname,cltreln,cidtype,csecuityno,ccltsex,cltpcode,rinternet,cltphoneidd,notifiDate,incurdt,location,inctype,cltdodx,causedeath,rgpytype,clamamt,zdoctor,zmedprv,hospitalLevel,diagcde,admiDate,dischargeDate,cltaddr01,cltaddr02,cltaddr03,cltaddr04, cltaddr05,cltreldsc,fupflg,invresults,notifnotes };
		screenOutFields = new BaseData[][] {addrdesc01Out, addrdesc02Out, addrdesc03Out, addrdesc04Out, addrdesc05Out,notifinumOut,lifcnumOut,lifenameOut,lidtypeOut,lsecuitynoOut,lcltsexOut,clntselOut,cltnameOut,cltrelnOut,cidtypeOut,csecuitynoOut,ccltsexOut,cltpcodeOut,rinternetOut,cltphoneiddOut,notifiDateOut,incurdtOut,locationOut,inctypeOut,cltdodxOut,causedeathOut,rgpytypeOut,clamamtOut,zdoctorOut,zmedprvOut,hospitalLevelOut,diagcdeOut,admiDateOut,dischargeDateOut,cltaddr01Out,cltaddr02Out,cltaddr03Out,cltaddr04Out, cltaddr05Out,cltreldscOut,fupflgOut,invresultOut,notifnotesOut };
		screenErrFields = new BaseData[] {addrdesc01Err, addrdesc02Err, addrdesc03Err, addrdesc04Err, addrdesc05Err,notifinumErr,lifcnumErr,lifenameErr,lidtypeErr,lsecuitynoErr,lcltsexErr,clntselErr,cltnameErr,cltrelnErr,cidtypeErr,csecuitynoErr,ccltsexErr,cltpcodeErr,rinternetErr,cltphoneiddErr,notifiDateErr,incurdtErr,locationErr,inctypeErr,cltdodxErr,causedeathErr,rgpytypeErr,clamamtErr,zdoctorErr,zmedprvErr,hospitalLevelErr,diagcdeErr,admiDateErr,dischargeDateErr,cltaddr01Err,cltaddr02Err,cltaddr03Err,cltaddr04Err, cltaddr05Err,cltreldscErr,fupflgErr,invresultErr,notifnotesErr};
		
		screenDateFields = new BaseData[] {incurdt,cltdodx,notifiDate,admiDate,dischargeDate};
		screenDateErrFields = new BaseData[] {incurdtErr,cltdodxErr,notifiDateErr,admiDateErr,dischargeDateErr};
		screenDateDispFields = new BaseData[] {incurdtDisp,cltdodxDisp,notifiDateDisp,admiDateDisp,dischargeDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sa512screen.class;
		protectRecord = Sa512protect.class;
	}
}