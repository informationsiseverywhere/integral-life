package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:27
 * Description:
 * Copybook name: HCLABLTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hclabltkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hclabltFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hclabltKey = new FixedLengthStringData(256).isAPartOf(hclabltFileKey, 0, REDEFINE);
  	public FixedLengthStringData hclabltChdrcoy = new FixedLengthStringData(1).isAPartOf(hclabltKey, 0);
  	public FixedLengthStringData hclabltChdrnum = new FixedLengthStringData(8).isAPartOf(hclabltKey, 1);
  	public FixedLengthStringData hclabltLife = new FixedLengthStringData(2).isAPartOf(hclabltKey, 9);
  	public FixedLengthStringData hclabltCoverage = new FixedLengthStringData(2).isAPartOf(hclabltKey, 11);
  	public FixedLengthStringData hclabltRider = new FixedLengthStringData(2).isAPartOf(hclabltKey, 13);
  	public FixedLengthStringData hclabltHosben = new FixedLengthStringData(5).isAPartOf(hclabltKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(236).isAPartOf(hclabltKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hclabltFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hclabltFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}