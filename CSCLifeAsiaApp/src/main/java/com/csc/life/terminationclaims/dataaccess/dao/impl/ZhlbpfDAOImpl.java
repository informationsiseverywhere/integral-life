package com.csc.life.terminationclaims.dataaccess.dao.impl;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.ZhlbpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Zhlbpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public  class ZhlbpfDAOImpl extends  BaseDAOImpl<Zhlbpf> implements ZhlbpfDAO{
	
		private static final Logger LOGGER = LoggerFactory.getLogger(ZhlbpfDAOImpl.class);
		PreparedStatement stmt ;  
		public boolean insertIntoZhlbpf(Zhlbpf zhlbpf){
	           
			stmt = null;
	
			java.util.Date date = new java.util.Date();
			Timestamp currentTimestamp = new Timestamp(date.getTime());
			
			try{
			String queryString = "INSERT INTO Zhlbpf " + "  (CHDRCOY,CHDRNUM,BNYCLT,BNYPC,VALIDFLAG,TRANNO,TRDT,TRTM,USR,ACTVALUE,ZCLMADJST,ZHLDCLMV,ZHLDCLMA,USRPRF,JOBNM ,DATIME) "+
   						 " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	         
	          
			stmt = getConnection().prepareStatement(queryString); // IJTI-714
	//stmt.executeUpdate(queryString);

			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentTimestamp);
			Timestamp dattime = Timestamp.valueOf(timeStamp);
			
			
			 stmt.setString(1, zhlbpf.getChdrcoy());              
             stmt.setString(2, zhlbpf.getChdrnum());
             stmt.setString(3, zhlbpf.getBnyclt());
             stmt.setBigDecimal(4, zhlbpf.getBnypc());
             stmt.setString(5, zhlbpf.getValidflag());
             stmt.setInt(6, zhlbpf.getTranno());
             stmt.setInt(7, zhlbpf.getTrdt());
             stmt.setInt(8, zhlbpf.getTrtm());
             stmt.setInt(9, zhlbpf.getUsr());
             stmt.setBigDecimal(10, zhlbpf.getActvalue());
             stmt.setBigDecimal(11, zhlbpf.getZclmadjst());
             stmt.setBigDecimal(12, zhlbpf.getZhldclmv());
             stmt.setBigDecimal(13, zhlbpf.getZhldclma());
             stmt.setString(14, getUsrprf());
             stmt.setString(15, getJobnm());
            // stmt.setTimestamp(16,  new Timestamp(System.currentTimeMillis()));
             stmt.setTimestamp(16,dattime);
	                
             int recordsRows= stmt.executeUpdate(); 
             stmt.close();
             if(recordsRows != 0)
             	{
            	 	return true;
             	}
             	else
             	{
             		return false;
             	}
             
            
			}
		
			catch (SQLException e) {
	           LOGGER.error("read()", e);//IJTI-1561
	           throw new SQLRuntimeException(e);
			}	
			finally {
				close(stmt, null);
			}
	                
		}

public List<Zhlbpf> readZhlbpf(Zhlbpf zhlbpf){
	
	
			List<Zhlbpf> zhlbpfList = null;
			String query = "select bnypc,tranno,trdt,trtm,usr,actvalue,zclmadjst,zhldclmv,zhldclma,usrprf,jobnm ,datime"
							+ " from zhlbpf where (chdrcoy = ? and chdrnum = ? and bnyclt = ? and validflag = ?)";
   
			ResultSet rs = null;
			Zhlbpf zhlbpfData = null;
			stmt = null;
			try {

				zhlbpfList = new ArrayList<>();
				stmt = getConnection().prepareStatement(query); // IJTI-714
				stmt.setString(1, zhlbpf.getChdrcoy());
				stmt.setString(2, zhlbpf.getChdrnum());
				stmt.setString(3, zhlbpf.getBnyclt());
				stmt.setString(4, zhlbpf.getValidflag());
          
          

				rs = stmt.executeQuery();
				while (rs.next()) {
					zhlbpfData = new Zhlbpf();
					if (rs.getString(1) != null) {
						zhlbpfData.setChdrcoy(zhlbpf.getChdrcoy());
						zhlbpfData.setChdrnum(zhlbpf.getChdrnum());
						zhlbpfData.setValidflag(zhlbpf.getValidflag());
						zhlbpfData.setBnypc(rs.getBigDecimal(1));
						zhlbpfData.setTranno(rs.getInt(2));
						zhlbpfData.setTrdt(rs.getInt(3));
						zhlbpfData.setTrtm(rs.getInt(4));
						zhlbpfData.setUsr(rs.getInt(5));
						zhlbpfData.setActvalue(rs.getBigDecimal(6));
               	 	zhlbpfData.setZclmadjst(rs.getBigDecimal(7));
               	 	zhlbpfData.setZhldclmv(rs.getBigDecimal(8));
               	 	zhlbpfData.setZhldclma(rs.getBigDecimal(9));
               	 	zhlbpfData.setUsrprf(rs.getString(10));
               	 	zhlbpfData.setJobnm(rs.getString(11));
               		zhlbpfData.setDatime(rs.getTimestamp(12));
               	 	zhlbpfList.add(zhlbpfData);
					}
				}
				} catch (SQLException e) {
						LOGGER.error("read()", e);//IJTI-1561
						throw new SQLRuntimeException(e);
				} finally {
						close(stmt, rs);
						}

				return zhlbpfList;
	
	
			}


public List<Zhlbpf> readZhlbpfData(Zhlbpf zhlbpf){
	
	
	List<Zhlbpf> zhlbpfList = null;
	String query = "select bnyclt, bnypc,tranno,trdt,trtm,usr,actvalue,zclmadjst,zhldclmv,zhldclma,usrprf,jobnm ,datime"
					+ " from zhlbpf where (chdrcoy = ? and chdrnum = ? and validflag = ?)";

	ResultSet rs = null;
	Zhlbpf zhlbpfData = null;
	stmt = null;
	try {

		zhlbpfList = new ArrayList<Zhlbpf>();
		stmt = getConnection().prepareStatement(query);
		stmt.setString(1, zhlbpf.getChdrcoy());
		stmt.setString(2, zhlbpf.getChdrnum());
		stmt.setString(3, zhlbpf.getValidflag());
  
  

		rs = stmt.executeQuery();
		while (rs.next()) {
			zhlbpfData = new Zhlbpf();
			if (rs.getString(1) != null) {
				zhlbpfData.setChdrcoy(zhlbpf.getChdrcoy());
				zhlbpfData.setChdrnum(zhlbpf.getChdrnum());
			
				zhlbpfData.setValidflag(zhlbpf.getValidflag());
				zhlbpfData.setBnyclt(rs.getString(1));
				zhlbpfData.setBnypc(rs.getBigDecimal(2));
				zhlbpfData.setTranno(rs.getInt(3));
				zhlbpfData.setTrdt(rs.getInt(4));
				zhlbpfData.setTrtm(rs.getInt(5));
				zhlbpfData.setUsr(rs.getInt(6));
				zhlbpfData.setActvalue(rs.getBigDecimal(7));
       	 		zhlbpfData.setZclmadjst(rs.getBigDecimal(8));
       	 		zhlbpfData.setZhldclmv(rs.getBigDecimal(9));
       	 		zhlbpfData.setZhldclma(rs.getBigDecimal(10));
       	 		zhlbpfData.setUsrprf(rs.getString(11));
       	 		zhlbpfData.setJobnm(rs.getString(12));
       	 		zhlbpfData.setDatime(rs.getTimestamp(13));
       	 		zhlbpfList.add(zhlbpfData);
			}
		}
		} catch (SQLException e) {
				LOGGER.error("read()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
		} finally {
				close(stmt, rs);
				}

		return zhlbpfList;


	}
	
	
public boolean updateIntoZhlbpf(Zhlbpf zhlbpf){
	
    		stmt = null;
    
    		try{
    			StringBuilder queryString = new StringBuilder();

    			queryString.append("UPDATE  ZHLBPF  SET ZHLDCLMV =? , ZHLDCLMA =? , VALIDFLAG = ? ");

    			queryString.append(" where CHDRCOY = ? AND CHDRNUM =? AND BNYCLT =?  and VALIDFLAG = 1");
      
    			stmt = getConnection().prepareStatement(queryString.toString()); 
    			stmt.setBigDecimal(1, zhlbpf.getZhldclmv());              
    			stmt.setBigDecimal(2, zhlbpf.getZhldclma());
    			stmt.setString(3, zhlbpf.getValidflag());
    			stmt.setString(4, zhlbpf.getChdrcoy());
    			stmt.setString(5, zhlbpf.getChdrnum());
    			stmt.setString(6, zhlbpf.getBnyclt());
    		
    			
    			 int recordsRows= stmt.executeUpdate(); 
    	         stmt.close();
    	         if(recordsRows != 0)
    	         	{
    	        	 	return true;
    	         	}
    	         	else
    	         	{
    	         		return false;
    	         	}
      
    		}
    
    		catch (SQLException e) {
    			LOGGER.error("updateIntoZhlbpf()", e);//IJTI-1561
    			throw new SQLRuntimeException(e);
    		} finally {
    			close(stmt, null);
    		}
            
	}
         
}
	
	
	
	

