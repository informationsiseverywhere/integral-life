package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author ndhingra9
 * Definition of cancellation detail  file
 */
public class Kjodpf {
	private Long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private Integer tranno;
	private Integer effdate;
	private String currcd;
	private String crtable;
	private String shortds;
	private String liencd;
	private String riind; 
	private String unityp;
	private BigDecimal actvalue;
	private BigDecimal emv;
	private String vrtfund;
	private String claim;
	private String termid;
	private Integer trdt;
	private Integer trtm;
	private Integer userid;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public Integer getPlnsfx() {
		return plnsfx;
	}

	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}

	public Integer getTranno() {
		return tranno;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public Integer getEffdate() {
		return effdate;
	}

	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}

	public String getCurrcd() {
		return currcd;
	}

	public void setCurrcd(String currcd) {
		this.currcd = currcd;
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public String getShortds() {
		return shortds;
	}

	public void setShortds(String shortds) {
		this.shortds = shortds;
	}

	public String getLiencd() {
		return liencd;
	}

	public void setLiencd(String liencd) {
		this.liencd = liencd;
	}

	public String getRiind() {
		return riind;
	}

	public void setRiind(String riind) {
		this.riind = riind;
	}

	public String getunityp() {
		return unityp;
	}

	public void setunityp(String unityp) {
		this.unityp = unityp;
	}

	public BigDecimal getActvalue() {
		return actvalue;
	}

	public void setActvalue(BigDecimal actvalue) {
		this.actvalue = actvalue;
	}

	public BigDecimal getEmv() {
		return emv;
	}

	public void setEmv(BigDecimal emv) {
		this.emv = emv;
	}

	public String getVrtfund() {
		return vrtfund;
	}

	public void setVrtfund(String vrtfund) {
		this.vrtfund = vrtfund;
	}

	public String getClaim() {
		return claim;
	}

	public void setClaim(String claim) {
		this.claim = claim;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public Integer getTrdt() {
		return trdt;
	}

	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}

	public Integer getTrtm() {
		return trtm;
	}

	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}

	public Integer getuserid() {
		return userid;
	}

	public void setUser(Integer userid) {
		this.userid = userid;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public Timestamp getDatime() {
		return datime;
	}

	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}

	public void setUniqueNumber(Long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public Long getUniqueNumber() {
		return uniqueNumber;
	}
	
	
	
}
