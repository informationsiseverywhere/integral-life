package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:40
 * Description:
 * Copybook name: MATHCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mathclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mathclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mathclmKey = new FixedLengthStringData(64).isAPartOf(mathclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData mathclmChdrcoy = new FixedLengthStringData(1).isAPartOf(mathclmKey, 0);
  	public FixedLengthStringData mathclmChdrnum = new FixedLengthStringData(8).isAPartOf(mathclmKey, 1);
  	public PackedDecimalData mathclmTranno = new PackedDecimalData(5, 0).isAPartOf(mathclmKey, 9);
  	public PackedDecimalData mathclmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(mathclmKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(mathclmKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mathclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mathclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}