package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.Date;
import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Zhlbpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;


public interface ZhlbpfDAO extends BaseDAO<Zhlbpf>{
	
	public boolean insertIntoZhlbpf(Zhlbpf zhlbpf);
	public List<Zhlbpf> readZhlbpf(Zhlbpf zhlbpf);
	public boolean updateIntoZhlbpf(Zhlbpf zhlbpf);
	public List<Zhlbpf> readZhlbpfData(Zhlbpf zhlbpf);

}