package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: RegxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:18
 * Class transformed from REGXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class RegxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 48;
	public FixedLengthStringData regxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData regxpfRecord = regxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(regxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(regxrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(regxrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(regxrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(regxrec);
	public PackedDecimalData rgpynum = DD.rgpynum.copy().isAPartOf(regxrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(regxrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(regxrec);
	public PackedDecimalData nextPaydate = DD.npaydate.copy().isAPartOf(regxrec);
	public FixedLengthStringData rgpytype = DD.rgpytype.copy().isAPartOf(regxrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(regxrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(regxrec);
	public PackedDecimalData finalPaydate = DD.epaydate.copy().isAPartOf(regxrec);
	public PackedDecimalData revdte = DD.revdte.copy().isAPartOf(regxrec);
	public FixedLengthStringData rgpystat = DD.rgpystat.copy().isAPartOf(regxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public RegxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for RegxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public RegxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for RegxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public RegxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for RegxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public RegxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("REGXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"RGPYNUM, " +
							"PLNSFX, " +
							"TRANNO, " +
							"NPAYDATE, " +
							"RGPYTYPE, " +
							"CRTABLE, " +
							"VALIDFLAG, " +
							"EPAYDATE, " +
							"REVDTE, " +
							"RGPYSTAT, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     rgpynum,
                                     planSuffix,
                                     tranno,
                                     nextPaydate,
                                     rgpytype,
                                     crtable,
                                     validflag,
                                     finalPaydate,
                                     revdte,
                                     rgpystat,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		rgpynum.clear();
  		planSuffix.clear();
  		tranno.clear();
  		nextPaydate.clear();
  		rgpytype.clear();
  		crtable.clear();
  		validflag.clear();
  		finalPaydate.clear();
  		revdte.clear();
  		rgpystat.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getRegxrec() {
  		return regxrec;
	}

	public FixedLengthStringData getRegxpfRecord() {
  		return regxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setRegxrec(what);
	}

	public void setRegxrec(Object what) {
  		this.regxrec.set(what);
	}

	public void setRegxpfRecord(Object what) {
  		this.regxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(regxrec.getLength());
		result.set(regxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}