package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:27
 * Description:
 * Copybook name: HCLABANKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hclabankey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hclabanFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hclabanKey = new FixedLengthStringData(256).isAPartOf(hclabanFileKey, 0, REDEFINE);
  	public FixedLengthStringData hclabanChdrcoy = new FixedLengthStringData(1).isAPartOf(hclabanKey, 0);
  	public FixedLengthStringData hclabanChdrnum = new FixedLengthStringData(8).isAPartOf(hclabanKey, 1);
  	public FixedLengthStringData hclabanLife = new FixedLengthStringData(2).isAPartOf(hclabanKey, 9);
  	public FixedLengthStringData hclabanCoverage = new FixedLengthStringData(2).isAPartOf(hclabanKey, 11);
  	public FixedLengthStringData hclabanRider = new FixedLengthStringData(2).isAPartOf(hclabanKey, 13);
  	public FixedLengthStringData hclabanHosben = new FixedLengthStringData(5).isAPartOf(hclabanKey, 15);
  	public PackedDecimalData hclabanAcumactyr = new PackedDecimalData(4, 0).isAPartOf(hclabanKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(233).isAPartOf(hclabanKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hclabanFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hclabanFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}