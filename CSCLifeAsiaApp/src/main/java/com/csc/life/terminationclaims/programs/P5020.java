/*
 * File: P5020.java
 * Date: 29 August 2009 23:56:11
 * Author: Quipoz Limited
 * 
 * Class transformed from P5020.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifematTableDAM;
import com.csc.life.terminationclaims.dataaccess.MatdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.MathclmTableDAM;
import com.csc.life.terminationclaims.screens.S5020ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  P5020 - Maturity Reversal AT Submission.
*  ----------------------------------------
*
*  This  transaction, Maturity/Expiry Reversal is selected from
*  the Reversals Sub-menu S5117/P5117.
*
* Initialise
* ----------
*
*  Read  the    Contract    header  (function  RETRV)  and  read
*  the  relevant  data  necessary  for  obtaining   the   status
*  description,   the   Owner,   CCD,   Paid-to-date   and   the
*  Bill-to-date.
*
*  Read  the  first/only  Maturity     Header  record  (MATHCLM)
*  for this contract.
*
*  LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To  obtain  the  life assured and joint-life details (if any)
*  do the following;-
*
*       - Obtain  the  life  details  using LIFEMAT,(READR) (for
*            this  contract  number, using the life number/joint
*            life  number  from the MATHCLM record).  Format the
*            name accordingly.
*
*       - Obtain  the  joint-life  details using LIFEMAT (READR)
*            (for   this   contract   number,   using  the  life
*            number/joint  life number '01'). Format the name
*            accordingly.
*
*  Obtain the  necessary  descriptions   by    calling  'DESCIO'
*  and output the descriptions of the statuses to the screen.
*
*  Format Name
*
*       Read the  client  details  record  and  use the relevant
*            copybook in order to format the required names.
*
*  Build the Coverage/Rider review details
*
*       Read the  Maturity   Claim  details record (MATDCLM) and
*            output  a  detail  line,  for  each record for this
*            contract, to the subfile and display the entries.
*
*       Sum the  Estimated  values  and  the  Actual amounts and
*            display them on the screen.
*
*       From the  Maturity   Claim  header display the following
*            details on the screen:
*
*            - effective date
*            - policy loan amount
*            - other adjustments amount
*            - currency
*            - Maturity reason code
*            - Maturity reason description
*
*  The above details are returned  to  the user and if he wishes
*  to continue  and  perform  the  Reversal  he  presses  enter,
*  otherwise  he    opts    for  KILL  to  kill  and  exit  from
*  this transaction.
*
* Validation
* ----------
*
*  If  KILL  was entered, then  skip   the   remainder   of  the
*  validation and exit from the program.
*
*  No validation occurs, skip this section.
*
* Updating
* --------
*
*  If the KILL  function  key  was  pressed,  skip the  updating
*  and exit from the program.
*
*  Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
*  Call   the  AT  submission module to submit REVGENAT, passing
*  the Contract number, as the 'primary key', this performs  the
*  Full Reversal transaction.
*
*   Next Program
*   ------------
*
*  For KILL or 'Enter', add 1 to the program pointer and exit.
*
*
*
*****************************************************************
* </pre>
*/
public class P5020 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5020");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaEstMatValueTot = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaActvalueTot = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).setUnsigned();

	private ZonedDecimalData wsaaSwitchPlan = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private Validator wholePlan = new Validator(wsaaSwitchPlan, "1");
	private Validator partPlan = new Validator(wsaaSwitchPlan, "2");
	private Validator summaryPartPlan = new Validator(wsaaSwitchPlan, "3");

	private FixedLengthStringData wsaaValidTrans = new FixedLengthStringData(1).init(SPACES);
	private Validator validTrans = new Validator(wsaaValidTrans, "Y");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(32);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(8, 5, filler1, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(70);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
		/* ERRORS */
	private String e304 = "E304";
	private String f910 = "F910";
	private String h093 = "H093";
		/* TABLES */
	private String t1688 = "T1688";
	private String t5688 = "T5688";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String mathclmrec = "MATHCLMREC";
	private String matdclmrec = "MATDCLMREC";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
	private Atreqrec atreqrec = new Atreqrec();
		/*Contract Header Maturities*/
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Maturity/Expiry logical over COVR*/
	private CovrmatTableDAM covrmatIO = new CovrmatTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Logical File on LIFEPF-Maturity/Expiry*/
	private LifematTableDAM lifematIO = new LifematTableDAM();
		/*Maturity Detail Record*/
	private MatdclmTableDAM matdclmIO = new MatdclmTableDAM();
		/*Maturity Header Claim File*/
	private MathclmTableDAM mathclmIO = new MathclmTableDAM();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private S5020ScreenVars sv = ScreenProgram.getScreenVars( S5020ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		buildSubfile1030, 
		exit1009, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit3090, 
		exit4390
	}

	public P5020() {
		super();
		screenVars = sv;
		new ScreenModel("S5020", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					init1000();
					getLifeNoName1010();
					getJlifeNoName1020();
				}
				case buildSubfile1030: {
					buildSubfile1030();
				}
				case exit1009: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init1000()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1009);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5020", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		ptrnrevIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		chdrmatIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmatIO);
		if (isNE(chdrmatIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmatIO.getParams());
			fatalError600();
		}
		covrmatIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmatIO.getParams());
			fatalError600();
		}
		if (isEQ(covrmatIO.getPlanSuffix(),ZERO)) {
			wsaaSwitchPlan.set(1);
		}
		else {
			wsaaSwitchPlan.set(2);
		}
		mathclmIO.setParams(SPACES);
		mathclmIO.setChdrcoy(chdrmatIO.getChdrcoy());
		mathclmIO.setChdrnum(chdrmatIO.getChdrnum());
		mathclmIO.setTranno(ptrnrevIO.getTranno());
		mathclmIO.setPlanSuffix(covrmatIO.getPlanSuffix());
		mathclmIO.setFormat(mathclmrec);
		mathclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(),varcom.oK)) {
			scrnparams.statuz.set("KILL");
			goTo(GotoLabel.exit1009);
		}
		sv.estimateTotalValue.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.tdbtamt.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.clamant.set(ZERO);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		getDescription1100();
		sv.descrip.set(descIO.getLongdesc());
		sv.chdrnum.set(chdrmatIO.getChdrnum());
		sv.cnttype.set(chdrmatIO.getCnttype());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmatIO.getCnttype());
		getDescription1100();
		sv.ctypedes.set(descIO.getShortdesc());
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmatIO.getStatcode());
		getDescription1100();
		sv.rstate.set(descIO.getShortdesc());
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmatIO.getPstatcode());
		getDescription1100();
		sv.pstate.set(descIO.getShortdesc());
		sv.occdate.set(chdrmatIO.getOccdate());
		sv.cownnum.set(chdrmatIO.getCownnum());
		cltsIO.setClntnum(chdrmatIO.getCownnum());
		getClientName1200();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.ptdate.set(chdrmatIO.getPtdate());
		sv.btdate.set(chdrmatIO.getBtdate());
		sv.effdate.set(wsaaToday);
		sv.planSuffix.set(covrmatIO.getPlanSuffix());
		if (wholePlan.isTrue()) {
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
		}
	}

protected void getLifeNoName1010()
	{
		lifematIO.setDataArea(SPACES);
		lifematIO.setChdrcoy(chdrmatIO.getChdrcoy());
		lifematIO.setChdrnum(chdrmatIO.getChdrnum());
		lifematIO.setLife(mathclmIO.getLife());
		lifematIO.setJlife(mathclmIO.getJlife());
		lifematIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		lifematIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifematIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, lifematIO);
		if (isNE(lifematIO.getStatuz(),varcom.oK)
		&& isNE(lifematIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifematIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,lifematIO.getChdrcoy())
		|| isNE(chdrmatIO.getChdrnum(),lifematIO.getChdrnum())
		|| (isNE(lifematIO.getJlife(),"00")
		&& isNE(lifematIO.getJlife(),SPACES))
		|| isEQ(lifematIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifematIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifematIO.getLifcnum());
		cltsIO.setClntnum(lifematIO.getLifcnum());
		getClientName1200();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
	}

protected void getJlifeNoName1020()
	{
		lifematIO.setJlife("01");
		lifematIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifematIO);
		if (isNE(lifematIO.getStatuz(),varcom.oK)
		&& isNE(lifematIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifematIO.getParams());
			fatalError600();
		}
		if (isEQ(lifematIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set("*NONE");
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.buildSubfile1030);
		}
		sv.jlifcnum.set(lifematIO.getLifcnum());
		cltsIO.setClntnum(lifematIO.getLifcnum());
		getClientName1200();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.jlinsnameErr.set(e304);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
	}

protected void buildSubfile1030()
	{
		matdclmIO.setDataArea(SPACES);
		matdclmIO.setChdrcoy(mathclmIO.getChdrcoy());
		matdclmIO.setChdrnum(mathclmIO.getChdrnum());
		matdclmIO.setTranno(mathclmIO.getTranno());
		matdclmIO.setPlanSuffix(ZERO);
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(),varcom.oK)
		&& isNE(matdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			fatalError600();
		}
		wsaaEstMatValueTot.set(ZERO);
		wsaaActvalueTot.set(ZERO);
		while ( !(isEQ(matdclmIO.getStatuz(),varcom.endp))) {
			processMatdclm1500();
		}
		
		sv.otheradjst.set(mathclmIO.getOtheradjst());
		sv.tdbtamt.set(mathclmIO.getTdbtamt());
		sv.currcd.set(mathclmIO.getCurrcd());
		sv.estimateTotalValue.set(wsaaEstMatValueTot);
		compute(wsaaActvalueTot, 2).set(sub(sub(wsaaActvalueTot,sv.otheradjst),sv.tdbtamt));
		sv.clamant.set(wsaaActvalueTot);
		sv.reasoncd.set(mathclmIO.getReasoncd());
		sv.resndesc.set(mathclmIO.getResndesc());
	}

protected void getDescription1100()
	{
		/*READ-DESC*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setShortdesc(SPACES);
			descIO.setLongdesc(SPACES);
		}
		/*EXIT*/
	}

protected void getClientName1200()
	{
		/*READ-CLTS*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(chdrmatIO.getCownnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void processMatdclm1500()
	{
		readMatdclm1500();
		readNextMatdclm1504();
	}

protected void readMatdclm1500()
	{
		sv.life.set(matdclmIO.getLife());
		sv.coverage.set(matdclmIO.getCoverage());
		sv.rider.set(matdclmIO.getRider());
		sv.shortds.set(matdclmIO.getShortds());
		sv.cnstcur.set(matdclmIO.getCurrcd());
		sv.estMatValue.set(matdclmIO.getEstMatValue());
		sv.actvalue.set(matdclmIO.getActvalue());
		sv.fund.set(matdclmIO.getVirtualFund());
		sv.fundtype.set(matdclmIO.getFieldType());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5020", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaEstMatValueTot.add(matdclmIO.getEstMatValue());
		wsaaActvalueTot.add(matdclmIO.getActvalue());
	}

protected void readNextMatdclm1504()
	{
		matdclmIO.setFormat(matdclmrec);
		matdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(),varcom.endp)
		&& isNE(matdclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(matdclmIO.getParams());
			fatalError600();
		}
		if (isNE(matdclmIO.getChdrcoy(),chdrmatIO.getChdrcoy())
		|| isNE(matdclmIO.getChdrnum(),chdrmatIO.getChdrnum())
		|| isNE(matdclmIO.getTranno(),mathclmIO.getTranno())
		|| isEQ(matdclmIO.getStatuz(),varcom.endp)) {
			matdclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrmatIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrmatIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTranDate.set(varcom.vrcmDate);
		wsaaTranTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaTranCode.set(ptrnrevIO.getBatctrcde());
		wsaaTodate.set(ptrnrevIO.getPtrneff());
		wsaaSupflag.set("N");
		wsaaSuppressTo.set(ZERO);
		wsaaPlnsfx.set(mathclmIO.getPlanSuffix());
		wsaaTranNum.set(mathclmIO.getTranno());
		wsaaCfiafiTranno.set(ZERO);
		wsaaCfiafiTranCode.set(SPACES);
		wsaaBbldat.set(ZERO);
		atreqrec.transArea.set(wsaaTransArea);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
		try {
			callSubroutine4310();
		}
		catch (GOTOException e){
		}
	}

protected void callSubroutine4310()
	{
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4390);
		}
		compute(wsaaX, 0).set(add(1,wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void loadProgram4400()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}
}
