package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5016
 * @version 1.0 generated on 30/08/09 06:30
 * @author Quipoz
 */
public class S5016ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(176);
	public FixedLengthStringData dataFields = new FixedLengthStringData(96).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 96);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 116);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(380);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(106).isAPartOf(subfileArea, 0);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData exprdate = DD.exprdate.copyToZonedDecimal().isAPartOf(subfileFields,18);
	public FixedLengthStringData fupcdes = DD.fupcdes.copy().isAPartOf(subfileFields,26);
	public ZonedDecimalData fupremdt = DD.fupdt.copyToZonedDecimal().isAPartOf(subfileFields,30);
	public ZonedDecimalData fupno = DD.fupno.copyToZonedDecimal().isAPartOf(subfileFields,38);
	public ZonedDecimalData fuprcvd = DD.fuprcvd.copyToZonedDecimal().isAPartOf(subfileFields,40);
	public FixedLengthStringData fupremk = DD.fuprmk.copy().isAPartOf(subfileFields,48);
	public FixedLengthStringData fupstat = DD.fupsts.copy().isAPartOf(subfileFields,88);
	public FixedLengthStringData fuptype = DD.fuptyp.copy().isAPartOf(subfileFields,89);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(subfileFields,90);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(subfileFields,91);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(subfileFields,93);
	public ZonedDecimalData lifeno = DD.lifeno.copyToZonedDecimal().isAPartOf(subfileFields,94);
	public FixedLengthStringData sfflg = DD.sfflg.copy().isAPartOf(subfileFields,96);
	public FixedLengthStringData uprflag = DD.uprflag.copy().isAPartOf(subfileFields,97);
	public FixedLengthStringData zitem = DD.zitem.copy().isAPartOf(subfileFields,98);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(68).isAPartOf(subfileArea, 106);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData crtuserErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData exprdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData fupcdesErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData fupdtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData fupnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData fuprcvdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData fuprmkErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData fupstsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData fuptypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData lifenoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData sfflgErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData uprflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData zitemErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 64);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(204).isAPartOf(subfileArea, 174);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] crtuserOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] exprdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] fupcdesOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] fupdtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] fupnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] fuprcvdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] fuprmkOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] fupstsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] fuptypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] lifenoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] sfflgOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public FixedLengthStringData[] uprflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	public FixedLengthStringData[] zitemOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 192);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 378);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData exprdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fupremdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fuprcvdDisp = new FixedLengthStringData(10);

	public LongData S5016screensflWritten = new LongData(0);
	public LongData S5016screenctlWritten = new LongData(0);
	public LongData S5016screenWritten = new LongData(0);
	public LongData S5016protectWritten = new LongData(0);
	public GeneralTable s5016screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5016screensfl;
	}

	public S5016ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(fupcdesOut,new String[] {"30","38","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifenoOut,new String[] {"31","37","-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlifeOut,new String[] {"35","37","-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupstsOut,new String[] {"32","37","-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupdtOut,new String[] {"33","37","-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuprmkOut,new String[] {"34","37","-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sfflgOut,new String[] {"44","47","-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuptypOut,new String[] {null, null, null, "40",null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuprcvdOut,new String[] {"41","39","-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(exprdateOut,new String[] {"42","39","-42",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {fupno, uprflag, crtuser, zitem, indic, language, fupcdes, lifeno, jlife, fupstat, fupremdt, crtdate, fupremk, sfflg, fuptype, fuprcvd, exprdate};
		screenSflOutFields = new BaseData[][] {fupnoOut, uprflagOut, crtuserOut, zitemOut, indicOut, languageOut, fupcdesOut, lifenoOut, jlifeOut, fupstsOut, fupdtOut, crtdateOut, fuprmkOut, sfflgOut, fuptypOut, fuprcvdOut, exprdateOut};
		screenSflErrFields = new BaseData[] {fupnoErr, uprflagErr, crtuserErr, zitemErr, indicErr, languageErr, fupcdesErr, lifenoErr, jlifeErr, fupstsErr, fupdtErr, crtdateErr, fuprmkErr, sfflgErr, fuptypErr, fuprcvdErr, exprdateErr};
		screenSflDateFields = new BaseData[] {fupremdt, crtdate, fuprcvd, exprdate};
		screenSflDateErrFields = new BaseData[] {fupdtErr, crtdateErr, fuprcvdErr, exprdateErr};
		screenSflDateDispFields = new BaseData[] {fupremdtDisp, crtdateDisp, fuprcvdDisp, exprdateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cownnum, ownername};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5016screen.class;
		screenSflRecord = S5016screensfl.class;
		screenCtlRecord = S5016screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5016protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5016screenctl.lrec.pageSubfile);
	}
}
