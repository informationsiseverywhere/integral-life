package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:42
 * Description:
 * Copybook name: CLMALLREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Clmallrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData clmallRec = new FixedLengthStringData(77);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(clmallRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(clmallRec, 4);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(clmallRec, 8);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(clmallRec, 9);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(clmallRec, 17);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(clmallRec, 19);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(clmallRec, 21);
  	public ZonedDecimalData planSuffix = new ZonedDecimalData(4, 0).isAPartOf(clmallRec, 23);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(clmallRec, 27);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(clmallRec, 29).setUnsigned();
  	public ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(clmallRec, 37).setUnsigned();
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(clmallRec, 42);
  	public FixedLengthStringData jlifefound = new FixedLengthStringData(1).isAPartOf(clmallRec, 46);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(clmallRec, 47);
	public PackedDecimalData user = new PackedDecimalData(6, 0).isAPartOf(clmallRec, 51);  //ILIFE-2714 by slakkala
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(clmallRec, 57, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(clmallRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		clmallRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}