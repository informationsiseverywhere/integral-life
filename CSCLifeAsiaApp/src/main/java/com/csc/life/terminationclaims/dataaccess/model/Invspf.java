package com.csc.life.terminationclaims.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Invspf implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long uniqueNumber;
	//INVSCOY
	private String invscoy;
	//NOTIFINUM
	private String notifinum;
	//LIFENUM
	private String lifenum;
	//CLAIMANT
	private String claimant;
	//RELATIONSHIP
	private String relationship;
	//INVESTIGATION RESULT
	private String investigationResult;
	// USRPRF
	private String usrprf;
	// JOBNM
	private String jobnm;
	// DATIME
	private String datime;
	private String claimno;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getInvscoy() {
		return invscoy;
	}
	public void setInvscoy(String invscoy) {
		this.invscoy = invscoy;
	}
	public String getNotifinum() {
		return notifinum;
	}
	public void setNotifinum(String notifinum) {
		this.notifinum = notifinum;
	}
	public String getLifenum() {
		return lifenum;
	}
	public void setLifenum(String lifenum) {
		this.lifenum = lifenum;
	}
	public String getClaimant() {
		return claimant;
	}
	public void setClaimant(String claimant) {
		this.claimant = claimant;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getInvestigationResult() {
		return investigationResult;
	}
	public void setInvestigationResult(String investigationResult) {
		this.investigationResult = investigationResult;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public String getClaimno() {
		return claimno;
	}
	public void setClaimno(String claimno) {
		this.claimno = claimno;
	}
	
}
