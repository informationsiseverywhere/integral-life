package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr689screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 12, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 15, 2, 69}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr689ScreenVars sv = (Sr689ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr689screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr689ScreenVars screenVars = (Sr689ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.inqopt.setClassString("");
		screenVars.hosben.setClassString("");
		screenVars.benefits.setClassString("");
		screenVars.zunit.setClassString("");
		screenVars.datefromDisp.setClassString("");
		screenVars.datetoDisp.setClassString("");
		screenVars.gincurr.setClassString("");
		screenVars.daclaim.setClassString("");
		screenVars.benlmt.setClassString("");
		screenVars.nofday.setClassString("");
		screenVars.gtotinc.setClassString("");
		screenVars.zdaycov.setClassString("");
		screenVars.gdeduct.setClassString("");
		screenVars.copay.setClassString("");
		screenVars.tamtyear.setClassString("");
		screenVars.amtyear.setClassString("");
		screenVars.tclmamt.setClassString("");
		screenVars.amtlife.setClassString("");
		screenVars.amtfld.setClassString("");
		screenVars.gcnetpy.setClassString("");
		screenVars.comt.setClassString("");
	}

/**
 * Clear all the variables in Sr689screen
 */
	public static void clear(VarModel pv) {
		Sr689ScreenVars screenVars = (Sr689ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.inqopt.clear();
		screenVars.hosben.clear();
		screenVars.benefits.clear();
		screenVars.zunit.clear();
		screenVars.datefromDisp.clear();
		screenVars.datefrom.clear();
		screenVars.datetoDisp.clear();
		screenVars.dateto.clear();
		screenVars.gincurr.clear();
		screenVars.daclaim.clear();
		screenVars.benlmt.clear();
		screenVars.nofday.clear();
		screenVars.gtotinc.clear();
		screenVars.zdaycov.clear();
		screenVars.gdeduct.clear();
		screenVars.copay.clear();
		screenVars.tamtyear.clear();
		screenVars.amtyear.clear();
		screenVars.tclmamt.clear();
		screenVars.amtlife.clear();
		screenVars.amtfld.clear();
		screenVars.gcnetpy.clear();
		screenVars.comt.clear();
	}
}
