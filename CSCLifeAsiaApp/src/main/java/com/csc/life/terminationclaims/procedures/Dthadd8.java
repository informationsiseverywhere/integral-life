/*
 * File: Dthadd8.java
 * Date: 16 Oct 2012 22:47:19
 * Author: Quipoz Limited
 * 
 * Class transformed from DTHADD8.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.anticipatedendowment.procedures.Zrdecplc;
import com.csc.life.anticipatedendowment.recordstructures.Zrdecplrec;
import com.csc.life.interestbearing.dataaccess.HitrclmTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.ClmdaddTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  ADDITIONAL PROCESSING 8 FOR DEATH CLAIMS.
*
*             DEATH CLAIM ADDITIONAL PROCESSING METHOD 8.
*            -------------------------------------------
*
*  This program is an item entry on T6598, the death claim
*  subroutine method table. This method is used in order to
*  update the relevant sub-accounts. The trigger module and
*  key were passed with other details with the UTRN record.
*
*   PROCESSING.
*   ----------
*
*  This routine is called once for each UTRN.
*
*  Read the Claim detail record (CLMDCLM) for this trigger
*  key and set the estimated amount to zero. Set the Actual
*  amount to the trigger-amount passed and re-write the CLMD
*  record.
*
*  Read all the CLMD records for this coverage/rider and sum
*  the estimates and actuals.
*
*  If the sum of the estimated amounts is not zero, then skip
*  all further processing.
*
*  If the accumulated estimated amount is equal to zero, then
*  the claim amount is calculated as follows:- (read the COVR
*  records and accumulate the Sum Insured (SI) amounts), the
*  claim amount is the SI amount minus the Actual amount.
*
*  If the claim amount is less than zero, then set the claim
*  amount to zero.
*
*  Update the 'S' type record with the Estimate amount equal
*  to zero and set the Actual amount to the claim amount.
*
*  Post the Claim amount to the following sub-accounts,
*  Outstanding Claims (01) and the Death Claims (02).
*
*     CONTRACT ACCOUNTING FOR DEATH CLAIMS
*
*  Read the transaction accounting rules from table T5645
*  (keyed on program no P5256) this contains the rules
*  necessary for posting the monies to the correct accounts.
*
*  Read the description of this table entry as well. This is
*  used as narrative description on all the postings generated.
*
*  If the amounts are not zero, then call LIFACMV ("non-cash"
*  posting subroutine) to post to the correct suspense account.
*  The posting required is defined in the appropriate line no.
*  on the T5645 table entry.  Set up and pass the linkage area
*  as follows:
*
*               Function               - PSTW
*               Batch key              - AT linkage
*               Document number        - contract number
*               Sequence number        - transaction no.
*               Sub-account code       - from applicable T5645
*                                        entry
*               Sub-account type       - ditto
*               Sub-account GL map     - ditto
*               Sub-account GL sign    - ditto
*               S/acct control total   - ditto
*               Cmpy code (sub ledger) - batch company
*               Cmpy code (GL)         - batch company
*               Subsidiary ledger      - contract number
*               Original currency code - currency payable in
*               Original currency amt  - claim amount
*               Accounting curr code   - blank (handled by
*                                        subroutine)
*               Accounting curr amount - zero (handled by
*                                        subroutine)
*               Exchange rate          - zero (handled by
*                                        subroutine)
*               Trans reference        - contract transaction
*                                        number
*               Trans description      - from transaction code
*                                        description
*               Posting month and year - defaulted
*               Effective date         - Death Claim
*                                        effective-date
*               Reconciliation amount  - zero
*               Reconciliation date    - Max date
*               Transaction Id         - AT linkage
*               Substitution code 1    - contract type
*
*  Read all the CLMD records for that coverage/rider and
*  accumulate the Estimated amounts and accumulate the Actual
*  amounts of 'F' type records.
*
*  If the accumulated estimated amount is equal to zero,
*  then the claim amount is calculated as the Sum Insured (SI)
*  minus the Actual amount.
*
*  If the claim amount is less than zero, then set the claim
*  amount to zero.
*
*  Update the 'S' type record with the Estimated amount
*  equal to zero and set the Actual amount to the claim amount.       .
*
*  If the claim amount is not zero, then post to the O/S
*  Claim (01) and to the Death Claim (02) sub-accounts.
*
*  If the amounts are not zero, then call LIFRTRN ("cash"
*  posting subroutine) to post to the correct suspense
*  account.  The posting required is defined in the
*  appropriate line no.  on the T5645 table entry.  Set up and
*  pass the linkage area as follows:
*
*             Function               - PSTW
*             Batch key              - from AT linkage
*             Document number        - contract number
*             Sequence number        - transaction no.
*             Sub-account code       - from applicable T5645
*             Sub-account type       - ditto
*             Sub-account GL map     - ditto
*             Sub-account GL sign    - ditto
*             S/acct control total   - ditto
*             Cmpy code (sub ledger) - batch company
*             Cmpy code (GL)         - batch company
*             Subsidiary ledger      - contract number
*             Original currency code - currency payable in
*             Original currency amt  - claim amount
*             Accounting curr code   - blank (handled by
*                                      subroutine)
*             Accounting curr amount - zero (handled by
*                                      subroutine)
*             Exchange rate          - Zero (handled by
*                                      subroutine)
*             Trans reference        - contract transaction
*                                      number
*             Trans description      - from transaction code
*                                      description
*             Posting month and year - defaulted
*             Effective date         - Death Claim
*                                      effective-date
*             Reconciliation amount  - zero
*             Reconciliation date    - Max date
*             Transaction Id         - from AT linkage
*             Substitution code 1    - contract type
/
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dthadd8 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "DTHADD8";
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String clmdaddrec = "CLMDADDREC";
	private static final String hitrclmrec = "HITRCLMREC";
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaActvalue = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaDeemCharge = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaGrossVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaGrossRound = new PackedDecimalData(17, 5).init(0);
	private PackedDecimalData wsaaAcumGrossVal = new PackedDecimalData(17, 2).init(0);
	private String wsaaInitialUnit = "";
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaClaimAmount = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).init(ZERO);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaContractType = new FixedLengthStringData(4).init(SPACES);
	private ClmdaddTableDAM clmdaddIO = new ClmdaddTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrclmTableDAM hitrclmIO = new HitrclmTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Udtrigrec udtrigrec = new Udtrigrec();

	public Dthadd8() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		udtrigrec.udtrigrecRec = convertAndSetParam(udtrigrec.udtrigrecRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		readUtrn1000();
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			a200ReadAndUpdateClaim();
		}
		else {
			readAndUpdateClaim2000();
		}
		mainProcClaimDetails3000();
		/*EXIT*/
		exitProgram();
	}

protected void readTabT5645350()
	{
		read351();
	}

protected void read351()
	{
		itemIO.setItemcoy(udtrigrec.tk3Chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void readTabT5688400()
	{
		read410();
	}

protected void read410()
	{
		itdmIO.setItemcoy(udtrigrec.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaContractType);
		itdmIO.setItmfrm(udtrigrec.effdate);
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
//		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), udtrigrec.company)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), wsaaContractType)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaContractType);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readUtrn1000()
	{
		go1010();
	}

protected void go1010()
	{
		varcom.vrcmTermid.set(SPACES);
		utrnIO.setParams(SPACES);
		utrnIO.setChdrcoy(udtrigrec.chdrcoy);
		utrnIO.setChdrnum(udtrigrec.chdrnum);
		utrnIO.setLife(udtrigrec.life);
		utrnIO.setCoverage(udtrigrec.coverage);
		utrnIO.setRider(udtrigrec.rider);
		utrnIO.setPlanSuffix(udtrigrec.planSuffix);
		utrnIO.setUnitVirtualFund(udtrigrec.unitVirtualFund);
		utrnIO.setUnitType(udtrigrec.unitType);
		utrnIO.setTranno(udtrigrec.tranno);
		utrnIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)
		&& isNE(utrnIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(utrnIO.getStatuz());
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			a100ReadHitr();
			return ;
		}
		wsaaContractType.set(utrnIO.getContractType());
	}

protected void readAndUpdateClaim2000()
	{
		go2010();
	}

protected void go2010()
	{
		clmdaddIO.setParams(SPACES);
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setLife(udtrigrec.tk3Life);
		clmdaddIO.setCoverage(udtrigrec.tk3Coverage);
		clmdaddIO.setRider(udtrigrec.tk3Rider);
		clmdaddIO.setFieldType(utrnIO.getUnitType());
		clmdaddIO.setVirtualFund(utrnIO.getUnitVirtualFund());
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clmdaddIO.getStatuz());
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setEstMatValue(ZERO);
		wsaaDeemCharge.set(ZERO);
		wsaaGrossVal.set(ZERO);
		wsaaGrossRound.set(ZERO);
		/*Check UTRN-UNIT-TYPE.*/
		if (isEQ(utrnIO.getUnitType(), "I")
		&& isNE(utrnIO.getNofDunits(), utrnIO.getNofUnits())) {
			wsaaInitialUnit = "Y";
		}
		else {
			wsaaInitialUnit = " ";
		}
		/* Calculate PEND-CLAIM amount for 'A' units.*/
		/* Round the amount to 2 decimal.*/
		if (isEQ(wsaaInitialUnit, " ")) {
			compute(wsaaGrossRound, 5).set(mult(utrnIO.getPriceUsed(), utrnIO.getNofDunits()));
			compute(wsaaGrossVal, 6).setRounded(mult(wsaaGrossRound, 1));
			if (isNE(wsaaGrossVal, 0)) {
				if (isLT(wsaaGrossVal, 0)) {
					compute(zrdecplrec.amountIn, 3).set(mult(wsaaGrossVal, -1));
				}
				else {
					zrdecplrec.amountIn.set(wsaaGrossVal);
				}
				zrdecplrec.currency.set(utrnIO.getCntcurr());
				b100ZrdecplcCall();
				if (isLT(wsaaGrossVal, 0)) {
					compute(wsaaGrossVal, 3).set(mult(zrdecplrec.amountOut, -1));
				}
				else {
					wsaaGrossVal.set(zrdecplrec.amountOut);
				}
			}
			wsaaAcumGrossVal.set(wsaaGrossVal);
		}
		/* Calculate deemed amount for 'I' units.*/
		if (isEQ(wsaaInitialUnit, "Y")) {
			compute(wsaaGrossRound, 5).set(mult(utrnIO.getPriceUsed(), utrnIO.getNofDunits()));
			/* Round up to 2 decimal.*/
			if (isEQ(wsaaGrossRound, ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				compute(wsaaGrossVal, 6).setRounded(mult(wsaaGrossRound, 1));
			}
			if (isNE(wsaaGrossVal, 0)) {
				zrdecplrec.currency.set(utrnIO.getCntcurr());
				zrdecplrec.amountIn.set(wsaaGrossVal);
				b100ZrdecplcCall();
				wsaaGrossVal.set(zrdecplrec.amountOut);
			}
			if (isEQ(wsaaGrossVal, ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				compute(wsaaDeemCharge, 2).set(sub(wsaaGrossVal, utrnIO.getContractAmount()));
			}
		}
		/*    If initial units, than move deem amount to*/
		/*    Claim detail file, else add utrn-cont-amt.*/
		if (isEQ(wsaaInitialUnit, "Y")) {
			clmdaddIO.setActvalue(wsaaGrossVal);
		}
		else {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(add(clmdaddIO.getActvalue(), utrnIO.getContractAmount()));
		}
		if (isGT(clmdaddIO.getActvalue(), ZERO)) {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), 1));
		}
		else {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), -1));
		}
		clmdaddIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clmdaddIO.getStatuz());
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
	}

	/**
	* <pre>
	*   read all the claim details per coverage/rider
	* </pre>
	*/
protected void mainProcClaimDetails3000()
	{
		/*GO*/
		wsaaEstimateTot.set(ZERO);
		clmdaddIO.setParams(SPACES);
		clmdaddIO.setFunction(varcom.begn);
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setFormat(clmdaddrec);
		while ( !(isEQ(clmdaddIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23 
//			clmdaddIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//			clmdaddIO.setFitKeysSearch("CHDRCOY","CHDRNUM","COVERAGE","RIDER");
			readClaims3500();
		}
		
		/*  if the accumulated estimated amount is not zero then*/
		/*  skip all further processing*/
		if (isNE(wsaaEstimateTot, ZERO)) {
			return ;
		}
		readCovr3600();
		/*EXIT*/
	}

protected void readClaims3500()
	{
		go3510();
	}

protected void go3510()
	{
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)
		&& isNE(clmdaddIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(clmdaddIO.getStatuz());
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
		if (isNE(udtrigrec.tk3Chdrcoy, clmdaddIO.getChdrcoy())
		|| isNE(udtrigrec.tk3Chdrnum, clmdaddIO.getChdrnum())
		|| isNE(udtrigrec.tk3Coverage, clmdaddIO.getCoverage())
		|| isNE(udtrigrec.tk3Rider, clmdaddIO.getRider())
		|| isEQ(clmdaddIO.getStatuz(), varcom.endp)) {
			clmdaddIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(clmdaddIO.getFieldType(), "I")
		|| isEQ(clmdaddIO.getFieldType(), "A")
		|| isEQ(clmdaddIO.getFieldType(), "D")) {
			wsaaEstimateTot.add(clmdaddIO.getEstMatValue());
		}
		if (isNE(wsaaEstimateTot, ZERO)) {
			clmdaddIO.setStatuz(varcom.endp);
		}
		else {
			clmdaddIO.setFunction(varcom.nextr);
		}
	}

protected void readCovr3600()
	{
		go3610();
	}

protected void go3610()
	{
		wsaaSumins.set(ZERO);
		wsaaClaimAmount.set(ZERO);
		wsaaStoredCoverage.set(SPACES);
		wsaaStoredRider.set(SPACES);
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		covrIO.setChdrnum(udtrigrec.tk3Chdrnum);
		covrIO.setCoverage(udtrigrec.tk3Coverage);
		covrIO.setRider(udtrigrec.tk3Rider);
		covrIO.setLife(udtrigrec.tk3Life);
		covrIO.setPlanSuffix(0);
		covrIO.setFunction(varcom.begn);


		//performance improvement --  atiwari23 
//		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		if (isNE(covrIO.getChdrnum(), udtrigrec.tk3Chdrnum)
		|| isNE(covrIO.getChdrcoy(), udtrigrec.tk3Chdrcoy)
		|| isNE(covrIO.getLife(), udtrigrec.tk3Life)
		|| isNE(covrIO.getCoverage(), udtrigrec.tk3Coverage)
		|| isNE(covrIO.getRider(), udtrigrec.tk3Rider)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		/*  store coverage and rider and currency*/
		wsaaStoredCoverage.set(covrIO.getCoverage());
		wsaaStoredRider.set(covrIO.getRider());
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			/*   Since we have already posted the FV. Here we want to post  **/
			/*   the sumin only.                                            **/
			/*   COMPUTE WSAA-CLAIM-AMOUNT   = WSAA-SUMINS - WSAA-ACTUAL-TOT.*/
			processComponents3700();
		}
		
		wsaaClaimAmount.set(wsaaSumins);
		if (isLTE(wsaaClaimAmount, ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			readAndUpdateSaClaim6000();
		}
		if (isNE(wsaaClaimAmount, ZERO)) {
			readTabT5645350();
			readTabT5688400();
			postToDeathClaim3800();
		}
	}

protected void processComponents3700()
	{
		/*READ*/
		wsaaSumins.add(covrIO.getSumins());
		while ( !((isNE(wsaaStoredRider, covrIO.getRider()))
		|| (isNE(wsaaStoredCoverage, covrIO.getCoverage()))
		|| isEQ(covrIO.getStatuz(), varcom.endp))) {
			findNextComponent4000();
		}
		
		if (isNE(wsaaStoredRider, covrIO.getRider())
		|| isNE(wsaaStoredCoverage, covrIO.getCoverage())) {
			covrIO.setStatuz(varcom.endp);
		}
		wsaaStoredCoverage.set(covrIO.getCoverage());
		wsaaStoredRider.set(covrIO.getRider());
		/*EXIT*/
	}

protected void postToDeathClaim3800()
	{
		read3810();
	}

protected void read3810()
	{
		batcdorrec.function.set("AUTO");
		varcom.vrcmTermid.set(SPACES);
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmUser.set(ZERO);
		batcdorrec.tranid.set(varcom.vrcmTranid);
		/*    MOVE UTRN-BATCCOY           TO LIFA-BATCCOY.                 */
		/*    MOVE UTRN-BATCBRN           TO LIFA-BATCBRN.                 */
		/*    MOVE UTRN-BATCACTYR         TO LIFA-BATCACTYR.               */
		/*    MOVE UTRN-BATCACTMN         TO LIFA-BATCACTMN.               */
		lifacmvrec1.batckey.set(udtrigrec.batchkey);
		lifacmvrec1.batctrcde.set(utrnIO.getBatctrcde());
		/*    MOVE UTRN-BATCBATCH         TO LIFA-BATCBATCH.               */
		batcdorrec.batchkey.set(lifacmvrec1.batckey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.params.set(batcdorrec.batcdorRec);
			syserrrec.statuz.set(batcdorrec.statuz);
			fatalError9000();
		}
		lifacmvrec1.batckey.set(batcdorrec.batchkey);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.rdocnum.set(udtrigrec.tk3Chdrnum);
		lifacmvrec1.tranno.set(utrnIO.getTranno());
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.rldgcoy.set(udtrigrec.tk3Chdrcoy);
		lifacmvrec1.genlcoy.set(udtrigrec.tk3Chdrcoy);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode01);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec1.glcode.set(t5645rec.glmap01);
			lifacmvrec1.glsign.set(t5645rec.sign01);
			lifacmvrec1.contot.set(t5645rec.cnttot01);
			lifacmvrec1.rldgacct.set(SPACES);
			wsaaRldgChdrnum.set(udtrigrec.chdrnum);
			wsaaRldgLife.set(udtrigrec.life);
			wsaaRldgCoverage.set(udtrigrec.coverage);
			wsaaRldgRider.set(udtrigrec.rider);
			wsaaPlansuff.set(udtrigrec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.substituteCode[1].set(SPACES);
			lifacmvrec1.substituteCode[6].set(utrnIO.getCrtable());
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode03);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec1.glcode.set(t5645rec.glmap03);
			lifacmvrec1.glsign.set(t5645rec.sign03);
			lifacmvrec1.contot.set(t5645rec.cnttot03);
			lifacmvrec1.substituteCode[1].set(wsaaContractType);
			lifacmvrec1.substituteCode[6].set(SPACES);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(udtrigrec.tk3Chdrnum);
		}
		lifacmvrec1.origcurr.set(utrnIO.getCntcurr());
		lifacmvrec1.origamt.set(wsaaClaimAmount);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.tranref.set(utrnIO.getTranno());
		descIO.setDescitem(lifacmvrec1.batctrcde);
		getDescription5000();
		lifacmvrec1.effdate.set(utrnIO.getMoniesDate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.termid.set(SPACES);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.transactionDate.set(getCobolDate());
		lifacmvrec1.transactionTime.set(getCobolTime());
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError9000();
		}
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode04);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec1.glcode.set(t5645rec.glmap04);
			lifacmvrec1.glsign.set(t5645rec.sign04);
			lifacmvrec1.contot.set(t5645rec.cnttot04);
			wsaaRldgChdrnum.set(udtrigrec.chdrnum);
			wsaaRldgLife.set(udtrigrec.life);
			wsaaRldgCoverage.set(udtrigrec.coverage);
			wsaaRldgRider.set(udtrigrec.rider);
			wsaaPlansuff.set(udtrigrec.planSuffix);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.substituteCode[1].set(SPACES);
			lifacmvrec1.substituteCode[6].set(utrnIO.getCrtable());
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode02);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec1.glcode.set(t5645rec.glmap02);
			lifacmvrec1.glsign.set(t5645rec.sign02);
			lifacmvrec1.contot.set(t5645rec.cnttot02);
			lifacmvrec1.substituteCode[1].set(wsaaContractType);
			lifacmvrec1.substituteCode[6].set(SPACES);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(udtrigrec.tk3Chdrnum);
		}
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError9000();
		}
	}

protected void postingsToDeathClaim3900()
	{
		read3910();
	}

protected void read3910()
	{
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(ZERO);
		/* Posting at contract level.  Total of accumulation and initial*/
		/* units paid to the client.*/
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rdocnum.set(udtrigrec.tk3Chdrnum);
		lifacmvrec1.tranno.set(utrnIO.getTranno());
		lifacmvrec1.sacscode.set(t5645rec.sacscode02);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec1.glcode.set(t5645rec.glmap02);
		lifacmvrec1.glsign.set(t5645rec.sign02);
		lifacmvrec1.contot.set(t5645rec.cnttot02);
		lifacmvrec1.rldgacct.set(udtrigrec.tk3Chdrnum);
		lifacmvrec1.rldgcoy.set(udtrigrec.tk3Chdrcoy);
		lifacmvrec1.genlcoy.set(udtrigrec.tk3Chdrcoy);
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.origcurr.set(utrnIO.getCntcurr());
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.tranref.set(utrnIO.getTranno());
		descIO.setDescitem(lifacmvrec1.batctrcde);
		getDescription5000();
		lifacmvrec1.effdate.set(utrnIO.getMoniesDate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(utrnIO.getContractType());
		lifacmvrec1.transactionDate.set(getCobolDate());
		lifacmvrec1.transactionTime.set(getCobolTime());
		lifacmvrec1.termid.set(SPACES);
		wsaaGrossVal.add(wsaaAcumGrossVal);
		lifacmvrec1.origamt.set(wsaaGrossVal);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError9000();
		}
		if (isEQ(wsaaDeemCharge, ZERO)) {
			return ;
		}
		lifacmvrec1.function.set("PSTW");
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			wsaaRldgChdrnum.set(utrnIO.getChdrnum());
			wsaaRldgLife.set(utrnIO.getLife());
			wsaaRldgCoverage.set(utrnIO.getCoverage());
			wsaaRldgRider.set(utrnIO.getRider());
			wsaaPlansuff.set(utrnIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.substituteCode[6].set(SPACES);
			lifacmvrec1.substituteCode[1].set(utrnIO.getCoverage());
			lifacmvrec1.substituteCode[2].set(utrnIO.getUnitVirtualFund());
			lifacmvrec1.sacscode.set(t5645rec.sacscode05);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec1.glcode.set(t5645rec.glmap05);
			lifacmvrec1.glsign.set(t5645rec.sign05);
			lifacmvrec1.contot.set(t5645rec.cnttot05);
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode06);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec1.glcode.set(t5645rec.glmap06);
			lifacmvrec1.glsign.set(t5645rec.sign06);
			lifacmvrec1.contot.set(t5645rec.cnttot06);
			lifacmvrec1.substituteCode[1].set(utrnIO.getCoverage());
			lifacmvrec1.substituteCode[2].set(utrnIO.getUnitVirtualFund());
			lifacmvrec1.substituteCode[6].set(SPACES);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(udtrigrec.tk3Chdrnum);
			lifacmvrec1.origamt.set(wsaaDeemCharge);
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz, varcom.oK)) {
				syserrrec.statuz.set(lifacmvrec1.statuz);
				syserrrec.params.set(lifacmvrec1.lifacmvRec);
				fatalError9000();
			}
		}
	}

protected void findNextComponent4000()
	{
		/*NEXT*/
		/* Read the next coverage/rider record.*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError9000();
		}
		if (isNE(covrIO.getChdrcoy(), udtrigrec.tk3Chdrcoy)
		|| isNE(covrIO.getChdrnum(), udtrigrec.tk3Chdrcoy)
		|| isNE(covrIO.getLife(), udtrigrec.tk3Life)) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void getDescription5000()
	{
		/*PARA*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(udtrigrec.tk3Chdrcoy);
		descIO.setLanguage(udtrigrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		lifacmvrec1.trandesc.set(descIO.getShortdesc());
		/*EXIT*/
	}

protected void readAndUpdateSaClaim6000()
	{
		go6010();
	}

protected void go6010()
	{
		clmdaddIO.setParams(SPACES);
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setLife(udtrigrec.tk3Life);
		clmdaddIO.setCoverage(udtrigrec.tk3Coverage);
		clmdaddIO.setRider(udtrigrec.tk3Rider);
		clmdaddIO.setFieldType("S");
		clmdaddIO.setVirtualFund(SPACES);
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdaddIO.getParams());
			syserrrec.statuz.set(clmdaddIO.getStatuz());
			fatalError9000();
		}
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setActvalue(clmdaddIO.getEstMatValue());
		clmdaddIO.setEstMatValue(ZERO);
		clmdaddIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clmdaddIO.getStatuz());
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
	}

protected void a100ReadHitr()
	{
		a110Hitr();
	}

protected void a110Hitr()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrcoy(udtrigrec.chdrcoy);
		hitrclmIO.setChdrnum(udtrigrec.chdrnum);
		hitrclmIO.setLife(udtrigrec.life);
		hitrclmIO.setCoverage(udtrigrec.coverage);
		hitrclmIO.setRider(udtrigrec.rider);
		hitrclmIO.setPlanSuffix(udtrigrec.planSuffix);
		hitrclmIO.setZintbfnd(udtrigrec.unitVirtualFund);
		hitrclmIO.setTranno(udtrigrec.tranno);
		hitrclmIO.setFormat(hitrclmrec);
		hitrclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hitrclmIO.getStatuz());
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
		/* Set up the UTRN fields required for posting ACMVs.*/
		wsaaContractType.set(hitrclmIO.getCnttyp());
		utrnIO.setBatccoy(hitrclmIO.getBatccoy());
		utrnIO.setBatcbrn(hitrclmIO.getBatcbrn());
		utrnIO.setBatcactyr(hitrclmIO.getBatcactyr());
		utrnIO.setBatcactmn(hitrclmIO.getBatcactmn());
		utrnIO.setBatctrcde(hitrclmIO.getBatctrcde());
		utrnIO.setBatcbatch(hitrclmIO.getBatcbatch());
		utrnIO.setTranno(hitrclmIO.getTranno());
		utrnIO.setCntcurr(hitrclmIO.getCntcurr());
		utrnIO.setMoniesDate(hitrclmIO.getEffdate());
		utrnIO.setCrtable(hitrclmIO.getCrtable());
	}

protected void a200ReadAndUpdateClaim()
	{
		a210Go();
	}

protected void a210Go()
	{
		clmdaddIO.setParams(SPACES);
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setLife(udtrigrec.tk3Life);
		clmdaddIO.setCoverage(udtrigrec.tk3Coverage);
		clmdaddIO.setRider(udtrigrec.tk3Rider);
		clmdaddIO.setFieldType("D");
		clmdaddIO.setVirtualFund(hitrclmIO.getZintbfnd());
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clmdaddIO.getStatuz());
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setEstMatValue(ZERO);
		setPrecision(clmdaddIO.getActvalue(), 2);
		clmdaddIO.setActvalue(add(clmdaddIO.getActvalue(), hitrclmIO.getContractAmount()));
		if (isGT(clmdaddIO.getActvalue(), ZERO)) {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), 1));
		}
		else {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), -1));
		}
		clmdaddIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clmdaddIO.getStatuz());
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		udtrigrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void b100ZrdecplcCall()
	{
		/*B110-ZRDECPLC-CALL*/
		zrdecplrec.company.set(udtrigrec.company);
		zrdecplrec.function.set(SPACES);
		zrdecplrec.batctrcde.set(utrnIO.getBatctrcde());
		zrdecplrec.amountOut.set(ZERO);
		zrdecplrec.statuz.set(varcom.oK);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			syserrrec.statuz.set(zrdecplrec.statuz);
			fatalError9000();
		}
		/*B190-EXIT*/
	}
}
