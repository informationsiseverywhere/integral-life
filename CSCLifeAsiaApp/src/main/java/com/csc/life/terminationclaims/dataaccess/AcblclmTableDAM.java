package com.csc.life.terminationclaims.dataaccess;

import com.csc.fsu.accounting.dataaccess.AcblpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AcblclmTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:07
 * Class transformed from ACBLCLM.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AcblclmTableDAM extends AcblpfTableDAM {

	public AcblclmTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ACBLCLM");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "RLDGCOY"
		             + ", SACSCODE"
		             + ", SACSTYP"
		             + ", RLDGACCT"
		             + ", ORIGCURR";
		
		QUALIFIEDCOLUMNS = 
		            "RLDGCOY, " +
		            "SACSCODE, " +
		            "RLDGACCT, " +
		            "SACSTYP, " +
		            "ORIGCURR, " +
		            "SACSCURBAL, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "RLDGCOY ASC, " +
		            "SACSCODE ASC, " +
		            "SACSTYP ASC, " +
		            "RLDGACCT ASC, " +
		            "ORIGCURR ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "RLDGCOY DESC, " +
		            "SACSCODE DESC, " +
		            "SACSTYP DESC, " +
		            "RLDGACCT DESC, " +
		            "ORIGCURR DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               rldgcoy,
                               sacscode,
                               rldgacct,
                               sacstyp,
                               origcurr,
                               sacscurbal,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(40);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getRldgcoy().toInternal()
					+ getSacscode().toInternal()
					+ getSacstyp().toInternal()
					+ getRldgacct().toInternal()
					+ getOrigcurr().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, rldgcoy);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, rldgacct);
			what = ExternalData.chop(what, origcurr);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(16);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(rldgcoy.toInternal());
	nonKeyFiller20.setInternal(sacscode.toInternal());
	nonKeyFiller30.setInternal(rldgacct.toInternal());
	nonKeyFiller40.setInternal(sacstyp.toInternal());
	nonKeyFiller50.setInternal(origcurr.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(79);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getSacscurbal().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, sacscurbal);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(Object what) {
		rldgcoy.set(what);
	}
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}
	public FixedLengthStringData getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(Object what) {
		rldgacct.set(what);
	}
	public FixedLengthStringData getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(Object what) {
		origcurr.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getSacscurbal() {
		return sacscurbal;
	}
	public void setSacscurbal(Object what) {
		setSacscurbal(what, false);
	}
	public void setSacscurbal(Object what, boolean rounded) {
		if (rounded)
			sacscurbal.setRounded(what);
		else
			sacscurbal.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		rldgcoy.clear();
		sacscode.clear();
		sacstyp.clear();
		rldgacct.clear();
		origcurr.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		sacscurbal.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}