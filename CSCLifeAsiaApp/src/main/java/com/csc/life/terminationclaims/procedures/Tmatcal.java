/*
 * File: Tmatcal.java
 * Date: 30 August 2009 2:38:39
 * Author: Quipoz Limited
 * 
 * Class transformed from TMATCAL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.Bonusrec;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.terminationclaims.dataaccess.CovrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrmatTableDAM;
import com.csc.life.terminationclaims.recordstructures.Matccpy;
import com.csc.life.terminationclaims.tablestructures.Tt550rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* TMATCAL - Cloned from TRDMATC.
*           Traditional Maturity Value reduction Calculation.
*           The percentage reduction is held on table TT550.
*
*   This subroutine is used as the calculation routine for
*   Traditional type component maturity values and bonuses.
*
*   It will return the following values:
*      1.  the Sum Assured of the component
*      2.  the Reversionary Bonus value.
*      3.  the Interim Bonus value.
*      4.  the Terminal Bonus value.
*      5.  the Extra Bonus value.
*
*   If the policy selected is part of a summary, then, the sum
*   assured and the bonuses must be divided by the number of
*   policies in the plan.
*
*   In the first time through it will calculate the above values:
*
*   1.  The Sum Assured of the component.
*       Sum the sum insured amounts of the component(s) COVR
*       multiply by percentage reduction (TT550-TMATPCT).
*
*   2.  The Reversionary Bonus Value.
*       It reads the ACBL and return the Reversionary bonus
*       value, by reading table T5645, the Transaction Accounting
*       Rules table.
*
*   3.  The Interim Bonus Value.
*       It calls the Interim Bonus Subroutine by reading table
*       T6639 to calculate the interim bonus. The interim bonus
*       is the sum of interim bonus from the sum assured and the
*       bonus from the interim bonus.
*
*   4.  The Terminal Bonus Value.
*       It calls the Terminal Bonus Subroutine by reading table
*       T6639 to calculate the terminal bonus. The terminal
*       bonus is the sum of terminal bonus from the sum assured
*       and the bonus from the interim bonus.
*
*   5.  The Extra Bonus Bonus.
*       It calls the Extra Bonus Subroutine by reading table
*       T6639 to calculate the extra bonus. The extra bonus is
*       the sum of extra bonus from the sum assured and the
*       bonus from the interim bonus.
*
*   6.  It returns the Sum Assured value and its description.
*       It also set the process status flag to '2', retrieve
*       the interim bonus next, and set the MATC-STATUS to
*       O-K.
*
*   NB: If Component is not eligible for maturity (ie. Effective
*       Date less than Risk Cessation Date) then set
*       MATC-STATUS to NETM.
*
*   In the second time through it will retrieve Reversionary
*   bonus value and its description. It also set the process
*   status flag to '3', retrieve the terminal bonus next, and
*   set MATC-STATUS to O-K.
*
*   In the third time through it will retrieve Interim bonus
*   value and its description. It also set the process status
*   flag to '4', retrieve the terminal bonus next, and set
*   MATC-STATUS to O-K.
*
*   In the fourth time through it will retrieve Terminal bonus
*   value and its description. It also set the process status
*   flag to '5', retrieve the extra bonus next, and set
*   MATC-STATUS to O-K.
*
*   In the fifth time through it will retrieve the Extra
*   bonus value and its description. It also set the process
*   status flag to '1', initial or first time through and set
*   MATC-STATUS to ENDP. The end of calculation.
*
*
*   SET EXIT VARIABLES.
*   ------------------
*   set linkage variables as follows :
*
*   To indicate the progress of processing :
*
*   - STATUS -        '****' - FIRST-TIME-THROUGH
*                     '****' - SECOND-TIME-THROUGH
*                     '****' - THIRD-TIME-THROUGH
*                     '****' - FOURTH-TIME-THROUGH
*                     'ENDP' - FIFTH-TIME-THROUGH
*                     'NETM' - Component not eligible for
*                              maturity.
*
*   To indicate the current pass :
*
*   - PROCESS-STATUS  '1'    - FIRST-TIME-THROUGH
*                     '2'    - SECOND-TIME-THROUGH
*                     '3'    - THIRD-TIME-THROUGH
*                     '4'    - FOURTH-TIME-THROUGH
*                     '5'    - FIFTH-TIME-THROUGH
*
*   To indicate the type of the current calculation :
*
*   - TYPE -          'S'    - FIRST-TIME-THROUGH
*                              (Sum Assured)
*                     'B'    - SECOND-TIME-THROUGH
*                              (Reversionary Bonus)
*                     'M'    - THIRD-TIME-THROUGH
*                              (Interim Bonus)
*                     'T'    - FOURTH-TIME-THROUGH
*                              (Terminal Bonus)
*                     'X'    - FIFTH-TIME-THROUGH
*                              (Extra/Addition Bonus)
*
*   The linkage copybook is MATCCPY and the following fields
*   must be initialed with valid values from the calling program:
*
*        - CHDR-CHDRCOY
*        - CHDR-CHDRNUM
*        - PLAN-SUFFIX
*        - POLSUM
*        - LIFE-LIFE
*        - LIFE-JLIFE
*        - COVR-COVERAGE
*        - COVR-RIDER
*        - CRTABLE
*        - CRRCD
*        - EFFDATE
*        - LANGUAGE
*        - CURRCODE
*        - CHDR-CURR
*        - CHDR-TYPE
*        - MAT-CALC-METH
*        - PSTATCODE
*        - BATCKEY
*        - STATUS = O-K
*        - PLAN-SWITCH
*
*   and the rest of the fields with zeroes for numeric fields
*   and blanks for character fields.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Tmatcal extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "TMATCAL";
	private PackedDecimalData wsaaSuminsTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAcblBalTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIntBonusTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTrmBonusTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaExtBonusTot = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaIntbonus = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrmbonus = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaExtbonus = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaIntbonusDesc = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaTrmbonusDesc = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaExtbonusDesc = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5645Item = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaMatureAllow = "";
	private ZonedDecimalData wsaaEligibleDate = new ZonedDecimalData(8, 0).setUnsigned();

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, "1");
	private Validator partPlan = new Validator(wsaaPlanSwitch, "2");
	private Validator summaryPartPlan = new Validator(wsaaPlanSwitch, "3");

	private FixedLengthStringData wsaaAcblKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAcblChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaAcblKey, 0);
	private FixedLengthStringData wsaaAcblLife = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 8);
	private FixedLengthStringData wsaaAcblCoverage = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 10);
	private FixedLengthStringData wsaaAcblRider = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 12);
	private FixedLengthStringData wsaaAcblPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 14);
		/* WSAA-CHDR-KEY */
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaProcessStatus = new FixedLengthStringData(1).init("1");
	private Validator firstTimeThrough = new Validator(wsaaProcessStatus, "1");
	private Validator secondTimeThrough = new Validator(wsaaProcessStatus, "2");
	private Validator thirdTimeThrough = new Validator(wsaaProcessStatus, "3");
	private Validator fourthTimeThrough = new Validator(wsaaProcessStatus, "4");
	private Validator fifthTimeThrough = new Validator(wsaaProcessStatus, "5");

	private FixedLengthStringData wsaaT6639Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaMaturityCalcMeth = new FixedLengthStringData(4).isAPartOf(wsaaT6639Key, 0);
	private FixedLengthStringData wsaaPstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT6639Key, 4);
		/* ERRORS */
	private String h034 = "H034";
	private String h134 = "H134";
	private String h135 = "H135";
	private String g408 = "G408";
	private String g244 = "G244";
	private String tl21 = "TL21";
		/* FORMATS */
	private String acblrec = "ACBLREC";
	private String covrmatrec = "COVRMATREC";
	private String covrclmrec = "COVRCLMREC";
		/* TABLES */
	private String t3695 = "T3695";
	private String t5645 = "T5645";
	private String t5679 = "T5679";
	private String t5687 = "T5687";
	private String t6639 = "T6639";
	private String t6640 = "T6640";
	private String tt550 = "TT550";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
	private Bonusrec bonusrec = new Bonusrec();
		/*Claim Coverage Record*/
	private CovrclmTableDAM covrclmIO = new CovrclmTableDAM();
		/*Maturity/Expiry logical over COVR*/
	private CovrmatTableDAM covrmatIO = new CovrmatTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Matccpy matccpy = new Matccpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T6639rec t6639rec = new T6639rec();
	private T6640rec t6640rec = new T6640rec();
	private Tt550rec tt550rec = new Tt550rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit0009, 
		exit2009, 
		readNextCovrclm2105, 
		loopRisk2152, 
		initialPrem2155, 
		loopPrem2157, 
		exit2159, 
		exit3209, 
		exit3309, 
		exit3409, 
		exit4009, 
		loopRisk4052, 
		initialPrem4055, 
		loopPrem4057, 
		exit4059, 
		exit5209, 
		exit5309, 
		exit5409, 
		seExit9090, 
		dbExit9190
	}

	public Tmatcal() {
		super();
	}

public void mainline(Object... parmArray)
	{
		matccpy.maturityRec = convertAndSetParam(matccpy.maturityRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			mainRoutine0000();
		}
		catch (GOTOException e){
		}
		finally{
			exit0009();
		}
	}

protected void mainRoutine0000()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (firstTimeThrough.isTrue()) {
			initialization1000();
			goTo(GotoLabel.exit0009);
		}
		if (wholePlan.isTrue()) {
			wholePlanChoices0010();
		}
		else {
			partPlanChoices0020();
		}
	}

protected void exit0009()
	{
		exitProgram();
	}

protected void wholePlanChoices0010()
	{
		/*CHOICES*/
		if (secondTimeThrough.isTrue()) {
			retrieveReversBonus3010();
		}
		else {
			if (thirdTimeThrough.isTrue()) {
				retrieveInterimBonus3210();
			}
			else {
				if (fourthTimeThrough.isTrue()) {
					retrieveTerminalBonus3310();
				}
				else {
					if (fifthTimeThrough.isTrue()) {
						retrieveExtraBonus3410();
					}
				}
			}
		}
		/*EXIT*/
	}

protected void partPlanChoices0020()
	{
		/*CHOICES*/
		if (secondTimeThrough.isTrue()) {
			retrieveReversBonus5010();
		}
		else {
			if (thirdTimeThrough.isTrue()) {
				retrieveInterimBonus5210();
			}
			else {
				if (fourthTimeThrough.isTrue()) {
					retrieveTerminalBonus5310();
				}
				else {
					if (fifthTimeThrough.isTrue()) {
						retrieveExtraBonus5410();
					}
				}
			}
		}
		/*EXIT*/
	}

protected void initialization1000()
	{
		init1000();
		getStatus1002();
		maturedWhat1004();
	}

protected void init1000()
	{
		wsaaChdrcoy.set(matccpy.chdrChdrcoy);
		wsaaChdrnum.set(matccpy.chdrChdrnum);
		wsaaCoverage.set(matccpy.covrCoverage);
		wsaaRider.set(matccpy.covrRider);
		wsaaLife.set(matccpy.lifeLife);
		wsaaPlanSuffix.set(matccpy.planSuffix);
		wsaaBatckey.set(matccpy.batckey);
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		wsaaSuminsTot.set(ZERO);
		wsaaAcblBalTot.set(ZERO);
		wsaaIntBonusTot.set(ZERO);
		wsaaTrmBonusTot.set(ZERO);
		wsaaExtBonusTot.set(ZERO);
		matccpy.description.set(SPACES);
		matccpy.status.set("NETM");
	}

protected void getStatus1002()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsaaChdrcoy);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void maturedWhat1004()
	{
		wsaaPlanSwitch.set(matccpy.planSwitch);
		if (summaryPartPlan.isTrue()) {
			wsaaPlanSuffix.set(ZERO);
			matccpy.planSuffix.set(ZERO);
		}
		datcon2rec.intDatex1.set(matccpy.effdate);
		datcon2rec.frequency.set("12");
		datcon2rec.freqFactor.set(3);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaEligibleDate.set(datcon2rec.intDatex2);
		if (wholePlan.isTrue()) {
			processWholePlan2000();
		}
		else {
			processPartPlan4000();
		}
	}

protected void readTableT66391500()
	{
		readT66391500();
	}

protected void readT66391500()
	{
		itdmIO.setDataKey(SPACES);
		wsaaMaturityCalcMeth.set(matccpy.matCalcMeth);
		wsaaPstatcode.set(matccpy.pstatcode);
		itdmIO.setItemcoy(matccpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6639);
		itdmIO.setItemitem(wsaaT6639Key);
		itdmIO.setItmfrm(matccpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),matccpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6639)
		|| isNE(itdmIO.getItemitem(),wsaaT6639Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT6639Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g408);
			dbError9100();
		}
		t6639rec.t6639Rec.set(itdmIO.getGenarea());
	}

protected void readTableT66401600()
	{
		readT66401600();
	}

protected void readT66401600()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(matccpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(matccpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),matccpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t6640)
		|| isNE(itdmIO.getItemitem(),wsaaCrtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaCrtable);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g244);
			dbError9100();
		}
		t6640rec.t6640Rec.set(itdmIO.getGenarea());
	}

protected void readTableT56451700()
	{
		readT56451700();
	}

protected void readT56451700()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(matccpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaT5645Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(h134);
			dbError9100();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void getDescription1750()
	{
		getT3695Descr1750();
	}

protected void getT3695Descr1750()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(matccpy.chdrChdrcoy);
		descIO.setDesctabl(t3695);
		descIO.setDescitem(t5645rec.sacstype01);
		descIO.setLanguage(matccpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError9100();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(h135);
			dbError9100();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			descIO.setShortdesc("??????????");
		}
	}

protected void processWholePlan2000()
	{
		try {
			processCalculations2000();
			checkMaturedComponent2003();
		}
		catch (GOTOException e){
		}
	}

protected void processCalculations2000()
	{
		covrclmIO.setDataArea(SPACES);
		covrclmIO.setChdrcoy(wsaaChdrcoy);
		covrclmIO.setChdrnum(wsaaChdrnum);
		covrclmIO.setLife(wsaaLife);
		covrclmIO.setCoverage(wsaaCoverage);
		covrclmIO.setRider(wsaaRider);
		covrclmIO.setFormat(covrclmrec);
		covrclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrclmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(),varcom.oK)
		&& isNE(covrclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrclmIO.getParams());
			syserrrec.statuz.set(covrclmIO.getStatuz());
			dbError9100();
		}
		if (isNE(covrclmIO.getChdrcoy(),wsaaChdrcoy)
		|| isNE(covrclmIO.getChdrnum(),wsaaChdrnum)
		|| isNE(covrclmIO.getLife(),wsaaLife)
		|| isNE(covrclmIO.getCoverage(),wsaaCoverage)
		|| isNE(covrclmIO.getRider(),wsaaRider)
		|| isEQ(covrclmIO.getStatuz(),varcom.endp)) {
			covrclmIO.setStatuz(varcom.endp);
			matccpy.status.set("NETM");
			goTo(GotoLabel.exit2009);
		}
		readTableT66391500();
		while ( !(isEQ(covrclmIO.getStatuz(),varcom.endp)
		|| isNE(covrclmIO.getChdrcoy(),wsaaChdrcoy)
		|| isNE(covrclmIO.getChdrnum(),wsaaChdrnum)
		|| isNE(covrclmIO.getLife(),wsaaLife)
		|| isNE(covrclmIO.getCoverage(),wsaaCoverage)
		|| isNE(covrclmIO.getRider(),wsaaRider))) {
			calcSumBonuses2100();
		}
		
	}

protected void checkMaturedComponent2003()
	{
		if (isEQ(matccpy.status,"NETM")) {
			wsaaProcessStatus.set("1");
			goTo(GotoLabel.exit2009);
		}
		/*GET-COMPONENT-DESC*/
		retrieveComponent2200();
	}

protected void calcSumBonuses2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					checkValidflag2100();
					calculations2102();
				}
				case readNextCovrclm2105: {
					readNextCovrclm2105();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkValidflag2100()
	{
		if (isNE(covrclmIO.getValidflag(),"1")
		|| isGT(covrclmIO.getRiskCessDate(),wsaaEligibleDate)) {
			goTo(GotoLabel.readNextCovrclm2105);
		}
		/*CHECK-STATUS*/
		checkStatus2150();
		if (isEQ(wsaaMatureAllow,"N")) {
			goTo(GotoLabel.readNextCovrclm2105);
		}
		matccpy.status.set(varcom.oK);
	}

protected void calculations2102()
	{
		wsaaSuminsTot.add(covrclmIO.getSumins());
		getRbAmt3000();
		initializeBonsPara3100();
		getInterimBonus3200();
		getTerminalBonus3300();
		getExtraBonus3400();
	}

protected void readNextCovrclm2105()
	{
		covrclmIO.setFormat(covrclmrec);
		covrclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(),varcom.oK)
		&& isNE(covrclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrclmIO.getParams());
			syserrrec.statuz.set(covrclmIO.getStatuz());
			dbError9100();
		}
		/*EXIT*/
	}

protected void checkStatus2150()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialRisk2150();
				}
				case loopRisk2152: {
					loopRisk2152();
				}
				case initialPrem2155: {
					initialPrem2155();
				}
				case loopPrem2157: {
					loopPrem2157();
				}
				case exit2159: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialRisk2150()
	{
		wsaaSub.set(ZERO);
	}

protected void loopRisk2152()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			wsaaMatureAllow = "N";
			goTo(GotoLabel.exit2159);
		}
		if (isEQ(covrclmIO.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
			wsaaMatureAllow = "Y";
			goTo(GotoLabel.initialPrem2155);
		}
		goTo(GotoLabel.loopRisk2152);
	}

protected void initialPrem2155()
	{
		wsaaSub.set(ZERO);
	}

protected void loopPrem2157()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			wsaaMatureAllow = "N";
			goTo(GotoLabel.exit2159);
		}
		if (isEQ(covrclmIO.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
			wsaaMatureAllow = "Y";
			goTo(GotoLabel.exit2159);
		}
		goTo(GotoLabel.loopPrem2157);
	}

protected void retrieveComponent2200()
	{
		getDescription2200();
		getPrctReduction2220();
	}

protected void getDescription2200()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(matccpy.chdrChdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(matccpy.crtable);
		descIO.setLanguage(matccpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError9100();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(h034);
			dbError9100();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			matccpy.description.fill("?");
		}
		else {
			matccpy.description.set(descIO.getShortdesc());
		}
	}

protected void getPrctReduction2220()
	{
		getPrctReduction2300();
		compute(wsaaSuminsTot, 2).set(div(mult(wsaaSuminsTot,tt550rec.tmatpct),100));
		matccpy.actualVal.set(wsaaSuminsTot);
		matccpy.type.set("S");
		matccpy.status.set(varcom.oK);
		wsaaProcessStatus.set("2");
		/*EXIT*/
	}

protected void getPrctReduction2300()
	{
		start2310();
	}

protected void start2310()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(matccpy.chdrChdrcoy);
		itdmIO.setItemtabl(tt550);
		itdmIO.setItemitem(matccpy.matCalcMeth);
		itdmIO.setItmfrm(matccpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),matccpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),tt550)
		|| isNE(itdmIO.getItemitem(),matccpy.matCalcMeth)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(matccpy.matCalcMeth);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(tl21);
			dbError9100();
		}
		tt550rec.tt550Rec.set(itdmIO.getGenarea());
	}

protected void getRbAmt3000()
	{
		readT56453000();
	}

protected void readT56453000()
	{
		wsaaT5645Item.set(wsaaSubr);
		readTableT56451700();
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(matccpy.chdrChdrcoy);
		acblIO.setSacscode(t5645rec.sacscode01);
		wsaaAcblChdrnum.set(matccpy.chdrChdrnum);
		wsaaAcblLife.set(matccpy.lifeLife);
		wsaaAcblCoverage.set(matccpy.covrCoverage);
		wsaaAcblRider.set(matccpy.covrRider);
		wsaaPlan.set(covrclmIO.getPlanSuffix());
		wsaaAcblPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaAcblKey);
		acblIO.setOrigcurr(matccpy.currcode);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			dbError9100();
		}
		if (isNE(acblIO.getStatuz(),varcom.mrnf)) {
			wsaaAcblBalTot.add(acblIO.getSacscurbal());
		}
	}

protected void retrieveReversBonus3010()
	{
		/*READ-T5645*/
		wsaaT5645Item.set(wsaaSubr);
		readTableT56451700();
		/*GET-DESCRIPTION*/
		getDescription1750();
		matccpy.description.set(descIO.getShortdesc());
		matccpy.actualVal.set(wsaaAcblBalTot);
		matccpy.type.set("B");
		matccpy.status.set(varcom.oK);
		wsaaProcessStatus.set("3");
		/*EXIT*/
	}

protected void initializeBonsPara3100()
	{
		setUpPara3100();
	}

protected void setUpPara3100()
	{
		wsaaCrtable.set(covrclmIO.getCrtable());
		readTableT66401600();
		bonusrec.bonusRec.set(SPACES);
		bonusrec.rvBonusSa.set(ZERO);
		bonusrec.trmBonusSa.set(ZERO);
		bonusrec.intBonusSa.set(ZERO);
		bonusrec.extBonusSa.set(ZERO);
		bonusrec.rvBonusBon.set(ZERO);
		bonusrec.trmBonusBon.set(ZERO);
		bonusrec.intBonusBon.set(ZERO);
		bonusrec.rvBalLy.set(ZERO);
		bonusrec.extBonusBon.set(ZERO);
		bonusrec.batckey.set(matccpy.batckey);
		bonusrec.transcd.set(wsaaBatckey.batcBatctrcde);
		bonusrec.effectiveDate.set(matccpy.effdate);
		if (isEQ(covrclmIO.getUnitStatementDate(),varcom.vrcmMaxDate)) {
			bonusrec.unitStmtDate.set(covrclmIO.getCrrcd());
		}
		else {
			bonusrec.unitStmtDate.set(covrclmIO.getUnitStatementDate());
		}
		bonusrec.language.set(matccpy.language);
		bonusrec.tranno.set(covrclmIO.getTranno());
		bonusrec.cntcurr.set(matccpy.chdrCurr);
		bonusrec.cnttype.set(matccpy.chdrType);
		bonusrec.chdrChdrcoy.set(covrclmIO.getChdrcoy());
		bonusrec.chdrChdrnum.set(covrclmIO.getChdrnum());
		bonusrec.lifeLife.set(covrclmIO.getLife());
		bonusrec.covrCoverage.set(covrclmIO.getCoverage());
		bonusrec.covrRider.set(covrclmIO.getRider());
		bonusrec.plnsfx.set(covrclmIO.getPlanSuffix());
		bonusrec.premStatus.set(covrclmIO.getPstatcode());
		bonusrec.crtable.set(covrclmIO.getCrtable());
		bonusrec.sumin.set(covrclmIO.getSumins());
		bonusrec.allocMethod.set("C");
		bonusrec.bonusCalcMeth.set(t6640rec.revBonusMeth);
		bonusrec.user.set(covrclmIO.getUser());
		bonusrec.bonusPeriod.set(1);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(matccpy.crrcd);
		datcon3rec.intDate2.set(matccpy.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		bonusrec.termInForce.set(datcon3rec.freqFactor);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(matccpy.crrcd);
		datcon3rec.intDate2.set(covrclmIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		bonusrec.duration.set(datcon3rec.freqFactor);
		bonusrec.statuz.set(varcom.oK);
	}

protected void getInterimBonus3200()
	{
		try {
			callIntbonusRoutine3200();
		}
		catch (GOTOException e){
		}
	}

protected void callIntbonusRoutine3200()
	{
		wsaaIntbonus.set(t6639rec.intBonusProg);
		if (isEQ(t6639rec.intBonusProg,SPACES)) {
			wsaaIntbonusDesc.set(SPACES);
			goTo(GotoLabel.exit3209);
		}
		callProgram(t6639rec.intBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaIntbonus);
			systemError9000();
		}
		compute(wsaaIntBonusTot, 2).add(add(bonusrec.intBonusSa,bonusrec.intBonusBon));
		wsaaIntbonusDesc.set(bonusrec.description);
	}

protected void retrieveInterimBonus3210()
	{
		/*MOVE-BONUS-FIELDS*/
		matccpy.description.set(wsaaIntbonusDesc);
		matccpy.actualVal.set(wsaaIntBonusTot);
		matccpy.type.set("M");
		wsaaProcessStatus.set("4");
		/*EXIT*/
	}

protected void getTerminalBonus3300()
	{
		try {
			callTrmbonusRoutine3300();
		}
		catch (GOTOException e){
		}
	}

protected void callTrmbonusRoutine3300()
	{
		wsaaTrmbonus.set(t6639rec.trmBonusProg);
		if (isEQ(t6639rec.trmBonusProg,SPACES)) {
			wsaaTrmbonusDesc.set(SPACES);
			goTo(GotoLabel.exit3309);
		}
		callProgram(t6639rec.trmBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaTrmbonus);
			systemError9000();
		}
		compute(wsaaTrmBonusTot, 2).add(add(bonusrec.trmBonusSa,bonusrec.trmBonusBon));
		wsaaTrmbonusDesc.set(bonusrec.description);
	}

protected void retrieveTerminalBonus3310()
	{
		/*GET-DESCRIPTION*/
		matccpy.description.set(wsaaTrmbonusDesc);
		matccpy.actualVal.set(wsaaTrmBonusTot);
		matccpy.type.set("T");
		matccpy.status.set(varcom.oK);
		wsaaProcessStatus.set("5");
		/*EXIT*/
	}

protected void getExtraBonus3400()
	{
		try {
			callAddbonusRoutine3400();
		}
		catch (GOTOException e){
		}
	}

protected void callAddbonusRoutine3400()
	{
		wsaaExtbonus.set(t6639rec.addBonusProg);
		if (isEQ(t6639rec.addBonusProg,SPACES)) {
			wsaaExtbonusDesc.set(SPACES);
			goTo(GotoLabel.exit3409);
		}
		callProgram(t6639rec.addBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaExtbonus);
			systemError9000();
		}
		compute(wsaaExtBonusTot, 2).add(add(bonusrec.extBonusSa,bonusrec.extBonusBon));
		wsaaExtbonusDesc.set(bonusrec.description);
	}

protected void retrieveExtraBonus3410()
	{
		/*RETRIEVE-DETAILS*/
		matccpy.description.set(wsaaExtbonusDesc);
		matccpy.actualVal.set(wsaaExtBonusTot);
		matccpy.type.set("X");
		matccpy.status.set(varcom.endp);
		wsaaProcessStatus.set("1");
		/*EXIT*/
	}

protected void processPartPlan4000()
	{
		try {
			processCalculations4000();
			checkValidflag4002();
			checkStatuz4003();
		}
		catch (GOTOException e){
		}
	}

protected void processCalculations4000()
	{
		covrmatIO.setDataArea(SPACES);
		covrmatIO.setChdrcoy(wsaaChdrcoy);
		covrmatIO.setChdrnum(wsaaChdrnum);
		covrmatIO.setPlanSuffix(wsaaPlanSuffix);
		covrmatIO.setLife(wsaaLife);
		covrmatIO.setCoverage(wsaaCoverage);
		covrmatIO.setRider(wsaaRider);
		covrmatIO.setFormat(covrmatrec);
		covrmatIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrmatIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, covrmatIO);
		if (isNE(covrmatIO.getStatuz(),varcom.oK)
		&& isNE(covrmatIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmatIO.getParams());
			dbError9100();
		}
		if (isNE(covrmatIO.getChdrcoy(),wsaaChdrcoy)
		|| isNE(covrmatIO.getChdrnum(),wsaaChdrnum)
		|| isNE(covrmatIO.getPlanSuffix(),wsaaPlanSuffix)
		|| isNE(covrmatIO.getLife(),wsaaLife)
		|| isNE(covrmatIO.getCoverage(),wsaaCoverage)
		|| isNE(covrmatIO.getRider(),wsaaRider)
		|| isEQ(covrmatIO.getStatuz(),varcom.endp)) {
			covrmatIO.setStatuz(varcom.endp);
			matccpy.status.set("NETM");
			wsaaProcessStatus.set("1");
			goTo(GotoLabel.exit4009);
		}
	}

protected void checkValidflag4002()
	{
		if (isGT(covrmatIO.getRiskCessDate(),wsaaEligibleDate)) {
			covrmatIO.setStatuz(varcom.endp);
			matccpy.status.set("NETM");
			wsaaProcessStatus.set("1");
			goTo(GotoLabel.exit4009);
		}
	}

protected void checkStatuz4003()
	{
		checkStatus4050();
		if (isEQ(wsaaMatureAllow,"N")) {
			matccpy.status.set("NETM");
			wsaaProcessStatus.set("1");
			goTo(GotoLabel.exit4009);
		}
		readTableT66391500();
		calcSumBonuses4100();
		/*GET-COMPONENT-DESC*/
		retrieveComponent4200();
	}

protected void checkStatus4050()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialRisk4050();
				}
				case loopRisk4052: {
					loopRisk4052();
				}
				case initialPrem4055: {
					initialPrem4055();
				}
				case loopPrem4057: {
					loopPrem4057();
				}
				case exit4059: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialRisk4050()
	{
		wsaaSub.set(ZERO);
	}

protected void loopRisk4052()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			wsaaMatureAllow = "N";
			goTo(GotoLabel.exit4059);
		}
		if (isEQ(covrmatIO.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
			wsaaMatureAllow = "Y";
			goTo(GotoLabel.initialPrem4055);
		}
		goTo(GotoLabel.loopRisk4052);
	}

protected void initialPrem4055()
	{
		wsaaSub.set(ZERO);
	}

protected void loopPrem4057()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			wsaaMatureAllow = "N";
			goTo(GotoLabel.exit4059);
		}
		if (isEQ(covrmatIO.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
			wsaaMatureAllow = "Y";
			goTo(GotoLabel.exit4059);
		}
		goTo(GotoLabel.loopPrem4057);
	}

protected void calcSumBonuses4100()
	{
		/*CALCULATIONS*/
		wsaaSuminsTot.add(covrmatIO.getSumins());
		getRbAmt5000();
		initializeBonsPara5100();
		getInterimBonus5200();
		getTerminalBonus5300();
		getExtraBonus5400();
		/*EXIT*/
	}

protected void retrieveComponent4200()
	{
		getDescription4200();
		getPrctReduction4220();
	}

protected void getDescription4200()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(matccpy.chdrChdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(matccpy.crtable);
		descIO.setLanguage(matccpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError9100();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(h034);
			dbError9100();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			matccpy.description.fill("?");
		}
		else {
			matccpy.description.set(descIO.getShortdesc());
		}
	}

protected void getPrctReduction4220()
	{
		getPrctReduction2300();
		compute(wsaaSuminsTot, 2).set(div(mult(wsaaSuminsTot,tt550rec.tmatpct),100));
		if (summaryPartPlan.isTrue()) {
			if (isNE(wsaaSuminsTot,ZERO)) {
				compute(wsaaSuminsTot, 3).setRounded(div(wsaaSuminsTot,matccpy.polsum));
			}
		}
		matccpy.actualVal.set(wsaaSuminsTot);
		matccpy.type.set("S");
		matccpy.status.set(varcom.oK);
		wsaaProcessStatus.set("2");
		/*EXIT*/
	}

protected void getRbAmt5000()
	{
		start5000();
	}

protected void start5000()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(matccpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(h134);
			dbError9100();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(matccpy.chdrChdrcoy);
		acblIO.setSacscode(t5645rec.sacscode01);
		wsaaAcblChdrnum.set(matccpy.chdrChdrnum);
		wsaaAcblLife.set(matccpy.lifeLife);
		wsaaAcblCoverage.set(matccpy.covrCoverage);
		wsaaAcblRider.set(matccpy.covrRider);
		wsaaPlan.set(matccpy.planSuffix);
		wsaaAcblPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaAcblKey);
		acblIO.setOrigcurr(matccpy.currcode);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			dbError9100();
		}
		if (isNE(acblIO.getStatuz(),varcom.mrnf)) {
			wsaaAcblBalTot.add(acblIO.getSacscurbal());
		}
	}

protected void retrieveReversBonus5010()
	{
		readT56455010();
		getDescription5012();
	}

protected void readT56455010()
	{
		wsaaT5645Item.set(wsaaSubr);
		readTableT56451700();
	}

protected void getDescription5012()
	{
		getDescription1750();
		matccpy.description.set(descIO.getShortdesc());
		if (summaryPartPlan.isTrue()) {
			if (isNE(wsaaAcblBalTot,ZERO)) {
				compute(wsaaAcblBalTot, 3).setRounded(div(wsaaAcblBalTot,matccpy.polsum));
			}
		}
		matccpy.actualVal.set(wsaaAcblBalTot);
		matccpy.type.set("B");
		matccpy.status.set(varcom.oK);
		wsaaProcessStatus.set("3");
		/*EXIT*/
	}

protected void initializeBonsPara5100()
	{
		setUpPara5100();
	}

protected void setUpPara5100()
	{
		wsaaCrtable.set(covrmatIO.getCrtable());
		readTableT66401600();
		wsaaBatckey.set(matccpy.batckey);
		bonusrec.bonusRec.set(SPACES);
		bonusrec.rvBonusSa.set(ZERO);
		bonusrec.trmBonusSa.set(ZERO);
		bonusrec.intBonusSa.set(ZERO);
		bonusrec.extBonusSa.set(ZERO);
		bonusrec.rvBonusBon.set(ZERO);
		bonusrec.trmBonusBon.set(ZERO);
		bonusrec.intBonusBon.set(ZERO);
		bonusrec.rvBalLy.set(ZERO);
		bonusrec.extBonusBon.set(ZERO);
		bonusrec.batckey.set(matccpy.batckey);
		bonusrec.transcd.set(wsaaBatckey.batcBatctrcde);
		bonusrec.effectiveDate.set(matccpy.effdate);
		if (isEQ(covrmatIO.getUnitStatementDate(),varcom.vrcmMaxDate)) {
			bonusrec.unitStmtDate.set(covrmatIO.getCrrcd());
		}
		else {
			bonusrec.unitStmtDate.set(covrmatIO.getUnitStatementDate());
		}
		bonusrec.language.set(matccpy.language);
		bonusrec.tranno.set(covrmatIO.getTranno());
		bonusrec.cntcurr.set(matccpy.chdrCurr);
		bonusrec.cnttype.set(matccpy.chdrType);
		bonusrec.chdrChdrcoy.set(covrmatIO.getChdrcoy());
		bonusrec.chdrChdrnum.set(covrmatIO.getChdrnum());
		bonusrec.lifeLife.set(covrmatIO.getLife());
		bonusrec.covrCoverage.set(covrmatIO.getCoverage());
		bonusrec.covrRider.set(covrmatIO.getRider());
		bonusrec.plnsfx.set(covrmatIO.getPlanSuffix());
		bonusrec.premStatus.set(covrmatIO.getPstatcode());
		bonusrec.crtable.set(covrmatIO.getCrtable());
		bonusrec.sumin.set(covrmatIO.getSumins());
		bonusrec.allocMethod.set("C");
		bonusrec.bonusCalcMeth.set(t6640rec.revBonusMeth);
		bonusrec.user.set(covrmatIO.getUser());
		bonusrec.bonusPeriod.set(1);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(matccpy.crrcd);
		datcon3rec.intDate2.set(matccpy.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		bonusrec.termInForce.set(datcon3rec.freqFactor);
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(matccpy.crrcd);
		datcon3rec.intDate2.set(covrmatIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		bonusrec.duration.set(datcon3rec.freqFactor);
	}

protected void getInterimBonus5200()
	{
		try {
			callIntbonusRoutine5200();
		}
		catch (GOTOException e){
		}
	}

protected void callIntbonusRoutine5200()
	{
		wsaaIntbonus.set(t6639rec.intBonusProg);
		if (isEQ(t6639rec.intBonusProg,SPACES)) {
			wsaaIntbonusDesc.set(SPACES);
			goTo(GotoLabel.exit5209);
		}
		callProgram(t6639rec.intBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaIntbonus);
			systemError9000();
		}
		compute(wsaaIntBonusTot, 2).add(add(bonusrec.intBonusSa,bonusrec.intBonusBon));
		wsaaIntbonusDesc.set(bonusrec.description);
	}

protected void retrieveInterimBonus5210()
	{
		/*RETRIEVE-DETAILS*/
		if (summaryPartPlan.isTrue()) {
			if (isNE(wsaaIntBonusTot,ZERO)) {
				compute(wsaaIntBonusTot, 3).setRounded(div(wsaaIntBonusTot,matccpy.polsum));
			}
		}
		matccpy.description.set(wsaaIntbonusDesc);
		matccpy.actualVal.set(wsaaIntBonusTot);
		matccpy.type.set("M");
		matccpy.status.set(varcom.oK);
		wsaaProcessStatus.set("4");
		/*EXIT*/
	}

protected void getTerminalBonus5300()
	{
		try {
			callTrmbonusRoutine5300();
		}
		catch (GOTOException e){
		}
	}

protected void callTrmbonusRoutine5300()
	{
		wsaaTrmbonus.set(t6639rec.trmBonusProg);
		if (isEQ(t6639rec.trmBonusProg,SPACES)) {
			wsaaTrmbonusDesc.set(SPACES);
			goTo(GotoLabel.exit5309);
		}
		callProgram(t6639rec.trmBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaTrmbonus);
			systemError9000();
		}
		compute(wsaaTrmBonusTot, 2).add(add(bonusrec.trmBonusSa,bonusrec.trmBonusBon));
		wsaaTrmbonusDesc.set(bonusrec.description);
	}

protected void retrieveTerminalBonus5310()
	{
		/*RETRIEVE-DETAILS*/
		if (summaryPartPlan.isTrue()) {
			if (isNE(wsaaTrmBonusTot,ZERO)) {
				compute(wsaaTrmBonusTot, 3).setRounded(div(wsaaTrmBonusTot,matccpy.polsum));
			}
		}
		matccpy.description.set(wsaaTrmbonusDesc);
		matccpy.actualVal.set(wsaaTrmBonusTot);
		matccpy.type.set("T");
		matccpy.status.set(varcom.oK);
		wsaaProcessStatus.set("5");
		/*EXIT*/
	}

protected void getExtraBonus5400()
	{
		try {
			callAddbonusRoutine5400();
		}
		catch (GOTOException e){
		}
	}

protected void callAddbonusRoutine5400()
	{
		wsaaExtbonus.set(t6639rec.addBonusProg);
		if (isEQ(t6639rec.addBonusProg,SPACES)) {
			wsaaExtbonusDesc.set(SPACES);
			goTo(GotoLabel.exit5409);
		}
		callProgram(t6639rec.addBonusProg, bonusrec.bonusRec);
		if (isEQ(bonusrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(bonusrec.statuz);
			syserrrec.subrname.set(wsaaExtbonus);
			systemError9000();
		}
		compute(wsaaExtBonusTot, 2).add(add(bonusrec.extBonusSa,bonusrec.extBonusBon));
		wsaaExtbonusDesc.set(bonusrec.description);
	}

protected void retrieveExtraBonus5410()
	{
		/*RETRIEVE-DETAILS*/
		if (summaryPartPlan.isTrue()) {
			if (isNE(wsaaExtBonusTot,ZERO)) {
				compute(wsaaExtBonusTot, 3).setRounded(div(wsaaExtBonusTot,matccpy.polsum));
			}
		}
		matccpy.description.set(wsaaExtbonusDesc);
		matccpy.actualVal.set(wsaaExtBonusTot);
		matccpy.type.set("X");
		matccpy.status.set(varcom.endp);
		wsaaProcessStatus.set("1");
		/*EXIT*/
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		matccpy.status.set(varcom.bomb);
		/*EXIT*/
		exit0009();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		matccpy.status.set(varcom.bomb);
		/*EXIT*/
		exit0009();
	}
}
