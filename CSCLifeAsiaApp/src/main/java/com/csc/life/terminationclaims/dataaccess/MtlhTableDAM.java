package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MtlhTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:47
 * Class transformed from MTLH.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MtlhTableDAM extends MtlhpfTableDAM {

	public MtlhTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("MTLH");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "COWNCOY, " +
		            "COWNNUM, " +
		            "TRANDATE, " +
		            "TRTM, " +
		            "JLIFE, " +
		            "SALUT, " +
		            "SURNAME, " +
		            "GIVNAME, " +
		            "INITIALS, " +
		            "CLTADDR01, " +
		            "CLTADDR02, " +
		            "CLTADDR03, " +
		            "CLTADDR04, " +
		            "CLTADDR05, " +
		            "CLTPCODE, " +
		            "CRTABLE01, " +
		            "CRTABLE02, " +
		            "CRTABLE03, " +
		            "CRTABLE04, " +
		            "CRTABLE05, " +
		            "CRTABLE06, " +
		            "CRTABLE07, " +
		            "CRTABLE08, " +
		            "CRTABLE09, " +
		            "CRTABLE10, " +
		            "CRTABLE11, " +
		            "CRTABLE12, " +
		            "CRTABLE13, " +
		            "CRTABLE14, " +
		            "CRTABLE15, " +
		            "CRTABLE16, " +
		            "CRTABLE17, " +
		            "CRTABLE18, " +
		            "CRTABLE19, " +
		            "CRTABLE20, " +
		            "BONUSAMT01, " +
		            "BONUSAMT02, " +
		            "BONUSAMT03, " +
		            "BONUSAMT04, " +
		            "BONUSAMT05, " +
		            "BONUSAMT06, " +
		            "BONUSAMT07, " +
		            "BONUSAMT08, " +
		            "BONUSAMT09, " +
		            "BONUSAMT10, " +
		            "BONUSAMT11, " +
		            "BONUSAMT12, " +
		            "BONUSAMT13, " +
		            "BONUSAMT14, " +
		            "BONUSAMT15, " +
		            "BONUSAMT16, " +
		            "BONUSAMT17, " +
		            "BONUSAMT18, " +
		            "BONUSAMT19, " +
		            "BONUSAMT20, " +
		            "BONUSAMT21, " +
		            "SUMINS01, " +
		            "SUMINS02, " +
		            "SUMINS03, " +
		            "SUMINS04, " +
		            "SUMINS05, " +
		            "SUMINS06, " +
		            "SUMINS07, " +
		            "SUMINS08, " +
		            "SUMINS09, " +
		            "SUMINS10, " +
		            "SUMINS11, " +
		            "SUMINS12, " +
		            "SUMINS13, " +
		            "SUMINS14, " +
		            "SUMINS15, " +
		            "SUMINS16, " +
		            "SUMINS17, " +
		            "SUMINS18, " +
		            "SUMINS19, " +
		            "SUMINS20, " +
		            "SUMINS21, " +
		            "MATAMT01, " +
		            "MATAMT02, " +
		            "MATAMT03, " +
		            "MATAMT04, " +
		            "MATAMT05, " +
		            "MATAMT06, " +
		            "MATAMT07, " +
		            "MATAMT08, " +
		            "MATAMT09, " +
		            "MATAMT10, " +
		            "MATAMT11, " +
		            "MATAMT12, " +
		            "MATAMT13, " +
		            "MATAMT14, " +
		            "MATAMT15, " +
		            "MATAMT16, " +
		            "MATAMT17, " +
		            "MATAMT18, " +
		            "MATAMT19, " +
		            "MATAMT20, " +
		            "MATAMT21, " +
		            "VRTFUND01, " +
		            "VRTFUND02, " +
		            "VRTFUND03, " +
		            "VRTFUND04, " +
		            "VRTFUND05, " +
		            "VRTFUND06, " +
		            "VRTFUND07, " +
		            "VRTFUND08, " +
		            "VRTFUND09, " +
		            "VRTFUND10, " +
		            "VRTFUND11, " +
		            "VRTFUND12, " +
		            "VRTFUND13, " +
		            "VRTFUND14, " +
		            "VRTFUND15, " +
		            "VRTFUND16, " +
		            "VRTFUND17, " +
		            "VRTFUND18, " +
		            "VRTFUND19, " +
		            "VRTFUND20, " +
		            "TYPE01, " +
		            "TYPE02, " +
		            "TYPE03, " +
		            "TYPE04, " +
		            "TYPE05, " +
		            "TYPE06, " +
		            "TYPE07, " +
		            "TYPE08, " +
		            "TYPE09, " +
		            "TYPE10, " +
		            "TYPE11, " +
		            "TYPE12, " +
		            "TYPE13, " +
		            "TYPE14, " +
		            "TYPE15, " +
		            "TYPE16, " +
		            "TYPE17, " +
		            "TYPE18, " +
		            "TYPE19, " +
		            "TYPE20, " +
		            "DESCRIP01, " +
		            "DESCRIP02, " +
		            "DESCRIP03, " +
		            "DESCRIP04, " +
		            "DESCRIP05, " +
		            "DESCRIP06, " +
		            "DESCRIP07, " +
		            "DESCRIP08, " +
		            "DESCRIP09, " +
		            "DESCRIP10, " +
		            "DESCRIP11, " +
		            "DESCRIP12, " +
		            "DESCRIP13, " +
		            "DESCRIP14, " +
		            "DESCRIP15, " +
		            "DESCRIP16, " +
		            "DESCRIP17, " +
		            "DESCRIP18, " +
		            "DESCRIP19, " +
		            "DESCRIP20, " +
		            "CURRCD01, " +
		            "CURRCD02, " +
		            "CURRCD03, " +
		            "CURRCD04, " +
		            "CURRCD05, " +
		            "CURRCD06, " +
		            "CURRCD07, " +
		            "CURRCD08, " +
		            "CURRCD09, " +
		            "CURRCD10, " +
		            "CURRCD11, " +
		            "CURRCD12, " +
		            "CURRCD13, " +
		            "CURRCD14, " +
		            "CURRCD15, " +
		            "CURRCD16, " +
		            "CURRCD17, " +
		            "CURRCD18, " +
		            "CURRCD19, " +
		            "CURRCD20, " +
		            "RCESDT01, " +
		            "RCESDT02, " +
		            "RCESDT03, " +
		            "RCESDT04, " +
		            "RCESDT05, " +
		            "RCESDT06, " +
		            "RCESDT07, " +
		            "RCESDT08, " +
		            "RCESDT09, " +
		            "RCESDT10, " +
		            "RCESDT11, " +
		            "RCESDT12, " +
		            "RCESDT13, " +
		            "RCESDT14, " +
		            "RCESDT15, " +
		            "RCESDT16, " +
		            "RCESDT17, " +
		            "RCESDT18, " +
		            "RCESDT19, " +
		            "RCESDT20, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               cowncoy,
                               cownnum,
                               trandate,
                               transactionTime,
                               jlife,
                               salut,
                               surname,
                               givname,
                               initials,
                               cltaddr01,
                               cltaddr02,
                               cltaddr03,
                               cltaddr04,
                               cltaddr05,
                               cltpcode,
                               crtable01,
                               crtable02,
                               crtable03,
                               crtable04,
                               crtable05,
                               crtable06,
                               crtable07,
                               crtable08,
                               crtable09,
                               crtable10,
                               crtable11,
                               crtable12,
                               crtable13,
                               crtable14,
                               crtable15,
                               crtable16,
                               crtable17,
                               crtable18,
                               crtable19,
                               crtable20,
                               bonusAmt01,
                               bonusAmt02,
                               bonusAmt03,
                               bonusAmt04,
                               bonusAmt05,
                               bonusAmt06,
                               bonusAmt07,
                               bonusAmt08,
                               bonusAmt09,
                               bonusAmt10,
                               bonusAmt11,
                               bonusAmt12,
                               bonusAmt13,
                               bonusAmt14,
                               bonusAmt15,
                               bonusAmt16,
                               bonusAmt17,
                               bonusAmt18,
                               bonusAmt19,
                               bonusAmt20,
                               bonusAmt21,
                               sumins01,
                               sumins02,
                               sumins03,
                               sumins04,
                               sumins05,
                               sumins06,
                               sumins07,
                               sumins08,
                               sumins09,
                               sumins10,
                               sumins11,
                               sumins12,
                               sumins13,
                               sumins14,
                               sumins15,
                               sumins16,
                               sumins17,
                               sumins18,
                               sumins19,
                               sumins20,
                               sumins21,
                               matamt01,
                               matamt02,
                               matamt03,
                               matamt04,
                               matamt05,
                               matamt06,
                               matamt07,
                               matamt08,
                               matamt09,
                               matamt10,
                               matamt11,
                               matamt12,
                               matamt13,
                               matamt14,
                               matamt15,
                               matamt16,
                               matamt17,
                               matamt18,
                               matamt19,
                               matamt20,
                               matamt21,
                               virtualFund01,
                               virtualFund02,
                               virtualFund03,
                               virtualFund04,
                               virtualFund05,
                               virtualFund06,
                               virtualFund07,
                               virtualFund08,
                               virtualFund09,
                               virtualFund10,
                               virtualFund11,
                               virtualFund12,
                               virtualFund13,
                               virtualFund14,
                               virtualFund15,
                               virtualFund16,
                               virtualFund17,
                               virtualFund18,
                               virtualFund19,
                               virtualFund20,
                               fieldType01,
                               fieldType02,
                               fieldType03,
                               fieldType04,
                               fieldType05,
                               fieldType06,
                               fieldType07,
                               fieldType08,
                               fieldType09,
                               fieldType10,
                               fieldType11,
                               fieldType12,
                               fieldType13,
                               fieldType14,
                               fieldType15,
                               fieldType16,
                               fieldType17,
                               fieldType18,
                               fieldType19,
                               fieldType20,
                               descrip01,
                               descrip02,
                               descrip03,
                               descrip04,
                               descrip05,
                               descrip06,
                               descrip07,
                               descrip08,
                               descrip09,
                               descrip10,
                               descrip11,
                               descrip12,
                               descrip13,
                               descrip14,
                               descrip15,
                               descrip16,
                               descrip17,
                               descrip18,
                               descrip19,
                               descrip20,
                               currcd01,
                               currcd02,
                               currcd03,
                               currcd04,
                               currcd05,
                               currcd06,
                               currcd07,
                               currcd08,
                               currcd09,
                               currcd10,
                               currcd11,
                               currcd12,
                               currcd13,
                               currcd14,
                               currcd15,
                               currcd16,
                               currcd17,
                               currcd18,
                               currcd19,
                               currcd20,
                               rcesdt01,
                               rcesdt02,
                               rcesdt03,
                               rcesdt04,
                               rcesdt05,
                               rcesdt06,
                               rcesdt07,
                               rcesdt08,
                               rcesdt09,
                               rcesdt10,
                               rcesdt11,
                               rcesdt12,
                               rcesdt13,
                               rcesdt14,
                               rcesdt15,
                               rcesdt16,
                               rcesdt17,
                               rcesdt18,
                               rcesdt19,
                               rcesdt20,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(1750+DD.cltaddr.length*5);//pmujavadiya
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getCowncoy().toInternal()
					+ getCownnum().toInternal()
					+ getTrandate().toInternal()
					+ getTransactionTime().toInternal()
					+ getJlife().toInternal()
					+ getSalut().toInternal()
					+ getSurname().toInternal()
					+ getGivname().toInternal()
					+ getInitials().toInternal()
					+ getCltaddr01().toInternal()
					+ getCltaddr02().toInternal()
					+ getCltaddr03().toInternal()
					+ getCltaddr04().toInternal()
					+ getCltaddr05().toInternal()
					+ getCltpcode().toInternal()
					+ getCrtable01().toInternal()
					+ getCrtable02().toInternal()
					+ getCrtable03().toInternal()
					+ getCrtable04().toInternal()
					+ getCrtable05().toInternal()
					+ getCrtable06().toInternal()
					+ getCrtable07().toInternal()
					+ getCrtable08().toInternal()
					+ getCrtable09().toInternal()
					+ getCrtable10().toInternal()
					+ getCrtable11().toInternal()
					+ getCrtable12().toInternal()
					+ getCrtable13().toInternal()
					+ getCrtable14().toInternal()
					+ getCrtable15().toInternal()
					+ getCrtable16().toInternal()
					+ getCrtable17().toInternal()
					+ getCrtable18().toInternal()
					+ getCrtable19().toInternal()
					+ getCrtable20().toInternal()
					+ getBonusAmt01().toInternal()
					+ getBonusAmt02().toInternal()
					+ getBonusAmt03().toInternal()
					+ getBonusAmt04().toInternal()
					+ getBonusAmt05().toInternal()
					+ getBonusAmt06().toInternal()
					+ getBonusAmt07().toInternal()
					+ getBonusAmt08().toInternal()
					+ getBonusAmt09().toInternal()
					+ getBonusAmt10().toInternal()
					+ getBonusAmt11().toInternal()
					+ getBonusAmt12().toInternal()
					+ getBonusAmt13().toInternal()
					+ getBonusAmt14().toInternal()
					+ getBonusAmt15().toInternal()
					+ getBonusAmt16().toInternal()
					+ getBonusAmt17().toInternal()
					+ getBonusAmt18().toInternal()
					+ getBonusAmt19().toInternal()
					+ getBonusAmt20().toInternal()
					+ getBonusAmt21().toInternal()
					+ getSumins01().toInternal()
					+ getSumins02().toInternal()
					+ getSumins03().toInternal()
					+ getSumins04().toInternal()
					+ getSumins05().toInternal()
					+ getSumins06().toInternal()
					+ getSumins07().toInternal()
					+ getSumins08().toInternal()
					+ getSumins09().toInternal()
					+ getSumins10().toInternal()
					+ getSumins11().toInternal()
					+ getSumins12().toInternal()
					+ getSumins13().toInternal()
					+ getSumins14().toInternal()
					+ getSumins15().toInternal()
					+ getSumins16().toInternal()
					+ getSumins17().toInternal()
					+ getSumins18().toInternal()
					+ getSumins19().toInternal()
					+ getSumins20().toInternal()
					+ getSumins21().toInternal()
					+ getMatamt01().toInternal()
					+ getMatamt02().toInternal()
					+ getMatamt03().toInternal()
					+ getMatamt04().toInternal()
					+ getMatamt05().toInternal()
					+ getMatamt06().toInternal()
					+ getMatamt07().toInternal()
					+ getMatamt08().toInternal()
					+ getMatamt09().toInternal()
					+ getMatamt10().toInternal()
					+ getMatamt11().toInternal()
					+ getMatamt12().toInternal()
					+ getMatamt13().toInternal()
					+ getMatamt14().toInternal()
					+ getMatamt15().toInternal()
					+ getMatamt16().toInternal()
					+ getMatamt17().toInternal()
					+ getMatamt18().toInternal()
					+ getMatamt19().toInternal()
					+ getMatamt20().toInternal()
					+ getMatamt21().toInternal()
					+ getVirtualFund01().toInternal()
					+ getVirtualFund02().toInternal()
					+ getVirtualFund03().toInternal()
					+ getVirtualFund04().toInternal()
					+ getVirtualFund05().toInternal()
					+ getVirtualFund06().toInternal()
					+ getVirtualFund07().toInternal()
					+ getVirtualFund08().toInternal()
					+ getVirtualFund09().toInternal()
					+ getVirtualFund10().toInternal()
					+ getVirtualFund11().toInternal()
					+ getVirtualFund12().toInternal()
					+ getVirtualFund13().toInternal()
					+ getVirtualFund14().toInternal()
					+ getVirtualFund15().toInternal()
					+ getVirtualFund16().toInternal()
					+ getVirtualFund17().toInternal()
					+ getVirtualFund18().toInternal()
					+ getVirtualFund19().toInternal()
					+ getVirtualFund20().toInternal()
					+ getFieldType01().toInternal()
					+ getFieldType02().toInternal()
					+ getFieldType03().toInternal()
					+ getFieldType04().toInternal()
					+ getFieldType05().toInternal()
					+ getFieldType06().toInternal()
					+ getFieldType07().toInternal()
					+ getFieldType08().toInternal()
					+ getFieldType09().toInternal()
					+ getFieldType10().toInternal()
					+ getFieldType11().toInternal()
					+ getFieldType12().toInternal()
					+ getFieldType13().toInternal()
					+ getFieldType14().toInternal()
					+ getFieldType15().toInternal()
					+ getFieldType16().toInternal()
					+ getFieldType17().toInternal()
					+ getFieldType18().toInternal()
					+ getFieldType19().toInternal()
					+ getFieldType20().toInternal()
					+ getDescrip01().toInternal()
					+ getDescrip02().toInternal()
					+ getDescrip03().toInternal()
					+ getDescrip04().toInternal()
					+ getDescrip05().toInternal()
					+ getDescrip06().toInternal()
					+ getDescrip07().toInternal()
					+ getDescrip08().toInternal()
					+ getDescrip09().toInternal()
					+ getDescrip10().toInternal()
					+ getDescrip11().toInternal()
					+ getDescrip12().toInternal()
					+ getDescrip13().toInternal()
					+ getDescrip14().toInternal()
					+ getDescrip15().toInternal()
					+ getDescrip16().toInternal()
					+ getDescrip17().toInternal()
					+ getDescrip18().toInternal()
					+ getDescrip19().toInternal()
					+ getDescrip20().toInternal()
					+ getCurrcd01().toInternal()
					+ getCurrcd02().toInternal()
					+ getCurrcd03().toInternal()
					+ getCurrcd04().toInternal()
					+ getCurrcd05().toInternal()
					+ getCurrcd06().toInternal()
					+ getCurrcd07().toInternal()
					+ getCurrcd08().toInternal()
					+ getCurrcd09().toInternal()
					+ getCurrcd10().toInternal()
					+ getCurrcd11().toInternal()
					+ getCurrcd12().toInternal()
					+ getCurrcd13().toInternal()
					+ getCurrcd14().toInternal()
					+ getCurrcd15().toInternal()
					+ getCurrcd16().toInternal()
					+ getCurrcd17().toInternal()
					+ getCurrcd18().toInternal()
					+ getCurrcd19().toInternal()
					+ getCurrcd20().toInternal()
					+ getRcesdt01().toInternal()
					+ getRcesdt02().toInternal()
					+ getRcesdt03().toInternal()
					+ getRcesdt04().toInternal()
					+ getRcesdt05().toInternal()
					+ getRcesdt06().toInternal()
					+ getRcesdt07().toInternal()
					+ getRcesdt08().toInternal()
					+ getRcesdt09().toInternal()
					+ getRcesdt10().toInternal()
					+ getRcesdt11().toInternal()
					+ getRcesdt12().toInternal()
					+ getRcesdt13().toInternal()
					+ getRcesdt14().toInternal()
					+ getRcesdt15().toInternal()
					+ getRcesdt16().toInternal()
					+ getRcesdt17().toInternal()
					+ getRcesdt18().toInternal()
					+ getRcesdt19().toInternal()
					+ getRcesdt20().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, cowncoy);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, trandate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, salut);
			what = ExternalData.chop(what, surname);
			what = ExternalData.chop(what, givname);
			what = ExternalData.chop(what, initials);
			what = ExternalData.chop(what, cltaddr01);
			what = ExternalData.chop(what, cltaddr02);
			what = ExternalData.chop(what, cltaddr03);
			what = ExternalData.chop(what, cltaddr04);
			what = ExternalData.chop(what, cltaddr05);
			what = ExternalData.chop(what, cltpcode);
			what = ExternalData.chop(what, crtable01);
			what = ExternalData.chop(what, crtable02);
			what = ExternalData.chop(what, crtable03);
			what = ExternalData.chop(what, crtable04);
			what = ExternalData.chop(what, crtable05);
			what = ExternalData.chop(what, crtable06);
			what = ExternalData.chop(what, crtable07);
			what = ExternalData.chop(what, crtable08);
			what = ExternalData.chop(what, crtable09);
			what = ExternalData.chop(what, crtable10);
			what = ExternalData.chop(what, crtable11);
			what = ExternalData.chop(what, crtable12);
			what = ExternalData.chop(what, crtable13);
			what = ExternalData.chop(what, crtable14);
			what = ExternalData.chop(what, crtable15);
			what = ExternalData.chop(what, crtable16);
			what = ExternalData.chop(what, crtable17);
			what = ExternalData.chop(what, crtable18);
			what = ExternalData.chop(what, crtable19);
			what = ExternalData.chop(what, crtable20);
			what = ExternalData.chop(what, bonusAmt01);
			what = ExternalData.chop(what, bonusAmt02);
			what = ExternalData.chop(what, bonusAmt03);
			what = ExternalData.chop(what, bonusAmt04);
			what = ExternalData.chop(what, bonusAmt05);
			what = ExternalData.chop(what, bonusAmt06);
			what = ExternalData.chop(what, bonusAmt07);
			what = ExternalData.chop(what, bonusAmt08);
			what = ExternalData.chop(what, bonusAmt09);
			what = ExternalData.chop(what, bonusAmt10);
			what = ExternalData.chop(what, bonusAmt11);
			what = ExternalData.chop(what, bonusAmt12);
			what = ExternalData.chop(what, bonusAmt13);
			what = ExternalData.chop(what, bonusAmt14);
			what = ExternalData.chop(what, bonusAmt15);
			what = ExternalData.chop(what, bonusAmt16);
			what = ExternalData.chop(what, bonusAmt17);
			what = ExternalData.chop(what, bonusAmt18);
			what = ExternalData.chop(what, bonusAmt19);
			what = ExternalData.chop(what, bonusAmt20);
			what = ExternalData.chop(what, bonusAmt21);
			what = ExternalData.chop(what, sumins01);
			what = ExternalData.chop(what, sumins02);
			what = ExternalData.chop(what, sumins03);
			what = ExternalData.chop(what, sumins04);
			what = ExternalData.chop(what, sumins05);
			what = ExternalData.chop(what, sumins06);
			what = ExternalData.chop(what, sumins07);
			what = ExternalData.chop(what, sumins08);
			what = ExternalData.chop(what, sumins09);
			what = ExternalData.chop(what, sumins10);
			what = ExternalData.chop(what, sumins11);
			what = ExternalData.chop(what, sumins12);
			what = ExternalData.chop(what, sumins13);
			what = ExternalData.chop(what, sumins14);
			what = ExternalData.chop(what, sumins15);
			what = ExternalData.chop(what, sumins16);
			what = ExternalData.chop(what, sumins17);
			what = ExternalData.chop(what, sumins18);
			what = ExternalData.chop(what, sumins19);
			what = ExternalData.chop(what, sumins20);
			what = ExternalData.chop(what, sumins21);
			what = ExternalData.chop(what, matamt01);
			what = ExternalData.chop(what, matamt02);
			what = ExternalData.chop(what, matamt03);
			what = ExternalData.chop(what, matamt04);
			what = ExternalData.chop(what, matamt05);
			what = ExternalData.chop(what, matamt06);
			what = ExternalData.chop(what, matamt07);
			what = ExternalData.chop(what, matamt08);
			what = ExternalData.chop(what, matamt09);
			what = ExternalData.chop(what, matamt10);
			what = ExternalData.chop(what, matamt11);
			what = ExternalData.chop(what, matamt12);
			what = ExternalData.chop(what, matamt13);
			what = ExternalData.chop(what, matamt14);
			what = ExternalData.chop(what, matamt15);
			what = ExternalData.chop(what, matamt16);
			what = ExternalData.chop(what, matamt17);
			what = ExternalData.chop(what, matamt18);
			what = ExternalData.chop(what, matamt19);
			what = ExternalData.chop(what, matamt20);
			what = ExternalData.chop(what, matamt21);
			what = ExternalData.chop(what, virtualFund01);
			what = ExternalData.chop(what, virtualFund02);
			what = ExternalData.chop(what, virtualFund03);
			what = ExternalData.chop(what, virtualFund04);
			what = ExternalData.chop(what, virtualFund05);
			what = ExternalData.chop(what, virtualFund06);
			what = ExternalData.chop(what, virtualFund07);
			what = ExternalData.chop(what, virtualFund08);
			what = ExternalData.chop(what, virtualFund09);
			what = ExternalData.chop(what, virtualFund10);
			what = ExternalData.chop(what, virtualFund11);
			what = ExternalData.chop(what, virtualFund12);
			what = ExternalData.chop(what, virtualFund13);
			what = ExternalData.chop(what, virtualFund14);
			what = ExternalData.chop(what, virtualFund15);
			what = ExternalData.chop(what, virtualFund16);
			what = ExternalData.chop(what, virtualFund17);
			what = ExternalData.chop(what, virtualFund18);
			what = ExternalData.chop(what, virtualFund19);
			what = ExternalData.chop(what, virtualFund20);
			what = ExternalData.chop(what, fieldType01);
			what = ExternalData.chop(what, fieldType02);
			what = ExternalData.chop(what, fieldType03);
			what = ExternalData.chop(what, fieldType04);
			what = ExternalData.chop(what, fieldType05);
			what = ExternalData.chop(what, fieldType06);
			what = ExternalData.chop(what, fieldType07);
			what = ExternalData.chop(what, fieldType08);
			what = ExternalData.chop(what, fieldType09);
			what = ExternalData.chop(what, fieldType10);
			what = ExternalData.chop(what, fieldType11);
			what = ExternalData.chop(what, fieldType12);
			what = ExternalData.chop(what, fieldType13);
			what = ExternalData.chop(what, fieldType14);
			what = ExternalData.chop(what, fieldType15);
			what = ExternalData.chop(what, fieldType16);
			what = ExternalData.chop(what, fieldType17);
			what = ExternalData.chop(what, fieldType18);
			what = ExternalData.chop(what, fieldType19);
			what = ExternalData.chop(what, fieldType20);
			what = ExternalData.chop(what, descrip01);
			what = ExternalData.chop(what, descrip02);
			what = ExternalData.chop(what, descrip03);
			what = ExternalData.chop(what, descrip04);
			what = ExternalData.chop(what, descrip05);
			what = ExternalData.chop(what, descrip06);
			what = ExternalData.chop(what, descrip07);
			what = ExternalData.chop(what, descrip08);
			what = ExternalData.chop(what, descrip09);
			what = ExternalData.chop(what, descrip10);
			what = ExternalData.chop(what, descrip11);
			what = ExternalData.chop(what, descrip12);
			what = ExternalData.chop(what, descrip13);
			what = ExternalData.chop(what, descrip14);
			what = ExternalData.chop(what, descrip15);
			what = ExternalData.chop(what, descrip16);
			what = ExternalData.chop(what, descrip17);
			what = ExternalData.chop(what, descrip18);
			what = ExternalData.chop(what, descrip19);
			what = ExternalData.chop(what, descrip20);
			what = ExternalData.chop(what, currcd01);
			what = ExternalData.chop(what, currcd02);
			what = ExternalData.chop(what, currcd03);
			what = ExternalData.chop(what, currcd04);
			what = ExternalData.chop(what, currcd05);
			what = ExternalData.chop(what, currcd06);
			what = ExternalData.chop(what, currcd07);
			what = ExternalData.chop(what, currcd08);
			what = ExternalData.chop(what, currcd09);
			what = ExternalData.chop(what, currcd10);
			what = ExternalData.chop(what, currcd11);
			what = ExternalData.chop(what, currcd12);
			what = ExternalData.chop(what, currcd13);
			what = ExternalData.chop(what, currcd14);
			what = ExternalData.chop(what, currcd15);
			what = ExternalData.chop(what, currcd16);
			what = ExternalData.chop(what, currcd17);
			what = ExternalData.chop(what, currcd18);
			what = ExternalData.chop(what, currcd19);
			what = ExternalData.chop(what, currcd20);
			what = ExternalData.chop(what, rcesdt01);
			what = ExternalData.chop(what, rcesdt02);
			what = ExternalData.chop(what, rcesdt03);
			what = ExternalData.chop(what, rcesdt04);
			what = ExternalData.chop(what, rcesdt05);
			what = ExternalData.chop(what, rcesdt06);
			what = ExternalData.chop(what, rcesdt07);
			what = ExternalData.chop(what, rcesdt08);
			what = ExternalData.chop(what, rcesdt09);
			what = ExternalData.chop(what, rcesdt10);
			what = ExternalData.chop(what, rcesdt11);
			what = ExternalData.chop(what, rcesdt12);
			what = ExternalData.chop(what, rcesdt13);
			what = ExternalData.chop(what, rcesdt14);
			what = ExternalData.chop(what, rcesdt15);
			what = ExternalData.chop(what, rcesdt16);
			what = ExternalData.chop(what, rcesdt17);
			what = ExternalData.chop(what, rcesdt18);
			what = ExternalData.chop(what, rcesdt19);
			what = ExternalData.chop(what, rcesdt20);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(Object what) {
		cowncoy.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public PackedDecimalData getTrandate() {
		return trandate;
	}
	public void setTrandate(Object what) {
		setTrandate(what, false);
	}
	public void setTrandate(Object what, boolean rounded) {
		if (rounded)
			trandate.setRounded(what);
		else
			trandate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getSalut() {
		return salut;
	}
	public void setSalut(Object what) {
		salut.set(what);
	}	
	public FixedLengthStringData getSurname() {
		return surname;
	}
	public void setSurname(Object what) {
		surname.set(what);
	}	
	public FixedLengthStringData getGivname() {
		return givname;
	}
	public void setGivname(Object what) {
		givname.set(what);
	}	
	public FixedLengthStringData getInitials() {
		return initials;
	}
	public void setInitials(Object what) {
		initials.set(what);
	}	
	public FixedLengthStringData getCltaddr01() {
		return cltaddr01;
	}
	public void setCltaddr01(Object what) {
		cltaddr01.set(what);
	}	
	public FixedLengthStringData getCltaddr02() {
		return cltaddr02;
	}
	public void setCltaddr02(Object what) {
		cltaddr02.set(what);
	}	
	public FixedLengthStringData getCltaddr03() {
		return cltaddr03;
	}
	public void setCltaddr03(Object what) {
		cltaddr03.set(what);
	}	
	public FixedLengthStringData getCltaddr04() {
		return cltaddr04;
	}
	public void setCltaddr04(Object what) {
		cltaddr04.set(what);
	}	
	public FixedLengthStringData getCltaddr05() {
		return cltaddr05;
	}
	public void setCltaddr05(Object what) {
		cltaddr05.set(what);
	}	
	public FixedLengthStringData getCltpcode() {
		return cltpcode;
	}
	public void setCltpcode(Object what) {
		cltpcode.set(what);
	}	
	public FixedLengthStringData getCrtable01() {
		return crtable01;
	}
	public void setCrtable01(Object what) {
		crtable01.set(what);
	}	
	public FixedLengthStringData getCrtable02() {
		return crtable02;
	}
	public void setCrtable02(Object what) {
		crtable02.set(what);
	}	
	public FixedLengthStringData getCrtable03() {
		return crtable03;
	}
	public void setCrtable03(Object what) {
		crtable03.set(what);
	}	
	public FixedLengthStringData getCrtable04() {
		return crtable04;
	}
	public void setCrtable04(Object what) {
		crtable04.set(what);
	}	
	public FixedLengthStringData getCrtable05() {
		return crtable05;
	}
	public void setCrtable05(Object what) {
		crtable05.set(what);
	}	
	public FixedLengthStringData getCrtable06() {
		return crtable06;
	}
	public void setCrtable06(Object what) {
		crtable06.set(what);
	}	
	public FixedLengthStringData getCrtable07() {
		return crtable07;
	}
	public void setCrtable07(Object what) {
		crtable07.set(what);
	}	
	public FixedLengthStringData getCrtable08() {
		return crtable08;
	}
	public void setCrtable08(Object what) {
		crtable08.set(what);
	}	
	public FixedLengthStringData getCrtable09() {
		return crtable09;
	}
	public void setCrtable09(Object what) {
		crtable09.set(what);
	}	
	public FixedLengthStringData getCrtable10() {
		return crtable10;
	}
	public void setCrtable10(Object what) {
		crtable10.set(what);
	}	
	public FixedLengthStringData getCrtable11() {
		return crtable11;
	}
	public void setCrtable11(Object what) {
		crtable11.set(what);
	}	
	public FixedLengthStringData getCrtable12() {
		return crtable12;
	}
	public void setCrtable12(Object what) {
		crtable12.set(what);
	}	
	public FixedLengthStringData getCrtable13() {
		return crtable13;
	}
	public void setCrtable13(Object what) {
		crtable13.set(what);
	}	
	public FixedLengthStringData getCrtable14() {
		return crtable14;
	}
	public void setCrtable14(Object what) {
		crtable14.set(what);
	}	
	public FixedLengthStringData getCrtable15() {
		return crtable15;
	}
	public void setCrtable15(Object what) {
		crtable15.set(what);
	}	
	public FixedLengthStringData getCrtable16() {
		return crtable16;
	}
	public void setCrtable16(Object what) {
		crtable16.set(what);
	}	
	public FixedLengthStringData getCrtable17() {
		return crtable17;
	}
	public void setCrtable17(Object what) {
		crtable17.set(what);
	}	
	public FixedLengthStringData getCrtable18() {
		return crtable18;
	}
	public void setCrtable18(Object what) {
		crtable18.set(what);
	}	
	public FixedLengthStringData getCrtable19() {
		return crtable19;
	}
	public void setCrtable19(Object what) {
		crtable19.set(what);
	}	
	public FixedLengthStringData getCrtable20() {
		return crtable20;
	}
	public void setCrtable20(Object what) {
		crtable20.set(what);
	}	
	public PackedDecimalData getBonusAmt01() {
		return bonusAmt01;
	}
	public void setBonusAmt01(Object what) {
		setBonusAmt01(what, false);
	}
	public void setBonusAmt01(Object what, boolean rounded) {
		if (rounded)
			bonusAmt01.setRounded(what);
		else
			bonusAmt01.set(what);
	}	
	public PackedDecimalData getBonusAmt02() {
		return bonusAmt02;
	}
	public void setBonusAmt02(Object what) {
		setBonusAmt02(what, false);
	}
	public void setBonusAmt02(Object what, boolean rounded) {
		if (rounded)
			bonusAmt02.setRounded(what);
		else
			bonusAmt02.set(what);
	}	
	public PackedDecimalData getBonusAmt03() {
		return bonusAmt03;
	}
	public void setBonusAmt03(Object what) {
		setBonusAmt03(what, false);
	}
	public void setBonusAmt03(Object what, boolean rounded) {
		if (rounded)
			bonusAmt03.setRounded(what);
		else
			bonusAmt03.set(what);
	}	
	public PackedDecimalData getBonusAmt04() {
		return bonusAmt04;
	}
	public void setBonusAmt04(Object what) {
		setBonusAmt04(what, false);
	}
	public void setBonusAmt04(Object what, boolean rounded) {
		if (rounded)
			bonusAmt04.setRounded(what);
		else
			bonusAmt04.set(what);
	}	
	public PackedDecimalData getBonusAmt05() {
		return bonusAmt05;
	}
	public void setBonusAmt05(Object what) {
		setBonusAmt05(what, false);
	}
	public void setBonusAmt05(Object what, boolean rounded) {
		if (rounded)
			bonusAmt05.setRounded(what);
		else
			bonusAmt05.set(what);
	}	
	public PackedDecimalData getBonusAmt06() {
		return bonusAmt06;
	}
	public void setBonusAmt06(Object what) {
		setBonusAmt06(what, false);
	}
	public void setBonusAmt06(Object what, boolean rounded) {
		if (rounded)
			bonusAmt06.setRounded(what);
		else
			bonusAmt06.set(what);
	}	
	public PackedDecimalData getBonusAmt07() {
		return bonusAmt07;
	}
	public void setBonusAmt07(Object what) {
		setBonusAmt07(what, false);
	}
	public void setBonusAmt07(Object what, boolean rounded) {
		if (rounded)
			bonusAmt07.setRounded(what);
		else
			bonusAmt07.set(what);
	}	
	public PackedDecimalData getBonusAmt08() {
		return bonusAmt08;
	}
	public void setBonusAmt08(Object what) {
		setBonusAmt08(what, false);
	}
	public void setBonusAmt08(Object what, boolean rounded) {
		if (rounded)
			bonusAmt08.setRounded(what);
		else
			bonusAmt08.set(what);
	}	
	public PackedDecimalData getBonusAmt09() {
		return bonusAmt09;
	}
	public void setBonusAmt09(Object what) {
		setBonusAmt09(what, false);
	}
	public void setBonusAmt09(Object what, boolean rounded) {
		if (rounded)
			bonusAmt09.setRounded(what);
		else
			bonusAmt09.set(what);
	}	
	public PackedDecimalData getBonusAmt10() {
		return bonusAmt10;
	}
	public void setBonusAmt10(Object what) {
		setBonusAmt10(what, false);
	}
	public void setBonusAmt10(Object what, boolean rounded) {
		if (rounded)
			bonusAmt10.setRounded(what);
		else
			bonusAmt10.set(what);
	}	
	public PackedDecimalData getBonusAmt11() {
		return bonusAmt11;
	}
	public void setBonusAmt11(Object what) {
		setBonusAmt11(what, false);
	}
	public void setBonusAmt11(Object what, boolean rounded) {
		if (rounded)
			bonusAmt11.setRounded(what);
		else
			bonusAmt11.set(what);
	}	
	public PackedDecimalData getBonusAmt12() {
		return bonusAmt12;
	}
	public void setBonusAmt12(Object what) {
		setBonusAmt12(what, false);
	}
	public void setBonusAmt12(Object what, boolean rounded) {
		if (rounded)
			bonusAmt12.setRounded(what);
		else
			bonusAmt12.set(what);
	}	
	public PackedDecimalData getBonusAmt13() {
		return bonusAmt13;
	}
	public void setBonusAmt13(Object what) {
		setBonusAmt13(what, false);
	}
	public void setBonusAmt13(Object what, boolean rounded) {
		if (rounded)
			bonusAmt13.setRounded(what);
		else
			bonusAmt13.set(what);
	}	
	public PackedDecimalData getBonusAmt14() {
		return bonusAmt14;
	}
	public void setBonusAmt14(Object what) {
		setBonusAmt14(what, false);
	}
	public void setBonusAmt14(Object what, boolean rounded) {
		if (rounded)
			bonusAmt14.setRounded(what);
		else
			bonusAmt14.set(what);
	}	
	public PackedDecimalData getBonusAmt15() {
		return bonusAmt15;
	}
	public void setBonusAmt15(Object what) {
		setBonusAmt15(what, false);
	}
	public void setBonusAmt15(Object what, boolean rounded) {
		if (rounded)
			bonusAmt15.setRounded(what);
		else
			bonusAmt15.set(what);
	}	
	public PackedDecimalData getBonusAmt16() {
		return bonusAmt16;
	}
	public void setBonusAmt16(Object what) {
		setBonusAmt16(what, false);
	}
	public void setBonusAmt16(Object what, boolean rounded) {
		if (rounded)
			bonusAmt16.setRounded(what);
		else
			bonusAmt16.set(what);
	}	
	public PackedDecimalData getBonusAmt17() {
		return bonusAmt17;
	}
	public void setBonusAmt17(Object what) {
		setBonusAmt17(what, false);
	}
	public void setBonusAmt17(Object what, boolean rounded) {
		if (rounded)
			bonusAmt17.setRounded(what);
		else
			bonusAmt17.set(what);
	}	
	public PackedDecimalData getBonusAmt18() {
		return bonusAmt18;
	}
	public void setBonusAmt18(Object what) {
		setBonusAmt18(what, false);
	}
	public void setBonusAmt18(Object what, boolean rounded) {
		if (rounded)
			bonusAmt18.setRounded(what);
		else
			bonusAmt18.set(what);
	}	
	public PackedDecimalData getBonusAmt19() {
		return bonusAmt19;
	}
	public void setBonusAmt19(Object what) {
		setBonusAmt19(what, false);
	}
	public void setBonusAmt19(Object what, boolean rounded) {
		if (rounded)
			bonusAmt19.setRounded(what);
		else
			bonusAmt19.set(what);
	}	
	public PackedDecimalData getBonusAmt20() {
		return bonusAmt20;
	}
	public void setBonusAmt20(Object what) {
		setBonusAmt20(what, false);
	}
	public void setBonusAmt20(Object what, boolean rounded) {
		if (rounded)
			bonusAmt20.setRounded(what);
		else
			bonusAmt20.set(what);
	}	
	public PackedDecimalData getBonusAmt21() {
		return bonusAmt21;
	}
	public void setBonusAmt21(Object what) {
		setBonusAmt21(what, false);
	}
	public void setBonusAmt21(Object what, boolean rounded) {
		if (rounded)
			bonusAmt21.setRounded(what);
		else
			bonusAmt21.set(what);
	}	
	public PackedDecimalData getSumins01() {
		return sumins01;
	}
	public void setSumins01(Object what) {
		setSumins01(what, false);
	}
	public void setSumins01(Object what, boolean rounded) {
		if (rounded)
			sumins01.setRounded(what);
		else
			sumins01.set(what);
	}	
	public PackedDecimalData getSumins02() {
		return sumins02;
	}
	public void setSumins02(Object what) {
		setSumins02(what, false);
	}
	public void setSumins02(Object what, boolean rounded) {
		if (rounded)
			sumins02.setRounded(what);
		else
			sumins02.set(what);
	}	
	public PackedDecimalData getSumins03() {
		return sumins03;
	}
	public void setSumins03(Object what) {
		setSumins03(what, false);
	}
	public void setSumins03(Object what, boolean rounded) {
		if (rounded)
			sumins03.setRounded(what);
		else
			sumins03.set(what);
	}	
	public PackedDecimalData getSumins04() {
		return sumins04;
	}
	public void setSumins04(Object what) {
		setSumins04(what, false);
	}
	public void setSumins04(Object what, boolean rounded) {
		if (rounded)
			sumins04.setRounded(what);
		else
			sumins04.set(what);
	}	
	public PackedDecimalData getSumins05() {
		return sumins05;
	}
	public void setSumins05(Object what) {
		setSumins05(what, false);
	}
	public void setSumins05(Object what, boolean rounded) {
		if (rounded)
			sumins05.setRounded(what);
		else
			sumins05.set(what);
	}	
	public PackedDecimalData getSumins06() {
		return sumins06;
	}
	public void setSumins06(Object what) {
		setSumins06(what, false);
	}
	public void setSumins06(Object what, boolean rounded) {
		if (rounded)
			sumins06.setRounded(what);
		else
			sumins06.set(what);
	}	
	public PackedDecimalData getSumins07() {
		return sumins07;
	}
	public void setSumins07(Object what) {
		setSumins07(what, false);
	}
	public void setSumins07(Object what, boolean rounded) {
		if (rounded)
			sumins07.setRounded(what);
		else
			sumins07.set(what);
	}	
	public PackedDecimalData getSumins08() {
		return sumins08;
	}
	public void setSumins08(Object what) {
		setSumins08(what, false);
	}
	public void setSumins08(Object what, boolean rounded) {
		if (rounded)
			sumins08.setRounded(what);
		else
			sumins08.set(what);
	}	
	public PackedDecimalData getSumins09() {
		return sumins09;
	}
	public void setSumins09(Object what) {
		setSumins09(what, false);
	}
	public void setSumins09(Object what, boolean rounded) {
		if (rounded)
			sumins09.setRounded(what);
		else
			sumins09.set(what);
	}	
	public PackedDecimalData getSumins10() {
		return sumins10;
	}
	public void setSumins10(Object what) {
		setSumins10(what, false);
	}
	public void setSumins10(Object what, boolean rounded) {
		if (rounded)
			sumins10.setRounded(what);
		else
			sumins10.set(what);
	}	
	public PackedDecimalData getSumins11() {
		return sumins11;
	}
	public void setSumins11(Object what) {
		setSumins11(what, false);
	}
	public void setSumins11(Object what, boolean rounded) {
		if (rounded)
			sumins11.setRounded(what);
		else
			sumins11.set(what);
	}	
	public PackedDecimalData getSumins12() {
		return sumins12;
	}
	public void setSumins12(Object what) {
		setSumins12(what, false);
	}
	public void setSumins12(Object what, boolean rounded) {
		if (rounded)
			sumins12.setRounded(what);
		else
			sumins12.set(what);
	}	
	public PackedDecimalData getSumins13() {
		return sumins13;
	}
	public void setSumins13(Object what) {
		setSumins13(what, false);
	}
	public void setSumins13(Object what, boolean rounded) {
		if (rounded)
			sumins13.setRounded(what);
		else
			sumins13.set(what);
	}	
	public PackedDecimalData getSumins14() {
		return sumins14;
	}
	public void setSumins14(Object what) {
		setSumins14(what, false);
	}
	public void setSumins14(Object what, boolean rounded) {
		if (rounded)
			sumins14.setRounded(what);
		else
			sumins14.set(what);
	}	
	public PackedDecimalData getSumins15() {
		return sumins15;
	}
	public void setSumins15(Object what) {
		setSumins15(what, false);
	}
	public void setSumins15(Object what, boolean rounded) {
		if (rounded)
			sumins15.setRounded(what);
		else
			sumins15.set(what);
	}	
	public PackedDecimalData getSumins16() {
		return sumins16;
	}
	public void setSumins16(Object what) {
		setSumins16(what, false);
	}
	public void setSumins16(Object what, boolean rounded) {
		if (rounded)
			sumins16.setRounded(what);
		else
			sumins16.set(what);
	}	
	public PackedDecimalData getSumins17() {
		return sumins17;
	}
	public void setSumins17(Object what) {
		setSumins17(what, false);
	}
	public void setSumins17(Object what, boolean rounded) {
		if (rounded)
			sumins17.setRounded(what);
		else
			sumins17.set(what);
	}	
	public PackedDecimalData getSumins18() {
		return sumins18;
	}
	public void setSumins18(Object what) {
		setSumins18(what, false);
	}
	public void setSumins18(Object what, boolean rounded) {
		if (rounded)
			sumins18.setRounded(what);
		else
			sumins18.set(what);
	}	
	public PackedDecimalData getSumins19() {
		return sumins19;
	}
	public void setSumins19(Object what) {
		setSumins19(what, false);
	}
	public void setSumins19(Object what, boolean rounded) {
		if (rounded)
			sumins19.setRounded(what);
		else
			sumins19.set(what);
	}	
	public PackedDecimalData getSumins20() {
		return sumins20;
	}
	public void setSumins20(Object what) {
		setSumins20(what, false);
	}
	public void setSumins20(Object what, boolean rounded) {
		if (rounded)
			sumins20.setRounded(what);
		else
			sumins20.set(what);
	}	
	public PackedDecimalData getSumins21() {
		return sumins21;
	}
	public void setSumins21(Object what) {
		setSumins21(what, false);
	}
	public void setSumins21(Object what, boolean rounded) {
		if (rounded)
			sumins21.setRounded(what);
		else
			sumins21.set(what);
	}	
	public PackedDecimalData getMatamt01() {
		return matamt01;
	}
	public void setMatamt01(Object what) {
		setMatamt01(what, false);
	}
	public void setMatamt01(Object what, boolean rounded) {
		if (rounded)
			matamt01.setRounded(what);
		else
			matamt01.set(what);
	}	
	public PackedDecimalData getMatamt02() {
		return matamt02;
	}
	public void setMatamt02(Object what) {
		setMatamt02(what, false);
	}
	public void setMatamt02(Object what, boolean rounded) {
		if (rounded)
			matamt02.setRounded(what);
		else
			matamt02.set(what);
	}	
	public PackedDecimalData getMatamt03() {
		return matamt03;
	}
	public void setMatamt03(Object what) {
		setMatamt03(what, false);
	}
	public void setMatamt03(Object what, boolean rounded) {
		if (rounded)
			matamt03.setRounded(what);
		else
			matamt03.set(what);
	}	
	public PackedDecimalData getMatamt04() {
		return matamt04;
	}
	public void setMatamt04(Object what) {
		setMatamt04(what, false);
	}
	public void setMatamt04(Object what, boolean rounded) {
		if (rounded)
			matamt04.setRounded(what);
		else
			matamt04.set(what);
	}	
	public PackedDecimalData getMatamt05() {
		return matamt05;
	}
	public void setMatamt05(Object what) {
		setMatamt05(what, false);
	}
	public void setMatamt05(Object what, boolean rounded) {
		if (rounded)
			matamt05.setRounded(what);
		else
			matamt05.set(what);
	}	
	public PackedDecimalData getMatamt06() {
		return matamt06;
	}
	public void setMatamt06(Object what) {
		setMatamt06(what, false);
	}
	public void setMatamt06(Object what, boolean rounded) {
		if (rounded)
			matamt06.setRounded(what);
		else
			matamt06.set(what);
	}	
	public PackedDecimalData getMatamt07() {
		return matamt07;
	}
	public void setMatamt07(Object what) {
		setMatamt07(what, false);
	}
	public void setMatamt07(Object what, boolean rounded) {
		if (rounded)
			matamt07.setRounded(what);
		else
			matamt07.set(what);
	}	
	public PackedDecimalData getMatamt08() {
		return matamt08;
	}
	public void setMatamt08(Object what) {
		setMatamt08(what, false);
	}
	public void setMatamt08(Object what, boolean rounded) {
		if (rounded)
			matamt08.setRounded(what);
		else
			matamt08.set(what);
	}	
	public PackedDecimalData getMatamt09() {
		return matamt09;
	}
	public void setMatamt09(Object what) {
		setMatamt09(what, false);
	}
	public void setMatamt09(Object what, boolean rounded) {
		if (rounded)
			matamt09.setRounded(what);
		else
			matamt09.set(what);
	}	
	public PackedDecimalData getMatamt10() {
		return matamt10;
	}
	public void setMatamt10(Object what) {
		setMatamt10(what, false);
	}
	public void setMatamt10(Object what, boolean rounded) {
		if (rounded)
			matamt10.setRounded(what);
		else
			matamt10.set(what);
	}	
	public PackedDecimalData getMatamt11() {
		return matamt11;
	}
	public void setMatamt11(Object what) {
		setMatamt11(what, false);
	}
	public void setMatamt11(Object what, boolean rounded) {
		if (rounded)
			matamt11.setRounded(what);
		else
			matamt11.set(what);
	}	
	public PackedDecimalData getMatamt12() {
		return matamt12;
	}
	public void setMatamt12(Object what) {
		setMatamt12(what, false);
	}
	public void setMatamt12(Object what, boolean rounded) {
		if (rounded)
			matamt12.setRounded(what);
		else
			matamt12.set(what);
	}	
	public PackedDecimalData getMatamt13() {
		return matamt13;
	}
	public void setMatamt13(Object what) {
		setMatamt13(what, false);
	}
	public void setMatamt13(Object what, boolean rounded) {
		if (rounded)
			matamt13.setRounded(what);
		else
			matamt13.set(what);
	}	
	public PackedDecimalData getMatamt14() {
		return matamt14;
	}
	public void setMatamt14(Object what) {
		setMatamt14(what, false);
	}
	public void setMatamt14(Object what, boolean rounded) {
		if (rounded)
			matamt14.setRounded(what);
		else
			matamt14.set(what);
	}	
	public PackedDecimalData getMatamt15() {
		return matamt15;
	}
	public void setMatamt15(Object what) {
		setMatamt15(what, false);
	}
	public void setMatamt15(Object what, boolean rounded) {
		if (rounded)
			matamt15.setRounded(what);
		else
			matamt15.set(what);
	}	
	public PackedDecimalData getMatamt16() {
		return matamt16;
	}
	public void setMatamt16(Object what) {
		setMatamt16(what, false);
	}
	public void setMatamt16(Object what, boolean rounded) {
		if (rounded)
			matamt16.setRounded(what);
		else
			matamt16.set(what);
	}	
	public PackedDecimalData getMatamt17() {
		return matamt17;
	}
	public void setMatamt17(Object what) {
		setMatamt17(what, false);
	}
	public void setMatamt17(Object what, boolean rounded) {
		if (rounded)
			matamt17.setRounded(what);
		else
			matamt17.set(what);
	}	
	public PackedDecimalData getMatamt18() {
		return matamt18;
	}
	public void setMatamt18(Object what) {
		setMatamt18(what, false);
	}
	public void setMatamt18(Object what, boolean rounded) {
		if (rounded)
			matamt18.setRounded(what);
		else
			matamt18.set(what);
	}	
	public PackedDecimalData getMatamt19() {
		return matamt19;
	}
	public void setMatamt19(Object what) {
		setMatamt19(what, false);
	}
	public void setMatamt19(Object what, boolean rounded) {
		if (rounded)
			matamt19.setRounded(what);
		else
			matamt19.set(what);
	}	
	public PackedDecimalData getMatamt20() {
		return matamt20;
	}
	public void setMatamt20(Object what) {
		setMatamt20(what, false);
	}
	public void setMatamt20(Object what, boolean rounded) {
		if (rounded)
			matamt20.setRounded(what);
		else
			matamt20.set(what);
	}	
	public PackedDecimalData getMatamt21() {
		return matamt21;
	}
	public void setMatamt21(Object what) {
		setMatamt21(what, false);
	}
	public void setMatamt21(Object what, boolean rounded) {
		if (rounded)
			matamt21.setRounded(what);
		else
			matamt21.set(what);
	}	
	public FixedLengthStringData getVirtualFund01() {
		return virtualFund01;
	}
	public void setVirtualFund01(Object what) {
		virtualFund01.set(what);
	}	
	public FixedLengthStringData getVirtualFund02() {
		return virtualFund02;
	}
	public void setVirtualFund02(Object what) {
		virtualFund02.set(what);
	}	
	public FixedLengthStringData getVirtualFund03() {
		return virtualFund03;
	}
	public void setVirtualFund03(Object what) {
		virtualFund03.set(what);
	}	
	public FixedLengthStringData getVirtualFund04() {
		return virtualFund04;
	}
	public void setVirtualFund04(Object what) {
		virtualFund04.set(what);
	}	
	public FixedLengthStringData getVirtualFund05() {
		return virtualFund05;
	}
	public void setVirtualFund05(Object what) {
		virtualFund05.set(what);
	}	
	public FixedLengthStringData getVirtualFund06() {
		return virtualFund06;
	}
	public void setVirtualFund06(Object what) {
		virtualFund06.set(what);
	}	
	public FixedLengthStringData getVirtualFund07() {
		return virtualFund07;
	}
	public void setVirtualFund07(Object what) {
		virtualFund07.set(what);
	}	
	public FixedLengthStringData getVirtualFund08() {
		return virtualFund08;
	}
	public void setVirtualFund08(Object what) {
		virtualFund08.set(what);
	}	
	public FixedLengthStringData getVirtualFund09() {
		return virtualFund09;
	}
	public void setVirtualFund09(Object what) {
		virtualFund09.set(what);
	}	
	public FixedLengthStringData getVirtualFund10() {
		return virtualFund10;
	}
	public void setVirtualFund10(Object what) {
		virtualFund10.set(what);
	}	
	public FixedLengthStringData getVirtualFund11() {
		return virtualFund11;
	}
	public void setVirtualFund11(Object what) {
		virtualFund11.set(what);
	}	
	public FixedLengthStringData getVirtualFund12() {
		return virtualFund12;
	}
	public void setVirtualFund12(Object what) {
		virtualFund12.set(what);
	}	
	public FixedLengthStringData getVirtualFund13() {
		return virtualFund13;
	}
	public void setVirtualFund13(Object what) {
		virtualFund13.set(what);
	}	
	public FixedLengthStringData getVirtualFund14() {
		return virtualFund14;
	}
	public void setVirtualFund14(Object what) {
		virtualFund14.set(what);
	}	
	public FixedLengthStringData getVirtualFund15() {
		return virtualFund15;
	}
	public void setVirtualFund15(Object what) {
		virtualFund15.set(what);
	}	
	public FixedLengthStringData getVirtualFund16() {
		return virtualFund16;
	}
	public void setVirtualFund16(Object what) {
		virtualFund16.set(what);
	}	
	public FixedLengthStringData getVirtualFund17() {
		return virtualFund17;
	}
	public void setVirtualFund17(Object what) {
		virtualFund17.set(what);
	}	
	public FixedLengthStringData getVirtualFund18() {
		return virtualFund18;
	}
	public void setVirtualFund18(Object what) {
		virtualFund18.set(what);
	}	
	public FixedLengthStringData getVirtualFund19() {
		return virtualFund19;
	}
	public void setVirtualFund19(Object what) {
		virtualFund19.set(what);
	}	
	public FixedLengthStringData getVirtualFund20() {
		return virtualFund20;
	}
	public void setVirtualFund20(Object what) {
		virtualFund20.set(what);
	}	
	public FixedLengthStringData getFieldType01() {
		return fieldType01;
	}
	public void setFieldType01(Object what) {
		fieldType01.set(what);
	}	
	public FixedLengthStringData getFieldType02() {
		return fieldType02;
	}
	public void setFieldType02(Object what) {
		fieldType02.set(what);
	}	
	public FixedLengthStringData getFieldType03() {
		return fieldType03;
	}
	public void setFieldType03(Object what) {
		fieldType03.set(what);
	}	
	public FixedLengthStringData getFieldType04() {
		return fieldType04;
	}
	public void setFieldType04(Object what) {
		fieldType04.set(what);
	}	
	public FixedLengthStringData getFieldType05() {
		return fieldType05;
	}
	public void setFieldType05(Object what) {
		fieldType05.set(what);
	}	
	public FixedLengthStringData getFieldType06() {
		return fieldType06;
	}
	public void setFieldType06(Object what) {
		fieldType06.set(what);
	}	
	public FixedLengthStringData getFieldType07() {
		return fieldType07;
	}
	public void setFieldType07(Object what) {
		fieldType07.set(what);
	}	
	public FixedLengthStringData getFieldType08() {
		return fieldType08;
	}
	public void setFieldType08(Object what) {
		fieldType08.set(what);
	}	
	public FixedLengthStringData getFieldType09() {
		return fieldType09;
	}
	public void setFieldType09(Object what) {
		fieldType09.set(what);
	}	
	public FixedLengthStringData getFieldType10() {
		return fieldType10;
	}
	public void setFieldType10(Object what) {
		fieldType10.set(what);
	}	
	public FixedLengthStringData getFieldType11() {
		return fieldType11;
	}
	public void setFieldType11(Object what) {
		fieldType11.set(what);
	}	
	public FixedLengthStringData getFieldType12() {
		return fieldType12;
	}
	public void setFieldType12(Object what) {
		fieldType12.set(what);
	}	
	public FixedLengthStringData getFieldType13() {
		return fieldType13;
	}
	public void setFieldType13(Object what) {
		fieldType13.set(what);
	}	
	public FixedLengthStringData getFieldType14() {
		return fieldType14;
	}
	public void setFieldType14(Object what) {
		fieldType14.set(what);
	}	
	public FixedLengthStringData getFieldType15() {
		return fieldType15;
	}
	public void setFieldType15(Object what) {
		fieldType15.set(what);
	}	
	public FixedLengthStringData getFieldType16() {
		return fieldType16;
	}
	public void setFieldType16(Object what) {
		fieldType16.set(what);
	}	
	public FixedLengthStringData getFieldType17() {
		return fieldType17;
	}
	public void setFieldType17(Object what) {
		fieldType17.set(what);
	}	
	public FixedLengthStringData getFieldType18() {
		return fieldType18;
	}
	public void setFieldType18(Object what) {
		fieldType18.set(what);
	}	
	public FixedLengthStringData getFieldType19() {
		return fieldType19;
	}
	public void setFieldType19(Object what) {
		fieldType19.set(what);
	}	
	public FixedLengthStringData getFieldType20() {
		return fieldType20;
	}
	public void setFieldType20(Object what) {
		fieldType20.set(what);
	}	
	public FixedLengthStringData getDescrip01() {
		return descrip01;
	}
	public void setDescrip01(Object what) {
		descrip01.set(what);
	}	
	public FixedLengthStringData getDescrip02() {
		return descrip02;
	}
	public void setDescrip02(Object what) {
		descrip02.set(what);
	}	
	public FixedLengthStringData getDescrip03() {
		return descrip03;
	}
	public void setDescrip03(Object what) {
		descrip03.set(what);
	}	
	public FixedLengthStringData getDescrip04() {
		return descrip04;
	}
	public void setDescrip04(Object what) {
		descrip04.set(what);
	}	
	public FixedLengthStringData getDescrip05() {
		return descrip05;
	}
	public void setDescrip05(Object what) {
		descrip05.set(what);
	}	
	public FixedLengthStringData getDescrip06() {
		return descrip06;
	}
	public void setDescrip06(Object what) {
		descrip06.set(what);
	}	
	public FixedLengthStringData getDescrip07() {
		return descrip07;
	}
	public void setDescrip07(Object what) {
		descrip07.set(what);
	}	
	public FixedLengthStringData getDescrip08() {
		return descrip08;
	}
	public void setDescrip08(Object what) {
		descrip08.set(what);
	}	
	public FixedLengthStringData getDescrip09() {
		return descrip09;
	}
	public void setDescrip09(Object what) {
		descrip09.set(what);
	}	
	public FixedLengthStringData getDescrip10() {
		return descrip10;
	}
	public void setDescrip10(Object what) {
		descrip10.set(what);
	}	
	public FixedLengthStringData getDescrip11() {
		return descrip11;
	}
	public void setDescrip11(Object what) {
		descrip11.set(what);
	}	
	public FixedLengthStringData getDescrip12() {
		return descrip12;
	}
	public void setDescrip12(Object what) {
		descrip12.set(what);
	}	
	public FixedLengthStringData getDescrip13() {
		return descrip13;
	}
	public void setDescrip13(Object what) {
		descrip13.set(what);
	}	
	public FixedLengthStringData getDescrip14() {
		return descrip14;
	}
	public void setDescrip14(Object what) {
		descrip14.set(what);
	}	
	public FixedLengthStringData getDescrip15() {
		return descrip15;
	}
	public void setDescrip15(Object what) {
		descrip15.set(what);
	}	
	public FixedLengthStringData getDescrip16() {
		return descrip16;
	}
	public void setDescrip16(Object what) {
		descrip16.set(what);
	}	
	public FixedLengthStringData getDescrip17() {
		return descrip17;
	}
	public void setDescrip17(Object what) {
		descrip17.set(what);
	}	
	public FixedLengthStringData getDescrip18() {
		return descrip18;
	}
	public void setDescrip18(Object what) {
		descrip18.set(what);
	}	
	public FixedLengthStringData getDescrip19() {
		return descrip19;
	}
	public void setDescrip19(Object what) {
		descrip19.set(what);
	}	
	public FixedLengthStringData getDescrip20() {
		return descrip20;
	}
	public void setDescrip20(Object what) {
		descrip20.set(what);
	}	
	public FixedLengthStringData getCurrcd01() {
		return currcd01;
	}
	public void setCurrcd01(Object what) {
		currcd01.set(what);
	}	
	public FixedLengthStringData getCurrcd02() {
		return currcd02;
	}
	public void setCurrcd02(Object what) {
		currcd02.set(what);
	}	
	public FixedLengthStringData getCurrcd03() {
		return currcd03;
	}
	public void setCurrcd03(Object what) {
		currcd03.set(what);
	}	
	public FixedLengthStringData getCurrcd04() {
		return currcd04;
	}
	public void setCurrcd04(Object what) {
		currcd04.set(what);
	}	
	public FixedLengthStringData getCurrcd05() {
		return currcd05;
	}
	public void setCurrcd05(Object what) {
		currcd05.set(what);
	}	
	public FixedLengthStringData getCurrcd06() {
		return currcd06;
	}
	public void setCurrcd06(Object what) {
		currcd06.set(what);
	}	
	public FixedLengthStringData getCurrcd07() {
		return currcd07;
	}
	public void setCurrcd07(Object what) {
		currcd07.set(what);
	}	
	public FixedLengthStringData getCurrcd08() {
		return currcd08;
	}
	public void setCurrcd08(Object what) {
		currcd08.set(what);
	}	
	public FixedLengthStringData getCurrcd09() {
		return currcd09;
	}
	public void setCurrcd09(Object what) {
		currcd09.set(what);
	}	
	public FixedLengthStringData getCurrcd10() {
		return currcd10;
	}
	public void setCurrcd10(Object what) {
		currcd10.set(what);
	}	
	public FixedLengthStringData getCurrcd11() {
		return currcd11;
	}
	public void setCurrcd11(Object what) {
		currcd11.set(what);
	}	
	public FixedLengthStringData getCurrcd12() {
		return currcd12;
	}
	public void setCurrcd12(Object what) {
		currcd12.set(what);
	}	
	public FixedLengthStringData getCurrcd13() {
		return currcd13;
	}
	public void setCurrcd13(Object what) {
		currcd13.set(what);
	}	
	public FixedLengthStringData getCurrcd14() {
		return currcd14;
	}
	public void setCurrcd14(Object what) {
		currcd14.set(what);
	}	
	public FixedLengthStringData getCurrcd15() {
		return currcd15;
	}
	public void setCurrcd15(Object what) {
		currcd15.set(what);
	}	
	public FixedLengthStringData getCurrcd16() {
		return currcd16;
	}
	public void setCurrcd16(Object what) {
		currcd16.set(what);
	}	
	public FixedLengthStringData getCurrcd17() {
		return currcd17;
	}
	public void setCurrcd17(Object what) {
		currcd17.set(what);
	}	
	public FixedLengthStringData getCurrcd18() {
		return currcd18;
	}
	public void setCurrcd18(Object what) {
		currcd18.set(what);
	}	
	public FixedLengthStringData getCurrcd19() {
		return currcd19;
	}
	public void setCurrcd19(Object what) {
		currcd19.set(what);
	}	
	public FixedLengthStringData getCurrcd20() {
		return currcd20;
	}
	public void setCurrcd20(Object what) {
		currcd20.set(what);
	}	
	public FixedLengthStringData getRcesdt01() {
		return rcesdt01;
	}
	public void setRcesdt01(Object what) {
		rcesdt01.set(what);
	}	
	public FixedLengthStringData getRcesdt02() {
		return rcesdt02;
	}
	public void setRcesdt02(Object what) {
		rcesdt02.set(what);
	}	
	public FixedLengthStringData getRcesdt03() {
		return rcesdt03;
	}
	public void setRcesdt03(Object what) {
		rcesdt03.set(what);
	}	
	public FixedLengthStringData getRcesdt04() {
		return rcesdt04;
	}
	public void setRcesdt04(Object what) {
		rcesdt04.set(what);
	}	
	public FixedLengthStringData getRcesdt05() {
		return rcesdt05;
	}
	public void setRcesdt05(Object what) {
		rcesdt05.set(what);
	}	
	public FixedLengthStringData getRcesdt06() {
		return rcesdt06;
	}
	public void setRcesdt06(Object what) {
		rcesdt06.set(what);
	}	
	public FixedLengthStringData getRcesdt07() {
		return rcesdt07;
	}
	public void setRcesdt07(Object what) {
		rcesdt07.set(what);
	}	
	public FixedLengthStringData getRcesdt08() {
		return rcesdt08;
	}
	public void setRcesdt08(Object what) {
		rcesdt08.set(what);
	}	
	public FixedLengthStringData getRcesdt09() {
		return rcesdt09;
	}
	public void setRcesdt09(Object what) {
		rcesdt09.set(what);
	}	
	public FixedLengthStringData getRcesdt10() {
		return rcesdt10;
	}
	public void setRcesdt10(Object what) {
		rcesdt10.set(what);
	}	
	public FixedLengthStringData getRcesdt11() {
		return rcesdt11;
	}
	public void setRcesdt11(Object what) {
		rcesdt11.set(what);
	}	
	public FixedLengthStringData getRcesdt12() {
		return rcesdt12;
	}
	public void setRcesdt12(Object what) {
		rcesdt12.set(what);
	}	
	public FixedLengthStringData getRcesdt13() {
		return rcesdt13;
	}
	public void setRcesdt13(Object what) {
		rcesdt13.set(what);
	}	
	public FixedLengthStringData getRcesdt14() {
		return rcesdt14;
	}
	public void setRcesdt14(Object what) {
		rcesdt14.set(what);
	}	
	public FixedLengthStringData getRcesdt15() {
		return rcesdt15;
	}
	public void setRcesdt15(Object what) {
		rcesdt15.set(what);
	}	
	public FixedLengthStringData getRcesdt16() {
		return rcesdt16;
	}
	public void setRcesdt16(Object what) {
		rcesdt16.set(what);
	}	
	public FixedLengthStringData getRcesdt17() {
		return rcesdt17;
	}
	public void setRcesdt17(Object what) {
		rcesdt17.set(what);
	}	
	public FixedLengthStringData getRcesdt18() {
		return rcesdt18;
	}
	public void setRcesdt18(Object what) {
		rcesdt18.set(what);
	}	
	public FixedLengthStringData getRcesdt19() {
		return rcesdt19;
	}
	public void setRcesdt19(Object what) {
		rcesdt19.set(what);
	}	
	public FixedLengthStringData getRcesdt20() {
		return rcesdt20;
	}
	public void setRcesdt20(Object what) {
		rcesdt20.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getVrtfunds() {
		return new FixedLengthStringData(virtualFund01.toInternal()
										+ virtualFund02.toInternal()
										+ virtualFund03.toInternal()
										+ virtualFund04.toInternal()
										+ virtualFund05.toInternal()
										+ virtualFund06.toInternal()
										+ virtualFund07.toInternal()
										+ virtualFund08.toInternal()
										+ virtualFund09.toInternal()
										+ virtualFund10.toInternal()
										+ virtualFund11.toInternal()
										+ virtualFund12.toInternal()
										+ virtualFund13.toInternal()
										+ virtualFund14.toInternal()
										+ virtualFund15.toInternal()
										+ virtualFund16.toInternal()
										+ virtualFund17.toInternal()
										+ virtualFund18.toInternal()
										+ virtualFund19.toInternal()
										+ virtualFund20.toInternal());
	}
	public void setVrtfunds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getVrtfunds().getLength()).init(obj);
	
		what = ExternalData.chop(what, virtualFund01);
		what = ExternalData.chop(what, virtualFund02);
		what = ExternalData.chop(what, virtualFund03);
		what = ExternalData.chop(what, virtualFund04);
		what = ExternalData.chop(what, virtualFund05);
		what = ExternalData.chop(what, virtualFund06);
		what = ExternalData.chop(what, virtualFund07);
		what = ExternalData.chop(what, virtualFund08);
		what = ExternalData.chop(what, virtualFund09);
		what = ExternalData.chop(what, virtualFund10);
		what = ExternalData.chop(what, virtualFund11);
		what = ExternalData.chop(what, virtualFund12);
		what = ExternalData.chop(what, virtualFund13);
		what = ExternalData.chop(what, virtualFund14);
		what = ExternalData.chop(what, virtualFund15);
		what = ExternalData.chop(what, virtualFund16);
		what = ExternalData.chop(what, virtualFund17);
		what = ExternalData.chop(what, virtualFund18);
		what = ExternalData.chop(what, virtualFund19);
		what = ExternalData.chop(what, virtualFund20);
	}
	public FixedLengthStringData getVrtfund(BaseData indx) {
		return getVrtfund(indx.toInt());
	}
	public FixedLengthStringData getVrtfund(int indx) {

		switch (indx) {
			case 1 : return virtualFund01;
			case 2 : return virtualFund02;
			case 3 : return virtualFund03;
			case 4 : return virtualFund04;
			case 5 : return virtualFund05;
			case 6 : return virtualFund06;
			case 7 : return virtualFund07;
			case 8 : return virtualFund08;
			case 9 : return virtualFund09;
			case 10 : return virtualFund10;
			case 11 : return virtualFund11;
			case 12 : return virtualFund12;
			case 13 : return virtualFund13;
			case 14 : return virtualFund14;
			case 15 : return virtualFund15;
			case 16 : return virtualFund16;
			case 17 : return virtualFund17;
			case 18 : return virtualFund18;
			case 19 : return virtualFund19;
			case 20 : return virtualFund20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setVrtfund(BaseData indx, Object what) {
		setVrtfund(indx.toInt(), what);
	}
	public void setVrtfund(int indx, Object what) {

		switch (indx) {
			case 1 : setVirtualFund01(what);
					 break;
			case 2 : setVirtualFund02(what);
					 break;
			case 3 : setVirtualFund03(what);
					 break;
			case 4 : setVirtualFund04(what);
					 break;
			case 5 : setVirtualFund05(what);
					 break;
			case 6 : setVirtualFund06(what);
					 break;
			case 7 : setVirtualFund07(what);
					 break;
			case 8 : setVirtualFund08(what);
					 break;
			case 9 : setVirtualFund09(what);
					 break;
			case 10 : setVirtualFund10(what);
					 break;
			case 11 : setVirtualFund11(what);
					 break;
			case 12 : setVirtualFund12(what);
					 break;
			case 13 : setVirtualFund13(what);
					 break;
			case 14 : setVirtualFund14(what);
					 break;
			case 15 : setVirtualFund15(what);
					 break;
			case 16 : setVirtualFund16(what);
					 break;
			case 17 : setVirtualFund17(what);
					 break;
			case 18 : setVirtualFund18(what);
					 break;
			case 19 : setVirtualFund19(what);
					 break;
			case 20 : setVirtualFund20(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getTypes() {
		return new FixedLengthStringData(fieldType01.toInternal()
										+ fieldType02.toInternal()
										+ fieldType03.toInternal()
										+ fieldType04.toInternal()
										+ fieldType05.toInternal()
										+ fieldType06.toInternal()
										+ fieldType07.toInternal()
										+ fieldType08.toInternal()
										+ fieldType09.toInternal()
										+ fieldType10.toInternal()
										+ fieldType11.toInternal()
										+ fieldType12.toInternal()
										+ fieldType13.toInternal()
										+ fieldType14.toInternal()
										+ fieldType15.toInternal()
										+ fieldType16.toInternal()
										+ fieldType17.toInternal()
										+ fieldType18.toInternal()
										+ fieldType19.toInternal()
										+ fieldType20.toInternal());
	}
	public void setTypes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getTypes().getLength()).init(obj);
	
		what = ExternalData.chop(what, fieldType01);
		what = ExternalData.chop(what, fieldType02);
		what = ExternalData.chop(what, fieldType03);
		what = ExternalData.chop(what, fieldType04);
		what = ExternalData.chop(what, fieldType05);
		what = ExternalData.chop(what, fieldType06);
		what = ExternalData.chop(what, fieldType07);
		what = ExternalData.chop(what, fieldType08);
		what = ExternalData.chop(what, fieldType09);
		what = ExternalData.chop(what, fieldType10);
		what = ExternalData.chop(what, fieldType11);
		what = ExternalData.chop(what, fieldType12);
		what = ExternalData.chop(what, fieldType13);
		what = ExternalData.chop(what, fieldType14);
		what = ExternalData.chop(what, fieldType15);
		what = ExternalData.chop(what, fieldType16);
		what = ExternalData.chop(what, fieldType17);
		what = ExternalData.chop(what, fieldType18);
		what = ExternalData.chop(what, fieldType19);
		what = ExternalData.chop(what, fieldType20);
	}
	public FixedLengthStringData getType(BaseData indx) {
		return getType(indx.toInt());
	}
	public FixedLengthStringData getType(int indx) {

		switch (indx) {
			case 1 : return fieldType01;
			case 2 : return fieldType02;
			case 3 : return fieldType03;
			case 4 : return fieldType04;
			case 5 : return fieldType05;
			case 6 : return fieldType06;
			case 7 : return fieldType07;
			case 8 : return fieldType08;
			case 9 : return fieldType09;
			case 10 : return fieldType10;
			case 11 : return fieldType11;
			case 12 : return fieldType12;
			case 13 : return fieldType13;
			case 14 : return fieldType14;
			case 15 : return fieldType15;
			case 16 : return fieldType16;
			case 17 : return fieldType17;
			case 18 : return fieldType18;
			case 19 : return fieldType19;
			case 20 : return fieldType20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setType(BaseData indx, Object what) {
		setType(indx.toInt(), what);
	}
	public void setType(int indx, Object what) {

		switch (indx) {
			case 1 : setFieldType01(what);
					 break;
			case 2 : setFieldType02(what);
					 break;
			case 3 : setFieldType03(what);
					 break;
			case 4 : setFieldType04(what);
					 break;
			case 5 : setFieldType05(what);
					 break;
			case 6 : setFieldType06(what);
					 break;
			case 7 : setFieldType07(what);
					 break;
			case 8 : setFieldType08(what);
					 break;
			case 9 : setFieldType09(what);
					 break;
			case 10 : setFieldType10(what);
					 break;
			case 11 : setFieldType11(what);
					 break;
			case 12 : setFieldType12(what);
					 break;
			case 13 : setFieldType13(what);
					 break;
			case 14 : setFieldType14(what);
					 break;
			case 15 : setFieldType15(what);
					 break;
			case 16 : setFieldType16(what);
					 break;
			case 17 : setFieldType17(what);
					 break;
			case 18 : setFieldType18(what);
					 break;
			case 19 : setFieldType19(what);
					 break;
			case 20 : setFieldType20(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSuminss() {
		return new FixedLengthStringData(sumins01.toInternal()
										+ sumins02.toInternal()
										+ sumins03.toInternal()
										+ sumins04.toInternal()
										+ sumins05.toInternal()
										+ sumins06.toInternal()
										+ sumins07.toInternal()
										+ sumins08.toInternal()
										+ sumins09.toInternal()
										+ sumins10.toInternal()
										+ sumins11.toInternal()
										+ sumins12.toInternal()
										+ sumins13.toInternal()
										+ sumins14.toInternal()
										+ sumins15.toInternal()
										+ sumins16.toInternal()
										+ sumins17.toInternal()
										+ sumins18.toInternal()
										+ sumins19.toInternal()
										+ sumins20.toInternal()
										+ sumins21.toInternal());
	}
	public void setSuminss(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuminss().getLength()).init(obj);
	
		what = ExternalData.chop(what, sumins01);
		what = ExternalData.chop(what, sumins02);
		what = ExternalData.chop(what, sumins03);
		what = ExternalData.chop(what, sumins04);
		what = ExternalData.chop(what, sumins05);
		what = ExternalData.chop(what, sumins06);
		what = ExternalData.chop(what, sumins07);
		what = ExternalData.chop(what, sumins08);
		what = ExternalData.chop(what, sumins09);
		what = ExternalData.chop(what, sumins10);
		what = ExternalData.chop(what, sumins11);
		what = ExternalData.chop(what, sumins12);
		what = ExternalData.chop(what, sumins13);
		what = ExternalData.chop(what, sumins14);
		what = ExternalData.chop(what, sumins15);
		what = ExternalData.chop(what, sumins16);
		what = ExternalData.chop(what, sumins17);
		what = ExternalData.chop(what, sumins18);
		what = ExternalData.chop(what, sumins19);
		what = ExternalData.chop(what, sumins20);
		what = ExternalData.chop(what, sumins21);
	}
	public PackedDecimalData getSumins(BaseData indx) {
		return getSumins(indx.toInt());
	}
	public PackedDecimalData getSumins(int indx) {

		switch (indx) {
			case 1 : return sumins01;
			case 2 : return sumins02;
			case 3 : return sumins03;
			case 4 : return sumins04;
			case 5 : return sumins05;
			case 6 : return sumins06;
			case 7 : return sumins07;
			case 8 : return sumins08;
			case 9 : return sumins09;
			case 10 : return sumins10;
			case 11 : return sumins11;
			case 12 : return sumins12;
			case 13 : return sumins13;
			case 14 : return sumins14;
			case 15 : return sumins15;
			case 16 : return sumins16;
			case 17 : return sumins17;
			case 18 : return sumins18;
			case 19 : return sumins19;
			case 20 : return sumins20;
			case 21 : return sumins21;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSumins(BaseData indx, Object what) {
		setSumins(indx, what, false);
	}
	public void setSumins(BaseData indx, Object what, boolean rounded) {
		setSumins(indx.toInt(), what, rounded);
	}
	public void setSumins(int indx, Object what) {
		setSumins(indx, what, false);
	}
	public void setSumins(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSumins01(what, rounded);
					 break;
			case 2 : setSumins02(what, rounded);
					 break;
			case 3 : setSumins03(what, rounded);
					 break;
			case 4 : setSumins04(what, rounded);
					 break;
			case 5 : setSumins05(what, rounded);
					 break;
			case 6 : setSumins06(what, rounded);
					 break;
			case 7 : setSumins07(what, rounded);
					 break;
			case 8 : setSumins08(what, rounded);
					 break;
			case 9 : setSumins09(what, rounded);
					 break;
			case 10 : setSumins10(what, rounded);
					 break;
			case 11 : setSumins11(what, rounded);
					 break;
			case 12 : setSumins12(what, rounded);
					 break;
			case 13 : setSumins13(what, rounded);
					 break;
			case 14 : setSumins14(what, rounded);
					 break;
			case 15 : setSumins15(what, rounded);
					 break;
			case 16 : setSumins16(what, rounded);
					 break;
			case 17 : setSumins17(what, rounded);
					 break;
			case 18 : setSumins18(what, rounded);
					 break;
			case 19 : setSumins19(what, rounded);
					 break;
			case 20 : setSumins20(what, rounded);
					 break;
			case 21 : setSumins21(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getRcesdts() {
		return new FixedLengthStringData(rcesdt01.toInternal()
										+ rcesdt02.toInternal()
										+ rcesdt03.toInternal()
										+ rcesdt04.toInternal()
										+ rcesdt05.toInternal()
										+ rcesdt06.toInternal()
										+ rcesdt07.toInternal()
										+ rcesdt08.toInternal()
										+ rcesdt09.toInternal()
										+ rcesdt10.toInternal()
										+ rcesdt11.toInternal()
										+ rcesdt12.toInternal()
										+ rcesdt13.toInternal()
										+ rcesdt14.toInternal()
										+ rcesdt15.toInternal()
										+ rcesdt16.toInternal()
										+ rcesdt17.toInternal()
										+ rcesdt18.toInternal()
										+ rcesdt19.toInternal()
										+ rcesdt20.toInternal());
	}
	public void setRcesdts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getRcesdts().getLength()).init(obj);
	
		what = ExternalData.chop(what, rcesdt01);
		what = ExternalData.chop(what, rcesdt02);
		what = ExternalData.chop(what, rcesdt03);
		what = ExternalData.chop(what, rcesdt04);
		what = ExternalData.chop(what, rcesdt05);
		what = ExternalData.chop(what, rcesdt06);
		what = ExternalData.chop(what, rcesdt07);
		what = ExternalData.chop(what, rcesdt08);
		what = ExternalData.chop(what, rcesdt09);
		what = ExternalData.chop(what, rcesdt10);
		what = ExternalData.chop(what, rcesdt11);
		what = ExternalData.chop(what, rcesdt12);
		what = ExternalData.chop(what, rcesdt13);
		what = ExternalData.chop(what, rcesdt14);
		what = ExternalData.chop(what, rcesdt15);
		what = ExternalData.chop(what, rcesdt16);
		what = ExternalData.chop(what, rcesdt17);
		what = ExternalData.chop(what, rcesdt18);
		what = ExternalData.chop(what, rcesdt19);
		what = ExternalData.chop(what, rcesdt20);
	}
	public FixedLengthStringData getRcesdt(BaseData indx) {
		return getRcesdt(indx.toInt());
	}
	public FixedLengthStringData getRcesdt(int indx) {

		switch (indx) {
			case 1 : return rcesdt01;
			case 2 : return rcesdt02;
			case 3 : return rcesdt03;
			case 4 : return rcesdt04;
			case 5 : return rcesdt05;
			case 6 : return rcesdt06;
			case 7 : return rcesdt07;
			case 8 : return rcesdt08;
			case 9 : return rcesdt09;
			case 10 : return rcesdt10;
			case 11 : return rcesdt11;
			case 12 : return rcesdt12;
			case 13 : return rcesdt13;
			case 14 : return rcesdt14;
			case 15 : return rcesdt15;
			case 16 : return rcesdt16;
			case 17 : return rcesdt17;
			case 18 : return rcesdt18;
			case 19 : return rcesdt19;
			case 20 : return rcesdt20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setRcesdt(BaseData indx, Object what) {
		setRcesdt(indx.toInt(), what);
	}
	public void setRcesdt(int indx, Object what) {

		switch (indx) {
			case 1 : setRcesdt01(what);
					 break;
			case 2 : setRcesdt02(what);
					 break;
			case 3 : setRcesdt03(what);
					 break;
			case 4 : setRcesdt04(what);
					 break;
			case 5 : setRcesdt05(what);
					 break;
			case 6 : setRcesdt06(what);
					 break;
			case 7 : setRcesdt07(what);
					 break;
			case 8 : setRcesdt08(what);
					 break;
			case 9 : setRcesdt09(what);
					 break;
			case 10 : setRcesdt10(what);
					 break;
			case 11 : setRcesdt11(what);
					 break;
			case 12 : setRcesdt12(what);
					 break;
			case 13 : setRcesdt13(what);
					 break;
			case 14 : setRcesdt14(what);
					 break;
			case 15 : setRcesdt15(what);
					 break;
			case 16 : setRcesdt16(what);
					 break;
			case 17 : setRcesdt17(what);
					 break;
			case 18 : setRcesdt18(what);
					 break;
			case 19 : setRcesdt19(what);
					 break;
			case 20 : setRcesdt20(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getMatamts() {
		return new FixedLengthStringData(matamt01.toInternal()
										+ matamt02.toInternal()
										+ matamt03.toInternal()
										+ matamt04.toInternal()
										+ matamt05.toInternal()
										+ matamt06.toInternal()
										+ matamt07.toInternal()
										+ matamt08.toInternal()
										+ matamt09.toInternal()
										+ matamt10.toInternal()
										+ matamt11.toInternal()
										+ matamt12.toInternal()
										+ matamt13.toInternal()
										+ matamt14.toInternal()
										+ matamt15.toInternal()
										+ matamt16.toInternal()
										+ matamt17.toInternal()
										+ matamt18.toInternal()
										+ matamt19.toInternal()
										+ matamt20.toInternal()
										+ matamt21.toInternal());
	}
	public void setMatamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getMatamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, matamt01);
		what = ExternalData.chop(what, matamt02);
		what = ExternalData.chop(what, matamt03);
		what = ExternalData.chop(what, matamt04);
		what = ExternalData.chop(what, matamt05);
		what = ExternalData.chop(what, matamt06);
		what = ExternalData.chop(what, matamt07);
		what = ExternalData.chop(what, matamt08);
		what = ExternalData.chop(what, matamt09);
		what = ExternalData.chop(what, matamt10);
		what = ExternalData.chop(what, matamt11);
		what = ExternalData.chop(what, matamt12);
		what = ExternalData.chop(what, matamt13);
		what = ExternalData.chop(what, matamt14);
		what = ExternalData.chop(what, matamt15);
		what = ExternalData.chop(what, matamt16);
		what = ExternalData.chop(what, matamt17);
		what = ExternalData.chop(what, matamt18);
		what = ExternalData.chop(what, matamt19);
		what = ExternalData.chop(what, matamt20);
		what = ExternalData.chop(what, matamt21);
	}
	public PackedDecimalData getMatamt(BaseData indx) {
		return getMatamt(indx.toInt());
	}
	public PackedDecimalData getMatamt(int indx) {

		switch (indx) {
			case 1 : return matamt01;
			case 2 : return matamt02;
			case 3 : return matamt03;
			case 4 : return matamt04;
			case 5 : return matamt05;
			case 6 : return matamt06;
			case 7 : return matamt07;
			case 8 : return matamt08;
			case 9 : return matamt09;
			case 10 : return matamt10;
			case 11 : return matamt11;
			case 12 : return matamt12;
			case 13 : return matamt13;
			case 14 : return matamt14;
			case 15 : return matamt15;
			case 16 : return matamt16;
			case 17 : return matamt17;
			case 18 : return matamt18;
			case 19 : return matamt19;
			case 20 : return matamt20;
			case 21 : return matamt21;
			default: return null; // Throw error instead?
		}
	
	}
	public void setMatamt(BaseData indx, Object what) {
		setMatamt(indx, what, false);
	}
	public void setMatamt(BaseData indx, Object what, boolean rounded) {
		setMatamt(indx.toInt(), what, rounded);
	}
	public void setMatamt(int indx, Object what) {
		setMatamt(indx, what, false);
	}
	public void setMatamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setMatamt01(what, rounded);
					 break;
			case 2 : setMatamt02(what, rounded);
					 break;
			case 3 : setMatamt03(what, rounded);
					 break;
			case 4 : setMatamt04(what, rounded);
					 break;
			case 5 : setMatamt05(what, rounded);
					 break;
			case 6 : setMatamt06(what, rounded);
					 break;
			case 7 : setMatamt07(what, rounded);
					 break;
			case 8 : setMatamt08(what, rounded);
					 break;
			case 9 : setMatamt09(what, rounded);
					 break;
			case 10 : setMatamt10(what, rounded);
					 break;
			case 11 : setMatamt11(what, rounded);
					 break;
			case 12 : setMatamt12(what, rounded);
					 break;
			case 13 : setMatamt13(what, rounded);
					 break;
			case 14 : setMatamt14(what, rounded);
					 break;
			case 15 : setMatamt15(what, rounded);
					 break;
			case 16 : setMatamt16(what, rounded);
					 break;
			case 17 : setMatamt17(what, rounded);
					 break;
			case 18 : setMatamt18(what, rounded);
					 break;
			case 19 : setMatamt19(what, rounded);
					 break;
			case 20 : setMatamt20(what, rounded);
					 break;
			case 21 : setMatamt21(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getDescrips() {
		return new FixedLengthStringData(descrip01.toInternal()
										+ descrip02.toInternal()
										+ descrip03.toInternal()
										+ descrip04.toInternal()
										+ descrip05.toInternal()
										+ descrip06.toInternal()
										+ descrip07.toInternal()
										+ descrip08.toInternal()
										+ descrip09.toInternal()
										+ descrip10.toInternal()
										+ descrip11.toInternal()
										+ descrip12.toInternal()
										+ descrip13.toInternal()
										+ descrip14.toInternal()
										+ descrip15.toInternal()
										+ descrip16.toInternal()
										+ descrip17.toInternal()
										+ descrip18.toInternal()
										+ descrip19.toInternal()
										+ descrip20.toInternal());
	}
	public void setDescrips(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getDescrips().getLength()).init(obj);
	
		what = ExternalData.chop(what, descrip01);
		what = ExternalData.chop(what, descrip02);
		what = ExternalData.chop(what, descrip03);
		what = ExternalData.chop(what, descrip04);
		what = ExternalData.chop(what, descrip05);
		what = ExternalData.chop(what, descrip06);
		what = ExternalData.chop(what, descrip07);
		what = ExternalData.chop(what, descrip08);
		what = ExternalData.chop(what, descrip09);
		what = ExternalData.chop(what, descrip10);
		what = ExternalData.chop(what, descrip11);
		what = ExternalData.chop(what, descrip12);
		what = ExternalData.chop(what, descrip13);
		what = ExternalData.chop(what, descrip14);
		what = ExternalData.chop(what, descrip15);
		what = ExternalData.chop(what, descrip16);
		what = ExternalData.chop(what, descrip17);
		what = ExternalData.chop(what, descrip18);
		what = ExternalData.chop(what, descrip19);
		what = ExternalData.chop(what, descrip20);
	}
	public FixedLengthStringData getDescrip(BaseData indx) {
		return getDescrip(indx.toInt());
	}
	public FixedLengthStringData getDescrip(int indx) {

		switch (indx) {
			case 1 : return descrip01;
			case 2 : return descrip02;
			case 3 : return descrip03;
			case 4 : return descrip04;
			case 5 : return descrip05;
			case 6 : return descrip06;
			case 7 : return descrip07;
			case 8 : return descrip08;
			case 9 : return descrip09;
			case 10 : return descrip10;
			case 11 : return descrip11;
			case 12 : return descrip12;
			case 13 : return descrip13;
			case 14 : return descrip14;
			case 15 : return descrip15;
			case 16 : return descrip16;
			case 17 : return descrip17;
			case 18 : return descrip18;
			case 19 : return descrip19;
			case 20 : return descrip20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setDescrip(BaseData indx, Object what) {
		setDescrip(indx.toInt(), what);
	}
	public void setDescrip(int indx, Object what) {

		switch (indx) {
			case 1 : setDescrip01(what);
					 break;
			case 2 : setDescrip02(what);
					 break;
			case 3 : setDescrip03(what);
					 break;
			case 4 : setDescrip04(what);
					 break;
			case 5 : setDescrip05(what);
					 break;
			case 6 : setDescrip06(what);
					 break;
			case 7 : setDescrip07(what);
					 break;
			case 8 : setDescrip08(what);
					 break;
			case 9 : setDescrip09(what);
					 break;
			case 10 : setDescrip10(what);
					 break;
			case 11 : setDescrip11(what);
					 break;
			case 12 : setDescrip12(what);
					 break;
			case 13 : setDescrip13(what);
					 break;
			case 14 : setDescrip14(what);
					 break;
			case 15 : setDescrip15(what);
					 break;
			case 16 : setDescrip16(what);
					 break;
			case 17 : setDescrip17(what);
					 break;
			case 18 : setDescrip18(what);
					 break;
			case 19 : setDescrip19(what);
					 break;
			case 20 : setDescrip20(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getCurrcds() {
		return new FixedLengthStringData(currcd01.toInternal()
										+ currcd02.toInternal()
										+ currcd03.toInternal()
										+ currcd04.toInternal()
										+ currcd05.toInternal()
										+ currcd06.toInternal()
										+ currcd07.toInternal()
										+ currcd08.toInternal()
										+ currcd09.toInternal()
										+ currcd10.toInternal()
										+ currcd11.toInternal()
										+ currcd12.toInternal()
										+ currcd13.toInternal()
										+ currcd14.toInternal()
										+ currcd15.toInternal()
										+ currcd16.toInternal()
										+ currcd17.toInternal()
										+ currcd18.toInternal()
										+ currcd19.toInternal()
										+ currcd20.toInternal());
	}
	public void setCurrcds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getCurrcds().getLength()).init(obj);
	
		what = ExternalData.chop(what, currcd01);
		what = ExternalData.chop(what, currcd02);
		what = ExternalData.chop(what, currcd03);
		what = ExternalData.chop(what, currcd04);
		what = ExternalData.chop(what, currcd05);
		what = ExternalData.chop(what, currcd06);
		what = ExternalData.chop(what, currcd07);
		what = ExternalData.chop(what, currcd08);
		what = ExternalData.chop(what, currcd09);
		what = ExternalData.chop(what, currcd10);
		what = ExternalData.chop(what, currcd11);
		what = ExternalData.chop(what, currcd12);
		what = ExternalData.chop(what, currcd13);
		what = ExternalData.chop(what, currcd14);
		what = ExternalData.chop(what, currcd15);
		what = ExternalData.chop(what, currcd16);
		what = ExternalData.chop(what, currcd17);
		what = ExternalData.chop(what, currcd18);
		what = ExternalData.chop(what, currcd19);
		what = ExternalData.chop(what, currcd20);
	}
	public FixedLengthStringData getCurrcd(BaseData indx) {
		return getCurrcd(indx.toInt());
	}
	public FixedLengthStringData getCurrcd(int indx) {

		switch (indx) {
			case 1 : return currcd01;
			case 2 : return currcd02;
			case 3 : return currcd03;
			case 4 : return currcd04;
			case 5 : return currcd05;
			case 6 : return currcd06;
			case 7 : return currcd07;
			case 8 : return currcd08;
			case 9 : return currcd09;
			case 10 : return currcd10;
			case 11 : return currcd11;
			case 12 : return currcd12;
			case 13 : return currcd13;
			case 14 : return currcd14;
			case 15 : return currcd15;
			case 16 : return currcd16;
			case 17 : return currcd17;
			case 18 : return currcd18;
			case 19 : return currcd19;
			case 20 : return currcd20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setCurrcd(BaseData indx, Object what) {
		setCurrcd(indx.toInt(), what);
	}
	public void setCurrcd(int indx, Object what) {

		switch (indx) {
			case 1 : setCurrcd01(what);
					 break;
			case 2 : setCurrcd02(what);
					 break;
			case 3 : setCurrcd03(what);
					 break;
			case 4 : setCurrcd04(what);
					 break;
			case 5 : setCurrcd05(what);
					 break;
			case 6 : setCurrcd06(what);
					 break;
			case 7 : setCurrcd07(what);
					 break;
			case 8 : setCurrcd08(what);
					 break;
			case 9 : setCurrcd09(what);
					 break;
			case 10 : setCurrcd10(what);
					 break;
			case 11 : setCurrcd11(what);
					 break;
			case 12 : setCurrcd12(what);
					 break;
			case 13 : setCurrcd13(what);
					 break;
			case 14 : setCurrcd14(what);
					 break;
			case 15 : setCurrcd15(what);
					 break;
			case 16 : setCurrcd16(what);
					 break;
			case 17 : setCurrcd17(what);
					 break;
			case 18 : setCurrcd18(what);
					 break;
			case 19 : setCurrcd19(what);
					 break;
			case 20 : setCurrcd20(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getCrtables() {
		return new FixedLengthStringData(crtable01.toInternal()
										+ crtable02.toInternal()
										+ crtable03.toInternal()
										+ crtable04.toInternal()
										+ crtable05.toInternal()
										+ crtable06.toInternal()
										+ crtable07.toInternal()
										+ crtable08.toInternal()
										+ crtable09.toInternal()
										+ crtable10.toInternal()
										+ crtable11.toInternal()
										+ crtable12.toInternal()
										+ crtable13.toInternal()
										+ crtable14.toInternal()
										+ crtable15.toInternal()
										+ crtable16.toInternal()
										+ crtable17.toInternal()
										+ crtable18.toInternal()
										+ crtable19.toInternal()
										+ crtable20.toInternal());
	}
	public void setCrtables(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getCrtables().getLength()).init(obj);
	
		what = ExternalData.chop(what, crtable01);
		what = ExternalData.chop(what, crtable02);
		what = ExternalData.chop(what, crtable03);
		what = ExternalData.chop(what, crtable04);
		what = ExternalData.chop(what, crtable05);
		what = ExternalData.chop(what, crtable06);
		what = ExternalData.chop(what, crtable07);
		what = ExternalData.chop(what, crtable08);
		what = ExternalData.chop(what, crtable09);
		what = ExternalData.chop(what, crtable10);
		what = ExternalData.chop(what, crtable11);
		what = ExternalData.chop(what, crtable12);
		what = ExternalData.chop(what, crtable13);
		what = ExternalData.chop(what, crtable14);
		what = ExternalData.chop(what, crtable15);
		what = ExternalData.chop(what, crtable16);
		what = ExternalData.chop(what, crtable17);
		what = ExternalData.chop(what, crtable18);
		what = ExternalData.chop(what, crtable19);
		what = ExternalData.chop(what, crtable20);
	}
	public FixedLengthStringData getCrtable(BaseData indx) {
		return getCrtable(indx.toInt());
	}
	public FixedLengthStringData getCrtable(int indx) {

		switch (indx) {
			case 1 : return crtable01;
			case 2 : return crtable02;
			case 3 : return crtable03;
			case 4 : return crtable04;
			case 5 : return crtable05;
			case 6 : return crtable06;
			case 7 : return crtable07;
			case 8 : return crtable08;
			case 9 : return crtable09;
			case 10 : return crtable10;
			case 11 : return crtable11;
			case 12 : return crtable12;
			case 13 : return crtable13;
			case 14 : return crtable14;
			case 15 : return crtable15;
			case 16 : return crtable16;
			case 17 : return crtable17;
			case 18 : return crtable18;
			case 19 : return crtable19;
			case 20 : return crtable20;
			default: return null; // Throw error instead?
		}
	
	}
	public void setCrtable(BaseData indx, Object what) {
		setCrtable(indx.toInt(), what);
	}
	public void setCrtable(int indx, Object what) {

		switch (indx) {
			case 1 : setCrtable01(what);
					 break;
			case 2 : setCrtable02(what);
					 break;
			case 3 : setCrtable03(what);
					 break;
			case 4 : setCrtable04(what);
					 break;
			case 5 : setCrtable05(what);
					 break;
			case 6 : setCrtable06(what);
					 break;
			case 7 : setCrtable07(what);
					 break;
			case 8 : setCrtable08(what);
					 break;
			case 9 : setCrtable09(what);
					 break;
			case 10 : setCrtable10(what);
					 break;
			case 11 : setCrtable11(what);
					 break;
			case 12 : setCrtable12(what);
					 break;
			case 13 : setCrtable13(what);
					 break;
			case 14 : setCrtable14(what);
					 break;
			case 15 : setCrtable15(what);
					 break;
			case 16 : setCrtable16(what);
					 break;
			case 17 : setCrtable17(what);
					 break;
			case 18 : setCrtable18(what);
					 break;
			case 19 : setCrtable19(what);
					 break;
			case 20 : setCrtable20(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getCltaddrs() {
		return new FixedLengthStringData(cltaddr01.toInternal()
										+ cltaddr02.toInternal()
										+ cltaddr03.toInternal()
										+ cltaddr04.toInternal()
										+ cltaddr05.toInternal());
	}
	public void setCltaddrs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getCltaddrs().getLength()).init(obj);
	
		what = ExternalData.chop(what, cltaddr01);
		what = ExternalData.chop(what, cltaddr02);
		what = ExternalData.chop(what, cltaddr03);
		what = ExternalData.chop(what, cltaddr04);
		what = ExternalData.chop(what, cltaddr05);
	}
	public FixedLengthStringData getCltaddr(BaseData indx) {
		return getCltaddr(indx.toInt());
	}
	public FixedLengthStringData getCltaddr(int indx) {

		switch (indx) {
			case 1 : return cltaddr01;
			case 2 : return cltaddr02;
			case 3 : return cltaddr03;
			case 4 : return cltaddr04;
			case 5 : return cltaddr05;
			default: return null; // Throw error instead?
		}
	
	}
	public void setCltaddr(BaseData indx, Object what) {
		setCltaddr(indx.toInt(), what);
	}
	public void setCltaddr(int indx, Object what) {

		switch (indx) {
			case 1 : setCltaddr01(what);
					 break;
			case 2 : setCltaddr02(what);
					 break;
			case 3 : setCltaddr03(what);
					 break;
			case 4 : setCltaddr04(what);
					 break;
			case 5 : setCltaddr05(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBonusamts() {
		return new FixedLengthStringData(bonusAmt01.toInternal()
										+ bonusAmt02.toInternal()
										+ bonusAmt03.toInternal()
										+ bonusAmt04.toInternal()
										+ bonusAmt05.toInternal()
										+ bonusAmt06.toInternal()
										+ bonusAmt07.toInternal()
										+ bonusAmt08.toInternal()
										+ bonusAmt09.toInternal()
										+ bonusAmt10.toInternal()
										+ bonusAmt11.toInternal()
										+ bonusAmt12.toInternal()
										+ bonusAmt13.toInternal()
										+ bonusAmt14.toInternal()
										+ bonusAmt15.toInternal()
										+ bonusAmt16.toInternal()
										+ bonusAmt17.toInternal()
										+ bonusAmt18.toInternal()
										+ bonusAmt19.toInternal()
										+ bonusAmt20.toInternal()
										+ bonusAmt21.toInternal());
	}
	public void setBonusamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBonusamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, bonusAmt01);
		what = ExternalData.chop(what, bonusAmt02);
		what = ExternalData.chop(what, bonusAmt03);
		what = ExternalData.chop(what, bonusAmt04);
		what = ExternalData.chop(what, bonusAmt05);
		what = ExternalData.chop(what, bonusAmt06);
		what = ExternalData.chop(what, bonusAmt07);
		what = ExternalData.chop(what, bonusAmt08);
		what = ExternalData.chop(what, bonusAmt09);
		what = ExternalData.chop(what, bonusAmt10);
		what = ExternalData.chop(what, bonusAmt11);
		what = ExternalData.chop(what, bonusAmt12);
		what = ExternalData.chop(what, bonusAmt13);
		what = ExternalData.chop(what, bonusAmt14);
		what = ExternalData.chop(what, bonusAmt15);
		what = ExternalData.chop(what, bonusAmt16);
		what = ExternalData.chop(what, bonusAmt17);
		what = ExternalData.chop(what, bonusAmt18);
		what = ExternalData.chop(what, bonusAmt19);
		what = ExternalData.chop(what, bonusAmt20);
		what = ExternalData.chop(what, bonusAmt21);
	}
	public PackedDecimalData getBonusamt(BaseData indx) {
		return getBonusamt(indx.toInt());
	}
	public PackedDecimalData getBonusamt(int indx) {

		switch (indx) {
			case 1 : return bonusAmt01;
			case 2 : return bonusAmt02;
			case 3 : return bonusAmt03;
			case 4 : return bonusAmt04;
			case 5 : return bonusAmt05;
			case 6 : return bonusAmt06;
			case 7 : return bonusAmt07;
			case 8 : return bonusAmt08;
			case 9 : return bonusAmt09;
			case 10 : return bonusAmt10;
			case 11 : return bonusAmt11;
			case 12 : return bonusAmt12;
			case 13 : return bonusAmt13;
			case 14 : return bonusAmt14;
			case 15 : return bonusAmt15;
			case 16 : return bonusAmt16;
			case 17 : return bonusAmt17;
			case 18 : return bonusAmt18;
			case 19 : return bonusAmt19;
			case 20 : return bonusAmt20;
			case 21 : return bonusAmt21;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBonusamt(BaseData indx, Object what) {
		setBonusamt(indx, what, false);
	}
	public void setBonusamt(BaseData indx, Object what, boolean rounded) {
		setBonusamt(indx.toInt(), what, rounded);
	}
	public void setBonusamt(int indx, Object what) {
		setBonusamt(indx, what, false);
	}
	public void setBonusamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBonusAmt01(what, rounded);
					 break;
			case 2 : setBonusAmt02(what, rounded);
					 break;
			case 3 : setBonusAmt03(what, rounded);
					 break;
			case 4 : setBonusAmt04(what, rounded);
					 break;
			case 5 : setBonusAmt05(what, rounded);
					 break;
			case 6 : setBonusAmt06(what, rounded);
					 break;
			case 7 : setBonusAmt07(what, rounded);
					 break;
			case 8 : setBonusAmt08(what, rounded);
					 break;
			case 9 : setBonusAmt09(what, rounded);
					 break;
			case 10 : setBonusAmt10(what, rounded);
					 break;
			case 11 : setBonusAmt11(what, rounded);
					 break;
			case 12 : setBonusAmt12(what, rounded);
					 break;
			case 13 : setBonusAmt13(what, rounded);
					 break;
			case 14 : setBonusAmt14(what, rounded);
					 break;
			case 15 : setBonusAmt15(what, rounded);
					 break;
			case 16 : setBonusAmt16(what, rounded);
					 break;
			case 17 : setBonusAmt17(what, rounded);
					 break;
			case 18 : setBonusAmt18(what, rounded);
					 break;
			case 19 : setBonusAmt19(what, rounded);
					 break;
			case 20 : setBonusAmt20(what, rounded);
					 break;
			case 21 : setBonusAmt21(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		cowncoy.clear();
		cownnum.clear();
		trandate.clear();
		transactionTime.clear();
		jlife.clear();
		salut.clear();
		surname.clear();
		givname.clear();
		initials.clear();
		cltaddr01.clear();
		cltaddr02.clear();
		cltaddr03.clear();
		cltaddr04.clear();
		cltaddr05.clear();
		cltpcode.clear();
		crtable01.clear();
		crtable02.clear();
		crtable03.clear();
		crtable04.clear();
		crtable05.clear();
		crtable06.clear();
		crtable07.clear();
		crtable08.clear();
		crtable09.clear();
		crtable10.clear();
		crtable11.clear();
		crtable12.clear();
		crtable13.clear();
		crtable14.clear();
		crtable15.clear();
		crtable16.clear();
		crtable17.clear();
		crtable18.clear();
		crtable19.clear();
		crtable20.clear();
		bonusAmt01.clear();
		bonusAmt02.clear();
		bonusAmt03.clear();
		bonusAmt04.clear();
		bonusAmt05.clear();
		bonusAmt06.clear();
		bonusAmt07.clear();
		bonusAmt08.clear();
		bonusAmt09.clear();
		bonusAmt10.clear();
		bonusAmt11.clear();
		bonusAmt12.clear();
		bonusAmt13.clear();
		bonusAmt14.clear();
		bonusAmt15.clear();
		bonusAmt16.clear();
		bonusAmt17.clear();
		bonusAmt18.clear();
		bonusAmt19.clear();
		bonusAmt20.clear();
		bonusAmt21.clear();
		sumins01.clear();
		sumins02.clear();
		sumins03.clear();
		sumins04.clear();
		sumins05.clear();
		sumins06.clear();
		sumins07.clear();
		sumins08.clear();
		sumins09.clear();
		sumins10.clear();
		sumins11.clear();
		sumins12.clear();
		sumins13.clear();
		sumins14.clear();
		sumins15.clear();
		sumins16.clear();
		sumins17.clear();
		sumins18.clear();
		sumins19.clear();
		sumins20.clear();
		sumins21.clear();
		matamt01.clear();
		matamt02.clear();
		matamt03.clear();
		matamt04.clear();
		matamt05.clear();
		matamt06.clear();
		matamt07.clear();
		matamt08.clear();
		matamt09.clear();
		matamt10.clear();
		matamt11.clear();
		matamt12.clear();
		matamt13.clear();
		matamt14.clear();
		matamt15.clear();
		matamt16.clear();
		matamt17.clear();
		matamt18.clear();
		matamt19.clear();
		matamt20.clear();
		matamt21.clear();
		virtualFund01.clear();
		virtualFund02.clear();
		virtualFund03.clear();
		virtualFund04.clear();
		virtualFund05.clear();
		virtualFund06.clear();
		virtualFund07.clear();
		virtualFund08.clear();
		virtualFund09.clear();
		virtualFund10.clear();
		virtualFund11.clear();
		virtualFund12.clear();
		virtualFund13.clear();
		virtualFund14.clear();
		virtualFund15.clear();
		virtualFund16.clear();
		virtualFund17.clear();
		virtualFund18.clear();
		virtualFund19.clear();
		virtualFund20.clear();
		fieldType01.clear();
		fieldType02.clear();
		fieldType03.clear();
		fieldType04.clear();
		fieldType05.clear();
		fieldType06.clear();
		fieldType07.clear();
		fieldType08.clear();
		fieldType09.clear();
		fieldType10.clear();
		fieldType11.clear();
		fieldType12.clear();
		fieldType13.clear();
		fieldType14.clear();
		fieldType15.clear();
		fieldType16.clear();
		fieldType17.clear();
		fieldType18.clear();
		fieldType19.clear();
		fieldType20.clear();
		descrip01.clear();
		descrip02.clear();
		descrip03.clear();
		descrip04.clear();
		descrip05.clear();
		descrip06.clear();
		descrip07.clear();
		descrip08.clear();
		descrip09.clear();
		descrip10.clear();
		descrip11.clear();
		descrip12.clear();
		descrip13.clear();
		descrip14.clear();
		descrip15.clear();
		descrip16.clear();
		descrip17.clear();
		descrip18.clear();
		descrip19.clear();
		descrip20.clear();
		currcd01.clear();
		currcd02.clear();
		currcd03.clear();
		currcd04.clear();
		currcd05.clear();
		currcd06.clear();
		currcd07.clear();
		currcd08.clear();
		currcd09.clear();
		currcd10.clear();
		currcd11.clear();
		currcd12.clear();
		currcd13.clear();
		currcd14.clear();
		currcd15.clear();
		currcd16.clear();
		currcd17.clear();
		currcd18.clear();
		currcd19.clear();
		currcd20.clear();
		rcesdt01.clear();
		rcesdt02.clear();
		rcesdt03.clear();
		rcesdt04.clear();
		rcesdt05.clear();
		rcesdt06.clear();
		rcesdt07.clear();
		rcesdt08.clear();
		rcesdt09.clear();
		rcesdt10.clear();
		rcesdt11.clear();
		rcesdt12.clear();
		rcesdt13.clear();
		rcesdt14.clear();
		rcesdt15.clear();
		rcesdt16.clear();
		rcesdt17.clear();
		rcesdt18.clear();
		rcesdt19.clear();
		rcesdt20.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}