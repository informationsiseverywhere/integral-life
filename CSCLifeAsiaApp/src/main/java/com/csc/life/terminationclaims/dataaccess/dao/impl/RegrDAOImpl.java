package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.RegrDAO;
import com.csc.life.terminationclaims.dataaccess.model.Regrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;



public class RegrDAOImpl extends BaseDAOImpl<Regrpf>  implements RegrDAO{
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RegrDAOImpl.class);	
	
	public	List<Regrpf> getRegrpfList(String programName, String chrdnum,String chdrnum1){
		
	            List< Regrpf > list = new ArrayList< Regrpf >();
	            
	            try
	            {// added for ILIFE-3168
	            //IJTI-306 START
	            StringBuilder sql = new StringBuilder("select UNIQUE_NUMBER,CHDRCOY,CHDRNUM, LIFE, COVERAGE, RIDER, RGPYNUM, PYMT, CURRCD, PRCNT, RGPYTYPE, ");
	            sql.append("PAYREASON, RGPYSTAT, REVDTE,FPAYDATE, LPAYDATE, PROGNAME, EXCODE, EXREPORT, CRTABLE, TRANNO, USRPRF, JOBNM, DATIME from regrpf where CHDRNUM between ");
	            //sql.append(chrdnum)
	            sql.append(" ? and ? ");
	            //sql.append(chdrnum1)
	            sql.append(" and PROGNAME= ? ");
	            //sql.append(programName)
	            sql.append(" order by PROGNAME ASC, EXREPORT ASC,CHDRNUM ASC, RGPYTYPE ASC, CRTABLE ASC, TRANNO DESC, UNIQUE_NUMBER DESC");
	            ResultSet rs = null;
	            PreparedStatement stmt1 = null;        
	            stmt1 = getPrepareStatement(sql.toString());
	            stmt1.setString(1, chrdnum);
	            stmt1.setString(2, chdrnum1);
	            stmt1.setString(3, programName);
	            rs =   stmt1.executeQuery();
	            //IJTI-306 END
	            try {
	                 
	            	Regrpf regrpf = null;
	                while(rs.next())
	                {
	                	 regrpf = new Regrpf();
	                	 regrpf.setChdrcoy(rs.getString("CHDRCOY"));
	                	 regrpf.setChdrnum(rs.getString("chdrnum"));
	                	 regrpf.setLife(rs.getString("life"));
	                	 regrpf.setCoverage(rs.getString("coverage"));
	                	 regrpf.setRider(rs.getString("rider"));
	                	 regrpf.setRgpynum(rs.getInt("rgpynum"));
	                	 regrpf.setPymt(rs.getBigDecimal("pymt"));
	                	 regrpf.setCurrcd(rs.getString("currcd"));
	                	 regrpf.setPrcnt(rs.getBigDecimal("prcnt"));
	                	 regrpf.setRgpytype(rs.getString("rgpytype"));
	                	 regrpf.setPayreason(rs.getString("PAYREASON"));
	                	 regrpf.setRgpystat(rs.getString("RGPYSTAT"));
	                	 regrpf.setRevdte(rs.getInt("REVDTE"));
	                	 regrpf.setFirstPaydate(rs.getInt("FPAYDATE"));
	                	 regrpf.setLastPaydate(rs.getInt("LPAYDATE"));
	                	 regrpf.setProgname(rs.getString("PROGNAME"));
	                	 regrpf.setExcode(rs.getString("EXCODE"));
	                	 regrpf.setExreport(rs.getString("EXREPORT"));
	                	 regrpf.setCrtable(rs.getString("CRTABLE"));
	                	 regrpf.setTranno(rs.getInt("TRANNO"));
	                	 regrpf.setUserProfile(rs.getString("USRPRF"));
	                	 regrpf.setJobName(rs.getString("JOBNM"));
	                	 regrpf.setDatime(rs.getString("DATIME"));
	                	 list.add(regrpf);
	                }
	            } catch (SQLException e) {
	                LOGGER.error("getItemsbyItemcodeLikeOperator(): ", e);//IJTI-1561
	                throw new SQLRuntimeException(e);
	            }
	            finally
	            {
	                close(stmt1,rs);
	            }
	            }
	            catch (SQLException ex)
	            {
	             
	            }
	            return list;

	        }
					
	@Override
	public void insertRecords(List<Regrpf> list) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO REGRPF(CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, RGPYNUM, PYMT, CURRCD, PRCNT, RGPYTYPE, PAYREASON, RGPYSTAT, REVDTE, FPAYDATE, PROGNAME, EXCODE, EXREPORT, CRTABLE, TRANNO, USRPRF, JOBNM, DATIME, LPAYDATE)"); 
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "); 
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for (Regrpf payr : list) {
				int i = 1;
				ps.setString(i++, payr.getChdrcoy());
				ps.setString(i++, payr.getChdrnum());
				ps.setString(i++, payr.getLife());
				ps.setString(i++, payr.getCoverage());
				ps.setString(i++, payr.getRider());
				ps.setInt(i++, payr.getRgpynum());
				ps.setBigDecimal(i++, payr.getPymt());
				ps.setString(i++, payr.getCurrcd());
				ps.setBigDecimal(i++, payr.getPrcnt());
				ps.setString(i++, payr.getRgpytype());
				ps.setString(i++, payr.getPayreason());
				ps.setString(i++, payr.getRgpystat());
				ps.setInt(i++, payr.getRevdte());
				ps.setInt(i++, payr.getFirstPaydate());
				ps.setString(i++, payr.getProgname());
				ps.setString(i++, payr.getExcode());
				ps.setString(i++, payr.getExreport());
				ps.setString(i++, payr.getCrtable());
				ps.setInt(i++, payr.getTranno());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.setInt(i++, payr.getLastPaydate());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertRecords()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
	}
		
	}


