package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class Sa513screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {8, 4, 5, 6, 7, 2, 3, 10, 9, 11}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa513ScreenVars sv = (Sa513ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sa513screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sa513screensfl, 
			sv.Sa513screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sa513ScreenVars sv = (Sa513ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sa513screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sa513ScreenVars sv = (Sa513ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sa513screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sa513screensflWritten.gt(0))
		{
			sv.sa513screensfl.setCurrentIndex(0);
			sv.Sa513screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sa513ScreenVars sv = (Sa513ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sa513screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa513ScreenVars screenVars = (Sa513ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");				
				screenVars.select.setFieldName("select");
				screenVars.tranno.setFieldName("tranno");
				screenVars.trdate.setFieldName("trdate");
				screenVars.effdates.setFieldName("effdates");
				screenVars.trancd.setFieldName("trancd");
				screenVars.trandes.setFieldName("trandes");
				screenVars.userid.setFieldName("userid");
				screenVars.transDateDisp.setFieldName("transDateDisp");
				screenVars.effDateDisp.setFieldName("effDateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.tranno.set(dm.getField("tranno"));
			screenVars.trdate.set(dm.getField("trdate"));
			screenVars.effdates.set(dm.getField("effdates"));
			screenVars.trancd.set(dm.getField("trancd"));
			screenVars.trandes.set(dm.getField("trandes"));
			screenVars.userid.set(dm.getField("userid"));
			screenVars.transDateDisp.set(dm.getField("transDateDisp"));
			screenVars.effDateDisp.set(dm.getField("effDateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa513ScreenVars screenVars = (Sa513ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.tranno.setFieldName("tranno");
				screenVars.trdate.setFieldName("trdate");
				screenVars.effdates.setFieldName("effdates");
				screenVars.trancd.setFieldName("trancd");
				screenVars.trandes.setFieldName("trandes");
				screenVars.userid.setFieldName("userid");
				screenVars.transDateDisp.setFieldName("transDateDisp");
				screenVars.effDateDisp.setFieldName("effDateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("tranno").set(screenVars.tranno);
			dm.getField("trdate").set(screenVars.trdate);
			dm.getField("effdates").set(screenVars.effdates);
			dm.getField("trancd").set(screenVars.trancd);
			dm.getField("trandes").set(screenVars.trandes);
			dm.getField("userid").set(screenVars.userid);
			dm.getField("transDateDisp").set(screenVars.transDateDisp);
			dm.getField("effDateDisp").set(screenVars.effDateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sa513screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sa513ScreenVars screenVars = (Sa513ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.tranno.clearFormatting();
		screenVars.trdate.clearFormatting();
		screenVars.effdates.clearFormatting();
		screenVars.trancd.clearFormatting();
		screenVars.trandes.clearFormatting();
		screenVars.userid.clearFormatting();
		screenVars.transDateDisp.clearFormatting();
		screenVars.effDateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sa513ScreenVars screenVars = (Sa513ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.tranno.setClassString("");
		screenVars.trdate.setClassString("");
		screenVars.effdates.setClassString("");
		screenVars.trancd.setClassString("");
		screenVars.trandes.setClassString("");
		screenVars.userid.setClassString("");
		screenVars.transDateDisp.setClassString("");
		screenVars.effDateDisp.setClassString("");
	}

/**
 * Clear all the variables in S5186screensfl
 */
	public static void clear(VarModel pv) {
		Sa513ScreenVars screenVars = (Sa513ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.tranno.clear();
		screenVars.trdate.clear();
		screenVars.effdates.clear();
		screenVars.trancd.clear();
		screenVars.trandes.clear();
		screenVars.userid.clear();
		screenVars.transDateDisp.clear();
		screenVars.effDateDisp.clear();
	}
}
