package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:45
 * Description:
 * Copybook name: REGPUDLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regpudlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regpudlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData regpudlKey = new FixedLengthStringData(256).isAPartOf(regpudlFileKey, 0, REDEFINE);
  	public FixedLengthStringData regpudlChdrcoy = new FixedLengthStringData(1).isAPartOf(regpudlKey, 0);
  	public FixedLengthStringData regpudlChdrnum = new FixedLengthStringData(8).isAPartOf(regpudlKey, 1);
  	public FixedLengthStringData regpudlLife = new FixedLengthStringData(2).isAPartOf(regpudlKey, 9);
  	public FixedLengthStringData regpudlCoverage = new FixedLengthStringData(2).isAPartOf(regpudlKey, 11);
  	public FixedLengthStringData regpudlRider = new FixedLengthStringData(2).isAPartOf(regpudlKey, 13);
  	public PackedDecimalData regpudlPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(regpudlKey, 15);
  	public PackedDecimalData regpudlRgpynum = new PackedDecimalData(5, 0).isAPartOf(regpudlKey, 18);
  	public PackedDecimalData regpudlTranno = new PackedDecimalData(5, 0).isAPartOf(regpudlKey, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(232).isAPartOf(regpudlKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regpudlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regpudlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}