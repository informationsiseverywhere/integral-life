/*
 * File: Pupm003.java
 * Date: 30 August 2009 2:02:46
 * Author: Quipoz Limited
 * 
 * Class transformed from PUPM003.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5585rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.underwriting.procedures.Actcalc;
import com.csc.life.underwriting.recordstructures.Actcalcrec;
import com.csc.life.underwriting.tablestructures.T6642rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  This subroutine calculates the Traditional Business Paid Up
*  Sum Assured for Whole of Life and Endowments.
*
*
*  Paid Up Sum Assured Calculation.
*  --------------------------------
*
*  The Paid Up Sun Assured is defined as follows:
*
*       PUSA = Sum Assured * ( 1 - NETP(t) / NETP(t-x) )
*
*
*  Where :
*     Sum Assured is the Original Sum Assured from the Coverage
*     File
*     NETP is the Net Premium obtained by calling ACTCALC
*     t is the original term
*     x is the term in force
*     i.e.  t-x is the remaining term
*
*  For this Policy/Component:
*
*       - Read Coverage File using the actual plan suffix passed
*         in the linkage (PRESERV)
*       - Get Age Next Birthday at Contract Commencement date
*         (CRRCD) taking into account; joint or single life, age
*         adjustments for females where appropriate (PRESERV).
*       - The ANB at the EFFECTIVE DATE date of the transaction
*         (i.e. the PAID TO DATE will be required).
*       - Get the DURATION, ANB at Premium Cessation Date, ANB at
*       - Risk Cessation Date and Assurance Type as per PRESERV.
*
*  - Calculate the Paid Up Sum Assured.
*    ----------------------------------
*
*     Call ACTCALC twice to get values for Net Premium with
*     parameters as follows:
*
*     For NETP (t)  -  Net Premium for original term
*
*     ACTC-INITIAL-AGE   -   ANB at Contract Commencement Date
*     ACTC-FINAL-AGE-1   -   ANB at Risk Cessation Date
*     ACTC-FINAL-AGE-2   -   ANB at premium Cessation Date
*
*
*     For NETP (t-x) -  Net Premium for remaining term
*
*     ACTC-INITIAL-AGE   -   ANB at Effective Date
*     ACTC-FINAL-AGE-1   -   ANB at Risk Cessation Date
*     ACTC-FINAL-AGE-2   -   ANB at premium Cessation Date
*
*  - Calculate the PUSA  using the formula as given above using
*    the Coverage Sum Assured. This should be returned in the
*    field OVRD-ACTUAL-VAL.
*
*  FILE READS.
*  ----------
*
*  Coverage file for :
*       - determining assurance type (whole life or endowment)
*       - risk and premium cessation ages and terms.
*
*  Life file for :
*       - determining if single or joint life
*       - sex of life (lives) assured
*   - age next birthday at commencement date of lives assured
*
*  PROCESSING.
*  ----------
*
*  INITIAL VALUES.
*  --------------
*
*   - Set run type either WHOLE-PLAN, SINGLE-COMPONENT or
*     NOTIONAL-SINGLE-COMPONENT.
*
*   - BEGN on the COVR file.
*
*   - Determine if single or joint life and sex of life(s)
*     assured.
*
*   - Read T6642 to establish:
*         - Age adjustment for females.
*         - Joint life equivalent age basis (for access to T5585)
*         - Single or joint life calculation basis as
*           appropriate.
*           Bases are read for Sum Assured and the relevant one
*           is set for use by "ACTCALC".
*
*   - Adjust ages if necessary and find joint equivalent age
*     for joint life cases.
*     - A single figure for Age Next Birthday at Contract
*       commencement date is required.
*     - Age adjustment is applied to any female life using the
*       figure read from T6642.
*     - Joint equivalent age is used in joint life cases and uses
*       the method read from T6642. The two ages are first
*       adjusted for females and then passed to T5585 to get a
*       single age equivalent.
*
*   MAIN PROCESSING.
*   ---------------
*
*   - Determine assurance type (whole life or endowment).
*     - If the sum of age next birthday at risk cessation date
*       and the Risk Cessation Term is = 99 then this is, by
*       definition, a whole of life. Otherwise it is an
*       endowment.
*
*   - Calculate NETP(t) using subroutine ACTCALC.
*
*   - Calculate NETP(t-x) using subroutine ACTCALC.
*
*   - Calculate the New Paid-Up Sum Assured using the values
*     from above.
*
*****************************************************************
* </pre>
*/
public class Pupm003 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "PUPM003";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAgeDifference = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaLifeInd = new FixedLengthStringData(1);
	private Validator singleLife = new Validator(wsaaLifeInd, "1");
	private Validator jointLife = new Validator(wsaaLifeInd, "2");
		/* WSAA-LIFE-SEXES */
	private FixedLengthStringData wsaaLife1Sex = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaLife2Sex = new FixedLengthStringData(1);
		/* WSAA-LIFE-AGES */
	private ZonedDecimalData wsaaLife1AnbAtCcd = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaLife2AnbAtCcd = new ZonedDecimalData(3, 0).setUnsigned();

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, 1);
	private Validator partPlan = new Validator(wsaaPlanSwitch, 2);
	private Validator summaryPartPlan = new Validator(wsaaPlanSwitch, 3);
		/* ERRORS */
	private static final String f262 = "F262";
	private static final String g344 = "G344";
	private static final String g579 = "G579";
	private static final String g580 = "G580";
	private static final String g585 = "G585";
	private static final String h053 = "H053";
		/* FORMATS */
	private static final String covrtrbrec = "COVRTRBREC";
	private static final String lifelnbrec = "LIFELNBREC";
		/* TABLES */
	private static final String t6642 = "T6642";
	private static final String t5585 = "T5585";
	private static final String t5687 = "T5687";
	private CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5585rec t5585rec = new T5585rec();
	private T6642rec t6642rec = new T6642rec();
	private Actcalcrec actcalcrec = new Actcalcrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private WsaaExactValuesInner wsaaExactValuesInner = new WsaaExactValuesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		seExit9090, 
		dbExit9190
	}

	public Pupm003() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline1000()
	{
		/*MAIN*/
		initialise2000();
		obtainInitVals3000();
		calculatePusa4000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		syserrrec.subrname.set(wsaaSubr);
		wsaaExactValuesInner.wsaaNetpT.set(ZERO);
		wsaaExactValuesInner.wsaaNetpTX.set(ZERO);
		if (isEQ(ovrduerec.planSuffix, ZERO)) {
			wsaaPlanSwitch.set(1);
		}
		else {
			if (isLTE(ovrduerec.planSuffix, ovrduerec.polsum)) {
				wsaaPlanSwitch.set(3);
			}
			else {
				wsaaPlanSwitch.set(2);
			}
		}
		readFiles2100();
		readT6642SetBasis2200();
		setAnbAtCcd2300();
		getSaDescription2400();
	}

protected void readFiles2100()
	{
		start2100();
	}

protected void start2100()
	{
		/*  Read Coverage File.*/
		covrtrbIO.setDataArea(SPACES);
		covrtrbIO.setChdrcoy(ovrduerec.chdrcoy);
		covrtrbIO.setChdrnum(ovrduerec.chdrnum);
		covrtrbIO.setLife(ovrduerec.life);
		covrtrbIO.setCoverage(ovrduerec.coverage);
		covrtrbIO.setRider(ovrduerec.rider);
		covrtrbIO.setPlanSuffix(ovrduerec.planSuffix);
		/*  Check for summary plan.*/
		if (summaryPartPlan.isTrue()) {
			covrtrbIO.setPlanSuffix(0);
		}
		covrtrbIO.setFormat(covrtrbrec);
		covrtrbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(), varcom.oK)
		&& isNE(covrtrbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrtrbIO.getParams());
			dbError9100();
		}
		if (isEQ(covrtrbIO.getStatuz(), varcom.mrnf)) {
			wsaaExactValuesInner.wsaaPlanSuffix.set(ovrduerec.planSuffix);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(ovrduerec.chdrcoy);
			stringVariable1.addExpression(ovrduerec.chdrnum);
			stringVariable1.addExpression(ovrduerec.life);
			stringVariable1.addExpression(ovrduerec.coverage);
			stringVariable1.addExpression(ovrduerec.rider);
			stringVariable1.addExpression(wsaaExactValuesInner.wsaaPlanSuffix);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(g344);
			dbError9100();
		}
		/*  Read Life file for first life.*/
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setChdrcoy(ovrduerec.chdrcoy);
		lifelnbIO.setChdrnum(ovrduerec.chdrnum);
		lifelnbIO.setLife(ovrduerec.life);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			dbError9100();
		}
		wsaaLife1Sex.set(lifelnbIO.getCltsex());
		wsaaLife1AnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		/*   Read Life file for second life, if not found then*/
		/*   this must be a single life coverage.*/
		lifelnbIO.setChdrcoy(ovrduerec.chdrcoy);
		lifelnbIO.setChdrnum(ovrduerec.chdrnum);
		lifelnbIO.setLife(ovrduerec.life);
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			dbError9100();
		}
		/*  Set Life indicator and, if joint life, 2nd life sex/anb.*/
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			wsaaLife2Sex.set(SPACES);
			wsaaLifeInd.set("1");
		}
		else {
			wsaaLife2Sex.set(lifelnbIO.getCltsex());
			wsaaLife2AnbAtCcd.set(lifelnbIO.getAnbAtCcd());
			wsaaLifeInd.set("2");
		}
	}

protected void readT6642SetBasis2200()
	{
		start2200();
	}

protected void start2200()
	{
		/*  Read T6642 to get age adjustment for females, joint equivalent*/
		/*  age basis, calculation basis.*/
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(t6642);
		itdmIO.setItemitem(ovrduerec.crtable);
		itdmIO.setItmfrm(ovrduerec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t6642)
		|| isNE(itdmIO.getItemitem(), ovrduerec.crtable)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(ovrduerec.crtable);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g579);
			dbError9100();
		}
		else {
			t6642rec.t6642Rec.set(itdmIO.getGenarea());
		}
		/*  Set the calculation basis according to type of run and*/
		/*  number of lives assured.*/
		/*  Calculate the Sum Assured reserve.*/
		/*  Use joint or single life basis as appropriate.*/
		if (singleLife.isTrue()) {
			actcalcrec.basis.set(t6642rec.slsumass);
		}
		else {
			actcalcrec.basis.set(t6642rec.jlsumass);
		}
		/*  If joint life, check that a joint life equivalent age basis*/
		/*  is present on T6642.*/
		if (jointLife.isTrue()) {
			if (isEQ(t6642rec.jleqage, SPACES)) {
				syserrrec.params.set(ovrduerec.crtable);
				syserrrec.statuz.set(g585);
				dbError9100();
			}
		}
	}

protected void setAnbAtCcd2300()
	{
		start2300();
	}

protected void start2300()
	{
		/*  Set Age Next Birthday at Contract Commencement Date*/
		/*  for Life 1. First, adjust for female if necessary.*/
		if (isEQ(wsaaLife1Sex, "F")) {
			wsaaLife1AnbAtCcd.add(t6642rec.fmageadj);
		}
		/*  Is this Single or Joint Life ie. for ANB-AT-CCD, is further*/
		/*  processing necessary.*/
		if (singleLife.isTrue()) {
			wsaaExactValuesInner.wsaaAnbAtCcdT.set(wsaaLife1AnbAtCcd);
			return ;
		}
		/*  Set Age Next Birthday at Contract Commencement Date*/
		/*  for Life 2. First, adjust for female if necessary.*/
		if (isEQ(wsaaLife2Sex, "F")) {
			wsaaLife2AnbAtCcd.add(t6642rec.fmageadj);
		}
		/*    Read T5585 to find single age equivalent for joint life*/
		/*    case.*/
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(ovrduerec.chdrcoy);
		itdmIO.setItemtabl(t5585);
		itdmIO.setItemitem(t6642rec.jleqage);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(), ovrduerec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5585)
		|| isNE(itdmIO.getItemitem(), t6642rec.jleqage)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(t6642rec.jleqage);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g580);
			dbError9100();
		}
		else {
			t5585rec.t5585Rec.set(itdmIO.getGenarea());
		}
		if (isGT(wsaaLife1AnbAtCcd, wsaaLife2AnbAtCcd)) {
			compute(wsaaAgeDifference, 0).set(sub(wsaaLife1AnbAtCcd, wsaaLife2AnbAtCcd));
		}
		else {
			compute(wsaaAgeDifference, 0).set(sub(wsaaLife2AnbAtCcd, wsaaLife1AnbAtCcd));
		}
		/*  Read table data to find appropriate age difference.*/
		/*  First increment subscript to correct table entry.*/
		wsaaSub.set(1);
		while ( !((isGT(wsaaSub, 18))
		|| (isLTE(wsaaAgeDifference, t5585rec.agedif[wsaaSub.toInt()])))) {
			wsaaSub.add(1);
		}
		
		/*  If subscript > 18 then the appropriate table entry not found.*/
		if (isGT(wsaaSub, 18)) {
			syserrrec.statuz.set(f262);
			systemError9000();
		}
		/*  Which age to adjust - highest or lowest ?*/
		/*  Which age is highest or lowest ?*/
		if (isEQ(t5585rec.hghlowage, "H")) {
			if (isGT(wsaaLife1AnbAtCcd, wsaaLife2AnbAtCcd)) {
				compute(wsaaExactValuesInner.wsaaAnbAtCcdT, 0).set(add(wsaaLife1AnbAtCcd, t5585rec.addage[wsaaSub.toInt()]));
			}
			else {
				compute(wsaaExactValuesInner.wsaaAnbAtCcdT, 0).set(add(wsaaLife2AnbAtCcd, t5585rec.addage[wsaaSub.toInt()]));
			}
		}
		if (isEQ(t5585rec.hghlowage, "L")) {
			if (isGT(wsaaLife1AnbAtCcd, wsaaLife2AnbAtCcd)) {
				compute(wsaaExactValuesInner.wsaaAnbAtCcdT, 0).set(add(wsaaLife2AnbAtCcd, t5585rec.addage[wsaaSub.toInt()]));
			}
			else {
				compute(wsaaExactValuesInner.wsaaAnbAtCcdT, 0).set(add(wsaaLife1AnbAtCcd, t5585rec.addage[wsaaSub.toInt()]));
			}
		}
	}

protected void getSaDescription2400()
	{
		start2400();
	}

	/**
	* <pre>
	*  It reads the T5687 using DESCIO to get the component
	*  description.
	* </pre>
	*/
protected void start2400()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ovrduerec.chdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(ovrduerec.crtable);
		descIO.setLanguage(ovrduerec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(h053);
			syserrrec.params.set(descIO.getParams());
			systemError9000();
		}
		if (isEQ(descIO.getShortdesc(), SPACES)) {
			ovrduerec.description.fill("?");
		}
		else {
			ovrduerec.description.set(descIO.getShortdesc());
		}
	}

protected void obtainInitVals3000()
	{
		setAnbAtCessation3010();
	}

protected void setAnbAtCessation3010()
	{
		/*  Calculate the Age Next Birthday at both Risk Cessation Date*/
		/*  and Premium Cessation Date by first calculating the Terms as*/
		/*  appropriate and adding these to the Age Next Birthday at*/
		/*  contract commencement date.*/
		/*  First calculate ANB at Risk Cessation Date.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(covrtrbIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		wsaaExactValuesInner.wsaaRcessTerm.set(datcon3rec.freqFactor);
		compute(wsaaExactValuesInner.wsaaAnbAtRcessDte, 0).set(add(wsaaExactValuesInner.wsaaAnbAtCcdT, wsaaExactValuesInner.wsaaRcessTerm));
		/*  Second calculate ANB at Premium Cessation Date.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(covrtrbIO.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		wsaaExactValuesInner.wsaaPcessTerm.set(datcon3rec.freqFactor);
		compute(wsaaExactValuesInner.wsaaAnbAtPcessDte, 0).set(add(wsaaExactValuesInner.wsaaAnbAtCcdT, wsaaExactValuesInner.wsaaPcessTerm));
		/*  Second calculate ANB at Effective Date which is Paid-to-date.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(ovrduerec.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		wsaaExactValuesInner.wsaaEffectiveTerm.set(datcon3rec.freqFactor);
		compute(wsaaExactValuesInner.wsaaAnbAtCcdTX, 0).set(add(wsaaExactValuesInner.wsaaAnbAtCcdT, wsaaExactValuesInner.wsaaEffectiveTerm));
	}

protected void calculatePusa4000()
	{
		getFunction4000();
		calculateNetpT4010();
		calculateNetpTX4020();
		calculatePusa4030();
	}

protected void getFunction4000()
	{
		/*  Set ACTC-FUNCTION to 'WNETP' or 'ENETP' depending on whether*/
		/*  whole life or endowment. IF the sum of Age Next Birthday at*/
		/*  Risk Cessation Date and Risk Cessation Term is equal to*/
		/*  99 then this must be whole life, else it must be endowment.*/
		/*    IF (WSAA-LIFE1-ANB-AT-CCD + WSAA-RCESS-TERM) = 99            */
		if ((setPrecision(99, 0)
		&& isGTE((add(wsaaLife1AnbAtCcd, wsaaExactValuesInner.wsaaRcessTerm)), 99))) {
			actcalcrec.function.set("WNETP");
		}
		else {
			actcalcrec.function.set("ENETP");
		}
		actcalcrec.company.set(ovrduerec.chdrcoy);
	}

protected void calculateNetpT4010()
	{
		actcalcrec.initialAge.set(wsaaExactValuesInner.wsaaAnbAtCcdT);
		actcalcrec.finalAge1.set(wsaaExactValuesInner.wsaaAnbAtRcessDte);
		actcalcrec.finalAge2.set(wsaaExactValuesInner.wsaaAnbAtPcessDte);
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			systemError9000();
		}
		wsaaExactValuesInner.wsaaNetpT.set(actcalcrec.value);
	}

protected void calculateNetpTX4020()
	{
		actcalcrec.initialAge.set(wsaaExactValuesInner.wsaaAnbAtCcdTX);
		actcalcrec.finalAge1.set(wsaaExactValuesInner.wsaaAnbAtRcessDte);
		actcalcrec.finalAge2.set(wsaaExactValuesInner.wsaaAnbAtPcessDte);
		callProgram(Actcalc.class, actcalcrec.actcalcRec);
		if (isNE(actcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(actcalcrec.actcalcRec);
			syserrrec.statuz.set(actcalcrec.statuz);
			systemError9000();
		}
		wsaaExactValuesInner.wsaaNetpTX.set(actcalcrec.value);
	}

protected void calculatePusa4030()
	{
		wsaaExactValuesInner.wsaaSumAssured.set(covrtrbIO.getSumins());
		if (summaryPartPlan.isTrue()) {
			if (isNE(ovrduerec.polsum, ZERO)) {
				compute(wsaaExactValuesInner.wsaaSumAssured, 3).setRounded(div(covrtrbIO.getSumins(), ovrduerec.polsum));
			}
		}
		if (isEQ(wsaaExactValuesInner.wsaaNetpTX, ZERO)) {
			wsaaExactValuesInner.wsaaPusa.set(wsaaExactValuesInner.wsaaSumAssured);
		}
		else {
			compute(wsaaExactValuesInner.wsaaPusa, 1).setRounded(mult(wsaaExactValuesInner.wsaaSumAssured, (sub(1, (div(wsaaExactValuesInner.wsaaNetpT, wsaaExactValuesInner.wsaaNetpTX))))));
		}
		ovrduerec.actualVal.set(wsaaExactValuesInner.wsaaPusa);
		ovrduerec.newSumins.set(wsaaExactValuesInner.wsaaPusa);
		/*EXIT*/
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9000();
				case seExit9090: 
					seExit9090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		ovrduerec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start9100();
				case dbExit9190: 
					dbExit9190();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		ovrduerec.statuz.set(varcom.bomb);
		exitProgram();
	}
/*
 * Class transformed  from Data Structure WSAA-EXACT-VALUES--INNER
 */
private static final class WsaaExactValuesInner { 
		/* WSAA-EXACT-VALUES */
	private ZonedDecimalData wsaaAnbAtCcdT = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbAtCcdTX = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbAtRcessDte = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbAtPcessDte = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaRcessTerm = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaPcessTerm = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaEffectiveTerm = new ZonedDecimalData(11, 5);
	private PackedDecimalData wsaaNetpT = new PackedDecimalData(18, 7);
	private PackedDecimalData wsaaNetpTX = new PackedDecimalData(18, 7);
	private PackedDecimalData wsaaSumAssured = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPusa = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaPlanSuffix = new FixedLengthStringData(4);
}
}
