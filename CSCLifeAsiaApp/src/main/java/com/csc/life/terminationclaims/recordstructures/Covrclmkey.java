package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:21
 * Description:
 * Copybook name: COVRCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrclmKey = new FixedLengthStringData(64).isAPartOf(covrclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrclmChdrcoy = new FixedLengthStringData(1).isAPartOf(covrclmKey, 0);
  	public FixedLengthStringData covrclmChdrnum = new FixedLengthStringData(8).isAPartOf(covrclmKey, 1);
  	public FixedLengthStringData covrclmLife = new FixedLengthStringData(2).isAPartOf(covrclmKey, 9);
  	public FixedLengthStringData covrclmCoverage = new FixedLengthStringData(2).isAPartOf(covrclmKey, 11);
  	public FixedLengthStringData covrclmRider = new FixedLengthStringData(2).isAPartOf(covrclmKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(covrclmKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}