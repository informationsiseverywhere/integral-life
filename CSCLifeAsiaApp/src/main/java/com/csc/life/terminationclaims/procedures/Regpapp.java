/*
 * File: Regpapp.java
 * Date: 30 August 2009 2:04:50
 * Author: Quipoz Limited
 * 
 * Class transformed from REGPAPP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.terminationclaims.dataaccess.RegplnbTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* OVERVIEW
* This subroutine forms part of the 9405 Annuities Development.
* It is called via T5671 as part of the AT processing for contract
* issue.
* It will update the status of the Regular Payment record (REGP)
* to approved, based on the entries on the Regular Payment
* allowable actions by status table, T6693, for the existing
* Regular Payment status and the coverage code.
*
*****************************************************************
* </pre>
*/
public class Regpapp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "REGPAPP";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaItemstat = new FixedLengthStringData(2).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaItemtabl = new FixedLengthStringData(4).isAPartOf(wsaaItem, 2);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
		/* ERRORS */
	private String h144 = "H144";
	private String h157 = "H157";
		/* FORMATS */
	private String regplnbrec = "REGPLNBREC";
		/* TABLES */
	private String t6693 = "T6693";
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Isuallrec isuallrec = new Isuallrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Regular Payments File*/
	private RegplnbTableDAM regplnbIO = new RegplnbTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6693rec t6693rec = new T6693rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Regpapp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		regplnbIO.setChdrcoy(isuallrec.company);
		regplnbIO.setChdrnum(isuallrec.chdrnum);
		regplnbIO.setLife(isuallrec.life);
		regplnbIO.setCoverage(isuallrec.coverage);
		regplnbIO.setRider(isuallrec.rider);
		regplnbIO.setPlanSuffix(isuallrec.planSuffix);
		regplnbIO.setRgpynum(ZERO);
		regplnbIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		regplnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regplnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		regplnbIO.setFormat(regplnbrec);
		SmartFileCode.execute(appVars, regplnbIO);
		if (isNE(regplnbIO.getStatuz(),varcom.oK)
		&& isNE(regplnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		while ( !(isEQ(regplnbIO.getStatuz(),varcom.endp)
		|| isNE(regplnbIO.getChdrcoy(),isuallrec.company)
		|| isNE(regplnbIO.getChdrnum(),isuallrec.chdrnum)
		|| isNE(regplnbIO.getLife(),isuallrec.life)
		|| isNE(regplnbIO.getCoverage(),isuallrec.coverage)
		|| isNE(regplnbIO.getRider(),isuallrec.rider)
		|| isNE(regplnbIO.getPlanSuffix(),isuallrec.planSuffix))) {
			authoriseRegp2000();
		}
		
		exitProgram();
	}

protected void authoriseRegp2000()
	{
		start2010();
	}

protected void start2010()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(t6693);
		wsaaItemstat.set(regplnbIO.getRgpystat());
		wsaaItemtabl.set(regplnbIO.getCrtable());
		itdmIO.setItemitem(wsaaItem);
		itdmIO.setItmfrm(isuallrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(),t6693)
		|| isNE(itdmIO.getItemitem(),wsaaItem)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(t6693);
			wsaaItemstat.set(regplnbIO.getRgpystat());
			wsaaItemtabl.set("****");
			itdmIO.setItemitem(wsaaItem);
			itdmIO.setItmfrm(isuallrec.effdate);
			itdmIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi 
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
			if (isNE(itdmIO.getItemcoy(),isuallrec.company)
			|| isNE(itdmIO.getItemtabl(),t6693)
			|| isNE(itdmIO.getItemitem(),wsaaItem)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(wsaaItem);
				syserrrec.statuz.set(h144);
				fatalError600();
			}
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		regplnbIO.setRgpystat(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			findStatus3000();
		}
		if (isEQ(regplnbIO.getRgpystat(),SPACES)) {
			syserrrec.params.set(isuallrec.batctrcde);
			syserrrec.statuz.set(h157);
			fatalError600();
		}
		regplnbIO.setTranno(chdrlnbIO.getTranno());
		regplnbIO.setAprvdate(datcon1rec.intDate);
		regplnbIO.setFunction(varcom.writd);
		regplnbIO.setFormat(regplnbrec);
		SmartFileCode.execute(appVars, regplnbIO);
		if (isNE(regplnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regplnbIO.getParams());
			syserrrec.statuz.set(regplnbIO.getStatuz());
			fatalError600();
		}
		regplnbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, regplnbIO);
		if (isNE(regplnbIO.getStatuz(),varcom.oK)
		&& isNE(regplnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regplnbIO.getParams());
			syserrrec.statuz.set(regplnbIO.getStatuz());
			fatalError600();
		}
	}

protected void findStatus3000()
	{
		/*START*/
		if (isEQ(t6693rec.trcode[wsaaIndex.toInt()],isuallrec.batctrcde)) {
			regplnbIO.setRgpystat(t6693rec.rgpystat[wsaaIndex.toInt()]);
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		isuallrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
