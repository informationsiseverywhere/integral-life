package com.csc.life.terminationclaims.procedures;

/*
 * File: PA505at.java
 * Date: 30 August 2009 0:50:51
 * Author: Quipoz Limited
 *
 * Class transformed from PA505AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */


import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.LaptTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CpsupfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.SurdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.SurdpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Cpsupf;
import com.csc.life.terminationclaims.dataaccess.model.Surdpf;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
/**
* <pre>
*AUTHOR.
*REMARKS.
*
*  This program will be run under AT and will perform the
*  general functions for the finalisation of Terminated
*  Processing.
*
*  The program will carry out the following functions:
*
*  1.  Increment the TRANNO field on the Contract Header.
*
*  2.  Call the Breakout routine if required.
*
*  3.  Validflag '2' the old CHDRPF record.
*
*  4.  Create a new CHDRPF record.
*
*
*  Processing:
*  -----------
*
*  Contract Header
*  ---------------
*  The ATMOD-PRIMARY-KEY will contain the Company and Contract
*  number of the contract being processed.  Use these to
*  perform a READH on CHDRMJA. Increment the TRANNO field by 1.
*
*
*
*  INSERT -PTRN AND UPDATE-CHDRPF
*  ------------------
*  Read all the Record form CPSUPF table With Status R.
*  Iterate CPSUPF . 
*  Insert one new Entry in PTRNPF.
*  Update CHDRPF Recoerd STATCOTE to Previous STATCODE
*  
*  UPDAT-COVR Processing
*  ---------------------
*    Read all the Record form CPSUPF table With Status R.
*       Call T6597-PREMSUBR-04  Using OVRD-OVRDUE-REC
*       *
*    i.e update the COVERAGE-PSTATCODE And COVERAGE
*        STATCODE With a statuz of TERMINATED.
*
*  Rewrite the existing COVRPF with the validflag set to '2'.
*  Create a new COVRPF record with fields set as follows:
*
*       TRANNO - CHDRMJA-TRANNO
*       INSTPREM - zeros
*       PREM-CESS-DATE - COVRPF-BTDATE
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through it's life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as it's item name, the transaction code and
* the contract status of a contract, eg. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, eg AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to Covrsta records, and
* current categories refer to Covrsts records. Agcmsts
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the Agcmsts, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
*
*
*****************************************************
* </pre>
*/
public class Pa505at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("PA505AT");
	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(17);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private PackedDecimalData wsaaBusinessDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsaaEndOfLapt = new FixedLengthStringData(1).init("N");
	private FixedLengthStringData wsaaEndOfCovr = new FixedLengthStringData(1).init("N");
	private FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).init("N");
	private Validator validStatuz = new Validator(wsaaValidStatuz, "Y");
	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1).init(SPACES);
	/*                                                         <D9604>*/
	private FixedLengthStringData wsaaFlexTots = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaFpcoUpdate = new FixedLengthStringData(1).init(SPACES);
	/*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t5729 = "T5729";
	private static final String t6598 = "T6598";
	private static final String t5645 = "T5645";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();	
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LaptTableDAM laptIO = new LaptTableDAM();	
	private LifeTableDAM lifeIO = new LifeTableDAM();	
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Batckey wsaaBatckey = new Batckey();
	private Syserrrec syserrrec = new Syserrrec();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();	
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Atmodrec atmodrec = new Atmodrec();
	private FormatsInner formatsInner = new FormatsInner();
	NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);
	Nlgtpf nlgtpf = new Nlgtpf();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Ptrnpf ptrnpf = new Ptrnpf();
	private List<Ptrnpf> ptrnpfList = null;
	private Cpsupf cpsupf ;
	private CpsupfDAO cpsupfDAO = getApplicationContext().getBean("cpsupfDAO", CpsupfDAO.class);
	private List<Cpsupf> cpsupfList= null;	
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf insChdrpf = new Chdrpf();	
	private List<Chdrpf> chdrBulkInstList = null;
	private Covrpf covrpf = new Covrpf();	
	private Covrpf covrpfins = new Covrpf();	
	private CovrpfDAO covrpupDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> insertCovrlnbList = null;
	private Surpcpy surpcpy = new Surpcpy();
	private Itempf itempf = null;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private T6598rec t6598rec = new T6598rec();
	private SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	List<Payrpf> payrInstlist  = null;
	private Payrpf payrpf = new Payrpf();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(ZERO);
	private T5645rec t5645rec = new T5645rec();
	private List<Incrpf> incrmjaList = new ArrayList<Incrpf>();
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private SurdpfDAO surdpfDAO =  getApplicationContext().getBean("surdpfDAO", SurdpfDAOImpl.class);
	private Surdpf surdpf = new Surdpf();
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTotCovrsurInstprem = new ZonedDecimalData(17, 2).init(0);
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private DescTableDAM descIO = new DescTableDAM();
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	private static final String t1688 = "T1688";
	private static final String e044 = "E044";
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	
	private static final String  presprc ="PRESPRC";
	private Descpf descpf = null;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		checkBrkoutReqd1150,
		exit1199,
		nextFpco2470,
		exit3290,
		writeNewContractHeader3420,
		writeRecord3430,
		exit3809
	}

	public Pa505at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		mainline0001();
		exit0009();
	}

protected void mainline0001()
	{
		initialise1000();
		//read surh list coy,num,wsaaTranno		
		surhclmIO.setDataKey(SPACES);
		surhclmIO.setChdrcoy(chdrpf.getChdrcoy());
		surhclmIO.setChdrnum(chdrpf.getChdrnum());
		surhclmIO.setTranno(wsaaTranno.toInt());		
		surhclmIO.setPlanSuffix(0);
		surhclmIO.setFormat(formatsInner.surhclmrec);
		surhclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)
		&& isNE(surhclmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError9000();
		}
		if (isGT(surhclmIO.getEffdate(), wsaaEffdate)) {
			wsaaEffdate.set(surhclmIO.getEffdate());
		}
		readT5679();
		wsaaCrtable.set(SPACES);
		readCpsupfRcdList();		
		if(cpsupfList!=null && cpsupfList.size()>0){
			for(Cpsupf cpsu: cpsupfList){			
				cpsupf =  cpsu;				
				processRecord();
				
			}
		}
		endOfDrivingFile();	
		
		a000Statistics();
	}
	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
	
		/*    Retrieve the contract header and store the transaction number*/
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		wsaaEffdate.set(0);
		wsaaTotCovrsurInstprem.set(0);
		wsaaSequenceNo.set(ZERO);
		/*    Get today's date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError9000();
		}
		wsaaBusinessDate.set(datcon1rec.intDate);
	
		/*    Read the Component Status Table (T5679) for the transaction.*/
		
		
		chdrpf = chdrpfDAO.getChdrpfByChdrnum("CH",atmodrec.company.toString(),wsaaPrimaryChdrnum.toString(),"1");
		
		if (chdrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set("2".concat(wsaaPrimaryChdrnum.toString()));
			fatalError9000();
		}
		
		wsaaItemCnttype.set(chdrpf.getCnttype());
		wsaaTranno.set(chdrpf.getTranno() + 1);
		payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(atmodrec.company.toString(),wsaaPrimaryChdrnum.toString());
		
		if (payrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set("2".concat(wsaaPrimaryChdrnum.toString()));
			fatalError9000();
		}
				
		contractAccounting();
		pendingIncrease1300();
		
		/*  MOVE CHDRMJA-CURRFROM       TO WSAA-CURRFROM.                */
		wsaaPlanSuffix.set(9999);
		//checkIfBreakoutReqd1100();
		readT57291300();
		
	}

protected void exit0009()
	{
		exitProgram();
	}

protected void contractAccounting()
{	
	itempf = new Itempf();
	itempf.setItempfx("IT");	
	itempf.setItemtabl(t5645);
	itempf.setItemcoy(atmodrec.company.toString());	
	itempf.setItemitem(wsaaProg.toString());
	itempf.setItemseq("  ");
	itempf = itemDAO.getItemRecordByItemkey(itempf);	

	if (itempf == null) {
		syserrrec.params.set("IT".concat(atmodrec.company.toString()).concat("T5645").concat(wsaaProg.toString()));
		fatalError9000();
	}
	
	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));	

}


protected void pendingIncrease1300()
{
	/*BEGNH*/
	/* Check for any pending Increase records (Validflag = '1')     */
	/* for the contract and delete them.                            */
	incrmjaList = incrpfDAO.getIncrpfList(atmodrec.company.toString(), wsaaPrimaryChdrnum.toString());
	
	for (Incrpf incrdata : incrmjaList) {
		resetCovrIndexation(incrdata);
		/*
		 * Delete query Write a dao to delete this record from IN CRPD.Keep
		 * unique number;
		 */
     incrpfDAO.deleteIncrpf(incrdata.getUniqueNumber());
	}
	/*EXIT*/
}



protected void resetCovrIndexation(Incrpf incrdata)
{
	
	covrpfins = covrpupDAO.getCovrRecord(incrdata.getChdrcoy(), incrdata.getChdrnum(), incrdata.getLife(), //IJTI-1410
			incrdata.getCoverage(),incrdata.getRider(), incrdata.getPlnsfx(), "1");//IJTI-1410
	
	covrpfins.setIndexationInd(SPACE);
	covrpupDAO.updateCovrIndexationind(covrpfins);

}


	/**
	* <pre>
	* Call the BRKOUT subroutine to breakout COVR and AGCM        *
	* INCR (Increase Pending) records are similarly broken out.   *
	* </pre>
	*/
protected void checkIfBreakoutReqd1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1110();
					nextr1130();
				case checkBrkoutReqd1150:
					checkBrkoutReqd1150();
					performBrkout1170();
				case exit1199:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1110()
	{
		/*    Read all the LAPT for the contract.*/
		/*    If the Plan Suffix on the first record equals zeros, no*/
		/*    physical breakout is required*/
		/*    else*/
		/*    if the lowest Plan Suffix on LAPT is less than or equal to*/
		/*    CHDRMJA-POLSUM, a breakout is required.*/
		/*    Set the OLD-SUMMARY to the value of CHDRMJA-POLSUM*/
		/*    and the NEW-SUMMARY to the value of the lowest LAPT-PLAN-SUFF*/
		/*    then call 'BRKOUT'.*/
		laptIO.setDataArea(SPACES);
		laptIO.setChdrcoy(chdrpf.getChdrcoy());
		laptIO.setChdrnum(chdrpf.getChdrnum());
		laptIO.setPlanSuffix(ZERO);
		laptIO.setFunction(varcom.begn);


		//performance improvement --  atiwari23
		laptIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		laptIO.setFitKeysSearch("CHDRNUM");

		laptIO.setFormat(formatsInner.laptrec);
		SmartFileCode.execute(appVars, laptIO);
		if (isNE(laptIO.getStatuz(), varcom.oK)
		&& isNE(laptIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(laptIO.getParams());
			syserrrec.statuz.set(laptIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(laptIO.getStatuz(), varcom.endp)
		|| isNE(chdrpf.getChdrnum(), laptIO.getChdrnum())) {
			wsaaEndOfLapt.set("Y");
			goTo(GotoLabel.exit1199);
		}
		if (isEQ(laptIO.getPlanSuffix(), ZERO)) {
			wsaaPlanSuffix.set(ZERO);
			goTo(GotoLabel.exit1199);
		}
		if (isEQ(chdrpf.getPolsum(), 1)) {
			wsaaPlanSuffix.set(laptIO.getPlanSuffix());
			goTo(GotoLabel.exit1199);
		}
		laptIO.setFunction(varcom.nextr);
	}

protected void nextr1130()
	{
		if ((setPrecision(chdrpf.getPolsum(), 0)
		&& isGT(chdrpf.getPolsum(), sub(laptIO.getPlanSuffix(), 1)))) {
			if (isLT(laptIO.getPlanSuffix(), wsaaPlanSuffix)) {
				wsaaPlanSuffix.set(laptIO.getPlanSuffix());
			}
		}
		SmartFileCode.execute(appVars, laptIO);
		if (isNE(laptIO.getStatuz(), varcom.oK)
		&& isNE(laptIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(laptIO.getParams());
			syserrrec.statuz.set(laptIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(laptIO.getStatuz(), varcom.endp)
		|| isNE(chdrpf.getChdrnum(), laptIO.getChdrnum())) {
			goTo(GotoLabel.checkBrkoutReqd1150);
		}
		nextr1130();
		return ;
	}

protected void checkBrkoutReqd1150()
	{
		if ((setPrecision(chdrpf.getPolsum(), 0)
		&& isGT(chdrpf.getPolsum(), sub(wsaaPlanSuffix, 1)))) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.exit1199);
		}
	}

protected void performBrkout1170()
	{
		brkoutrec.brkOldSummary.set(chdrpf.getPolsum());
		compute(brkoutrec.brkNewSummary, 0).set(sub(wsaaPlanSuffix, 1));
		brkoutrec.brkChdrnum.set(chdrpf.getChdrnum());
		brkoutrec.brkChdrcoy.set(chdrmjaIO.getChdrcoy());
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		brkoutrec.brkStatuz.set(varcom.oK);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz, varcom.oK)) {
			syserrrec.params.set(brkoutrec.outRec);
			syserrrec.statuz.set(brkoutrec.brkStatuz);
			fatalError9000();
		}
	}

protected void readCpsupfRcdList(){
	 cpsupfList = new ArrayList<Cpsupf>();
	 cpsupfList = cpsupfDAO.getCpsupfRcdList(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), "R");//IJTI-1410
}



protected void readT57291300()
	{
		
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5729);
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemitem(chdrpf.getCnttype());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			initialize(wsaaFlexTots);
			wsaaFlexiblePremium.set("Y");
		}
	}

protected void processRecord(){
	
		readhCovr2200();// read coverage
		updateCovrpf();	 // update coverage
		updateCpsupf(); // update Cpsupf	
		
		
	}
			
	

protected void updateCovrpf(){ //  approval

	/*    Rewrite the existing COVRMJA with the validflag set to '2'*/
	
	covrpf.setValidflag("2");
	initialize(datcon2rec.datcon2Rec);
	datcon2rec.freqFactor.set(-1);
	datcon2rec.frequency.set("DY");
	datcon2rec.intDate1.set(wsaaEffdate);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	covrpf.setCurrto(datcon2rec.intDate2.toInt());
	covrpupDAO.updateCovrValidAndCurrtoFlag(covrpf);

	//insert new record with validflag=1
	Covrpf covrpfNew = new Covrpf(covrpf);
	covrpfNew.setValidflag("1");		
	covrpfNew.setTranno(wsaaTranno.toInt()); 
	covrpfNew.setCurrto(varcom.vrcmMaxDate.toInt());
	covrpfNew.setCurrfrom(surhclmIO.getEffdate().toInt());
	covrpfNew.setPstatcode(t5679rec.setRidPremStat.toString().trim());
	wsaaTotCovrsurInstprem.add(covrpf.getInstprem().doubleValue());
	if(insertCovrlnbList == null){
		insertCovrlnbList = new LinkedList<>();
	}
	insertCovrlnbList.add(covrpfNew);
	
	if (insertCovrlnbList != null && !insertCovrlnbList.isEmpty()) {
		covrpupDAO.insertCovrRecord(insertCovrlnbList);
		insertCovrlnbList.clear();
	}


}

protected void updateCpsupf(){ //  approval
	//change validflag 1 to 2
	
	cpsupfDAO.updateCpsupfRecord(cpsupf);
	
	//insert new record with validflag=1 
	
	Cpsupf cpsupfNew = cpsupf;
	cpsupfNew.setComppremcode(t5679rec.setRidPremStat.toString().trim());		
	cpsupfNew.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	cpsupfNew.setTranno(wsaaTranno.toInt());
	cpsupfNew.setStatus("A");
	cpsupfNew.setValidflag("1");
	cpsupfDAO.insertCpsupfRecord(cpsupfNew);
	
}


protected void endOfDrivingFile()
{
	/*HOUSEKEEPING-TERMINATE*/
	updateSurrenders();
	batchHeader3500();
	updateChdrpf();
	updatePayer();
	writePtrn();
	a100DelPolUnderwritting();
	releaseSoftlock();
	/*EXIT*/
}

protected void updateSurrenders()
{
	//read surd
	List<Surdpf> surdpfList = surdpfDAO.searchSurdclmRecordList(chdrpf.getChdrcoy().toString().trim(), chdrpf.getChdrnum().trim(),wsaaTranno.toInt());//IJTI-1410
	if(surdpfList!=null && surdpfList.size()>0){
		for(Surdpf surd:surdpfList){
			surdpf = surd;	
			
			readTablesT5687();
			/* The Incr-Check section is now performed per COVR updated and */
			/* not per SURD.                                                */
			/* PERFORM 6000-INCR-CHECK.                             <A06275>*/
				
			if(!t5687rec.svMethod.toString().trim().isEmpty()){	
				/*  store fields of the surrender header for use later when*/
				/*  calling the Surrender processing sub routine*/
				surpcpy.effdate.set(surdpf.getEffdate());
				
				/* Get the latest Surrendering date.                               */
				
				surpcpy.cnttype.set(chdrpf.getCnttype());
			surpcpy.planSuffix.set(surdpf.getPlnsfx());
			surpcpy.crtable.set(surdpf.getCrtable());
			surpcpy.chdrcoy.set(surdpf.getChdrcoy());
			surpcpy.chdrnum.set(surdpf.getChdrnum());
			surpcpy.life.set(surdpf.getLife());
			//surpcpy.jlife.set(surdclmIO.getJlife()); // need to check
			surpcpy.coverage.set(surdpf.getCoverage());
			surpcpy.rider.set(surdpf.getRider());
			//surpcpy.fund.set(surdclmIO.getVirtualFund()); // need to check
			surpcpy.status.set(varcom.oK);
			surpcpy.tranno.set(surdpf.getTranno());
			surpcpy.estimatedVal.set(0);
			surpcpy.actualVal.set(surdpf.getActvalue());
			surpcpy.type.set(surdpf.getTypeT());  // need to check
			
			/*    MOVE SURDCLM-CURRCD         TO SURP-CURRCODE.                */
			/*    MOVE SURHCLM-CURRCD         TO SURP-CNTCURR.                 */
			if (isEQ(surhclmIO.getCurrcd(), SPACES)) {
				surpcpy.currcode.set(surdpf.getCurrcd());
				/*                                       SURP-CNTCURR              */
				surpcpy.cntcurr.set(chdrpf.getCntcurr());
			}
			else {
				surpcpy.currcode.set(surhclmIO.getCurrcd());
				surpcpy.cntcurr.set(surhclmIO.getCurrcd());
			}
			surpcpy.batckey.set(wsaaBatckey.batcFileKey);
			surpcpy.termid.set(wsaaTermid);
			surpcpy.date_var.set(wsaaTransactionDate);
			surpcpy.time.set(wsaaTransactionTime);
			surpcpy.user.set(wsaaUser);
			surpcpy.status.set("****");
			surpcpy.language.set(atmodrec.language);
			callSurrenderMethod4250();
		
			}else if(isEQ(surdpf.getActvalue(),ZERO) && isNE(surdpf.getOtheradjst(),ZERO)){
				transactionCodeDesc2130();
				/*  Set up LIFACMV parameters which are common to all postings.*/
				lifacmvrec1.lifacmvRec.set(SPACES);
				lifacmvrec1.function.set("PSTW");
				lifacmvrec1.batccoy.set(surdpf.getChdrcoy());
				lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
				lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
				lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
				lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
				lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
				lifacmvrec1.rdocnum.set(surdpf.getChdrnum());
				lifacmvrec1.rldgcoy.set(surdpf.getChdrcoy());
				lifacmvrec1.origcurr.set(chdrpf.getCntcurr());
				lifacmvrec1.tranref.set(surdpf.getChdrnum());
				lifacmvrec1.trandesc.set(wsaaLongdesc);
				lifacmvrec1.crate.set(0);
				lifacmvrec1.acctamt.set(0);
				lifacmvrec1.genlcoy.set(surdpf.getChdrcoy());
				lifacmvrec1.genlcur.set(SPACES);
				lifacmvrec1.effdate.set(surdpf.getEffdate());
				lifacmvrec1.rcamt.set(0);
				lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
				lifacmvrec1.transactionDate.set(surdpf.getEffdate());
				lifacmvrec1.transactionTime.set(varcom.vrcmTime);
				lifacmvrec1.user.set(wsaaUser);
				lifacmvrec1.termid.set(wsaaTermid);
				lifacmvrec1.postyear.set(SPACES);
				lifacmvrec1.postmonth.set(SPACES);
				lifacmvrec1.substituteCode[1].set(chdrpf.getCnttype());
				lifacmvrec1.tranno.set(surdpf.getTranno());
				
				posting();
				
			}
		}
	}
}
protected void posting(){

	/*  Now post records for Adjustment value (read from the header)*/
	/*  and for Total value of all postings made (suspense).*/
	lifacmvrec1.rldgacct.set(SPACES);
//	if (isEQ(surdpf.getTypeT(), "C") ) {
		wsaaRldgChdrnum.set(surdpf.getChdrnum());
		wsaaPlan.set(surdpf.getPlnsfx());
		/*        MOVE SURP-LIFE              TO WSAA-RLDG-LIFE            */
		/*        MOVE SURP-COVERAGE          TO WSAA-RLDG-COVERAGE        */
		/*        MOVE SURP-RIDER             TO WSAA-RLDG-RIDER           */
		wsaaRldgLife.set(surdpf.getLife());
		wsaaRldgCoverage.set(surdpf.getCoverage());
		wsaaRldgRider.set(surdpf.getRider());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		lifacmvrec1.sacscode.set(t5645rec.sacscode13);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype13);
		lifacmvrec1.glcode.set(t5645rec.glmap13);
		lifacmvrec1.glsign.set(t5645rec.sign13);
		lifacmvrec1.contot.set(t5645rec.cnttot13);
		lifacmvrec1.substituteCode[6].set(surdpf.getCrtable());
//	}
	
	/*  Postings 5 and 6 always done at contract level.                */
	/*    IF  COMPONENT-LEVEL-ACCOUNTING                          <002>*/
	/*        MOVE SURP-CHDRNUM           TO WSAA-RLDG-CHDRNUM    <002>*/
	/*        MOVE SURDCLM-PLAN-SUFFIX    TO WSAA-PLAN            <002>*/
	/*        MOVE SURP-LIFE              TO WSAA-RLDG-LIFE       <002>*/
	/*        MOVE SURP-COVERAGE          TO WSAA-RLDG-COVERAGE   <002>*/
	/*        MOVE SURP-RIDER             TO WSAA-RLDG-RIDER      <002>*/
	/*        MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX<002>*/
	/*        MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT        <002>*/
	/*    ELSE                                                    <002>*/
	/*        MOVE SURP-CHDRNUM           TO LIFA-RLDGACCT        <002>*/
	/*    END-IF                                                  <002>*/
	/*                                                            <002>*/
	lifacmvrec1.origamt.set(surdpf.getOtheradjst());
	if (isNE(lifacmvrec1.origamt, 0)) {
		postAcmvRecord5000();
	}
	//wsaaAccumValue.add(lifacmvrec1.origamt);
	
	/*  Post any remaining money to Suspense.*/
	lifacmvrec1.sacscode.set(t5645rec.sacscode06);
	lifacmvrec1.sacstyp.set(t5645rec.sacstype06);
	lifacmvrec1.glcode.set(t5645rec.glmap06);
	lifacmvrec1.glsign.set(t5645rec.sign06);
	lifacmvrec1.contot.set(t5645rec.cnttot06);
	lifacmvrec1.origamt.set(surdpf.getOtheradjst());
	if (isNE(lifacmvrec1.origamt, 0)) {
		postAcmvRecord5000();
	}
	surpcpy.status.set(varcom.endp);

}
protected void postAcmvRecord5000()
{
	/*START*/
	/*  Call "LIFACMV" to write account movement records to the ACMV*/
	/*  file.*/
	wsaaSequenceNo.add(1);
	lifacmvrec1.jrnseq.set(wsaaSequenceNo);
	callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
	if (isNE(lifacmvrec1.statuz, varcom.oK)) {
		syserrrec.statuz.set(lifacmvrec1.statuz);
		syserrrec.params.set(lifacmvrec1.lifacmvRec);
		fatalError9000();
	}
	/*EXIT*/
}
protected void transactionCodeDesc2130()
{
	/*  Read T1688 to get transaction code description.*/
	wsaaBatckey.set(wsaaBatckey.batcFileKey);
			
	descpf = descDAO.getdescData("IT", t1688, wsaaBatckey.batcBatctrcde.toString(), surdpf.getChdrcoy(), atmodrec.language.toString());//IJTI-1410
	if(descpf == null) {
		wsaaLongdesc.fill("?");
	}else {
		wsaaLongdesc.set(descpf.getLongdesc());
	}
}

protected void readTablesT5687()
{
	
	itempf = new Itempf();
	itempf.setItempfx("IT");	
	itempf.setItemtabl(t5687);
	itempf.setItemcoy(atmodrec.company.toString());	
	itempf.setItemitem(surdpf.getCrtable());//IJTI-1410
	itempf.setItemseq("  ");
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	

	if (itempf == null) {
		syserrrec.params.set("IT".concat(atmodrec.company.toString()).concat("T5687").concat(wsaaProg.toString()));
		fatalError9000();
	}
	
	t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));

	if(!t5687rec.svMethod.toString().trim().isEmpty()){
		itempf = new Itempf();
		itempf.setItempfx("IT");	
		itempf.setItemtabl(t6598);
		itempf.setItemcoy(atmodrec.company.toString());	
		itempf.setItemitem(t5687rec.svMethod.toString());
		itempf.setItemseq("  ");
		itempf = itemDAO.getItemRecordByItemkey(itempf);
	
		if (itempf == null) {
			syserrrec.params.set("IT".concat(atmodrec.company.toString()).concat("T6598").concat(wsaaProg.toString()));
			fatalError9000();
		}
		
		t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	
}

protected void callSurrenderMethod4250()
{	
	/*READ*/
	callProgram(t6598rec.procesprog, surpcpy.surrenderRec);
	if (isEQ(surpcpy.status, varcom.bomb)) {
		sysrSyserrRecInner.sysrStatuz.set(surpcpy.status);
		xxxxFatalError();
	}
	if (isEQ(surpcpy.status, varcom.endp)) {
		return ;
	}
	if (isNE(surpcpy.status, varcom.oK)) {
		sysrSyserrRecInner.sysrStatuz.set(surpcpy.status);
		xxxxFatalError();
	}
	/*EXIT*/
}

protected void readhCovr2200()
	{
	
	covrpf = covrpupDAO.getCovrRecord(cpsupf.getChdrcoy(), cpsupf.getChdrnum(), cpsupf.getLife(), //IJTI-1410
				cpsupf.getCoverage(),cpsupf.getRider(), cpsupf.getPlnsfx(), "1");//IJTI-1410
		
	wsaaCrtable.set(covrpf.getCrtable());
		
		

		/*EXIT*/
	}


protected void writePtrn()
	{
		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		ptrnpf.setTranno(wsaaTranno.toInt());
		ptrnpf.setPtrneff(wsaaEffdate.toInt());
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());//IJTI-1410
		ptrnpf.setChdrpfx(chdrpf.getChdrpfx());//IJTI-1410
		ptrnpf.setValidflag("1");
		ptrnpf.setTrdt(wsaaBusinessDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());
	
		ptrnpfList = new ArrayList<Ptrnpf>();
		ptrnpfList.add(ptrnpf);	
		boolean result;
		if(ptrnpfList != null && ptrnpfList.size() > 0){
			result = ptrnpfDAO.insertPtrnPF(ptrnpfList);
			if (!result) {
				syserrrec.params.set(chdrpf.getChdrcoy().toString().concat(chdrpf.getChdrnum()));//IJTI-1410
				fatalError9000();
			} else ptrnpfList.clear();
		}
}

protected void checkChdrPaidup3200()
	{
		try {
			start3210();
			//checkStatuz3250();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start3210()
	{
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(chdrpf.getChdrcoy());
		covrmjaIO.setChdrnum(chdrpf.getChdrnum());
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		covrmjaIO.setFormat(formatsInner.covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.endp)
		|| isNE(chdrpf.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), covrmjaIO.getChdrnum())) {
			wsaaEndOfCovr.set("Y");
			goTo(GotoLabel.exit3290);
		}
	}

protected void checkStatuz3250()
	{
		if (isNE(covrmjaIO.getRider(), "00")) {
			checkRiderStatuz3350();
		}
		else {
			checkCoverageStatuz3300();
		}
		if (validStatuz.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaEndOfCovr.set("Y");
			return ;
		}
		covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.endp)
		|| isNE(chdrpf.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), covrmjaIO.getChdrnum())) {
			wsaaEndOfCovr.set("Y");
		}
		else {
			//checkStatuz3250();
			return ;
		}
	}

protected void checkCoverageStatuz3300()
	{
		/*START*/
		if ((isNE(t5679rec.setCovRiskStat, SPACES))
		&& (isEQ(t5679rec.setCovRiskStat, covrmjaIO.getStatcode()))) {
			if ((isNE(t5679rec.setCovPremStat, SPACES))
			&& (isEQ(t5679rec.setCovPremStat, covrmjaIO.getPstatcode()))) {
				wsaaValidStatuz.set("Y");
			}
		}
		else {
			wsaaValidStatuz.set("N");
		}
		/*EXIT*/
	}

protected void checkRiderStatuz3350()
	{
		/*START*/
		if ((isNE(t5679rec.setRidRiskStat, SPACES))
		&& (isEQ(t5679rec.setRidRiskStat, covrmjaIO.getStatcode()))) {
			if ((isNE(t5679rec.setRidPremStat, SPACES))
			&& (isEQ(t5679rec.setRidPremStat, covrmjaIO.getPstatcode()))) {
				wsaaValidStatuz.set("Y");
			}
		}
		else {
			wsaaValidStatuz.set("N");
		}
		/*EXIT*/
	}

protected void updateChdrpf()
	{		
	chdrpf.setCurrto(datcon2rec.intDate2.toInt());
	chdrpfDAO.updateChdrValidflag(chdrpf,"2");
	
	insChdrpf = new Chdrpf(chdrpf);
	insChdrpf.setValidflag('1');
	insChdrpf.setStatcode(cpsupf.getCompstatus());//IJTI-1410
	insChdrpf.setCurrfrom(wsaaEffdate.toInt());
	insChdrpf.setCurrto(varcom.vrcmMaxDate.toInt());
	insChdrpf.setTranno(wsaaTranno.toInt());
	insChdrpf.setUniqueNumber(chdrpf.getUniqueNumber());
	if (isNE(insChdrpf.getBillfreq(), "00")
		&& isNE(insChdrpf.getBillfreq(), "  ")) {
			setPrecision(insChdrpf.getSinstamt01(), 2);
			insChdrpf.setSinstamt01((sub(insChdrpf.getSinstamt01(), wsaaTotCovrsurInstprem)).getbigdata());
			setPrecision(insChdrpf.getSinstamt06(), 2);
			insChdrpf.setSinstamt06((sub(insChdrpf.getSinstamt06(), wsaaTotCovrsurInstprem)).getbigdata());
	}
	chdrBulkInstList = new ArrayList();
	chdrBulkInstList.add(insChdrpf);
	chdrpfDAO.insertChdrValidRecord(chdrBulkInstList);	

	}
protected void updatePayer(){
	if (payrpf != null){
		
		payrpf.setValidflag("2");
		payrpfDAO.updateValidForPayr(payrpf);
	
		Payrpf payrpfnew = new Payrpf(payrpf);
		payrpfnew.setPstatcode(cpsupf.getCompstatus());
		payrpfnew.setValidflag("1");
		payrpfnew.setEffdate(wsaaEffdate.toInt());
		payrpfnew.setTranno(wsaaTranno.toInt());
		payrpfnew.setSinstamt01(insChdrpf.getSinstamt01());
		payrpfnew.setSinstamt06(insChdrpf.getSinstamt06());
		payrInstlist = new ArrayList();
		payrInstlist.add(payrpfnew);
		payrpfDAO.insertPayrpfList(payrInstlist);
	}
}


protected void xxxxFatalError()
{
	xxxxFatalErrors();
	xxxxErrorProg();
}

protected void xxxxFatalErrors()
{
	if (isEQ(sysrSyserrRecInner.sysrStatuz, varcom.bomb)) {
		return ;
	}
	sysrSyserrRecInner.sysrSyserrStatuz.set(sysrSyserrRecInner.sysrStatuz);
	if (isNE(sysrSyserrRecInner.sysrSyserrType, "2")) {
		sysrSyserrRecInner.sysrSyserrType.set("1");
	}
	callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
}

protected void xxxxErrorProg()
{
	/* AT*/
	atmodrec.statuz.set(varcom.bomb);
	/*XXXX-EXIT*/
	exitProgram();
}




protected void batchHeader3500()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void releaseSoftlock()
	{
	
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError9000();
		}
	}


protected void readT5679()
	{
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(wsaaBatckey.batcBatctrcde.toString());

		FixedLengthStringData itemitem = new FixedLengthStringData(8);
		itemitem.setLeft(stringVariable1.toString());
		List<Itempf> itemlist = this.itemDAO.getAllItemitem("IT", atmodrec.company.toString(), t5679, itemitem.toString());
		if(itemlist.isEmpty()) {
			syserrrec.params.set(t5679);
			fatalError9000();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itemlist.get(0).getGenarea()));
	
		
	}






	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void fatalError9000()
	{
		start9010();
		errorProg9050();
	}

protected void start9010()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg9050()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrpf.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrpf.getChdrnum());
		lifsttrrec.tranno.set(chdrpf.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError9000();
		}
	}

protected void a100DelPolUnderwritting()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrpf.getChdrcoy());
		crtundwrec.currcode.set(chdrpf.getCntcurr());
		crtundwrec.chdrnum.set(chdrpf.getChdrnum());
		crtundwrec.cnttyp.set(chdrpf.getCnttype());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError9000();
		}
	}

protected void a200DelCovUnderwritting()
	{
		a200Ctrl();
	}

protected void a200Ctrl()
	{
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrpf.getChdrcoy());
		crtundwrec.currcode.set(chdrpf.getCntcurr());
		crtundwrec.chdrnum.set(chdrpf.getChdrnum());
		crtundwrec.cnttyp.set(chdrpf.getCnttype());
		crtundwrec.crtable.set(covrmjaIO.getCrtable());
		crtundwrec.life.set(covrmjaIO.getLife());
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError9000();
		}
	}

protected void a300BegnLifeio()
	{
		a300Ctrl();
	}

protected void a300Ctrl()
	{
		lifeIO.setRecKeyData(SPACES);
		lifeIO.setChdrcoy(chdrpf.getChdrcoy());
		lifeIO.setChdrnum(chdrpf.getChdrnum());
		lifeIO.setLife(laptIO.getLife());
		/* MOVE                        TO LIFE-JLIFE                    */
		/* MOVE                        TO LIFE-CURRFROM                 */
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (!(isEQ(lifeIO.getStatuz(), varcom.oK)
		&& isEQ(chdrpf.getChdrcoy(), lifeIO.getChdrcoy())
		&& isEQ(chdrpf.getChdrnum(), lifeIO.getChdrnum())
		&& isEQ(laptIO.getLife(), lifeIO.getLife()))) {
			syserrrec.params.set(lifeIO.getParams());
			syserrrec.statuz.set(lifeIO.getStatuz());
			fatalError9000();
		}
	}


/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
private static final class SysrSyserrRecInner {

		/* System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(528);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	private FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrSyserrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 356);
	private FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
}

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData laptrec = new FixedLengthStringData(10).init("LAPTREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData incrrec = new FixedLengthStringData(10).init("INCRREC");
	private FixedLengthStringData laptlaprec = new FixedLengthStringData(10).init("LAPTLAPREC");
	private FixedLengthStringData fpcorec = new FixedLengthStringData(10).init("FPCOREC");
	private FixedLengthStringData fpcolf1rec = new FixedLengthStringData(10).init("FPCOLF1REC");
	private FixedLengthStringData fprmrec = new FixedLengthStringData(10).init("FPRMREC");
	private FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
	private FixedLengthStringData incrmjarec = new FixedLengthStringData(10).init("INCRMJAREC");
	private FixedLengthStringData surhclmrec = new FixedLengthStringData(10).init("SURHCLMREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
