package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:51
 * @author Quipoz
 */
public class Sr57qscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {17, 23, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr57qScreenVars sv = (Sr57qScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr57qscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr57qScreenVars screenVars = (Sr57qScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.causeofdth.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.dtofdeath.setClassString("");
		screenVars.effdate.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.otheradjst.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.totclaim.setClassString("");
		screenVars.asterisk.setClassString("");
		screenVars.btdate.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.occdate.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.ptdate.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.causeofdthdsc.setClassString("");
	}

/**
 * Clear all the variables in Sr57qscreen
 */
	public static void clear(VarModel pv) {
		Sr57qScreenVars screenVars = (Sr57qScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.causeofdth.clear();
		screenVars.currcd.clear();
		screenVars.dtofdeath.clear();
		screenVars.effdate.clear();
		screenVars.longdesc.clear();
		screenVars.otheradjst.clear();
		screenVars.reasoncd.clear();
		screenVars.totclaim.clear();
		screenVars.asterisk.clear();
		screenVars.btdate.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.cownnum.clear();
		screenVars.ctypedes.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.occdate.clear();
		screenVars.ownername.clear();
		screenVars.pstate.clear();
		screenVars.ptdate.clear();
		screenVars.rstate.clear();
		screenVars.causeofdthdsc.clear();
	}
}
