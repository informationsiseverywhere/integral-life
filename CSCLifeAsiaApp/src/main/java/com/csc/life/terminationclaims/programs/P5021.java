/*
 * File: P5021.java
 * Date: 29 August 2009 23:56:21
 * Author: Quipoz Limited
 * 
 * Class transformed from P5021.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.List; //ILIFE-8253

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO; //ILIFE-8253
import com.csc.life.productdefinition.dataaccess.model.Covrpf; //ILIFE-8253
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.screens.S5021ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                  FULL BONUS SURRENDER
*
*  This transaction, Full Bonus Surrender, is selected from the
*  surrender sub-menu  S5245/P5245. This program allows the user
*  to surrender bonuses on a component.
*  The transaction may process a single policy or the whole plan.
*  If any of the policy components are broken out, then whole
*  plan cannot be selected.
*  This transaction cannot be selected if any loans are in
*  existence. This decision is taken at sub-menu stage.
*
*
*  Bonus surrender is only applicable to TRADITIONAL TYPE
*  components.
*
*  The user must register the correct amount to surrender to each
*  component if the total is being spread across several
*  components.
*
*  The program uses an effective date passed from S5245 and an
*  amount of bonus to be surrendered applicable to this
*  component.
*
*  NOTE : The Effective Date is passed in using WSSP-CURRFROM.
*
*  The COVR record stored in P6351 is used to process the
*  component. The COVR file is not actually read.
*
*  The calculation program to be used is read from T6598
*  using a method obtained from T6640 (keyed by CRTABLE).
*  A surrender detail record is then written to be picked up
*  subsequently by the AT processing module.
*
*  For this particular component, the RESERVE value is then
*  calculated using the Reserve Calculation routine from T6598.
*  This is called using the SURC linkage area and SURC-STATUS is
*  set to 'BONS' to indicate that this is a bonus only surrender.
*  The Bonus Reserve Calculation routine will return the BONUS
*  value for this component (from the ACBL file) in
*  SURC-ESTIMATED-VAL and the RESERVE value in SURC-ACTUAL-VAL.
*
*  If processing a WHOLE-PLAN then the figures returned will
*  be accumulations from all the policy component records
*  pertaining to the current COVERAGE/RIDER component.
*
*  If processing a single policy, then the calculation
*  routine will return the value from that policy.
*
*  NOTE - SURC-ESTIMATED-VAL is used differently in UNIT LINKING
*         routines. For BONUS Reserve calculations this field is
*         not relevant and hence it is used to return the BONUS
*         value in this manner.
*
*  The details are displayed on the screen and the user must
*  enter an amount to surrender (not exceeding the TOTAL BONUS
*  VALUE).
*
*  The user may additionally request that payment be made in
*  a different currency to that of the Contract Header (default).
*
*  From the Total Bonus, the requested amount to surrender and
*  the Total Bonus Reserve, the Bonus Surrender Reserve Value
*  is calculated. This is done whenever the amount to surrender
*  is changed.
*
*  The Bonus Surrender Reserve value is then converted to the
*  appropriate Currency.
*
*  The details are then redisplayed if the CALC PF key is
*  pressed otherwise processing continues.
*
*  The details may be changed and displayed any number of times
*  as long as the CALC PF key is used.
*
*  If the details are changed but ENTER is used, the changed
*  values will be used for further processing.
*
*  A Surrender Header and a Surrender Detail are written.
*
*  For a whole plan bonus surrender, this process is repeated
*  for all component under this COVERAGE/RIDER.
*
*  For a single component surrender processing is carried out
*  only once.
*
*  FILES READ.
*  ----------
*        COVRMJA - Coverage File
*        CHDRMJA - Contract Header File
*        LIFEMJA - Life File
*        SURHCLM - Surrender Header File
*        SURDCLM - Surrender Detail File
*        ACBL    - Account Balance File
*        CLTS    - Client File
*        DESC    - Description File
*        ITEM    - Item File
*
*  TABLES READ.
*  -----------
*        T6640   - Traditional Component Methods Table
*        T5688   - Contract Type Descriptions Table
*        T3588   - Risk Status Descriptions Table
*        T3623   - Premium Status Descriptions Table
*        T6598   - Calculation and Processing Subroutines
*
*  PROCESSING.
*  ----------
*
*  INITIALISE.
*  ----------
*
*  If returning from program further down the stack (POINTER =
*  '*') skip this section.
*
*  Read the  Contract  header  (function  RETRV)  and  read  the
*  relevant data necessary for obtaining the status description,
*  the Owner,  the  Life-assured,  Joint-life, CCD, Paid-to-date
*  and the Bill-to-date.
*
*  RETRV the "Plan" or  the  policy  to  be  worked  on from the
*  COVRMJA  I/O  module,  it was stored from program P6351.
*  If the Plan suffix for the key "kept" is zero, the whole Plan
*  is to be surrendered, otherwise, a single policy is to be
*  be surrendered.
*
*  LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To obtain the life assured and joint-life details (if any) do
*  the following;-
*
*      - READR  the   life  details  using  LIFESUR  (for  this
*           contract  number,  joint life number '00').  Format
*           the name accordingly.
*
*      - READR the  joint-life  details using LIFESUR (for this
*           contract  number,  joint life number '01').  Format
*           the name accordingly.
*
*
*  Obtain the  necessary  descriptions  by  calling 'DESCIO' and
*  output the descriptions of the statuses to the screen.
*
*  Format Name
*
*      Read the  client  details  record  and  use the relevant
*           copybook in order to format the required names.
*
*      For this coverage/rider :-
*
*           - call  the  surrender  calculation  subroutine  as
*                defined on  T6640,  this  method  is  used  to
*                access  T6598,  which contains the subroutines
*                necessary   for   the  surrender  calculation.
*
*  Linkage area passed to the surrender calculation subroutine:-
*
*        - company
*        - contract header number
*        - suffix
*        - life number
*        - joint-life number
*        - coverage
*        - rider
*        - crtable
*        - language
*        - estimated value
*        - actual value
*        - currency
*        - element code
*        - description
*        - type code
*        - status
*
*  Use the SURC linkage area with SURC-STATUS set to 'BONS'.
*
*      If the policy selected is part of a summary then the
*      amounts returned must be divided by "n" ("n" being
*      the number of policies in the plan).
*
*  Increment the Transaction Number.                              <001>
*                                                                 <001>
*  SURC-ESTIMATED-VAL contains the BONUS VALUE.
*  SURC-ACTUAL-VAL    contains the BONUS RESERVE VALUE.
*
*  Write a SURH record which will subsequently be deleted
*  by the AT module. Only 1 SURH record is required per
*  transaction therefore check to see if one already exists
*  first. When written, this is always done using plan suffix
*  zero.
*
*  The actual key of this record is not important beyond the
*  contract header number and transaction number (this record
*  is only used for the effective date) but when checking to
*  see whether this record exists above, the key is needed to
*  know what to look for.
*
*  Set Screen values.
*
*  RECEIVE SCREEN.
*  --------------
*
*  If returning from program further down the stack (POINTER =
*  '*') skip this section.
*
*  Validation
*  ----------
*
*  If the KILL PF key was pressed, then skip the remainder
*  of the validation and exit from the program.
*
*  Amount to surrender
*      Must be entered and cannot exceed Bonus Value.
*
*  Currency
*      Validated by the I/O module.
*      A blank entry defaults to the contract currency.
*
*  Process Screen.
*  --------------
*
*  If the Bonus Amount to surrender has changed then the new
*  reserve value is calculated as follows :
*
*    (Amt to Surrender / Total Bonus ) * Total Bonus Reserve
*
*  eg. Total Bonus = $1000.00
*      Total Bonus Reserve = $500.00
*      Amt to Surr = $250.00
*
*    Bonus Amt To Surr Reserve = (250 / 1000) * 500 = $125.00
*
*  If the currency code was changed then convert reserve amount
*  to the new currency.
*
*  Processing is always carried out in this order ie. Reserve
*  recalculation followed by currency conversion.
*
*  Only if the CALC function key is used is the screen
*  redisplayed with the new values otherwise processing continues
*  assuming the recalculated values.
*
*  UPDATING
*  --------
*
*  If returning from program further down the stack (POINTER =
*  '*') skip this section.
*
*  If the  KILL  function  key  was  pressed,  skip  the
*  updating and exit from the program.
*
*  Create Surrender Detail Record.
*  ------------------------------
*
*  Create a SURD record only if the amount to surrender > 0.
*
*           - company
*           - contract number
*           - suffix
*           - transaction
*           - life number
*           - joint-life number
*           - effective date
*           - currency
*           - policy loan amount
*           - other adjustments amount
*           - adjustment reason code
*           - adjustment reason description
*
*  NEXT PROGRAM.
*  ------------
*
*  Add 1 to the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5021 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5021");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaProcessingType = new FixedLengthStringData(1);
	private Validator wholePlan = new Validator(wsaaProcessingType, "1");
	private Validator singleComponent = new Validator(wsaaProcessingType, "2");
	private PackedDecimalData wsaaAcblCurrentBalance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaStoredReserve = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaReserve = new PackedDecimalData(18, 4);
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3);
	private PackedDecimalData wsaaStoredSurrVal = new PackedDecimalData(17, 2);
		/* ERRORS */
	private static final String g588 = "G588";
	private static final String e070 = "E070";
	private static final String e657 = "E657";
	private static final String h021 = "H021";
	private static final String h152 = "H152";
	private static final String h153 = "H153";
	private static final String h154 = "H154";
	private static final String rfik = "RFIK";
		/* TABLES */
	private static final String t6640 = "T6640";
	private static final String t5688 = "T5688";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t6598 = "T6598";
		/* FORMATS */
	private static final String covrmjarec = "COVRMJAREC";
	private static final String lifemjarec = "LIFEMJAREC";
	private static final String surhclmrec = "SURHCLMREC";
	private static final String surdclmrec = "SURDCLMREC";
	private static final String cltsrec = "CLTSREC";
	private static final String itemrec = "ITEMREC";
	private AcblTableDAM acblIO = new AcblTableDAM();
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Details File - Major Alts*/
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
		/*Full Surrender header record*/
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T6640rec t6640rec = new T6640rec();
	private T6598rec t6598rec = new T6598rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5021ScreenVars sv = ScreenProgram.getScreenVars( S5021ScreenVars.class);
	private ExternalisedRules er = new ExternalisedRules();
	
	//ILIFE-8253 start
	private Covrpf covrpf = new Covrpf();
	private List<Covrpf> covrpfList;
	private int covrpfCount = 0;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	//ILIFE-8253 end
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1099, 
		checkForErrors2080, 
		exit2090
	}

	public P5021() {
		super();
		screenVars = sv;
		new ScreenModel("S5021", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		try {
			start1000();
			setScreen1020();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start1000()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1099);
		}
		sv.dataArea.set(SPACES);
		wsaaStoredCurrency.set(SPACES);
		wsaaStoredSurrVal.set(ZERO);
		sv.bonusReserveValue.set(ZERO);
		sv.bonusValue.set(ZERO);
		sv.bonusValueSurrender.set(ZERO);
		sv.plansfx.set(ZERO);
		sv.numpols.set(ZERO);
		sv.bonusDecDate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.rcdate.set(varcom.vrcmMaxDate);
		wsaaBatckey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatckey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatckey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		if (isEQ(wsaaToday,0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				syserrrec.statuz.set(datcon1rec.statuz);
				fatalError600();
			}
			else {
				wsaaToday.set(datcon1rec.intDate);
			}
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
 //ILIFE-8253 start		
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				fatalError600();
		}
		else {
		covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
					covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}
		}
	}
		/*covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}*/
		wsaaCovrChdrcoy.set(covrpf.getChdrcoy());
		wsaaCovrChdrnum.set(covrpf.getChdrnum());
		wsaaCovrLife.set(covrpf.getLife());
		wsaaCovrCoverage.set(covrpf.getCoverage());
		wsaaCovrRider.set(covrpf.getRider());
 //ILIFE-8253 end			
		chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(covrpf.getCrtable()); //ILIFE-8253
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t6640)
		|| isNE(itdmIO.getItemitem(),covrpf.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrpf.getCrtable());
			syserrrec.statuz.set(g588);
			fatalError600();
		}
		else {
			t6640rec.t6640Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t6640rec.surrenderBonusMethod,SPACES)) {
			sv.bonvalsurrErr.set(e657);
			goTo(GotoLabel.exit1099);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(itemrec);
		itemIO.setItemtabl(t6598);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(t6640rec.surrenderBonusMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		if (isEQ(t6598rec.calcprog,SPACES)) {
			syserrrec.params.set(t6640rec.surrenderBonusMethod);
			syserrrec.statuz.set(h021);
			fatalError600();
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.cntdesc.fill("?");
		}
		else {
			sv.cntdesc.set(descIO.getLongdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			descIO.setStatuz(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.rstatdesc.fill("?");
		}
		else {
			sv.rstatdesc.set(descIO.getShortdesc());
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premStatDesc.fill("?");
		}
		else {
			sv.premStatDesc.set(descIO.getShortdesc());
		}
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(covrpf.getChdrnum()); //ILIFE-8253
		lifemjaIO.setLife(covrpf.getLife()); //ILIFE-8253
		sv.life.set(covrpf.getLife()); //ILIFE-8253
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		cltsIO.setClntnum(chdrmjaIO.getCownnum());
		sv.cownum.set(chdrmjaIO.getCownnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(covrpf.getChdrnum()); //ILIFE-8253
		lifemjaIO.setLife(covrpf.getLife()); //ILIFE-8253
		sv.life.set(covrpf.getLife()); //ILIFE-8253
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		lifemjaIO.setJlife("01");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)
		&& isNE(lifemjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(),varcom.mrnf)) {
			sv.jlife.set(SPACES);
			sv.jlifename.set(SPACES);
		}
		else {
			sv.jlife.set(lifemjaIO.getLifcnum());
			cltsIO.setClntnum(lifemjaIO.getLifcnum());
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				syserrrec.statuz.set(cltsIO.getStatuz());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(),1));
		surhclmIO.setDataArea(SPACES);
		surhclmIO.setChdrcoy(wsspcomn.company);
		surhclmIO.setChdrnum(chdrmjaIO.getChdrnum());
		surhclmIO.setTranno(chdrmjaIO.getTranno());
		surhclmIO.setPlanSuffix(0);
		surhclmIO.setFunction(varcom.readr);
		surhclmIO.setFormat(surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(),varcom.oK)
		&& isNE(surhclmIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(surhclmIO.getParams());
			syserrrec.statuz.set(surhclmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(surhclmIO.getStatuz(),varcom.mrnf)) {
			surhclmIO.setDataArea(SPACES);
			surhclmIO.setChdrcoy(wsspcomn.company);
			surhclmIO.setChdrnum(chdrmjaIO.getChdrnum());
			surhclmIO.setTranno(chdrmjaIO.getTranno());
			surhclmIO.setPlanSuffix(0);
			surhclmIO.setLife(covrpf.getLife()); //ILIFE-8253
			surhclmIO.setJlife(covrpf.getJlife()); //ILIFE-8253
			surhclmIO.setEffdate(wsspcomn.currfrom);
			surhclmIO.setCurrcd(chdrmjaIO.getCntcurr());
			surhclmIO.setCnttype(chdrmjaIO.getCnttype());
			surhclmIO.setOtheradjst(0);
			surhclmIO.setPolicyloan(0);
			surhclmIO.setZrcshamt(0);
			surhclmIO.setTaxamt(0);
			surhclmIO.setFunction(varcom.writr);
			surhclmIO.setFormat(surhclmrec);
			SmartFileCode.execute(appVars, surhclmIO);
			if (isNE(surhclmIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(surhclmIO.getParams());
				syserrrec.statuz.set(surhclmIO.getStatuz());
				fatalError600();
			}
		}
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			wsaaProcessingType.set("1");
		}
		else {
			sv.plansfx.set(covrpf.getPlanSuffix());
			wsaaProcessingType.set("2");
		}
		policyLoad5000();
		
	}

protected void setScreen1020()
	{
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.numpols.set(chdrmjaIO.getPolinc());
		sv.coverage.set(covrpf.getCoverage()); //ILIFE-8253
		sv.rider.set(covrpf.getRider()); //ILIFE-8253
		sv.ptdate.set(chdrmjaIO.getPtdate());
		sv.btdate.set(chdrmjaIO.getBtdate());
		sv.bonusDecDate.set(covrpf.getUnitStatementDate()); //ILIFE-8253
		sv.rcdate.set(covrpf.getCrrcd()); //ILIFE-8253
		sv.effdate.set(wsspcomn.currfrom);
		sv.paycurr.set(chdrmjaIO.getCntcurr());
		wsaaStoredCurrency.set(chdrmjaIO.getCntcurr());
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*  Is control returning from another program further down         */
		/*  the stack.                                                     */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(sv.bonusValue,0)) {
			sv.bonvalsurrOut[varcom.pr.toInt()].set("Y");
			sv.bonusvalueErr.set(h154);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2000();
					validate2010();
					checkForChanges2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2000()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2010()
	{
		if (isEQ(sv.bonusValue,0)) {
			sv.bonvalsurrOut[varcom.pr.toInt()].set("Y");
			sv.bonusvalueErr.set(h154);
		}
		if (isGT(sv.bonusValue,0)) {
			if (isEQ(sv.bonusValueSurrender,0)) {
				sv.bonvalsurrErr.set(h153);
			}
		}
		if (isGT(sv.bonusValueSurrender,sv.bonusValue)) {
			sv.bonvalsurrErr.set(h152);
		}
		if (isNE(sv.bonusValueSurrender, ZERO)) {
			zrdecplrec.amountIn.set(sv.bonusValueSurrender);
			callRounding6000();
			if (isNE(zrdecplrec.amountOut, sv.bonusValueSurrender)) {
				sv.bonvalsurrErr.set(rfik);
			}
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.paycurr,SPACES)) {
			sv.paycurr.set(chdrmjaIO.getCntcurr());
		}
	}

protected void checkForChanges2020()
	{
		if (isNE(sv.bonusValueSurrender,wsaaStoredSurrVal)) {
			recalculateReserve2100();
			sv.bonusReserveValue.set(wsaaReserve);
			wsaaStoredSurrVal.set(sv.bonusValueSurrender);
			if (isEQ(wsspcomn.edterror,"Y")) {
				goTo(GotoLabel.checkForErrors2080);
			}
			if (isNE(sv.paycurr,chdrmjaIO.getCntcurr())) {
				conlinkrec.currIn.set(chdrmjaIO.getCntcurr());
				conlinkrec.currOut.set(sv.paycurr);
				readjustCurrencies2200();
				sv.bonusReserveValue.set(conlinkrec.amountOut);
				wsaaStoredCurrency.set(sv.paycurr);
				if (isEQ(wsspcomn.edterror,"Y")) {
					goTo(GotoLabel.checkForErrors2080);
				}
			}
		}
		else {
			if (isNE(sv.paycurr,wsaaStoredCurrency)) {
				conlinkrec.currIn.set(wsaaStoredCurrency);
				conlinkrec.currOut.set(sv.paycurr);
				readjustCurrencies2200();
				sv.bonusReserveValue.set(conlinkrec.amountOut);
				wsaaStoredCurrency.set(sv.paycurr);
				if (isEQ(wsspcomn.edterror,"Y")) {
					goTo(GotoLabel.checkForErrors2080);
				}
			}
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void recalculateReserve2100()
	{
		/*START*/
		compute(wsaaReserve, 4).set(mult((div(sv.bonusValueSurrender,sv.bonusValue)),wsaaStoredReserve));
		zrdecplrec.amountIn.set(wsaaReserve);
		callRounding6000();
		wsaaReserve.set(zrdecplrec.amountOut);
		/*EXIT*/
	}

protected void readjustCurrencies2200()
	{
		start2200();
	}

protected void start2200()
	{
		/*  Convert the Bonus Reserve value to the new currency.*/
		conlinkrec.amountIn.set(sv.bonusReserveValue);
		conlinkrec.function.set("CVRT");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrmjaIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			scrnparams.errorCode.set(conlinkrec.statuz);
			wsspcomn.edterror.set("Y");
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		callRounding6000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void update3000()
	{
			start3000();
		}

protected void start3000()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,"KILL")) {
			return ;
		}
		if (isEQ(sv.bonusValue,ZERO)) {
			return ;
		}
		surdclmIO.setDataArea(SPACES);
		surdclmIO.setChdrcoy(wsspcomn.company);
		surdclmIO.setChdrnum(chdrmjaIO.getChdrnum());
		surdclmIO.setLife(covrpf.getLife()); //ILIFE-8253
		surdclmIO.setCrtable(covrpf.getCrtable()); //ILIFE-8253
		surdclmIO.setJlife(covrpf.getJlife()); //ILIFE-8253
		surdclmIO.setCoverage(covrpf.getCoverage()); //ILIFE-8253
		surdclmIO.setPlanSuffix(covrpf.getPlanSuffix()); //ILIFE-8253
		surdclmIO.setRider(covrpf.getRider()); //ILIFE-8253
		surdclmIO.setTranno(chdrmjaIO.getTranno());
		surdclmIO.setCurrcd(sv.paycurr);
		surdclmIO.setActvalue(sv.bonusReserveValue);
		surdclmIO.setEstMatValue(sv.bonusValueSurrender);
		surdclmIO.setFunction(varcom.writr);
		surdclmIO.setFormat(surdclmrec);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(surdclmIO.getParams());
			syserrrec.statuz.set(surdclmIO.getStatuz());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*START*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		wsspcomn.programPtr.add(1);
	}

protected void policyLoad5000()
	{
		/*LOAD-POLICY*/
		if (singleComponent.isTrue()) {
			sv.plansfxOut[varcom.hi.toInt()].set("Y");
			sv.plansfx.set(covrpf.getPlanSuffix());
		}
		getReserve5100();
		sv.bonusValue.set(wsaaAcblCurrentBalance);
		/*EXIT*/
	}

protected void getReserve5100()
	{
		start5100();
	}

protected void start5100()
	{
		srcalcpy.currcode.set(chdrmjaIO.getCntcurr());
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrpf.getChdrcoy()); //ILIFE-8253
		srcalcpy.chdrChdrnum.set(covrpf.getChdrnum()); //ILIFE-8253
		srcalcpy.lifeLife.set(covrpf.getLife()); //ILIFE-8253
		srcalcpy.lifeJlife.set(covrpf.getJlife()); //ILIFE-8253
		srcalcpy.covrCoverage.set(covrpf.getCoverage()); //ILIFE-8253
		srcalcpy.covrRider.set(covrpf.getRider()); //ILIFE-8253
		srcalcpy.crtable.set(covrpf.getCrtable()); //ILIFE-8253
		srcalcpy.crrcd.set(covrpf.getCrrcd()); //ILIFE-8253
		srcalcpy.status.set("BONS");
		srcalcpy.pstatcode.set(covrpf.getPstatcode()); //ILIFE-8253
		srcalcpy.polsum.set(chdrmjaIO.getPolsum());
		srcalcpy.ptdate.set(chdrmjaIO.getPtdate());
		srcalcpy.effdate.set(wsspcomn.currfrom);
		srcalcpy.language.set(wsspcomn.language);
		srcalcpy.type.set("F");
		srcalcpy.planSuffix.set(covrpf.getPlanSuffix()); //ILIFE-8253
		while ( !(isEQ(srcalcpy.status,varcom.endp))) {
			/*IVE-796 RUL Product - Partial Surrender Calculation started*/
			//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))  
			{
				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			}
			else
			{
		 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
				Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
				vpxsurcrec.function.set("INIT");
				callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
				vpmfmtrec.initialize();
				vpmfmtrec.amount02.set(wsaaEstimateTot);			

				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrmjaIO);//VPMS call
				
				if(isEQ(srcalcpy.type,"L"))
				{
					vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
					callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
					srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
					srcalcpy.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpy.type,"C"))
				{
					srcalcpy.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
				{
					srcalcpy.status.set(varcom.endp);
				}
				else
				{
					srcalcpy.status.set(varcom.oK);
				}
			}

			/*IVE-796 RUL Product - Partial Surrender Calculation end*/
			if (isNE(srcalcpy.status,varcom.oK)
			&& isNE(srcalcpy.status,varcom.endp)) {
				syserrrec.statuz.set(srcalcpy.status);
				fatalError600();
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			callRounding6000();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			callRounding6000();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
			wsaaStoredReserve.add(srcalcpy.actualVal);
			wsaaAcblCurrentBalance.add(srcalcpy.estimatedVal);
			if (singleComponent.isTrue()
			&& isLTE(covrpf.getPlanSuffix(),chdrmjaIO.getPolsum())) {
				compute(wsaaAcblCurrentBalance, 2).set(div(wsaaAcblCurrentBalance,chdrmjaIO.getPolsum()));
				compute(wsaaStoredReserve, 2).set(div(wsaaStoredReserve,chdrmjaIO.getPolsum()));
			}
		}
		zrdecplrec.amountIn.set(wsaaAcblCurrentBalance);
		callRounding6000();
		wsaaAcblCurrentBalance.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaStoredReserve);
		callRounding6000();
		wsaaStoredReserve.set(zrdecplrec.amountOut);
		
		
	}

protected void callRounding6000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.paycurr);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
}
