package com.csc.life.terminationclaims.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Hmtdpf implements Serializable {

	private static final long serialVersionUID = -7440403482683307887L;
	
	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private int tranno;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private String crtable;
	private int plnsfx;
	private BigDecimal hactval;
	private BigDecimal hemv;
	private String hcnstcur;
	private String type_t;
	private String usrprf;
	private String jobnm;
	private Date datime;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getJlife() {
		return jlife;
	}
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public BigDecimal getHactval() {
		return hactval;
	}
	public void setHactval(BigDecimal hactval) {
		this.hactval = hactval;
	}
	public BigDecimal getHemv() {
		return hemv;
	}
	public void setHemv(BigDecimal hemv) {
		this.hemv = hemv;
	}
	public String getHcnstcur() {
		return hcnstcur;
	}
	public void setHcnstcur(String hcnstcur) {
		this.hcnstcur = hcnstcur;
	}
	public String getType_t() {
		return type_t;
	}
	public void setType_t(String type_t) {
		this.type_t = type_t;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobName() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}

}