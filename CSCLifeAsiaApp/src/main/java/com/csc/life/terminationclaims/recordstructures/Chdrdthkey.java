package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:20
 * Description:
 * Copybook name: CHDRDTHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrdthkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrdthFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrdthKey = new FixedLengthStringData(64).isAPartOf(chdrdthFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrdthChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrdthKey, 0);
  	public FixedLengthStringData chdrdthChdrnum = new FixedLengthStringData(8).isAPartOf(chdrdthKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrdthKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrdthFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrdthFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}