package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh595screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh595ScreenVars sv = (Sh595ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh595screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh595ScreenVars screenVars = (Sh595ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.statdsc.setClassString("");
		screenVars.premStatDesc.setClassString("");
		screenVars.ownersel.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.znfopt01.setClassString("");
		screenVars.znfoptd01.setClassString("");
		screenVars.znfopt02.setClassString("");
		screenVars.znfoptd02.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.resndesc.setClassString("");
	}

/**
 * Clear all the variables in Sh595screen
 */
	public static void clear(VarModel pv) {
		Sh595ScreenVars screenVars = (Sh595ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.ctypedes.clear();
		screenVars.statdsc.clear();
		screenVars.premStatDesc.clear();
		screenVars.ownersel.clear();
		screenVars.ownername.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.znfopt01.clear();
		screenVars.znfoptd01.clear();
		screenVars.znfopt02.clear();
		screenVars.znfoptd02.clear();
		screenVars.reasoncd.clear();
		screenVars.resndesc.clear();
	}
}
