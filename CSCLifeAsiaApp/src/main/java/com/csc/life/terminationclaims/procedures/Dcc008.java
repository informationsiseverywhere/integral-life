/*
 * File: Dcc008.java
 * Date: December 5, 2013 9:36:15 AM ICT
 * Author: CSC
 * 
 * Class transformed from DCC008.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.life.terminationclaims.recordstructures.Vpxdtavrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrndthTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrssurTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;

/**
* <pre>
*REMARKS.
*
*  CALCULATE SUM ASSURED + BID VALUE OF UNITS.
*
*  This death calculation module calculates the Current Fund Holdi gs
*  AND the Sum Assured value.
*
* For each UTRS
*   - return the Estimate Amount.
*
* If No UTRS found check if there are any unprocessed UTRN's and
* if so, create a negative UTRN as if it had found a UTRS. This
* prevents writing a claim record where there is no Estimate
* amount, thus causing the Sum Assured ONLY to be directly paid
* out.
*
* IF all Estimate Amounts are not zero
*    Calculate an extra portion as:-
*         Sum Assured + Accumulated Amt (where if extra portion is
*                                        < zero, move 0 to the
*                                        extra portion)
* ELSE
*    Return Actual = Sum Assured when Estimate = 0.
*
* NB. There will be NO ACTUAL VALUE returned if ANY Fund Balance
*     exists, whether UTRS or unprocessed UTRN's.
*
* Example 1.
*  Fund ABCD has UTRS Balance 6000.00
*  Sum Assured is             5000.00
*  Returned Value will be    11000.00
*
* Example 2.
*  Fund ABCD has UTRS Balance 3000.00
*  Sum Assured is             4000.00
*  Returned Value will be     7000.00
*
****************************************************************** ****
* ORIGINAL PROGRAM SPECIFICATION
****************************************************************** ****
*
*
*  DEATH CALCULATION METHOD 8.
*  ---------------------------
*
*  DCC008
*  ------
*
*  This program is an item entry on T6598, the death claim
*  subroutine method table. This is the calculation process
*  for obtaining the greater of the Bid Value of the
*  investments and the Sum Insured . Returned the amount in
*  the currency of the contract header.
*
*  The following is the linkage information passed to this
*  subroutine:-
*
*             - company
*             - contract header number
*             - life number
*             - joint-life number
*             - coverage
*             - rider
*             - crtable
*             - effective date
*             - language
*             - estimated value
*             - actual value
*             - currency
*             - element code
*             - description
*             - type code
*             - status
*
*    PROCESSING.
*    ----------
*
*  Convert the amounts to the contract header currency amount
*  and accumulate.
*
*    If it is the first time through this routine:-
*
*  - read all the COVR records for this component for this
*    'dead' life and accumulate the sum assured amounts and
*    convert to the contract currency.
*
*      - set-up the key and BEGN option  and read fund UTRS
*        - keyed, Coy, Chdr, Life, J/life, Coverage, Rider
*
*      - for each fund on component get the now bid price
*          read the VPRC (prices file), keyed on:
*          - Virtual fund, Unit type and Effective-date.
*
*      - calc value to estimate value
*        multiply the number-of-units by the price returned
*        converted to contract currency.
*
*      - at end of UTRS
*        Return Sum Insured + Estimated Amount
*        If the amount being returned is negative, return it
*        as zero.
*        Return amount, crtable, desc, type = 'S',chdrccy
*        set SI to zero.
*
*  else
*
*      - set status = ENDP.
*
*  These fields are returned to the main transaction for
*  further processing.
*
*****************************************************************
* </pre>
*/
public class Dcc008 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "DCC008";
	protected static final String g094 = "G094";
	private static final String e652 = "E652";
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t5687 = "T5687";
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";

	private FixedLengthStringData wsaaUtype = new FixedLengthStringData(1).init(SPACES);
	private Validator initialUnits = new Validator(wsaaUtype, "I");
	private PackedDecimalData wsaaInitialBidPrice = new PackedDecimalData(9, 5).init(0);
	protected PackedDecimalData wsaaAccumBidPrice = new PackedDecimalData(9, 5).init(0);
	private PackedDecimalData wsaaInitialAmount = new PackedDecimalData(18, 5).init(0);
	protected PackedDecimalData wsaaAccumAmount = new PackedDecimalData(18, 5).init(0);
	private PackedDecimalData wsaaAccFundTot = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(18, 2).init(0);
	protected FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private String wsaaValidStatuz = "";
	private PackedDecimalData wsaaFundValue = new PackedDecimalData(17, 5);

	private FixedLengthStringData wsaaNoMore = new FixedLengthStringData(1);
	private Validator endOfFund = new Validator(wsaaNoMore, "Y");

	private FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaSwitch, "Y");
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrndthTableDAM utrndthIO = new UtrndthTableDAM();
	private UtrssurTableDAM utrssurIO = new UtrssurTableDAM();
	private VprcTableDAM vprcIO = new VprcTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	protected Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5687rec t5687rec = new T5687rec();
	private T5679rec t5679rec = new T5679rec();
	private T5515rec t5515rec = new T5515rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected Deathrec deathrec = new Deathrec();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private static final String chdrrec = "CHDRREC";
	private ExternalisedRules er = new ExternalisedRules();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readAccum122, 
		nextr512, 
		exit515
	}

	public Dcc008() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		deathrec.deathRec = convertAndSetParam(deathrec.deathRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		if (isEQ(deathrec.endf, "Y")) {
			deathrec.status.set(varcom.endp);
			return ;
		}
		if (isEQ(deathrec.status, SPACES)) {
			wsaaSwitch.set("Y");
			utrssurIO.setStatuz(varcom.oK);
		}
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(deathrec.endf, "S")) {
			deathrec.status.set(varcom.oK);
			wsaaSwitch.set("Y");
			utrssurIO.setStatuz(SPACES);
			calcTerm400();
			return ;
		}
		if (firstTime.isTrue()) {
			wsaaChdrcoy.set(deathrec.chdrChdrcoy);
			wsaaChdrnum.set(deathrec.chdrChdrnum);
			wsaaCoverage.set(deathrec.covrCoverage);
			wsaaRider.set(deathrec.covrRider);
			wsaaLife.set(deathrec.lifeLife);
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			wsaaAccFundTot.set(ZERO);
			deathrec.estimatedVal.set(ZERO);
			deathrec.actualVal.set(ZERO);
			wsaaNoMore.set(SPACES);
			deathrec.valueType.set(SPACES);
			wsaaVirtualFund.set(SPACES);
			wsaaUnitType.set(SPACES);
			deathrec.status.set(varcom.oK);
			deathrec.endf.set(SPACES);
		}
		if (isEQ(deathrec.endf, "I")) {
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			deathrec.estimatedVal.set(ZERO);
			deathrec.actualVal.set(ZERO);
		}
		if (isEQ(deathrec.status, varcom.oK)) {
			if (isEQ(deathrec.endf, "I")) {
				a100ReadInterestBearing();
				/*          ADD WSAA-ACCUM-AMOUNT TO WSAA-ACC-FUND-TOT             */
				compute(wsaaAccFundTot, 6).setRounded(add(wsaaAccFundTot, wsaaAccumAmount));
			}
			else {
				readFunds100();
				/*       ADD WSAA-INITIAL-AMOUNT  TO WSAA-ACC-FUND-TOT             */
				/*       ADD WSAA-ACCUM-AMOUNT    TO WSAA-ACC-FUND-TOT             */
				compute(wsaaAccFundTot, 6).setRounded(add(add(wsaaAccFundTot, wsaaInitialAmount), wsaaAccumAmount));
			}
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readFunds100()
	{
		para101();
	}

protected void para101()
	{
		utrssurIO.setParams(SPACES);
		utrssurIO.setChdrcoy(wsaaChdrcoy);
		utrssurIO.setChdrnum(wsaaChdrnum);
		utrssurIO.setLife(wsaaLife);
		utrssurIO.setCoverage(wsaaCoverage);
		utrssurIO.setRider(wsaaRider);
		utrssurIO.setUnitVirtualFund(wsaaVirtualFund);
		utrssurIO.setUnitType(wsaaUnitType);
		utrssurIO.setPlanSuffix(0);
		utrssurIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, utrssurIO);
		if (isNE(utrssurIO.getStatuz(), varcom.oK)
		&& isNE(utrssurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrssurIO.getParams());
			fatalError9000();
		}
		if (isNE(utrssurIO.getChdrcoy(), deathrec.chdrChdrcoy)
		|| isNE(utrssurIO.getChdrnum(), deathrec.chdrChdrnum)
		|| isNE(utrssurIO.getLife(), deathrec.lifeLife)
		|| isNE(utrssurIO.getCoverage(), deathrec.covrCoverage)
		|| isNE(utrssurIO.getRider(), deathrec.covrRider)
		|| isEQ(utrssurIO.getStatuz(), varcom.endp)) {
			utrndthIO.setUnitVirtualFund(SPACES);
			utrndthIO.setUnitType(SPACES);
			utrndthIO.setFunction(varcom.begn);
			while ( !(isEQ(utrndthIO.getStatuz(), varcom.endp))) {
				checkUnprocessedUtrns700();
			}
			
			utrssurIO.setStatuz(varcom.endp);
			deathrec.endf.set("I");
			wsaaChdrcoy.set(deathrec.chdrChdrcoy);
			wsaaChdrnum.set(deathrec.chdrChdrnum);
			wsaaCoverage.set(deathrec.covrCoverage);
			wsaaRider.set(deathrec.covrRider);
			wsaaLife.set(deathrec.lifeLife);
			wsaaVirtualFund.set(SPACES);
			wsaaSwitch.set("N");
			return ;
		}
		wsaaUnitType.set(utrssurIO.getUnitType());
		wsaaVirtualFund.set(utrssurIO.getUnitVirtualFund());
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		deathrec.estimatedVal.set(ZERO);
		deathrec.actualVal.set(ZERO);
		while ( !(endOfFund.isTrue()
		|| isNE(utrssurIO.getUnitVirtualFund(), wsaaVirtualFund)
		|| isNE(utrssurIO.getUnitType(), wsaaUnitType))) {
			accumAmount110();
		}
		
		if (isEQ(utrssurIO.getStatuz(), varcom.oK)
		|| isEQ(utrssurIO.getStatuz(), varcom.endp)) {
			readVprc120();
			checkEof200();
			wsaaVirtualFund.set(utrssurIO.getUnitVirtualFund());
			wsaaUnitType.set(utrssurIO.getUnitType());
			wsaaLife.set(utrssurIO.getLife());
			wsaaRider.set(utrssurIO.getRider());
			wsaaCoverage.set(utrssurIO.getCoverage());
			wsaaChdrnum.set(utrssurIO.getChdrnum());
			if (isEQ(deathrec.endf, "I")) {
				wsaaChdrcoy.set(deathrec.chdrChdrcoy);
				wsaaChdrnum.set(deathrec.chdrChdrnum);
				wsaaCoverage.set(deathrec.covrCoverage);
				wsaaRider.set(deathrec.covrRider);
				wsaaLife.set(deathrec.lifeLife);
				wsaaVirtualFund.set(SPACES);
			}
			wsaaSwitch.set("N");
		}
	}

protected void accumAmount110()
	{
		para111();
	}

protected void para111()
	{
		if (isEQ(utrssurIO.getUnitType(), "I")) {
			wsaaInitialAmount.add(utrssurIO.getCurrentDunitBal());
			wsaaUtype.set("I");
		}
		else {
			wsaaAccumAmount.add(utrssurIO.getCurrentDunitBal());
			wsaaUtype.set("A");
		}
		utrndthIO.setUnitVirtualFund(utrssurIO.getUnitVirtualFund());
		utrndthIO.setUnitType(utrssurIO.getUnitType());
		utrndthIO.setFunction(varcom.begn);
		while ( !(isEQ(utrndthIO.getStatuz(), varcom.endp))) {
			checkUnprocessedUtrns700();
		}
		
		utrssurIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrssurIO);
		if (isNE(utrssurIO.getStatuz(), varcom.oK)
		&& isNE(utrssurIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(utrssurIO.getStatuz());
			syserrrec.params.set(utrssurIO.getParams());
			fatalError9000();
		}
		if (isNE(utrssurIO.getChdrcoy(), deathrec.chdrChdrcoy)
		|| isNE(utrssurIO.getChdrnum(), deathrec.chdrChdrnum)
		|| isNE(utrssurIO.getLife(), deathrec.lifeLife)
		|| isNE(utrssurIO.getCoverage(), deathrec.covrCoverage)
		|| isNE(utrssurIO.getRider(), deathrec.covrRider)
		|| isEQ(utrssurIO.getStatuz(), varcom.endp)) {
			utrssurIO.setStatuz(varcom.endp);
			deathrec.endf.set("I");
			wsaaNoMore.set("Y");
		}
	}

protected void readVprc120()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read121();
				case readAccum122: 
					readAccum122();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read121()
	{
		/*  read the vprc file for each fund type*/
		if (isEQ(wsaaInitialAmount, ZERO)) {
			goTo(GotoLabel.readAccum122);
		}
		vprcIO.setDataArea(SPACES);
		vprcIO.setUnitVirtualFund(wsaaVirtualFund);
		vprcIO.setUnitType("I");
		vprcIO.setEffdate(deathrec.effdate);
		vprcIO.setJobno(ZERO);
		vprcIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, vprcIO);
		if (isNE(vprcIO.getStatuz(), varcom.oK)
		&& isNE(vprcIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(vprcIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund, vprcIO.getUnitVirtualFund())
		|| isNE(vprcIO.getUnitType(), "I")
		|| isEQ(vprcIO.getStatuz(), varcom.endp)) {
			deathrec.status.set(varcom.mrnf);
			vprcIO.setFunction(varcom.begn);
			vprcIO.setStatuz(varcom.endp);
			vprcIO.setUnitVirtualFund(wsaaVirtualFund);
			vprcIO.setUnitType("I");
			vprcIO.setEffdate(deathrec.effdate);
			vprcIO.setJobno(ZERO);
			syserrrec.statuz.set(g094);
			syserrrec.params.set(vprcIO.getParams());
			fatalError9000();
		}
		wsaaInitialBidPrice.set(vprcIO.getUnitBidPrice());
	}

protected void readAccum122()
	{
		if (isEQ(wsaaAccumAmount, ZERO)) {
			return ;
		}
		vprcIO.setDataArea(SPACES);
		vprcIO.setUnitVirtualFund(wsaaVirtualFund);
		vprcIO.setUnitType("A");
		vprcIO.setEffdate(deathrec.effdate);
		vprcIO.setJobno(ZERO);
		vprcIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, vprcIO);
		if (isNE(vprcIO.getStatuz(), varcom.oK)
		&& isNE(vprcIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(vprcIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund, vprcIO.getUnitVirtualFund())
		|| isNE(vprcIO.getUnitType(), "A")
		|| isEQ(vprcIO.getStatuz(), varcom.endp)) {
			deathrec.status.set(varcom.mrnf);
			vprcIO.setFunction(varcom.begn);
			vprcIO.setStatuz(varcom.endp);
			vprcIO.setUnitVirtualFund(wsaaVirtualFund);
			vprcIO.setUnitType("A");
			vprcIO.setEffdate(deathrec.effdate);
			vprcIO.setJobno(ZERO);
			syserrrec.statuz.set(g094);
			syserrrec.params.set(vprcIO.getParams());
			fatalError9000();
		}
		wsaaAccumBidPrice.set(vprcIO.getUnitBidPrice());
	}

protected void checkEof200()
	{
		eof210();
	}

protected void eof210()
	{
		readTable300();
	//Ticket #IVE-790 - RUL Product - Death Calculation - Integration with latest PA compatible models - START	
	//****Ticket # IVE-655 RUL Calculations -DeathCalulation Start
	//ILIFE-3389 SUM Screen bomb (Updated 1 line)
	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable()  && er.isCallExternal("VPMDCC008") && er.isExternalized(chdrlnbIO.cnttype.toString(), deathrec.crtable.toString())))
	{
		if (initialUnits.isTrue()) {
			compute(wsaaInitialAmount, 6).setRounded(mult(wsaaInitialAmount, wsaaInitialBidPrice));
			if (isNE(deathrec.currcode, t5515rec.currcode)) {
				wsaaFundValue.set(wsaaInitialAmount);
				a400ConvertCurrency();
				wsaaInitialAmount.set(wsaaFundValue);
			}
			/*       MOVE WSAA-INITIAL-AMOUNT TO CDTH-ESTIMATED-VAL            */
			deathrec.estimatedVal.setRounded(wsaaInitialAmount);
			deathrec.fieldType.set("I");
		}
		else {
			deathrec.fieldType.set("A");
			/*       MULTIPLY WSAA-ACCUM-AMOUNT BY  WSAA-ACCUM-BID-PRICE       */
			/*                                GIVING WSAA-ACCUM-AMOUNT         */
			compute(wsaaAccumAmount, 6).setRounded(mult(wsaaAccumAmount, wsaaAccumBidPrice));
			if (isNE(deathrec.currcode, t5515rec.currcode)) {
				wsaaFundValue.set(wsaaAccumAmount);
				a400ConvertCurrency();
				wsaaAccumAmount.set(wsaaFundValue);
			}
			/*       MOVE WSAA-ACCUM-AMOUNT   TO CDTH-ESTIMATED-VAL            */
			deathrec.estimatedVal.setRounded(wsaaAccumAmount);
		}
		deathrec.valueType.set("S");
		deathrec.element.set(wsaaVirtualFund);
	
	}
	else{
		chdrlnbIO.setChdrcoy(deathrec.chdrChdrcoy);
		chdrlnbIO.setChdrnum(deathrec.chdrChdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		
		Vpmcalcrec vpmcalcrec =new Vpmcalcrec();
		Vpmfmtrec vpmfmtrec =new Vpmfmtrec();
		vpmcalcrec.linkageArea.set(deathrec.deathRec);
		Vpxdtavrec vpxdtavrec = new Vpxdtavrec();
		
		if(initialUnits.isTrue()){
			deathrec.fieldType.set("I");
			vpxdtavrec.curduntbal.set(wsaaInitialAmount);
			vpxdtavrec.currcode.set(t5515rec.currcode);
			callProgram("VPMDCC008",deathrec.deathRec,vpxdtavrec,vpmfmtrec,chdrlnbIO);
		}
		else{
			deathrec.fieldType.set("A");
			vpxdtavrec.curduntbal.set(wsaaAccumAmount);
			vpxdtavrec.currcode.set(t5515rec.currcode);
			callProgram("VPMDCC008",deathrec.deathRec,vpxdtavrec,vpmfmtrec,chdrlnbIO);
		}
	}
	//****Ticket # IVE-655 RUL Calculations -DeathCalulation end
	//Ticket #IVE-790 - RUL Product - Death Calculation - Integration with latest PA compatible models - END
}

protected void readTable300()
	{
		t5515300();
	}

protected void t5515300()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescitem(wsaaVirtualFund);
		descIO.setDesctabl(t5515);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(deathrec.chdrChdrcoy);
		descIO.setLanguage(deathrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			deathrec.description.set(descIO.getLongdesc());
		}
		else {
			deathrec.description.fill("?");
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(deathrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(wsaaVirtualFund);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), deathrec.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5515)
		|| isNE(itdmIO.getItemitem(), wsaaVirtualFund)) {
			t5515rec.t5515Rec.set(SPACES);
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		deathrec.element.set(wsaaVirtualFund);
	}

protected void calcTerm400()
	{
		/*GO*/
		if (firstTime.isTrue()) {
			wsaaSumins.set(ZERO);
			deathrec.estimatedVal.set(ZERO);
			deathrec.actualVal.set(ZERO);
			readComponentsAndTables500();
		}
		if (isEQ(deathrec.status, varcom.oK)) {
			checkMethods600();
		}
		/*EXIT*/
	}

protected void readComponentsAndTables500()
	{
		para501();
	}

protected void para501()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(deathrec.chdrChdrcoy);
		covrIO.setChdrnum(deathrec.chdrChdrnum);
		covrIO.setLife(deathrec.lifeLife);
		covrIO.setCoverage(deathrec.covrCoverage);
		covrIO.setRider(deathrec.covrRider);
		covrIO.setPlanSuffix(0);
		covrIO.setFunction(varcom.begn);
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			accumSumins510();
		}
		
		if (isEQ(wsaaAccFundTot, ZERO)) {
			deathrec.actualVal.set(wsaaSumins);
		}
		else {
			deathrec.estimatedVal.set(wsaaSumins);
			if (isLT(deathrec.estimatedVal, 0)) {
				deathrec.estimatedVal.set(ZERO);
				wsaaSumins.set(wsaaAccFundTot);
			}
			if (isLT(deathrec.actualVal, 0)) {
				deathrec.estimatedVal.set(0);
			}
		}
		if (isEQ(deathrec.status, varcom.oK)) {
			readT5687520();
		}
		wsaaSwitch.set("N");
	}

protected void accumSumins510()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para511();
				case nextr512: 
					nextr512();
				case exit515: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para511()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError9000();
		}
		if (isNE(covrIO.getChdrcoy(), deathrec.chdrChdrcoy)
		|| isNE(covrIO.getChdrnum(), deathrec.chdrChdrnum)
		|| isNE(covrIO.getCoverage(), deathrec.covrCoverage)
		|| isNE(covrIO.getRider(), deathrec.covrRider)
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit515);
		}
		checkT5679800();
		if (isEQ(wsaaValidStatuz, "Y")) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.nextr512);
		}
		if (isEQ(covrIO.getValidflag(), "1")) {
			wsaaSumins.add(covrIO.getSumins());
		}
	}

protected void nextr512()
	{
		covrIO.setFunction(varcom.nextr);
	}

protected void readT5687520()
	{
		read521();
	}

protected void read521()
	{
		itdmIO.setItemcoy(deathrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(deathrec.crtable);
		itdmIO.setItmfrm("99999999");
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(deathrec.crtable, itdmIO.getItemitem())
		|| isNE(deathrec.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			deathrec.status.set(e652);
			return ;
			/*****    GO TO 125-EXIT                                            */
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		/* find description for crtable.*/
		descIO.setDescitem(deathrec.crtable);
		descIO.setDesctabl(t5687);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(deathrec.chdrChdrcoy);
		descIO.setLanguage(deathrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			deathrec.description.set(descIO.getLongdesc());
		}
		else {
			deathrec.description.fill("?");
		}
	}

protected void checkMethods600()
	{
		/*CHECK-DEATH-METHOD*/
		deathrec.endf.set("Y");
		if (isNE(t5687rec.dcmeth, SPACES)) {
			deathrec.fieldType.set("S");
			t5687rec.dcmeth.set(SPACES);
		}
		else {
			wsaaSwitch.set("Y");
			deathrec.status.set(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkUnprocessedUtrns700()
	{
		para701();
		doCheck710();
	}

protected void para701()
	{
		utrndthIO.setChdrcoy(deathrec.chdrChdrcoy);
		utrndthIO.setChdrnum(deathrec.chdrChdrnum);
		utrndthIO.setLife(deathrec.lifeLife);
		utrndthIO.setCoverage(deathrec.covrCoverage);
		utrndthIO.setRider(deathrec.covrRider);
		utrndthIO.setPlanSuffix("9999");
		utrndthIO.setProcSeqNo(ZERO);
		utrndthIO.setTranno(ZERO);
	}

protected void doCheck710()
	{
		SmartFileCode.execute(appVars, utrndthIO);
		if (isNE(utrndthIO.getStatuz(), varcom.oK)
		&& isNE(utrndthIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(utrndthIO.getStatuz());
			syserrrec.params.set(utrndthIO.getParams());
			fatalError9000();
		}
		if ((isNE(utrndthIO.getChdrcoy(), deathrec.chdrChdrcoy))
		|| (isNE(utrndthIO.getChdrnum(), deathrec.chdrChdrnum))
		|| (isNE(utrndthIO.getLife(), deathrec.lifeLife))
		|| (isNE(utrndthIO.getCoverage(), deathrec.covrCoverage))
		|| (isNE(utrndthIO.getRider(), deathrec.covrRider))
		|| (isEQ(utrndthIO.getStatuz(), varcom.endp))) {
			utrndthIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(utrndthIO.getSvp(), ZERO)) {
			if (isEQ(utrndthIO.getUnitType(), "I")) {
				wsaaInitialAmount.add(utrndthIO.getContractAmount());
				wsaaUtype.set("I");
			}
			else {
				wsaaAccumAmount.add(utrndthIO.getContractAmount());
				wsaaUtype.set("A");
			}
		}
		utrndthIO.setFunction(varcom.nextr);
		doCheck710();
		return ;
	}

protected void checkT5679800()
	{
		readT5679810();
		doCheck840();
	}

protected void readT5679810()
	{
		wsaaBatckey.set(deathrec.batckey);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(deathrec.chdrChdrcoy);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void doCheck840()
	{
		wsaaValidStatuz = "N";
		if (isEQ(covrIO.getRider(), "00")) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
			|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
				checkCoverage900();
			}
		}
		else {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
			|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
				checkRider950();
			}
		}
		/*EXIT*/
	}

protected void checkCoverage900()
	{
		/*COVERAGE*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()], covrIO.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
			|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()], covrIO.getPstatcode())) {
					wsaaValidStatuz = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void checkRider950()
	{
		/*COVERAGE*/
		if (isEQ(t5679rec.ridRiskStat[wsaaSub.toInt()], covrIO.getStatcode())) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
			|| isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)){
				if (isEQ(t5679rec.ridPremStat[wsaaSub.toInt()], covrIO.getPstatcode())) {
					wsaaValidStatuz = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void a100ReadInterestBearing()
	{
		a110Para();
	}

protected void a110Para()
	{
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(wsaaChdrcoy);
		hitsIO.setChdrnum(wsaaChdrnum);
		hitsIO.setLife(wsaaLife);
		hitsIO.setCoverage(wsaaCoverage);
		hitsIO.setRider(wsaaRider);
		hitsIO.setPlanSuffix(ZERO);
		hitsIO.setZintbfnd(wsaaVirtualFund);
		hitsIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(hitsIO.getChdrcoy(), deathrec.chdrChdrcoy)
		|| isNE(hitsIO.getChdrnum(), deathrec.chdrChdrnum)
		|| isNE(hitsIO.getLife(), deathrec.lifeLife)
		|| isNE(hitsIO.getCoverage(), deathrec.covrCoverage)
		|| isNE(hitsIO.getRider(), deathrec.covrRider)
		|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
			hitraloIO.setZintbfnd(SPACES);
			hitraloIO.setFunction(varcom.begn);
			while ( !(isEQ(hitraloIO.getStatuz(), varcom.endp))) {
				a300CheckUnprocessedHitrs();
			}
			
			deathrec.endf.set("S");
			hitsIO.setStatuz(varcom.endp);
			wsaaSwitch.set("N");
			return ;
		}
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		deathrec.estimatedVal.set(ZERO);
		deathrec.actualVal.set(ZERO);
		deathrec.element.set(SPACES);
		wsaaVirtualFund.set(hitsIO.getZintbfnd());
		wsaaNoMore.set(SPACES);
		while ( !(endOfFund.isTrue()
		|| isNE(hitsIO.getZintbfnd(), wsaaVirtualFund))) {
			a120AccumInterestBearing();
		}
		
		if (isEQ(hitsIO.getStatuz(), varcom.endp)
		|| isEQ(hitsIO.getStatuz(), varcom.oK)) {
			a200CheckEof();
			wsaaVirtualFund.set(hitsIO.getZintbfnd());
			wsaaLife.set(hitsIO.getLife());
			wsaaRider.set(hitsIO.getRider());
			wsaaCoverage.set(hitsIO.getCoverage());
			wsaaChdrnum.set(hitsIO.getChdrnum());
			wsaaSwitch.set("N");
		}
	}

protected void a120AccumInterestBearing()
	{
		a121Para();
	}

protected void a121Para()
	{
		wsaaAccumAmount.add(hitsIO.getZcurprmbal());
		hitraloIO.setZintbfnd(hitsIO.getZintbfnd());
		hitraloIO.setFunction(varcom.begn);
		while ( !(isEQ(hitraloIO.getStatuz(), varcom.endp))) {
			a300CheckUnprocessedHitrs();
		}
		
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(hitsIO.getChdrcoy(), deathrec.chdrChdrcoy)
		|| isNE(hitsIO.getChdrnum(), deathrec.chdrChdrnum)
		|| isNE(hitsIO.getLife(), deathrec.lifeLife)
		|| isNE(hitsIO.getCoverage(), deathrec.covrCoverage)
		|| isNE(hitsIO.getRider(), deathrec.covrRider)
		|| isEQ(hitsIO.getStatuz(), varcom.endp)) {
			/* A new coverage (or end of file) has been read, therefore,*/
			/* set ENDF processing indicator to "S". This will be picked*/
			/* up next time through to indicate that sum assured only*/
			/* processing should be done.*/
			deathrec.endf.set("S");
			wsaaNoMore.set("Y");
		}
	}

protected void a200CheckEof()
	{
		/*A210-CHECK*/
		/* MOVE WSAA-ACCUM-AMOUNT      TO CDTH-ESTIMATED-VAL.           */
		deathrec.element.set(wsaaVirtualFund);
		deathrec.fieldType.set("D");
		readTable300();
		if (isNE(deathrec.currcode, t5515rec.currcode)) {
			wsaaFundValue.set(wsaaAccumAmount);
			a400ConvertCurrency();
			wsaaAccumAmount.set(wsaaFundValue);
		}
		deathrec.estimatedVal.setRounded(wsaaAccumAmount);
		/*A290-EXIT*/
	}

protected void a300CheckUnprocessedHitrs()
	{
		a310Hitr();
		a320DoCheck();
	}

protected void a310Hitr()
	{
		hitraloIO.setChdrcoy(deathrec.chdrChdrcoy);
		hitraloIO.setChdrnum(deathrec.chdrChdrnum);
		hitraloIO.setLife(deathrec.lifeLife);
		hitraloIO.setCoverage(deathrec.covrCoverage);
		hitraloIO.setRider(deathrec.covrRider);
		hitraloIO.setPlanSuffix(ZERO);
		hitraloIO.setProcSeqNo(ZERO);
		hitraloIO.setTranno(ZERO);
		hitraloIO.setEffdate(ZERO);
	}

protected void a320DoCheck()
	{
		SmartFileCode.execute(appVars, hitraloIO);
		if (isNE(hitraloIO.getStatuz(), varcom.oK)
		&& isNE(hitraloIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitraloIO.getParams());
			fatalError9000();
		}
		if (isNE(hitraloIO.getChdrcoy(), deathrec.chdrChdrcoy)
		|| isNE(hitraloIO.getChdrnum(), deathrec.chdrChdrnum)
		|| isNE(hitraloIO.getLife(), deathrec.lifeLife)
		|| isNE(hitraloIO.getCoverage(), deathrec.covrCoverage)
		|| isNE(hitraloIO.getRider(), deathrec.covrRider)
		|| isEQ(hitraloIO.getStatuz(), varcom.endp)) {
			hitraloIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(hitraloIO.getSvp(), ZERO)) {
			wsaaAccumAmount.add(hitraloIO.getContractAmount());
		}
		hitraloIO.setFunction(varcom.nextr);
		a320DoCheck();
		return ;
	}

protected void a400ConvertCurrency()
	{
		a410Convert();
	}

protected void a410Convert()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.function.set("SURR");
		conlinkrec.currIn.set(t5515rec.currcode);
		conlinkrec.currOut.set(deathrec.currcode);
		conlinkrec.amountIn.set(wsaaFundValue);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(deathrec.effdate);
		conlinkrec.company.set(deathrec.chdrChdrcoy);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError9000();
		}
		wsaaFundValue.set(conlinkrec.amountOut);
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		deathrec.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
