package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr582screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 4, 17, 18, 23, 5, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 11;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {43, 44, 42, 41, 40, 37, 50}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 20, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr582ScreenVars sv = (Sr582ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr582screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr582screensfl, 
			sv.Sr582screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr582ScreenVars sv = (Sr582ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr582screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr582ScreenVars sv = (Sr582ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr582screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr582screensflWritten.gt(0))
		{
			sv.sr582screensfl.setCurrentIndex(0);
			sv.Sr582screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr582ScreenVars sv = (Sr582ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr582screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr582ScreenVars screenVars = (Sr582ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.sel.setFieldName("sel");
				screenVars.acdben.setFieldName("acdben");
				screenVars.bendesc.setFieldName("bendesc");
				screenVars.amtaout.setFieldName("amtaout");
				screenVars.benfreq.setFieldName("benfreq");
				screenVars.mxbenunt.setFieldName("mxbenunt");
				screenVars.actexp.setFieldName("actexp");
				screenVars.acbenunt.setFieldName("acbenunt");
				screenVars.gcdblind.setFieldName("gcdblind");
				screenVars.gcnetpy.setFieldName("gcnetpy");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.sel.set(dm.getField("sel"));
			screenVars.acdben.set(dm.getField("acdben"));
			screenVars.bendesc.set(dm.getField("bendesc"));
			screenVars.amtaout.set(dm.getField("amtaout"));
			screenVars.benfreq.set(dm.getField("benfreq"));
			screenVars.mxbenunt.set(dm.getField("mxbenunt"));
			screenVars.actexp.set(dm.getField("actexp"));
			screenVars.acbenunt.set(dm.getField("acbenunt"));
			screenVars.gcdblind.set(dm.getField("gcdblind"));
			screenVars.gcnetpy.set(dm.getField("gcnetpy"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr582ScreenVars screenVars = (Sr582ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.sel.setFieldName("sel");
				screenVars.acdben.setFieldName("acdben");
				screenVars.bendesc.setFieldName("bendesc");
				screenVars.amtaout.setFieldName("amtaout");
				screenVars.benfreq.setFieldName("benfreq");
				screenVars.mxbenunt.setFieldName("mxbenunt");
				screenVars.actexp.setFieldName("actexp");
				screenVars.acbenunt.setFieldName("acbenunt");
				screenVars.gcdblind.setFieldName("gcdblind");
				screenVars.gcnetpy.setFieldName("gcnetpy");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("sel").set(screenVars.sel);
			dm.getField("acdben").set(screenVars.acdben);
			dm.getField("bendesc").set(screenVars.bendesc);
			dm.getField("amtaout").set(screenVars.amtaout);
			dm.getField("benfreq").set(screenVars.benfreq);
			dm.getField("mxbenunt").set(screenVars.mxbenunt);
			dm.getField("actexp").set(screenVars.actexp);
			dm.getField("acbenunt").set(screenVars.acbenunt);
			dm.getField("gcdblind").set(screenVars.gcdblind);
			dm.getField("gcnetpy").set(screenVars.gcnetpy);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr582screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr582ScreenVars screenVars = (Sr582ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.sel.clearFormatting();
		screenVars.acdben.clearFormatting();
		screenVars.bendesc.clearFormatting();
		screenVars.amtaout.clearFormatting();
		screenVars.benfreq.clearFormatting();
		screenVars.mxbenunt.clearFormatting();
		screenVars.actexp.clearFormatting();
		screenVars.acbenunt.clearFormatting();
		screenVars.gcdblind.clearFormatting();
		screenVars.gcnetpy.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr582ScreenVars screenVars = (Sr582ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.sel.setClassString("");
		screenVars.acdben.setClassString("");
		screenVars.bendesc.setClassString("");
		screenVars.amtaout.setClassString("");
		screenVars.benfreq.setClassString("");
		screenVars.mxbenunt.setClassString("");
		screenVars.actexp.setClassString("");
		screenVars.acbenunt.setClassString("");
		screenVars.gcdblind.setClassString("");
		screenVars.gcnetpy.setClassString("");
	}

/**
 * Clear all the variables in Sr582screensfl
 */
	public static void clear(VarModel pv) {
		Sr582ScreenVars screenVars = (Sr582ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.sel.clear();
		screenVars.acdben.clear();
		screenVars.bendesc.clear();
		screenVars.amtaout.clear();
		screenVars.benfreq.clear();
		screenVars.mxbenunt.clear();
		screenVars.actexp.clear();
		screenVars.acbenunt.clear();
		screenVars.gcdblind.clear();
		screenVars.gcnetpy.clear();
	}
}
