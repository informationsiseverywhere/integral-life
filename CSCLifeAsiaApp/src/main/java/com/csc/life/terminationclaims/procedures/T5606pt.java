/*
 * File: T5606pt.java
 * Date: 30 August 2009 2:23:42
 * Author: Quipoz Limited
 * 
 * Class transformed from T5606PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5606.
*
*
*****************************************************************
* </pre>
*/
public class T5606pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5606rec t5606rec = new T5606rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5606pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5606rec.t5606Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(t5606rec.sumInsMin);
		generalCopyLinesInner.fieldNo008.set(t5606rec.sumInsMax);
		generalCopyLinesInner.fieldNo009.set(t5606rec.benfreq);
		generalCopyLinesInner.fieldNo010.set(t5606rec.dfrprd);
		generalCopyLinesInner.fieldNo011.set(t5606rec.ageIssageFrm01);
		generalCopyLinesInner.fieldNo012.set(t5606rec.ageIssageTo01);
		generalCopyLinesInner.fieldNo013.set(t5606rec.ageIssageFrm02);
		generalCopyLinesInner.fieldNo014.set(t5606rec.ageIssageTo02);
		generalCopyLinesInner.fieldNo015.set(t5606rec.ageIssageFrm03);
		generalCopyLinesInner.fieldNo016.set(t5606rec.ageIssageTo03);
		generalCopyLinesInner.fieldNo017.set(t5606rec.ageIssageFrm04);
		generalCopyLinesInner.fieldNo018.set(t5606rec.ageIssageTo04);
		generalCopyLinesInner.fieldNo019.set(t5606rec.ageIssageFrm05);
		generalCopyLinesInner.fieldNo020.set(t5606rec.ageIssageTo05);
		generalCopyLinesInner.fieldNo021.set(t5606rec.ageIssageFrm06);
		generalCopyLinesInner.fieldNo022.set(t5606rec.ageIssageTo06);
		generalCopyLinesInner.fieldNo023.set(t5606rec.ageIssageFrm07);
		generalCopyLinesInner.fieldNo024.set(t5606rec.ageIssageTo07);
		generalCopyLinesInner.fieldNo025.set(t5606rec.ageIssageFrm08);
		generalCopyLinesInner.fieldNo026.set(t5606rec.ageIssageTo08);
		generalCopyLinesInner.fieldNo027.set(t5606rec.riskCessageFrom01);
		generalCopyLinesInner.fieldNo028.set(t5606rec.riskCessageTo01);
		generalCopyLinesInner.fieldNo029.set(t5606rec.riskCessageFrom02);
		generalCopyLinesInner.fieldNo030.set(t5606rec.riskCessageTo02);
		generalCopyLinesInner.fieldNo031.set(t5606rec.riskCessageFrom03);
		generalCopyLinesInner.fieldNo032.set(t5606rec.riskCessageTo03);
		generalCopyLinesInner.fieldNo033.set(t5606rec.riskCessageFrom04);
		generalCopyLinesInner.fieldNo034.set(t5606rec.riskCessageTo04);
		generalCopyLinesInner.fieldNo035.set(t5606rec.riskCessageFrom05);
		generalCopyLinesInner.fieldNo036.set(t5606rec.riskCessageTo05);
		generalCopyLinesInner.fieldNo037.set(t5606rec.riskCessageFrom06);
		generalCopyLinesInner.fieldNo038.set(t5606rec.riskCessageTo06);
		generalCopyLinesInner.fieldNo039.set(t5606rec.riskCessageFrom07);
		generalCopyLinesInner.fieldNo040.set(t5606rec.riskCessageTo07);
		generalCopyLinesInner.fieldNo041.set(t5606rec.riskCessageFrom08);
		generalCopyLinesInner.fieldNo042.set(t5606rec.riskCessageTo08);
		generalCopyLinesInner.fieldNo043.set(t5606rec.premCessageFrom01);
		generalCopyLinesInner.fieldNo044.set(t5606rec.premCessageTo01);
		generalCopyLinesInner.fieldNo045.set(t5606rec.premCessageFrom02);
		generalCopyLinesInner.fieldNo046.set(t5606rec.premCessageTo02);
		generalCopyLinesInner.fieldNo047.set(t5606rec.premCessageFrom03);
		generalCopyLinesInner.fieldNo048.set(t5606rec.premCessageTo03);
		generalCopyLinesInner.fieldNo049.set(t5606rec.premCessageFrom04);
		generalCopyLinesInner.fieldNo050.set(t5606rec.premCessageTo04);
		generalCopyLinesInner.fieldNo051.set(t5606rec.premCessageFrom05);
		generalCopyLinesInner.fieldNo052.set(t5606rec.premCessageTo05);
		generalCopyLinesInner.fieldNo053.set(t5606rec.premCessageFrom06);
		generalCopyLinesInner.fieldNo054.set(t5606rec.premCessageTo06);
		generalCopyLinesInner.fieldNo055.set(t5606rec.premCessageFrom07);
		generalCopyLinesInner.fieldNo056.set(t5606rec.premCessageTo07);
		generalCopyLinesInner.fieldNo057.set(t5606rec.premCessageFrom08);
		generalCopyLinesInner.fieldNo058.set(t5606rec.premCessageTo08);
		generalCopyLinesInner.fieldNo075.set(t5606rec.eaage);
		generalCopyLinesInner.fieldNo076.set(t5606rec.termIssageFrm01);
		generalCopyLinesInner.fieldNo077.set(t5606rec.termIssageTo01);
		generalCopyLinesInner.fieldNo078.set(t5606rec.termIssageFrm02);
		generalCopyLinesInner.fieldNo079.set(t5606rec.termIssageTo02);
		generalCopyLinesInner.fieldNo080.set(t5606rec.termIssageFrm03);
		generalCopyLinesInner.fieldNo081.set(t5606rec.termIssageTo03);
		generalCopyLinesInner.fieldNo082.set(t5606rec.termIssageFrm04);
		generalCopyLinesInner.fieldNo083.set(t5606rec.termIssageTo04);
		generalCopyLinesInner.fieldNo084.set(t5606rec.termIssageFrm05);
		generalCopyLinesInner.fieldNo085.set(t5606rec.termIssageTo05);
		generalCopyLinesInner.fieldNo086.set(t5606rec.termIssageFrm06);
		generalCopyLinesInner.fieldNo087.set(t5606rec.termIssageTo06);
		generalCopyLinesInner.fieldNo088.set(t5606rec.termIssageFrm07);
		generalCopyLinesInner.fieldNo089.set(t5606rec.termIssageTo07);
		generalCopyLinesInner.fieldNo090.set(t5606rec.termIssageFrm08);
		generalCopyLinesInner.fieldNo091.set(t5606rec.termIssageTo08);
		generalCopyLinesInner.fieldNo092.set(t5606rec.riskCesstermFrom01);
		generalCopyLinesInner.fieldNo093.set(t5606rec.riskCesstermTo01);
		generalCopyLinesInner.fieldNo094.set(t5606rec.riskCesstermFrom02);
		generalCopyLinesInner.fieldNo095.set(t5606rec.riskCesstermTo02);
		generalCopyLinesInner.fieldNo096.set(t5606rec.riskCesstermFrom03);
		generalCopyLinesInner.fieldNo097.set(t5606rec.riskCesstermTo03);
		generalCopyLinesInner.fieldNo098.set(t5606rec.riskCesstermFrom04);
		generalCopyLinesInner.fieldNo099.set(t5606rec.riskCesstermTo04);
		generalCopyLinesInner.fieldNo100.set(t5606rec.riskCesstermFrom05);
		generalCopyLinesInner.fieldNo101.set(t5606rec.riskCesstermTo05);
		generalCopyLinesInner.fieldNo102.set(t5606rec.riskCesstermFrom06);
		generalCopyLinesInner.fieldNo103.set(t5606rec.riskCesstermTo06);
		generalCopyLinesInner.fieldNo104.set(t5606rec.riskCesstermFrom07);
		generalCopyLinesInner.fieldNo105.set(t5606rec.riskCesstermTo07);
		generalCopyLinesInner.fieldNo106.set(t5606rec.riskCesstermFrom08);
		generalCopyLinesInner.fieldNo107.set(t5606rec.riskCesstermTo08);
		generalCopyLinesInner.fieldNo108.set(t5606rec.premCesstermFrom01);
		generalCopyLinesInner.fieldNo109.set(t5606rec.premCesstermTo01);
		generalCopyLinesInner.fieldNo110.set(t5606rec.premCesstermFrom02);
		generalCopyLinesInner.fieldNo111.set(t5606rec.premCesstermTo02);
		generalCopyLinesInner.fieldNo112.set(t5606rec.premCesstermFrom03);
		generalCopyLinesInner.fieldNo113.set(t5606rec.premCesstermTo03);
		generalCopyLinesInner.fieldNo114.set(t5606rec.premCesstermFrom04);
		generalCopyLinesInner.fieldNo115.set(t5606rec.premCesstermTo04);
		generalCopyLinesInner.fieldNo116.set(t5606rec.premCesstermFrom05);
		generalCopyLinesInner.fieldNo117.set(t5606rec.premCesstermTo05);
		generalCopyLinesInner.fieldNo118.set(t5606rec.premCesstermFrom06);
		generalCopyLinesInner.fieldNo119.set(t5606rec.premCesstermTo06);
		generalCopyLinesInner.fieldNo120.set(t5606rec.premCesstermFrom07);
		generalCopyLinesInner.fieldNo121.set(t5606rec.premCesstermTo07);
		generalCopyLinesInner.fieldNo122.set(t5606rec.premCesstermFrom08);
		generalCopyLinesInner.fieldNo123.set(t5606rec.premCesstermTo08);
		generalCopyLinesInner.fieldNo140.set(t5606rec.specind);
		generalCopyLinesInner.fieldNo141.set(t5606rec.mortcls01);
		generalCopyLinesInner.fieldNo142.set(t5606rec.mortcls02);
		generalCopyLinesInner.fieldNo143.set(t5606rec.mortcls03);
		generalCopyLinesInner.fieldNo144.set(t5606rec.mortcls04);
		generalCopyLinesInner.fieldNo145.set(t5606rec.mortcls05);
		generalCopyLinesInner.fieldNo146.set(t5606rec.mortcls06);
		generalCopyLinesInner.fieldNo147.set(t5606rec.liencd01);
		generalCopyLinesInner.fieldNo148.set(t5606rec.liencd02);
		generalCopyLinesInner.fieldNo149.set(t5606rec.liencd03);
		generalCopyLinesInner.fieldNo150.set(t5606rec.liencd04);
		generalCopyLinesInner.fieldNo151.set(t5606rec.liencd05);
		generalCopyLinesInner.fieldNo152.set(t5606rec.liencd06);
		generalCopyLinesInner.fieldNo059.set(t5606rec.benCessageFrom01);
		generalCopyLinesInner.fieldNo060.set(t5606rec.benCessageTo01);
		generalCopyLinesInner.fieldNo061.set(t5606rec.benCessageFrom02);
		generalCopyLinesInner.fieldNo062.set(t5606rec.benCessageTo02);
		generalCopyLinesInner.fieldNo063.set(t5606rec.benCessageFrom03);
		generalCopyLinesInner.fieldNo064.set(t5606rec.benCessageTo03);
		generalCopyLinesInner.fieldNo065.set(t5606rec.benCessageFrom04);
		generalCopyLinesInner.fieldNo066.set(t5606rec.benCessageTo04);
		generalCopyLinesInner.fieldNo067.set(t5606rec.benCessageFrom05);
		generalCopyLinesInner.fieldNo068.set(t5606rec.benCessageTo05);
		generalCopyLinesInner.fieldNo069.set(t5606rec.benCessageFrom06);
		generalCopyLinesInner.fieldNo070.set(t5606rec.benCessageTo06);
		generalCopyLinesInner.fieldNo071.set(t5606rec.benCessageFrom07);
		generalCopyLinesInner.fieldNo072.set(t5606rec.benCessageTo07);
		generalCopyLinesInner.fieldNo073.set(t5606rec.benCessageFrom08);
		generalCopyLinesInner.fieldNo074.set(t5606rec.benCessageTo08);
		generalCopyLinesInner.fieldNo124.set(t5606rec.benCesstermFrm01);
		generalCopyLinesInner.fieldNo125.set(t5606rec.benCesstermTo01);
		generalCopyLinesInner.fieldNo126.set(t5606rec.benCesstermFrm02);
		generalCopyLinesInner.fieldNo127.set(t5606rec.benCesstermTo02);
		generalCopyLinesInner.fieldNo128.set(t5606rec.benCesstermFrm03);
		generalCopyLinesInner.fieldNo129.set(t5606rec.benCesstermTo03);
		generalCopyLinesInner.fieldNo130.set(t5606rec.benCesstermFrm04);
		generalCopyLinesInner.fieldNo131.set(t5606rec.benCesstermTo04);
		generalCopyLinesInner.fieldNo132.set(t5606rec.benCesstermFrm05);
		generalCopyLinesInner.fieldNo133.set(t5606rec.benCesstermTo05);
		generalCopyLinesInner.fieldNo134.set(t5606rec.benCesstermFrm06);
		generalCopyLinesInner.fieldNo135.set(t5606rec.benCesstermTo06);
		generalCopyLinesInner.fieldNo136.set(t5606rec.benCesstermFrm07);
		generalCopyLinesInner.fieldNo137.set(t5606rec.benCesstermTo07);
		generalCopyLinesInner.fieldNo138.set(t5606rec.benCesstermFrm08);
		generalCopyLinesInner.fieldNo139.set(t5606rec.benCesstermTo08);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine001, 23, FILLER).init("Regular Benefit Generic Edit Rules              S5606");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 11, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 21);
	private FixedLengthStringData filler5 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 26, FILLER).init("   Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(46);
	private FixedLengthStringData filler7 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 19);
	private FixedLengthStringData filler8 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 29, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 36);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(79);
	private FixedLengthStringData filler9 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Benefit Min:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine004, 15).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine004, 30, FILLER).init("   Max:");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine004, 39).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 54, FILLER).init("  Freq:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 62);
	private FixedLengthStringData filler12 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 64, FILLER).init(" Deferment:");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine004, 76).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(79);
	private FixedLengthStringData filler13 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(61).isAPartOf(wsaaPrtLine005, 18, FILLER).init("F---T   F---TF---T   F---T   F---T   F---T   F---T   F---T");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(80);
	private FixedLengthStringData filler15 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Issue Age:");
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 17).setPattern("ZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 21).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 25).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 29).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 33).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 37).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 41).setPattern("ZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 45).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 49).setPattern("ZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 53).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 61).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 65).setPattern("ZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 69).setPattern("ZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 73).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(80);
	private FixedLengthStringData filler31 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine007, 0, FILLER).init(" Cessation Age:");
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 17).setPattern("ZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 21).setPattern("ZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 25).setPattern("ZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 29).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 33).setPattern("ZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 37).setPattern("ZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 41).setPattern("ZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 45).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 49).setPattern("ZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 53).setPattern("ZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 57).setPattern("ZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 61).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 65).setPattern("ZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 69).setPattern("ZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 73).setPattern("ZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(80);
	private FixedLengthStringData filler47 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" Prem Cess Age:");
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 17).setPattern("ZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 21).setPattern("ZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 25).setPattern("ZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 29).setPattern("ZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 33).setPattern("ZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 37).setPattern("ZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 41).setPattern("ZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 45).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 49).setPattern("ZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 53).setPattern("ZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 57).setPattern("ZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 61).setPattern("ZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 65).setPattern("ZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 69).setPattern("ZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 73).setPattern("ZZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(80);
	private FixedLengthStringData filler63 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine009, 0, FILLER).init(" Bene Cess Age:");
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 17).setPattern("ZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 21).setPattern("ZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 25).setPattern("ZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 29).setPattern("ZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 33).setPattern("ZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 37).setPattern("ZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 41).setPattern("ZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 45).setPattern("ZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 49).setPattern("ZZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 53).setPattern("ZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 57).setPattern("ZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 61).setPattern("ZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 65).setPattern("ZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 69).setPattern("ZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 73).setPattern("ZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(44);
	private FixedLengthStringData filler79 = new FixedLengthStringData(35).isAPartOf(wsaaPrtLine010, 0, FILLER).init(" Cess date, Anniversary or Exact:");
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 35);
	private FixedLengthStringData filler80 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 36, FILLER).init("   (A/E)");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(80);
	private FixedLengthStringData filler81 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" Issue Age:");
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 17).setPattern("ZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 21).setPattern("ZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 25).setPattern("ZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 29).setPattern("ZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 33).setPattern("ZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 37).setPattern("ZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 41).setPattern("ZZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 45).setPattern("ZZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 49).setPattern("ZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 53).setPattern("ZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 57).setPattern("ZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 61).setPattern("ZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 65).setPattern("ZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 69).setPattern("ZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 73).setPattern("ZZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(80);
	private FixedLengthStringData filler97 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine012, 0, FILLER).init(" Coverage Term:");
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 17).setPattern("ZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 21).setPattern("ZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 25).setPattern("ZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 29).setPattern("ZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 33).setPattern("ZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 37).setPattern("ZZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 41).setPattern("ZZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 45).setPattern("ZZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 49).setPattern("ZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 53).setPattern("ZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 57).setPattern("ZZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 61).setPattern("ZZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 65).setPattern("ZZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 69).setPattern("ZZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 73).setPattern("ZZZ");
	private FixedLengthStringData filler112 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(80);
	private FixedLengthStringData filler113 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" Prem Cess Term:");
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 17).setPattern("ZZZ");
	private FixedLengthStringData filler114 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo109 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 21).setPattern("ZZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo110 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 25).setPattern("ZZZ");
	private FixedLengthStringData filler116 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo111 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 29).setPattern("ZZZ");
	private FixedLengthStringData filler117 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo112 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 33).setPattern("ZZZ");
	private FixedLengthStringData filler118 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 37).setPattern("ZZZ");
	private FixedLengthStringData filler119 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo114 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 41).setPattern("ZZZ");
	private FixedLengthStringData filler120 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo115 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 45).setPattern("ZZZ");
	private FixedLengthStringData filler121 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo116 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 49).setPattern("ZZZ");
	private FixedLengthStringData filler122 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo117 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 53).setPattern("ZZZ");
	private FixedLengthStringData filler123 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo118 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 57).setPattern("ZZZ");
	private FixedLengthStringData filler124 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo119 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 61).setPattern("ZZZ");
	private FixedLengthStringData filler125 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo120 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 65).setPattern("ZZZ");
	private FixedLengthStringData filler126 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo121 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 69).setPattern("ZZZ");
	private FixedLengthStringData filler127 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo122 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 73).setPattern("ZZZ");
	private FixedLengthStringData filler128 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo123 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(80);
	private FixedLengthStringData filler129 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" Bene Cess Term:");
	private ZonedDecimalData fieldNo124 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 17).setPattern("ZZZ");
	private FixedLengthStringData filler130 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo125 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 21).setPattern("ZZZ");
	private FixedLengthStringData filler131 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo126 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 25).setPattern("ZZZ");
	private FixedLengthStringData filler132 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo127 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 29).setPattern("ZZZ");
	private FixedLengthStringData filler133 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo128 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 33).setPattern("ZZZ");
	private FixedLengthStringData filler134 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo129 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 37).setPattern("ZZZ");
	private FixedLengthStringData filler135 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo130 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 41).setPattern("ZZZ");
	private FixedLengthStringData filler136 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo131 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 45).setPattern("ZZZ");
	private FixedLengthStringData filler137 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo132 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 49).setPattern("ZZZ");
	private FixedLengthStringData filler138 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo133 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 53).setPattern("ZZZ");
	private FixedLengthStringData filler139 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo134 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 57).setPattern("ZZZ");
	private FixedLengthStringData filler140 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo135 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 61).setPattern("ZZZ");
	private FixedLengthStringData filler141 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo136 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 65).setPattern("ZZZ");
	private FixedLengthStringData filler142 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo137 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 69).setPattern("ZZZ");
	private FixedLengthStringData filler143 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo138 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 73).setPattern("ZZZ");
	private FixedLengthStringData filler144 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo139 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(18);
	private FixedLengthStringData filler145 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" Special Terms:");
	private FixedLengthStringData fieldNo140 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 17);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(47);
	private FixedLengthStringData filler146 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" Mortality Class:");
	private FixedLengthStringData fieldNo141 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 21);
	private FixedLengthStringData filler147 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo142 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 26);
	private FixedLengthStringData filler148 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo143 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 31);
	private FixedLengthStringData filler149 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo144 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 36);
	private FixedLengthStringData filler150 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo145 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 41);
	private FixedLengthStringData filler151 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo146 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 46);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(48);
	private FixedLengthStringData filler152 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" Lien Codes:");
	private FixedLengthStringData fieldNo147 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 21);
	private FixedLengthStringData filler153 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo148 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 26);
	private FixedLengthStringData filler154 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo149 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 31);
	private FixedLengthStringData filler155 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo150 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 36);
	private FixedLengthStringData filler156 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo151 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 41);
	private FixedLengthStringData filler157 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo152 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 46);
}
}
