package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR582
 * @version 1.0 generated on 30/08/09 07:20
 * @author Quipoz
 */
public class Sr582ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(520);
	public FixedLengthStringData dataFields = new FixedLengthStringData(232).isAPartOf(dataArea, 0);
	public ZonedDecimalData tclmamt = DD.tclmamt.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData cdesc = DD.cdesc.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,32);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,40);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData crtabled = DD.crtabled.copy().isAPartOf(dataFields,52);
	public FixedLengthStringData diagcde = DD.diagcde.copy().isAPartOf(dataFields,82);
	public ZonedDecimalData dischdt = DD.dischdt.copyToZonedDecimal().isAPartOf(dataFields,87);
	public ZonedDecimalData gcadmdt = DD.gcadmdt.copyToZonedDecimal().isAPartOf(dataFields,95);
	public FixedLengthStringData givname = DD.givname.copy().isAPartOf(dataFields,103);
	public ZonedDecimalData incurdt = DD.incurdt.copyToZonedDecimal().isAPartOf(dataFields,123);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,178);
	public ZonedDecimalData rgpynum = DD.rgpynum.copyToZonedDecimal().isAPartOf(dataFields,186);
	public FixedLengthStringData shortdesc = DD.shortdesc.copy().isAPartOf(dataFields,191);
	public FixedLengthStringData zdoctor = DD.zdoctor.copy().isAPartOf(dataFields,201);
	public FixedLengthStringData zmedprv = DD.zmedprv.copy().isAPartOf(dataFields,209);
	public ZonedDecimalData zrsumin = DD.zrsumin.copyToZonedDecimal().isAPartOf(dataFields,219);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 232);
	public FixedLengthStringData tclmamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData crtabledErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData diagcdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData dischdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData gcadmdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData givnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData incurdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData rgpynumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData shortdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData zdoctorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData zmedprvErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData zrsuminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 304);
	public FixedLengthStringData[] tclmamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] crtabledOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] diagcdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] dischdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] gcadmdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] givnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] incurdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] rgpynumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] shortdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] zdoctorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] zmedprvOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] zrsuminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
//Ticket TLIFE-833 start-sgadkari
	//ILIFE-1821 starts
//	public FixedLengthStringData subfileArea = new FixedLengthStringData(239);
//	public FixedLengthStringData subfileFields = new FixedLengthStringData(77).isAPartOf(subfileArea, 0);
	public FixedLengthStringData subfileArea = new FixedLengthStringData(244);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(82).isAPartOf(subfileArea, 0);
	//ILIFE-1821 ends
	public ZonedDecimalData acbenunt = DD.acbenunt.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData acdben = DD.acdben.copy().isAPartOf(subfileFields,3);
	public ZonedDecimalData actexp = DD.actexp.copyToZonedDecimal().isAPartOf(subfileFields,8);
	//ILIFE-1821 starts
//	public ZonedDecimalData amtaout = DD.amtaout.copyToZonedDecimal().isAPartOf(subfileFields,20);
	public ZonedDecimalData amtaout = DD.amtaout1.copyToZonedDecimal().isAPartOf(subfileFields,20);
	public FixedLengthStringData bendesc = DD.benefits.copy().isAPartOf(subfileFields,33);
	public FixedLengthStringData benfreq = DD.benfreq.copy().isAPartOf(subfileFields,62);
	public FixedLengthStringData gcdblind = DD.gcdblind.copy().isAPartOf(subfileFields,64);
//	public ZonedDecimalData gcnetpy = DD.gcnetpy.copyToZonedDecimal().isAPartOf(subfileFields,65);
	public ZonedDecimalData gcnetpy = DD.amtaout1.copyToZonedDecimal().isAPartOf(subfileFields,65);
	public ZonedDecimalData mxbenunt = DD.mxbenunt.copyToZonedDecimal().isAPartOf(subfileFields,78);
	public FixedLengthStringData sel = DD.sel.copy().isAPartOf(subfileFields,81);
//	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 77);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 82);
	//ILIFE-1821 ends
	public FixedLengthStringData acbenuntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData acdbenErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData actexpErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData amtaoutErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData bendescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData benfreqErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData gcdblindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData gcnetpyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData mxbenuntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData selErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	//ILIFE-1821 starts
//	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 117);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 122);
	//ILIFE-1821 ends
	public FixedLengthStringData[] acbenuntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] acdbenOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] actexpOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] amtaoutOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] bendescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] benfreqOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] gcdblindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] gcnetpyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] mxbenuntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] selOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	//ILIFE-1821 starts
//	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 237);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 242);
	//ILIFE-1821 ends
//	Ticket ILIFE-833 end
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dischdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData gcadmdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData incurdtDisp = new FixedLengthStringData(10);

	public LongData Sr582screensflWritten = new LongData(0);
	public LongData Sr582screenctlWritten = new LongData(0);
	public LongData Sr582screenWritten = new LongData(0);
	public LongData Sr582protectWritten = new LongData(0);
	public GeneralTable sr582screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr582screensfl;
	}

	public Sr582ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(diagcdeOut,new String[] {"41","50","-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gcadmdtOut,new String[] {"42","50","-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dischdtOut,new String[] {"43","50","-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdoctorOut,new String[] {"44","50","-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmedprvOut,new String[] {"40","37","-40",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {sel, acdben, bendesc, amtaout, benfreq, mxbenunt, actexp, acbenunt, gcdblind, gcnetpy};
		screenSflOutFields = new BaseData[][] {selOut, acdbenOut, bendescOut, amtaoutOut, benfreqOut, mxbenuntOut, actexpOut, acbenuntOut, gcdblindOut, gcnetpyOut};
		screenSflErrFields = new BaseData[] {selErr, acdbenErr, bendescErr, amtaoutErr, benfreqErr, mxbenuntErr, actexpErr, acbenuntErr, gcdblindErr, gcnetpyErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, crtable, crtabled, lifenum, lifename, rgpynum, diagcde, shortdesc, crrcd, gcadmdt, dischdt, incurdt, zdoctor, givname, zmedprv, cdesc, zrsumin, tclmamt};
		screenOutFields = new BaseData[][] {chdrnumOut, crtableOut, crtabledOut, lifenumOut, lifenameOut, rgpynumOut, diagcdeOut, shortdescOut, crrcdOut, gcadmdtOut, dischdtOut, incurdtOut, zdoctorOut, givnameOut, zmedprvOut, cdescOut, zrsuminOut, tclmamtOut};
		screenErrFields = new BaseData[] {chdrnumErr, crtableErr, crtabledErr, lifenumErr, lifenameErr, rgpynumErr, diagcdeErr, shortdescErr, crrcdErr, gcadmdtErr, dischdtErr, incurdtErr, zdoctorErr, givnameErr, zmedprvErr, cdescErr, zrsuminErr, tclmamtErr};
		screenDateFields = new BaseData[] {crrcd, gcadmdt, dischdt, incurdt};
		screenDateErrFields = new BaseData[] {crrcdErr, gcadmdtErr, dischdtErr, incurdtErr};
		screenDateDispFields = new BaseData[] {crrcdDisp, gcadmdtDisp, dischdtDisp, incurdtDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr582screen.class;
		screenSflRecord = Sr582screensfl.class;
		screenCtlRecord = Sr582screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr582protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr582screenctl.lrec.pageSubfile);
	}
}
