package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class Sa509screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {8, 4, 5, 6, 7, 2, 3, 10, 9, 11}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa509ScreenVars sv = (Sa509ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sa509screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sa509screensfl, 
			sv.Sa509screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sa509ScreenVars sv = (Sa509ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sa509screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sa509ScreenVars sv = (Sa509ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sa509screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sa509screensflWritten.gt(0))
		{
			sv.sa509screensfl.setCurrentIndex(0);
			sv.Sa509screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sa509ScreenVars sv = (Sa509ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sa509screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa509ScreenVars screenVars = (Sa509ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");				
				screenVars.select.setFieldName("select");
				screenVars.seqnoen.setFieldName("seqnoen");
				screenVars.seqnogp.setFieldName("seqnogp");
				screenVars.accdesc.setFieldName("accdesc");
				screenVars.effdates.setFieldName("effdates");
				screenVars.acctime.setFieldName("acctime");
				screenVars.userid.setFieldName("userid");
				screenVars.effdatesDisp.setFieldName("effdatesDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.select.set(dm.getField("select"));
			screenVars.seqnoen.set(dm.getField("seqnoen"));
			screenVars.seqnogp.set(dm.getField("seqnogp"));
			screenVars.accdesc.set(dm.getField("accdesc"));
			screenVars.effdates.set(dm.getField("effdates"));
			screenVars.acctime.set(dm.getField("acctime"));
			screenVars.userid.set(dm.getField("userid"));
			screenVars.effdatesDisp.set(dm.getField("effdatesDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sa509ScreenVars screenVars = (Sa509ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.select.setFieldName("select");
				screenVars.seqnoen.setFieldName("seqnoen");
				screenVars.seqnogp.setFieldName("seqnogp");
				screenVars.accdesc.setFieldName("accdesc");
				screenVars.effdates.setFieldName("effdates");
				screenVars.acctime.setFieldName("acctime");
				screenVars.userid.setFieldName("userid");
				screenVars.effdatesDisp.setFieldName("effdatesDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("select").set(screenVars.select);
			dm.getField("seqnoen").set(screenVars.seqnoen);
			dm.getField("seqnogp").set(screenVars.seqnogp);
			dm.getField("accdesc").set(screenVars.accdesc);
			dm.getField("effdates").set(screenVars.effdates);
			dm.getField("acctime").set(screenVars.acctime);
			dm.getField("userid").set(screenVars.userid);
			dm.getField("effdatesDisp").set(screenVars.effdatesDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sa509screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sa509ScreenVars screenVars = (Sa509ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.seqnoen.clearFormatting();
		screenVars.seqnogp.clearFormatting();
		screenVars.accdesc.clearFormatting();
		screenVars.effdates.clearFormatting();
		screenVars.acctime.clearFormatting();
		screenVars.userid.clearFormatting();
		screenVars.effdatesDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sa509ScreenVars screenVars = (Sa509ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.select.setClassString("");
		screenVars.seqnoen.setClassString("");
		screenVars.seqnogp.setClassString("");
		screenVars.accdesc.setClassString("");
		screenVars.effdates.setClassString("");
		screenVars.acctime.setClassString("");
		screenVars.userid.setClassString("");
		screenVars.effdatesDisp.setClassString("");
	}

/**
 * Clear all the variables in S5186screensfl
 */
	public static void clear(VarModel pv) {
		Sa509ScreenVars screenVars = (Sa509ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.select.clear();
		screenVars.seqnoen.clear();
		screenVars.seqnogp.clear();
		screenVars.accdesc.clear();
		screenVars.effdates.clear();
		screenVars.acctime.clear();
		screenVars.userid.clear();
		screenVars.effdatesDisp.clear();
	}
}

