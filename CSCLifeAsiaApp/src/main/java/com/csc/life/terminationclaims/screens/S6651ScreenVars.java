package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6651
 * @version 1.0 generated on 30/08/09 06:56
 * @author Quipoz
 */
public class S6651ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(807);
	public FixedLengthStringData dataFields = new FixedLengthStringData(343).isAPartOf(dataArea, 0);
	public FixedLengthStringData adjustiu = DD.adjustiu.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bidoffer = DD.bidoffer.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData feepcs = new FixedLengthStringData(25).isAPartOf(dataFields, 3);
	public ZonedDecimalData[] feepc = ZDArrayPartOfStructure(5, 5, 2, feepcs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(25).isAPartOf(feepcs, 0, FILLER_REDEFINE);
	public ZonedDecimalData feepc02 = DD.feepc.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData feepc03 = DD.feepc.copyToZonedDecimal().isAPartOf(filler,5);
	public ZonedDecimalData feepc04 = DD.feepc.copyToZonedDecimal().isAPartOf(filler,10);
	public ZonedDecimalData feepc05 = DD.feepc.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData feepc06 = DD.feepc.copyToZonedDecimal().isAPartOf(filler,20);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,28);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,36);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,44);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,52);
	public FixedLengthStringData ovrsuma = DD.ovrsuma.copy().isAPartOf(dataFields,82);
	public FixedLengthStringData pufeemaxs = new FixedLengthStringData(85).isAPartOf(dataFields, 83);
	public ZonedDecimalData[] pufeemax = ZDArrayPartOfStructure(5, 17, 2, pufeemaxs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(85).isAPartOf(pufeemaxs, 0, FILLER_REDEFINE);
	public ZonedDecimalData pufeemax02 = DD.pufeemax.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData pufeemax03 = DD.pufeemax.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData pufeemax04 = DD.pufeemax.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData pufeemax05 = DD.pufeemax.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData pufeemax06 = DD.pufeemax.copyToZonedDecimal().isAPartOf(filler1,68);
	public FixedLengthStringData pufeemins = new FixedLengthStringData(85).isAPartOf(dataFields, 168);
	public ZonedDecimalData[] pufeemin = ZDArrayPartOfStructure(5, 17, 2, pufeemins, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(85).isAPartOf(pufeemins, 0, FILLER_REDEFINE);
	public ZonedDecimalData pufeemin02 = DD.pufeemin.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData pufeemin03 = DD.pufeemin.copyToZonedDecimal().isAPartOf(filler2,17);
	public ZonedDecimalData pufeemin04 = DD.pufeemin.copyToZonedDecimal().isAPartOf(filler2,34);
	public ZonedDecimalData pufeemin05 = DD.pufeemin.copyToZonedDecimal().isAPartOf(filler2,51);
	public ZonedDecimalData pufeemin06 = DD.pufeemin.copyToZonedDecimal().isAPartOf(filler2,68);
	public FixedLengthStringData puffamts = new FixedLengthStringData(85).isAPartOf(dataFields, 253);
	public ZonedDecimalData[] puffamt = ZDArrayPartOfStructure(5, 17, 2, puffamts, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(85).isAPartOf(puffamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData puffamt02 = DD.puffamt.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData puffamt03 = DD.puffamt.copyToZonedDecimal().isAPartOf(filler3,17);
	public ZonedDecimalData puffamt04 = DD.puffamt.copyToZonedDecimal().isAPartOf(filler3,34);
	public ZonedDecimalData puffamt05 = DD.puffamt.copyToZonedDecimal().isAPartOf(filler3,51);
	public ZonedDecimalData puffamt06 = DD.puffamt.copyToZonedDecimal().isAPartOf(filler3,68);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,338);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(116).isAPartOf(dataArea, 343);
	public FixedLengthStringData adjustiuErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bidofferErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData feepcsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] feepcErr = FLSArrayPartOfStructure(5, 4, feepcsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(20).isAPartOf(feepcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData feepc02Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData feepc03Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData feepc04Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData feepc05Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData feepc06Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ovrsumaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData pufeemaxsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] pufeemaxErr = FLSArrayPartOfStructure(5, 4, pufeemaxsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(20).isAPartOf(pufeemaxsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pufeemax02Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData pufeemax03Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData pufeemax04Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData pufeemax05Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData pufeemax06Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData pufeeminsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] pufeeminErr = FLSArrayPartOfStructure(5, 4, pufeeminsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(20).isAPartOf(pufeeminsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pufeemin02Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData pufeemin03Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData pufeemin04Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData pufeemin05Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData pufeemin06Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData puffamtsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData[] puffamtErr = FLSArrayPartOfStructure(5, 4, puffamtsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(20).isAPartOf(puffamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData puffamt02Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData puffamt03Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData puffamt04Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData puffamt05Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData puffamt06Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(348).isAPartOf(dataArea, 459);
	public FixedLengthStringData[] adjustiuOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bidofferOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData feepcsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] feepcOut = FLSArrayPartOfStructure(5, 12, feepcsOut, 0);
	public FixedLengthStringData[][] feepcO = FLSDArrayPartOfArrayStructure(12, 1, feepcOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(60).isAPartOf(feepcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] feepc02Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] feepc03Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] feepc04Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] feepc05Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] feepc06Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ovrsumaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData pufeemaxsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] pufeemaxOut = FLSArrayPartOfStructure(5, 12, pufeemaxsOut, 0);
	public FixedLengthStringData[][] pufeemaxO = FLSDArrayPartOfArrayStructure(12, 1, pufeemaxOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(60).isAPartOf(pufeemaxsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pufeemax02Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] pufeemax03Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] pufeemax04Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] pufeemax05Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData[] pufeemax06Out = FLSArrayPartOfStructure(12, 1, filler9, 48);
	public FixedLengthStringData pufeeminsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] pufeeminOut = FLSArrayPartOfStructure(5, 12, pufeeminsOut, 0);
	public FixedLengthStringData[][] pufeeminO = FLSDArrayPartOfArrayStructure(12, 1, pufeeminOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(60).isAPartOf(pufeeminsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pufeemin02Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] pufeemin03Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] pufeemin04Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] pufeemin05Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] pufeemin06Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData puffamtsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 276);
	public FixedLengthStringData[] puffamtOut = FLSArrayPartOfStructure(5, 12, puffamtsOut, 0);
	public FixedLengthStringData[][] puffamtO = FLSDArrayPartOfArrayStructure(12, 1, puffamtOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(60).isAPartOf(puffamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] puffamt02Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] puffamt03Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] puffamt04Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] puffamt05Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] puffamt06Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6651screenWritten = new LongData(0);
	public LongData S6651protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6651ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(puffamt02Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(feepc02Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pufeemin02Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pufeemax02Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(puffamt03Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(feepc03Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pufeemin03Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pufeemax03Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(puffamt04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(feepc04Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pufeemin04Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pufeemax04Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(puffamt05Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(feepc05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pufeemin05Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pufeemax05Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(puffamt06Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(feepc06Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pufeemin06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pufeemax06Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjustiuOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bidofferOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ovrsumaOut,new String[] {"60",null, "-60",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, puffamt02, feepc02, pufeemin02, pufeemax02, puffamt03, feepc03, pufeemin03, pufeemax03, puffamt04, feepc04, pufeemin04, pufeemax04, puffamt05, feepc05, pufeemin05, pufeemax05, puffamt06, feepc06, pufeemin06, pufeemax06, adjustiu, bidoffer, ovrsuma};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, puffamt02Out, feepc02Out, pufeemin02Out, pufeemax02Out, puffamt03Out, feepc03Out, pufeemin03Out, pufeemax03Out, puffamt04Out, feepc04Out, pufeemin04Out, pufeemax04Out, puffamt05Out, feepc05Out, pufeemin05Out, pufeemax05Out, puffamt06Out, feepc06Out, pufeemin06Out, pufeemax06Out, adjustiuOut, bidofferOut, ovrsumaOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, puffamt02Err, feepc02Err, pufeemin02Err, pufeemax02Err, puffamt03Err, feepc03Err, pufeemin03Err, pufeemax03Err, puffamt04Err, feepc04Err, pufeemin04Err, pufeemax04Err, puffamt05Err, feepc05Err, pufeemin05Err, pufeemax05Err, puffamt06Err, feepc06Err, pufeemin06Err, pufeemax06Err, adjustiuErr, bidofferErr, ovrsumaErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6651screen.class;
		protectRecord = S6651protect.class;
	}

}
