package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6625
 * @version 1.0 generated on 30/08/09 06:53
 * @author Quipoz
 */
public class S6625ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData bonalloc = DD.bonalloc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData comtmeth = DD.comtmeth.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData comtmthj = DD.comtmthj.copy().isAPartOf(dataFields,6);
	public ZonedDecimalData comtperc = DD.comtperc.copyToZonedDecimal().isAPartOf(dataFields,10);
	public ZonedDecimalData dthpercn = DD.dthpercn.copyToZonedDecimal().isAPartOf(dataFields,15);
	public ZonedDecimalData dthperco = DD.dthperco.copyToZonedDecimal().isAPartOf(dataFields,20);
	public ZonedDecimalData evstperd = DD.evstperd.copyToZonedDecimal().isAPartOf(dataFields,25);
	public FixedLengthStringData freqann = DD.freqann.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData frequency = DD.frequency.copy().isAPartOf(dataFields,30);
	public ZonedDecimalData guarperd = DD.guarperd.copyToZonedDecimal().isAPartOf(dataFields,32);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,35);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,43);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,51);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,59);
	public ZonedDecimalData lvstperd = DD.lvstperd.copyToZonedDecimal().isAPartOf(dataFields,89);
	public FixedLengthStringData ncovvest = DD.ncovvest.copy().isAPartOf(dataFields,92);
	public FixedLengthStringData revBonusMeth = DD.revbmeth.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,100);
	public FixedLengthStringData vcalcmth = DD.vcalcmth.copy().isAPartOf(dataFields,105);
	public FixedLengthStringData vclcmthj = DD.vclcmthj.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData annuty = DD.gfflg.copy().isAPartOf(dataFields,113);//ILIFE-3595
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData bonallocErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData comtmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData comtmthjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData comtpercErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData dthpercnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData dthpercoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData evstperdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData freqannErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData frequencyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData guarperdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lvstperdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ncovvestErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData revbmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData vcalcmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData vclcmthjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData annutyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);//ILIFE-3595
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] bonallocOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] comtmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] comtmthjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] comtpercOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] dthpercnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] dthpercoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] evstperdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] freqannOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] frequencyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] guarperdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lvstperdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ncovvestOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] revbmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] vcalcmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] vclcmthjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] annutyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);//ILIFE-3595
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6625screenWritten = new LongData(0);
	public LongData S6625protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6625ScreenVars() {
		super();
		initialiseScreenVars();
	}

	public void initialiseScreenVars() {
		fieldIndMap.put(ncovvestOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(guarperdOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(evstperdOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lvstperdOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(revbmethOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bonallocOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comtmethOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dthpercnOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dthpercoOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comtpercOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqannOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(frequencyOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comtmthjOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vcalcmthOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(vclcmthjOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(annutyOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});//ILIFE-3595
//		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, ncovvest, guarperd, evstperd, lvstperd, revBonusMeth, bonalloc, comtmeth, dthpercn, dthperco, comtperc, freqann, frequency, comtmthj, vcalcmth, vclcmthj,annuty};
//		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, ncovvestOut, guarperdOut, evstperdOut, lvstperdOut, revbmethOut, bonallocOut, comtmethOut, dthpercnOut, dthpercoOut, comtpercOut, freqannOut, frequencyOut, comtmthjOut, vcalcmthOut, vclcmthjOut,annutyOut};
//		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, ncovvestErr, guarperdErr, evstperdErr, lvstperdErr, revbmethErr, bonallocErr, comtmethErr, dthpercnErr, dthpercoErr, comtpercErr, freqannErr, frequencyErr, comtmthjErr, vcalcmthErr, vclcmthjErr,annutyErr};
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6625screen.class;
		protectRecord = S6625protect.class;
	}
	
	public BaseData[] getscreenFields()
	{
		
		return new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, ncovvest, guarperd, evstperd, lvstperd, revBonusMeth, bonalloc, comtmeth, dthpercn, dthperco, comtperc, freqann, frequency, comtmthj, vcalcmth, vclcmthj,annuty};
		
	}
	
	public BaseData[][] getscreenOutFields()
	{
		
		return new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, ncovvestOut, guarperdOut, evstperdOut, lvstperdOut, revbmethOut, bonallocOut, comtmethOut, dthpercnOut, dthpercoOut, comtpercOut, freqannOut, frequencyOut, comtmthjOut, vcalcmthOut, vclcmthjOut,annutyOut};
		
	}
	
	public BaseData[] getscreenErrFields()
	{
		
		return new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, ncovvestErr, guarperdErr, evstperdErr, lvstperdErr, revbmethErr, bonallocErr, comtmethErr, dthpercnErr, dthpercoErr, comtpercErr, freqannErr, frequencyErr, comtmthjErr, vcalcmthErr, vclcmthjErr,annutyErr};
		
	}
	
	public int getDataAreaSize()
	{
		return 466;
	}
	
	public int getDataFieldsSize()
	{
		return 114;
	}
	public int getErrorIndicatorSize()
	{
		return 88;
	}
	public int getOutputFieldSize()
	{
		return 264;
	}

}
