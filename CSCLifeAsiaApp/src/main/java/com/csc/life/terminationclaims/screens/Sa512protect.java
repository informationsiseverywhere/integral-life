package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for PROTECT
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sa512protect extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa512ScreenVars sv = (Sa512ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa512protectWritten, null, av, null, ind2, ind3);
		
		
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sa512ScreenVars screenVars = (Sa512ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.notifinum.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.lidtype.setClassString("");
		screenVars.lsecuityno.setClassString("");
		screenVars.lcltsex.setClassString("");
		screenVars.clntsel.setClassString("");
		screenVars.cltname.setClassString("");
		screenVars.cltreln.setClassString("");
		screenVars.cltreldsc.setClassString("");
		screenVars.cidtype.setClassString("");
		screenVars.csecuityno.setClassString("");
		screenVars.ccltsex.setClassString("");
		screenVars.cltpcode.setClassString("");
		screenVars.rinternet.setClassString("");
		screenVars.cltphoneidd.setClassString("");
		screenVars.notifiDate.setClassString("");
		screenVars.incurdt.setClassString("");
		screenVars.location.setClassString("");
		screenVars.inctype.setClassString("");
		screenVars.cltdodx.setClassString("");
		screenVars.causedeath.setClassString("");
		screenVars.rgpytype.setClassString("");
		screenVars.clamamt.setClassString("");
		screenVars.zdoctor.setClassString("");
		screenVars.zmedprv.setClassString("");
		screenVars.hospitalLevel.setClassString("");
		screenVars.diagcde.setClassString("");
		screenVars.admiDate.setClassString("");
		screenVars.dischargeDate.setClassString("");
		
		screenVars.cltaddr01.setClassString("");
		screenVars.cltaddr02.setClassString("");
		screenVars.cltaddr03.setClassString("");
		screenVars.cltaddr04.setClassString("");
		screenVars.cltaddr05.setClassString("");
		//ScreenRecord.setClassStringFormatting(pv);	
	}

/**
 * Clear all the variables in Sa508protect
 */
	public static void clear(VarModel pv) {
		Sa512ScreenVars screenVars = (Sa512ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.notifinum.clear();
		screenVars.lidtype.clear();
		screenVars.lsecuityno.clear();
		screenVars.lcltsex.clear();
		screenVars.clntsel.clear();
		screenVars.cltname.clear();
		screenVars.cltreln.clear();
		screenVars.cltreldsc.clear();
		screenVars.cidtype.clear();
		screenVars.csecuityno.clear();
		screenVars.ccltsex.clear();
		screenVars.cltpcode.clear();
		screenVars.cltphoneidd.clear();
		screenVars.notifiDate.clear();
		screenVars.incurdt.clear();
		screenVars.location.clear();
		screenVars.inctype.clear();
		screenVars.cltdodx.clear();
		screenVars.causedeath.clear();
		screenVars.rgpytype.clear();
		screenVars.clamamt.clear();
		screenVars.zdoctor.clear();
		screenVars.zmedprv.clear();
		screenVars.hospitalLevel.clear();
		screenVars.diagcde.clear();
		screenVars.admiDate.clear();
		screenVars.dischargeDate.clear();
		//ScreenRecord.clear(pv);
	}
}
