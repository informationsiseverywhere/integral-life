package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5097screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5097ScreenVars sv = (S5097ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5097screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5097ScreenVars screenVars = (S5097ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.hnewinsval.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.register.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.bilfrqdesc.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.planSuffix.setClassString("");
		screenVars.numpols.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.sumins.setClassString("");
		screenVars.newinsval.setClassString("");
		screenVars.statcode.setClassString("");
		screenVars.statdesc.setClassString("");
		screenVars.pstatcode.setClassString("");
		screenVars.pstatdesc.setClassString("");
		screenVars.pumeth.setClassString("");
		screenVars.pumethdesc.setClassString("");
	}

/**
 * Clear all the variables in S5097screen
 */
	public static void clear(VarModel pv) {
		S5097ScreenVars screenVars = (S5097ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.hnewinsval.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.register.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.jlife.clear();
		screenVars.jlifename.clear();
		screenVars.crtable.clear();
		screenVars.crtabdesc.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.billfreq.clear();
		screenVars.bilfrqdesc.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.planSuffix.clear();
		screenVars.numpols.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.sumins.clear();
		screenVars.newinsval.clear();
		screenVars.statcode.clear();
		screenVars.statdesc.clear();
		screenVars.pstatcode.clear();
		screenVars.pstatdesc.clear();
		screenVars.pumeth.clear();
		screenVars.pumethdesc.clear();
	}
}
