package com.csc.life.terminationclaims.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5370.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5370Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private FixedLengthStringData currcd = new FixedLengthStringData(3);
	private FixedLengthStringData dateReportVariable = new FixedLengthStringData(10);
	private ZonedDecimalData jobnum = new ZonedDecimalData(8, 0);
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData payreason = new FixedLengthStringData(2);
	private ZonedDecimalData prcnt = new ZonedDecimalData(5, 2);
	private ZonedDecimalData pymnt = new ZonedDecimalData(13, 2);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private ZonedDecimalData rgpynum = new ZonedDecimalData(5, 0);
	private FixedLengthStringData rgpystat = new FixedLengthStringData(2);
	private FixedLengthStringData rgpytype = new FixedLengthStringData(2);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5370Report() {
		super();
	}


	/**
	 * Print the XML for R5370d01
	 */
	public void printR5370d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 9, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 11, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 13, 2));
		rgpynum.setFieldName("rgpynum");
		rgpynum.setInternal(subString(recordData, 15, 5));
		pymnt.setFieldName("pymnt");
		pymnt.setInternal(subString(recordData, 20, 13));
		currcd.setFieldName("currcd");
		currcd.setInternal(subString(recordData, 33, 3));
		prcnt.setFieldName("prcnt");
		prcnt.setInternal(subString(recordData, 36, 5));
		rgpytype.setFieldName("rgpytype");
		rgpytype.setInternal(subString(recordData, 41, 2));
		payreason.setFieldName("payreason");
		payreason.setInternal(subString(recordData, 43, 2));
		rgpystat.setFieldName("rgpystat");
		rgpystat.setInternal(subString(recordData, 45, 2));
		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.setInternal(subString(recordData, 47, 10));
		printLayout("R5370d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				life,
				coverage,
				rider,
				rgpynum,
				pymnt,
				currcd,
				prcnt,
				rgpytype,
				payreason,
				rgpystat,
				dateReportVariable
			}
		);

	}

	/**
	 * Print the XML for R5370d02
	 */
	public void printR5370d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(16).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 9, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 11, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 13, 2));
		rgpynum.setFieldName("rgpynum");
		rgpynum.setInternal(subString(recordData, 15, 5));
		pymnt.setFieldName("pymnt");
		pymnt.setInternal(subString(recordData, 20, 13));
		currcd.setFieldName("currcd");
		currcd.setInternal(subString(recordData, 33, 3));
		prcnt.setFieldName("prcnt");
		prcnt.setInternal(subString(recordData, 36, 5));
		rgpytype.setFieldName("rgpytype");
		rgpytype.setInternal(subString(recordData, 41, 2));
		payreason.setFieldName("payreason");
		payreason.setInternal(subString(recordData, 43, 2));
		rgpystat.setFieldName("rgpystat");
		rgpystat.setInternal(subString(recordData, 45, 2));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 47, 30));
		printLayout("R5370d02",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				life,
				coverage,
				rider,
				rgpynum,
				pymnt,
				currcd,
				prcnt,
				rgpytype,
				payreason,
				rgpystat,
				longdesc
			}
			, new Object[] {			// indicators
				new Object[]{"ind15", indicArea.charAt(15)},
				new Object[]{"ind16", indicArea.charAt(16)}
			}
		);

	}

	/**
	 * Print the XML for R5370d03
	 */
	public void printR5370d03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(4);

		printLayout("R5370d03",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R5370d04
	 */
	public void printR5370d04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(4);

		printLayout("R5370d04",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R5370d05
	 */
	public void printR5370d05(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(4);

		printLayout("R5370d05",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R5370d06
	 */
	public void printR5370d06(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(4);

		printLayout("R5370d06",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R5370d07
	 */
	public void printR5370d07(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(4);

		printLayout("R5370d07",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for R5370h01
	 */
	public void printR5370h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(14).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		jobnum.setFieldName("jobnum");
		jobnum.setInternal(subString(recordData, 11, 8));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		company.setFieldName("company");
		company.setInternal(subString(recordData, 19, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 20, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 50, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 60, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 62, 30));
		time.setFieldName("time");
		time.set(getTime());
		printLayout("R5370h01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				jobnum,
				pagnbr,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time
			}
			, new Object[] {			// indicators
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind14", indicArea.charAt(14)},
				new Object[]{"ind12", indicArea.charAt(12)},
				new Object[]{"ind13", indicArea.charAt(13)}
			}
		);
		
		currentPrintLine.set(15);
	}

	/**
	 * Print the XML for R5370h02
	 */
	public void printR5370h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(14).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		jobnum.setFieldName("jobnum");
		jobnum.setInternal(subString(recordData, 11, 8));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		company.setFieldName("company");
		company.setInternal(subString(recordData, 19, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 20, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 50, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 60, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 62, 30));
		time.setFieldName("time");
		time.set(getTime());
		printLayout("R5370h02",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				jobnum,
				pagnbr,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time
			}
			, new Object[] {			// indicators
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind14", indicArea.charAt(14)},
				new Object[]{"ind12", indicArea.charAt(12)},
				new Object[]{"ind13", indicArea.charAt(13)}
			}
		);

		currentPrintLine.set(15);
	}


}
