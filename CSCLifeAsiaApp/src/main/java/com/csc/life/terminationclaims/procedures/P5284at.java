
/*
 * File: P5284at.java
 * Date: 30 August 2009 0:23:33
 * Author: Quipoz Limited
 *
 * Class transformed from P5284AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.financials.procedures.Payreq;
import com.csc.fsu.financials.recordstructures.Payreqrec;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.procedures.LincCSService;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CpbnfypfDAO;
import com.csc.life.newbusiness.dataaccess.model.Cpbnfypf;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;
import com.csc.life.productdefinition.dataaccess.MliaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
//added for death claim flexibility
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th602rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.AcblclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.HdcdTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmhpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.recordstructures.Dthclmflxrec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*  CLAIMS APPROVAL   -   AT MODULE
*  -------------------------------
*
*  This AT module is called by the Death Claims approval
*  program P5284  and the Contract number is passed in the AT
*  parameters area as the "primary key".
*
*  AT PROCESSING
*
*  Check the return status  codes  for  each  of  the
*  following records being updated.
*
*  CLAIM HEADER RECORD
*
*  Read the  Claim Header record (CLMHCLM) for the relevant
*  contract.  The  interest  accrued amount and/or the
*  office   charges,   are   used   in   updating  the
*  sub-accounts.
*
*  Read T5494,  keyed  on transaction and change the status
*  on  the  contract  header to the appropriate status
*  for  this  transaction,  add  1  to the transaction
*  number and update the contract header.
*
*  CONTRACT ACCOUNTING FOR DEATH CLAIMS APPROVAL
*
*  Read the transaction accounting rules from table T5645
*  (keyed on  program  number),  this  contains the rules
*  necessary for posting the monies to the correct accounts.
*
*             - Death Claims Outstanding (Contract Level)
*             - Death Claims Outstanding (Component Level)
*             - Death Claim Interest account
*             - Death Claim Office Charges account
*             - Death Claim Payment Suspense account
*
*  Read  the  description  of  this table entry as well. This
*  is used as narrative description on all the postings
*  generated.
*
*  Only  post  when  the amount  is greater  than zero.  Call
*  LIFACMV  ("cash"   posting  subroutine)  to  post  to  the
*  correct  account. The  posting  required is defined in the
*  appropriate line no.  on  the  T5645  table entry.  Set up
*  and pass the linkage area as follows:
*
*  Post to the sub-account (01) and (05) see below:
*  Use the  amount  on  the  sub-account with the sign from
*  T3695 (this should reverse the amount).
*
*  If the interest amount is > 0:-
*  then post to the sub-account (02) see below:
*
*  If the office charge amount is > 0:-
*  then post to the sub-account (03) see below:
*
*  Calculate the Payment suspense amount as follows:
*  Accumulate  the value of sub-account 01,02 and 05, less the
*  value in sub-account 03.
*
*  Then post to the sub-account (04) see below:
*
*             Function               - PSTW
*             Batch key              - from AT linkage
*             Document number        - contract number
*             Transaction number     - transaction no.
*             Journal sequence no.   - 0
*             Sub-account code       - from applicable T5645
*                                      entry
*             Sub-account type       - ditto
*             Sub-account GL map     - ditto
*             Sub-account GL sign    - ditto
*             S/acct control total   - ditto
*             Cmpy code (sub ledger) - batch company
*             Cmpy code (GL)         - batch company
*             Subsidiary ledger      - contract number
*             Original currency code - currency payable in
*             Original currency amt  - see '*' below
*             Accounting curr code   - blank (handled by
*                                      subroutine)
*             Accounting curr amount - zero (handled by
*                                      subroutine)
*             Exchange rate          - zero (handled by
*                                      subroutine)
*             Trans reference        - contract transaction
*                                      number
*             Trans description      - from transaction code
*                                      description
*             Posting month and year - defaulted
*             Effective date         - Today (Claim
*                                      Approval-date)
*             Reconciliation amount  - zero
*             Reconciliation date    - Max date
*             Transaction Id         - from AT linkage
*             Substitution code 1    - contract type
*
*  Sub-account  01  is  the O/S claims amount with the sign
*  altered (T3695). This is for Contract Level and is retrieved
*  from the ACBL (Account Balance) for the selected Sub Account
*  Code and Type.
*
*  Sub-account  02 is the Interest amount obtained from the
*  CLMH record.
*
*  Sub-account  03  is  the Office charge obtained from the
*  CLMH record.
*
*  Sub-account  04  is  the  Payment  Suspense   amount  as
*  calculated.
*
*  Sub-account  05  is  the O/S claims amount with the sign
*  altered (T3695). This is for Component Level and is retrieved
*  from the ACBL (Account Balance) for the selected Sub Account
*  Code and Type. There may be multiple records for this so a
*  BEGN read is used via ACBLCLM logical view.
*
*  GENERAL HOUSE-KEEPING
*
*  1) Write a PTRN transaction record:
*  - contract key from contract header
*  - transaction number from contract header
*  - transaction effective date equals todays
*  - batch key information from AT linkage.
*
*  2) Update the  batch  header  by  calling  BATCUP with a
*     function of WRITS and the following parameters:
*
*  - transaction count equals 1
*  - all other amounts zero
*  - batch key from AT linkage
*
*  3) Release contract SFTLOCK.
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are covrsts and covrsta.
* Covrsts allow only validflag 1 records, and Covrsta
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through it's life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as it's item name, the transaction code and
* the contract status of a contract, eg. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, eg AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to Covrsta records, and
* current categories refer to Covrsts records. Agcmsts
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the Agcmsts, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out upto and including the TRANNOR number.
*
*
*****************************************************
* </pre>
*/
public class P5284at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5284AT");
	protected ZonedDecimalData wsaaPost1 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	protected ZonedDecimalData wsaaPost2 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	protected ZonedDecimalData wsaaPost3 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	protected ZonedDecimalData wsaaPost4 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	//ILIFE-1137
	//Death Claim Flexibility
	private ZonedDecimalData wsaaPost6 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private ZonedDecimalData wsaaPost7 = new ZonedDecimalData(2, 0).init(7).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	protected FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	protected PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	protected PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	protected PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	protected FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	protected FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaTransArea, 16);
	protected FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 19);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaLastOrigCcy = new FixedLengthStringData(3).init(SPACES);
	protected FixedLengthStringData wsaaLedgerCcy = new FixedLengthStringData(3);
	protected PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();
	protected PackedDecimalData wsaaTotAcbl = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTotClaim = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTotHdcd = new PackedDecimalData(17, 2);

	protected FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	protected FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	protected FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	protected FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	protected FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	protected FixedLengthStringData wsaaRldgPlnsfx = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* TABLES */
	private static final String t5679 = "T5679";
	protected static final String t5645 = "T5645";
	protected static final String t3695 = "T3695";
	private static final String t3629 = "T3629";
	protected static final String th602 = "TH602";
	protected AcblTableDAM acblIO = new AcblTableDAM();
	protected AcblclmTableDAM acblclmIO = new AcblclmTableDAM();
	protected ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	protected ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM();
	protected ClmdclmTableDAM clmdIO = new ClmdclmTableDAM();
	protected ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
	protected ClntTableDAM clntIO = new ClntTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	protected HdcdTableDAM hdcdIO = new HdcdTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	protected MliaTableDAM mliaIO = new MliaTableDAM();
	protected PtrnTableDAM ptrnIO = new PtrnTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	protected Varcom varcom = new Varcom();
	protected Lifacmvrec lifacmvrec = new Lifacmvrec();
	protected Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	
	protected T5679rec t5679rec = new T5679rec();
	protected T5645rec t5645rec = new T5645rec();
	protected T3695rec t3695rec = new T3695rec();
	protected T3629rec t3629rec = new T3629rec();
	protected Th602rec th602rec = new Th602rec();
	protected Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	protected Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Atmodrec atmodrec = new Atmodrec();
	protected FormatsInner formatsInner = new FormatsInner();
	private SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();
	//ILIFE-1137
	//added for death claim flexibility	
	protected T5688rec t5688rec = new T5688rec();
	protected Dthclmflxrec dthclmflxRec =  getDthclmflxrec();
	protected static final String t5688 = "T5688";
	private static final String e308 = "E308";
	//end added for death claim flexibility
    private CpbnfypfDAO cpbnfypfDAO = getApplicationContext().getBean("cpbnfypfDAO", CpbnfypfDAO.class);
    protected PackedDecimalData wsaaTotClaimPayees = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaSelectClaimPayees = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaOtherClaimPayees = new PackedDecimalData(17, 2);
	protected Payreqrec payreqrec = new Payreqrec();
	private Itempf itempf = null;
    protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    protected boolean CMDTH006Permission  = false;
    private Conlinkrec conlinkrec = new Conlinkrec();
    protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO" , CattpfDAO.class);
    private ClmhpfDAO clmhpfDAO = getApplicationContext().getBean("clmhpfDAO" , ClmhpfDAO.class);
	private Cattpf cattpf;
	boolean CMDTH010Permission  = false;
	private String t6693 = "T6693";
	private T6693rec t6693rec = new T6693rec();
	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);
	private ZonedDecimalData index1 = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaClmstat = new FixedLengthStringData(2);
	private boolean lincFlag = false;
	private static final String lincFeature = "NBPRP116";
	private static final String tjl47 = "TJL47";
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	private static final String feaConfigPreRegistartion= "CMDTH010";
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		m600Nextr,
		m600Exit,
		complvl2405,
		exit2409,
		exit6190,
		exit7190
	}
	
	public Dthclmflxrec getDthclmflxrec() {
		return new Dthclmflxrec();
	}

	public P5284at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		housekeepingTerminate3000();
		m500UpdateMlia();
		a000Statistics();
		mainCustomerSpecific();
		if(lincFlag)
			performLincProcessing();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/

protected void mainCustomerSpecific(){
	
}
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(sysrSyserrRecInner.sysrStatuz, varcom.bomb)) {
			return ;
		}
		sysrSyserrRecInner.sysrSyserrStatuz.set(sysrSyserrRecInner.sysrStatuz);
		if (isNE(sysrSyserrRecInner.sysrSyserrType, "2")) {
			sysrSyserrRecInner.sysrSyserrType.set("1");
		}
		callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
        CMDTH006Permission  = FeaConfg.isFeatureExist("2", "CMDTH006", appVars, "IT");
        CMDTH010Permission  = FeaConfg.isFeatureExist("2", feaConfigPreRegistartion, appVars, "IT");
        lincFlag = FeaConfg.isFeatureExist(atmodrec.company.toString(), lincFeature, appVars, "IT");
		/*INITIALISE*/
		readClaimHeader1050();
		readStatusCodes1100();
		getTranAcctRules1200();
		readContractHeader1300();
		callDatcons1400();
		/*EXIT*/
	}

	/**
	* <pre>
	* Initial read of the Contract Header.                        *
	* </pre>
	*/
protected void readClaimHeader1050()
	{
		readClaim1051();
	}

protected void readClaim1051()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		clmhclmIO.setDataArea(SPACES);
		clmhclmIO.setChdrcoy(atmodrec.company);
		clmhclmIO.setChdrnum(wsaaPrimaryChdrnum);
		clmhclmIO.setFunction(varcom.readr);
		clmhclmIO.setFormat(formatsInner.clmhclmrec);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(clmhclmIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Read the status codes for the contract type.                *
	* </pre>
	*/
protected void readStatusCodes1100()
	{
		readStatusCodes1101();
	}

protected void readStatusCodes1101()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void getTranAcctRules1200()
	{
		read1201();
	}

protected void read1201()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(clmhclmIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("P5284");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(clmhclmIO.getChdrcoy());
		descIO.setDesctabl(t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("P5284");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(descIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Initial read of the Contract Header.                        *
	* </pre>
	*/
protected void readContractHeader1300()
	{
		/*READ-CONTRACT-HEADER*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Call DATCON1 subroutine.                                    *
	* </pre>
	*/
protected void callDatcons1400()
	{
		/*DATCON1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}

	/**
	* <pre>
	* Do everything.                                              *
	* </pre>
	*/

protected void process2000()
	{
		/*PROCESS*/
		//start ILIFE-1137
		postUnpaidPrem4000();
		postExcessPrem4100();
		//end ILIFE-1137	
		//set claim amount
        if(CMDTH006Permission){
            postClaimPayeesAmount6000();
        }
		processContractHeader2100();
		processCovers2200(); //IJS-351
		postDthClaimsOutstan2400();
		postClaimInterestAcct2700();
		postClaimOfficeCharges3000();
		if(CMDTH010Permission){
		updateClaimStatus2130();
		}

		
		/*    it is no longer necessary to post adjustments as they        */
		/*    have been posted as part of Death Claim Amount               */
		/*PERFORM 6100-POST-OTHER-ADJUSTMENT.                     <005>*/
		/* Should use LIFRTRN to post the cash suspense, not LIFACMV.      */
		/*PERFORM 3300-POST-DEATH-CLAIM-SUSP.                          */
		//postDeathClaimSusp6000();
		postDeathClaimSusp3300();
		/*EXIT*/
	}

protected void updateClaimStatus2130()
{
	readT6693Table2700();
}

protected void readT6693Table2700(){

	cattpf = cattpfDAO.selectRecords(chdrlifIO.getChdrcoy().toString(),chdrlifIO.getChdrnum().toString());
	if(null == cattpf){
		sysrSyserrRecInner.sysrParams.set(chdrlifIO.getChdrcoy().toString()+ chdrlifIO.getChdrnum().toString());
		xxxxFatalError();
	}	
	wsaaT6693Paystat.set(cattpf.getClamstat());
	wsaaT6693Crtable.set("****");

	itempf=new Itempf();
	itempf = itemDAO.findItemByItem(atmodrec.company.toString(),t6693,wsaaT6693Key.toString());
	if (null == itempf) {
		sysrSyserrRecInner.sysrParams.set(atmodrec.company.toString()+wsaaT6693Key.toString());
		xxxxFatalError();
	}
	else{
		t6693rec.t6693Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		index1.set(1);
		while ( !(isGT(index1, 12))) {
			findStatus3310();
		}
	}
	cattpfDAO.updateCattpf(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString());
	cattpf.setTrcode(wsaaBatckey.batcBatctrcde.toString().trim());
	cattpf.setTranno(chdrlifIO.getTranno().toInt());
	cattpf.setClamstat(wsaaClmstat.toString());
	cattpf.setValidflag("1");
	cattpfDAO.insertCattpfRecord(cattpf);	
	clmhpfDAO.updateClaimStatus(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString(),wsaaClmstat.toString());
}	



protected void findStatus3310()
{
	/*FIND-STATUS*/
	if (isEQ(t6693rec.trcode[index1.toInt()], wsaaBatckey.batcBatctrcde)) {
		wsaaClmstat.set(t6693rec.rgpystat[index1.toInt()]);
		index1.set(15);
	}
	index1.add(1);
	/*EXIT*/	
	
}	


	protected void postClaimPayeesAmount6000()
	{
        List<Cpbnfypf> cpbnfypfList = cpbnfypfDAO.searchCpbnfypfRecord(clmhclmIO.getChdrcoy().toString(),clmhclmIO.getChdrnum().toString());
		if (cpbnfypfList!=null && !cpbnfypfList.isEmpty()) {
            wsaaTotClaimPayees.set(ZERO);
            wsaaSelectClaimPayees.set(ZERO);
            wsaaOtherClaimPayees.set(ZERO);
			for (Cpbnfypf cpbnfypf : cpbnfypfList) {
				//select
				if (null == cpbnfypf.getPaymmeth() || isEQ(cpbnfypf.getPaymmeth(),SPACES)){
					wsaaSelectClaimPayees.add(cpbnfypf.getAmount().doubleValue());
				}
				//Direct Credit
				else if (cpbnfypf.getPaymmeth().equals("4")){	//ILIFE-9573
					payreqrec.glcode.set(t5645rec.glmap[6]);
                	payreqrec.sacscode.set(t5645rec.sacscode[6]);
                	payreqrec.sacstype.set(t5645rec.sacstype[6]);
                	payreqrec.sign.set(t5645rec.sign[6]);
                	payreqrec.cnttot.set(t5645rec.cnttot[6]);
					wsaaOtherClaimPayees.add(cpbnfypf.getAmount().doubleValue());
					callPayreq(cpbnfypf);
                }
				//Automatic Cheque
				else if (cpbnfypf.getPaymmeth().equals("1")){	//ILIFE-9573
					payreqrec.glcode.set(t5645rec.glmap[6]);
                	payreqrec.sacscode.set(t5645rec.sacscode[6]);
                	payreqrec.sacstype.set(t5645rec.sacstype[6]);
                	payreqrec.sign.set(t5645rec.sign[6]);
                	payreqrec.cnttot.set(t5645rec.cnttot[6]);
					wsaaOtherClaimPayees.add(cpbnfypf.getAmount().doubleValue());
					callPayreq(cpbnfypf);
				};
				wsaaTotClaimPayees.add(cpbnfypf.getAmount().doubleValue());
			}
		}
	}

	protected void callPayreq(Cpbnfypf cpbnfypf){
		payreqrec.user.set(wsaaUser);
		payreqrec.bankkey.set(cpbnfypf.getBankkey());
		payreqrec.bankacckey.set(cpbnfypf.getBankacckey());
		payreqrec.reqntype.set(cpbnfypf.getPaymmeth());
		payreqrec.paycurr.set(clmhclmIO.currcd);
		/*    MOVE SQL-SACSCURBAL         TO PAYREQ-PYMT.                  */
		payreqrec.pymt.set(cpbnfypf.getAmount());
		payreqrec.frmRldgacct.set(cpbnfypf.getChdrnum());
		payreqrec.trandesc.set(descIO.getLongdesc());
		payreqrec.clntcoy.set(chdrlifIO.getCowncoy());
		payreqrec.clntnum.set(cpbnfypf.getBnyclt());
		readT36293260();
		payreqrec.bankcode.set(t3629rec.bankcode);
		payreqrec.tranref.set(chdrlifIO.getTranno());
		payreqrec.tranno.set(chdrlifIO.getTranno());
		/* BRD-489 STARTS */
		payreqrec.zbatctrcde.set(wsaaBatckey.batcBatctrcde);
		/* BRD-489 ENDS*/
		payreqrec.batccoy.set(cpbnfypf.getChdrcoy());
		payreqrec.effdate.set(wsaaToday);
		payreqrec.function.set("REQN");
		callProgram(Payreq.class, payreqrec.rec);
		if (isNE(payreqrec.statuz,varcom.oK)) {
			xxxxFatalError();
		}
	}

	protected void readT36293260()
	{
		start3260();
	}

	protected void start3260()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(clmhclmIO.getChdrcoy().toString());
		itempf.setItemtabl(t3629);
		itempf.setItemitem(clmhclmIO.getCurrcd().toString());
		itempf = itemDAO.getItempfRecord(itempf);
		if (itempf == null) {
			xxxxFatalError();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		if (isEQ(t3629rec.bankcode,SPACES)) {
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Process the contract header.                                *
	* </pre>
	*/
protected void processContractHeader2100()
	{
		processContractHeader2101();
	}

protected void processContractHeader2101()
	{
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		chdrlifIO.setStatcode(t5679rec.setCnRiskStat);
		chdrlifIO.setPstattran(chdrlifIO.getTranno());
		varcom.vrcmCompTermid.set(wsaaTermid);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransactionTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrlifIO.setTranid(varcom.vrcmCompTranid);
		/* MOVE CLMHCLM-EFFDATE        TO CHDRLIF-CURRTO.               */
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
	}

protected void processCovers2200() {
	List<String> data = new ArrayList<>();
	data.add(chdrlifIO.getChdrnum().toString());
	Map<String,List<Covrpf>> covrMap = covrpfDAO.searchCovrMap(atmodrec.company.toString(),data);
	Iterator<Entry<String, List<Covrpf>>> keyit = covrMap.entrySet().iterator();
	while(keyit.hasNext()) {
		List<Covrpf> covr = keyit.next().getValue();
		Iterator<Covrpf> covrit = covr.iterator();
		while(covrit.hasNext()) {
			Covrpf covrpf = covrit.next();
			updateComponent2300(covrpf);
		}
	}
}

protected void updateComponent2300(Covrpf covrpf) {
	covrpf.setValidflag("2");
	covrpf.setCurrto(clmhclmIO.getEffdate().toInt());
	covrpfDAO.updateCovrValidAndCurrtoFlag(covrpf);
	T5687rec t5687rec = new T5687rec();
	Itempf t5687Item = itemDAO.findItemByDate("IT", atmodrec.company.toString(), "T5687", covrpf.getCrtable(), covrpf.getCrrcd(), "1");
	if(t5687Item!=null) {
		t5687rec.t5687Rec.set(t5687Item.getGenareaString());
	}
	covrpf.setValidflag("1");
	covrpf.setCurrto(varcom.vrcmMaxDate.toInt());
	covrpf.setCurrfrom(clmhclmIO.getEffdate().toInt());
	covrpf.setTranno(chdrlifIO.getTranno().toInt());
	covrpf.setStatcode(t5679rec.setCovRiskStat.toString());
	if (isEQ(covrpf.getRider(), "00")
	|| isEQ(covrpf.getRider(), SPACES)) {
		covrpf.setStatcode(t5679rec.setCovRiskStat.toString());
	}
	else {
		covrpf.setStatcode(t5679rec.setRidRiskStat.toString());
	}
	if (isNE(chdrlifIO.getBillfreq(), "00") && isNE(t5687rec.singlePremInd, "Y")) {
		if (isEQ(covrpf.getRider(), SPACES) || isEQ(covrpf.getRider(), "00") && isNE(t5679rec.setCovPremStat,SPACES)) {
			covrpf.setPstatcode(t5679rec.setCovPremStat.toString());
		} else if (isNE(t5679rec.setRidPremStat,SPACES)){
			covrpf.setPstatcode(t5679rec.setRidPremStat.toString());
		}
	} else {
		if (isEQ(covrpf.getRider(), SPACES) || isEQ(covrpf.getRider(), "00") && isNE(t5679rec.setSngpCovStat,SPACES)) {
			covrpf.setPstatcode(t5679rec.setSngpCovStat.toString());
		} else if (isNE(t5679rec.setSngpRidStat,SPACES)){
			covrpf.setPstatcode(t5679rec.setSngpRidStat.toString());
		}
	}
	covrpf.setTransactionDate(wsaaTransactionDate.toInt());
	covrpf.setTransactionTime(wsaaTransactionTime.toInt());
	covrpf.setUser(wsaaUser.toInt());
	covrpf.setTermid(wsaaTermid.toString());
	List<Covrpf> data = new ArrayList<>();
	data.add(covrpf);
	covrpfDAO.insertCovrpfList(data);
}

protected void m500UpdateMlia()
	{
		/*M500-START*/
		lifelnbIO.setStatuz(varcom.oK);
		lifelnbIO.setChdrcoy(chdrlifIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlifIO.getChdrnum());
		lifelnbIO.setLife(SPACES);
		lifelnbIO.setJlife(SPACES);
		lifelnbIO.setFunction(varcom.begn);
		while ( !(isEQ(lifelnbIO.getStatuz(),varcom.endp))) {


			//performance improvement --  atiwari23
			lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			lifelnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

			m600ReadLifelnb();
		}

		/*M500-EXIT*/
	}

protected void m600ReadLifelnb()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					m600Start();
				case m600Nextr:
					m600Nextr();
				case m600Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void m600Start()
	{
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(lifelnbIO.getParams());
			xxxxFatalError();
		}
		if (isNE(lifelnbIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(), chdrlifIO.getChdrnum())) {
			lifelnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(lifelnbIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.m600Exit);
		}
		if (isEQ(lifelnbIO.getValidflag(), "2")) {
			goTo(GotoLabel.m600Nextr);
		}
		clntIO.setClntpfx("CN");
		clntIO.setClntcoy(wsaaFsuCoy);
		clntIO.setClntnum(lifelnbIO.getLifcnum());
		clntIO.setFunction(varcom.readr);
		clntIO.setFormat(formatsInner.clntrec);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(), varcom.oK)
		&& isNE(clntIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(clntIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(clntIO.getCltdod(), varcom.vrcmMaxDate)) {
			goTo(GotoLabel.m600Nextr);
		}
		mliaIO.setSecurityno(clntIO.getSecuityno());
		mliaIO.setMlentity(lifelnbIO.getChdrnum());
		mliaIO.setFormat(formatsInner.mliarec);
		mliaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(), varcom.oK)
		&& isNE(mliaIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(mliaIO.getParams());
			xxxxFatalError();
		}
		if (isNE(mliaIO.getStatuz(), varcom.oK)) {
			goTo(GotoLabel.m600Nextr);
		}
		if (isEQ(mliaIO.getActn(), "3")) {
			goTo(GotoLabel.m600Nextr);
		}
		m700ReadTh602();
		if (isEQ(mliaIO.getMind(), "Y")) {
			mliaIO.setActn("2");
		}
		mliaIO.setRskflg(th602rec.rskflg);
		mliaIO.setMind("N");
		mliaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, mliaIO);
		if (isNE(mliaIO.getStatuz(), varcom.oK)
		&& isNE(mliaIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(mliaIO.getParams());
			xxxxFatalError();
		}
	}

protected void m600Nextr()
	{
		lifelnbIO.setFunction(varcom.nextr);
	}

protected void m700ReadTh602()
	{
		m700Start();
	}

protected void m700Start()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lifelnbIO.getChdrcoy());
		itemIO.setItemtabl(th602);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(sysrSyserrRecInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		th602rec.th602Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	* Post the outstanding death claims                           *
	* </pre>
	*/
protected void postDthClaimsOutstan2400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					go2401();
				case complvl2405:
					complvl2405();
					acbl2406();
				case exit2409:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go2401()
	{
		wsaaTotAcbl.set(ZERO);
		/* Read the accounting balance.*/
		acblIO.setRldgcoy(atmodrec.company);
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setRldgacct(clmhclmIO.getChdrnum());
		/*    MOVE CLMHCLM-CURRCD         TO ACBL-ORIGCURR.                */
		acblIO.setOrigcurr(wsaaCntcurr);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(acblIO.getParams());
			xxxxFatalError();
		}
		 if (isEQ(acblIO.getStatuz(), varcom.mrnf) ) 
		 {
			 	 getDeathRegact();
				/* Read the accounting balance.*/
				acblIO.setRldgcoy(atmodrec.company);
				acblIO.setSacscode(t5645rec.sacscode01);
				acblIO.setRldgacct(wsaaRldgacct);
				/*    MOVE CLMHCLM-CURRCD         TO ACBL-ORIGCURR.                */
				acblIO.setOrigcurr(wsaaCntcurr);
				acblIO.setSacstyp(t5645rec.sacstype01);
				acblIO.setFunction(varcom.readr);
				acblIO.setFormat(formatsInner.acblrec);
				SmartFileCode.execute(appVars, acblIO);
				if (isNE(acblIO.getStatuz(), varcom.oK)
				&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
					sysrSyserrRecInner.sysrParams.set(acblIO.getParams());
					xxxxFatalError();
				}
		 }
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)
		|| isEQ(acblIO.getSacscurbal(), 0)) {
			acblIO.setSacscurbal(0);
			goTo(GotoLabel.complvl2405);
		}
		/*     GO TO 2409-EXIT.                                          */
		/* Sign is attained from T3695.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(clmhclmIO.getChdrcoy());
		itemIO.setItemtabl(t3695);
		itemIO.setItemitem(t5645rec.sacstype01);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.effdate.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
		/*MOVE CLMHCLM-TRANNO                  TO LIFA-TRANNO.         */
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaPost1.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaPost1.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaPost1.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaPost1.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaPost1.toInt()]);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
		/*    MOVE CLMHCLM-CURRCD                  TO LIFA-ORIGCURR.       */
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(acblIO.getSacscurbal());
		if (isEQ(t3695rec.sign, "-")) {
			compute(lifacmvrec.origamt, 2).set(mult(acblIO.getSacscurbal(), -1));
		}
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			goTo(GotoLabel.exit2409);
		}
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(atmodrec.company);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		/*MOVE CLMHCLM-TRANNO         TO LIFA-TRANREF,                 */
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getShortdesc());
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}

		Boolean isDeathClaimPayees = CMDTH006Permission && isNE(wsaaTotClaimPayees, ZERO);
		if(!isDeathClaimPayees){
			wsaaTotAcbl.add(acblIO.getSacscurbal());
		}
	}

protected void complvl2405()
	{
		/* Read the accounting balance.                                    */
		acblclmIO.setRldgcoy(atmodrec.company);
		acblclmIO.setSacscode(t5645rec.sacscode05);
		acblclmIO.setRldgacct(clmhclmIO.getChdrnum());
		/*    MOVE CLMHCLM-CURRCD         TO ACBLCLM-ORIGCURR.     <LA2108>*/
		acblIO.setOrigcurr(wsaaCntcurr);
		acblclmIO.setSacstyp(t5645rec.sacstype05);
		acblclmIO.setFormat(formatsInner.acblclmrec);
		acblclmIO.setFunction(varcom.begn);
	}

protected void acbl2406()
	{

	//performance improvement --  atiwari23
	acblclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	acblclmIO.setFitKeysSearch("RLDGCOY","SACSCODE","SACSTYP");

		SmartFileCode.execute(appVars, acblclmIO);
		if (isNE(acblclmIO.getStatuz(), varcom.oK)
		&& isNE(acblclmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(acblclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(acblclmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(acblclmIO.getStatuz(), varcom.endp)) {
			return ;
		}
		wsaaRldgacct.set(acblclmIO.getRldgacct());
		if (isEQ(acblclmIO.getRldgcoy(), atmodrec.company)
		&& isEQ(wsaaRldgChdrnum, clmhclmIO.getChdrnum())
		&& isEQ(acblclmIO.getSacscode(), t5645rec.sacscode05)
		&& isEQ(acblclmIO.getSacstyp(), t5645rec.sacstype05)) {
			/*NEXT_SENTENCE*/
		}
		else {
			return ;
		}
		if (isEQ(acblclmIO.getSacscurbal(), 0)) {
			acblclmIO.setFunction(varcom.nextr);
			/*    GO TO 2406-ACBL.                                  <LA4554>*/
			acbl2406();
			return ;
		}
		/* Sign is attained from T3695.                                    */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(clmhclmIO.getChdrcoy());
		itemIO.setItemtabl(t3695);
		itemIO.setItemitem(t5645rec.sacstype05);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.effdate.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode05);
		lifacmvrec.sacstyp.set(t5645rec.sacstype05);
		lifacmvrec.glcode.set(t5645rec.glmap05);
		lifacmvrec.glsign.set(t5645rec.sign05);
		lifacmvrec.contot.set(t5645rec.cnttot05);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		/*  MOVE CLMHCLM-CHDRNUM                 TO LIFA-RLDGACCT.       */
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		/*    MOVE CLMHCLM-CURRCD                  TO LIFA-ORIGCURR.       */
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		//start ILIFE-1137
		//lifacmvrec.origamt.set(acblclmIO.getSacscurbal());
		if (isEQ(subString(acblclmIO.getRldgacct(),acblclmIO.getRldgacct().length()-5,4), "0100")) {
			lifacmvrec.origamt.set(add(add(acblclmIO.getSacscurbal(),dthclmflxRec.nextinsamt),dthclmflxRec.susamt));
		}else{
			lifacmvrec.origamt.set(acblclmIO.getSacscurbal());
		}	
		updateAcmvCustomerSpecific();
		//end ILIFE-1137		
		if (isEQ(t3695rec.sign, "-")) {			
			compute(lifacmvrec.origamt, 2).set(mult(acblclmIO.getSacscurbal(), -1));						
		}
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(atmodrec.company);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getShortdesc());
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}

		CustomerSpecificPosting();
		Boolean isDeathClaimPayees = CMDTH006Permission && isNE(wsaaTotClaimPayees, ZERO);
		if(!isDeathClaimPayees){
			wsaaTotAcbl.add(acblclmIO.getSacscurbal());
		}
		acblclmIO.setFunction(varcom.nextr);
		acbl2406();
		return ;
	}

protected void postClaimInterestAcct2700()
	{
		post2701();
	}

protected void post2701()
	{
		if (isEQ(clmhclmIO.getInterest(), ZERO)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.effdate.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
		/*MOVE CLMHCLM-TRANNO                  TO LIFA-TRANNO.         */
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaPost2.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaPost2.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaPost2.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaPost2.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaPost2.toInt()]);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
		lifacmvrec.origamt.set(clmhclmIO.getInterest());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(atmodrec.company);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		/*MOVE CLMHCLM-TRANNO         TO LIFA-TRANREF,                 */
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getShortdesc());
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

protected void postClaimOfficeCharges3000()
	{
		post3301();
	}

protected void post3301()
	{
		if (isEQ(clmhclmIO.getOfcharge(), ZERO)) {
			return ;
		}
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.effdate.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
		/*MOVE CLMHCLM-TRANNO                  TO LIFA-TRANNO.         */
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaPost3.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaPost3.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaPost3.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaPost3.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaPost3.toInt()]);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
		lifacmvrec.origamt.set(clmhclmIO.getOfcharge());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(atmodrec.company);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		/*MOVE CLMHCLM-TRANNO         TO LIFA-TRANREF,                 */
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getShortdesc());
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}
//ILIFE-1137
//Death Claim Flexibility
protected void postUnpaidPrem4000()
{
	readTabT5688250();
	if (isNE(t5688rec.dflexmeth,SPACES)){		
		dthclmflxRec.chdrChdrcoy.set(chdrlifIO.getChdrcoy());
		dthclmflxRec.chdrChdrnum.set(chdrlifIO.getChdrnum());			
		dthclmflxRec.ptdate.set(chdrlifIO.ptdate);
		dthclmflxRec.dtofdeath.set(clmhclmIO.dtofdeath);
		dthclmflxRec.btdate.set(chdrlifIO.btdate);
		dthclmflxRec.cnttype.set(chdrlifIO.cnttype);
		dthclmflxRec.register.set(chdrlifIO.getRegister());
		dthclmflxRec.cntcurr.set(chdrlifIO.getCntcurr());	
		dthclmflxRec.effdate.set(chdrlifIO.getCurrfrom());
		callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
		if (isNE(dthclmflxRec.status, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(dthclmflxRec.dthclmflxrec);
			sysrSyserrRecInner.sysrStatuz.set(dthclmflxRec.status);				
			xxxxFatalError();
		}
		post4000();
	}	
}


protected void postExcessPrem4100()
{
	readTabT5688250();
	if (isNE(t5688rec.dflexmeth,SPACES)){
		dthclmflxRec.chdrChdrcoy.set(chdrlifIO.getChdrcoy());
		dthclmflxRec.chdrChdrnum.set(chdrlifIO.getChdrnum());			
		dthclmflxRec.ptdate.set(chdrlifIO.ptdate);
		dthclmflxRec.dtofdeath.set(clmhclmIO.dtofdeath);
		dthclmflxRec.btdate.set(chdrlifIO.btdate);
		dthclmflxRec.cnttype.set(chdrlifIO.cnttype);
		dthclmflxRec.register.set(chdrlifIO.getRegister());
		dthclmflxRec.cntcurr.set(chdrlifIO.getCntcurr());	
		dthclmflxRec.effdate.set(chdrlifIO.getCurrfrom());
		callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
		if (isNE(dthclmflxRec.status, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(dthclmflxRec.dthclmflxrec);
			sysrSyserrRecInner.sysrStatuz.set(dthclmflxRec.status);				
			xxxxFatalError();
		}
		post4100();
	}	
}

protected void readTabT5688250()
{
	read251();
}

protected void read251()
{
	itdmIO.setItemcoy(clmhclmIO.getChdrcoy());
	itdmIO.setItemtabl(t5688);
	itdmIO.setItempfx("IT");
	itdmIO.setItemitem(clmhclmIO.getCnttype());
	itdmIO.setItmfrm(wsaaToday);
	itdmIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
		xxxxFatalError();
	}
	if (isNE(itdmIO.getItemcoy(), clmhclmIO.getChdrcoy())
	|| isNE(itdmIO.getItemtabl(), t5688)
	|| isNE(itdmIO.getItemitem(), clmhclmIO.getCnttype())
	|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
		itdmIO.setItemitem(clmhclmIO.getCnttype());
		sysrSyserrRecInner.sysrParams.set(itdmIO.getParams());
		sysrSyserrRecInner.sysrStatuz.set(itdmIO.getStatuz());		
		xxxxFatalError();
	}
	t5688rec.t5688Rec.set(itdmIO.getGenarea());
}

protected void postDeathClaimSusp3300()
	{
		post13301();
		if(CMDTH006Permission){
            post3302();
        }
	}

protected void post13301()
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.effdate.set(ZERO);
		lifacmvrec.origamt.set(ZERO);
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		/*  COMPUTE LIFA-ORIGAMT      =          ACBL-SACSCURBAL +       */
		/*	START OF ILIFE-6727	*/
		if ( isEQ((t5645rec.sign[wsaaPost4.toInt()]),"-"))
		{
			if(isNE(wsaaSelectClaimPayees,ZERO)){
				compute(lifacmvrec.origamt, 2).set(sub(add(wsaaTotAcbl, mult(add(clmhclmIO.getInterest(), wsaaSelectClaimPayees), -1)), mult(clmhclmIO.getOfcharge(), -1)));
			}else{
				if(isNE(clmhclmIO.getCurrcd(),chdrlifIO.getCntcurr())) {
					para6010();
					compute(lifacmvrec.origamt, 2).set(sub(add(conlinkrec.amountOut, mult(clmhclmIO.getInterest(), -1)), mult(clmhclmIO.getOfcharge(), -1)));
				}
				else {
					compute(lifacmvrec.origamt, 2).set(sub(add(wsaaTotAcbl, mult(clmhclmIO.getInterest(), -1)), mult(clmhclmIO.getOfcharge(), -1)));
				}
			}
			CustomerSpecificAddIntAmt();
		}
		else{
            if(isNE(wsaaSelectClaimPayees,ZERO)){
                compute(lifacmvrec.origamt, 2).set(sub(add(wsaaTotAcbl, clmhclmIO.getInterest(),wsaaSelectClaimPayees), clmhclmIO.getOfcharge()));
            }else{
                compute(lifacmvrec.origamt, 2).set(sub(add(wsaaTotAcbl, clmhclmIO.getInterest()), clmhclmIO.getOfcharge()));
            }
		}
		/*	END OF ILIFE-6727	*/
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		if ( isEQ((t5645rec.sign[wsaaPost4.toInt()]),"-")  && isLT((lifacmvrec.origamt),0)     )
		{
			compute(lifacmvrec.origamt, 2).set(mult(lifacmvrec.origamt,-1) );
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
		/*MOVE CLMHCLM-TRANNO                  TO LIFA-TRANNO.         */
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaPost4.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaPost4.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaPost4.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaPost4.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaPost4.toInt()]);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(atmodrec.company);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		/*MOVE CLMHCLM-TRANNO         TO LIFA-TRANREF,                 */
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getShortdesc());
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set("99999999");
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

    protected void post3302()
    {
        lifacmvrec.rcamt.set(ZERO);
        lifacmvrec.contot.set(ZERO);
        lifacmvrec.acctamt.set(ZERO);
        lifacmvrec.frcdate.set(ZERO);
        lifacmvrec.effdate.set(ZERO);
        lifacmvrec.origamt.set(ZERO);
        lifacmvrec.tranno.set(ZERO);
        lifacmvrec.jrnseq.set(ZERO);
        lifacmvrec.crate.set(ZERO);
        lifacmvrec.transactionDate.set(ZERO);
        lifacmvrec.transactionTime.set(ZERO);
        lifacmvrec.user.set(ZERO);
        lifacmvrec.origamt.set(wsaaOtherClaimPayees);
        if (isEQ(lifacmvrec.origamt, ZERO)) {
            return ;
        }
        lifacmvrec.function.set("PSTW");
        lifacmvrec.batckey.set(wsaaBatckey);
        lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
        lifacmvrec.tranno.set(chdrlifIO.getTranno());
        lifacmvrec.sacscode.set(t5645rec.sacscode06);
        lifacmvrec.sacstyp.set(t5645rec.sacstype06);
        lifacmvrec.glcode.set(t5645rec.glmap06);
        lifacmvrec.glsign.set(t5645rec.sign06);
        lifacmvrec.contot.set(t5645rec.cnttot06);
        lifacmvrec.jrnseq.set(ZERO);
        lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
        lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
        lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
        lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
        lifacmvrec.genlcur.set(SPACES);
        lifacmvrec.genlcoy.set(atmodrec.company);
        lifacmvrec.acctamt.set(ZERO);
        lifacmvrec.crate.set(ZERO);
        lifacmvrec.postyear.set(SPACES);
        lifacmvrec.postmonth.set(SPACES);
        lifacmvrec.tranref.set(chdrlifIO.getTranno());
        lifacmvrec.trandesc.set(descIO.getShortdesc());
        lifacmvrec.effdate.set(wsaaToday);
        lifacmvrec.frcdate.set("99999999");
        lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
        lifacmvrec.termid.set(wsaaTermid);
        lifacmvrec.user.set(wsaaUser);
        lifacmvrec.transactionTime.set(wsaaTransactionTime);
        lifacmvrec.transactionDate.set(wsaaTransactionDate);
        callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
        if (isNE(lifacmvrec.statuz, varcom.oK)) {
            sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
            xxxxFatalError();
        }
    }

protected void postDeathClaimSusp6000()
	{
		post6001();
	}

protected void post6001()
	{
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.contot.set(ZERO);
		lifrtrnrec.acctamt.set(ZERO);
		lifrtrnrec.frcdate.set(ZERO);
		lifrtrnrec.effdate.set(ZERO);
		lifrtrnrec.origamt.set(ZERO);
		lifrtrnrec.tranno.set(ZERO);
		lifrtrnrec.jrnseq.set(ZERO);
		lifrtrnrec.crate.set(ZERO);
		lifrtrnrec.transactionDate.set(ZERO);
		lifrtrnrec.transactionTime.set(ZERO);
		lifrtrnrec.user.set(ZERO);
		/*  COMPUTE LIFR-ORIGAMT =  ACBL-SACSCURBAL + CLMHCLM-INTEREST   */
		/*COMPUTE LIFR-ORIGAMT =  WSAA-TOT-ACBL + CLMHCLM-INTEREST<011>*/
		/*                        - CLMHCLM-OFCHARGE.             <011>*/
		/*+ CLMHCLM-OTHERADJST.           <002>*/
		wsaaTotClaim.set(ZERO);
		clmdIO.setDataArea(SPACES);
		clmdIO.setChdrcoy(clmhclmIO.getChdrcoy());
		clmdIO.setChdrnum(clmhclmIO.getChdrnum());
		clmdIO.setFunction(varcom.begn);
		while ( !(isEQ(clmdIO.getStatuz(), varcom.endp))) {

			//performance improvement --  atiwari23
			clmdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			clmdIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

			calcClaim6100();
		}
		

		postXcurr6200();
		//death claim flexibility
		//compute(lifrtrnrec.origamt, 2).set(sub(add(add(add(add(wsaaTotClaim, clmhclmIO.getOtheradjst()), clmhclmIO.getZrcshamt()), clmhclmIO.getPolicyloan()), clmhclmIO.getInterest()), clmhclmIO.getOfcharge()));
		compute(lifrtrnrec.origamt, 2).set(sub(add(add(add(add(sub(sub(wsaaTotClaim, dthclmflxRec.nextinsamt), dthclmflxRec.susamt), clmhclmIO.getOtheradjst()), clmhclmIO.getZrcshamt()), clmhclmIO.getPolicyloan()), clmhclmIO.getInterest()), clmhclmIO.getOfcharge()));
		/*                      +  CLMHCLM-INTEREST - CLMHCLM-OFCHARGE. */
		if (isEQ(lifrtrnrec.origamt, ZERO)) {
			return ;
		}
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(wsaaBatckey);
		lifrtrnrec.rdocnum.set(clmhclmIO.getChdrnum());
		lifrtrnrec.tranno.set(chdrlifIO.getTranno());
		lifrtrnrec.sacscode.set(t5645rec.sacscode[wsaaPost4.toInt()]);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype[wsaaPost4.toInt()]);
		lifrtrnrec.glcode.set(t5645rec.glmap[wsaaPost4.toInt()]);
		lifrtrnrec.glsign.set(t5645rec.sign[wsaaPost4.toInt()]);
		lifrtrnrec.contot.set(t5645rec.cnttot[wsaaPost4.toInt()]);
		lifrtrnrec.jrnseq.set(ZERO);
		lifrtrnrec.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifrtrnrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifrtrnrec.rldgacct.set(clmhclmIO.getChdrnum());
		lifrtrnrec.origcurr.set(clmhclmIO.getCurrcd());
		lifrtrnrec.genlcoy.set(atmodrec.company);
		lifrtrnrec.acctamt.set(ZERO);
		lifrtrnrec.crate.set(ZERO);
		if (isEQ(clmhclmIO.getCurrcd(), wsaaCntcurr)) {
			lifrtrnrec.genlcur.set(SPACES);
		}
		else {
			getCurrencyRate7000();
			lifrtrnrec.crate.set(wsaaNominalRate);
			lifrtrnrec.genlcur.set(wsaaLedgerCcy);
			compute(lifrtrnrec.acctamt, 9).set(mult(wsaaNominalRate, lifrtrnrec.origamt));
		}
		zrdecplrec.amountIn.set(lifrtrnrec.acctamt);
		b000CallRounding();
		lifrtrnrec.acctamt.set(zrdecplrec.amountOut);
		lifrtrnrec.postyear.set(SPACES);
		lifrtrnrec.postmonth.set(SPACES);
		lifrtrnrec.tranref.set(chdrlifIO.getTranno());
		lifrtrnrec.trandesc.set(descIO.getShortdesc());
		lifrtrnrec.effdate.set(wsaaToday);
		lifrtrnrec.frcdate.set("99999999");
		lifrtrnrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifrtrnrec.termid.set(wsaaTermid);
		lifrtrnrec.user.set(wsaaUser);
		lifrtrnrec.transactionTime.set(wsaaTransactionTime);
		lifrtrnrec.transactionDate.set(wsaaTransactionDate);
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifrtrnrec.lifrtrnRec);
			xxxxFatalError();
		}
	}

protected void calcClaim6100()
	{
		try {
			readClmdclm6110();
			calcAmt6120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readClmdclm6110()
	{
		SmartFileCode.execute(appVars, clmdIO);
		if (isNE(clmdIO.getStatuz(), varcom.oK)
		&& isNE(clmdIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(clmdIO.getParams());
			xxxxFatalError();
		}
		if (isNE(clmhclmIO.getChdrcoy(), clmdIO.getChdrcoy())
		|| isNE(clmhclmIO.getChdrnum(), clmdIO.getChdrnum())
		|| isEQ(clmdIO.getStatuz(), varcom.endp)) {
			clmdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit6190);
		}
		clmdIO.setFunction(varcom.nextr);
		readHdcdValue6115();
	}

protected void calcAmt6120()
	{
		compute(wsaaTotHdcd, 2).set(add(wsaaTotHdcd, hdcdIO.getHactval()));
		compute(wsaaTotClaim, 2).set(add(wsaaTotClaim, clmdIO.getActvalue()));
	}

protected void readHdcdValue6115()
	{
		start6115();
	}

protected void start6115()
	{
		hdcdIO.setParams(SPACES);
		hdcdIO.setChdrcoy(clmdIO.getChdrcoy());
		hdcdIO.setChdrnum(clmdIO.getChdrnum());
		hdcdIO.setLife(clmdIO.getLife());
		hdcdIO.setCoverage(clmdIO.getCoverage());
		hdcdIO.setRider(clmdIO.getRider());
		hdcdIO.setCrtable(clmdIO.getCrtable());
		hdcdIO.setTranno(clmdIO.getTranno());
		hdcdIO.setFieldType(clmdIO.getFieldType());
		hdcdIO.setFormat(formatsInner.hdcdrec);
		hdcdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hdcdIO);
		if (isNE(hdcdIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(hdcdIO.getParams());
			xxxxFatalError();
		}
	}

protected void postXcurr6200()
	{
		start6200();
	}

protected void start6200()
	{
		if (isEQ(clmhclmIO.getCurrcd(), wsaaCntcurr)) {
			return ;
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(clmhclmIO.getChdrnum());
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.sacscode.set(t5645rec.sacscode[12]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[12]);
		lifacmvrec.glcode.set(t5645rec.glmap[12]);
		lifacmvrec.glsign.set(t5645rec.sign[12]);
		lifacmvrec.contot.set(t5645rec.cnttot[12]);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec.rldgacct.set(clmhclmIO.getChdrnum());
		lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
		lifacmvrec.origamt.set(clmhclmIO.getOfcharge());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(atmodrec.company);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.effdate.set(wsaaToday);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.origcurr.set(hdcdIO.getHcnstcur());
		lifacmvrec.origamt.set(wsaaTotHdcd);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode[13]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[13]);
		lifacmvrec.glcode.set(t5645rec.glmap[13]);
		lifacmvrec.glsign.set(t5645rec.sign[13]);
		lifacmvrec.contot.set(t5645rec.cnttot[13]);
		lifacmvrec.origcurr.set(clmhclmIO.getCurrcd());
		lifacmvrec.origamt.set(wsaaTotClaim);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec.lifacmvRec);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	*6100-POST-OTHER-ADJUSTMENT SECTION.                          <005
	************************************                          <005
	*                                                             <005
	*6101-POST.                                                   <005
	*                                                             <005
	*    IF CLMHCLM-OTHERADJST = ZERO                             <005
	*        GO TO 6190-EXIT.                                     <005
	*                                                             <005
	*    MOVE ZERO                   TO LIFA-RCAMT,               <005
	*                                   LIFA-CONTOT,              <005
	*                                   LIFA-ACCTAMT,             <005
	*                                   LIFA-FRCDATE,             <005
	*                                   LIFA-EFFDATE,             <005
	*                                   LIFA-ORIGAMT,             <005
	*                                   LIFA-TRANNO,              <005
	*                                   LIFA-JRNSEQ,              <005
	*                                   LIFA-CRATE,               <005
	*                                   LIFA-TRANSACTION-DATE,    <005
	*                                   LIFA-TRANSACTION-TIME,    <005
	*                                   LIFA-USER.                <005
	*                                                             <005
	*    MOVE 'PSTW'                          TO LIFA-FUNCTION.   <005
	*    MOVE WSAA-BATCKEY                    TO LIFA-BATCKEY.    <005
	*    MOVE CLMHCLM-CHDRNUM                 TO LIFA-RDOCNUM.    <005
	*    MOVE CHDRLIF-TRANNO                  TO LIFA-TRANNO.     <005
	*    MOVE T5645-SACSCODE(WSAA-POST5)      TO LIFA-SACSCODE.   <005
	*    MOVE T5645-SACSTYPE(WSAA-POST5)      TO LIFA-SACSTYP.    <005
	*    MOVE T5645-GLMAP(WSAA-POST5)         TO LIFA-GLCODE.     <005
	*    MOVE T5645-SIGN(WSAA-POST5)          TO LIFA-GLSIGN.     <005
	*    MOVE T5645-CNTTOT(WSAA-POST5)        TO LIFA-CONTOT.     <005
	*    MOVE ZERO                            TO LIFA-JRNSEQ.     <005
	*    MOVE CLMHCLM-CHDRCOY                 TO LIFA-RLDGCOY.    <005
	*    MOVE CLMHCLM-CHDRCOY                 TO LIFA-GENLCOY.    <005
	*    MOVE CLMHCLM-CHDRNUM                 TO LIFA-RLDGACCT.   <005
	*    MOVE CLMHCLM-CURRCD                  TO LIFA-ORIGCURR.   <005
	*    MOVE CLMHCLM-OTHERADJST              TO LIFA-ORIGAMT.    <005
	*    MOVE SPACES                 TO LIFA-GENLCUR.             <005
	*    MOVE ATMD-COMPANY           TO LIFA-GENLCOY.             <005
	*    MOVE ZERO                   TO LIFA-ACCTAMT.             <005
	*    MOVE ZERO                   TO LIFA-CRATE.               <005
	*    MOVE SPACES                 TO LIFA-POSTYEAR,            <005
	*                                   LIFA-POSTMONTH.           <005
	*    MOVE CHDRLIF-TRANNO         TO LIFA-TRANREF,             <005
	*    MOVE DESC-SHORTDESC         TO LIFA-TRANDESC.            <005
	*    MOVE WSAA-TODAY             TO LIFA-EFFDATE.             <005
	*    MOVE VRCM-MAX-DATE          TO LIFA-FRCDATE.             <005
	*    MOVE CHDRLIF-CNTTYPE        TO LIFA-SUBSTITUTE-CODE(01). <005
	*    MOVE WSAA-TERMID            TO LIFA-TERMID.              <005
	*    MOVE WSAA-USER              TO LIFA-USER.                <005
	*    MOVE WSAA-TRANSACTION-TIME  TO LIFA-TRANSACTION-TIME.    <005
	*    MOVE WSAA-TRANSACTION-DATE  TO LIFA-TRANSACTION-DATE.    <005
	*                                                             <005
	*    CALL 'LIFACMV'           USING LIFA-LIFACMV-REC.         <005
	*                                                             <005
	*    IF LIFA-STATUZ       NOT = O-K                           <005
	*         MOVE LIFA-LIFACMV-REC    TO SYSR-PARAMS             <005
	*         PERFORM XXXX-FATAL-ERROR.                           <005
	*                                                             <005
	*6190-EXIT.                                                   <005
	*    EXIT.                                                    <005
	/                                                             <005
	* </pre>
	*/
protected void getCurrencyRate7000()
	{
		try {
			start7000();
			reRead7120();
			findRate7150();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start7000()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(lifrtrnrec.origcurr);
	}

protected void reRead7120()
	{
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), "****")) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaLedgerCcy.set(t3629rec.ledgcurr);
		wsaaNominalRate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaNominalRate, ZERO)
		|| isGT(wsaaX, 7))) {
			findRate7150();
		}

		if (isEQ(wsaaNominalRate, ZERO)) {
			if (isNE(t3629rec.contitem, SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				reRead7120();
				return ;
			}
			else {
				sysrSyserrRecInner.sysrStatuz.set("MRNF");
				xxxxFatalError();
			}
		}
		else {
			goTo(GotoLabel.exit7190);
		}
	}

protected void findRate7150()
	{
		if (isGTE(clmhclmIO.getEffdate(), t3629rec.frmdate[wsaaX.toInt()])
		&& isLTE(clmhclmIO.getEffdate(), t3629rec.todate[wsaaX.toInt()])) {
			wsaaNominalRate.set(t3629rec.scrate[wsaaX.toInt()]);
		}
		else {
			wsaaX.add(1);
		}
	}

	/**
	* <pre>
	* Housekeeping prior to termination.                          *
	* </pre>
	*/
protected void housekeepingTerminate3000()
	{
		/*HOUSEKEEPING-TERMINATE*/
		ptrnTransaction3100();
		batchHeader3200();
		releaseSoftlock3300();
		/*EXIT*/
	}

	/**
	* <pre>
	* Write PTRN transaction.                                     *
	* </pre>
	*/
protected void ptrnTransaction3100()
	{
		ptrnTransaction3101();
	}

protected void ptrnTransaction3101()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTransactionDate(0);
		ptrnIO.setTransactionTime(0);
		ptrnIO.setUser(0);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptrnIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Write batch header transaction.                             *
	* </pre>
	*/
protected void batchHeader3200()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(batcuprec.batcupRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Release Soft locked record.                                 *
	* </pre>
	*/
protected void releaseSoftlock3300()
	{
		releaseSoftlock3301();
	}

protected void releaseSoftlock3301()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(chdrlifIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifsttrrec.lifsttrRec);
			sysrSyserrRecInner.sysrStatuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}

protected void b000CallRounding()
	{
		/*B100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(atmodrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(clmhclmIO.getCurrcd());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(zrdecplrec.statuz);
			sysrSyserrRecInner.sysrParams.set(zrdecplrec.zrdecplRec);
			xxxxFatalError();
		}
		/*B900-EXIT*/
	}

  protected void getDeathRegact()
  {
	    clmdclmIO.setDataArea(SPACES);
		clmdclmIO.setChdrcoy(clmhclmIO.getChdrcoy());
		clmdclmIO.setChdrnum(clmhclmIO.getChdrnum());
		clmdclmIO.setFunction(varcom.begn);
		while ( !(isEQ(clmdclmIO.getStatuz(), varcom.endp))) {

			//performance improvement --  atiwari23
			clmdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			clmdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

			calcClaim();
		}
  }
  protected void calcClaim()
	{
		try {
			readClmdclm();
			 
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}
  
  protected void readClmdclm()
	{
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.oK)
		&& isNE(clmdclmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(clmdclmIO.getParams());
			xxxxFatalError();
		}
		
		if ( isEQ(clmdclmIO.getStatuz(), varcom.oK) && isEQ(clmhclmIO.getChdrcoy(), clmdclmIO.getChdrcoy()) &&  isEQ(clmhclmIO.getChdrnum(), clmdclmIO.getChdrnum())	 ) 
			{
				 wsaaRldgChdrnum.set(clmdclmIO.getChdrnum());
				  wsaaRldgLife.set(clmdclmIO.getLife());
				  wsaaRldgCoverage.set(clmdclmIO.getCoverage());
				  wsaaRldgRider.set(clmdclmIO.getRider());  
				  wsaaRldgPlnsfx.set("00");
			  
			}
		
		if (isNE(clmhclmIO.getChdrcoy(), clmdclmIO.getChdrcoy())
		|| isNE(clmhclmIO.getChdrnum(), clmdclmIO.getChdrnum())
		|| isEQ(clmdclmIO.getStatuz(), varcom.endp)) {
			clmdclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit6190);
		}
		clmdclmIO.setFunction(varcom.nextr);
		 
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData clmhclmrec = new FixedLengthStringData(10).init("CLMHCLMREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
	private FixedLengthStringData acblclmrec = new FixedLengthStringData(10).init("ACBLCLMREC");
	private FixedLengthStringData hdcdrec = new FixedLengthStringData(10).init("HDCDREC");
	private FixedLengthStringData mliarec = new FixedLengthStringData(10).init("MLIAREC");
	private FixedLengthStringData clntrec = new FixedLengthStringData(10).init("CLNTREC");
}
/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
private static final class SysrSyserrRecInner {

		/* System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(538);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	private FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrSyserrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 356);
	private FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 528).init("ITDMREC");
}

	protected void CustomerSpecificPosting() {}

	protected void CustomerSpecificAddIntAmt() {}
	
	protected void updateAcmvCustomerSpecific() {}
	
	protected void post4000(){}
	
	protected void post4100(){}
	
	protected void para6010()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.function.set("CVRT");
		/*MOVE WSAA-CURR-STORE        TO CLNK-CURR-IN.            <007>*/
		conlinkrec.currIn.set(chdrlifIO.getCntcurr());
		conlinkrec.currOut.set(clmhclmIO.getCurrcd());
		conlinkrec.amountIn.set(wsaaTotAcbl);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(wsaaToday);
		conlinkrec.company.set(atmodrec.company);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(conlinkrec.clnk002Rec);
			sysrSyserrRecInner.sysrStatuz.set(conlinkrec.statuz);
			xxxxFatalError();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(clmhclmIO.getCurrcd());
		callRounding7000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}
	
	protected void callRounding7000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(atmodrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(zrdecplrec.statuz);
			sysrSyserrRecInner.sysrParams.set(zrdecplrec.zrdecplRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}
	
	protected void performLincProcessing() {
		Tjl47rec tjl47rec = new Tjl47rec(); 
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(atmodrec.company.toString());
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf.setItemtabl(tjl47);
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if(itempf != null) {
			tjl47rec.tjl47Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			LincCSService lincService = (LincCSService) getApplicationContext().getBean(tjl47rec.lincsubr.toString().toLowerCase());
			lincService.generalProcessing(setLincpfHelper(), tjl47rec);
		}
	}
	
	protected LincpfHelper setLincpfHelper() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		LincpfHelper lincpfHelper = new LincpfHelper();
		lincpfHelper.setChdrcoy(atmodrec.company.toString());
		lincpfHelper.setChdrnum(wsaaPrimaryChdrnum.toString());
		lincpfHelper.setOwncoy(wsaaFsuCoy.toString());
		lincpfHelper.setClntnum(chdrlifIO.getCownnum().toString());
		lincpfHelper.setCownnum(chdrlifIO.getCownnum().toString());
		lincpfHelper.setCnttype(chdrlifIO.getCnttype().toString());
		lincpfHelper.setOccdate(chdrlifIO.getOccdate().toInt());	
		lincpfHelper.setTransactionDate(wsaaToday.toInt());
		lincpfHelper.setTranscd(wsaaBatckey.batcBatctrcde.toString());
		lincpfHelper.setLanguage(atmodrec.language.toString());
		lincpfHelper.setTranno(chdrlifIO.getTranno().toInt());
		lincpfHelper.setPtrnEff(wsaaToday.toInt());
		lincpfHelper.setTermId(varcom.vrcmTermid.toString());
		lincpfHelper.setTrdt(varcom.vrcmDate.toInt());
		lincpfHelper.setTrtm(varcom.vrcmTime.toInt());
		lincpfHelper.setUsrprf(varcom.vrcmUser.toString());
		lincpfHelper.setJobnm(wsaaUser.toString());
		lincpfHelper = setClientDetails(lincpfHelper);
		lincpfHelper = getLifeDetails(lincpfHelper);
		return lincpfHelper;
	}
	
	protected LincpfHelper getLifeDetails(LincpfHelper lincpfHelper) {
		lincpfHelper.setLifcnum(lifelnbIO.getLifcnum().toString());
		lincpfHelper.setLcltdob(clntIO.getCltdob().toInt());
		lincpfHelper.setLcltsex(lifelnbIO.getCltsex().toString());
		lincpfHelper.setLzkanasnm(clntIO.getzkanasnm().toString());
		lincpfHelper.setLzkanagnm(clntIO.getzkanagnm().toString());
		lincpfHelper.setLzkanaddr01(clntIO.getzkanaddr01().toString());
		lincpfHelper.setLzkanaddr02(clntIO.getzkanaddr02().toString());
		lincpfHelper.setLzkanaddr03(clntIO.getzkanaddr03().toString());
		lincpfHelper.setLzkanaddr04(clntIO.getzkanaddr04().toString());
		lincpfHelper.setLzkanaddr05(clntIO.getzkanaddr05().toString());
		lincpfHelper.setLgivname(clntIO.getGivname().toString());
		lincpfHelper.setLsurname(clntIO.getSurname().toString());
		return lincpfHelper;
	}
	
	protected LincpfHelper setClientDetails(LincpfHelper lincpfHelper) {
		Clntpf clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(lincpfHelper.getOwncoy());
		clntpf.setClntnum(lincpfHelper.getClntnum());
		clntpf = clntpfDAO.selectClient(clntpf);
		lincpfHelper.setZkanasnm(clntpf.getZkanasnm());
		lincpfHelper.setZkanagnm(clntpf.getZkanagnm());
		lincpfHelper.setZkanaddr01(clntpf.getZkanaddr01());
		lincpfHelper.setZkanaddr02(clntpf.getZkanaddr02());
		lincpfHelper.setZkanaddr03(clntpf.getZkanaddr03());
		lincpfHelper.setZkanaddr04(clntpf.getZkanaddr04());
		lincpfHelper.setZkanaddr05(clntpf.getZkanaddr05());
		lincpfHelper.setGivname(clntpf.getGivname());
		lincpfHelper.setSurname(clntpf.getSurname());
		lincpfHelper.setCltsex(clntpf.getCltsex());
		lincpfHelper.setCltdob(clntpf.getCltdob());
		lincpfHelper.setClttype(clntpf.getClttype());
		return lincpfHelper;
	}
	protected void updateValidflagCustomerSpecific() {}
	protected void setLifeAsClmdParam() {}
	protected void setLifeAsClmdSearchKey() {}
	protected void setRldgacctWithLif()	{}
}
