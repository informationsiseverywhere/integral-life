package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.screens.Sd5hqScreenVars;
import com.csc.life.terminationclaims.tablestructures.Td5hqrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pd5hq extends ScreenProgCS {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5HQ");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Td5hqrec td5hqrec = new Td5hqrec();
	private String wsaaUpdateFlag = "";
	private Sd5hqScreenVars sv = ScreenProgram.getScreenVars( Sd5hqScreenVars.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);	
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Itempf itemkey = new Itempf();
	private Descpf descpf = new Descpf();
	private Itempf itempf = null;
	
	
	public Pd5hq() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5hq", AppVars.getInstance(), sv);
	}
	
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}
	
	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray){
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000(){
	sv.dataArea.set(SPACES);
	readRecord1010();
	moveToScreen1020();
	
 }

protected void readRecord1010() {
		
	itemkey.setItempfx(wsspsmart.itemkey.substring(0, 2));
	itemkey.setItemcoy(wsspsmart.itemkey.substring(2, 3));
	itemkey.setItemtabl(wsspsmart.itemkey.substring(3, 8));
	itemkey.setItemitem(wsspsmart.itemkey.substring(8, 16));
	
	itempf = itemDAO.getItemRecordByItemkey(itemkey);
	
	if(itempf == null) {
		syserrrec.params.set(itemkey.toString());
		fatalError600();
	}
	
	itemkey.setItemseq(itempf.getItemseq());
	
	td5hqrec.td5hqRec.set(StringUtil.rawToString(itempf.getGenarea()));
	
	descpf = descDAO.getdescData(itemkey.getItempfx(), itemkey.getItemtabl(), itemkey.getItemitem(), itemkey.getItemcoy(), wsspcomn.language.toString());
	
	if(descpf == null) {
		syserrrec.params.set(itemkey.toString());
		fatalError600();
	}
	sv.longdesc.set(descpf.getLongdesc());
}

protected void moveToScreen1020() {
   sv.company.set(itemkey.getItemcoy());
   sv.item.set(itemkey.getItemitem());
   sv.tabl.set(itemkey.getItemtabl());
   sv.calBasis.set(td5hqrec.calBasis);
   
}

protected void preScreenEdit(){
	/*PRE-START*/
	/*    This section will handle any action required on the screen **/
	/*    before the screen is painted.                              **/
	if (isEQ(wsspcomn.flag,"I")) {
		scrnparams.function.set(varcom.prot);
	}
	return ;
	/*PRE-EXIT*/
}
/**
* <pre>
*     RETRIEVE SCREEN FIELDS AND EDIT
* </pre>
*/
protected void screenEdit2000(){
			screenIo2010();
			exit2090();
		}

protected void screenIo2010(){
	wsspcomn.edterror.set(varcom.oK);
	/*VALIDATE*/
	if (isEQ(wsspcomn.flag,"I")) {
		return ;
	}
	/*OTHER*/
}

protected void exit2090(){
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}


protected void checkChanges3100()
{
	/*CHECK*/
    //IFSU-2957
	if (isNE(sv.calBasis,td5hqrec.calBasis)) {
		td5hqrec.calBasis.set(sv.calBasis);		
		wsaaUpdateFlag = "Y";
	}
	/*EXIT*/
}

protected void update3000(){
	
	if (isEQ(wsspcomn.flag,"I")) {
		return ;
	}
	/*CHECK-CHANGES*/
	wsaaUpdateFlag = "N";
	if (isEQ(wsspcomn.flag,"C")) {
		wsaaUpdateFlag = "Y";
	}
	checkChanges3100();
	
	if(wsaaUpdateFlag.equals("Y")) {
		itemkey.setGenarea(td5hqrec.td5hqRec.toString().getBytes());
		itemDAO.updateSmartTableItem(itemkey);
	}
	
}

protected void whereNext4000(){
	/*NEXT-PROGRAM*/
	wsspcomn.programPtr.add(1);
	/*EXIT*/
	
}

}
