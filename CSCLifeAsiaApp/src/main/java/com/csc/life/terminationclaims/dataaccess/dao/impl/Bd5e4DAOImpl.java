package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.Bd5e4DAO;
import com.csc.life.terminationclaims.dataaccess.model.Zlreinpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Bd5e4DAOImpl extends BaseDAOImpl<Zlreinpf> implements Bd5e4DAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5e4DAOImpl.class);
	
	public List<Zlreinpf> loadDataByBatch(int batchID) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM Bd5e4TEMP ");
		sb.append("WHERE BATCHID = ? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, RDOCNUM DESC ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;	
		List<Zlreinpf> zlreinpfList = new ArrayList<Zlreinpf>();

		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Integer.toString(batchID));
			rs = ps.executeQuery();

			while(rs.next()){
				Zlreinpf zlreinpf = new Zlreinpf();		
				zlreinpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				zlreinpf.setChdrcoy(rs.getString("CHDRCOY")!= null ? (rs.getString("CHDRCOY").trim()) : "");
				zlreinpf.setChdrnum(rs.getString("CHDRNUM")!= null ? (rs.getString("CHDRNUM").trim()) : "");	
				zlreinpf.setCnttype(rs.getString("CNTTYPE")!= null ? (rs.getString("CNTTYPE").trim()) : "");
				zlreinpf.setOwnnum(rs.getString("OWNNUM")!= null ? (rs.getString("OWNNUM").trim()) : "");
				zlreinpf.setRdocnum(rs.getString("RDOCNUM")!= null ? (rs.getString("RDOCNUM").trim()) : "");
				zlreinpf.setRcpttype(rs.getString("RCPTTYPE")!= null ? (rs.getString("RCPTTYPE").trim()) : "");
				zlreinpf.setEffdate(rs.getInt("EFFDATE"));				
				zlreinpf.setOrigamt(rs.getBigDecimal("ORIGAMT"));
				zlreinpfList.add(zlreinpf);
			}
		}catch (SQLException e) {
			LOGGER.error("loadDataByBatch()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return zlreinpfList;
	}	
	
	private void initializeBd5e4Temp() {
		StringBuilder sb = new StringBuilder("");
		sb.append("DELETE FROM BD5E4TEMP ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			int rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("initializeBd5e4Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}	
	
	public int populateBd5e4Temp(int batchExtractSize, String payxtempTable,String ChdrnumFrom,String ChdrnumTo) {
		initializeBd5e4Temp();
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		 
		sb.append("INSERT INTO VM1DTA.BD5E4TEMP ");
		sb.append("(BATCHID, UNIQUE_NUMBER, CHDRCOY, CHDRNUM, CNTTYPE, OWNNUM, RDOCNUM, EFFDATE, ORIGAMT, RCPTTYPE)   ");
		sb.append("SELECT floor((row_number()over(ORDER BY RFLP.CHDRCOY , RFLP.CHDRNUM , RFLP.RDOCNUM )-1)/?) BATCHID, ");
		sb.append("RFLP.UNIQUE_NUMBER, RFLP.CHDRCOY, RFLP.CHDRNUM, RFLP.CNTTYPE, RFLP.OWNNUM, RFLP.RDOCNUM, RFLP.EFFDATE, RFLP.ORIGAMT, RFLP.RCPTTYPE ");		
		sb.append("FROM  ");
		sb.append(payxtempTable); 
		sb.append(" RFLP ");
		sb.append("WHERE ERORCDE = ' ' AND CHDRNUM BETWEEN ? AND ? ");
		sb.append("ORDER BY CHDRCOY, CHDRNUM, RDOCNUM  DESC");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setInt(1, batchExtractSize);		
			ps.setString(2, ChdrnumFrom);	
			ps.setString(3, ChdrnumTo);	
			rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("populateBd5e4Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		return rows;
	}	
	
	public void populateFailBIRptTmp(String payxtempTable,String wsaaChdrnumFrom,String wsaaChdrnumTo,int Sno) {
		
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		 
		sb.append("INSERT INTO VM1DTA.BD5E4RPTTMP ");
		sb.append("(CHDRCOY, CHDRNUM, CNTTYPE, OWNNUM, RDOCNUM, EFFDATE, ORIGAMT, RCPTTYPE, REINSDATE, FFLAG, ERORCDE, USRPRF, SNO, DATIME)   ");
		sb.append("SELECT RFLP.CHDRCOY, RFLP.CHDRNUM, RFLP.CNTTYPE, RFLP.OWNNUM, RFLP.RDOCNUM, RFLP.EFFDATE, RFLP.ORIGAMT, RFLP.RCPTTYPE, RFLP.EFFDATE, ? , RFLP.ERORCDE, ?,?,?");		
		sb.append(" FROM  ");
		sb.append(payxtempTable); 
		sb.append(" RFLP ");
		sb.append("WHERE ERORCDE IS NOT NULL AND CHDRNUM BETWEEN ? AND ? ");
		sb.append("ORDER BY CHDRCOY, CHDRNUM, RDOCNUM  DESC");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, "F");
			ps.setString(2, getUsrprf());
			ps.setInt(3, Sno);
			ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			ps.setString(5, wsaaChdrnumFrom);	
			ps.setString(6, wsaaChdrnumTo);	
			rows = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("populateBd5e4Temp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
	}	
	
	public void populateBIReportTmp(List<Zlreinpf> zlreinpfList, int Sno) {		
		int rows = 0;
		StringBuilder sb = new StringBuilder("");
		 
		sb.append("INSERT INTO VM1DTA.BD5E4RPTTMP ");
		sb.append("(CHDRCOY, CHDRNUM, CNTTYPE, OWNNUM, RDOCNUM, EFFDATE, ORIGAMT, RCPTTYPE, REINSDATE, FFLAG, ERORCDE, USRPRF, SNO, DATIME)   ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");		
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try{
			ps = getPrepareStatement(sb.toString());
			for (Zlreinpf z : zlreinpfList){
				ps.setString(1, z.getChdrcoy());
				ps.setString(2, z.getChdrnum());
				ps.setString(3, z.getCnttype());
				ps.setString(4, z.getOwnnum());
				ps.setString(5, z.getRdocnum());
				ps.setInt(6, z.getEffdate());
				ps.setBigDecimal(7, z.getOrigamt());
				ps.setString(8, z.getRcpttype());
				ps.setInt(9, z.getEffdate());
				if(z.getErorcde() != null && z.getErorcde().length() > 0)
					ps.setString(10, "F");
				else
					ps.setString(10, "S");
				ps.setString(11, z.getErorcde());
				ps.setString(12, getUsrprf());
				ps.setInt(13, Sno);
				ps.setTimestamp(14, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
			
		}catch (SQLException e) {
			LOGGER.error("populateBIReportTmp()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}	
		
	}

}
