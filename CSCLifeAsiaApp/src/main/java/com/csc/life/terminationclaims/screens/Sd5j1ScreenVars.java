package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sd5j1ScreenVars extends SmartVarModel{

	public FixedLengthStringData dataArea = new FixedLengthStringData(169);
	public FixedLengthStringData dataFields = new FixedLengthStringData(57).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData allowperiod = DD.allowperiod1.copyToZonedDecimal().isAPartOf(dataFields,1);
	public ZonedDecimalData daycheck = DD.daycheck.copyToZonedDecimal().isAPartOf(dataFields,4);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,45);
	public FixedLengthStringData subroutine = DD.subroutine.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 57);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData allowperiodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData daycheckErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData subroutineErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 85);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] allowperiodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] daycheckOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] subroutineOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sd5j1screenWritten = new LongData(0);
	public LongData Sd5j1protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5j1ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, allowperiod, daycheck , subroutine};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, allowperiodOut, daycheckOut, subroutineOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr,  allowperiodErr, daycheckErr, subroutineErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5j1screen.class;
		protectRecord = Sd5j1protect.class;
	}

}
