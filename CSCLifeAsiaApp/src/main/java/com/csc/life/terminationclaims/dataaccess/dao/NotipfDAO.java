package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

/**
 * 
 * @author hxu32
 * related database table NOTIPF DAO
 */

public interface NotipfDAO extends BaseDAO<Notipf>{
	public boolean insertNotipf(Notipf notipf);
	public int getCountByLifeAssNum(String notificnum);
	public String getMaxNotifinum();
	//ICIL-1502
	public String getMaxTransno(String notifinum);
	public List<Notipf> getNotifiRecord(String notificnum);
	public List<Notipf> getNotifiRecordByValidFlag(String notificnum,String validFlag);
	public Notipf getNotiReByNotifin(String notifinum,String notificoy);
	public Notipf getNotiReByNotifinAndValidflag(String notifinum,String notificoy,String validflag);
	//update notification status to 'Closed'
	public int updateNotifiStatus(String notifistatus,String notifinum);
	public int updateNotipf(Notipf notipf);
	//ICIL-1502
	public int updateNotipfValidFlag(String validflag, String notifinum);
	public int updateNotipfValidFlagToClose(String validflag, String notifinum);
	public Notipf getExactNotipfByTransno(String notifinum, String transno);
	public Notipf getStatusByNumAndCnum(String notificnum,String chdrnum);
	public Notipf getChdrByFinum(String notifinum);
	
}
