package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PymtpfDAO;
import com.csc.fsu.general.dataaccess.model.Pymtpf;
import com.csc.fsu.general.procedures.Chkbkcd;
import com.csc.fsu.general.recordstructures.Chkbkcdrec;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.screens.Sjl51ScreenVars;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.utils.UserMapingUtils;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* CLAIM PAYMENT SUB MENU.
**/

public class Pjl51 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl51.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL51");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Sjl51ScreenVars sv = ScreenProgram.getScreenVars( Sjl51ScreenVars.class);
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);

	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Chkbkcdrec chkbkcdrec = new Chkbkcdrec();
	private Batckey wsaaBatckey = new Batckey();
	private FixedLengthStringData wsaaTrancde = new FixedLengthStringData(4);
	private CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Cattpf cattpf;
	private Pymtpf pymtpf;
	private PymtpfDAO pymtpfDAO = getApplicationContext().getBean("pymtpfDAO",PymtpfDAO.class);
	private List<Pymtpf> pymtpfList = new ArrayList<Pymtpf>();
	private String longUserID;
	/* ERRORS */
	private static final String H137 = "H137";
	private static final String F649 = "F649";
	private static final String F022 = "F022";
	private static final String JL71 = "JL71";
	private static final String JL72 = "JL72";
	private static final String JL73 = "JL73";
	private static final String JL74 = "JL74";
	private static final String JL75 = "JL75";
	private static final String JL76 = "JL76";
	private static final String JL77 = "JL77";
	private static final String JL78 = "JL78";
	private static final String JL79 = "JL79";
	private static final String W557 = "W557";
	private static final String JL83 = "JL83";
	private static final String E174 = "E174";  
	private static final String H419 = "H419";  
	private static final String I065 = "I065";
	private static final String RF20 = "RF20";
	
	public Pjl51() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl51", AppVars.getInstance(), sv);
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}

		/**
		* The mainline method is the default entry point of the program when called by other programs using the
		* Quipoz runtime framework.
		*/
	@Override
	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.info("mainline {}", e);
		}
	}
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo {}", e);
		}
}

		/**
		* <pre>
		***  COPY LARGENMCPY.                                             
		***  COPY PAYENMCPY.                                              
		*      INITIALISE FIELDS FOR SHOWING ON SCREEN
		* </pre>
		*/
	@Override
	protected void initialise1000()
		{
			/*INITIALISE*/
			wsspcomn.bankcode.set(SPACES);
			sv.dataArea.set(SPACES);
			sv.windowtype.set("1");
			sv.action.set(wsspcomn.sbmaction);
			wsaaBatckey.batcFileKey.set(wsspcomn.batchkey);
			wsaaTrancde.set(wsaaBatckey.batcBatctrcde);
			longUserID = UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString());
		}


	protected void preScreenEdit()
		{
			/*PRE-START*/
			return ;
			/*PRE-EXIT*/
		}

	@Override
	protected void screenEdit2000()
		{
			screenIo2010();
			checkForErrors2080();
		}

	protected void screenIo2010()
		{
			validateAction2100();
			if(isEQ(sv.actionErr, SPACES)) {
				validateScreen2030();
			}
		}
	
	protected void validateAction2100()
	{
		checkAgainstTable2110();
		checkSanctions2120();
	}
	
	protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, Varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			return;
		}
	}
	
	protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, Varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, Varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

	private void validateScreen2030()
		{
			List<Pymtpf> pymtpfListByPayNum = pymtpfDAO.getpymtpfList(sv.paymentNum.toString());
			if(!pymtpfListByPayNum.isEmpty()) {
				pymtpf = pymtpfListByPayNum.get(0);
			}	
			if(isEQ(sv.action, "A")) {
				
				validateCreateAction2040();
				
			}
			
			if(isNE(sv.action, "A")) {
				
				validateOtherAction2050();
			}
		}


	private void validateOtherAction2050() {
		
		if(isEQ(sv.paymentNum, SPACES)) {
			sv.paymentnumErr.set(JL75);
		}else {		
			if(null == pymtpf) {
				sv.paymentnumErr.set(W557);
			}else {
				validatePaymentStatus();
				if(isEQ(sv.paymentnumErr, SPACES) && longUserID != null) {
					auditCheck10000();
				}
			}
		}
		if(isNE(sv.claimnmber, SPACES)) {
			sv.claimnmberErr.set(JL73);
		}
		
		if(isNE(sv.bankcode, SPACES)) {
			sv.bankcodeErr.set(JL74);
		}
	}


	private void validatePaymentStatus() {
		
		if(isEQ(sv.action, "B") || isEQ(sv.action, "C")) {
			if(isEQ("AU",pymtpf.getProcind())) {
				sv.paymentnumErr.set(JL77);
			}else if(isEQ("AQ",pymtpf.getProcind())) {
				sv.paymentnumErr.set(JL76);
			}else if(isEQ("RC",pymtpf.getProcind())) {
				sv.paymentnumErr.set(E174);
			}
		}
		
		validateAuthorizeAction();
		validateRemoveAction();
				
		if(isEQ("PR",pymtpf.getProcind()) && isNE(sv.action, "E") && isNE(sv.action, "F")) {
			sv.paymentnumErr.set(H419);
		}
	}


	private void validateAuthorizeAction() {
		if(isEQ(sv.action, "D")) {
			if(isEQ("AU",pymtpf.getProcind())) {
				sv.paymentnumErr.set(JL77);
			}else if(isEQ("RC",pymtpf.getProcind())) {
				sv.paymentnumErr.set(E174);
			}else if(isNE("AQ",pymtpf.getProcind()) && isNE("PR",pymtpf.getProcind())) {
				sv.paymentnumErr.set(RF20);
			}
		}
	}


	private void validateRemoveAction() {
		if(isEQ(sv.action, "E") && isNE("RQ",pymtpf.getProcind()) && 
				isNE("RM",pymtpf.getProcind())) {
			sv.paymentnumErr.set(I065);
		}
	}

	private void validateCreateAction2040() {
		if(isEQ(sv.claimnmber, SPACES)) {
			sv.claimnmberErr.set(F649);
		}else {
			validateClaimNumber2050();
		}
		
		if(isEQ(sv.bankcode, SPACES)) {
			sv.bankcodeErr.set(F022);
		}else {
			chkbkcdrec.function.set("CASHBANK");
			chkbkcdrec.bankcode.set(sv.bankcode);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			chkbkcdrec.user.set(varcom.vrcmUser);
			chkbkcdrec.company.set(wsspcomn.company);
			callProgram(Chkbkcd.class, chkbkcdrec.chkbkcdRec);
			if (isEQ(chkbkcdrec.statuz, Varcom.bomb)) {
				syserrrec.statuz.set(chkbkcdrec.statuz);
				fatalError600();
			}
			if (isNE(chkbkcdrec.statuz, Varcom.oK)) {
				sv.bankcodeErr.set(chkbkcdrec.statuz);
				wsspcomn.edterror.set("Y");
				return;
			}
		}
		
		if(isNE(sv.paymentNum, SPACES)) {
			sv.paymentnumErr.set(JL72);
		}
	}


	private void validateClaimNumber2050() {
		cattpf = cattpfDAO.approvedClaimRecord(sv.claimnmber.toString());
		if(null == cattpf) {
			sv.claimnmberErr.set(JL71);
		}else {
			pymtpfList = pymtpfDAO.getpaymentList();
			for(Pymtpf pymtpfRec : pymtpfList) {
				if(isNE("RC",pymtpfRec.getProcind()) && isEQ(pymtpfRec.getClaimnum(), sv.claimnmber.toString())){
					sv.claimnmberErr.set(JL83);
				}
			}
			Chdrpf chdrpfObj = chdrpfDAO.getchdrRecord(wsspcomn.company.toString(), cattpf.getChdrnum());
			if(chdrpfObj !=null && !chdrpfObj.getStatcode().equals("DH") && isEQ(sv.claimnmberErr,SPACES)) {
				sv.claimnmberErr.set(H137);
			}
		}
	}

	

	protected void checkForErrors2080()
		{
			if (isNE(sv.errorIndicators, SPACES)) {
				wsspcomn.edterror.set("Y");
			}
			if (isEQ(scrnparams.statuz, "CALC")) {
				wsspcomn.edterror.set("Y");
			}  
			/*EXIT*/
			
		}
	
	protected void auditCheck10000()
	{
		/*    The user who provides approval cannot be the user
		 *  who created and/or modified the claim payment    */
		/*The user who provides authorization cannot be the user
		 *  who created  the claim payment or approved the claim payment*/
		if (isEQ(sv.action, "D") &&
			(isEQ(longUserID, pymtpf.getCreateuser().trim()) || isEQ(longUserID, pymtpf.getAppruser().trim())
					||isEQ(longUserID, pymtpf.getModuser().trim()))) {
				sv.actionErr.set(JL79);
		}else if(isEQ(sv.action, "C") && 
			(isEQ(longUserID, pymtpf.getCreateuser().trim()) || isEQ(longUserID, pymtpf.getAuthuser().trim())
					||isEQ(longUserID, pymtpf.getModuser().trim()))) {
				sv.actionErr.set(JL78);
		}
	}
	
	@Override
	protected void update3000()
		{
		updateWssp3010();
		}	
	
	protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaBatckey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatckey);
		wsspcomn.submenu.set(wsaaProg);
		wsspcomn.confirmationKey.set(sv.paymentNum.trim());

	
		if (isEQ(scrnparams.statuz, "BACH")) {
			return;
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);	
	}


	@Override
	protected void whereNext4000()
		{
		if (isEQ(wsspcomn.sbmaction, "E") || (isEQ(wsspcomn.sbmaction, "B"))) {
			wsspcomn.bankcode.set(pymtpf.getReqnbcde());
			
		}
		else {
			wsspcomn.bankcode.set(sv.bankcode);
		}
		wsspcomn.wsaaclaimno.set(sv.claimnmber.trim());
		wsspcomn.confirmationKey.set(sv.paymentNum.trim());
		wsspcomn.flag.set("F");
		/*NEXT-PROGRAM*/
 		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
		}

}
