package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:53
 * Description:
 * Copybook name: DEATHREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Deathrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData deathRec = new FixedLengthStringData(getDeathRecSize());
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(deathRec, 0);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(deathRec, 1);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(deathRec, 9);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(deathRec, 11);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(deathRec, 13);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(deathRec, 15);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(deathRec, 17);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(deathRec, 21);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(deathRec, 29);
  	public PackedDecimalData estimatedVal = new PackedDecimalData(17, 2).isAPartOf(deathRec, 30);
  	public PackedDecimalData actualVal = new PackedDecimalData(17, 2).isAPartOf(deathRec, 39);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(deathRec, 48);
  	public FixedLengthStringData element = new FixedLengthStringData(4).isAPartOf(deathRec, 51);
  	public FixedLengthStringData valueType = new FixedLengthStringData(1).isAPartOf(deathRec, 55);
  	public FixedLengthStringData description = new FixedLengthStringData(30).isAPartOf(deathRec, 56);
  	public FixedLengthStringData fieldType = new FixedLengthStringData(1).isAPartOf(deathRec, 86);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(deathRec, 87);
  	public FixedLengthStringData endf = new FixedLengthStringData(1).isAPartOf(deathRec, 91);
  	public FixedLengthStringData batckey = new FixedLengthStringData(64).isAPartOf(deathRec, 92);
  	public FixedLengthStringData virtualFundStore = new FixedLengthStringData(4).isAPartOf(deathRec, 156);
  	public FixedLengthStringData unitTypeStore = new FixedLengthStringData(1).isAPartOf(deathRec, 160);
  	public FixedLengthStringData processInd = new FixedLengthStringData(1).isAPartOf(deathRec, 161);


	public void initialize() {
		COBOLFunctions.initialize(deathRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		deathRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

	public int getDeathRecSize()
	{
		return 162;
	}
	
}