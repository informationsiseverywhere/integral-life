package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ClmhclmTableDAM.java
 * Date: Sun, 30 Aug 2009 03:33:41
 * Class transformed from CLMHCLM.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ClmhclmTableDAM extends ClmhpfTableDAM {

	public ClmhclmTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CLMHCLM");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "TRANNO, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "DTOFDEATH, " +
		            "EFFDATE, " +
		            "CAUSEOFDTH, " +
		            "FUPCDE, " +
		            "CURRCD, " +
		            "POLICYLOAN, " +
		            "TDBTAMT, " +
		            "OTHERADJST, " +
		            "REASONCD, " +
		            "RESNDESC, " +
		            "INTDAYS, " +
		            "INTEREST, " +
		            "OFCHARGE, " +
		            "CNTTYPE, " +
		            "ZRCSHAMT, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "VALIDFLAG, " +
		            "INTERESTRATE, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               tranno,
                               life,
                               jlife,
                               dtofdeath,
                               effdate,
                               causeofdth,
                               fupcode,
                               currcd,
                               policyloan,
                               tdbtamt,
                               otheradjst,
                               reasoncd,
                               resndesc,
                               intdays,
                               interest,
                               ofcharge,
                               cnttype,
                               zrcshamt,
                               userProfile,
                               jobName,
                               datime,
                               validflag,
                               interestrate,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(247);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(191);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getTranno().toInternal()
					+ getLife().toInternal()
					+ getJlife().toInternal()
					+ getDtofdeath().toInternal()
					+ getEffdate().toInternal()
					+ getCauseofdth().toInternal()
					+ getFupcode().toInternal()
					+ getCurrcd().toInternal()
					+ getPolicyloan().toInternal()
					+ getTdbtamt().toInternal()
					+ getOtheradjst().toInternal()
					+ getReasoncd().toInternal()
					+ getResndesc().toInternal()
					+ getIntdays().toInternal()
					+ getInterest().toInternal()
					+ getOfcharge().toInternal()
					+ getCnttype().toInternal()
					+ getZrcshamt().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getValidflag().toInternal()
					+ getInterestrate().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, dtofdeath);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, causeofdth);
			what = ExternalData.chop(what, fupcode);
			what = ExternalData.chop(what, currcd);
			what = ExternalData.chop(what, policyloan);
			what = ExternalData.chop(what, tdbtamt);
			what = ExternalData.chop(what, otheradjst);
			what = ExternalData.chop(what, reasoncd);
			what = ExternalData.chop(what, resndesc);
			what = ExternalData.chop(what, intdays);
			what = ExternalData.chop(what, interest);
			what = ExternalData.chop(what, ofcharge);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, zrcshamt);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			what = ExternalData.chop(what, validflag);	
			what = ExternalData.chop(what, interestrate);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public PackedDecimalData getDtofdeath() {
		return dtofdeath;
	}
	public void setDtofdeath(Object what) {
		setDtofdeath(what, false);
	}
	public void setDtofdeath(Object what, boolean rounded) {
		if (rounded)
			dtofdeath.setRounded(what);
		else
			dtofdeath.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getCauseofdth() {
		return causeofdth;
	}
	public void setCauseofdth(Object what) {
		causeofdth.set(what);
	}	
	public FixedLengthStringData getFupcode() {
		return fupcode;
	}
	public void setFupcode(Object what) {
		fupcode.set(what);
	}	
	public FixedLengthStringData getCurrcd() {
		return currcd;
	}
	public void setCurrcd(Object what) {
		currcd.set(what);
	}	
	public PackedDecimalData getPolicyloan() {
		return policyloan;
	}
	public void setPolicyloan(Object what) {
		setPolicyloan(what, false);
	}
	public void setPolicyloan(Object what, boolean rounded) {
		if (rounded)
			policyloan.setRounded(what);
		else
			policyloan.set(what);
	}	
	public PackedDecimalData getTdbtamt() {
		return tdbtamt;
	}
	public void setTdbtamt(Object what) {
		setTdbtamt(what, false);
	}
	public void setTdbtamt(Object what, boolean rounded) {
		if (rounded)
			tdbtamt.setRounded(what);
		else
			tdbtamt.set(what);
	}	
	public PackedDecimalData getOtheradjst() {
		return otheradjst;
	}
	public void setOtheradjst(Object what) {
		setOtheradjst(what, false);
	}
	public void setOtheradjst(Object what, boolean rounded) {
		if (rounded)
			otheradjst.setRounded(what);
		else
			otheradjst.set(what);
	}	
	public FixedLengthStringData getReasoncd() {
		return reasoncd;
	}
	public void setReasoncd(Object what) {
		reasoncd.set(what);
	}	
	public FixedLengthStringData getResndesc() {
		return resndesc;
	}
	public void setResndesc(Object what) {
		resndesc.set(what);
	}	
	public PackedDecimalData getIntdays() {
		return intdays;
	}
	public void setIntdays(Object what) {
		setIntdays(what, false);
	}
	public void setIntdays(Object what, boolean rounded) {
		if (rounded)
			intdays.setRounded(what);
		else
			intdays.set(what);
	}
	
	public PackedDecimalData getInterestrate() {
		return interestrate;
	}
	public void setInterestrate(Object what) {
		setInterestrate(what, false);
	}
	public void setInterestrate(Object what, boolean rounded) {
		if (rounded)
			interestrate.setRounded(what);
		else
			interestrate.set(what);
	}
	
	
	public PackedDecimalData getInterest() {
		return interest;
	}
	public void setInterest(Object what) {
		setInterest(what, false);
	}
	public void setInterest(Object what, boolean rounded) {
		if (rounded)
			interest.setRounded(what);
		else
			interest.set(what);
	}	
	public PackedDecimalData getOfcharge() {
		return ofcharge;
	}
	public void setOfcharge(Object what) {
		setOfcharge(what, false);
	}
	public void setOfcharge(Object what, boolean rounded) {
		if (rounded)
			ofcharge.setRounded(what);
		else
			ofcharge.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public PackedDecimalData getZrcshamt() {
		return zrcshamt;
	}
	public void setZrcshamt(Object what) {
		setZrcshamt(what, false);
	}
	public void setZrcshamt(Object what, boolean rounded) {
		if (rounded)
			zrcshamt.setRounded(what);
		else
			zrcshamt.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		tranno.clear();
		life.clear();
		jlife.clear();
		dtofdeath.clear();
		effdate.clear();
		causeofdth.clear();
		fupcode.clear();
		currcd.clear();
		policyloan.clear();
		tdbtamt.clear();
		otheradjst.clear();
		reasoncd.clear();
		resndesc.clear();
		intdays.clear();
		interest.clear();
		ofcharge.clear();
		cnttype.clear();
		zrcshamt.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
		validflag.clear();	
		interestrate.clear();
	}


}