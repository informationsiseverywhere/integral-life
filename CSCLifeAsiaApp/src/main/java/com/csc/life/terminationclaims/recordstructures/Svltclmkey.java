package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:58
 * Description:
 * Copybook name: SVLTCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Svltclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData svltclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData svltclmKey = new FixedLengthStringData(64).isAPartOf(svltclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData svltclmChdrcoy = new FixedLengthStringData(1).isAPartOf(svltclmKey, 0);
  	public FixedLengthStringData svltclmChdrnum = new FixedLengthStringData(8).isAPartOf(svltclmKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(svltclmKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(svltclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		svltclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}