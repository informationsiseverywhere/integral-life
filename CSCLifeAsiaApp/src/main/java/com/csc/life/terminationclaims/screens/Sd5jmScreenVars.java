package com.csc.life.terminationclaims.screens;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

/**
 * Screen variables for Sd5jm
 * @version 1.0 generated on 05/12/18
 * @author qzhang52
 */
public class Sd5jmScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(17);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	public FixedLengthStringData confirm = DD.confirm.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(4).isAPartOf(dataArea, 1);
	public FixedLengthStringData confirmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 5);
	public FixedLengthStringData[] confirmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);

	public LongData Sd5jmscreenWritten = new LongData(0);
	public LongData Sd5jmwindowWritten = new LongData(0);
	public LongData Sd5jmprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5jmScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {confirm};
		screenOutFields = new BaseData[][] {confirmOut};
		screenErrFields = new BaseData[] {confirmErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5jmscreen.class;
		protectRecord = Sd5jmprotect.class;
	}

}
