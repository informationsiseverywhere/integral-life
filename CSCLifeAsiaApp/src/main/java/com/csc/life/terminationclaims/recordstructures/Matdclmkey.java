package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:40
 * Description:
 * Copybook name: MATDCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Matdclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData matdclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData matdclmKey = new FixedLengthStringData(64).isAPartOf(matdclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData matdclmChdrcoy = new FixedLengthStringData(1).isAPartOf(matdclmKey, 0);
  	public FixedLengthStringData matdclmChdrnum = new FixedLengthStringData(8).isAPartOf(matdclmKey, 1);
  	public PackedDecimalData matdclmTranno = new PackedDecimalData(5, 0).isAPartOf(matdclmKey, 9);
  	public PackedDecimalData matdclmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(matdclmKey, 12);
  	public FixedLengthStringData matdclmLife = new FixedLengthStringData(2).isAPartOf(matdclmKey, 15);
  	public FixedLengthStringData matdclmCoverage = new FixedLengthStringData(2).isAPartOf(matdclmKey, 17);
  	public FixedLengthStringData matdclmRider = new FixedLengthStringData(2).isAPartOf(matdclmKey, 19);
  	public FixedLengthStringData matdclmLiencd = new FixedLengthStringData(2).isAPartOf(matdclmKey, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(matdclmKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(matdclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		matdclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}