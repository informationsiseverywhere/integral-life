package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:56
 * Description:
 * Copybook name: PR676CPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pr676cpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(9);
  	public FixedLengthStringData mortcls = new FixedLengthStringData(1).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData livesno = new FixedLengthStringData(1).isAPartOf(parmRecord, 1);
  	public FixedLengthStringData waiverprem = new FixedLengthStringData(1).isAPartOf(parmRecord, 2);
  	public FixedLengthStringData waiverCrtable = new FixedLengthStringData(4).isAPartOf(parmRecord, 3);
  	public FixedLengthStringData benpln = new FixedLengthStringData(2).isAPartOf(parmRecord, 7);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}