package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Td5j2rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData td5j2Rec = new FixedLengthStringData(495);
  	public FixedLengthStringData shortTerm = new FixedLengthStringData(1).isAPartOf(td5j2Rec, 0);
  	public FixedLengthStringData longTerm = new FixedLengthStringData(1).isAPartOf(td5j2Rec, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(493).isAPartOf(td5j2Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(td5j2Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			td5j2Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}