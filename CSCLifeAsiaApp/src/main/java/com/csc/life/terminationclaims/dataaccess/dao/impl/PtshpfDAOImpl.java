package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.csc.life.terminationclaims.dataaccess.dao.PtshpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Ptshpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class PtshpfDAOImpl extends BaseDAOImpl<Ptshpf> implements PtshpfDAO {

	@Override
	public List<Ptshpf> serachPtshRecord(Ptshpf ptshObj) {
		StringBuilder sqlPtshSelect1 = new StringBuilder(
                "SELECT CHDRCOY, CHDRNUM, TRANNO, LIFE, JLIFE, EFFDATE, CURRCD, TOTALAMT, PRCNT, PLNSFX, CNTTYPE");
        sqlPtshSelect1
                .append(" FROM VM1DTA.PTSHPF WHERE CHDRCOY= "+ptshObj.getChdrcoy()+" AND CHDRNUM = ?");
        sqlPtshSelect1
        .append(" AND TRANNO >= "+ptshObj.getTranno()+" ");
        sqlPtshSelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, TRANNO ASC, UNIQUE_NUMBER DESC ");
        PreparedStatement psPtshSelect = getPrepareStatement(sqlPtshSelect1.toString());
        ResultSet sqlPtshpf1rs = null;
        List<Ptshpf> ptshpfList = new ArrayList<Ptshpf>();
        try {
        	psPtshSelect.setString(1, ptshObj.getChdrnum());
        	sqlPtshpf1rs = executeQuery(psPtshSelect);
             while (sqlPtshpf1rs.next()) {
            	 Ptshpf ptshpf=new Ptshpf();
            	 ptshpf.setChdrcoy(sqlPtshpf1rs.getString(1));
            	 ptshpf.setChdrnum(sqlPtshpf1rs.getString(2));
            	 ptshpf.setTranno(sqlPtshpf1rs.getInt(3));
            	 ptshpf.setLife(sqlPtshpf1rs.getString(4));
            	 ptshpf.setJlife(sqlPtshpf1rs.getString(5));
            	 ptshpf.setEffdate(sqlPtshpf1rs.getLong(6));
            	 ptshpf.setCurrcd(sqlPtshpf1rs.getString(7));
            	 ptshpf.setTotalamt(sqlPtshpf1rs.getBigDecimal(8));
            	 ptshpf.setPrcnt(sqlPtshpf1rs.getBigDecimal(9));
            	 ptshpf.setPlanSuffix(sqlPtshpf1rs.getBigDecimal(10));
            	 ptshpf.setCnttype(sqlPtshpf1rs.getString(11));
            	 ptshpfList.add(ptshpf);
             }
        }catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(psPtshSelect, sqlPtshpf1rs);
        }
        return ptshpfList;
	}
	
	public void insertPtshRecord(Ptshpf ptshpf) {
		
		StringBuilder sb = new StringBuilder("insert into vm1dta.ptshpf(CHDRCOY,CHDRNUM,TRANNO,LIFE,JLIFE,EFFDATE,CURRCD,TOTALAMT,PRCNT,PLNSFX,CNTTYPE,USRPRF,JOBNM,DATIME)");
		sb.append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	
			ps.setString(1, ptshpf.getChdrcoy());
			ps.setString(2, ptshpf.getChdrnum());
			ps.setInt(3, ptshpf.getTranno());
			ps.setString(4, ptshpf.getLife());
			ps.setString(5, ptshpf.getJlife());
			ps.setLong(6, ptshpf.getEffdate());
			ps.setString(7, ptshpf.getCurrcd());
			ps.setBigDecimal(8, ptshpf.getTotalamt());
			ps.setBigDecimal(9, ptshpf.getPrcnt());
			ps.setBigDecimal(10, ptshpf.getPlanSuffix());
			ps.setString(11, ptshpf.getCnttype());
			ps.setString(12, getUsrprf());
			ps.setString(13, getJobnm());
			ps.setTimestamp(14, new Timestamp(System.currentTimeMillis()));
				
            ps.executeUpdate();
		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
	}

}
