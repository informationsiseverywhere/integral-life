package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6679
 * @version 1.0 generated on 30/08/09 06:57
 * @author Quipoz
 */
public class S6679ScreenVars extends SmartVarModel { 


	//public FixedLengthStringData dataArea = new FixedLengthStringData(1229);
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	//public FixedLengthStringData dataFields = new FixedLengthStringData(509).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public ZonedDecimalData anvdate = DD.anvdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData aprvdate = DD.aprvdate.copyToZonedDecimal().isAPartOf(dataFields,8);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,16);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData claimcur = DD.claimcur.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData claimevd = DD.claimevd.copy().isAPartOf(dataFields,35);
	public FixedLengthStringData clmcurdsc = DD.clmcurdsc.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData clmdesc = DD.clmdesc.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData cltype = DD.cltype.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,95);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,106);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(dataFields,110);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,118);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,148);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,151);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,161);
	public FixedLengthStringData destkey = DD.destkey.copy().isAPartOf(dataFields,162);
	public ZonedDecimalData finalPaydate = DD.epaydate.copyToZonedDecimal().isAPartOf(dataFields,172);
	public ZonedDecimalData firstPaydate = DD.fpaydate.copyToZonedDecimal().isAPartOf(dataFields,180);
	public FixedLengthStringData frqdesc = DD.frqdesc.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,198);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,199);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,207);
	public ZonedDecimalData lastPaydate = DD.lpaydate.copyToZonedDecimal().isAPartOf(dataFields,254);
	public ZonedDecimalData nextPaydate = DD.npaydate.copyToZonedDecimal().isAPartOf(dataFields,262);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,270);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,278);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(dataFields,325);
	public FixedLengthStringData payenme = DD.payenme.copy().isAPartOf(dataFields,333);
	public ZonedDecimalData prcnt = DD.prcnt.copyToZonedDecimal().isAPartOf(dataFields,380);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,385);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,395);
	public ZonedDecimalData pymt = DD.pymt.copyToZonedDecimal().isAPartOf(dataFields,403);
	public FixedLengthStringData regpayfreq = DD.regpayfreq.copy().isAPartOf(dataFields,420);
	public ZonedDecimalData revdte = DD.revdte.copyToZonedDecimal().isAPartOf(dataFields,422);
	public FixedLengthStringData rgpymop = DD.rgpymop.copy().isAPartOf(dataFields,430);
	public ZonedDecimalData rgpynum = DD.rgpynum.copyToZonedDecimal().isAPartOf(dataFields,431);
	public FixedLengthStringData rgpyshort = DD.rgpyshort.copy().isAPartOf(dataFields,436);
	public FixedLengthStringData rgpystat = DD.rgpystat.copy().isAPartOf(dataFields,446);
	public FixedLengthStringData rgpytypesd = DD.rgpytypesd.copy().isAPartOf(dataFields,448);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,458);
	public FixedLengthStringData statdsc = DD.statdsc.copy().isAPartOf(dataFields,468);
	public ZonedDecimalData totamnt = DD.totamnt.copyToZonedDecimal().isAPartOf(dataFields,478);
	public ZonedDecimalData zrsumin = DD.zrsumin.copyToZonedDecimal().isAPartOf(dataFields,496);

	// CML-009
	public ZonedDecimalData adjustamt = DD.adjamt.copyToZonedDecimal().isAPartOf(dataFields,509);
	public ZonedDecimalData netclaimamt = DD.netamt.copyToZonedDecimal().isAPartOf(dataFields,521);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,538);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields, 542);
	public ZonedDecimalData pymtAdj = DD.pymt.copyToZonedDecimal().isAPartOf(dataFields, 592);
	
//    public ZonedDecimalData netclaimamt = DD.netamt.copyToZonedDecimal().isAPartOf(dataFields,653);
//	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,670);
//
//	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(
//			dataFields, 674);
//	public ZonedDecimalData pymtAdj = DD.pymt.copyToZonedDecimal().isAPartOf(
//			dataFields, 741);
	/*
	 * CML004
	 */
	public ZonedDecimalData itstdays = DD.itstdays.copyToZonedDecimal().isAPartOf(dataFields, 609);
	public ZonedDecimalData itstrate = DD.intrat.copyToZonedDecimal().isAPartOf(dataFields, 612);
	public ZonedDecimalData itstamt = DD.itstamt.copyToZonedDecimal().isAPartOf(dataFields, 620);
	
	public ZonedDecimalData filll = DD.facevalue.copyToZonedDecimal().isAPartOf(dataFields, 637); //5;//IGI
	public FixedLengthStringData ovrpermently = DD.ovrpermently.copy().isAPartOf(dataFields, 642); //1;//IGI
	public ZonedDecimalData ovrsumin = DD.ovrsumin.copyToZonedDecimal().isAPartOf(dataFields, 643); //17;//IGI
	
	// clm008
	public FixedLengthStringData claimnumber = DD.claimnumber.copy().isAPartOf(dataFields,660); 
	public FixedLengthStringData aacct = DD.aacct.copy().isAPartOf(dataFields,669); 
	public FixedLengthStringData claimnotes = DD.claimnotes.copy().isAPartOf(dataFields,683);
	public FixedLengthStringData investres = DD.investres.copy().isAPartOf(dataFields,684);
	

	

	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(180).isAPartOf(dataArea, 509);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());

	public FixedLengthStringData anvdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData aprvdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData claimcurErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData claimevdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData clmcurdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData clmdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData destkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData epaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData fpaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData frqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData lpaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData npaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData paycltErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData payenmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData prcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData pymtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData regpayfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData revdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData rgpymopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData rgpynumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData rgpyshortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData rgpystatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData rgpytypesdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData statdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData totamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData zrsuminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);

	//TODO adding new ERR fields
	public FixedLengthStringData adjustamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData netClaimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4)
			.isAPartOf(errorIndicators, 192);
	public FixedLengthStringData pymtAdjErr = new FixedLengthStringData(4)
			.isAPartOf(errorIndicators, 196);
	public FixedLengthStringData filllErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,200); //IGI
	public FixedLengthStringData ovrpermentlyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,204); //IGI
	public FixedLengthStringData ovrsuminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators,208); //IGI
	
	// clm008
	public FixedLengthStringData claimnumberErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData aacctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData claimnotesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData investresErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	
	
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(540).isAPartOf(dataArea, 689);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());

	public FixedLengthStringData[] anvdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] aprvdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] claimcurOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] claimevdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] clmcurdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] clmdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] destkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] epaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] fpaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] frqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] lpaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] npaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] paycltOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] payenmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] prcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] pymtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] regpayfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] revdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] rgpymopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] rgpynumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] rgpyshortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] rgpystatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] rgpytypesdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] statdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] totamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] zrsuminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	// CML-009
	public FixedLengthStringData[] adjustamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] netclaimamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1,
			outputIndicators, 576);
	public FixedLengthStringData[] pymtAdjOut = FLSArrayPartOfStructure(12, 1,
			outputIndicators, 588);
	/*
	 * CML004
	 */
	public FixedLengthStringData[] itstdaysOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[] itstrateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[] itstamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[] filllOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636); //IGI
	public FixedLengthStringData[] ovrpermentlyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648); //IGI
	public FixedLengthStringData[] ovrsuminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660); //IGI
	
	// clm008
	public FixedLengthStringData[] claimnumberOut = FLSArrayPartOfStructure(12, 1,outputIndicators, 672);
	public FixedLengthStringData[] aacctOut = FLSArrayPartOfStructure(12, 1,outputIndicators, 684);
	public FixedLengthStringData[] claimnotesOut = FLSArrayPartOfStructure(12, 1,outputIndicators, 696);
	public FixedLengthStringData[] investresOut = FLSArrayPartOfStructure(12, 1,outputIndicators, 708);
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData anvdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData aprvdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData finalPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData firstPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lastPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData revdteDisp = new FixedLengthStringData(10);

	public LongData S6679screenWritten = new LongData(0);
	public LongData S6679protectWritten = new LongData(0);

	public FixedLengthStringData cmoth002flag = new FixedLengthStringData(1).init("N"); //CLM004
	public FixedLengthStringData cmoth008flag = new FixedLengthStringData(1); //CLM008
	
	
	public boolean hasSubfile() {
		return false;
	}


	public S6679ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(aprvdateOut,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupflgOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49

		// CML-009
		fieldIndMap.put(reasoncdOut, new String[] { "07", "49", "-07", null,
				null, null, null, null, null, null, null, null });
		fieldIndMap.put(netclaimamtOut, new String[] { "09", "50", "-09", null,
				null, null, null, null, null, null, null, null });
		fieldIndMap.put(adjustamtOut, new String[] { "08", "69", "69", null,
				null, null, null, null, null, null, null, null });
		fieldIndMap.put(resndescOut, new String[] { "10", "70", "70", null,
				null, null, null, null, null, null, null, null });
		fieldIndMap.put(pymtAdjOut, new String[] { "44", "67", "-44", null,
				null, null, null, null, null, null, null, null });

		/*screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, lifcnum, linsname, cownnum, ownername, occdate, rstate, pstate, ptdate, btdate, currcd, currds, rgpynum, rgpytypesd, rgpystat, statdsc, cltype, clmdesc, claimevd, rgpymop, rgpyshort, regpayfreq, frqdesc, payclt, payenme, prcnt, destkey, pymt, totamnt, claimcur, clmcurdsc, aprvdate, crtdate, revdte, firstPaydate, lastPaydate, nextPaydate, anvdate, finalPaydate, fupflg, ddind, crtable, zrsumin};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, lifcnumOut, linsnameOut, cownnumOut, ownernameOut, occdateOut, rstateOut, pstateOut, ptdateOut, btdateOut, currcdOut, currdsOut, rgpynumOut, rgpytypesdOut, rgpystatOut, statdscOut, cltypeOut, clmdescOut, claimevdOut, rgpymopOut, rgpyshortOut, regpayfreqOut, frqdescOut, paycltOut, payenmeOut, prcntOut, destkeyOut, pymtOut, totamntOut, claimcurOut, clmcurdscOut, aprvdateOut, crtdateOut, revdteOut, fpaydateOut, lpaydateOut, npaydateOut, anvdateOut, epaydateOut, fupflgOut, ddindOut, crtableOut, zrsuminOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, lifcnumErr, linsnameErr, cownnumErr, ownernameErr, occdateErr, rstateErr, pstateErr, ptdateErr, btdateErr, currcdErr, currdsErr, rgpynumErr, rgpytypesdErr, rgpystatErr, statdscErr, cltypeErr, clmdescErr, claimevdErr, rgpymopErr, rgpyshortErr, regpayfreqErr, frqdescErr, paycltErr, payenmeErr, prcntErr, destkeyErr, pymtErr, totamntErr, claimcurErr, clmcurdscErr, aprvdateErr, crtdateErr, revdteErr, fpaydateErr, lpaydateErr, npaydateErr, anvdateErr, epaydateErr, fupflgErr, ddindErr, crtableErr, zrsuminErr};
	     */	

		screenDateFields = new BaseData[] {occdate, ptdate, btdate, aprvdate, crtdate, revdte, firstPaydate, lastPaydate, nextPaydate, anvdate, finalPaydate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, aprvdateErr, crtdateErr, revdteErr, fpaydateErr, lpaydateErr, npaydateErr, anvdateErr, epaydateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, aprvdateDisp, crtdateDisp, revdteDisp, firstPaydateDisp, lastPaydateDisp, nextPaydateDisp, anvdateDisp, finalPaydateDisp};
		screenFields =getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6679screen.class;
		protectRecord = S6679protect.class;
	}
	public int getDataAreaSize() {
		return 685+228+720;
	}
	public int getDataFieldsSize(){
		return 685;
	}
	public int getErrorIndicatorSize(){
		return 228;
	}
	public int getOutputFieldSize(){
		return 720;
	}
	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, cnttype, ctypedes, lifcnum, linsname, cownnum, ownername, occdate, rstate, pstate, ptdate, btdate, currcd, currds, rgpynum, rgpytypesd, rgpystat, statdsc, cltype, clmdesc, claimevd, rgpymop, rgpyshort, regpayfreq, frqdesc, payclt, payenme, prcnt, destkey, pymt, totamnt, claimcur, clmcurdsc, aprvdate, crtdate, revdte, firstPaydate, lastPaydate, nextPaydate, anvdate, finalPaydate, fupflg, ddind, crtable, zrsumin, adjustamt, netclaimamt, reasoncd, resndesc, pymtAdj, itstdays, itstrate, itstamt, filll, ovrpermently, ovrsumin ,claimnumber, aacct,claimnotes,investres };     
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, lifcnumOut, linsnameOut, cownnumOut, ownernameOut, occdateOut, rstateOut, pstateOut, ptdateOut, btdateOut, currcdOut, currdsOut, rgpynumOut, rgpytypesdOut, rgpystatOut, statdscOut, cltypeOut, clmdescOut, claimevdOut, rgpymopOut, rgpyshortOut, regpayfreqOut, frqdescOut, paycltOut, payenmeOut, prcntOut, destkeyOut, pymtOut, totamntOut, claimcurOut, clmcurdscOut, aprvdateOut, crtdateOut, revdteOut, fpaydateOut, lpaydateOut, npaydateOut, anvdateOut, epaydateOut, fupflgOut, ddindOut, crtableOut, zrsuminOut, adjustamtOut, netclaimamtOut,	reasoncdOut, resndescOut, pymtAdjOut, itstdaysOut, itstrateOut, itstamtOut, filllOut, ovrpermentlyOut, ovrsuminOut,claimnumberOut, aacctOut,claimnotesOut,investresOut };     
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, lifcnumErr, linsnameErr, cownnumErr, ownernameErr, occdateErr, rstateErr, pstateErr, ptdateErr, btdateErr, currcdErr, currdsErr, rgpynumErr, rgpytypesdErr, rgpystatErr, statdscErr, cltypeErr, clmdescErr, claimevdErr, rgpymopErr, rgpyshortErr, regpayfreqErr, frqdescErr, paycltErr, payenmeErr, prcntErr, destkeyErr, pymtErr, totamntErr, claimcurErr, clmcurdscErr, aprvdateErr, crtdateErr, revdteErr, fpaydateErr, lpaydateErr, npaydateErr, anvdateErr, epaydateErr, fupflgErr, ddindErr, crtableErr, zrsuminErr, adjustamtErr, netClaimErr, reasoncdErr, resndescErr, pymtErr, filllErr, ovrpermentlyErr, ovrsuminErr ,claimnumberErr, aacctErr,claimnotesErr,investresErr };    
		
	}

}
