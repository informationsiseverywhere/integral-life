package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5533rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Zprmdm1 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "ZPRMDM1";

	private FixedLengthStringData wsaaT5533Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5533Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5533Item, 0);
	private FixedLengthStringData wsaaT5533Currcode = new FixedLengthStringData(4).isAPartOf(wsaaT5533Item, 4);
	private PackedDecimalData wsaaAge00 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaAge01 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaDob00 = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaDob01 = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaYoungerLife = new PackedDecimalData(3, 0).init(ZERO);
	private ZonedDecimalData wsaaRndfact = new ZonedDecimalData(6, 0);
	private PackedDecimalData wsaaTotPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFactor = new PackedDecimalData(7, 4);
	
	private ZonedDecimalData wsaaDateToUse = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
		/* ERRORS */
	private String g070 = "G070";
		/* TABLES */
	private String t5533 = "T5533";
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Life and joint life details - new busine*/
	private Premiumrec premiumrec = new Premiumrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5533rec t5533rec = new T5533rec();
	
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit260, 
		exit520, 
		exit570, 
		para660, 
		exit9020
	}

	public Zprmdm1() {
		super();
	}

public void mainline(Object... parmArray)
	{
		premiumrec.premiumRec = convertAndSetParam(premiumrec.premiumRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		/*INIT*/
		premiumrec.statuz.set("****");
		syserrrec.subrname.set(wsaaSubr);
		main100();
		/*EXIT*/
		exitProgram();
	}

protected void main100()
	{
		para100();
	}

protected void para100()
	{
		initialize200();
		if (isEQ(premiumrec.statuz,"****")) {
			readTablT5533250();
		}
	}

protected void initialize200()
	{
		/*PARA*/
		wsaaYoungerLife.set(ZERO);
		wsaaFactor.set(ZERO);
		wsaaTotPremium.set(ZERO);
		wsaaAge00.set(ZERO);
		wsaaAge01.set(ZERO);
		wsaaDob00.set(ZERO);
		wsaaDob01.set(ZERO);
		wsaaDateToUse.set(ZERO);
		wsaaRndfact.set(ZERO);
		/*EXIT*/
	}

protected void readTablT5533250()
	{
		try {
			para250();
		}
		catch (GOTOException e){
		}
	}

protected void para250()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(premiumrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5533);
		wsaaT5533Crtable.set(premiumrec.crtable);
		wsaaT5533Currcode.set(premiumrec.currcode);
		itdmIO.setItemitem(wsaaT5533Item);
		itdmIO.setItmfrm(premiumrec.ratingdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),"****")
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaT5533Item,itdmIO.getItemitem())
		|| isNE(premiumrec.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5533)
		|| isEQ(itdmIO.getStatuz(),"ENDP")) {
			return;
		}
		else {
			t5533rec.t5533Rec.set(itdmIO.getGenarea());
			wsaaRndfact.set(t5533rec.rndfact);
			int wsaaSub=1;
			while (wsaaSub <= 8) {
					if( isEQ(premiumrec.billfreq,t5533rec.frequency[wsaaSub])){
						if ((isLT(premiumrec.calcPrem,t5533rec.cmin[wsaaSub])) || (isGT(premiumrec.calcPrem,t5533rec.cmax[wsaaSub]))) {
							premiumrec.statuz.set(g070);
						}
				}
					wsaaSub++;	
			}
			if (isGT(wsaaSub,8)) {
				return;
			}
		}
		
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		premiumrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}

}
