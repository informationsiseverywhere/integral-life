package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ClmhpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:28
 * Class transformed from CLMHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ClmhpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 365;
	public FixedLengthStringData clmhrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData clmhpfRecord = clmhrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(clmhrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(clmhrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(clmhrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(clmhrec);
	public FixedLengthStringData recode = DD.recode.copy().isAPartOf(clmhrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(clmhrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(clmhrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(clmhrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(clmhrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(clmhrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(clmhrec);
	public FixedLengthStringData clamstat = DD.clamstat.copy().isAPartOf(clmhrec);
	public FixedLengthStringData clamtyp = DD.clamtyp.copy().isAPartOf(clmhrec);
	public PackedDecimalData dteclam = DD.dteclam.copy().isAPartOf(clmhrec);
	public PackedDecimalData dtereg = DD.dtereg.copy().isAPartOf(clmhrec);
	public PackedDecimalData dteappr = DD.dteappr.copy().isAPartOf(clmhrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(clmhrec);
	public PackedDecimalData dtestschg = DD.dtestschg.copy().isAPartOf(clmhrec);
	public FixedLengthStringData ctrmflag = DD.ctrmflag.copy().isAPartOf(clmhrec);
	public FixedLengthStringData chgflag = DD.chgflag.copy().isAPartOf(clmhrec);
	public FixedLengthStringData aplsupr = DD.aplsupr.copy().isAPartOf(clmhrec);
	public PackedDecimalData aplspfrom = DD.aplspfrom.copy().isAPartOf(clmhrec);
	public PackedDecimalData aplspto = DD.aplspto.copy().isAPartOf(clmhrec);
	public FixedLengthStringData billsupr = DD.billsupr.copy().isAPartOf(clmhrec);
	public PackedDecimalData billspfrom = DD.billspfrom.copy().isAPartOf(clmhrec);
	public PackedDecimalData billspto = DD.billspto.copy().isAPartOf(clmhrec);
	public FixedLengthStringData plsupr = DD.plsupr.copy().isAPartOf(clmhrec);
	public PackedDecimalData plspfrom = DD.plspfrom.copy().isAPartOf(clmhrec);
	public PackedDecimalData plspto = DD.plspto.copy().isAPartOf(clmhrec);
	public PackedDecimalData intdays = DD.intdays.copy().isAPartOf(clmhrec);
	public PackedDecimalData premsusp = DD.premsusp.copy().isAPartOf(clmhrec);
	public PackedDecimalData premadvbal = DD.premadvbal.copy().isAPartOf(clmhrec);
	public PackedDecimalData premcurr = DD.premadvint.copy().isAPartOf(clmhrec);
	public PackedDecimalData bonusterm = DD.bonusterm.copy().isAPartOf(clmhrec);
	public PackedDecimalData aplint = DD.aplint.copy().isAPartOf(clmhrec);
	public PackedDecimalData plint = DD.plint.copy().isAPartOf(clmhrec);
	public PackedDecimalData aplamt = DD.aplamt.copy().isAPartOf(clmhrec);
	public PackedDecimalData plamt = DD.plamt.copy().isAPartOf(clmhrec);
	public PackedDecimalData interest = DD.interest.copy().isAPartOf(clmhrec);
	public PackedDecimalData proceeds = DD.proceeds.copy().isAPartOf(clmhrec);
	public PackedDecimalData ofcharge = DD.ofcharge.copy().isAPartOf(clmhrec);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(clmhrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(clmhrec);
	public FixedLengthStringData billchnl = DD.billchnl.copy().isAPartOf(clmhrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(clmhrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(clmhrec);
	public PackedDecimalData dtofdeath = DD.dtofdeath.copy().isAPartOf(clmhrec);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(clmhrec);
	public PackedDecimalData otheradjst = DD.otheradjst.copy().isAPartOf(clmhrec);
	public PackedDecimalData policyloan = DD.policyloan.copy().isAPartOf(clmhrec);
	public PackedDecimalData tdbtamt = DD.tdbtamt.copy().isAPartOf(clmhrec);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(clmhrec);
	public FixedLengthStringData causeofdth = DD.causeofdth.copy().isAPartOf(clmhrec);
	public FixedLengthStringData fupcode = DD.fupcde.copy().isAPartOf(clmhrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(clmhrec);
	public PackedDecimalData zrcshamt = DD.zrcshamt.copy().isAPartOf(clmhrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(clmhrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(clmhrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(clmhrec);
	public PackedDecimalData interestrate = DD.intrat.copy().isAPartOf(clmhrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ClmhpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ClmhpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ClmhpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ClmhpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ClmhpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ClmhpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ClmhpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CLMHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"VALIDFLAG, " +
							"CURRFROM, " +
							"RECODE, " +
							"TRANNO, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"CURRTO, " +
							"CLAMSTAT, " +
							"CLAMTYP, " +
							"DTECLAM, " +
							"DTEREG, " +
							"DTEAPPR, " +
							"EFFDATE, " +
							"DTESTSCHG, " +
							"CTRMFLAG, " +
							"CHGFLAG, " +
							"APLSUPR, " +
							"APLSPFROM, " +
							"APLSPTO, " +
							"BILLSUPR, " +
							"BILLSPFROM, " +
							"BILLSPTO, " +
							"PLSUPR, " +
							"PLSPFROM, " +
							"PLSPTO, " +
							"INTDAYS, " +
							"PREMSUSP, " +
							"PREMADVBAL, " +
							"PREMADVINT, " +
							"BONUSTERM, " +
							"APLINT, " +
							"PLINT, " +
							"APLAMT, " +
							"PLAMT, " +
							"INTEREST, " +
							"PROCEEDS, " +
							"OFCHARGE, " +
							"REASONCD, " +
							"BILLFREQ, " +
							"BILLCHNL, " +
							"LIFE, " +
							"JLIFE, " +
							"DTOFDEATH, " +
							"RESNDESC, " +
							"OTHERADJST, " +
							"POLICYLOAN, " +
							"TDBTAMT, " +
							"CURRCD, " +
							"CAUSEOFDTH, " +
							"FUPCDE, " +
							"CNTTYPE, " +
							"ZRCSHAMT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"INTERESTRATE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     validflag,
                                     currfrom,
                                     recode,
                                     tranno,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     currto,
                                     clamstat,
                                     clamtyp,
                                     dteclam,
                                     dtereg,
                                     dteappr,
                                     effdate,
                                     dtestschg,
                                     ctrmflag,
                                     chgflag,
                                     aplsupr,
                                     aplspfrom,
                                     aplspto,
                                     billsupr,
                                     billspfrom,
                                     billspto,
                                     plsupr,
                                     plspfrom,
                                     plspto,
                                     intdays,
                                     premsusp,
                                     premadvbal,
                                     premcurr,
                                     bonusterm,
                                     aplint,
                                     plint,
                                     aplamt,
                                     plamt,
                                     interest,
                                     proceeds,
                                     ofcharge,
                                     reasoncd,
                                     billfreq,
                                     billchnl,
                                     life,
                                     jlife,
                                     dtofdeath,
                                     resndesc,
                                     otheradjst,
                                     policyloan,
                                     tdbtamt,
                                     currcd,
                                     causeofdth,
                                     fupcode,
                                     cnttype,
                                     zrcshamt,
                                     userProfile,
                                     jobName,
                                     datime,
                                     interestrate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		validflag.clear();
  		currfrom.clear();
  		recode.clear();
  		tranno.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		currto.clear();
  		clamstat.clear();
  		clamtyp.clear();
  		dteclam.clear();
  		dtereg.clear();
  		dteappr.clear();
  		effdate.clear();
  		dtestschg.clear();
  		ctrmflag.clear();
  		chgflag.clear();
  		aplsupr.clear();
  		aplspfrom.clear();
  		aplspto.clear();
  		billsupr.clear();
  		billspfrom.clear();
  		billspto.clear();
  		plsupr.clear();
  		plspfrom.clear();
  		plspto.clear();
  		intdays.clear();
  		premsusp.clear();
  		premadvbal.clear();
  		premcurr.clear();
  		bonusterm.clear();
  		aplint.clear();
  		plint.clear();
  		aplamt.clear();
  		plamt.clear();
  		interest.clear();
  		proceeds.clear();
  		ofcharge.clear();
  		reasoncd.clear();
  		billfreq.clear();
  		billchnl.clear();
  		life.clear();
  		jlife.clear();
  		dtofdeath.clear();
  		resndesc.clear();
  		otheradjst.clear();
  		policyloan.clear();
  		tdbtamt.clear();
  		currcd.clear();
  		causeofdth.clear();
  		fupcode.clear();
  		cnttype.clear();
  		zrcshamt.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		interestrate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getClmhrec() {
  		return clmhrec;
	}

	public FixedLengthStringData getClmhpfRecord() {
  		return clmhpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setClmhrec(what);
	}

	public void setClmhrec(Object what) {
  		this.clmhrec.set(what);
	}

	public void setClmhpfRecord(Object what) {
  		this.clmhpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(clmhrec.getLength());
		result.set(clmhrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}