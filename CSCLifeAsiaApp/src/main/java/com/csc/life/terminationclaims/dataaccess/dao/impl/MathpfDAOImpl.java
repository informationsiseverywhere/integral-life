package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.MathpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Mathpf;

import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MathpfDAOImpl extends BaseDAOImpl<Mathpf> implements MathpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(MathpfDAOImpl.class);
	
	@Override
	public boolean insertMathpf(List<Mathpf> mathpfList) {
		
		boolean isInsertSuccessful = true;
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO MATHPF (CHDRNUM, CHDRCOY, PLNSFX, LIFE, JLIFE, TRANNO, TERMID, TRDT, USER_T, EFFDATE, CURRCD, POLICYLOAN, TDBTAMT, OTHERADJST, REASONCD, RESNDESC, CNTTYPE, TRTM, ESTIMTOTAL, CLAMAMT, ZRCSHAMT, USRPRF, JOBNM, DATIME) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "); 
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Mathpf math : mathpfList) {
				ps.setString(1, math.getChdrnum());
				ps.setString(2, math.getChdrcoy());
				ps.setInt(3, math.getPlnsfx());
				ps.setString(4, math.getLife());
				ps.setString(5, math.getJlife());
				ps.setInt(6, math.getTranno());
				ps.setString(7, math.getTermid());
				ps.setInt(8, math.getTrdt());
				ps.setInt(9, math.getUser_t());
				ps.setInt(10, math.getEffdate());
				ps.setString(11, math.getCurrcd());
				ps.setBigDecimal(12, math.getPolicyloan());
				ps.setBigDecimal(13, math.getTdbtamt());
				ps.setBigDecimal(14, math.getOtheradjst());
				ps.setString(15, math.getReasoncd());
				ps.setString(16, math.getResndesc());
				ps.setString(17, math.getCnttype());
				ps.setInt(18, math.getTrtm());
				ps.setBigDecimal(19, math.getEstimtotal());
				ps.setBigDecimal(20, math.getClamamt());
				ps.setBigDecimal(21, math.getZrcshamt());
				ps.setString(22, this.getUsrprf());
				ps.setString(23, this.getJobnm());
				ps.setTimestamp(24, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertMathpf()", e);//IJTI-1561
			isInsertSuccessful = false;
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return isInsertSuccessful;
	}

	@Override
	public List<Mathpf> getmathclmRecordList(String chdrcoy, String chdrnum, int tranno) {
		StringBuilder sqlMatySelect1 = new StringBuilder(
                "SELECT CHDRNUM, CHDRCOY, PLNSFX, LIFE, JLIFE, TRANNO, TERMID, TRDT,USER_T, EFFDATE, CURRCD, POLICYLOAN, TDBTAMT, OTHERADJST, REASONCD, RESNDESC, CNTTYPE, TRTM, ESTIMTOTAL, CLAMAMT, ZRCSHAMT, USRPRF, JOBNM, DATIME ");
        sqlMatySelect1.append(" FROM MATHPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND TRANNO = ? and PLNSFX = '0'");
    
        sqlMatySelect1
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC,UNIQUE_NUMBER DESC ");
     
   	
        PreparedStatement ps = getPrepareStatement(sqlMatySelect1.toString());
   	 	ResultSet sqlMathpf1rs = null;
        List<Mathpf> mathlist = null;
    	 try {
 			 ps.setInt(1,Integer.parseInt(chdrcoy));
 			 ps.setString(2, chdrnum);
 			 ps.setInt(3, tranno);	
 			
 			
 			 sqlMathpf1rs = executeQuery(ps);
    		 mathlist = new ArrayList<Mathpf>();
			 while(sqlMathpf1rs.next()){
                Mathpf Mathpf = new Mathpf();
                Mathpf.setChdrnum(sqlMathpf1rs.getString(1));
                Mathpf.setChdrcoy(sqlMathpf1rs.getString(2));
                Mathpf.setPlnsfx(sqlMathpf1rs.getInt(3));
                Mathpf.setLife(sqlMathpf1rs.getString(4));
                Mathpf.setJlife(sqlMathpf1rs.getString(5));
                Mathpf.setTranno(sqlMathpf1rs.getInt(6));
                Mathpf.setTermid(sqlMathpf1rs.getString(7));
                Mathpf.setTrdt(sqlMathpf1rs.getInt(8));
                Mathpf.setUser_t(sqlMathpf1rs.getInt(9));
                Mathpf.setEffdate(sqlMathpf1rs.getInt(10));
                Mathpf.setCurrcd(sqlMathpf1rs.getString(11));
                Mathpf.setPolicyloan(sqlMathpf1rs.getBigDecimal(12));
                Mathpf.setTdbtamt(sqlMathpf1rs.getBigDecimal(13));
                Mathpf.setOtheradjst(sqlMathpf1rs.getBigDecimal(14));
                Mathpf.setReasoncd(sqlMathpf1rs.getString(15));
                Mathpf.setResndesc(sqlMathpf1rs.getString(16));
                Mathpf.setCnttype(sqlMathpf1rs.getString(17));
                Mathpf.setTrtm(sqlMathpf1rs.getInt(18));
                Mathpf.setEstimtotal(sqlMathpf1rs.getBigDecimal(19));
                Mathpf.setClamamt(sqlMathpf1rs.getBigDecimal(20));
                Mathpf.setZrcshamt(sqlMathpf1rs.getBigDecimal(21));
                Mathpf.setUsrprf(sqlMathpf1rs.getString(22));
                Mathpf.setJobnm(sqlMathpf1rs.getString(23));
                Mathpf.setDatime(sqlMathpf1rs.getTimestamp(24));
                
                mathlist.add(Mathpf);
				}
			} catch (SQLException e) {
	            LOGGER.error("getMatyRecord()", e);//IJTI-1561
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, null);
	        }	
	    	 return mathlist;
	    	 }
	}


