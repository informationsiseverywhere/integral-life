/*
 * File: Dpp001.java
 * Date: 29 August 2009 22:46:16
 * Author: Quipoz Limited
 * 
 * Class transformed from DPP001.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.recordstructures.Cashedrec;
import com.csc.life.contractservicing.procedures.Loanpymt;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.recordstructures.Dthcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  DPP001 - Death Claim Processing Subroutine No. 1
*  -------------------------------------------------
*
*  This program is an item entry on T6598, the death claim
*  subroutine method table. This method is used in order to update
*  the relevant sub-accounts.
*
*  The following linkage information is passed to this
*  subroutine:-
*             - DEATHREC
*
*  Processing
*  ----------
*
*  From the data passed in the linkage section, check whether the
*  CDTH-FIELD-TYPE is 'S', 'B' , 'T', 'M', 'X' OR 'L'.
*  Depending on what it is, perform one of the following:-
*
*  If CDTH-FIELD-TYPE = 'S' (sum insured):-
*     post to the sub-accounts-
*     (01),(02) If not Component Level Accounting.
*     (01),(08) If Component Level Accounting.
*
*  If CDTH-FIELD-TYPE = 'B' (bonus):-
*     post to the sub-accounts-
*     (01),(03) If not Component Level Accounting.
*     (01),(09) If Component Level Accounting.
*     (14)      To clear the Reversionary Bonus balance.
*
*  If CDTH-FIELD-TYPE = 'T' (terminal bonus):-
*     post to the sub-accounts-
*     (01),(05) If not Component Level Accounting.
*     (01),(11) If Component Level Accounting.
*
*  If CDTH-FIELD-TYPE = 'M' (Interim bonus):-
*     post to the sub-accounts-
*     (01),(04) If not Component Level Accounting.
*     (01),(10) If Component Level Accounting.
*
*  If CDTH-FIELD-TYPE = 'X' (Additional bonus):-
*     post to the sub-accounts-
*     (01),(06) If not Component Level Accounting.
*     (01),(12) If Component Level Accounting.
*
*  If CDTH-FIELD-TYPE = 'L' (Low Cost Endowment):-
*     post to the sub-accounts-
*     (01),(07) If not Component Level Accounting.
*     (01),(13) If Component Level Accounting.
*
*  The sub-account entries are found on T5645, accessed by  this
*  program number.
*
*      If the amounts are not zero, then call 'LIFACMV' ("cash"
*      posting subroutine) to post to the correct account.  The
*      posting required is defined in the appropriate line no. on
*      the T5645 table entry.
*      The linkage area LIFACMVREC will need to be set up for the
*      call to 'LIFACMV'.
*
*  When all records have been processed, make postings for
*  loans.
*  This is done by first calling TOTLOAN to bring
*  the interest on loans for this contract up to date.
*  ACMVs are then required for paying off loans. For each of
*  relevant entries on T5645, call LOANPYMT with the outstanding
*  claim amount until all surrender is 'allocated' or
*  all entries have been processed and some of the claim
*  value remains.
*  Then write an ACMV record to suspense for the residual of
*  the value to be claimed after the above.
*
*
*****************************************************************
* </pre>
*/
public class Dpp001 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected String wsaaSubr = "DPP001";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* ERRORS */
	private String e308 = "E308";
		/* TABLES */
	protected String t5645 = "T5645";
	private String t5688 = "T5688";
		/* FORMATS */
	protected String itemrec = "ITEMREC";
	protected ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();
	protected PackedDecimalData wsaaFirstPost = new PackedDecimalData(3, 0).init(ZERO);
	protected PackedDecimalData wsaaSecondPost = new PackedDecimalData(3, 0).init(ZERO);
	protected PackedDecimalData wsaaBonusPost = new PackedDecimalData(3, 0).init(ZERO);
	protected PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaElement = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(2, 0).isAPartOf(wsaaElement, 0).setUnsigned();

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	protected FixedLengthStringData wsaaTranTermid = new FixedLengthStringData(4).isAPartOf(wsaaTranid, 0);
	protected ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	protected ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	protected ZonedDecimalData wsaaTranUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

	private FixedLengthStringData wsaaT5645 = new FixedLengthStringData(105);
	private FixedLengthStringData[] wsaaStoredT5645 = FLSArrayPartOfStructure(5, 21, wsaaT5645, 0);
	protected ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	protected FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	protected FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	protected FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	protected FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);

	private FixedLengthStringData wsaaTrankey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaTranPrefix = new FixedLengthStringData(2).isAPartOf(wsaaTrankey, 0);
	private FixedLengthStringData wsaaTranCompany = new FixedLengthStringData(1).isAPartOf(wsaaTrankey, 2);
	private FixedLengthStringData wsaaTranEntity = new FixedLengthStringData(13).isAPartOf(wsaaTrankey, 3);
	protected PackedDecimalData wsaaNetVal = new PackedDecimalData(17, 2);
	protected ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	protected ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();
	protected Cashedrec cashedrec = new Cashedrec();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
		/*Logical File: Extra data screen*/
	protected DescTableDAM descIO = new DescTableDAM();
	protected Dthcpy dthcpy = new Dthcpy();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	protected Syserrrec syserrrec = new Syserrrec();
	protected T5645rec t5645rec = new T5645rec();
	protected T5688rec t5688rec = new T5688rec();
	protected Varcom varcom = new Varcom();
	protected Batckey wsaaBatckey = new Batckey();

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		bonus505, 
		exit590, 
		exit9020
	}

	public Dpp001() {
		super();
	}

public void mainline(Object... parmArray)
	{
		dthcpy.deathRec = convertAndSetParam(dthcpy.deathRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		dthcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaTranTermid.set(SPACES);
		wsaaTranDate.set(ZERO);
		wsaaTranTime.set(ZERO);
		wsaaTranUser.set(ZERO);
		wsaaBatckey.set(dthcpy.batckey);
		readTabT5645200();
		readTabT5688250();
		wsaaJrnseq.set(ZERO);
		wsaaBonusPost.set(ZERO);
		if (isEQ(dthcpy.fieldType,"S")) {
			if (isEQ(t5688rec.comlvlacc,"Y")) {
				wsaaSub2.set(5);
				wsaaSecondPost.set(8);
				customerSpeceificTPDMethod();
				componentPosting400();
			}
			else {
				wsaaFirstPost.set(1);
				wsaaSecondPost.set(2);
				contractPosting300();
			}
		}
		else {
			if (isEQ(dthcpy.fieldType,"B")) {
				if (isEQ(t5688rec.comlvlacc,"Y")) {
					wsaaSub2.set(5);
					wsaaSecondPost.set(9);
					componentPosting400();
					wsaaBonusPost.set(14);
					componentPosting400();
					wsaaBonusPost.set(0);
				}
				else {
					wsaaFirstPost.set(1);
					wsaaSecondPost.set(3);
					contractPosting300();
					wsaaBonusPost.set(14);
					componentPosting400();
					wsaaBonusPost.set(0);
				}
			}
			else {
				if (isEQ(dthcpy.fieldType,"T")) {
					lifacmvrec1.origamt.set(dthcpy.actualVal);
					if (isEQ(t5688rec.comlvlacc,"Y")) {
						wsaaSub2.set(5);
						wsaaSecondPost.set(11);
						componentPosting400();
					}
					else {
						wsaaFirstPost.set(1);
						wsaaSecondPost.set(5);
						contractPosting300();
					}
				}
				else {
					if (isEQ(dthcpy.fieldType,"M")) {
						if (isEQ(t5688rec.comlvlacc,"Y")) {
							wsaaSub2.set(5);
							wsaaSecondPost.set(10);
							componentPosting400();
						}
						else {
							wsaaFirstPost.set(1);
							wsaaSecondPost.set(4);
							contractPosting300();
						}
					}
					else {
						if (isEQ(dthcpy.fieldType,"X")) {
							lifacmvrec1.origamt.set(dthcpy.actualVal);
							if (isEQ(t5688rec.comlvlacc,"Y")) {
								wsaaSub2.set(5);
								wsaaSecondPost.set(12);
								componentPosting400();
							}
							else {
								wsaaFirstPost.set(1);
								wsaaSecondPost.set(6);
								contractPosting300();
							}
						}
						else {
							if (isEQ(dthcpy.fieldType,"L")) {
								lifacmvrec1.origamt.set(dthcpy.actualVal);
								if (isEQ(t5688rec.comlvlacc,"Y")) {
									wsaaSub2.set(5);
									wsaaSecondPost.set(13);
									componentPosting400();
								}
								else {
									wsaaFirstPost.set(1);
									wsaaSecondPost.set(7);
									contractPosting300();
								}
							}
						}
					}
				}
			}
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTabT5645200()
	{
		read231();
	}

protected void read231()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dthcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq("01");
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,4)); wsaaSub1.add(1)){
			compute(wsaaSub2, 0).set(add(1,wsaaSub1));
			wsaaT5645Cnttot[wsaaSub2.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
			wsaaT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
			wsaaT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
			wsaaT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
			wsaaT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		}
		itemIO.setItemcoy(dthcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaSub1.set(15);
		wsaaSub2.set(1);
		wsaaT5645Cnttot[wsaaSub2.toInt()].set(t5645rec.cnttot[wsaaSub1.toInt()]);
		wsaaT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
		wsaaT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
		wsaaT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
		wsaaT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(dthcpy.chdrChdrcoy);
		descIO.setLanguage(dthcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
	}

protected void readTabT5688250()
	{
		read251();
	}

protected void read251()
	{
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(dthcpy.cnttype);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	





		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),dthcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),dthcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(dthcpy.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void contractPosting300()
	{
		/*PARA*/
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rldgacct.set(dthcpy.chdrChdrnum);
		writeAcmv500();
		/*EXIT*/
	}

protected void componentPosting400()
	{
		para401();
	}

protected void para401()
	{
		covrenqIO.setChdrcoy(dthcpy.chdrChdrcoy);
		covrenqIO.setChdrnum(dthcpy.chdrChdrnum);
		covrenqIO.setLife(dthcpy.lifeLife);
		covrenqIO.setCoverage(dthcpy.covrCoverage);
		covrenqIO.setRider(dthcpy.covrRider);
		wsaaElement.set(dthcpy.element);
		covrenqIO.setPlanSuffix(wsaaPlnsfx);
		covrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError9000();
		}
		lifacmvrec1.substituteCode[6].set(covrenqIO.getCrtable());
		wsaaRldgChdrnum.set(dthcpy.chdrChdrnum);
		wsaaRldgLife.set(dthcpy.lifeLife);
		wsaaRldgCoverage.set(dthcpy.covrCoverage);
		wsaaRldgRider.set(dthcpy.covrRider);
		wsaaRldgPlanSuffix.set(dthcpy.element);
		lifacmvrec1.rldgacct.set(wsaaRldgacct);
		writeAcmv500();
	}

protected void writeAcmv500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					go501();
				}
				case bonus505: {
					bonus505();
				}
				case exit590: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go501()
	{
		lifacmvrec1.batckey.set(wsaaBatckey);
		lifacmvrec1.rdocnum.set(dthcpy.chdrChdrnum);
		lifacmvrec1.tranno.set(dthcpy.tranno);
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.effdate.set(dthcpy.effdate);
		lifacmvrec1.origcurr.set(dthcpy.currcode);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.genlcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec1.crate.set(ZERO);
		wsaaNetVal.set(dthcpy.actualVal);
		wsaaTranTermid.set(dthcpy.termid);
		wsaaTranUser.set(dthcpy.user);
		wsaaTranTime.set(dthcpy.time);
		wsaaTranDate.set(dthcpy.date_var);
		allocateToLoans600();
		if (isNE(wsaaBonusPost,ZERO)) {
			goTo(GotoLabel.bonus505);
		}
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.termid.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec1.sacscode.set(wsaaT5645Sacscode[wsaaSub2.toInt()]);
			lifacmvrec1.sacstyp.set(wsaaT5645Sacstype[wsaaSub2.toInt()]);
			lifacmvrec1.glcode.set(wsaaT5645Glmap[wsaaSub2.toInt()]);
			lifacmvrec1.glsign.set(wsaaT5645Sign[wsaaSub2.toInt()]);
			lifacmvrec1.contot.set(wsaaT5645Cnttot[wsaaSub2.toInt()]);
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode[wsaaFirstPost.toInt()]);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype[wsaaFirstPost.toInt()]);
			lifacmvrec1.glcode.set(t5645rec.glmap[wsaaFirstPost.toInt()]);
			lifacmvrec1.glsign.set(t5645rec.sign[wsaaFirstPost.toInt()]);
			lifacmvrec1.contot.set(t5645rec.cnttot[wsaaFirstPost.toInt()]);
		}
		lifacmvrec1.rldgcoy.set(dthcpy.chdrChdrcoy);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		wsaaTranno.set(dthcpy.tranno);
		lifacmvrec1.tranref.set(wsaaTranno);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(dthcpy.cnttype);
		lifacmvrec1.origamt.set(wsaaNetVal);
		lifacmvrec1.termid.set(dthcpy.termid);
		lifacmvrec1.user.set(dthcpy.user);
		lifacmvrec1.transactionTime.set(dthcpy.time);
		lifacmvrec1.transactionDate.set(dthcpy.date_var);
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		if (isNE(lifacmvrec1.origamt,0)) {
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz,varcom.oK)) {
				syserrrec.params.set(lifacmvrec1.lifacmvRec);
				fatalError9000();
			}
		}
		lifacmvrec1.origamt.set(dthcpy.actualVal);
		lifacmvrec1.sacscode.set(t5645rec.sacscode[wsaaSecondPost.toInt()]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[wsaaSecondPost.toInt()]);
		lifacmvrec1.glcode.set(t5645rec.glmap[wsaaSecondPost.toInt()]);
		lifacmvrec1.glsign.set(t5645rec.sign[wsaaSecondPost.toInt()]);
		lifacmvrec1.contot.set(t5645rec.cnttot[wsaaSecondPost.toInt()]);
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError9000();
		}
	}

protected void bonus505()
	{
		if (isEQ(wsaaBonusPost,ZERO)) {
			goTo(GotoLabel.exit590);
		}
		lifacmvrec1.sacscode.set(t5645rec.sacscode[wsaaBonusPost.toInt()]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[wsaaBonusPost.toInt()]);
		lifacmvrec1.glcode.set(t5645rec.glmap[wsaaBonusPost.toInt()]);
		lifacmvrec1.glsign.set(t5645rec.sign[wsaaBonusPost.toInt()]);
		lifacmvrec1.contot.set(t5645rec.cnttot[wsaaBonusPost.toInt()]);
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		if (isNE(lifacmvrec1.origamt,0)) {
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz,varcom.oK)) {
				syserrrec.params.set(lifacmvrec1.lifacmvRec);
				fatalError9000();
			}
		}
	}

protected void allocateToLoans600()
	{
		start600();
	}

protected void start600()
	{
		cashedrec.cashedRec.set(SPACES);
		cashedrec.function.set("RESID");
		cashedrec.batchkey.set(lifacmvrec1.batckey);
		cashedrec.doctNumber.set(lifacmvrec1.rdocnum);
		cashedrec.doctCompany.set(lifacmvrec1.batccoy);
		cashedrec.trandate.set(lifacmvrec1.effdate);
		cashedrec.docamt.set(0);
		cashedrec.contot.set(0);
		cashedrec.transeq.set(wsaaJrnseq);
		cashedrec.acctamt.set(0);
		cashedrec.origccy.set(lifacmvrec1.origcurr);
		cashedrec.dissrate.set(lifacmvrec1.crate);
		cashedrec.trandesc.set(lifacmvrec1.trandesc);
		cashedrec.genlCompany.set(lifacmvrec1.genlcoy);
		cashedrec.genlCurrency.set(lifacmvrec1.genlcur);
		cashedrec.chdrcoy.set(dthcpy.chdrChdrcoy);
		cashedrec.chdrnum.set(dthcpy.chdrChdrnum);
		wsaaTrankey.set(SPACES);
		wsaaTranCompany.set(dthcpy.chdrChdrcoy);
		wsaaTranEntity.set(dthcpy.chdrChdrnum);
		cashedrec.trankey.set(wsaaTrankey);
		cashedrec.tranno.set(lifacmvrec1.tranno);
		cashedrec.tranid.set(wsaaTranid);
		cashedrec.language.set(dthcpy.language);
		for (wsaaSub1.set(1); !(isGT(wsaaSub1,4)
		|| isEQ(wsaaNetVal,0)); wsaaSub1.add(1)){
			loans620();
		}
		wsaaJrnseq.set(cashedrec.transeq);
	}

protected void loans620()
	{
		/*START*/
		cashedrec.sign.set(wsaaT5645Sign[wsaaSub1.toInt()]);
		cashedrec.sacscode.set(wsaaT5645Sacscode[wsaaSub1.toInt()]);
		cashedrec.sacstyp.set(wsaaT5645Sacstype[wsaaSub1.toInt()]);
		cashedrec.genlAccount.set(wsaaT5645Glmap[wsaaSub1.toInt()]);
		cashedrec.origamt.set(wsaaNetVal);
		callProgram(Loanpymt.class, cashedrec.cashedRec);
		if (isNE(cashedrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(cashedrec.statuz);
			fatalError9000();
		}
		wsaaNetVal.set(cashedrec.docamt);
		/*EXIT*/
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		dthcpy.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
protected void customerSpeceificTPDMethod() {
	
}
}
