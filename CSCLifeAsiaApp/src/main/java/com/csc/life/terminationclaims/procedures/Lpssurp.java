package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.dao.SurdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.SurhpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Surdpf;
import com.csc.life.terminationclaims.dataaccess.model.Surhpf;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.dataaccess.model.Descpf;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Lpssurp extends SMARTCodeModel{
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaSubr = new FixedLengthStringData(7).init("LPSSURP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Batckey wsaaBatckey = new Batckey();

	private Surpcpy surpcpy = new Surpcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	
	private ItempfDAO itemDAO = getApplicationContext().getBean("itempfDAO",ItempfDAO.class);
	private Itempf itempft5645 = new Itempf();
	private Itempf itempft5688 = new Itempf();
	
	private FixedLengthStringData wsaaAcctLevel = new FixedLengthStringData(1);
	private Validator componentLevelAccounting = new Validator(wsaaAcctLevel, "Y");
	
	private SurdpfDAO surdpfDAO = getApplicationContext().getBean("surdpfDAO", SurdpfDAO.class);
	private List<Surdpf> surdpfList = new ArrayList<Surdpf>();
	private List<String> tempChdrnumList = new ArrayList<String>();
	private Map<String, List<Surdpf>> surdpfMap = new HashMap<String, List<Surdpf>>();
	
	private DescpfDAO descpfDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class); 
	private Descpf descpf = new Descpf();
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	
	private T5688rec t5688rec = new T5688rec();
	private T5645rec t5645rec = new T5645rec();
	
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();
	
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (5, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);
	
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	
	private PackedDecimalData wsaaAccumValue = new PackedDecimalData(17, 2);
	
	private SurhpfDAO surhpfDAO = getApplicationContext().getBean("surhpfDAO", SurhpfDAO.class);
	private List<Surhpf> surhpfList = new ArrayList<Surhpf>();
	
	public Lpssurp() {
		super();
	}
	
	public void mainline(Object... parmArray)
	{
		surpcpy.surrenderRec = convertAndSetParam(surpcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

	protected void mainline1000()
	{
		/*MAIN*/
		  initialise2000();
		  exitProgram();
	}

	protected void initialise2000()
	{
		tempChdrnumList.add(surpcpy.chdrnum.toString());
		syserrrec.subrname.set(wsaaSubr);
		varcom.vrcmTime.set(getCobolTime());
		wsaaBatckey.set(surpcpy.batckey);
		wsaaAccumValue.set(ZERO);
		readT5645();
		readT5688();
		readT1688();
		initialAcmvFields2400();
		readSurdpf();
		readSurhpf();
		endOfContractPosting();
		surpcpy.status.set(varcom.endp);
	}

	protected void readT5645() {
	
		itempft5645 = itemDAO.findItemByItem("IT", surpcpy.chdrcoy.toString(), "T5645", wsaaSubr.toString());
		if(!(itempft5645 == null)) {
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempft5645.getGenarea()));
			for (wsaaSub1.set(1); !(isGT(wsaaSub1, 5)); wsaaSub1.add(1)){
				wsaaSub2.add(1);
				wsaaT5645Glmap[wsaaSub2.toInt()].set(t5645rec.glmap[wsaaSub1.toInt()]);
				wsaaT5645Sacscode[wsaaSub2.toInt()].set(t5645rec.sacscode[wsaaSub1.toInt()]);
				wsaaT5645Sacstype[wsaaSub2.toInt()].set(t5645rec.sacstype[wsaaSub1.toInt()]);
				wsaaT5645Sign[wsaaSub2.toInt()].set(t5645rec.sign[wsaaSub1.toInt()]);
			}
		}
		else {
			surpcpy.status.set(varcom.bomb);
		}
	}

	protected void readT5688() {
	
		itempft5688 = itemDAO.findItemByDate("IT", surpcpy.chdrcoy.toString(), "T5688", surpcpy.cnttype.toString(), surpcpy.effdate.toString());
		if(!(itempft5688 == null)) {
			t5688rec.t5688Rec.set(StringUtil.rawToString(itempft5688.getGenarea()));
			wsaaAcctLevel.set(t5688rec.comlvlacc);
		}
		else {
			surpcpy.status.set(varcom.bomb);
		}
	}
	
	protected void readT1688() {
		
		descpf = descpfDAO.getDesc("IT", surpcpy.chdrcoy.toString(), "T1688", wsaaBatckey.batcBatctrcde.toString(), surpcpy.language.toString());
		if(!(descpf == null)) {
			wsaaLongdesc.set(descpf.getLongdesc());
		}
		else {
			surpcpy.status.set(varcom.bomb);
		}
	}

	protected void readSurdpf() {
	
		surdpfMap = surdpfDAO.getSurdpfMap(surpcpy.chdrcoy.toString(), tempChdrnumList);
		if(surdpfMap.isEmpty()) {
			surpcpy.status.set(varcom.bomb);
		}
		else {
			surdpfList = surdpfMap.get(surpcpy.chdrnum.toString());
			for(Surdpf surd : surdpfList) {
				insertPostingRecord(surd);
				if (isEQ(surd.getTypeT(), "R")) {
					insertcommposting(surd);
				}
			}
		}
	}
	
	protected void insertcommposting(Surdpf surd) {
		lifacmvrec.origamt.set(surd.getCommclaw());
		lifacmvrec.tranno.set(surd.getTranno());
		if (componentLevelAccounting.isTrue()) {
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			wsaaRldgChdrnum.set(surpcpy.chdrnum);
			wsaaPlan.set(surd.getPlnsfx());
			wsaaRldgLife.set(surd.getLife());
			wsaaRldgCoverage.set(surd.getCoverage());
			wsaaRldgRider.set(surd.getRider());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
		}
		lifacmvrec.substituteCode[6].set(surd.getCrtable());
		if (isNE(lifacmvrec.origamt, 0)) {
			postAcmvRecord5000();
		}
		lifacmvrec.rldgacct.set(SPACES);
	}
	protected void initialAcmvFields2400() {
		/*  Set up LIFACMV parameters which are common to all postings.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batccoy.set(surpcpy.chdrcoy);
		lifacmvrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec.rdocnum.set(surpcpy.chdrnum);
		lifacmvrec.rldgcoy.set(surpcpy.chdrcoy);
		lifacmvrec.origcurr.set(surpcpy.cntcurr);
		lifacmvrec.tranref.set(surpcpy.chdrnum);
		lifacmvrec.trandesc.set(wsaaLongdesc);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.genlcoy.set(surpcpy.chdrcoy);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.effdate.set(surpcpy.effdate);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.transactionDate.set(surpcpy.effdate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(surpcpy.user);
		lifacmvrec.termid.set(surpcpy.termid);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.substituteCode[1].set(surpcpy.cnttype);
	}
	
	protected void insertPostingRecord(Surdpf surd) {
		
		lifacmvrec.origamt.set(surd.getActvalue());
		lifacmvrec.tranno.set(surd.getTranno());
		if (isEQ(surd.getTypeT(), "R")) {
			if (componentLevelAccounting.isTrue()) {
				lifacmvrec.sacscode.set(t5645rec.sacscode01);
				lifacmvrec.sacstyp.set(t5645rec.sacstype01);
				lifacmvrec.glcode.set(t5645rec.glmap01);
				lifacmvrec.glsign.set(t5645rec.sign01);
				lifacmvrec.contot.set(t5645rec.cnttot01);
				wsaaRldgChdrnum.set(surpcpy.chdrnum);
				wsaaPlan.set(surd.getPlnsfx());
				wsaaRldgLife.set(surd.getLife());
				wsaaRldgCoverage.set(surd.getCoverage());
				wsaaRldgRider.set(surd.getRider());
				wsaaRldgPlanSuffix.set(wsaaPlansuff);
				lifacmvrec.rldgacct.set(wsaaRldgacct);
			}
			else {
				lifacmvrec.sacscode.set(t5645rec.sacscode02);
				lifacmvrec.sacstyp.set(t5645rec.sacstype02);
				lifacmvrec.glcode.set(t5645rec.glmap02);
				lifacmvrec.glsign.set(t5645rec.sign02);
				lifacmvrec.contot.set(t5645rec.cnttot02);
				lifacmvrec.rldgacct.set(surpcpy.chdrnum);
			}
			lifacmvrec.substituteCode[6].set(surd.getCrtable());
			postAcmvRecord5000();
		}
		else {
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
			lifacmvrec.rldgacct.set(surpcpy.chdrnum);
			postAcmvRecord5000();
		}
		lifacmvrec.rldgacct.set(SPACES);
		wsaaAccumValue.add(surd.getActvalue().doubleValue());
	}
	
	protected void postAcmvRecord5000() {
		/*START*/
		/*  Call "LIFACMV" to write account movement records to the ACMV*/
		/*  file.*/
		wsaaSequenceNo.add(1);
		lifacmvrec.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			surpcpy.status.set(varcom.bomb);
		}
		/*EXIT*/
	}
	
	protected void readSurhpf() {
		surhpfList = surhpfDAO.getSurhpfRecord(surpcpy.chdrcoy.toString(), surpcpy.chdrnum.toString());
		for(Surhpf surh : surhpfList) {
			compute(wsaaAccumValue, 2).set(sub(sub(wsaaAccumValue, surh.getTdbtamt()), surh.getTaxamt()));
		}
	}
	
	protected void endOfContractPosting() {
		lifacmvrec.rldgacct.set(surpcpy.chdrnum);
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.origamt.set(wsaaAccumValue);
		postAcmvRecord5000();
	}
}
