/*
 * File: Dpp002.java
 * Date: 29 August 2009 22:46:23
 * Author: Quipoz Limited
 * 
 * Class transformed from DPP002.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.terminationclaims.recordstructures.Dthcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrssurTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZutrpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zutrpf;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        SPECIFICATION FOR DEATH PROCESSING METHOD No. 2
*
* This program is an item entry on T6598, the death claim
* subroutine method table. This method is used in order to update
* theUTRNs, which will be used later to post to the sub-accounts.
* The following is the linkage information passed to this
* subroutine:-
*          - company
*          - contract header number
*          - life number
*          - joint-life number
*          - coverage
*          - rider
*          - crtable
*          - effective date
*          - estimated value
*          - actual value
*          - currency
*          - element code
*          - type code
*          - status
*          - batch key
*PROCESSING.
*----------
* From the data passed in the linkage section,
* Perform the following:
* For each UTRS record write a UTRN record.
*    - Write a UTRN record with the following data for the
*    (outstanding claims sub-account):-
*      Processing Seq No. -  from T6647
*      Surrender %        -  100
*      Amount (signed)    -  0
*     Effective date     -  Effective-date of the claim
*     Bid/Offer Ind      -  'B'
*     Now/Deferred Ind   -  T6647 entry
*     Price              -  0
*     Fund               -  linkage (element code)
*     Number of Units    -  0
*     Contract No.       -  linkage
*     Contract Type      -  linkage
*     Sub-Account Code   -  T5645 entry for this sub-account
*     Sub-Account Type   -  ditto
*     G/L key            -  ditto
*     Trigger module     -  T5687/T6598 entry
*     Trigger key        -  Contract no./Life/Coverage/Rider
*     Surr Value Penalty -  1
*
*     This Subroutine now also checks the value of the Unit
*     Statement Method on T6647 and uses it to access T6659.
*     The appropriate Subroutine will be called if set up.
*
*****************************************************************
* </pre>
*/
public class Dpp002 extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Dpp002.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "DPP002";
	private static final String e308 = "E308";
	private static final String f294 = "F294";
	private static final String h072 = "H072";
	private static final String g029 = "G029";
	private static final String h115 = "H115";
		/* TABLES */
	private static final String t6647 = "T6647";
	private static final String t5645 = "T5645";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t6598 = "T6598";
	private static final String t6659 = "T6659";
	private static final String utrnrec = "UTRNREC";
	private static final String hitrrec = "HITRREC";
	private static final String hitralorec = "HITRALOREC";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaTrigger = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaTriggerChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTrigger, 0);
	private FixedLengthStringData wsaaTriggerChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTrigger, 1);
	private FixedLengthStringData wsaaTriggerLife = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 9);
	private FixedLengthStringData wsaaTriggerCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 11);
	private FixedLengthStringData wsaaTriggerRider = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 13);
	private FixedLengthStringData filler = new FixedLengthStringData(5).isAPartOf(wsaaTrigger, 15, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTabBatckey = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaUtrnWritten = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaHitrWritten = new FixedLengthStringData(1);
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrnclmTableDAM utrnclmIO = new UtrnclmTableDAM();
	private UtrssurTableDAM utrssurIO = new UtrssurTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T6647rec t6647rec = new T6647rec();
	private T5645rec t5645rec = new T5645rec();
	private T6598rec t6598rec = new T6598rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T6659rec t6659rec = new T6659rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Dthcpy dthcpy = new Dthcpy();
	private Zutrpf zutrpf = new Zutrpf();//ILIFE-5462
	private ZutrpfDAO zutrpfDAO = getApplicationContext().getBean("zutrpfDAO", ZutrpfDAO.class);//ILIFE-5462
	private List<Zutrpf> zutrpfList = null; //ILIFE-5462
	private FixedLengthStringData wsaaRecordFound = new FixedLengthStringData(1);//ILIFE-5462
	

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		utrn220, 
		exit290
	}

	public Dpp002() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dthcpy.deathRec = convertAndSetParam(dthcpy.deathRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		dthcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaBatckey.set(dthcpy.batckey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaTime.set(getCobolTime());
		wsaaUtrnWritten.set(SPACES);
		wsaaHitrWritten.set(SPACES);
		/*    IF DTHP-TYPE = 'F'*/
		mainProcessing200();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void mainProcessing200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para201();
				case utrn220: 
					utrn220();
				case exit290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para201()
	{
		/* Check to see if Component or Contract level accounting          */
		/* is required.                                                    */
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(dthcpy.cnttype);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), dthcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), dthcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(dthcpy.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		if (isEQ(dthcpy.status, varcom.oK)) {
			readTabT6647300();
		}
		if (isEQ(dthcpy.status, varcom.oK)
		&& isNE(t6647rec.unitStatMethod, SPACES)) {
			a00ReadTabT6659();
		}
		if (isEQ(dthcpy.status, varcom.oK)) {
			readTabT5645400();
		}
		if (isEQ(dthcpy.status, varcom.oK)) {
			getTrigger500();
		}
		if (isEQ(dthcpy.fieldType, "D")) {
			a100ReadHitrWriteHitr();
			/* Do not perform A200 section if HITR already written             */
			if (isEQ(wsaaHitrWritten, "Y")) {
				goTo(GotoLabel.utrn220);
			}
			a200ReadHitsWriteHitr();
			goTo(GotoLabel.exit290);
		}
	}

protected void utrn220()
	{
		readReservePricing();//ILIFE-5462
		readUtrnWriteUtrn800();
		/* Do not perform 600 section if UTRN already written for fund     */
		if (isEQ(wsaaUtrnWritten, "Y")) {
			return ;
		}
		readUtrsWriteUtrn600();
	}

protected void readTabT6647300()
	{
		read331();
	}

protected void read331()
	{
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaTabBatckey.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(dthcpy.cnttype);
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaT6647Key, itdmIO.getItemitem())
		|| isNE(dthcpy.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t6647)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*     MOVE F265                TO DTHP-STATUS                   */
			dthcpy.status.set(h115);
			itdmIO.setItemitem(wsaaT6647Key);
			syserrrec.params.set(itdmIO.getParams());
			/*     MOVE F265                TO SYSR-STATUZ           <A07446>*/
			syserrrec.statuz.set(h115);
			fatalError9000();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void readTabT5645400()
	{
		/*READ*/
		itemIO.setItemcoy(dthcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void getTrigger500()
	{
		read531();
	}

protected void read531()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(dthcpy.crtable);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), dthcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), dthcpy.crtable)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			dthcpy.status.set(f294);
			itdmIO.setItemitem(dthcpy.crtable);
			syserrrec.statuz.set(f294);
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6598);
		itdmIO.setItemitem(t5687rec.dcmeth);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), dthcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), t6598)
		|| isNE(itdmIO.getItemitem(), t5687rec.dcmeth)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/*     MOVE U040                TO DTHP-STATUS                   */
			dthcpy.status.set(h072);
			itdmIO.setItemitem(t5687rec.dcmeth);
			syserrrec.params.set(itdmIO.getParams());
			/*     MOVE U040                TO SYSR-STATUZ                   */
			syserrrec.statuz.set(h072);
			fatalError9000();
		}
		else {
			t6598rec.t6598Rec.set(itdmIO.getGenarea());
		}
	}

protected void readUtrsWriteUtrn600()
	{
		readUtrsWriteUtrnPara600();
	}

protected void readUtrsWriteUtrnPara600()
	{
		utrssurIO.setParams(SPACES);
		utrssurIO.setChdrcoy(dthcpy.chdrChdrcoy);
		utrssurIO.setChdrnum(dthcpy.chdrChdrnum);
		utrssurIO.setLife(dthcpy.lifeLife);
		utrssurIO.setCoverage(dthcpy.covrCoverage);
		utrssurIO.setRider(dthcpy.covrRider);
		utrssurIO.setUnitVirtualFund(dthcpy.element);
		utrssurIO.setUnitType(dthcpy.fieldType);
		utrssurIO.setPlanSuffix(ZERO);
		utrssurIO.setFunction(varcom.begn);
		while ( !(isEQ(utrssurIO.getStatuz(), varcom.endp))) {
			
		
          //performance improvement --  atiwari23 
            utrssurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
            utrssurIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","VRTFND","UNITYP");
			readUtrs650();
		}
		
	}

protected void readUtrs650()
	{
		/*GO*/
		SmartFileCode.execute(appVars, utrssurIO);
		if (isNE(utrssurIO.getStatuz(), varcom.oK)
		&& isNE(utrssurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrssurIO.getParams());
			fatalError9000();
		}
		if (isNE(dthcpy.chdrChdrcoy, utrssurIO.getChdrcoy())
		|| isNE(dthcpy.chdrChdrnum, utrssurIO.getChdrnum())
		|| isNE(dthcpy.lifeLife, utrssurIO.getLife())
		|| isNE(dthcpy.covrCoverage, utrssurIO.getCoverage())
		|| isNE(dthcpy.covrRider, utrssurIO.getRider())
		|| isNE(dthcpy.element, utrssurIO.getUnitVirtualFund())
		|| isNE(dthcpy.fieldType, utrssurIO.getUnitType())
		|| isEQ(utrssurIO.getStatuz(), varcom.endp)) {
			utrssurIO.setStatuz(varcom.endp);
		}
		else {
			wsaaPlanSuffix.set(utrssurIO.getPlanSuffix());
			/*    PERFORM 700-WRITE-UTRN.                                   */
			if (isNE(utrssurIO.getCurrentUnitBal(), ZERO)) {
				writeUtrn700();
			}
		}
		utrssurIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void writeUtrn700()
	{
		go701();
	}

protected void go701()
	{
		utrnIO.setParams(SPACES);
		utrnIO.setTranno(ZERO);
		utrnIO.setPlanSuffix(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setBatcactyr(ZERO);
		utrnIO.setBatcactmn(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setContractAmount(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setProcSeqNo(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setSvp(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setCrComDate(ZERO);
		utrnIO.setPlanSuffix(wsaaPlanSuffix);
		utrnIO.setChdrcoy(dthcpy.chdrChdrcoy);
		wsaaTriggerChdrcoy.set(dthcpy.chdrChdrcoy);
		utrnIO.setChdrnum(dthcpy.chdrChdrnum);
		wsaaTriggerChdrnum.set(dthcpy.chdrChdrnum);
		utrnIO.setLife(dthcpy.lifeLife);
		wsaaTriggerLife.set(dthcpy.lifeLife);
		/*    Set up the utrn batch key correctly                          */
		utrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		utrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		utrnIO.setCoverage(dthcpy.covrCoverage);
		wsaaTriggerCoverage.set(dthcpy.covrCoverage);
		utrnIO.setRider(dthcpy.covrRider);
		wsaaTriggerRider.set(dthcpy.covrRider);
		utrnIO.setCrtable(dthcpy.crtable);
		utrnIO.setMoniesDate(dthcpy.effdate);
		
		// ILIFE-5462 Start
		if (isEQ(wsaaRecordFound, "Y")) {
			if (isGT(zutrpfList.size(), 0)){
				for(Zutrpf zutrpf1 : zutrpfList){
					utrnIO.setMoniesDate(zutrpf1.getReserveUnitsDate()); 
				}
			}	
		}
		// ILIFE-5462 End
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setSurrenderPercent(100);
		/*    No Fund or Contract Amount Required because Surr % is 100    */
		utrnIO.setFundAmount(0);
		utrnIO.setUnitVirtualFund(dthcpy.element);
		utrnIO.setContractType(dthcpy.cnttype);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setNowDeferInd(t6647rec.dealin);
		
		if (isEQ(wsaaRecordFound, "Y")) {// ILIFE-5462
			utrnIO.setNowDeferInd('N');
		}
		
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		utrnIO.setTriggerModule(t6598rec.addprocess);
		utrnIO.setTriggerKey(wsaaTrigger);
		utrnIO.setNofUnits(0);
		utrnIO.setTransactionDate(wsaaToday);
		utrnIO.setTransactionTime(wsaaTime);
		utrnIO.setUser(dthcpy.user);
		utrnIO.setTranno(dthcpy.tranno);
		utrnIO.setSvp(1);
		utrnIO.setFundCurrency(dthcpy.currcode);
		utrnIO.setCntcurr(dthcpy.contractCurr);
		utrnIO.setUnitType(dthcpy.fieldType);
		utrnIO.setTermid(SPACES);
		utrnIO.setFormat(utrnrec);
		utrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
	}

protected void readUtrnWriteUtrn800()
	{
		/*GO*/
		utrnclmIO.setParams(SPACES);
		utrnclmIO.setChdrcoy(dthcpy.chdrChdrcoy);
		utrnclmIO.setChdrnum(dthcpy.chdrChdrnum);
		utrnclmIO.setCoverage(dthcpy.covrCoverage);
		utrnclmIO.setRider(dthcpy.covrRider);
		utrnclmIO.setUnitVirtualFund(dthcpy.element);
		utrnclmIO.setUnitType(dthcpy.fieldType);
		utrnclmIO.setFunction(varcom.begn);
		while ( !(isEQ(utrnclmIO.getStatuz(),varcom.endp))) {
			  //performance improvement --  atiwari23 
//			utrnclmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
			
			
			readUtrn850();
		}
		
		/*EXIT*/
	}

protected void readUtrn850()
	{
		go851();
	}

protected void go851()
	{
		SmartFileCode.execute(appVars, utrnclmIO);
		if (isNE(utrnclmIO.getStatuz(), varcom.oK)
		&& isNE(utrnclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrnclmIO.getParams());
			fatalError9000();
		}
		if (isNE(dthcpy.chdrChdrcoy, utrnclmIO.getChdrcoy())
		|| isNE(dthcpy.chdrChdrnum, utrnclmIO.getChdrnum())
		|| isNE(dthcpy.covrCoverage, utrnclmIO.getCoverage())
		|| isNE(dthcpy.covrRider, utrnclmIO.getRider())
		|| isNE(dthcpy.element, utrnclmIO.getUnitVirtualFund())
		|| isNE(dthcpy.fieldType, utrnclmIO.getUnitType())
		|| isEQ(utrnclmIO.getStatuz(), varcom.endp)) {
			utrnclmIO.setStatuz(varcom.endp);
		}
		else {
			if (isEQ(utrnclmIO.getFeedbackInd(), SPACES)
			|| isEQ(utrnclmIO.getFeedbackInd(), "N")) {
				wsaaPlanSuffix.set(utrnclmIO.getPlanSuffix());
				/*    PERFORM 700-WRITE-UTRN.                                   */
				writeUtrn700();
				utrnclmIO.setStatuz(varcom.endp);
				wsaaUtrnWritten.set("Y");
			}
		}
		utrnclmIO.setFunction(varcom.nextr);
	}

protected void a00ReadTabT6659()
	{
		a00Read();
		a10CheckT6659Details();
	}

protected void a00Read()
	{
		itdmIO.setItemcoy(dthcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(dthcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(t6647rec.unitStatMethod, itdmIO.getItemitem())
		|| isNE(dthcpy.chdrChdrcoy, itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t6659)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g029);
			fatalError9000();
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

protected void a10CheckT6659Details()
	{
		/* Check the details and call the generic subroutine from T6659.   */
		if (isEQ(t6659rec.subprog, SPACES)
		|| isNE(t6659rec.annOrPayInd, "P")
		|| isNE(t6659rec.osUtrnInd, "Y")) {
			return ;
		}
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(dthcpy.chdrChdrcoy);
		annprocrec.chdrnum.set(dthcpy.chdrChdrnum);
		annprocrec.life.set(SPACES);
		annprocrec.coverage.set(SPACES);
		annprocrec.rider.set(SPACES);
		annprocrec.planSuffix.set(0);
		annprocrec.effdate.set(dthcpy.effdate);
		annprocrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		annprocrec.crdate.set(0);
		annprocrec.crtable.set(SPACES);
		annprocrec.user.set(dthcpy.user);
		annprocrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		annprocrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		annprocrec.batccoy.set(wsaaBatckey.batcBatccoy);
		annprocrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		annprocrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		annprocrec.batcpfx.set(wsaaBatckey.batcBatcpfx);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(annprocrec.statuz);
			syserrrec.params.set(annprocrec.annpllRec);
			fatalError9000();
		}
	}

protected void a100ReadHitrWriteHitr()
	{
		a101Hitr();
	}

protected void a101Hitr()
	{
		/* MOVE SPACES                 TO HITR-PARAMS.          <LA4551>*/
		/* MOVE DTHP-CHDR-CHDRCOY      TO HITR-CHDRCOY.         <LA4551>*/
		/* MOVE DTHP-CHDR-CHDRNUM      TO HITR-CHDRNUM.         <LA4551>*/
		/* MOVE DTHP-LIFE-LIFE         TO HITR-LIFE.            <LA4551>*/
		/* MOVE DTHP-COVR-COVERAGE     TO HITR-COVERAGE.        <LA4551>*/
		/* MOVE DTHP-COVR-RIDER        TO HITR-RIDER.           <LA4551>*/
		/* MOVE DTHP-ELEMENT           TO HITR-ZINTBFND.        <LA4551>*/
		/* MOVE ZEROES                 TO HITR-PLAN-SUFFIX      <LA4551>*/
		/*                                HITR-TRANNO.          <LA4551>*/
		/* MOVE HITRREC                TO HITR-FORMAT.          <LA4551>*/
		/* MOVE BEGN                   TO HITR-FUNCTION.        <LA4551>*/
		hitraloIO.setParams(SPACES);
		hitraloIO.setChdrcoy(dthcpy.chdrChdrcoy);
		hitraloIO.setChdrnum(dthcpy.chdrChdrnum);
		hitraloIO.setLife(dthcpy.lifeLife);
		hitraloIO.setCoverage(dthcpy.covrCoverage);
		hitraloIO.setRider(dthcpy.covrRider);
		hitraloIO.setZintbfnd(dthcpy.element);
		hitraloIO.setZrectyp(SPACES);
		hitraloIO.setPlanSuffix(ZERO);
		hitraloIO.setProcSeqNo(ZERO);
		hitraloIO.setEffdate(ZERO);
		hitraloIO.setTranno(ZERO);
		hitraloIO.setFormat(hitralorec);
		hitraloIO.setFunction(varcom.begn);
		/* PERFORM A150-READ-HITR UNTIL HITR-STATUZ = ENDP.     <LA4551>*/
		while ( !(isEQ(hitraloIO.getStatuz(), varcom.endp))) {

      

    		//performance improvement --  atiwari23 
            hitrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
            hitrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","ZINTBFND");

			a150ReadHitr();
		}
		
	}

protected void a150ReadHitr()
	{
		a151Hitr();
	}

protected void a151Hitr()
	{
		/* CALL 'HITRIO'                USING HITR-PARAMS.      <LA4551>*/
		/*                                                      <LA4551>*/
		/* IF  HITR-STATUZ              NOT = O-K               <LA4551>*/
		/* AND HITR-STATUZ              NOT = ENDP              <LA4551>*/
		/*     MOVE HITR-PARAMS         TO SYSR-PARAMS          <LA4551>*/
		/*     PERFORM 9000-FATAL-ERROR                         <LA4551>*/
		/* END-IF.                                              <LA4551>*/
		/*                                                      <LA4551>*/
		/* IF DTHP-CHDR-CHDRCOY        NOT = HITR-CHDRCOY       <LA4551>*/
		/* OR DTHP-CHDR-CHDRNUM        NOT = HITR-CHDRNUM       <LA4551>*/
		/* OR DTHP-LIFE-LIFE           NOT = HITR-LIFE          <LA4551>*/
		/* OR DTHP-COVR-COVERAGE       NOT = HITR-COVERAGE      <LA4551>*/
		/* OR DTHP-COVR-RIDER          NOT = HITR-RIDER         <LA4551>*/
		/* OR DTHP-ELEMENT             NOT = HITR-ZINTBFND      <LA4551>*/
		/*    MOVE ENDP                TO HITR-STATUZ           <LA4551>*/
		/* ELSE                                                 <LA4551>*/
		/*       MOVE HITR-PLAN-SUFFIX TO WSAA-PLAN-SUFFIX      <LA4551>*/
		/*       PERFORM A300-WRITE-HITR                        <LA4551>*/
		/*    END-IF                                            <LA4551>*/
		/* END-IF.                                              <LA4551>*/
		/*                                                      <LA4551>*/
		/* MOVE NEXTR                  TO HITR-FUNCTION.        <LA4551>*/
		SmartFileCode.execute(appVars, hitraloIO);
		if (isNE(hitraloIO.getStatuz(), varcom.oK)
		&& isNE(hitraloIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitraloIO.getParams());
			fatalError9000();
		}
		if (isNE(dthcpy.chdrChdrcoy, hitraloIO.getChdrcoy())
		|| isNE(dthcpy.chdrChdrnum, hitraloIO.getChdrnum())
		|| isNE(dthcpy.lifeLife, hitraloIO.getLife())
		|| isNE(dthcpy.covrCoverage, hitraloIO.getCoverage())
		|| isNE(dthcpy.covrRider, hitraloIO.getRider())
		|| isNE(dthcpy.element, hitraloIO.getZintbfnd())
		|| isEQ(hitraloIO.getStatuz(), varcom.endp)) {
			hitraloIO.setStatuz(varcom.endp);
		}
		else {
			if (isNE(hitraloIO.getFeedbackInd(), "Y")) {
				wsaaPlanSuffix.set(hitraloIO.getPlanSuffix());
				a300WriteHitr();
				hitraloIO.setStatuz(varcom.endp);
				wsaaHitrWritten.set("Y");
			}
		}
		hitraloIO.setFunction(varcom.nextr);
	}

protected void a200ReadHitsWriteHitr()
	{
		a201Hits();
	}

protected void a201Hits()
	{
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(dthcpy.chdrChdrcoy);
		hitsIO.setChdrnum(dthcpy.chdrChdrnum);
		hitsIO.setLife(dthcpy.lifeLife);
		hitsIO.setCoverage(dthcpy.covrCoverage);
		hitsIO.setRider(dthcpy.covrRider);
		hitsIO.setZintbfnd(dthcpy.element);
		hitsIO.setPlanSuffix(ZERO);
		hitsIO.setFunction(varcom.begn);
		

		//performance improvement --  atiwari23 
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","ZINTBFND");
		
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(dthcpy.chdrChdrnum, hitsIO.getChdrnum())
		|| isNE(dthcpy.chdrChdrcoy, hitsIO.getChdrcoy())
		|| isNE(dthcpy.lifeLife, hitsIO.getLife())
		|| isNE(dthcpy.covrCoverage, hitsIO.getCoverage())
		|| isNE(dthcpy.covrRider, hitsIO.getRider())
		|| isNE(dthcpy.element, hitsIO.getZintbfnd())) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		while ( !(isEQ(hitsIO.getStatuz(), varcom.endp))) {

			//performance improvement --  atiwari23 
			hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","ZINTBFND");
		
			a250ReadHits();
		}
		
	}

protected void a250ReadHits()
	{
		/*A251-READ*/
		if (isNE(hitsIO.getZcurprmbal(), 0)) {
			wsaaPlanSuffix.set(hitsIO.getPlanSuffix());
			a300WriteHitr();
		}
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(dthcpy.chdrChdrnum, hitsIO.getChdrnum())
		|| isNE(dthcpy.chdrChdrcoy, hitsIO.getChdrcoy())
		|| isNE(dthcpy.lifeLife, hitsIO.getLife())
		|| isNE(dthcpy.covrCoverage, hitsIO.getCoverage())
		|| isNE(dthcpy.covrRider, hitsIO.getRider())
		|| isNE(dthcpy.element, hitsIO.getZintbfnd())) {
			hitsIO.setStatuz(varcom.endp);
		}
		/*A259-EXIT*/
	}

protected void a300WriteHitr()
	{
		a301Hitr();
	}

protected void a301Hitr()
	{
		hitrIO.setParams(SPACES);
		hitrIO.setTranno(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setPlanSuffix(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setContractAmount(ZERO);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setProcSeqNo(ZERO);
		hitrIO.setSvp(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setChdrcoy(dthcpy.chdrChdrcoy);
		wsaaTriggerChdrcoy.set(dthcpy.chdrChdrcoy);
		hitrIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hitrIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hitrIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hitrIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hitrIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hitrIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		hitrIO.setChdrnum(dthcpy.chdrChdrnum);
		wsaaTriggerChdrnum.set(dthcpy.chdrChdrnum);
		hitrIO.setPlanSuffix(wsaaPlanSuffix);
		hitrIO.setLife(dthcpy.lifeLife);
		wsaaTriggerLife.set(dthcpy.lifeLife);
		hitrIO.setCoverage(dthcpy.covrCoverage);
		wsaaTriggerCoverage.set(dthcpy.covrCoverage);
		hitrIO.setRider(dthcpy.covrRider);
		wsaaTriggerRider.set(dthcpy.covrRider);
		hitrIO.setCrtable(dthcpy.crtable);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		hitrIO.setSurrenderPercent(100);
		hitrIO.setFundAmount(0);
		hitrIO.setZintbfnd(dthcpy.element);
		hitrIO.setCnttyp(dthcpy.cnttype);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			hitrIO.setSacscode(t5645rec.sacscode02);
			hitrIO.setSacstyp(t5645rec.sacstype02);
			hitrIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			hitrIO.setSacscode(t5645rec.sacscode01);
			hitrIO.setSacstyp(t5645rec.sacstype01);
			hitrIO.setGenlcde(t5645rec.glmap01);
		}
		hitrIO.setTriggerModule(t6598rec.addprocess);
		hitrIO.setTriggerKey(wsaaTrigger);
		hitrIO.setTranno(dthcpy.tranno);
		hitrIO.setSvp(1);
		hitrIO.setZrectyp("P");
		hitrIO.setFundCurrency(dthcpy.currcode);
		hitrIO.setCntcurr(dthcpy.contractCurr);
		hitrIO.setEffdate(dthcpy.effdate);
		hitrIO.setZintalloc("N");
		hitrIO.setZintrate(ZERO);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setFormat(hitrrec);
		hitrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			fatalError9000();
		}
	}

//ILIFE-5462 
protected void readReservePricing(){

	zutrpf.setChdrcoy(dthcpy.chdrChdrcoy.toString().trim());
	zutrpf.setChdrnum(dthcpy.chdrChdrnum.toString().trim());
	zutrpf.setTranno(dthcpy.tranno.toInt());
	try {
		zutrpfList = zutrpfDAO.readReservePricing(zutrpf);
	} catch (Exception e) {
		LOGGER.error("Exception occured in readReservePricing()",e);
		fatalError9000();
	}
	if(zutrpfList.size()>0) {
		wsaaRecordFound.set("Y");
	}	

}
protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		dthcpy.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
