/*
 * File: B5371.java
 * Date: 29 August 2009 21:15:53
 * Author: Quipoz Limited
 * 
 * Class transformed from B5371.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import com.csc.life.terminationclaims.dataaccess.dao.RegrpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Regrpf;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*                        CLEAR DOWN REGR FILE
*                        ====================
*
*   This program will be the first process to run within the multi-
*   thread regular payments batch schedule. It will clear down records
*   in regular payments reporting for a given conract range. Using
*   the logical REGRDEL.
*
*   1000 Section
*   ------------
*   Set primary file key
*   Increment control total (records read)
*
*   2000 Section
*   ------------
*   Read REGR    (using readh & REGRDEL logical)
*   Check if record required, if not then release it
*
*   2500 Section
*   ------------
*   Check if contract has been soft-locked previously, if not
*         then soft-lock it
*   Increment control total(records soft locked)
*
*   3000 Section
*   ------------
*   Delete record
*   Increment control total(records deleted)
*
*
*   Control Totals
*   --------------
*   CT01  :  records read
*   CT02  :  records soft-locked
*   CT03  :  records deleted
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5371 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5371");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		
	private int ct01Value = 0;
	private int ct02Value = 0;
	private int ct03Value = 0;
	/*Regular Payment Reporting*/
	private RegrpfDAO regrpfDAO = getApplicationContext().getBean("regrpfDAO", RegrpfDAO.class);
	
	private List<Regrpf> regrpfList;
	private List<Long> deleteRegrpfList;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}

	public B5371() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsspEdterror.set(varcom.oK);
		regrpfList = regrpfDAO.searchRegrpfRecordByCoy(bsprIO.getCompany().toString());
		/*EXIT*/
	}

protected void readFile2000()
	{
		if(regrpfList == null || regrpfList.isEmpty()){
			wsspEdterror.set(varcom.endp);
			return;
		}
		ct01Value = regrpfList.size();
	}

protected void edit2500()
	{
		wsspEdterror.set(varcom.oK);
		for(Regrpf regrpf:regrpfList){
			if (isNE(regrpf.getChdrnum(),wsaaPrevChdrnum)) {
				softlockContract2600(regrpf.getChdrnum());
			}
			if (isEQ(sftlockrec.statuz,"LOCK")) {
				conlogrec.error.set(sftlockrec.statuz);
				conlogrec.params.set(sftlockrec.sftlockRec);
				callConlog003();
				ct02Value++;
				wsspEdterror.set(SPACES);
			}else{
				if(deleteRegrpfList == null){
					deleteRegrpfList = new ArrayList<Long>();
				}
				deleteRegrpfList.add(regrpf.getUniqueNumber());
			}
		}
		regrpfList.clear();
	}

protected void softlockContract2600(String chdrnum)
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.enttyp.set("CH");
		sftlockrec.company.set(bsprIO.getCompany());
		sftlockrec.user.set(99999);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.function.set("LOCK");
		sftlockrec.entity.set(chdrnum);
		sftlockrec.statuz.set(varcom.oK);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		wsaaPrevChdrnum.set(chdrnum);
	}

protected void update3000()
	{
		if(deleteRegrpfList!=null&&!deleteRegrpfList.isEmpty()){
			ct03Value = regrpfDAO.deleteRegrpfRecord(deleteRegrpfList);
			deleteRegrpfList.clear();
		}
	}

protected void commit3500()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(ct01Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct01Value = 0;
		contotrec.totno.set(ct02);
		contotrec.totval.set(ct02Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct02Value = 0;
		contotrec.totno.set(ct03);
		contotrec.totval.set(ct03Value);
		callProgram(Contot.class, contotrec.contotRec);
		ct03Value = 0;
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
