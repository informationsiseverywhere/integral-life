package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:26
 * Description:
 * Copybook name: HCLAANLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hclaanlkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hclaanlFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hclaanlKey = new FixedLengthStringData(256).isAPartOf(hclaanlFileKey, 0, REDEFINE);
  	public FixedLengthStringData hclaanlChdrcoy = new FixedLengthStringData(1).isAPartOf(hclaanlKey, 0);
  	public FixedLengthStringData hclaanlChdrnum = new FixedLengthStringData(8).isAPartOf(hclaanlKey, 1);
  	public FixedLengthStringData hclaanlLife = new FixedLengthStringData(2).isAPartOf(hclaanlKey, 9);
  	public FixedLengthStringData hclaanlCoverage = new FixedLengthStringData(2).isAPartOf(hclaanlKey, 11);
  	public FixedLengthStringData hclaanlRider = new FixedLengthStringData(2).isAPartOf(hclaanlKey, 13);
  	public PackedDecimalData hclaanlAcumactyr = new PackedDecimalData(4, 0).isAPartOf(hclaanlKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(hclaanlKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hclaanlFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hclaanlFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}