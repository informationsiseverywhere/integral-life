package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:10
 * Description:
 * Copybook name: T5606REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5606rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5606Rec = new FixedLengthStringData(getRecSize());
  	public FixedLengthStringData ageIssageFrms = new FixedLengthStringData(24).isAPartOf(t5606Rec, 0);
  	public ZonedDecimalData[] ageIssageFrm = ZDArrayPartOfStructure(8, 3, 0, ageIssageFrms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(ageIssageFrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData ageIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData ageIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData ageIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData ageIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData ageIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData ageIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData ageIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(24).isAPartOf(t5606Rec, 24);
  	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(8, 3, 0, ageIssageTos, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData ageIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData ageIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
  	public ZonedDecimalData ageIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData ageIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 9);
  	public ZonedDecimalData ageIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData ageIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 15);
  	public ZonedDecimalData ageIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData ageIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 21);
  	public FixedLengthStringData benCessageFroms = new FixedLengthStringData(24).isAPartOf(t5606Rec, 48);
  	public ZonedDecimalData[] benCessageFrom = ZDArrayPartOfStructure(8, 3, 0, benCessageFroms, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(benCessageFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData benCessageFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData benCessageFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 3);
  	public ZonedDecimalData benCessageFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 6);
  	public ZonedDecimalData benCessageFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 9);
  	public ZonedDecimalData benCessageFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 12);
  	public ZonedDecimalData benCessageFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 15);
  	public ZonedDecimalData benCessageFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 18);
  	public ZonedDecimalData benCessageFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler2, 21);
  	public FixedLengthStringData benCessageTos = new FixedLengthStringData(24).isAPartOf(t5606Rec, 72);
  	public ZonedDecimalData[] benCessageTo = ZDArrayPartOfStructure(8, 3, 0, benCessageTos, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(24).isAPartOf(benCessageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData benCessageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 0);
  	public ZonedDecimalData benCessageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 3);
  	public ZonedDecimalData benCessageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 6);
  	public ZonedDecimalData benCessageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 9);
  	public ZonedDecimalData benCessageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 12);
  	public ZonedDecimalData benCessageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 15);
  	public ZonedDecimalData benCessageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 18);
  	public ZonedDecimalData benCessageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler3, 21);
  	public FixedLengthStringData benCesstermFrms = new FixedLengthStringData(24).isAPartOf(t5606Rec, 96);
  	public ZonedDecimalData[] benCesstermFrm = ZDArrayPartOfStructure(8, 3, 0, benCesstermFrms, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(benCesstermFrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData benCesstermFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 0);
  	public ZonedDecimalData benCesstermFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 3);
  	public ZonedDecimalData benCesstermFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 6);
  	public ZonedDecimalData benCesstermFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 9);
  	public ZonedDecimalData benCesstermFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 12);
  	public ZonedDecimalData benCesstermFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 15);
  	public ZonedDecimalData benCesstermFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 18);
  	public ZonedDecimalData benCesstermFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler4, 21);
  	public FixedLengthStringData benCesstermTos = new FixedLengthStringData(24).isAPartOf(t5606Rec, 120);
  	public ZonedDecimalData[] benCesstermTo = ZDArrayPartOfStructure(8, 3, 0, benCesstermTos, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(benCesstermTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData benCesstermTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 0);
  	public ZonedDecimalData benCesstermTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 3);
  	public ZonedDecimalData benCesstermTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 6);
  	public ZonedDecimalData benCesstermTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 9);
  	public ZonedDecimalData benCesstermTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 12);
  	public ZonedDecimalData benCesstermTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 15);
  	public ZonedDecimalData benCesstermTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 18);
  	public ZonedDecimalData benCesstermTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler5, 21);
  	public FixedLengthStringData benfreq = new FixedLengthStringData(2).isAPartOf(t5606Rec, 144);
  	public ZonedDecimalData dfrprd = new ZonedDecimalData(3, 0).isAPartOf(t5606Rec, 146);
  	public FixedLengthStringData eaage = new FixedLengthStringData(1).isAPartOf(t5606Rec, 149);
  	public FixedLengthStringData liencds = new FixedLengthStringData(12).isAPartOf(t5606Rec, 150);
  	public FixedLengthStringData[] liencd = FLSArrayPartOfStructure(6, 2, liencds, 0);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(12).isAPartOf(liencds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData liencd01 = new FixedLengthStringData(2).isAPartOf(filler6, 0);
  	public FixedLengthStringData liencd02 = new FixedLengthStringData(2).isAPartOf(filler6, 2);
  	public FixedLengthStringData liencd03 = new FixedLengthStringData(2).isAPartOf(filler6, 4);
  	public FixedLengthStringData liencd04 = new FixedLengthStringData(2).isAPartOf(filler6, 6);
  	public FixedLengthStringData liencd05 = new FixedLengthStringData(2).isAPartOf(filler6, 8);
  	public FixedLengthStringData liencd06 = new FixedLengthStringData(2).isAPartOf(filler6, 10);
  	public FixedLengthStringData mortclss = new FixedLengthStringData(6).isAPartOf(t5606Rec, 162);
  	public FixedLengthStringData[] mortcls = FLSArrayPartOfStructure(6, 1, mortclss, 0);
  	public FixedLengthStringData filler7 = new FixedLengthStringData(6).isAPartOf(mortclss, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mortcls01 = new FixedLengthStringData(1).isAPartOf(filler7, 0);
  	public FixedLengthStringData mortcls02 = new FixedLengthStringData(1).isAPartOf(filler7, 1);
  	public FixedLengthStringData mortcls03 = new FixedLengthStringData(1).isAPartOf(filler7, 2);
  	public FixedLengthStringData mortcls04 = new FixedLengthStringData(1).isAPartOf(filler7, 3);
  	public FixedLengthStringData mortcls05 = new FixedLengthStringData(1).isAPartOf(filler7, 4);
  	public FixedLengthStringData mortcls06 = new FixedLengthStringData(1).isAPartOf(filler7, 5);
  	public FixedLengthStringData premCessageFroms = new FixedLengthStringData(24).isAPartOf(t5606Rec, 168);
  	public ZonedDecimalData[] premCessageFrom = ZDArrayPartOfStructure(8, 3, 0, premCessageFroms, 0);
  	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(premCessageFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCessageFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 0);
  	public ZonedDecimalData premCessageFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 3);
  	public ZonedDecimalData premCessageFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 6);
  	public ZonedDecimalData premCessageFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 9);
  	public ZonedDecimalData premCessageFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 12);
  	public ZonedDecimalData premCessageFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 15);
  	public ZonedDecimalData premCessageFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 18);
  	public ZonedDecimalData premCessageFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler8, 21);
  	public FixedLengthStringData premCessageTos = new FixedLengthStringData(24).isAPartOf(t5606Rec, 192);
  	public ZonedDecimalData[] premCessageTo = ZDArrayPartOfStructure(8, 3, 0, premCessageTos, 0);
  	public FixedLengthStringData filler9 = new FixedLengthStringData(24).isAPartOf(premCessageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCessageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 0);
  	public ZonedDecimalData premCessageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 3);
  	public ZonedDecimalData premCessageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 6);
  	public ZonedDecimalData premCessageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 9);
  	public ZonedDecimalData premCessageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 12);
  	public ZonedDecimalData premCessageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 15);
  	public ZonedDecimalData premCessageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 18);
  	public ZonedDecimalData premCessageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler9, 21);
  	public FixedLengthStringData premCesstermFroms = new FixedLengthStringData(24).isAPartOf(t5606Rec, 216);
  	public ZonedDecimalData[] premCesstermFrom = ZDArrayPartOfStructure(8, 3, 0, premCesstermFroms, 0);
  	public FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(premCesstermFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCesstermFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 0);
  	public ZonedDecimalData premCesstermFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 3);
  	public ZonedDecimalData premCesstermFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 6);
  	public ZonedDecimalData premCesstermFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 9);
  	public ZonedDecimalData premCesstermFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 12);
  	public ZonedDecimalData premCesstermFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 15);
  	public ZonedDecimalData premCesstermFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 18);
  	public ZonedDecimalData premCesstermFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler10, 21);
  	public FixedLengthStringData premCesstermTos = new FixedLengthStringData(24).isAPartOf(t5606Rec, 240);
  	public ZonedDecimalData[] premCesstermTo = ZDArrayPartOfStructure(8, 3, 0, premCesstermTos, 0);
  	public FixedLengthStringData filler11 = new FixedLengthStringData(24).isAPartOf(premCesstermTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premCesstermTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 0);
  	public ZonedDecimalData premCesstermTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 3);
  	public ZonedDecimalData premCesstermTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 6);
  	public ZonedDecimalData premCesstermTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 9);
  	public ZonedDecimalData premCesstermTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 12);
  	public ZonedDecimalData premCesstermTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 15);
  	public ZonedDecimalData premCesstermTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 18);
  	public ZonedDecimalData premCesstermTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler11, 21);
  	public FixedLengthStringData riskCessageFroms = new FixedLengthStringData(24).isAPartOf(t5606Rec, 264);
  	public ZonedDecimalData[] riskCessageFrom = ZDArrayPartOfStructure(8, 3, 0, riskCessageFroms, 0);
  	public FixedLengthStringData filler12 = new FixedLengthStringData(24).isAPartOf(riskCessageFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData riskCessageFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 0);
  	public ZonedDecimalData riskCessageFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 3);
  	public ZonedDecimalData riskCessageFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 6);
  	public ZonedDecimalData riskCessageFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 9);
  	public ZonedDecimalData riskCessageFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 12);
  	public ZonedDecimalData riskCessageFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 15);
  	public ZonedDecimalData riskCessageFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 18);
  	public ZonedDecimalData riskCessageFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler12, 21);
  	public FixedLengthStringData riskCessageTos = new FixedLengthStringData(24).isAPartOf(t5606Rec, 288);
  	public ZonedDecimalData[] riskCessageTo = ZDArrayPartOfStructure(8, 3, 0, riskCessageTos, 0);
  	public FixedLengthStringData filler13 = new FixedLengthStringData(24).isAPartOf(riskCessageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData riskCessageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 0);
  	public ZonedDecimalData riskCessageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 3);
  	public ZonedDecimalData riskCessageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 6);
  	public ZonedDecimalData riskCessageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 9);
  	public ZonedDecimalData riskCessageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 12);
  	public ZonedDecimalData riskCessageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 15);
  	public ZonedDecimalData riskCessageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 18);
  	public ZonedDecimalData riskCessageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler13, 21);
  	public FixedLengthStringData riskCesstermFroms = new FixedLengthStringData(24).isAPartOf(t5606Rec, 312);
  	public ZonedDecimalData[] riskCesstermFrom = ZDArrayPartOfStructure(8, 3, 0, riskCesstermFroms, 0);
  	public FixedLengthStringData filler14 = new FixedLengthStringData(24).isAPartOf(riskCesstermFroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData riskCesstermFrom01 = new ZonedDecimalData(3, 0).isAPartOf(filler14, 0);
  	public ZonedDecimalData riskCesstermFrom02 = new ZonedDecimalData(3, 0).isAPartOf(filler14, 3);
  	public ZonedDecimalData riskCesstermFrom03 = new ZonedDecimalData(3, 0).isAPartOf(filler14, 6);
  	public ZonedDecimalData riskCesstermFrom04 = new ZonedDecimalData(3, 0).isAPartOf(filler14, 9);
  	public ZonedDecimalData riskCesstermFrom05 = new ZonedDecimalData(3, 0).isAPartOf(filler14, 12);
  	public ZonedDecimalData riskCesstermFrom06 = new ZonedDecimalData(3, 0).isAPartOf(filler14, 15);
  	public ZonedDecimalData riskCesstermFrom07 = new ZonedDecimalData(3, 0).isAPartOf(filler14, 18);
  	public ZonedDecimalData riskCesstermFrom08 = new ZonedDecimalData(3, 0).isAPartOf(filler14, 21);
  	public FixedLengthStringData riskCesstermTos = new FixedLengthStringData(24).isAPartOf(t5606Rec, 336);
  	public ZonedDecimalData[] riskCesstermTo = ZDArrayPartOfStructure(8, 3, 0, riskCesstermTos, 0);
  	public FixedLengthStringData filler15 = new FixedLengthStringData(24).isAPartOf(riskCesstermTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData riskCesstermTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler15, 0);
  	public ZonedDecimalData riskCesstermTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler15, 3);
  	public ZonedDecimalData riskCesstermTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler15, 6);
  	public ZonedDecimalData riskCesstermTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler15, 9);
  	public ZonedDecimalData riskCesstermTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler15, 12);
  	public ZonedDecimalData riskCesstermTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler15, 15);
  	public ZonedDecimalData riskCesstermTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler15, 18);
  	public ZonedDecimalData riskCesstermTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler15, 21);
  	public FixedLengthStringData specind = new FixedLengthStringData(1).isAPartOf(t5606Rec, 360);
  	public ZonedDecimalData sumInsMax = new ZonedDecimalData(15, 0).isAPartOf(t5606Rec, 361);
  	public ZonedDecimalData sumInsMin = new ZonedDecimalData(15, 0).isAPartOf(t5606Rec, 376);
  	public FixedLengthStringData termIssageFrms = new FixedLengthStringData(24).isAPartOf(t5606Rec, 391);
  	public ZonedDecimalData[] termIssageFrm = ZDArrayPartOfStructure(8, 3, 0, termIssageFrms, 0);
  	public FixedLengthStringData filler16 = new FixedLengthStringData(24).isAPartOf(termIssageFrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData termIssageFrm01 = new ZonedDecimalData(3, 0).isAPartOf(filler16, 0);
  	public ZonedDecimalData termIssageFrm02 = new ZonedDecimalData(3, 0).isAPartOf(filler16, 3);
  	public ZonedDecimalData termIssageFrm03 = new ZonedDecimalData(3, 0).isAPartOf(filler16, 6);
  	public ZonedDecimalData termIssageFrm04 = new ZonedDecimalData(3, 0).isAPartOf(filler16, 9);
  	public ZonedDecimalData termIssageFrm05 = new ZonedDecimalData(3, 0).isAPartOf(filler16, 12);
  	public ZonedDecimalData termIssageFrm06 = new ZonedDecimalData(3, 0).isAPartOf(filler16, 15);
  	public ZonedDecimalData termIssageFrm07 = new ZonedDecimalData(3, 0).isAPartOf(filler16, 18);
  	public ZonedDecimalData termIssageFrm08 = new ZonedDecimalData(3, 0).isAPartOf(filler16, 21);
  	public FixedLengthStringData termIssageTos = new FixedLengthStringData(24).isAPartOf(t5606Rec, 415);
  	public ZonedDecimalData[] termIssageTo = ZDArrayPartOfStructure(8, 3, 0, termIssageTos, 0);
  	public FixedLengthStringData filler17 = new FixedLengthStringData(24).isAPartOf(termIssageTos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData termIssageTo01 = new ZonedDecimalData(3, 0).isAPartOf(filler17, 0);
  	public ZonedDecimalData termIssageTo02 = new ZonedDecimalData(3, 0).isAPartOf(filler17, 3);
  	public ZonedDecimalData termIssageTo03 = new ZonedDecimalData(3, 0).isAPartOf(filler17, 6);
  	public ZonedDecimalData termIssageTo04 = new ZonedDecimalData(3, 0).isAPartOf(filler17, 9);
  	public ZonedDecimalData termIssageTo05 = new ZonedDecimalData(3, 0).isAPartOf(filler17, 12);
  	public ZonedDecimalData termIssageTo06 = new ZonedDecimalData(3, 0).isAPartOf(filler17, 15);
  	public ZonedDecimalData termIssageTo07 = new ZonedDecimalData(3, 0).isAPartOf(filler17, 18);
  	public ZonedDecimalData termIssageTo08 = new ZonedDecimalData(3, 0).isAPartOf(filler17, 21);
  	public FixedLengthStringData waitperiods = new FixedLengthStringData(27).isAPartOf(t5606Rec,439);
	public FixedLengthStringData[] waitperiod = FLSArrayPartOfStructure(9, 3, waitperiods, 0);
	public FixedLengthStringData filler22 = new FixedLengthStringData(27).isAPartOf(waitperiods, 0, FILLER_REDEFINE);
	public FixedLengthStringData waitperiod01 =new FixedLengthStringData(3).isAPartOf(filler22,0);
	public FixedLengthStringData waitperiod02 =new FixedLengthStringData(3).isAPartOf(filler22,3);
	public FixedLengthStringData waitperiod03 =new FixedLengthStringData(3).isAPartOf(filler22,6);
	public FixedLengthStringData waitperiod04 =new FixedLengthStringData(3).isAPartOf(filler22,9);
	public FixedLengthStringData waitperiod05 =new FixedLengthStringData(3).isAPartOf(filler22,12);
	public FixedLengthStringData waitperiod06 =new FixedLengthStringData(3).isAPartOf(filler22,15);
	public FixedLengthStringData waitperiod07 =new FixedLengthStringData(3).isAPartOf(filler22,18);
	public FixedLengthStringData waitperiod08 =new FixedLengthStringData(3).isAPartOf(filler22,21);
	public FixedLengthStringData waitperiod09 =new FixedLengthStringData(3).isAPartOf(filler22,24);
	public FixedLengthStringData bentrms = new FixedLengthStringData(18).isAPartOf(t5606Rec,466);
	public FixedLengthStringData[] bentrm = FLSArrayPartOfStructure(9, 2, bentrms, 0);
	public FixedLengthStringData filler19 = new FixedLengthStringData(18).isAPartOf(bentrms, 0, FILLER_REDEFINE);
	public FixedLengthStringData bentrm01 = new FixedLengthStringData(2).isAPartOf(filler19,0);
	public FixedLengthStringData bentrm02 = new FixedLengthStringData(2).isAPartOf(filler19,2);
	public FixedLengthStringData bentrm03 = new FixedLengthStringData(2).isAPartOf(filler19,4);
	public FixedLengthStringData bentrm04 = new FixedLengthStringData(2).isAPartOf(filler19,6);
	public FixedLengthStringData bentrm05 = new FixedLengthStringData(2).isAPartOf(filler19,8);
	public FixedLengthStringData bentrm06 = new FixedLengthStringData(2).isAPartOf(filler19,10);
	public FixedLengthStringData bentrm07 = new FixedLengthStringData(2).isAPartOf(filler19,12);
	public FixedLengthStringData bentrm08 = new FixedLengthStringData(2).isAPartOf(filler19,14);
	public FixedLengthStringData bentrm09 = new FixedLengthStringData(2).isAPartOf(filler19,16);
	public FixedLengthStringData poltyps = new FixedLengthStringData(6).isAPartOf(t5606Rec,484);
	public FixedLengthStringData[] poltyp = FLSArrayPartOfStructure(6, 1, poltyps, 0);
	public FixedLengthStringData filler20 = new FixedLengthStringData(6).isAPartOf(poltyps, 0, FILLER_REDEFINE);
	public FixedLengthStringData poltyp01 = new FixedLengthStringData(1).isAPartOf(filler20,0);
	public FixedLengthStringData poltyp02 = new FixedLengthStringData(1).isAPartOf(filler20,1);
	public FixedLengthStringData poltyp03 = new FixedLengthStringData(1).isAPartOf(filler20,2);
	public FixedLengthStringData poltyp04 = new FixedLengthStringData(1).isAPartOf(filler20,3);
	public FixedLengthStringData poltyp05 = new FixedLengthStringData(1).isAPartOf(filler20,4);
	public FixedLengthStringData poltyp06 = new FixedLengthStringData(1).isAPartOf(filler20,5);
	public FixedLengthStringData prmbasiss = new FixedLengthStringData(6).isAPartOf(t5606Rec,490);
	public FixedLengthStringData[] prmbasis = FLSArrayPartOfStructure(6, 1, prmbasiss, 0);
	public FixedLengthStringData filler21 = new FixedLengthStringData(6).isAPartOf(prmbasiss, 0, FILLER_REDEFINE);
	public FixedLengthStringData prmbasis01 = new FixedLengthStringData(1).isAPartOf(filler21,0);
	public FixedLengthStringData prmbasis02 = new FixedLengthStringData(1).isAPartOf(filler21,1);
	public FixedLengthStringData prmbasis03 = new FixedLengthStringData(1).isAPartOf(filler21,2);
	public FixedLengthStringData prmbasis04 = new FixedLengthStringData(1).isAPartOf(filler21,3);
	public FixedLengthStringData prmbasis05 = new FixedLengthStringData(1).isAPartOf(filler21,4);
	public FixedLengthStringData prmbasis06 = new FixedLengthStringData(1).isAPartOf(filler21,5);
	public FixedLengthStringData filler18 = new FixedLengthStringData(getFillerSize()).isAPartOf(t5606Rec, 496, FILLER);

	public void initialize() {
		COBOLFunctions.initialize(t5606Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5606Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	public int getRecSize() {
		return 500;
	}
	
	public int getFillerSize(){
		return 4;
	}

}