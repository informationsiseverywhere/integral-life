package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class Sa507screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sa507screensfl";
		lrec.subfileClass = S5186screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 14;
		lrec.pageSubfile = 13;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 8, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa507ScreenVars sv = (Sa507ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa507screenctlWritten, sv.Sa507screensflWritten, av, sv.sa507screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sa507ScreenVars screenVars = (Sa507ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.chdrnumflg.setClassString("");
	}

/**
 * Clear all the variables in S5186screenctl
 */
	public static void clear(VarModel pv) {
		Sa507ScreenVars screenVars = (Sa507ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.lifcnum.clear();
		screenVars.lifename.clear();
		screenVars.chdrnumflg.clear();
	}
}
