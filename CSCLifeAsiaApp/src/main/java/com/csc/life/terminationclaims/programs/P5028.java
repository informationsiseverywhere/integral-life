/*
 * File: P5028.java
 * Date: 29 August 2009 23:57:41
 * Author: Quipoz Limited
 * 
 * Class transformed from P5028.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* P5028 - Maturity/Expiry End Processing.
* ---------------------------------------
*
* This program will initiate the final processing of the
* on-line portion of Maturity/Expiry. It has no screen
* and is performed after all the on-line processing has
* been completed.
*
*    Initialise
*    ----------
*
*     RETRV the Contract Header record.
*
*     Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
*     Call the ATREQ module to submit P5028AT which will
*     complete the processing. Set the primary key for AT
*     as the Company, Contract Number, Termid and User.
*
*
*****************************************************************
* </pre>
*/
public class P5028 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5028");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	protected FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	protected FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(209);
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 0);
	private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 14);
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 20);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 26);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 30);
	private FixedLengthStringData filler1 = new FixedLengthStringData(178).isAPartOf(wsaaTransactionRec, 31, FILLER).init(SPACES);
		/* FORMATS */
	private String chdrmatrec = "CHDRMATREC";
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	protected Atreqrec atreqrec = new Atreqrec();
		/*Contract Header Maturities*/
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	protected Sftlockrec sftlockrec = new Sftlockrec();
	protected Batckey wsaaBatckey = new Batckey();
	protected Chdrpf chdrpf;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);

	public P5028() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		chdrpf = new Chdrpf();
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf){
		chdrmatIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmatIO);
		if (isNE(chdrmatIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmatIO.getParams());
			fatalError600();
		}
			else {
				chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsspcomn.company.toString(),chdrmatIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
			}
		} 
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		chdrpfDAO.updateChdrpfRecord(chdrpf) ;
	}

protected void screenEdit2000()
	{
		/*GO*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		go3010();
	}

protected void go3010()
	{
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5028AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrpf.getChdrnum());
		wsaaFsuco.set(wsspcomn.fsuco);
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaEffectiveDate.set(wsspcomn.currfrom);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
