package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5256screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {17, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5256ScreenVars sv = (S5256ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5256screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5256ScreenVars screenVars = (S5256ScreenVars)pv;
		screenVars.policyloan.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.causeofdth.setClassString("");
		screenVars.otheradjst.setClassString("");
		screenVars.fupflg.setClassString("");
		screenVars.estimateTotalValue.setClassString("");
		screenVars.clamamt.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.resndesc.setClassString("");
		screenVars.dtofdeathDisp.setClassString("");
		screenVars.currcd.setClassString("");
		screenVars.zrcshamt.setClassString("");
		screenVars.tdbtamt.setClassString("");
		screenVars.contactDateDisp.setClassString("");
		//ILIFE-1137
		//added for death claim flexibility
		screenVars.susamt.setClassString("");	
		screenVars.nextinsamt.setClassString("");
		screenVars.reserveUnitsInd.setClassString("");//ILIFE-5462
		screenVars.reserveUnitsDateDisp.setClassString("");//ILIFE-5462
		screenVars.bnfying.setClassString("");//CLM015
		screenVars.claimnumber.setClassString("");
		screenVars.notifinumber.setClassString("");
		screenVars.claimnotes.setClassString("");
		screenVars.investres.setClassString("");
		screenVars.riskcommdteDisp.setClassString("");	//ILJ-48
	}

/**
 * Clear all the variables in S5256screen
 */
	public static void clear(VarModel pv) {
		S5256ScreenVars screenVars = (S5256ScreenVars) pv;
		screenVars.policyloan.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.causeofdth.clear();
		screenVars.otheradjst.clear();
		screenVars.fupflg.clear();
		screenVars.estimateTotalValue.clear();
		screenVars.clamamt.clear();
		screenVars.reasoncd.clear();
		screenVars.resndesc.clear();
		screenVars.dtofdeathDisp.clear();
		screenVars.dtofdeath.clear();
		screenVars.currcd.clear();
		screenVars.zrcshamt.clear();
		screenVars.tdbtamt.clear();
		//ILIFE-1137
		//added for death claim flexibility
		screenVars.susamt.clear();
		screenVars.nextinsamt.clear();	
		screenVars.reserveUnitsInd.clear();//ILIFE-5462
		screenVars.reserveUnitsDateDisp.clear();//ILIFE-5462
		screenVars.bnfying.clear();//CLM015
		screenVars.claimnumber.clear();
		screenVars.notifinumber.clear();
		screenVars.claimnotes.clear();
		screenVars.investres.clear();
		screenVars.riskcommdteDisp.clear();	//ILJ-48
		screenVars.riskcommdte.clear();		//ILJ-48
		screenVars.contactDate.clear();
		screenVars.contactDateDisp.clear();
	}
}
