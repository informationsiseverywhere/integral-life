package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MatdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:48
 * Class transformed from MATDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MatdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 132;
	public FixedLengthStringData matdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData matdpfRecord = matdrec;
	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(matdrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(matdrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(matdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(matdrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(matdrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(matdrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(matdrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(matdrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(matdrec);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(matdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(matdrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(matdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(matdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(matdrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(matdrec);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(matdrec);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(matdrec);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(matdrec);
	public PackedDecimalData actvalue = DD.actvalue.copy().isAPartOf(matdrec);
	public PackedDecimalData estMatValue = DD.emv.copy().isAPartOf(matdrec);
	public FixedLengthStringData virtualFund = DD.vrtfund.copy().isAPartOf(matdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(matdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(matdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(matdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public MatdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for MatdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MatdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MatdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MatdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MatdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MatdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MATDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRNUM, " +
							"CHDRCOY, " +
							"PLNSFX, " +
							"TRANNO, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"EFFDATE, " +
							"CURRCD, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"SHORTDS, " +
							"LIENCD, " +
							"TYPE_T, " +
							"ACTVALUE, " +
							"EMV, " +
							"VRTFUND, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrnum,
                                     chdrcoy,
                                     planSuffix,
                                     tranno,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     effdate,
                                     currcd,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     crtable,
                                     shortds,
                                     liencd,
                                     fieldType,
                                     actvalue,
                                     estMatValue,
                                     virtualFund,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrnum.clear();
  		chdrcoy.clear();
  		planSuffix.clear();
  		tranno.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		effdate.clear();
  		currcd.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		shortds.clear();
  		liencd.clear();
  		fieldType.clear();
  		actvalue.clear();
  		estMatValue.clear();
  		virtualFund.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMatdrec() {
  		return matdrec;
	}

	public FixedLengthStringData getMatdpfRecord() {
  		return matdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMatdrec(what);
	}

	public void setMatdrec(Object what) {
  		this.matdrec.set(what);
	}

	public void setMatdpfRecord(Object what) {
  		this.matdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(matdrec.getLength());
		result.set(matdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}