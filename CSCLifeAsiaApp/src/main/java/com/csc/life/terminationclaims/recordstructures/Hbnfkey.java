package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:26
 * Description:
 * Copybook name: HBNFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hbnfkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hbnfFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hbnfKey = new FixedLengthStringData(256).isAPartOf(hbnfFileKey, 0, REDEFINE);
  	public FixedLengthStringData hbnfChdrcoy = new FixedLengthStringData(1).isAPartOf(hbnfKey, 0);
  	public FixedLengthStringData hbnfChdrnum = new FixedLengthStringData(8).isAPartOf(hbnfKey, 1);
  	public FixedLengthStringData hbnflife  = new FixedLengthStringData(2).isAPartOf(hbnfKey, 9);
  	public FixedLengthStringData hbnfcoverage  = new FixedLengthStringData(2).isAPartOf(hbnfKey, 11);
  	public FixedLengthStringData hbnfrider  = new FixedLengthStringData(2).isAPartOf(hbnfKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(hbnfKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hbnfFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hbnfFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}