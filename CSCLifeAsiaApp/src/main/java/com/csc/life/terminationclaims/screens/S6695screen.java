package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6695screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6695ScreenVars sv = (S6695ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6695screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6695ScreenVars screenVars = (S6695ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.tupyr01.setClassString("");
		screenVars.pctinc01.setClassString("");
		screenVars.tupyr02.setClassString("");
		screenVars.pctinc02.setClassString("");
		screenVars.tupyr03.setClassString("");
		screenVars.pctinc03.setClassString("");
		screenVars.tupyr04.setClassString("");
		screenVars.pctinc04.setClassString("");
		screenVars.tupyr05.setClassString("");
		screenVars.pctinc05.setClassString("");
		screenVars.tupyr06.setClassString("");
		screenVars.pctinc06.setClassString("");
		screenVars.tupyr07.setClassString("");
		screenVars.pctinc07.setClassString("");
		screenVars.tupyr08.setClassString("");
		screenVars.pctinc08.setClassString("");
		screenVars.tupyr09.setClassString("");
		screenVars.pctinc09.setClassString("");
		screenVars.tupyr10.setClassString("");
		screenVars.pctinc10.setClassString("");
		screenVars.tupyr11.setClassString("");
		screenVars.pctinc11.setClassString("");
		screenVars.tupyr12.setClassString("");
		screenVars.pctinc12.setClassString("");
		screenVars.contitem.setClassString("");
	}

/**
 * Clear all the variables in S6695screen
 */
	public static void clear(VarModel pv) {
		S6695ScreenVars screenVars = (S6695ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.tupyr01.clear();
		screenVars.pctinc01.clear();
		screenVars.tupyr02.clear();
		screenVars.pctinc02.clear();
		screenVars.tupyr03.clear();
		screenVars.pctinc03.clear();
		screenVars.tupyr04.clear();
		screenVars.pctinc04.clear();
		screenVars.tupyr05.clear();
		screenVars.pctinc05.clear();
		screenVars.tupyr06.clear();
		screenVars.pctinc06.clear();
		screenVars.tupyr07.clear();
		screenVars.pctinc07.clear();
		screenVars.tupyr08.clear();
		screenVars.pctinc08.clear();
		screenVars.tupyr09.clear();
		screenVars.pctinc09.clear();
		screenVars.tupyr10.clear();
		screenVars.pctinc10.clear();
		screenVars.tupyr11.clear();
		screenVars.pctinc11.clear();
		screenVars.tupyr12.clear();
		screenVars.pctinc12.clear();
		screenVars.contitem.clear();
	}
}
