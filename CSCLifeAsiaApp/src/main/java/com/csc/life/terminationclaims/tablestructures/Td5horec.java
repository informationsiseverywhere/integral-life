package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Td5horec  extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData td5hoRec = new FixedLengthStringData(200);
	  	public FixedLengthStringData remamons = new FixedLengthStringData(60).isAPartOf(td5hoRec, 0);
	  	public ZonedDecimalData[] remamon = ZDArrayPartOfStructure(20, 3, 0, remamons, 0);
	  	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(remamons, 0, FILLER_REDEFINE);
	  	public ZonedDecimalData remamon01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
	  	public ZonedDecimalData remamon02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
	  	public ZonedDecimalData remamon03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
	  	public ZonedDecimalData remamon04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
	  	public ZonedDecimalData remamon05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
	  	public ZonedDecimalData remamon06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
	  	public ZonedDecimalData remamon07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
	  	public ZonedDecimalData remamon08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
	  	public ZonedDecimalData remamon09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
	  	public ZonedDecimalData remamon10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
	  	public ZonedDecimalData remamon11 = new ZonedDecimalData(3, 0).isAPartOf(filler, 30);
	  	public ZonedDecimalData remamon12 = new ZonedDecimalData(3, 0).isAPartOf(filler, 33);
	  	public ZonedDecimalData remamon13 = new ZonedDecimalData(3, 0).isAPartOf(filler, 36);
	  	public ZonedDecimalData remamon14 = new ZonedDecimalData(3, 0).isAPartOf(filler, 39);
	  	public ZonedDecimalData remamon15 = new ZonedDecimalData(3, 0).isAPartOf(filler, 42);
	  	public ZonedDecimalData remamon16 = new ZonedDecimalData(3, 0).isAPartOf(filler, 45);
	  	public ZonedDecimalData remamon17 = new ZonedDecimalData(3, 0).isAPartOf(filler, 48);
	  	public ZonedDecimalData remamon18 = new ZonedDecimalData(3, 0).isAPartOf(filler, 51);
	  	public ZonedDecimalData remamon19 = new ZonedDecimalData(3, 0).isAPartOf(filler, 54);
	  	public ZonedDecimalData remamon20 = new ZonedDecimalData(3, 0).isAPartOf(filler, 57);
	  	public FixedLengthStringData rates = new FixedLengthStringData(120).isAPartOf(td5hoRec, 60);
	  	public ZonedDecimalData[] rate = ZDArrayPartOfStructure(20, 6, 4, rates, 0);
	  	public FixedLengthStringData filler1 = new FixedLengthStringData(120).isAPartOf(rates, 0, FILLER_REDEFINE);
		public ZonedDecimalData rate01 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 0);
		public ZonedDecimalData rate02 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 6);
		public ZonedDecimalData rate03 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 12);
		public ZonedDecimalData rate04 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 18);
		public ZonedDecimalData rate05 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 24);
		public ZonedDecimalData rate06 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 30);
		public ZonedDecimalData rate07 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 36);
		public ZonedDecimalData rate08 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 42);
		public ZonedDecimalData rate09 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 48);
		public ZonedDecimalData rate10 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 54);
		public ZonedDecimalData rate11 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 60);
		public ZonedDecimalData rate12 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 66);
		public ZonedDecimalData rate13 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 72);
		public ZonedDecimalData rate14 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 78);
		public ZonedDecimalData rate15 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 84);
		public ZonedDecimalData rate16 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 90);
		public ZonedDecimalData rate17 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 96);
		public ZonedDecimalData rate18 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 102);
		public ZonedDecimalData rate19 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 108);
		public ZonedDecimalData rate20 = new ZonedDecimalData(6, 4).isAPartOf(filler1, 114);
	  	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(td5hoRec, 180, FILLER);


		public void initialize() {
			COBOLFunctions.initialize(td5hoRec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			td5hoRec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}

}
