package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6678
 * @version 1.0 generated on 30/08/09 06:57
 * @author Quipoz
 */
public class S6678ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(3);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(dataFields, 0, FILLER);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 1);
	public FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(errorIndicators, 0, FILLER);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 2);
	public FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(outputIndicators, 0, FILLER);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(345);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(103).isAPartOf(subfileArea, 0);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(subfileFields,8);
	public ZonedDecimalData exprdate = DD.exprdate.copyToZonedDecimal().isAPartOf(subfileFields,18);
	public FixedLengthStringData fupcdes = DD.fupcdes.copy().isAPartOf(subfileFields,26);
	public ZonedDecimalData fupremdt = DD.fupdt.copyToZonedDecimal().isAPartOf(subfileFields,30);
	public ZonedDecimalData fuprcvd = DD.fuprcvd.copyToZonedDecimal().isAPartOf(subfileFields,38);
	public FixedLengthStringData fupremk = DD.fuprmk.copy().isAPartOf(subfileFields,46);
	public FixedLengthStringData fupstat = DD.fupsts.copy().isAPartOf(subfileFields,86);
	public FixedLengthStringData fuptype = DD.fuptyp.copy().isAPartOf(subfileFields,87);
	public ZonedDecimalData hseqno = DD.hseqno.copyToZonedDecimal().isAPartOf(subfileFields,88);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(subfileFields,91);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(subfileFields,92);
	public FixedLengthStringData sfflg = DD.sfflg.copy().isAPartOf(subfileFields,93);
	public FixedLengthStringData updteflag = DD.updteflag.copy().isAPartOf(subfileFields,94);
	public FixedLengthStringData zitem = DD.zitem.copy().isAPartOf(subfileFields,95);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(60).isAPartOf(subfileArea, 103);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData crtuserErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData exprdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData fupcdesErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData fupdtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData fuprcvdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData fuprmkErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData fupstsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData fuptypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData hseqnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData sfflgErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData updteflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData zitemErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(180).isAPartOf(subfileArea, 163);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] crtuserOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] exprdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] fupcdesOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] fupdtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] fuprcvdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] fuprmkOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] fupstsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] fuptypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] hseqnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] sfflgOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] updteflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] zitemOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 343);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData exprdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fupremdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fuprcvdDisp = new FixedLengthStringData(10);

	public LongData S6678screensflWritten = new LongData(0);
	public LongData S6678screenctlWritten = new LongData(0);
	public LongData S6678screenWritten = new LongData(0);
	public LongData S6678windowWritten = new LongData(0);
	public LongData S6678hideWritten = new LongData(0);
	public LongData S6678protectWritten = new LongData(0);
	public GeneralTable s6678screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6678screensfl;
	}

	public S6678ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(fupcdesOut,new String[] {"30","38","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupstsOut,new String[] {"02","32","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupdtOut,new String[] {"03","32","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuprmkOut,new String[] {"04","33","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sfflgOut,new String[] {"05","34","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuptypOut,new String[] {null, null, null, "40",null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuprcvdOut,new String[] {"41","43","-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(exprdateOut,new String[] {"42","44","-42",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {updteflag, hseqno, crtuser, language, indic, zitem, fupcdes, fupstat, fupremdt, crtdate, fupremk, sfflg, fuptype, fuprcvd, exprdate};
		screenSflOutFields = new BaseData[][] {updteflagOut, hseqnoOut, crtuserOut, languageOut, indicOut, zitemOut, fupcdesOut, fupstsOut, fupdtOut, crtdateOut, fuprmkOut, sfflgOut, fuptypOut, fuprcvdOut, exprdateOut};
		screenSflErrFields = new BaseData[] {updteflagErr, hseqnoErr, crtuserErr, languageErr, indicErr, zitemErr, fupcdesErr, fupstsErr, fupdtErr, crtdateErr, fuprmkErr, sfflgErr, fuptypErr, fuprcvdErr, exprdateErr};
		screenSflDateFields = new BaseData[] {fupremdt, crtdate, fuprcvd, exprdate};
		screenSflDateErrFields = new BaseData[] {fupdtErr, crtdateErr, fuprcvdErr, exprdateErr};
		screenSflDateDispFields = new BaseData[] {fupremdtDisp, crtdateDisp, fuprcvdDisp, exprdateDisp};

		screenFields = new BaseData[] {};
		screenOutFields = new BaseData[][] {};
		screenErrFields = new BaseData[] {};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6678screen.class;
		screenSflRecord = S6678screensfl.class;
		screenCtlRecord = S6678screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6678protect.class;
		hideRecord = S6678hide.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6678screenctl.lrec.pageSubfile);
	}
}
