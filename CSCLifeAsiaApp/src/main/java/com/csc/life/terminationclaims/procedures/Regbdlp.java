/*
 * File: Regbdlp.java
 * Date: 30 August 2009 2:04:32
 * Author: Quipoz Limited
 * 
 * Class transformed from REGBDLP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.recordstructures.Compdelrec;
import com.csc.life.terminationclaims.dataaccess.AnntlnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegtlnbTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*            DELETE REGULAR BENEFIT PROPOSAL COMPONENTS
*
*
*  PROGRAMMING NOTES
* --------------------
*
* OVERVIEW
*
*    This program may be called to delete Regular Benefit
*    component records. These are as follows:
*
*    1) Delete all Annuity Detail (ANNT) components for a
*       coverage.
*
*    2) Delete all Regular Payment (REGT) components for a
*       rider.
*
*     It will be called from P5306 as part of generic processing
*     ie. T5671 subroutine call.
*
*     The process is invoked when deleting a component during
*     New Business proposal.
*
* UPDATING
* --------
* - Read then delete all records for the Company , contract
*   life , coverage and rider passed.
*
* NEXT
* ----
*
* Exit program and return to called from program. (P5306)
*
*
*
*****************************************************************
* </pre>
*/
public class Regbdlp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("REGBDLP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String regtlnbrec = "REGTLNBREC";
		/*Annuity Details - Temporary*/
	private AnntlnbTableDAM anntlnbIO = new AnntlnbTableDAM();
	private Compdelrec compdelrec = new Compdelrec();
		/*Regular Payment Temporary Record New Bus*/
	private RegtlnbTableDAM regtlnbIO = new RegtlnbTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Wsspcomn wsspcomn = new Wsspcomn();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Regbdlp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 1);
		compdelrec.compdelRec = convertAndSetParam(compdelrec.compdelRec, parmArray, 0);
		try {
			initialise1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		retrieveAnntlnb1020();
		retrieveRegtlnb1030();
		exit1090();
	}

protected void initialise1010()
	{
		compdelrec.statuz.set(varcom.oK);
		anntlnbIO.setChdrcoy(compdelrec.chdrcoy);
		anntlnbIO.setChdrnum(compdelrec.chdrnum);
		anntlnbIO.setLife(compdelrec.life);
		anntlnbIO.setCoverage(compdelrec.coverage);
		anntlnbIO.setRider(compdelrec.rider);
		anntlnbIO.setSeqnbr(ZERO);
	}

protected void retrieveAnntlnb1020()
	{
		anntlnbIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		anntlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		anntlnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, anntlnbIO);
		if (isNE(anntlnbIO.getStatuz(),varcom.oK)
		&& isNE(anntlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(anntlnbIO.getParams());
			fatalError600();
		}
		while ( !(isEQ(anntlnbIO.getStatuz(),varcom.endp))) {
			processAnnt2000();
		}
		
	}

protected void retrieveRegtlnb1030()
	{
		regtlnbIO.setFunction(varcom.rlse);
		regtlnbIO.setFormat(regtlnbrec);
		SmartFileCode.execute(appVars, regtlnbIO);
		if (isNE(regtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtlnbIO.getParams());
			fatalError600();
		}
		regtlnbIO.setChdrcoy(compdelrec.chdrcoy);
		regtlnbIO.setChdrnum(compdelrec.chdrnum);
		regtlnbIO.setLife(compdelrec.life);
		regtlnbIO.setCoverage(compdelrec.coverage);
		regtlnbIO.setRider(compdelrec.rider);
		regtlnbIO.setSeqnbr(ZERO);
		regtlnbIO.setRgpynum(ZERO);
		regtlnbIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		regtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regtlnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		regtlnbIO.setFormat(regtlnbrec);
		SmartFileCode.execute(appVars, regtlnbIO);
		if (isNE(regtlnbIO.getStatuz(),varcom.oK)
		&& isNE(regtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(regtlnbIO.getParams());
			fatalError600();
		}
		while ( !(isEQ(regtlnbIO.getStatuz(),varcom.endp))) {
			processRegt3000();
		}
		
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void processAnnt2000()
	{
		/*START*/
		if (isNE(compdelrec.chdrcoy,anntlnbIO.getChdrcoy())
		|| isNE(compdelrec.chdrnum,anntlnbIO.getChdrnum())
		|| isNE(compdelrec.life,anntlnbIO.getLife())
		|| isNE(compdelrec.coverage,anntlnbIO.getCoverage())
		|| isNE(compdelrec.rider,anntlnbIO.getRider())) {
			anntlnbIO.setStatuz(varcom.endp);
		}
		else {
			deleteAnnt2100();
		}
		/*EXIT*/
	}

protected void deleteAnnt2100()
	{
		/*START*/
		anntlnbIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, anntlnbIO);
		if (isNE(anntlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(anntlnbIO.getParams());
			fatalError600();
		}
		else {
			anntlnbIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, anntlnbIO);
			if (isNE(anntlnbIO.getStatuz(),varcom.oK)
			&& isNE(anntlnbIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(anntlnbIO.getParams());
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void processRegt3000()
	{
		/*START*/
		if (isNE(compdelrec.chdrcoy,regtlnbIO.getChdrcoy())
		|| isNE(compdelrec.chdrnum,regtlnbIO.getChdrnum())
		|| isNE(compdelrec.life,regtlnbIO.getLife())
		|| isNE(compdelrec.coverage,regtlnbIO.getCoverage())
		|| isNE(compdelrec.rider,regtlnbIO.getRider())) {
			regtlnbIO.setStatuz(varcom.endp);
		}
		else {
			deleteRegt3100();
		}
		/*EXIT*/
	}

protected void deleteRegt3100()
	{
		/*START*/
		regtlnbIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, regtlnbIO);
		if (isNE(regtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtlnbIO.getParams());
			fatalError600();
		}
		else {
			regtlnbIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, regtlnbIO);
			if (isNE(regtlnbIO.getStatuz(),varcom.oK)
			&& isNE(regtlnbIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(regtlnbIO.getParams());
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		wsspcomn.edterror.set(varcom.bomb);
		varcom.vrcmInit.set(SPACES);
		wsspcomn.lastprog.set(wsaaProg);
		wsspcomn.nextprog.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
