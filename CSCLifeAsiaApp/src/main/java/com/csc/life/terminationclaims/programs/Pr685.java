/*
 * File: Pr685.java
 * Date: 30 August 2009 1:55:54
 * Author: Quipoz Limited
 *
 * Class transformed from PR685.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.FluprgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclaanlTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclabanTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclabcaTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclabltTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclacapTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclaltlTableDAM;
import com.csc.life.terminationclaims.dataaccess.HcldTableDAM;
import com.csc.life.terminationclaims.dataaccess.HclhTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.screens.Sr685ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  (NOTE: This program is actually cloned from P6680 for product
*         Hospital Benefit.....)
*  PR685 - Regular Benefit Claims Cancellation.
*  --------------------------------------------
*
*  Overview.
*  ---------
*
*  This program will be used to terminate  or  reverse  Regular
*  Benefit  Claims.  The  difference  between  a Reversal and a
*  Termination is purely one of the  difference  in  status  of
*  the  claim before the transaction is carried out. A Reversal
*  will be performed on a claim that has  yet  to  be  approved
*  whereas  a  Termination  will   be performed on a claim that
*  has been approved at least once. The status of the claim  is
*  table  driven  but  it  is  likely  that a claim, when first
*  registered,  will  have  a  status  code  corresponding   to
*  'Pending'.  After  registration  the claim will be something
*  like 'In Force', 'Active' or 'Claim Paying'. If it passes  a
*  Review  Date or if it is modified when active it will be set
*  to something like  'In  Review'.  Therefore  a  Reversal  is
*  likely  to  be  allowed against a claim in a 'Pending' state
*  whereas a Termination will  be  performed  against  a  claim
*  that  is  'In Review' or 'In Force' or any other status that
*  signifies that it is active. These status code  descriptions
*  are  given  here  only  as an example as they are completely
*  table driven and therefore client dependant.
*
*  The  program  will  be  invoked  by  the  Regular   Payments
*  sub-menu  which  will  check that the claim is in a suitable
*  state  for  cancellation.  Therefore  control  will  not  be
*  passed  here  unless  the  transaction may be carried out on
*  the selected claim record.
*
*  The only data field that may be entered by the user  is  tha
*  Date  of Cancellation. This will be defaulted to the current
*  date but may be overridden subject to certain criteria.
*
*  The only other fields on the screen accessible to  the  user
*  will be the Follow Ups and Bank Details indicators.
*
*
*  Initialise.
*  -----------
*
*  If   returning  from  a  program  further  down  the  stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  Initialise all relevant variables  and  prepare  the  screen
*  for display.
*
*  Perform a RETRV on the CHDRRGP and REGP files.
*
*
*  Use  CHDRRGP  to  display  the heading details on the screen
*  using T5688  for  the  contract  type  description  and  the
*  Client  file  for  the  relevant  client  names.  The  short
*  descriptions for the  contract's  risk  and  premium  status
*  codes  should  be  obtained  from  T3623,  (Risk Status) and
*  T3588, (Premium Status).
*
*  Display the REGP details on the screen  looking  up  all  of
*  the descriptions from DESC where appropriate.
*
*  Set the Cancellation Date to today's date.
*
*  If  there  are  Follow  Ups  in  existence  set a '+' in the
*  Follow Ups  indicator  field.  This  can  be  determined  by
*  reading  FLUPRGP  with  a key of CHDRCOY, CHDRNUM, a CLAMNUM
*  of '00000000' and a function of BEGN. If a record  is  found
*  that  matches  on  Company, Contract Number and Claim Number
*  then there are Follow Ups for the claim.
*
*  If the bank details on REGP are non-blank set a '+'  in  the
*  Bank Details indicator field.
*
*
*  Display and Validation. (2000 Section).
*  ---------------------------------------
*
*  If   returning  from  a  program  further  down  the  stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  Converse with the screen using the I/O module.
*
*  If 'KILL' has been pressed skip this section.
*
*  If 'CALC' has been pressed set WSSP-EDTERROR to 'Y'.
*
*  Cancellation Date: The Cancellation Date will have been  set
*  to  today's  date  but  it  may  be overwritten by the user.
*  Check that it is not less than the Last Paid  Date  nor  the
*  First Payment  Date, and  it is  not  greater than the  Next
*  Payment Date.
*
*  .  Follow Ups Indicator: This may only be 'X', '+' or space.
*  If it is 'X' check  that  there  are  Follow  Ups  to  view,
*  (determined  in  the  1000  section).  If there are not then
*  give an error message.
*
*  . Bank Details Indicator: This  may  only  be  'X',  '+'  or
*  space.  If  it  is  'X' check that there are Bank Details to
*  view. If there are not display an error message.
*
*  Updating.
*  ---------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  If 'KILL' has been pressed skip this section.
*
*  If  either  the  Follow  Ups  indicator  or the Bank Details
*  indicator are 'X' then perform a KEEPS on the REGP file  and
*  skip the updating.
*
*  Otherwise  the  update  proper is to be performed. This will
*  be as follows:
*
*       Read  table  T6693  with  the  current  Payment  Status
*       concatenated   with   the  CRTABLE  of  the  associated
*       component. If the item is not  found  read  again  with
*       the  Payment Status set to '**'. Locate the Transaction
*       Code of the transaction currently being  processed  and
*       select the corresponding Next Payment Status.
*
*       A  history must be kept of the Regular Payment Details.
*       Set the Validflag to '2' and perform an UPDAT on REGP.
*
*       Rad  the  CHDRRGP  record  with  READH,  increment  the
*       TRANNO  and re-write it. Use the new TRANNO to create a
*       new REGP record  with  a  Validflag  of  '1'.  Set  the
*       Payment  Status  to the appropriate Next Payment Status
*       from T6693.
*
*       Write a new REGP record.
*
*       Call Softlock to unlock the contract.
*
*
*  Where Next.
*  -----------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN  =  '*',  then  re-load the next 8 programs in
*  the stack from Working Storage.
*
*  If returning  from  the  Follow  Ups  path  the  Follow  Ups
*  indicator  will  be  '?'. If so set the Follow Ups indicator
*  field to '+'.
*
*  If returning from the Bank Details  path  the  Bank  Details
*  indicator  will be '?'. If so set the Bank Details indicator
*  field to '+'.
*
*  If the Follow Ups  indicator  is  'X'  then  switch  to  the
*  Follow Ups path as follows:
*
*       Store  the  next  8  programs  in  the stack in Working
*       Storage.
*
*       Call GENSSW with an action of 'A'.
*
*       If there is an error code returned from GENSSW  use  it
*       as  an  error  code  on the Follow Ups indicator field,
*       set WSSP-EDTERROR  to  'Y'  and  set  WSSP-NEXTPROG  to
*       SCRN-SCRNAME and go to exit.
*
*       Load  the  8  programs  returned  from GENSSW in to the
*       next 8 positions in the program stack.
*
*       Set the Follow Ups indicator to '?'.
*
*       Set WSSP-SEC-ACTN to '*'.
*
*       Add 1 to the program pointer and go to exit.
*
*
*  If the Bank Details indicator is  'X'  then  switch  to  the
*  Bank Details path as follows:
*
*       Store  the  next  8  programs  in  the stack in Working
*       Storage.
*
*       Call GENSSW with an action of 'B'.
*
*       If there is an error code returned from GENSSW  use  it
*       as  an  error code on the Bank Details indicator field,
*       set WSSP-EDTERROR  to  'Y'  and  set  WSSP-NEXTPROG  to
*       SCRN-SCRNAME and go to exit.
*
*       Load  the  8  programs  returned  from GENSSW in to the
*       next 8 positions in the program stack.
*
*       Set the Bank Details indicator to '?'.
*
*       Set WSSP-SEC-ACTN to '*'.
*
*       Add 1 to the program pointer and go to exit.
*
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN  =  '*',  then move space to WSSP-SEC-ACTN and
*  cause the program to redisplay  from  the  2000  section  by
*  setting WSSP-NEXTPROG to SCRN-SCRNAME and go to exit.
*
*  Add 1 to the program pointer and exit.
*
*  Notes.
*  ------
*
*  Tables Used.
*  ------------
*
*  . T3000 - Currency Code Details
*            Key: CURRCD
*
*  . T3588 - Contract Premium Status Codes
*            Key: PSTCDE from CHDRRGP
*
*  . T3623 - Contract Risk Status Codes
*            Key: STATCODE from CHDRRGP
*
*  . T5688 - Contract Definition
*            Key: Contract Type
*
*  . T5400 - Regular Payment Status
*            Key: Regular Payment Status Code
*
*  . T6692 - Regular Payment Reason Codes
*            Key: Regular Payment Reason Code
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be '****' and for selections against
*            a Component set Payment Status to '**'.
*
*  . T6694 - Regular Payment Method of Payment
*            Key: Regular Payment MOP
*
******************Enhancements for Life Asia 1.0****************
*
* This module has been enhanced to cater for waiver of premium
* (WOP) processing. The waiver of premium sum assured amount is
* based on the accumulation of all the premiums of the
* components on the proposal and the policy fee as specified on
* table TR517. The Following additional processing has been
* introduced :
*
* - For the component being processed access TR517. If the item
*   is present, then we are processing a WOP component.
*
* - Obtain the PAYR record for this contract. The reason for this
*   is that the frequency of payment is the payment frequency
*   of the contract because the waiver is really the premium
*   instalment of the contract and is only payable on the date
*   present on the PAYR record.
*
* - If this is waiver of premium component, then add the
*   WOP premium back to the CHDR and PAYR premiums.
*   This will ensure that during the renewal run, the payment
*   amount is what the contract expects. But first check whether
*   the WOP premium should be addes. For example if the premiums
*   on the CHDR and PAYR records are the correct instalments then
*   we don't want to add the WOP premium again.
*
* - Recalculate the premium instalment on the CHDR and PAYR
*   records.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr685 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR685");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaMiscNumeric = new FixedLengthStringData(47);
	private ZonedDecimalData wsaaClamnum = new ZonedDecimalData(5, 0).isAPartOf(wsaaMiscNumeric, 0).setUnsigned();
	private ZonedDecimalData index1 = new ZonedDecimalData(2, 0).isAPartOf(wsaaMiscNumeric, 5).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).isAPartOf(wsaaMiscNumeric, 7);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).isAPartOf(wsaaMiscNumeric, 9);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).isAPartOf(wsaaMiscNumeric, 11).setUnsigned();
	private PackedDecimalData wsaaWopInstprem = new PackedDecimalData(17, 2).isAPartOf(wsaaMiscNumeric, 20).setUnsigned();
	private PackedDecimalData wsaaCovrInstprem = new PackedDecimalData(17, 2).isAPartOf(wsaaMiscNumeric, 29).setUnsigned();
	private ZonedDecimalData wsaaBenfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaMiscNumeric, 38).setUnsigned();
	private ZonedDecimalData wsaaRegpayfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaMiscNumeric, 40).setUnsigned();
	private PackedDecimalData wsaaCrrcd = new PackedDecimalData(8, 0).isAPartOf(wsaaMiscNumeric, 42);

	private FixedLengthStringData wsaaMiscAlpha = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaRgpystat = new FixedLengthStringData(2).isAPartOf(wsaaMiscAlpha, 0);
	private FixedLengthStringData firstTime = new FixedLengthStringData(1).isAPartOf(wsaaMiscAlpha, 2);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaMiscAlpha, 3);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).isAPartOf(wsaaMiscAlpha, 5);
	private FixedLengthStringData wsaaPremCurrency = new FixedLengthStringData(3).isAPartOf(wsaaMiscAlpha, 7);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaMiscAlpha, 10);
	private FixedLengthStringData wsaaEdtitm = new FixedLengthStringData(5).isAPartOf(wsaaMiscAlpha, 14);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);

	private FixedLengthStringData wsaaT5606Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5606Edtitm = new FixedLengthStringData(5).isAPartOf(wsaaT5606Key, 0);
	private FixedLengthStringData wsaaT5606Currcd = new FixedLengthStringData(3).isAPartOf(wsaaT5606Key, 5);

	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Rgpystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);

	private FixedLengthStringData wsaaClamnum2 = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaClamnumFill = new ZonedDecimalData(3, 0).isAPartOf(wsaaClamnum2, 0).setUnsigned();
	private ZonedDecimalData wsaaRgpynum = new ZonedDecimalData(5, 0).isAPartOf(wsaaClamnum2, 3).setUnsigned();
	private FixedLengthStringData wsaaIsWop = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaPayrBillfreq = new FixedLengthStringData(2).init(SPACES);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT7508Key, 4);
	private ZonedDecimalData wsaaPayrseqno = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaCompYear = new ZonedDecimalData(3, 0).setUnsigned();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluprgpTableDAM fluprgpIO = new FluprgpTableDAM();
	private HclaanlTableDAM hclaanlIO = new HclaanlTableDAM();
	private HclabanTableDAM hclabanIO = new HclabanTableDAM();
	private HclabcaTableDAM hclabcaIO = new HclabcaTableDAM();
	private HclabltTableDAM hclabltIO = new HclabltTableDAM();
	private HclacapTableDAM hclacapIO = new HclacapTableDAM();
	private HclaltlTableDAM hclaltlIO = new HclaltlTableDAM();
	private HcldTableDAM hcldIO = new HcldTableDAM();
	private HclhTableDAM hclhIO = new HclhTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T7508rec t7508rec = new T7508rec();
	private T5606rec t5606rec = new T5606rec();
	private T5671rec t5671rec = new T5671rec();
	private T6693rec t6693rec = new T6693rec();
	private Tr517rec tr517rec = new Tr517rec();
	private T5679rec t5679rec = new T5679rec();
	private Wssplife wssplife = new Wssplife();
	private Sr685ScreenVars sv = ScreenProgram.getScreenVars( Sr685ScreenVars.class);
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

	// CMRPY005Permission
	boolean CMRPY005Permission = false;
	
	/*
	 * CML008
	 */
	private boolean cml008Permission = false ;
	public static final String CLMPREFIX = "CLMNTF";
	private static final String FEATUREID_CLAIM_NOTES = "CMOTH003";
	

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readRegp1060,
		exit1090,
		followUp2050,
		exit2090,
		exit3090,
		hcldio3620,
		exit3690,
		life3750,
		amtAccum3780,
		benLife3850,
		benAmtAccum3880,
		popUp4050,
		gensww4010,
		nextProgram4020,
		exit4090,
		callCovrio8110,
		calculateWopInst8310,
		h910CallCovrio,
		h990Exit
	}

	public Pr685() {
		super();
		screenVars = sv;
		new ScreenModel("Sr685", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
				case readRegp1060:
					readRegp1060();
				case exit1090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{	
		// CML009
		CMRPY005Permission = FeaConfg.isFeatureExist("2", "CMRPY005", appVars,
				"IT");
		cml008Permission = FeaConfg.isFeatureExist("2", FEATUREID_CLAIM_NOTES, appVars, "IT");//CML008

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		wsaaMiscAlpha.set(SPACES);
		wsaaMiscNumeric.set(ZERO);
		firstTime.set("Y");
		sv.anvdate.set(varcom.vrcmMaxDate);
		sv.aprvdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.cancelDate.set(varcom.vrcmMaxDate);
		sv.crtdate.set(varcom.vrcmMaxDate);
		sv.finalPaydate.set(varcom.vrcmMaxDate);
		sv.firstPaydate.set(varcom.vrcmMaxDate);
		sv.lastPaydate.set(varcom.vrcmMaxDate);
		sv.nextPaydate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.recvdDate.set(varcom.vrcmMaxDate);
		sv.incurdt.set(varcom.vrcmMaxDate);
		wsaaCompYear.set(varcom.vrcmMaxDate);
		sv.revdte.set(varcom.vrcmMaxDate);
		sv.rgpynum.set(ZERO);
		sv.clmamt.set(ZERO);
		wsaaPayrseqno.set(ZERO);
		sv.totamnt.set(ZERO);
		wsaaSumins.set(ZERO);
		wsaaWopInstprem.set(ZERO);
		/* Find Today's Date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, "****")) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/* Retrieve the Contract Header.*/
		chdrrgpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		/* Release the Contract Header.*/
		chdrrgpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		/* Read the Contract Type Description*/
		descIO.setDescitem(chdrrgpIO.getCnttype());
		descIO.setDesctabl(tablesInner.t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		/* Read the Contract Currency Description*/
		descIO.setDescitem(chdrrgpIO.getCntcurr());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.currds.set(descIO.getShortdesc());
		}
		else {
			sv.currds.fill("?");
		}
		/* Read the Premium Status Description*/
		descIO.setDescitem(chdrrgpIO.getPstatcode());
		descIO.setDesctabl(tablesInner.t3588);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getLongdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/* Read the Risk Status Description*/
		descIO.setDescitem(chdrrgpIO.getStatcode());
		descIO.setDesctabl(tablesInner.t3623);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getLongdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/* Read Owner Details*/
		cltsIO.setClntnum(chdrrgpIO.getCownnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(), varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(), 1))) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		/* Set screen fields*/
		sv.chdrnum.set(chdrrgpIO.getChdrnum());
		sv.cnttype.set(chdrrgpIO.getCnttype());
		sv.cownnum.set(chdrrgpIO.getCownnum());
		sv.occdate.set(chdrrgpIO.getOccdate());
		sv.ptdate.set(chdrrgpIO.getPtdate());
		sv.btdate.set(chdrrgpIO.getBtdate());
		sv.cancelDate.set(datcon1rec.intDate);
		sv.currcd.set(chdrrgpIO.getCntcurr());
		/* Retrieve the Regular Payment Record.*/
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Release the Regular Payment Record.*/
		regpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Set the Cmpnt (Component) field  - SR685-CRTABLE from the*/
		/* information in the REGP record*/
		sv.crtable.set(regpIO.getCrtable());
		sv.recvdDate.set(regpIO.getRecvdDate());
		sv.incurdt.set(regpIO.getIncurdt());
		/*    PERFORM 1750-READ-HPCL.                                      */
		checkFollowUp5000();
		readPayr7000();
		/* Accumulate the Total Sum Insured within the Plan.*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		covrIO.setChdrnum(chdrrgpIO.getChdrnum());
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) {
			accumCovr1600();
		}

		if (isEQ(wsaaPayrseqno, 0)) {
			wsaaPayrseqno.set(1);
		}
		/* Recalculate the Sum Assured if the REGP frequency and T5606*/
		/*  frequency differ. Read T5671 to obtain the Edit item to read*/
		/*  T5606. Read T5606 to obtain the Benefit Billing frequency*/
		readT56711700();
		wsaaT5606Edtitm.set(wsaaEdtitm);
		wsaaT5606Currcd.set(chdrrgpIO.getCntcurr());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5606);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT5606Key);
		itdmIO.setItmfrm(wsaaCrrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t5606, itdmIO.getItemtabl()))
		|| (isNE(wsaaT5606Key, itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setGenarea(SPACES);
		}
		t5606rec.t5606Rec.set(itdmIO.getGenarea());
		if (isEQ(wsaaIsWop, "N")) {
			if ((isNE(t5606rec.benfreq, regpIO.getRegpayfreq()))
			&& (isNE(t5606rec.benfreq, SPACES))) {
				wsaaBenfreq.set(t5606rec.benfreq);
				wsaaRegpayfreq.set(regpIO.getRegpayfreq());
				compute(wsaaSumins, 2).set((div((mult(wsaaSumins, wsaaBenfreq)), wsaaRegpayfreq)));
			}
		}
		else {
			if ((isNE(wsaaPayrBillfreq, regpIO.getRegpayfreq()))
			&& (isNE(wsaaPayrBillfreq, SPACES))) {
				wsaaBenfreq.set(wsaaPayrBillfreq);
				wsaaRegpayfreq.set(regpIO.getRegpayfreq());
				compute(wsaaSumins, 2).set((div((mult(wsaaSumins, wsaaBenfreq)), wsaaRegpayfreq)));
			}
		}
		covrIO.setLife(wsaaLife);
		covrIO.setJlife(wsaaJlife);
		covrIO.setPremCurrency(wsaaPremCurrency);
		covrIO.setCrtable(wsaaCrtable);
		/* Read Life Details*/
		lifeenqIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrrgpIO.getChdrnum());
		lifeenqIO.setLife(covrIO.getLife());
		lifeenqIO.setJlife(covrIO.getJlife());
		lifeenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		lifeenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");

		SmartFileCode.execute(appVars, lifeenqIO);
		if ((isNE(lifeenqIO.getStatuz(), varcom.oK))
		&& (isNE(lifeenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrrgpIO.getChdrcoy(), lifeenqIO.getChdrcoy()))
		|| (isNE(chdrrgpIO.getChdrnum(), lifeenqIO.getChdrnum()))
		|| (isNE(covrIO.getLife(), lifeenqIO.getLife()))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
			goTo(GotoLabel.readRegp1060);
		}
		sv.lifcnum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(), varcom.mrnf))
		|| (isNE(cltsIO.getValidflag(), 1))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		
		if(cml008Permission)
		{
		sv.cmoth008flag.set("Y");
		sv.claimnumber.set(regpIO.getClaimno());
		if(isEQ(regpIO.getClaimnotifino(),SPACES)){
			sv.aacct.set(SPACES);
		}
		else{
			sv.aacct.set(CLMPREFIX+regpIO.getClaimnotifino());
		}
		}
		else
		{
		sv.cmoth008flag.set("N");
		}
	}

protected void readRegp1060()
	{
		readRegpDetails1300();
	}

protected void findDesc1100()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(), varcom.oK))
		&& (isNE(cltsIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readRegpDetails1300()
	{
		read1310();
	}

protected void read1310()
	{
		/* Read Payment Type Details.*/
		descIO.setDescitem(regpIO.getRgpytype());
		descIO.setDesctabl(tablesInner.t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		}
		else {
			sv.rgpytypesd.fill("?");
		}
		/* Read Payee Details*/
		if (isNE(regpIO.getPayclt(), SPACES)) {
			cltsIO.setClntnum(regpIO.getPayclt());
			getClientDetails1200();
			if ((isEQ(cltsIO.getStatuz(), varcom.mrnf))
			|| (isNE(cltsIO.getValidflag(), 1))) {
				sv.payenmeErr.set(errorsInner.e335);
				sv.payenme.set(SPACES);
			}
			else {
				plainname();
				sv.payenme.set(wsspcomn.longconfname);
			}
		}
		/* Read the Claim Status Description.*/
		descIO.setDescitem(regpIO.getRgpystat());
		descIO.setDesctabl(tablesInner.t5400);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.statdsc.set(descIO.getShortdesc());
		}
		else {
			sv.statdsc.fill("?");
		}
		/* Read the Reason Code Description.*/
		descIO.setDescitem(regpIO.getPayreason());
		descIO.setDesctabl(tablesInner.t6692);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmdesc.set(descIO.getLongdesc());
		}
		else {
			if (isNE(regpIO.getPayreason(), SPACES)) {
				sv.clmdesc.fill("?");
			}
			else {
				sv.clmdesc.set(SPACES);
			}
		}
		/* Read the Payment Method Description.*/
		descIO.setDescitem(regpIO.getRgpymop());
		descIO.setDesctabl(tablesInner.t6694);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpyshort.set(descIO.getShortdesc());
		}
		else {
			sv.rgpyshort.fill("?");
		}
		/* Read the Frequency Code description.*/
		descIO.setDescitem(regpIO.getRegpayfreq());
		descIO.setDesctabl(tablesInner.t3590);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.frqdesc.set(descIO.getShortdesc());
		}
		else {
			sv.frqdesc.fill("?");
		}
		/*  Read the claim currency description.*/
		descIO.setDescitem(regpIO.getCurrcd());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		}
		else {
			sv.clmcurdsc.fill("?");
		}
		/* Set up the screen.*/
		sv.payclt.set(regpIO.getPayclt());
		sv.rgpynum.set(regpIO.getRgpynum());
		sv.rgpystat.set(regpIO.getRgpystat());
		sv.cltype.set(regpIO.getPayreason());
		sv.claimevd.set(regpIO.getClaimevd());
		sv.rgpymop.set(regpIO.getRgpymop());
		sv.regpayfreq.set(regpIO.getRegpayfreq());
		sv.clamparty.set(regpIO.getClamparty());
		sv.destkey.set(regpIO.getDestkey());
		sv.clmamt.set(regpIO.getPymt());
		sv.totamnt.set(regpIO.getTotamnt());
		sv.claimcur.set(regpIO.getCurrcd());
		sv.aprvdate.set(regpIO.getAprvdate());
		sv.crtdate.set(regpIO.getCrtdate());
		sv.revdte.set(regpIO.getRevdte());
		sv.firstPaydate.set(regpIO.getFirstPaydate());
		sv.lastPaydate.set(regpIO.getLastPaydate());
		sv.nextPaydate.set(regpIO.getNextPaydate());
		sv.anvdate.set(regpIO.getAnvdate());
		sv.finalPaydate.set(regpIO.getFinalPaydate());
		if (CMRPY005Permission) {
			// CML009
			sv.adjustamt.set(regpIO.getAdjamt());
			sv.reasoncd.set(regpIO.getReasoncd());
			sv.netclaimamt.set(regpIO.getNetamt());
			sv.resndesc.set(regpIO.getReason());
		}
		/* Check if there are any Bank Details on the current contract.*/
		if ((isEQ(regpIO.getBankkey(), SPACES))
		&& (isEQ(regpIO.getBankacckey(), SPACES))) {
			sv.ddind.set(SPACES);
		}
		else {
			sv.ddind.set("+");
		}
	}

protected void accumCovr1600()
	{
		accum1610();
	}

protected void accum1610()
	{
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(), varcom.oK))
		&& (isNE(covrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrrgpIO.getChdrcoy(), covrIO.getChdrcoy()))
		|| (isNE(chdrrgpIO.getChdrnum(), covrIO.getChdrnum()))
		|| (isNE(regpIO.getLife(), covrIO.getLife()))
		|| (isNE(regpIO.getCoverage(), covrIO.getCoverage()))
		|| (isNE(regpIO.getRider(), covrIO.getRider()))
		|| (isEQ(covrIO.getStatuz(), varcom.endp))) {
			covrIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			covrIO.setFunction(varcom.nextr);
			return ;
		}
		if (isEQ(regpIO.getCoverage(), covrIO.getCoverage())) {
			if (isGT(covrIO.getPayrseqno(), 0)) {
				wsaaPayrseqno.set(covrIO.getPayrseqno());
			}
			else {
				wsaaPayrseqno.set(1);
			}
		}
		wsaaLife.set(covrIO.getLife());
		wsaaJlife.set(covrIO.getJlife());
		wsaaPremCurrency.set(covrIO.getPremCurrency());
		wsaaCrtable.set(covrIO.getCrtable());
		wsaaCrrcd.set(covrIO.getCrrcd());
		wsaaSumins.add(covrIO.getSumins());
		covrIO.setFunction(varcom.nextr);
		readTr5176000();
		if (isEQ(wsaaIsWop, "Y")) {
			wsaaWopInstprem.add(covrIO.getInstprem());
		}
	}

protected void readT56711700()
	{
		read1710();
	}

protected void read1710()
	{
		/* Set up the key for T5671*/
		wsaaT5671Trancd.set(wsaaBatckey.batcBatctrcde);
		wsaaT5671Crtable.set(wsaaCrtable);
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		index1.set(1);
		while ( !((isGT(index1, 4))
		|| (isEQ(wsaaProg, t5671rec.pgm[index1.toInt()])))) {
			if (isNE(t5671rec.pgm[index1.toInt()], wsaaProg)) {
				index1.add(1);
			}
		}

		if (isEQ(t5671rec.pgm[index1.toInt()], wsaaProg)) {
			wsaaEdtitm.set(t5671rec.edtitm[index1.toInt()]);
		}
	}

	/**
	* <pre>
	*1750-READ-HPCL SECTION.
	*1751-START.
	*    MOVE REGP-CHDRCOY           TO HPCL-CHDRCOY.
	*    MOVE REGP-CHDRNUM           TO HPCL-CHDRNUM.
	*    MOVE REGP-LIFE              TO HPCL-LIFE.
	*    MOVE REGP-COVERAGE          TO HPCL-COVERAGE.
	*    MOVE REGP-RIDER             TO HPCL-RIDER.
	*    MOVE REGP-RGPYNUM           TO HPCL-RGPYNUM.
	*    MOVE READR                  TO HPCL-FUNCTION.
	*    MOVE HPCLREC                TO HPCL-FORMAT.
	*    CALL 'HPCLIO'            USING HPCL-PARAMS.
	*    IF   HPCL-STATUZ           NOT = O-K AND MRNF
	*         MOVE HPCL-PARAMS       TO SYSR-PARAMS
	*         PERFORM 600-FATAL-ERROR.
	*    IF   HPCL-STATUZ               = O-K
	*         MOVE HPCL-CLAIMTOT     TO SR685-MAXCLMAMT
	*    ELSE
	*         MOVE ZEROES            TO SR685-MAXCLMAMT
	*    END-IF.
	*1759-EXIT.
	*     EXIT.
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		// CML009
		if (CMRPY005Permission) {
			sv.adjustamtOut[varcom.nd.toInt()].set("N");
			sv.netclaimamtOut[varcom.nd.toInt()].set("N");
			sv.reasoncdOut[varcom.nd.toInt()].set("N");
			sv.resndescOut[varcom.nd.toInt()].set("N");
			sv.pymtAdjOut[varcom.nd.toInt()].set("N");
		} else {
			sv.adjustamtOut[varcom.nd.toInt()].set("Y");
			sv.netclaimamtOut[varcom.nd.toInt()].set("Y");
			sv.reasoncdOut[varcom.nd.toInt()].set("Y");
			sv.resndescOut[varcom.nd.toInt()].set("Y");
			sv.pymtAdjOut[varcom.nd.toInt()].set("Y");
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					validate2020();
				case followUp2050:
					followUp2050();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/* Screen errors are now handled in the calling program.*/
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		/*    Validate fields*/
		if ((isNE(scrnparams.statuz, varcom.calc))
		&& (isNE(scrnparams.statuz, varcom.oK))) {
			scrnparams.errorCode.set(errorsInner.curs);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/* Cancellation Date must be entered*/
		if ((isEQ(sv.cancelDate, varcom.vrcmMaxDate))
		|| (isEQ(sv.cancelDate, ZERO))) {
			sv.canceldateErr.set(errorsInner.e186);
			goTo(GotoLabel.followUp2050);
		}
		/* CANCELLATION - A NOT APPROVED PAYMENT.*/
		/* TERMINATION - AN APPROVED PAYMENT.*/
		/* Cancellation Date should not be greater than today*/
		/*  and should not be less than registration Date.*/
		if (isEQ(sv.aprvdate, varcom.vrcmMaxDate)) {
			if (isGT(sv.cancelDate, datcon1rec.intDate)) {
				sv.canceldateErr.set(errorsInner.f073);
			}
			if (isLT(sv.cancelDate, sv.crtdate)) {
				sv.canceldateErr.set(errorsInner.g531);
			}
		}
		/* Termination Date should not be less than Last Paid Date,*/
		/*  should not be less than Approval Date and*/
		/*  should not be Greater than Next Payment Date.*/
		if (isNE(sv.aprvdate, varcom.vrcmMaxDate)) {
			if ((isNE(sv.lastPaydate, ZERO))
			&& (isNE(sv.lastPaydate, varcom.vrcmMaxDate))) {
				if (isLT(sv.cancelDate, sv.lastPaydate)) {
					sv.canceldateErr.set(errorsInner.g526);
				}
			}
			if (isLT(sv.cancelDate, sv.aprvdate)) {
				sv.canceldateErr.set(errorsInner.g533);
			}
			if (isGT(sv.cancelDate, sv.nextPaydate)) {
				sv.canceldateErr.set(errorsInner.g535);
			}
		}
	}

	/**
	* <pre>
	* Validate Follow-up Indicator.
	* </pre>
	*/
protected void followUp2050()
	{
		if ((isNE(sv.fupflg, "+"))
		&& (isNE(sv.fupflg, "X"))
		&& (isNE(sv.fupflg, SPACES))) {
			sv.fupflgErr.set(errorsInner.h118);
		}
		/* Validate Bank Details Indicator.*/
		if ((isNE(sv.ddind, "+"))
		&& (isNE(sv.ddind, "X"))
		&& (isNE(sv.ddind, SPACES))) {
			sv.ddindErr.set(errorsInner.h118);
		}
		/* Check if there are any Bank Details to show.*/
		if ((isEQ(sv.ddind, "X"))
		&& (isEQ(sv.ddindErr, SPACES))
		&& (isEQ(regpIO.getBankkey(), SPACES))) {
			sv.ddindErr.set(errorsInner.e493);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			updateDatabase3010();
			softLock3080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required / WSSP*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(firstTime, "Y")) {
			firstTime.set("N");
			chdrlifIO.setChdrpfx(chdrrgpIO.getChdrpfx());
			chdrlifIO.setChdrcoy(chdrrgpIO.getChdrcoy());
			chdrlifIO.setChdrnum(chdrrgpIO.getChdrnum());
			chdrlifIO.setValidflag(chdrrgpIO.getValidflag());
			chdrlifIO.setCurrfrom(chdrrgpIO.getCurrfrom());
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			/*       MOVE BEGNH               TO CHDRLIF-FUNCTION              */
			chdrlifIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			/* Rewrite CHDR with a validflag of '2'*/
			chdrlifIO.setCurrto(datcon1rec.intDate);
			chdrlifIO.setValidflag("2");
			/*       MOVE REWRT               TO CHDRLIF-FUNCTION              */
			chdrlifIO.setFunction(varcom.writd);
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			/* Write the new CHDR record with the updated fields*/
			if ((isNE(regpIO.getDestkey(), SPACES))
			&& (isNE(sv.aprvdate, varcom.vrcmMaxDate))
			&& (isNE(sv.aprvdate, SPACES))) {
				setPrecision(chdrlifIO.getSinstamt05(), 2);
				chdrlifIO.setSinstamt05((add(regpIO.getPymt(), chdrlifIO.getSinstamt05())));
				setPrecision(chdrlifIO.getSinstamt06(), 2);
				chdrlifIO.setSinstamt06((add(add(add(add(chdrlifIO.getSinstamt01(), chdrlifIO.getSinstamt02()), chdrlifIO.getSinstamt03()), chdrlifIO.getSinstamt04()), chdrlifIO.getSinstamt05())));
			}
			/* If a WOP is being terminated, then add the premium*/
			/* for the WOP back into the CHDR record.*/
			/* But first check to whether any premium should be added.*/
			if (isEQ(wsaaIsWop, "Y")) {
				if (isNE(tr517rec.zrwvflg01, "Y")) {
					calcWopInstprem8000();
					setPrecision(chdrlifIO.getSinstamt01(), 2);
					chdrlifIO.setSinstamt01(add(chdrlifIO.getSinstamt01(), wsaaWopInstprem));
					setPrecision(chdrlifIO.getSinstamt06(), 2);
					chdrlifIO.setSinstamt06((add(add(add(add(chdrlifIO.getSinstamt01(), chdrlifIO.getSinstamt02()), chdrlifIO.getSinstamt03()), chdrlifIO.getSinstamt04()), chdrlifIO.getSinstamt05())));
				}
			}
			chdrlifIO.setCurrfrom(datcon1rec.intDate);
			chdrlifIO.setCurrto(varcom.vrcmMaxDate);
			chdrlifIO.setValidflag("1");
			setPrecision(chdrlifIO.getTranno(), 0);
			chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
			chdrlifIO.setFunction(varcom.writr);
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			/* Write the new PAYR record with the updated fields*/
			payrIO.setDataArea(SPACES);
			payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
			payrIO.setChdrnum(chdrrgpIO.getChdrnum());
			payrIO.setValidflag("1");
			payrIO.setPayrseqno(wsaaPayrseqno);
			payrIO.setFormat(formatsInner.payrrec);
			payrIO.setFunction(varcom.begnh);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getStatuz());
				fatalError600();
			}
			payrIO.setValidflag("2");
			payrIO.setFunction(varcom.rewrt);
			payrIO.setFormat(formatsInner.payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
			if ((isNE(regpIO.getDestkey(), SPACES))
			&& (isNE(regpIO.getAprvdate(), varcom.vrcmMaxDate))) {
				setPrecision(payrIO.getSinstamt05(), 2);
				payrIO.setSinstamt05((add(regpIO.getPymt(), payrIO.getSinstamt05())));
				setPrecision(payrIO.getSinstamt06(), 2);
				payrIO.setSinstamt06((add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()), payrIO.getSinstamt04()), payrIO.getSinstamt05())));
			}
			/* If a WOP is being terminated, then add the premium*/
			/* for the WOP back into the PAYR record.*/
			if (isEQ(wsaaIsWop, "Y")) {
				if (isNE(tr517rec.zrwvflg01, "Y")) {
					setPrecision(payrIO.getSinstamt01(), 2);
					payrIO.setSinstamt01(add(payrIO.getSinstamt01(), wsaaWopInstprem));
					setPrecision(payrIO.getSinstamt06(), 2);
					payrIO.setSinstamt06((add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()), payrIO.getSinstamt04()), payrIO.getSinstamt05())));
				}
			}
			payrIO.setValidflag("1");
			payrIO.setTranno(chdrlifIO.getTranno());
			payrIO.setFunction(varcom.writr);
			payrIO.setFormat(formatsInner.payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
			h900UpdateCovr();
			regpIO.setValidflag("2");
			regpIO.setFormat(formatsInner.regprec);
			regpIO.setFunction(varcom.updat);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			regpIO.setValidflag("1");
		}
		if (isEQ(sv.fupflg, "X")) {
			moveToRegp3400();
			regpIO.setFunction(varcom.keeps);
			regpIO.setFormat(formatsInner.regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(sv.ddind, "X")) {
			moveToRegp3400();
			regpIO.setFunction(varcom.keeps);
			regpIO.setFormat(formatsInner.regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		readT66933300();
		updateRegp3200();
		if (isNE(sv.aprvdate, ZERO)
		&& isNE(sv.aprvdate, varcom.vrcmMaxDate)) {
			claimAccum3600();
		}
		diaryProcessing5100();
	}

protected void softLock3080()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrrgpIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	* </pre>
	*/
protected void updateRegp3200()
	{
		update3210();
	}

protected void update3210()
	{
		regpIO.setRgpystat(wsaaRgpystat);
		regpIO.setCancelDate(sv.cancelDate);
		regpIO.setAprvdate(varcom.vrcmMaxDate);
		regpIO.setTranno(chdrlifIO.getTranno());
		regpIO.setFunction(varcom.writr);
		regpIO.setFormat(formatsInner.regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		updatPtrn3500();
	}

protected void readT66933300()
	{
		readT66933310();
	}

protected void readT66933310()
	{
		wsaaT6693Rgpystat.set(regpIO.getRgpystat());
		wsaaT6693Crtable.set(covrIO.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6693);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6693Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy()))
		|| (isNE(tablesInner.t6693, itdmIO.getItemtabl()))
		|| (isNE(wsaaT6693Key, itdmIO.getItemitem()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			wsaaT6693Crtable.set("****");
			readT66933320();
			if ((isNE(wsspcomn.company, itdmIO.getItemcoy()))
			|| (isNE(tablesInner.t6693, itdmIO.getItemtabl()))
			|| (isNE(wsaaT6693Key, itdmIO.getItemitem()))
			|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
				itdmIO.setStatuz(varcom.endp);
				itdmIO.setGenarea(SPACES);
				return ;
			}
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaRgpystat.set(SPACES);
		index1.set(1);
		while ( !(isGT(index1, 12))) {
			findStatus3310();
		}

	}

protected void findStatus3310()
	{
		/*FIND-STATUS*/
		if (isEQ(t6693rec.trcode[index1.toInt()], wsaaBatckey.batcBatctrcde)) {
			wsaaRgpystat.set(t6693rec.rgpystat[index1.toInt()]);
			index1.set(15);
		}
		index1.add(1);
		/*EXIT*/
	}

protected void readT66933320()
	{
		readAgain3325();
	}

protected void readAgain3325()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6693);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6693Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
	}

protected void moveToRegp3400()
	{
		moveToRegp3410();
	}

protected void moveToRegp3410()
	{
		regpIO.setDestkey(sv.destkey);
		regpIO.setRgpynum(sv.rgpynum);
		regpIO.setPayclt(sv.payclt);
		regpIO.setPayreason(sv.cltype);
		regpIO.setClaimevd(sv.claimevd);
		regpIO.setRgpymop(sv.rgpymop);
		regpIO.setRegpayfreq(sv.regpayfreq);
		regpIO.setClamparty(sv.clamparty);
		regpIO.setPymt(sv.clmamt);
		regpIO.setCurrcd(sv.currcd);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setCrtdate(sv.crtdate);
		regpIO.setRevdte(sv.revdte);
		regpIO.setFirstPaydate(sv.firstPaydate);
		regpIO.setAnvdate(sv.anvdate);
		regpIO.setFinalPaydate(sv.finalPaydate);
		regpIO.setCancelDate(sv.cancelDate);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setNextPaydate(sv.nextPaydate);
		regpIO.setLastPaydate(sv.lastPaydate);
		regpIO.setCrtable(wsaaCrtable);
		regpIO.setTranno(chdrlifIO.getTranno());
	}

protected void updatPtrn3500()
	{
		updat3510();
	}

protected void updat3510()
	{
		/* Write a PTRN record.*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrrgpIO.getChdrnum());
		ptrnIO.setRecode(chdrrgpIO.getRecode());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setBatccoy(wsspcomn.company);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setPrtflg(SPACES);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setValidflag("1");
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void claimAccum3600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					accum3610();
				case hcldio3620:
					hcldio3620();
					nextHcld3680();
				case exit3690:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void accum3610()
	{
		/* Calculate the component year                                    */
		datcon3rec.intDate1.set(wsaaCrrcd);
		datcon3rec.intDate2.set(regpIO.getIncurdt());
		datcon3rec.frequency.set("01");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		compute(wsaaCompYear, 5).set(add(0.99999, datcon3rec.freqFactor));
		hclhIO.setChdrcoy(regpIO.getChdrcoy());
		hclhIO.setChdrnum(regpIO.getChdrnum());
		hclhIO.setLife(regpIO.getLife());
		hclhIO.setCoverage(regpIO.getCoverage());
		hclhIO.setRider(regpIO.getRider());
		hclhIO.setRgpynum(regpIO.getRgpynum());
		hclhIO.setFunction(varcom.readr);
		hclhIO.setFormat(formatsInner.hclhrec);
		SmartFileCode.execute(appVars, hclhIO);
		if (isNE(hclhIO.getStatuz(), varcom.oK)
		&& isNE(hclhIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hclhIO.getParams());
			fatalError600();
		}
		/* Plan level accumulation                                         */
		planAccum3700();
		/* Benefit level checking                                          */
		hcldIO.setDataKey(SPACES);
		hcldIO.setChdrcoy(regpIO.getChdrcoy());
		hcldIO.setChdrnum(regpIO.getChdrnum());
		hcldIO.setLife(regpIO.getLife());
		hcldIO.setCoverage(regpIO.getCoverage());
		hcldIO.setRider(regpIO.getRider());
		hcldIO.setRgpynum(regpIO.getRgpynum());
		hcldIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		hcldIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hcldIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","RGPYNUM");
		hcldIO.setFormat(formatsInner.hcldrec);
	}

protected void hcldio3620()
	{
		SmartFileCode.execute(appVars, hcldIO);
		if (isNE(hcldIO.getStatuz(), varcom.oK)
		&& isNE(hcldIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hclhIO.getParams());
			syserrrec.statuz.set(hclhIO.getStatuz());
			fatalError600();
		}
		if (isNE(regpIO.getChdrcoy(), hcldIO.getChdrcoy())
		|| isNE(regpIO.getChdrnum(), hcldIO.getChdrnum())
		|| isNE(regpIO.getLife(), hcldIO.getLife())
		|| isNE(regpIO.getCoverage(), hcldIO.getCoverage())
		|| isNE(regpIO.getRider(), hcldIO.getRider())
		|| isNE(regpIO.getRgpynum(), hcldIO.getRgpynum())
		|| isEQ(hcldIO.getStatuz(), varcom.endp)) {
			hcldIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3690);
		}
		/* Benefit level checking                                          */
		benAccum3800();
	}

protected void nextHcld3680()
	{
		hcldIO.setFunction(varcom.nextr);
		goTo(GotoLabel.hcldio3620);
	}

protected void planAccum3700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					annual3710();
				case life3750:
					life3750();
				case amtAccum3780:
					amtAccum3780();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void annual3710()
	{
		if (isNE(hclhIO.getLmtyear(), "Y")) {
			goTo(GotoLabel.life3750);
		}
		hclaanlIO.setDataKey(SPACES);
		hclaanlIO.setChdrcoy(hclhIO.getChdrcoy());
		hclaanlIO.setChdrnum(hclhIO.getChdrnum());
		hclaanlIO.setLife(hclhIO.getLife());
		hclaanlIO.setCoverage(hclhIO.getCoverage());
		hclaanlIO.setRider(hclhIO.getRider());
		hclaanlIO.setAcumactyr(wsaaCompYear);
		hclaanlIO.setFunction(varcom.readr);
		hclaanlIO.setFormat(formatsInner.hclaanlrec);
		SmartFileCode.execute(appVars, hclaanlIO);
		if (isNE(hclaanlIO.getStatuz(), varcom.oK)
		&& isNE(hclaanlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclaanlIO.getStatuz());
			syserrrec.params.set(hclaanlIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclaanlIO.getStatuz(), varcom.oK)) {
			if (isEQ(hclaanlIO.getAad(), hclhIO.getAad())
			&& isEQ(hclaanlIO.getClmpaid(), hclhIO.getTclmamt())) {
				hclaanlIO.setFunction(varcom.deltd);
			}
			else {
				setPrecision(hclaanlIO.getAad(), 0);
				hclaanlIO.setAad(sub(hclaanlIO.getAad(), hclhIO.getAad()));
				setPrecision(hclaanlIO.getClmpaid(), 2);
				hclaanlIO.setClmpaid(sub(hclaanlIO.getClmpaid(), hclhIO.getTclmamt()));
				hclaanlIO.setFunction(varcom.writd);
			}
			SmartFileCode.execute(appVars, hclaanlIO);
			if (isNE(hclaanlIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclaanlIO.getStatuz());
				syserrrec.params.set(hclaanlIO.getParams());
				fatalError600();
			}
		}
	}

protected void life3750()
	{
		if (isNE(hclhIO.getLmtlife(), "Y")) {
			goTo(GotoLabel.amtAccum3780);
		}
		hclaltlIO.setDataKey(SPACES);
		hclaltlIO.setChdrcoy(hclhIO.getChdrcoy());
		hclaltlIO.setChdrnum(hclhIO.getChdrnum());
		hclaltlIO.setLife(hclhIO.getLife());
		hclaltlIO.setCoverage(hclhIO.getCoverage());
		hclaltlIO.setRider(hclhIO.getRider());
		hclaltlIO.setFunction(varcom.readr);
		hclaltlIO.setFormat(formatsInner.hclaltlrec);
		SmartFileCode.execute(appVars, hclaltlIO);
		if (isNE(hclaltlIO.getStatuz(), varcom.oK)
		&& isNE(hclaltlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclaltlIO.getStatuz());
			syserrrec.params.set(hclaltlIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclaltlIO.getStatuz(), varcom.oK)) {
			if (isEQ(hclaltlIO.getAad(), hclhIO.getAad())
			&& isEQ(hclaltlIO.getClmpaid(), hclhIO.getTclmamt())) {
				hclaltlIO.setFunction(varcom.deltd);
			}
			else {
				setPrecision(hclaltlIO.getAad(), 0);
				hclaltlIO.setAad(sub(hclaltlIO.getAad(), hclhIO.getAad()));
				setPrecision(hclaltlIO.getClmpaid(), 2);
				hclaltlIO.setClmpaid(sub(hclaltlIO.getClmpaid(), hclhIO.getTclmamt()));
				hclaltlIO.setFunction(varcom.writd);
			}
			SmartFileCode.execute(appVars, hclaltlIO);
			if (isNE(hclaltlIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclaltlIO.getStatuz());
				syserrrec.params.set(hclaltlIO.getParams());
				fatalError600();
			}
		}
	}

protected void amtAccum3780()
	{
		if (isEQ(hclhIO.getLmtyear(), "Y")
		|| isEQ(hclhIO.getLmtlife(), "Y")) {
			return ;
		}
		hclacapIO.setDataKey(SPACES);
		hclacapIO.setChdrcoy(hclhIO.getChdrcoy());
		hclacapIO.setChdrnum(hclhIO.getChdrnum());
		hclacapIO.setLife(hclhIO.getLife());
		hclacapIO.setCoverage(hclhIO.getCoverage());
		hclacapIO.setRider(hclhIO.getRider());
		hclacapIO.setFunction(varcom.readr);
		hclacapIO.setFormat(formatsInner.hclacaprec);
		SmartFileCode.execute(appVars, hclacapIO);
		if (isNE(hclacapIO.getStatuz(), varcom.oK)
		&& isNE(hclacapIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclacapIO.getStatuz());
			syserrrec.params.set(hclacapIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclacapIO.getStatuz(), varcom.oK)) {
			if (isEQ(hclacapIO.getAad(), hclhIO.getAad())
			&& isEQ(hclacapIO.getClmpaid(), hclhIO.getTclmamt())) {
				hclacapIO.setFunction(varcom.deltd);
			}
			else {
				setPrecision(hclacapIO.getAad(), 0);
				hclacapIO.setAad(sub(hclacapIO.getAad(), hclhIO.getAad()));
				setPrecision(hclacapIO.getClmpaid(), 2);
				hclacapIO.setClmpaid(sub(hclacapIO.getClmpaid(), hclhIO.getTclmamt()));
				hclacapIO.setFunction(varcom.writd);
			}
			SmartFileCode.execute(appVars, hclacapIO);
			if (isNE(hclacapIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclacapIO.getStatuz());
				syserrrec.params.set(hclacapIO.getParams());
				fatalError600();
			}
		}
	}

protected void benAccum3800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					benAnn3800();
				case benLife3850:
					benLife3850();
				case benAmtAccum3880:
					benAmtAccum3880();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void benAnn3800()
	{
		if (isNE(hcldIO.getLmtyear(), "Y")) {
			goTo(GotoLabel.benLife3850);
		}
		hclabanIO.setDataKey(SPACES);
		hclabanIO.setChdrcoy(hcldIO.getChdrcoy());
		hclabanIO.setChdrnum(hcldIO.getChdrnum());
		hclabanIO.setLife(hcldIO.getLife());
		hclabanIO.setCoverage(hcldIO.getCoverage());
		hclabanIO.setRider(hcldIO.getRider());
		hclabanIO.setHosben(hcldIO.getHosben());
		hclabanIO.setAcumactyr(wsaaCompYear);
		hclabanIO.setFunction(varcom.readr);
		hclabanIO.setFormat(formatsInner.hclabanrec);
		SmartFileCode.execute(appVars, hclabanIO);
		if (isNE(hclabanIO.getStatuz(), varcom.oK)
		&& isNE(hclabanIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclabanIO.getStatuz());
			syserrrec.params.set(hclabanIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclabanIO.getStatuz(), varcom.oK)) {
			if (isEQ(hclabanIO.getAccday(), hcldIO.getZdaycov())
			&& isEQ(hclabanIO.getClmpaid(), hcldIO.getGcnetpy())) {
				hclabanIO.setFunction(varcom.deltd);
			}
			else {
				setPrecision(hclabanIO.getAccday(), 0);
				hclabanIO.setAccday(sub(hclabanIO.getAccday(), hcldIO.getZdaycov()));
				setPrecision(hclabanIO.getClmpaid(), 2);
				hclabanIO.setClmpaid(sub(hclabanIO.getClmpaid(), hcldIO.getGcnetpy()));
				hclabanIO.setFunction(varcom.writd);
			}
			SmartFileCode.execute(appVars, hclabanIO);
			if (isNE(hclabanIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclabanIO.getStatuz());
				syserrrec.params.set(hclabanIO.getParams());
				fatalError600();
			}
		}
	}

protected void benLife3850()
	{
		if (isNE(hcldIO.getLmtlife(), "Y")) {
			goTo(GotoLabel.benAmtAccum3880);
		}
		hclabltIO.setDataKey(SPACES);
		hclabltIO.setChdrcoy(hcldIO.getChdrcoy());
		hclabltIO.setChdrnum(hcldIO.getChdrnum());
		hclabltIO.setLife(hcldIO.getLife());
		hclabltIO.setCoverage(hcldIO.getCoverage());
		hclabltIO.setRider(hcldIO.getRider());
		hclabltIO.setHosben(hcldIO.getHosben());
		hclabltIO.setFunction(varcom.readr);
		hclabltIO.setFormat(formatsInner.hclabltrec);
		SmartFileCode.execute(appVars, hclabltIO);
		if (isNE(hclabltIO.getStatuz(), varcom.oK)
		&& isNE(hclabltIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclabltIO.getStatuz());
			syserrrec.params.set(hclabltIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclabltIO.getStatuz(), varcom.oK)) {
			if (isEQ(hclabltIO.getAccday(), hcldIO.getZdaycov())
			&& isEQ(hclabltIO.getClmpaid(), hcldIO.getGcnetpy())) {
				hclabltIO.setFunction(varcom.deltd);
			}
			else {
				setPrecision(hclabltIO.getAccday(), 0);
				hclabltIO.setAccday(sub(hclabltIO.getAccday(), hcldIO.getZdaycov()));
				setPrecision(hclabltIO.getClmpaid(), 2);
				hclabltIO.setClmpaid(sub(hclabltIO.getClmpaid(), hcldIO.getGcnetpy()));
				hclabltIO.setFunction(varcom.writd);
			}
			SmartFileCode.execute(appVars, hclabltIO);
			if (isNE(hclabltIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclabltIO.getStatuz());
				syserrrec.params.set(hclabltIO.getParams());
				fatalError600();
			}
		}
	}

protected void benAmtAccum3880()
	{
		if (isEQ(hcldIO.getLmtyear(), "Y")
		|| isEQ(hcldIO.getLmtlife(), "Y")) {
			return ;
		}
		hclabcaIO.setDataKey(SPACES);
		hclabcaIO.setChdrcoy(hcldIO.getChdrcoy());
		hclabcaIO.setChdrnum(hcldIO.getChdrnum());
		hclabcaIO.setLife(hcldIO.getLife());
		hclabcaIO.setCoverage(hcldIO.getCoverage());
		hclabcaIO.setRider(hcldIO.getRider());
		hclabcaIO.setFunction(varcom.readr);
		hclabcaIO.setFormat(formatsInner.hclabcarec);
		SmartFileCode.execute(appVars, hclabcaIO);
		if (isNE(hclabcaIO.getStatuz(), varcom.oK)
		&& isNE(hclabcaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hclabcaIO.getStatuz());
			syserrrec.params.set(hclabcaIO.getParams());
			fatalError600();
		}
		/* Update existing record                                          */
		if (isEQ(hclabcaIO.getStatuz(), varcom.oK)) {
			if (isEQ(hclabcaIO.getAccday(), hcldIO.getZdaycov())
			&& isEQ(hclabcaIO.getClmpaid(), hcldIO.getGcnetpy())) {
				hclabcaIO.setFunction(varcom.deltd);
			}
			else {
				setPrecision(hclabcaIO.getAccday(), 0);
				hclabcaIO.setAccday(sub(hclabcaIO.getAccday(), hcldIO.getZdaycov()));
				setPrecision(hclabcaIO.getClmpaid(), 2);
				hclabcaIO.setClmpaid(sub(hclabcaIO.getClmpaid(), hcldIO.getGcnetpy()));
				hclabcaIO.setFunction(varcom.writd);
			}
			hclabcaIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, hclabcaIO);
			if (isNE(hclabcaIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(hclabcaIO.getStatuz());
				syserrrec.params.set(hclabcaIO.getParams());
				fatalError600();
			}
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					nextProgram4010();
				case popUp4050:
					popUp4050();
				case gensww4010:
					gensww4010();
				case nextProgram4020:
					nextProgram4020();
				case exit4090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		/* If returning from a program further down the stack then*/
		/* first restore the original programs in the program stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}
		if (isEQ(sv.fupflg, "?")) {
			if (isEQ(sv.ddind, "X")) {
				regpIO.setFunction(varcom.keeps);
				regpIO.setFormat(formatsInner.regprec);
				SmartFileCode.execute(appVars, regpIO);
				if (isNE(regpIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(regpIO.getParams());
					fatalError600();
				}
				goTo(GotoLabel.popUp4050);
			}
			else {
				checkFollowUp5000();
			}
		}
		if (isEQ(sv.ddind, "?")) {
			checkBankDetails4400();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
	}

	/**
	* <pre>
	* If any of the indicators have been selected, (value - 'X'),
	* then set an asterisk in the program stack action field to
	* ensure that control returns here, set the parameters for
	* generalised secondary switching and save the original
	* programs from the program stack.
	* </pre>
	*/
protected void popUp4050()
	{
		if ((isEQ(sv.ddind, "X"))
		|| (isEQ(sv.fupflg, "X"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				saveProgramStack4200();
			}
		}
		gensswrec.function.set(SPACES);
		/* If FOLLOW-UP has been selected set 'B' in the function.*/
		if (isEQ(sv.fupflg, "X")) {
			sv.fupflg.set("?");
			gensswrec.function.set("B");
			goTo(GotoLabel.gensww4010);
		}
		/* If BANK DETAILS has been selected set 'A' in the function.*/
		if (isEQ(sv.ddind, "X")) {
			sv.ddind.set("?");
			gensswrec.function.set("A");
			goTo(GotoLabel.gensww4010);
		}
	}

	/**
	* <pre>
	* If a value has been placed in the GENS-FUNCTION then call
	* the generalised secondary switching module to obtain the
	* next 8 programs and load them into the program stack.
	* </pre>
	*/
protected void gensww4010()
	{
		if (isEQ(gensswrec.function, SPACES)) {
			goTo(GotoLabel.nextProgram4020);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz, varcom.oK))
		&& (isNE(gensswrec.statuz, varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgramStack4300();
		}
	}

protected void nextProgram4020()
	{
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

	/**
	* <pre>
	*    Sections performed from the 4000 section above.
	* </pre>
	*/
protected void restoreProgram4100()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void saveProgramStack4200()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4300()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/*EXIT*/
	}

protected void checkBankDetails4400()
	{
		bank4400();
	}

protected void bank4400()
	{
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		regpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Check if there are any Bank Details on the current contract.*/
		if ((isEQ(regpIO.getBankkey(), SPACES))
		&& (isEQ(regpIO.getBankacckey(), SPACES))) {
			sv.ddind.set(SPACES);
		}
		else {
			sv.ddind.set("+");
		}
	}

protected void checkFollowUp5000()
	{
		follow5010();
	}

protected void follow5010()
	{
		/* Check if there are any follow-ups on the current contract.*/
		fluprgpIO.setChdrcoy(wsspcomn.company);
		fluprgpIO.setChdrnum(chdrrgpIO.getChdrnum());
		wsaaRgpynum.set(regpIO.getRgpynum());
		wsaaClamnumFill.set(ZERO);
		fluprgpIO.setClamnum(wsaaClamnum2);
		fluprgpIO.setFupno(0);
		/* We will use WSAA-CLAMNUM to comapre to FLUPRGP-CLAMNUM*/
		/* instead of comparing REGP-RGPYNUM and FLUPRGP-CLAMNUM.*/
		/* This is due to the fact that RGPYNUM is 5 bytes long*/
		/* and CLAMNUM is 8 bytes long.*/
		fluprgpIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		fluprgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluprgpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","CLAMNUM");
		SmartFileCode.execute(appVars, fluprgpIO);
		if (isNE(fluprgpIO.getStatuz(), varcom.oK)
		&& isNE(fluprgpIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
		if ((isEQ(wsspcomn.company, fluprgpIO.getChdrcoy()))
		&& (isEQ(chdrrgpIO.getChdrnum(), fluprgpIO.getChdrnum()))
		&& (isEQ(fluprgpIO.getClamnum(), wsaaClamnum2))
		&& (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			sv.fupflg.set("+");
		}
		else {
			sv.fupflg.set(SPACES);
		}
	}

protected void diaryProcessing5100()
	{
		start5110();
	}

protected void start5110()
	{
		/* This section will determine if the DIARY system is present   */
		/* If so, the appropriate parameters are filled and the         */
		/* diary processor is called.                                   */
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		wsaaT7508Cnttype.set(chdrrgpIO.getCnttype());
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.t7508);
			itemIO.setItemitem(wsaaT7508Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		/* If item not found no Diary Update Processing is Required.    */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(regpIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readTr5176000()
	{
		para6100();
	}

protected void para6100()
	{
		if (isNE(wsaaIsWop, SPACES)) {
			return ;
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(), covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaIsWop.set("N");
		}
		else {
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
			wsaaIsWop.set("Y");
		}
	}

protected void readPayr7000()
	{
		para7100();
	}

protected void para7100()
	{
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		payrIO.setChdrnum(chdrrgpIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		payrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		if (isNE(payrIO.getChdrcoy(), chdrrgpIO.getChdrcoy())
		|| isNE(payrIO.getChdrnum(), chdrrgpIO.getChdrnum())
		|| isEQ(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrrgpIO.getChdrnum());
			syserrrec.statuz.set(errorsInner.e540);
			fatalError600();
		}
		wsaaPayrBillfreq.set(payrIO.getBillfreq());
	}

protected void calcWopInstprem8000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para8100();
				case callCovrio8110:
					callCovrio8110();
				case calculateWopInst8310:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para8100()
	{
		/* To ensure that we don't add the WOP premium more than once*/
		/* accumulate the COVR premiums and compare it with the*/
		/* SINSTAMT01 on the CHDR record. If they are the same then the*/
		/* the WOP premium has already been added on.*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		covrIO.setChdrnum(chdrlifIO.getChdrnum());
		covrIO.setLife(SPACES);
		covrIO.setCoverage(SPACES);
		covrIO.setRider(SPACES);
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	}

protected void callCovrio8110()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isNE(covrIO.getLife(), regpIO.getLife())
		|| isNE(covrIO.getCoverage(), regpIO.getCoverage())
		|| isNE(covrIO.getRider(), regpIO.getRider())
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.calculateWopInst8310);
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			covrIO.setFunction(varcom.nextr);
			goTo(GotoLabel.callCovrio8110);
		}
		covrIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callCovrio8110);
	}

protected void h900UpdateCovr()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					h901Para();
				case h910CallCovrio:
					h910CallCovrio();
				case h990Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void h901Para()
	{
		if (isEQ(wsaaIsWop, "Y")) {
			if (isEQ(tr517rec.zrwvflg01, "Y")) {
				goTo(GotoLabel.h990Exit);
			}
		}
		else {
			goTo(GotoLabel.h990Exit);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(regpIO.getChdrcoy());
		covrIO.setChdrnum(regpIO.getChdrnum());
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.begnh);
	}

protected void h910CallCovrio()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isNE(covrIO.getLife(), regpIO.getLife())
		|| isNE(covrIO.getCoverage(), regpIO.getCoverage())
		|| isNE(covrIO.getRider(), regpIO.getRider())
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		covrIO.setValidflag("2");
		covrIO.setCurrto(datcon1rec.intDate);
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrIO.getRider(), "00")) {
			covrIO.setStatcode(t5679rec.setCovRiskStat);
			covrIO.setPstatcode(t5679rec.setCovPremStat);
		}
		else {
			covrIO.setStatcode(t5679rec.setRidRiskStat);
			covrIO.setPstatcode(t5679rec.setRidPremStat);
		}
		covrIO.setValidflag("1");
		covrIO.setCurrfrom(datcon1rec.intDate);
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setTranno(chdrlifIO.getTranno());
		covrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		setPrecision(covrIO.getPlanSuffix(), 0);
		covrIO.setPlanSuffix(add(covrIO.getPlanSuffix(), 1));
		covrIO.setFunction(varcom.begnh);
		goTo(GotoLabel.h910CallCovrio);
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
	private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
	private FixedLengthStringData e493 = new FixedLengthStringData(4).init("E493");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData g526 = new FixedLengthStringData(4).init("G526");
	private FixedLengthStringData g531 = new FixedLengthStringData(4).init("G531");
	private FixedLengthStringData g533 = new FixedLengthStringData(4).init("G533");
	private FixedLengthStringData g535 = new FixedLengthStringData(4).init("G535");
	private FixedLengthStringData h118 = new FixedLengthStringData(4).init("H118");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
	private FixedLengthStringData e540 = new FixedLengthStringData(4).init("E540");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3629 = new FixedLengthStringData(5).init("T3629");
	private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5400 = new FixedLengthStringData(5).init("T5400");
	private FixedLengthStringData t6691 = new FixedLengthStringData(5).init("T6691");
	private FixedLengthStringData t6692 = new FixedLengthStringData(5).init("T6692");
	private FixedLengthStringData t6693 = new FixedLengthStringData(5).init("T6693");
	private FixedLengthStringData t6694 = new FixedLengthStringData(5).init("T6694");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC   ");
	private FixedLengthStringData regprec = new FixedLengthStringData(10).init("REGPREC   ");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData hclhrec = new FixedLengthStringData(10).init("HCLHREC   ");
	private FixedLengthStringData hcldrec = new FixedLengthStringData(10).init("HCLDREC   ");
	private FixedLengthStringData hclaanlrec = new FixedLengthStringData(10).init("HCLAANLREC");
	private FixedLengthStringData hclaltlrec = new FixedLengthStringData(10).init("HCLALTLREC");
	private FixedLengthStringData hclacaprec = new FixedLengthStringData(10).init("HCLACAPREC");
	private FixedLengthStringData hclabanrec = new FixedLengthStringData(10).init("HCLABANREC");
	private FixedLengthStringData hclabltrec = new FixedLengthStringData(10).init("HCLABLTREC");
	private FixedLengthStringData hclabcarec = new FixedLengthStringData(10).init("HCLABCAREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
}
}
