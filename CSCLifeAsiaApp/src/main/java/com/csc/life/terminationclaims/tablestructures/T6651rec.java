package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:55
 * Description:
 * Copybook name: T6651REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6651rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6651Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData adjustiu = new FixedLengthStringData(1).isAPartOf(t6651Rec, 0);
  	public FixedLengthStringData bidoffer = new FixedLengthStringData(1).isAPartOf(t6651Rec, 1);
  	public FixedLengthStringData feepcs = new FixedLengthStringData(25).isAPartOf(t6651Rec, 2);
  	public ZonedDecimalData[] feepc = ZDArrayPartOfStructure(5, 5, 2, feepcs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(25).isAPartOf(feepcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData feepc02 = new ZonedDecimalData(5, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData feepc03 = new ZonedDecimalData(5, 2).isAPartOf(filler, 5);
  	public ZonedDecimalData feepc04 = new ZonedDecimalData(5, 2).isAPartOf(filler, 10);
  	public ZonedDecimalData feepc05 = new ZonedDecimalData(5, 2).isAPartOf(filler, 15);
  	public ZonedDecimalData feepc06 = new ZonedDecimalData(5, 2).isAPartOf(filler, 20);
  	public FixedLengthStringData ovrsuma = new FixedLengthStringData(1).isAPartOf(t6651Rec, 27);
  	public FixedLengthStringData pufeemaxs = new FixedLengthStringData(85).isAPartOf(t6651Rec, 28);
  	public ZonedDecimalData[] pufeemax = ZDArrayPartOfStructure(5, 17, 2, pufeemaxs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(85).isAPartOf(pufeemaxs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pufeemax02 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData pufeemax03 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 17);
  	public ZonedDecimalData pufeemax04 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 34);
  	public ZonedDecimalData pufeemax05 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 51);
  	public ZonedDecimalData pufeemax06 = new ZonedDecimalData(17, 2).isAPartOf(filler1, 68);
  	public FixedLengthStringData pufeemins = new FixedLengthStringData(85).isAPartOf(t6651Rec, 113);
  	public ZonedDecimalData[] pufeemin = ZDArrayPartOfStructure(5, 17, 2, pufeemins, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(85).isAPartOf(pufeemins, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pufeemin02 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData pufeemin03 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 17);
  	public ZonedDecimalData pufeemin04 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 34);
  	public ZonedDecimalData pufeemin05 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 51);
  	public ZonedDecimalData pufeemin06 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 68);
  	public FixedLengthStringData puffamts = new FixedLengthStringData(85).isAPartOf(t6651Rec, 198);
  	public ZonedDecimalData[] puffamt = ZDArrayPartOfStructure(5, 17, 2, puffamts, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(85).isAPartOf(puffamts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData puffamt02 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 0);
  	public ZonedDecimalData puffamt03 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 17);
  	public ZonedDecimalData puffamt04 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 34);
  	public ZonedDecimalData puffamt05 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 51);
  	public ZonedDecimalData puffamt06 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 68);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(217).isAPartOf(t6651Rec, 283, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6651Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6651Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}