package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:46
 * Description:
 * Copybook name: REGRDELKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regrdelkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regrdelFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData regrdelKey = new FixedLengthStringData(64).isAPartOf(regrdelFileKey, 0, REDEFINE);
  	public FixedLengthStringData regrdelChdrcoy = new FixedLengthStringData(1).isAPartOf(regrdelKey, 0);
  	public FixedLengthStringData regrdelChdrnum = new FixedLengthStringData(8).isAPartOf(regrdelKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(regrdelKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regrdelFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regrdelFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}