/******************************************************************************
 * File Name 		: ZlreinpfDAOImpl.java
 * Author			: rsubramani42
 * Creation Date	: 13 April 2018
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for ZLREINPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.ZlreinpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Zlreinpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public  class ZlreinpfDAOImpl extends BaseDAOImpl<Zlreinpf> implements ZlreinpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ZlreinpfDAOImpl.class);

	/*public List<Zlreinpf> searchZlreinpfRecord(<Your_Arguments>) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, CNTTYPE, OWNNUM, RDOCNUM, ");
		sqlSelect.append("EFFDATE, ORIGAMT, RCPTTYPE, FFLAG, ERORCDE, ");
		sqlSelect.append("USRPRF, JOBNM, DATIME ");
		sqlSelect.append("FROM ZLREINPF WHERE <Your_Where_Clause> ");
		sqlSelect.append("ORDER BY <Your_OrderBy_Clause> ");


		PreparedStatement psZlreinpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlZlreinpfRs = null;
		List<Zlreinpf> outputList = new ArrayList<Zlreinpf>();

		try {

			psZlreinpfSelect.setString(1, <Your_Key>);
			psZlreinpfSelect.setString(2, <Your_Key>);
			psZlreinpfSelect.setString(3, <Your_Key>);

			sqlZlreinpfRs = executeQuery(psZlreinpfSelect);

			while (sqlZlreinpfRs.next()) {

				Zlreinpf zlreinpf = new Zlreinpf();

				zlreinpf.setUniqueNumber(sqlZlreinpfRs.getInt("UNIQUE_NUMBER"));
				zlreinpf.setChdrcoy(sqlZlreinpfRs.getString("CHDRCOY"));
				zlreinpf.setChdrnum(sqlZlreinpfRs.getString("CHDRNUM"));
				zlreinpf.setCnttype(sqlZlreinpfRs.getString("CNTTYPE"));
				zlreinpf.setOwnnum(sqlZlreinpfRs.getString("OWNNUM"));
				zlreinpf.setRdocnum(sqlZlreinpfRs.getString("RDOCNUM"));
				zlreinpf.setEffdate(sqlZlreinpfRs.getInt("EFFDATE"));
				zlreinpf.setOrigamt(sqlZlreinpfRs.getBigDecimal("ORIGAMT"));
				zlreinpf.setRcpttype(sqlZlreinpfRs.getString("RCPTTYPE"));
				zlreinpf.setFflag(sqlZlreinpfRs.getString("FFLAG"));
				zlreinpf.setErorcde(sqlZlreinpfRs.getString("ERORCDE"));
				zlreinpf.setUsrprf(sqlZlreinpfRs.getString("USRPRF"));
				zlreinpf.setJobnm(sqlZlreinpfRs.getString("JOBNM"));
				zlreinpf.setDatime(sqlZlreinpfRs.getDate("DATIME"));

				outputList.add(zlreinpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchZlreinpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psZlreinpfSelect, sqlZlreinpfRs);
		}

		return outputList;
	}*/

	/*public void insertIntoZlreinpf(Zlreinpf zlreinpf){

		StringBuilder sqlInsert = new StringBuilder();

		sqlInsert.append("INSERT INTO ZLREINPF (");
		sqlInsert.append("CHDRCOY, CHDRNUM, CNTTYPE, OWNNUM, RDOCNUM, ");
		sqlInsert.append("EFFDATE, ORIGAMT, RCPTTYPE, FFLAG, ERORCDE, ");
		sqlInsert.append("USRPRF, JOBNM, DATIME ");
		sqlInsert.append(") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		PreparedStatement psZlreinpfInsert = getPrepareStatement(sqlInsert.toString());
		int index = 1;
		ResultSet rs = null;

		try {
			psZlreinpfInsert.executeQuery();
			//Write insert code here

		} catch (SQLException e) {
			LOGGER.error("insertIntoZlreinpf()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psZlreinpfInsert, null);
		}
	}*/
	
	public void bulkUpdateZlreinpf(List<Zlreinpf> zlreinpfUpdList) 
	{
		
		 if (zlreinpfUpdList != null && !zlreinpfUpdList.isEmpty()) {
	            String SQL_CHDR_UPDATE = "UPDATE ZLREINPF SET ERORCDE=?,JOBNM=?,USRPRF=?,DATIME=? WHERE CHDRNUM=? ";

	            PreparedStatement psZlreinUpdate = getPrepareStatement(SQL_CHDR_UPDATE);
	            try {
	                for (Zlreinpf c : zlreinpfUpdList) {
	                    psZlreinUpdate.setString(1, c.getErorcde());
	                    psZlreinUpdate.setString(2, getJobnm());
	                    psZlreinUpdate.setString(3, getUsrprf());
	                    psZlreinUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
	                    psZlreinUpdate.setString(5, c.getChdrnum());					
	                    psZlreinUpdate.addBatch();
	                }
	                psZlreinUpdate.executeBatch();
	            } catch (SQLException e) {
	                LOGGER.error("BulkUpdateZlreinpf()", e);//IJTI-1561
	                throw new SQLRuntimeException(e);
	            } finally {
	                close(psZlreinUpdate, null);
	            }
	        }
		
	}
	
	public void bulkDeleteZlreinpf(List<String> chdrList) {

		
		if (chdrList != null && !chdrList.isEmpty()) {
            String SQL_CHDR_DELETE = "DELETE FROM ZLREINPF WHERE CHDRNUM = ?";

            PreparedStatement psZlreinDelete = getPrepareStatement(SQL_CHDR_DELETE);
            try {
                for (String c : chdrList) {
                	psZlreinDelete.setString(1, c);			
                    psZlreinDelete.addBatch();
                }
                psZlreinDelete.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("bulkDeleteZlreinpf()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(psZlreinDelete, null);
            }
        }
		
	}

	


}
