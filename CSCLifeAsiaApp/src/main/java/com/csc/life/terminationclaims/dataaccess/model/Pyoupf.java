package com.csc.life.terminationclaims.dataaccess.model;


import java.io.Serializable;
import java.util.Date;

public class Pyoupf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "PYOUPF";

	//member variables for columns
	private long uniqueNumber;
	private String chdrpfx;
	private String chdrcoy; 
	private String chdrnum;
	private Integer tranno;
	private String batctrcde;
	private String payrnum;
	private String reqntype;
	private String bankacckey;
	private String bankkey;
	private String bankdesc;
	private String paymentflag;
	private String reqnno;
	private String rldgacct;
	private String usrprf;
	private String jobnm;
	private String datime;

	// Constructor
	public Pyoupf ( ) {}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrpfx() {
		return chdrpfx;
	}

	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public Integer getTranno() {
		return tranno;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public String getBatctrcde() {
		return batctrcde;
	}

	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

	public String getPayrnum() {
		return payrnum;
	}

	public void setPayrnum(String payrnum) {
		this.payrnum = payrnum;
	}

	public String getReqntype() {
		return reqntype;
	}

	public void setReqntype(String reqntype) {
		this.reqntype = reqntype;
	}

	public String getBankacckey() {
		return bankacckey;
	}

	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}

	public String getBankkey() {
		return bankkey;
	}

	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}

	public String getBankdesc() {
		return bankdesc;
	}

	public void setBankdesc(String bankdesc) {
		this.bankdesc = bankdesc;
	}

	public String getPaymentflag() {
		return paymentflag;
	}

	public void setPaymentflag(String paymentflag) {
		this.paymentflag = paymentflag;
	}

	public String getReqnno() {
		return reqnno;
	}

	public void setReqnno(String reqnno) {
		this.reqnno = reqnno;
	}
	
	public String getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(String rldgacct) {
		this.rldgacct = rldgacct;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	};

	@Override
	public String toString() {
		return "Abbbpf [uniqueNumber=" + uniqueNumber + ", chdrpfx=" + chdrpfx + ", chdrcoy=" + chdrcoy + ", chdrnum=" + chdrnum + ", tranno=" + tranno + ", batctrcde=" + batctrcde + ", payrnum=" + payrnum + ", reqntype=" + reqntype + ", bankacckey=" + bankacckey + ",bankkey=" + bankkey + ", bankdesc=" + bankdesc + ", paymentflag=" + paymentflag + ",reqnno=" + reqnno + ", usrprf=" + usrprf + ", jobnm=" + jobnm + ", datime=" + datime + "]";
	}
}