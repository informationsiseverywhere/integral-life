package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR583
 * @version 1.0 generated on 30/08/09 07:20
 * @author Quipoz
 */
public class Sr583ScreenVars extends SmartVarModel { 

	//ILIFE-1821 starts
//	public FixedLengthStringData dataArea = new FixedLengthStringData(387);
//	public FixedLengthStringData dataFields = new FixedLengthStringData(147).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataArea = new FixedLengthStringData(392);
	public FixedLengthStringData dataFields = new FixedLengthStringData(152).isAPartOf(dataArea, 0);
	//ILIFE-1821 ends
	public ZonedDecimalData acbenunt = DD.acbenunt.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData acdben = DD.acdben.copy().isAPartOf(dataFields,3);
	//ILIFE-1821 starts
	public ZonedDecimalData amtaout = DD.amtaout1.copyToZonedDecimal().isAPartOf(dataFields,8);
	public FixedLengthStringData benefits = DD.benefits.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData benfreq = DD.benfreq.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData comt = DD.comt.copy().isAPartOf(dataFields,52);
	public ZonedDecimalData cvbenunt = DD.cvbenunt.copyToZonedDecimal().isAPartOf(dataFields,82);
	public ZonedDecimalData datefrom = new ZonedDecimalData(8, 0).isAPartOf(dataFields, 85);
	public ZonedDecimalData dateto = DD.dateto.copyToZonedDecimal().isAPartOf(dataFields,93);
	public FixedLengthStringData gcdblind = DD.gcdblind.copy().isAPartOf(dataFields,101);
//	public ZonedDecimalData gcnetpy = DD.gcnetpy.copyToZonedDecimal().isAPartOf(dataFields,102);
	public ZonedDecimalData gcnetpy = DD.amtaout1.copyToZonedDecimal().isAPartOf(dataFields,102);
	public ZonedDecimalData gincurr = DD.gincurr.copyToZonedDecimal().isAPartOf(dataFields,115);
	public ZonedDecimalData gtotinc = DD.gtotinc.copyToZonedDecimal().isAPartOf(dataFields,128);
	public FixedLengthStringData inqopt = DD.inqopt.copy().isAPartOf(dataFields,141);
	public ZonedDecimalData mxbenunt = DD.mxbenunt.copyToZonedDecimal().isAPartOf(dataFields,149);
//	public FixedLengthStringData errorIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 147);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 152);
	//ILIFE-1821 ends
	public FixedLengthStringData acbenuntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acdbenErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData amtaoutErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData benefitsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData benfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData comtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cvbenuntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData datefromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData datetoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData gcdblindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData gcnetpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData gincurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData gtotincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData inqoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mxbenuntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	//ILIFE-1821 starts
//	public FixedLengthStringData outputIndicators = new FixedLengthStringData(180).isAPartOf(dataArea, 207);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(180).isAPartOf(dataArea, 212);
	//ILIFE-1821 ends
	public FixedLengthStringData[] acbenuntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acdbenOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] amtaoutOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] benefitsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] benfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] comtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cvbenuntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] datefromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] datetoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] gcdblindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] gcnetpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] gincurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] gtotincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] inqoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mxbenuntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData datefromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData datetoDisp = new FixedLengthStringData(10);

	public LongData Sr583screenWritten = new LongData(0);
	public LongData Sr583windowWritten = new LongData(0);
	public LongData Sr583hideWritten = new LongData(0);
	public LongData Sr583protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr583ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(datefromOut,new String[] {"20","70","-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datetoOut,new String[] {"21","70","-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gincurrOut,new String[] {"22","70","-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(acbenuntOut,new String[] {"23","71","-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(gcnetpyOut,new String[] {"24","70","-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comtOut,new String[] {null, null, null, "75",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {inqopt, acdben, benefits, benfreq, gcdblind, datefrom, dateto, gincurr, acbenunt, amtaout, mxbenunt, gtotinc, cvbenunt, gcnetpy, comt};
		screenOutFields = new BaseData[][] {inqoptOut, acdbenOut, benefitsOut, benfreqOut, gcdblindOut, datefromOut, datetoOut, gincurrOut, acbenuntOut, amtaoutOut, mxbenuntOut, gtotincOut, cvbenuntOut, gcnetpyOut, comtOut};
		screenErrFields = new BaseData[] {inqoptErr, acdbenErr, benefitsErr, benfreqErr, gcdblindErr, datefromErr, datetoErr, gincurrErr, acbenuntErr, amtaoutErr, mxbenuntErr, gtotincErr, cvbenuntErr, gcnetpyErr, comtErr};
		screenDateFields = new BaseData[] {datefrom, dateto};
		screenDateErrFields = new BaseData[] {datefromErr, datetoErr};
		screenDateDispFields = new BaseData[] {datefromDisp, datetoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr583screen.class;
		protectRecord = Sr583protect.class;
		hideRecord = Sr583hide.class;
	}

}
