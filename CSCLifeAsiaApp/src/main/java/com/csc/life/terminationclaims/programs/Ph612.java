/*
 * File: Ph612.java
 * Date: 30 August 2009 1:10:57
 * Author: Quipoz Limited
 * 
 * Class transformed from PH612.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.CovrpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Covrpf;
import com.csc.life.contractservicing.procedures.UwoccupationUtil;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.UwrspfDAO;
import com.csc.life.newbusiness.dataaccess.model.Uwrspf;
import com.csc.life.newbusiness.tablestructures.Th565rec;
import com.csc.life.productdefinition.recordstructures.UWOccupationRec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.screens.Sh612ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Td5j1rec;
import com.csc.smart.procedures.BatcdorUtil;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 * Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
 *REMARKS.
 *
 *        LAPSE REINSTATEMENT SUB MENU
 *        ----------------------------
 *
 * Overview.
 * ---------
 *
 *  This is the Lapse Reinstatement Submenun program. This screen
 *  (SH612) allows the user to select a Contract, an Effective
 *  date and an Action.
 *  The Contract validation is described below, the date must be
 *   a valid date and the  Action  must also be valid - see  the
 *   PH612 entry in T1690 for allowable Actions.
 *
 * Validation.
 * -----------
 *
 *    Key 1 - Contract number:
 *    -----
 *  The Contract must exist.
 *  The Contract must have a valid status for transaction that is
 *   being selected - check this via the table T5679.
 *  The Contract must be either Lapsed or Surrendered.
 *   Get its latest policy transaction so as to reveals that
 *   this contract is not yet processed by Full Contract Reversal
 *   AT module (via PTRN-BATCTRCDE CALL TRCDECHK).
 *
 *    The ACTION must be a valid selection.
 *     Currently, the Actions are 'A' - Lapse Reinstatement
 *                                'B' - Lapse Reinstatement Enquiry
 *                                'C' - Reinstatement with Redating
 *                                'D' - Reverse Contract for Redating
 *                                'E' - Convert to Proposal for Redating
 *   The Reinstatement Date inputed must be within the succeeding
 *   year of the current Billing renewal date (i.e. Screen
 *   Reinstatement Date not < PAYR-BILLCD and with the succeeding
 *   year of PAYR-BILLCD)
 *
 * Updating.
 * ---------
 *
 * Softlock the Contract Header - If already locked, display
 *  an error message.
 *
 * Store CHDRMJA/CHDRLIF/CHDRLNB for use by next program in
 * stack - do KEEPS
 * Update the Batch Transaction Code.
 * Commence Batching if required.
 *
 *****************************************************************
 * </pre>
 */
public class Ph612 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH612");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	protected ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	protected FixedLengthStringData wsaaTrcde = new FixedLengthStringData(1);
	protected Validator trcdeMatch = new Validator(wsaaTrcde, "Y");
	protected Validator trcdeNotMatch = new Validator(wsaaTrcde, "N");

	private FixedLengthStringData wsaaPrcde = new FixedLengthStringData(1);
	protected Validator prcdeMatch = new Validator(wsaaPrcde, "Y");
	protected Validator prcdeNotMatch = new Validator(wsaaPrcde, "N");
	private String wsaaFirstTime = "Y";
	protected FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4);
	/* TABLES */
	protected static final String t5679 = "T5679";
	protected static final String t6661 = "T6661";
	protected static final String th565 = "TH565";
	/* FORMATS */
	protected static final String chdrmjarec = "CHDRMJAREC";
	protected static final String chdrlifrec = "CHDRLIFREC";
	protected static final String chdrlnbrec = "CHDRLNBREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	protected ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	protected Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	protected T5679rec t5679rec = new T5679rec();
	protected T6661rec t6661rec = new T6661rec();
	protected Th565rec th565rec = new Th565rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	protected Datcon4rec datcon4rec = new Datcon4rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	protected Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	protected Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	protected Sdasancrec sdasancrec = new Sdasancrec();
	protected Sh612ScreenVars sv = getLScreenVars();
	
	protected Sh612ScreenVars getLScreenVars() {

		return ScreenProgram.getScreenVars(Sh612ScreenVars.class);

	}
	protected ErrorsInner errorsInner = new ErrorsInner();

	
	//MIBT-240 STARTS
	private static final String b673 = "B673";
	private static final String t514 = "T514";
	private static final String t575 = "T575";
	//MIBT-240 ENDS
	private static final String t6A3 = "T6A3";	//MIBT-311
	//fwang3
	protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    boolean CSMIN003Permission  = false;
    boolean ctcnl002Permission = false; // IBPLIFE-1092
    private Td5j1rec td5j1rec = new Td5j1rec();
	private static final String td5j1 = "TD5J1";
	private boolean reinstflag = false;
/**
 * Contains all possible labels used by goTo action.
 */
	protected PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO",  PtrnpfDAO.class);
	protected PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	protected  Ptrnpf ptrnpf;
	private Itempf itempf = null;
	protected Boolean wsaaEof;
	private Payrpf payrpf;
	private Sftlockrec sftlockrec = new Sftlockrec();
	private boolean uwFlag = false;
	public static final String UWQUESTIONNAIRECACHE= "UWQUESTIONNAIRE";
	private UWOccupationRec uwOccupationRec = new UWOccupationRec();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAOP6351", CovrpfDAO.class);
	private UwrspfDAO uwrspfDAO = getApplicationContext().getBean("uwrspfDAO", UwrspfDAO.class);
	private Clntpf clntpfobj =  new Clntpf();
	private List<Covrpf> covrpfobjList;
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	private UwoccupationUtil uwoccupationUtil = getApplicationContext().
			getBean("uwoccupationUtil", UwoccupationUtil.class);
	boolean flag = false;
	public static final String UWREC_CACHE = "UWREC_CACHE";
	private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumChanged = new FixedLengthStringData(1);
	private List<String> wsaaCrtableList;
	
	private boolean lapseReinstJpnFlag = false;	
	private static final String CSLRI007="CSLRI007"; 	
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		exit2190, 
		exit12090, 
		exit3090,
		//MIBT-240 STARTS
		exit2290
		//MIBT-240 ENDS
	}

	public Ph612() {
		super();
		screenVars = sv;
		new ScreenModel("Sh612", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void initialise1000()
	{
		initialise1010();
	}

	protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaEof = false;
		if (isNE(wsaaBatckey.batcBatcactmn,wsspcomn.acctmonth)
				|| isNE(wsaaBatckey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		if (isEQ(wsaaFirstTime,"Y")) {
			wsaaFirstTime = "N";
			wsaaBatctrcde.set(wsaaBatckey.batcBatctrcde);
		}
		sv.effdate.set(varcom.vrcmMaxDate);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
        CSMIN003Permission  = FeaConfg.isFeatureExist("2", "CSMIN003", appVars, "IT");
        reinstflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSLRI003", appVars, "IT");
        ctcnl002Permission  = FeaConfg.isFeatureExist("2", "CTCNL002", appVars, "IT");     // IBPLIFE-1092
        lapseReinstJpnFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), CSLRI007, appVars, "IT"); //IBPLIFE-3729
        if(CSMIN003Permission) {
            sv.fuflag.set("Y");
        } else {
            sv.fuflag.set("N");
        }
        uwFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP123", appVars, smtpfxcpy.item.toString());
		if(uwFlag) {
			sv.uwFlag = 1;
			initialiseUWparameter();
		}else {
			sv.uwFlag = 2;
		}
        wsaaLastChdrnum.set(ZERO);
        wsaaChdrnumChanged.set(SPACES);
	}
	
	private void initialiseUWparameter() {
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		uwlvIO.setParams(SPACES);
		uwlvIO.setCompany(wsspcomn.company);
		uwlvIO.setUserid(wsspcomn.userid);
		uwlvIO.setFormat("UWLVREC");
		uwlvIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, uwlvIO);
		sv.mainCovrDisFlag = -1;
}
	
	protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

	protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'SH612IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SH612-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		
		if (isNE(sv.chdrsel, wsaaLastChdrnum)) {
			wsaaChdrnumChanged.set("Y");
			wsaaLastChdrnum.set(sv.chdrsel);
		}
		else {
			wsaaChdrnumChanged.set("N");
		}
		
		/*VALIDATE*/
		validateAction2100();
		if(ctcnl002Permission){
			if(isEQ(sv.effdate,varcom.vrcmMaxDate)){
				this.wsspcomn.occdate.set(wsaaToday);
			}else{
				this.wsspcomn.occdate.set(sv.effdate);
				if(isGT(sv.effdate, wsaaToday)){
					sv.effdateErr.set(errorsInner.f073);
				}
			}
		
		}		
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		processUW();
		
		/*IBPLIFE-3729 IBPLIFE-3731 - Start*/
		if(lapseReinstJpnFlag){			
			validateReinstateAllowPeriod2160();
		}
		/*IBPLIFE-3729  IBPLIFE-3731 - End*/
		
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}
	private void processUW() {
		if(uwFlag && isNE(sv.chdrsel,SPACES) &&isEQ(sv.uworflagErr,SPACES) && isNE(sv.uworflag,"Y")) {
			clntpfobj.setClntpfx("CN");
			clntpfobj.setClntnum(chdrmjaIO.getCownnum().toString());
			clntpfobj.setClntcoy(wsspcomn.fsuco.toString());
			clntpfobj = clntpfDAO.selectActiveClient(clntpfobj);
			uwOccupationRec = new UWOccupationRec();
			uwOccupationRec.indusType.set(clntpfobj.getStatcode());
			uwOccupationRec.occupCode.set(clntpfobj.getOccpcode());
			uwOccupationRec.transEffdate.set(datcon1rec.intDate);
			covrpfobjList = covrpfDAO.searchCovrRecordByCoyNumDescUniquNo(wsspcomn.company.toString(), sv.chdrsel.toString());
			String company = wsspcomn.company.toString();
			String fsuco = wsspcomn.fsuco.toString();
			String cnttype = chdrmjaIO.getCnttype().toString();
			wsaaCrtableList = new ArrayList<String>();
			for(Covrpf covrpfobj: covrpfobjList) {
				if (covrpfobj.getCrtable() != null && !covrpfobj.getCrtable().isEmpty()) {
					int result = uwoccupationUtil.getUWResult(uwOccupationRec, company, fsuco, cnttype, covrpfobj.getCrtable());
					checkUWResult(result, covrpfobj.getCrtable().trim());
				}
			}
//			if(sv.mainCovrDisFlag == 1){
//				goTo(GotoLabel.exit3090);
//			}
		}
	}
	private void checkUWResult(int result, String crtable) {
		
//		if (result == 2 &&isEQ(wsaaChdrnumChanged, "N")) {
//			return ;
//		}
		if (result == 2 && isEQ(uwlvIO.getUwdecision(),'N')) {
			scrnparams.errorCode.set("UWDV");
			wsspcomn.edterror.set("Y");
		}
		if(result == 0){
			sv.mainCovrDisFlag = -1;
		}
		if(result == 1 || (result == 2 && isEQ(uwlvIO.getUwdecision(),'Y'))){
			sv.mainCovrDisFlag = 1;
			wsaaCrtableList.add(crtable);
			
			wsspcomn.edterror.set("Y");
		}
	}
	protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
		}
	}

	protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

	protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

	protected void validateKeys2200()
	{
		validateKeys2210();
	}

	protected void validateKeys2210()
	{
		if (isEQ(sv.chdrsel,SPACES)) {
			sv.chdrselErr.set(errorsInner.g667);
			return ;
		}
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)
				&& isNE(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmjaIO.getStatuz(),varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.f259);
			return ;
		}
		if (isNE(chdrmjaIO.getCntbranch(),wsspcomn.branch)) {
			sv.chdrselErr.set(errorsInner.e455);
			return ;
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			return ;
		}
		if (isEQ(chdrmjaIO.getBillfreq(), "00")) {
			sv.chdrselErr.set(errorsInner.h085);
			return ;
		}
		/* If Reversing Contract for Redating prepare to KEEPS on CHDRLIF  */
		if (isEQ(scrnparams.action,"D")) {
			chdrlifIO.setParams(SPACES);
			chdrlifIO.setChdrcoy(wsspcomn.company);
			chdrlifIO.setChdrnum(sv.chdrsel);
			chdrlifIO.setFormat(chdrlifrec);
			chdrlifIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(),varcom.oK)
					&& isNE(chdrlifIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
		}
		if (isEQ(scrnparams.action,"E")) {
			chdrlnbIO.setParams(SPACES);
			chdrlnbIO.setChdrcoy(wsspcomn.company);
			chdrlnbIO.setChdrnum(sv.chdrsel);
			chdrlnbIO.setFormat(chdrlnbrec);
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
					&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
		}

		
			
			List<Itempf> itemList = this.itemDAO.getAllItemitem(smtpfxcpy.item.toString(), wsspcomn.company.toString(), t5679, subprogrec.transcd.toString());
			if (itemList.isEmpty()) {
				sv.chdrselErr.set(errorsInner.f321);
				return ;
			}
			t5679rec.t5679Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
			trcdeNotMatch.setTrue();
			for (wsaaSub.set(1); !(isGT(wsaaSub,12)
			|| trcdeMatch.isTrue()); wsaaSub.add(1)){
				if (isEQ(chdrmjaIO.getStatcode(),t5679rec.cnRiskStat[wsaaSub.toInt()])) {
					trcdeMatch.setTrue();
				}
			}
			if (trcdeNotMatch.isTrue()) {
				sv.chdrselErr.set(errorsInner.h137);
				return ;
			}
			/*   Validate Contract Premium status code                         */
			prcdeNotMatch.setTrue();
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
			|| prcdeMatch.isTrue()); wsaaSub.add(1)){
				if (isEQ(chdrmjaIO.getPstatcode(), t5679rec.cnPremStat[wsaaSub.toInt()])) {
					prcdeMatch.setTrue();
				}
			}
			if (prcdeNotMatch.isTrue()) {
				sv.chdrselErr.set(errorsInner.h504);
				return ;
			}
			/*   Validate Reinstatement Date .....*/
			if (isNE(sv.effdateErr,SPACES)) {
				return ;
			}
			if (isEQ(sv.effdate,varcom.vrcmMaxDate)) {
				sv.effdate.set(wsaaToday);
			}
			if (isEQ(subprogrec.key2,"Y")) {
				if (isGT(sv.effdate,wsaaToday)) {
					sv.effdateErr.set(errorsInner.f073);
					return ;
				}
			}


			payrpf = payrpfDAO.getpayrRecord(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
		
			if (payrpf == null) {
				syserrrec.params.set(chdrmjaIO.getChdrcoy().toString().concat(chdrmjaIO.getChdrnum().toString()));
				syserrrec.statuz.set(errorsInner.f379);
				fatalError600();
			}
			datcon4rec.freqFactor.set(payrpf.getBillfreq());
			datcon4rec.frequency.set(payrpf.getBillfreq());
			datcon4rec.intDate1.set(payrpf.getBillcd());
			datcon4rec.billday.set(payrpf.getBillday());
			datcon4rec.billmonth.set(payrpf.getBillmonth());
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				fatalError600();
			}
			if(!reinstflag && !lapseReinstJpnFlag){
			if (isNE(sv.action,"C")
			&& isNE(sv.action,"D")
			&& isNE(sv.action,"E")
			&& isNE(sv.action,"F") ) {
				if (isGTE(sv.effdate,datcon4rec.intDate2)) {
					sv.effdateErr.set(errorsInner.hl66);
					return ;
				}
			}
			}
			if (isLT(sv.effdate,payrpf.getBillcd())) {
				sv.effdateErr.set(errorsInner.rl00);
				return ;
			}
//MIBT-240 STARTS

			List<Ptrnpf> ptrnpfList = ptrnpfDAO.searchPtrnrevData(chdrmjaIO.getChdrcoy().toString(),chdrmjaIO.getChdrnum().toString());
			if(ptrnpfList.isEmpty()){
				syserrrec.params.set("searchPtrnenqRecord  ".concat(chdrmjaIO.getChdrcoy().toString()).concat("  ").concat(chdrmjaIO.getChdrnum().toString()));
				fatalError600();
			}

			Iterator<Ptrnpf> iteratorPtrnpf =   ptrnpfList.iterator();


			while ( !(wsaaEof) ) {
				if(iteratorPtrnpf.hasNext()){
					ptrnpf = iteratorPtrnpf.next();
					processPtrn2230();
				}
				else
					wsaaEof = true;	
			}
			if(reinstflag){
				getTd5j1();
				checkTD5J1Period();
			}
			if(isNE(sv.action,"F")) {
				itemList = this.itemDAO.getAllItemitem(smtpfxcpy.item.toString(), wsspcomn.company.toString(), th565, wsaaBatctrcde.toString());
				if (itemList.isEmpty()) {
					sv.chdrselErr.set(errorsInner.hl68);
					return ;
				}
				th565rec.th565Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
				trcdeNotMatch.setTrue();
				for (wsaaSub.set(1); !(isGT(wsaaSub,10)
				|| trcdeMatch.isTrue()); wsaaSub.add(1)){
					if (isEQ(ptrnpf.getBatctrcde(),th565rec.ztrcde[wsaaSub.toInt()])) {
						trcdeMatch.setTrue();
					}
				}
				if (trcdeNotMatch.isTrue()) {
					sv.chdrselErr.set(errorsInner.hl69);
					return ;
				}
	//			itemIO.setParams(SPACES);
	//			itemIO.setItempfx(smtpfxcpy.item);
	//			itemIO.setItemcoy(wsspcomn.company);
	//			itemIO.setItemtabl(t6661);
	//			itemIO.setItemitem(ptrnrevIO.getBatctrcde());
	//			itemIO.setFormat(itemrec);
	//			itemIO.setFunction(varcom.readr);
	//			SmartFileCode.execute(appVars, itemIO);
	//			if (isNE(itemIO.getStatuz(),varcom.oK)
	//			&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
	//				syserrrec.params.set(itemIO.getParams());
	//				fatalError600();
	//			}
	//			if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
	//				sv.chdrselErr.set(errorsInner.f014);
	//				return ;
	//			}
	//			t6661rec.t6661Rec.set(itemIO.getGenarea());
				//fwang3
				itemList = this.itemDAO.getAllItemitem(smtpfxcpy.item.toString(), wsspcomn.company.toString(), t6661, ptrnpf.getBatctrcde());/* IJTI-1523 */
				if (itemList.isEmpty()) {
					sv.chdrselErr.set(errorsInner.f014);
					return ;
				}
				t6661rec.t6661Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
				if (isNE(t6661rec.contRevFlag,"Y")) {
					sv.chdrselErr.set(errorsInner.hl67);
				}		
			}
	}	
	
	protected void validateReinstateAllowPeriod2160(){
		getTd5j1();	
		checkTD5J1PeriodWithPaidToDt();
	}	
	
	protected void checkTD5J1PeriodWithPaidToDt(){		
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate2.set(sv.effdate);
		datcon3rec.intDate1.set(chdrmjaIO.getPtdate());
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(sv.effdate, varcom.vrcmMaxDate) && isLT(chdrmjaIO.getPtdate(), sv.effdate) && isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		else {
			if(isNE(td5j1rec.allowperiod,SPACES) && isNE(td5j1rec.allowperiod,0)){
				if(isGT(datcon3rec.freqFactor,td5j1rec.allowperiod)){
					sv.effdateErr.set(errorsInner.ruom);
					wsspcomn.edterror.set("Y");
				}
			}	
		}
	}
		
	protected void getTd5j1(){
		
		itempf = new Itempf();

		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(chdrmjaIO.getChdrcoy().toString().trim());
		itempf.setItemtabl(td5j1);
		itempf.setItemitem(chdrmjaIO.getCnttype().toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(chdrmjaIO.getChdrcoy().toString());
			itempf.setItemtabl(td5j1);
			itempf.setItemitem("***");
	        itempf = itemDAO.getItempfRecord(itempf);
	        if(itempf == null){
	        	syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat(td5j1).concat("***"));
				fatalError600();
	        }
		}
		td5j1rec.td5j1Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
	}
	protected void checkTD5J1Period(){
		
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate2.set(sv.effdate);
		datcon3rec.intDate1.set(ptrnpf.getDatesub());
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		else {
			if(isNE(td5j1rec.allowperiod,SPACES) && isNE(td5j1rec.allowperiod,0)){
			if(isGT(datcon3rec.freqFactor,td5j1rec.allowperiod)){
				sv.effdateErr.set(errorsInner.rrj2);
				wsspcomn.edterror.set("Y");
			}
		  }	
		}
	}
	//MIBT-240 STARTS
	protected void processPtrn2230()
	{
		// ILB460
		if ((isEQ(ptrnpf.getBatctrcde(),b673)) ||
				(isEQ(ptrnpf.getBatctrcde(),t514)) ||
				(isEQ(ptrnpf.getBatctrcde(),t575))) {
			wsaaEof = true;
		}
		if ((isEQ(ptrnpf.getBatctrcde(),t6A3)) &&
				(isEQ(scrnparams.action,"D") ||
						isEQ(scrnparams.action,"E"))) {
			wsaaEof = true;
		}
		if (isNE(ptrnpf.getChdrcoy(),chdrlifIO.getChdrcoy())
				|| isNE(ptrnpf.getChdrnum(),chdrlifIO.getChdrnum())) {
			wsaaEof = true;
		}
	}
		
	//MIBT-240 STARTS

	//MIBT-240 ENDS
	protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
		}
	}

	protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(errorsInner.e073);
			goTo(GotoLabel.exit12090);
		}
	}

	protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	protected void update3000()
	{
		try {
			updateWssp3010();
			batching3080();
		}
		catch (GOTOException e){
		}
	}

	protected void updateWssp3010()
	{
		validateUwrs();
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaBatckey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatckey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		wsspcomn.currfrom.set(sv.effdate);
		chdrmjaIO.setFunction(varcom.keeps);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(scrnparams.action,"D")) {
			chdrlifIO.setFunction(varcom.keeps);
			chdrlifIO.setFormat(chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
		}
		if (isEQ(scrnparams.action,"E")) {
			chdrlnbIO.setFunction(varcom.keeps);
			chdrlnbIO.setFormat(chdrlnbrec);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
		}
		wsspcomn.flag.set("M");
		if (isEQ(scrnparams.action,"A")) {
			wsspcomn.flag.set("C");
		}
		if (isEQ(scrnparams.action,"B")) {
			wsspcomn.flag.set("I");
		}
		if (isEQ(scrnparams.action,"C")
				|| isEQ(scrnparams.action,"D")
				|| isEQ(scrnparams.action,"E")) {
			wsspcomn.flag.set("R");
		}
		if (isEQ(wsspcomn.flag,"C")
				|| isEQ(wsspcomn.flag,"R") || isEQ(wsspcomn.flag,"M")) {
			softlockContract5000();
		}
		wsaaBatckey.batcBatctrcde.set(ptrnpf.getBatctrcde());
		wsspcomn.batchkey2.set(wsaaBatckey);
	}

	private void validateUwrs() {
		if(uwFlag && isNE(sv.uworreason,SPACE) && isEQ(sv.uworflag,"Y")&& !wsaaCrtableList.isEmpty()){
			saveUwrspfRecord();
		}
	}
	protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
				&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

	protected void updateBatchControl3100()
	{
		// ILB-460
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
//		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		BatcdorUtil.getInstance().process(batcdorrec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
	private void saveUwrspfRecord() {
		Uwrspf uwrspf = new Uwrspf();
		uwrspf.setChdrcoy(chdrmjaIO.getChdrcoy().toString());
		uwrspf.setChdrnum(chdrmjaIO.getChdrnum().toString());
//		uwrspf.setCrtable(wsaaCrtable.toString());
		uwrspf.setReason(sv.uworreason.toString());
		uwrspf.setTrancde(wsaaBatckey.batcBatctrcde.toString());
		uwrspf.setTranno(chdrlnbIO.getTranno().toInt());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		uwrspf.setTrdt(datcon1rec.intDate.toInt());
		uwrspf.setTrtm(varcom.vrcmTime.toInt());
		uwrspf.setUser(varcom.vrcmUser.toInt());
		uwrspf.setTermid(varcom.vrcmTermid.toString());
		List<Uwrspf> uwrsList = new ArrayList<Uwrspf>();
		for(String crtable:wsaaCrtableList){
			uwrspf.setCrtable(crtable);
			uwrsList.add(uwrspf);
		}
		uwrspfDAO.insertUwrsData(uwrsList);
			
	}
	protected void softlockContract5000()
	{
		// ILB-460
//		SftlockRecBean sftlockbean = new SftlockRecBean();
//		sftlockbean.setFunction("LOCK");
//		sftlockbean.setCompany(wsspcomn.company.toString());
//		sftlockbean.setEnttyp(fsupfxcpy.chdr.toString());
//		sftlockbean.setEntity(sv.chdrsel.toString());
//		sftlockbean.setTransaction(subprogrec.transcd.toString());
//		sftlockbean.setUser(varcom.vrcmUser.toString());
//		SftlockUtil sftlockTool = new SftlockUtil();
//		sftlockbean = sftlockTool.process(sftlockbean, appVars);
//		if (isNE(sftlockbean.getStatuz(),varcom.oK)
//				&& isNE(sftlockbean.getStatuz(),"LOCK")) {
//			syserrrec.statuz.set(sftlockbean.getStatuz());
//			fatalError600();
//		}
//		if (isEQ(sftlockbean.getStatuz(),"LOCK")) {
//			sv.chdrselErr.set(errorsInner.f910);
//			wsspcomn.edterror.set("Y");
//		}
		
	softlockContract5010();
	}

	
	protected void softlockContract5010()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set(fsupfxcpy.chdr);
		sftlockrec.entity.set(sv.chdrsel);
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
				&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
		}
	}
	// ILB-460
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	protected static final class ErrorsInner { 
		/* ERRORS */
		private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
		private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
		public FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
		public FixedLengthStringData f014 = new FixedLengthStringData(4).init("F014");
		public FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
		public FixedLengthStringData f259 = new FixedLengthStringData(4).init("F259");
		public FixedLengthStringData f321 = new FixedLengthStringData(4).init("F321");
		public FixedLengthStringData f379 = new FixedLengthStringData(4).init("F379");
		public FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
		public FixedLengthStringData g667 = new FixedLengthStringData(4).init("G667");
		public FixedLengthStringData h137 = new FixedLengthStringData(4).init("H137");
		public FixedLengthStringData h504 = new FixedLengthStringData(4).init("H504");
		public FixedLengthStringData hl66 = new FixedLengthStringData(4).init("HL66");
		public FixedLengthStringData hl67 = new FixedLengthStringData(4).init("HL67");
		public FixedLengthStringData rl00 = new FixedLengthStringData(4).init("RL00");
		public FixedLengthStringData hl68 = new FixedLengthStringData(4).init("HL68");
		public FixedLengthStringData hl69 = new FixedLengthStringData(4).init("HL69");
		public FixedLengthStringData h085 = new FixedLengthStringData(4).init("H085");
		public FixedLengthStringData rrj2 = new FixedLengthStringData(4).init("RRJ2");		
		public FixedLengthStringData ruom = new FixedLengthStringData(4).init("RUOM");
	}
	
}
