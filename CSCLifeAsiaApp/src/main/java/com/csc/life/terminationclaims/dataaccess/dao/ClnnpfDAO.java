package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ClnnpfDAO extends BaseDAO<Clnnpf>{
	
	public boolean insertClnnpf(Clnnpf clnnpf);
	public Clnnpf getClnnpfBySeq(long uniqueNumber);
	public List<Clnnpf> getClnnpfList(String clnncoy, String notifinum, String claimno);
	public int removeClnnpfRecord(long uniqueNumber);
	public int updateClnnpf(Clnnpf clnnpf);
	public int updateClnnpfClaimno(String claimno, String notifinum);
}
