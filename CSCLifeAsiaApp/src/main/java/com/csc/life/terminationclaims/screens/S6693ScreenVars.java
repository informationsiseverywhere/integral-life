package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6693
 * @version 1.0 generated on 30/08/09 06:58
 * @author Quipoz
 */
public class S6693ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(612);
	public FixedLengthStringData dataFields = new FixedLengthStringData(132).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData rgpystats = new FixedLengthStringData(24).isAPartOf(dataFields, 55);
	public FixedLengthStringData[] rgpystat = FLSArrayPartOfStructure(12, 2, rgpystats, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(rgpystats, 0, FILLER_REDEFINE);
	public FixedLengthStringData rgpystat01 = DD.rgpystat.copy().isAPartOf(filler,0);
	public FixedLengthStringData rgpystat02 = DD.rgpystat.copy().isAPartOf(filler,2);
	public FixedLengthStringData rgpystat03 = DD.rgpystat.copy().isAPartOf(filler,4);
	public FixedLengthStringData rgpystat04 = DD.rgpystat.copy().isAPartOf(filler,6);
	public FixedLengthStringData rgpystat05 = DD.rgpystat.copy().isAPartOf(filler,8);
	public FixedLengthStringData rgpystat06 = DD.rgpystat.copy().isAPartOf(filler,10);
	public FixedLengthStringData rgpystat07 = DD.rgpystat.copy().isAPartOf(filler,12);
	public FixedLengthStringData rgpystat08 = DD.rgpystat.copy().isAPartOf(filler,14);
	public FixedLengthStringData rgpystat09 = DD.rgpystat.copy().isAPartOf(filler,16);
	public FixedLengthStringData rgpystat10 = DD.rgpystat.copy().isAPartOf(filler,18);
	public FixedLengthStringData rgpystat11 = DD.rgpystat.copy().isAPartOf(filler,20);
	public FixedLengthStringData rgpystat12 = DD.rgpystat.copy().isAPartOf(filler,22);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData trcodes = new FixedLengthStringData(48).isAPartOf(dataFields, 84);
	public FixedLengthStringData[] trcode = FLSArrayPartOfStructure(12, 4, trcodes, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(48).isAPartOf(trcodes, 0, FILLER_REDEFINE);
	public FixedLengthStringData trcode01 = DD.trcode.copy().isAPartOf(filler1,0);
	public FixedLengthStringData trcode02 = DD.trcode.copy().isAPartOf(filler1,4);
	public FixedLengthStringData trcode03 = DD.trcode.copy().isAPartOf(filler1,8);
	public FixedLengthStringData trcode04 = DD.trcode.copy().isAPartOf(filler1,12);
	public FixedLengthStringData trcode05 = DD.trcode.copy().isAPartOf(filler1,16);
	public FixedLengthStringData trcode06 = DD.trcode.copy().isAPartOf(filler1,20);
	public FixedLengthStringData trcode07 = DD.trcode.copy().isAPartOf(filler1,24);
	public FixedLengthStringData trcode08 = DD.trcode.copy().isAPartOf(filler1,28);
	public FixedLengthStringData trcode09 = DD.trcode.copy().isAPartOf(filler1,32);
	public FixedLengthStringData trcode10 = DD.trcode.copy().isAPartOf(filler1,36);
	public FixedLengthStringData trcode11 = DD.trcode.copy().isAPartOf(filler1,40);
	public FixedLengthStringData trcode12 = DD.trcode.copy().isAPartOf(filler1,44);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 132);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData rgpystatsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] rgpystatErr = FLSArrayPartOfStructure(12, 4, rgpystatsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(rgpystatsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rgpystat01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData rgpystat02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData rgpystat03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData rgpystat04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData rgpystat05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData rgpystat06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData rgpystat07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData rgpystat08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData rgpystat09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData rgpystat10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData rgpystat11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData rgpystat12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData trcodesErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] trcodeErr = FLSArrayPartOfStructure(12, 4, trcodesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(trcodesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData trcode01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData trcode02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData trcode03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData trcode04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData trcode05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData trcode06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData trcode07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData trcode08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData trcode09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData trcode10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData trcode11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData trcode12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 252);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData rgpystatsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] rgpystatOut = FLSArrayPartOfStructure(12, 12, rgpystatsOut, 0);
	public FixedLengthStringData[][] rgpystatO = FLSDArrayPartOfArrayStructure(12, 1, rgpystatOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(144).isAPartOf(rgpystatsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rgpystat01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] rgpystat02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] rgpystat03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] rgpystat04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] rgpystat05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] rgpystat06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] rgpystat07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] rgpystat08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] rgpystat09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] rgpystat10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] rgpystat11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] rgpystat12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData trcodesOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(12, 12, trcodesOut, 0);
	public FixedLengthStringData[][] trcodeO = FLSDArrayPartOfArrayStructure(12, 1, trcodeOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(144).isAPartOf(trcodesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] trcode01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] trcode02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] trcode03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] trcode04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] trcode05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] trcode06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] trcode07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] trcode08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] trcode09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] trcode10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] trcode11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] trcode12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S6693screenWritten = new LongData(0);
	public LongData S6693protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6693ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(trcode01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat01Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat02Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat03Out,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat04Out,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat05Out,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat06Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat07Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat08Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat09Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat10Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode11Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat11Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(trcode12Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rgpystat12Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, trcode01, rgpystat01, trcode02, rgpystat02, trcode03, rgpystat03, trcode04, rgpystat04, trcode05, rgpystat05, trcode06, rgpystat06, trcode07, rgpystat07, trcode08, rgpystat08, trcode09, rgpystat09, trcode10, rgpystat10, trcode11, rgpystat11, trcode12, rgpystat12};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, trcode01Out, rgpystat01Out, trcode02Out, rgpystat02Out, trcode03Out, rgpystat03Out, trcode04Out, rgpystat04Out, trcode05Out, rgpystat05Out, trcode06Out, rgpystat06Out, trcode07Out, rgpystat07Out, trcode08Out, rgpystat08Out, trcode09Out, rgpystat09Out, trcode10Out, rgpystat10Out, trcode11Out, rgpystat11Out, trcode12Out, rgpystat12Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, trcode01Err, rgpystat01Err, trcode02Err, rgpystat02Err, trcode03Err, rgpystat03Err, trcode04Err, rgpystat04Err, trcode05Err, rgpystat05Err, trcode06Err, rgpystat06Err, trcode07Err, rgpystat07Err, trcode08Err, rgpystat08Err, trcode09Err, rgpystat09Err, trcode10Err, rgpystat10Err, trcode11Err, rgpystat11Err, trcode12Err, rgpystat12Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6693screen.class;
		protectRecord = S6693protect.class;
	}

}
