/*
 * File: Unlmatp.java
 * Date: 30 August 2009 2:50:54
 * Author: Quipoz Limited
 * 
 * Class transformed from UNLMATP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.UtrsmatTableDAM;
import com.csc.life.terminationclaims.recordstructures.Matpcpy;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsclmTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Unit Linked Maturity Processing Method.
* --------------------------------------
*
* This program is an item entry on T6598, the claim subroutine
* method table. This method is used in order to update the
* Unit Transactions.
*
* The following is the linkage information passed to this subroutine:
*
*            - company
*            - contract header number
*            - suffix
*            - life number
*            - joint-life number
*            - coverage
*            - rider
*            - crtable
*            - effective date
*            - estimated value
*            - actual value
*            - currency
*            - element code
*            - type code
*            - status
*            - batch key
*
* PROCESSING.
* ----------
*
* For each Unit Summary record (UTRS) for  this  contract  perform  the
* following:-
*
*   If initial units
*
*     Write a UTRN record with the following data for the
*     pending Maturity sub-account:-
*
*       Processing Seq No. -  from T6647
*       Mature %          -  100
*       Amount (signed)    -  0
*       Effective date     -  Effective-date of the Maturity
*       Bid/Offer Ind      -  'B'
*       Now/Deferred Ind   -  T6647 entry (deallocation field)
*       Price              -  0
*       Fund               -  linkage (element code)
*       Number of Units    -  0
*       Contract No.       -  linkage
*       Contract Type      -  linkage
*       Sub-Account Code   -  T5645 entry for this sub-account
*       Sub-Account Type   -  ditto
*       G/L key            -  ditto
*       Trigger module     -  T5687/T6598 entry
*       Trigger key        -  Contract no./Suffix/
*                             Life/Coverage/Rider
*       Mat. Value Penalty -  0
*
* If accumulation units
*
*     Write  a  UTRN  record  with  the following data for the pending
*     Maturity sub-account:-
*
*       Processing Seq No. -  from T6647
*       Mature %          -  100
*       Amount (signed)    -  0
*       Effective date     -  Effective-date of the Maturity
*       Bid/Offer Ind      -  'B'
*       Now/Deferred Ind   -  T6647 entry (deallocation field)
*       Price              -  0
*       Fund               -  linkage (element code)
*       Number of Units    -  0
*       Contract No.       -  linkage
*       Contract Type      -  linkage
*       Sub-Account Code   -  T5645 entry for this sub-account
*       Sub-Account Type   -  ditto
*       G/L key            -  ditto
*       Trigger module     -  T5687/T6598 entry
*       Trigger key        -  Contract no./Suffix/
*                             Life/Coverage/Rider
*       Mat. Value Penalty -  1
*                                                                     *
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*       Include processing for interest bearing funds.                *
*                                                                     *
*****************************************************************
* </pre>
*/
public class Unlmatp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected String wsaaSubr = "UNLMATP";
	private String e308 = "E308";
	private String f294 = "F294";
	private String h115 = "H115";
		/* TABLES */
	private String t6647 = "T6647";
	protected String t5515 = "T5515";
	private String t5645 = "T5645";
	private String t5687 = "T5687";
	private String t6598 = "T6598";
	private String t5688 = "T5688";
	protected String itdmrec = "ITEMREC";
	protected String utrnrec = "UTRNREC";
	private String utrsmatrec = "UTRSMATREC";
	private String hitrrec = "HITRREC";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();

	protected FixedLengthStringData wsaaTrigger = new FixedLengthStringData(20);
	protected FixedLengthStringData wsaaTriggerChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTrigger, 0);
	protected FixedLengthStringData wsaaTriggerChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTrigger, 1);
	protected PackedDecimalData wsaaTriggerSuffix = new PackedDecimalData(5, 0).isAPartOf(wsaaTrigger, 9);
	protected PackedDecimalData wsaaTriggerTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaTrigger, 12);
	protected FixedLengthStringData wsaaTriggerCoverage = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 15);
	protected FixedLengthStringData wsaaTriggerRider = new FixedLengthStringData(2).isAPartOf(wsaaTrigger, 17);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaTrigger, 19, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTabBatckey = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);

	protected FixedLengthStringData wsaaTime = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaHhmmss = new ZonedDecimalData(6, 0).isAPartOf(wsaaTime, 0).setUnsigned();
	protected Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	protected DescTableDAM descIO = new DescTableDAM();
		/*Interest Bearing Transaction Details*/
	private HitrTableDAM hitrIO = new HitrTableDAM();
		/*Interest Bearing Transaction Summary*/
	private HitsTableDAM hitsIO = new HitsTableDAM();
		/*Table items, date - maintenance view*/
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected Matpcpy matpcpy = new Matpcpy();
	protected Syserrrec syserrrec = new Syserrrec();
	protected T5515rec t5515rec = new T5515rec();
	protected T5645rec t5645rec = new T5645rec();
	private T5687rec t5687rec = new T5687rec();
	protected T5688rec t5688rec = new T5688rec();
	protected T6598rec t6598rec = new T6598rec();
	protected T6647rec t6647rec = new T6647rec();
		/*UTRN LOGICAL VIEW.*/
	protected UtrnTableDAM utrnIO = new UtrnTableDAM();
		/*Unit Transactions for Claims*/
	protected UtrsclmTableDAM utrsclmIO = new UtrsclmTableDAM();
		/*United linked details - Maturity*/
	protected UtrsmatTableDAM utrsmatIO = new UtrsmatTableDAM();
	protected Varcom varcom = new Varcom();
	protected Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit220, 
		nextRecord253, 
		a190Exit, 
		seExit9090, 
		dbExit9190
	}

	public Unlmatp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		matpcpy.maturityRec = convertAndSetParam(matpcpy.maturityRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		initialize100();
		if (isEQ(matpcpy.type,"A")
		|| isEQ(matpcpy.type,"I")) {
			mainProcessing200();
		}
		if (isEQ(matpcpy.type,"D")) {
			a100InterestBearing();
		}
		/*EXIT*/
		exitProgram();
	}

protected void initialize100()
	{
		go101();
	}

protected void go101()
	{
		matpcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaBatckey.set(matpcpy.batckey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTime.set(getCobolTime());
		if (isEQ(matpcpy.status,varcom.oK)) {
			readTabT6647120();
		}
		if (isEQ(matpcpy.status,varcom.oK)) {
			readTabT5645140();
		}
		if (isEQ(matpcpy.status,varcom.oK)) {
			readTabT5688145();
		}
	}

protected void readTabT6647120()
	{
		read121();
	}

protected void read121()
	{
		itdmIO.setItemcoy(matpcpy.chdrcoy);
		itdmIO.setItemtabl(t6647);
		wsaaTabBatckey.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(matpcpy.cnttype);
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(matpcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(wsaaT6647Key,itdmIO.getItemitem())
		|| isNE(matpcpy.chdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t6647)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(wsaaT6647Key);
			itdmIO.setParams(itdmIO.getParams());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h115);
			systemError9000();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void readTabT5645140()
	{
		read141();
	}

protected void read141()
	{
		itemIO.setItemcoy(matpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError9100();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(matpcpy.chdrcoy);
		descIO.setLanguage(matpcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			dbError9100();
		}
	}

protected void readTabT5688145()
	{
		read146();
	}

protected void read146()
	{
		itdmIO.setItemcoy(matpcpy.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(matpcpy.cnttype);
		itdmIO.setItmfrm(matpcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),matpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),matpcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(matpcpy.cnttype);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			systemError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void mainProcessing200()
	{
		try {
			para201();
		}
		catch (GOTOException e){
		}
	}

protected void para201()
	{
		if (isEQ(matpcpy.estimatedVal,0)) {
			goTo(GotoLabel.exit220);
		}
		if (isEQ(matpcpy.planSuffix,ZERO)) {
			writeUtrnWholePlan280();
			goTo(GotoLabel.exit220);
		}
		utrsclmIO.setParams(SPACES);
		utrsclmIO.setPlanSuffix(matpcpy.planSuffix);
		utrsclmIO.setLife(matpcpy.life);
		utrsclmIO.setUnitVirtualFund(matpcpy.fund);
		utrsclmIO.setUnitType(matpcpy.type);
		utrsclmIO.setChdrcoy(matpcpy.chdrcoy);
		utrsclmIO.setChdrnum(matpcpy.chdrnum);
		utrsclmIO.setCoverage(matpcpy.coverage);
		utrsclmIO.setRider(matpcpy.rider);
		utrsclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrsclmIO);
		if (isNE(utrsclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrsclmIO.getParams());
			dbError9100();
		}
		getTrigger500();
		setupUtrns600();
	}

protected void writeUtrnWholePlan280()
	{
		para1201();
	}

protected void para1201()
	{
		utrsmatIO.setParams(SPACES);
		utrsmatIO.setChdrcoy(matpcpy.chdrcoy);
		utrsmatIO.setChdrnum(matpcpy.chdrnum);
		utrsmatIO.setCoverage(matpcpy.coverage);
		utrsmatIO.setRider(matpcpy.rider);
		utrsmatIO.setUnitVirtualFund(matpcpy.fund);
		utrsmatIO.setUnitType(matpcpy.type);
		utrsmatIO.setPlanSuffix(ZERO);
		utrsmatIO.setLife(matpcpy.life);
		utrsmatIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrsmatIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsmatIO.setFitKeysSearch("CHDRCOY","CHDRNUM","COVERAGE","RIDER","VRTFND","UNITYP");
		SmartFileCode.execute(appVars, utrsmatIO);
		if (isNE(utrsmatIO.getStatuz(),varcom.oK)
		&& isNE(utrsmatIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsmatIO.getParams());
			dbError9100();
		}
		if (isNE(matpcpy.chdrcoy,utrsmatIO.getChdrcoy())
		|| isNE(matpcpy.chdrnum,utrsmatIO.getChdrnum())
		|| isNE(matpcpy.coverage,utrsmatIO.getCoverage())
		|| isNE(matpcpy.rider,utrsmatIO.getRider())
		|| isNE(matpcpy.fund,utrsmatIO.getUnitVirtualFund())
		|| isNE(matpcpy.type,utrsmatIO.getUnitType())) {
			utrsmatIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(utrsmatIO.getStatuz(),varcom.endp))) {
			readUtrsWriteUtrn290();
		}
		
	}

protected void readUtrsWriteUtrn290()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					read251();
				}
				case nextRecord253: {
					nextRecord253();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read251()
	{
		if (isEQ(utrsmatIO.getCurrentDunitBal(),0)) {
			goTo(GotoLabel.nextRecord253);
		}
		getTrigger500();
		setupUtrns600();
	}

protected void nextRecord253()
	{
		utrsmatIO.setFormat(utrsmatrec);
		utrsmatIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsmatIO);
		if (isNE(utrsmatIO.getStatuz(),varcom.oK)
		&& isNE(utrsmatIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsmatIO.getParams());
			dbError9100();
		}
		if (isNE(matpcpy.chdrcoy,utrsmatIO.getChdrcoy())
		|| isNE(matpcpy.chdrnum,utrsmatIO.getChdrnum())
		|| isNE(matpcpy.coverage,utrsmatIO.getCoverage())
		|| isNE(matpcpy.rider,utrsmatIO.getRider())
		|| isNE(matpcpy.fund,utrsmatIO.getUnitVirtualFund())
		|| isNE(matpcpy.type,utrsmatIO.getUnitType())) {
			utrsmatIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void getTrigger500()
	{
		read531();
	}

protected void read531()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(matpcpy.chdrcoy);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(matpcpy.crtable);
		itdmIO.setItmfrm(matpcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),matpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),matpcpy.crtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(matpcpy.crtable);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			systemError9000();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(matpcpy.chdrcoy);
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.maturityCalcMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError9100();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}

protected void setupUtrns600()
	{
		go601();
	}

protected void go601()
	{
		utrnIO.setParams(SPACES);
		utrnIO.setTranno(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setPlanSuffix(ZERO);
		utrnIO.setBatcactyr(ZERO);
		utrnIO.setBatcactmn(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setContractAmount(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setProcSeqNo(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setSvp(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setCrComDate(ZERO);
		utrnIO.setChdrcoy(matpcpy.chdrcoy);
		wsaaTriggerChdrcoy.set(matpcpy.chdrcoy);
		utrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		utrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		utrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		utrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		utrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		utrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		wsaaTriggerTranno.set(matpcpy.tranno);
		utrnIO.setChdrnum(matpcpy.chdrnum);
		wsaaTriggerChdrnum.set(matpcpy.chdrnum);
		if (isEQ(matpcpy.planSuffix,ZERO)) {
			utrnIO.setPlanSuffix(utrsmatIO.getPlanSuffix());
			wsaaTriggerSuffix.set(utrsmatIO.getPlanSuffix());
		}
		else {
			utrnIO.setPlanSuffix(matpcpy.planSuffix);
			wsaaTriggerSuffix.set(matpcpy.planSuffix);
		}
		utrnIO.setLife(matpcpy.life);
		utrnIO.setCoverage(matpcpy.coverage);
		wsaaTriggerCoverage.set(matpcpy.coverage);
		utrnIO.setRider(matpcpy.rider);
		wsaaTriggerRider.set(matpcpy.rider);
		utrnIO.setCrtable(matpcpy.crtable);
		utrnIO.setMoniesDate(matpcpy.effdate);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setSurrenderPercent(100);
		utrnIO.setFundAmount(0);
		if (isEQ(matpcpy.element,SPACES)) {
			utrnIO.setUnitVirtualFund(matpcpy.fund);
		}
		else {
			utrnIO.setUnitVirtualFund(matpcpy.element);
		}
		utrnIO.setContractType(matpcpy.cnttype);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setNowDeferInd(t6647rec.dealin);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		utrnIO.setTriggerModule(t6598rec.addprocess);
		utrnIO.setTriggerKey(wsaaTrigger);
		utrnIO.setNofUnits(0);
		utrnIO.setTransactionDate(matpcpy.date_var);
		utrnIO.setTransactionTime(matpcpy.time);
		utrnIO.setTranno(matpcpy.tranno);
		utrnIO.setUser(matpcpy.user);
		if (isEQ(matpcpy.planSuffix,ZERO)) {
			utrnIO.setUnitType(utrsmatIO.getUnitType());
		}
		else {
			utrnIO.setUnitType(utrsclmIO.getUnitType());
		}
		utrnIO.setSvp(1);
		if (isEQ(utrnIO.getUnitType(),"A")) {
			utrnIO.setUnitSubAccount("ACUM");
		}
		else {
			utrnIO.setUnitSubAccount("INIT");
		}
		utrnIO.setTermid(SPACES);
		itdmIO.setItemcoy(utrnIO.getChdrcoy());
		itdmIO.setItemtabl(t5515);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
		itdmIO.setItmfrm(matpcpy.effdate);
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),"ENDP")) {
			itdmIO.setItemcoy(utrnIO.getChdrcoy());
			itdmIO.setItemtabl(t5515);
			itdmIO.setItempfx("IT");
			itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
			itdmIO.setItmfrm(matpcpy.effdate);
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),utrnIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),utrnIO.getUnitVirtualFund())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(utrnIO.getChdrcoy());
			itdmIO.setItemtabl(t5515);
			itdmIO.setItempfx("IT");
			itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
			itdmIO.setItmfrm(matpcpy.effdate);
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		if (isNE(utrnIO.getUnitType(),t5515rec.unitType)
		&& isNE(t5515rec.unitType,"B")) {
			syserrrec.params.set(itdmIO.getParams());
			dbError9100();
		}
		utrnIO.setFundCurrency(t5515rec.currcode);
		utrnIO.setCntcurr(matpcpy.cntcurr);
		utrnIO.setFormat(utrnrec);
		utrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(utrnIO.getParams());
			dbError9100();
		}
	}

protected void a100InterestBearing()
	{
		try {
			a110Para();
		}
		catch (GOTOException e){
		}
	}

protected void a110Para()
	{
		if (isEQ(matpcpy.estimatedVal,0)) {
			goTo(GotoLabel.a190Exit);
		}
		hitsIO.setParams(SPACES);
		hitsIO.setPlanSuffix(ZERO);
		hitsIO.setLife(matpcpy.life);
		hitsIO.setZintbfnd(matpcpy.fund);
		hitsIO.setChdrcoy(matpcpy.chdrcoy);
		hitsIO.setChdrnum(matpcpy.chdrnum);
		hitsIO.setLife(matpcpy.life);
		hitsIO.setCoverage(matpcpy.coverage);
		hitsIO.setRider(matpcpy.rider);
		hitsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","ZINTBFND");
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			dbError9100();
		}
		if (isNE(matpcpy.chdrnum,hitsIO.getChdrnum())
		|| isNE(matpcpy.chdrcoy,hitsIO.getChdrcoy())
		|| isNE(matpcpy.life,hitsIO.getLife())
		|| isNE(matpcpy.coverage,hitsIO.getCoverage())
		|| isNE(matpcpy.rider,hitsIO.getRider())
		|| isNE(matpcpy.fund,hitsIO.getZintbfnd())) {
			syserrrec.params.set(hitsIO.getParams());
			dbError9100();
		}
		while ( !(isEQ(hitsIO.getStatuz(),varcom.endp))) {
			a200ReadHitsWriteHitr();
		}
		
	}

protected void a200ReadHitsWriteHitr()
	{
		/*A210-READ*/
		if (isNE(hitsIO.getZcurprmbal(),0)) {
			getTrigger500();
			a300SetupHitrs();
		}
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			dbError9100();
		}
		if (isNE(matpcpy.chdrnum,hitsIO.getChdrnum())
		|| isNE(matpcpy.chdrcoy,hitsIO.getChdrcoy())
		|| isNE(matpcpy.life,hitsIO.getLife())
		|| isNE(matpcpy.coverage,hitsIO.getCoverage())
		|| isNE(matpcpy.rider,hitsIO.getRider())
		|| isNE(matpcpy.fund,hitsIO.getZintbfnd())) {
			hitsIO.setStatuz(varcom.endp);
		}
		/*A290-EXIT*/
	}

protected void a300SetupHitrs()
	{
		a310Go();
	}

protected void a310Go()
	{
		hitrIO.setParams(SPACES);
		hitrIO.setTranno(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setPlanSuffix(ZERO);
		hitrIO.setFundRate(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciPerd01(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setContractAmount(ZERO);
		hitrIO.setFundAmount(ZERO);
		hitrIO.setProcSeqNo(ZERO);
		hitrIO.setSvp(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setChdrcoy(matpcpy.chdrcoy);
		wsaaTriggerChdrcoy.set(matpcpy.chdrcoy);
		hitrIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hitrIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hitrIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hitrIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hitrIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hitrIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		wsaaTriggerTranno.set(matpcpy.tranno);
		hitrIO.setChdrnum(matpcpy.chdrnum);
		wsaaTriggerChdrnum.set(matpcpy.chdrnum);
		hitrIO.setPlanSuffix(matpcpy.planSuffix);
		wsaaTriggerSuffix.set(matpcpy.planSuffix);
		hitrIO.setLife(matpcpy.life);
		hitrIO.setCoverage(matpcpy.coverage);
		wsaaTriggerCoverage.set(matpcpy.coverage);
		hitrIO.setRider(matpcpy.rider);
		wsaaTriggerRider.set(matpcpy.rider);
		hitrIO.setCrtable(matpcpy.crtable);
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);
		if (isEQ(matpcpy.type,"P")) {
			hitrIO.setSurrenderPercent(matpcpy.percreqd);
		}
		else {
			hitrIO.setSurrenderPercent(100);
		}
		hitrIO.setFundAmount(0);
		if (isEQ(matpcpy.element,SPACES)) {
			hitrIO.setZintbfnd(matpcpy.fund);
		}
		else {
			hitrIO.setZintbfnd(matpcpy.element);
		}
		hitrIO.setCnttyp(matpcpy.cnttype);
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			hitrIO.setSacscode(t5645rec.sacscode02);
			hitrIO.setSacstyp(t5645rec.sacstype02);
			hitrIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			hitrIO.setSacscode(t5645rec.sacscode01);
			hitrIO.setSacstyp(t5645rec.sacstype01);
			hitrIO.setGenlcde(t5645rec.glmap01);
		}
		hitrIO.setTriggerModule(t6598rec.addprocess);
		hitrIO.setTriggerKey(wsaaTrigger);
		hitrIO.setTranno(matpcpy.tranno);
		hitrIO.setSvp(1);
		hitrIO.setZrectyp("P");
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hitrIO.getChdrcoy());
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(hitrIO.getZintbfnd());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			dbError9100();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		hitrIO.setFundCurrency(t5515rec.currcode);
		hitrIO.setCntcurr(matpcpy.cntcurr);
		hitrIO.setEffdate(matpcpy.effdate);
		hitrIO.setZintalloc("N");
		hitrIO.setZintrate(ZERO);
		hitrIO.setZinteffdt(varcom.vrcmMaxDate);
		hitrIO.setZlstintdte(varcom.vrcmMaxDate);
		hitrIO.setFormat(hitrrec);
		hitrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitrIO);
		if (isNE(hitrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hitrIO.getParams());
			dbError9100();
		}
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		matpcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		matpcpy.status.set(varcom.bomb);
		exitProgram();
	}
}
