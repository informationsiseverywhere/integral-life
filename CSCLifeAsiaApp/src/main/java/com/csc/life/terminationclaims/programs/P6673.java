/*
 * File: P6673.java
 * Date: 30 August 2009 0:50:34
 * Author: Quipoz Limited
 * 
 * Class transformed from P6673.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifeclnt;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.dataaccess.dao.LaptpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Laptpf;
import com.csc.life.terminationclaims.screens.S6673ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
*
*                 COMPONENT/CONTRACT LAPSE
*
*  The  user  may  have  selected to 'lapse ' either individual
*  policies within the plan or the whole plan and either a cov-
*  erage  or a rider.  If a coverage has been selected then any
*  attatched  riders should  also be processed.  After this the
*  actual  component  will be selected.  If individual policies
*  are being lapsed then this screen will be invoked once  for
*  each selected component.
*
*  If  the whole plan has been selected, (COVRMJA-PLAN-SUFFIX =
*  0000), then this screen will again be invoked once for  each
*  component  selected.  It will loop around between  the  2000
*  and 4000 sections  until  all  the  COVRMJA records  with  a
*  key  matching on Company, Contract Number, Life and Coverage
*  or Rider have been processed. The program will read all  the
*  COVRMJA records from the summary record, if  it  exists,  to
*  the last COVRMJA record  in the plan for the given key. The
*  plan suffix being processed will be displayed in the heading
*  portion of the screen in order to show the user which policy
*  is currently  being  processed.  When  the summary record is
*  being processed display the range of policies represented by
*  the summary eg. Policy Number: 0001 - 0006.
*
*
*  Initialise
*  ----------
*
*  Skip this section if returning from a program  further  down
*  the stack, (current stack position action flag = '*').
*
*  Perform  a RETRV on the COVRMJA I/O module. This will return
*  the COVRMJA record on the selected policy.
*
*  If COVRMJA-PLAN-SUFFIX = 0000 then the whole plan  is  being
*  processed  and every COVRMJA record whose key matches on the
*  passed  Company,  Contract  Number,  Life and Coverage/Rider
*  will be processed.
*
*  If  the COVRMJA-PLAN-SUFFIX is not greater than the value of
*  CHDRMJA-POLSUM then a 'notional' policy is being  processed,
*  ie.  a  policy  has been selected that is at present part of
*  the summarised portion of the contract. If this is the  case
*  then  the COVRMJA summary record will have been saved by the
*  KEEPS command and the values from here  will  be  displayed.
*  The  details  of the contract being processed will be stored
*  in the CHDRMJA I/O  module.    Retrieve  the  details  using
*  RETRV.
*  Distinguish between Coverage and Rider level  lapse  because
*  if a coverage has been selected  attatched  riders  with the
*  correct status also need to be processed.
*
*  Set  up  the header portion of the screen with the following
*  fields, descriptions and names:
*
*       Contract  Type  (CHDRMJA-CNTTYPE)  -  long  description
*       from T5688
*       Contract  Status (CHDRMJA-STATCODE) - short description
*       from T3623
*       Premium      Status   (CHDRMJA-PSTATCODE)    -    short
*       description from T3588
*       Paid to Date (CHDRMJA-PTDATE)
*       Billed to Date (CHDRMJA-BTDATE)
*       Billing Frequency (CHDRMJA-BILLFREQ)
*       The owner's client (CLTS) details.
*       The first Life Assured details from the LIFE dataset.
*       The  Joint  Life  Assured details from the LIFE dataset
*       if they exist.
*       All  the  client  numbers  should  have   their   names
*       displayed  using  CONFNAME.    Obtain  the confirmation
*       names.
*
*  These screen fields are for display only.
*
*
*  Screen Display and Validation
*  -----------------------------
*
*  Skip this section if returning from a program  further  down
*  the stack, (current stack position action flag = '*').
*
*
*  Read  the LAPT file, checking for an existing record for the
*  component selected.  If so, proceed to the  'Field  Display'
*  section, displaying the following message:
*   'Lapse Already Requested'.
*
*  Also allow one online manual lapse per day.
*     i.e check that CURRFROM-DATE on CHDRMJA is not
*     equal to todays date.
*
*  Check that the coverage/rider  status  matches  one  of  the
*  pairs on the Component Status Table:-
*       Read  T5679 using the program's transaction code as the
*       item key.
*       If COVRPUP-RIDER = '00',  check  against  the  coverage
*       statii, else check against the rider statii.
*       Use    the    fields    T5679-RID(COV)-PREM-STAT    and
*       T5679-RID(COV)-RISK-STAT against COVRPUP-PSTATCODE  and
*       COVRPUP-STATCODE.
*       If  the  statii  do  not  match one of the pairs on the
*       table entry, issue an error and disallow Lapse.
*
*  Check   that   the  coverage/rider  exists  on  the  General
*  Coverage/Rider Details Table:-
*       Read T5687  for  the  coverage/rider  (COVRPUP-CRTABLE)
*       selected.      If  no  table  item  exists,  abort  the
*       program.
*       If  no  non-forfeiture  method exists for the component
*       display an error and disallow the lapse.
*
*  Field Display
*  -------------
*  Display the following component/rider level fields:
*       The Rider being processed (COVRPUP-RIDER)
*       Existing  Sum  Assured  (COVRPUP-SUMINS)  - if the plan
*       suffix is  zeros,  the  sum-assured  will  need  to  be
*       divided  by the number of policies currently summarised
*       (CHDRMJA-POLSUM)
*       Risk Commencement Date (COVRPUP-CRRCD)
*       Component  Risk  Status   (COVRPUP-STATCODE)   -   long
*       description from T5682
*       Component  Premium  Status  (COVRPUP-PSTATCODE)  - long
*       description from T5681
*  Display the screen, with all fields protected.
*
*
*  If 'CF11' is requested, exit this section.
*
*
*  Updating
*  --------
*  Skip  this  section if returning from a program further down
*  the stack (current stack position action  flag  =  '*'),  or
*  'CF11' was requested.
*
*  If there are no errors write one LAPT for the component to be
*  lapsed using the UPDAT function.
*
*  Set the following fields on the LAPT:
*
*  CHDRCOY   -|
*  CHDRNUM    |
*  LIFE       |
*  COVERAGE   |---key from COVRMJA record
*  RIDER      |
*  PLNSFX    -|
*  Termid
*  Transaction Date
*  Transaction Time
*  User
*
*
*  Next Program
*  ------------
*  If 'CF11' was requested move spaces  to  the  current  stack
*  action  and  program  fields and do not add 1 to the program
*  pointer.  Then exit.
*
*  If the current stack action field is '*'  then  re-load  the
*  next  8  positions in the program stack from the saved area,
*  and move spaces to WSSP-SEC-ACTION(current position).
*
*  If processing the whole Plan, (COVRMJA-PLAN-SUFFIX =  0000),
*  then  the  program will read the next COVRMJA record for the
*  matching Company, Contract Number,  Life and  Coverage/Rider
*  and  loop back around to the 2000 section and re-display the
*  details from the next COVRMJA record as they were first  set
*  up  by  the  first  pass through the 1000 section. This will
*  continue until all the matching COVRMJA  records  have  been
*  processed.
*
*  Add 1 to the current program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P6673 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6673");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaValidStatuz = "N";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0).init(ZERO);

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator wsaaWholePlan = new Validator(wsaaPlanproc, "Y");
	private Validator wsaaPartPlan = new Validator(wsaaPlanproc, "N");

	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaAlreadyLapse = new FixedLengthStringData(1).init("N");
	private Validator alreadyLapse = new Validator(wsaaAlreadyLapse, "Y");
	private String wsaaErrorFlag = "";
	private String wsaaWrongStatuz = "";
	private String e070 = "E070";
	private String f050 = "F050";
	private String i028 = "I028";
	private String i061 = "I061";
	private String e664 = "E664";
	private String f146 = "F146";
		/* TABLES */
	private String t3623 = "T3623";
//	private String t5679 = "T5679";
	private String t5687 = "T5687";
		/* FORMATS */
	private String covrmjarec = "COVRMJAREC";
	private String chdrmjarec = "CHDRMJAREC";
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();

	private Datcon1rec datcon1rec = new Datcon1rec();
		
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();

	private Batckey wsaaBatckey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6673ScreenVars sv = ScreenProgram.getScreenVars( S6673ScreenVars.class);
	
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	//ILIFE-5023 liwei
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private CovrpfDAO covrpupDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	//ILB-461
	/*Lapse Temporary File*/
	private LaptpfDAO laptpfDAO = getApplicationContext().getBean("laptpfDAO", LaptpfDAO.class);
	private Map<String, List<Itempf>> t5679ListMap = new HashMap<String, List<Itempf>>();
	private Covrpf covrpup = new Covrpf();
	private List<Covrpf> covrpupList = new ArrayList<Covrpf>();
	private Laptpf laptpf = null;
	private List<Itempf> list = new ArrayList<Itempf>();
	//ILB-456 start 
	
	private Covrpf covrpf = new Covrpf();
	private Iterator<Covrpf> iteratorList;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	
	//ILB-500
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);

	

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1900, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		continue2110, 
		setFields2300, 
		display2350, 
		exit2690, 
		exit3900, 
		exit4090, 
		exit4390, 
		exit5090
	}

	public P6673() {
		super();
		screenVars = sv;
		new ScreenModel("S6673", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1100();
		setScreen1200();
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		wsaaBatckey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatckey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatckey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		if (isEQ(wsaaToday,0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				syserrrec.statuz.set(datcon1rec.statuz);
				fatalError600();
			}
			else {
				wsaaToday.set(datcon1rec.intDate);
			}
		}
		sv.btdate.set(ZERO);
		sv.ptdate.set(ZERO);
		sv.sumin.set(ZERO);
		sv.crrcd.set(ZERO);
		sv.numpols.set(ZERO);
		sv.planSuffix.set(ZERO);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		//ILB-456
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			else {

				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				} 
	
				else {
					covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		/*covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		//ILB-456
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
*/
		
		int plansuffix = 0;
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			sv.plnsfxOut[varcom.hi.toInt()].set("Y");
			wsaaPlanproc.set("Y");
			plansuffix = covrpf.getPlanSuffix();
		}
		else {
			wsaaPlanproc.set("N");
			if (isLTE(covrpf.getPlanSuffix(),chdrpf.getPolsum())) {
				plansuffix = 0;
			}
			else {
				plansuffix = covrpf.getPlanSuffix();
			}
		}
		if (isEQ(covrpf.getRider(),"00")) {
			covrpupList = covrpupDAO.getCovrpupRecord(covrpf.getChdrcoy(),covrpf.getChdrnum(), covrpf.getLife(), //IJTI-1410
					covrpf.getCoverage());//IJTI-1410
			if(covrpupList == null){
				fatalError600();
			}
			iteratorList = covrpupList.iterator();
			covrpup = iteratorList.next();
		}else {
			covrpup = covrpupDAO.getCovrRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), //IJTI-1410
					covrpf.getCoverage(),covrpf.getRider(), plansuffix, "1");//IJTI-1410
			if(covrpup == null){
				fatalError600();
			}
		}
		t5679ListMap = itemDAO.loadSmartTable("IT",wsspcomn.company.toString(), "T5679");
	}

protected void setScreen1200()
 {
        sv.coverage.set(covrpf.getCoverage());
        sv.rider.set(covrpf.getRider());
        sv.crtable.set(covrpf.getCrtable());
        sv.ptdate.set(chdrpf.getPtdate());
        sv.btdate.set(chdrpf.getBtdate());
        sv.billfreq.set(chdrpf.getBillfreq());
        sv.register.set(chdrpf.getReg());
        sv.cntcurr.set(chdrpf.getCntcurr());
        sv.numpols.set(chdrpf.getPolinc());
        sv.chdrnum.set(chdrpf.getChdrnum());
        sv.cnttype.set(chdrpf.getCnttype());
        sv.planSuffix.set(covrpf.getPlanSuffix());

        String coy = wsspcomn.company.toString();
        Map<String, String> keyMap = new HashMap<String, String>();
        keyMap.put("T5688", chdrpf.getCnttype());//IJTI-1410
        keyMap.put("T3623", chdrpf.getStatcode());//IJTI-1410
        keyMap.put("T3588", chdrpf.getPstcde());//IJTI-1410
        keyMap.put("T3590", chdrpf.getBillfreq());//IJTI-1410
        keyMap.put("T5687", covrpf.getCrtable());//IJTI-1410

        Map<String, Descpf> descMap = descDAO.searchMultiDescpf(coy, wsspcomn.language.toString(), keyMap);

        if (!descMap.containsKey("T5688")) {
            sv.ctypedes.fill("?");
        } else {
            sv.ctypedes.set(descMap.get("T5688").getLongdesc());
        }

        if (!descMap.containsKey("T3623")) {
            sv.chdrstatus.fill("?");
        } else {
            sv.chdrstatus.set(descMap.get(t3623).getShortdesc());
        }

        if (!descMap.containsKey("T3588")) {
            sv.premstatus.fill("?");
        } else {
            sv.premstatus.set(descMap.get("T3588").getShortdesc());
        }
        if (!descMap.containsKey("T3590")) {
            sv.bilfrqdesc.fill("?");
        } else {
            sv.bilfrqdesc.set(descMap.get("T3590").getLongdesc());
        }
        if (!descMap.containsKey("T5687")) {
            sv.crtabdesc.fill("?");
        } else {
            sv.crtabdesc.set(descMap.get("T5687").getLongdesc());
        }

        sv.life.set(covrpf.getLife());
        List<Lifeclnt> lifeclntList = lifepfDAO.searchLifeClntRecord(coy, covrpf.getChdrnum(), "00",//IJTI-1410
                wsspcomn.fsuco.toString(), covrpf.getLife());//IJTI-1410
        Lifeclnt p = null;//IJTI-320
        if (lifeclntList == null || lifeclntList.size() <= 0) {
            //syserrrec.params.set(covrpf.getParams());
            fatalError600();
        } else {//IJTI-320 START
	        p = lifeclntList.get(0);
	        sv.lifenum.set(p.getLifcnum());
	        plainname(p);
	        sv.lifename.set(wsspcomn.longconfname);
        }//IJTI-320 END
        lifeclntList = lifepfDAO.searchLifeClntRecord(coy, covrpf.getChdrnum(), "01",//IJTI-1410
                wsspcomn.fsuco.toString(), covrpf.getLife());//IJTI-1410

        if (lifeclntList == null || lifeclntList.size() <= 0) {
            sv.jlife.set(SPACES);
            sv.jlifename.set(SPACES);
            return;
        }
        p = lifeclntList.get(0);
        sv.jlife.set(p.getLifcnum());
        plainname(p);
        sv.jlifename.set(wsspcomn.longconfname);
    }

protected void plainname(Lifeclnt p)
	{

    wsspcomn.longconfname.set(SPACES);
    if (isEQ(p.getClttype(),"C")) {
        corpname(p);
        return;
    }
    if (isNE(p.getCllgivname(),SPACES)) {
        StringBuilder stringVariable1 = new StringBuilder();
        stringVariable1.append(delimitedExp(p.getClsurname(), "  "));
        stringVariable1.append(", ");
        stringVariable1.append(delimitedExp(p.getClgivname(), "  "));
        wsspcomn.longconfname.setLeft(stringVariable1.toString());
    }
    else {
        wsspcomn.longconfname.set(p.getClsurname());
    }

	}

protected void corpname(Lifeclnt p)
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(p.getCllsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(p.getCllgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
 {
        GotoLabel nextMethod = GotoLabel.DEFAULT;
        while (true) {
            try {
                switch (nextMethod) {
                case DEFAULT: {
                    para2610();
                }
                case continue2110: {
                    continue2110();
                    checkCovr2150();
                }
                case setFields2300: {
                    setFields2300();
                }
                case display2350: {
                    display2350();
                }
                case exit2690: {
                }
                }
                break;
            } catch (GOTOException e) {
                nextMethod = (GotoLabel) e.getNextMethod();
            }
        }
    }

protected void screenEdit2000()
 {
        if (isEQ(scrnparams.statuz, "KILL")) {
            wsspcomn.edterror.set(varcom.oK);
            return;
        }
        if (isEQ(scrnparams.statuz, "CALC")) {
            wsspcomn.edterror.set("Y");
        }
    }


protected void validateCovr2450()
	{
		/*COVR*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub1.toInt()],covrpup.getStatcode())) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub1.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaSub1.toInt()],covrpup.getPstatcode())) {
					wsaaValidStatuz = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void validateRidr2500()
	{
		/*RIDR*/
		if (isEQ(t5679rec.ridRiskStat[wsaaSub1.toInt()],covrpup.getStatcode())) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub1.add(1)){
				if (isEQ(t5679rec.ridPremStat[wsaaSub1.toInt()],covrpup.getPstatcode())) {
					wsaaValidStatuz = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void para2610()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaErrorFlag = "N";
		wsaaWrongStatuz = "N";
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.display2350);
		}
		/*VALIDATE*/
		if (isEQ(chdrpf.getCurrfrom(),ZERO)) {
			goTo(GotoLabel.continue2110);
		}
	}

protected void continue2110()
	{
	
		laptpf = laptpfDAO.getLaptByKey(wsspcomn.company.toString(), covrpup.getChdrnum(), covrpup.getLife(), covrpup.getCoverage(), covrpup.getRider(), covrpf.getPlanSuffix());
		if (laptpf != null) {
			sv.chdrstatusErr.set(i061);
			wsspcomn.edterror.set("Y");
			wsaaErrorFlag = "Y";
			goTo(GotoLabel.setFields2300);
		}
		//ILIFE-5023 by liwei
		boolean itemFound = false;
		String keyItemitem = wsaaBatckey.batcBatctrcde.toString();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		if (t5679ListMap.containsKey(keyItemitem)){
			itempfList = t5679ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();			
					t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;	
			}
		}
		if (!itemFound) {
			sv.coverageErr.set(f050);
			wsspcomn.edterror.set("Y");
			wsaaErrorFlag = "Y";
			goTo(GotoLabel.setFields2300);		
		}
	}

protected void checkCovr2150()
	{
		wsaaValidStatuz = "N";
		wsaaAlreadyLapse.set("N");
		if (isEQ(covrpup.getRider(),"00")) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub1.add(1)){
				validateCovr2450();
			}
		}
		else {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub1.add(1)){
				validateRidr2500();
			}
		}
		if (isEQ(covrpf.getRider(),"00")
		&& isNE(covrpup.getRider(),"00")
		&& isEQ(wsaaValidStatuz,"N")) {
			wsaaWrongStatuz = "Y";
			goTo(GotoLabel.exit2690);
		}
		if (isEQ(wsaaValidStatuz,"N")) {
			sv.coverageErr.set(i028);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.setFields2300);
		}
		if ((isNE(t5679rec.setCnPremStat,SPACES))
		&& (isEQ(t5679rec.setCnPremStat,covrpup.getPstatcode()))) {
			if ((isNE(t5679rec.setCnRiskStat,SPACES))
			&& (isEQ(t5679rec.setCnRiskStat,covrpup.getStatcode()))) {
				wsaaAlreadyLapse.set("Y");
				goTo(GotoLabel.exit2690);
			}
		}
		list = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), t5687, covrpup.getCrtable(), wsaaToday.toInt());
		if(list == null || list.size()==0) {
			sv.riderErr.set(f050);
			wsspcomn.edterror.set("Y");
			wsaaErrorFlag = "Y";
			goTo(GotoLabel.setFields2300);
		}
		else {
			t5687rec.t5687Rec.set(StringUtil.rawToString(list.get(0).getGenarea()));
		}
		if (isEQ(t5687rec.nonForfeitMethod,SPACES)) {
			if (isEQ(covrpup.getRider(),"00")) {
				sv.riderErr.set(e664);
			}
			else {
				sv.riderErr.set(f146);
			}
			wsspcomn.edterror.set("Y");
			wsaaErrorFlag = "Y";
			goTo(GotoLabel.setFields2300);
		}
		if (isEQ(wsspcomn.edterror,"Y")) {
			goTo(GotoLabel.setFields2300);
		}
		if (wsaaWholePlan.isTrue()) {
			sv.sumin.set(covrpup.getSumins());
		}
		else {
			if (isGT(covrpup.getPlanSuffix(),chdrpf.getPolsum())) {
				sv.sumin.set(covrpup.getSumins());
			}
			else {
				compute(sv.sumin, 2).set(div(covrpup.getSumins(),chdrpf.getPolsum()));
			}
		}
		if (isEQ(wsspcomn.edterror,"Y")) {
			goTo(GotoLabel.setFields2300);
		}
	}

protected void setFields2300()
 {
        sv.ridnum.set(covrpup.getRider());
        sv.crrcd.set(covrpup.getCrrcd());
        sv.statcode.set(covrpup.getStatcode());

        Map<String, String> keyMap = new HashMap<String, String>();
        keyMap.put("T5682", covrpup.getStatcode());
        keyMap.put("T5681", covrpup.getPstatcode());

        Map<String, Descpf> descMap = descDAO.searchMultiDescpf(wsspcomn.company.toString(),
                wsspcomn.language.toString(), keyMap);

        if (!descMap.containsKey("T5682")) {
            sv.statdesc.fill("?");
        } else {
            sv.statdesc.set(descMap.get("T5682").getLongdesc());
        }

        if (!descMap.containsKey("T5681")) {
            sv.pstatdesc.fill("?");
        } else {
            sv.pstatdesc.set(descMap.get("T5681").getLongdesc());
        }

        sv.pstatcode.set(covrpup.getPstatcode());
    }

protected void display2350()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		sv.outputIndicators.set(SPACES);
		sv.bilfrqdescOut[varcom.pr.toInt()].set("Y");
		sv.billfreqOut[varcom.pr.toInt()].set("Y");
		sv.btdateOut[varcom.pr.toInt()].set("Y");
		sv.chdrnumOut[varcom.pr.toInt()].set("Y");
		sv.chdrstatusOut[varcom.pr.toInt()].set("Y");
		sv.cntcurrOut[varcom.pr.toInt()].set("Y");
		sv.cnttypeOut[varcom.pr.toInt()].set("Y");
		sv.coverageOut[varcom.pr.toInt()].set("Y");
		sv.crrcdOut[varcom.pr.toInt()].set("Y");
		sv.crtabdescOut[varcom.pr.toInt()].set("Y");
		sv.crtableOut[varcom.pr.toInt()].set("Y");
		sv.ctypedesOut[varcom.pr.toInt()].set("Y");
		sv.jlifenameOut[varcom.pr.toInt()].set("Y");
		sv.jlifenumOut[varcom.pr.toInt()].set("Y");
		sv.lifeOut[varcom.pr.toInt()].set("Y");
		sv.lifenameOut[varcom.pr.toInt()].set("Y");
		sv.lifenumOut[varcom.pr.toInt()].set("Y");
		sv.numpolsOut[varcom.pr.toInt()].set("Y");
		sv.plnsfxOut[varcom.pr.toInt()].set("Y");
		sv.premstatusOut[varcom.pr.toInt()].set("Y");
		sv.pstatcodeOut[varcom.pr.toInt()].set("Y");
		sv.pstatdescOut[varcom.pr.toInt()].set("Y");
		sv.ptdateOut[varcom.pr.toInt()].set("Y");
		sv.registerOut[varcom.pr.toInt()].set("Y");
		sv.riderOut[varcom.pr.toInt()].set("Y");
		sv.statcodeOut[varcom.pr.toInt()].set("Y");
		sv.statdescOut[varcom.pr.toInt()].set("Y");
		sv.suminOut[varcom.pr.toInt()].set("Y");
		sv.ridnumOut[varcom.pr.toInt()].set("Y");
		if (isEQ(covrpup.getRider(),"00")
		|| isNE(covrpf.getRider(),"00")) {
			sv.ridnumOut[varcom.nd.toInt()].set("Y");
		}
	}

protected void update3000()
 {
        if (isEQ(wsaaAlreadyLapse, "Y")) {
            return;
        }
        if (isEQ(scrnparams.statuz, varcom.kill)) {
            return;
        }
        if (isEQ(wsaaErrorFlag, "Y") || isEQ(wsaaWrongStatuz, "Y")) {
            return;
        }
        if (isEQ(covrpf.getRider(), "00") && isNE(covrpup.getRider(), "00")) {
            return;
        }
        laptpf = new Laptpf();
        laptpf.setChdrcoy(covrpup.getChdrcoy());
        laptpf.setChdrnum(covrpup.getChdrnum());
        laptpf.setLife(covrpup.getLife());
        laptpf.setCoverage(covrpup.getCoverage());
        laptpf.setRider(covrpup.getRider());
        laptpf.setPlnsfx(covrpf.getPlanSuffix());
        laptpf.setNewsuma(BigDecimal.ZERO);
        laptpf.setTermid(varcom.vrcmTermid.toString());
        laptpf.setTrdt(varcom.vrcmDate.toInt());
        laptpf.setTrtm(varcom.vrcmTime.toInt());
        laptpf.setUser_t(varcom.vrcmUser.toInt());
        if(!laptpfDAO.updtOrInstLaptByKey(laptpf)) {
        	 syserrrec.params.set(covrpup.getChdrcoy().concat(covrpup.getChdrnum()).concat(covrpup.getLife()).concat(covrpup.getCoverage()).
        			 concat(covrpup.getRider()).concat(Integer.toString(covrpf.getPlanSuffix())));
        	 fatalError600();
        }
      
    }

protected void whereNext4000()
 {
        if (isEQ(scrnparams.statuz, "KILL")) {
            wayout4300();
            return;
        }
        wsspcomn.nextprog.set(wsaaProg);
        if (isEQ(wsaaErrorFlag, "Y")) {
            wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
            wayout4300();
            return;
        }
        if (isEQ(covrpf.getRider(), "00")) {
            boolean flag = begnRead5500();
            if (!flag) {
                wsspcomn.nextprog.set(wsaaProg);
                wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
                wayout4300();
            } else {
                wsspcomn.nextprog.set(scrnparams.scrname);
                wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
            }
        } else {
            wsspcomn.nextprog.set(wsaaProg);
            wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
            wayout4300();
        }

    }

	protected boolean begnRead5500() {
		boolean flag = true;
		if(!iteratorList.hasNext()){
			return false;
		}
		covrpup = iteratorList.next();
		if (wsaaPartPlan.isTrue()) {
			if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())&& isNE(covrpup.getPlanSuffix(), ZERO)) {
				flag = false;
			} else {
				if (isGT(covrpf.getPlanSuffix(), chdrpf.getPolsum())&& isNE(covrpup.getPlanSuffix(),covrpf.getPlanSuffix())) {
					flag = false;
				}
			}
		}
		return flag;
	}
protected void wayout4300()
 {
        if (isEQ(scrnparams.statuz, "KILL")) {
            wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
            wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
            return;
        }
        wsspcomn.programPtr.add(1);
    }


}
