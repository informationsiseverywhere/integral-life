package com.csc.life.terminationclaims.dataaccess.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Cpsupf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "CPSUPF";

	//member variables for columns
	private long uniqueNumber;
	private String chdrpfx;
	private String chdrcoy; 
	private String chdrnum;
	private Integer tranno;
	private String batctrcde;
	private String rider;
	private String crtable;
	private String cnttype ;
	private String comprskcode;
	private String comppremcode;	
	private double compsumss;
	private double compbilprm;
	private double refprm;
	private double refadjust;
	private double refamt;
	private String status;
	private String payrnum;
	private String reqntype;
	private String bankacckey;
	private String bankkey;
	private String bankdesc;
	private String compstatus;
	private String validflag;
	private String life;
	private String coverage;
	private int plnsfx;
	private String usrprf;
	private String jobnm;
	private String datime;	
	// Constructor
	public Cpsupf ( ) {}

	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getComprskcode() {
		return comprskcode;
	}
	public void setComprskcode(String comprskcode) {
		this.comprskcode = comprskcode;
	}
	public String getComppremcode() {
		return comppremcode;
	}
	public void setComppremcode(String comppremcode) {
		this.comppremcode = comppremcode;
	}
	public double getCompsumss() {
		return compsumss;
	}
	public void setCompsumss(double compsumss) {
		this.compsumss = compsumss;
	}
	public double getCompbilprm() {
		return compbilprm;
	}
	public void setCompbilprm(double compbilprm) {
		this.compbilprm = compbilprm;
	}
	public double getRefprm() {
		return refprm;
	}
	public void setRefprm(double refprm) {
		this.refprm = refprm;
	}
	public double getRefadjust() {
		return refadjust;
	}
	public void setRefadjust(double refadjust) {
		this.refadjust = refadjust;
	}
	public double getRefamt() {
		return refamt;
	}
	public void setRefamt(double refamt) {
		this.refamt = refamt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(String payrnum) {
		this.payrnum = payrnum;
	}
	public String getReqntype() {
		return reqntype;
	}
	public void setReqntype(String reqntype) {
		this.reqntype = reqntype;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankdesc() {
		return bankdesc;
	}
	public void setBankdesc(String bankdesc) {
		this.bankdesc = bankdesc;
	}
	public String getCompstatus() {
		return compstatus;
	}
	public void setCompstatus(String compstatus) {
		this.compstatus = compstatus;
	}
	public String getValidflag() {
		return validflag;
	}


	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}


	public String getLife() {
		return life;
	}


	public void setLife(String life) {
		this.life = life;
	}


	public String getCoverage() {
		return coverage;
	}


	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}


	public int getPlnsfx() {
		return plnsfx;
	}


	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}


	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public static String getTableName() {
		return TABLE_NAME;
	}


	/*@Override
	public String toString() {
		return "Abbbpf [uniqueNumber=" + uniqueNumber + ", chdrpfx=" + chdrpfx + ", chdrcoy=" + chdrcoy + ", chdrnum=" + chdrnum + ", tranno=" + tranno + ", batctrcde=" + batctrcde + ",rider=" + rider + ",crtable=" + crtable + ",sortds=" + sortds + ",comprskcode=" + comprskcode + ",comppremcode=" + comppremcode + ",compsumss=" + compsumss + ",compbilprm=" + compbilprm + ",refprm=" + refprm + ",refadjust=" + refadjust + ",refamt=" + refamt + ",status=" + status + ",reqntype=" + reqntype + ",bankacckey=" + bankacckey + ",bankkey=" + bankkey + ",bankdesc=" + bankdesc + ", usrprf=" + usrprf + ", jobnm=" + jobnm + ", datime=" + datime + "]";
	}*/
}