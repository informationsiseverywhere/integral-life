package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:30
 * Description:
 * Copybook name: HDCDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hdcdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hdcdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hdcdKey = new FixedLengthStringData(64).isAPartOf(hdcdFileKey, 0, REDEFINE);
  	public FixedLengthStringData hdcdChdrcoy = new FixedLengthStringData(1).isAPartOf(hdcdKey, 0);
  	public FixedLengthStringData hdcdChdrnum = new FixedLengthStringData(8).isAPartOf(hdcdKey, 1);
  	public FixedLengthStringData hdcdLife = new FixedLengthStringData(2).isAPartOf(hdcdKey, 9);
  	public FixedLengthStringData hdcdCoverage = new FixedLengthStringData(2).isAPartOf(hdcdKey, 11);
  	public FixedLengthStringData hdcdRider = new FixedLengthStringData(2).isAPartOf(hdcdKey, 13);
  	public FixedLengthStringData hdcdCrtable = new FixedLengthStringData(4).isAPartOf(hdcdKey, 15);
  	public PackedDecimalData hdcdTranno = new PackedDecimalData(5, 0).isAPartOf(hdcdKey, 19);
  	public FixedLengthStringData hdcdFieldType = new FixedLengthStringData(1).isAPartOf(hdcdKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(41).isAPartOf(hdcdKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hdcdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hdcdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}