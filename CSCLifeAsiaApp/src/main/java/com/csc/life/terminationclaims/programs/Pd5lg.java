/*
 * File: Pd5lg.java
 * Date: 30 August 2009 0:42:20
 * Author: Quipoz Limited
 * 
 * Class transformed from Pd5lg.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE; 
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.screens.Sd5lgScreenVars;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData; 
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*     Traditional Component Surrender Sub Menu.
*
* Validation
* ----------
*
*  Key 1 - Contract number (CHDRPTS)
*
*       Y = mandatory, must exist on file.
*            - CHDRPTS  -  Life  new  business  contract  header
*                 logical view.
*            - must be correct status for transaction (T5679).
*            - MUST BE CORRECT BRANCH.
*
*       N = IRREVELANT.
*
*       Blank = irrelevant.
*
*
* Updating
* --------*

*  For maintenance transactions  (WSSP-FLAG  set to  'M' above),
*  soft lock the contract header (call SFTLOCK). If the contract
*  is already locked, display an error message.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*  Include option for traditional Component surrender.  This option
*  is not allowed if the paid-to-date is not = the billed-to
*  date.
*
*****************************************************************
* </pre>
*/
public class Pd5lg extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5LG");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	/* ERRORS */
	private static final String e070 = "E070";
	private static final String f910 = "F910";
	private static final String e073 = "E073";
	private static final String e544 = "E544";
	private static final String e186 = "E186";
	private static final String e455 = "E455";
	private static final String e767 = "E767";
	private static final String f918 = "F918";
	private static final String g008 = "G008";	
	private static final String f617 = "F617";	
	private static final String e032 = "E032";
	private static final String e944 = "E944";
		private static final String chdrsurrec = "CHDRSURREC";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String covrmjarec = "COVRMJAREC";
		/* Dummy user wssp. Replace with required version.
		    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();	
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private T5679rec t5679rec = new T5679rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec(); //ICIL-297
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Sd5lgScreenVars sv = ScreenProgram.getScreenVars( Sd5lgScreenVars.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> t5679Map;
	private ZonedDecimalData wsaaPrevPtDate = new ZonedDecimalData(8, 0).setUnsigned();
	public Pd5lg() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5lg", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/	
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		sv.effdateDisp.clear();
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		if (isEQ(wsaaToday,0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}		
		
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();		
		validateEffdate();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'Sd5lgIO' USING SCRN-SCREEN-PARAMS                      */
		/*                          Sd5lg-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
				if(isNE(sv.chdrsel,SPACES)){					
						chkChdrDates2700();
				}				
			}
			else {
				verifyBatchControl2900();
			}
		}
	}

protected void validateEffdate() {	
	
	if(isEQ(scrnparams.action,"A") && isEQ(sv.chdrselErr,SPACES)){
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.intDate1.set(chdrlnbIO.getPtdate());
		datcon2rec.frequency.set(chdrlnbIO.getBillfreq());
		datcon2rec.freqFactor.set(-1); 
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaPrevPtDate.set(datcon2rec.intDatex2);
		if(isEQ(sv.effdate, varcom.vrcmMaxDate)) 
			sv.effdate.set(wsaaToday);			
		
		if(isGT(sv.effdate, chdrlnbIO.getPtdate()) || isLTE(sv.effdate, wsaaPrevPtDate)) {
			sv.effdateErr.set(e032); 
			wsspcomn.edterror.set("Y");
			return;
		}		
		wsspcomn.currfrom.set(sv.effdate);
	}else if(isEQ(scrnparams.action,"B") || isEQ(scrnparams.action,"C")){
		if(isNE(sv.effdate,varcom.vrcmMaxDate)){
			 sv.effdateErr.set(f617);
			 return;
		}				
	}
	wsspcomn.currfrom.set(wsaaToday);
}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		/* MOVE WSSP-PASSWORD          TO SNCT-PASSWORD.                */
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		validateKey12210();			
	}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1,SPACES)) {
			validateKey22220();
		}
		if(isEQ(sv.chdrsel,SPACES)){
			sv.chdrselErr.set(e186);
			wsspcomn.edterror.set("Y");	return;	
		}
		chdrlnbIO.setDataArea(SPACES);
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		/*                                 CHDRSUR-CHDRCOY.              */
		chdrlnbIO.setChdrnum(sv.chdrsel);
		chdrlnbIO.setFunction(varcom.readr);		
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getStatuz(),varcom.mrnf)
		&& isEQ(subprogrec.key1,"Y")) {
			sv.chdrselErr.set(e544);
		}
		if (isEQ(chdrlnbIO.getStatuz(),varcom.oK)
		&& isEQ(subprogrec.key1,"N")
		&& isEQ(sv.chdrselErr,SPACES)) {
			sv.chdrselErr.set(f918);
		}
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
			}
		}
		
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)) {
			checkStatus2400();
		}
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)
		&& isNE(wsspcomn.branch,chdrlnbIO.getCntbranch())) {
			sv.chdrselErr.set(e455);
		}
	}

protected void validateKey22220()
	{
		if (isEQ(subprogrec.key2,SPACES)
		|| isEQ(subprogrec.key2,"N")) {
			return ;
		}		
	}
protected void checkStatus2400(){

	//ILIFE-6594 wli31
	t5679Map = itemDAO.loadSmartTable("IT", wsspcomn.company.toString().trim(), "T5679");
	List<Itempf> itempfList = new ArrayList<Itempf>();
	String keyItemitem = subprogrec.transcd.toString().trim();
	if (t5679Map.containsKey(keyItemitem)){	
		itempfList = t5679Map.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
		}		
	}
	wsaaSub.set(ZERO);
	
	lookForStatus();
}

protected void lookForStatus()
	{
		search2510();			
	}

protected void search2510()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)
		&& isEQ(sv.chdrselErr,SPACES)) {
			sv.chdrselErr.set(e767);
			wsspcomn.edterror.set("Y");			
		}
		else {
			if (isNE(chdrlnbIO.getStatcode(),t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				search2510();
			}
		}		
		/*EXIT*/
	}

protected void chkChdrDates2700()
	{
		/*CHDR-DATES*/		
		if (isNE(chdrlnbIO.getBtdate(),chdrlnbIO.getPtdate())) {
			sv.actionErr.set(g008);
		}
		
		/*EXIT*/
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);		
			wsspcomn.edterror.set("Y");
			exit3090();
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		updateWssp3010();		
		keeps3070();			
		batching3080();				
	}
protected void exit3090() {
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
}

protected void updateWssp3010()
	{		
		wsspcomn.flag.set("S");
		wsspcomn.chdrChdrnum.set(sv.chdrsel);
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			exit3090();
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		
		if (isNE(sv.errorIndicators,SPACES)) {
			batching3080();
		}
		if (isEQ(scrnparams.deviceInd,"*RMT")) {			
			preKeeps3060();
		}
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")
		&& isEQ(sv.chdrselErr,SPACES)) {
			sv.chdrselErr.set(f910);
			wsspcomn.edterror.set("Y");
			exit3090();
		}
	}

protected void preKeeps3060()
	{
		/*    For existing proposals, add one to the transaction number.*/
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			setPrecision(chdrlnbIO.getTranno(), 0);
			chdrlnbIO.setTranno(add(chdrlnbIO.getTranno(),1));
		}
	}

protected void keeps3070()
	{
		
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(chdrsurrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		chdrmjaIO.setFunction("KEEPS");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(wsspcomn.company);
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife("01");
		covrmjaIO.setCoverage("00");
		covrmjaIO.setRider("00");
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFunction(varcom.begn);
		
        covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
        covrmjaIO.setFitKeysSearch("CHDRNUM");

		SmartFileCode.execute(appVars, covrmjaIO);
		if ((isNE(covrmjaIO.getStatuz(),varcom.oK))
		&& (isNE(covrmjaIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company,covrmjaIO.getChdrcoy())
		|| isNE(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum())
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		
		
		
		/*EXIT*/		
	}
 
}
