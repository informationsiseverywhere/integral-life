/*
 * File: Dccan6.java
 * Date: 29 August 2009 22:45:43
 * Author: Quipoz Limited
 * 
 * Class transformed from DCCAN6.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* DCCAN6
* ======
* OVERVIEW
* ~~~~~~~~
* This is  a  new  subroutine  which  forms  part  of  the  9405
* Annuitied  Development.    It  is  the  subroutine  which will
* calculate the amount payable on a death  claim  of  a  benefit
* paying annuity.
*
* It  will  be called during the on-line registration of a death
* claim from T6598, which is read using the Death  Claim  method
* from T5687 for the component.
*
* Its purpose is to return a zero-value line with a status of O-K
* to P5256 which will then be written as a CLMD record and will
* thus trigger the processing of P5256AT.
*
*****************************************************************
* </pre>
*/
public class Dccan6 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;

	private FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaSwitch, " ");
		/* FORMATS */
	private String covrrec = "COVRREC";
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Deathrec deathrec = new Deathrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit190, 
		exit290, 
		exit602
	}

	public Dccan6() {
		super();
	}

public void mainline(Object... parmArray)
	{
		deathrec.deathRec = convertAndSetParam(deathrec.deathRec, parmArray, 0);
		try {
			init000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void init000()
	{
		try {
			para010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void para010()
	{
		deathrec.status.set(varcom.oK);
		deathrec.estimatedVal.set(ZERO);
		deathrec.actualVal.set(ZERO);
		deathrec.element.set(SPACES);
		deathrec.description.set(SPACES);
		deathrec.processInd.set("*");
		if (firstTime.isTrue()) {
			begnCovr100();
		}
		else {
			nextrCovr200();
		}
		wsaaSwitch.set("N");
		if (isEQ(covrIO.getStatuz(),varcom.endp)) {
			deathrec.status.set(varcom.endp);
			wsaaSwitch.set(SPACES);
			goTo(GotoLabel.exit090);
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void begnCovr100()
	{
		try {
			para110();
		}
		catch (GOTOException e){
		}
	}

protected void para110()
	{
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(deathrec.chdrChdrcoy);
		covrIO.setChdrnum(deathrec.chdrChdrnum);
		covrIO.setLife(deathrec.lifeLife);
		covrIO.setCoverage(deathrec.covrCoverage);
		covrIO.setRider(deathrec.covrRider);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");



		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if ((isNE(covrIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(covrIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(covrIO.getLife(),deathrec.lifeLife)
		|| isNE(covrIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(covrIO.getRider(),deathrec.covrRider)
		|| isEQ(covrIO.getStatuz(),varcom.endp))) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit190);
		}
	}

protected void nextrCovr200()
	{
		try {
			para210();
		}
		catch (GOTOException e){
		}
	}

protected void para210()
	{
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if ((isNE(covrIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(covrIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(covrIO.getLife(),deathrec.lifeLife)
		|| isNE(covrIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(covrIO.getRider(),deathrec.covrRider)
		|| isEQ(covrIO.getStatuz(),varcom.endp))) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit290);
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error600();
				}
				case exit602: {
					exit602();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error600()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit602);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit602()
	{
		deathrec.status.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
