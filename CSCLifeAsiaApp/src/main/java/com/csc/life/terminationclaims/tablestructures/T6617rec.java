package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:23
 * Description:
 * Copybook name: T6617REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6617rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6617Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData intAccrualMethod = new FixedLengthStringData(2).isAPartOf(t6617Rec, 0);
  	public FixedLengthStringData intCalcMethod = new FixedLengthStringData(2).isAPartOf(t6617Rec, 2);
  	public FixedLengthStringData intCapFrequency = new FixedLengthStringData(2).isAPartOf(t6617Rec, 4);
  	public FixedLengthStringData intCapMethod = new FixedLengthStringData(2).isAPartOf(t6617Rec, 6);
  	public ZonedDecimalData intRate = new ZonedDecimalData(8, 5).isAPartOf(t6617Rec, 8);
  	public ZonedDecimalData defInterestDay = new ZonedDecimalData(3, 0).isAPartOf(t6617Rec, 16);
  	public FixedLengthStringData filler = new FixedLengthStringData(481).isAPartOf(t6617Rec, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6617Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6617Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}