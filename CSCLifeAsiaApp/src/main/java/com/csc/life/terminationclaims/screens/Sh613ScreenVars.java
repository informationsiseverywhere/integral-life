package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH613
 * @version 1.0 generated on 30/08/09 07:06
 * @author Quipoz
 */
public class Sh613ScreenVars extends SmartVarModel { 

//ILIFE-1715 STARTS BY SLAKKALA
	//public FixedLengthStringData dataArea = new FixedLengthStringData(1100);
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	//public FixedLengthStringData dataFields = new FixedLengthStringData(540).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
//ILIFE-1715 ENDS 	
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,3);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,5);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,37);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,45);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,75);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,83);
	public ZonedDecimalData hbillosprm = DD.hbillosprm.copyToZonedDecimal().isAPartOf(dataFields,91);
	public ZonedDecimalData hprjptdate = DD.hprjptdate.copyToZonedDecimal().isAPartOf(dataFields,108);
	public ZonedDecimalData hregp = DD.hregp.copyToZonedDecimal().isAPartOf(dataFields,116);
	//ILIFE-1715 STARTS BY SLAKKALA	
	public ZonedDecimalData hreinstfee = DD.hreinstfee.copyToZonedDecimal().isAPartOf(dataFields,133);
	public ZonedDecimalData hrifeecnt = DD.hrifeecnt.copyToZonedDecimal().isAPartOf(dataFields,150);
	public ZonedDecimalData intanny = DD.intanny.copyToZonedDecimal().isAPartOf(dataFields,167);
	public FixedLengthStringData jlifedesc = DD.jlifedesc.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,212);
	public FixedLengthStringData lifedesc = DD.lifedesc.copy().isAPartOf(dataFields,220);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,260);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,268);
	public ZonedDecimalData newamnt = DD.newamnt.copyToZonedDecimal().isAPartOf(dataFields,269);
	public ZonedDecimalData osbal = DD.osbal.copyToZonedDecimal().isAPartOf(dataFields,286);
	public FixedLengthStringData ownerdesc = DD.ownerdesc.copy().isAPartOf(dataFields,303);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,343);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,353);
	public ZonedDecimalData sacscurbal = DD.sacscurbal.copyToZonedDecimal().isAPartOf(dataFields,361);
	public FixedLengthStringData sfcdesc = DD.sfcdesc.copy().isAPartOf(dataFields,378);
	public FixedLengthStringData taxamts = new FixedLengthStringData(34).isAPartOf(dataFields, 438);
	//ILIFE-1715 ENDS 
	public ZonedDecimalData[] taxamt = ZDArrayPartOfStructure(2, 17, 2, taxamts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(34).isAPartOf(taxamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData taxamt01 = DD.taxamt.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData taxamt02 = DD.taxamt.copyToZonedDecimal().isAPartOf(filler,17);
	//ILIFE-1715 STARTS BY SLAKKALA
	public ZonedDecimalData toamount = DD.toamount.copyToZonedDecimal().isAPartOf(dataFields,472);
	public ZonedDecimalData tolerance = DD.toler.copyToZonedDecimal().isAPartOf(dataFields,489);
	public ZonedDecimalData xamt = DD.xamt.copyToZonedDecimal().isAPartOf(dataFields,506);
	public ZonedDecimalData zlstfndval = DD.zlstfndval.copyToZonedDecimal().isAPartOf(dataFields,523);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	//ILIFE-1715 ENDS 
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData hbillosprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData hprjptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData hregpErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData hreinstfeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData hrifeecntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData intannyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData jlifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData lifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData newamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData osbalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData ownerdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData sacscurbalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData sfcdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData taxamtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData[] taxamtErr = FLSArrayPartOfStructure(2, 4, taxamtsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(taxamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData taxamt01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData taxamt02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData toamountErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData tolerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData xamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData zlstfndvalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
//ILIFE-1715 STARTS BY SLAKKALA
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
//ILIFE-1715 ENDS 
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] hbillosprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] hprjptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] hregpOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] hreinstfeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] hrifeecntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] intannyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] jlifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] lifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] newamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] osbalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] ownerdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] sacscurbalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] sfcdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData taxamtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 348);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(2, 12, taxamtsOut, 0);
	public FixedLengthStringData[][] taxamtO = FLSDArrayPartOfArrayStructure(12, 1, taxamtOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(taxamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] taxamt01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] taxamt02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] toamountOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] tolerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] xamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] zlstfndvalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hprjptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	
	public FixedLengthStringData msgPopup = new FixedLengthStringData(1000);
	public FixedLengthStringData msgresult = new FixedLengthStringData(10);
	public FixedLengthStringData msgDisplay = new FixedLengthStringData(10);

	public LongData Sh613screenWritten = new LongData(0);
	public LongData Sh613protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh613ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(osbalOut,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hrifeecntOut,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(xamtOut,new String[] {null, "01",null, null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamt01Out,new String[] {null, null, null, "41",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamt02Out,new String[] {null, null, null, "41",null, null, null, null, null, null, null, null});
		//screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, currfrom, chdrstatus, premstatus, cownnum, ownerdesc, lifenum, lifedesc, jlife, jlifedesc, sfcdesc, ptdate, btdate, effdate, hprjptdate, billfreq, mop, hregp, cntcurr, billcurr, sacscurbal, intanny, osbal, hbillosprm, hrifeecnt, hreinstfee, xamt, newamnt, toamount, zlstfndval, tolerance, taxamt01, taxamt02};
		screenFields = getScreenFields();
		//screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, currfromOut, chdrstatusOut, premstatusOut, cownnumOut, ownerdescOut, lifenumOut, lifedescOut, jlifenumOut, jlifedescOut, sfcdescOut, ptdateOut, btdateOut, effdateOut, hprjptdateOut, billfreqOut, mopOut, hregpOut, cntcurrOut, billcurrOut, sacscurbalOut, intannyOut, osbalOut, hbillosprmOut, hrifeecntOut, hreinstfeeOut, xamtOut, newamntOut, toamountOut, zlstfndvalOut, tolerOut, taxamt01Out, taxamt02Out};
		screenOutFields  = getScreenOutFields();
		//screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, currfromErr, chdrstatusErr, premstatusErr, cownnumErr, ownerdescErr, lifenumErr, lifedescErr, jlifenumErr, jlifedescErr, sfcdescErr, ptdateErr, btdateErr, effdateErr, hprjptdateErr, billfreqErr, mopErr, hregpErr, cntcurrErr, billcurrErr, sacscurbalErr, intannyErr, osbalErr, hbillosprmErr, hrifeecntErr, hreinstfeeErr, xamtErr, newamntErr, toamountErr, zlstfndvalErr, tolerErr, taxamt01Err, taxamt02Err};
		screenErrFields = getScreenErrFields();
		/*screenDateFields = new BaseData[] {currfrom, ptdate, btdate, effdate, hprjptdate};
		screenDateErrFields = new BaseData[] {currfromErr, ptdateErr, btdateErr, effdateErr, hprjptdateErr};
		screenDateDispFields = new BaseData[] {currfromDisp, ptdateDisp, btdateDisp, effdateDisp, hprjptdateDisp};*/
		screenDateFields = getScreenDateFields();
		screenDateErrFields = getScreenDateErrFields();
		screenDateDispFields = getScreenDateDispFields(); 

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh613screen.class;
		protectRecord = Sh613protect.class;
	}
	public BaseData[] getScreenFields(){
		return new BaseData[] {chdrnum, cnttype, ctypedes, currfrom, chdrstatus, premstatus, cownnum, ownerdesc, lifenum, lifedesc, jlife, jlifedesc, sfcdesc, ptdate, btdate, effdate, hprjptdate, billfreq, mop, hregp, cntcurr, billcurr, sacscurbal, intanny, osbal, hbillosprm, hrifeecnt, hreinstfee, xamt, newamnt, toamount, zlstfndval, tolerance, taxamt01, taxamt02};  
	}
	
	public BaseData[][] getScreenOutFields(){
		return new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, currfromOut, chdrstatusOut, premstatusOut, cownnumOut, ownerdescOut, lifenumOut, lifedescOut, jlifenumOut, jlifedescOut, sfcdescOut, ptdateOut, btdateOut, effdateOut, hprjptdateOut, billfreqOut, mopOut, hregpOut, cntcurrOut, billcurrOut, sacscurbalOut, intannyOut, osbalOut, hbillosprmOut, hrifeecntOut, hreinstfeeOut, xamtOut, newamntOut, toamountOut, zlstfndvalOut, tolerOut, taxamt01Out, taxamt02Out }; //IFSU-2036
	}
	
	public BaseData[] getScreenErrFields(){
		return new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, currfromErr, chdrstatusErr, premstatusErr, cownnumErr, ownerdescErr, lifenumErr, lifedescErr, jlifenumErr, jlifedescErr, sfcdescErr, ptdateErr, btdateErr, effdateErr, hprjptdateErr, billfreqErr, mopErr, hregpErr, cntcurrErr, billcurrErr, sacscurbalErr, intannyErr, osbalErr, hbillosprmErr, hrifeecntErr, hreinstfeeErr, xamtErr, newamntErr, toamountErr, zlstfndvalErr, tolerErr, taxamt01Err, taxamt02Err};
		
	}
	
	public BaseData[] getScreenDateFields(){
		return new BaseData[] {currfrom, ptdate, btdate, effdate, hprjptdate};
	}
	

	public BaseData[] getScreenDateDispFields(){
		return new BaseData[] {currfromDisp, ptdateDisp, btdateDisp, effdateDisp, hprjptdateDisp};
	}
	
	
	public BaseData[] getScreenDateErrFields(){
		return new BaseData[] {currfromErr, ptdateErr, btdateErr, effdateErr, hprjptdateErr};
	}

	
	public int getDataAreaSize(){
		return  1100;
	}
	
	public int getDataFieldsSize(){
		return 540;
	}
	
	public int getErrorIndicatorSize(){
		return  140;
	}
	
	public int getOutputFieldSize(){
		return 420;  
	}
}
