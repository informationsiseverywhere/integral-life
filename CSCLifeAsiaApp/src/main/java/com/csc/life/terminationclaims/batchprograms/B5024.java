/*
 * File: B5024.java
 * Date: 29 August 2009 20:51:59
 * Author: Quipoz Limited
 * 
 * Class transformed from B5024.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.sql.SQLException;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.procedures.Vpxmatc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxmatcrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.MtlhTableDAM;
import com.csc.life.terminationclaims.recordstructures.Matccpy;
import com.csc.life.terminationclaims.recordstructures.P5024par;
import com.csc.life.terminationclaims.reports.R5024Report;
import com.csc.life.terminationclaims.tablestructures.Tt550rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*   This  batch  program  produces  the  Pending  Maturity/Expiry
*   notice  and report.  The notice is a  letter to  the contract
*   owner  informing  him or  her of the  pending Maturity and/or
*   Expiry.  The  report  will  be  a  hard  copy  of  all  those
*   components  whose  Maturity  dates  are  drawing  near  their
*   Risk Cessation Date.
*
*   The  parameter  program P5024  allows  selection of contracts
*   within  Date From and  To dates.  These two  dates are passed
*   in to the batch program for use in the selection criteria.
*
*   For  each  component  found whose Risk Cessation  Date  falls
*   between  the  two  dates  passed  in  from P5024 are selected.
*   This selection  is  to  be  retrieved  by using SQL on COVRPF.
*
*   For  each contract selected whose  components maturity values
*   are not  spaces  create a  new  MTLH  record  with the  those
*   components that are not spaces.
*
*   To  get  the maturity Value for a component read T5687  using
*   the COVR-CRTABLE and if the Maturity Calc Method is not spaces
*   then  read  T6598  and  call  the  T6598-CALCPROG.  For  each
*   returned value from T6598-CALCPROG, the Maturity  Calculation
*   program, move the CRTABLE, SUMINS, BONUSAMT, MATAMT, VRTFUNG,
*   TYPE,  CURRCD and DESCRIP to the next occurance in  the array
*   and add  SUMINS to  SUMINS(21),   BONUSAMT to  BONUS(21)  and
*   ESTIMATED-VAL to  MATAMT(21) on MTLH and print the component.
*   Index 11 hold the sum of index 1 through 20.
*
*   We  want to send  a single letter of all the  components in a
*   contract to  the client.  As the automatic  interface between
*   the database  and the  letter  generation  in  AS/400  Office
*   will  produce one letter  for each  input  record we  need to
*   store  the  Maturity  Header and  details (components)  on to
*   one record for input into the Letter Generation.
*
*   NOTE: We  are  assuming  that  there  are  no  more  than ten
*         components in a contract. The program will halt if there
*         are more than  20 components  in  a contract.  To cater
*         for more than 20 components in a contract increase  the
*         array  size  in MTLHPF.  The last index holds the total
*         values.
*
*   Control totals:
*     01  -  Number of records selected via SQL
*     02  -  Number of MTLH records selected
*     03  -  Number of LETC records selected
*     04  -  Number of duplicate MTLH records
*
*
*****************************************************************
* </pre>
*/
public class B5024 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlcovrpf1rs = null;
	private java.sql.PreparedStatement sqlcovrpf1ps = null;
	private java.sql.Connection sqlcovrpf1conn = null;
	private String sqlcovrpf1 = "";
	private R5024Report printerFile = new R5024Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5024");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();
	private final int wsaaSize = 20;
	private ZonedDecimalData wsaaSizeT = new ZonedDecimalData(2, 0).init(21).setUnsigned();
	private ZonedDecimalData wsaaIntDatecfrom = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaIntDatecto = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaRcesdt = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaChdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumto = new FixedLengthStringData(8);
	private PackedDecimalData wsaaTransDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);
	private FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSqlChdrcoy = new FixedLengthStringData(1);
	private String wsaaValidStatus = "";
	private PackedDecimalData wsaaTotMatValues = new PackedDecimalData(17, 2);
	private String wsaaCrtableFound = "";
		/* ERRORS */
	private static final String i006 = "I006";
		/* FORMATS */
	private static final String descrec = "DESCREC";
	private static final String itdmrec = "ITEMREC";
	private static final String mtlhrec = "MTLHREC";
	private static final String chdrmatrec = "CHDRMATREC";
	private static final String payrrec = "PAYRREC";
	private static final String cltsrec = "CLTSREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t5687 = "T5687";
	private static final String t5679 = "T5679";
	private static final String t6598 = "T6598";
	private static final String t6625 = "T6625";
	private static final String tr384 = "TR384";
	private static final String tt550 = "TT550";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler1, 5);

		/*  Control indicators*/
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/*   Main, standard page headings*/
	private FixedLengthStringData r5024H01 = new FixedLengthStringData(93);
	private FixedLengthStringData r5024h01O = new FixedLengthStringData(93).isAPartOf(r5024H01, 0);
	private FixedLengthStringData rh01Datecfrom = new FixedLengthStringData(10).isAPartOf(r5024h01O, 0);
	private FixedLengthStringData rh01Datecto = new FixedLengthStringData(10).isAPartOf(r5024h01O, 10);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r5024h01O, 20);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r5024h01O, 21);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(r5024h01O, 51);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(r5024h01O, 61);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(r5024h01O, 63);

	private FixedLengthStringData r5024D02 = new FixedLengthStringData(8);
	private FixedLengthStringData r5024d02O = new FixedLengthStringData(8).isAPartOf(r5024D02, 0);
	private FixedLengthStringData rd02Chdrnum = new FixedLengthStringData(8).isAPartOf(r5024d02O, 0);
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private MtlhTableDAM mtlhIO = new MtlhTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T5687rec t5687rec = new T5687rec();
	private T5679rec t5679rec = new T5679rec();
	private T6598rec t6598rec = new T6598rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Tt550rec tt550rec = new Tt550rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Matccpy matccpy = new Matccpy();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private P5024par p5024par = new P5024par();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private R5024D01Inner r5024D01Inner = new R5024D01Inner();
	private SqlCovrpfInner sqlCovrpfInner = new SqlCovrpfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit109, 
		eof2106, 
		exit2109, 
		initialPrem3405, 
		exit3409, 
		readNextRecord4006, 
		exit4009, 
		exit4309, 
		initialPrem4405, 
		exit4409, 
		continue4505, 
		exit4509, 
		exit4609, 
		exit6009
	}

	public B5024() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		try {
			main110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void main110()
	{
		initialise1000();
		readFirstRecord2000();
		while ( !(endOfFile.isTrue())) {
			printReport3000();
		}
		
		finished9000();
		goTo(GotoLabel.exit109);
	}

protected void sqlError180()
	{
		sqlError9999();
	}

protected void initialise1000()
	{
		init1000();
		setUpHeadingCompany1001();
		setUpHeadingBranch1002();
		setUpHeadingDates1003();
		setupBatchkey1005();
		getStatus1008();
	}

protected void init1000()
	{
		p5024par.parmRecord.set(conjobrec.params);
		wsaaTransTime.set(getCobolTime());
		printerFile.openOutput();
	}

protected void setUpHeadingCompany1001()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(runparmrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		rh01Company.set(runparmrec.company);
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1002()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(runparmrec.branch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		rh01Branch.set(runparmrec.branch);
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1003()
	{
		rh01Datecfrom.set(p5024par.datecfrom);
		rh01Datecto.set(p5024par.datecto);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		wsaaTransDate.set(datcon1rec.intDate);
	}

protected void setupBatchkey1005()
	{
		wsaaBatckey.set(SPACES);
		wsaaBatckey.batcBatcpfx.set(runparmrec.runid);
		wsaaBatckey.batcBatccoy.set(runparmrec.company);
		wsaaBatckey.batcBatcbrn.set(runparmrec.batcbranch);
		wsaaBatckey.batcBatcactyr.set(runparmrec.acctyear);
		wsaaBatckey.batcBatcactmn.set(runparmrec.acctmonth);
		wsaaBatckey.batcBatctrcde.set(runparmrec.transcode);
	}

protected void getStatus1008()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readFirstRecord2000()
	{
		selectCovrRecords2000();
	}

protected void selectCovrRecords2000()
	{
		/*  Define the query required by declaring a cursor*/
		/*  Retrieve all records whose Risk Cessation Date are between*/
		/*  DateFrom and DateTo. The physical file used is COVRPF.*/
		wsaaChdrChdrcoy.set(SPACES);
		wsaaChdrChdrnum.set(SPACES);
		wsaaIntDatecfrom.set(p5024par.intDatecfrom);
		wsaaIntDatecto.set(p5024par.intDatecto);
		/*ILIFE-1024 starts sgadkari*/
		if (isEQ(p5024par.chdrnum, SPACES)) {
			p5024par.chdrnum.set(LOVALUE);
		}
		if (isEQ(p5024par.chdrnum1, SPACES)) {
			p5024par.chdrnum1.set(HIVALUE);
		}
		/*ILIFE-1024 ends*/
		/*    MOVE P5024-CHDRNUMFRM       TO WSAA-CHDRNUMFRM.              */
		wsaaChdrnumfrm.set(p5024par.chdrnum);
		/*    MOVE P5024-CHDRNUMTO        TO WSAA-CHDRNUMTO.               */
		wsaaChdrnumto.set(p5024par.chdrnum1);
		wsaaSqlChdrcoy.set(runparmrec.company);
		sqlcovrpf1 = " SELECT  CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, STATCODE, PSTATCODE, CRRCD, PRMCUR, CRTABLE, RCESDTE, SUMINS, INSTPREM, SINGP, SICURR, CBCVIN" +
" FROM   " + getAppVars().getTableNameOverriden("COVRPF") + " " +
" WHERE CHDRCOY = ?" +
" AND RCESDTE >= ?" +
" AND RCESDTE <= ?" +
" AND CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" AND VALIDFLAG = '1'" +
" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX";
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlcovrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM());
			sqlcovrpf1ps = getAppVars().prepareStatementEmbeded(sqlcovrpf1conn, sqlcovrpf1, "COVRPF");
			getAppVars().setDBString(sqlcovrpf1ps, 1, wsaaSqlChdrcoy);
			getAppVars().setDBNumber(sqlcovrpf1ps, 2, wsaaIntDatecfrom);
			getAppVars().setDBNumber(sqlcovrpf1ps, 3, wsaaIntDatecto);
			getAppVars().setDBString(sqlcovrpf1ps, 4, wsaaChdrnumfrm);
			getAppVars().setDBString(sqlcovrpf1ps, 5, wsaaChdrnumto);
			sqlcovrpf1rs = getAppVars().executeQuery(sqlcovrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError180();
			throw new RuntimeException("B5024: ERROR (1) - Unexpected return from a transformed GO TO statement: 180-SQL-ERROR");
		}
		/*   Fetch first record*/
		fetchPrimary2100();
	}

protected void fetchPrimary2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fetchRecord2100();
				case eof2106: 
					eof2106();
				case exit2109: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fetchRecord2100()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlcovrpf1rs)) {
				getAppVars().getDBObject(sqlcovrpf1rs, 1, sqlCovrpfInner.sqlChdrcoy);
				getAppVars().getDBObject(sqlcovrpf1rs, 2, sqlCovrpfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlcovrpf1rs, 3, sqlCovrpfInner.sqlLife);
				getAppVars().getDBObject(sqlcovrpf1rs, 4, sqlCovrpfInner.sqlJlife);
				getAppVars().getDBObject(sqlcovrpf1rs, 5, sqlCovrpfInner.sqlCoverage);
				getAppVars().getDBObject(sqlcovrpf1rs, 6, sqlCovrpfInner.sqlRider);
				getAppVars().getDBObject(sqlcovrpf1rs, 7, sqlCovrpfInner.sqlPlnsfx);
				getAppVars().getDBObject(sqlcovrpf1rs, 8, sqlCovrpfInner.sqlStatcode);
				getAppVars().getDBObject(sqlcovrpf1rs, 9, sqlCovrpfInner.sqlPstatcode);
				getAppVars().getDBObject(sqlcovrpf1rs, 10, sqlCovrpfInner.sqlCrrcd);
				getAppVars().getDBObject(sqlcovrpf1rs, 11, sqlCovrpfInner.sqlPrmcur);
				getAppVars().getDBObject(sqlcovrpf1rs, 12, sqlCovrpfInner.sqlCrtable);
				getAppVars().getDBObject(sqlcovrpf1rs, 13, sqlCovrpfInner.sqlRcesdte);
				getAppVars().getDBObject(sqlcovrpf1rs, 14, sqlCovrpfInner.sqlSumins);
				getAppVars().getDBObject(sqlcovrpf1rs, 15, sqlCovrpfInner.sqlInstprem);
				getAppVars().getDBObject(sqlcovrpf1rs, 16, sqlCovrpfInner.sqlSingp);
				getAppVars().getDBObject(sqlcovrpf1rs, 17, sqlCovrpfInner.sqlSicurr);
				getAppVars().getDBObject(sqlcovrpf1rs, 18, sqlCovrpfInner.sqlCbcvin);
			}
			else {
				goTo(GotoLabel.eof2106);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError180();
			throw new RuntimeException("B5024: ERROR (2) - Unexpected return from a transformed GO TO statement: 180-SQL-ERROR");
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2109);
	}

protected void eof2106()
	{
		wsaaEof.set("Y");
	}

protected void printReport3000()
	{
		/*PROCESS-SQL-RECORD*/
		wsaaChdrChdrcoy.set(sqlCovrpfInner.sqlChdrcoy);
		wsaaChdrChdrnum.set(sqlCovrpfInner.sqlChdrnum);
		chkMtlhRec3100();
		/* If Status = o-k, duplicate contract number found, skip to*/
		/* the next contract number.*/
		/* If status not = o-k, print the contract.*/
		if (isEQ(mtlhIO.getStatuz(), varcom.oK)) {
			skipContractNo3050();
		}
		else {
			processContract3070();
		}
		/*EXIT*/
	}

protected void skipContractNo3050()
	{
		/*FETCH-NEXT-COVR*/
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
		while ( !(isNE(sqlCovrpfInner.sqlChdrnum, wsaaChdrChdrnum)
		|| endOfFile.isTrue())) {
			fetchPrimary2100();
		}
		
		/*EXIT*/
	}

protected void processContract3070()
	{
		/*READ-CHDRMAT*/
		readChdrmatClntnum3300();
		/*CHECK-STATUS*/
		checkStatus3400();
		if (isEQ(wsaaValidStatus, "N")) {
			while ( !(isNE(sqlCovrpfInner.sqlChdrnum, wsaaChdrChdrnum)
			|| endOfFile.isTrue())) {
				fetchPrimary2100();
			}
			
			return ;
		}
		wsaaIndex.set(ZERO);
		wsaaSub.set(ZERO);
		initializeMtlhRecord3200();
		while ( !(isNE(sqlCovrpfInner.sqlChdrnum, wsaaChdrChdrnum)
		|| isNE(sqlCovrpfInner.sqlChdrcoy, wsaaChdrChdrcoy)
		|| endOfFile.isTrue())) {
			processComponents4000();
		}
		
		/* IF WSAA-TOT-MAT-VALUES      = ZEROES                         */
		/*     GO TO                   3079-EXIT.                       */
		writeMtlhRecord5000();
		writeLetcRecord6000();
		/*EXIT*/
	}

protected void chkMtlhRec3100()
	{
		/*READ-MTLH*/
		mtlhIO.setChdrcoy(sqlCovrpfInner.sqlChdrcoy);
		mtlhIO.setChdrnum(sqlCovrpfInner.sqlChdrnum);
		mtlhIO.setFunction(varcom.readr);
		mtlhIO.setFormat(mtlhrec);
		SmartFileCode.execute(appVars, mtlhIO);
		if (isNE(mtlhIO.getStatuz(), varcom.oK)
		&& isNE(mtlhIO.getStatuz(), varcom.mrnf)) {
			conerrrec.params.set(mtlhIO.getParams());
			databaseError006();
		}
		/*EXIT*/
	}

protected void initializeMtlhRecord3200()
	{
		/*MOVE-SPACES-ZEROES*/
		mtlhIO.setParams(SPACES);
		wsaaSub2.set(1);
		while ( !(isGT(wsaaSub2, wsaaSizeT))) {
			mtlhIO.setBonusamt(wsaaSub2, ZERO);
			mtlhIO.setSumins(wsaaSub2, ZERO);
			mtlhIO.setMatamt(wsaaSub2, ZERO);
			wsaaSub2.add(1);
		}
		
		/*EXIT*/
	}

	/**
	* <pre>
	*  This section retreive the contract prefix number, company
	*  number and the client number of the selected contract.
	* </pre>
	*/
protected void readChdrmatClntnum3300()
	{
		callChdrmat3300();
	}

protected void callChdrmat3300()
	{
		chdrmatIO.setParams(SPACES);
		chdrmatIO.setChdrcoy(sqlCovrpfInner.sqlChdrcoy);
		chdrmatIO.setChdrnum(sqlCovrpfInner.sqlChdrnum);
		chdrmatIO.setFormat(chdrmatrec);
		chdrmatIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmatIO);
		if (isNE(chdrmatIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(chdrmatIO.getParams());
			databaseError006();
		}
		/*  Read the PAYR file                                             */
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(sqlCovrpfInner.sqlChdrcoy);
		payrIO.setChdrnum(sqlCovrpfInner.sqlChdrnum);
		payrIO.setValidflag("1");
		payrIO.setPayrseqno(1);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			conerrrec.statuz.set(payrIO.getStatuz());
			conerrrec.params.set(payrIO.getParams());
			databaseError006();
		}
	}

	/**
	* <pre>
	* This section check the contract status and the premium status
	* against table T5679.
	* </pre>
	*/
protected void checkStatus3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialRisk3400();
					loopRisk3402();
				case initialPrem3405: 
					initialPrem3405();
					loopPrem3407();
				case exit3409: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialRisk3400()
	{
		wsaaSub1.set(ZERO);
	}

protected void loopRisk3402()
	{
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, 12)) {
			wsaaValidStatus = "N";
			goTo(GotoLabel.exit3409);
		}
		if (isEQ(chdrmatIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub1.toInt()])) {
			goTo(GotoLabel.initialPrem3405);
		}
		loopRisk3402();
		return ;
	}

protected void initialPrem3405()
	{
		wsaaSub1.set(ZERO);
	}

protected void loopPrem3407()
	{
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, 12)) {
			wsaaValidStatus = "N";
			return ;
		}
		if (isEQ(chdrmatIO.getPstatcode(), t5679rec.cnPremStat[wsaaSub1.toInt()])) {
			wsaaValidStatus = "Y";
			return ;
		}
		loopPrem3407();
		return ;
	}

	/**
	* <pre>
	*  This section select and process all the components for a
	*  contract where the Maturity Calculation Method and the
	*  Calculation program are not spaces.
	* </pre>
	*/
protected void processComponents4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					processComponent4000();
					chkComponentStatii4003();
				case readNextRecord4006: 
					readNextRecord4006();
				case exit4009: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void processComponent4000()
	{
		wsaaRider.set(sqlCovrpfInner.sqlRider);
		wsaaCoverage.set(sqlCovrpfInner.sqlCoverage);
		wsaaLife.set(sqlCovrpfInner.sqlLife);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sqlCovrpfInner.sqlRcesdte);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaRcesdt.set(datcon1rec.extDate);
		/* When T5687-MATURITY-CALC-METH = spaces, we are assuming*/
		/* that this contract is a expiry contract. It will go and print*/
		/* the components*/
		readTableT56874100();
		/* IF T5687-MATURITY-CALC-METH = SPACES                         */
		/*     PERFORM 4300-EXPIRY-CONTRACT                             */
		/*     GO TO                   4009-EXIT.                       */
		/*    Deferred Annuitites do not mature, therefore check annuity   */
		/*    details by reading table T6625 with coverage code as the     */
		/*    item.  If the coverage code IS found, skip this section.     */
		/*    If the coverage code is NOT found, the existing code for     */
		/*    maturity should proceed.                                     */
		wsaaCrtableFound = "N";
		checkIfAnnuity4800();
		if (isEQ(wsaaCrtableFound, "Y")) {
			goTo(GotoLabel.readNextRecord4006);
		}
		if (isEQ(t5687rec.maturityCalcMeth, SPACES)) {
			expiryContract4300();
			goTo(GotoLabel.exit4009);
		}
		/* When T6598-CALCPROG = spaces, we are assuming*/
		/* that this contract is a expiry contract. It will go and print*/
		/* the components.*/
		readTableT65984150();
		if (isEQ(t6598rec.calcprog, SPACES)) {
			expiryContract4300();
			goTo(GotoLabel.exit4009);
		}
	}

	/**
	* <pre>
	* skip this component if it is already matured.
	* </pre>
	*/
protected void chkComponentStatii4003()
	{
		chkComponentStatii4400();
		if (isEQ(wsaaValidStatus, "N")) {
			fetchPrimary2100();
			goTo(GotoLabel.exit4009);
		}
		initializeParameter4200();
		wsaaSub.add(1);
		while ( !(isEQ(matccpy.status, varcom.endp)
		|| isEQ(matccpy.status, "NETM"))) {
			callMatCalcprog4500();
		}
		
	}

protected void readNextRecord4006()
	{
		while ( !(isNE(sqlCovrpfInner.sqlRider, wsaaRider)
		|| isNE(sqlCovrpfInner.sqlCoverage, wsaaCoverage)
		|| isNE(sqlCovrpfInner.sqlLife, wsaaLife)
		|| isNE(sqlCovrpfInner.sqlChdrnum, wsaaChdrChdrnum)
		|| isNE(sqlCovrpfInner.sqlChdrcoy, wsaaChdrChdrcoy)
		|| endOfFile.isTrue())) {
			fetchPrimary2100();
		}
		
	}

	/**
	* <pre>
	*  This section retrieves the Maturity Calculation Method from
	*  table T5687.
	* </pre>
	*/
protected void readTableT56874100()
	{
		readT56874100();
	}

protected void readT56874100()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(sqlCovrpfInner.sqlCrtable);
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(), runparmrec.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), sqlCovrpfInner.sqlCrtable)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemcoy(runparmrec.company);
			itdmIO.setItemtabl(t5687);
			itdmIO.setItemitem(sqlCovrpfInner.sqlCrtable);
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	}

	/**
	* <pre>
	*  This section retrieves the Calculation Program from table
	*  T6598.
	* </pre>
	*/
protected void readTableT65984150()
	{
		readT65984150();
	}

protected void readT65984150()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.maturityCalcMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*  This section initializes the MTLH record for each contract
	*  selected.
	*  Initialize MATC-PARAMS for each component.
	* </pre>
	*/
protected void initializeParameter4200()
	{
		initPara4200();
	}

protected void initPara4200()
	{
		matccpy.maturityRec.set(SPACES);
		matccpy.batckey.set(wsaaBatckey);
		matccpy.chdrChdrcoy.set(sqlCovrpfInner.sqlChdrcoy);
		matccpy.chdrChdrnum.set(sqlCovrpfInner.sqlChdrnum);
		matccpy.planSuffix.set(ZERO);
		matccpy.polsum.set(chdrmatIO.getPolsum());
		matccpy.lifeLife.set(sqlCovrpfInner.sqlLife);
		matccpy.lifeJlife.set(sqlCovrpfInner.sqlJlife);
		matccpy.covrCoverage.set(sqlCovrpfInner.sqlCoverage);
		matccpy.covrRider.set(sqlCovrpfInner.sqlRider);
		matccpy.crtable.set(sqlCovrpfInner.sqlCrtable);
		matccpy.crrcd.set(sqlCovrpfInner.sqlCrrcd);
		matccpy.effdate.set(sqlCovrpfInner.sqlRcesdte);
		/* MOVE CHDRMAT-BILLFREQ       TO MATC-BILLFREQ.                */
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			matccpy.billfreq.set("00");
		}
		else {
			matccpy.billfreq.set(payrIO.getBillfreq());
		}
		matccpy.chdrCurr.set(chdrmatIO.getCntcurr());
		matccpy.chdrType.set(chdrmatIO.getCnttype());
		if (isGT(sqlCovrpfInner.sqlInstprem, ZERO)) {
			matccpy.singp.set(sqlCovrpfInner.sqlInstprem);
		}
		else {
			matccpy.singp.set(sqlCovrpfInner.sqlSingp);
		}
		matccpy.convUnits.set(sqlCovrpfInner.sqlCbcvin);
		matccpy.language.set(runparmrec.language);
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		matccpy.currcode.set(sqlCovrpfInner.sqlPrmcur);
		matccpy.matCalcMeth.set(t5687rec.maturityCalcMeth);
		matccpy.pstatcode.set(sqlCovrpfInner.sqlPstatcode);
		matccpy.planSwitch.set(1);
		matccpy.status.set(varcom.oK);
	}

	/**
	* <pre>
	*  This section moves the initialize the expiry values in MTLH.
	* </pre>
	*/
protected void expiryContract4300()
	{
		try {
			skipUntilDiffChdr4300();
			initialize4302();
			getDescription4305();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void skipUntilDiffChdr4300()
	{
		initializeParameter4200();
		/*CHK-COMPONENT-STATII*/
		chkComponentStatii4400();
		if (isEQ(wsaaValidStatus, "N")) {
			fetchPrimary2100();
			goTo(GotoLabel.exit4309);
		}
	}

protected void initialize4302()
	{
		wsaaIndex.add(1);
		/* Array size is 10. Index must not be greater than 10.*/
		/* Index number 11 contains the sum of the components for a*/
		/* contract.*/
		if (isGT(wsaaIndex, wsaaSize)) {
			conerrrec.statuz.set(i006);
			systemError005();
			goTo(GotoLabel.exit4309);
		}
		wsaaRider.set(sqlCovrpfInner.sqlRider);
		wsaaCoverage.set(sqlCovrpfInner.sqlCoverage);
		wsaaLife.set(sqlCovrpfInner.sqlLife);
		mtlhIO.setJlife(sqlCovrpfInner.sqlJlife);
		mtlhIO.setCurrcd(wsaaIndex, chdrmatIO.getCntcurr());
		mtlhIO.setRcesdt(wsaaIndex, wsaaRcesdt);
	}

protected void getDescription4305()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrmatIO.getChdrcoy());
		descIO.setDesctabl(t5687);
		descIO.setDescitem(sqlCovrpfInner.sqlCrtable);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		mtlhIO.setDescrip(wsaaIndex, descIO.getLongdesc());
		matccpy.chdrChdrnum.set(sqlCovrpfInner.sqlChdrnum);
		matccpy.crtable.set(sqlCovrpfInner.sqlCrtable);
		matccpy.description.set(descIO.getLongdesc());
		writeDetailsReport4700();
		while ( !(isNE(sqlCovrpfInner.sqlRider, wsaaRider)
		|| isNE(sqlCovrpfInner.sqlCoverage, wsaaCoverage)
		|| isNE(sqlCovrpfInner.sqlLife, wsaaLife)
		|| isNE(sqlCovrpfInner.sqlChdrnum, wsaaChdrChdrnum)
		|| isNE(sqlCovrpfInner.sqlChdrcoy, wsaaChdrChdrcoy)
		|| endOfFile.isTrue())) {
			fetchPrimary2100();
		}
		
	}

	/**
	* <pre>
	* This section check the component status and the premium status
	* against table T5679.
	* </pre>
	*/
protected void chkComponentStatii4400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialRisk4400();
					loopRisk4402();
				case initialPrem4405: 
					initialPrem4405();
					loopPrem4407();
				case exit4409: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialRisk4400()
	{
		wsaaSub1.set(ZERO);
	}

protected void loopRisk4402()
	{
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, 12)) {
			wsaaValidStatus = "N";
			goTo(GotoLabel.exit4409);
		}
		if (isEQ(sqlCovrpfInner.sqlStatcode, t5679rec.covRiskStat[wsaaSub1.toInt()])) {
			goTo(GotoLabel.initialPrem4405);
		}
		loopRisk4402();
		return ;
	}

protected void initialPrem4405()
	{
		wsaaSub1.set(ZERO);
	}

protected void loopPrem4407()
	{
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, 12)) {
			wsaaValidStatus = "N";
			return ;
		}
		if (isEQ(sqlCovrpfInner.sqlPstatcode, t5679rec.covPremStat[wsaaSub1.toInt()])) {
			wsaaValidStatus = "Y";
			return ;
		}
		loopPrem4407();
		return ;
	}

	/**
	* <pre>
	*  This section calls the Maturity Calculation Method program.
	*  and store the components into the appropriate arrays in MTLH
	*  and prints the component.
	*  The Calculation Method program was retrieved from T6598.
	* </pre>
	*/
protected void callMatCalcprog4500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					callCalcProg4500();
				case continue4505: 
					continue4505();
				case exit4509: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void callCalcProg4500()
	{
		//add VPMS code to redirect to LIFERUL 
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		Vpxmatcrec vpxmatcRec = new Vpxmatcrec();
		if(!AppVars.getInstance().getAppConfig().isVpmsEnable()) 
		{
			callProgram(t6598rec.calcprog, matccpy.maturityRec);
		}
		else
		{	
			vpmcalcrec.linkageArea.set(matccpy.maturityRec);		
			vpxmatcRec.function.set("INIT");
			callProgram(Vpxmatc.class, vpmcalcrec.vpmcalcRec,vpxmatcRec);	
			matccpy.maturityRec.set(vpmcalcrec.linkageArea);			
			if(isEQ(matccpy.fund,SPACES))
				return;	
			callProgram(t6598rec.calcprog, matccpy.maturityRec, vpxmatcRec);
		} 	
		if (isEQ(matccpy.status, varcom.bomb)) {
			conerrrec.statuz.set(matccpy.status);
			systemError005();
		}
		/* If MATC-STATUS = 'NETM', it means component is not eligible*/
		/* for maturity. Effective Date less than Risk Cessation Date.*/
		if (isEQ(matccpy.status, "NETM")) {
			goTo(GotoLabel.exit4509);
		}
		/* MOVE MATC-ACTUAL-VAL        TO ZRDP-AMOUNT-IN.               */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO MATC-ACTUAL-VAL.              */
		if (isNE(matccpy.actualVal, 0)) {
			zrdecplrec.amountIn.set(matccpy.actualVal);
			callRounding8000();
			matccpy.actualVal.set(zrdecplrec.amountOut);
		}
		/* MOVE MATC-ESTIMATED-VAL     TO ZRDP-AMOUNT-IN.               */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO MATC-ESTIMATED-VAL.           */
		if (isNE(matccpy.estimatedVal, 0)) {
			zrdecplrec.amountIn.set(matccpy.estimatedVal);
			callRounding8000();
			matccpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		/* If U/L component go and write details report.*/
		if (isEQ(matccpy.type, "A")
		|| isEQ(matccpy.type, "I")) {
			goTo(GotoLabel.continue4505);
		}
		/* Do not display the bonus if bonus value is zeroes.*/
		if (isNE(matccpy.type, "S")
		&& isEQ(matccpy.actualVal, ZERO)) {
			goTo(GotoLabel.exit4509);
		}
	}

protected void continue4505()
	{
		storeMatccpyMtlh4600();
		writeDetailsReport4700();
	}

	/**
	* <pre>
	*  This section moves the appropriate values into the
	*  corresponding indexes and sum them into index 11 in MTLH.
	* Array size is 10. Index must not be greater than 10.
	* Index number 11 contains the sum of the components for a
	* contract.
	* </pre>
	*/
protected void storeMatccpyMtlh4600()
	{
		try {
			storeUlValues4600();
			storeTpValues4604();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void storeUlValues4600()
	{
		mtlhIO.setJlife(matccpy.lifeJlife);
		if (isEQ(matccpy.type, "A")
		|| isEQ(matccpy.type, "I")) {
			wsaaIndex.add(1);
			if (isGT(wsaaIndex, wsaaSize)) {
				conerrrec.statuz.set(i006);
				systemError005();
				goTo(GotoLabel.exit4609);
			}
			mtlhIO.setMatamt(wsaaIndex, matccpy.estimatedVal);
			mtlhIO.setCrtable(wsaaIndex, matccpy.crtable);
			mtlhIO.setVrtfund(wsaaIndex, matccpy.fund);
			mtlhIO.setType(wsaaIndex, matccpy.type);
			mtlhIO.setCurrcd(wsaaIndex, matccpy.chdrCurr);
			mtlhIO.setDescrip(wsaaIndex, matccpy.description);
			mtlhIO.setRcesdt(wsaaIndex, wsaaRcesdt);
			setPrecision(mtlhIO.getMatamt(wsaaSizeT), 2);
			mtlhIO.setMatamt(wsaaSizeT, add(mtlhIO.getMatamt(wsaaSizeT), matccpy.estimatedVal));
			goTo(GotoLabel.exit4609);
		}
	}

protected void storeTpValues4604()
	{
		if (isGT(wsaaSub, wsaaSize)) {
			conerrrec.statuz.set(i006);
			systemError005();
			return ;
		}
		if (isEQ(matccpy.type, "B")
		|| isEQ(matccpy.type, "M")) {
			setPrecision(mtlhIO.getBonusamt(wsaaSub), 2);
			mtlhIO.setBonusamt(wsaaSub, add(mtlhIO.getBonusamt(wsaaSub), matccpy.actualVal));
			setPrecision(mtlhIO.getBonusamt(wsaaSizeT), 2);
			mtlhIO.setBonusamt(wsaaSizeT, add(mtlhIO.getBonusamt(wsaaSizeT), matccpy.actualVal));
		}
		else {
			if (isEQ(matccpy.type, "S")) {
				mtlhIO.setSumins(wsaaSub, matccpy.actualVal);
				mtlhIO.setCrtable(wsaaSub, matccpy.crtable);
				mtlhIO.setVrtfund(wsaaSub, matccpy.fund);
				mtlhIO.setType(wsaaSub, matccpy.type);
				mtlhIO.setCurrcd(wsaaSub, matccpy.chdrCurr);
				mtlhIO.setDescrip(wsaaSub, matccpy.description);
				mtlhIO.setRcesdt(wsaaSub, wsaaRcesdt);
				setPrecision(mtlhIO.getSumins(wsaaSizeT), 2);
				mtlhIO.setSumins(wsaaSizeT, add(mtlhIO.getSumins(wsaaSizeT), matccpy.actualVal));
			}
		}
	}

protected void writeDetailsReport4700()
	{
		writeDetail4700();
		getPrctReduction4707();
	}

protected void writeDetail4700()
	{
		/* If first page/overflow - write standard headings*/
		if (isNE(r5024D01Inner.rd01Chdrnum, sqlCovrpfInner.sqlChdrnum)) {
			rd02Chdrnum.set(SPACES);
			printerFile.printR5024d02(r5024D02, indicArea);
		}
		if (newPageReq.isTrue()) {
			printerFile.printR5024h01(r5024H01, indicArea);
			wsaaOverflow.set("N");
		}
		r5024D01Inner.rd01Chdrnum.set(matccpy.chdrChdrnum);
		r5024D01Inner.rd01Life.set(matccpy.lifeLife);
		r5024D01Inner.rd01Coverage.set(matccpy.covrCoverage);
		r5024D01Inner.rd01Rider.set(matccpy.covrRider);
		r5024D01Inner.rd01Crtable.set(matccpy.crtable);
		r5024D01Inner.rd01Descrip.set(matccpy.description);
		r5024D01Inner.rd01Sumin.set(ZERO);
		r5024D01Inner.rd01Bonuses.set(ZERO);
		if (isEQ(matccpy.type, "B")
		|| isEQ(matccpy.type, "M")
		|| isEQ(matccpy.type, "T")
		|| isEQ(matccpy.type, "X")) {
			r5024D01Inner.rd01Bonuses.set(matccpy.actualVal);
		}
		else {
			r5024D01Inner.rd01Sumin.set(matccpy.actualVal);
		}
		r5024D01Inner.rd01Estimated.set(matccpy.estimatedVal);
		r5024D01Inner.rd01Rcesdte.set(wsaaRcesdt);
		r5024D01Inner.rd01Currency.set(matccpy.currcode);
		r5024D01Inner.rd01Vrtfund.set(matccpy.fund);
	}

protected void getPrctReduction4707()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(matccpy.chdrChdrcoy);
		itdmIO.setItemtabl(tt550);
		itdmIO.setItemitem(matccpy.matCalcMeth);
		itdmIO.setItmfrm(matccpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			conerrrec.statuz.set(itdmIO.getStatuz());
			databaseError006();
		}
		if (isNE(itdmIO.getItemcoy(), matccpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(), tt550)
		|| isNE(itdmIO.getItemitem(), matccpy.matCalcMeth)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			r5024D01Inner.rd01Tmatpct.set(100);
		}
		else {
			tt550rec.tt550Rec.set(itdmIO.getGenarea());
			r5024D01Inner.rd01Tmatpct.set(tt550rec.tmatpct);
		}
		/*  Write detail, checking for page overflow*/
		printerFile.printR5024d01(r5024D01Inner.r5024D01, indicArea);
	}

protected void checkIfAnnuity4800()
	{
		para4801();
	}

protected void para4801()
	{
		/*    Read table T6625 using coverage code as item.                */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(sqlCovrpfInner.sqlCrtable);
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if (isEQ(sqlCovrpfInner.sqlCrtable, itdmIO.getItemitem())) {
			wsaaCrtableFound = "Y";
		}
	}

	/**
	* <pre>
	*  This section writes a MTLH record for each contract that was
	*  selected.
	* </pre>
	*/
protected void writeMtlhRecord5000()
	{
		readClntRec5000();
		writeRecord5004();
	}

protected void readClntRec5000()
	{
		/* MOVE SPACES                 TO CLNT-PARAMS.                  */
		/* MOVE CHDRMAT-COWNPFX        TO CLNT-CLNTPFX.                 */
		/* MOVE CHDRMAT-COWNCOY        TO CLNT-CLNTCOY.                 */
		/* MOVE CHDRMAT-COWNNUM        TO CLNT-CLNTNUM.                 */
		/* MOVE READR                  TO CLNT-FUNCTION.                */
		/* CALL 'CLNTIO' USING         CLNT-PARAMS.                     */
		/* IF CLNT-STATUZ              NOT = O-K                        */
		/*     MOVE CLNT-PARAMS        TO CONR-PARAMS                   */
		/*     PERFORM 006-DATABASE-ERROR.                              */
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx(chdrmatIO.getCownpfx());
		cltsIO.setClntcoy(chdrmatIO.getCowncoy());
		cltsIO.setClntnum(chdrmatIO.getCownnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(cltsIO.getParams());
			conerrrec.statuz.set(cltsIO.getStatuz());
			databaseError006();
		}
	}

protected void writeRecord5004()
	{
		/* Note the A06316 change to pull in the SALUTL field....          */
		mtlhIO.setChdrcoy(wsaaChdrChdrcoy);
		mtlhIO.setChdrnum(wsaaChdrChdrnum);
		mtlhIO.setCowncoy(chdrmatIO.getCowncoy());
		mtlhIO.setCownnum(chdrmatIO.getCownnum());
		mtlhIO.setTrandate(wsaaTransDate);
		mtlhIO.setTransactionTime(wsaaTransTime);
		/* MOVE CLNT-SURNAME           TO MTLH-SURNAME.                 */
		/* MOVE CLNT-GIVNAME           TO MTLH-GIVNAME.                 */
		/* MOVE CLNT-SALUT             TO MTLH-SALUT.                   */
		/* MOVE CLNT-INITIALS          TO MTLH-INITIALS.                */
		/* MOVE CLNT-CLTADDR01         TO MTLH-CLTADDR01.               */
		/* MOVE CLNT-CLTADDR02         TO MTLH-CLTADDR02.               */
		/* MOVE CLNT-CLTADDR03         TO MTLH-CLTADDR03.               */
		/* MOVE CLNT-CLTADDR04         TO MTLH-CLTADDR04.               */
		/* MOVE CLNT-CLTADDR05         TO MTLH-CLTADDR05.               */
		/* MOVE CLNT-CLTPCODE          TO MTLH-CLTPCODE.                */
		mtlhIO.setSurname(cltsIO.getSurname());
		mtlhIO.setGivname(cltsIO.getGivname());
		mtlhIO.setSalut(cltsIO.getSalutl());
		mtlhIO.setInitials(cltsIO.getInitials());
		mtlhIO.setCltaddr01(cltsIO.getCltaddr01());
		mtlhIO.setCltaddr02(cltsIO.getCltaddr02());
		mtlhIO.setCltaddr03(cltsIO.getCltaddr03());
		mtlhIO.setCltaddr04(cltsIO.getCltaddr04());
		mtlhIO.setCltaddr05(cltsIO.getCltaddr05());
		mtlhIO.setCltpcode(cltsIO.getCltpcode());
		mtlhIO.setFunction(varcom.writr);
		mtlhIO.setFormat(mtlhrec);
		SmartFileCode.execute(appVars, mtlhIO);
		if (isNE(mtlhIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(mtlhIO.getParams());
			databaseError006();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

	/**
	* <pre>
	*  This section write a corresponding LETC record for each MTLH
	*  record. The LETOKEYS field is the contract header number
	*  CHDRNUM. Do not write letter if letter not found on T6634.
	* </pre>
	*/
protected void writeLetcRecord6000()
	{
		try {
			readT6634Record6000();
			writeRecord6002();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*  Read T6634 the Auto Letters table to get the letter type.
	*  The ITEM-ITEMITEM key is made up of CHDRMAT-CNTTYPE and
	*  the transaction code (B507).
	* </pre>
	*/
protected void readT6634Record6000()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		/* MOVE T6634                  TO ITEM-ITEMTABL.                */
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from contract type & transaction code.      */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmatIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.exit6009);
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.              */
		tr384rec.tr384Rec.set(itemIO.getGenarea());
	}

protected void writeRecord6002()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(mtlhIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.          */
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaTransDate);
		letrqstrec.clntcoy.set(mtlhIO.getCowncoy());
		letrqstrec.clntnum.set(mtlhIO.getCownnum());
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(mtlhIO.getChdrcoy());
		letrqstrec.chdrcoy.set(mtlhIO.getChdrcoy());
		letrqstrec.rdocnum.set(mtlhIO.getChdrnum());
		letrqstrec.chdrnum.set(mtlhIO.getChdrnum());
		/*  MOVE MTLH-CHDRNUM           TO LETRQST-OTHER-KEYS.           */
		letrqstrec.branch.set(chdrmatIO.getCntbranch());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(letrqstrec.statuz);
			systemError005();
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(runparmrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrmatIO.getCntcurr());
		zrdecplrec.batctrcde.set(runparmrec.transcode);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			conerrrec.statuz.set(zrdecplrec.statuz);
			conerrrec.params.set(zrdecplrec.zrdecplRec);
			systemError005();
		}
		/*EXIT*/
	}

protected void finished9000()
	{
		/*CLOSE-FILES*/
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlcovrpf1conn, sqlcovrpf1ps, sqlcovrpf1rs);
		printerFile.close();
		/*EXIT*/
	}

protected void sqlError9999()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		conerrrec.syserror.set(sqlStatuz);
		systemError005();
		/*EXIT-SQL-ERROR*/
	}
/*
 * Class transformed  from Data Structure SQL-COVRPF--INNER
 */
private static final class SqlCovrpfInner { 

		/* SQL-COVRPF */
	private FixedLengthStringData sqlCovrrec = new FixedLengthStringData(76);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlCovrrec, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlCovrrec, 1);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 9);
	private FixedLengthStringData sqlJlife = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 11);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 13);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 15);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlCovrrec, 17);
	private FixedLengthStringData sqlStatcode = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 20);
	private FixedLengthStringData sqlPstatcode = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 22);
	private PackedDecimalData sqlCrrcd = new PackedDecimalData(8, 0).isAPartOf(sqlCovrrec, 24);
	private FixedLengthStringData sqlPrmcur = new FixedLengthStringData(3).isAPartOf(sqlCovrrec, 29);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlCovrrec, 32);
	private PackedDecimalData sqlRcesdte = new PackedDecimalData(8, 0).isAPartOf(sqlCovrrec, 36);
	private PackedDecimalData sqlSumins = new PackedDecimalData(17, 2).isAPartOf(sqlCovrrec, 41);
	private PackedDecimalData sqlInstprem = new PackedDecimalData(17, 2).isAPartOf(sqlCovrrec, 50);
	private PackedDecimalData sqlSingp = new PackedDecimalData(17, 2).isAPartOf(sqlCovrrec, 59);
	private FixedLengthStringData sqlSicurr = new FixedLengthStringData(3).isAPartOf(sqlCovrrec, 68);
	private PackedDecimalData sqlCbcvin = new PackedDecimalData(8, 0).isAPartOf(sqlCovrrec, 71);
}
/*
 * Class transformed  from Data Structure R5024-D01--INNER
 */
private static final class R5024D01Inner { 

	private FixedLengthStringData r5024D01 = new FixedLengthStringData(119);
	private FixedLengthStringData r5024d01O = new FixedLengthStringData(119).isAPartOf(r5024D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r5024d01O, 0);
	private FixedLengthStringData rd01Life = new FixedLengthStringData(2).isAPartOf(r5024d01O, 8);
	private FixedLengthStringData rd01Coverage = new FixedLengthStringData(2).isAPartOf(r5024d01O, 10);
	private FixedLengthStringData rd01Rider = new FixedLengthStringData(2).isAPartOf(r5024d01O, 12);
	private FixedLengthStringData rd01Crtable = new FixedLengthStringData(4).isAPartOf(r5024d01O, 14);
	private FixedLengthStringData rd01Descrip = new FixedLengthStringData(30).isAPartOf(r5024d01O, 18);
	private ZonedDecimalData rd01Tmatpct = new ZonedDecimalData(5, 2).isAPartOf(r5024d01O, 48);
	private ZonedDecimalData rd01Sumin = new ZonedDecimalData(15, 0).isAPartOf(r5024d01O, 53);
	private ZonedDecimalData rd01Bonuses = new ZonedDecimalData(17, 2).isAPartOf(r5024d01O, 68);
	private FixedLengthStringData rd01Rcesdte = new FixedLengthStringData(10).isAPartOf(r5024d01O, 85);
	private FixedLengthStringData rd01Currency = new FixedLengthStringData(3).isAPartOf(r5024d01O, 95);
	private FixedLengthStringData rd01Vrtfund = new FixedLengthStringData(4).isAPartOf(r5024d01O, 98);
	private ZonedDecimalData rd01Estimated = new ZonedDecimalData(17, 2).isAPartOf(r5024d01O, 102);
}
}
