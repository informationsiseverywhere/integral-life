package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR690
 * @version 1.0 generated on 30/08/09 07:24
 * @author Quipoz
 */
public class Sr690ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(164);
	public FixedLengthStringData dataFields = new FixedLengthStringData(52).isAPartOf(dataArea, 0);
	public FixedLengthStringData benpln = DD.benpln.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData taccamts = new FixedLengthStringData(24).isAPartOf(dataFields, 14);
	public ZonedDecimalData[] taccamt = ZDArrayPartOfStructure(2, 12, 2, taccamts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(taccamts, 0, FILLER_REDEFINE);
	public ZonedDecimalData taccamt1 = new ZonedDecimalData(12, 2).isAPartOf(filler, 0);
	public ZonedDecimalData taccamt2 = new ZonedDecimalData(12, 2).isAPartOf(filler, 12);
	public ZonedDecimalData tdeduct = DD.tdeduct.copyToZonedDecimal().isAPartOf(dataFields,38);
	public ZonedDecimalData zunit = DD.zunit.copyToZonedDecimal().isAPartOf(dataFields,50);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 52);
	public FixedLengthStringData benplnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData taccamtsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] taccamtErr = FLSArrayPartOfStructure(2, 4, taccamtsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(taccamtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData taccamt1Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData taccamt2Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData tdeductErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData zunitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 80);
	public FixedLengthStringData[] benplnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData taccamtsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] taccamtOut = FLSArrayPartOfStructure(2, 12, taccamtsOut, 0);
	public FixedLengthStringData[][] taccamtO = FLSDArrayPartOfArrayStructure(12, 1, taccamtOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(taccamtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] taccamt1Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] taccamt2Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] tdeductOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] zunitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(164);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(66).isAPartOf(subfileArea, 0);
	public ZonedDecimalData amtlife = DD.amtlife.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData amtyear = DD.amtyear.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public FixedLengthStringData desc = DD.desca.copy().isAPartOf(subfileFields,20);
	public FixedLengthStringData hosben = DD.hosben.copy().isAPartOf(subfileFields,45);
	public FixedLengthStringData limitc = DD.limitc.copy().isAPartOf(subfileFields,50);
	public ZonedDecimalData tactexp = DD.tactexp.copyToZonedDecimal().isAPartOf(subfileFields,54);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 66);
	public FixedLengthStringData amtlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData amtyearErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData descaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hosbenErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData limitcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData tactexpErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 90);
	public FixedLengthStringData[] amtlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] amtyearOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] descaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hosbenOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] limitcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] tactexpOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 162);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sr690screensflWritten = new LongData(0);
	public LongData Sr690screenctlWritten = new LongData(0);
	public LongData Sr690screenWritten = new LongData(0);
	public LongData Sr690protectWritten = new LongData(0);
	public GeneralTable sr690screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr690screensfl;
	}

	public Sr690ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(amtlifeOut,new String[] {null, null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(amtyearOut,new String[] {null, null, "-32",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {hosben, desc, limitc, amtlife, amtyear, tactexp};
		screenSflOutFields = new BaseData[][] {hosbenOut, descaOut, limitcOut, amtlifeOut, amtyearOut, tactexpOut};
		screenSflErrFields = new BaseData[] {hosbenErr, descaErr, limitcErr, amtlifeErr, amtyearErr, tactexpErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, crtable, benpln, zunit, tdeduct};
		screenOutFields = new BaseData[][] {chdrnumOut, crtableOut, benplnOut, zunitOut, tdeductOut};
		screenErrFields = new BaseData[] {chdrnumErr, crtableErr, benplnErr, zunitErr, tdeductErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr690screen.class;
		screenSflRecord = Sr690screensfl.class;
		screenCtlRecord = Sr690screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr690protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr690screenctl.lrec.pageSubfile);
	}
}
