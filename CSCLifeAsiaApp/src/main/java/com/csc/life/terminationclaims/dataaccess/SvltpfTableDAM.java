package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: SvltpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:32
 * Class transformed from SVLTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class SvltpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 721;
	public FixedLengthStringData svltrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData svltpfRecord = svltrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(svltrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(svltrec);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(svltrec);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(svltrec);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(svltrec);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(svltrec);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(svltrec);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(svltrec);
	public PackedDecimalData occdate = DD.occdate.copy().isAPartOf(svltrec);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(svltrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(svltrec);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(svltrec);
	public PackedDecimalData ptdate = DD.ptdate.copy().isAPartOf(svltrec);
	public FixedLengthStringData coverage01 = DD.coverage.copy().isAPartOf(svltrec);
	public FixedLengthStringData coverage02 = DD.coverage.copy().isAPartOf(svltrec);
	public FixedLengthStringData coverage03 = DD.coverage.copy().isAPartOf(svltrec);
	public FixedLengthStringData coverage04 = DD.coverage.copy().isAPartOf(svltrec);
	public FixedLengthStringData coverage05 = DD.coverage.copy().isAPartOf(svltrec);
	public FixedLengthStringData coverage06 = DD.coverage.copy().isAPartOf(svltrec);
	public FixedLengthStringData coverage07 = DD.coverage.copy().isAPartOf(svltrec);
	public FixedLengthStringData coverage08 = DD.coverage.copy().isAPartOf(svltrec);
	public FixedLengthStringData coverage09 = DD.coverage.copy().isAPartOf(svltrec);
	public FixedLengthStringData coverage10 = DD.coverage.copy().isAPartOf(svltrec);
	public FixedLengthStringData rider01 = DD.rider.copy().isAPartOf(svltrec);
	public FixedLengthStringData rider02 = DD.rider.copy().isAPartOf(svltrec);
	public FixedLengthStringData rider03 = DD.rider.copy().isAPartOf(svltrec);
	public FixedLengthStringData rider04 = DD.rider.copy().isAPartOf(svltrec);
	public FixedLengthStringData rider05 = DD.rider.copy().isAPartOf(svltrec);
	public FixedLengthStringData rider06 = DD.rider.copy().isAPartOf(svltrec);
	public FixedLengthStringData rider07 = DD.rider.copy().isAPartOf(svltrec);
	public FixedLengthStringData rider08 = DD.rider.copy().isAPartOf(svltrec);
	public FixedLengthStringData rider09 = DD.rider.copy().isAPartOf(svltrec);
	public FixedLengthStringData rider10 = DD.rider.copy().isAPartOf(svltrec);
	public FixedLengthStringData shortds01 = DD.shortds.copy().isAPartOf(svltrec);
	public FixedLengthStringData shortds02 = DD.shortds.copy().isAPartOf(svltrec);
	public FixedLengthStringData shortds03 = DD.shortds.copy().isAPartOf(svltrec);
	public FixedLengthStringData shortds04 = DD.shortds.copy().isAPartOf(svltrec);
	public FixedLengthStringData shortds05 = DD.shortds.copy().isAPartOf(svltrec);
	public FixedLengthStringData shortds06 = DD.shortds.copy().isAPartOf(svltrec);
	public FixedLengthStringData shortds07 = DD.shortds.copy().isAPartOf(svltrec);
	public FixedLengthStringData shortds08 = DD.shortds.copy().isAPartOf(svltrec);
	public FixedLengthStringData shortds09 = DD.shortds.copy().isAPartOf(svltrec);
	public FixedLengthStringData shortds10 = DD.shortds.copy().isAPartOf(svltrec);
	public FixedLengthStringData fieldType01 = DD.type.copy().isAPartOf(svltrec);
	public FixedLengthStringData fieldType02 = DD.type.copy().isAPartOf(svltrec);
	public FixedLengthStringData fieldType03 = DD.type.copy().isAPartOf(svltrec);
	public FixedLengthStringData fieldType04 = DD.type.copy().isAPartOf(svltrec);
	public FixedLengthStringData fieldType05 = DD.type.copy().isAPartOf(svltrec);
	public FixedLengthStringData fieldType06 = DD.type.copy().isAPartOf(svltrec);
	public FixedLengthStringData fieldType07 = DD.type.copy().isAPartOf(svltrec);
	public FixedLengthStringData fieldType08 = DD.type.copy().isAPartOf(svltrec);
	public FixedLengthStringData fieldType09 = DD.type.copy().isAPartOf(svltrec);
	public FixedLengthStringData fieldType10 = DD.type.copy().isAPartOf(svltrec);
	public FixedLengthStringData fund01 = DD.virtfund.copy().isAPartOf(svltrec);
	public FixedLengthStringData fund02 = DD.virtfund.copy().isAPartOf(svltrec);
	public FixedLengthStringData fund03 = DD.virtfund.copy().isAPartOf(svltrec);
	public FixedLengthStringData fund04 = DD.virtfund.copy().isAPartOf(svltrec);
	public FixedLengthStringData fund05 = DD.virtfund.copy().isAPartOf(svltrec);
	public FixedLengthStringData fund06 = DD.virtfund.copy().isAPartOf(svltrec);
	public FixedLengthStringData fund07 = DD.virtfund.copy().isAPartOf(svltrec);
	public FixedLengthStringData fund08 = DD.virtfund.copy().isAPartOf(svltrec);
	public FixedLengthStringData fund09 = DD.virtfund.copy().isAPartOf(svltrec);
	public FixedLengthStringData fund10 = DD.virtfund.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnstcur01 = DD.cnstcur.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnstcur02 = DD.cnstcur.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnstcur03 = DD.cnstcur.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnstcur04 = DD.cnstcur.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnstcur05 = DD.cnstcur.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnstcur06 = DD.cnstcur.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnstcur07 = DD.cnstcur.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnstcur08 = DD.cnstcur.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnstcur09 = DD.cnstcur.copy().isAPartOf(svltrec);
	public FixedLengthStringData cnstcur10 = DD.cnstcur.copy().isAPartOf(svltrec);
	public PackedDecimalData estMatValue01 = DD.emv.copy().isAPartOf(svltrec);
	public PackedDecimalData estMatValue02 = DD.emv.copy().isAPartOf(svltrec);
	public PackedDecimalData estMatValue03 = DD.emv.copy().isAPartOf(svltrec);
	public PackedDecimalData estMatValue04 = DD.emv.copy().isAPartOf(svltrec);
	public PackedDecimalData estMatValue05 = DD.emv.copy().isAPartOf(svltrec);
	public PackedDecimalData estMatValue06 = DD.emv.copy().isAPartOf(svltrec);
	public PackedDecimalData estMatValue07 = DD.emv.copy().isAPartOf(svltrec);
	public PackedDecimalData estMatValue08 = DD.emv.copy().isAPartOf(svltrec);
	public PackedDecimalData estMatValue09 = DD.emv.copy().isAPartOf(svltrec);
	public PackedDecimalData estMatValue10 = DD.emv.copy().isAPartOf(svltrec);
	public PackedDecimalData actvalue01 = DD.actvalue.copy().isAPartOf(svltrec);
	public PackedDecimalData actvalue02 = DD.actvalue.copy().isAPartOf(svltrec);
	public PackedDecimalData actvalue03 = DD.actvalue.copy().isAPartOf(svltrec);
	public PackedDecimalData actvalue04 = DD.actvalue.copy().isAPartOf(svltrec);
	public PackedDecimalData actvalue05 = DD.actvalue.copy().isAPartOf(svltrec);
	public PackedDecimalData actvalue06 = DD.actvalue.copy().isAPartOf(svltrec);
	public PackedDecimalData actvalue07 = DD.actvalue.copy().isAPartOf(svltrec);
	public PackedDecimalData actvalue08 = DD.actvalue.copy().isAPartOf(svltrec);
	public PackedDecimalData actvalue09 = DD.actvalue.copy().isAPartOf(svltrec);
	public PackedDecimalData actvalue10 = DD.actvalue.copy().isAPartOf(svltrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(svltrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(svltrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(svltrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(svltrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(svltrec);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(svltrec);
	public PackedDecimalData policyloan = DD.policyloan.copy().isAPartOf(svltrec);
	public PackedDecimalData otheradjst = DD.otheradjst.copy().isAPartOf(svltrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(svltrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(svltrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(svltrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public SvltpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for SvltpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public SvltpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for SvltpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public SvltpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for SvltpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public SvltpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("SVLTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"CNTTYPE, " +
							"COWNNUM, " +
							"CTYPEDES, " +
							"JLIFCNUM, " +
							"JLINSNAME, " +
							"LIFCNUM, " +
							"LINSNAME, " +
							"OCCDATE, " +
							"OWNERNAME, " +
							"PLNSFX, " +
							"PSTATE, " +
							"PTDATE, " +
							"COVERAGE01, " +
							"COVERAGE02, " +
							"COVERAGE03, " +
							"COVERAGE04, " +
							"COVERAGE05, " +
							"COVERAGE06, " +
							"COVERAGE07, " +
							"COVERAGE08, " +
							"COVERAGE09, " +
							"COVERAGE10, " +
							"RIDER01, " +
							"RIDER02, " +
							"RIDER03, " +
							"RIDER04, " +
							"RIDER05, " +
							"RIDER06, " +
							"RIDER07, " +
							"RIDER08, " +
							"RIDER09, " +
							"RIDER10, " +
							"SHORTDS01, " +
							"SHORTDS02, " +
							"SHORTDS03, " +
							"SHORTDS04, " +
							"SHORTDS05, " +
							"SHORTDS06, " +
							"SHORTDS07, " +
							"SHORTDS08, " +
							"SHORTDS09, " +
							"SHORTDS10, " +
							"TYPE01, " +
							"TYPE02, " +
							"TYPE03, " +
							"TYPE04, " +
							"TYPE05, " +
							"TYPE06, " +
							"TYPE07, " +
							"TYPE08, " +
							"TYPE09, " +
							"TYPE10, " +
							"VIRTFUND01, " +
							"VIRTFUND02, " +
							"VIRTFUND03, " +
							"VIRTFUND04, " +
							"VIRTFUND05, " +
							"VIRTFUND06, " +
							"VIRTFUND07, " +
							"VIRTFUND08, " +
							"VIRTFUND09, " +
							"VIRTFUND10, " +
							"CNSTCUR01, " +
							"CNSTCUR02, " +
							"CNSTCUR03, " +
							"CNSTCUR04, " +
							"CNSTCUR05, " +
							"CNSTCUR06, " +
							"CNSTCUR07, " +
							"CNSTCUR08, " +
							"CNSTCUR09, " +
							"CNSTCUR10, " +
							"EMV01, " +
							"EMV02, " +
							"EMV03, " +
							"EMV04, " +
							"EMV05, " +
							"EMV06, " +
							"EMV07, " +
							"EMV08, " +
							"EMV09, " +
							"EMV10, " +
							"ACTVALUE01, " +
							"ACTVALUE02, " +
							"ACTVALUE03, " +
							"ACTVALUE04, " +
							"ACTVALUE05, " +
							"ACTVALUE06, " +
							"ACTVALUE07, " +
							"ACTVALUE08, " +
							"ACTVALUE09, " +
							"ACTVALUE10, " +
							"VALIDFLAG, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"RSTATE, " +
							"POLICYLOAN, " +
							"OTHERADJST, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     cnttype,
                                     cownnum,
                                     ctypedes,
                                     jlifcnum,
                                     jlinsname,
                                     lifcnum,
                                     linsname,
                                     occdate,
                                     ownername,
                                     planSuffix,
                                     pstate,
                                     ptdate,
                                     coverage01,
                                     coverage02,
                                     coverage03,
                                     coverage04,
                                     coverage05,
                                     coverage06,
                                     coverage07,
                                     coverage08,
                                     coverage09,
                                     coverage10,
                                     rider01,
                                     rider02,
                                     rider03,
                                     rider04,
                                     rider05,
                                     rider06,
                                     rider07,
                                     rider08,
                                     rider09,
                                     rider10,
                                     shortds01,
                                     shortds02,
                                     shortds03,
                                     shortds04,
                                     shortds05,
                                     shortds06,
                                     shortds07,
                                     shortds08,
                                     shortds09,
                                     shortds10,
                                     fieldType01,
                                     fieldType02,
                                     fieldType03,
                                     fieldType04,
                                     fieldType05,
                                     fieldType06,
                                     fieldType07,
                                     fieldType08,
                                     fieldType09,
                                     fieldType10,
                                     fund01,
                                     fund02,
                                     fund03,
                                     fund04,
                                     fund05,
                                     fund06,
                                     fund07,
                                     fund08,
                                     fund09,
                                     fund10,
                                     cnstcur01,
                                     cnstcur02,
                                     cnstcur03,
                                     cnstcur04,
                                     cnstcur05,
                                     cnstcur06,
                                     cnstcur07,
                                     cnstcur08,
                                     cnstcur09,
                                     cnstcur10,
                                     estMatValue01,
                                     estMatValue02,
                                     estMatValue03,
                                     estMatValue04,
                                     estMatValue05,
                                     estMatValue06,
                                     estMatValue07,
                                     estMatValue08,
                                     estMatValue09,
                                     estMatValue10,
                                     actvalue01,
                                     actvalue02,
                                     actvalue03,
                                     actvalue04,
                                     actvalue05,
                                     actvalue06,
                                     actvalue07,
                                     actvalue08,
                                     actvalue09,
                                     actvalue10,
                                     validflag,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     rstate,
                                     policyloan,
                                     otheradjst,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		cnttype.clear();
  		cownnum.clear();
  		ctypedes.clear();
  		jlifcnum.clear();
  		jlinsname.clear();
  		lifcnum.clear();
  		linsname.clear();
  		occdate.clear();
  		ownername.clear();
  		planSuffix.clear();
  		pstate.clear();
  		ptdate.clear();
  		coverage01.clear();
  		coverage02.clear();
  		coverage03.clear();
  		coverage04.clear();
  		coverage05.clear();
  		coverage06.clear();
  		coverage07.clear();
  		coverage08.clear();
  		coverage09.clear();
  		coverage10.clear();
  		rider01.clear();
  		rider02.clear();
  		rider03.clear();
  		rider04.clear();
  		rider05.clear();
  		rider06.clear();
  		rider07.clear();
  		rider08.clear();
  		rider09.clear();
  		rider10.clear();
  		shortds01.clear();
  		shortds02.clear();
  		shortds03.clear();
  		shortds04.clear();
  		shortds05.clear();
  		shortds06.clear();
  		shortds07.clear();
  		shortds08.clear();
  		shortds09.clear();
  		shortds10.clear();
  		fieldType01.clear();
  		fieldType02.clear();
  		fieldType03.clear();
  		fieldType04.clear();
  		fieldType05.clear();
  		fieldType06.clear();
  		fieldType07.clear();
  		fieldType08.clear();
  		fieldType09.clear();
  		fieldType10.clear();
  		fund01.clear();
  		fund02.clear();
  		fund03.clear();
  		fund04.clear();
  		fund05.clear();
  		fund06.clear();
  		fund07.clear();
  		fund08.clear();
  		fund09.clear();
  		fund10.clear();
  		cnstcur01.clear();
  		cnstcur02.clear();
  		cnstcur03.clear();
  		cnstcur04.clear();
  		cnstcur05.clear();
  		cnstcur06.clear();
  		cnstcur07.clear();
  		cnstcur08.clear();
  		cnstcur09.clear();
  		cnstcur10.clear();
  		estMatValue01.clear();
  		estMatValue02.clear();
  		estMatValue03.clear();
  		estMatValue04.clear();
  		estMatValue05.clear();
  		estMatValue06.clear();
  		estMatValue07.clear();
  		estMatValue08.clear();
  		estMatValue09.clear();
  		estMatValue10.clear();
  		actvalue01.clear();
  		actvalue02.clear();
  		actvalue03.clear();
  		actvalue04.clear();
  		actvalue05.clear();
  		actvalue06.clear();
  		actvalue07.clear();
  		actvalue08.clear();
  		actvalue09.clear();
  		actvalue10.clear();
  		validflag.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		rstate.clear();
  		policyloan.clear();
  		otheradjst.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getSvltrec() {
  		return svltrec;
	}

	public FixedLengthStringData getSvltpfRecord() {
  		return svltpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setSvltrec(what);
	}

	public void setSvltrec(Object what) {
  		this.svltrec.set(what);
	}

	public void setSvltpfRecord(Object what) {
  		this.svltpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(svltrec.getLength());
		result.set(svltrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}