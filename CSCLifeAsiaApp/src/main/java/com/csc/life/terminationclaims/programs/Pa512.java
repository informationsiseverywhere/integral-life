/**
 * Pa512 —— Claim Notification Enquiry
 */
package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.smart.procedures.Genssw;
import com.quipoz.framework.datatype.BaseScreenData;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.dao.NohspfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Nohspf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.life.terminationclaims.screens.Sa512ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.dao.CrelpfDAO;
import com.csc.fsu.clients.dataaccess.model.Crelpf;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The details of  the  contract  being  enquired  upon  will be
*     stored in the  CHDRENQ  I/O  module. Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*     The PAYR file is read for the contract,followed by T3620.
*     If the contract requires bank details(i.e Direct Debit with
****  T3620-DDIND NOT = SPACES),then the Mandate file is read
****  to obtain the Mandate detalis.
*     T3620-DDIND NOT = SPACES or Credit Card with T3620-CRCIND
*     NOT = SPACES),then the Mandate file is read to obtain the
*     Mandate detalis.
*
*     Set up the  Factoring  House, (CHDR-FACTHOUS), on the screen.
*     Valid Factoring House  codes  are held on T3684. (See GETDESC
*     for an example of how to obtain the long description from the
*     DESC data-set. Do  not  call  GETDESC,  the  appropriate code
*     should be moved into the mainline.
*
*     Set up the  Bank  Code,  (CHDR-BANKKEY),  on  the  screen and
*     obtain the  corresponding  description  from the Bank Details
*     data-set BABR.  Read  BABR  with  a  key  of CHDR-BANKKEY and
*     obtain BABR-BANKDESC.
*
*     Set up the  Account  Number, (CHDR-BANKACCKEY), on the screen
*     and  obtain  the  correeponding  description  from  the  CLBL
*     data-set.  Read  CLBL   with   a   key  of  CHDR-BANKKEY  and
*     CHDR-BANKACCKEY and obtain CLBL-BANKADDDSC and CLBL-CURRCODE.
*
*     If the Assignee client, (CHDR-ASGNNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the usual way.
*
*     If the Despatch client, (CHDR-DESPNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the ususal way.
*
*
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The only validation  required  is  of  the  indicator field -
*     CONBEN. This may be space or 'X'.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was  pressed  move  spaces  to  the current program
*     position in the program stack and exit.
*
*     If returning from  processing  a  selection  further down the
*     program stack, (current  stack  action  field  will  be '*'),
*     restore the next  8  programs  from  the  WSSP and remove the
*     asterisk.
*
*     If nothing was  selected move '*' to the current stack action
*     field, add 1 to the program pointer and exit.
*
*     If a selection has  been found use GENSWCH to locate the next
*     program(s)  to process  the  selection,  (up  to  8).  Use  a
*     function of 'A'. Save  the  next  8 programs in the stack and
*     replace them with  the  ones  returned from GENSWCH. Place an
*     asterisk in the  current  stack  action  field,  add 1 to the
*     program pointer and exit.
*
*
* Notes.
* ------
*
*     Tables Used:
*
* T1692 - Branch Codes                      Key: Branch Code
* T3588 - Contract Premium Status           Key: PSTATCODE
* T3623 - Contract Risk Status              Key: STATCODE
* T5688 - Contract Structure                Key: CNTTYPE
* T3684 - Factoring House                   Key: FACTHOUS
* T3620 - Billing Channel                   Key: BILLCHNL
*
*
*****************************************************************
* </pre>
*/
public class Pa512 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final String CLMPREFIX = "CLMNTF";
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pa512");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	
	private Chdrpf chdrpf = new Chdrpf();

	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sa512ScreenVars sv = getPScreenVars() ;
	private FormatsInner formatsInner = new FormatsInner();
	private Clntpf clntpf;
	private Clexpf clexpf;
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	
	private NohspfDAO nohspfDAO = getApplicationContext().getBean("nohspfDAO", NohspfDAO.class);
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ClexpfDAO clexpfDAO = getApplicationContext().getBean("clexpfDAO", ClexpfDAO.class);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private CrelpfDAO crelpfDAO = getApplicationContext().getBean("crelpfDAO", CrelpfDAO.class);
	private Crelpf crelpf = null;	
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private FixedLengthStringData wsaaGenkey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaGenkey, 2);
	private Fluppf fluppf = null ;
	protected static final String h093 = "H093";
	private Batckey wsaaBatchkey = new Batckey();
	private Subprogrec subprogrec = new Subprogrec();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	protected static final String chdrenqrec = "CHDRENQREC";
	protected ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Notipf notipf=new Notipf();
	private Nohspf nohspf=new Nohspf();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator planLevel = new Validator(wsaaPlanproc, "Y");
	 protected static final String e186 = "E186";
	 String inctype="DC";
	 String inctype1="RP";
	 String rgpytype="HS";
	 private int count=0;
	 private String notifistatus="Open";
	 private String wsaaChdrnum=null;
	 private Alocnorec alocnorec = new Alocnorec();
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2090,
		exit3090
	}

	public Pa512() {
		super();
		screenVars = sv;
		new ScreenModel("Sa512", AppVars.getInstance(), sv);
	}

	protected Sa512ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sa512ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			e.printStackTrace();
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
			e.printStackTrace();
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		
		initialise1010();
		setscreen();
	}

private void setscreen() {
		sv.cltreln.setEnabled(BaseScreenData.ENABLED);
		sv.clntsel.setEnabled(BaseScreenData.ENABLED);
	}



protected void initialise1010()
	{
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*      GO TO 1090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		//auto genarate Notifinum number
		allocateNumber2600();
		sv.notifiDate.set(wsaaToday);
		getLifeAssSection();
		sv.cltdodxDisp.set(SPACES);
		sv.incurdtDisp.set(SPACES);
		sv.admiDateDisp.set(SPACES);
		sv.dischargeDateDisp.set(SPACES);
	}
	

/**
 * <pre>
 *    Used to be 3200-ALLOCATE-NUMBER !!!
 * </pre>
 */
protected void allocateNumber2600()
{
	alocnorec.function.set("NEXT");
	alocnorec.prefix.set("NF");
	alocnorec.company.set(wsspcomn.company);
	wsaaBranch.set("");
	alocnorec.genkey.set(wsaaGenkey);
	callProgram(Alocno.class, alocnorec.alocnoRec);
	if (isEQ(alocnorec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(alocnorec.statuz);
		fatalError600();
	}
	if (isNE(alocnorec.statuz, varcom.oK)) {
		sv.notifinumErr.set(alocnorec.statuz);
	}
	else {
		
		sv.notifinum.set(CLMPREFIX+alocnorec.alocNo);
		//put Notifinum number into cache
		wsspcomn.chdrChdrnum.set(alocnorec.alocNo);
	}
}
	
	private void getClaimantSection() {
		clexpf=clexpfDAO.getClexpfByClntkey("CN", wsspcomn.fsuco.toString(), sv.clntsel.toString());
		if(clexpf!=null){
			sv.rinternet.set(clexpf.getRinternet());
			if(isEQ(sv.cltphoneidd, SPACES)||sv.cltphoneidd.getFormData().trim()==""||sv.cltphoneidd.getFormData()==null){
				sv.cltphoneidd.set(clexpf.getRmblphone());
			}
		}
		
		clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), sv.clntsel.toString());
		if(clntpf!=null){
			sv.cidtype.set(clntpf.getIdtype());
			sv.csecuityno.set(clntpf.getSecuityno());
			sv.ccltsex.set(clntpf.getCltsex());
			sv.cltaddr01.set(clntpf.getCltaddr01());
			sv.cltaddr02.set(clntpf.getCltaddr02());
			sv.cltaddr03.set(clntpf.getCltaddr03());
			sv.cltaddr04.set(clntpf.getCltaddr04());
			sv.cltaddr05.set(clntpf.getCltaddr05());
			sv.cltpcode.set(clntpf.getCltpcode());
			sv.cltname.set(clntpf.getSurname().trim()+","+clntpf.getGivname().trim());
			if(isEQ(sv.cltphoneidd, SPACES)||sv.cltphoneidd.getFormData().trim()==""||sv.cltphoneidd.getFormData()==null){
				sv.cltphoneidd.set(clntpf.getCltphone02());
			}
			if(isEQ(sv.cltphoneidd, SPACES)||sv.cltphoneidd.getFormData().trim()==""||sv.cltphoneidd.getFormData()==null){
				sv.cltphoneidd.set(clntpf.getCltphone01());
			}
			
		}
	
}

	private void getLifeAssSection() {
		try{
		clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), wsspcomn.chdrCownnum.toString());
		if(clntpf!=null){
			sv.lifcnum.set(wsspcomn.chdrCownnum);
			sv.lifename.set((clntpf.getSurname()!= null?clntpf.getSurname().trim(): " ")+","+(clntpf.getGivname()!= null?clntpf.getGivname().trim(): " "));/* IJTI-1523 */
			sv.lidtype.set(clntpf.getIdtype());
			sv.lsecuityno.set(clntpf.getSecuityno());
			//Get Gender value
			sv.lcltsex.set(clntpf.getCltsex());
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}


protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* IF   WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                  */
		/*      MOVE O-K               TO WSSP-EDTERROR                 */
		/*      GO TO 2090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	
	//CHECK-FOR-ERRORS
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		//EXIT	
		
	}

	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			//goTo(GotoLabel.exit2090);
			return;
		}
		wsspcomn.edterror.set(varcom.oK);
		/*    Catering for F11.                                    <V4L011>*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			//goTo(GotoLabel.exit2090);
			return;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			/*       PERFORM 2100-OPTSWCH-CHCK                         <V4L011>*/
			/*       PERFORM 2200-CHECK-SCROLLING                              */
			//goTo(GotoLabel.exit2090);
			return;
		}	
		
	}

protected void validate2020()
	{
	 if (isEQ(scrnparams.statuz, varcom.kill)) {
         return;
     }
     if (isEQ(wsspcomn.flag, "I")) {
         return;
     }
     if (isEQ(scrnparams.statuz, varcom.calc)) {
         wsspcomn.edterror.set("Y");
     }
	getClaimantSection();
	getRelationShip();
	if(sv.incurdtDisp.equals(SPACES)||sv.incurdtDisp==null){
   	 sv.incurdtErr.set(e186);
			return ;
	}
	
	if(sv.inctype.equals(SPACES)||sv.inctype==null){
	   	 sv.inctypeErr.set(e186);
				return ;
		}
	if(sv.inctype.getData().trim().equals(inctype)){
		if(isEQ(sv.cltdodxDisp, SPACES)){
	    	sv.cltdodxErr.set(e186);
	    	return ;
	    }else{
	    	if(isEQ(sv.causedeath, SPACES)){
	    		sv.causedeathErr.set(e186);
	    		return ;
	    	}
	    }
		
	}else if(sv.inctype.getData().trim().equals(inctype1)){
		if (isEQ(sv.rgpytype, SPACES)) {
			sv.rgpytypeErr.set(e186);
			return ;
		}
	}
	if(sv.rgpytype.getData().trim().equals(rgpytype)){
		if(isEQ(sv.hospitalLevel, SPACES)){
			sv.hospitalLevelErr.set(e186);
			return ;
		}else{
			if(isEQ(sv.diagcde, SPACES)){
				sv.diagcdeErr.set(e186);
				return ;
			}else{
				if(isEQ(sv.admiDateDisp, SPACES)){
					sv.admiDateErr.set(e186);
					return ;
				}else{
					
					if(isEQ(sv.dischargeDateDisp, SPACES)){
						sv.dischargeDateErr.set(e186);
						return ;
					}	
				}
			}
		}
			
	}
	}

	private void getRelationShip() {
		
		crelpf = crelpfDAO.getMcrlByCoyAndNum(wsspcomn.fsuco.toString(), wsspcomn.chdrCownnum.toString(), wsspcomn.fsuco.toString(), sv.clntsel.toString());
		if (crelpf!=null && isEQ(sv.cltreln, SPACES)) {
			sv.cltreln.set(crelpf.getCltreln());
			return ;
		}
	
}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
	if (isEQ(scrnparams.statuz,varcom.kill)) {
		//goTo(GotoLabel.exit3090);
		throw new GOTOException(GotoLabel.exit3090); 
	}
	//updateNotipf();
	//updateNohspf();
	//insert nochpf
	String chdrNumStr = wsspcomn.chdrNumList.getData();
	ArrayList<String> chdrnumList=new ArrayList(Arrays.asList(chdrNumStr.split(",")));
	count = chdrnumList.size()-1;
	wsaaChdrnum = chdrnumList.get(count).trim();
	updateWssp3010();
	return ;	
	}



	
protected void updateWssp3010()
{
	wsspcomn.sbmaction.set(scrnparams.action);
	wsaaBatchkey.set(wsspcomn.batchkey);
	wsspcomn.batchkey.set(wsaaBatchkey);
	
	if (isEQ(scrnparams.statuz, "BACH")) {
		goTo(GotoLabel.exit3090);
	}
}



protected void keeps3070()
{
	/*   Store the contract header for use by the transaction programs*/
//ILB-459 start
	chdrpfDAO.setCacheObject(chdrpf);
	//ILB-459 end
}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
{
	nextProgram4010();
	/*NEXT-PROGRAM
	wsspcomn.programPtr.add(1);
	EXIT*/
}

protected void wayout4800()
{
	/*PROGRAM-EXIT*/
	/* If control is passed to this  part of the 4000 section on the*/
	/*   way out of the program, i.e. after  screen  I/O,  then  the*/
	/*   current stack position action  flag will be  blank.  If the*/
	/*   4000-section  is  being   performed  after  returning  from*/
	/*   processing another program then  the current stack position*/
	/*   action flag will be '*'.*/
	/* If 'KILL' has been requested, (CF11), then move spaces to the*/
	/*   current program entry in the program stack and exit.*/
	if (isEQ(scrnparams.statuz, "KILL")) {
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		return ;
	}
	/* Add 1 to the program pointer and exit.*/
	wsspcomn.programPtr.add(1);
	/*EXIT*/
}

protected void nextProgram4010()
{
	gensswrec.company.set(wsspcomn.company);
	gensswrec.progIn.set(wsaaProg);
	gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
	wsspcomn.nextprog.set(wsaaProg);
	
	//*    If returning from a program further down the stack then*/
		/*    first restore the original programs in the program stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
						wsaaX.set(wsspcomn.programPtr);
						wsaaY.set(1);
						for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
							saveProgram4100();
						}
					} 
			 

	if (isEQ(sv.fupflg, "?")) {
		checkFollowups1850();
	}
	if (isEQ(sv.fupflg, "X")) {
		gensswrec.function.set("A");
		sv.fupflg.set("?");
		callGenssw4300();
		return ;
	}
	if (isEQ(sv.invresults, "X")) {
		gensswrec.function.set("B");
		sv.invresults.set("?");
		callGenssw4300();
		return ;
	}
	if (isEQ(sv.notifnotes, "X")) {
		gensswrec.function.set("C");
		sv.notifnotes.set("?");
		callGenssw4300();
		return ;
	}
	/*  Check if beneficiary selected previously*/
	
	/*   No more selected (or none)*/
	/*      - restore stack form wsaa to wssp*/
	wsaaX.set(wsspcomn.programPtr);
	wsaaY.set(1);
	for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
		restoreProgram4200();
	}
	
	/* If we are processing a Whole Plan selection then we must*/
	/*   process all Policies for the selected Coverage/Rider,*/
	/*   Looping back to the 2000-section after having loaded the*/
	/*   screen for the next Policy*/
	/* Else we exit to the next program in the stack.*/
	if (planLevel.isTrue()) {
		wsspcomn.nextprog.set(scrnparams.scrname);
	}
	else {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wayout4800();
	}
}

protected void saveProgram4100()
{
	/*SAVE*/
	wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
} 
protected void restoreProgram4100()
{
	/*PARA*/
	wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
	sub1.add(1);
	sub2.add(1);
	/*EXIT*/
}

protected void callGenssw4300()
{
	callSubroutine4310();
}

protected void callSubroutine4310()
{
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)
	&& isNE(gensswrec.statuz, varcom.mrnf)) {
		syserrrec.params.set(gensswrec.gensswRec);
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	/* If an entry on T1675 was not found by genswch redisplay the scre*/
	/* with an error and the options and extras indicator*/
	/* with its initial load value*/
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*     MOVE V045                TO SCRN-ERROR-CODE               */
		scrnparams.errorCode.set(h093);//ILB-459
		wsspcomn.nextprog.set(scrnparams.scrname);
		return ;
	}
	/*    load from gensw to wssp*/
	compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
	wsaaY.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		loadProgram4400();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
}
protected void checkFollowups1850() {
	//ILIFE-6296 by wli31
	String chdrnum = (chdrpf.getChdrnum()!= null?chdrpf.getChdrnum(): " ");//ILB-459
	String chdrcoy = (chdrpf.getChdrcoy()!=null?chdrpf.getChdrcoy().toString():" ");//ILB-459
	boolean recFound = fluppfDAO.checkFluppfRecordByChdrnum(chdrcoy, chdrnum);
	if (!recFound) {
		//fatalError600();
		sv.fupflg.set(SPACES);
	} else {
		sv.fupflg.set("+");
	}

}
protected void restoreProgram4200()
{
	/*RESTORE*/
	wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
}

protected void loadProgram4400()
{
	/*RESTORE1*/
	wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT1*/
}
public Chdrpf getChdrpf() {
		return chdrpf;
	}

	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
    private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
}

	protected void prepareName(String firstName, String lastName) {
		String fullName = "";
		if (isNE(firstName, SPACES)) {
			fullName = getStringUtil().plainName(firstName, lastName, ",");
			 
		} else {
			fullName = lastName;
		}
		
		wsspcomn.longconfname.set(fullName);

	}

	/**
	 * can be overriden to change the its behaviour
	 * @return
	 */
	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}
	
	public Subprogrec getSubprogrec() {
		return subprogrec;
	}
	public void setSubprogrec(Subprogrec subprogrec) {
		this.subprogrec = subprogrec;
	}
	
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 
	
	}

}
