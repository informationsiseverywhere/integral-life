/*
 * File: Ph580.java
 * Date: 30 August 2009 1:08:59
 * Author: Quipoz Limited
 * 
 * Class transformed from PH580.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.utility.Datcon1;
import java.util.LinkedList;
import java.util.List;
import java.math.BigDecimal;
import java.util.ArrayList;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;//ILIFE-7968
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.procedures.Hrtotlon;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HsudTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.tablestructures.T6632rec;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Regppf;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.recordstructures.Regpsubrec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.tablestructures.T6696rec;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.PyoupfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RegppfDAO;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILIFE-7968
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.life.terminationclaims.dataaccess.model.Pyoupf;

import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.screens.Sh580ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Td5h6rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
/* BSD-POS009 Start */
import com.csc.smart.procedures.ParmsForVPMS;
import com.csc.life.productdefinition.procedures.Vpxsutd;
import com.csc.life.productdefinition.recordstructures.Vpxsutdrec;
/* BSD-POS009 End */
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Traditional Part Surrender
*
* After a component is selected from P6351, this program will
* call the surrender calculation routines from T6598 and display
* the surrender value details for the component and any bonuses
* attached to the component.  It will calculate a new premium
* based on the decreased sum assured by calling the appropriate
* premium calculation routine on T5675.  A COVT record will be
* created with the new sum assured and premium details to be
* processed later by P5132AT, the component change AT module
* (which will adjust commission and reassurance and write a new
* COVR record based on the COVT details).
*
* Inputs
* ------
*    COVRMJA - Coverage/Rider details logical file
*    CHDRMJA - Contract header details logical file
*    PAYR    - Payor details logical file
*
* Outputs
* -------
*    COVTMJA    - Coverage/Rider temporary details
*    SRCALCPY   - Surrender calculation linkage copybook
*    PREMIUMREC - Premium calculation linkage copybook
*    SURHCLM    - Surrender header logical file
*    SURDCLM    - Surrender details logical file
*    PCDTMJA    - Commission Split Details file
*
* 1000-initialise section
* -----------------------
* - Retrieve CHDRMJA, COVRMJA.
* - Release COVRMJA.
* - Read PAYR, LIFEMJA.
* - Initialise screen fields.
* - Clear the subfile.
* - Set subfile RRN to 1.
* - Set up header & footer fields from CHDRMJA, COVRMJA, LIFEMJA.
*     . Contract number, type, RCD, Owner number, Paid-to-date,
*       bill-to-date from CHDRMJA.
*     . Life number, Joint life number from LIFEMJA.
*     . Component code, sum insured, instalment premium from
*       COVRMJA.
*     . Contract type description from T5688.
*     . Contract status from T3623.
*     . Premium statuz from T3588.
*     . Owner name, life name, joint life name from CLTS,
*       format using PLAINNAME.
* - Check if SURHCLM exist, if not, set flag to first time.
* - Read T5687 using component code as key, obtain surrender
*   method and premium calculation method.
* - Read T6598 using T5687- surrender method as key, obtain
*   calculation routine.
* - Set up linkage for surrender calculation routine.
* - Set up subfile fields .
*     . call T6598-calcprog until ENDP.
*          * set up subfile fields from SURC-
*              Type = SURC-TYPE
*              Description = SURC-DESCRIPTION
*              Currency = SURC-CURRCODE if exist, else set to
*                         CHDRMJA-CNTCURR
*              Surrender value = SURC-ACTVALUE
*          * add to subfile
* - Set up COVTMJA fields.
* - Read T5675 for premium calculation subroutine.
*
* 2000-screen-edit section.
* -------------------------
* - set wssp-edterror to o-k.
* - set scrn-subfile-rrn to 1.
* - call screen io.
* - exit section if scrn-statuz = calc.
* - validate that new sum assured must be less than existing
*   sum assured.
* - validate that new sum assured must not be zero.
* - calculate new instalment premium:
*     . set up linkage PREMIUMREC.
*     . call T5675-premsubr.
*     . new instalment premium = CPRM-CALC-PREM.
* - compute proportion of new and existing sum assured:
*      new sum assured / existing sum assured
* - compute surrender amount:
*      existing sum assured - new sum assured
* - compute total: surrender amount + adjustments
* - recalculate surrender value for each subfile record:
*     . for each subfile record, compute the new surrender value
*       as: existing surrender value * proportion.
*
* 3000-update section.
* --------------------
* - for each subfile record, write a corresponding SURDCLM record.
* - if first time, write a SURHCLM record.
* - write a COVTMJA record.
*
* 4000-where-next section.
* ------------------------
* - add 1 to wssp-program-ptr.
*
****************************************************************** ****
* </pre>
*/
public class Ph580 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH580");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaProportion = new ZonedDecimalData(8, 7);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
		/* WSAA-LIFE-DETAILS */
	private FixedLengthStringData wsaaLifeSex = new FixedLengthStringData(1);
	private PackedDecimalData wsaaLifeAnbccd = new PackedDecimalData(3, 0).init(0);
		/* WSAA-JLIFE-DETAILS */
	private FixedLengthStringData wsaaJlifeSex = new FixedLengthStringData(1);
	private PackedDecimalData wsaaJlifeAnbccd = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaNoncashvalue = new FixedLengthStringData(1);
	
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private FixedLengthStringData wsaaPremMeth= new FixedLengthStringData(4);
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);
	
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private PackedDecimalData wsaaTotalCashVal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRemCashVal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompTotal = new PackedDecimalData(17, 2);
	/*private ZonedDecimalData wsaaPrevSumins = new ZonedDecimalData(8, 7);*/
	private PackedDecimalData wsaaPrevSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaKeepSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegpynum = new PackedDecimalData(5, 0);
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5675 = "T5675";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t6598 = "T6598";
	private static final String t6632 = "T6632";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HsudTableDAM hsudIO = new HsudTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Freqcpy freqcpy = new Freqcpy();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Premiumrec premiumrec = new Premiumrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T6632rec t6632rec = new T6632rec();
	private Sh580ScreenVars sv = ScreenProgram.getScreenVars( Sh580ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private ExternalisedRules er = new ExternalisedRules();
	//ICIL-254
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private BabrpfDAO babrpfDao = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class);
	private PyoupfDAO pyoupfDAO = getApplicationContext().getBean("pyoupfDAO", PyoupfDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);	
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private Clbapf clbapf;
	private BabrTableDAM babrIO = new BabrTableDAM();
	private Pyoupf pyoupf = new Pyoupf();/*ICIL-254*/
	private Td5h6rec td5h6rec = new Td5h6rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaBankdesc = new FixedLengthStringData(30).isAPartOf(wsaaBankkey, 0);
	
	private static final String td5h6 = "TD5H6";
	private static final String Batctrcde = "TA83";
	boolean susur002Permission = false; 
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Itempf itempf = null;
	private Mandpf mandpf = null;
        private RegpTableDAM regpIO = new RegpTableDAM();
	private static final String regprec = "REGPREC";
	private List<Itempf> itempfList;
	private Babrpf babrpf = null;
	private int len = 0;
	private StringBuffer banktemp = null;
		private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private FixedLengthStringData wsaaT6696Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6696Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6696Key, 0);
	private RegppfDAO regpDAO = getApplicationContext().getBean("regppfDAO", RegppfDAO.class);
	private FixedLengthStringData wsaaT6696Cltype = new FixedLengthStringData(2).isAPartOf(wsaaT6696Key, 4);
	private T6696rec t6696rec = new T6696rec();
	private Regpsubrec regpsubrec = new Regpsubrec();
	private Regppf regppf = null;
	private Batckey wsaaBatckey = new Batckey();
	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);
	private List<Itempf> itempfList1;
	private List<Regppf> regppfList;
	private List<Regppf> regpList;
	private boolean regpayFlag = false;
	private final String wsaaLargeName = "LGNMS";
	/* BSD-POS009 Start */
	private ParmsForVPMS parmsForVPMS = getApplicationContext().getBean("parmsForVPMS", ParmsForVPMS.class);
	private String[] suTypeList = null; /* store all the surrender type */
	private String suTypeProportion = null; /* store all the Proportion type */
	private int i = 0;
	private String suEndType = null; 	/* the last surrender type */
	/* BSD-POS009 End */
	
	//ILIFE-7968 start
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	//ILIFE-7968 end
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	private CovrpfDAO covrpfDAO= getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next1580, 
		exit1590
	}

	public Ph580() {
		super();
		screenVars = sv;
		new ScreenModel("Sh580", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
 	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try{
		initialise1010();
		}catch(Exception e){
			e.printStackTrace();
		}
		if(susur002Permission) {
		 initpayout1020();
		}
	}

protected void initialise1010() throws Exception
	{
		/*  Skip this section if returning from an optional screen.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		
		regpayFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSCOM006", appVars, "IT");
		susur002Permission = FeaConfg.isFeatureExist("2", "SUSUR002", appVars, "IT");
		if(susur002Permission) {
			wsaaNoncashvalue.set("N");
		}
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaLifeAnbccd.set(ZERO);
		wsaaJlifeAnbccd.set(ZERO);
		/*  Retrieve CHDRMJA.*/
		//ILIFE-7968 start
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());//ILIFE-7991
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
				else {
					covrpfDAO.setCacheObject(covrpf);
				}
			}
		}
		/*  Read Payor details*/
		payrIO.setDataKey(SPACES);
		payrIO.setChdrcoy(chdrpf.getChdrcoy().toString());//ILIFE-7968
		payrIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/*  Read life details.*/
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(chdrpf.getChdrcoy().toString());//ILIFE-7968
		lifemjaIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		//ILIFE-7968
		if (isEQ(lifemjaIO.getStatuz(), varcom.endp)
		|| isNE(lifemjaIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(lifemjaIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(lifemjaIO.getLife(), covrpf.getLife())
		|| (isNE(lifemjaIO.getJlife(), "00")
		&& isNE(lifemjaIO.getJlife(), " "))) {
			syserrrec.statuz.set(errorsInner.h143);
			lifemjaIO.setStatuz(errorsInner.h143);
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		/*  Initialise screen.*/
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(!cntDteFlag) {
					sv.occdateOut[varcom.nd.toInt()].set("Y");
					}
//ILJ-49 End
		scrnparams.function.set(varcom.sclr);
		processScreen("SH580", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*  Set subfile-rrn to 1.*/
		scrnparams.subfileRrn.set(1);
		/*  Initialise numeric fields.*/
		sv.clamant.set(ZERO);
		sv.policyloan.set(ZERO);
		sv.estimateTotalValue.set(ZERO);
		sv.instprem01.set(ZERO);
		sv.instprem02.set(ZERO);
		sv.zlinstprem.set(ZERO);
		sv.zbinstprem.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.sumins01.set(ZERO);
		sv.sumins02.set(ZERO);
		sv.btdate.set(varcom.maxdate);
		sv.occdate.set(varcom.maxdate);
		sv.ptdate.set(varcom.maxdate);
		/*  Load header & footer fields.*/
		sv.chdrnum.set(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
		sv.cnttype.set(chdrpf.getCnttype());//ILIFE-7968 /* IJTI-1386 */
		sv.occdate.set(chdrpf.getOccdate());//ILIFE-7968
		sv.cownnum.set(chdrpf.getCownnum());//ILIFE-7968
		sv.lifcnum.set(lifemjaIO.getLifcnum().toString());
		sv.ptdate.set(chdrpf.getPtdate());//ILIFE-7968
		sv.btdate.set(chdrpf.getBtdate());//ILIFE-7968
		sv.crtable.set(covrpf.getCrtable());
		sv.sumins01.set(covrpf.getSumins());
		sv.instprem01.set(covrpf.getInstprem());
		/*ICIL-254*/
		if(!susur002Permission) {
			sv.payeeOut[varcom.nd.toInt()].set("Y");
			sv.payeeOut[varcom.pr.toInt()].set("Y");
		}	
		if(susur002Permission) {
		sv.payee.set(chdrpf.getCownnum());/* IJTI-1386 */
		sv.bankacckey.set(SPACES);
		sv.bankkey.set(SPACES);
		sv.reqntype.set(chdrpf.getReqntype());/* IJTI-1386 */
		 
		//ILIFE-2472-START
		//ILIFE-7968 start
		 if(null!=chdrpf.getPayclt() && isNE(chdrpf.getPayclt(),SPACES)) {
			sv.payee.set(chdrpf.getPayclt());
		}
		//ILIFE-2472-END
		a1000GetPayorname();
		sv.payeename.set(namadrsrec.name);
		wsaaClntkey.set(SPACES);
		//ILIFE-2472-START
		if (isNE(chdrpf.getCownnum(),SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrpf.getCownpfx());
			stringVariable1.addExpression(chdrpf.getCowncoy());
			stringVariable1.addExpression(chdrpf.getCownnum());
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		
		if (isNE(sv.reqntype, '4') || isNE(sv.reqntype, 'C') ) {
			sv.bankacckeyOut[varcom.nd.toInt()].set("Y");
			sv.crdtcrdOut[varcom.nd.toInt()].set("Y");
			sv.bankacckey.set(SPACES);
			sv.crdtcrd.set(SPACES);
			
		}
		}
		
		/*  Contract type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescitem(chdrpf.getCnttype());/* IJTI-1386 */
		descIO.setDesctabl(t5688);
		getDescription1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		/*  Contract status from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());/* IJTI-1386 */
		getDescription1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Get premium status from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());/* IJTI-1386 */
		getDescription1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/*  Owner name from client.*/
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(chdrpf.getCownnum());
		getClientDetails1200();
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), "1")) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		/*  Life name from client.*/
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		getClientDetails1200();
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), "1")) {
			sv.linsnameErr.set(errorsInner.h143);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		wsaaLifeSex.set(lifemjaIO.getCltsex());
		wsaaLifeAnbccd.set(lifemjaIO.getAnbAtCcd());
		/*  Read LIFEMJA for joint life*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		if (isEQ(lifemjaIO.getStatuz(), varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
		}
		else {
			wsaaJlifeSex.set(lifemjaIO.getCltsex());
			wsaaJlifeAnbccd.set(lifemjaIO.getAnbAtCcd());
			/*     MOVE 'Y'                TO WSAA-JLIFE-EXIST              */
			cltsIO.setDataKey(SPACES);
			sv.jlifcnum.set(lifemjaIO.getLifcnum());
			cltsIO.setClntnum(lifemjaIO.getLifcnum());
			getClientDetails1200();
			if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
			|| isNE(cltsIO.getValidflag(), "1")) {
				sv.jlinsnameErr.set(errorsInner.e058);
				sv.jlinsname.set(SPACES);
			}
			else {
				plainname();
				sv.jlinsname.set(wsspcomn.longconfname);
			}
		}
		/*  Get value of outstanding loans for this contract.              */
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy().toString());
		totloanrec.chdrnum.set(chdrpf.getChdrnum());/* IJTI-1386 */
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(chdrpf.getPtdate());
		totloanrec.function.set("LOAN");
		totloanrec.language.set(wsspcomn.language);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(sv.policyloan, 2).set(add(totloanrec.principal, totloanrec.interest));
		/*  Check if SURHCLM exists.*/
		surhclmIO.setDataKey(SPACES);
		surhclmIO.setChdrcoy(chdrpf.getChdrcoy());
		surhclmIO.setChdrnum(chdrpf.getChdrnum());
		surhclmIO.setTranno(chdrpf.getTranno());
		setPrecision(surhclmIO.getTranno(), 0);
		surhclmIO.setTranno(add(surhclmIO.getTranno(), 1));
		surhclmIO.setPlanSuffix(covrpf.getPlanSuffix());
		surhclmIO.setFormat(formatsInner.surhclmrec);
		surhclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)
		&& isNE(surhclmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		if (isEQ(surhclmIO.getStatuz(), varcom.mrnf)) {
			wsaaFirstTime.set("Y");
		}
		else {
			wsaaFirstTime.set("N");
		}
		/*  Read T5687 to obtain the surrender method for the component*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrpf.getCrtable())) {
			syserrrec.statuz.set(errorsInner.h053);
			itdmIO.setStatuz(errorsInner.h053);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		/*    IF  T5687-SV-METHOD         = SPACES                         */
		if (isEQ(t5687rec.partsurr, SPACES)) {
			syserrrec.statuz.set(errorsInner.e656);
			itdmIO.setStatuz(errorsInner.e656);
			//ILIFE-8522
			sv.chdrnumErr.set(errorsInner.e656);
			return;
		}
		/*  Use the surrender method from T5687, obtain the calcultion*/
		/*  routines from T6598.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6598);
		/*    MOVE T5687-SV-METHOD        TO ITEM-ITEMITEM.                */
		itemIO.setItemitem(t5687rec.partsurr);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		/*  Set up the linkage for calling the surrender calculation*/
		/*  subroutine.*/
		initialize(srcalcpy.surrenderRec);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrpf.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrpf.getChdrnum());
		srcalcpy.planSuffix.set(covrpf.getPlanSuffix());
		srcalcpy.polsum.set(chdrpf.getPolsum());
		srcalcpy.lifeLife.set(covrpf.getLife());
		srcalcpy.lifeJlife.set(covrpf.getJlife());
		srcalcpy.covrCoverage.set(covrpf.getCoverage());
		srcalcpy.covrRider.set(covrpf.getRider());
		srcalcpy.crtable.set(covrpf.getCrtable());
		srcalcpy.crrcd.set(covrpf.getCrrcd());
		srcalcpy.ptdate.set(payrIO.getPtdate());
		srcalcpy.effdate.set(payrIO.getPtdate());
		if(susur002Permission){
			srcalcpy.effdate.set(wsspcomn.currfrom);
		}		
		srcalcpy.convUnits.set(covrpf.getConvertInitialUnits());
		srcalcpy.language.set(wsspcomn.language);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.currcode.set(covrpf.getPremCurrency());
		srcalcpy.chdrCurr.set(chdrpf.getCntcurr());
		srcalcpy.pstatcode.set(covrpf.getPstatcode());
		srcalcpy.singp.set(covrpf.getInstprem());
		srcalcpy.billfreq.set(payrIO.getBillfreq());
		srcalcpy.type.set("F");
		srcalcpy.status.set(varcom.oK);
		srcalcpy.element.set(SPACES);
		srcalcpy.description.set(SPACES);
		srcalcpy.fund.set(SPACES);
		srcalcpy.endf.set(SPACES);
		srcalcpy.neUnits.set(SPACES);
		srcalcpy.tmUnits.set(SPACES);
		srcalcpy.psNotAllwd.set(SPACES);
		/*  Load subfile.*/
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			loadSubfile1300();
		}
		
		/* Write dummy subfile line if subfile is empty. This is to        */
		/* initialise the subfile area. */
		 //ILIFE-7126                                   
		if (isLT(scrnparams.subfileRrn, 1)) {
			blankSubfile1300x();
		}
		/*  Set up new COVTMJA record.*/
		setupCovtmja1400();
		/*  Read T5675.*/
		/*  If the component is attached to the joint life, use the joint  */
		/*  premium method from t5687 as the key, otherwise use the basic  */
		/*  premium method from T5687 as the key.                          */
		/*if(isNE(t5687rec.premmeth,SPACES) || isNE(t5687rec.jlPremMeth,SPACES))*/
		 
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5675);
		/* IF  WSAA-JLIFE-EXIST        = 'Y'                            */
		if (isEQ(covrpf.getJlife(), "01")) {
			itemIO.setItemitem(t5687rec.jlPremMeth);
		}
		else {
			itemIO.setItemitem(t5687rec.premmeth);
		}
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		wsaaPremMeth.set(itemIO.getItemitem());
		t5675rec.t5675Rec.set(itemIO.getGenarea());
		 
		/* If there are any outstanding loans, calculate the total cash    */
		/* value for the entire policy, for validation in the 2000 section */
		wsaaTotalCashVal.set(ZERO);
		wsaaPrevSumins.set(ZERO);
		if (isNE(sv.policyloan, 0)) {
			covrenqIO.setParams(SPACES);
			covrenqIO.setChdrcoy(chdrpf.getChdrcoy().toString());
			covrenqIO.setChdrnum(chdrpf.getChdrnum());/* IJTI-1386 */
			covrenqIO.setPlanSuffix(ZERO);
			covrenqIO.setFormat(formatsInner.covrenqrec);
			covrenqIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi 
			covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
			while ( !(isEQ(covrenqIO.getStatuz(), varcom.endp))) {
				calcTotalCashVal1500();
			}
			//ILIFE-7968 end
		}
	}
protected void initpayout1020() {
		
	
	sv.payee.set(chdrpf.getCownnum());//ILIFE-7968 /* IJTI-1386 */
	sv.effdate.set(wsspcomn.currfrom);
	
}


protected void getDescription1100()
	{
		/*GET-DESC*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1200()
	{
		/*GET-CLTS*/
		/*  Read CLTS to obtain the client name*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFormat(formatsInner.cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadSubfile1300() throws Exception
	{
		loadSfl1300();
	}

protected void loadSfl1300() throws Exception
	{
		/*  Call calculation subroutine.*/
		if (isEQ(t6598rec.calcprog, SPACES)) {
			sv.fieldType.set(SPACES);
			sv.descrip.set(SPACES);
			sv.cnstcur.set(SPACES);
			sv.actvalue.set(ZERO);
			sv.estMatValue.set(ZERO);
			sv.hemv.set(ZERO);
			sv.hactval.set(ZERO);
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		if (isEQ(srcalcpy.endf, "B")) {
			srcalcpy.status.set("BONS");
		}
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//ICIL-585
		/* BSD-POS009 Start */
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		/* BSD-POS009 End */
		/*if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()))) 
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrmjaIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}*/
		
		/* BSD-POS009 Start */
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()))) 
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
			if (susur002Permission){
				// if calling at first time, obtain the Surrender Type from <VPMS_PS_CN_Type_File> in QuipozCfg.xml
				if (isEQ(srcalcpy.type,"F")){
					i = 0;
					try {
						if (i == 0){
							suTypeList = parmsForVPMS.getSurCalTypeList(t5687rec.partsurr.toString());
							suEndType = suTypeList[suTypeList.length - 1];
							suTypeProportion = parmsForVPMS.getSurProportionType(t5687rec.partsurr.toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
						throw e;
					}
				}
				// get the Surrender Type
				srcalcpy.type.set(suTypeList[i]);
				i++;
				// set up  the request information before calling VPMS
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
				Vpxsutdrec vpxsutdrec = new Vpxsutdrec();

				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
				vpxsutdrec.function.set("INIT");
				vpxsutdrec.svMethod.set(t5687rec.partsurr);
				vpxsutdrec.subname.set(t6598rec.calcprog);

				callProgram(Vpxsutd.class, vpmcalcrec.vpmcalcRec,vpxsutdrec);

				if (isNE(vpxsutdrec.statuz,varcom.oK)){
					syserrrec.params.set(srcalcpy.surrenderRec);
					syserrrec.statuz.set(vpxsutdrec.statuz);
					fatalError600();
				}
				
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.effdate.set(wsspcomn.currfrom);
				
				// Call VPMS
				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpxsutdrec);//VPMS call

				if (isEQ(srcalcpy.type,suEndType)){
					srcalcpy.status.set(varcom.endp);
					//Set the Proportaion Type
					if (suTypeProportion.length() > 0){
						srcalcpy.endf.set(suTypeProportion);
					}
				}
			}
			else{
				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			}
		}
		/* BSD-POS009 End */
		if ((susur002Permission)
		&& (isEQ(srcalcpy.endf, "N"))) {
			wsaaNoncashvalue.set("Y");
		}

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		if (isNE(srcalcpy.actualVal, 0)) {
			if (isEQ(srcalcpy.currcode, SPACES)) {
				zrdecplrec.currency.set(chdrpf.getCntcurr());
			}
			else {
				zrdecplrec.currency.set(srcalcpy.currcode);
			}
			zrdecplrec.amountIn.set(srcalcpy.actualVal);
			callRounding5000();
			srcalcpy.actualVal.set(zrdecplrec.amountOut);
		}
		if (isNE(srcalcpy.estimatedVal, 0)) {
			if (isEQ(srcalcpy.currcode, SPACES)) {
				zrdecplrec.currency.set(chdrpf.getCntcurr());
			}
			else {
				zrdecplrec.currency.set(srcalcpy.currcode);
			}
			zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
			callRounding5000();
			srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		}
		if (isEQ(srcalcpy.actualVal, ZERO)) {
			return ;
		}
		/* MOVE SURC-ACTUAL-VAL      TO ZRDP-AMOUNT-IN.                 */
		/* PERFORM 5000-CALL-ROUNDING                                   */
		/* MOVE ZRDP-AMOUNT-OUT      TO SURC-ACTUAL-VAL.                */
		/* MOVE SURC-ESTIMATED-VAL   TO ZRDP-AMOUNT-IN.                 */
		/* PERFORM 5000-CALL-ROUNDING                                   */
		/* MOVE ZRDP-AMOUNT-OUT      TO SURC-ESTIMATED-VAL.             */
		if (isEQ(srcalcpy.currcode, SPACES)) {
			sv.cnstcur.set(chdrpf.getCntcurr());
		}
		else {
			sv.cnstcur.set(srcalcpy.currcode);
		}
		sv.fieldType.set(srcalcpy.type);
		sv.actvalue.set(srcalcpy.actualVal);
		sv.hactval.set(srcalcpy.actualVal);
		sv.estMatValue.set(srcalcpy.estimatedVal);
		sv.hemv.set(srcalcpy.estimatedVal);
		sv.descrip.set(srcalcpy.description);
		/*  Add to subfile.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SH580", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void blankSubfile1300x()
	{
		/*X-START*/
		sv.cnstcur.set(SPACES);
		sv.descrip.set(SPACES);
		sv.fieldType.set(SPACES);
		sv.actvalue.set(ZERO);
		sv.estMatValue.set(ZERO);
		sv.hactval.set(ZERO);
		sv.hemv.set(ZERO);
		scrnparams.function.set(varcom.sadd);
		processScreen("SH580", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*X-EXIT*/
	}

protected void setupCovtmja1400()
	{
		setupCovt1400();
	}

protected void setupCovt1400()
	{
		/*  Set up fields in COVTMJA from COVRMJA.*/
		covtmjaIO.setDataArea(SPACES);
		covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covtmjaIO.setChdrnum(covrpf.getChdrnum());
		covtmjaIO.setLife(covrpf.getLife());
		covtmjaIO.setCoverage(covrpf.getCoverage());
		covtmjaIO.setRider(covrpf.getRider());
		covtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		covtmjaIO.setFunction(varcom.readr);/*ICIL-604*/
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isEQ(covtmjaIO.getStatuz(), varcom.oK)){
			covtmjaIO.setFunction(varcom.delet);
			SmartFileCode.execute(appVars, covtmjaIO);
			if (isEQ(covtmjaIO.getStatuz(),varcom.oK)) {
				covtmjaIO.setDataArea(SPACES);
				covtmjaIO.setChdrcoy(covrpf.getChdrcoy());
				covtmjaIO.setChdrnum(covrpf.getChdrnum());
				covtmjaIO.setLife(covrpf.getLife());
				covtmjaIO.setCoverage(covrpf.getCoverage());
				covtmjaIO.setRider(covrpf.getRider());
				covtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
			}
		}
		covtmjaIO.setSeqnbr(ZERO);
		covtmjaIO.setCrtable(covrpf.getCrtable());
		covtmjaIO.setRiskCessDate(covrpf.getRiskCessDate());
		covtmjaIO.setPremCessDate(covrpf.getPremCessDate());
		covtmjaIO.setRiskCessAge(covrpf.getRiskCessAge());
		covtmjaIO.setPremCessAge(covrpf.getPremCessAge());
		covtmjaIO.setRiskCessTerm(covrpf.getRiskCessTerm());
		covtmjaIO.setPremCessTerm(covrpf.getPremCessTerm());
		covtmjaIO.setSumins(covrpf.getSumins());
		covtmjaIO.setMortcls(covrpf.getMortcls());
		covtmjaIO.setLiencd(covrpf.getLiencd());
		covtmjaIO.setReserveUnitsInd(covrpf.getReserveUnitsInd());
		covtmjaIO.setReserveUnitsDate(covrpf.getReserveUnitsDate());
		covtmjaIO.setPolinc(chdrpf.getPolinc());//ILIFE-7968
		covtmjaIO.setJlife(covrpf.getJlife());
		covtmjaIO.setEffdate(covrpf.getCrrcd());
		covtmjaIO.setSex01(wsaaLifeSex);
		covtmjaIO.setSex02(wsaaJlifeSex);
		covtmjaIO.setAnbAtCcd01(wsaaLifeAnbccd);
		covtmjaIO.setAnbAtCcd02(wsaaJlifeAnbccd);
		covtmjaIO.setBillfreq(payrIO.getBillfreq());
		covtmjaIO.setBillchnl(payrIO.getBillchnl());
		covtmjaIO.setSingp(covrpf.getSingp());
		covtmjaIO.setInstprem(covrpf.getInstprem());
		covtmjaIO.setZbinstprem(covrpf.getZbinstprem());
		covtmjaIO.setZlinstprem(covrpf.getZlinstprem());
		covtmjaIO.setPayrseqno(covrpf.getPayrseqno());
		covtmjaIO.setBenCessAge(ZERO);
		covtmjaIO.setBenCessTerm(ZERO);
		covtmjaIO.setBenCessDate(varcom.vrcmMaxDate);
		covtmjaIO.setBappmeth(covrpf.getBappmeth());
		covtmjaIO.setZdivopt(SPACES);
		covtmjaIO.setPaycoy(SPACES);
		covtmjaIO.setPayclt(SPACES);
		covtmjaIO.setPaymth(SPACES);
		covtmjaIO.setBankkey(SPACES);
		covtmjaIO.setBankacckey(SPACES);
		covtmjaIO.setPaycurr(SPACES);
		covtmjaIO.setFacthous(SPACES);
		hcsdIO.setDataKey(SPACES);
		hcsdIO.setChdrcoy(covrpf.getChdrcoy());
		hcsdIO.setChdrnum(covrpf.getChdrnum());
		hcsdIO.setLife(covrpf.getLife());
		hcsdIO.setCoverage(covrpf.getCoverage());
		hcsdIO.setRider(covrpf.getRider());
		hcsdIO.setPlanSuffix(covrpf.getPlanSuffix());
		hcsdIO.setFormat(formatsInner.hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)
		&& isNE(hcsdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(hcsdIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		covtmjaIO.setZdivopt(hcsdIO.getZdivopt());
		covtmjaIO.setPaycoy(hcsdIO.getPaycoy());
		covtmjaIO.setPayclt(hcsdIO.getPayclt());
		covtmjaIO.setPaymth(hcsdIO.getPaymth());
		covtmjaIO.setBankkey(hcsdIO.getBankkey());
		covtmjaIO.setBankacckey(hcsdIO.getBankacckey());
		covtmjaIO.setPaycurr(hcsdIO.getPaycurr());
		covtmjaIO.setFacthous(hcsdIO.getFacthous());
	}

protected void calcTotalCashVal1500() throws Exception
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					tot1510();
				case next1580: 
					next1580();
				case exit1590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void tot1510() throws Exception
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		//ILIFE-7968
		if (isNE(covrenqIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(), chdrpf.getChdrnum())
		|| isEQ(covrenqIO.getStatuz(), varcom.endp)) {
			covrenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1590);
		}
		/*  Read T5687 to obtain the surrender method for the component    */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrenqIO.getCrtable());
		itdmIO.setItmfrm(covrenqIO.getCrrcd());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrenqIO.getCrtable())) {
			syserrrec.statuz.set(errorsInner.h053);
			itdmIO.setStatuz(errorsInner.h053);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (isEQ(t5687rec.svMethod, SPACES)) {
			goTo(GotoLabel.next1580);
		}
		/*  Use the surrender method from T5687, obtain the calcultion     */
		/*  routines from T6598.                                           */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		if (isEQ(t6598rec.calcprog, SPACES)) {
			goTo(GotoLabel.next1580);
		}
		/*  Set up the linkage for calling the surrender calculation       */
		/*  subroutine.                                                    */
		initialize(srcalcpy.surrenderRec);
		srcalcpy.chdrChdrcoy.set(covrenqIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrenqIO.getChdrnum());
		srcalcpy.planSuffix.set(covrenqIO.getPlanSuffix());
		srcalcpy.polsum.set(chdrpf.getPolsum());//ILIFE-7968
		srcalcpy.lifeLife.set(covrenqIO.getLife());
		srcalcpy.lifeJlife.set(covrenqIO.getJlife());
		srcalcpy.covrCoverage.set(covrenqIO.getCoverage());
		srcalcpy.covrRider.set(covrenqIO.getRider());
		srcalcpy.crtable.set(covrenqIO.getCrtable());
		srcalcpy.crrcd.set(covrenqIO.getCrrcd());
		srcalcpy.ptdate.set(payrIO.getPtdate());
		srcalcpy.effdate.set(payrIO.getPtdate());
		if(susur002Permission){
			srcalcpy.effdate.set(wsspcomn.currfrom);
		}		
		srcalcpy.convUnits.set(ZERO);
		srcalcpy.language.set(wsspcomn.language);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.currcode.set(chdrpf.getCntcurr());//ILIFE-7968 /* IJTI-1386 */
		srcalcpy.chdrCurr.set(chdrpf.getCntcurr());//ILIFE-7968 /* IJTI-1386 */
		srcalcpy.pstatcode.set(covrenqIO.getPstatcode());
		srcalcpy.singp.set(covrenqIO.getInstprem());
		srcalcpy.billfreq.set(payrIO.getBillfreq());
		srcalcpy.type.set("F");
		srcalcpy.status.set(varcom.oK);
		srcalcpy.element.set(SPACES);
		srcalcpy.description.set(SPACES);
		srcalcpy.fund.set(SPACES);
		srcalcpy.endf.set(SPACES);
		srcalcpy.neUnits.set(SPACES);
		srcalcpy.tmUnits.set(SPACES);
		srcalcpy.psNotAllwd.set(SPACES);
		wsaaCompTotal.set(ZERO);
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			calcValue1600();
		}
		
		/* If a loan method exists on T5687, read T6632 for the maximum    */
		/* allowable percentage and apply this to the calculated cash      */
		/* value.                                                          */
		if (isNE(t5687rec.loanmeth, SPACES)) {
			itdmIO.setParams(SPACES);
			itdmIO.setItemcoy(covrenqIO.getChdrcoy());
			itdmIO.setItemtabl(t6632);
			itdmIO.setItemitem(t5687rec.loanmeth);
			itdmIO.setItmfrm(covrenqIO.getCrrcd());
			itdmIO.setFormat(formatsInner.itemrec);
			itdmIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi 
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if (isNE(itdmIO.getItemcoy(), covrenqIO.getChdrcoy())
			|| isNE(itdmIO.getItemtabl(), t6632)
			|| isNE(itdmIO.getItemitem(), t5687rec.loanmeth)
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				itdmIO.setItemitem(t5687rec.loanmeth);
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(errorsInner.g542);
				fatalError600();
			}
			else {
				t6632rec.t6632Rec.set(itdmIO.getGenarea());
			}
			compute(wsaaCompTotal, 3).setRounded(div(mult(wsaaCompTotal, t6632rec.maxpcnt), 100));
		}
		/* MOVE WSAA-COMP-TOTAL      TO ZRDP-AMOUNT-IN.                 */
		/* PERFORM 5000-CALL-ROUNDING                                   */
		/* MOVE ZRDP-AMOUNT-OUT      TO WSAA-COMP-TOTAL.                */
		if (isNE(wsaaCompTotal, 0)) {
			zrdecplrec.currency.set(chdrpf.getCntcurr());//ILIFE-7968 /* IJTI-1386 */
			zrdecplrec.amountIn.set(wsaaCompTotal);
			callRounding5000();
			wsaaCompTotal.set(zrdecplrec.amountOut);
		}
		wsaaTotalCashVal.add(wsaaCompTotal);
	}

protected void next1580()
	{
		covrenqIO.setFunction(varcom.nextr);
	}

protected void calcValue1600() throws Exception
	{
		/*CALC*/
		/*  Call calculation subroutine.                                   */
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		if (isEQ(srcalcpy.endf, "B")) {
			srcalcpy.status.set("BONS");
		}
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//ICIL-585
		/* BSD-POS009 Start */
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		/* BSD-POS009 End */
		/*if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()))) 
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
	 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

			vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
			vpxsurcrec.function.set("INIT");
			callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
			srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
			vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
			vpmfmtrec.initialize();
			vpmfmtrec.amount02.set(wsaaEstimateTot);			

			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrmjaIO);//VPMS call
			
			
			if(isEQ(srcalcpy.type,"L"))
			{
				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
				callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"C"))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
			{
				srcalcpy.status.set(varcom.endp);
			}
			else
			{
				srcalcpy.status.set(varcom.oK);
			}
		}*/
		
		/* BSD-POS009 Start */
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()))) 
		{
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
		}
		else
		{
			if (susur002Permission){
				// if calling at first time, obtain the Surrender Type from <VPMS_PS_CN_Type_File> in QuipozCfg.xml
				if (isEQ(srcalcpy.type,"F")){
					i = 0;
					suTypeList = null;
					suEndType = null;
					// sv_mothod of t5687 by Main coverage
					try {
						if (i == 0){
							suTypeList = parmsForVPMS.getSurCalTypeList(t5687rec.svMethod.toString());
							suEndType = suTypeList[suTypeList.length - 1];
							suTypeProportion = parmsForVPMS.getSurProportionType(t5687rec.svMethod.toString());
						}
					} catch (Exception e) {
						e.printStackTrace();
						throw e;
					}
				}
				// get the Surrender Type
				srcalcpy.type.set(suTypeList[i]);
				i++;
				/* set the request information before calling VPMS */
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
				Vpxsutdrec vpxsutdrec = new Vpxsutdrec();

				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);
				vpxsutdrec.function.set("INIT");
				vpxsutdrec.svMethod.set(t5687rec.svMethod); //using the sv_method of t6587 by main coverage
				vpxsutdrec.subname.set(t6598rec.calcprog);

				callProgram(Vpxsutd.class, vpmcalcrec.vpmcalcRec,vpxsutdrec);

				if (isNE(vpxsutdrec.statuz,varcom.oK)){
					syserrrec.params.set(srcalcpy.surrenderRec);
					syserrrec.statuz.set(vpxsutdrec.statuz);
					fatalError600();
				}
				
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				srcalcpy.effdate.set(wsspcomn.currfrom);

				/* Call VPMS */
				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpxsutdrec);
		
				if (isEQ(srcalcpy.type,suEndType)){
					srcalcpy.status.set(varcom.endp); // End the loop
					//Set the Proportaion Type
					if (suTypeProportion.length() > 0){
						srcalcpy.endf.set(suTypeProportion);
					}
				}
			}
			else{
				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			}
		}
		/* BSD-POS009 End */
		if ((susur002Permission)
		&& (isEQ(srcalcpy.endf, "N"))) {
			wsaaNoncashvalue.set("Y");
		}

		/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)) {
			syserrrec.params.set(srcalcpy.surrenderRec);
			syserrrec.statuz.set(srcalcpy.status);
			fatalError600();
		}
		wsaaCompTotal.add(srcalcpy.actualVal);
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*  Skip this section if returning from an optional screen.        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateScreen2010();
		checkForErrors2050();
		validateSubfile2060();
		checkLimit2070();
	}

protected void screenIo2010()
	{
		/*    CALL 'SH580IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         SH580-DATA-AREA                         */
		/*                         SH580-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		
		wsaaPrevSumins.set(sv.sumins01);
		if(susur002Permission)
		{
		if(isEQ(sv.payee, SPACES)){
			sv.payee.set(chdrpf.getCownnum());//ILIFE-7968
		}
		//ILIFE-7968 start
		if (isNE(chdrpf.getCownnum(), SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrpf.getCownpfx());
			stringVariable1.addExpression(chdrpf.getCowncoy());
			stringVariable1.addExpression(sv.payee);
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		//ILIFE-7968 end
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(sv.payee);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
	
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(),varcom.oK))
		&& (isNE(cltsIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.payeeErr.set(errorsInner.e335);
			wsspcomn.edterror.set("Y");
			sv.payeename.set(SPACES);
			goTo(GotoLabel.exit1590);
		}
		a1000GetPayorname();
		sv.payeename.set(namadrsrec.name);
		
		if (isEQ(sv.reqntype, "4")) {
			sv.crdtcrd.set(SPACES);
			sv.bankacckeyOut[varcom.nd.toInt()].set("N");
			sv.crdtcrdOut[varcom.nd.toInt()].set("Y");
			if (isEQ(sv.bankacckey, SPACES)) {
				sv.bankacckeyErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				checkForErrors2050();
			}
			
		}
		
		if (isEQ(sv.reqntype, "C")) {
			sv.bankacckey.set(SPACES);
			sv.crdtcrdOut[varcom.nd.toInt()].set("N");
			sv.bankacckeyOut[varcom.nd.toInt()].set("Y");
			if (isEQ(sv.crdtcrd, SPACES)) {
				sv.crdtcrdErr.set(errorsInner.e186);
				wsspcomn.edterror.set("Y");
				checkForErrors2050();
			}
		}
		
		if (isNE(sv.reqntype, '4') && isNE(sv.reqntype, 'C') ) {
			sv.bankacckeyOut[varcom.nd.toInt()].set("Y");
			sv.crdtcrdOut[varcom.nd.toInt()].set("Y");
			sv.bankacckey.set(SPACES);
			sv.crdtcrd.set(SPACES);
			
		}
		
		if (isEQ(sv.reqntype, SPACES) ) {
			sv.reqntypeErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			checkForErrors2050();
			
		}
		sv.bankkey.set(SPACES);

	}
	}

protected void a1000GetPayorname()	{
	/*A1010-NAMADRS*/
	initialize(namadrsrec.namadrsRec);
	namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
	namadrsrec.clntCompany.set(wsspcomn.fsuco);
	namadrsrec.clntNumber.set(sv.payee);
	namadrsrec.language.set(wsspcomn.language);
	namadrsrec.function.set(wsaaLargeName);
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(namadrsrec.statuz);
		fatalError600();
	}
	/*A1090-EXIT*/
}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*  New sum assured must be less than existing sum assured*/
		if (isGTE(sv.sumins02, sv.sumins01)) {
			sv.sumins02Err.set(errorsInner.hl53);
		}
		if (isLTE(sv.sumins02, ZERO)) {
			sv.sumins02Err.set(errorsInner.hl54);
		}
		if (isNE(sv.sumins02, ZERO)) {
			zrdecplrec.amountIn.set(sv.sumins02);
			zrdecplrec.currency.set(chdrpf.getCntcurr());
			callRounding5000();
			if (isNE(zrdecplrec.amountOut, sv.sumins02)) {
				sv.sumins02Err.set(errorsInner.rfik);
			}
		} 
		
		if(regpayFlag){
			if(isNE(wsaaPrevSumins,sv.sumins02)){ 
				if(isEQ(wsaaBatchkey.batcBatctrcde.toString(),Batctrcde))
					CheckT6625();
				 }
			} 
		
		/*  Calculate new instalment premium.*/ 
		calcNewPremium2100();
		/*  Calculate surrender amount and total.*/
		compute(wsaaProportion, 8).setRounded(div(sv.sumins02, sv.sumins01));
		if((susur002Permission)
		&& (isEQ(wsaaNoncashvalue, "Y"))){
			compute(wsaaProportion, 8).setRounded(div(sv.instprem02, sv.instprem01));
		}
		compute(wsaaProportion, 7).set(sub(1, wsaaProportion));
		
		if(susur002Permission)
  		{
		if (isNE(sv.bankacckey,SPACES) || isNE(sv.crdtcrd,SPACES)) {
		clbapf = new Clbapf();
		clbapf.setClntpfx("CN");
		clbapf.setClntcoy(wsspcomn.fsuco.toString().trim());
		clbapf.setClntnum(sv.payee.toString().trim());
		List<Clbapf> clbalist = clbapfDAO.searchClbapfDatabyObject(clbapf);
		if (clbalist != null && clbalist.size() > 0) {
			for (Clbapf clbaitem : clbalist) {
				sv.bankkey.set(clbaitem.getBankkey());
				
			}
		}
		}
		
		if (isNE(sv.bankacckey,SPACES)) {
			bankBranchDesc1120();
		}
		
		if (isNE(sv.crdtcrd,SPACES)) {
			bankBranchDesc1120();
		}
  		}
		
	}
protected void bankBranchDesc1120()
{
	babrIO.setDataKey(SPACES);
	babrIO.setBankkey(sv.bankkey);
	babrIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, babrIO);
	if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
	sv.bankkeyErr.set(errorsInner.f906);
	}
	if (isNE(babrIO.getStatuz(),varcom.oK)
			&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(babrIO.getParams());
		fatalError600();
	}
	wsaaBankkey.set(babrIO.getBankdesc());
	sv.bankdesc.set(wsaaBankdesc);
}

	/**
	* <pre>
	**** COMPUTE SH580-ESTIMATE-TOTAL-VALUE                           
	****                             = SH580-SUMINS-01                
	****                             - SH580-SUMINS-02.               
	**** COMPUTE SH580-CLAMANT       = SH580-ESTIMATE-TOTAL-VALUE     
	****                             + SH580-OTHERADJST.              
	* </pre>
	*/
protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
protected void CheckT6625(){
	itempf = new Itempf();
	
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl("T6625");
	itempf.setItemitem(covrpf.getCrtable());//IJTI-1793
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItmto(new BigDecimal(varcom.vrcmMaxDate.toString()));
	itempfList = itemDAO.findByItemDates(itempf);
	if(itempfList.size() > 0) {
		ChangeRegp();
	}
}



protected void ChangeRegp(){
	regppfList = regpDAO.readRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(),covrpf.getRider());//IJTI-1793
	if (regppfList != null && regppfList.size() > 0) {
	for(Regppf regpobj : regppfList){
		if(isEQ(regpobj.getValidflag(),"1")){
			regpIO.setNonKey(SPACES);
			regpIO.setFormat(regprec);
			regpIO.setChdrcoy(chdrpf.getChdrcoy().toString());//ILIFE-7968
			regpIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
			regpIO.setLife(covrpf.getLife());
			regpIO.setCoverage(covrpf.getCoverage());
			regpIO.setRider(covrpf.getRider());
			regpIO.setValidflag("1");
			regpIO.setRgpynum(regpobj.getRgpynum());
			
			regpIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, regpIO);
			if (isEQ(regpIO.getStatuz(), varcom.mrnf)) {
				return;
			}
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			regpsubrec.subCompany.set(chdrpf.getChdrcoy());
			regpsubrec.subBatctrcde.set(wsaaBatchkey.batcBatctrcde);
			compute(wsaaKeepSumins,2).set(div(mult(sv.sumins02,regpobj.getPrcnt()),100));
					read6693Action();
					if(itempfList1.size() > 0) {
					 
						wsaaT6696Crtable.set(regpIO.getCrtable());
						wsaaT6696Cltype.set(regpIO.getPayreason());
						itempf.setItempfx("IT");
						itempf.setItemtabl("T6696");
						itempf.setItemcoy(chdrpf.getChdrcoy().toString());//ILIFE-7968
						itempf.setItemitem(wsaaT6696Key.toString());
						itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));//ILIFE-7968
						itempf.setItmto(new BigDecimal(varcom.vrcmMaxDate.toString()));
						itempfList = itemDAO.findByItemDates(itempf);
						if(itempfList.size() > 0) {
							itempf = itempfList.get(0);
							t6696rec.t6696Rec.set(StringUtil.rawToString(itempf.getGenarea()));
							regpIO.setPymt(wsaaKeepSumins.toString());
							regpIO.setFunction(varcom.keeps);
							SmartFileCode.execute(appVars, regpIO);
							if (isNE(regpIO.getStatuz(), varcom.oK)) {
								syserrrec.params.set(regpIO.getParams());
								fatalError600();
							}
							callProgram(t6696rec.inxsbm, regpsubrec.batcsubRec);
							if (isNE(regpsubrec.subStatuz, varcom.oK)) {
									releaseregp();
									return ;
								}
								/*  Retrieve the REGP record to obtain indexation changes*/
								regpIO.setFunction(varcom.retrv);
								SmartFileCode.execute(appVars, regpIO);
								if (isNE(regpIO.getStatuz(), varcom.oK)) {
									regpsubrec.subStatuz.set(regpIO.getStatuz());
									releaseregp();
									return ;
								}
								/*    Rewrite the existing REGP with the validflag set to '2'*/
								ChangeAcctoFreq();
									regpobj.setValidflag("2");
									regpDAO.updateRegpValidFlag(regpobj);
									Regppf regp = new Regppf(regpobj);
									regp.setValidflag("1");
									regp.setPymt(regpIO.getPymt().getbigdata());
									regpList = new ArrayList<>();
									regpList.add(regp);
									regpDAO.insertRegppfRecord(regpList);	
								releaseregp();
							}
						}
			}
		}
	}
}
	
	
	
protected void read6693Action(){
	/*Read T6693 to check if the selected record has a valid action*/
	wsaaT6693Paystat.set(regpIO.getRgpystat());
	wsaaT6693Crtable.set(regpIO.getCrtable());
	itempf=new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl("T6693");
	itempf.setItemitem(wsaaT6693Key.toString());
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));//ILIFE-7968
	itempf.setItmto(new BigDecimal(varcom.vrcmMaxDate.toString()));
	itempfList1 = itemDAO.findByItemDates(itempf);
}

protected void ChangeAcctoFreq(){
	if(isEQ(regpIO.regpayfreq,"12")){
		compute(wsaaKeepSumins,2).set(div(regpIO.getPymt(),12));
		regpIO.setPymt(wsaaKeepSumins.toString());
	}
	if(isEQ(regpIO.regpayfreq,"02")){
		compute(wsaaKeepSumins,2).set(div(regpIO.getPymt(),2));
		regpIO.setPymt(wsaaKeepSumins.toString());
	}
	if(isEQ(regpIO.regpayfreq,"04")){
		compute(wsaaKeepSumins,2).set(div(regpIO.getPymt(),4));
		regpIO.setPymt(wsaaKeepSumins.toString());
	}
	if(isEQ(regpIO.regpayfreq,"26")){
		compute(wsaaKeepSumins,2).set(div(regpIO.getPymt(),26));
		regpIO.setPymt(wsaaKeepSumins.toString());
	}
}

protected void releaseregp(){
	regpIO.setFunction(varcom.rlse);
	SmartFileCode.execute(appVars, regpIO);
	if (isNE(regpIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(regpIO.getParams());
		fatalError600();
	}
}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("SH580", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.estimateTotalValue.set(ZERO);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}
		
		if (isNE(sv.otheradjst, ZERO)) {
			zrdecplrec.amountIn.set(sv.otheradjst);
			zrdecplrec.currency.set(chdrpf.getCntcurr());//ILIFE-7968 /* IJTI-1386 */
			callRounding5000();
			if (isNE(zrdecplrec.amountOut, sv.otheradjst)) {
				sv.otheradjstErr.set(errorsInner.rfik);
			}
		}
		compute(sv.clamant, 2).set(add(sv.estimateTotalValue, sv.otheradjst));
		/* If there are any outstanding loans, ensure that the amount      */
		/* chosen to surrender will not result in the policy's tota<M04>   */
		/* remaining cash value becoming less than the total outsta<M04>   */
		/* loan amount. If this is the case, display an error messa<M04>   */
		/* (to force the user to choose a smaller surrender amount <M04>   */
		/* else to abandon the transaction).                       <M04>   */
		if (isNE(sv.policyloan, 0)) {
			compute(wsaaRemCashVal, 2).set(sub(wsaaTotalCashVal, sv.estimateTotalValue));
			if (isLT(wsaaRemCashVal, sv.policyloan)) {
				sv.policyloanErr.set(errorsInner.hl55);
				wsspcomn.edterror.set("Y");
			}
		}
	}

protected void checkLimit2070()
{
 if(!susur002Permission) return ;	
 uwlvIO.setDataKey(SPACES);
 uwlvIO.setUserid(wsspcomn.userid);
 uwlvIO.setCompany(wsspcomn.company);
 uwlvIO.setFunction(varcom.readr);
 uwlvIO.setFormat(formatsInner.uwlvrec);
 SmartFileCode.execute(appVars, uwlvIO);


 
 if (isNE(uwlvIO.getStatuz(), varcom.oK) &&
			isNE(uwlvIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(uwlvIO.getParams());
		syserrrec.statuz.set(uwlvIO.getStatuz());
		fatalError600();
	}
 
 
 if (isEQ(uwlvIO.getStatuz(), varcom.mrnf)) {
		sv.clamantErr.set(errorsInner.rf05);
		wsspcomn.edterror.set("Y");
		return;
	}
 
 
 
 else {
		
	 
	if((null!=uwlvIO.getPoslevel()) && isEQ(uwlvIO.getPoslevel(),SPACES)) {
			sv.clamantErr.set(errorsInner.rf05);
			wsspcomn.edterror.set("Y");
			return;
		}
	 readTd5h6();
	 
	 if(isGT(sv.clamant, td5h6rec.fullsurauthlim))
	   {
	    	sv.clamantErr.set(errorsInner.rrfn);
	    	wsspcomn.edterror.set("Y");
	   }
 }	
}


protected void readTd5h6() 
{
itempf = new Itempf();
itempf.setItempfx(smtpfxcpy.item.toString());
itempf.setItemcoy(wsspcomn.company.toString());
itempf.setItemtabl(td5h6);/* IJTI-1386 */
itempf.setItemitem(uwlvIO.getPoslevel().toString());
itempf = itempfDAO.getItempfRecord(itempf);
if (itempf== null ) {
	syserrrec.params.set(itempf);
	fatalError600();
}
else {
	td5h6rec.td5h6Rec.set(StringUtil.rawToString(itempf.getGenarea()));
}
}

protected void calcNewPremium2100()
	{
		calcPremium2100();
	}

protected void calcPremium2100()
	{
		/*  Set up linkage for premium calculation.*/
		initialize(premiumrec.premiumRec);
		premiumrec.statuz.set(varcom.oK);
		premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy().toString());//ILIFE-7968
		premiumrec.chdrChdrnum.set(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
		premiumrec.lifeLife.set(covrpf.getLife());
		premiumrec.covrCoverage.set(covrpf.getCoverage());
		premiumrec.covrRider.set(covrpf.getRider());
		premiumrec.plnsfx.set(covrpf.getPlanSuffix());
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.lifeJlife.set(covrpf.getJlife());
		premiumrec.crtable.set(covrpf.getCrtable());
		premiumrec.mortcls.set(covrpf.getMortcls());
		premiumrec.currcode.set(payrIO.getCntcurr());
		premiumrec.effectdt.set(payrIO.getPtdate());
		premiumrec.termdate.set(covrpf.getPremCessDate());
		premiumrec.sumin.set(sv.sumins02);
		premiumrec.ratingdate.set(chdrpf.getOccdate().toString());//ILIFE-7968
		premiumrec.reRateDate.set(chdrpf.getOccdate().toString());//ILIFE-7968
		premiumrec.lage.set(wsaaLifeAnbccd);
		premiumrec.jlage.set(wsaaJlifeAnbccd);
		premiumrec.lsex.set(wsaaLifeSex);
		premiumrec.jlsex.set(wsaaJlifeSex);
		premiumrec.calcPrem.set(ZERO);
		premiumrec.calcBasPrem.set(ZERO);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.benCessTerm.set(ZERO);
		premiumrec.cnttype.set(chdrpf.getCnttype());/* IJTI-1386 */
		/*  Duration from premium cessation date.*/
		datcon3rec.intDate1.set(payrIO.getPtdate());
		datcon3rec.intDate2.set(covrpf.getPremCessDate());
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/*  Risk cessation term from risk cessation date.*/
		datcon3rec.intDate1.set(payrIO.getPtdate());
		datcon3rec.intDate2.set(covrpf.getRiskCessDate());
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.riskCessTerm.set(datcon3rec.freqFactor);
		/*  Call premium calcultion subroutine.*/
		if (isEQ(t5675rec.premsubr, SPACES)) {
			sv.instprem02Err.set(errorsInner.e694);
			return ;
		}
		premiumrec.language.set(wsspcomn.language);

		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
    		premiumrec.premMethod.set(wsaaPremMeth);	
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End*/

		if (isEQ(premiumrec.statuz, "BOMB")) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.instprem02Err.set(premiumrec.statuz);
			return ;
		}
		/* MOVE CPRM-CALC-PREM       TO ZRDP-AMOUNT-IN.                 */
		/* PERFORM 5000-CALL-ROUNDING                                   */
		/* MOVE ZRDP-AMOUNT-OUT      TO CPRM-CALC-PREM.                 */
		/* MOVE CPRM-CALC-LOA-PREM   TO ZRDP-AMOUNT-IN.                 */
		/* PERFORM 5000-CALL-ROUNDING                                   */
		/* MOVE ZRDP-AMOUNT-OUT      TO CPRM-CALC-LOA-PREM.             */
		/* MOVE CPRM-CALC-BAS-PREM   TO ZRDP-AMOUNT-IN.                 */
		/* PERFORM 5000-CALL-ROUNDING                                   */
		/* MOVE ZRDP-AMOUNT-OUT      TO CPRM-CALC-BAS-PREM.             */
		sv.instprem02.set(premiumrec.calcPrem);
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
	}

protected void validateSubfile2600()
	{
		validation2610();
		updateErrorIndicators2670();
		readNextRecord2680();
	}

protected void validation2610()
	{
		compute(sv.actvalue, 8).setRounded(mult(sv.hactval, wsaaProportion));
		/* MOVE SH580-ACTVALUE       TO ZRDP-AMOUNT-IN.                 */
		/* PERFORM 5000-CALL-ROUNDING                                   */
		/* MOVE ZRDP-AMOUNT-OUT      TO SH580-ACTVALUE.                 */
		if (isNE(sv.actvalue, 0)) {
			zrdecplrec.currency.set(sv.cnstcur);
			zrdecplrec.amountIn.set(sv.actvalue);
			callRounding5000();
			sv.actvalue.set(zrdecplrec.amountOut);
		}
		compute(sv.estMatValue, 8).setRounded(mult(sv.hemv, wsaaProportion));
		/* MOVE SH580-EST-MAT-VALUE  TO ZRDP-AMOUNT-IN.                 */
		/* PERFORM 5000-CALL-ROUNDING                                   */
		/* MOVE ZRDP-AMOUNT-OUT      TO SH580-EST-MAT-VALUE.            */
		if (isNE(sv.estMatValue, 0)) {
			zrdecplrec.currency.set(sv.cnstcur);
			zrdecplrec.amountIn.set(sv.estMatValue);
			callRounding5000();
			sv.estMatValue.set(zrdecplrec.amountOut);
		}
		sv.estimateTotalValue.add(sv.actvalue);
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SH580", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SH580", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Skip this section if returning from an optional screen.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		/*  For each subfile record, create a corresponding SURDCLM record.*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("SH580", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			createSurdclm3100();
		}
		
		/*  Create a SURHCLM record if it is the first time.*/
		if (firstTime.isTrue()) {
			createSurhclm3200();
		}
		/*  Set up COVT with new values and create record in COVT.*/
		covtmjaIO.setSumins(sv.sumins02);
		covtmjaIO.setInstprem(sv.instprem02);
		covtmjaIO.setZlinstprem(sv.zlinstprem);
		covtmjaIO.setZbinstprem(sv.zbinstprem);
		covtmjaIO.setTermid(varcom.vrcmTermid);
		covtmjaIO.setUser(varcom.vrcmUser);
		covtmjaIO.setTransactionDate(varcom.vrcmDate);
		covtmjaIO.setTransactionTime(varcom.vrcmTime);
		covtmjaIO.setEffdate(payrIO.getPtdate());
		covtmjaIO.setFormat(formatsInner.covtmjarec);
		covtmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		/*  Check if PCDTMJA record exist, if not, create one.*/
		pcdtmjaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		pcdtmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcdtmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy().toString());//ILIFE-7968
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));//ILIFE-7968
		pcdtmjaIO.setLife(covrpf.getLife());
		pcdtmjaIO.setCoverage(covrpf.getCoverage());
		pcdtmjaIO.setRider(covrpf.getRider());
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		//ILIFE-7968
		if (isNE(pcdtmjaIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(pcdtmjaIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(pcdtmjaIO.getLife(), covrpf.getLife())
		|| isNE(pcdtmjaIO.getCoverage(), covrpf.getCoverage())
		|| isNE(pcdtmjaIO.getRider(), covrpf.getRider())
		|| isNE(pcdtmjaIO.getPlanSuffix(), covrpf.getPlanSuffix())) {
			pcdtmjaIO.setStatuz(varcom.endp);
		}
		//ILIFE-7968 end
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(pcdtmjaIO.getStatuz(), varcom.endp)) {
			defaultPcdt3300();
		}
		if(susur002Permission) {
		 pyoupf = new Pyoupf();
		 
		 if(isNE(sv.bankacckey,SPACE)){
		    pyoupf.setBankacckey(sv.bankacckey.toString());
		    }
		 if(isNE(sv.crdtcrd,SPACE)){
		    pyoupf.setBankacckey(sv.crdtcrd.toString());
		    }
		if(isEQ(sv.bankacckey,SPACE)){
		     pyoupf.setBankacckey(sv.crdtcrd.toString());
		    }
		if(isEQ(sv.crdtcrd,SPACE)){
		     pyoupf.setBankacckey(sv.bankacckey.toString());
		    }
		 pyoupf.setBankkey(sv.bankkey.toString());
		 pyoupf.setBankdesc(sv.bankdesc.toString());
		 pyoupf.setBatctrcde(wsaaBatchkey.batcBatctrcde.toString());
		 pyoupf.setChdrcoy(covtmjaIO.getChdrcoy().toString());
		 pyoupf.setChdrnum(sv.chdrnum.toString());
		 pyoupf.setChdrpfx("CH");
		 pyoupf.setPayrnum(sv.payee.trim());//ICIL-484
		 pyoupf.setPaymentflag(" ");
		 pyoupf.setReqntype(sv.reqntype.toString());
		 pyoupf.setReqnno(" ");
		 pyoupf.setTranno((add(chdrpf.getTranno(), 1)).toInt());//ILIFE-7968
		 pyoupfDAO.insertPyoupfRecord(pyoupf);
		 //ICIL-484
		
		
		}
	}

protected void executeChdrmja3020() {
	 SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
}

protected void createSurdclm3100()
	{
		create3100();
		readNextSubfile3150();
	}

protected void create3100()
	{
	
		if (isEQ(sv.estMatValue, ZERO) && isEQ(sv.actvalue, ZERO)) {
			return;
		}
		surdclmIO.setDataArea(SPACES);
		surdclmIO.setChdrcoy(chdrpf.getChdrcoy().toString());//ILIFE-7968
		surdclmIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
		surdclmIO.setTranno(chdrpf.getTranno());//ILIFE-7968
		setPrecision(surdclmIO.getTranno(), 0);
		surdclmIO.setTranno(add(surdclmIO.getTranno(), 1));
		surdclmIO.setPlanSuffix(covrpf.getPlanSuffix());
		surdclmIO.setLife(covrpf.getLife());
		surdclmIO.setCoverage(covrpf.getCoverage());
		surdclmIO.setRider(covrpf.getRider());
		surdclmIO.setCrtable(covrpf.getCrtable());
		surdclmIO.setShortds(sv.descrip);
		surdclmIO.setLiencd(covrpf.getLiencd());
		surdclmIO.setCurrcd(chdrpf.getCntcurr());//ILIFE-7968 /* IJTI-1386 */
		surdclmIO.setActvalue(sv.actvalue);
		surdclmIO.setFieldType(sv.fieldType);
		/*  MOVE SPACES                 TO SURDCLM-RIIND                 */
		surdclmIO.setVirtualFund(SPACES);
		surdclmIO.setEstMatValue(sv.estMatValue);
		surdclmIO.setFormat(formatsInner.surdclmrec);
		surdclmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surdclmIO.getParams());
			fatalError600();
		}
		/* For every SURDCLM written, write a HSUD                         */
		hsudIO.setParams(SPACES);
		hsudIO.setChdrcoy(surdclmIO.getChdrcoy());
		hsudIO.setChdrnum(surdclmIO.getChdrnum());
		hsudIO.setLife(surdclmIO.getLife());
		hsudIO.setCoverage(surdclmIO.getCoverage());
		hsudIO.setRider(surdclmIO.getRider());
		hsudIO.setPlanSuffix(surdclmIO.getPlanSuffix());
		hsudIO.setCrtable(surdclmIO.getCrtable());
		hsudIO.setJlife(surdclmIO.getJlife());
		hsudIO.setTranno(surdclmIO.getTranno());
		/* MOVE SH580-HACTVAL          TO HSUD-HACTVAL.         <R77>   */
		/* MOVE SH580-HEMV             TO HSUD-HEMV.            <R77>   */
		hsudIO.setHactval(sv.actvalue);
		hsudIO.setHemv(sv.estMatValue);
		hsudIO.setHcnstcur(chdrpf.getCntcurr());//ILIFE-7968 /* IJTI-1386 */
		hsudIO.setFieldType(sv.fieldType);
		hsudIO.setFormat(formatsInner.hsudrec);
		hsudIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hsudIO);
		if (isNE(hsudIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hsudIO.getParams());
			fatalError600();
		}
	}

protected void readNextSubfile3150()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SH580", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void createSurhclm3200()
	{
		surhclm3200();
	}

protected void surhclm3200()
	{
		surhclmIO.setDataArea(SPACES);
		surhclmIO.setChdrcoy(chdrpf.getChdrcoy().toString());//ILIFE-7968
		surhclmIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
		surhclmIO.setTranno(chdrpf.getTranno());//ILIFE-7968
		setPrecision(surhclmIO.getTranno(), 0);
		surhclmIO.setTranno(add(surhclmIO.getTranno(), 1));
		surhclmIO.setPlanSuffix(covrpf.getPlanSuffix());
		surhclmIO.setLife(covrpf.getLife());
		surhclmIO.setJlife(covrpf.getJlife());
		surhclmIO.setEffdate(payrIO.getPtdate());
		surhclmIO.setCurrcd(chdrpf.getCntcurr());//ILIFE-7968 /* IJTI-1386 */
		surhclmIO.setOtheradjst(sv.otheradjst);
		surhclmIO.setPolicyloan(ZERO);
		surhclmIO.setTaxamt(ZERO);
		surhclmIO.setTdbtamt(ZERO);
		surhclmIO.setZrcshamt(ZERO);
		surhclmIO.setReasoncd(SPACES);
		surhclmIO.setResndesc(SPACES);
		surhclmIO.setCnttype(chdrpf.getCnttype());//ILIFE-7968 /* IJTI-1386 */
		surhclmIO.setFormat(formatsInner.surhclmrec);
		surhclmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
	}

protected void defaultPcdt3300()
	{
		moveValues3300();
	}

protected void moveValues3300()
	{
		pcdtmjaIO.setDataArea(SPACES);
		pcdtmjaIO.setInstprem(ZERO);
		pcdtmjaIO.setPlanSuffix(ZERO);
		pcdtmjaIO.setSplitBcomm01(ZERO);
		pcdtmjaIO.setSplitBcomm02(ZERO);
		pcdtmjaIO.setSplitBcomm03(ZERO);
		pcdtmjaIO.setSplitBcomm04(ZERO);
		pcdtmjaIO.setSplitBcomm05(ZERO);
		pcdtmjaIO.setSplitBcomm06(ZERO);
		pcdtmjaIO.setSplitBcomm07(ZERO);
		pcdtmjaIO.setSplitBcomm08(ZERO);
		pcdtmjaIO.setSplitBcomm09(ZERO);
		pcdtmjaIO.setSplitBcomm10(ZERO);
		pcdtmjaIO.setTranno(ZERO);
		for (wsaaIndex.set(1); !(isLT(wsaaIndex, 10)); wsaaIndex.add(1)){
			pcdtmjaIO.setSplitc(wsaaIndex, 0);
		}
		pcdtmjaIO.setChdrcoy(chdrpf.getChdrcoy().toString());//ILIFE-7968
		pcdtmjaIO.setChdrnum(chdrpf.getChdrnum());//ILIFE-7968 /* IJTI-1386 */
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));//ILIFE-7968
		pcdtmjaIO.setLife(covrpf.getLife());
		pcdtmjaIO.setCoverage(covrpf.getCoverage());
		pcdtmjaIO.setRider(covrpf.getRider());
		pcdtmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		pcdtmjaIO.setInstprem(ZERO);
		setPrecision(pcdtmjaIO.getTranno(), 0);
		pcdtmjaIO.setTranno(add(chdrpf.getTranno(), 1));//ILIFE-7968
		wsaaIndex.set(1);
		pcdtmjaIO.setAgntnum(wsaaIndex, chdrpf.getAgntnum());//ILIFE-7968 /* IJTI-1386 */
		pcdtmjaIO.setSplitc(wsaaIndex, 100);
		pcdtmjaIO.setFunction(varcom.writr);
		pcdtmjaIO.setFormat(formatsInner.pcdtmjarec);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), varcom.oK)
		&& isNE(pcdtmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(pcdtmjaIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		/* MOVE SH580-CNSTCUR          TO ZRDP-CURRENCY.                */
		zrdecplrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e058 = new FixedLengthStringData(4).init("E058");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e656 = new FixedLengthStringData(4).init("E656");
	private FixedLengthStringData e694 = new FixedLengthStringData(4).init("E694");
	private FixedLengthStringData g542 = new FixedLengthStringData(4).init("G542");
	private FixedLengthStringData h053 = new FixedLengthStringData(4).init("H053");
	private FixedLengthStringData h143 = new FixedLengthStringData(4).init("H143");
	private FixedLengthStringData hl53 = new FixedLengthStringData(4).init("HL53");
	private FixedLengthStringData hl54 = new FixedLengthStringData(4).init("HL54");
	private FixedLengthStringData hl55 = new FixedLengthStringData(4).init("HL55");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
	private FixedLengthStringData rrfn = new FixedLengthStringData(4).init("RRFN");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData f906 = new FixedLengthStringData(4).init("F906");
	private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
	private FixedLengthStringData rf05 = new FixedLengthStringData(4).init("RF05");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData covtmjarec = new FixedLengthStringData(10).init("COVTMJAREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData lifemjarec = new FixedLengthStringData(10).init("LIFEMJAREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData pcdtmjarec = new FixedLengthStringData(10).init("PCDTMJAREC");
	private FixedLengthStringData surdclmrec = new FixedLengthStringData(10).init("SURDCLMREC");
	private FixedLengthStringData surhclmrec = new FixedLengthStringData(10).init("SURHCLMREC");
	private FixedLengthStringData hsudrec = new FixedLengthStringData(10).init("HSUDREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData uwlvrec = new FixedLengthStringData(10).init("UWLVREC");
}
}
