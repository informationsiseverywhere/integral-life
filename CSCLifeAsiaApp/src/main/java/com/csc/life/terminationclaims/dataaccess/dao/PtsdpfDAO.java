package com.csc.life.terminationclaims.dataaccess.dao;

import java.math.BigDecimal;
import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Ptsdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface PtsdpfDAO extends BaseDAO<Ptsdpf>{
	
	public List<Ptsdpf> serachPtsdRecord(Ptsdpf ptshObj);
	public void insertPtsdpfRecord(List<Ptsdpf> ptsdpflist);
	public Ptsdpf getPtsdpfByKey(Ptsdpf ptsdObj);
	public void updatePtsdpfForEMTByNo(long uniqueNumber, BigDecimal emv);
}
