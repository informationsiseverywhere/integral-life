/*
 * File: Pr687.java
 * Date: 30 August 2009 1:56:17
 * Author: Quipoz Limited
 * 
 * Class transformed from PR687.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.procedures.Tr687pt;
import com.csc.life.terminationclaims.screens.Sr687ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Tr687rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   Hospital Benefits
*   -----------------
*
*       DO NOT COMPILE THIS TABLE SCREEN WITH *XDD
*       COMPILE USING *DSPF IF ANY CHANGES TO THIS SCREEN
*
*
*   IMPORTANT NOTE - Please take note
*   ==============
*   This program has changed to be more dynamic as the data to be
*   entered from this screen exceeds the limit of GENAREA 500
*   characters.
*
*   The field BENEFITS has changed to output field in order to
*   reduce the storage requirement (limited 500 char).
*
*   In additional several fields have been added for more
*   information
*   For example: HOSBEN   - Hospital Benefit Code windowable
*                AMTLIFE  - Amount per Life Time
*                AMTYEAR  - Amount per Year
*                NOFDAY   - Maximum Number of Benefits
*                GDEDUCT  - Deductible Amount
*                COPAY    - Co-Payment %
*                AAD      - Annual Aggregate Deductible Amount
*                           at plan level
*
***********************************************************************
* </pre>
*/
public class Pr687 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR687");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);

	private FixedLengthStringData wsaaVariables = new FixedLengthStringData(32);
	private FixedLengthStringData wsaaBenDesc = new FixedLengthStringData(30).isAPartOf(wsaaVariables, 0);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).isAPartOf(wsaaVariables, 30);
	private String tr50a = "TR50A";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Tr687rec tr687rec = new Tr687rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr687ScreenVars sv = ScreenProgram.getScreenVars( Sr687ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public Pr687() {
		super();
		screenVars = sv;
		new ScreenModel("Sr687", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
					confirmationFields1050();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		initialize(wsaaVariables);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr687rec.tr687Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		tr687rec.aad.set(ZERO);
		tr687rec.amtlife01.set(ZERO);
		tr687rec.amtlife02.set(ZERO);
		tr687rec.amtlife03.set(ZERO);
		tr687rec.amtlife04.set(ZERO);
		tr687rec.amtlife05.set(ZERO);
		tr687rec.amtlife06.set(ZERO);
		tr687rec.amtyear01.set(ZERO);
		tr687rec.amtyear02.set(ZERO);
		tr687rec.amtyear03.set(ZERO);
		tr687rec.amtyear04.set(ZERO);
		tr687rec.amtyear05.set(ZERO);
		tr687rec.amtyear06.set(ZERO);
		tr687rec.benfamt01.set(ZERO);
		tr687rec.benfamt02.set(ZERO);
		tr687rec.benfamt03.set(ZERO);
		tr687rec.benfamt04.set(ZERO);
		tr687rec.benfamt05.set(ZERO);
		tr687rec.benfamt06.set(ZERO);
		tr687rec.copay01.set(ZERO);
		tr687rec.copay02.set(ZERO);
		tr687rec.copay03.set(ZERO);
		tr687rec.copay04.set(ZERO);
		tr687rec.copay05.set(ZERO);
		tr687rec.copay06.set(ZERO);
		tr687rec.factor.set(ZERO);
		tr687rec.gdeduct01.set(ZERO);
		tr687rec.gdeduct02.set(ZERO);
		tr687rec.gdeduct03.set(ZERO);
		tr687rec.gdeduct04.set(ZERO);
		tr687rec.gdeduct05.set(ZERO);
		tr687rec.gdeduct06.set(ZERO);
		tr687rec.nofday01.set(ZERO);
		tr687rec.nofday02.set(ZERO);
		tr687rec.nofday03.set(ZERO);
		tr687rec.nofday04.set(ZERO);
		tr687rec.nofday05.set(ZERO);
		tr687rec.nofday06.set(ZERO);
		tr687rec.premunit.set(ZERO);
		tr687rec.zssi.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.aad.set(tr687rec.aad);
		sv.amtlifes.set(tr687rec.amtlifes);
		sv.amtyears.set(tr687rec.amtyears);
		sv.benfamts.set(tr687rec.benfamts);
		sv.contitem.set(tr687rec.contitem);
		sv.copays.set(tr687rec.copays);
		sv.factor.set(tr687rec.factor);
		sv.gdeducts.set(tr687rec.gdeducts);
		sv.hosbens.set(tr687rec.hosbens);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.nofdays.set(tr687rec.nofdays);
		sv.premunit.set(tr687rec.premunit);
		sv.zssi.set(tr687rec.zssi);
	}

protected void confirmationFields1050()
	{
		for (ix.set(1); !(isGT(ix,6)); ix.add(1)){
			if (isNE(sv.hosben[ix.toInt()],SPACES)) {
				getBenefitDesc2100();
				sv.benefits[ix.toInt()].set(wsaaBenDesc);
			}
			else {
				sv.benefits[ix.toInt()].set(SPACES);
			}
		}
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*OTHER*/
		for (ix.set(1); !(isGT(ix,6)); ix.add(1)){
			if (isNE(sv.hosben[ix.toInt()],SPACES)) {
				getBenefitDesc2100();
				sv.benefits[ix.toInt()].set(wsaaBenDesc);
			}
			else {
				sv.benefits[ix.toInt()].set(SPACES);
			}
		}
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void getBenefitDesc2100()
	{
		start2110();
	}

protected void start2110()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tr50a);
		descIO.setDescitem(sv.hosben[ix.toInt()]);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaBenDesc.set(descIO.getLongdesc());
		}
		else {
			wsaaBenDesc.fill("?");
		}
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(tr687rec.tr687Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.aad,tr687rec.aad)) {
			tr687rec.aad.set(sv.aad);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.amtlifes,tr687rec.amtlifes)) {
			tr687rec.amtlifes.set(sv.amtlifes);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.amtyears,tr687rec.amtyears)) {
			tr687rec.amtyears.set(sv.amtyears);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.benfamts,tr687rec.benfamts)) {
			tr687rec.benfamts.set(sv.benfamts);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.contitem,tr687rec.contitem)) {
			tr687rec.contitem.set(sv.contitem);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.copays,tr687rec.copays)) {
			tr687rec.copays.set(sv.copays);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.factor,tr687rec.factor)) {
			tr687rec.factor.set(sv.factor);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.gdeducts,tr687rec.gdeducts)) {
			tr687rec.gdeducts.set(sv.gdeducts);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.hosbens,tr687rec.hosbens)) {
			tr687rec.hosbens.set(sv.hosbens);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.nofdays,tr687rec.nofdays)) {
			tr687rec.nofdays.set(sv.nofdays);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premunit,tr687rec.premunit)) {
			tr687rec.premunit.set(sv.premunit);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zssi,tr687rec.zssi)) {
			tr687rec.zssi.set(sv.zssi);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tr687pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
