/*
 * File: P5318at.java
 * Date: 30 August 2009 0:24:10
 * Author: Quipoz Limited
 *
 * Class transformed from P5318AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmhpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmhpf;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*               CLAIMS ADJUSTMENTS  - AT MODULE
*               -------------------------------
*
*   POSTINGS ARE AS FOLLOWS:
*  ( ON CHANGE OF CURRENCY )
*
*   CLAIMS AMOUNT.
*     01 - TOTAL CLAMAMT IN OLD CURRENCY
*     02 - TOTAL CLAMAMT IN NEW CURRENCY
*     03 - TOTAL OLD ADJUSTMENT IN NEW CURRENCY
*     04 - TOTAL OLD ADJUSTMENT IN NEW CURRENCY
*     05 - TOTAL NEW ADJUSTMENT IN NEW CURRENCY
*     06 - TOTAL NEW ADJUSTMENT IN NEW CURRENCY
*
*
*  ( ON DIFFERENCE OF ADJUSTMENT AMOUNT)
*
*   ADJUSTMENT AMOUNT.
*     03 - OLD ADJUSTMENT AMOUNT IN OLD CURRENCY
*     04 - OLD ADJUSTMENT AMOUNT IN OLD CURRENCY
*     05 - NEW ADJUSTMENT AMOUNT IN OLD CURRENCY
*     06 - NEW ADJUSTMENT AMOUNT IN OLD CURRENCY
*
*  This AT module is  called  by  the Death Claims program
*  P5256 and the Contract  number, is passed in the AT
*  parameters area as the "primary key".
*
*
*  AT PROCESSING
*
*  Amendments are  made  to  the  sub-accounts,  if the
*  currency and/or the 'Other adjustment amount' was changed.
*
*  CLAIM HEADER
*
*  Read the  Claim  Header  record  (CLMHCLM)  for  the
*  relevant contract.
*
*  CURRENCY CHANGE
*
*  If the  currency  is  changed,  then the following
*  processing occurs:-
*
*  Read table   T5645   (keyed  on  program  number),  this
*  contains the rules necessary for posting the monies
*  to  the  correct  accounts.  The  Outstanding (O/S)
*  Claims  amount  is  held  in  the (01) sub-account,
*  access  this sub-account and submit the money found
*  to the currency conversion subroutine.
*
*  Post the returned amounts as follows:
*
*  Post to sub-account  (01),  the O/S claims, with the old
*  currency amount with the sign from T3695. This will
*  reverse   the   amount   already  present  on  that
*  sub-account.
*
*  Post to  sub-account  (02),  the  old converted currency
*  amount.
*
*  Post to sub-account  (03),  the O/S claims, with the new
*  converted currency amount.
*
*  Post to  sub-account  (04),  the  new converted currency
*  amount.
*
*
*  PROCESS UTRNS (for change in currency)
*
*  Read the UTRNs  for  this transaction number and if they
*  have not  been   processed  by  the  'Feed  Back-loop',
*  i.e.  the 'FDBKIND' is  spaces,  then  alter the currency
*  code found to the new currency code.
*
*
*  CHANGE TO 'OTHER AMENDMENTS AMOUNT'
*
*  If there is  a  change  in  the other admentments amount,
*  the following processing occurs (if currency code also
*  changed do the processing for the currency conversion
*  first):
*
*  Read table   T5645   (keyed  on  program  number),  this
*  contains the rules necessary for posting the monies
*  to  the  correct  accounts.  The  Outstanding (O/S)
*  Claims amount is held in the (01) sub-account.
*
*  The other adjustment amount (OAA) from the AT linkage is
*  converted into the new currency and subtracted from
*  the new  OAA.  The  difference is the posted to the
*  relevant sub-accounts.
*
*  Post the returned amounts as follows:
*
*  Post to sub-account  (03),  the O/S claims, with the new
*  converted currency amount.
*
*  Post to  sub-account  (05),  the  adjustment  difference
*  amount,  i.e. the screen amount minus the converted
*  record amount.
*
*
*  GENERAL HOUSE-KEEPING
*
*  1) Write a PTRN transaction record:
*
*  - contract key from contract header
*  - transaction number from contract header
*  - transaction effective date equals todays
*  - batch key information from AT linkage.
*
*  2) Update the  batch  header  by  calling  BATCUP with a
*     function of WRITS and the following parameters:
*
*  - transaction count equals 1
*  - all other amounts zero
*  - batch key from AT linkage
*
*  3) Release contract SFTLOCK.
*
*
*****************************************************
* </pre>
*/
public class P5318at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5318AT");
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-SUBSCRIPTS */
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
		/* TABLES */
	private static final String t5671 = "T5671";
	private static final String t5645 = "T5645";
	private static final String clmhclmrec = "CLMHCLMREC";
	private static final String clmdclmrec = "CLMDCLMREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	protected ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM();
	protected ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected PtrnTableDAM ptrnIO = new PtrnTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	protected Varcom varcom = new Varcom();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	protected Atmodrec atmodrec = new Atmodrec();
	private SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();
	protected WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();
	boolean CMDTH010Permission  = false;
	private static final String feaConfigPreRegistartion= "CMDTH010";
	private Cattpf cattpf;
	protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO" , CattpfDAO.class);
	private String t6693 = "T6693";
	private T6693rec t6693rec = new T6693rec();
	private FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	private FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);
	private ZonedDecimalData index1 = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaClmstat = new FixedLengthStringData(2);
	private Itempf itempf = null;
	ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ClmhpfDAO clmhpfDAO = getApplicationContext().getBean("clmhpfDAO", ClmhpfDAO.class);

	public P5318at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		housekeepingTerminate3000();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(sysrSyserrRecInner.sysrStatuz, varcom.bomb)) {
			return ;
		}
		sysrSyserrRecInner.sysrSyserrStatuz.set(sysrSyserrRecInner.sysrStatuz);
		if (isNE(sysrSyserrRecInner.sysrSyserrType, "2")) {
			sysrSyserrRecInner.sysrSyserrType.set("1");
		}
		callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		readClaimHeader1050();
		readSubAccountTab1100();
		callDatcons1300();
		CMDTH010Permission  = FeaConfg.isFeatureExist(atmodrec.company.toString(), feaConfigPreRegistartion, appVars, "IT");
		/*EXIT*/
	}

	/**
	* <pre>
	* Initial read of the Claim Header.                           *
	* </pre>
	*/
protected void readClaimHeader1050()
	{
		readClaim1051();
	}

protected void readClaim1051()
	{
		/*  move the linkage area to working storage*/
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransAreaInner.wsaaTransArea.set(atmodrec.transArea);
		wsaaBatckey.set(atmodrec.batchKey);
		clmhclmIO.setDataArea(SPACES);
		clmhclmIO.setChdrcoy(atmodrec.company);
		clmhclmIO.setChdrnum(wsaaPrimaryChdrnum);
		clmhclmIO.setFunction("READH");
		clmhclmIO.setFormat(clmhclmrec);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(clmhclmIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Read table T5645 in order to obtain the sub account details *
	* </pre>
	*/
protected void readSubAccountTab1100()
	{
		go1101();
	}

protected void go1101()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(clmhclmIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("P5318");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(clmhclmIO.getChdrcoy());
		descIO.setDesctabl(t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("P5318");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(descIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Call DATCON1 subroutine.                                    *
	* </pre>
	*/
protected void callDatcons1300()
	{
		/*CALL-DATCONS*/
		/*DATCON1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}

	/**
	* <pre>
	* Do everything.                                              *
	* </pre>
	*/
protected void process2000()
	{
		/*PROCESS*/
		updateChdrlif3400();
		ptrnTransaction3100();
		genericProcessing2700();
		if(CMDTH010Permission)
			updateClaimStatus2130();
		/* Only do a posting if the 'Other Adjust' field on the screen     */
		/* has actually changed                                            */
		if (isEQ(wsaaTransAreaInner.wsaaOtheradjst, clmhclmIO.getOtheradjst())) {
			return ;
		}
		/* IF WSAA-CURRCD = CLMHCLM-CURRCD                              */
		/*     MOVE 3 TO WSAA-SUB                                       */
		/*PERFORM 2500-POST-SUB-ACCOUNT-3-4                        */
		/*     PERFORM 2200-POST-SUB-ACCOUNT-3-4                <CAS1.0>*/
		/*     MOVE 4 TO WSAA-SUB                                       */
		/*PERFORM 2500-POST-SUB-ACCOUNT-3-4                        */
		/*     PERFORM 2200-POST-SUB-ACCOUNT-3-4                <CAS1.0>*/
		/*     MOVE 5 TO WSAA-SUB                                       */
		/*     PERFORM 2400-POST-SUB-ACCOUNT-5-6                        */
		/*     MOVE 6 TO WSAA-SUB                                       */
		/*     PERFORM 2400-POST-SUB-ACCOUNT-5-6                        */
		wsaaSub.set(5);
		postSubAccount562400();
		wsaaSub.set(6);
		postSubAccount562400();
		/*EXIT*/
	}

	/**
	* <pre>
	* Currency has changed so post new amounts.                   *
	*2100-POST-SUB-ACCOUNT-1      SECTION.
	*2101-GO.
	*  post old claims amount to sub-account no 1 AND 2
	*    IF WSAA-CLAMAMT-OLD                = ZERO
	*        GO TO 2149-EXIT.
	*    MOVE ZERO                          TO LIFA-RCAMT,
	*                                          LIFA-CONTOT,
	*                                          LIFA-RCAMT,
	*                                          LIFA-FRCDATE,
	*                                          LIFA-TRANSACTION-DATE,
	*                                          LIFA-TRANSACTION-TIME,
	*                                          LIFA-USER.
	*    MOVE 'PSTW'                          TO LIFA-FUNCTION.
	*    MOVE WSAA-BATCKEY                    TO LIFA-BATCKEY.
	*    MOVE CLMHCLM-CHDRNUM                 TO LIFA-RDOCNUM.
	*    MOVE CLMHCLM-TRANNO                  TO LIFA-TRANNO.
	*    MOVE DESC-LONGDESC                   TO LIFA-TRANDESC.
	*    MOVE T5645-SACSCODE(WSAA-SUB)        TO LIFA-SACSCODE.
	*    MOVE T5645-SACSTYPE(WSAA-SUB)        TO LIFA-SACSTYP.
	*    MOVE T5645-GLMAP(WSAA-SUB)           TO LIFA-GLCODE.
	*    MOVE T5645-SIGN(WSAA-SUB)            TO LIFA-GLSIGN.
	*    MOVE T5645-CNTTOT(WSAA-SUB)          TO LIFA-CONTOT.
	*    MOVE ZERO                            TO LIFA-JRNSEQ.
	*    MOVE CLMHCLM-CHDRCOY                 TO LIFA-RLDGCOY,
	*                                            LIFA-GENLCOY.
	*    MOVE CLMHCLM-CHDRNUM                 TO LIFA-RLDGACCT.
	*    MOVE WSAA-CURRCD                     TO LIFA-ORIGCURR.
	*    MOVE WSAA-CLAMAMT-OLD                TO LIFA-ORIGAMT.
	*    MOVE SPACES                          TO LIFA-GENLCUR.
	*    MOVE CLMHCLM-CHDRCOY                 TO LIFA-GENLCOY.
	*    MOVE ZERO                            TO LIFA-ACCTAMT.
	*    MOVE ZERO                            TO LIFA-CRATE.
	*    MOVE SPACES                          TO LIFA-POSTYEAR,
	*                                            LIFA-POSTMONTH.
	*    MOVE CLMHCLM-TRANNO                  TO WSAA-TRANNO.
	*    MOVE WSAA-TRANNO                     TO LIFA-TRANREF.
	*    MOVE WSAA-TODAY             TO LIFA-EFFDATE.
	*    MOVE '99999999'             TO LIFA-FRCDATE.
	*    MOVE CLMHCLM-CNTTYPE    TO LIFA-SUBSTITUTE-CODE(01).
	*    MOVE WSAA-TERMID            TO LIFA-TERMID.
	*    MOVE WSAA-USER              TO LIFA-USER.
	*    MOVE WSAA-TRANSACTION-TIME  TO LIFA-TRANSACTION-TIME.
	*    MOVE WSAA-TRANSACTION-DATE  TO LIFA-TRANSACTION-DATE.
	*    CALL 'LIFACMV'              USING LIFA-LIFACMV-REC.
	*    IF LIFA-STATUZ              NOT = '****'
	*         MOVE LIFA-LIFACMV-REC  TO SYSR-PARAMS
	*         PERFORM XXXX-FATAL-ERROR.
	*2149-EXIT.
	*    EXIT.
	*2150-POST-SUB-ACCOUNT-2      SECTION.
	*2151-GO.
	*  post old claims amount to sub-account no 1 AND 2
	*    IF WSAA-CLAMAMT-NEW                = ZERO
	*        GO TO 2159-EXIT.
	*    MOVE ZERO                          TO LIFA-RCAMT,
	*                                          LIFA-CONTOT,
	*                                          LIFA-RCAMT,
	*                                          LIFA-FRCDATE,
	*                                          LIFA-TRANSACTION-DATE,
	*                                          LIFA-TRANSACTION-TIME,
	*                                          LIFA-USER.
	*    MOVE 'PSTW'                          TO LIFA-FUNCTION.
	*    MOVE WSAA-BATCKEY                    TO LIFA-BATCKEY.
	*    MOVE CLMHCLM-CHDRNUM                 TO LIFA-RDOCNUM.
	*    MOVE CLMHCLM-TRANNO                  TO LIFA-TRANNO.
	*    MOVE DESC-LONGDESC                   TO LIFA-TRANDESC.
	*    MOVE T5645-SACSCODE(WSAA-SUB)        TO LIFA-SACSCODE.
	*    MOVE T5645-SACSTYPE(WSAA-SUB)        TO LIFA-SACSTYP.
	*    MOVE T5645-GLMAP(WSAA-SUB)           TO LIFA-GLCODE.
	*    MOVE T5645-SIGN(WSAA-SUB)            TO LIFA-GLSIGN.
	*    MOVE T5645-CNTTOT(WSAA-SUB)          TO LIFA-CONTOT.
	*    MOVE ZERO                            TO LIFA-JRNSEQ.
	*    MOVE CLMHCLM-CHDRCOY                 TO LIFA-RLDGCOY,
	*                                            LIFA-GENLCOY.
	*    MOVE CLMHCLM-CHDRNUM                 TO LIFA-RLDGACCT.
	*    MOVE WSAA-CURRCD                     TO LIFA-ORIGCURR.
	*    MOVE WSAA-CLAMAMT-NEW                TO LIFA-ORIGAMT.
	*    MOVE SPACES                          TO LIFA-GENLCUR.
	*    MOVE CLMHCLM-CHDRCOY                 TO LIFA-GENLCOY.
	*    MOVE ZERO                            TO LIFA-ACCTAMT.
	*    MOVE ZERO                            TO LIFA-CRATE.
	*    MOVE SPACES                          TO LIFA-POSTYEAR,
	*                                            LIFA-POSTMONTH.
	*    MOVE CLMHCLM-TRANNO                  TO WSAA-TRANNO.
	*    MOVE WSAA-TRANNO                     TO LIFA-TRANREF.
	*    MOVE WSAA-TODAY             TO LIFA-EFFDATE.
	*    MOVE '99999999'             TO LIFA-FRCDATE.
	*    MOVE CLMHCLM-CNTTYPE    TO LIFA-SUBSTITUTE-CODE(01).
	*    MOVE WSAA-TERMID            TO LIFA-TERMID.
	*    MOVE WSAA-USER              TO LIFA-USER.
	*    MOVE WSAA-TRANSACTION-TIME  TO LIFA-TRANSACTION-TIME.
	*    MOVE WSAA-TRANSACTION-DATE  TO LIFA-TRANSACTION-DATE.
	*    CALL 'LIFACMV'              USING LIFA-LIFACMV-REC.
	*    IF LIFA-STATUZ              NOT = '****'
	*         MOVE LIFA-LIFACMV-REC  TO SYSR-PARAMS
	*         PERFORM XXXX-FATAL-ERROR.
	*2159-EXIT.
	*    EXIT.
	*2200-POST-SUB-ACCOUNT-3-4    SECTION.
	*2201-GO.
	***Post policy loan to sub-account no. 3 and 4,
	***if there is one
	**** IF CLMHCLM-POLICYLOAN              = ZERO
	****     GO TO 2290-EXIT.
	**** MOVE ZERO                          TO LIFA-RCAMT,
	****                                       LIFA-CONTOT,
	****                                       LIFA-RCAMT,
	****                                       LIFA-FRCDATE,
	****                                       LIFA-TRANSACTION-DATE,
	****                                       LIFA-TRANSACTION-TIME,
	****                                       LIFA-USER.
	**** MOVE 'PSTW'                          TO LIFA-FUNCTION.
	**** MOVE WSAA-BATCKEY                    TO LIFA-BATCKEY.
	**** MOVE CLMHCLM-CHDRNUM                 TO LIFA-RDOCNUM.
	**** MOVE CLMHCLM-TRANNO                  TO LIFA-TRANNO.
	**** MOVE DESC-LONGDESC                   TO LIFA-TRANDESC.
	**** MOVE T5645-SACSCODE(WSAA-SUB)        TO LIFA-SACSCODE.
	**** MOVE T5645-SACSTYPE(WSAA-SUB)        TO LIFA-SACSTYP.
	**** MOVE T5645-GLMAP(WSAA-SUB)           TO LIFA-GLCODE.
	**** MOVE T5645-SIGN(WSAA-SUB)            TO LIFA-GLSIGN.
	**** MOVE T5645-CNTTOT(WSAA-SUB)          TO LIFA-CONTOT.
	**** MOVE ZERO                            TO LIFA-JRNSEQ.
	**** MOVE CLMHCLM-CHDRCOY                 TO LIFA-RLDGCOY,
	****                                         LIFA-GENLCOY.
	**** MOVE CLMHCLM-CHDRNUM                 TO LIFA-RLDGACCT.
	**** MOVE CLMHCLM-CURRCD                  TO LIFA-ORIGCURR.
	**** MOVE CLMHCLM-POLICYLOAN              TO LIFA-ORIGAMT.
	**** MOVE SPACES                          TO LIFA-GENLCUR.
	**** MOVE CLMHCLM-CHDRCOY                 TO LIFA-GENLCOY.
	**** MOVE ZERO                            TO LIFA-ACCTAMT.
	**** MOVE ZERO                            TO LIFA-CRATE.
	**** MOVE SPACES                          TO LIFA-POSTYEAR,
	****                                         LIFA-POSTMONTH.
	**** MOVE CLMHCLM-TRANNO                  TO WSAA-TRANNO.
	**** MOVE WSAA-TRANNO                     TO LIFA-TRANREF.
	**** MOVE WSAA-TODAY             TO LIFA-EFFDATE.
	**** MOVE '99999999'             TO LIFA-FRCDATE.
	**** MOVE CLMHCLM-CNTTYPE    TO LIFA-SUBSTITUTE-CODE(01).
	**** MOVE WSAA-TERMID            TO LIFA-TERMID.
	**** MOVE WSAA-USER              TO LIFA-USER.
	**** MOVE WSAA-TRANSACTION-TIME  TO LIFA-TRANSACTION-TIME.
	**** MOVE WSAA-TRANSACTION-DATE  TO LIFA-TRANSACTION-DATE.
	**** CALL 'LIFACMV'              USING LIFA-LIFACMV-REC.
	**** IF LIFA-STATUZ              NOT = '****'
	****      MOVE LIFA-LIFACMV-REC  TO SYSR-PARAMS
	****      PERFORM XXXX-FATAL-ERROR.
	*2290-EXIT.
	**** EXIT.
	* </pre>
	*/
protected void postSubAccount342300()
	{
	}

protected void postSubAccount562400()
	{
		go2401();
	}

protected void go2401()
	{
		/*  Post the new adjustment amount to sub accounts 5*/
		/*  and 6, if there is one*/
		/*IF CLMHCLM-OTHERADJST       = ZERO                           */
		wsaaTransAreaInner.wsaaDiffadj.set(ZERO);
		/*COMPUTE WSAA-DIFFADJ = WSAA-OTHERADJST - CLMHCLM-OTHERADJST. */
		compute(wsaaTransAreaInner.wsaaDiffadj, 2).set(mult((sub(wsaaTransAreaInner.wsaaOtheradjst, clmhclmIO.getOtheradjst())), -1));
		if (isEQ(wsaaTransAreaInner.wsaaDiffadj, ZERO)) {
			return ;
		}
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batckey.set(wsaaBatckey);
		lifacmvrec1.rdocnum.set(clmhclmIO.getChdrnum());
		lifacmvrec1.tranno.set(clmhclmIO.getTranno());
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.sacscode.set(t5645rec.sacscode[wsaaSub.toInt()]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[wsaaSub.toInt()]);
		lifacmvrec1.glcode.set(t5645rec.glmap[wsaaSub.toInt()]);
		lifacmvrec1.glsign.set(t5645rec.sign[wsaaSub.toInt()]);
		lifacmvrec1.contot.set(t5645rec.cnttot[wsaaSub.toInt()]);
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec1.rldgacct.set(clmhclmIO.getChdrnum());
		lifacmvrec1.origcurr.set(clmhclmIO.getCurrcd());
		/* The amount to be posted is the difference between 'Other Adjust'*/
		/* on the screen and 'Other Adjust' on the file                    */
		/*MOVE CLMHCLM-OTHERADJST              TO LIFA-ORIGAMT.        */
		lifacmvrec1.origamt.set(wsaaTransAreaInner.wsaaDiffadj);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		wsaaTranno.set(clmhclmIO.getTranno());
		lifacmvrec1.tranref.set(wsaaTranno);
		lifacmvrec1.effdate.set(wsaaToday);
		lifacmvrec1.frcdate.set("99999999");
		lifacmvrec1.substituteCode[1].set(clmhclmIO.getCnttype());
		lifacmvrec1.termid.set(wsaaTransAreaInner.wsaaTermid);
		lifacmvrec1.user.set(wsaaTransAreaInner.wsaaUser);
		lifacmvrec1.transactionTime.set(wsaaTransAreaInner.wsaaTransactionTime);
		lifacmvrec1.transactionDate.set(wsaaTransAreaInner.wsaaTransactionDate);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, "****")) {
			sysrSyserrRecInner.sysrParams.set(lifacmvrec1.lifacmvRec);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	*2500-POST-SUB-ACCOUNT-3-4    SECTION.
	*2401-GO.
	*  post the old adjustment amount in old currency
	*  currency to sub accounts 3 and 4
	*    IF WSAA-OTHERADJST                    = ZERO
	*        GO TO 2590-EXIT.
	*    MOVE ZERO                          TO LIFA-RCAMT,
	*                                          LIFA-CONTOT,
	*                                          LIFA-RCAMT,
	*                                          LIFA-FRCDATE,
	*                                          LIFA-TRANSACTION-DATE,
	*                                          LIFA-TRANSACTION-TIME,
	*                                          LIFA-USER.
	*    MOVE 'PSTW'                          TO LIFA-FUNCTION.
	*    MOVE WSAA-BATCKEY                    TO LIFA-BATCKEY.
	*    MOVE CLMHCLM-CHDRNUM                 TO LIFA-RDOCNUM.
	*    MOVE CLMHCLM-TRANNO                  TO LIFA-TRANNO.
	*    MOVE DESC-LONGDESC                   TO LIFA-TRANDESC.
	*    MOVE T5645-SACSCODE(WSAA-SUB)        TO LIFA-SACSCODE.
	*    MOVE T5645-SACSTYPE(WSAA-SUB)        TO LIFA-SACSTYP.
	*    MOVE T5645-GLMAP(WSAA-SUB)           TO LIFA-GLCODE.
	*    MOVE T5645-SIGN(WSAA-SUB)            TO LIFA-GLSIGN.
	*    MOVE T5645-CNTTOT(WSAA-SUB)          TO LIFA-CONTOT.
	*    MOVE ZERO                            TO LIFA-JRNSEQ.
	*    MOVE CLMHCLM-CHDRCOY                 TO LIFA-RLDGCOY,
	*                                            LIFA-GENLCOY.
	*    MOVE CLMHCLM-CHDRNUM                 TO LIFA-RLDGACCT.
	*    MOVE CLMHCLM-CURRCD                  TO LIFA-ORIGCURR.
	*    MOVE WSAA-OTHERADJST                 TO LIFA-ORIGAMT.
	*    MOVE SPACES                          TO LIFA-GENLCUR.
	*    MOVE CLMHCLM-CHDRCOY                 TO LIFA-GENLCOY.
	*    MOVE ZERO                            TO LIFA-ACCTAMT.
	*    MOVE ZERO                            TO LIFA-CRATE.
	*    MOVE SPACES                          TO LIFA-POSTYEAR,
	*                                            LIFA-POSTMONTH.
	*    MOVE CLMHCLM-TRANNO                  TO WSAA-TRANNO.
	*    MOVE WSAA-TRANNO                     TO LIFA-TRANREF.
	*    MOVE WSAA-TODAY             TO LIFA-EFFDATE.
	*    MOVE '99999999'             TO LIFA-FRCDATE.
	*    MOVE CLMHCLM-CNTTYPE  TO LIFA-SUBSTITUTE-CODE(01).
	*    MOVE WSAA-TERMID            TO LIFA-TERMID.
	*    MOVE WSAA-USER              TO LIFA-USER.
	*    MOVE WSAA-TRANSACTION-TIME  TO LIFA-TRANSACTION-TIME.
	*    MOVE WSAA-TRANSACTION-DATE  TO LIFA-TRANSACTION-DATE.
	*    CALL 'LIFACMV'              USING LIFA-LIFACMV-REC.
	*    IF LIFA-STATUZ              NOT = '****'
	*         MOVE LIFA-LIFACMV-REC  TO SYSR-PARAMS
	*         PERFORM XXXX-FATAL-ERROR.
	*2590-EXIT.
	*    EXIT.
	*2600-CONVERT-ADJUSTMENT-AMOUNT  SECTION.
	*2601-GO.
	*    MOVE 'CVRT'                 TO CLNK-FUNCTION.
	*    MOVE SPACES                 TO CLNK-STATUZ.
	*    MOVE WSAA-CURRCD            TO CLNK-CURR-IN.
	*    MOVE VRCM-MAX-DATE          TO CLNK-CASHDATE.
	*    MOVE CLMHCLM-CURRCD         TO CLNK-CURR-OUT.
	*    MOVE WSAA-OTHERADJST        TO CLNK-AMOUNT-IN.
	*    MOVE ZERO                   TO CLNK-AMOUNT-OUT.
	*    MOVE CLMHCLM-CHDRCOY        TO CLNK-COMPANY.
	*    CALL 'XCVRT'                USING CLNK-CLNK002-REC.
	*    IF CLNK-STATUZ              NOT = '****'
	*        MOVE CLNK-CLNK002-REC   TO SYSR-PARAMS
	*        PERFORM                 XXXX-FATAL-ERROR.
	*2690-EXIT.
	*    EXIT.
	* Process the related CLMDS and call generic proc. sub-routines
	* </pre>
	*/
protected void genericProcessing2700()
	{
		/*COVERS*/
		clmdclmIO.setParams(SPACES);
		clmdclmIO.setChdrcoy(atmodrec.company);
		clmdclmIO.setChdrnum(clmdclmIO.getChdrnum());
		clmdclmIO.setFunction(varcom.begn);
		clmdclmIO.setFormat(clmdclmrec);
		while ( !(isEQ(clmdclmIO.getStatuz(), varcom.endp))) {


			//performance improvement --  atiwari23
			clmdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			clmdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

			readClaims2800();
		}

		/*EXIT*/
	}

protected void readClaims2800()
	{
		/*UPDATE*/
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.oK)
		&& isNE(clmdclmIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(clmdclmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(clmdclmIO.getStatuz(), varcom.endp)
		|| isNE(clmhclmIO.getChdrnum(), clmdclmIO.getChdrnum())
		|| isNE(atmodrec.company, clmdclmIO.getChdrcoy())) {
			clmdclmIO.setStatuz(varcom.endp);
			return ;
		}
		callGenericProc2900();
		clmdclmIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

	/**
	* <pre>
	* Read table T5671 to ge the generic processing sub-routines
	* </pre>
	*/
protected void callGenericProc2900()
	{
		go1051();
	}

protected void go1051()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(clmhclmIO.getChdrcoy());
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(wsaaBatckey.batcBatctrcde);
		wsaaT5671Crtable.set(clmdclmIO.getCrtable());
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFunction(varcom.readr);
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		wsaaSub.set(0);
		for (int loopVar1 = 0; !(loopVar1 == 4); loopVar1 += 1){
			callSubroutine2950();
		}
	}

protected void callSubroutine2950()
	{
		/*GO*/
		wsaaSub.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	* Housekeeping prior to termination.                          *
	* </pre>
	*/
protected void housekeepingTerminate3000()
	{
		/*HOUSEKEEPING-TERMINATE*/
		/* PTRN's only to be written if change has occurred, that is why   */
		/* the PERFORM has been moved to the end of the 2000- Section.     */
		/*PERFORM 3100-PTRN-TRANSACTION.                               */
		batchHeader3200();
		releaseSoftlock3300();
		/*EXIT*/
	}

	/**
	* <pre>
	* Write PTRN transaction.                                     *
	* </pre>
	*/
protected void ptrnTransaction3100()
	{
		ptrnTransaction3101();
	}

protected void ptrnTransaction3101()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		updateValidflagCustomerSpecific();
		ptrnIO.setTransactionDate(0);
		ptrnIO.setTransactionTime(0);
		ptrnIO.setUser(0);
		ptrnIO.setDataKey(atmodrec.batchKey);
		/*MOVE CLMHCLM-TRANNO         TO PTRN-TRANNO.                  */
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(clmhclmIO.getEffdate());
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(clmhclmIO.getChdrcoy());
		ptrnIO.setChdrnum(clmhclmIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransAreaInner.wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransAreaInner.wsaaTransactionTime);
		ptrnIO.setUser(wsaaTransAreaInner.wsaaUser);
		ptrnIO.setTermid(wsaaTransAreaInner.wsaaTermid);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptrnIO.getParams());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Write batch header transaction.                             *
	* </pre>
	*/
protected void batchHeader3200()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(batcuprec.batcupRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Release Soft locked record.                                 *
	* </pre>
	*/
protected void releaseSoftlock3300()
	{
		releaseSoftlock3301();
	}

protected void releaseSoftlock3301()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(clmhclmIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Update the CHDRLIF transaction number.
	* </pre>
	*/
protected void updateChdrlif3400()
	{
		readrChdrlif3410();
		updatChdrlif3430();
		writeChdrlif3450();
	}

protected void readrChdrlif3410()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(clmhclmIO.getChdrcoy());
		chdrlifIO.setChdrnum(clmhclmIO.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
	}

protected void updatChdrlif3430()
	{
		chdrlifIO.setValidflag("2");
		/* MOVE VRCM-MAX-DATE          TO CHDRLIF-CURRFROM.        <004>*/
		chdrlifIO.setCurrto(clmhclmIO.getEffdate());
		chdrlifIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
	}

protected void writeChdrlif3450()
	{
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(clmhclmIO.getEffdate());
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		chdrlifIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void updateClaimStatus2130()
{
	readT6693Table2700();
}

protected void readT6693Table2700(){
	Clmhpf clmhpf = clmhpfDAO.getClmhpfH(chdrlifIO.getChdrcoy().toString(),chdrlifIO.getChdrnum().toString());
	cattpf = cattpfDAO.selectRecords(chdrlifIO.getChdrcoy().toString(),chdrlifIO.getChdrnum().toString());
	if(null == cattpf){
		sysrSyserrRecInner.sysrParams.set(chdrlifIO.getChdrcoy().toString()+ chdrlifIO.getChdrnum().toString());
		xxxxFatalError();
	}	
	wsaaT6693Paystat.set(cattpf.getClamstat());
	wsaaT6693Crtable.set("****");
	itempf=new Itempf();
	itempf = itemDAO.findItemByItem(atmodrec.company.toString(),t6693,wsaaT6693Key.toString());
	if (null == itempf) {
		sysrSyserrRecInner.sysrParams.set(atmodrec.company.toString()+wsaaT6693Key.toString());
		xxxxFatalError();
	}
	else{
		t6693rec.t6693Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		index1.set(1);
		while ( !(isGT(index1, 12))) {
			findStatus3310();
		}
	}
	cattpfDAO.updateCattpf(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum().toString());
	cattpf.setTrcode(wsaaBatckey.batcBatctrcde.toString().trim());
	cattpf.setTranno(chdrlifIO.getTranno().toInt());
	cattpf.setClamstat(wsaaClmstat.toString());
	if(null != clmhpf) {
		cattpf.setCondte(clmhpf.getCondte());
		cattpf.setDtofdeath(clmhpf.getDtofdeath());
		cattpf.setCauseofdth(clmhpf.getCauseofdth().trim());
	}
	cattpf.setValidflag("1");
	cattpfDAO.insertCattpfRecord(cattpf);
}

protected void findStatus3310()
{
	/*FIND-STATUS*/
	if (isEQ(t6693rec.trcode[index1.toInt()], wsaaBatckey.batcBatctrcde)) {
		wsaaClmstat.set(t6693rec.rgpystat[index1.toInt()]);
		index1.set(15);
	}
	index1.add(1);
	/*EXIT*/
}
/*
 * Class transformed  from Data Structure WSAA-TRANS-AREA--INNER
 */
public static final class WsaaTransAreaInner {

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(215);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private PackedDecimalData wsaaClamamtOld = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 1);
	private PackedDecimalData wsaaClamamtNew = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 10);
	private PackedDecimalData wsaaOtheradjst = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 19);
	private PackedDecimalData wsaaDiffadj = new PackedDecimalData(17, 2).isAPartOf(wsaaTransArea, 28);
	private FixedLengthStringData wsaaCurrcd = new FixedLengthStringData(3).isAPartOf(wsaaTransArea, 37);
	public PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 40);
	public PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 44);
	public PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 48);
	public FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 52);
}
/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
private static final class SysrSyserrRecInner {

		/* System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(538);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	private FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrSyserrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 356);
	private FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 528).init("ITDMREC");
}
	protected void updateValidflagCustomerSpecific() {

	}

}
