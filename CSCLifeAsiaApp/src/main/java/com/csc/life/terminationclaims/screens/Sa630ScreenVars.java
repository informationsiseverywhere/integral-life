package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR50D
 * @version 1.0 generated on 30/08/09 07:14
 * @author Quipoz
 */
public class Sa630ScreenVars extends SmartVarModel { 


public FixedLengthStringData dataArea = new FixedLengthStringData(3052); //ILIFE-2472 
	public FixedLengthStringData dataFields = new FixedLengthStringData(3040).isAPartOf(dataArea, 0); //ILIFE-2472
	//Claim Notification
	public FixedLengthStringData notifinum = DD.aacct.copy().isAPartOf(dataFields,0);
	//Investigation Result
	public FixedLengthStringData accdesc = DD.accdesc1.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData datime = DD.crtdatime.copy().isAPartOf(dataFields,3014);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 3040);
	public FixedLengthStringData notifinumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData accdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData datimeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 82);
	public FixedLengthStringData[] notifinumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] accdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] datimeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sa630screenWritten = new LongData(0);
	public LongData Sa630protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sa630ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(notifinumOut,new String[] {"01","25","-01", null, null, null, null, null, null, null, null, null});	
		fieldIndMap.put(accdescOut,new String[] {"122","65","-122",null, null, null, null, null, null, null, null, null});

		screenFields = new BaseData[] {notifinum,accdesc,datime};
		screenOutFields = new BaseData[][] {notifinumOut,accdescOut,datimeOut};
		screenErrFields = new BaseData[] {notifinumErr,accdescErr,datimeErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sa630screen.class;
		protectRecord = Sa630protect.class;
	}
}
