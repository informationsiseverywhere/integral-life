package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.impl.ChdrpfDAOImpl;
import com.csc.fsu.general.dataaccess.dao.impl.PayrpfDAOImpl;
import  com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.dao.TpdbpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Tpdbpf;
import com.csc.life.contractservicing.procedures.Totloan;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.impl.CovrpfDAOImpl;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.HmtdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.MatdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.MathpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.MatypfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.MybkpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.HmtdpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.MatdpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.MathpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Hmtdpf;
import com.csc.life.terminationclaims.dataaccess.model.Matdpf;
import com.csc.life.terminationclaims.dataaccess.model.Mathpf;
import com.csc.life.terminationclaims.dataaccess.model.Matypf;
import com.csc.life.terminationclaims.recordstructures.Matccpy;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Br59g extends Mainb {
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR59G");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	
	protected FixedLengthStringData wsaaMatyFn = new FixedLengthStringData(10);
	protected FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaMatyFn, 0, FILLER).init("MATY");
	protected FixedLengthStringData wsaaMatyRunid = new FixedLengthStringData(2).isAPartOf(wsaaMatyFn, 4);
	protected ZonedDecimalData wsaaMatyJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaMatyFn, 6).setUnsigned();
	
	protected PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).init(0).setUnsigned();
	protected FixedLengthStringData wsaaMybkFn = new FixedLengthStringData(10);
	protected FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaMybkFn, 0, FILLER).init("MYBK");
	protected FixedLengthStringData wsaaMybkRunid = new FixedLengthStringData(2).isAPartOf(wsaaMybkFn, 4);
	protected ZonedDecimalData wsaaMybkJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaMybkFn, 6).setUnsigned();
	protected ZonedDecimalData wsaaSwitchFirst = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator firstTime = new Validator(wsaaSwitchFirst, 1);
	protected String ivrm = "IVRM";
	protected FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	protected FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	protected ZonedDecimalData wsaaSwitchPlan = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator wholePlan = new Validator(wsaaSwitchPlan, 1);
	private Validator partPlan = new Validator(wsaaSwitchPlan, 2);
	private Validator summaryPartPlan = new Validator(wsaaSwitchPlan, 3);
	public FixedLengthStringData wsaaShortds = new FixedLengthStringData(10);
	protected FixedLengthStringData wsaaCurrcd = new FixedLengthStringData(3);
	protected FixedLengthStringData Wsaahcurrcd = new FixedLengthStringData(3);
	protected ZonedDecimalData wsaaSwitchCurrency = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected Validator detailsSameCurrency = new Validator(wsaaSwitchCurrency, 0);
	private Validator detailsDifferent = new Validator(wsaaSwitchCurrency, 1);
	private Validator totalCurrDifferent = new Validator(wsaaSwitchCurrency, 2);
	protected ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	protected ZonedDecimalData wsaaLineno = new ZonedDecimalData(2, 0).setUnsigned();
	protected PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0);
	protected PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0);
	protected PackedDecimalData wsaaUser = new PackedDecimalData(6, 0);
	protected FixedLengthStringData wsaaTermid = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaflg = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(0);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
	protected FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaPlnsfx = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaPremCurrency = new FixedLengthStringData(3).init(SPACES);
	protected PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaPolicyLoan = new PackedDecimalData(17, 2).init(0);  
	protected PackedDecimalData wsaaActvalue = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaLoanValue = new PackedDecimalData(17, 2).init(0);
	protected FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	protected FixedLengthStringData wsaaCnstcur = new FixedLengthStringData(3).init(SPACES);
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Totloanrec totloanrec = new Totloanrec();
	protected Batckey wsaaBatchkey = new Batckey();

	/* CONTROL-TOTALS */
	private static final int ct01 = 1;

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	protected static final String tm604 = "TM604";
	
	protected Mathpf mathpf;
	protected Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	protected T6598rec t6598rec = new T6598rec();
	protected T5687rec t5687rec = new T5687rec();
	
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected Matypf matypf;
	protected Matdpf matdpf;
	protected Hmtdpf hmtdpf;
	private Tpdbpf tpdbpf;
	private Payrpf payrpf;
	private List<Covrpf> covrlist = new ArrayList<Covrpf>();
	protected List<Mathpf> mathpfList;
	protected List<Matdpf> matdpfList;
	protected List<Matdpf> matdpfList1;
	protected List<Hmtdpf> hmtdpfList;
	private List<Tpdbpf> tpdbpfList = new ArrayList<Tpdbpf>();
	protected List<Matypf> matypfList = new ArrayList<Matypf>();
	protected Covrpf covrpf = new Covrpf();
	private Covrpf covrpf1 = new Covrpf();
	protected Chdrpf chdrpf = new Chdrpf();
	protected Matccpy matccpy = new Matccpy();
	protected Zrdecplrec zrdecplrec = new Zrdecplrec();

	protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAOImpl.class);
	protected MatypfDAO matypfDAO = getApplicationContext().getBean("matypfDAO", MatypfDAO.class);
	protected MybkpfDAO MybkpfDao = getApplicationContext().getBean("mybkpfDAO", MybkpfDAO.class);
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAOImpl.class);
	protected MatdpfDAO matdpfDAO = getApplicationContext().getBean("matdpfDAO", MatdpfDAOImpl.class);
	private MathpfDAO mathpfDAO = getApplicationContext().getBean("mathpfDAO", MathpfDAOImpl.class);
	protected HmtdpfDAO hmtdpfDAO = getApplicationContext().getBean("hmtdpfDAO", HmtdpfDAOImpl.class);
	private TpdbpfDAO tpdbpfDAO = getApplicationContext().getBean("tpdbpfDAO", TpdbpfDAO.class);
	protected PayrpfDAO payrDAO = getApplicationContext().getBean("payrDAO", PayrpfDAOImpl.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	
    protected Map<String, List<Itempf>> t6598Map = null;
    protected Map<String, List<Itempf>> t3000Map = new HashMap<String, List<Itempf>>();
	private Map<String, List<Itempf>> tm604Map = new HashMap<String, List<Itempf>>();
	protected Map<String, List<Itempf>> t5687Map = new HashMap<String, List<Itempf>>();
	
	protected Iterator<Matypf> iteratorList;
	
	protected String matyTableName;
	protected String mybkTableName;

	protected String wsaaBillfreq;
	
	protected int wsaacount;
	protected BigDecimal wsaatdbtamt;
	protected BigDecimal wsaaClamant;
	protected BigDecimal wsaaActualTot;
	protected BigDecimal  wsaaEstimateTot;

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	protected Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData wsaadetail = new FixedLengthStringData(1).init("N");
	private static final Logger LOGGER = LoggerFactory.getLogger(Br59g.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, initialPrem3205, exit3209
	}
	
	
	

	public Br59g() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
			
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

protected void restart0900() {
		/* RESTART */
		/* EXIT */

	}

protected void initialise1000() {
		initialise1100();
}

protected void initialise1100()
{
		wsspEdterror.set(Varcom.oK);
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		wsaacount = 0;
		wsaaHeldCurrLoans.set(ZERO);
		wsaaShortds.set(SPACES);
		wsaaCnstcur.set(SPACES);
		wsaaCompany.set(bsprIO.getCompany().toString());
		varcom.vrcmTranid.set(batcdorrec.tranid);
		varcom.vrcmDate.set(getCobolDate());
	
				/*    Check the restart method is compatible with the program.*/
				/*    This program is restartable but will always re-run from*/
				/*    the beginning. This is because the extract file, GEXT, is*/
				/*    not under commitment control.*/
				if (isNE(bprdIO.getRestartMethod(), "1")) {
					syserrrec.statuz.set(ivrm);
					fatalError600();
				}
		wsaaMatyRunid.set(bprdIO.getSystemParam04());
		wsaaMatyJobno.set(bsscIO.getScheduleNumber());
	
		wsaaMybkRunid.set(bprdIO.getSystemParam04());
		wsaaMybkJobno.set(bsscIO.getScheduleNumber());
	
	    String coy = bsprIO.getCompany().toString();
	    String pfx = smtpfxcpy.item.toString();
	    
		t6598Map = itemDAO.loadSmartTable(pfx, coy, "T6598");
		t3000Map = itemDAO.loadSmartTable(pfx, coy, "T3000");
		t5687Map = itemDAO.loadSmartTable(pfx, coy, "T5687");
		matyTableName = wsaaMatyFn.trim();
		wsaacount = matypfDAO.getMatyCount(matyTableName);/* IJTI-1523 */
		mybkTableName = wsaaMybkFn.trim();
		MybkpfDao.deleteMybkpf(mybkTableName);/* IJTI-1523 */

	
}

protected void readFile2000()
{
	if(wsaacount > 0)
		{
			contotrec.totno.set(ct01);
			contotrec.totval.set(wsaacount);
			callProgram(Contot.class, contotrec.contotRec);
			wsspEdterror.set(Varcom.oK);
			wsaacount = 0;
			wsaaEof.set("N");

		}
		else
		{
			wsspEdterror.set(Varcom.endp);
		} 
}


protected void edit2500() {
	
	wsspEdterror.set(varcom.oK);
	
}

protected void  update3000(){

	matypfList = matypfDAO.getMatypfList(wsaaMatyFn.toString());
	iteratorList = matypfList.iterator();
	readMatyData3100();
	wsaaChdrChdrcoy.set(SPACES);
	wsaaChdrChdrnum.set(SPACES);
	mathpfList = new ArrayList<Mathpf>(); 
	matdpfList = new ArrayList<Matdpf>();  
	matdpfList1 = new ArrayList<Matdpf>();  
	hmtdpfList = new ArrayList<Hmtdpf>(); 
	while(!endOfFile.isTrue()){
		while ( (isNE(matypf.getChdrnum(), wsaaChdrChdrnum))){
			wsaaChdrChdrcoy.set(matypf.getChdrcoy());
			wsaaChdrChdrnum.set(matypf.getChdrnum());
			calcMaturityVal3200();
			if (t3000Map.containsKey(wsaaCurrcd)) {
				wsaaflg.set("Y");
				}	
			if (isNE(wsaaflg, "Y")) {
				matdpfList.clear();
				hmtdpfList.clear();
				itdmIO.setItemcoy(wsaaCompany);
				itdmIO.setItemtabl(tm604);
				itdmIO.setItemitem(wsaaCurrcd);
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.params.set(varcom.mrnf);
				fatalError600();
			}
			mathpf.setChdrnum(wsaaChdrChdrnum.toString());
			mathpf.setChdrcoy(wsaaChdrChdrcoy.toString());

			mathpfList.add(mathpf);
			}
			readMatyData3100();

			}
	}



protected void readMatyData3100() {
	
	if(iteratorList.hasNext()) {
		matypf = new Matypf();
		matypf = iteratorList.next();
		} 
	else {
			wsaaEof.set("Y");
		}
}


protected void calcMaturityVal3200(){
	
		wsaaSwitchPlan.set(ZERO);
		wsaaSwitchFirst.set(1);
		wsaaSwitchCurrency.set(ZERO);
		wsaaEstimateTot = BigDecimal.ZERO;
		wsaaActualTot = BigDecimal.ZERO;
		wsaaClamant  = BigDecimal.ZERO;
		wsaaCurrcd.set(SPACES);

		wsaaTranno.set(add(matypf.getTranno(),1));
		wsaaLineno.set(ZERO);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		mathpf = new Mathpf();
		mathpf.setTranno(wsaaTranno.toInt());
	
		mathpf.setTrdt(wsaaTransactionDate.toInt());
		mathpf.setTrtm(wsaaTransactionTime.toInt());
		mathpf.setUser_t(wsaaUser.toInt());
		mathpf.setTermid(wsaaTermid.toString());
		mathpf.setEffdate(wsaaEffdate.toInt());
		mathpf.setOtheradjst(BigDecimal.ZERO);
		mathpf.setReasoncd(SPACE);
		mathpf.setResndesc(SPACE);
		mathpf.setZrcshamt(BigDecimal.ZERO);
		wsaaRider.set(ZERO);
		wsaaPlnsfx.set(ZERO);	
		covrpf = covrpfDAO.getcovrincrRecord(matypf.getChdrcoy(), matypf.getChdrnum(),matypf.getLife(),matypf.getCoverage(),wsaaRider.toString(),wsaaPlnsfx.toInt());
		chdrpf = chdrpfDAO.getchdrRecordservunit(matypf.getChdrcoy(), matypf.getChdrnum());
		//payrpf = payrDAO.getBillfreq(matypf.getChdrcoy(), matypf.getChdrnum());
		wsaaBillfreq	= payrDAO.getBillfreq(matypf.getChdrcoy(), matypf.getChdrnum());
	
				mathpf.setPlnsfx(covrpf.getPlanSuffix());
				mathpf.setCurrcd(chdrpf.getCntcurr());
				mathpf.setCnttype(chdrpf.getCnttype());
				if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
					wsaaSwitchPlan.set(1);
				}
				else {
					if (isGT(covrpf.getPlanSuffix(),chdrpf.getPolsum())
						|| isEQ(chdrpf.getPolsum(),1)) {
						wsaaSwitchPlan.set(2);
						}
					else {
						wsaaSwitchPlan.set(3);
					}
					}
				if (wholePlan.isTrue()) {
					processWholePlan3300();
				}
				else {
					processPolicy3380();
				}
				wsaaPolicyLoan.set(0);
				getLoanDetails3420();
				wsaaPolicyLoan.set(wsaaHeldCurrLoans);
				if (!detailsSameCurrency.isTrue()) {
					wsaaPremCurrency.set(SPACES);
				} else {
					wsaaClamant = wsaaActualTot;
					wsaaClamant = wsaaClamant.subtract(wsaaPolicyLoan.getbigdata());
					wsaaCurrcd.set(chdrpf.getCntcurr());	
					Wsaahcurrcd.set(chdrpf.getCntcurr());
				}
				getPolicyDebt1980();
				mathpf.setPolicyloan(wsaaPolicyLoan.getbigdata());
				mathpf.setTdbtamt(wsaatdbtamt);
				wsaaClamant = wsaaClamant.subtract((wsaatdbtamt));
		
				if ( wsaaClamant.compareTo(BigDecimal.ZERO) < 0 ) {
					wsaaClamant = BigDecimal.ZERO;
				}
				
				mathpf.setClamamt(wsaaClamant);
				mathpf.setEstimtotal(wsaaEstimateTot);
						
	}
		

protected void processWholePlan3300()
{ 

	readCovrclm3310();
}
protected void  readCovrclm3310()

{
		wsaaChdrcoy.set(chdrpf.getChdrcoy());
		wsaaChdrnum.set(chdrpf.getChdrnum());
		covrpf.setPlanSuffix(0);
		covrlist = covrpfDAO.searchCovrrnlRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider(),covrpf.getPlanSuffix());/* IJTI-1523 */
		for (Covrpf covrpf1 : covrlist){
			processComponents3320(covrpf1);
		}
}

protected void processComponents3320(Covrpf covrpf1)
{
		wsaaLife.set(covrpf1.getLife());
		wsaaCoverage.set(covrpf1.getCoverage());
		wsaaRider.set(covrpf1.getRider());
		wsaaPremCurrency.set(covrpf1.getPremCurrency());
		matypf = matypfDAO.getMatyRecord(covrpf1.getChdrcoy(), covrpf1.getChdrnum(),covrpf1.getCoverage(),covrpf1.getRider(),covrpf1.getLife(),matyTableName);/* IJTI-1523 */
		readTableT65983330();
		readTableT56873340();
		loadMatc3350(covrpf1);
}

protected void readTableT65983330()
{
	
	if (t6598Map.containsKey(matypf.getMaturitycalcmeth())) {
		List<Itempf> t6598ItemList = t6598Map.get(matypf.getMaturitycalcmeth());
		Itempf t65987Item = t6598ItemList.get(0);
		t6598rec.t6598Rec.set(StringUtil.rawToString(t65987Item.getGenarea()));
		return;
	}
	fatalError600();
}

protected void readTableT56873340(){
	
	if (t5687Map.containsKey(covrpf.getCrtable())) {
		List<Itempf> t5687ItemList = t5687Map.get(covrpf.getCrtable());
		Itempf t5687Item = t5687ItemList.get(0);
		t5687rec.t5687Rec.set(StringUtil.rawToString(t5687Item.getGenarea()));
	}
	 else {
		 	fatalError600();
	 }
	
}

protected void loadMatc3350(Covrpf covrpf1)
{
		matccpy.maturityRec.set(SPACES);
		matccpy.batckey.set(batcdorrec.batchkey);
		matccpy.chdrChdrcoy.set(covrpf1.getChdrcoy());
		matccpy.chdrChdrnum.set(covrpf1.getChdrnum());
		matccpy.planSuffix.set(ZERO);
		matccpy.polsum.set(chdrpf.getPolsum());
		matccpy.lifeLife.set(covrpf1.getLife());
		matccpy.lifeJlife.set(covrpf1.getJlife());
		matccpy.covrCoverage.set(covrpf1.getCoverage());
		matccpy.covrRider.set(covrpf1.getRider());
		matccpy.crtable.set(covrpf1.getCrtable());
		matccpy.crrcd.set(covrpf1.getCrrcd());
		matccpy.effdate.set(covrpf1.getRiskCessDate());
		if (isEQ(matypf.getSinglepremind(),"Y")) {
			matccpy.billfreq.set("00");
		}
		else {
			matccpy.billfreq.set(wsaaBillfreq);
		}
		matccpy.chdrCurr.set(chdrpf.getCntcurr());
		matccpy.chdrType.set(chdrpf.getCnttype());
		if (isGT(covrpf.getInstprem(),ZERO)) {
			matccpy.singp.set(covrpf1.getInstprem());
			
		}
		else {
			matccpy.singp.set(covrpf1.getSingp());
		}
		matccpy.convUnits.set(covrpf1.getConvertInitialUnits());
		matccpy.language.set(bsscIO.getLanguage());
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		matccpy.currcode.set(covrpf1.getPremCurrency());
		matccpy.matCalcMeth.set(matypf.getMaturitycalcmeth());
		matccpy.pstatcode.set(covrpf1.getPstatcode());
		matccpy.planSwitch.set(wsaaSwitchPlan);
		matccpy.status.set(varcom.oK);
		matccpy.endf.set(SPACES);
		while ( !(isEQ(matccpy.status,varcom.endp)
		|| isEQ(matccpy.status,"NETM"))) {
			callMatMethodWhole3340(covrpf1);
	}

}

protected void callMatMethodWhole3340(Covrpf covrpf1)
{
	matdpf = new Matdpf();
	hmtdpf = new Hmtdpf();
	
	matdpf.setLife(covrpf1.getLife());
	matdpf.setJlife(covrpf1.getJlife());
	mathpf.setLife(covrpf1.getLife());
	mathpf.setJlife(covrpf1.getJlife());
		
	matdpf.setCoverage(covrpf1.getCoverage());
	matdpf.setRider(covrpf1.getRider());
	matdpf.setPlnsfx(covrpf.getPlanSuffix());

	callProgramX(t6598rec.calcprog, matccpy.maturityRec);
	if (isEQ(matccpy.status,varcom.bomb)) {
			syserrrec.statuz.set(matccpy.status);
			fatalError600();
		}
		if (isEQ(matccpy.status,"NETM")) {
			return ;
		}
		if (isEQ(matccpy.estimatedVal,ZERO)
		&& isEQ(matccpy.actualVal,ZERO)) {
			return ;
		}
		matdpf.setCrtable((matccpy.crtable).toString());
		wsaaShortds.set(matccpy.description);
		matdpf.setShortds(wsaaShortds.toString());
		zrdecplrec.amountIn.set(matccpy.estimatedVal);
		callRounding5000();
		matccpy.estimatedVal.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(matccpy.actualVal);
		callRounding5000();
		matccpy.actualVal.set(zrdecplrec.amountOut);
		if (isEQ(matccpy.currcode,SPACES)) {
			wsaaCnstcur.set(chdrpf.getCntcurr());
			matdpf.setCurrcd(chdrpf.getCntcurr());
			hmtdpf .setHcnstcur(chdrpf.getCntcurr());
		}
		else {
			wsaaCnstcur.set(matccpy.currcode);
			matdpf.setCurrcd((matccpy.currcode).toString());
			hmtdpf .setHcnstcur((matccpy.currcode).toString());


		}
		matdpf.setEmv(matccpy.estimatedVal.getbigdata());
		matdpf.setActvalue(matccpy.actualVal.getbigdata());
		matdpf.setType_t((matccpy.type).toString());
		matdpf.setVrtfund((matccpy.fund).toString());

		if (firstTime.isTrue()) {
			wsaaSwitchFirst.set(0);
			wsaaPremCurrency.set(wsaaCnstcur);
			wsaaEstimateTot = wsaaEstimateTot.add(matccpy.estimatedVal.getbigdata());
			wsaaActualTot = wsaaActualTot.add(matccpy.actualVal.getbigdata());
		}
		else {
			if (isEQ(wsaaCnstcur,wsaaPremCurrency)) {
				wsaaEstimateTot = wsaaEstimateTot.add(matccpy.estimatedVal.getbigdata());
				wsaaActualTot = wsaaActualTot.add(matccpy.actualVal.getbigdata());
			}
			else {
				wsaaEstimateTot = BigDecimal.ZERO;
				wsaaActualTot = BigDecimal.ZERO;
				wsaaSwitchCurrency.set(1);
			}
		}
		matdpf.setChdrnum(matypf.getChdrnum());
		matdpf.setChdrcoy(matypf.getChdrcoy());
		wsaaLineno.add(1);
		matdpf.setLiencd((wsaaLineno).toString());																																																						
		matdpf.setEffdate(wsaaEffdate.toInt());
		matdpf.setTranno(wsaaTranno.toInt());
		matdpf.setTrdt(wsaaTransactionDate.toInt());
		matdpf.setTrtm(wsaaTransactionTime.toInt());
		matdpf.setUser_t(wsaaUser.toInt());
		matdpf.setTermid(wsaaTermid.toString());
		matdpfList.add(matdpf);

		hmtdpf.setChdrcoy(matdpf.getChdrcoy());
		hmtdpf.setChdrnum(matdpf.getChdrnum());
		hmtdpf.setLife(matdpf.getLife());
		hmtdpf.setCoverage(matdpf.getCoverage());
		hmtdpf.setRider(matdpf.getRider());
		hmtdpf.setPlnsfx(matdpf.getPlnsfx());
		hmtdpf.setCrtable(matdpf.getCrtable());
		hmtdpf.setJlife(matdpf.getJlife());
		hmtdpf.setTranno(matdpf.getTranno());
		hmtdpf.setHactval(matccpy.actualVal.getbigdata());
		hmtdpf.setHemv(matccpy.estimatedVal.getbigdata());
		hmtdpf.setType_t(matdpf.getType_t());
		hmtdpfList.add(hmtdpf);
}

protected void processPolicy3380()
{
		wsaaPlanSuffix.set(covrpf.getPlanSuffix());
		if (summaryPartPlan.isTrue()) {
			wsaaPlanSuffix.set(ZERO);
		}
		covrpf.setPlanSuffix(0);
		covrlist = covrpfDAO.SearchRecordsforMatd(covrpf.getChdrcoy(), covrpf.getChdrnum(),covrpf.getPlanSuffix());
		for (Covrpf covrpf1: covrlist){
		processPartComponents3390(covrpf1);
		}
}

protected void processPartComponents3390(Covrpf covrpf1)
{
		matdpf.setLife(covrpf1.getLife()); 
		matdpf.setJlife(covrpf1.getJlife());
		matdpf.setCoverage(covrpf1.getCoverage());
		matdpf.setRider(covrpf1.getRider());
		
		matypf = matypfDAO.getMatyRecord(covrpf1.getChdrcoy(), covrpf1.getChdrnum(),covrpf1.getCoverage(),covrpf1.getRider(),covrpf1.getLife(),matyTableName);/* IJTI-1523 */
		readTableT65983330();
		
		if (t5687Map.containsKey(covrpf.getCrtable())) {
			List<Itempf> t5687ItemList = t5687Map.get(covrpf.getCrtable());
			Itempf t5687Item = t5687ItemList.get(0);
			t5687rec.t5687Rec.set(StringUtil.rawToString(t5687Item.getGenarea()));
		}
		 else {
			 	fatalError600();
		 }

		
		loadMatc3400(covrpf1);

}
protected void loadMatc3400(Covrpf covrpf1)
{
		matccpy.maturityRec.set(SPACES);
		matccpy.batckey.set(batcdorrec.batchkey);
		matccpy.chdrChdrcoy.set(covrpf1.getChdrcoy());
		matccpy.chdrChdrnum.set(covrpf1.getChdrnum());
		matccpy.planSuffix.set(covrpf1.getPlanSuffix());
		matccpy.polsum.set(chdrpf.getPolsum());
		matccpy.lifeLife.set(covrpf1.getLife());
		matccpy.lifeJlife.set(covrpf1.getJlife());
		matccpy.covrCoverage.set(covrpf1.getCoverage());
		matccpy.covrRider.set(covrpf1.getRider());
		matccpy.crtable.set(covrpf1.getCrtable());
		matccpy.crrcd.set(covrpf1.getCrrcd());
		matccpy.ptdate.set(chdrpf.getPtdate());
		matccpy.effdate.set(covrpf1.getRiskCessDate());
		if (isEQ(t5687rec.singlePremInd,"Y")) {
			matccpy.billfreq.set("00");
		}
		else {
			matccpy.billfreq.set(wsaaBillfreq);
		}
		matccpy.chdrCurr.set(chdrpf.getCntcurr());
		matccpy.chdrType.set(chdrpf.getCnttype());
		if (isGT(covrpf1.getInstprem(),0)) {
			matccpy.singp.set(covrpf1.getInstprem());
		}
		else {
			matccpy.singp.set(covrpf1.getSingp());
		}
		matccpy.convUnits.set(covrpf1.getConvertInitialUnits());
		matccpy.language.set(bsscIO.getLanguage());
		matccpy.estimatedVal.set(ZERO);
		matccpy.actualVal.set(ZERO);
		matccpy.currcode.set(covrpf1.getPremCurrency());
		matccpy.matCalcMeth.set(t5687rec.maturityCalcMeth);
		matccpy.pstatcode.set(covrpf1.getPstatcode());
		matccpy.planSwitch.set(wsaaSwitchPlan);	
		matccpy.status.set(varcom.oK);
		while ( !(isEQ(matccpy.status,varcom.endp))) {
			callMatCalcProg3410(covrpf1);
		}

}

protected void callMatCalcProg3410(Covrpf covrpf1)
{
		matdpf = new Matdpf();
		hmtdpf = new Hmtdpf();

		matdpf.setLife(covrpf1.getLife());
		matdpf.setJlife(covrpf1.getJlife());
		mathpf.setLife(covrpf1.getLife());
		mathpf.setJlife(covrpf1.getJlife());

		matdpf.setCoverage(covrpf1.getCoverage());
		matdpf.setRider(covrpf1.getRider());
		matdpf.setPlnsfx(covrpf.getPlanSuffix());

		callProgramX(t6598rec.calcprog, matccpy.maturityRec);
		if (isEQ(matccpy.status,varcom.bomb)) {
			syserrrec.statuz.set(matccpy.status);
			fatalError600();
		}
		if (isEQ(matccpy.status,"NETM")) {
			matccpy.status.set(varcom.endp);
			return ;
		}
		if (isEQ(matccpy.estimatedVal,ZERO)
		&& isEQ(matccpy.actualVal,ZERO)) {
			return ;
		}
		matdpf.setCrtable((matccpy.crtable).toString());
		wsaaShortds.set(matccpy.description);
		matdpf.setShortds(wsaaShortds.toString());
		if (isEQ(matccpy.currcode,SPACES)) {
			wsaaCnstcur.set(chdrpf.getCntcurr());
			matdpf.setCurrcd(chdrpf.getCntcurr());
			hmtdpf .setHcnstcur(chdrpf.getCntcurr());

		}
		else {
			wsaaCnstcur.set(matccpy.currcode);
			matdpf.setCurrcd((matccpy.currcode).toString());
			hmtdpf.setHcnstcur((matccpy.currcode).toString());
			}
		
		matdpf.setEmv(matccpy.estimatedVal.getbigdata());
		matdpf.setActvalue(matccpy.actualVal.getbigdata());
		matdpf.setType_t((matccpy.type).toString());
		matdpf.setVrtfund((matccpy.fund).toString());

		if (firstTime.isTrue()) {
			wsaaSwitchFirst.set(ZERO);
			wsaaPremCurrency.set(wsaaCnstcur);
			wsaaEstimateTot = wsaaEstimateTot.add(matccpy.estimatedVal.getbigdata());
			wsaaActualTot = wsaaActualTot.add(matccpy.actualVal.getbigdata());
		}
		else {
			if (isEQ(matccpy.currcode,wsaaPremCurrency)) {
				wsaaEstimateTot = wsaaEstimateTot.add(matccpy.estimatedVal.getbigdata());
				wsaaActualTot = wsaaActualTot.add(matccpy.actualVal.getbigdata());
			}
			else {
				wsaaEstimateTot = BigDecimal.ZERO;
				wsaaActualTot = BigDecimal.ZERO;
				wsaaSwitchCurrency.set(1);
			}
		}

		matdpf.setChdrnum(matypf.getChdrnum());
		matdpf.setChdrcoy(matypf.getChdrcoy());
		wsaaLineno.add(1);
		matdpf.setLiencd((wsaaLineno).toString());	
		matdpf.setEffdate(wsaaEffdate.toInt());
		matdpf.setTranno(wsaaTranno.toInt());
		matdpf.setTrdt(wsaaTransactionDate.toInt());
		matdpf.setTrtm(wsaaTransactionTime.toInt());
		matdpf.setUser_t(wsaaUser.toInt());
		matdpf.setTermid(wsaaTermid.toString());
		matdpfList.add(matdpf);

		hmtdpf.setChdrcoy(matdpf.getChdrcoy());
		hmtdpf.setChdrnum(matdpf.getChdrnum());
		hmtdpf.setLife(matdpf.getLife());
		hmtdpf.setCoverage(matdpf.getCoverage());
		hmtdpf.setRider(matdpf.getRider());
		hmtdpf.setPlnsfx(matdpf.getPlnsfx());
		hmtdpf.setCrtable(matdpf.getCrtable());
		hmtdpf.setJlife(matdpf.getJlife());
		hmtdpf.setTranno(matdpf.getTranno());
		hmtdpf.setHactval(matccpy.actualVal.getbigdata());
		hmtdpf.setHemv(matccpy.estimatedVal.getbigdata());
		hmtdpf.setType_t(matdpf.getType_t());
		hmtdpfList.add(hmtdpf);
		

}

protected void getLoanDetails3420()
{

		wsaaLoanValue.set(0);
		wsaaActvalue.set(0);

		matdpfList1 = matdpfDAO.getMatdRecord(Integer.parseInt(chdrpf.getChdrcoy().toString()), chdrpf.getChdrnum(),covrpf.getTranno());

	
		for(Matdpf matdpf :matdpfList1) {

	if (isNE(matdpf.getCurrcd(),chdrpf.getCntcurr())) {
		readjustMatd3430();
	}
	else {
		wsaaActvalue.set(matdpf.getActvalue());
	}
		wsaaLoanValue.add(wsaaActvalue);		
}


		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrpf.getChdrcoy());
		totloanrec.chdrnum.set(chdrpf.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(wsaaEffdate);
		totloanrec.language.set(bsscIO.getLanguage());
		callProgram(Totloan.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz,varcom.oK)) {
			syserrrec.params.set(totloanrec.totloanRec);
			syserrrec.statuz.set(totloanrec.statuz);
			fatalError600();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal,totloanrec.interest));
		
		if (isLT(wsaaLoanValue, wsaaHeldCurrLoans))
		 {
			wsaaHeldCurrLoans.subtract(wsaaLoanValue);
		}
		else {
			wsaaHeldCurrLoans.set(0);
		}

}
	

protected void readjustMatd3430()
{
	if (isEQ(matdpf.getActvalue(),ZERO)) {
			wsaaActvalue.set(ZERO);
			return;
		}
		conlinkrec.function.set("SURR");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(matdpf.getCurrcd());
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(chdrpf.getCntcurr());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(matdpf.getActvalue());
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		wsaaActvalue.set(conlinkrec.amountOut);

}
protected void getPolicyDebt1980()
{
		wsaatdbtamt = BigDecimal.ZERO;
		tpdbpfList = tpdbpfDAO.getTpdbtamt((chdrpf.getChdrcoy()).toString(),chdrpf.getChdrnum());
		if (tpdbpfList.isEmpty()){
			return;
		}
		
		for(Tpdbpf tpdbpf : tpdbpfList){
			wsaatdbtamt.add(tpdbpf.getTdbtamt());
		}
}



protected void commit3500()
{
	if(matdpfList != null){
		matdpfDAO.insertMatdpf(matdpfList);
	}
	if (hmtdpfList != null){
		hmtdpfDAO.insertHmtdpf(hmtdpfList);
	}
	if (mathpfList != null){
		mathpfDAO.insertMathpf(mathpfList);
	}
	
}
	

protected void rollback3600() {
	/* ROLLBACK */
	/* EXIT */

}


protected void close4000() {

	/* CLOSE-FILES */
		if (matdpfList != null){
			matdpfList.clear();
		}
			matdpfList = null;
			
		if (mathpfList != null){
			mathpfList.clear();
		}
			mathpfList = null;
			
		if (hmtdpfList != null){
			hmtdpfList.clear();
		}
			hmtdpfList = null;
		
		if (t6598Map != null) {
			t6598Map.clear();
	    }
		t6598Map = null;
		
		if (tm604Map != null) {
			tm604Map.clear();
	    }
		tm604Map = null;
		
		if (t3000Map != null) {
			t3000Map.clear();
	    }
		t3000Map = null;
		
		if (t5687Map != null) {
			t5687Map.clear();
	    }
		t5687Map = null;
	
	lsaaStatuz.set(varcom.oK);
	}

protected void callRounding5000()
{
	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(wsaaCompany);
	zrdecplrec.statuz.set(varcom.oK);
	/* MOVE 'NTD'                  TO ZRDP-CURRENCY.                */
	zrdecplrec.currency.set(matccpy.chdrCurr);
	zrdecplrec.batctrcde.set(wsaaBatchkey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		syserrrec.params.set(zrdecplrec.zrdecplRec);
		fatalError600();
	}
	/*EXIT*/
}

}

