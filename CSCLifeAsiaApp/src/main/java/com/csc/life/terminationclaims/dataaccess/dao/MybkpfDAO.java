package com.csc.life.terminationclaims.dataaccess.dao;

import com.csc.life.terminationclaims.dataaccess.model.Mybkpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MybkpfDAO extends BaseDAO<Mybkpf> {
	
	public void deleteMybkpf(String tableName);

}
