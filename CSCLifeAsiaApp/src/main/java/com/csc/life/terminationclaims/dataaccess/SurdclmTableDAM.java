package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: SurdclmTableDAM.java
 * Date: Sun, 30 Aug 2009 03:49:10
 * Class transformed from SURDCLM.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class SurdclmTableDAM extends SurdpfTableDAM {

	public SurdclmTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("SURDCLM");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO"
		             + ", PLNSFX"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", CRTABLE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "TRANNO, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "CRTABLE, " +
		            "SHORTDS, " +
		            "LIENCD, " +
		            "CURRCD, " +
		            "PLNSFX, " +
		            "EMV, " +
		            "ACTVALUE, " +
		            "TYPE_T, " +
		            "VRTFUND, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "OTHERADJST," +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO ASC, " +
		            "PLNSFX ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "CRTABLE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO DESC, " +
		            "PLNSFX DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "CRTABLE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               tranno,
                               life,
                               jlife,
                               coverage,
                               rider,
                               crtable,
                               shortds,
                               liencd,
                               currcd,
                               planSuffix,
                               estMatValue,
                               actvalue,
                               fieldType,
                               virtualFund,
                               userProfile,
                               jobName,
                               datime,
                               otheradjst,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(39);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ getPlanSuffix().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getCrtable().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller130 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(tranno.toInternal());
	nonKeyFiller40.setInternal(life.toInternal());
	nonKeyFiller60.setInternal(coverage.toInternal());
	nonKeyFiller70.setInternal(rider.toInternal());
	nonKeyFiller80.setInternal(crtable.toInternal());
	nonKeyFiller130.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(111);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getJlife().toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ getShortds().toInternal()
					+ getLiencd().toInternal()
					+ getCurrcd().toInternal()
					+ nonKeyFiller130.toInternal()
					+ getEstMatValue().toInternal()
					+ getActvalue().toInternal()
					+ getFieldType().toInternal()
					+ getVirtualFund().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getOtheradjst().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, shortds);
			what = ExternalData.chop(what, liencd);
			what = ExternalData.chop(what, currcd);
			what = ExternalData.chop(what, nonKeyFiller130);
			what = ExternalData.chop(what, estMatValue);
			what = ExternalData.chop(what, actvalue);
			what = ExternalData.chop(what, fieldType);
			what = ExternalData.chop(what, virtualFund);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);	
			what = ExternalData.chop(what, otheradjst);	
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getShortds() {
		return shortds;
	}
	public void setShortds(Object what) {
		shortds.set(what);
	}	
	public FixedLengthStringData getLiencd() {
		return liencd;
	}
	public void setLiencd(Object what) {
		liencd.set(what);
	}	
	public FixedLengthStringData getCurrcd() {
		return currcd;
	}
	public void setCurrcd(Object what) {
		currcd.set(what);
	}	
	public PackedDecimalData getEstMatValue() {
		return estMatValue;
	}
	public void setEstMatValue(Object what) {
		setEstMatValue(what, false);
	}
	public void setEstMatValue(Object what, boolean rounded) {
		if (rounded)
			estMatValue.setRounded(what);
		else
			estMatValue.set(what);
	}	
	public PackedDecimalData getActvalue() {
		return actvalue;
	}
	public void setActvalue(Object what) {
		setActvalue(what, false);
	}
	public void setActvalue(Object what, boolean rounded) {
		if (rounded)
			actvalue.setRounded(what);
		else
			actvalue.set(what);
	}	
	public FixedLengthStringData getFieldType() {
		return fieldType;
	}
	public void setFieldType(Object what) {
		fieldType.set(what);
	}	
	public FixedLengthStringData getVirtualFund() {
		return virtualFund;
	}
	public void setVirtualFund(Object what) {
		virtualFund.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	public PackedDecimalData getOtheradjst() {
		return otheradjst;
	}
	public void setOtheradjst(Object what) {
		otheradjst.set(what);
	}	
	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		planSuffix.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		crtable.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		jlife.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		shortds.clear();
		liencd.clear();
		currcd.clear();
		nonKeyFiller130.clear();
		estMatValue.clear();
		actvalue.clear();
		fieldType.clear();
		virtualFund.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
		otheradjst.clear();
	}


}