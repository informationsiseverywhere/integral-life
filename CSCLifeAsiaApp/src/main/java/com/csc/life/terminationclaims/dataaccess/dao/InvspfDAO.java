package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;


public interface InvspfDAO extends BaseDAO<Invspf>{
	
	public boolean insertInvspf(Invspf invspf);
	
	public int removeInvspfRecord(long uniqueNumber);
	
	public int updateInvspf(Invspf invspf);
	
	public List<Invspf> getInvspfList(String clnncoy, String notifinum, String claimno);
	
	public int updateInvspfClaimno(String claimno, String notifinum);
	
	public List<Invspf> getInvspfByNotifinum(String clnncoy, String notifinum); //IBPLIFE-2338
}
