package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:47
 * Description:
 * Copybook name: CLMHCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Clmhclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData clmhclmFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData clmhclmKey = new FixedLengthStringData(256).isAPartOf(clmhclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData clmhclmChdrcoy = new FixedLengthStringData(1).isAPartOf(clmhclmKey, 0);
  	public FixedLengthStringData clmhclmChdrnum = new FixedLengthStringData(8).isAPartOf(clmhclmKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(clmhclmKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(clmhclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		clmhclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}