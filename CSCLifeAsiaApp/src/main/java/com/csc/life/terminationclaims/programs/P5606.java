/*
 * File: P5606.java
 * Date: 30 August 2009 0:31:36
 * Author: Quipoz Limited
 * 
 * Class transformed from P5606.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.procedures.T5606pt;
import com.csc.life.terminationclaims.screens.S5606ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5606 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5606");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	protected T5606rec t5606rec = getTrec();//new T5606rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5606ScreenVars sv = getPScreenVars();//ScreenProgram.getScreenVars( S5606ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		other3080, 
		exit3090
	}

	public P5606() {
		super();
		screenVars = sv;
		new ScreenModel("S5606", AppVars.getInstance(), sv);
	}
	
	protected S5606ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( S5606ScreenVars.class);
	}
	
	protected T5606rec getTrec() {
		return new T5606rec();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5606rec.t5606Rec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5606rec.ageIssageFrm01.set(ZERO);
		t5606rec.ageIssageFrm02.set(ZERO);
		t5606rec.ageIssageFrm03.set(ZERO);
		t5606rec.ageIssageFrm04.set(ZERO);
		t5606rec.ageIssageFrm05.set(ZERO);
		t5606rec.ageIssageFrm06.set(ZERO);
		t5606rec.ageIssageFrm07.set(ZERO);
		t5606rec.ageIssageFrm08.set(ZERO);
		t5606rec.ageIssageTo01.set(ZERO);
		t5606rec.ageIssageTo02.set(ZERO);
		t5606rec.ageIssageTo03.set(ZERO);
		t5606rec.ageIssageTo04.set(ZERO);
		t5606rec.ageIssageTo05.set(ZERO);
		t5606rec.ageIssageTo06.set(ZERO);
		t5606rec.ageIssageTo07.set(ZERO);
		t5606rec.ageIssageTo08.set(ZERO);
		t5606rec.benCessageFrom01.set(ZERO);
		t5606rec.benCessageFrom02.set(ZERO);
		t5606rec.benCessageFrom03.set(ZERO);
		t5606rec.benCessageFrom04.set(ZERO);
		t5606rec.benCessageFrom05.set(ZERO);
		t5606rec.benCessageFrom06.set(ZERO);
		t5606rec.benCessageFrom07.set(ZERO);
		t5606rec.benCessageFrom08.set(ZERO);
		t5606rec.benCessageTo01.set(ZERO);
		t5606rec.benCessageTo02.set(ZERO);
		t5606rec.benCessageTo03.set(ZERO);
		t5606rec.benCessageTo04.set(ZERO);
		t5606rec.benCessageTo05.set(ZERO);
		t5606rec.benCessageTo06.set(ZERO);
		t5606rec.benCessageTo07.set(ZERO);
		t5606rec.benCessageTo08.set(ZERO);
		t5606rec.benCesstermFrm01.set(ZERO);
		t5606rec.benCesstermFrm02.set(ZERO);
		t5606rec.benCesstermFrm03.set(ZERO);
		t5606rec.benCesstermFrm04.set(ZERO);
		t5606rec.benCesstermFrm05.set(ZERO);
		t5606rec.benCesstermFrm06.set(ZERO);
		t5606rec.benCesstermFrm07.set(ZERO);
		t5606rec.benCesstermFrm08.set(ZERO);
		t5606rec.benCesstermTo01.set(ZERO);
		t5606rec.benCesstermTo02.set(ZERO);
		t5606rec.benCesstermTo03.set(ZERO);
		t5606rec.benCesstermTo04.set(ZERO);
		t5606rec.benCesstermTo05.set(ZERO);
		t5606rec.benCesstermTo06.set(ZERO);
		t5606rec.benCesstermTo07.set(ZERO);
		t5606rec.benCesstermTo08.set(ZERO);
		t5606rec.dfrprd.set(ZERO);
		t5606rec.premCessageFrom01.set(ZERO);
		t5606rec.premCessageFrom02.set(ZERO);
		t5606rec.premCessageFrom03.set(ZERO);
		t5606rec.premCessageFrom04.set(ZERO);
		t5606rec.premCessageFrom05.set(ZERO);
		t5606rec.premCessageFrom06.set(ZERO);
		t5606rec.premCessageFrom07.set(ZERO);
		t5606rec.premCessageFrom08.set(ZERO);
		t5606rec.premCessageTo01.set(ZERO);
		t5606rec.premCessageTo02.set(ZERO);
		t5606rec.premCessageTo03.set(ZERO);
		t5606rec.premCessageTo04.set(ZERO);
		t5606rec.premCessageTo05.set(ZERO);
		t5606rec.premCessageTo06.set(ZERO);
		t5606rec.premCessageTo07.set(ZERO);
		t5606rec.premCessageTo08.set(ZERO);
		t5606rec.premCesstermFrom01.set(ZERO);
		t5606rec.premCesstermFrom02.set(ZERO);
		t5606rec.premCesstermFrom03.set(ZERO);
		t5606rec.premCesstermFrom04.set(ZERO);
		t5606rec.premCesstermFrom05.set(ZERO);
		t5606rec.premCesstermFrom06.set(ZERO);
		t5606rec.premCesstermFrom07.set(ZERO);
		t5606rec.premCesstermFrom08.set(ZERO);
		t5606rec.premCesstermTo01.set(ZERO);
		t5606rec.premCesstermTo02.set(ZERO);
		t5606rec.premCesstermTo03.set(ZERO);
		t5606rec.premCesstermTo04.set(ZERO);
		t5606rec.premCesstermTo05.set(ZERO);
		t5606rec.premCesstermTo06.set(ZERO);
		t5606rec.premCesstermTo07.set(ZERO);
		t5606rec.premCesstermTo08.set(ZERO);
		t5606rec.riskCessageFrom01.set(ZERO);
		t5606rec.riskCessageFrom02.set(ZERO);
		t5606rec.riskCessageFrom03.set(ZERO);
		t5606rec.riskCessageFrom04.set(ZERO);
		t5606rec.riskCessageFrom05.set(ZERO);
		t5606rec.riskCessageFrom06.set(ZERO);
		t5606rec.riskCessageFrom07.set(ZERO);
		t5606rec.riskCessageFrom08.set(ZERO);
		t5606rec.riskCessageTo01.set(ZERO);
		t5606rec.riskCessageTo02.set(ZERO);
		t5606rec.riskCessageTo03.set(ZERO);
		t5606rec.riskCessageTo04.set(ZERO);
		t5606rec.riskCessageTo05.set(ZERO);
		t5606rec.riskCessageTo06.set(ZERO);
		t5606rec.riskCessageTo07.set(ZERO);
		t5606rec.riskCessageTo08.set(ZERO);
		t5606rec.riskCesstermFrom01.set(ZERO);
		t5606rec.riskCesstermFrom02.set(ZERO);
		t5606rec.riskCesstermFrom03.set(ZERO);
		t5606rec.riskCesstermFrom04.set(ZERO);
		t5606rec.riskCesstermFrom05.set(ZERO);
		t5606rec.riskCesstermFrom06.set(ZERO);
		t5606rec.riskCesstermFrom07.set(ZERO);
		t5606rec.riskCesstermFrom08.set(ZERO);
		t5606rec.riskCesstermTo01.set(ZERO);
		t5606rec.riskCesstermTo02.set(ZERO);
		t5606rec.riskCesstermTo03.set(ZERO);
		t5606rec.riskCesstermTo04.set(ZERO);
		t5606rec.riskCesstermTo05.set(ZERO);
		t5606rec.riskCesstermTo06.set(ZERO);
		t5606rec.riskCesstermTo07.set(ZERO);
		t5606rec.riskCesstermTo08.set(ZERO);
		t5606rec.sumInsMax.set(ZERO);
		t5606rec.sumInsMin.set(ZERO);
		t5606rec.termIssageFrm01.set(ZERO);
		t5606rec.termIssageFrm02.set(ZERO);
		t5606rec.termIssageFrm03.set(ZERO);
		t5606rec.termIssageFrm04.set(ZERO);
		t5606rec.termIssageFrm05.set(ZERO);
		t5606rec.termIssageFrm06.set(ZERO);
		t5606rec.termIssageFrm07.set(ZERO);
		t5606rec.termIssageFrm08.set(ZERO);
		t5606rec.termIssageTo01.set(ZERO);
		t5606rec.termIssageTo02.set(ZERO);
		t5606rec.termIssageTo03.set(ZERO);
		t5606rec.termIssageTo04.set(ZERO);
		t5606rec.termIssageTo05.set(ZERO);
		t5606rec.termIssageTo06.set(ZERO);
		t5606rec.termIssageTo07.set(ZERO);
		t5606rec.termIssageTo08.set(ZERO);
		t5606rec.waitperiod01.set(SPACES);
		t5606rec.waitperiod09.set(SPACES);
		t5606rec.waitperiod02.set(SPACES);
		t5606rec.waitperiod03.set(SPACES);
		t5606rec.waitperiod04.set(SPACES);
		t5606rec.waitperiod05.set(SPACES);
		t5606rec.waitperiod06.set(SPACES);
		t5606rec.waitperiod07.set(SPACES);
		t5606rec.waitperiod08.set(SPACES);
		t5606rec.bentrm01.set(SPACES);
		t5606rec.bentrm02.set(SPACES);
		t5606rec.bentrm03.set(SPACES);
		t5606rec.bentrm04.set(SPACES);
		t5606rec.bentrm05.set(SPACES);
		t5606rec.bentrm06.set(SPACES);
		t5606rec.bentrm07.set(SPACES);
		t5606rec.bentrm08.set(SPACES);
		t5606rec.bentrm09.set(SPACES);
		t5606rec.poltyp01.set(SPACES);
		t5606rec.poltyp02.set(SPACES);
		t5606rec.poltyp03.set(SPACES);
		t5606rec.poltyp04.set(SPACES);
		t5606rec.poltyp05.set(SPACES);
		t5606rec.poltyp06.set(SPACES);
		t5606rec.prmbasis01.set(SPACES);
		t5606rec.prmbasis02.set(SPACES);
		t5606rec.prmbasis03.set(SPACES);
		t5606rec.prmbasis04.set(SPACES);
		t5606rec.prmbasis05.set(SPACES);
		t5606rec.prmbasis06.set(SPACES);
		
	}

protected void generalArea1045()
	{
		sv.ageIssageFrms.set(t5606rec.ageIssageFrms);
		sv.ageIssageTos.set(t5606rec.ageIssageTos);
		sv.benCessageFroms.set(t5606rec.benCessageFroms);
		sv.benCessageTos.set(t5606rec.benCessageTos);
		sv.benCesstermFrms.set(t5606rec.benCesstermFrms);
		sv.benCesstermTos.set(t5606rec.benCesstermTos);
		sv.benfreq.set(t5606rec.benfreq);
		sv.dfrprd.set(t5606rec.dfrprd);
		sv.eaage.set(t5606rec.eaage);
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.liencds.set(t5606rec.liencds);
		sv.mortclss.set(t5606rec.mortclss);
		sv.premCessageFroms.set(t5606rec.premCessageFroms);
		sv.premCessageTos.set(t5606rec.premCessageTos);
		sv.premCesstermFroms.set(t5606rec.premCesstermFroms);
		sv.premCesstermTos.set(t5606rec.premCesstermTos);
		sv.riskCessageFroms.set(t5606rec.riskCessageFroms);
		sv.riskCessageTos.set(t5606rec.riskCessageTos);
		sv.riskCesstermFroms.set(t5606rec.riskCesstermFroms);
		sv.riskCesstermTos.set(t5606rec.riskCesstermTos);
		sv.specind.set(t5606rec.specind);
		sv.sumInsMax.set(t5606rec.sumInsMax);
		sv.sumInsMin.set(t5606rec.sumInsMin);
		sv.termIssageFrms.set(t5606rec.termIssageFrms);
		sv.termIssageTos.set(t5606rec.termIssageTos);
		sv.waitperiods.set(t5606rec.waitperiods);
		sv.bentrms.set(t5606rec.bentrms);
		sv.poltyps.set(t5606rec.poltyps);
		sv.prmbasiss.set(t5606rec.prmbasiss);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5606rec.t5606Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.ageIssageFrms, t5606rec.ageIssageFrms)) {
			t5606rec.ageIssageFrms.set(sv.ageIssageFrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ageIssageTos, t5606rec.ageIssageTos)) {
			t5606rec.ageIssageTos.set(sv.ageIssageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.benCessageFroms, t5606rec.benCessageFroms)) {
			t5606rec.benCessageFroms.set(sv.benCessageFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.benCessageTos, t5606rec.benCessageTos)) {
			t5606rec.benCessageTos.set(sv.benCessageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.benCesstermFrms, t5606rec.benCesstermFrms)) {
			t5606rec.benCesstermFrms.set(sv.benCesstermFrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.benCesstermTos, t5606rec.benCesstermTos)) {
			t5606rec.benCesstermTos.set(sv.benCesstermTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.benfreq, t5606rec.benfreq)) {
			t5606rec.benfreq.set(sv.benfreq);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.dfrprd, t5606rec.dfrprd)) {
			t5606rec.dfrprd.set(sv.dfrprd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.eaage, t5606rec.eaage)) {
			t5606rec.eaage.set(sv.eaage);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.liencds, t5606rec.liencds)) {
			t5606rec.liencds.set(sv.liencds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.mortclss, t5606rec.mortclss)) {
			t5606rec.mortclss.set(sv.mortclss);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCessageFroms, t5606rec.premCessageFroms)) {
			t5606rec.premCessageFroms.set(sv.premCessageFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCessageTos, t5606rec.premCessageTos)) {
			t5606rec.premCessageTos.set(sv.premCessageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCesstermFroms, t5606rec.premCesstermFroms)) {
			t5606rec.premCesstermFroms.set(sv.premCesstermFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCesstermTos, t5606rec.premCesstermTos)) {
			t5606rec.premCesstermTos.set(sv.premCesstermTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCessageFroms, t5606rec.riskCessageFroms)) {
			t5606rec.riskCessageFroms.set(sv.riskCessageFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCessageTos, t5606rec.riskCessageTos)) {
			t5606rec.riskCessageTos.set(sv.riskCessageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCesstermFroms, t5606rec.riskCesstermFroms)) {
			t5606rec.riskCesstermFroms.set(sv.riskCesstermFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCesstermTos, t5606rec.riskCesstermTos)) {
			t5606rec.riskCesstermTos.set(sv.riskCesstermTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.specind, t5606rec.specind)) {
			t5606rec.specind.set(sv.specind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sumInsMax, t5606rec.sumInsMax)) {
			t5606rec.sumInsMax.set(sv.sumInsMax);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sumInsMin, t5606rec.sumInsMin)) {
			t5606rec.sumInsMin.set(sv.sumInsMin);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.termIssageFrms, t5606rec.termIssageFrms)) {
			t5606rec.termIssageFrms.set(sv.termIssageFrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.termIssageTos, t5606rec.termIssageTos)) {
			t5606rec.termIssageTos.set(sv.termIssageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.waitperiods, t5606rec.waitperiods)) {
			t5606rec.waitperiods.set(sv.waitperiods);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.bentrms, t5606rec.bentrms)) {
			t5606rec.bentrms.set(sv.bentrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.poltyps, t5606rec.poltyps)) {
			t5606rec.poltyps.set(sv.poltyps);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prmbasiss, t5606rec.prmbasiss)) {
			t5606rec.prmbasiss.set(sv.prmbasiss);
			wsaaUpdateFlag = "Y";
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5606pt.class, wsaaTablistrec);
		/*EXIT*/
	}

	

	public String getWsaaUpdateFlag() {
		return wsaaUpdateFlag;
	}

	public void setWsaaUpdateFlag(String wsaaUpdateFlag) {
		this.wsaaUpdateFlag = wsaaUpdateFlag;
	}

	
}
