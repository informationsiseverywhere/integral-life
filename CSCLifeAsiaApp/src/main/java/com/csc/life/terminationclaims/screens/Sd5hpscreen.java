package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sd5hpscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {22, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5hpScreenVars sv = (Sd5hpScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sd5hpscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sd5hpScreenVars screenVars = (Sd5hpScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.item.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.remaday01.setClassString("");
		screenVars.remaday02.setClassString("");
		screenVars.remaday03.setClassString("");
		screenVars.remaday04.setClassString("");
		screenVars.remaday05.setClassString("");
		screenVars.remaday06.setClassString("");
		screenVars.remaday07.setClassString("");
		screenVars.remaday08.setClassString("");
		screenVars.remaday09.setClassString("");
		screenVars.remaday10.setClassString("");
		screenVars.remaday11.setClassString("");
		screenVars.remaday12.setClassString("");
		screenVars.remaday13.setClassString("");
		screenVars.remaday14.setClassString("");
		screenVars.remaday15.setClassString("");
		screenVars.remaday16.setClassString("");
		screenVars.remaday17.setClassString("");
		screenVars.remaday18.setClassString("");
		screenVars.remaday19.setClassString("");
		screenVars.remaday20.setClassString("");
		screenVars.rate01.setClassString("");
		screenVars.rate02.setClassString("");
		screenVars.rate03.setClassString("");
		screenVars.rate04.setClassString("");
		screenVars.rate05.setClassString("");
		screenVars.rate06.setClassString("");
		screenVars.rate07.setClassString("");
		screenVars.rate08.setClassString("");
		screenVars.rate09.setClassString("");
		screenVars.rate10.setClassString("");
		screenVars.rate11.setClassString("");
		screenVars.rate12.setClassString("");
		screenVars.rate13.setClassString("");
		screenVars.rate14.setClassString("");
		screenVars.rate15.setClassString("");
		screenVars.rate16.setClassString("");
		screenVars.rate17.setClassString("");
		screenVars.rate18.setClassString("");
		screenVars.rate19.setClassString("");
		screenVars.rate20.setClassString("");
	}

/**
 * Clear all the variables in Sd5hpscreen
 */
	public static void clear(VarModel pv) {
		Sd5hpScreenVars screenVars = (Sd5hpScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.remaday01.clear();
		screenVars.remaday02.clear();
		screenVars.remaday03.clear();
		screenVars.remaday04.clear();
		screenVars.remaday05.clear();
		screenVars.remaday06.clear();
		screenVars.remaday07.clear();
		screenVars.remaday08.clear();
		screenVars.remaday09.clear();
		screenVars.remaday10.clear();
		screenVars.remaday11.clear();
		screenVars.remaday12.clear();
		screenVars.remaday13.clear();
		screenVars.remaday14.clear();
		screenVars.remaday15.clear();
		screenVars.remaday16.clear();
		screenVars.remaday17.clear();
		screenVars.remaday18.clear();
		screenVars.remaday19.clear();
		screenVars.remaday20.clear();
		screenVars.rate01.clear();
		screenVars.rate02.clear();
		screenVars.rate03.clear();
		screenVars.rate04.clear();
		screenVars.rate05.clear();
		screenVars.rate06.clear();
		screenVars.rate07.clear();
		screenVars.rate08.clear();
		screenVars.rate09.clear();
		screenVars.rate10.clear();
		screenVars.rate11.clear();
		screenVars.rate12.clear();
		screenVars.rate13.clear();
		screenVars.rate14.clear();
		screenVars.rate15.clear();
		screenVars.rate16.clear();
		screenVars.rate17.clear();
		screenVars.rate18.clear();
		screenVars.rate19.clear();
		screenVars.rate20.clear();
	}
}
