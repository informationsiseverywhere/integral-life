/*
 * File: Regwpay.java
 * Date: 30 August 2009 2:05:03
 * Author: Quipoz Limited
 * 
 * Class transformed from REGWPAY.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.recordstructures.Regpsubrec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrrgwTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5542rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  REGWPAY - Regular Payments Withdrawal Subroutine
*  -------------------------------------------------
*
*  This subroutine will be called from the main module  of  the
*  Regular  Payments  batch job. It will be registered on T5671
*  which is keyed by Transaction Code and  Component  Code  and
*  so  the  processing  subroutine called may disagree for each
*  component type.
*
*  It will extract and  process  all  the  Regular  Withdrawals
*  that are due on or before the effective run date of the job.
*
*  The  Regular  Withdrawal  itself  will  be  set  up so as to
*  extract a specified amount or a  specified  percentage  from
*  all   of   the   funds   currently   associated   with   the
*  Component/Policy Number on the withdrawal.
*
*  It will spread the amount or percentage pro-rata across  all
*  of the funds associated with the specified coverage/rider.
*
*  The  table T5542 will be read and the values on here will be
*  checked to see if  the  amounts  necessary  to  satisfy  the
*  withdrawal can be permitted.
*
*  NB.  This  subroutine  will  be  called  using  the copybook
*  REGPSUBREC.  Any   exception   errors   detected   in   this
*  subroutine  will be passed back to the main Regular Payments
*  Batch run and output on report as applicable.
*
*  Processing.
*  -----------
*
*
*  Set the return STATUZ in linkage to O-K.
*
*  Perform a RETRV on REGP.
*
*  Perform a READR on CHDRRGP.
*
*  Set Percentage or Amount indicator.
*
*  Read in the Coverage details using COVRRGW.
*
*  Read T5645 to obtain Transaction Accounting Rules.
*
*  Read T5540 with CRTABLE to obtain the Withdrawal Method.
*
*  Read T5542 using Withdrawal Method from  T5540  concatonated
*  with currency code to obtain Minimum Withdrawal Amount.
*
*  Withdrawal Processing.
*  ----------------------
*
*  Set up UTRS key from REGP details and read  with BEGNH.
*
*  If Key Break move 'Y' set indicator for End of UTRS records.
*
*  If  REGP-PLAN-SUFFIX  Not  =  UTRS-PLAN-SUFFIX set indicator
*  for End of UTRS records.
*
*  If indicator for End of UTRS  records  is  on,  set  Linkage
*  STATUZ accordingly and exit section.
*
*  If  UTRS  record  found  read  T6647  with  Transaction Code
*  concatonated with Contract Type.
*
*  Set UTRS-INDEX to 1 and perform the following until  End  of
*  UTRS  records  indicator  is on, Linkage STATUZ not = O-K or
*  UTRS-INDEX > 99.
*
*       If the UTRS balance is Zero or the UTRS is initial
*       units, get the next UTRS.
*
*       Move LIFE, COVERAGE, RIDER, PLAN-SUFFIX,
*       UNIT-VIRTUAL-FUND, UNIT-TYPE, CURRENT-UNIT-BALANCE to
*       a Working Storage array subscripted by UTRS-INDEX.
*
*       Read T5515 with the UNIT-VIRTUAL-FUND to obtain the
*       Fund Currency. Move T5515-FUND-CURRENCY to the array.
*
*       Read VPRNUDL with values from UTRS and an
*       Effective Date from REGP-NEXT-PAYDATE, to obtain the
*       unit price. Store the Bid or Offer price according
*       to T6647-BIDOFFER indicator.
*
*       Perform the following to calculate the Fund Value:
*            Compute WSAA-FUND-VALUE rounded =
*            UTRS-CURRENT-UNIT-BAL * previously stored
*            Bid or Offer price.
*
*            If REGP Currency Code not = T5515 Currency,
*            call XCVRT with a FUNCTION of 'REAL' and an
*            Effective Date from VPRNUDL to convert fund
*            value to Withdrawal Currency.
*
*            Store the Fund Value in the array indexed by
*            UTRS-INDEX.
*
*       Read UTRS with a function of NEXTR.
*
*  If UTRS-INDEX > 99 move appropriate error to Linkage  STATUZ
*  and exit subroutine.
*
*  If  withdrawal  is  by  amount  check if the calculated fund
*  value is < Withdrawal Amount. If  it  is,  move  appropriate
*  error to Linkage STATUZ and exit subroutine.
*
*  To  calculate  the  Withdrawal  Amount perform the following
*  until UTRS-INDEX > 99 or Fund on array = spaces.
*
*       Calculate the withdrawal amount rounded = (REGP-PYMT *
*       array Fund Value (UTRS-INDEX) ) / Total Fund Value.
*
*       If percentage, calculate withdrawal amount rounded
*       = array Fund Value (UTRS-INDEX) * REGP-PRCNT.
*
*       Move withdrawal amount to array Withdrawal Amount
*       (UTRS-INDEX).
*       Accumulate the withdrawal amount.
*
*  If the withdrawal is by amount and the REGP-PYMT not  =  the
*  Accumulated  withdrawal amount, calculate the difference set
*  UTRS-INDEX to '1'  and  add  the  difference  to  the  array
*  Withdrawal Amount (UTRS-INDEX).
*
*  If  the  actual withdrawal amount < T5542 Minimum Withdrawal
*  move the appropriate error message  to  the  Linkage  STATUZ
*  and exit subroutine.
*
*  Calculate  the  remaining  fund  value  = Total Fund Value -
*  Actual withdrawal amount  and  if  not  greater  than  T5542
*  Minimum   Remainder  allowed,  move  the  appropriate  error
*  message to Linkage STATUZ and exit subroutine.
*
*  Create UTRN record until  UTRS-INDEX  >  99,  array  Virtual
*  Fund (UTRS-INDEX) = spaces, or Linkage STATUZ not = O-K.
*
*  Read  T6598  with  T5540 Withdrawal Method to obtain Trigger
*  Module.
*
*  Write a  UTRN  record  for  each  fund  involved,  with  the
*  following   data   for   the   pending   regular  withdrawal
*  sub-account:-
*
*       Processing Seq No. -  from T6647
*       Surrender %        -  may be a percentage across the
*                             fund
*       Amount (signed)    -  may be an amount spread across
*                             the fund
*       Effective date     -  Effective-date of the surrender
*       Bid/Offer Ind      -  'B'
*       Now/Deferred Ind   -  T6647 entry (deallocation field)
*       Price              -  0
*       Fund               -  from array  (UTRS-INDEX)
*       Number of Units    -  0
*       Contract No.       -  REGP-CHDRNUM
*       Contract Type      -  CHDRRGP-CNTTYPE
*       Sub-Account Code   -  T5645 entry for this sub-account
*       Sub-Account Type   -  ditto
*       G/L key            -  ditto
*       Trigger module     -  T6598 Additional Processing
*                             Module
*       Trigger key        -  Contract no./Contract
*                             Coy/Suffix/ TRANNO/RGPYNUM
*       Monies Date        -  Next Payment Date
*       Pricing Date       -  Blank
*       Fund Currency      -  T5515 for fund
*       Surr Value Penalty -  1
*
*  Perform a KEEPS on REGP.
*
*  Exit Subroutine.
*
*
*  Notes.
*  ------
*
*  If any bad STATUZ returns are received from any access to  a
*  file  then  place  the  return code in the subroutine return
*  STATUZ and return to the calling module.
*
*  If the  read  of  any  table  entry  fails  then  set  up  a
*  meaningful  error  message and pass this back to the calling
*  module in the return STATUZ in the linkage area.
*
*  Tables Used.
*  ------------
*
*  .T5515 - Virtual Fund Details
*           Key: Fund
*
*  .T5540 - Unit Linked Details
*           Key: CRTABLE
*
*  .T5542 - Unit Withdrawal Rules.
*           Key: T5540 Withdrawal Method || Currency Code
*
*  .T5645 - Transaction Accounting Rules
*           Key: Program ID ie. REGWPAY
*
*  .T6598 - Surrender Value Calculation Methods
*           Key: T5540 Withdrawal Method
*
*  .T6647 - Unit Linked Contract Details
*           Key: BATCTRCDE || CNTTYPE
*
*****************************************************************
* </pre>
*/
public class Regwpay extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REGWPAY   ";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaFundValue = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaPrice = new PackedDecimalData(9, 5);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaIfWholePlan = new FixedLengthStringData(1);
	private Validator wsaaWholePlan = new Validator(wsaaIfWholePlan, "Y");

	private FixedLengthStringData wsaaIfEndOfUtrs = new FixedLengthStringData(1);
	private Validator wsaaEndOfUtrs = new Validator(wsaaIfEndOfUtrs, "Y");
		/* WSAA-ERR-CODE */
	private static final String wsaaInsuffFund = "INSF";
	private static final String wsaaNoPrice = "NOPR";
	private static final String wsaaLessMinAmt = "LTMA";

	private FixedLengthStringData wsaaPrcntAmntInd = new FixedLengthStringData(1);
	private Validator wsaaWithdrawalByPercent = new Validator(wsaaPrcntAmntInd, "P");
	private Validator wsaaWithdrawalByAmount = new Validator(wsaaPrcntAmntInd, "A");
	private PackedDecimalData wsaaTotalFundValue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWithdrawalAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRemainingAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalcAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaDiffAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPymt = new PackedDecimalData(17, 2);
		/* WSAA-T5542-INFO */
	private PackedDecimalData wsaaFeepc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaFeemax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFeemin = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFfamt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWdlAmount = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWdrem = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaTrigger = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaTriggerChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaTrigger, 0);
	private FixedLengthStringData wsaaTriggerChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaTrigger, 1);
	private PackedDecimalData wsaaTriggerSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaTrigger, 9);
	private PackedDecimalData wsaaTriggerTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaTrigger, 12);
	private PackedDecimalData wsaaTriggerSeqnumb = new PackedDecimalData(3, 0).isAPartOf(wsaaTrigger, 15);
	private FixedLengthStringData filler = new FixedLengthStringData(3).isAPartOf(wsaaTrigger, 17, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTabBatckey = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);

	private FixedLengthStringData wsaaT5542Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5542KeyWdmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5542Key, 0);
	private FixedLengthStringData wsaaT5542KeyCurrcd = new FixedLengthStringData(3).isAPartOf(wsaaT5542Key, 4);
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t5540 = "T5540";
	private static final String t5645 = "T5645";
	private static final String t6598 = "T6598";
	private static final String t6647 = "T6647";
	private static final String t5542 = "T5542";
	private static final String t5688 = "T5688";
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String g371 = "G371";
	private static final String h055 = "H055";
	private static final String h116 = "H116";
	private static final String h388 = "H388";
	private static final String h414 = "H414";
	private static final String h965 = "H965";
		/* FORMATS */
	private static final String chdrrgprec = "CHDRRGPREC";
	private static final String utrnrec = "UTRNREC   ";
	private IntegerData utrsIndex = new IntegerData();
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CovrrgwTableDAM covrrgwIO = new CovrrgwTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private VprcTableDAM vprcIO = new VprcTableDAM();
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5540rec t5540rec = new T5540rec();
	private T6598rec t6598rec = new T6598rec();
	private T5515rec t5515rec = new T5515rec();
	private T6647rec t6647rec = new T6647rec();
	private T5645rec t5645rec = new T5645rec();
	private T5542rec t5542rec = new T5542rec();
	private T5688rec t5688rec = new T5688rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Regpsubrec regpsubrec = new Regpsubrec();
	private WsaaUtrsDataArrayInner wsaaUtrsDataArrayInner = new WsaaUtrsDataArrayInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readNextUtrs2180, 
		exit2190
	}

	public Regwpay() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		regpsubrec.batcsubRec = convertAndSetParam(regpsubrec.batcsubRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		wsaaWdlAmount.set(ZERO);
		wsaaWdrem.set(ZERO);
		regpsubrec.subStatuz.set(varcom.oK);
		regpsubrec.subSubrname.set(wsaaSubr);
		syserrrec.subrname.set(wsaaSubr);
		initialise100();
		if (isEQ(regpsubrec.subStatuz, varcom.oK)) {
			processWithdrawal2000();
		}
		if (isEQ(regpsubrec.subStatuz, varcom.oK)) {
			updateRegp5500();
		}
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		process110();
	}

protected void process110()
	{
		/* Retrieve the Regular Payment Record*/
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if ((isNE(regpIO.getStatuz(), varcom.oK))) {
			regpsubrec.subStatuz.set(regpIO.getStatuz());
			return ;
		}
		/* Obtain the Contract Header Details*/
		chdrrgpIO.setParams(SPACES);
		chdrrgpIO.setChdrnum(regpIO.getChdrnum());
		chdrrgpIO.setChdrcoy(regpIO.getChdrcoy());
		chdrrgpIO.setFormat(chdrrgprec);
		chdrrgpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if ((isNE(chdrrgpIO.getStatuz(), varcom.oK))) {
			regpsubrec.subStatuz.set(chdrrgpIO.getStatuz());
			return ;
		}
		/* Read T5688 to obtain the accounting level,                      */
		/* Component or contract.                                          */
		itdmIO.setItemcoy(chdrrgpIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrrgpIO.getCnttype());
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			regpsubrec.subStatuz.set(itdmIO.getStatuz());
			return ;
		}
		if (isNE(itdmIO.getItemcoy(), chdrrgpIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrrgpIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			regpsubrec.subStatuz.set(e308);
			return ;
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/* Determine if Percentage or Withdrawal Amount*/
		if ((isNE(regpIO.getPrcnt(), ZERO))) {
			wsaaPrcntAmntInd.set("P");
		}
		else {
			wsaaPrcntAmntInd.set("A");
		}
		/* Obtain the component details*/
		readCovrRec4100();
		if (isNE(regpsubrec.subStatuz, varcom.oK)) {
			return ;
		}
		/*  Obtain Transaction Accounting Rules*/
		readT56455200();
		if (isNE(regpsubrec.subStatuz, varcom.oK)) {
			return ;
		}
		/* To obtain regular withdrawal method*/
		readT55405300();
		if (isNE(regpsubrec.subStatuz, varcom.oK)) {
			return ;
		}
		/* To obtain the minimum withdrawal amount*/
		readT55425400();
		if (isEQ(regpIO.getCurrcd(), chdrrgpIO.getCntcurr())) {
			wsaaPymt.set(regpIO.getPymt());
			return ;
		}
		/* Convert Payment Amount to payment currency*/
		conlinkrec.company.set(regpIO.getChdrcoy());
		conlinkrec.cashdate.set(regpsubrec.subEffdate);
		conlinkrec.amountIn.set(regpIO.getPymt());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.currOut.set(regpIO.getCurrcd());
		conlinkrec.currIn.set(chdrrgpIO.getCntcurr());
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			regpsubrec.subStatuz.set(conlinkrec.statuz);
			regpsubrec.subSubrname.set("XCVRT");
			return ;
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		wsaaPymt.set(conlinkrec.amountOut);
	}

protected void processWithdrawal2000()
	{
		start2000();
	}

protected void start2000()
	{
		wsaaUtrsDataArrayInner.wsaaUtrsDataArray.set(SPACES);
		wsaaIfEndOfUtrs.set("N");
		wsaaTotalFundValue.set(ZERO);
		wsaaPrice.set(ZERO);
		utrsIO.setParams(SPACES);
		utrsIO.setChdrcoy(regpIO.getChdrcoy());
		utrsIO.setChdrnum(regpIO.getChdrnum());
		utrsIO.setLife(regpIO.getLife());
		utrsIO.setCoverage(regpIO.getCoverage());
		utrsIO.setRider(regpIO.getRider());
		utrsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");

		if (isEQ(regpIO.getPlanSuffix(),ZERO)) {
			wsaaIfWholePlan.set("Y");
			utrsIO.setPlanSuffix(ZERO);
		}
		else {
			wsaaIfWholePlan.set("N");
			utrsIO.setPlanSuffix(regpIO.getPlanSuffix());
		}
		SmartFileCode.execute(appVars, utrsIO);
		if ((isNE(utrsIO.getStatuz(), varcom.oK))
		&& (isNE(utrsIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(utrsIO.getStatuz());
			return ;
		}
		if ((isNE(regpIO.getChdrnum(), utrsIO.getChdrnum()))
		|| (isNE(regpIO.getChdrcoy(), utrsIO.getChdrcoy()))
		|| (isNE(regpIO.getLife(), utrsIO.getLife()))
		|| (isNE(regpIO.getCoverage(), utrsIO.getCoverage()))
		|| (isNE(regpIO.getRider(), utrsIO.getRider()))
		|| (isNE(regpIO.getPlanSuffix(), utrsIO.getPlanSuffix()))
		|| (isEQ(utrsIO.getStatuz(), varcom.endp))) {
			wsaaIfEndOfUtrs.set("Y");
		}
		if (!wsaaWholePlan.isTrue()) {
			if ((isNE(regpIO.getPlanSuffix(), utrsIO.getPlanSuffix()))) {
				wsaaIfEndOfUtrs.set("Y");
			}
		}
		if (wsaaEndOfUtrs.isTrue()) {
			regpsubrec.subStatuz.set(wsaaInsuffFund);
			return ;
		}
		readT66475000();
		utrsIndex.set(1);
		while ( !((wsaaEndOfUtrs.isTrue())
		|| (isNE(regpsubrec.subStatuz, varcom.oK))
		|| (isGT(utrsIndex, 99)))) {
			setupUtrsInfo2100();
		}
		
		if (isNE(regpsubrec.subStatuz, varcom.oK)) {
			return ;
		}
		if ((isGT(utrsIndex, 99))) {
			regpsubrec.subStatuz.set(h414);
			return ;
		}
		if (wsaaWithdrawalByAmount.isTrue()) {
			if (isLT(wsaaTotalFundValue, wsaaPymt)) {
				regpsubrec.subStatuz.set(wsaaInsuffFund);
				return ;
			}
		}
		wsaaCalcAmount.set(ZERO);
		for (utrsIndex.set(1); !((isGT(utrsIndex, 99))
		|| (isEQ(wsaaUtrsDataArrayInner.wsaaUtrsUnitVirtualFund[utrsIndex.toInt()], SPACES))); utrsIndex.add(1)){
			calcWithdrawalAmount2400();
		}
		wsaaDiffAmount.set(0);
		if (wsaaWithdrawalByAmount.isTrue()) {
			if (isNE(wsaaPymt, wsaaCalcAmount)) {
				compute(wsaaDiffAmount, 2).set(sub(wsaaPymt, wsaaCalcAmount));
				utrsIndex.set(1);
				wsaaUtrsDataArrayInner.wsaaUtrsWithdrawalAmt[utrsIndex.toInt()].add(wsaaDiffAmount);
			}
		}
		validateWdlAmount2600();
		if (isNE(regpsubrec.subStatuz, varcom.oK)) {
			return ;
		}
		for (utrsIndex.set(1); !((isGT(utrsIndex, 99))
		|| (isEQ(wsaaUtrsDataArrayInner.wsaaUtrsUnitVirtualFund[utrsIndex.toInt()], SPACES))
		|| (isNE(regpsubrec.subStatuz, varcom.oK))); utrsIndex.add(1)){
			createUtrnRecord2500();
		}
	}

protected void setupUtrsInfo2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begnFundUtrs2110();
				case readNextUtrs2180: 
					readNextUtrs2180();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begnFundUtrs2110()
	{
		/* If the UTRS balance is zero or the UTRS is Inital units*/
		/* then we do not add in the fund value. As regular withdrawals*/
		/* should only be taken from Accumulation units.*/
		if ((isLTE(utrsIO.getCurrentUnitBal(), 0))
		|| (isEQ(utrsIO.getUnitType(), "I"))) {
			goTo(GotoLabel.readNextUtrs2180);
		}
		wsaaUtrsDataArrayInner.wsaaUtrsPlanSuffix[utrsIndex.toInt()].set(utrsIO.getPlanSuffix());
		wsaaUtrsDataArrayInner.wsaaUtrsUnitVirtualFund[utrsIndex.toInt()].set(utrsIO.getUnitVirtualFund());
		wsaaUtrsDataArrayInner.wsaaUtrsUnitType[utrsIndex.toInt()].set(utrsIO.getUnitType());
		readT55155100();
		if (isNE(regpsubrec.subStatuz, varcom.oK)) {
			goTo(GotoLabel.exit2190);
		}
		wsaaUtrsDataArrayInner.wsaaUtrsFundCurrency[utrsIndex.toInt()].set(t5515rec.currcode);
		obtainUnitPrice2200();
		if (isNE(regpsubrec.subStatuz, varcom.oK)) {
			goTo(GotoLabel.exit2190);
		}
		calcFundValue2300();
		if (isNE(regpsubrec.subStatuz, varcom.oK)) {
			goTo(GotoLabel.exit2190);
		}
		utrsIndex.add(1);
	}

protected void readNextUtrs2180()
	{
		utrsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsIO);
		if ((isNE(utrsIO.getStatuz(), varcom.oK))
		&& (isNE(utrsIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(utrsIO.getStatuz());
			return ;
		}
		if ((isNE(regpIO.getChdrnum(), utrsIO.getChdrnum()))
		|| (isNE(regpIO.getChdrcoy(), utrsIO.getChdrcoy()))
		|| (isNE(regpIO.getLife(), utrsIO.getLife()))
		|| (isNE(regpIO.getCoverage(), utrsIO.getCoverage()))
		|| (isNE(regpIO.getRider(), utrsIO.getRider()))
		|| (isNE(regpIO.getPlanSuffix(), utrsIO.getPlanSuffix()))
		|| (isEQ(utrsIO.getStatuz(), varcom.endp))) {
			wsaaIfEndOfUtrs.set("Y");
		}
		if (!wsaaWholePlan.isTrue()) {
			if ((isNE(regpIO.getPlanSuffix(), utrsIO.getPlanSuffix()))) {
				wsaaIfEndOfUtrs.set("Y");
			}
		}
	}

protected void obtainUnitPrice2200()
	{
		start2210();
	}

protected void start2210()
	{
		/*    For each of the records found accumulate the*/
		/*    accumulation values in working storage using the bare price*/
		/*    from file (VPRCPF) with logical view VPRNUDL.*/
		vprnudlIO.setParams(SPACES);
		vprnudlIO.setCompany(utrsIO.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		vprnudlIO.setUnitType(utrsIO.getUnitType());
		vprnudlIO.setEffdate(regpIO.getNextPaydate());
		vprnudlIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY","VRTFND","ULTYPE");
		 
		SmartFileCode.execute(appVars, vprnudlIO);
		if ((isNE(vprnudlIO.getStatuz(), varcom.oK))
		&& (isNE(vprnudlIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(vprnudlIO.getStatuz());
			return ;
		}
		if ((isNE(vprnudlIO.getCompany(), utrsIO.getChdrcoy()))
		|| (isNE(vprnudlIO.getUnitVirtualFund(), utrsIO.getUnitVirtualFund()))
		|| (isNE(vprnudlIO.getUnitType(), utrsIO.getUnitType()))
		|| (isEQ(vprnudlIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(wsaaNoPrice);
			return ;
		}
		/*    If the bid offer indicator is set to 'B' move WSAA-BID to*/
		/*    WSAA-PRICE else move WSAA-OFFER to WSAA-PRICE.*/
		if (isEQ(t6647rec.bidoffer, "B")) {
			wsaaPrice.set(vprnudlIO.getUnitBidPrice());
		}
		else {
			wsaaPrice.set(vprnudlIO.getUnitOfferPrice());
		}
	}

protected void calcFundValue2300()
	{
		start2300();
	}

protected void start2300()
	{
		/*    Calculate the total fund value.*/
		compute(wsaaFundValue, 6).setRounded((mult(utrsIO.getCurrentUnitBal(), wsaaPrice)));
		if (isNE(regpIO.getCurrcd(), t5515rec.currcode)) {
			conlinkrec.currIn.set(t5515rec.currcode);
			conlinkrec.cashdate.set(vprnudlIO.getEffdate());
			conlinkrec.currOut.set(regpIO.getCurrcd());
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.function.set("CVRT");
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(regpIO.getChdrcoy());
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				regpsubrec.subStatuz.set(conlinkrec.statuz);
				regpsubrec.subSubrname.set("XCVRT");
				return ;
			}
			else {
				wsaaUtrsDataArrayInner.wsaaUtrsFundValue[utrsIndex.toInt()].set(conlinkrec.amountOut);
			}
		}
		else {
			wsaaUtrsDataArrayInner.wsaaUtrsFundValue[utrsIndex.toInt()].set(wsaaFundValue);
		}
		/*    Accumulate FUND VALUE*/
		wsaaTotalFundValue.add(wsaaUtrsDataArrayInner.wsaaUtrsFundValue[utrsIndex.toInt()]);
	}

protected void calcWithdrawalAmount2400()
	{
		/*START*/
		/*    This section calculates the withdrawl amount and checks*/
		/*    the minimum withdrawal amount from T5542.*/
		wsaaWithdrawalAmount.set(0);
		/* Increase the accuracy of the Withdrawal amount to avoid a large*/
		/* remainder*/
		if (wsaaWithdrawalByAmount.isTrue()) {
			compute(wsaaWithdrawalAmount, 3).setRounded(div((mult(wsaaPymt, wsaaUtrsDataArrayInner.wsaaUtrsFundValue[utrsIndex.toInt()])), wsaaTotalFundValue));
		}
		else {
			compute(wsaaWithdrawalAmount, 2).set(div(mult(wsaaUtrsDataArrayInner.wsaaUtrsFundValue[utrsIndex.toInt()], regpIO.getPrcnt()), 100));
		}
		zrdecplrec.amountIn.set(wsaaWithdrawalAmount);
		a000CallRounding();
		wsaaWithdrawalAmount.set(zrdecplrec.amountOut);
		wsaaUtrsDataArrayInner.wsaaUtrsWithdrawalAmt[utrsIndex.toInt()].set(wsaaWithdrawalAmount);
		wsaaCalcAmount.add(wsaaWithdrawalAmount);
		/*EXIT*/
	}

protected void createUtrnRecord2500()
	{
		start2510();
	}

protected void start2510()
	{
		if (isEQ(wsaaUtrsDataArrayInner.wsaaUtrsWithdrawalAmt[utrsIndex.toInt()], 0)) {
			return ;
		}
		getTrigger3100();
		utrnIO.setParams(SPACES);
		utrnIO.setBatcactyr(ZERO);
		utrnIO.setBatcactmn(ZERO);
		utrnIO.setFundRate(ZERO);
		utrnIO.setStrpdate(ZERO);
		utrnIO.setNofUnits(ZERO);
		utrnIO.setNofDunits(ZERO);
		utrnIO.setMoniesDate(ZERO);
		utrnIO.setPriceDateUsed(ZERO);
		utrnIO.setJobnoPrice(ZERO);
		utrnIO.setPriceUsed(ZERO);
		utrnIO.setUnitBarePrice(ZERO);
		utrnIO.setInciNum(ZERO);
		utrnIO.setInciPerd01(ZERO);
		utrnIO.setInciPerd02(ZERO);
		utrnIO.setInciprm01(ZERO);
		utrnIO.setInciprm02(ZERO);
		utrnIO.setContractAmount(ZERO);
		utrnIO.setFundAmount(ZERO);
		utrnIO.setProcSeqNo(ZERO);
		utrnIO.setDiscountFactor(ZERO);
		utrnIO.setSurrenderPercent(ZERO);
		utrnIO.setUstmno(ZERO);
		utrnIO.setCrComDate(ZERO);
		varcom.vrcmTranid.set(regpsubrec.subTranid);
		utrnIO.setTermid(varcom.vrcmTermid);
		utrnIO.setTransactionDate(varcom.vrcmDate);
		utrnIO.setTransactionTime(varcom.vrcmTime);
		utrnIO.setUser(varcom.vrcmUser);
		utrnIO.setBatccoy(regpsubrec.subBatccoy);
		utrnIO.setBatcbrn(regpsubrec.subBatcbrn);
		utrnIO.setBatcactyr(regpsubrec.subBatcactyr);
		utrnIO.setBatcactmn(regpsubrec.subBatcactmn);
		utrnIO.setBatctrcde(regpsubrec.subBatctrcde);
		utrnIO.setBatcbatch(regpsubrec.subBatcbatch);
		utrnIO.setChdrcoy(regpIO.getChdrcoy());
		wsaaTriggerChdrcoy.set(regpIO.getChdrcoy());
		utrnIO.setChdrnum(regpIO.getChdrnum());
		wsaaTriggerChdrnum.set(regpIO.getChdrnum());
		utrnIO.setLife(regpIO.getLife());
		utrnIO.setCoverage(regpIO.getCoverage());
		utrnIO.setRider(regpIO.getRider());
		setPrecision(utrnIO.getTranno(), 0);
		utrnIO.setTranno(add(chdrrgpIO.getTranno(), 1));
		wsaaTriggerTranno.set(utrnIO.getTranno());
		/* MOVE REGP-TRANNO            TO WSAA-TRIGGER-TRANNO.          */
		wsaaTriggerSeqnumb.set(regpIO.getRgpynum());
		utrnIO.setCrtable(covrrgwIO.getCrtable());
		utrnIO.setCrComDate(covrrgwIO.getCrrcd());
		utrnIO.setMoniesDate(regpIO.getNextPaydate());
		utrnIO.setPlanSuffix(wsaaUtrsDataArrayInner.wsaaUtrsPlanSuffix[utrsIndex.toInt()]);
		wsaaTriggerSuffix.set(wsaaUtrsDataArrayInner.wsaaUtrsPlanSuffix[utrsIndex.toInt()]);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		if (wsaaWithdrawalByAmount.isTrue()) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(mult(wsaaUtrsDataArrayInner.wsaaUtrsWithdrawalAmt[utrsIndex.toInt()], -1));
		}
		else {
			utrnIO.setSurrenderPercent(regpIO.getPrcnt());
		}
		utrnIO.setSvp(1);
		utrnIO.setFundAmount(0);
		/* Calculate the detail amount*/
		utrnIO.setUnitVirtualFund(wsaaUtrsDataArrayInner.wsaaUtrsUnitVirtualFund[utrsIndex.toInt()]);
		utrnIO.setContractType(chdrrgpIO.getCnttype());
		utrnIO.setNowDeferInd(t6647rec.dealin);
		if (isEQ(wsaaUtrsDataArrayInner.wsaaUtrsUnitType[utrsIndex.toInt()], "I")) {
			utrnIO.setUnitSubAccount("INIT");
		}
		else {
			utrnIO.setUnitSubAccount("ACUM");
		}
		/*    MOVE T5645-SACSCODE-01      TO UTRN-SACSCODE.                */
		/*    MOVE T5645-SACSTYPE-01      TO UTRN-SACSTYP.                 */
		/*    MOVE T5645-GLMAP-01         TO UTRN-GENLCDE.                 */
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		utrnIO.setFundCurrency(wsaaUtrsDataArrayInner.wsaaUtrsFundCurrency[utrsIndex.toInt()]);
		utrnIO.setUnitType(wsaaUtrsDataArrayInner.wsaaUtrsUnitType[utrsIndex.toInt()]);
		utrnIO.setCntcurr(regpIO.getCurrcd());
		utrnIO.setTriggerModule(t6598rec.addprocess);
		utrnIO.setTriggerKey(wsaaTrigger);
		utrnIO.setFormat(utrnrec);
		utrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			regpsubrec.subStatuz.set(utrnIO.getStatuz());
		}
	}

protected void validateWdlAmount2600()
	{
		/*START*/
		compute(wsaaWithdrawalAmount, 2).set(add(wsaaCalcAmount, wsaaDiffAmount));
		/*     Checks the minimum withdrawal amount.*/
		if (isLT(wsaaWithdrawalAmount, wsaaWdlAmount)) {
			regpsubrec.subStatuz.set(wsaaLessMinAmt);
			return ;
		}
		/*     Checks the remaining withdrawal amount.*/
		compute(wsaaRemainingAmount, 2).set(sub(wsaaTotalFundValue, wsaaWithdrawalAmount));
		if (isLT(wsaaRemainingAmount, wsaaWdrem)) {
			regpsubrec.subStatuz.set(wsaaInsuffFund);
			return ;
		}
		/*EXIT*/
	}

protected void getTrigger3100()
	{
		read3110();
	}

protected void read3110()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(regpIO.getChdrcoy());
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(covrrgwIO.getCrtable());
		itdmIO.setItmfrm(regpIO.getNextPaydate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(itdmIO.getStatuz());
			return ;
		}
		if ((isNE(itdmIO.getItemcoy(), regpIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), t5540))
		|| (isNE(itdmIO.getItemitem(), covrrgwIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(h055);
			return ;
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(regpIO.getChdrcoy());
		itdmIO.setItemtabl(t6598);
		itdmIO.setItemitem(t5540rec.wdmeth);
		itdmIO.setItmfrm(regpIO.getNextPaydate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(itdmIO.getStatuz());
			return ;
		}
		if ((isNE(itdmIO.getItemcoy(), regpIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), t6598))
		|| (isNE(itdmIO.getItemitem(), t5540rec.wdmeth))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(g371);
			return ;
		}
		else {
			t6598rec.t6598Rec.set(itdmIO.getGenarea());
		}
	}

protected void readCovrRec4100()
	{
		start4100();
	}

protected void start4100()
	{
		/*  Read the COVRRGW file to obtain the coverage information*/
		covrrgwIO.setDataArea(SPACES);
		covrrgwIO.setChdrcoy(regpIO.getChdrcoy());
		covrrgwIO.setChdrnum(regpIO.getChdrnum());
		covrrgwIO.setLife(regpIO.getLife());
		covrrgwIO.setCoverage(regpIO.getCoverage());
		covrrgwIO.setRider(regpIO.getRider());
		if ((isEQ(regpIO.getPlanSuffix(), ZERO))
		|| (isLTE(regpIO.getPlanSuffix(), chdrrgpIO.getPolsum()))) {
			covrrgwIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi 
			covrrgwIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrrgwIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
			covrrgwIO.setPlanSuffix(999);
		}
		else {
			covrrgwIO.setFunction(varcom.readr);
			covrrgwIO.setPlanSuffix(regpIO.getPlanSuffix());
		}
		SmartFileCode.execute(appVars, covrrgwIO);
		if ((isNE(covrrgwIO.getStatuz(), varcom.oK))
		&& (isNE(covrrgwIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(covrrgwIO.getStatuz());
			return ;
		}
		if ((isNE(regpIO.getChdrcoy(), covrrgwIO.getChdrcoy()))
		|| (isNE(regpIO.getChdrnum(), covrrgwIO.getChdrnum()))
		|| (isNE(regpIO.getLife(), covrrgwIO.getLife()))
		|| (isNE(regpIO.getCoverage(), covrrgwIO.getCoverage()))
		|| (isNE(regpIO.getRider(), covrrgwIO.getRider()))
		|| (isEQ(covrrgwIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(varcom.endp);
		}
		if (isEQ(covrrgwIO.getFunction(), varcom.readr)) {
			if (isNE(regpIO.getPlanSuffix(), covrrgwIO.getPlanSuffix())) {
				regpsubrec.subStatuz.set(varcom.endp);
			}
		}
	}

protected void readT66475000()
	{
		start5000();
	}

protected void start5000()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(regpIO.getChdrcoy());
		itdmIO.setItemtabl(t6647);
		wsaaTabBatckey.set(regpsubrec.subBatctrcde);
		wsaaCnttype.set(chdrrgpIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(regpsubrec.subEffdate);
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(itdmIO.getStatuz());
			return ;
		}
		if ((isNE(itdmIO.getItempfx(), "IT"))
		|| (isNE(wsaaT6647Key, itdmIO.getItemitem()))
		|| (isNE(regpIO.getChdrcoy(), itdmIO.getItemcoy()))
		|| (isNE(itdmIO.getItemtabl(), t6647))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(h965);
			return ;
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT55155100()
	{
		start5100();
	}

protected void start5100()
	{
		/*     Read the Table T5515 to find the Fund currency.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(regpIO.getChdrcoy());
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(utrsIO.getUnitVirtualFund());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			regpsubrec.subStatuz.set(h116);
			return ;
		}
		else {
			t5515rec.t5515Rec.set(itemIO.getGenarea());
		}
	}

protected void readT56455200()
	{
		start5200();
	}

protected void start5200()
	{
		/*     Read the Table T5645 to obtain the sub account code/type*/
		itemIO.setItemcoy(regpsubrec.subBatccoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem("REGWPAY");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			regpsubrec.subStatuz.set(itemIO.getStatuz());
			return ;
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem("REGWPAY");
		descIO.setDesccoy(regpsubrec.subBatccoy);
		descIO.setLanguage(regpsubrec.subLanguage);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			regpsubrec.subStatuz.set(descIO.getStatuz());
			return ;
		}
	}

protected void readT55405300()
	{
		start5300();
	}

protected void start5300()
	{
		/*     Read the Table T5540 to obtain the regular withdrawal*/
		/*     method.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(regpIO.getChdrcoy());
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(covrrgwIO.getCrtable());
		itdmIO.setItmfrm(regpsubrec.subEffdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(itdmIO.getStatuz());
			return ;
		}
		if ((isNE(itdmIO.getItemcoy(), regpIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), t5540))
		|| (isNE(itdmIO.getItemitem(), covrrgwIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(h055);
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
	}

protected void readT55425400()
	{
		start5400();
	}

protected void start5400()
	{
		/*  Read the Table T5542 to obtain the withdrawal information*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(regpIO.getChdrcoy());
		itdmIO.setItemtabl(t5542);
		wsaaT5542KeyWdmeth.set(t5540rec.wdmeth);
		wsaaT5542KeyCurrcd.set(regpIO.getCurrcd());
		itdmIO.setItemitem(wsaaT5542Key);
		itdmIO.setItmfrm(regpsubrec.subEffdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(itdmIO.getStatuz());
			return ;
		}
		if ((isNE(itdmIO.getItempfx(), "IT"))
		|| (isNE(itdmIO.getItemcoy(), regpIO.getChdrcoy()))
		|| (isNE(itdmIO.getItemtabl(), t5542))
		|| (isNE(itdmIO.getItemitem(), wsaaT5542Key))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			regpsubrec.subStatuz.set(h388);
			return ;
		}
		else {
			t5542rec.t5542Rec.set(itdmIO.getGenarea());
		}
		/* IF  REGP-REGPAYFREQ = '00'                                   */
		/*     MOVE 1 TO SUB1                                           */
		/* ELSE                                                         */
		/*    IF  REGP-REGPAYFREQ = '01'                                */
		/*        MOVE 2 TO SUB1                                        */
		/*    ELSE                                                      */
		/*       IF  REGP-REGPAYFREQ = '02'                             */
		/*           MOVE 3 TO SUB1                                     */
		/*       ELSE                                                   */
		/*           IF  REGP-REGPAYFREQ = '04'                         */
		/*               MOVE 4 TO SUB1                                 */
		/*           ELSE                                               */
		/*               MOVE 5 TO SUB1                                 */
		/*           END-IF                                             */
		/*       END-IF                                                 */
		/*    END-IF                                                    */
		/* END-IF.                                                      */
		/* MOVE T5542-FEEMAX (SUB1) TO WSAA-FEEMAX.                     */
		/* MOVE T5542-FEEMIN (SUB1) TO WSAA-FEEMIN.                     */
		/* MOVE T5542-FEEPC  (SUB1) TO WSAA-FEEPC.                      */
		/* MOVE T5542-FFAMT  (SUB1) TO WSAA-FFAMT.                      */
		/* MOVE T5542-WDL-AMOUNT  (SUB1) TO WSAA-WDL-AMOUNT.            */
		/* MOVE T5542-WDREM       (SUB1) TO WSAA-WDREM.                 */
		/* Check frequency with those on T5542.                            */
		wsaaSub.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 6)); wsaaSub.add(1)){
			if (isEQ(regpIO.getRegpayfreq(), t5542rec.billfreq[wsaaSub.toInt()])) {
				wsaaWdlAmount.set(t5542rec.wdlAmount[wsaaSub.toInt()]);
				wsaaWdrem.set(t5542rec.wdrem[wsaaSub.toInt()]);
				/*        MOVE 10  TO WSAA-SUB*/
				return ;
			}
		}
		/* IF WSAA-SUB   > 9                                    <LA4555>*/
		if (isGT(wsaaSub, 6)) {
			regpsubrec.subStatuz.set(h388);
			return ;
		}
	}

protected void updateRegp5500()
	{
		/*UPDATE*/
		if (isNE(regpIO.getPymt(), ZERO)) {
			setPrecision(regpIO.getTotamnt(), 2);
			regpIO.setTotamnt(add(regpIO.getTotamnt(), wsaaPymt));
		}
		regpIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			regpsubrec.subStatuz.set(regpIO.getStatuz());
		}
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(regpIO.getChdrcoy());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(regpIO.getCurrcd());
		zrdecplrec.batctrcde.set(regpsubrec.subBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			regpsubrec.subStatuz.set(zrdecplrec.statuz);
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-UTRS-DATA-ARRAY--INNER
 */
private static final class WsaaUtrsDataArrayInner { 

	private FixedLengthStringData wsaaUtrsDataArray = new FixedLengthStringData(4356);
	private FixedLengthStringData[] wsaaUtrsData = FLSArrayPartOfStructure(99, 44, wsaaUtrsDataArray, 0);
	private FixedLengthStringData[] wsaaUtrsLife = FLSDArrayPartOfArrayStructure(2, wsaaUtrsData, 0);
	private FixedLengthStringData[] wsaaUtrsCoverage = FLSDArrayPartOfArrayStructure(2, wsaaUtrsData, 2);
	private FixedLengthStringData[] wsaaUtrsRider = FLSDArrayPartOfArrayStructure(2, wsaaUtrsData, 4);
	private PackedDecimalData[] wsaaUtrsPlanSuffix = PDArrayPartOfArrayStructure(4, 0, wsaaUtrsData, 6);
	private FixedLengthStringData[] wsaaUtrsUnitVirtualFund = FLSDArrayPartOfArrayStructure(4, wsaaUtrsData, 9);
	private FixedLengthStringData[] wsaaUtrsUnitType = FLSDArrayPartOfArrayStructure(1, wsaaUtrsData, 13);
	private FixedLengthStringData[] wsaaUtrsFundCurrency = FLSDArrayPartOfArrayStructure(3, wsaaUtrsData, 14);
	private PackedDecimalData[] wsaaUtrsCurrentUnitBal = PDArrayPartOfArrayStructure(16, 5, wsaaUtrsData, 17);
	private PackedDecimalData[] wsaaUtrsFundValue = PDArrayPartOfArrayStructure(17, 2, wsaaUtrsData, 26);
	private PackedDecimalData[] wsaaUtrsWithdrawalAmt = PDArrayPartOfArrayStructure(17, 2, wsaaUtrsData, 35);
}
}
