package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:18
 * Description:
 * Copybook name: LAPTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Laptkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData laptFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData laptKey = new FixedLengthStringData(64).isAPartOf(laptFileKey, 0, REDEFINE);
  	public FixedLengthStringData laptChdrcoy = new FixedLengthStringData(1).isAPartOf(laptKey, 0);
  	public FixedLengthStringData laptChdrnum = new FixedLengthStringData(8).isAPartOf(laptKey, 1);
  	public FixedLengthStringData laptLife = new FixedLengthStringData(2).isAPartOf(laptKey, 9);
  	public FixedLengthStringData laptCoverage = new FixedLengthStringData(2).isAPartOf(laptKey, 11);
  	public FixedLengthStringData laptRider = new FixedLengthStringData(2).isAPartOf(laptKey, 13);
  	public PackedDecimalData laptPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(laptKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(laptKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(laptFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		laptFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}