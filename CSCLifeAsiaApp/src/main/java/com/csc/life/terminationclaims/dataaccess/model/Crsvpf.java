package com.csc.life.terminationclaims.dataaccess.model;


import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CRSVPF", schema = "VM1DTA")
public class Crsvpf implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private long uniqueNumber;
	private String wclmpfx;
	private String wclmcoy;
	private String chdrnum;
	private Integer tranno;
	private String trcode;
	private Integer condte;
	private String claim;
	private BigDecimal balo;
	private BigDecimal wpaid;
	private String wprcl;
	private BigDecimal wreqamt;
	private String wrscd;
	private String termid;
	private String validflag;
	private Integer trdt;
	private Integer trtm;
	private Integer userT;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private String regflg;
	private Integer paydate;
	

	public Crsvpf() {
	}

	public Crsvpf(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	
	@Id
	@Column(name = "UNIQUE_NUMBER", unique = true, nullable = false, precision = 18, scale = 0)
	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	
	@Column(name = "WCLMPFX", length = 2)
	public String getWclmpfx() {
		return wclmpfx;
	}

	public void setWclmpfx(String wclmpfx) {
		this.wclmpfx = wclmpfx;
	}
	@Column(name = "WCLMCOY", length = 1)
	public String getWclmcoy() {
		return wclmcoy;
	}

	public void setWclmcoy(String wclmcoy) {
		this.wclmcoy = wclmcoy;
	}
	@Column(name = "CHDRNUM", length = 12)
	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	@Column(name = "TRANNO", precision = 5, scale = 0)
	public Integer getTranno() {
		return tranno;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	@Column(name = "TRCODE", length = 4)
	public String getTrcode() {
		return trcode;
	}

	public void setTrcode(String trcode) {
		this.trcode = trcode;
	}
	@Column(name = "CONDTE", precision = 8, scale = 0)
	public Integer getCondte() {
		return condte;
	}

	public void setCondte(Integer condte) {
		this.condte = condte;
	}
	@Column(name = "CLAIM", length = 8)
	public String getClaim() {
		return claim;
	}

	public void setClaim(String claim) {
		this.claim = claim;
	}
	@Column(name = "BALO", precision = 13, scale = 2)
	public BigDecimal getBalo() {
		return balo;
	}

	public void setBalo(BigDecimal balo) {
		this.balo = balo;
	}
	@Column(name = "WPAID", precision = 13, scale = 2)
	public BigDecimal getWpaid() {
		return wpaid;
	}

	public void setWpaid(BigDecimal wpaid) {
		this.wpaid = wpaid;
	}
	@Column(name = "WPRCL", length = 4)
	public String getWprcl() {
		return wprcl;
	}

	public void setWprcl(String wprcl) {
		this.wprcl = wprcl;
	}
	@Column(name = "WREQAMT", precision = 13, scale = 2)
	public BigDecimal getWreqamt() {
		return wreqamt;
	}

	public void setWreqamt(BigDecimal wreqamt) {
		this.wreqamt = wreqamt;
	}
	@Column(name = "WRSCD", length = 2)
	public String getWrscd() {
		return wrscd;
	}
	
	public void setWrscd(String wrscd) {
		this.wrscd = wrscd;
	}
	@Column(name = "TERMID", length = 4)
	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}
	@Column(name = "VALIDFLAG", length = 1)
	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	@Column(name = "TRDT", precision = 6, scale = 0)
	public Integer getTrdt() {
		return trdt;
	}

	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	@Column(name = "TRTM", precision = 6, scale = 0)
	public Integer getTrtm() {
		return trtm;
	}
	
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	@Column(name = "USER_T", precision = 6, scale = 0)
	public Integer getUserT() {
		return userT;
	}

	public void setUserT(Integer userT) {
		this.userT = userT;
	}
	@Column(name = "USRPRF", length = 10)
	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	@Column(name = "JOBNM", length = 10)
	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATIME", length = 11)
	public Date getDatime() {
		return datime;
	}

	public void setDatime(Date datime) {
		this.datime = datime;
	}
	@Column(name = "REGFLG", length = 1)
	public String getRegflg() {
		return regflg;
	}

	public void setRegflg(String regflg) {
		this.regflg = regflg;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getPaydate() {
		return paydate;
	}

	public void setPaydate(Integer paydate) {
		this.paydate = paydate;
	}
}
