package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.life.cashdividends.tablestructures.Th528rec;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.tablestructures.T5585rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.underwriting.procedures.Actcalc;
import com.csc.life.underwriting.recordstructures.Actcalcrec;
import com.csc.life.underwriting.tablestructures.T6642rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;


// Create a new Surrender calculation routine to calculate the cash value based on the sum assured 
// and the given cash value rate. The cash value rate is varied by entry age and policy year.

public class Zcvsurc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaLifeInd = new FixedLengthStringData(1);
	private Validator singleLife = new Validator(wsaaLifeInd, "1");
	private Validator jointLife = new Validator(wsaaLifeInd, "2");
		/* WSAA-LIFE-SEXES */
	private FixedLengthStringData wsaaLife1Sex = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaLife2Sex = new FixedLengthStringData(1);
		/* WSAA-LIFE-AGES */
	private ZonedDecimalData wsaaLife1AnbAtCcd = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLife2AnbAtCcd = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbAtCcd = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbAtRcessDte = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbAtPcessDte = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaRcessTerm = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaPcessTerm = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaAgeDifference = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaAnbDiff = new ZonedDecimalData(3, 0);

	private FixedLengthStringData wsaaAssType = new FixedLengthStringData(1);
	private Validator wholeLife = new Validator(wsaaAssType, "W");
	private Validator endowment = new Validator(wsaaAssType, "E");
	/*private PackedDecimalData wsaaSurrenderVal = new PackedDecimalData(18, 9);
	private PackedDecimalData wsaaReserveFactor = new PackedDecimalData(18, 9);
		TMLII-2276 	Nilai Tunai Polis Digits (Clone TMLII-2283)

	*/
	private PackedDecimalData wsaaSurrenderVal = new PackedDecimalData(14, 3);
	private PackedDecimalData wsaaReserveFactor = new PackedDecimalData(14, 3);
	private FixedLengthStringData wsaaSurrFlag = new FixedLengthStringData(1).init(SPACES);
		/* WSAA-EXACT-VALUES */
	private PackedDecimalData wsaaBiga = new PackedDecimalData(18, 10);
	private PackedDecimalData wsaaAdue = new PackedDecimalData(18, 10);
	private PackedDecimalData wsaaNetPremium = new PackedDecimalData(18, 10);
		/* WSAA-T-VALUES */
	private ZonedDecimalData wsaaIntLowTBiga = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIntHighTBiga = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIntLowTAdue = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIntHighTAdue = new ZonedDecimalData(3, 0).setUnsigned();
		/* WSAA-FRACTIONS-DURATIONS */
	private ZonedDecimalData wsaaFractionBiga = new ZonedDecimalData(6, 5);
	private ZonedDecimalData wsaaDurationBiga = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaFractionAdue = new ZonedDecimalData(6, 5);
	private ZonedDecimalData wsaaDurationAdue = new ZonedDecimalData(11, 5);
		/* WSAA-BIGA-ADUE-VALUES */
	private PackedDecimalData wsaaLowValue = new PackedDecimalData(18, 10);
	private PackedDecimalData wsaaHighValue = new PackedDecimalData(18, 10);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaRunIndicator = new FixedLengthStringData(1);
	private Validator firstTimeThrough = new Validator(wsaaRunIndicator, " ");
	private Validator secondTimeThrough = new Validator(wsaaRunIndicator, "B");

	private FixedLengthStringData wsaaAcblKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAcblChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaAcblKey, 0);
	private FixedLengthStringData wsaaAcblLife = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 8);
	private FixedLengthStringData wsaaAcblCoverage = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 10);
	private FixedLengthStringData wsaaAcblRider = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 12);
	private FixedLengthStringData wsaaAcblPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 14);
	private PackedDecimalData wsaaAcblCurrentBalance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaReserveTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaBonusTot = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaProcessingType = new FixedLengthStringData(1);
	private Validator wholePlan = new Validator(wsaaProcessingType, "1");
	private Validator singleComponent = new Validator(wsaaProcessingType, "2");
	private Validator notionalSingleComponent = new Validator(wsaaProcessingType, "3");
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		/* ERRORS */
	private String g344 = "G344";
	private String e031 = "E031";
	private String g580 = "G580";
	private String g584 = "G584";
	private String g585 = "G585";
	private String f262 = "F262";
	private String h134 = "H134";
	private String h135 = "H135";
	private String h053 = "H053";
	private String h155 = "H155";
	private String e544 = "E544";
	private String hl62 = "HL62";
		/* FORMATS */
	private String covrtrbrec = "COVRTRBREC";
	private String acblrec = "ACBLREC";
	private String itemrec = "ITEMREC";
	private String chdrsurrec = "CHDRSURREC";
		/* TABLES */
	private String th528 = "TH528";
	private String t5585 = "T5585";
	private String t5645 = "T5645";
	private String t5687 = "T5687";
	private String t3695 = "T3695";
	private String th506 = "TH506";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
	private Actcalcrec actcalcrec = new Actcalcrec();
		/*Contract Header - Surrenders*/
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
		/*COVR layout for trad. reversionary bonus*/
	private CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5585rec t5585rec = new T5585rec();
	private T5645rec t5645rec = new T5645rec();
	private Th528rec th528rec = new Th528rec();
	private Th506rec th506rec = new Th506rec();
	private Varcom varcom = new Varcom();
	
	private FixedLengthStringData  wsaaItem	 = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaAge = new ZonedDecimalData(2, 0);	
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaL	= new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaH	= new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaDay = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCvl = new PackedDecimalData(11, 3);
	private PackedDecimalData wsaaCvh = new PackedDecimalData(11, 3);
	private FixedLengthStringData wsaaSubr = new FixedLengthStringData(8).init("ZCVSURC");
	            
	private ZonedDecimalData wsaaSurcCrrcddate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaSurcCrrcddate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillmonth = new ZonedDecimalData(2, 0).isAPartOf(filler, 4).setUnsigned();
	private ZonedDecimalData wsaaBillday = new ZonedDecimalData(2, 0).isAPartOf(filler, 6).setUnsigned();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue2160, 
		exit2399, 
		exit3199, 
		exit4099, 
		seExit9090, 
		dbExit9190
	}

	public Zcvsurc() { 
		super();
	}

public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		initialise2000();
		while ( !(isEQ(covrtrbIO.getStatuz(),varcom.endp))) {
			readAcbl7000();
			calcSurrenderValue4000();
			if (singleComponent.isTrue()
			|| notionalSingleComponent.isTrue()) {
				covrtrbIO.setStatuz(varcom.endp);
			}
			else {
				readCovr6000();
			}
		}
		setExitVariables5000();
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		syserrrec.subrname.set(wsaaSubr);
		wsaaSurrFlag.set(SPACES);
		if (isEQ(srcalcpy.status,"BONS")) {
			wsaaRunIndicator.set("B");
		}
		else {
			wsaaRunIndicator.set(srcalcpy.endf);
		}
		if (!firstTimeThrough.isTrue()
		&& !secondTimeThrough.isTrue()) {
			syserrrec.params.set(srcalcpy.endf);
			syserrrec.statuz.set(h155);
			systemError9000();
		}
		wsaaReserveTot.set(ZERO);
		wsaaBonusTot.set(ZERO);
		if (isEQ(srcalcpy.planSuffix,ZERO)) {
			wsaaProcessingType.set("1");
		}
		else {
			if (isLTE(srcalcpy.planSuffix,srcalcpy.polsum)
			&& isNE(srcalcpy.polsum,1)) {
				wsaaProcessingType.set("3");
			}
			else {
				wsaaProcessingType.set("2");
			}
		}
		readFiles2100();
		readTH528SetBasis2200();
		if (firstTimeThrough.isTrue()) {
			getSaDescription2400();
		}
		else {
			getRbDescAndAmt2500();
		}
	}

protected void readFiles2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2100();
					catchAll2150();
				}
				case continue2160: {
					continue2160();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2100()
	{
		chdrsurIO.setDataArea(SPACES);
		chdrsurIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		chdrsurIO.setChdrnum(srcalcpy.chdrChdrnum);
		chdrsurIO.setFormat(chdrsurrec);
		chdrsurIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(),varcom.oK)
		&& isNE(chdrsurIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrsurIO.getParams());
			dbError9100();
		}
		if (isEQ(chdrsurIO.getStatuz(),varcom.mrnf)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(srcalcpy.chdrChdrcoy.toString());
			stringVariable1.append(srcalcpy.chdrChdrnum.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(e544);
			dbError9100();
		}
		wsaaSurrFlag.set("N");
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrsurIO.getChdrcoy());
		itemIO.setItemtabl(th506);
		itemIO.setItemitem(chdrsurIO.getCnttype());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.continue2160);
		}
	}

protected void catchAll2150()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrsurIO.getChdrcoy());
		itemIO.setItemtabl(th506);
		itemIO.setItemitem("***");
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(hl62);
			dbError9100();
		}
	}

protected void continue2160()
	{
		th506rec.th506Rec.set(itemIO.getGenarea());
		if (isNE(th506rec.yearInforce,ZERO)) {
			datcon2rec.intDate1.set(chdrsurIO.getOccdate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(th506rec.yearInforce);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				dbError9100();
			}
			if (isGT(chdrsurIO.getPtdate(),datcon2rec.intDate2)) {
				wsaaSurrFlag.set("Y");
			}
		}
		else {
			wsaaSurrFlag.set("Y");
		}
		covrtrbIO.setDataArea(SPACES);
		covrtrbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		covrtrbIO.setChdrnum(srcalcpy.chdrChdrnum);
		covrtrbIO.setLife(srcalcpy.lifeLife);
		covrtrbIO.setCoverage(srcalcpy.covrCoverage);
		covrtrbIO.setRider(srcalcpy.covrRider);
		covrtrbIO.setPlanSuffix(srcalcpy.planSuffix);
		if (notionalSingleComponent.isTrue()) {
			covrtrbIO.setPlanSuffix(0);
		}
		covrtrbIO.setFormat(covrtrbrec);
		covrtrbIO.setFunction(varcom.begn);
		covrtrbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrtrbIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)
		&& isNE(covrtrbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrtrbIO.getParams());
			syserrrec.statuz.set(covrtrbIO.getStatuz());
			dbError9100();
		}
		if (isNE(covrtrbIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(covrtrbIO.getLife(),srcalcpy.lifeLife)
		|| isNE(covrtrbIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(covrtrbIO.getRider(),srcalcpy.covrRider)
		|| isEQ(covrtrbIO.getStatuz(),varcom.endp)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(srcalcpy.chdrChdrcoy.toString());
			stringVariable1.append(srcalcpy.chdrChdrnum.toString());
			stringVariable1.append(srcalcpy.lifeLife.toString());
			stringVariable1.append(srcalcpy.covrCoverage.toString());
			stringVariable1.append(srcalcpy.covrRider.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(g344);
			dbError9100();
		}
		if ((singleComponent.isTrue()
		&& isNE(covrtrbIO.getPlanSuffix(),srcalcpy.planSuffix))
		|| (notionalSingleComponent.isTrue()
		&& isNE(covrtrbIO.getPlanSuffix(),0))) {
			wsaaPlan.set(srcalcpy.planSuffix);
			StringBuilder stringVariable2 = new StringBuilder();
			stringVariable2.append(srcalcpy.chdrChdrcoy.toString());
			stringVariable2.append(srcalcpy.chdrChdrnum.toString());
			stringVariable2.append(srcalcpy.lifeLife.toString());
			stringVariable2.append(srcalcpy.covrCoverage.toString());
			stringVariable2.append(srcalcpy.covrRider.toString());
			stringVariable2.append(wsaaPlan.toString());
			syserrrec.params.setLeft(stringVariable2.toString());
			syserrrec.statuz.set(g344);
			dbError9100();
		}
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		lifelnbIO.setChdrnum(srcalcpy.chdrChdrnum);
		lifelnbIO.setLife(srcalcpy.lifeLife);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			dbError9100();
		}
		wsaaLife1Sex.set(lifelnbIO.getCltsex());
		wsaaLife1AnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		lifelnbIO.setChdrcoy(srcalcpy.chdrChdrcoy);
		lifelnbIO.setChdrnum(srcalcpy.chdrChdrnum);
		lifelnbIO.setLife(srcalcpy.lifeLife);
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			dbError9100();
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			wsaaLife2Sex.set(SPACES);
			wsaaLifeInd.set("1");
		}
		else {
			wsaaLife2Sex.set(lifelnbIO.getCltsex());
			wsaaLife2AnbAtCcd.set(lifelnbIO.getAnbAtCcd());
			wsaaLifeInd.set("2");
		}
	}

protected void readTH528SetBasis2200()
	{
		start2200();
	}

protected void start2200()
	{
		wsaaAge.set(covrtrbIO.getAnbAtCcd());
		wsaaItem.set(srcalcpy.crtable+""+wsaaAge+""+covrtrbIO.getMortcls()+""+covrtrbIO.getSex());
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(th528);
		itdmIO.setItemitem(wsaaItem);
		itdmIO.setItmfrm(srcalcpy.crrcd);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isEQ(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		&& isEQ(itdmIO.getItemtabl(),th528)
		&& isEQ(itdmIO.getItemitem(),wsaaItem)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			
			th528rec.th528Rec.set(itdmIO.getGenarea());
			return;	
		}
		else{
			
			wsaaItem.set(srcalcpy.crtable+""+wsaaAge+""+covrtrbIO.getMortcls()+"*" );
			itdmIO.setItemitem(wsaaItem);
			itdmIO.setItempfx("IT");
			itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
			itdmIO.setItemtabl(th528);
			itdmIO.setItmfrm(srcalcpy.crrcd);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				dbError9100();
			}
			if (isEQ(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
			&& isEQ(itdmIO.getItemtabl(),th528)
			&& isEQ(itdmIO.getItemitem(),wsaaItem)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				th528rec.th528Rec.set(itdmIO.getGenarea());
				return;
			}
			else
			 {	
				wsaaItem.set(srcalcpy.crtable+""+wsaaAge+"**");
				itdmIO.setItemitem(wsaaItem);
				itdmIO.setItempfx("IT");
				itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
				itdmIO.setItemtabl(th528);
				itdmIO.setItmfrm(srcalcpy.crrcd);
				itdmIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, itdmIO);
				if (isNE(itdmIO.getStatuz(),varcom.oK)
				&& isNE(itdmIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(itdmIO.getParams());
					syserrrec.statuz.set(itdmIO.getStatuz());
					dbError9100();
				}
				if (isEQ(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
				&& isEQ(itdmIO.getItemtabl(),th528)
				&& isEQ(itdmIO.getItemitem(),wsaaItem)
				&& isNE(itdmIO.getStatuz(),varcom.endp)) {
					th528rec.th528Rec.set(itdmIO.getGenarea());
					return;
				}
				else{	
						wsaaItem.set(srcalcpy.crtable+""+"***");
						itdmIO.setItemitem(wsaaItem);
						itdmIO.setItempfx("IT");
						itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
						itdmIO.setItemtabl(th528);
						itdmIO.setItmfrm(srcalcpy.crrcd);
						itdmIO.setFunction(varcom.begn);
						SmartFileCode.execute(appVars, itdmIO);
						if (isNE(itdmIO.getStatuz(),varcom.oK)
						&& isNE(itdmIO.getStatuz(),varcom.endp)) {
							syserrrec.params.set(itdmIO.getParams());
							syserrrec.statuz.set(itdmIO.getStatuz());
							dbError9100();
						}
							
						if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
						&& isNE(itdmIO.getItemtabl(),th528)
						&& isNE(itdmIO.getItemitem(),wsaaItem)
						&& isEQ(itdmIO.getStatuz(),varcom.endp)){	
						syserrrec.params.set(srcalcpy.crtable);
						syserrrec.statuz.set(e031);
						dbError9100();
					}
						if (isEQ(itdmIO.getStatuz(),varcom.oK))
								{
									th528rec.th528Rec.set(itdmIO.getGenarea());
									return;
								
									
								}
									
				 }
				}
			}
	}

protected void getSaDescription2400()
	{
		start2400();
	}

protected void start2400()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(srcalcpy.crtable);
		descIO.setLanguage(srcalcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(h053);
			syserrrec.params.set(descIO.getParams());
			systemError9000();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			srcalcpy.description.fill("?");
		}
		else {
			srcalcpy.description.set(descIO.getShortdesc());
		}
	}

protected void getRbDescAndAmt2500()
	{
		start2500();
	}

protected void start2500()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(h134);
			systemError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t3695);
		descIO.setDescitem(t5645rec.sacstype01);
		descIO.setLanguage(srcalcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(h135);
			systemError9000();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			srcalcpy.description.fill("?");
		}
		else {
			srcalcpy.description.set(descIO.getShortdesc());
		}
	}

protected void calcSurrenderValue4000()
	{
		try {
			start4000();
		}
		catch (GOTOException e){
		}
	}

protected void start4000()
	{
		actcalcrec.company.set(srcalcpy.chdrChdrcoy);
		wsaaSurrenderVal.set(0);
		wsaaBiga.set(0);
		wsaaNetPremium.set(0);
		wsaaAdue.set(0);
		
		if (secondTimeThrough.isTrue()
		&& isEQ(wsaaAcblCurrentBalance,0)) {
			goTo(GotoLabel.exit4099);
		}
		if (firstTimeThrough.isTrue()){
			calcSurrenderValue4000A();
		}
		else{
			calcSurrenderValue4000B();
		}
		wsaaReserveTot.add(wsaaSurrenderVal);
	}
		
		
	protected void calcSurrenderValue4000A(){
			
		if(isGTE(srcalcpy.ptdate,covrtrbIO.getPremCessDate())) {
			wsaaEffdate.set(srcalcpy.effdate);
		}
		else{
			if(isGTE(srcalcpy.ptdate,srcalcpy.effdate)) {
				wsaaEffdate.set(srcalcpy.effdate);	
			}
			else{
				wsaaEffdate.set(srcalcpy.ptdate);	
			}
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.crrcd);
		datcon3rec.intDate2.set(wsaaEffdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaL.set(datcon3rec.freqFactor);
		wsaaH.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor,ZERO)) {	
			compute(wsaaH, 3).set(add(wsaaL,1));
		}
		
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(srcalcpy.crrcd);
		datcon4rec.intDate2.set(ZERO);
		datcon4rec.frequency.set("01");
		datcon4rec.freqFactor.set(wsaaL);
		datcon4rec.billday.set(wsaaBillday);
		datcon4rec.billmonth.set(wsaaBillmonth);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz,varcom.oK)) {
			datcon4rec.intDate2.set(0);
		}

		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(datcon4rec.intDate2);
		datcon3rec.intDate2.set(wsaaEffdate);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaDay.set(datcon3rec.freqFactor);
		if(isEQ(wsaaL,ZERO)){
			wsaaCvl.set(ZERO);	
		}
		else{
			if(isGT(wsaaL,99)){
				wsaaCvl.set(th528rec.instpr);	
			}
			else{
				wsaaCvl.set(th528rec.insprm[wsaaL.toInt()]);	
			}
		}
		if(isEQ(wsaaH,ZERO)){
			wsaaCvh.set(ZERO);	
		}
		else{
			if(isGT(wsaaL,99)){
				wsaaCvh.set(th528rec.instpr);	
			}
			else{
				wsaaCvh.set(th528rec.insprm[wsaaH.toInt()]);	
			}
		}
		compute(wsaaSurrenderVal, 3).setRounded(div(mult(add(wsaaCvl,mult(sub(wsaaCvh,wsaaCvl),div(wsaaDay,365))),covrtrbIO.getSumins()),mult(th528rec.unit,th528rec.premUnit)));
	}
	
protected void calcSurrenderValue4000B(){
	
	wsaaSurrenderVal.set(wsaaAcblCurrentBalance);

	}

protected void setExitVariables5000()
	{	
		start5000();
	}

protected void start5000()
	{
		if (isEQ(wsaaSurrFlag,"N")) {
			wsaaReserveTot.set(ZERO);
			wsaaBonusTot.set(ZERO);
		}
		srcalcpy.actualVal.set(wsaaReserveTot);
		srcalcpy.estimatedVal.set(0);
		if (firstTimeThrough.isTrue()) {
			srcalcpy.status.set("****");
			srcalcpy.endf.set("B");
			srcalcpy.type.set("S");
		}
		else {
			if (isEQ(srcalcpy.status,"BONS")) {
				srcalcpy.estimatedVal.set(wsaaBonusTot);
			}
			srcalcpy.status.set("ENDP");
			srcalcpy.endf.set(SPACES);
			srcalcpy.type.set("B");
		}
	}

protected void readCovr6000()
	{
		/*START*/
		covrtrbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)
		&& isNE(covrtrbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrtrbIO.getParams());
			syserrrec.statuz.set(covrtrbIO.getStatuz());
			dbError9100();
		}
		if (isNE(covrtrbIO.getChdrcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(covrtrbIO.getChdrnum(),srcalcpy.chdrChdrnum)
		|| isNE(covrtrbIO.getLife(),srcalcpy.lifeLife)
		|| isNE(covrtrbIO.getCoverage(),srcalcpy.covrCoverage)
		|| isNE(covrtrbIO.getRider(),srcalcpy.covrRider)) {
			covrtrbIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void readAcbl7000()
	{
		start7000();
	}

protected void start7000()
	{
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(srcalcpy.chdrChdrcoy);
		acblIO.setSacscode(t5645rec.sacscode01);
		wsaaAcblChdrnum.set(srcalcpy.chdrChdrnum);
		wsaaAcblLife.set(srcalcpy.lifeLife);
		wsaaAcblCoverage.set(srcalcpy.covrCoverage);
		wsaaAcblRider.set(srcalcpy.covrRider);
		wsaaPlan.set(covrtrbIO.getPlanSuffix());
		wsaaAcblPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaAcblKey);
		acblIO.setOrigcurr(srcalcpy.currcode);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			dbError9100();
		}
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			wsaaAcblCurrentBalance.set(0);
		}
		else {
			wsaaAcblCurrentBalance.set(acblIO.getSacscurbal());
		}
		wsaaBonusTot.add(wsaaAcblCurrentBalance);
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}

}
