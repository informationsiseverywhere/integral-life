package com.csc.life.terminationclaims.screens;

import com.csc.smart400framework.SmartVarModel;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import com.csc.common.DD;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sjl53ScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(257);  
	public FixedLengthStringData dataFields = new FixedLengthStringData(113).isAPartOf(dataArea, 0);
	public FixedLengthStringData paymentFrom =  DD.claimnumber.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData paymentDate = DD.srdate.copyToZonedDecimal().isAPartOf(dataFields,9);
	public FixedLengthStringData payee =  DD.clttwo.copy().isAPartOf(dataFields,17);
	public FixedLengthStringData payeeName =  DD.ownername.copy().isAPartOf(dataFields,27);
	public ZonedDecimalData amountFrom = DD.amountFrom.copyToZonedDecimal().isAPartOf(dataFields,74);
	public ZonedDecimalData amountTo = DD.amountTo.copyToZonedDecimal().isAPartOf(dataFields,91);
	public FixedLengthStringData paymentMethod = DD.reqntype.copy().isAPartOf(dataFields,108);
	public FixedLengthStringData bankcode = DD.bankcode.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData paymentStatus = DD.paymtstatus.copy().isAPartOf(dataFields,111);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 113);
	public FixedLengthStringData paymentFromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData paymentDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData payeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData payeeNameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData amountFromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData amountToErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData paymentMethodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData bankcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData paymentStatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 149);
	public FixedLengthStringData[] paymentFromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] paymentDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] payeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] payeeNameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] amountFromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] amountToOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] paymentMethodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] bankcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] paymentStatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(325);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(178).isAPartOf(subfileArea, 0);
	public ZonedDecimalData seqenum = DD.seq.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData paymentNum = DD.paymentNum.copy().isAPartOf(subfileFields,3);
	public ZonedDecimalData paymtDate = DD.srdate.copyToZonedDecimal().isAPartOf(subfileFields,12);
	public FixedLengthStringData clntId = DD.conOwner.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData amount = DD.docorigamt.copyToZonedDecimal().isAPartOf(subfileFields,70);
	public FixedLengthStringData paymtMethod = DD.claimType.copy().isAPartOf(subfileFields,87);
	public FixedLengthStringData bankAccount = DD.claimType.copy().isAPartOf(subfileFields,117);
	public FixedLengthStringData paymtStatus = DD.claimType.copy().isAPartOf(subfileFields,147);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,177);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 178);
	public FixedLengthStringData seqenumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData paymentNumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData paymtDateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData clntIdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData amountErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData paymtMethodErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData bankAccountErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData paymtStatusErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea,214);
	public FixedLengthStringData[] seqenumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] paymentNumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] paymtDateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] clntIdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] amountOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] paymtMethodOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] bankAccountOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] paymtStatusOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea,322);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public FixedLengthStringData paymtDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData paymentDateDisp = new FixedLengthStringData(10);
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	public LongData Sjl53screensflWritten = new LongData(0);
	public LongData Sjl53screenctlWritten = new LongData(0);
	public LongData Sjl53screenWritten = new LongData(0);
	public LongData Sjl53windowWritten = new LongData(0);
	public LongData Sjl53protectWritten = new LongData(0);
	public GeneralTable Sjl53screensfl = new GeneralTable(AppVars.getInstance());

	public GeneralTable getScreenSubfileTable() {
		return Sjl53screensfl;
	}

	public Sjl53ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		fieldIndMap.put(paymentFromOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payeeOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(seqenumOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {paymentFrom, paymentDate, payee, payeeName, amountFrom, amountTo, paymentMethod, bankcode, paymentStatus};
		screenOutFields = new BaseData[][] {paymentFromOut, paymentDateOut, payeeOut,payeeNameOut, amountFromOut, amountToOut, paymentMethodOut, bankcodeOut, paymentStatusOut};
		screenErrFields = new BaseData[] {paymentFromErr, paymentDateErr, payeeErr,payeeNameErr, amountFromErr, amountToErr, paymentMethodErr, bankcodeErr, paymentStatusErr};
		
		screenDateFields = new BaseData[] {paymentDate};
		screenDateErrFields = new BaseData[] {paymentDateErr};
		screenDateDispFields = new BaseData[] {paymentDateDisp};
		
		screenSflFields = new BaseData[] { seqenum, paymentNum, paymtDate, clntId, amount, paymtMethod, bankAccount, paymtStatus, select};
		screenSflOutFields = new BaseData[][] {seqenumOut, paymentNumOut, paymtDateOut, clntIdOut, amountOut, paymtMethodOut, bankAccountOut, paymtStatusOut, selectOut};
		screenSflErrFields = new BaseData[] {seqenumErr, paymentNumErr, paymtDateErr, clntIdErr, amountErr, paymtMethodErr, bankAccountErr, paymtStatusErr, selectErr};
		
		screenSflDateFields = new BaseData[] {paymtDate};
		screenSflDateErrFields = new BaseData[] {paymtDateErr};
		screenSflDateDispFields = new BaseData[] {paymtDateDisp};
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sjl53screen.class;
		screenSflRecord = Sjl53screensfl.class;
		screenCtlRecord = Sjl53screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sjl53protect.class;
		
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl53screenctl.lrec.pageSubfile);
	}

	private long maxRow;
	
	public long getMaxRow() {
		return maxRow;
	}

	public void setMaxRow(long maxRow) {
		this.maxRow = maxRow;
	}
	
	public boolean hasSubfile() {
		return true;
	}
}
