package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:45
 * Description:
 * Copybook name: REGPSUBREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regpsubrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData batcsubRec = new FixedLengthStringData(69);
  	public FixedLengthStringData subStatuz = new FixedLengthStringData(4).isAPartOf(batcsubRec, 0);
  	public FixedLengthStringData subCompany = new FixedLengthStringData(1).isAPartOf(batcsubRec, 4);
  	public ZonedDecimalData subEffdate = new ZonedDecimalData(8, 0).isAPartOf(batcsubRec, 5);
  	public FixedLengthStringData subLanguage = new FixedLengthStringData(1).isAPartOf(batcsubRec, 13);
  	public FixedLengthStringData subSubrname = new FixedLengthStringData(10).isAPartOf(batcsubRec, 14);
  	public FixedLengthStringData subBatckey = new FixedLengthStringData(20).isAPartOf(batcsubRec, 24);
  	public FixedLengthStringData subBatcpfx = new FixedLengthStringData(2).isAPartOf(subBatckey, 0);
  	public FixedLengthStringData subBatccoy = new FixedLengthStringData(1).isAPartOf(subBatckey, 2);
  	public FixedLengthStringData subBatcbrn = new FixedLengthStringData(2).isAPartOf(subBatckey, 3);
  	public PackedDecimalData subBatcactyr = new PackedDecimalData(4, 0).isAPartOf(subBatckey, 5);
  	public PackedDecimalData subBatcactmn = new PackedDecimalData(2, 0).isAPartOf(subBatckey, 9);
  	public FixedLengthStringData subBatctrcde = new FixedLengthStringData(4).isAPartOf(subBatckey, 11);
  	public FixedLengthStringData subBatcbatch = new FixedLengthStringData(5).isAPartOf(subBatckey, 15);
  	public FixedLengthStringData subCnttype = new FixedLengthStringData(3).isAPartOf(batcsubRec, 44);
  	public FixedLengthStringData subTranid = new FixedLengthStringData(22).isAPartOf(batcsubRec, 47);
  	public FixedLengthStringData subTermid = new FixedLengthStringData(4).isAPartOf(subTranid, 0);
  	public ZonedDecimalData subTranidN = new ZonedDecimalData(18, 0).isAPartOf(subTranid, 4).setUnsigned();
  	public ZonedDecimalData subTranidX = new ZonedDecimalData(18, 0).isAPartOf(subTranidN, 0, REDEFINE);
  	public ZonedDecimalData subDate = new ZonedDecimalData(6, 0).isAPartOf(subTranidX, 0).setUnsigned();
  	public ZonedDecimalData subTime = new ZonedDecimalData(6, 0).isAPartOf(subTranidX, 6).setUnsigned();
  	public ZonedDecimalData subUser = new ZonedDecimalData(6, 0).isAPartOf(subTranidX, 12).setUnsigned();


	public void initialize() {
		COBOLFunctions.initialize(batcsubRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		batcsubRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}