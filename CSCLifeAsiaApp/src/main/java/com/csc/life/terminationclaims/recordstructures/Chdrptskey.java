package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:22
 * Description:
 * Copybook name: CHDRPTSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrptskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrptsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrptsKey = new FixedLengthStringData(64).isAPartOf(chdrptsFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrptsChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrptsKey, 0);
  	public FixedLengthStringData chdrptsChdrnum = new FixedLengthStringData(8).isAPartOf(chdrptsKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrptsKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrptsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrptsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}