/*
 * File: Dthadd2.java
 * Date: 29 August 2009 22:47:10
 * Author: Quipoz Limited
 * 
 * Class transformed from DTHADD2.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.UtrnrevTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrclmTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.ClmdaddTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*
*        DEATH CLAIM ADDITIONAL PROCESSING METHOD 1.
*        -------------------------------------------
*
*                 PROGRAM NAME DTHADD2
*
* This program is an item entry on T6598, the death claim
* subroutine method table. This method is used in order to
* update the relevant sub-accounts. The trigger module and
* key were passed with other data passed within the details
* on the UTRN record.
*
*
* PROCESSING.
* ----------
*
* This routine is called once for each UTRN.
*
* Read the Claim detail record (CLMDCLM) for this trigger key
* (will contain the contract number) and set the estimated
* amount to zero. Set the Actual amount to the trigger-amount
* passed and re-write the CLMD record.
*
*
* Read the UTRN file to until all the records have been
* processed. At this point do a posting for the total amount
* of the claim by taking the values from the CLMD file.
* Please note that component level accounting is not possible
* as the CLMD does not hold plan suffix.
*
*
*****************************************************************
* </pre>
*/
public class Dthadd2 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "DTHADD2";
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String clmdaddrec = "CLMDADDREC";
	private static final String utrnrevrec = "UTRNREVREC";
	private static final String hitrclmrec = "HITRCLMREC";
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaActvalue = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaDeemCharge = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaGrossVal = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaGrossRound = new PackedDecimalData(17, 5).init(0);
	private String wsaaInitialUnit = "";
	private FixedLengthStringData wsaaFeedbackInd = new FixedLengthStringData(1);
	private PackedDecimalData wsaaAcumGrossVal = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(22);
	private ZonedDecimalData wsaaTransDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaTransTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 16).setUnsigned();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClmdaddTableDAM clmdaddIO = new ClmdaddTableDAM();
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrclmTableDAM hitrclmIO = new HitrclmTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private UtrnrevTableDAM utrnrevIO = new UtrnrevTableDAM();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Udtrigrec udtrigrec = new Udtrigrec();

	public Dthadd2() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		udtrigrec.udtrigrecRec = convertAndSetParam(udtrigrec.udtrigrecRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		readUtrn1000();
		readClaimHeader1200();
		readChdrlif1500();
		readTabT56451800();
		readTabT56881900();
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			a200ReadAndUpdateClaim();
		}
		else {
			readAndUpdateClaim2000();
		}
		summaryPosting2500();
		return ;
	}

	/**
	* <pre>
	*    PERFORM 3000-READ-ALL-CLAIM-DETS.                            
	* </pre>
	*/
protected void exit090()
	{
		exitProgram();
	}

protected void readUtrn1000()
	{
		go1010();
	}

protected void go1010()
	{
		/* Set up WSAA-TRANID                                              */
		wsaaTransDate.set(getCobolDate());
		wsaaTransTime.set(getCobolTime());
		wsaaUser.set(ZERO);
		utrnIO.setParams(SPACES);
		/* MOVE UTRN-LINKAGE           TO UTRN-DATA-KEY.                */
		utrnIO.setChdrcoy(udtrigrec.chdrcoy);
		utrnIO.setChdrnum(udtrigrec.chdrnum);
		utrnIO.setLife(udtrigrec.life);
		utrnIO.setCoverage(udtrigrec.coverage);
		utrnIO.setRider(udtrigrec.rider);
		utrnIO.setPlanSuffix(udtrigrec.planSuffix);
		utrnIO.setUnitVirtualFund(udtrigrec.unitVirtualFund);
		utrnIO.setUnitType(udtrigrec.unitType);
		utrnIO.setTranno(udtrigrec.tranno);
		/*    MOVE UDTG-PROC-SEQ-NO       TO UTRN-PROC-SEQ-NO              */
		utrnIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)
		&& isNE(utrnIO.getStatuz(), varcom.mrnf)) {
			/*      MOVE UTRN-STATUZ       TO WSBB-TRIGGER-STATUZ           */
			/*      MOVE UTRN-FORMAT       TO WSBB-TRIGGER-FORMAT           */
			/*      MOVE WSBB-TRIGGER      TO WSAA-TRIGGER                  */
			syserrrec.params.set(utrnIO.getParams());
			fatalError9000();
		}
		wsaaDeemCharge.set(ZERO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			a100ReadHitr();
			return ;
		}
	}

protected void readClaimHeader1200()
	{
		/*GO*/
		clmhclmIO.setDataArea(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO CLMHCLM-CHDRCOY.*/
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO CLMHCLM-CHDRNUM.*/
		clmhclmIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmhclmIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmhclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void readChdrlif1500()
	{
		/*GO*/
		chdrlifIO.setDataArea(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO CHDRLIF-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO CHDRLIF-CHDRNUM.              */
		chdrlifIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		chdrlifIO.setChdrnum(udtrigrec.tk3Chdrnum);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError9000();
		}
		/*EXIT1*/
	}

protected void readTabT56451800()
	{
		read1810();
	}

protected void read1810()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(clmhclmIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			/*     MOVE 'ITDMREC'          TO WSBB-TRIGGER-FORMAT      <006>*/
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaSubr);
		descIO.setDesccoy(clmhclmIO.getChdrcoy());
		/* MOVE 'E'                    TO DESC-LANGUAGE.        <LA3998>*/
		descIO.setLanguage(udtrigrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			/*     MOVE 'DESCREC'          TO WSBB-TRIGGER-FORMAT      <006>*/
			fatalError9000();
		}
	}

protected void readTabT56881900()
	{
		read1900();
	}

protected void read1900()
	{
		/* MOVE PARM-COMPANY           TO ITDM-ITEMCOY.            <011>*/
		itdmIO.setItemcoy(udtrigrec.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		/* MOVE PARM-EFFDATE           TO ITDM-ITMFRM.             <011>*/
		itdmIO.setItmfrm(udtrigrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		/* IF ITDM-ITEMCOY           NOT = PARM-COMPANY            <011>*/
		if (isNE(itdmIO.getItemcoy(), udtrigrec.company)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrlifIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readAndUpdateClaim2000()
	{
		go2010();
	}

protected void go2010()
	{
		clmdaddIO.setParams(SPACES);
		clmdaddIO.setTranno(ZERO);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO CLMDADD-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO CLMDADD-CHDRNUM.              */
		/* MOVE WSAA-TRIGGER-COVERAGE  TO CLMDADD-COVERAGE.             */
		/* MOVE WSAA-TRIGGER-RIDER     TO CLMDADD-RIDER.                */
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setCoverage(udtrigrec.tk3Coverage);
		clmdaddIO.setRider(udtrigrec.tk3Rider);
		clmdaddIO.setLife(utrnIO.getLife());
		clmdaddIO.setFieldType(utrnIO.getUnitType());
		clmdaddIO.setVirtualFund(utrnIO.getUnitVirtualFund());
		clmdaddIO.setFormat(clmdaddrec);
		/* MOVE BEGNH                  TO CLMDADD-FUNCTION.             */
		clmdaddIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)
		&& isNE(clmdaddIO.getStatuz(), varcom.endp)) {
			/*    MOVE CLMDADD-STATUZ        TO WSBB-TRIGGER-STATUZ         */
			/*    MOVE CLMDADD-FORMAT        TO WSBB-TRIGGER-FORMAT         */
			/*    MOVE WSBB-TRIGGER          TO WSAA-TRIGGER                */
			syserrrec.params.set(clmdaddIO.getStatuz());
			fatalError9000();
		}
		/*    IF WSAA-TRIGGER-CHDRCOY       NOT = CLMDADD-CHDRCOY          */
		/*       OR  WSAA-TRIGGER-CHDRNUM   NOT = CLMDADD-CHDRNUM          */
		/*       OR  WSAA-TRIGGER-COVERAGE  NOT = CLMDADD-COVERAGE         */
		/*       OR  WSAA-TRIGGER-RIDER     NOT = CLMDADD-RIDER            */
		if (isNE(udtrigrec.tk3Chdrcoy, clmdaddIO.getChdrcoy())
		|| isNE(udtrigrec.tk3Chdrnum, clmdaddIO.getChdrnum())
		|| isNE(udtrigrec.tk3Coverage, clmdaddIO.getCoverage())
		|| isNE(udtrigrec.tk3Rider, clmdaddIO.getRider())
		|| isNE(utrnIO.getLife(), clmdaddIO.getLife())
		|| isNE(utrnIO.getUnitType(), clmdaddIO.getFieldType())
		|| isNE(utrnIO.getUnitVirtualFund(), clmdaddIO.getVirtualFund())) {
			/*    MOVE CLMDADD-STATUZ        TO WSBB-TRIGGER-STATUZ         */
			/*    MOVE CLMDADD-FORMAT        TO WSBB-TRIGGER-FORMAT         */
			/*    MOVE WSBB-TRIGGER          TO WSAA-TRIGGER                */
			/*    MOVE CLMDADD-STATUZ        TO SYSR-PARAMS                 */
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setEstMatValue(ZERO);
		/*  MOVE UTRN-CONTRACT-AMOUNT     TO CLMDADD-ACTVALUE.           */
		/* ADD  UTRN-CONTRACT-AMOUNT     TO CLMDADD-ACTVALUE.      <005>*/
		wsaaDeemCharge.set(ZERO);
		wsaaGrossVal.set(ZERO);
		wsaaGrossRound.set(ZERO);
		/* Check UTRN-UNIT-TYPE.                                           */
		if (isEQ(utrnIO.getUnitType(), "I")
		&& isNE(utrnIO.getNofDunits(), utrnIO.getNofUnits())) {
			wsaaInitialUnit = "Y";
		}
		else {
			wsaaInitialUnit = " ";
		}
		/* Calculate PEND-CLAIM amount for 'A' units.                      */
		/* Round the amount to 2 decimal places.                           */
		if (isEQ(wsaaInitialUnit, " ")) {
			compute(wsaaGrossRound, 5).set(mult(utrnIO.getPriceUsed(), utrnIO.getNofDunits()));
			compute(wsaaGrossVal, 6).setRounded(mult(wsaaGrossRound, 1));
			zrdecplrec.amountIn.set(wsaaGrossVal);
			a000CallRounding();
			wsaaGrossVal.set(zrdecplrec.amountOut);
			wsaaAcumGrossVal.set(wsaaGrossVal);
		}
		/* Calculate deemed amount for 'I' units.                          */
		if (isEQ(wsaaInitialUnit, "Y")) {
			compute(wsaaGrossRound, 5).set(mult(utrnIO.getPriceUsed(), utrnIO.getNofDunits()));
			/* Round up to 2 decimal.                                          */
			if (isEQ(wsaaGrossRound, ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				compute(wsaaGrossVal, 6).setRounded(mult(wsaaGrossRound, 1));
			}
			if (isEQ(wsaaGrossVal, ZERO)) {
				/*NEXT_SENTENCE*/
			}
			else {
				compute(wsaaDeemCharge, 2).set(sub(wsaaGrossVal, utrnIO.getContractAmount()));
			}
		}
		/*    If initial units, than move deem amount to                   */
		/*    Claim detail file, else add utrn-cont-amt.                   */
		if (isEQ(wsaaInitialUnit, "Y")) {
			clmdaddIO.setActvalue(wsaaGrossVal);
		}
		else {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(add(clmdaddIO.getActvalue(), utrnIO.getContractAmount()));
		}
		if (isGT(clmdaddIO.getActvalue(), ZERO)) {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), 1));
		}
		else {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), -1));
		}
		/*MOVE ZERO                   TO WSAA-AMOUNT.             <015>*/
		/*MOVE UTRN-CONTRACT-AMOUNT   TO WSAA-AMOUNT.             <015>*/
		/*IF WSAA-AMOUNT > ZERO                                   <015>*/
		/*   ADD WSAA-AMOUNT          TO CLMDADD-ACTVALUE         <015>*/
		/*ELSE                                                    <015>*/
		/*   COMPUTE WSAA-AMOUNT      = WSAA-AMOUNT * -1          <015>*/
		/*   ADD WSAA-AMOUNT          TO CLMDADD-ACTVALUE         <015>*/
		/*END-IF.                                                 <015>*/
		/* IF CLMDADD-ACTVALUE > ZERO                                   */
		/*     COMPUTE CLMDADD-ACTVALUE  = CLMDADD-ACTVALUE * +1        */
		/* ELSE                                                         */
		/*     COMPUTE CLMDADD-ACTVALUE  = CLMDADD-ACTVALUE * -1.       */
		/* MOVE REWRT                    TO CLMDADD-FUNCTION.   <LA3993>*/
		clmdaddIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			/*     MOVE CLMDADD-STATUZ       TO WSBB-TRIGGER-STATUZ*/
			/*     MOVE CLMDADD-FORMAT       TO WSBB-TRIGGER-FORMAT*/
			/*     MOVE WSBB-TRIGGER         TO WSAA-TRIGGER*/
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
	}

protected void summaryPosting2500()
	{
		summaryPostingPara2500();
	}

protected void summaryPostingPara2500()
	{
		/* MOVE UTRN-LINKAGE           TO  WSAA-UTRN-KEY.               */
		utrnrevIO.setParams(SPACES);
		utrnrevIO.setChdrcoy(udtrigrec.chdrcoy);
		utrnrevIO.setChdrnum(udtrigrec.chdrnum);
		utrnrevIO.setPlanSuffix(udtrigrec.planSuffix);
		utrnrevIO.setTranno(udtrigrec.tranno);
		/* MOVE WSAA-UTRN-CHDRCOY      TO  UTRNREV-CHDRCOY.     <D60401>*/
		/* MOVE WSAA-UTRN-CHDRNUM      TO  UTRNREV-CHDRNUM.     <D60401>*/
		/* MOVE WSAA-UTRN-TRANNO       TO  UTRNREV-TRANNO.      <D60401>*/
		utrnrevIO.setFeedbackInd(SPACES);
		utrnrevIO.setFormat(utrnrevrec);
		utrnrevIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(), varcom.oK)) {
			/*     MOVE UTRNREV-STATUZ     TO WSBB-TRIGGER-STATUZ           */
			/*     MOVE UTRNREV-FORMAT     TO WSBB-TRIGGER-FORMAT           */
			/*     MOVE WSBB-TRIGGER       TO WSAA-TRIGGER                  */
			syserrrec.params.set(utrnrevIO.getParams());
			fatalError9000();
		}
		/* IF  UTRNREV-CHDRCOY       = WSAA-UTRN-CHDRCOY AND    <D60904>*/
		/*     UTRNREV-CHDRNUM       = WSAA-UTRN-CHDRNUM AND    <D60904>*/
		/*     UTRNREV-TRANNO        = WSAA-UTRN-TRANNO  AND    <D60904>*/
		if (isEQ(utrnrevIO.getChdrcoy(), udtrigrec.chdrcoy)
		&& isEQ(utrnrevIO.getChdrnum(), udtrigrec.chdrnum)
		&& isEQ(utrnrevIO.getTranno(), udtrigrec.tranno)
		&& isEQ(utrnrevIO.getFeedbackInd(), SPACES)) {
			return ;
		}
		else {
			readAllClaimDets3000();
		}
	}

protected void readAllClaimDets3000()
	{
		error3010();
	}

protected void error3010()
	{
		clmdaddIO.setParams(SPACES);
		/* MOVE WSAA-TRIGGER-CHDRCOY   TO CLMDADD-CHDRCOY.              */
		/* MOVE WSAA-TRIGGER-CHDRNUM   TO CLMDADD-CHDRNUM.              */
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)
		&& isNE(clmdaddIO.getStatuz(), varcom.endp)) {
			/*    MOVE CLMDADD-STATUZ      TO WSBB-TRIGGER-STATUZ           */
			/*    MOVE CLMDADD-FORMAT      TO WSBB-TRIGGER-FORMAT           */
			/*    MOVE WSBB-TRIGGER        TO WSAA-TRIGGER                  */
			/*    MOVE CLMDADD-STATUZ      TO SYSR-PARAMS                   */
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = CLMDADD-CHDRCOY OR         */
		/*    WSAA-TRIGGER-CHDRNUM     NOT =  CLMDADD-CHDRNUM OR        */
		if (isNE(udtrigrec.tk3Chdrcoy, clmdaddIO.getChdrcoy())
		|| isNE(udtrigrec.tk3Chdrnum, clmdaddIO.getChdrnum())
		|| isEQ(clmdaddIO.getStatuz(), varcom.endp)) {
			/*    MOVE CLMDADD-STATUZ      TO WSBB-TRIGGER-STATUZ*/
			/*    MOVE CLMDADD-FORMAT      TO WSBB-TRIGGER-FORMAT*/
			/*    MOVE WSBB-TRIGGER        TO WSAA-TRIGGER*/
			syserrrec.params.set(clmdaddIO.getStatuz());
			fatalError9000();
		}
		clmdaddIO.setFunction(varcom.begn);
		while ( !(isEQ(clmdaddIO.getStatuz(), varcom.endp))) {
			readAllClaims4000();
		}
		
	}

protected void readAllClaims4000()
	{
		/*ERROR*/
		/* IF CLMDADD-ACTVALUE = ZERO                              <006>*/
		/*     MOVE ENDP TO CLMDADD-STATUZ                         <006>*/
		/*     GO TO 4090-EXIT.                                    <006>*/
		/*ADD CLMDADD-ACTVALUE        TO WSAA-ACTVALUE.           <006>*/
		/*MOVE CLMDADD-ACTVALUE       TO WSAA-ACTVALUE.           <013>*/
		clmdaddIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)
		&& isNE(clmdaddIO.getStatuz(), varcom.endp)) {
			/*    MOVE CLMDADD-STATUZ      TO WSBB-TRIGGER-STATUZ      <006>*/
			/*    MOVE CLMDADD-FORMAT      TO WSBB-TRIGGER-FORMAT      <006>*/
			/*    MOVE WSBB-TRIGGER        TO WSAA-TRIGGER             <006>*/
			syserrrec.params.set(clmdaddIO.getStatuz());
			fatalError9000();
		}
		/* IF WSAA-TRIGGER-CHDRCOY     NOT = CLMDADD-CHDRCOY OR    <006>*/
		/*    WSAA-TRIGGER-CHDRNUM     NOT =  CLMDADD-CHDRNUM OR   <006>*/
		if (isNE(udtrigrec.tk3Chdrcoy, clmdaddIO.getChdrcoy())
		|| isNE(udtrigrec.tk3Chdrnum, clmdaddIO.getChdrnum())
		|| isEQ(clmdaddIO.getStatuz(), varcom.endp)) {
			clmdaddIO.setStatuz(varcom.endp);
			postings7100();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	***  THIS SECTION IS NO LONGER USED. SEE NOTES ABOVE REGARDING    
	***  COMPONANT LEVEL ACCOUNTING                                   
	*6000-POSTINGS    SECTION.                                   <006>
	*6010-GO.                                                    <006>
	*****                                                        <006>
	*****MOVE 'AUTO'                 TO BATD-FUNCTION.           <007>
	*****MOVE PARM-TRANID            TO BATD-TRANID.             <007>
	*****MOVE UTRN-BATCCOY           TO LIFA-BATCCOY.            <007>
	*****MOVE UTRN-BATCBRN           TO LIFA-BATCBRN.            <007>
	*****MOVE UTRN-BATCACTYR         TO LIFA-BATCACTYR.          <007>
	*****MOVE UTRN-BATCACTMN         TO LIFA-BATCACTMN.          <007>
	*****MOVE UTRN-BATCTRCDE         TO LIFA-BATCTRCDE.          <007>
	*****MOVE UTRN-BATCBATCH         TO LIFA-BATCBATCH.          <007>
	*****MOVE LIFA-BATCKEY           TO BATD-BATCHKEY.           <007>
	*****MOVE 'BA'                   TO BATD-PREFIX.             <007>
	*****                                                        <007>
	*****CALL 'BATCDOR' USING BATD-BATCDOR-REC.                  <007>
	*****IF BATD-STATUZ              NOT = O-K                   <007>
	*****    MOVE BATD-BATCDOR-REC   TO SYSR-PARAMS              <007>
	*****    PERFORM 9000-FATAL-ERROR.                           <007>
	*****                                                        <008>
	*****MOVE BATD-BATCHKEY          TO LIFA-BATCKEY.            <008>
	*****                                                        <008>
	*    MOVE 1                          TO WSAA-SUB.                 
	*****MOVE ZERO                       TO LIFA-RCAMT           <006>
	*****                                   LIFA-CONTOT          <006>
	*****                                   LIFA-RCAMT           <006>
	*****                                   LIFA-FRCDATE.        <006>
	*****                                                        <006>
	*****MOVE 'PSTW'                     TO LIFA-FUNCTION.       <006>
	***  MOVE PARM-COMPANY           TO LIFA-BATCCOY.                 
	***  MOVE PARM-BRANCH            TO LIFA-BATCBRN.                 
	***  MOVE PARM-ACCTYEAR          TO LIFA-BATCACTYR.               
	***  MOVE PARM-ACCTMONTH         TO LIFA-BATCACTMN.               
	***  MOVE 'T668'                 TO LIFA-BATCTRCDE.               
	***  MOVE SPACE                  TO LIFA-BATCBATCH.               
	*****MOVE CLMHCLM-CHDRNUM            TO LIFA-RDOCNUM.        <006>
	*****MOVE CLMHCLM-TRANNO             TO LIFA-TRANNO.         <006>
	*    MOVE T5645-SACSCODE(WSAA-SUB)   TO LIFA-SACSCODE.            
	*    MOVE T5645-SACSTYPE(WSAA-SUB)   TO LIFA-SACSTYP.             
	*    MOVE T5645-GLMAP(WSAA-SUB)      TO LIFA-GLCODE.              
	*    MOVE T5645-SIGN(WSAA-SUB)       TO LIFA-GLSIGN.              
	*    MOVE T5645-CNTTOT(WSAA-SUB)     TO LIFA-CONTOT.              
	*****IF T5688-COMLVLACC = 'Y'                                     
	****    MOVE T5645-SACSCODE-01          TO LIFA-SACSCODE     <011>
	****    MOVE T5645-SACSTYPE-01          TO LIFA-SACSTYP      <011>
	****    MOVE T5645-GLMAP-01             TO LIFA-GLCODE       <011>
	****    MOVE T5645-SIGN-01              TO LIFA-GLSIGN       <011>
	****    MOVE T5645-CNTTOT-01            TO LIFA-CONTOT       <011>
	*****   MOVE T5645-SACSCODE-03          TO LIFA-SACSCODE     <012>
	*****   MOVE T5645-SACSTYPE-03          TO LIFA-SACSTYP      <012>
	*****   MOVE T5645-GLMAP-03             TO LIFA-GLCODE       <012>
	*****   MOVE T5645-SIGN-03              TO LIFA-GLSIGN       <012>
	*****   MOVE T5645-CNTTOT-03            TO LIFA-CONTOT       <012>
	*****   MOVE UTRN-CHDRNUM           TO WSAA-RLDG-CHDRNUM     <011>
	*****   MOVE UTRN-LIFE              TO WSAA-RLDG-LIFE        <011>
	*****   MOVE UTRN-COVERAGE          TO WSAA-RLDG-COVERAGE    <011>
	*****   MOVE UTRN-RIDER             TO WSAA-RLDG-RIDER       <011>
	*****   MOVE UTRN-PLAN-SUFFIX       TO WSAA-PLANSUFF         <011>
	*****   MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX <011>
	*****   MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT         <011>
	*****   MOVE UTRN-CRTABLE            TO LIFA-SUBSTITUTE-CODE(06)  
	*****ELSE                                                    <011>
	****    MOVE T5645-SACSCODE-03          TO LIFA-SACSCODE     <011>
	****    MOVE T5645-SACSTYPE-03          TO LIFA-SACSTYP      <011>
	****    MOVE T5645-GLMAP-03             TO LIFA-GLCODE       <011>
	****    MOVE T5645-SIGN-03              TO LIFA-GLSIGN       <011>
	****    MOVE T5645-CNTTOT-03            TO LIFA-CONTOT       <011>
	*****   MOVE T5645-SACSCODE-01          TO LIFA-SACSCODE     <012>
	*****   MOVE T5645-SACSTYPE-01          TO LIFA-SACSTYP      <012>
	*****   MOVE T5645-GLMAP-01             TO LIFA-GLCODE       <012>
	*****   MOVE T5645-SIGN-01              TO LIFA-GLSIGN       <012>
	*****   MOVE T5645-CNTTOT-01            TO LIFA-CONTOT       <012>
	*****   MOVE SPACES                  TO LIFA-SUBSTITUTE-CODE(06)  
	*****   MOVE SPACES                     TO LIFA-RLDGACCT     <011>
	*****   MOVE CLMHCLM-CHDRNUM            TO LIFA-RLDGACCT.    <011>
	*****MOVE ZERO                       TO LIFA-JRNSEQ.         <006>
	*****MOVE CLMHCLM-CHDRCOY            TO LIFA-RLDGCOY         <006>
	*****                                   LIFA-GENLCOY.        <006>
	*****MOVE CLMHCLM-CHDRNUM            TO LIFA-RLDGACCT.            
	*    MOVE UTRN-CHDRNUM           TO WSAA-RLDG-CHDRNUM.            
	*    MOVE UTRN-LIFE              TO WSAA-RLDG-LIFE.               
	*    MOVE UTRN-COVERAGE          TO WSAA-RLDG-COVERAGE.           
	*    MOVE UTRN-RIDER             TO WSAA-RLDG-RIDER.              
	*    MOVE UTRN-PLAN-SUFFIX       TO WSAA-PLANSUFF.                
	*    MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX.        
	*    MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT.                
	*****MOVE CLMHCLM-CURRCD             TO LIFA-ORIGCURR.       <006>
	*****MOVE WSAA-ACTVALUE              TO LIFA-ORIGAMT.        <006>
	*****MOVE SPACES                     TO LIFA-GENLCUR.        <006>
	*****MOVE ZERO                       TO LIFA-ACCTAMT         <006>
	*****                                   LIFA-CRATE.          <006>
	*****MOVE SPACES                     TO LIFA-POSTYEAR,       <006>
	*****                                   LIFA-POSTMONTH.      <006>
	*****MOVE CLMHCLM-TRANNO             TO LIFA-TRANREF.        <006>
	*****MOVE DESC-LONGDESC              TO LIFA-TRANDESC.       <006>
	*****MOVE CLMHCLM-EFFDATE            TO LIFA-EFFDATE.        <006>
	*****MOVE VRCM-MAX-DATE              TO LIFA-FRCDATE.        <006>
	*****MOVE CHDRLIF-CNTTYPE            TO LIFA-SUBSTITUTE-CODE(01). 
	*    MOVE UTRN-CRTABLE               TO LIFA-SUBSTITUTE-CODE(06). 
	*****MOVE PARM-TRANID                TO VRCM-TRANID.         <006>
	*****MOVE VRCM-TERMID                TO LIFA-TERMID.         <006>
	*****MOVE VRCM-USER                  TO LIFA-USER.           <006>
	*****MOVE VRCM-TIME                  TO LIFA-TRANSACTION-TIME.006>
	*****MOVE VRCM-DATE                  TO LIFA-TRANSACTION-DATE.006>
	*****CALL 'LIFACMV'           USING LIFA-LIFACMV-REC.        <006>
	*****                                                        <006>
	*****IF LIFA-STATUZ       NOT = O-K                          <006>
	*****     MOVE LIFA-LIFACMV-REC    TO SYSR-PARAMS            <006>
	*****     MOVE 'LIFACMVREC'        TO WSBB-TRIGGER-FORMAT    <006>
	*****     PERFORM 9000-FATAL-ERROR.                          <006>
	* Write an offset ACMV.                                           
	*    ADD 1                       TO WSAA-SUB.                     
	*****MOVE SPACES                     TO LIFA-SUBSTITUTE-CODE(06). 
	*    MOVE T5645-SACSCODE(WSAA-SUB)   TO LIFA-SACSCODE.            
	*    MOVE T5645-SACSTYPE(WSAA-SUB)   TO LIFA-SACSTYP.             
	*    MOVE T5645-GLMAP(WSAA-SUB)      TO LIFA-GLCODE.              
	*    MOVE T5645-SIGN(WSAA-SUB)       TO LIFA-GLSIGN.              
	*    MOVE T5645-CNTTOT(WSAA-SUB)     TO LIFA-CONTOT.              
	*****MOVE T5645-SACSCODE-02          TO LIFA-SACSCODE.       <011>
	*****MOVE T5645-SACSTYPE-02          TO LIFA-SACSTYP.        <011>
	*****MOVE T5645-GLMAP-02             TO LIFA-GLCODE.         <011>
	*****MOVE T5645-SIGN-02              TO LIFA-GLSIGN.         <011>
	*****MOVE T5645-CNTTOT-02            TO LIFA-CONTOT.         <011>
	*****MOVE 'PSTW'                     TO LIFA-FUNCTION.       <011>
	* Second posting at contract level.                               
	*****MOVE SPACES                     TO LIFA-RLDGACCT.       <009>
	*****IF T5688-COMLVLACC              = 'Y'                   <009>
	*****   MOVE T5645-SACSCODE-04       TO LIFA-SACSCODE        <014>
	*****   MOVE T5645-SACSTYPE-04       TO LIFA-SACSTYP         <014>
	*****   MOVE T5645-GLMAP-04          TO LIFA-GLCODE          <014>
	*****   MOVE T5645-SIGN-04           TO LIFA-GLSIGN          <014>
	*****   MOVE T5645-CNTTOT-04         TO LIFA-CONTOT          <014>
	*****   MOVE UTRN-CHDRNUM           TO WSAA-RLDG-CHDRNUM     <014>
	*****   MOVE UTRN-LIFE              TO WSAA-RLDG-LIFE        <014>
	*****   MOVE UTRN-COVERAGE          TO WSAA-RLDG-COVERAGE    <014>
	*****   MOVE UTRN-RIDER             TO WSAA-RLDG-RIDER       <014>
	*****   MOVE UTRN-PLAN-SUFFIX       TO WSAA-PLANSUFF         <014>
	*****   MOVE WSAA-PLANSUFF          TO WSAA-RLDG-PLAN-SUFFIX <014>
	*****   MOVE WSAA-RLDGACCT          TO LIFA-RLDGACCT         <014>
	*****   MOVE UTRN-CRTABLE            TO LIFA-SUBSTITUTE-CODE(06)  
	*****ELSE                                                    <014>
	*****   MOVE T5645-SACSCODE-02       TO LIFA-SACSCODE        <014>
	*****   MOVE T5645-SACSTYPE-02       TO LIFA-SACSTYP         <014>
	*****   MOVE T5645-GLMAP-02          TO LIFA-GLCODE          <014>
	*****   MOVE T5645-SIGN-02           TO LIFA-GLSIGN          <014>
	*****   MOVE T5645-CNTTOT-02         TO LIFA-CONTOT          <014>
	*****   MOVE CLMHCLM-CHDRNUM         TO LIFA-RLDGACCT        <014>
	*****END-IF.                                                 <014>
	*****MOVE CLMHCLM-CHDRNUM            TO LIFA-RLDGACCT.       <009>
	*****CALL 'LIFACMV'           USING LIFA-LIFACMV-REC.        <006>
	*****IF LIFA-STATUZ       NOT = O-K                          <006>
	*****     MOVE LIFA-LIFACMV-REC    TO SYSR-PARAMS            <006>
	*****     MOVE 'LIFACMVREC'        TO WSBB-TRIGGER-FORMAT    <006>
	*****     PERFORM 9000-FATAL-ERROR.                          <006>
	*6090-EXIT.                                                  <006>
	*    EXIT.                                                        
	* </pre>
	*/
protected void postings7100()
	{
		go7110();
	}

protected void go7110()
	{
		batcdorrec.function.set("AUTO");
		/*    MOVE PARM-TRANID            TO BATD-TRANID.             <015>*/
		batcdorrec.tranid.set(wsaaTranid);
		/*   MOVE UTRN-BATCCOY           TO LIFA-BATCCOY.         <LA4524>*/
		/*   MOVE UTRN-BATCBRN           TO LIFA-BATCBRN.         <LA4524>*/
		/*   MOVE UTRN-BATCACTYR         TO LIFA-BATCACTYR.       <LA4524>*/
		/*   MOVE UTRN-BATCACTMN         TO LIFA-BATCACTMN.       <LA4524>*/
		lifacmvrec1.batckey.set(udtrigrec.batchkey);
		lifacmvrec1.batctrcde.set(utrnIO.getBatctrcde());
		/*   MOVE UTRN-BATCBATCH         TO LIFA-BATCBATCH.       <LA4524>*/
		batcdorrec.batchkey.set(lifacmvrec1.batckey);
		batcdorrec.prefix.set("BA");
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			syserrrec.subrname.set(wsaaSubr);
			getAppVars().addDiagnostic("ERROR IN BATCDOR SUBROUTINE");
			syserrrec.params.set(batcdorrec.batcdorRec);
			fatalError9000();
		}
		lifacmvrec1.batckey.set(batcdorrec.batchkey);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(ZERO);
		/*MOVE 'PSTW'                     TO LIFA-FUNCTION.       <015>*/
		/*MOVE CLMHCLM-CHDRNUM            TO LIFA-RDOCNUM.        <015>*/
		/*MOVE CLMHCLM-TRANNO             TO LIFA-TRANNO.         <015>*/
		/*MOVE T5645-SACSCODE-01          TO LIFA-SACSCODE.       <015>*/
		/*MOVE T5645-SACSTYPE-01          TO LIFA-SACSTYP.        <015>*/
		/*MOVE T5645-GLMAP-01             TO LIFA-GLCODE.         <015>*/
		/*MOVE T5645-SIGN-01              TO LIFA-GLSIGN.         <015>*/
		/*MOVE T5645-CNTTOT-01            TO LIFA-CONTOT.         <015>*/
		/*MOVE SPACES                  TO LIFA-SUBSTITUTE-CODE(06)<015>*/
		/*MOVE SPACES                     TO LIFA-RLDGACCT.       <015>*/
		/*MOVE CLMHCLM-CHDRNUM            TO LIFA-RLDGACCT.       <015>*/
		/*MOVE ZERO                       TO LIFA-JRNSEQ.         <015>*/
		/*MOVE CLMHCLM-CHDRCOY            TO LIFA-RLDGCOY         <015>*/
		/*                                   LIFA-GENLCOY.        <015>*/
		/*MOVE CLMHCLM-CURRCD             TO LIFA-ORIGCURR.       <015>*/
		/*MOVE UTRN-CONTRACT-AMOUNT       TO LIFA-ORIGAMT.        <015>*/
		/*MOVE SPACES                     TO LIFA-GENLCUR.        <015>*/
		/*MOVE ZERO                       TO LIFA-ACCTAMT         <015>*/
		/*                                   LIFA-CRATE.          <015>*/
		/*MOVE SPACES                     TO LIFA-POSTYEAR,       <015>*/
		/*                                   LIFA-POSTMONTH.      <015>*/
		/*MOVE CLMHCLM-TRANNO             TO LIFA-TRANREF.        <015>*/
		/*MOVE DESC-LONGDESC              TO LIFA-TRANDESC.       <015>*/
		/*MOVE CLMHCLM-EFFDATE            TO LIFA-EFFDATE.        <015>*/
		/*MOVE VRCM-MAX-DATE              TO LIFA-FRCDATE.        <015>*/
		/*MOVE CHDRLIF-CNTTYPE            TO LIFA-SUBSTITUTE-CODE(01). */
		/*MOVE PARM-TRANID                TO VRCM-TRANID.         <015>*/
		/*MOVE VRCM-TERMID                TO LIFA-TERMID.         <015>*/
		/*MOVE VRCM-USER                  TO LIFA-USER.           <015>*/
		/*MOVE VRCM-TIME                  TO LIFA-TRANSACTION-TIME.    */
		/*MOVE VRCM-DATE                  TO LIFA-TRANSACTION-DATE.    */
		/*                                                            <015>*/
		/*CALL 'LIFACMV'           USING LIFA-LIFACMV-REC.        <015>*/
		/*                                                        <015>*/
		/*IF LIFA-STATUZ       NOT = O-K                          <015>*/
		/*     MOVE LIFA-LIFACMV-REC    TO SYSR-PARAMS            <015>*/
		/*     MOVE 'LIFACMVREC'        TO WSBB-TRIGGER-FORMAT    <015>*/
		/*     PERFORM 9000-FATAL-ERROR.                          <015>*/
		/*                                                            <015>*/
		/* Write an offset ACMV.                                           */
		/*    ADD 1                       TO WSAA-SUB.                     */
		lifacmvrec1.substituteCode[6].set(SPACES);
		lifacmvrec1.function.set("PSTW");
		/* Posting at contract level.  Total of accumulation and initial   */
		/* units paid to the client.                                       */
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rdocnum.set(clmhclmIO.getChdrnum());
		lifacmvrec1.tranno.set(clmhclmIO.getTranno());
		lifacmvrec1.sacscode.set(t5645rec.sacscode02);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec1.glcode.set(t5645rec.glmap02);
		lifacmvrec1.glsign.set(t5645rec.sign02);
		lifacmvrec1.contot.set(t5645rec.cnttot02);
		lifacmvrec1.rldgacct.set(clmhclmIO.getChdrnum());
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.rldgcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec1.genlcoy.set(clmhclmIO.getChdrcoy());
		lifacmvrec1.origcurr.set(clmhclmIO.getCurrcd());
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.tranref.set(clmhclmIO.getTranno());
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.effdate.set(clmhclmIO.getEffdate());
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.substituteCode[1].set(chdrlifIO.getCnttype());
		/* MOVE PARM-TRANID                TO VRCM-TRANID.      <D60401>*/
		/* MOVE VRCM-TERMID                TO LIFA-TERMID.      <D60401>*/
		/* MOVE VRCM-USER                  TO LIFA-USER.        <D60401>*/
		lifacmvrec1.user.set(wsaaUser);
		/* MOVE VRCM-TIME                  TO LIFA-TRANSACTION-TIME.<D60*/
		/* MOVE VRCM-DATE                  TO LIFA-TRANSACTION-DATE.<D60*/
		lifacmvrec1.transactionTime.set(wsaaTransTime);
		lifacmvrec1.transactionDate.set(wsaaTransDate);
		wsaaGrossVal.add(wsaaAcumGrossVal);
		lifacmvrec1.origamt.set(wsaaGrossVal);
		/* CALL 'LIFACMV'           USING LIFA-LIFACMV-REC.        <015>*/
		/*                                                            <015>*/
		/* IF LIFA-STATUZ       NOT = O-K                          <015>*/
		/*      MOVE LIFA-LIFACMV-REC    TO SYSR-PARAMS            <015>*/
		/*      MOVE 'LIFACMVREC'        TO WSBB-TRIGGER-FORMAT  <D60401*/
		/*      MOVE WSAA-SUBR           TO SYSR-SUBRNAME       <D60401>*/
		/*      DISPLAY 'ERROR IN LIFACMV SUBROUTINE'           <D60401>*/
		/*      PERFORM 9000-FATAL-ERROR.                          <015>*/
		if (isEQ(wsaaDeemCharge, ZERO)) {
			return ;
		}
		lifacmvrec1.function.set("PSTW");
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			wsaaRldgChdrnum.set(clmhclmIO.getChdrnum());
			/*    MOVE WSAA-TRIGGER-LIFE      TO WSAA-RLDG-LIFE     <D60401>*/
			/*    MOVE WSAA-TRIGGER-COVERAGE  TO WSAA-RLDG-COVERAGE <D60401>*/
			/*    MOVE WSAA-TRIGGER-RIDER     TO WSAA-RLDG-RIDER    <D60401>*/
			wsaaRldgLife.set(udtrigrec.tk3Life);
			wsaaRldgCoverage.set(udtrigrec.tk3Coverage);
			wsaaRldgRider.set(udtrigrec.tk3Rider);
			wsaaPlan.set(utrnIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
			lifacmvrec1.substituteCode[6].set(SPACES);
			lifacmvrec1.substituteCode[1].set(utrnIO.getCoverage());
			lifacmvrec1.substituteCode[2].set(utrnIO.getUnitVirtualFund());
			lifacmvrec1.sacscode.set(t5645rec.sacscode05);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec1.glcode.set(t5645rec.glmap05);
			lifacmvrec1.glsign.set(t5645rec.sign05);
			lifacmvrec1.contot.set(t5645rec.cnttot05);
		}
		else {
			wsaaAcumGrossVal.set(ZERO);
			lifacmvrec1.sacscode.set(t5645rec.sacscode06);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec1.glcode.set(t5645rec.glmap06);
			lifacmvrec1.glsign.set(t5645rec.sign06);
			lifacmvrec1.contot.set(t5645rec.cnttot06);
			lifacmvrec1.substituteCode[1].set(utrnIO.getCoverage());
			lifacmvrec1.substituteCode[2].set(utrnIO.getUnitVirtualFund());
			lifacmvrec1.substituteCode[6].set(SPACES);
			lifacmvrec1.rldgacct.set(SPACES);
			lifacmvrec1.rldgacct.set(clmhclmIO.getChdrnum());
			lifacmvrec1.origamt.set(wsaaDeemCharge);
			callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
			if (isNE(lifacmvrec1.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec1.lifacmvRec);
				/*      MOVE 'LIFACMVREC'     TO WSBB-TRIGGER-FORMAT    <D60401>*/
				syserrrec.subrname.set(wsaaSubr);
				fatalError9000();
			}
		}
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(udtrigrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(clmhclmIO.getCurrcd());
		zrdecplrec.batctrcde.set(udtrigrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError9000();
		}
		/*A090-EXIT*/
	}

protected void a100ReadHitr()
	{
		a110Hitr();
	}

protected void a110Hitr()
	{
		hitrclmIO.setParams(SPACES);
		hitrclmIO.setChdrcoy(udtrigrec.chdrcoy);
		hitrclmIO.setChdrnum(udtrigrec.chdrnum);
		hitrclmIO.setLife(udtrigrec.life);
		hitrclmIO.setCoverage(udtrigrec.coverage);
		hitrclmIO.setRider(udtrigrec.rider);
		hitrclmIO.setPlanSuffix(udtrigrec.planSuffix);
		hitrclmIO.setZintbfnd(udtrigrec.unitVirtualFund);
		hitrclmIO.setTranno(udtrigrec.tranno);
		hitrclmIO.setFormat(hitrclmrec);
		hitrclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrclmIO);
		if (isNE(hitrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hitrclmIO.getParams());
			fatalError9000();
		}
	}

protected void a200ReadAndUpdateClaim()
	{
		a210Go();
	}

protected void a210Go()
	{
		clmdaddIO.setParams(SPACES);
		clmdaddIO.setChdrcoy(udtrigrec.tk3Chdrcoy);
		clmdaddIO.setChdrnum(udtrigrec.tk3Chdrnum);
		clmdaddIO.setLife(udtrigrec.tk3Life);
		clmdaddIO.setCoverage(udtrigrec.tk3Coverage);
		clmdaddIO.setRider(udtrigrec.tk3Rider);
		clmdaddIO.setFieldType("D");
		clmdaddIO.setVirtualFund(hitrclmIO.getZintbfnd());
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
		clmdaddIO.setFormat(clmdaddrec);
		clmdaddIO.setEstMatValue(ZERO);
		setPrecision(clmdaddIO.getActvalue(), 2);
		clmdaddIO.setActvalue(add(clmdaddIO.getActvalue(), hitrclmIO.getContractAmount()));
		if (isGT(clmdaddIO.getActvalue(), ZERO)) {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), 1));
		}
		else {
			setPrecision(clmdaddIO.getActvalue(), 2);
			clmdaddIO.setActvalue(mult(clmdaddIO.getActvalue(), -1));
		}
		clmdaddIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, clmdaddIO);
		if (isNE(clmdaddIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdaddIO.getParams());
			fatalError9000();
		}
	}

protected void fatalError9000()
	{
		error9010();
		exit9020();
	}

protected void error9010()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		/*    MOVE BOMB                   TO PARM-STATUZ.          <D60401>*/
		udtrigrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
