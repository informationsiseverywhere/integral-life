package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: SurdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:30
 * Class transformed from SURDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class SurdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 250;
	public FixedLengthStringData surdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData surdpfRecord = surdrec;
	
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(surdrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(surdrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(surdrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(surdrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(surdrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(surdrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(surdrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(surdrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(surdrec);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(surdrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(surdrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(surdrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(surdrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(surdrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(surdrec);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(surdrec);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(surdrec);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(surdrec);
	public PackedDecimalData actvalue = DD.actvalue.copy().isAPartOf(surdrec);
	public PackedDecimalData estMatValue = DD.emv.copy().isAPartOf(surdrec);
	public FixedLengthStringData virtualFund = DD.vrtfund.copy().isAPartOf(surdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(surdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(surdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(surdrec);
	public PackedDecimalData otheradjst =  DD.otheradjst.copy().isAPartOf(surdrec); 

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public SurdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for SurdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public SurdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for SurdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public SurdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for SurdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public SurdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("SURDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRNUM, " +
							"CHDRCOY, " +
							"PLNSFX, " +
							"TRANNO, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"EFFDATE, " +
							"CURRCD, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"SHORTDS, " +
							"LIENCD, " +
							"TYPE_T, " +
							"ACTVALUE, " +
							"EMV, " +
							"VRTFUND, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"OTHERADJST, "+
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrnum,
                                     chdrcoy,
                                     planSuffix,
                                     tranno,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     effdate,
                                     currcd,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     crtable,
                                     shortds,
                                     liencd,
                                     fieldType,
                                     actvalue,
                                     estMatValue,
                                     virtualFund,
                                     userProfile,
                                     jobName,
                                     datime,
                                     otheradjst,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrnum.clear();
  		chdrcoy.clear();
  		planSuffix.clear();
  		tranno.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		effdate.clear();
  		currcd.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		shortds.clear();
  		liencd.clear();
  		fieldType.clear();
  		actvalue.clear();
  		estMatValue.clear();
  		virtualFund.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		otheradjst.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getSurdrec() {
  		return surdrec;
	}

	public FixedLengthStringData getSurdpfRecord() {
  		return surdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setSurdrec(what);
	}

	public void setSurdrec(Object what) {
  		this.surdrec.set(what);
	}

	public void setSurdpfRecord(Object what) {
  		this.surdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(surdrec.getLength());
		result.set(surdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}