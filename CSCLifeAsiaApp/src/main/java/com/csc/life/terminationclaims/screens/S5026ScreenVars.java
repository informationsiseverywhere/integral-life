package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5026
 * @version 1.0 generated on 30/08/09 06:31
 * @author Quipoz
 */
public class S5026ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public ZonedDecimalData clamant = DD.clamant.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,17);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,20);
	public ZonedDecimalData estimateTotalValue = DD.estimtotal.copyToZonedDecimal().isAPartOf(dataFields,28);
	public ZonedDecimalData netOfSvDebt = DD.netdebt.copyToZonedDecimal().isAPartOf(dataFields,45);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,62);
	public ZonedDecimalData policyloan = DD.policyloan.copyToZonedDecimal().isAPartOf(dataFields,79);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,100);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,150);
	public ZonedDecimalData tdbtamt = DD.tdbtamt.copyToZonedDecimal().isAPartOf(dataFields,167);
	public ZonedDecimalData zrcshamt = DD.zrcshamt.copyToZonedDecimal().isAPartOf(dataFields,184);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,197);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,205);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,213);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,216);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,224);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,254);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,284);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,292);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,339);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,347);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,394);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,402);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,449);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,453);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,463);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,471);
	public FixedLengthStringData reserveUnitsInd = DD.rsunin.copy().isAPartOf(dataFields,481);//ILIFE-5452
	public ZonedDecimalData reserveUnitsDate = DD.rundte.copyToZonedDecimal().isAPartOf(dataFields,482);//ILIFE-5452
	public ZonedDecimalData sacscurbal = DD.sacscurbal.copyToZonedDecimal().isAPartOf(dataFields,490);
	public ZonedDecimalData sacscurbal1 = DD.sacscurbal1.copyToZonedDecimal().isAPartOf(dataFields,507);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,524);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,532);
	public FixedLengthStringData reqntype = DD.reqntype.copy().isAPartOf(dataFields,579);
	public FixedLengthStringData bankacckey = DD.bankacckey1.copy().isAPartOf(dataFields,580);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,600);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,630);
	public FixedLengthStringData crdtcrd = DD.crdtcrd.copy().isAPartOf(dataFields,640);
	public ZonedDecimalData unexpiredprm = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,660);
	public ZonedDecimalData suspenseamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,677);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());	
	public FixedLengthStringData clamantErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData estimtotalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData netdebtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData policyloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData tdbtamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData zrcshamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData rsuninErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);//ILIFE-5452
	public FixedLengthStringData rundteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);//ILIFE-5452
	public FixedLengthStringData sacscurbalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData sacscurbal1Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData reqntypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData crdtcrdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData unexpiredprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData suspenseamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] clamantOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] estimtotalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] netdebtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] policyloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] tdbtamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] zrcshamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] rsuninOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);//ILIFE-5452
	public FixedLengthStringData[] rundteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);//ILIFE-5452
	public FixedLengthStringData[] sacscurbalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] sacscurbal1Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] reqntypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] crdtcrdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] unexpiredprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] suspenseamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(395);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(121).isAPartOf(subfileArea, 0);
	public ZonedDecimalData actvalue = DD.actvalue.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData estMatValue = DD.emv.copyToZonedDecimal().isAPartOf(subfileFields,22);
	public ZonedDecimalData hactval = DD.hactval.copyToZonedDecimal().isAPartOf(subfileFields,39);
	public FixedLengthStringData hcnstcur = DD.hcnstcur.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData hcover = DD.hcover.copy().isAPartOf(subfileFields,59);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,61);
	public ZonedDecimalData hemv = DD.hemv.copyToZonedDecimal().isAPartOf(subfileFields,65);
	public FixedLengthStringData hjlife = DD.hjlife.copy().isAPartOf(subfileFields,82);
	public FixedLengthStringData htype = DD.htype.copy().isAPartOf(subfileFields,84);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,85);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,87);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(subfileFields,89);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(subfileFields,99);
	public FixedLengthStringData fund = DD.virtfund.copy().isAPartOf(subfileFields,100);
	public ZonedDecimalData commclaw = DD.commclaw.copyToZonedDecimal().isAPartOf(subfileFields,104);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(68).isAPartOf(subfileArea, 121);
	public FixedLengthStringData actvalueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData emvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hactvalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hcnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hcoverErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData hemvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData hjlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData htypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData shortdsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData typeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData virtfundErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData commclawErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 64);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(204).isAPartOf(subfileArea, 189);
	public FixedLengthStringData[] actvalueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] emvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hactvalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hcnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hcoverOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] hemvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] hjlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] htypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] typeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public FixedLengthStringData[] virtfundOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	public FixedLengthStringData[] commclawOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 192);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 393);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	
	public FixedLengthStringData msgPopup = new FixedLengthStringData(1000);
	public FixedLengthStringData msgresult = new FixedLengthStringData(10);
	public FixedLengthStringData msgDisplay = new FixedLengthStringData(10);

	public LongData S5026screensflWritten = new LongData(0);
	public LongData S5026screenctlWritten = new LongData(0);
	public LongData S5026screenWritten = new LongData(0);
	public LongData S5026protectWritten = new LongData(0);
	public GeneralTable s5026screensfl = new GeneralTable(AppVars.getInstance());
	public FixedLengthStringData reserveUnitsDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData susur002flag = new FixedLengthStringData(1); 

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5026screensfl;
	}

	public S5026ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(ptdateOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(btdateOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "-01","01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(clamantOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"05","12","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(resndescOut,new String[] {"06","12","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currcdOut,new String[] {"08","12","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(otheradjstOut,new String[] {"07","12","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rsuninOut,new String[] {"70","31","-70","59",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rundteOut,new String[] {"71","32","-71","69",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankacckeyOut,new String[] {"55", "56", "55", "57", null, null, null, null, null, null, null, null});
		fieldIndMap.put(crdtcrdOut,new String[] {"60", "61", "60", "62", null, null, null, null, null, null, null, null});
		fieldIndMap.put(reqntypeOut,new String[] {"80", "81", "80", "82", null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		fieldIndMap.put(unexpiredprmOut,new String[] {null,null, null,"19", null, null, null, null, null, null, null, null});//ILB-1018
		fieldIndMap.put(suspenseamtOut,new String[] {null,null, null,"20", null, null, null, null, null, null, null, null});//ILB-1018
		
		screenSflFields = new BaseData[] {hactval, hcrtable, hemv, htype, hcnstcur, hcover, hjlife, coverage, rider, fund, fieldType, shortds, cnstcur, estMatValue, actvalue, life,commclaw};
		screenSflOutFields = new BaseData[][] {hactvalOut, hcrtableOut, hemvOut, htypeOut, hcnstcurOut, hcoverOut, hjlifeOut, coverageOut, riderOut, virtfundOut, typeOut, shortdsOut, cnstcurOut, emvOut, actvalueOut, lifeOut,commclawOut};
		screenSflErrFields = new BaseData[] {hactvalErr, hcrtableErr, hemvErr, htypeErr, hcnstcurErr, hcoverErr, hjlifeErr, coverageErr, riderErr, virtfundErr, typeErr, shortdsErr, cnstcurErr, emvErr, actvalueErr, lifeErr,commclawErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, effdate,reserveUnitsDate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, effdateErr,reserveUnitsDate};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdateDisp,reserveUnitsDateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5026screen.class;
		screenSflRecord = S5026screensfl.class;
		screenCtlRecord = S5026screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5026protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5026screenctl.lrec.pageSubfile);
	}
	
	public int getDataAreaSize() {
		return 1350;
	}
	public int getDataFieldsSize(){
		return 694;
	}
	public int getErrorIndicatorSize(){
		return 164;
	}
	public int getOutputFieldSize(){
		return 492;
	}
	
	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, planSuffix, descrip, policyloan, clamant, reasoncd, resndesc, currcd, otheradjst, estimateTotalValue, effdate, netOfSvDebt, zrcshamt, tdbtamt, taxamt,reserveUnitsInd,reserveUnitsDate,sacscurbal,sacscurbal1,payrnum, payorname,reqntype,bankacckey,bankdesc,bankkey,crdtcrd,unexpiredprm,suspenseamt};     
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, plnsfxOut, descripOut, policyloanOut, clamantOut, reasoncdOut, resndescOut, currcdOut, otheradjstOut, estimtotalOut, effdateOut, netdebtOut, zrcshamtOut, tdbtamtOut, taxamtOut,rsuninOut,rundteOut,sacscurbalOut,sacscurbal1Out,payrnumOut, payornameOut,reqntypeOut,bankacckeyOut,bankdescOut,bankkeyOut,crdtcrdOut,unexpiredprmOut,suspenseamtOut};    
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, plnsfxErr, descripErr, policyloanErr, clamantErr, reasoncdErr, resndescErr, currcdErr, otheradjstErr, estimtotalErr, effdateErr, netdebtErr, zrcshamtErr, tdbtamtErr, taxamtErr,rsuninErr,rundteErr,sacscurbalErr,sacscurbal1Err,payrnumErr, payornameErr,reqntypeErr,bankacckeyErr,bankdescErr,bankkeyErr,crdtcrdErr,unexpiredprmErr,suspenseamtErr};    
		
	}

}
