/*
 * File: P6677.java
 * Date: 30 August 2009 0:51:42
 * Author: Quipoz Limited
 *
 * Class transformed from P6677.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.terminationclaims.dataaccess.dao.ClnnpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.InvspfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.recordstructures.Livclmrec;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.enquiries.dataaccess.AsgnenqTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.productdefinition.tablestructures.Ty501rec;
import com.csc.life.terminationclaims.dataaccess.AclhTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.FluprgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpenqTableDAM;
import com.csc.life.terminationclaims.screens.S6677ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.terminationclaims.tablestructures.T6617rec;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.life.terminationclaims.tablestructures.T6690rec;
import com.csc.life.terminationclaims.tablestructures.T6692rec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.terminationclaims.tablestructures.T6694rec;
import com.csc.life.terminationclaims.tablestructures.T6696rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE; //ILIFE-8299

/**
 * <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
*
*  P6677 - Regular Benefit Claims.
*  -------------------------------
*
*  Overview.
*  ---------
*
*  This program will perform some of the maintenance  functions
*  for  Regular Benefit Claims. It will be invoked initially by
*  the Regular Payments sub-menu and may also  be  called  from
*  within Contract Enquiry.
*
*  The following functions will be catered for:
*
*       . Register
*       . Adjust
*       . Enquire
*
*  The  Register  function  corresponds  to  Create and will be
*  carried out when WSSP-FLAG is 'C'.
*
*  The Adjust  function  corresponds  to  Modify  and  will  be
*  performed when WSSP-FLAG is 'M'.
*
*  The  Enquire  function will be carried out when WSSP-FLAG is
*  'I'.
*
*  The actual  payments  themselves  are  created  by  a  batch
*  payments  job  which will only operate on claims that are in
*  an 'Approved' state and  are  due  for  payment.  The  Batch
*  Payments  job  will effect any currency conversions that are
*  necessary at the time the payments are made. Claims will  be
*  registered  in  the  currency  of the contract to which they
*  attach but a separate Payment Currency may be  specified  at
*  the time the claim is approved.
*
*  The  Sub-menu will control which actions are allowed against
*  the claim details.
*
*  The Registration and Adjust functions may allow the user  to
*  change  the  frequency  of  the  claim  as  defined  on  the
*  component record.  This  will  be  dependant  on  the  Claim
*  Detail  Rules  on  table  T6696. If the frequency is altered
*  then this will of course affect the  amount  of  the  claim.
*  The   program  will  take  this  into  account  when  it  is
*  comparing  the  requested  amount  of  claim   against   the
*  originally  defined  amount  on  COVR  and  also against the
*  total so far of all existing claims.
*
*  The  Total  Sum  Assured  will  be  displayed  in   contract
*  currency  and  the  claim  details  will be captured in this
*  currency.
*
*  Initialise.
*  -----------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  Initialise  all  relevant  variables  and prepare the screen
*  for display.
*
*  Set a 'First Time' flag for use in the update section.
*
*  Set the variable heading of the screen to the long description
*  of the Transaction Code by reading table T1688.
*
*  Perform a RETRV on the CHDRRGP and REGP files.
*
*  Use CHDRRGP to display the heading  details  on  the  screen
*  using  T5688  for  the  contract  type  description  and the
*  Client  file  for  the  relevant  client  names.  The  short
*  descriptions  for  the  contracts  risk  and  premium status
*  codes should be  obtained  from  T3623,  (Risk  Status)  and
*  T3588, (Premium Status).
*
*  Set  the  Contract Currency in the heading from the Contract
*  Header.
*
*  For Registration set the Payment Currency  to  the  Contract
*  Currency.
*
*  Read  all  the  COVR  records for this component on the plan
*  accumulating all of the Sum  Assured  amounts.  All  of  the
*  COVR  records across the plan will share the same frequency.
*  Display this amount in the Total Sum Assured field.
*
*  Display the REGP details on the screen  looking  up  all  of
*  the descriptions from DESC where appropriate.
*
*  For  Adjustments  or  Enquiries use the claim status to read
*  table T5400 to obtain the short description.  Also  use  the
*  Payment  Type  to  read  table  T6691  to obtain the Regular
*  Payment Type short description. For  Registration  where  no
*  claim  status  or payment type has yet been established read
*  T6690 with the Component Code as key, pick  up  the  Default
*  Regular  Payment  Type  and use this to access T6691 for the
*  short description. Remember to replace  this  later  if  the
*  entered Reason Code has a different Payment Type.
*
*  Read   table   T6693   with   the   current  Payment  Status
*  concatenated with the CRTABLE of the  associated  component.
*  Locate  the  Transaction  Code  of the transaction currently
*  being processed and select the  corresponding  Next  Payment
*  Status.  For  Registration the payment status will be set to
*  '**' for the read of the table. Also  for  Registration  use
*  the  Next Payment Status immediately to access T5400 for the
*  short description.
*
*  If there are Follow Ups currently awaiting completion set  a
*  '+'   in  the  Follow  Ups  indicator  field.  This  can  be
*  determined  by  reading  FLUPRGP  with  a  key  of  CHDRCOY,
*  CHDRNUM,  CLAMNUM a FUPNO of zero and a function of BEGN. If
*  a record is found that matches on Company,  Contract  Number
*  and Claim Number then there are Follow Ups for the payment.
*  If in Create mode, a '+' in the Follow Ups indicator field
*  indicates that default Follow Ups exist.
*
*  If Annuity Details are found, set a '+' in the Annuity Details
*  indicator field.
*
*  If  the  bank details on REGP are non-blank set a '+' in the
*  Bank Details indicator field.
*
*  For Register, the Regular Payment Sequence Number,  RGPYNUM,
*  should  be  set  to  1  greater  than  the  previous highest
*  sequence number for this contract. So read  REGPENQ  with  a
*  function  of  BEGN  and  a Sequence Number set to all 9's to
*  find the most recently  added  Regular  Payment  record  and
*  increment  that Sequence Number by 1 to use on this file. If
*  End of File  is  received  or  the  returned  record  has  a
*  different  Company  or Contract number then set the Sequence
*  Number to 1. Display the value on the screen.
*
*  Read table T5671  with  the  key  of  Transaction  Code  and
*  Component  Type.  Match the current program id. (WSAA-PROG),
*  against the programs and pick  up  the  matching  Validation
*  Item  Key.  Concatenate this with the Contract Currency Code
*  from CHDRRGP and read T5606.
*
*  For Modify and Enquire display the frequency from  the  REGP
*  file.
*
*  For  Register display the default Frequency from T5606. Also
*  default the Claim Currency to  the  Contract  Currency  Code
*  from CHDRRGP.
*  The calculation of the Sum Assured should allow a frequency of
*  00 (for one-off payments) therefore a 1 should be used for the
*  computation.
*
*  Display and Validation. (2000 Section).
*  ---------------------------------------
*
*  If   returning  from  a  program  further  down  the  stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  If WSSP-FLAG = 'I' protect the screen  apart  from  the  two
*  screen switching indicators.
*
*  If  there  is  a  cancellation date display it on the screen
*  and set off the indicators so that the date and its  literal
*  appear on the screen.
*
*  Converse with the screen using the I/O module.
*
*  If 'KILL' has been pressed skip this section.
*
*  If 'CALC' has been pressed set WSSP-EDTERROR to 'Y'.
*
*  .  Reason  Code.  This  is mandatory, check that it has been
*  entered. Read T6692 to obtain the Associated  Payment  Type.
*  If  entered  it  will  be validated by the screen I/O module
*  but the long description will only be present  if  the  user
*  has  employed windowing to select the entry. Therefore if it
*  is blank you should read the DESC file to  obtain  the  long
*  description.  If  the associated payment type from the Extra
*  Data screen differs from  that  previously  used  then  also
*  read   T6691  to  obtain  the  Regular  Payment  Type  short
*  description for display.
*
*  . Evidence. This field is free format and optional.
*
*  . Payment Method. This is mandatory, check that it has  been
*  entered.  If  entered it will be validated by the screen I/O
*  module but the short description will  only  be  present  if
*  the  user  has  employed  windowing  to  make the selection.
*  Therefore if it is blank you should read the  DESC  file  to
*  obtain the short description.
*
*  .  Payment Currency. This is validated by the I/O module but
*  the  program  must  read  the  DESC  file  to   obtain   the
*  decription.  If the user leaves it blank then re-set it from
*  the Contract Currency.
*
*  . Frequency Code. The user may only override  the  Frequency
*  if  the rules on T6696 allow. If Override is not allowed and
*  the user has changed it then set it back to the  value  from
*  T5606  and  give  a message indicating what has happened. If
*  Override is allowed then check that it has been entered.  If
*  entered  it  will  be validated by the screen I/O module but
*  the short description will only be present if the  user  has
*  employed  windowing  to make the selection. Therefore always
*  read the DESC file to obtain the short description.
*
*  If the  Frequency  Code  has  changed  then  the  Total  Sum
*  Assured  field  must  also be re-calculated to bring it into
*  line with the new frequency.
*  The calculation of the Sum Assured should allow a frequency of
*  00 (for one-off payments) therefore a 1 should be used for the
*  computation.
*
*  .  Payee  Client.  Read  table  T6694  with  the  Method  of
*  Payment.  The  'TO'  Sub Account is obtained from here along
*  with rules determining  what  extra  details  are  required.
*  Check   the   Bank,  Payee  and  Contract  details  required
*  indicators. These are not mutually exclusive so all must  be
*  checked.  If  the  'Payee  Details'  is  'Y'  then the Payee
*  client field is mandatory otherwise it must not be  entered.
*  If  it  is  mandatory check that the entry is a valid client
*  number. If found format the name for display.
*
*  . Percentage.  Obtain  the  default  percentage  from  table
*  T6696.  If  the user has entered nothing in the amount field
*  then  display  the  default   percentage   here.   Otherwise
*  calculate  this  percentage  value  as  the  Amount  To  Pay
*  expressed as a percentage of the Sum Assured on the screen.
*
*  .  Destination  Key.  If  the  'Contract  Details  Required'
*  indicator  on T6694 is 'Y' then a valid contract number must
*  be entered here. Otherwise the field must be blank.
*
*  . Amount To Pay. This entry may be left as zero by the  user
*  in  which case the program will calculate the value from the
*  default  percentage  from  T6696   and   the   Sum   Assured
*  calculated  by  the  program  earlier.  If an entry has been
*  made then it must be validated against the  rules  on  T6696
*  and  the  Total  Sum  Assured.  First  calculate the maximum
*  amount to pay  and  the  minimum  amount  to  pay  from  the
*  maximum  and  minimum values on T6696. If the entered amount
*  falls outside this range then give an error message.
*
*
*  . Registration Date. This is the  Effective  Date  of  Claim
*  and  is  mandatory.  It  may not be greater than the current
*  system date although a date in the past will be accepted.
*
*  . First Payment Date. This is  optional.  If  no  entry  has
*  been made then proceed as follows:
*
*       Check  for  a Default Deferment Period on T6696. If one
*       is there use this to calculate a default First  Payment
*       Date  and  set this in the field. The Default Deferment
*       Period is held as an  integer  number  of  frequencies.
*       The  frequency  itself  is  held against the period. If
*       there is no Default Deferment Period on T6696 then  use
*       the  Deferment  Period  on T5606 to calculate the First
*       Payment Date.
*
*       If there is no entry here  either  then  calculate  the
*       minimum  Deferment  Date  from  the  Minimum  Deferment
*       period on  T6696  and  set  this  value  in  the  First
*       Payment  Date.  The  Minimum  Deferment  period is also
*       held as a number of frequencies.
*
*       If this also is blank then default  the  First  Payment
*       Date to the Registration Date.
*
*  If  an  entry has been made then check that it does not fall
*  within the Minimum Deferment Period as defined on T6696.
*
*  . Review Date. This is optional.  Use  the  Review  Term  on
*  T6696  to  check  that  it is at least  as far in advance of
*  the Registration Date as this  value  indicates.  It  should
*  not  be less than the First Payment Date. The Review Term is
*  held as an integer number  of  frequencies.  If  it  is  not
*  entered it should be set to Max Date.
*
*  .  Next  Payment  Date.  For  Registration  default the Next
*  Payment Date to the First Payment Date.
*
*  . Final Payment Date. This is optional  and  if  left  blank
*  will  be  set  to  Max  Date. If entered it must not be less
*  than the Next Payment Date.
*
*  . Anniversary Date. This may be entered by the user  but  if
*  not  then  calculate  it  from the Registration Date and the
*  Indexation Frequency from T6696. The default is Max Date.
*
*  A check must also be made that the total amount of claim  at
*  any  one time does not exceed 100% of the Total Sum Assured.
*  In order to do this the following check must be carried out:
*
*       Read all of the REGPENQ records that match on  the  key
*       apart from Payment Number.
*
*       Check  the  First  Payment  Date, (FPAYDATE), and Final
*       Payment Date, (EPAYDATE) on  the  REGPENQ  record,  and
*       the  Cancellation Date. If there is any overlap between
*       those and the same dates on the screen for the  current
*       claim   then   include   the  REGPENQ  record  in  your
*       calculations.
*
*       Convert the Claim Amount to the same frequency  as  the
*       current claim, if necessary, and accumulate it.
*
*       Finally  take  the total accumulated amount from all of
*       the selected REGPENQ records and add it to the  entered
*       Claim  Amount. If the new total is now greater than the
*       calculated Sum Assured then indicate to the  user  that
*       the  total  has  been exceeded and display the total in
*       the field at the foot of the screen  which  should  now
*       be revealed.
*
*  .  The  Follow  Ups indicator, the Annuity Details indicator
*  and the Bank details indicator may only be 'X', '+' or space.
*
*  Updating.
*  ---------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN = '*', then skip this section.
*
*  If 'KILL' has been pressed skip this section.
*
*  If WSSP-FLAG = 'I' then skip this section.
*
*  If  this is the first time through the 3000 section, ('First
*  Time Flag' is set to 'Y'), then carry  out  the  Valid  Flag
*  '2'  updating on the Contract Header and, if in Modify mode,
*  the Regular Payment File as follows:
*
*       Perform a READH on  CHDR,  set  Validflag  to  '2'  and
*       re-write the record.
*
*       Increment the TRANNO by 1
*
*       Write  a  new  CHDR with a Validflag of '1' and the new
*       TRANNO.
*
*       If the program is in Modify mode,  (WSSP-FLAG  =  'M'),
*       then  the  current  REGP  record  must  be  made into a
*       history record by setting the  REGP  Validflag  to  '2'
*       and performing an UPDAT on REGP.
*
*  Move  all the screen fields to REGP and place the new TRANNO
*  on the record.
*
*  If either the Follow  Ups  indicator  or  the  Bank  Details
*  indicator are 'X' then perform a KEEPS on the REGP file.
*
*  Otherwise  the update proper is to be performed so write the
*  new REGP record and write a PTRN record.
*
*  Call Softlock to unlock the contract.
*
*  Where Next.
*  -----------
*
*  If  returning  from  a  program  further  down  the   stack,
*  WSSP-SEC-ACTN  =  '*',  then  re-load the next 8 programs in
*  the stack from Working Storage.
*
*  If returning  from  the  Follow  Ups  path  the  Follow  Ups
*  indicator  will  be  '?'.  Check if Follow Ups were actually
*  added by reading the FLUPRGP file for this  Payment  Details
*  record  using  a  key  of  CHDRCOY,  CHDRNUM,  a  CLAMNUM of
*  '00000000' and a function of BEGN.  If  a  record  is  found
*  that  matches on Company and Contract Number then Follow Ups
*  have been set up. If a record has been set up by the  Follow
*  Ups  processing  indicate  this  to  the user by setting the
*  Follow Ups indicator field to '+'. If  there  is  no  record
*  then move space to the Follow Ups indicator field.
*
*  If  returning  from  the  Bank Details path the Bank Details
*  indicator will be '?'. Check if Bank Details  were  actually
*  added  by  performing  a RETRV on the REGP file. If the Bank
*  details on the REGP file are non blank indicate this to  the
*  user  by setting the Bank Details indicator field to '+'. If
*  there  are  no  details  move  space  to  the  Bank  Details
*  indicator field.
*
*  If  the  Follow  Ups  indicator  is  'X'  then switch to the
*  Follow Ups path as follows:
*
*       Store the next 8  programs  in  the  stack  in  Working
*       Storage.
*
*       Call GENSSW with an action of 'A'.
*
*       If  there  is an error code returned from GENSSW use it
*       as an error code on the  Follow  Ups  indicator  field,
*       set  WSSP-EDTERROR  to  'Y'  and  set  WSSP-NEXTPROG to
*       SCRN-SCRNAME and go to exit.
*
*       Load the 8 programs returned  from  GENSSW  in  to  the
*       next 8 positions in the program stack.
*
*       Set the Follow Ups indicator to '?'.
*
*       Set WSSP-SEC-ACTN to '*'.
*
*       Add 1 to the program pointer and go to exit.
*
*
*  If  the  Bank  Details  indicator  is 'X' then switch to the
*  Bank Details path as follows:
*
*       Store the next 8  programs  in  the  stack  in  Working
*       Storage.
*
*       Call GENSSW with an action of 'B'.
*
*       If  there  is an error code returned from GENSSW use it
*       as an error code on the Bank Details  indicator  field,
*       set  WSSP-EDTERROR  to  'Y'  and  set  WSSP-NEXTPROG to
*       SCRN-SCRNAME and go to exit.
*
*       Load the 8 programs returned  from  GENSSW  in  to  the
*       next 8 positions in the program stack.
*
*       Set the Bank Details indicator to '?'.
*
*       Set WSSP-SEC-ACTN to '*'.
*
*       Add 1 to the program pointer and go to exit.
*
*  If   returning  from  a  program  further  down  the  stack,
*  WSSP-SEC-ACTN = '*', then move space  to  WSSP-SEC-ACTN  and
*  cause  the  program  to  redisplay  from the 2000 section by
*  setting WSSP-NEXTPROG to SCRN-SCRNAME and go to exit.
*
*  Add 1 to the program pointer and exit.
*
*  Notes.
*  ------
*
*  Tables Used.
*  ------------
*
*  . T1688 - Transactions Codes
*            Key: Transaction Code
*
*  . T3629 - Currency Code Details
*            Key: CURRCD
*
*  . T3588 - Contract Premium Status Codes
*            Key: PSTCDE from CHDRRGP
*
*  . T3623 - Contract Risk Status Codes
*            Key: STATCODE from CHDRRGP
*
*  . T5606 - Regular Benefit Component Edit Rules
*            Key: Validation Item Key (from T5671) || Currency
*                                                     Code
*  . T5661 - Follow-up Codes
*            Key: Follow-up Code
*
*  . T5671 - Generic Program Switching
*            Key: Transaction Code || CRTABLE
*
*  . T5677 - Default Follow-up Codes
*            Key: Transaction Code and Follow-up Method
*
*  . T5688 - Contract Definition
*            Key: Contract Type
*
*  . T5400 - Regular Payment Status
*            Key: Regular Payment Status Code
*
*  . T6625 - General Annuity Details
*            Key: CRTABLE
*
*  . T6692 - Regular Payment Reason Codes
*            Key: Regular Payment Reason Code
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be '****' and for selections against
*            a Component set Payment Status to '**'.
*
*  . T6694 - Regular Payment Method of Payment
*            Key: Regular Payment MOP
*
*  . T6696 - Regular Claim Detail Rules
*            Key: CRTABLE || Reason Code
*
******************Enhancements for Life Asia 1.0****************
*
* This module has been enhanced to cater for waiver of premium
* (WOP) processing. The waiver of premium sum assured amount is
* based on the accumulation of all the premiums of the
* components on the proposal and the policy fee as specified on
* table TR517. The Following additional processing has been
* introduced :
*
* - For the component being processed access TR517. If the item
*   is present, then we are processing a WOP component.
*
* - Obtain the PAYR record for this contract. The reason for this
*   is that the frequency of payment is the payment frequency
*   of the contract because the waiver is really the premium
*   instalment of the contract and is only payable on the date
*   present on the PAYR record.
*
* - Default :
*
*     - Sum assured
*     - Claim amount
*     - Percentage of Sum Assured to be claimed (100%)
*     - Billing frequency (from PAYR record)
*     - Regular Payment Reason (WOP)
 *
 *****************************************************************
 * </pre>
 */
public class P6677 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(P6677.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6677");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT5606Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5606Edtitm = new FixedLengthStringData(5).isAPartOf(wsaaT5606Key, 0);
	private FixedLengthStringData wsaaT5606EdtitmW = new FixedLengthStringData(5).isAPartOf(wsaaT5606Edtitm, 0,
			REDEFINE);
	private FixedLengthStringData wsaaT5606Wild = new FixedLengthStringData(1).isAPartOf(wsaaT5606EdtitmW, 4);
	private FixedLengthStringData wsaaT5606Currcd = new FixedLengthStringData(3).isAPartOf(wsaaT5606Key, 5);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);

	protected FixedLengthStringData wsaaT6693Key = new FixedLengthStringData(6);
	protected FixedLengthStringData wsaaT6693Rgpystat = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key, 0);
	protected FixedLengthStringData wsaaT6693Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key, 2);

	private FixedLengthStringData wsaaT6696Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6696Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6696Key, 0);
	private FixedLengthStringData wsaaT6696Cltype = new FixedLengthStringData(2).isAPartOf(wsaaT6696Key, 4);

	private FixedLengthStringData wsaaClamnum2 = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaClamnumFill = new ZonedDecimalData(3, 0).isAPartOf(wsaaClamnum2, 0).setUnsigned();
	private ZonedDecimalData wsaaRgpynum = new ZonedDecimalData(5, 0).isAPartOf(wsaaClamnum2, 3).setUnsigned();
	private FixedLengthStringData wsaaRgpymop = new FixedLengthStringData(1);
	private String wsaaAnnuity = "";
	protected PackedDecimalData wsaaIntermed = new PackedDecimalData(7, 5);
	protected ZonedDecimalData wsaaNextRgpy = new ZonedDecimalData(5, 0).setUnsigned();
	protected FixedLengthStringData wsaaPayclt = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaRgpystat = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaEdtitm = new FixedLengthStringData(5);
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLastFreq = new ZonedDecimalData(2, 0).setUnsigned();
	protected ZonedDecimalData wsaaFreq2 = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStatuz = "";
	private String wsaaStopProcess = "";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	protected FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	protected ZonedDecimalData wsaaPayrseqno = new ZonedDecimalData(1, 0).setUnsigned();
	protected String wsaaFreqFlag = "";
	private String wsaaAccidentClaimInd = "";
	/* Contract Frequency. */
	private ZonedDecimalData wsaaFreq3 = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPremCurrency = new FixedLengthStringData(3);
	protected FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private String wsaaOverlap = "";
	/* Sum insured in Contract Frequency */
	protected PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2).setUnsigned();
	/* Sum insured in Payment Frequency. */
	protected PackedDecimalData wsaaSumins2 = new PackedDecimalData(17, 2).setUnsigned();
	private FixedLengthStringData wsaaCurrcd = new FixedLengthStringData(3);
	protected PackedDecimalData wsaaS6677Pymt = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaPrcnt = new PackedDecimalData(7, 4).setUnsigned();
	private PackedDecimalData wsaaPymt = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaPymt2 = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaTotPymt = new PackedDecimalData(17, 2).setUnsigned();
	private final String wsaaLargeName = "LGNMS";
	private PackedDecimalData index1 = new PackedDecimalData(3, 0).setUnsigned();
	protected PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	protected PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	protected String firstTime = "";
	private PackedDecimalData wsaaMaxPrcntValue = new PackedDecimalData(17, 2).setUnsigned();
	private static final String wsaaClaimCompMess = "  Total claim for component, this term :";
	private static final String wsaaClaimReasMess = "Total claim for Reason Code, this term :";

	protected FixedLengthStringData wsaaWopFlag = new FixedLengthStringData(1);
	protected Validator wopMop = new Validator(wsaaWopFlag, "Y");
	protected String wsaaIsWop = "N";
	private FixedLengthStringData wsaaPayrBillfreq = new FixedLengthStringData(2).init(SPACES);
	protected ZonedDecimalData wsaaPayrBillfreqNum = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCovrCrrcd = new ZonedDecimalData(8, 0).init(ZERO);
	protected PackedDecimalData wsaaCovrInstprem = new PackedDecimalData(13, 2);
	/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	/* Payor Details Logical File */
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private AclhTableDAM aclhIO = new AclhTableDAM();
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	protected ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	protected ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	protected ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	protected CovrTableDAM covrIO = new CovrTableDAM();
	protected CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluprgpTableDAM fluprgpIO = new FluprgpTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	protected PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	protected RegpTableDAM regpIO = new RegpTableDAM();
	private RegpenqTableDAM regpenqIO = new RegpenqTableDAM();
	/* Contract header - life new business */
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected Gensswrec gensswrec = new Gensswrec();
	protected Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	protected Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5606rec t5606rec = new T5606rec();
	protected T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	protected T6625rec t6625rec = getT6625rec();
	
	public T6625rec getT6625rec(){
		return new T6625rec();
	}
	
	private T6690rec t6690rec = new T6690rec();
	protected T6692rec t6692rec = new T6692rec();
	private T6693rec t6693rec = new T6693rec();
	protected T6694rec t6694rec = new T6694rec();
	protected T6696rec t6696rec = new T6696rec();
	protected Tr517rec tr517rec = new Tr517rec();
	private Wssplife wssplife = new Wssplife();
	private S6677ScreenVars sv = getLScreenVars(); // ScreenProgram.getScreenVars(
													// S6677ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	// ILIFE-1138 STARTS
	private FixedLengthStringData wsaaContractStatuzCheck = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaContStatcode = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 1);
	private FixedLengthStringData wsaaContPstcde = new FixedLengthStringData(2).isAPartOf(wsaaContractStatuzCheck, 3);
	private static final String ty501 = "TY501";
	private Ty501rec ty501rec = new Ty501rec();

	private static final String t6617 = "T6617";
	private static final String t517 = "T517";
	private T6617rec t6617rec = new T6617rec();
	// ILIFE-1138 ENDS

	private Taxdpf taxdbilpf = new Taxdpf();
	private TaxdpfDAO taxdbilpfDAO = getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
	private List<Taxdpf> taxdbilpfList = null;
	private PackedDecimalData wsaaCompTax = new PackedDecimalData(17, 2);
	protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	// CMRPY005Permission
	protected boolean CMRPY005Permission = false;
	boolean cmoth002flag = false;
	private static final String feaConfigCMOTH002Flag = "CMOTH002";
	private static final String feaConfigCMRPY005Permission = "CMRPY005";
	
	//CLM008 & CLM023
	private static final String FEAID_CMOTH003_PERMISSION = "CMOTH003"; 
	private boolean cmoth003permission = false;
	boolean entryFlag = false;
	private Alocnorec alocnorec = new Alocnorec();
	private Notipf notipf=null;
	private static final String RP = "RP";
	public static final String CLMPREFIX = "CLMNTF";
	private List<Clnnpf> clnnpfList1 = new ArrayList<Clnnpf>();
	private ClnnpfDAO clnnpfDAO= getApplicationContext().getBean("clnnpfDAO",ClnnpfDAO.class);
	private List<Invspf> invspfList1 = new ArrayList<Invspf>();
	private InvspfDAO invspfDAO= getApplicationContext().getBean("invspfDAO",InvspfDAO.class);
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	
	
	//ILIFE-8299-START
	private FixedLengthStringData wsaaNoMoreAssignees = new FixedLengthStringData(1).init(SPACES);
	private Validator asgnenqEof = new Validator(wsaaNoMoreAssignees, "Y");
	private AsgnenqTableDAM asgnenqIO = new AsgnenqTableDAM();	
	private PackedDecimalData wsbbSumins = new PackedDecimalData(17, 2).setUnsigned();
	private Livclmrec livclmrec = new Livclmrec();
	protected ExternalisedRules er = new ExternalisedRules();
	private String wsbbCrtable ="";
	protected boolean isFPPresent=false;
	protected boolean isSPPresent=false;
	protected boolean isValidationerror=false;
	protected boolean isFirstTime=false;	
	protected boolean CMRPY012Permission = false;
	private static final String feaConfigCMRPY012Permission = "CMRPY012";
	protected static final String LWPR = "LWPR";
	protected static final String LPS = "LPS";
	//ILIFE-8299-END
	//ILJ-48 Starts
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);
	private String itemPFX = "IT";
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-48 End
	private boolean CMRPY008Permission = false;
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf;
	protected Chdrpf chdrpf = null;
	private Mandpf mandlnb = null;
	private static final String MANDATE ="Mandlnb";
	private static final String CLIENT ="CN";
	private static final String CHEQUE ="C";
	private static final String BANKCREDIT ="B";
	//IBPLIFE-1702
	private String expiredFlag="2";
	private String effectiveFlag="1";
	private String claimRegi="Claim Registered";
	private int noTransnoCount = 1;
	private PackedDecimalData wsaaTempForClaimValid = new PackedDecimalData(17, 2).setUnsigned();
	
	/**
	 * Contains all possible labels used by goTo action.
	 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, create1060, exit1090, preExit, validatePayee2045, checkPercentage2050, checkDestination2060, noReviewTerm2076, checkPayDate2077, validateFollow2079, checkForErrors2080, exit2090, exit3090, popUp4050, gensww4010, nextProgram4020, exit4090, callK2050,investResult,claimNotes
	}

	public P6677() {
		super();
		screenVars = sv;
		new ScreenModel("S6677", AppVars.getInstance(), sv);
	}

	protected S6677ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6677ScreenVars.class);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	/**
	 * <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	protected void initialise1000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;

		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
				case create1060:
					create1060();
				case exit1090:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void initialise1010() {
		// CML009cmoth002
		CMRPY005Permission = FeaConfg.isFeatureExist("2", feaConfigCMRPY005Permission, appVars, "IT");

		cmoth002flag = FeaConfg.isFeatureExist("2", feaConfigCMOTH002Flag, appVars, "IT");
		CMRPY012Permission = FeaConfg.isFeatureExist("2", feaConfigCMRPY012Permission, appVars, "IT");//ILIFE-8299
		cmoth003permission = FeaConfg.isFeatureExist("2", FEAID_CMOTH003_PERMISSION, appVars, "IT");//ICIL-1045
		CMRPY008Permission = FeaConfg.isFeatureExist("2", "CMRPY008", appVars,"IT");	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}

		wsaaRgpymop.set(SPACES);
		wsaaAnnuity = "N";
		wsaaBatckey.set(wsspcomn.batchkey);
		// ILIFE-1138 STARTS
		wsaaContractStatuzCheck.set(SPACES);
		// ILIFE-1138 ENDS
		sv.dataArea.set(SPACES);
		firstTime = "Y";
		wsaaStopProcess = "N";
		sv.anvdate.set(varcom.vrcmMaxDate);
		sv.aprvdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.cancelDate.set(varcom.vrcmMaxDate);
		sv.crtdate.set(varcom.vrcmMaxDate);
		sv.finalPaydate.set(varcom.vrcmMaxDate);
		sv.firstPaydate.set(varcom.vrcmMaxDate);
		sv.lastPaydate.set(varcom.vrcmMaxDate);
		sv.nextPaydate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.recvdDate.set(varcom.vrcmMaxDate);
		sv.incurdt.set(varcom.vrcmMaxDate);
		sv.revdte.set(varcom.vrcmMaxDate);
		sv.riskcommdte.set(varcom.vrcmMaxDate);	//ILJ-48
		if(CMRPY012Permission){
			sv.ovrpermently.set("N");//ILIFE-8299
			sv.ovrsumin.set(ZERO);//ILIFE-8299
			wsbbSumins.set(ZERO); //ILIFE-8299
		}		
		wsaaSumins.set(ZERO);
		wsaaSumins2.set(ZERO);
		wsaaPayrseqno.set(ZERO);
		wsaaPymt.set(ZERO);
		wsaaTotPymt.set(ZERO);
		wsaaLastFreq.set(ZERO);
		wsaaIntermed.set(ZERO);
		sv.pymt.set(ZERO);
		sv.prcnt.set(ZERO);
		sv.zrsumin.set(ZERO);
		sv.totalamt.set(ZERO);
		sv.totamnt.set(ZERO);
		if (CMRPY005Permission) {
			// ICIL-556
			sv.adjustamt.set(ZERO);
			sv.netclaimamt.set(ZERO);
			sv.resndesc.set(SPACES);
			sv.reasoncd.set(SPACES);
		}
		tr517rec.tr517Rec.set(SPACES);
		wsaaCovrInstprem.set(ZERO);
		//ILJ-48 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, itemPFX);
		if(!cntDteFlag)	{
			sv.riskcommdteOut[varcom.nd.toInt()].set("Y");
		}
		//ILJ-48 End
		/* Get Todays Date. */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, "****")) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/* Read the Transaction Code Description for variable heading */
		descIO.setDataKey(SPACES);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setDesctabl(tablesInner.t1688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.descrip.set(descIO.getLongdesc());
		} else {
			sv.descrip.fill("?");
		}
		if (isEQ(wsspcomn.flag, "I")) {
			try {
				if (isNE(chdrrgpIO.getClusterWsaaInUse(), "Y")) {
					chdrrgpIO.setClusterWsaaInUse("Y");
					chdrrgpIO.setFunction(varcom.rlse);
					SmartFileCode.execute(appVars, chdrrgpIO);
					if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
						syserrrec.params.set(chdrrgpIO.getParams());
						syserrrec.statuz.set(chdrrgpIO.getStatuz());
						fatalError600();
					}
				}
			} catch (SQLException e) {
				LOGGER.error("Exception in Variable heading code description",e);
			}
		}
		/* Retrieve the Contract Header. */
		chdrrgpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}		
		/*  Read the Contract Type description.*/
		descIO.setDescitem(chdrrgpIO.getCnttype());
		descIO.setDesctabl(tablesInner.t5688);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		} else {
			sv.ctypedes.fill("?");
		}
		/* Read the Contract Currency Description. */
		descIO.setDescitem(chdrrgpIO.getCntcurr());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.currds.set(descIO.getShortdesc());
		} else {
			sv.currds.fill("?");
		}
		/* Read the Premium Status Description. */
		descIO.setDescitem(chdrrgpIO.getPstatcode());
		descIO.setDesctabl(tablesInner.t3588);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getLongdesc());
		} else {
			sv.pstate.fill("?");
		}
		/* Read the Risk Status Description. */
		descIO.setDescitem(chdrrgpIO.getStatcode());
		descIO.setDesctabl(tablesInner.t3623);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getLongdesc());
		} else {
			sv.rstate.fill("?");
		}
		/* Read Owner Details */
		cltsIO.setClntnum(chdrrgpIO.getCownnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(), varcom.mrnf)) || (isNE(cltsIO.getValidflag(), 1))) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		} else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		/* Set screen fields */
		sv.chdrnum.set(chdrrgpIO.getChdrnum());
		sv.cnttype.set(chdrrgpIO.getCnttype());
		sv.cownnum.set(chdrrgpIO.getCownnum());
		sv.occdate.set(chdrrgpIO.getOccdate());
		sv.ptdate.set(chdrrgpIO.getPtdate());
		sv.btdate.set(chdrrgpIO.getBtdate());
		sv.currcd.set(chdrrgpIO.getCntcurr());
		//ILJ-48 Starts
		if(cntDteFlag) {
			Hpadpf hpadpf = hpadpfDAO.getHpadData(chdrrgpIO.getChdrcoy().toString(), chdrrgpIO.getChdrnum().toString());
			if(hpadpf!=null) {
				sv.riskcommdte.set(hpadpf.getRskcommdate());
			}
		}
		//ILJ-48 End

		// ILIFE-1138 STARTS
		wsaaContStatcode.set(chdrrgpIO.getStatcode());
		wsaaContPstcde.set(chdrrgpIO.getPstatcode());
		// ILIFE-1138 ENDS
		/* Retrieve the Regular Payment Record. */
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Release the Regular Payment Record. */
		regpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Set the Cmpnt (Component) field - S6677-CRTABLE from the */
		/* information in the REGP record */
		sv.crtable.set(regpIO.getCrtable());
		wsaaCompTax.set(ZERO);
		/*ILIFE-8299-Start*/
		if(CMRPY012Permission && isEQ(sv.cnttype, LPS) && isEQ(sv.crtable, LWPR)){
			loadAssignee1020();//BRD-12 
			/*  Release the Contract Header.*/
			chdrrgpIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, chdrrgpIO);
			if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrrgpIO.getParams());
				fatalError600();
			}
		}	
		/*ILIFE-8299-End*/
		/* Default Follow Ups can exist in Create mode. */
		/* This perform has been moved to the end of this section, after */
		/* the RGPYNUM has been allocated. This forms part of the key */
		/* of the FLUP. */
		/* IF WSSP-FLAG NOT = 'C' */
		/* PERFORM 5000-CHECK-FOLLOW-UP */
		/* END-IF. */
		/* Accumulate the total sum insured within the plan. */
		covrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		covrIO.setChdrnum(chdrrgpIO.getChdrnum());
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setStatuz(varcom.oK);
		covrIO.setFunction(varcom.begn);

		// performance improvement -- Niharika Modi
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		while (!(isEQ(covrIO.getStatuz(), varcom.endp))) {
			accumCovr1600();
		}

		readTr5177000();
		readPayr8000();
		if (isEQ(wsaaPayrseqno, 0)) {
			wsaaPayrseqno.set(1);
		}
		/* Read T5645 to obtain the WOP payment method's sacscode, */
		/* sacstype and GLMAP. */
		readT56451800();
		/* Recalculate the Sum Assured if the REGP frequency and T5606 */
		/* frequency differ. Read T5671 to obtain the Edit item to read */
		/* T5606. Read T5606 to obtain the Benefit Billing frequency */
		readT56711500();
		wsaaT5606Edtitm.set(wsaaEdtitm);
		wsaaT5606Currcd.set(sv.currcd);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5606);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT5606Key);
		/* MOVE COVR-CRRCD TO ITDM-ITMFRM */
		itdmIO.setItmfrm(wsaaCovrCrrcd);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t5606, itdmIO.getItemtabl()))
				|| (isNE(wsaaT5606Key, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setDataKey(SPACES);
			itdmIO.setItemcoy(wsspcomn.company);
			itdmIO.setItemtabl(tablesInner.t5606);
			itdmIO.setItempfx("IT");
			wsaaT5606Wild.set("*");
			itdmIO.setItemitem(wsaaT5606Key);
			itdmIO.setItmfrm(wsaaCovrCrrcd);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t5606, itdmIO.getItemtabl()))
					|| (isNE(wsaaT5606Key, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
				itdmIO.setStatuz(varcom.endp);
				itdmIO.setGenarea(SPACES);
			}
			/**** MOVE G528 TO S6677-CURRCD-ERR */
			/**** MOVE 'Y' TO WSSP-EDTERROR */
		}
		t5606rec.t5606Rec.set(itdmIO.getGenarea());
		if (isNE(wsspcomn.flag, "C")) {
			if (isEQ(wsaaIsWop, "N")) {
				if ((isNE(t5606rec.benfreq, regpIO.getRegpayfreq())) && (isNE(t5606rec.benfreq, SPACES))) {
					wsaaFreq.set(t5606rec.benfreq);
					wsaaFreq2.set(regpIO.getRegpayfreq());
					/*
					 * 1 replaces 0 in the computation for a frequency of '00'
					 */
					if (isEQ(wsaaFreq, 0)) {
						wsaaFreq.set(1);
					}
					if (isEQ(wsaaFreq2, 0)) {
						wsaaFreq2.set(1);
					}
					compute(wsaaSumins2, 2).set((div((mult(wsaaSumins, wsaaFreq)), wsaaFreq2)));
					zrdecplrec.amountIn.set(wsaaSumins2);
					a000CallRounding();
					wsaaSumins2.set(zrdecplrec.amountOut);
					wsaaSumins.set(wsaaSumins2);
				} else {
					wsaaSumins2.set(wsaaSumins);
				}
			} else {
				if ((isNE(wsaaPayrBillfreq, regpIO.getRegpayfreq())) && (isNE(wsaaPayrBillfreq, SPACES))) {
					wsaaFreq.set(wsaaPayrBillfreq);
					wsaaFreq2.set(regpIO.getRegpayfreq());
					/*
					 * 1 replaces 0 in the computation for a frequency of '00'
					 */
					if (isEQ(wsaaFreq, 0)) {
						wsaaFreq.set(1);
					}
					if (isEQ(wsaaFreq2, 0)) {
						wsaaFreq2.set(1);
					}
					compute(wsaaSumins2, 2).set((div((mult(wsaaSumins, wsaaFreq)), wsaaFreq2)));
					zrdecplrec.amountIn.set(wsaaSumins2);
					a000CallRounding();
					wsaaSumins2.set(zrdecplrec.amountOut);
					wsaaSumins.set(wsaaSumins2);
				} else {
					wsaaSumins2.set(wsaaSumins);
				}
			}
		} else {
			wsaaSumins2.set(wsaaSumins);
		}
		/* MOVE WSAA-SUMINS2 TO S6677-SUMINS. */
		/* MOVE WSAA-SUMINS2 TO S6677-SUMIN. <004> */
		sv.zrsumin.set(wsaaSumins2);
		covrIO.setLife(wsaaLife);
		covrIO.setJlife(wsaaJlife);
		covrIO.setPremCurrency(wsaaPremCurrency);
		covrIO.setCrtable(wsaaCrtable);
		/* Read Life Details */
		lifeenqIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrrgpIO.getChdrnum());
		lifeenqIO.setLife(covrIO.getLife());
		lifeenqIO.setJlife(covrIO.getJlife());
		lifeenqIO.setFunction(varcom.begn);

		// performance improvement -- Niharika Modi
		lifeenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		SmartFileCode.execute(appVars, lifeenqIO);
		if ((isNE(lifeenqIO.getStatuz(), varcom.oK)) && (isNE(lifeenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrrgpIO.getChdrcoy(), lifeenqIO.getChdrcoy()))
				|| (isNE(chdrrgpIO.getChdrnum(), lifeenqIO.getChdrnum()))
				|| (isNE(covrIO.getLife(), lifeenqIO.getLife())) || (isEQ(lifeenqIO.getStatuz(), varcom.endp))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
			goTo(GotoLabel.create1060);
		}
		/* Non-Display Cancel Date and Total Claim Amount. */
		sv.canceldateOut[varcom.nd.toInt()].set("Y");
		sv.totalamtOut[varcom.nd.toInt()].set("Y");
		sv.lifcnum.set(lifeenqIO.getLifcnum());
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		getClientDetails1200();
		if ((isEQ(cltsIO.getStatuz(), varcom.mrnf)) || (isNE(cltsIO.getValidflag(), 1))) {
			sv.linsnameErr.set(errorsInner.e355);
			sv.linsname.set(SPACES);
		} else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		/* Check on TR585 to see whether it is Accident Claim */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.tr585);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(sv.crtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK) && isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.agclmstzOut[varcom.nd.toInt()].set("Y");
			wsaaAccidentClaimInd = "N";
		} else {
			wsaaAccidentClaimInd = "Y";
			sv.pymtOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(wsaaAccidentClaimInd, "Y")) {
			accidentBenefitClaims9180();
		}
		wsspcomn.chdrCownnum.set(chdrrgpIO.getCownnum().toString());

		wsspcomn.chdrClntcoy.set(wsspcomn.fsuco);

		if (cmoth002flag) {
			sv.intstdays.set(regpIO.getInterestdays());
			sv.intstrate.set(regpIO.getInterestrate());
			sv.intstamt.set(regpIO.getInterestamt());

			if (isEQ(sv.intstrate, ZERO)) {
				readT6617();

			}
			sv.action.set("Y");
		} else {
			sv.action.set("N");
		}
		
		// agoel51
		if (cmoth003permission) {
			if (isEQ(wsspcomn.flag, "C")) {
				allocateNumber();
			} else {
				sv.claimno.set(regpIO.getClaimno());
			}
			if(isEQ(regpIO.getClaimnotifino(),SPACES)){
				sv.notifino.set(SPACES);
			}
			else{
				sv.notifino.set(CLMPREFIX+regpIO.getClaimnotifino());
			}
		}
	}
	
	private void getChdrDetails()
	{
		chdrpf = chdrpfDAO.getChdrpf(chdrrgpIO.getChdrcoy().toString(), chdrrgpIO.getChdrnum().toString());
		if(null == chdrpf){
			syserrrec.params.set(chdrrgpIO.getChdrcoy().concat(chdrrgpIO));
			fatalError600();
		}
		if(isEQ(sv.rgpymop,SPACES))
			defaultPayment();
	}
	private void defaultPayment()
	{
		if(isEQ(chdrpf.getReqntype(),"4")){
			sv.rgpymop.set(BANKCREDIT);
			mandlnb = mandpfDAO.searchMandpfRecord(wsspcomn.fsuco.toString().trim(), chdrpf.getPayclt().trim(),
					chdrpf.getZmandref().trim(),MANDATE);
			if (mandlnb != null && isEQ(mandlnb.getCrcind(), SPACES)) {
				sv.ddind.set("X");
				sv.payclt.set(mandlnb.getPayrnum());
				clntpf = clntpfDAO.searchClntRecord(CLIENT, wsspcomn.fsuco.toString(), mandlnb.getPayrnum());
				if(null == clntpf){
					syserrrec.params.set(CLIENT.concat( wsspcomn.fsuco.toString()).concat(mandlnb.getPayrnum()));
					fatalError600();
				}
				if(isEQ(clntpf.getValidflag(),"1")){
					plainname100();
					sv.payenme.set(wsspcomn.longconfname);
				}
				wsspcomn.chdrCownnum.set(sv.payclt);
				
			}
		}
		else if(isEQ(chdrpf.getReqntype(),"B") || isEQ(chdrpf.getReqntype(),"1") || isEQ(chdrpf.getReqntype(),"2")){
			sv.rgpymop.set(CHEQUE);
		}
	}

	protected void plainname100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(),"C")) {
			corpname100();
			return ;
		}
		if (isNE(clntpf.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}

	protected void corpname100()
	{
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
	}
	/**
	 * @author agoel51 
	 *  
	 */
	private void allocateNumber()
    {
		alocnorec.function.set("NEXT");
		alocnorec.prefix.set("CM");
		alocnorec.company.set(wsspcomn.company);
		/*wsaaBranch.set(wsspcomn.branch);*/
		alocnorec.genkey.set(SPACES);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isEQ(alocnorec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(alocnorec.statuz);
			fatalError600();
		}
		if (isNE(alocnorec.statuz, varcom.oK)) {
			sv.claimnoErr.set(alocnorec.statuz);
		} else {
			sv.claimno.set(alocnorec.alocNo);
		}
    }
	
	protected void create1060() {
		/* Create a new Regular Payment. */
		if (isEQ(wsspcomn.flag, "C")) {
			readT66935200();
			createRegp1700();
		}
		/* Default values if we are processing a WOP that is present on */
		/* TR517. */
		if (isEQ(wsspcomn.flag, "C")) {
			if (isEQ(wsaaIsWop, "Y")) {
				/*
				 * Sum insured, claim amount ,and percentage of sum assured to
				 */
				/* be claimed. */
				sv.pymt.set(wsaaSumins);
				// CML009 set value for net claim amount initialization
				sv.netclaimamt.set(wsaaSumins);
				if (isEQ(tr517rec.zrwvflg01, "Y")) {
					compute(sv.pymt, 2).set(add(sv.pymt, (div(mult(wsaaCovrInstprem, wsaaPayrBillfreqNum), wsaaFreq))));
					zrdecplrec.amountIn.set(sv.pymt);
					a000CallRounding();
					sv.pymt.set(zrdecplrec.amountOut);
					// CML009 set value for net claim amount initialization
					sv.netclaimamt.set(zrdecplrec.amountOut);
					compute(wsaaIntermed, 6).setRounded(div(sv.pymt, wsaaSumins2));
					compute(wsaaPrcnt, 6).setRounded(mult(wsaaIntermed, 100));
					sv.prcnt.set(wsaaPrcnt);
				}
				sv.claimcurOut[varcom.pr.toInt()].set("Y");
				sv.cltype.set(t6690rec.rgpytype);
				/* MOVE 100 TO S6677-PRCNT <LA2110> */
				/*
				 * The first payment date. This is the date when the premium is
				 */
				/* next due. */
				/* MOVE PAYR-NEXTDATE <LA2110> */
				/* TO S6677-FIRST-PAYDATE <LA2110> */
				sv.firstPaydate.set(payrIO.getBillcd());
				/* The estimated next payment date. */
				datcon2rec.freqFactor.set(1);
				datcon2rec.frequency.set(sv.regpayfreq);
				/* MOVE PAYR-NEXTDATE TO DTC2-INT-DATE-1 <LA2110> */
				datcon2rec.intDate1.set(payrIO.getBillcd());
				datcon2rec.intDate2.set(ZERO);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					syserrrec.statuz.set(datcon2rec.statuz);
					fatalError600();
				} else {
					sv.nextPaydate.set(datcon2rec.intDate2);
				}
			}
		}
		/* Modify or inquire on an existing Regular Payment. */
		if (isNE(wsspcomn.flag, "C")) {
			readRegpDetails1300();
		}
		/* Check for annuity details */
		checkForAnnuity1900();
		/* Check T6694-CONTREQ details for annuities only */
		if (isEQ(wsaaAnnuity, "Y") && isEQ(wsspcomn.flag, "M")) {
			if (isEQ(t6694rec.contreq, "N")) {
				sv.destkeyOut[varcom.pr.toInt()].set("Y");
			} else {
				sv.destkeyOut[varcom.pr.toInt()].set(SPACES);
			}
			wsaaRgpymop.set(sv.rgpymop);
		}
		/* Default Follow Ups can exist in Create mode. */
		checkFollowUp5000();
		checkAnnyDetails6000();
		if(CMRPY008Permission){
			getChdrDetails();
		}
		compute(wsaaTempForClaimValid, 2).set(add(wsaaSumins, (div(mult(wsaaCovrInstprem, wsaaPayrBillfreqNum), wsaaFreq))));
		zrdecplrec.amountIn.set(wsaaTempForClaimValid);
		a000CallRounding();
		wsaaTempForClaimValid.set(zrdecplrec.amountOut);
	}

	/**
	 * <pre>
	*    Sections performed from the 1000 section above.
	 * </pre>
	 */
	protected void findDesc1100() {
		/* READ */
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK)) && (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/* EXIT */
	}

	protected void getClientDetails1200() {
		/* READ */
		/* Look up the contract details of the client owner (CLTS) */
		/* and format the name as a CONFIRMATION NAME. */
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if ((isNE(cltsIO.getStatuz(), varcom.oK)) && (isNE(cltsIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/* EXIT */
	}

	protected void readRegpDetails1300() {
		read1310();
		bypassBankDetails1380();
	}

	protected void read1310() {
		/* Read Payment Type Details. */
		descIO.setDescitem(regpIO.getRgpytype());
		descIO.setDesctabl(tablesInner.t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		} else {
			sv.rgpytypesd.fill("?");
		}
		/* Read Payee Details */
		if (isNE(regpIO.getPayclt(), SPACES)) {
			cltsIO.setClntnum(regpIO.getPayclt());
			getClientDetails1200();
			if ((isEQ(cltsIO.getStatuz(), varcom.mrnf)) || (isNE(cltsIO.getValidflag(), 1))) {
				sv.payenmeErr.set(errorsInner.e335);
				sv.payenme.set(SPACES);
			} else {
				plainname();
				sv.payenme.set(wsspcomn.longconfname);
			}
		}
		/* Read the Claim Status Description. */
		descIO.setDescitem(regpIO.getRgpystat());
		/* MOVE T6663 TO DESC-DESCTABL. */
		descIO.setDesctabl(tablesInner.t5400);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.statdsc.set(descIO.getShortdesc());
		} else {
			sv.statdsc.fill("?");
		}
		/* Read the Reason Code Description. */
		descIO.setDescitem(regpIO.getPayreason());
		descIO.setDesctabl(tablesInner.t6692);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmdesc.set(descIO.getLongdesc());
		} else {
			sv.clmdesc.fill("?");
		}
		/* Read the Payment Method Description. */
		descIO.setDescitem(regpIO.getRgpymop());
		descIO.setDesctabl(tablesInner.t6694);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpyshort.set(descIO.getShortdesc());
		} else {
			sv.rgpyshort.fill("?");
		}
		/* Read the Frequency Code Description. */
		descIO.setDescitem(regpIO.getRegpayfreq());
		descIO.setDesctabl(tablesInner.t3590);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.frqdesc.set(descIO.getShortdesc());
		} else {
			sv.frqdesc.fill("?");
		}
		/* Read the Claim Currency Description. */
		descIO.setDescitem(regpIO.getCurrcd());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		} else {
			sv.clmcurdsc.fill("?");
		}
		/* Set up the Screen. */
		sv.payclt.set(regpIO.getPayclt());
		sv.rgpynum.set(regpIO.getRgpynum());
		sv.rgpystat.set(regpIO.getRgpystat());
		sv.cltype.set(regpIO.getPayreason());
		sv.claimevd.set(regpIO.getClaimevd());
		sv.rgpymop.set(regpIO.getRgpymop());
		sv.regpayfreq.set(regpIO.getRegpayfreq());
		// CML009
		sv.reasoncd.set(regpIO.getReasoncd());
		sv.netclaimamt.set(regpIO.getNetamt());
		sv.resndesc.set(regpIO.getReason());
		sv.adjustamt.set(regpIO.getAdjamt());
		wsaaFreq.set(regpIO.getRegpayfreq());
		wsaaFreq3.set(regpIO.getRegpayfreq());
		wsaaLastFreq.set(regpIO.getRegpayfreq());
		sv.prcnt.set(regpIO.getPrcnt());
		if (isNE(regpIO.getDestkey(), SPACES)) {
			sv.destkey.set(regpIO.getDestkey());
			/***** MOVE 'Y' TO S6677-REGPAYFREQ-OUT(PR) */
		}
		sv.pymt.set(regpIO.getPymt());
		sv.totamnt.set(regpIO.getTotamnt());
		sv.claimcur.set(regpIO.getCurrcd());

		setRegpDetalsCustomerSpecific1310();
		if ((isEQ(wsspcomn.flag, "M")) && (isNE(regpIO.getPayclt(), SPACES))) {
			wsaaPayclt.set(regpIO.getPayclt());
		}
		/* Check if Bank Details are required on the current Contract. */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(regpIO.getRgpymop());
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
				|| (isNE(regpIO.getRgpymop(), itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.rgpymopErr.set(errorsInner.g529);
			/* GO TO 2050-CHECK-PERCENTAGE <D509CS> */
			return;
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		if (isEQ(t6694rec.bankreq, "Y")) {
			if ((isEQ(regpIO.getBankkey(), SPACES)) || (isEQ(regpIO.getBankacckey(), SPACES))) {
				sv.ddind.set("X");
			} else {
				sv.ddind.set("+");
			}
		} else {
			if ((isEQ(regpIO.getBankkey(), SPACES)) && (isEQ(regpIO.getBankacckey(), SPACES))) {
				sv.ddind.set(SPACES);
			} else {
				sv.ddind.set("+");
			}
		}
	}
	protected void setRegpDetalsCustomerSpecific1310() {
		if (isEQ(wsspcomn.flag, "I")) {
			sv.aprvdate.set(regpIO.getAprvdate());
		} else {
			sv.aprvdate.set(varcom.vrcmMaxDate);
		}
		sv.crtdate.set(regpIO.getCrtdate());
		sv.revdte.set(regpIO.getRevdte());
		sv.firstPaydate.set(regpIO.getFirstPaydate());
		sv.lastPaydate.set(regpIO.getLastPaydate());
		sv.nextPaydate.set(regpIO.getNextPaydate());
		sv.anvdate.set(regpIO.getAnvdate());
		sv.finalPaydate.set(regpIO.getFinalPaydate());
		sv.recvdDate.set(regpIO.getRecvdDate());
		sv.incurdt.set(regpIO.getIncurdt());
		if (isEQ(wsspcomn.flag, "I")) {
			sv.cancelDate.set(regpIO.getCancelDate());
			sv.canceldateOut[varcom.nd.toInt()].set(" ");
		} else {
			sv.canceldateOut[varcom.nd.toInt()].set("Y");
		}
	}
	protected void bypassBankDetails1380() {
		sv.crtdateOut[varcom.pr.toInt()].set("Y");
		if (isLT(regpIO.getFirstPaydate(), datcon1rec.intDate)) {
			sv.fpaydateOut[varcom.pr.toInt()].set("Y");
		}
		/* EXIT */
	}

	protected void findNextRegp1400() {
		find1410();
	}

	protected void find1410() {
		SmartFileCode.execute(appVars, regpenqIO);
		if ((isNE(regpenqIO.getStatuz(), varcom.oK)) && (isNE(regpenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(regpenqIO.getParams());
			fatalError600();
		}
		if (isEQ(regpenqIO.getStatuz(), varcom.endp)) {
			return;
		}
		if ((isEQ(regpIO.getChdrcoy(), regpenqIO.getChdrcoy()))
				&& (isEQ(regpIO.getChdrnum(), regpenqIO.getChdrnum()))) {
			if (isGT(regpenqIO.getRgpynum(), wsaaNextRgpy)) {
				wsaaNextRgpy.set(regpenqIO.getRgpynum());
			}
		} else {
			regpenqIO.setStatuz(varcom.endp);
		}
		regpenqIO.setFunction(varcom.nextr);
	}

	protected void readT56711500() {
		read1510();
	}

	protected void read1510() {
		/* Set up the key for T5671 */
		wsaaT5671Trancd.set(wsaaBatckey.batcBatctrcde);
		wsaaT5671Crtable.set(regpIO.getCrtable());
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK)) && (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			scrnparams.errorCode.set(errorsInner.g520);
			wsspcomn.edterror.set("Y");
			itemIO.setGenarea(SPACES);
			return;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		index1.set(1);
		while (!((isGT(index1, 4)) || (isEQ(wsaaProg, t5671rec.pgm[index1.toInt()])))) {
			if (isNE(t5671rec.pgm[index1.toInt()], wsaaProg)) {
				index1.add(1);
			}
		}

		if (isEQ(t5671rec.pgm[index1.toInt()], wsaaProg)) {
			wsaaEdtitm.set(t5671rec.edtitm[index1.toInt()]);
		}
	}

	protected void accumCovr1600() {
		accum1610();
	}

	protected void accum1610() {
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(), varcom.oK)) && (isNE(covrIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if ((isNE(chdrrgpIO.getChdrcoy(), covrIO.getChdrcoy())) || (isNE(chdrrgpIO.getChdrnum(), covrIO.getChdrnum()))
				|| (isNE(regpIO.getLife(), covrIO.getLife())) || (isNE(regpIO.getCoverage(), covrIO.getCoverage()))
				|| (isNE(regpIO.getRider(), covrIO.getRider())) || (isEQ(covrIO.getStatuz(), varcom.endp))) {
			covrIO.setStatuz(varcom.endp);
			return;
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			covrIO.setFunction(varcom.nextr);
			return;
		}
		if (isEQ(regpIO.getCoverage(), covrIO.getCoverage())) {
			if (isGT(covrIO.getPayrseqno(), 0)) {
				wsaaPayrseqno.set(covrIO.getPayrseqno());
			} else {
				wsaaPayrseqno.set(1);
			}
		}
		wsaaLife.set(covrIO.getLife());
		wsaaJlife.set(covrIO.getJlife());
		wsaaPremCurrency.set(covrIO.getPremCurrency());
		wsaaCrtable.set(covrIO.getCrtable());
		wsaaCovrCrrcd.set(covrIO.getCrrcd());
		/* ADD COVR-SUMINS TO WSAA-SUMINS. */
		/* Add SUMINS only if record has valid flag of 1 */
		if (isEQ(covrIO.getValidflag(), 1)) {
			wsaaCovrInstprem.add(covrIO.getInstprem());
			calcCompTax();
			wsaaCovrInstprem.add(wsaaCompTax);
			wsaaSumins.add(covrIO.getSumins());
			if(CMRPY012Permission && isEQ(sv.cnttype, LPS) && isEQ(sv.crtable, LWPR)){
			wsbbSumins.add(covrIO.getSumins()); //ILIFE-8299
			}
		}
		covrIO.setFunction(varcom.nextr);
		/* Check coverage statii against T5679 */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatuz = "N";
		/* MOVE 1 TO WSAA-SUB. */
		wsaaStatcode.set(covrIO.getStatcode());
		wsaaPstcde.set(covrIO.getPstatcode());
		// ILIFE-1138 STARTS
		if ((isEQ(wsaaContStatcode, "RD") || isEQ(wsaaContStatcode, "DH")) && isEQ(wsaaContPstcde, "DH")) {
			readTy501Covrstat();
		}

		/* PERFORM UNTIL WSAA-SUB > 12 */
		/* IF T5679-COV-RISK-STAT(WSAA-SUB) NOT = SPACE */
		/* IF (T5679-COV-RISK-STAT(WSAA-SUB) = WSAA-STATCODE) AND */
		/* (T5679-COV-PREM-STAT(WSAA-SUB) = WSAA-PSTCDE) */
		/* MOVE 13 TO WSAA-SUB */
		/* MOVE 'Y' TO WSAA-VALID-STATUZ */
		/* GO TO 1690-EXIT */
		/* END-IF */
		/* END-IF */
		/* ADD 1 TO WSAA-SUB */
		/* END-PERFORM. */
		else {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12) || isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)) {
				if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
					for (wsaaSub.set(1); !(isGT(wsaaSub, 12) || isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)) {
						if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()], wsaaPstcde)) {
							wsaaValidStatuz = "Y";
						}
					}
				}
			}
		}
		// ILIFE-1138 ENDS
		if (isEQ(wsaaValidStatuz, "N")) {
			wsaaStopProcess = "Y";
		}
	}

	protected void calcCompTax() {
		/*
		 * ilife-3396 Improve the performance of Contract enquiry- TEN
		 * transaction
		 */
		wsaaCompTax.set(ZERO);
		taxdbilpf.setChdrcoy(covrIO.getChdrcoy().toString());
		taxdbilpf.setChdrnum(covrIO.getChdrnum().toString());
		taxdbilpf.setLife(covrIO.getLife().toString());
		taxdbilpf.setCoverage(covrIO.getCoverage().toString());
		taxdbilpf.setRider(covrIO.getRider().toString());
		taxdbilpf.setTrantype("PREM");
		taxdbilpf.setPlansfx(0);
		// taxdbilpf.setTranno(covrIO.getTranno().toInt());
		taxdbilpf.setInstfrom(covrIO.getCurrfrom().toInt());
		taxdbilpfList = taxdbilpfDAO.searchTaxdpfRecord(taxdbilpf);
		if (taxdbilpfList != null && taxdbilpfList.size() > 00) {
			for (Taxdpf taxdbilpf : taxdbilpfList) {
				if (taxdbilpf.getTxabsind01().equals("N")) {
					wsaaCompTax.add(taxdbilpf.getTaxamt01().doubleValue());
				}
				if (taxdbilpf.getTxabsind02().equals("N")) {
					wsaaCompTax.add(taxdbilpf.getTaxamt02().doubleValue());
				}
				if (taxdbilpf.getTxabsind03().equals("N")) {
					wsaaCompTax.add(taxdbilpf.getTaxamt03().doubleValue());
				}
			}
		}
	}

	// ILIFE-1138 STARTS
	protected void readTy501Covrstat() {
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(ty501);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(covrIO.getCrtable());
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK) && isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			ty501rec.ty501Rec.set(itemIO.getGenarea());
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12) || isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)) {
				if (isEQ(ty501rec.covRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
					for (wsaaSub.set(1); !(isGT(wsaaSub, 12) || isEQ(wsaaValidStatuz, "Y")); wsaaSub.add(1)) {
						if (isEQ(ty501rec.covPremStat[wsaaSub.toInt()], wsaaPstcde)) {
							wsaaValidStatuz = "Y";
						}
					}
				}
			}
		}
	}
	// ILIFE-1138 ENDS

	protected void createRegp1700() {
		create1710();
	}

	protected void create1710() {
		wsaaNextRgpy.set(ZERO);
		regpenqIO.setStatuz(varcom.oK);
		regpenqIO.setChdrcoy(regpIO.getChdrcoy());
		regpenqIO.setChdrnum(regpIO.getChdrnum());
		regpenqIO.setLife(ZERO);
		regpenqIO.setCoverage(ZERO);
		regpenqIO.setRider(ZERO);
		regpenqIO.setRgpynum(99999);
		regpenqIO.setFunction(varcom.begn);
		if (CMRPY005Permission) {
			// ICIL-556
			regpenqIO.setAdjamt(regpIO.getAdjamt());
			regpenqIO.setReason(regpIO.getReason());
			regpenqIO.setReasoncd(regpIO.getReasoncd());
			regpenqIO.setNetamt(regpIO.getNetamt());
		}

		while (!(isEQ(regpenqIO.getStatuz(), varcom.endp))) {
			findNextRegp1400();
		}

		setPrecision(regpIO.getRgpynum(), 0);
		regpIO.setRgpynum(add(wsaaNextRgpy, 1));
		if (isNE(t5606rec.benfreq, SPACES)) {
			if (isEQ(wsaaIsWop, "N")) {
				sv.regpayfreq.set(t5606rec.benfreq);
				wsaaFreq.set(t5606rec.benfreq);
				wsaaFreq3.set(t5606rec.benfreq);
				wsaaLastFreq.set(t5606rec.benfreq);
			} else {
				sv.regpayfreq.set(wsaaPayrBillfreq);
				wsaaFreq.set(wsaaPayrBillfreq);
				wsaaFreq3.set(wsaaPayrBillfreq);
				wsaaLastFreq.set(wsaaPayrBillfreq);
			}
			/* MOVE T5606-BENFREQ TO DESC-DESCITEM */
			descIO.setDescitem(sv.regpayfreq);
			descIO.setDesctabl(tablesInner.t3590);
			findDesc1100();
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.frqdesc.set(descIO.getShortdesc());
			} else {
				sv.frqdesc.fill("?");
			}
		} else {
			sv.regpayfreqErr.set(errorsInner.g528);
			wsspcomn.edterror.set("Y");
		}
		sv.crtdate.set(datcon1rec.intDate);
		sv.claimcur.set(chdrrgpIO.getCntcurr());
		sv.rgpynum.set(regpIO.getRgpynum());
		if (isNE(covrIO.getPremCurrency(), SPACES)) {
			descIO.setDescitem(covrIO.getPremCurrency());
			descIO.setDesctabl(tablesInner.t3629);
			findDesc1100();
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.currds.set(descIO.getShortdesc());
			} else {
				sv.currds.fill("?");
			}
		}
		/* Read the Claim Currency Description. */
		descIO.setDescitem(chdrrgpIO.getCntcurr());
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		} else {
			sv.clmcurdsc.fill("?");
		}
		/* Read the Claim Status Description. */
		sv.rgpystat.set(wsaaRgpystat);
		descIO.setDescitem(wsaaRgpystat);
		/* MOVE T6663 TO DESC-DESCTABL. */
		descIO.setDesctabl(tablesInner.t5400);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.statdsc.set(descIO.getShortdesc());
		} else {
			sv.statdsc.fill("?");
		}
		/* Read the Defualt Claim Type Description. */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t6690);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(covrIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK)) && (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
			return;
		}
		t6690rec.t6690Rec.set(itemIO.getGenarea());
		descIO.setDescitem(t6690rec.rgpytype);
		descIO.setDesctabl(tablesInner.t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		} else {
			sv.rgpytypesd.fill("?");
		}
	}

	protected void readT6617() {
		Itempf itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), t6617, t517);/* IJTI-1523 */
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T6617").concat(t517));
			fatalError600();
		}

		t6617rec.t6617Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		sv.intstrate.set(t6617rec.intRate);

	}

	protected void readT56451800() {
		start1810();
	}

	protected void start1810() {
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK) && isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(errorsInner.h134);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

	protected void checkForAnnuity1900() {
		starts1900();
	}

	protected void starts1900() {
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6625);
		itdmIO.setItemitem(sv.crtable);
		itdmIO.setItmfrm(sv.crtdate);
		itdmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK) && isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company) || isNE(itdmIO.getItemtabl(), tablesInner.t6625)
				|| isNE(itdmIO.getItemitem(), sv.crtable) || isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sv.anntind.set(" ");
			sv.anntindOut[varcom.pr.toInt()].set("Y");
		} else {
			wsaaAnnuity = "Y";
			t6625rec.t6625Rec.set(itdmIO.getGenarea());
		}
	}

	protected void largename() {
		/* LGNM-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/* LGNM-EXIT */
	}

	protected void plainname() {
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/* PLAIN-EXIT */
	}

	protected void payeename() {
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/* PAYEE-EXIT */
	}

	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/* CORP-EXIT */
	}

	/**
	 * <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	 * </pre>
	 */
	protected void preScreenEdit() {
		try {
			preStart();
			screenio2015();
		} catch (GOTOException e) {
			/* Expected exception for control flow purposes. */
		}
	}

	protected void preStart() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		protectScr2410CustomerSpecific();
		if (isEQ(wsspcomn.flag, "I")) {
			protectScr2400();
		}
		if (isNE(wsspcomn.flag, "C") && isNE(wsaaAccidentClaimInd, "Y")) {
			sv.agclmstzOut[varcom.pr.toInt()].set("Y");
			sv.agclmstz.set(SPACES);
			if (isEQ(wsspcomn.flag, "M")) {
				sv.agclmstzOut[varcom.pr.toInt()].set("N");
			}
		}
		/* If the payment has not yet started, ie. no date in the Last */
		/* Paid Date field. then the Next Payment Date will be protected. */
		/* This means that before the first payment has been made the Next */
		/* Payment Date will be protected, but the user will be able to */
		/* influence the Next Payment Date by changing the First Payment */
		/* Date. Once payment has started the user will have the option of */
		/* directly changing the Next Payment Date. */
		if (isEQ(sv.lastPaydate, varcom.vrcmMaxDate)) {
			sv.npaydateOut[varcom.pr.toInt()].set("Y");
		}
		/* Protect Destination Key at all times, but if allowing destinatio */
		/* other than current contract this code can be removed. */
		/* MOVE 'Y' TO S6677-DESTKEY-OUT(PR). */
		if (isEQ(wsaaAnnuity, "N")) {
			sv.destkeyOut[varcom.pr.toInt()].set("Y");
		}
		/* If invalid coverage status is discovered, output error message */
		/* and protect the screen. */
		if (isEQ(wsaaStopProcess, "Y")) {
			scrnparams.errorCode.set(errorsInner.h136);
			scrnparams.function.set(varcom.prot);
			wsspcomn.edterror.set("Y");
		}
		/* If frequency override is set to 'N' no manual changes are */
		/* allowed to the frequency. Read T6696 to get this info. Note */
		/* this is not done in register as there is no REGP data. */
		/* IF WSSP-FLAG = 'C' <D509CS> */
		/* GO TO 2015-SCREENIO <D509CS> */
		/* END-IF. <D509CS> */
		/* <D509CS> */
		/* MOVE COVR-CRTABLE TO WSAA-T6696-CRTABLE. <D509CS> */
		/* MOVE REGP-PAYREASON TO WSAA-T6696-CLTYPE. <D509CS> */
		/* <D509CS> */
		/* MOVE SPACES TO ITDM-DATA-KEY. <D509CS> */
		/* MOVE WSSP-COMPANY TO ITDM-ITEMCOY. <D509CS> */
		/* MOVE ITDMREC TO ITDM-FORMAT. <D509CS> */
		/* MOVE T6696 TO ITDM-ITEMTABL. <D509CS> */
		/* MOVE 'IT' TO ITDM-ITEMPFX. <D509CS> */
		/* MOVE WSAA-T6696-KEY TO ITDM-ITEMITEM. <D509CS> */
		/* MOVE CHDRRGP-OCCDATE TO ITDM-ITMFRM. <D509CS> */
		/* MOVE BEGN TO ITDM-FUNCTION. <D509CS> */
		/* <D509CS> */
		/* CALL 'ITDMIO' USING ITDM-PARAMS. <D509CS> */
		/* <D509CS> */
		/* IF (ITDM-STATUZ NOT = O-K) AND <D509CS> */
		/* (ITDM-STATUZ NOT = ENDP) <D509CS> */
		/* MOVE ITDM-PARAMS TO SYSR-PARAMS <D509CS> */
		/* PERFORM 600-FATAL-ERROR <D509CS> */
		/* END-IF. <D509CS> */
		/* <D509CS> */
		/* IF (WSSP-COMPANY NOT = ITDM-ITEMCOY) OR <D509CS> */
		/* (T6696 NOT = ITDM-ITEMTABL) OR <D509CS> */
		/* (WSAA-T6696-KEY NOT = ITDM-ITEMITEM) OR <D509CS> */
		/* (ITDM-STATUZ = ENDP) <D509CS> */
		/* MOVE SPACE TO ITDM-GENAREA <D509CS> */
		/* MOVE G522 TO S6677-CLTYPE-ERR <D509CS> */
		/* MOVE 'Y' TO WSSP-EDTERROR <D509CS> */
		/* GO TO 2090-EXIT <D509CS> */
		/* ELSE <D509CS> */
		/* MOVE ITDM-GENAREA TO T6696-T6696-REC <D509CS> */
		/* END-IF. <D509CS> */
		/* <D509CS> */
		/* IF T6696-FRQORIDE = 'N' <D509CS> */
		/* MOVE 'Y' TO S6677-REGPAYFREQ-OUT<D509CS> */
		/* END-IF. <D509CS> */
		if (isEQ(wsspcomn.flag, "C")) {
			/* NEXT_SENTENCE */
		} else {
			wsaaT6696Crtable.set(covrIO.getCrtable());
			wsaaT6696Cltype.set(regpIO.getPayreason());
			itdmIO.setDataKey(SPACES);
			itdmIO.setItemcoy(wsspcomn.company);
			itdmIO.setFormat(formatsInner.itdmrec);
			itdmIO.setItemtabl(tablesInner.t6696);
			itdmIO.setItempfx("IT");
			itdmIO.setItemitem(wsaaT6696Key);
			itdmIO.setItmfrm(chdrrgpIO.getOccdate());
			itdmIO.setFunction(varcom.begn);
			// performance improvement -- Niharika Modi
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

			SmartFileCode.execute(appVars, itdmIO);
			if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t6696, itdmIO.getItemtabl()))
					|| (isNE(wsaaT6696Key, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
				itdmIO.setGenarea(SPACES);
				sv.cltypeErr.set(errorsInner.g522);
				wsspcomn.edterror.set("Y");
			} else {
				t6696rec.t6696Rec.set(itdmIO.getGenarea());
				if (isEQ(t6696rec.frqoride, "N")) {
					sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
				}
			}
		}
		// CML009
		if (CMRPY005Permission) {
			sv.adjustamtOut[varcom.nd.toInt()].set("N");
			sv.netclaimamtOut[varcom.nd.toInt()].set("N");
			sv.reasoncdOut[varcom.nd.toInt()].set("N");
			sv.resndescOut[varcom.nd.toInt()].set("N");
			sv.pymtAdjOut[varcom.nd.toInt()].set("N");
		} else {
			sv.adjustamtOut[varcom.nd.toInt()].set("Y");
			sv.netclaimamtOut[varcom.nd.toInt()].set("Y");
			sv.reasoncdOut[varcom.nd.toInt()].set("Y");
			sv.resndescOut[varcom.nd.toInt()].set("Y");
			sv.pymtAdjOut[varcom.nd.toInt()].set("Y");
		}
		//fwang3
		if (cmoth003permission) {
			sv.showFlag.set("Y");
		} else {
			sv.showFlag.set("N");
		}
		
	}
	
	protected void screenio2015() {
		return;
	}

	protected void screenEdit2000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					validate2020();
					checkPayment2030();
					checkCurrency2035();
					checkFreqency2040();
					checkReasonDesc2045();
				case validatePayee2045:
					validatePayee2045();
				case checkPercentage2050:
					checkPercentage2050();
					if (CMRPY005Permission) {
						checkAdjustment2055();
					}
				case checkDestination2060:
					checkDestination2060();
					checkNextDate2069();
					checkReviewDate2075();
				case noReviewTerm2076:
					noReviewTerm2076();
				case checkPayDate2077:
					checkPayDate2077();
				case validateFollow2079:
					if(!(CMRPY012Permission && isEQ(sv.cnttype, LPS) && isEQ(sv.crtable, LWPR))){
						validateFollow2079();
					}else{
						validateAusFollow2079();//ILIFE-8299						
					}					
					checkLottery();
				case checkForErrors2080:
					checkForErrors2080();
				case exit2090:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	private void checkReasonDesc2045() {
		/* Read the Premium Status Description. */
		if (isNE(sv.reasoncd, SPACES)) {
			if (isEQ(sv.resndesc, SPACES)) {
				descIO.setDescitem(sv.reasoncd);
				descIO.setDesctabl(tablesInner.t5500);
				findDesc1100();
				if (isEQ(descIO.getStatuz(), varcom.oK)) {
					sv.resndesc.set(descIO.getLongdesc());
				} else {
					sv.resndesc.fill("?");
				}
			}
		}
	}

	/**
	 * check adjustment
	 */
	protected void checkAdjustment2055() {
		double temp = sv.pymt.getbigdata().doubleValue() + sv.adjustamt.getbigdata().doubleValue();
		sv.netclaimamt.set(temp);
		if (temp < 0) {
			// if the net claim amount is not followed regular rule,
			// then display error messages.
			sv.netclaimamtOut[varcom.pr.toInt()].set("Y");
			sv.netClaimErr.set(errorsInner.rrkv);
		}
	}

	protected void screenIo2010() {
		/* CALL 'S6677IO' USING SCRN-SCREEN-PARAMS */
		/* S6677-DATA-AREA. */
		/* Screen errors are now handled in the calling program. */
		/* PERFORM 200-SCREEN-ERRORS. */
		wsspcomn.edterror.set(varcom.oK);
		sv.zrsumin.set(wsaaSumins2);
		if(isGTE(sv.pymt, wsaaTempForClaimValid)) {
			sv.pymt.set(wsaaTempForClaimValid);
		}
	}

	protected void validate2020() {
		/* If Stop Processing flag has been set by an invalid coverage stat */
		/* protect the entire screen AND ALLOW NO FURTHER PROCESSING */
		if (isEQ(wsaaStopProcess, "Y")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Validate fields */
		/* If the user is working with annuities and has changed the */
		/* RGPYMOP (or filled it in, in create mode) check wether */
		/* to protect DESTKEY by looking at T6694: */
		if (isEQ(wsaaAnnuity, "Y") && isNE(wsaaRgpymop, sv.rgpymop)) {
			checkT66942600();
		}
		if ((isNE(scrnparams.statuz, varcom.calc)) && (isNE(scrnparams.statuz, varcom.oK))) {
			scrnparams.errorCode.set(errorsInner.curs);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag, "I")) {
			/* Even in enquiry mode, we must validate the bank account */
			/* flag field. */
			if ((isNE(sv.ddind, "+") && isNE(sv.ddind, "X") && isNE(sv.ddind, SPACES))) {
				sv.ddindErr.set(errorsInner.h118);
			}
			/* If user selects bank details and there are none, put out error */
			if ((isEQ(sv.ddind, "X") && isEQ(regpIO.getBankkey(), SPACES))) {
				sv.ddindErr.set(errorsInner.e493);
				// ILIFE-2073-STARTS
				sv.ddind.set(SPACES);
				// ILIFE-2073-ENDS
			}
			goTo(GotoLabel.checkForErrors2080);
			/***** GO TO 2090-EXIT */
		}
		if (cmoth003permission) {
			if(isNE(sv.notifino,SPACES) && isEQ(wsspcomn.flag, "C")){
				notipf = notipfDAO.getNotiReByNotifin(sv.notifino.toString().replace(CLMPREFIX, ""),wsspcomn.company.toString());
				if(notipf != null && isEQ(notipf.getIncidentt().trim(),RP)){
					if(isNE(notipf.getIncurdate(),SPACES) && isNE(notipf.getRclaimreason(),SPACES) && !entryFlag ){
							entryFlag = true;
							sv.incurdt.set(notipf.getIncurdate());
							sv.cltype.set(notipf.getRclaimreason());
					}
				}
				
				else{
				sv.notifinoErr.set(errorsInner.rrsn);
				}
			}
		}
		
		/* Validate Reason Code. */
		if (isEQ(sv.cltype, SPACES)) {
			sv.cltypeErr.set(errorsInner.e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.reasoncd,SPACES) && isEQ(wsspcomn.flag, "M")) {
			sv.adjustamtErr.set(errorsInner.e186);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setItemtabl(tablesInner.t6692);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(sv.cltype);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK)) && (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.cltypeErr.set(errorsInner.g521);
			itemIO.setGenarea(SPACES);
		}
		t6692rec.t6692Rec.set(itemIO.getGenarea());
		/* Read T6696 */
		wsaaT6696Crtable.set(covrIO.getCrtable());
		wsaaT6696Cltype.set(sv.cltype);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6696);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6696Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t6696, itdmIO.getItemtabl()))
				|| (isNE(wsaaT6696Key, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.cltypeErr.set(errorsInner.g522);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		t6696rec.t6696Rec.set(itdmIO.getGenarea());
		descIO.setDescitem(sv.cltype);
		descIO.setDesctabl(tablesInner.t6692);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmdesc.set(descIO.getLongdesc());
		} else {
			sv.clmdesc.fill("?");
		}
		/* Read the Claim Type Details. */
		descIO.setDescitem(t6692rec.rgpytype);
		descIO.setDesctabl(tablesInner.t6691);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpytypesd.set(descIO.getShortdesc());
		} else {
			sv.rgpytypesd.fill("?");
		}
		validate2021CustomerSPecific();
		/* Valid Claim Receive Date and Incur Date */
		if (isEQ(sv.recvdDate, varcom.vrcmMaxDate) || isEQ(sv.recvdDate, ZERO)) {
			sv.zclmrecdErr.set(errorsInner.f665);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.incurdt, varcom.vrcmMaxDate) || isEQ(sv.incurdt, ZERO)) {
			sv.incurdtErr.set(errorsInner.f665);
			wsspcomn.edterror.set("Y");
		}
		if (isLT(sv.recvdDate, sv.incurdt)) {
			sv.zclmrecdErr.set(errorsInner.p132);
			wsspcomn.edterror.set("Y");
		}
		//ILJ-48 Starts
		if(!cntDteFlag) {

			if (isLT(sv.recvdDate, wsaaCovrCrrcd)) {
				sv.zclmrecdErr.set(errorsInner.t058);
				wsspcomn.edterror.set("Y");
			}
			
			if (isLT(sv.incurdt, wsaaCovrCrrcd)) {
				sv.incurdtErr.set(errorsInner.t058);
				wsspcomn.edterror.set("Y");
			}
		} else if(isNE(sv.incurdt,varcom.vrcmMaxDate)) {
			
			if (isLT(sv.incurdt, sv.riskcommdte)) {
				sv.incurdtErr.set(errorsInner.t058);
				wsspcomn.edterror.set("Y");
			}
			
			if(isGTE(sv.incurdt, sv.riskcommdte) && isLT(sv.incurdt,sv.occdate)) {
				scrnparams.errorCode.set(errorsInner.jl25);
			}
		}
		//ILJ-48 End
		//ILIFE-8685 starts
		if(isNE(t5606rec.waitperiods,SPACES)) {
			datcon2rec.freqFactor.set(t5606rec.waitperiod01.trim());
			datcon2rec.frequency.set("DY");
			datcon2rec.intDate1.set(sv.occdate);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon2rec.statuz);
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			if(isLTE(datcon1rec.intDate, datcon2rec.intDate2)) {
				sv.incurdtErr.set(errorsInner.rfuy);
				wsspcomn.edterror.set("Y");
			}
		}
		//ILIFE-8685 ends
		if (isNE(sv.incurdtErr, SPACES) || isNE(sv.zclmrecdErr, SPACES)) {
			goTo(GotoLabel.exit2090);
		}
	}

	/**
	 * <pre>
	* Validate Payment Method.
	 * </pre>
	 */
	protected void checkPayment2030() {
		if (isEQ(sv.rgpymop, SPACES)) {
			sv.rgpymopErr.set(errorsInner.e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.rgpymop);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
				|| (isNE(sv.rgpymop, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.rgpymopErr.set(errorsInner.g529);
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		/* Get Method of Payment Description */
		descIO.setDescitem(sv.rgpymop);
		descIO.setDesctabl(tablesInner.t6694);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rgpyshort.set(descIO.getShortdesc());
		} else {
			sv.rgpyshort.fill("?");
		}
	}

	/**
	 * <pre>
	* Validate Claim Currency.
	 * </pre>
	 */
	protected void checkCurrency2035() {
		if (isEQ(sv.claimcur, SPACES)) {
			sv.claimcur.set(chdrrgpIO.getCntcurr());
		}
		descIO.setDescitem(sv.claimcur);
		descIO.setDesctabl(tablesInner.t3629);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.clmcurdsc.set(descIO.getShortdesc());
		} else {
			sv.clmcurdsc.fill("?");
		}
	}

	/**
	 * <pre>
	* Validate Frequency
	 * </pre>
	 */
	protected void checkFreqency2040() {
		if (isEQ(sv.regpayfreq, SPACES)) {
			sv.regpayfreqErr.set(errorsInner.e186);
			goTo(GotoLabel.validatePayee2045);
		}
		/* If the user has changed the frequency we will read T6696 */
		/* to find out whether the frequency is changeable (If */
		/* FRQORIDE = N/Y). */
		/* If it is not allowed to change the frequency and the frequency */
		/* on the screen is different from the frequency on T5606 */
		/* move the frequency from the table to the frequency */
		/* on the screen and issue an error message. */
		/* IF WSAA-FREQ NOT = S6677-REGPAYFREQ */
		if (isNE(wsaaLastFreq, sv.regpayfreq)) {
			chdrenqIO.setChdrnum(sv.chdrnum);
			checkContractNo2100();
			if (isEQ(sv.cltype, SPACES)) {
				goTo(GotoLabel.validatePayee2045);
			}
			if ((isEQ(t6696rec.frqoride, "N"))) {
				if ((isEQ(t6694rec.contreq, "Y")) && (isNE(chdrenqIO.getBillfreq(), sv.regpayfreq))) {
					sv.regpayfreq.set(chdrenqIO.getBillfreq());
					sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
					wsaaFreq2.set(sv.regpayfreq);
					wsaaFreq.set(sv.regpayfreq);
					wsaaSumins2.set(ZERO);
				} else {
					if (isEQ(wsaaIsWop, "N")) {
						if ((isNE(t5606rec.benfreq, sv.regpayfreq))) {
							sv.regpayfreq.set(t5606rec.benfreq);
							sv.regpayfreqErr.set(errorsInner.g514);
							wsspcomn.edterror.set("Y");
						}
					} else {
						if (isNE(wsaaPayrBillfreq, sv.regpayfreq)) {
							sv.regpayfreq.set(wsaaPayrBillfreq);
							sv.regpayfreqErr.set(errorsInner.g514);
							wsspcomn.edterror.set("Y");
						}
					}
				}
			}
			if (isEQ(t6696rec.frqoride, "Y")) {
				wsaaFreq2.set(sv.regpayfreq);
				wsaaFreq.set(sv.regpayfreq);
				wsaaLastFreq.set(sv.regpayfreq);
				wsaaSumins2.set(ZERO);
			}
			/* 1 replaces 0 in the computation for a frequency of '00' */
			if (isEQ(wsaaFreq3, 0)) {
				wsaaFreq3.set(1);
			}
			if (isEQ(wsaaFreq2, 0)) {
				wsaaFreq2.set(1);
			}
			compute(wsaaSumins2, 2).set((div((mult(wsaaSumins, wsaaFreq3)), wsaaFreq2)));
			zrdecplrec.amountIn.set(wsaaSumins2);
			a000CallRounding();
			wsaaSumins2.set(zrdecplrec.amountOut);
			/* MOVE WSAA-SUMINS2 TO S6677-SUMINS */
			/* MOVE WSAA-SUMINS2 TO S6677-SUMIN <004> */
			sv.zrsumin.set(wsaaSumins2);
		} else {
			chdrenqIO.setChdrnum(sv.chdrnum);
			checkContractNo2100();
			/* IF (T6694-CONTREQ = 'Y') AND */
			/* (CHDRENQ-BILLFREQ NOT = S6677-REGPAYFREQ) */
			if (isEQ(t6696rec.frqoride, "N") && isEQ(t6694rec.contreq, "Y")) {
				/* ILIFE-917 start sgadkari */
				/*
				 * if ((isNE(chdrenqIO.getBillfreq(), sv.regpayfreq) &&
				 * isEQ(wsspcomn.flag, "C"))) {
				 * sv.regpayfreq.set(chdrenqIO.getBillfreq()); }
				 */
				/* ILIFE-917 End */

				wsaaFreq2.set(sv.regpayfreq);
				wsaaFreq.set(sv.regpayfreq);
				wsaaSumins2.set(ZERO);
				/* 1 replaces 0 in the computation for a frequency of '00' */
				if (isEQ(wsaaFreq3, 0)) {
					wsaaFreq3.set(1);
				}
				if (isEQ(wsaaFreq2, 0)) {
					wsaaFreq2.set(1);
				}
				compute(wsaaSumins2, 2).set((div((mult(wsaaSumins, wsaaFreq3)), wsaaFreq2)));
				zrdecplrec.amountIn.set(wsaaSumins2);
				a000CallRounding();
				wsaaSumins2.set(zrdecplrec.amountOut);
				/* MOVE WSAA-SUMINS2 TO S6677-SUMINS */
				/* MOVE WSAA-SUMINS2 TO S6677-SUMIN <004> */
				sv.zrsumin.set(wsaaSumins2);
			} else {
				wsaaFreq.set(sv.regpayfreq);
				wsaaFreq2.set(sv.regpayfreq);
				if (isEQ(wsaaFreq2, ZERO)) {
					wsaaFreq2.set(1);
				}
			}
		}
		/* Read the frequency code description. */
		descIO.setDescitem(sv.regpayfreq);
		descIO.setDesctabl(tablesInner.t3590);
		findDesc1100();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.frqdesc.set(descIO.getShortdesc());
		} else {
			sv.frqdesc.fill("?");
		}
	}

	/**
	 * <pre>
	* Validate payee client.
	 * </pre>
	 */
	protected void validatePayee2045() {
		if (isEQ(sv.rgpymop, SPACES)) {
			goTo(GotoLabel.checkPercentage2050);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.rgpymop);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
				|| (isNE(sv.rgpymop, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setGenarea(SPACES);
			sv.rgpymopErr.set(errorsInner.g529);
			goTo(GotoLabel.checkPercentage2050);
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		if (isEQ(t6694rec.payeereq, "Y")) {
			if (isEQ(sv.payclt, SPACES)) {
				sv.payclt.set(sv.cownnum);
			}
		} else {
			if (isNE(sv.payclt, SPACES)) {
				sv.paycltErr.set(errorsInner.e492);
			}
		}
		if (isEQ(sv.payclt, SPACES)) {
			sv.payenme.set(SPACES);
		}
		if (isEQ(t6694rec.payeereq, "Y")) {
			if (isEQ(sv.paycltErr, SPACES)) {
				cltsIO.setClntnum(sv.payclt);
				getClientDetails1200();
				if (isEQ(cltsIO.getStatuz(), varcom.mrnf) || isNE(cltsIO.getValidflag(), 1)) {
					sv.paycltErr.set(errorsInner.e335);
					sv.payenme.set(SPACES);
				} else {
					plainname();
					sv.payenme.set(wsspcomn.longconfname);
				}
			}
		}
	}

	/**
	 * <pre>
	* Validate Percentage.
	 * </pre>
	 */
	protected void checkPercentage2050() {
		/* Check that we will not get to divide by zero. */
		if (isEQ(wsaaSumins, ZERO)) {
			sv.pymt.set(ZERO);
			sv.prcnt.set(ZERO);
			/* ERROR MESSAGE : Sum Insured on Component is Zero. */
			scrnparams.errorCode.set(errorsInner.g516);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkDestination2060);
		}
		if (isNE(sv.cltypeErr, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkDestination2060);
		}
		if (isNE(sv.pymt, ZERO)) {
			zrdecplrec.amountIn.set(sv.pymt);
			a000CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.pymt)) {
				sv.pymtErr.set(errorsInner.rfik);
			}
		}
		if (isEQ(sv.pymt, ZERO)) {
			sv.prcnt.set(t6696rec.dfclmpct);
		} else {
			/* COMPUTE WSAA-PRCNT = ((S6677-PYMT / WSAA-SUMINS2) * 100) */
			if (isEQ(sv.regpayfreq, "00")) {
				compute(wsaaIntermed, 6).setRounded(div(sv.pymt, wsaaSumins2));
				compute(wsaaPrcnt, 6).setRounded(mult(wsaaIntermed, 100));
				sv.prcnt.set(wsaaPrcnt);
				if (CMRPY005Permission) {
					checkAdjustment2055();
				}
				goTo(GotoLabel.checkDestination2060);
			}
			compute(wsaaIntermed, 6).setRounded(div(sv.pymt, wsaaSumins2));
			compute(wsaaPrcnt, 6).setRounded(mult(wsaaIntermed, 100));
			sv.prcnt.set(wsaaPrcnt);
		}
		/* Allow to exceed max percentage if waive itself. */
		if (isGT(sv.prcnt, t6696rec.mxovrpct)) {
			if (isEQ(wsaaIsWop, "Y") && isEQ(tr517rec.zrwvflg01, "Y")) {
				/* NEXT_SENTENCE */
			} else {
				if (isEQ(sv.pymt, ZERO)) {
					sv.prcntErr.set(errorsInner.g515);
				} else {
					sv.pymtErr.set(errorsInner.g515);
				}
			}
		}
		if (isLT(sv.prcnt, t6696rec.mnovrpct)) {
			if (isEQ(sv.pymt, ZERO)) {
				sv.prcntErr.set(errorsInner.g517);
			} else {
				sv.pymtErr.set(errorsInner.g517);
			}
		}
		if ((isEQ(sv.pymt, ZERO)) && (isEQ(sv.pymtErr, SPACES)) && (isEQ(sv.prcntErr, SPACES))) {
			compute(sv.pymt, 2).set((div((mult(wsaaSumins2, sv.prcnt)), 100)));
			if (isEQ(wsaaIsWop, "Y") && isEQ(tr517rec.zrwvflg01, "Y")) {
				compute(sv.pymt, 2).set(add(sv.pymt, (div(mult(wsaaCovrInstprem, wsaaPayrBillfreqNum), wsaaFreq2))));
				zrdecplrec.amountIn.set(sv.pymt);
				a000CallRounding();
				sv.pymt.set(zrdecplrec.amountOut);
				compute(wsaaIntermed, 6).setRounded(div(sv.pymt, wsaaSumins2));
				compute(wsaaPrcnt, 6).setRounded(mult(wsaaIntermed, 100));
				sv.prcnt.set(wsaaPrcnt);
				/**** IF S6677-PRCNT > T6696-MXOVRPCT <LA2110> */
				/**** IF S6677-PYMT = ZEROES <LA2110> */
				/**** MOVE G515 TO S6677-PRCNT-ERR <LA2110> */
				/**** ELSE <LA2110> */
				/**** MOVE G515 TO S6677-PYMT-ERR <LA2110> */
				/**** END-IF <LA2110> */
				/**** END-IF <LA2110> */
			}
		}
	}

	/**
	 * <pre>
	* Validate Destination.
	 * </pre>
	 */
	protected void checkDestination2060() {
		/* IF T6694-CONTREQ = 'Y' */
		/* MOVE S6677-CHDRNUM TO S6677-DESTKEY */
		/* ELSE */
		/* MOVE SPACES TO S6677-DESTKEY */
		/* END-IF. */
		if (isEQ(t6694rec.contreq, "Y")) {
			if (isEQ(sv.destkey, SPACES)) {
				sv.destkey.set(sv.chdrnum);
			}
		} else {
			sv.destkey.set(SPACES);
		}
		/* The following code is not currently required in this version of */
		/* the program as the DESTKEY is defaulted to the Contract No.; */
		/* But should it be required to allow payment of waivers to */
		/* other contracts, the validation is already here. */
		if (isEQ(t6694rec.contreq, "Y")) {
			if (isEQ(sv.destkey, SPACES)) {
				sv.destkeyErr.set(errorsInner.e186);
			}
		} else {
			if (isNE(sv.destkey, SPACES)) {
				sv.destkeyErr.set(errorsInner.e492);
			}
		}
		if ((isEQ(t6694rec.contreq, "Y")) && (isEQ(sv.destkeyErr, SPACES))) {
			chdrenqIO.setChdrnum(sv.destkey);
			checkContractNo2100();
		}
		/* Validate Registration Date. */
		/* Should not be greater than Today */
		/* Should not be less than risk commencement day. */
		if ((isEQ(sv.crtdate, varcom.vrcmMaxDate)) || (isEQ(sv.crtdate, SPACES))) {
			sv.crtdateErr.set(errorsInner.e186);
			goTo(GotoLabel.validateFollow2079);
		} else {
			if (isGT(sv.crtdate, datcon1rec.intDate)) {
				sv.crtdateErr.set(errorsInner.f073);
			} else {
				if (isLT(sv.crtdate, sv.occdate)) {
					sv.crtdateErr.set(errorsInner.f616);
				}
			}
		}
		datcon2rec.freqFactor.set(ZERO);
		/* Validate first payment date. */
		if (isEQ(sv.firstPaydate, varcom.vrcmMaxDate)) {
			if (isNE(sv.cltype, SPACES)) {
				if (isEQ(wsaaIsWop, "Y")) {
					sv.firstPaydate.set(payrIO.getBillcd());
				} else {
					if (isNE(t6696rec.dfdefprd, ZERO)) {
						datcon2rec.freqFactor.set(t6696rec.dfdefprd);
						/* MOVE T6696-FREQCY-01 TO DTC2-FREQUENCY */
						datcon2rec.frequency.set(t6696rec.freqcy01);
						/****
						 * MOVE S6677-REGPAYFREQ TO DTC2-FREQUENCY <LA2110>
						 */
					} else {
						if (isNE(t5606rec.dfrprd, ZERO)) {
							datcon2rec.freqFactor.set(t5606rec.dfrprd);
							datcon2rec.frequency.set("12");
						}
					}
					if (isNE(datcon2rec.freqFactor, ZERO)) {
						datcon2rec.intDate2.set(ZERO);
						datcon2rec.intDate1.set(sv.crtdate);
						callProgram(Datcon2.class, datcon2rec.datcon2Rec);
						if (isNE(datcon2rec.statuz, varcom.oK)) {
							syserrrec.params.set(datcon2rec.datcon2Rec);
							syserrrec.statuz.set(datcon2rec.statuz);
							fatalError600();
						}
						sv.firstPaydate.set(datcon2rec.intDate2);
					} else {
						sv.firstPaydate.set(sv.crtdate);
					}
				}
			}
		} else {
			if (isNE(sv.cltype, SPACES)) {
				if ((isNE(t6696rec.mndefprd, ZERO)) && (isNE(t6696rec.freqcy02, SPACES))) {
					/* (S6677-REGPAYFREQ NOT = SPACES) <LA2110> */
					datcon2rec.freqFactor.set(t6696rec.mndefprd);
					/* MOVE T6696-FREQCY-02 TO DTC2-FREQUENCY */
					datcon2rec.frequency.set(t6696rec.freqcy02);
					/* MOVE S6677-REGPAYFREQ TO DTC2-FREQUENCY <LA2110> */
					datcon2rec.intDate2.set(ZERO);
					datcon2rec.intDate1.set(sv.crtdate);
					callProgram(Datcon2.class, datcon2rec.datcon2Rec);
					if (isNE(datcon2rec.statuz, varcom.oK)) {
						syserrrec.params.set(datcon2rec.datcon2Rec);
						syserrrec.statuz.set(datcon2rec.statuz);
						fatalError600();
					}
					if (isGT(datcon2rec.intDate2, sv.firstPaydate)) {
						sv.fpaydateErr.set(errorsInner.g518);
					}
				} else {
					if (isLT(sv.firstPaydate, sv.crtdate)) {
						sv.fpaydateErr.set(errorsInner.h132);
					}
				}
			} else {
				if (isLT(sv.firstPaydate, sv.crtdate)) {
					sv.fpaydateErr.set(errorsInner.h132);
				}
			}
		}
	}

	/**
	 * <pre>
	* If Last Paid Date is not Max Date then validate the Next
	* Payment Date.
	 * </pre>
	 */
	protected void checkNextDate2069() {
		if (isNE(sv.lastPaydate, varcom.vrcmMaxDate)) {
			if ((isEQ(sv.nextPaydate, varcom.vrcmMaxDate)) || (isEQ(sv.nextPaydate, ZERO))) {
				sv.npaydateErr.set(errorsInner.g540);
			}
			if (isLTE(sv.nextPaydate, sv.lastPaydate)) {
				sv.npaydateErr.set(errorsInner.g537);
			}
			if (isGT(sv.nextPaydate, sv.revdte)) {
				sv.npaydateErr.set(errorsInner.g538);
			}
			if (isGT(sv.nextPaydate, sv.finalPaydate)) {
				sv.npaydateErr.set(errorsInner.g539);
			}
		} else {
			sv.nextPaydate.set(sv.firstPaydate);
		}
	}

	protected void checkReviewDate2075() {
		/* Validate Review Date. */
		if ((isEQ(sv.revdte, varcom.vrcmMaxDate)) || (isEQ(sv.revdte, ZERO))) {
			sv.revdte.set(varcom.vrcmMaxDate);
		}
		if (isNE(sv.cltype, SPACES)) {
			if (isEQ(t6696rec.revitrm, ZERO)) {
				goTo(GotoLabel.noReviewTerm2076);
			}
		} else {
			goTo(GotoLabel.checkPayDate2077);
		}
		/* If the review date has not been entered we will create it */
		/* from defaults on T6696. */
		if (isEQ(sv.revdte, varcom.vrcmMaxDate)) {
			datcon2rec.freqFactor.set(t6696rec.revitrm);
			/* MOVE T6696-FREQCY-03 TO DTC2-FREQUENCY */
			datcon2rec.frequency.set(t6696rec.freqcy03);
			/* MOVE S6677-REGPAYFREQ TO DTC2-FREQUENCY <LA2110> */
			datcon2rec.intDate2.set(ZERO);
			datcon2rec.intDate1.set(sv.crtdate);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			sv.revdte.set(datcon2rec.intDate2);
		}
		/* We will now foward the REGIST. DATE with the term and */
		/* freqency on T6696, to be able to compare the 2 dates. */
		datcon2rec.freqFactor.set(t6696rec.revitrm);
		/* MOVE T6696-FREQCY-03 TO DTC2-FREQUENCY. */
		datcon2rec.frequency.set(t6696rec.freqcy03);
		/* MOVE S6677-REGPAYFREQ TO DTC2-FREQUENCY. <LA2110> */
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.intDate1.set(sv.crtdate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		if (isGT(datcon2rec.intDate2, sv.revdte)) {
			sv.revdteErr.set(errorsInner.g518);
			goTo(GotoLabel.checkPayDate2077);
		}
	}

	protected void noReviewTerm2076() {
		if (isNE(sv.revdte, varcom.vrcmMaxDate)) {
			if (isLT(sv.revdte, sv.firstPaydate)) {
				sv.revdteErr.set(errorsInner.g530);
			}
		}
	}

	protected void checkPayDate2077() {
		/* Validate Final Payment Date. */
		if (isEQ(sv.regpayfreq, "00")) {
			sv.finalPaydate.set(sv.firstPaydate);
			wsaaFreqFlag = "Y";
		} else {
			/* IF REGP-FINAL-PAYDATE = 0 <LA1200> */
			/* MOVE VRCM-MAX-DATE TO S6677-FINAL-PAYDATE <LA1200> */
			/* ELSE <LA1200> */
			if (isNE(regpIO.getFinalPaydate(), 0)) {
				if (isEQ(wsaaFreqFlag, "Y") || isEQ(sv.finalPaydate, varcom.vrcmMaxDate)) {
					sv.finalPaydate.set(regpIO.getFinalPaydate());
				}
			}
			wsaaFreqFlag = "N";
		}
		if (isNE(sv.finalPaydate, varcom.vrcmMaxDate)) {
			if (isLT(sv.finalPaydate, sv.nextPaydate)) {
				sv.epaydateErr.set(errorsInner.g519);
			}
		}
		/* Anniversary Date should not be less than Registration Date. */
		if ((isNE(sv.anvdate, varcom.vrcmMaxDate)) && (isNE(sv.anvdate, ZERO))) {
			if (isLT(sv.anvdate, sv.crtdate)) {
				sv.anvdateErr.set(errorsInner.g534);
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		if ((isEQ(sv.anvdate, varcom.vrcmMaxDate)) || (isEQ(sv.anvdate, ZERO))) {
			if (isNE(t6696rec.inxfrq, SPACES)) {
				datcon2rec.freqFactor.set(1);
				datcon2rec.frequency.set(t6696rec.inxfrq);
				datcon2rec.intDate2.set(ZERO);
				datcon2rec.intDate1.set(sv.crtdate);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz, varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					syserrrec.statuz.set(datcon2rec.statuz);
					fatalError600();
				}
				sv.anvdate.set(datcon2rec.intDate2);
			} else {
				sv.anvdate.set(varcom.vrcmMaxDate);
			}
		}
	}

	protected void validateFollow2079() {
		/* Validate Follow-Up Indicator. */
		if ((isNE(sv.fupflg, "+")) && (isNE(sv.fupflg, "X")) && (isNE(sv.fupflg, SPACES))) {
			sv.fupflgErr.set(errorsInner.h118);
		}
		/* Validate Annuity Details Indicator */
		if ((isNE(sv.anntind, "+")) && (isNE(sv.anntind, "X")) && (isNE(sv.anntind, SPACES))) {
			sv.anntindErr.set(errorsInner.h118);
		}
		/* Validate Bank Details Indicator. */
		if ((isNE(sv.ddind, "+")) && (isNE(sv.ddind, "X")) && (isNE(sv.ddind, SPACES))) {
			sv.ddindErr.set(errorsInner.h118);
		}
		/* Validate Accident Indicator. */
		if ((isNE(sv.agclmstz, "+")) && (isNE(sv.agclmstz, "X")) && (isNE(sv.agclmstz, SPACES))) {
			sv.agclmstzErr.set(errorsInner.h118);
		}
		if ((isNE(wsaaPayclt, SPACES)) && (isNE(sv.payclt, SPACES))) {
			if (isNE(wsaaPayclt, sv.payclt)) {
				if (isEQ(t6694rec.bankreq, "Y")) {
					regpIO.setBankkey(SPACES);
					regpIO.setBankacckey(SPACES);
					sv.ddind.set("X");
				} else {
					if (isEQ(t6694rec.bankreq, "N") || isEQ(t6694rec.bankreq, " ")) {
						regpIO.setBankkey(SPACES);
						regpIO.setBankacckey(SPACES);
						sv.ddind.set(SPACES);
					}
				}
			}
		}
		if ((isEQ(t6694rec.bankreq, "Y")) && (isEQ(sv.ddind, SPACES))) {
			sv.ddind.set("X");
		}
		/* Accumulate all claims (REGPENQ) which do not have a */
		/* status of terminated and have an overlapping period */
		/* with the new claim. We will not attempt to do an */
		/* accumulation if this is the first claim on the contract. */
		/* The accumulation ONLY takes into account claims on this */
		/* component that have the same Reason Code. */
		if ((isGT(sv.rgpynum, 1)) && (isNE(wsspcomn.flag, "I"))) {
			wsaaPymt.set(ZERO);
			wsaaPymt2.set(ZERO);
			regpenqIO.setChdrcoy(regpIO.getChdrcoy());
			regpenqIO.setChdrnum(regpIO.getChdrnum());
			regpenqIO.setLife(regpIO.getLife());
			regpenqIO.setCoverage(regpIO.getCoverage());
			regpenqIO.setRider(regpIO.getRider());
			regpenqIO.setRgpynum(99999);
			regpenqIO.setStatuz(varcom.oK);
			regpenqIO.setFunction(varcom.begn);
			if (CMRPY005Permission) {
				// ICIL-556
				regpenqIO.setAdjamt(regpIO.getAdjamt());
				regpenqIO.setReason(regpIO.getReason());
				regpenqIO.setReasoncd(regpIO.getReasoncd());
				regpenqIO.setNetamt(regpIO.getNetamt());
			}

			// performance improvement -- Niharika Modi
			regpenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			regpenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

			wsaaTotPymt.set(ZERO);
			while (!(isEQ(regpenqIO.getStatuz(), varcom.endp))) {
				/* COMPUTE WSAA-PYMT = WSAA-PYMT + S6677-PYMT */
				/* COMPUTE WSAA-TOT-PYMT = WSAA-TOT-PYMT + S6677-PYMT <006> */
				accumRegp2500();
			}
			// ILIFE-1195 STARTS
			// if (isNE(sv.regpayfreq, "00")) {
			compute(wsaaPymt, 2).set(add(wsaaPymt, sv.pymt));
			compute(wsaaTotPymt, 2).set(add(wsaaTotPymt, sv.pymt));
			// }
			// ILIFE-1195 ENDS
		} else {
			/* MOVE S6677-PYMT TO WSAA-PYMT */
			if (isNE(sv.regpayfreq, "00")) {
				wsaaPymt.set(sv.pymt);
			}
		}
		/* Obtain the maximum override percentage value */
		/* COMPUTE WSAA-MAX-PRCNT-VALUE = */
		/* (WSAA-SUMINS2 * T6696-MXOVRPCT / 100). */
		/* COMPUTE WSAA-MAX-PRCNT-VALUE = <006> */
		/* (WSAA-SUMINS * T6696-MXOVRPCT / 100).<006> */
		compute(wsaaMaxPrcntValue, 2).set((div(mult(wsaaSumins2, t6696rec.mxovrpct), 100)));
		/* Check if the total claim sum in the period specified */
		/* is higher than the maximum override percentage value */
		if (isGT(wsaaPymt, wsaaMaxPrcntValue)) {
			if (isEQ(wsaaIsWop, "Y") && isEQ(tr517rec.zrwvflg01, "Y")) {
				/* NEXT_SENTENCE */
			} else {
				sv.totalamt.set(wsaaPymt);
				/* MOVE G527 TO S6677-TOTALAMT-ERR */
				sv.totalamtErr.set(errorsInner.g515);
				sv.msgclaim.set(wsaaClaimReasMess);
				sv.msgclaimOut[varcom.nd.toInt()].set(SPACES);
				sv.totalamtOut[varcom.nd.toInt()].set(SPACES);
				// goTo(GotoLabel.checkForErrors2080);
			}
		} else {
			sv.totalamtOut[varcom.nd.toInt()].set("Y");
			sv.msgclaimOut[varcom.nd.toInt()].set("Y");
		}
		/* Check that the total Sum of all claims is not greater */
		/* than the sum insured. */
		/* IF WSAA-TOT-PYMT > WSAA-SUMINS <006> */
		if (isGT(wsaaTotPymt, wsaaSumins2)) {
			if (isEQ(wsaaIsWop, "Y") && isEQ(tr517rec.zrwvflg01, "Y")) {
				/* NEXT_SENTENCE */
			} else {
				sv.totalamt.set(wsaaTotPymt);
				sv.totalamtErr.set(errorsInner.g527);
				sv.msgclaim.set(wsaaClaimCompMess);
				sv.msgclaimOut[varcom.nd.toInt()].set(SPACES);
				sv.totalamtOut[varcom.nd.toInt()].set(SPACES);
			}
		} else {
			sv.totalamtOut[varcom.nd.toInt()].set("Y");
			sv.msgclaimOut[varcom.nd.toInt()].set("Y");
		}
	}

	protected void checkForErrors2080() {
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	 * <pre>
	*    Sections performed from the 2000 section above.
	 * </pre>
	 */
	protected void checkContractNo2100() {
		/* CHECK */
		chdrenqIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if ((isNE(chdrenqIO.getStatuz(), varcom.oK)) && (isNE(chdrenqIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrenqIO.getStatuz(), varcom.mrnf)) {
			sv.destkeyErr.set(errorsInner.g126);
			wsspcomn.edterror.set("Y");
		}
		/* EXIT */
	}

	protected void protectScr2400() {
		/* PROTECT */
		sv.paycltOut[varcom.pr.toInt()].set("Y");
		sv.cltypeOut[varcom.pr.toInt()].set("Y");
		sv.claimevdOut[varcom.pr.toInt()].set("Y");
		sv.rgpymopOut[varcom.pr.toInt()].set("Y");
		sv.regpayfreqOut[varcom.pr.toInt()].set("Y");
		sv.destkeyOut[varcom.pr.toInt()].set("Y");
		sv.pymtOut[varcom.pr.toInt()].set("Y");
		sv.claimcurOut[varcom.pr.toInt()].set("Y");
		sv.crtdateOut[varcom.pr.toInt()].set("Y");
		sv.revdteOut[varcom.pr.toInt()].set("Y");
		sv.fpaydateOut[varcom.pr.toInt()].set("Y");
		sv.npaydateOut[varcom.pr.toInt()].set("Y");
		sv.anvdateOut[varcom.pr.toInt()].set("Y");
		sv.canceldateOut[varcom.pr.toInt()].set("Y");
		sv.zrsuminOut[varcom.pr.toInt()].set("Y");
		sv.zclmrecdOut[varcom.pr.toInt()].set("Y");
		sv.incurdtOut[varcom.pr.toInt()].set("Y");
		sv.epaydateOut[varcom.pr.toInt()].set("Y");
		// ICIL-556
		sv.adjustamtOut[varcom.pr.toInt()].set("Y");
		sv.netclaimamtOut[varcom.pr.toInt()].set("Y");
		sv.reasoncdOut[varcom.pr.toInt()].set("Y");
		sv.resndescOut[varcom.pr.toInt()].set("Y");
		sv.notifinoOut[varcom.pr.toInt()].set("Y");//fwang3
		/* EXIT */
	}

	protected void accumRegp2500() {
		accum2510();
	}

	protected void accum2510() {
		SmartFileCode.execute(appVars, regpenqIO);
		if ((isNE(regpenqIO.getStatuz(), varcom.oK)) && (isNE(regpenqIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(regpenqIO.getParams());
			fatalError600();
		}
		if (isEQ(regpenqIO.getStatuz(), varcom.endp)) {
			return;
		}
		/* Check all claims with the same reason codes, and add up */
		/* payment amounts. */
		if ((isEQ(regpIO.getChdrcoy(), regpenqIO.getChdrcoy())) && (isEQ(regpIO.getChdrnum(), regpenqIO.getChdrnum()))
				&& (isEQ(regpIO.getLife(), regpenqIO.getLife()))
				&& (isEQ(regpIO.getCoverage(), regpenqIO.getCoverage()))
				&& (isEQ(regpIO.getRider(), regpenqIO.getRider()))) {
			/* IF REGP-PAYREASON = REGPENQ-PAYREASON */
			if (isEQ(sv.cltype, regpenqIO.getPayreason())) {
				if (isNE(regpIO.getRgpynum(), regpenqIO.getRgpynum())) {
					wsaaOverlap = "N";
					checkOverlap2510();
					if (isEQ(wsaaOverlap, "Y")) {
						wsaaOverlap = "N";
						wsaaFreq2.set(regpenqIO.getRegpayfreq());
						wsaaPymt2.set(regpenqIO.getPymt());
						if (isNE(wsaaFreq2, ZERO)) {
							compute(wsaaPymt2, 2).set((div((mult(wsaaPymt2, wsaaFreq2)), wsaaFreq)));
							zrdecplrec.amountIn.set(wsaaPymt2);
							a000CallRounding();
							wsaaPymt2.set(zrdecplrec.amountOut);
							compute(wsaaPymt, 2).set(add(wsaaPymt, wsaaPymt2));
						}
					}
				}
			}
		} else {
			regpenqIO.setStatuz(varcom.endp);
		}
		/* Check all active claims for period, and add up payment */
		/* amounts. These claims can have different reason codes. */
		if ((isEQ(regpIO.getChdrcoy(), regpenqIO.getChdrcoy())) && (isEQ(regpIO.getChdrnum(), regpenqIO.getChdrnum()))
				&& (isEQ(regpIO.getLife(), regpenqIO.getLife()))
				&& (isEQ(regpIO.getCoverage(), regpenqIO.getCoverage()))
				&& (isEQ(regpIO.getRider(), regpenqIO.getRider()))) {
			if (isNE(regpIO.getRgpynum(), regpenqIO.getRgpynum())) {
				wsaaOverlap = "N";
				checkOverlap2510();
				if (isEQ(wsaaOverlap, "Y")) {
					wsaaOverlap = "N";
					wsaaFreq2.set(regpenqIO.getRegpayfreq());
					wsaaPymt2.set(regpenqIO.getPymt());
					// ILIFE-1195 STARTS
					if (isEQ(wsaaFreq2, ZERO)) {
						wsaaFreq2.set(1);
					}
					if (isEQ(wsaaFreq, ZERO)) {
						wsaaFreq.set(1);
					}
					// ILIFE-1195 ENDS
					if (isNE(wsaaFreq2, ZERO)) {
						compute(wsaaPymt2, 2).set((div((mult(wsaaPymt2, wsaaFreq2)), wsaaFreq)));
						zrdecplrec.amountIn.set(wsaaPymt2);
						a000CallRounding();
						wsaaPymt2.set(zrdecplrec.amountOut);
						wsaaTotPymt.add(wsaaPymt2);
					}
				}
			}
		}
		regpenqIO.setFunction(varcom.nextr);
	}

	protected void checkOverlap2510() {
		/* OVERLAP */
		/* If the cancel date doesn't = max date then do not include */
		/* the records in payment amount as they have been cancelled. */
		if (isEQ(regpenqIO.getCancelDate(), varcom.vrcmMaxDate)) {
			if ((isGT(sv.firstPaydate, regpenqIO.getFinalPaydate()))
					|| (isLT(sv.finalPaydate, regpenqIO.getFirstPaydate()))) {
				wsaaOverlap = "N";
			} else {
				wsaaOverlap = "Y";
			}
		} else {
			wsaaOverlap = "N";
		}
		/* EXIT */
	}

	protected void checkT66942600() {
		starts2600();
	}

	protected void starts2600() {
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6694);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(sv.rgpymop);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
				|| (isNE(sv.rgpymop, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			sv.destkeyOut[varcom.pr.toInt()].set("Y");
			return;
		}
		t6694rec.t6694Rec.set(itdmIO.getGenarea());
		wsaaRgpymop.set(sv.rgpymop);
		if (isEQ(t6694rec.contreq, "N")) {
			sv.destkeyOut[varcom.pr.toInt()].set("Y");
		} else {
			sv.destkeyOut[varcom.pr.toInt()].set(SPACES);
		}
	}

	/**
	 * <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000() {
		try {
			updateDatabase3010();
			softLock3080();
		} catch (GOTOException e) {
			/* Expected exception for control flow purposes. */
		}
	}

	protected void updateDatabase3010() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3090);
		}
		if ((isEQ(firstTime, "Y")) && (isNE(wsspcomn.flag, "I"))) {
			chdrlifIO.setChdrpfx(chdrrgpIO.getChdrpfx());
			chdrlifIO.setChdrcoy(chdrrgpIO.getChdrcoy());
			chdrlifIO.setChdrnum(chdrrgpIO.getChdrnum());
			chdrlifIO.setValidflag(chdrrgpIO.getValidflag());
			chdrlifIO.setCurrfrom(chdrrgpIO.getCurrfrom());
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			/* MOVE BEGNH TO CHDRLIF-FUNCTION */
			chdrlifIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			chdrlifIO.setCurrto(datcon1rec.intDate);
			chdrlifIO.setValidflag("2");
			/* MOVE REWRT TO CHDRLIF-FUNCTION */
			chdrlifIO.setFunction(varcom.writd);
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			/* f "WOP" method is selected, perform the Sinstamt */
			/* validation. Otherwise skip this validation. */
			itdmIO.setDataKey(SPACES);
			itdmIO.setItemcoy(wsspcomn.company);
			itdmIO.setFormat(formatsInner.itdmrec);
			itdmIO.setItemtabl(tablesInner.t6694);
			itdmIO.setItempfx("IT");
			itdmIO.setItemitem(sv.rgpymop);
			itdmIO.setItmfrm(chdrrgpIO.getOccdate());
			itdmIO.setFunction(varcom.begn);
			// performance improvement -- Niharika Modi
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

			SmartFileCode.execute(appVars, itdmIO);
			if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t6694, itdmIO.getItemtabl()))
					|| (isNE(sv.rgpymop, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
				itdmIO.setStatuz(varcom.endp);
				itdmIO.setGenarea(SPACES);
			}
			t6694rec.t6694Rec.set(itdmIO.getGenarea());
			wsaaWopFlag.set(SPACES);
			if (isEQ(t6694rec.sacscode, t5645rec.sacscode01) && isEQ(t6694rec.sacstype, t5645rec.sacstype01)) {
				wopMop.setTrue();
			}
			/* Adjustment after an approval requires immediate updating done */
			/* to SINSTAMTs on CHDR and PAYR since approval program will add */
			/* the claim amount to SINSTAMTs again. */
			/* Further to above statement, adjustment is necessary if last */
			/* trans is in review status. Because in B6682, adjustment to */
			/* SINSTAMTs on CHDR and PAYR is commented off. Also SINSTAMT05 */
			/* is wrongly updated. */
			/* IF (REGP-DESTKEY NOT = SPACES) AND */
			/* (REGP-APRVDATE NOT = VRCM-MAX-DATE) */
			/* (REGP-APRVDATE NOT = VRCM-MAX-DATE) AND <LA1172> */
			/* (REGP-APRVDATE NOT = ZERO) AND <LA1172> */
			/* WOP-MOP <LA1172> */
			if ((wopMop.isTrue() && isNE(regpIO.getDestkey(), SPACES) && isNE(regpIO.getAprvdate(), varcom.vrcmMaxDate)
					&& isNE(regpIO.getAprvdate(), ZERO)) || (wopMop.isTrue() && isEQ(regpIO.getRgpystat(), "IR"))) {
				/* COMPUTE CHDRLIF-SINSTAMT05 = (REGP-PYMT + */
				/* COMPUTE CHDRLIF-SINSTAMT05 = ((REGP-PYMT * -1) + <003> */
				/* CHDRLIF-SINSTAMT05) */
				/* COMPUTE CHDRLIF-SINSTAMT05 = (REGP-PYMT * -1) <LA2108> */
				/* MOVE S6677-PYMT TO WSAA-S6677-PYMT <LA1172> */
				wsaaS6677Pymt.set(regpIO.getPymt());
				setPrecision(chdrlifIO.getSinstamt05(), 2);
				chdrlifIO.setSinstamt05(sub(chdrlifIO.getSinstamt05(), (mult(wsaaS6677Pymt, -1))));
				if (isEQ(wsaaIsWop, "Y")) {
					if (isEQ(tr517rec.zrwvflg01, "N")) {
						setPrecision(chdrlifIO.getSinstamt01(), 2);
						chdrlifIO.setSinstamt01(add(chdrlifIO.getSinstamt01(), wsaaCovrInstprem));
					}
				}
				setPrecision(chdrlifIO.getSinstamt06(), 2);
				chdrlifIO
						.setSinstamt06((add(
								add(add(add(chdrlifIO.getSinstamt01(), chdrlifIO.getSinstamt02()),
										chdrlifIO.getSinstamt03()), chdrlifIO.getSinstamt04()),
								chdrlifIO.getSinstamt05())));
			}
			if(CMRPY012Permission && isNE(wsspcomn.flag, "M") && isEQ(sv.cnttype, LPS) && isEQ(sv.crtable, LWPR)){//ILIFE-8299
				setContStatus();
			}			
			chdrlifIO.setCurrfrom(datcon1rec.intDate);
			chdrlifIO.setCurrto(varcom.vrcmMaxDate);
			chdrlifIO.setValidflag("1");
			setPrecision(chdrlifIO.getTranno(), 0);
			chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
			chdrlifIO.setFunction(varcom.writr);
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			payrIO.setDataArea(SPACES);
			payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
			payrIO.setChdrnum(chdrrgpIO.getChdrnum());
			payrIO.setValidflag("1");
			payrIO.setPayrseqno(wsaaPayrseqno);
			payrIO.setFormat(formatsInner.payrrec);
			payrIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getStatuz());
				fatalError600();
			}
			payrIO.setValidflag("2");
			payrIO.setFunction(varcom.rewrt);
			payrIO.setFormat(formatsInner.payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
			/* IF (REGP-DESTKEY NOT = SPACES) AND <LA1172> */
			/* (REGP-APRVDATE NOT = VRCM-MAX-DATE) <LA2110> */
			/* (REGP-APRVDATE NOT = VRCM-MAX-DATE) AND <LA1172> */
			/* (REGP-APRVDATE NOT = ZERO) AND <LA1172> */
			/* WOP-MOP <LA1172> */
			if ((wopMop.isTrue() && isNE(regpIO.getDestkey(), SPACES) && isNE(regpIO.getAprvdate(), varcom.vrcmMaxDate)
					&& isNE(regpIO.getAprvdate(), ZERO)) || (wopMop.isTrue() && isEQ(regpIO.getRgpystat(), "IR"))) {
				/* COMPUTE PAYR-SINSTAMT05 = (REGP-PYMT + */
				/* COMPUTE PAYR-SINSTAMT05 = ((REGP-PYMT * -1) + <003> */
				/* PAYR-SINSTAMT05) <003> */
				/* COMPUTE PAYR-SINSTAMT05 = (REGP-PYMT * -1) <LA2110> */
				/* MOVE S6677-PYMT TO WSAA-S6677-PYMT <LA1172> */
				wsaaS6677Pymt.set(regpIO.getPymt());
				setPrecision(payrIO.getSinstamt05(), 2);
				payrIO.setSinstamt05(sub(payrIO.getSinstamt05(), (mult(wsaaS6677Pymt, -1))));
				if (isEQ(wsaaIsWop, "Y")) {
					if (isEQ(tr517rec.zrwvflg01, "N")) {
						setPrecision(payrIO.getSinstamt01(), 2);
						payrIO.setSinstamt01(add(payrIO.getSinstamt01(), wsaaCovrInstprem));
					}
				}
				setPrecision(payrIO.getSinstamt06(), 2);
				payrIO.setSinstamt06(
						(add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()),
								payrIO.getSinstamt04()), payrIO.getSinstamt05())));
			}
			payrIO.setValidflag("1");
			payrIO.setTranno(chdrlifIO.getTranno());
			payrIO.setFunction(varcom.writr);
			payrIO.setFormat(formatsInner.payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
		}
		if ((isEQ(firstTime, "Y")) && (isEQ(wsspcomn.flag, "I"))) {
			chdrlifIO.setChdrpfx(chdrrgpIO.getChdrpfx());
			chdrlifIO.setChdrcoy(chdrrgpIO.getChdrcoy());
			chdrlifIO.setChdrnum(chdrrgpIO.getChdrnum());
			chdrlifIO.setValidflag(chdrrgpIO.getValidflag());
			chdrlifIO.setCurrfrom(chdrrgpIO.getCurrfrom());
			chdrlifIO.setFormat(formatsInner.chdrlifrec);
			chdrlifIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, chdrlifIO);
			if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlifIO.getParams());
				fatalError600();
			}
			setPrecision(chdrlifIO.getTranno(), 0);
			chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		}
		if (isEQ(firstTime, "Y")) {
			firstTime = "N";
			if (CMRPY012Permission && isEQ(wsspcomn.flag, "C") && isEQ(sv.cnttype, LPS) && isEQ(sv.crtable, LWPR)) {//ILIFE-8299
				i900UpdateCovr();//ILIFE-8299
			}
			if (isEQ(wsspcomn.flag, "M")) {
				regpIO.setValidflag("2");
				regpIO.setFormat(formatsInner.regprec);
				regpIO.setFunction(varcom.updat);
				SmartFileCode.execute(appVars, regpIO);
				if (isNE(regpIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(regpIO.getParams());
					fatalError600();
				}
				regpIO.setValidflag("1");
			}
		}
		updateDatabaseCustomerSpecific();
		/* Update database files as required / WSSP */
		if (isEQ(sv.fupflg, "X")) {
			moveToRegp3400();
			regpIO.setFunction(varcom.keeps);
			regpIO.setFormat(formatsInner.regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
	
		if (isEQ(sv.ddind, "X")) {
			moveToRegp3400();
			regpIO.setFunction(varcom.keeps);
			regpIO.setFormat(formatsInner.regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(sv.agclmstz, "X")) {
			moveToRegp3400();
			regpIO.setFunction(varcom.keeps);
			regpIO.setFormat(formatsInner.regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(regpIO.getStatuz());
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		/* No Update on an Inquiry. */
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}	
		//ICIL-1546 Starts
		if(cmoth003permission) {
			if (isEQ(sv.claimnotes, "X")) {
					goTo(GotoLabel.claimNotes);
				}
			if (isEQ(sv.investres, "X")) {
				goTo(GotoLabel.investResult);
			}
		}
		//ICIL-1546 End
		readT66935200();
		if(cmoth003permission){
			updateClaimNoAndNotifiNo();//fwang3
			if(isNE(sv.notifino,SPACES) && isNE(wsspcomn.flag,"I")){
				clnnpfDAO.updateClnnpfClaimno(sv.claimno.toString(),sv.notifino.toString().replace(CLMPREFIX, ""));
			}
			if(isNE(sv.notifino,SPACES) && isNE(wsspcomn.flag,"I")){
				invspfDAO.updateInvspfClaimno(sv.claimno.toString(),sv.notifino.toString().replace(CLMPREFIX, ""));
			}
			
		}
		if (isEQ(wsspcomn.flag, "C")) {
			createRegp3100();
			//IBPLIFE-1702
			setupNotipf();
	        if(notipf!=null) {//IBPLIFE-2838
			 notipfDAO.updateNotipfValidFlag(expiredFlag,notipf.getNotifinum());
			 //insert new Notipf record with validFlag 1;
			 notipf.setValidFlag(effectiveFlag);
			 notipf.setNotifistatus(claimRegi);
			 addTransnoToNotipf(wsspcomn.wsaanotificationNum.toString().trim().replace(CLMPREFIX, ""));
			 notipfDAO.insertNotipf(notipf);
	        }
			//end
		}
		if (isEQ(wsspcomn.flag, "M")) {
			updateRegp3200();
		}
		updateRegpayCltCustomerSpecific3010();
		/* Release the Contract Header. */
		chdrrgpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrrgpIO.getParams());
			fatalError600();
		}
		/* Read the Contract Type description. */
	}
	
	//IBPLIFE-1702
	private void addTransnoToNotipf(String notifinum) {
		int newTransno;
		String maxTransnum = notipfDAO.getMaxTransno(notifinum);
		if("".equals(maxTransnum) || maxTransnum==null) {
			notipf.setTransno(String.valueOf(noTransnoCount));
		}else {
			newTransno = Integer.valueOf(maxTransnum.trim())+noTransnoCount;	//IBPLIFE-1071
			notipf.setTransno(String.valueOf(newTransno));
		}
	}

	/**
	 * @author agoel51 
	 * Update the fields
	 */
	private void updateClaimNoAndNotifiNo() {
		if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "M")) {
			regpIO.setClaimno(sv.claimno);
			regpIO.setClaimnotifino(sv.notifino.toString().replace(CLMPREFIX, ""));
		}
	}
	

protected void setupNotipf(){
	

	wsspcomn.chdrCownnum.set(sv.lifcnum.toString());	
	wsspcomn.wsaaclaimno.set(sv.claimno.toString().trim());
	if(isEQ(sv.notifino,SPACES)){
		wsspcomn.wsaarelationship.set(SPACES);
		wsspcomn.wsaaclaimant.set(sv.lifcnum.toString());
		wsspcomn.wsaanotificationNum.set(SPACES);
	}
	else{
		notipf = notipfDAO.getNotiReByNotifin(sv.notifino.toString().replace(CLMPREFIX, ""),wsspcomn.company.toString());
		wsspcomn.wsaarelationship.set(notipf.getRelationcnum());
		wsspcomn.wsaaclaimant.set(notipf.getClaimant());
		wsspcomn.wsaanotificationNum.set(sv.notifino.toString());
	}

} 

protected void updateClaimnoInAll(){
	
	clnnpfList1 =  clnnpfDAO.getClnnpfList(wsspcomn.company.toString(), sv.notifino.toString().replace(CLMPREFIX, "").trim(),sv.claimno.toString().trim());
	if(clnnpfList1!=null && !clnnpfList1.isEmpty()){
		sv.claimnotes.set("+");
	}
	else{
		sv.claimnotes.set(SPACES);
	}
}
protected void checkInvspf(){
	
	invspfList1 =  invspfDAO.getInvspfList(wsspcomn.company.toString(), sv.notifino.toString().replace(CLMPREFIX, "").trim(),sv.claimno.toString().trim());
	if(invspfList1!=null && !invspfList1.isEmpty()){
		sv.investres.set("+");
	}
	else{
		sv.investres.set(SPACES);
	}
}

	
	protected void softLock3080() {
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrrgpIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	 * <pre>
	*    Sections performed from the 3000 section above.
	 * </pre>
	 */
	protected void createRegp3100() {
		create3110();
	}

	protected void create3110() {
		if (isEQ(t6694rec.contreq, "Y")) {
			regpIO.setDestkey(sv.destkey);
			regpIO.setGlact(t6694rec.glact);
		} else {
			regpIO.setDestkey(SPACES);
			regpIO.setGlact(t6694rec.glact);
		}
		regpIO.setSacscode(t6694rec.sacscode);
		regpIO.setSacstype(t6694rec.sacstype);
		regpIO.setDebcred(t6694rec.debcred);
		regpIO.setRgpytype(t6692rec.rgpytype);
		regpIO.setTranno(chdrlifIO.getTranno());
		regpIO.setValidflag("1");
		regpIO.setRgpystat(wsaaRgpystat);
		regpIO.setRgpynum(sv.rgpynum);
		regpIO.setPayclt(sv.payclt);
		regpIO.setPayreason(sv.cltype);
		regpIO.setClaimevd(sv.claimevd);
		regpIO.setRgpymop(sv.rgpymop);
		regpIO.setRegpayfreq(sv.regpayfreq);
		regpIO.setPrcnt(sv.prcnt);
		regpIO.setPymt(sv.pymt);
		if (CMRPY005Permission) {
			// ICIL-556
			regpIO.setAdjamt(sv.adjustamt);
			regpIO.setReasoncd(sv.reasoncd);
			regpIO.setNetamt(sv.netclaimamt);
			regpIO.setReason(sv.resndesc);
		}
		regpIO.setCurrcd(sv.claimcur);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setCrtdate(sv.crtdate);
		regpIO.setRevdte(sv.revdte);
		regpIO.setFirstPaydate(sv.firstPaydate);
		regpIO.setAnvdate(sv.anvdate);
		regpIO.setFinalPaydate(sv.finalPaydate);
		regpIO.setCancelDate(varcom.vrcmMaxDate);
		regpIO.setAprvdate(varcom.vrcmMaxDate);
		regpIO.setNextPaydate(sv.nextPaydate);
		regpIO.setLastPaydate(varcom.vrcmMaxDate);
		regpIO.setCertdate(varcom.vrcmMaxDate);
		regpIO.setRecvdDate(sv.recvdDate);
		regpIO.setIncurdt(sv.incurdt);
		if(CMRPY012Permission && isEQ(sv.cnttype, LPS) && isEQ(sv.crtable, LWPR)){
			regpIO.setOvrpermently(sv.ovrpermently);
			regpIO.setOvrsumin(sv.ovrsumin);
		}
		calculateDate3500();
		regpIO.setCrtable(wsaaCrtable);
		regpIO.setPaycoy(wsspcomn.fsuco);
		regpIO.setFunction(varcom.writr);
		regpIO.setFormat(formatsInner.regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		updatPtrn3300();
	}

	protected void updateRegp3200() {
		update3210();
	}

	protected void update3210() {
		if (isEQ(t6694rec.contreq, "Y")) {
			regpIO.setDestkey(sv.destkey);
			regpIO.setGlact(t6694rec.glact);
		} else {
			regpIO.setDestkey(SPACES);
			regpIO.setGlact(t6694rec.glact);
		}
		regpIO.setSacscode(t6694rec.sacscode);
		regpIO.setSacstype(t6694rec.sacstype);
		regpIO.setDebcred(t6694rec.debcred);
		regpIO.setRgpytype(t6692rec.rgpytype);
		regpIO.setTranno(chdrlifIO.getTranno());
		if (isNE(wsaaRgpystat, SPACES)) {
			regpIO.setRgpystat(wsaaRgpystat);
		}
		regpIO.setRgpynum(sv.rgpynum);
		regpIO.setPayclt(sv.payclt);
		regpIO.setPayreason(sv.cltype);
		regpIO.setClaimevd(sv.claimevd);
		regpIO.setRgpymop(sv.rgpymop);
		regpIO.setRegpayfreq(sv.regpayfreq);
		regpIO.setPrcnt(sv.prcnt);
		regpIO.setPymt(sv.pymt);
		regpIO.setCurrcd(sv.claimcur);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setCrtdate(sv.crtdate);
		regpIO.setRevdte(sv.revdte);
		regpIO.setFirstPaydate(sv.firstPaydate);
		regpIO.setNextPaydate(sv.nextPaydate);
		regpIO.setAnvdate(sv.anvdate);
		regpIO.setFinalPaydate(sv.finalPaydate);
		regpIO.setCancelDate(varcom.vrcmMaxDate);
		regpIO.setAprvdate(varcom.vrcmMaxDate);
		regpIO.setRecvdDate(sv.recvdDate);
		regpIO.setIncurdt(sv.incurdt);
		regpIO.setFunction(varcom.writr);
		regpIO.setFormat(formatsInner.regprec);
		if (CMRPY005Permission) {
			// ICIL-556
			regpIO.setAdjamt(sv.adjustamt);
			regpIO.setReasoncd(sv.reasoncd);
			regpIO.setNetamt(sv.netclaimamt);
			regpIO.setReason(sv.resndesc);
		}
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		updateCustomerSpecificFields(regpIO.chdrcoy.toString(), regpIO.chdrnum.toString());
		updatPtrn3300();
	}

	protected void updatPtrn3300() {
		updat3310();
	}

	protected void updat3310() {
		/* Write a PTRN record. */
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrrgpIO.getChdrnum());
		ptrnIO.setRecode(chdrrgpIO.getRecode());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTerm);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setBatccoy(wsspcomn.company);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setPrtflg(SPACES);
		ptrnIO.setValidflag("1");
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

	protected void moveToRegp3400() {
		moveToRegp3410();
	}

	protected void moveToRegp3410() {
		regpIO.setDestkey(sv.destkey);
		regpIO.setRgpynum(sv.rgpynum);
		regpIO.setPayclt(sv.payclt);
		regpIO.setPayreason(sv.cltype);
		regpIO.setClaimevd(sv.claimevd);
		regpIO.setRgpymop(sv.rgpymop);
		regpIO.setRegpayfreq(sv.regpayfreq);
		regpIO.setPrcnt(sv.prcnt);
		regpIO.setPymt(sv.pymt);
		if (CMRPY005Permission) {
			// ICIL-556
			regpIO.setAdjamt(sv.adjustamt);
			regpIO.setReasoncd(sv.reasoncd);
			regpIO.setNetamt(sv.netclaimamt);
			regpIO.setReason(sv.resndesc);
		}
		regpIO.setCurrcd(sv.claimcur);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setCrtdate(sv.crtdate);
		regpIO.setRevdte(sv.revdte);
		regpIO.setFirstPaydate(sv.firstPaydate);
		regpIO.setAnvdate(sv.anvdate);
		regpIO.setFinalPaydate(sv.finalPaydate);
		regpIO.setCancelDate(sv.cancelDate);
		regpIO.setAprvdate(sv.aprvdate);
		regpIO.setNextPaydate(sv.nextPaydate);
		regpIO.setLastPaydate(sv.lastPaydate);
		regpIO.setCrtable(wsaaCrtable);
		regpIO.setTranno(chdrlifIO.getTranno());
	}

	protected void calculateDate3500() {
		/* START */
		datcon2rec.intDate1.set(sv.crtdate);
		if (isNE(t6625rec.frequency, SPACES)) {
			datcon2rec.freqFactor.set(1);
			datcon2rec.frequency.set(t6625rec.frequency);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			regpIO.setCertdate(datcon2rec.intDate2);
		}
		/* EXIT */
	}

	/**
	 * <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					nextProgram4010();
				case popUp4050:
					popUp4050();
				case investResult:
					investResult4060();
				case claimNotes:
					claimNotes4070();
				case gensww4010:
					gensww4010();
				case nextProgram4020:
					nextProgram4020();
				case exit4090:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void nextProgram4010() {
		/* If returning from a program further down the stack then */
		/* first restore the original programs in the program stack. */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1) {
				restoreProgram4100();
			}
		}
		if (isEQ(sv.fupflg, "?")) {
			if (isEQ(sv.ddind, "X")) {
				regpIO.setFunction(varcom.keeps);
				regpIO.setFormat(formatsInner.regprec);
				SmartFileCode.execute(appVars, regpIO);
				if (isNE(regpIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(regpIO.getParams());
					fatalError600();
				}
				goTo(GotoLabel.popUp4050);
			} else {
				checkFollowUp5000();
			}
		}
		if (isEQ(sv.ddind, "?")) {
			checkBankDetails4400();
		}
		if (isEQ(sv.anntind, "?")) {
			checkAnnyDetails6000();
		}
		beneficiaryCustomerSpecific4500();
		if (isEQ(sv.agclmstz, "?")) {
			accidentBenefitClaims9190();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
	}


protected void investResult4060()
{
	
	if ((isEQ(sv.investres, "X")))  {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			saveProgramStack4200();
		}
	}
	gensswrec.function.set(SPACES);
	
	if (isEQ(sv.investres, "?")) {
		 checkInvspf();
		}
	
	if (isEQ(sv.investres, "X")) {
		sv.investres.set("?");
		setupNotipf();
		gensswrec.function.set("E");
		goTo(GotoLabel.gensww4010);
	}

	
	
}

protected void claimNotes4070()
{
	
	if ((isEQ(sv.claimnotes, "X")))  {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			saveProgramStack4200();
		}
	}
	gensswrec.function.set(SPACES);
	
	if (isEQ(sv.claimnotes, "?")) {
		updateClaimnoInAll();
	}
	
	if (isEQ(sv.claimnotes, "X")) {
		sv.claimnotes.set("?");
		setupNotipf();
		gensswrec.function.set("F");
		goTo(GotoLabel.gensww4010);
		return ;
	}
	
	
}


	/**
	 * <pre>
	*    If any of the indicators have been selected, (value - 'X'),
	*    then set an asterisk in the program stack action field to
	*    ensure that control returns here, set the parameters for
	*    generalised secondary switching and save the original
	*    programs from the program stack.
	 * </pre>
	 */
	protected void popUp4050() {
		if ((isEQ(sv.ddind, "X")) || (isEQ(sv.fupflg, "X")) || (isEQ(sv.agclmstz, "X")) || (isEQ(sv.anntind, "X"))) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			gensswrec.company.set(wsspcomn.company);
			gensswrec.progIn.set(wsaaProg);
			gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
			compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
			sub2.set(1);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1) {
				saveProgramStack4200();
			}
		}
		gensswrec.function.set(SPACES);
		/* If FOLLOW-UP has been selected set 'B' in the function. */
		if (isEQ(sv.fupflg, "X")) {
			sv.fupflg.set("?");
			gensswrec.function.set("B");
			goTo(GotoLabel.gensww4010);
		}
		/* If BANK DETAILS has been selected set 'A' in the function. */
		if (isEQ(sv.ddind, "X")) {
			sv.ddind.set("?");
			gensswrec.function.set("A");
			goTo(GotoLabel.gensww4010);
		}
		/* If ANNUITY DETAILS has been selected set 'C' in the function. */
		if (isEQ(sv.anntind, "X")) {
			sv.anntind.set("?");
			gensswrec.function.set("C");
			covrmjaIO.setChdrcoy(regpIO.getChdrcoy());
			covrmjaIO.setChdrnum(regpIO.getChdrnum());
			covrmjaIO.setLife(regpIO.getLife());
			covrmjaIO.setCoverage(regpIO.getCoverage());
			covrmjaIO.setRider(regpIO.getRider());
			covrmjaIO.setPlanSuffix(regpIO.getPlanSuffix());
			covrmjaIO.setFunction(varcom.readr);
			covrmjaIO.setFormat(formatsInner.covrmjarec);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			covrmjaIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.gensww4010);
		}
		/* If Accident has been selected set 'D' in the function. */
		if (isEQ(sv.agclmstz, "X")) {
			sv.agclmstz.set("?");
			gensswrec.function.set("D");
			regpIO.setIncurdt(sv.incurdt);
			regpIO.setRecvdDate(sv.recvdDate);
			regpIO.setFunction(varcom.keeps);
			regpIO.setFormat(formatsInner.regprec);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			covrmjaIO.setChdrcoy(regpIO.getChdrcoy());
			covrmjaIO.setChdrnum(regpIO.getChdrnum());
			covrmjaIO.setLife(regpIO.getLife());
			covrmjaIO.setCoverage(regpIO.getCoverage());
			covrmjaIO.setRider(regpIO.getRider());
			covrmjaIO.setPlanSuffix(regpIO.getPlanSuffix());
			covrmjaIO.setFunction(varcom.readr);
			covrmjaIO.setFormat(formatsInner.covrmjarec);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			covrmjaIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.gensww4010);
		}
	}

	/**
	 * <pre>
	*   If a value has been placed in the GENS-FUNCTION then call
	*   the generalised secondary switching module to obtain the
	*   next 8 programs and load them into the program stack.
	 * </pre>
	 */
	protected void gensww4010() {
		if (isEQ(gensswrec.function, SPACES)) {
			goTo(GotoLabel.nextProgram4020);
		}
		callProgram(Genssw.class, gensswrec.gensswRec);
		if ((isNE(gensswrec.statuz, varcom.oK)) && (isNE(gensswrec.statuz, varcom.mrnf))) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre */
		/* with an error and the options and extras indicator */
		/* with its initial load value */
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/* MOVE V045 TO SCRN-ERROR-CODE */
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1) {
			loadProgramStack4300();
		}
	}

	protected void nextProgram4020() {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

	/**
	 * <pre>
	*    Sections performed from the 4000 section above.
	 * </pre>
	 */
	protected void restoreProgram4100() {
		/* PARA */
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void saveProgramStack4200() {
		/* PARA */
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void loadProgramStack4300() {
		/* PARA */
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
		/* EXIT */
	}

	protected void checkBankDetails4400() {
		bank4400();
	}

	protected void bank4400() {
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Release the Regular Payment Record. */
		regpIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Check if there are any bank details on the current contract. */
		if ((isEQ(regpIO.getBankkey(), SPACES)) && (isEQ(regpIO.getBankacckey(), SPACES))) {
			sv.ddind.set(SPACES);
		} else {
			sv.ddind.set("+");
			wsaaPayclt.set(sv.payclt);
		}
	}

	/**
	 * <pre>
	*    Sections performed from more than 1 section.
	 * </pre>
	 */
	protected void checkFollowUp5000() {
		follow5010();
	}

	protected void follow5010() {
		/* Check if there are any follow-ups details on the current contrac */
		fluprgpIO.setChdrcoy(wsspcomn.company);
		fluprgpIO.setChdrnum(chdrrgpIO.getChdrnum());
		wsaaRgpynum.set(regpIO.getRgpynum());
		wsaaClamnumFill.set(ZERO);
		fluprgpIO.setClamnum(wsaaClamnum2);
		fluprgpIO.setFupno(0);
		/* Use WSAA-CLAMNUM2 to compare to FLUPRGP-CLAMNUM */
		/* instead of comparing REGP-RGPYNUM and FLUPRGP-CLAMNUM. */
		/* Due to the fact that RGPYNUM is 5 bytes long */
		/* and CLAMNUM is 8 bytes long. */
		fluprgpIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		fluprgpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluprgpIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		SmartFileCode.execute(appVars, fluprgpIO);
		if ((isNE(fluprgpIO.getStatuz(), varcom.oK)) && (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprgpIO.getParams());
			fatalError600();
		}
		if ((isEQ(wsspcomn.company, fluprgpIO.getChdrcoy())) && (isEQ(chdrrgpIO.getChdrnum(), fluprgpIO.getChdrnum()))
				&& (isEQ(fluprgpIO.getClamnum(), wsaaClamnum2)) && (isNE(fluprgpIO.getStatuz(), varcom.endp))) {
			sv.fupflg.set("+");
		} else {
			sv.fupflg.set(SPACES);
		}
	}

	protected void readT56065100() {
		read5110();
	}

	protected void read5110() {
		/* Set up the key for T5606 */
		wsaaT5606Edtitm.set(wsaaEdtitm);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t5606);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT5606Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t5606, itdmIO.getItemtabl()))
				|| (isNE(wsaaT5606Key, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setStatuz(varcom.endp);
			itdmIO.setGenarea(SPACES);
		}
		t5606rec.t5606Rec.set(itdmIO.getGenarea());
	}

	protected void readT66935200() {
		readT66935210();
	}

	protected void readT66935210() {
		setRgpystat5210CustomerSpecific();
		wsaaT6693Crtable.set(covrIO.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6693);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6693Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t6693, itdmIO.getItemtabl()))
				|| (isNE(wsaaT6693Key, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			wsaaT6693Crtable.set("****");
			readT66935220();
			if ((isNE(wsspcomn.company, itdmIO.getItemcoy())) || (isNE(tablesInner.t6693, itdmIO.getItemtabl()))
					|| (isNE(wsaaT6693Key, itdmIO.getItemitem())) || (isEQ(itdmIO.getStatuz(), varcom.endp))) {
				itdmIO.setStatuz(varcom.endp);
				itdmIO.setGenarea(SPACES);
				return;
			}
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaRgpystat.set(SPACES);
		index1.set(1);
		while (!(isGT(index1, 12))) {
			findStatus5210();
		}

	}
	protected void setRgpystat5210CustomerSpecific(){
		if (isEQ(wsspcomn.flag, "M")) {
			wsaaT6693Rgpystat.set(regpIO.getRgpystat());
		} else {
			wsaaT6693Rgpystat.set("**");
		}
	}

	protected void findStatus5210() {
		/* FIND-STATUS */
		if (isEQ(t6693rec.trcode[index1.toInt()], wsaaBatckey.batcBatctrcde)) {
			wsaaRgpystat.set(t6693rec.rgpystat[index1.toInt()]);
			index1.set(15);
		}
		index1.add(1);
		/* EXIT */
	}

	protected void readT66935220() {
		readAgain5225();
	}

	protected void readAgain5225() {
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setItemtabl(tablesInner.t6693);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6693Key);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK)) && (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
	}

	protected void checkAnnyDetails6000() {
		readAnny6010();
	}

	protected void readAnny6010() {
		/* Read Annuity Details for defaulting indicator */
		annyIO.setChdrcoy(regpIO.getChdrcoy());
		annyIO.setChdrnum(regpIO.getChdrnum());
		annyIO.setLife(regpIO.getLife());
		annyIO.setCoverage(regpIO.getCoverage());
		annyIO.setRider(regpIO.getRider());
		annyIO.setPlanSuffix(regpIO.getPlanSuffix());
		annyIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), varcom.oK) && isNE(annyIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isEQ(annyIO.getStatuz(), varcom.oK)) {
			sv.anntind.set("+");
		} else {
			sv.anntind.set(" ");
			sv.anntindOut[varcom.pr.toInt()].set("Y");
		}
	}

	protected void readTr5177000() {
		para7100();
	}

	protected void para7100() {
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr517);
		/* MOVE COVR-CRTABLE TO ITDM-ITEMITEM. <LA2110> */
		/* MOVE COVR-CRRCD TO ITDM-ITMFRM. <LA2110> */
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(wsaaCovrCrrcd);
		itdmIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK) && isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company) || isNE(itdmIO.getItemtabl(), tablesInner.tr517)
				|| isNE(itdmIO.getItemitem(), wsaaCrtable) || isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaIsWop = "N";
		} else {
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
			wsaaIsWop = "Y";
		}
	}

	protected void readPayr8000() {
		para8100();
	}

	protected void para8100() {
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		payrIO.setChdrnum(chdrrgpIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFunction(varcom.begn);
		// performance improvement -- Niharika Modi
		payrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK) && isNE(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		if (isNE(payrIO.getChdrcoy(), chdrrgpIO.getChdrcoy()) || isNE(payrIO.getChdrnum(), chdrrgpIO.getChdrnum())
				|| isEQ(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrrgpIO.getChdrnum());
			syserrrec.statuz.set(errorsInner.e540);
			fatalError600();
		}
		wsaaPayrBillfreq.set(payrIO.getBillfreq());
		wsaaPayrBillfreqNum.set(wsaaPayrBillfreq);
	}

	protected void accidentBenefitClaims9180() {
		accidentClaims9181();
	}

	protected void accidentClaims9181() {
		/* Check for any Accident Claim Benefit. */
		aclhIO.setDataKey(SPACES);
		aclhIO.setChdrcoy(regpIO.getChdrcoy());
		aclhIO.setChdrnum(regpIO.getChdrnum());
		aclhIO.setLife(regpIO.getLife());
		aclhIO.setCoverage(regpIO.getCoverage());
		aclhIO.setRider(regpIO.getRider());
		aclhIO.setRgpynum(regpIO.getRgpynum());
		aclhIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aclhIO);
		if (isNE(aclhIO.getStatuz(), varcom.oK) && isNE(aclhIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aclhIO.getStatuz());
			syserrrec.params.set(aclhIO.getParams());
			fatalError600();
		}
		if (isEQ(aclhIO.getStatuz(), varcom.oK)) {
			sv.pymt.set(aclhIO.getTclmamt());
			sv.agclmstz.set("+");
		} else {
			sv.agclmstz.set("X");
		}
	}

	/**
	 * <pre>
	*                                                         <V71L10>
	 * </pre>
	 */
	protected void accidentBenefitClaims9190() {
		accidentClaims9191();
	}

	protected void accidentClaims9191() {
		/* Check for any Accident Claim Benefit. */
		aclhIO.setDataKey(SPACES);
		aclhIO.setChdrcoy(regpIO.getChdrcoy());
		aclhIO.setChdrnum(regpIO.getChdrnum());
		aclhIO.setLife(regpIO.getLife());
		aclhIO.setCoverage(regpIO.getCoverage());
		aclhIO.setRider(regpIO.getRider());
		aclhIO.setRgpynum(regpIO.getRgpynum());
		aclhIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aclhIO);
		if (isNE(aclhIO.getStatuz(), varcom.oK) && isNE(aclhIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aclhIO.getStatuz());
			syserrrec.params.set(aclhIO.getParams());
			fatalError600();
		}
		if (isEQ(aclhIO.getStatuz(), varcom.oK)) {
			sv.pymt.set(aclhIO.getTclmamt());
			sv.agclmstz.set("+");
			regpIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
			/* Release the Regular Payment Record. */
			regpIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, regpIO);
			if (isNE(regpIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(regpIO.getParams());
				fatalError600();
			}
		} else {
			sv.agclmstz.set(SPACES);
		}
	}

	protected void a000CallRounding() {
		/* A100-CALL */
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.claimcur);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/* A900-EXIT */
	}

	/*
	 * Class transformed from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner {
		/* ERRORS */
		private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
		private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
		private FixedLengthStringData e335 = new FixedLengthStringData(4).init("E335");
		private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
		private FixedLengthStringData e492 = new FixedLengthStringData(4).init("E492");
		private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
		private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
		private FixedLengthStringData g126 = new FixedLengthStringData(4).init("G126");
		private FixedLengthStringData g514 = new FixedLengthStringData(4).init("G514");
		private FixedLengthStringData g515 = new FixedLengthStringData(4).init("G515");
		private FixedLengthStringData g516 = new FixedLengthStringData(4).init("G516");
		private FixedLengthStringData g517 = new FixedLengthStringData(4).init("G517");
		private FixedLengthStringData g518 = new FixedLengthStringData(4).init("G518");
		private FixedLengthStringData g519 = new FixedLengthStringData(4).init("G519");
		private FixedLengthStringData g520 = new FixedLengthStringData(4).init("G520");
		private FixedLengthStringData g521 = new FixedLengthStringData(4).init("G521");
		private FixedLengthStringData g522 = new FixedLengthStringData(4).init("G522");
		private FixedLengthStringData g527 = new FixedLengthStringData(4).init("G527");
		private FixedLengthStringData g528 = new FixedLengthStringData(4).init("G528");
		private FixedLengthStringData g529 = new FixedLengthStringData(4).init("G529");
		private FixedLengthStringData g530 = new FixedLengthStringData(4).init("G530");
		private FixedLengthStringData g534 = new FixedLengthStringData(4).init("G534");
		private FixedLengthStringData g537 = new FixedLengthStringData(4).init("G537");
		private FixedLengthStringData g538 = new FixedLengthStringData(4).init("G538");
		private FixedLengthStringData g539 = new FixedLengthStringData(4).init("G539");
		private FixedLengthStringData g540 = new FixedLengthStringData(4).init("G540");
		private FixedLengthStringData h118 = new FixedLengthStringData(4).init("H118");
		private FixedLengthStringData h132 = new FixedLengthStringData(4).init("H132");
		private FixedLengthStringData h134 = new FixedLengthStringData(4).init("H134");
		private FixedLengthStringData h136 = new FixedLengthStringData(4).init("H136");
		private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
		private FixedLengthStringData curs = new FixedLengthStringData(4).init("CURS");
		private FixedLengthStringData e493 = new FixedLengthStringData(4).init("E493");
		private FixedLengthStringData e540 = new FixedLengthStringData(4).init("E540");
		private FixedLengthStringData f665 = new FixedLengthStringData(4).init("F665");
		private FixedLengthStringData p132 = new FixedLengthStringData(4).init("P132");
		private FixedLengthStringData t058 = new FixedLengthStringData(4).init("T058");
		private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
		private FixedLengthStringData rlbd = new FixedLengthStringData(4).init("RLBD");
		private FixedLengthStringData rrkv = new FixedLengthStringData(4).init("RRKV");
		private FixedLengthStringData zp31 = new FixedLengthStringData(4).init("ZP31");//ILIFE-8299
		private FixedLengthStringData g882 = new FixedLengthStringData(4).init("G882");//ILIFE-8299
		private FixedLengthStringData rreq = new FixedLengthStringData(4).init("RREQ");//ILIFE-8299
		private FixedLengthStringData rfo2 = new FixedLengthStringData(4).init("RFO2");//ILIFE-8299	
		private FixedLengthStringData rrsn = new FixedLengthStringData(4).init("RRSN");
		private FixedLengthStringData rfuy = new FixedLengthStringData(4).init("RFUY");
		private FixedLengthStringData jl25 = new FixedLengthStringData(4).init("JL25");//ILJ-48
		private FixedLengthStringData rusz = new FixedLengthStringData(4).init("RUSZ");
	}

	/*
	 * Class transformed from Data Structure TABLES--INNER
	 */
	private static final class TablesInner {
		/* TABLES */
		private FixedLengthStringData t1688 = new FixedLengthStringData(5).init("T1688");
		private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
		private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
		private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
		private FixedLengthStringData t3629 = new FixedLengthStringData(5).init("T3629");
		// ICIL-556
		private FixedLengthStringData t5500 = new FixedLengthStringData(5).init("T5500");
		private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
		private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
		private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
		private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
		private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
		private FixedLengthStringData t5400 = new FixedLengthStringData(5).init("T5400");
		private FixedLengthStringData t6625 = new FixedLengthStringData(5).init("T6625");
		private FixedLengthStringData t6690 = new FixedLengthStringData(5).init("T6690");
		private FixedLengthStringData t6691 = new FixedLengthStringData(5).init("T6691");
		private FixedLengthStringData t6692 = new FixedLengthStringData(5).init("T6692");
		private FixedLengthStringData t6693 = new FixedLengthStringData(5).init("T6693");
		private FixedLengthStringData t6694 = new FixedLengthStringData(5).init("T6694");
		private FixedLengthStringData t6696 = new FixedLengthStringData(5).init("T6696");
		private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
		private FixedLengthStringData tr585 = new FixedLengthStringData(5).init("TR585");
	}

	/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		/* FORMATS */
		private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
		private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
		private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC   ");
		private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC   ");
		private FixedLengthStringData regprec = new FixedLengthStringData(10).init("REGPREC   ");
		private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	}

	protected void updateDatabaseCustomerSpecific() {

	}

	protected void beneficiaryCustomerSpecific4500() {

	}

	protected void updateCustomerSpecificFields(String chdrcoy, String chdrnum) {

	}
	
	//ILIFE-8299-START	
	protected void loadAssignee1020() {
		wsaaNoMoreAssignees.set("Y");
		asgnenqIO.setDataArea(SPACES);
		asgnenqIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		asgnenqIO.setChdrnum(chdrrgpIO.getChdrnum());
		asgnenqIO.setSeqno(ZERO);
		asgnenqIO.setFunction(varcom.begn);
		asgnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		asgnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		loadAssignee5000();
	}
	protected void loadAssignee5000(){
		try {
			assigneeRead5010();
			asgnToScreen5030();
		 }
		catch (GOTOException e){
		}
	}
	protected void assigneeRead5010(){
		SmartFileCode.execute(appVars, asgnenqIO);
		if (isNE(asgnenqIO.getStatuz(),varcom.oK)
		&& isNE(asgnenqIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit4090);
		}
		/*END-OF-FILE*/
		if (isNE(asgnenqIO.getChdrcoy(),chdrrgpIO.getChdrcoy())
		|| isNE(asgnenqIO.getChdrnum(),chdrrgpIO.getChdrnum())
		|| isEQ(asgnenqIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit4090);
		}
	}

    protected void asgnToScreen5030(){
		if (isLTE(asgnenqIO.getCommfrom(),datcon1rec.intDate)
			&& (isEQ(asgnenqIO.getCommto(),datcon1rec.intDate) || isGT(asgnenqIO.getCommto(),datcon1rec.intDate))) {
			wsaaNoMoreAssignees.set("N");
			checkAsgn5040();
			goTo(GotoLabel.exit4090);
		}
		asgnenqIO.setFunction(varcom.nextr);
		loadAssignee5000();
	}
    protected void checkAsgn5040() {
		if(!asgnenqEof.isTrue()) {
			sv.filllErr.set(errorsInner.rreq);
		}
	}
    
    protected void setContStatus() {

        livclmrec.transEffdate.set(chdrrgpIO.getOccdate());
        livclmrec.system.set("BASE");
        livclmrec.cnttype.set(chdrrgpIO.getCnttype());       
        livclmrec.covcde.set(regpIO.getCrtable());
        livclmrec.trancde.set(wsaaBatckey.batcBatctrcde);
        livclmrec.percent.set(sv.prcnt.toFloat());
        if ((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("LIVCLM")
                                     && er.isExternalized(livclmrec.cnttype.toString(), null))) {
                                     callProgram("LIVCLM", livclmrec);
        }
        if (isNE(livclmrec.waiver.trim(), "N") && isNE(livclmrec.waiver.trim(), "Y")) {
                       fatalError600();
        }
        if (isEQ(livclmrec.waiver, "Y")) {                      
        	chdrlifIO.setStatcode(livclmrec.setriskstat);
            chdrlifIO.setPstatcode(livclmrec.setpremstat);
        }
	}
	
	protected void i900UpdateCovr() {

		try {
			i901Para();
	 	}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void i901Para()
	{
		/* Find todays date*/
		datcon1rec.function.set(varcom.tday);

		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, "****")) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		//	Read VPMS PASS TRANSCODE+REGPIO.GETCRTABLE();
		livclmrec.transEffdate.set(chdrrgpIO.getOccdate());
		livclmrec.system.set("BASE");
		livclmrec.cnttype.set(chdrrgpIO.getCnttype());		
		livclmrec.covcde.set(regpIO.getCrtable());
		livclmrec.trancde.set(wsaaBatckey.batcBatctrcde);
		livclmrec.percent.set(sv.prcnt.toFloat());
		if ((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("LIVCLM")
				&& er.isExternalized(livclmrec.cnttype.toString(), null))) {
			callProgram("LIVCLM", livclmrec);
		}
		if (isNE(livclmrec.waiver.trim(), "N") && isNE(livclmrec.waiver.trim(), "Y")) {
			fatalError600();
		}
		if (isEQ(livclmrec.waiver, "N")) {
			J100UpdateCover();
		}
		if (isEQ(livclmrec.waiver, "Y")) {
			//  deductPremium();
			k200UpdateAllCover();
		}

	}

	protected void J100UpdateCover() {
		if (isEQ(livclmrec.lastpaydate, "N")) {
			getAccumRegpJ2000();
		}
		if (isEQ(livclmrec.lastpaydate, "Y")) {
			J200UpdateComponent();
		}
	 }

	protected void k200UpdateAllCover() {

		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					parak2010();
				case callK2050:
					callk2050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
		}

	protected void getAccumRegpJ2000() {

		wsaaPymt.set(ZERO);
		wsaaPymt2.set(ZERO);
		wsaaFreq.set(regpIO.getRegpayfreq());
		regpenqIO.setChdrcoy(regpIO.getChdrcoy());
		regpenqIO.setChdrnum(regpIO.getChdrnum());
		regpenqIO.setLife(regpIO.getLife());
		regpenqIO.setCoverage(regpIO.getCoverage());
		regpenqIO.setRider(regpIO.getRider());
		regpenqIO.setRgpynum(99999);
		regpenqIO.setStatuz(varcom.oK);
		regpenqIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		regpenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		wsaaTotPymt.set(ZERO);
		while ( !(isEQ(regpenqIO.getStatuz(), varcom.endp))) {
			accumRegp2500();
		}
		compute(wsaaTotPymt, 2).set(add(wsaaTotPymt, sv.pymt));
		if(isNE(livclmrec.benefitvalue, ZERO)) {
			if(isGTE(wsaaTotPymt,wsbbSumins)) {
				J200UpdateComponent();
				return;
			}
		}
	}

	protected void accumRegpJ2500() {
		accumJ2510();
	}

	protected void accumJ2510()
		{
			SmartFileCode.execute(appVars, regpenqIO);
			if ((isNE(regpenqIO.getStatuz(), varcom.oK))
			&& (isNE(regpenqIO.getStatuz(), varcom.endp))) {
				syserrrec.params.set(regpenqIO.getParams());
				fatalError600();
			}
			if (isEQ(regpenqIO.getStatuz(), varcom.endp)) {
				return ;
			}
			if ((isEQ(regpIO.getChdrcoy(), regpenqIO.getChdrcoy()))
			&& (isEQ(regpIO.getChdrnum(), regpenqIO.getChdrnum()))
			&& (isEQ(regpIO.getLife(), regpenqIO.getLife()))
			&& (isEQ(regpIO.getCoverage(), regpenqIO.getCoverage()))
			&& (isEQ(regpIO.getRider(), regpenqIO.getRider()))) {
				if (isEQ(sv.cltype, regpenqIO.getPayreason())) {
					if (isNE(regpIO.getRgpynum(), regpenqIO.getRgpynum())) {
						wsaaOverlap = "N";
						checkOverlap2510();
						if (isEQ(wsaaOverlap, "Y")) {
							wsaaOverlap = "N";
							wsaaFreq2.set(regpenqIO.getRegpayfreq());
							wsaaPymt2.set(regpenqIO.getPymt());
							if (isNE(wsaaFreq2, ZERO)) {
								compute(wsaaPymt2, 2).set((div((mult(wsaaPymt2, wsaaFreq2)), wsaaFreq)));
								zrdecplrec.amountIn.set(wsaaPymt2);
								a000CallRounding();
								wsaaPymt2.set(zrdecplrec.amountOut);
								compute(wsaaPymt, 2).set(add(wsaaPymt, wsaaPymt2));
							}
						}
					}
				}
			}
			else {
				regpenqIO.setStatuz(varcom.endp);
			}
			if ((isEQ(regpIO.getChdrcoy(), regpenqIO.getChdrcoy()))
			&& (isEQ(regpIO.getChdrnum(), regpenqIO.getChdrnum()))
			&& (isEQ(regpIO.getLife(), regpenqIO.getLife()))
			&& (isEQ(regpIO.getCoverage(), regpenqIO.getCoverage()))
			&& (isEQ(regpIO.getRider(), regpenqIO.getRider()))) {
				if (isNE(regpIO.getRgpynum(), regpenqIO.getRgpynum())) {
					wsaaOverlap = "N";
					checkOverlap2510();
					if (isEQ(wsaaOverlap, "Y")) {
						wsaaOverlap = "N";
						wsaaFreq2.set(regpenqIO.getRegpayfreq());
						wsaaPymt2.set(regpenqIO.getPymt());
				        	if (isEQ(wsaaFreq2, ZERO)) {
							wsaaFreq2.set(1);
						}
						if (isEQ(wsaaFreq, ZERO)) {
							wsaaFreq.set(1);
						}
				        	if (isNE(wsaaFreq2, ZERO)) {
							compute(wsaaPymt2, 2).set((div((mult(wsaaPymt2, wsaaFreq2)), wsaaFreq)));
							zrdecplrec.amountIn.set(wsaaPymt2);
							a000CallRounding();
							wsaaPymt2.set(zrdecplrec.amountOut);
							wsaaTotPymt.add(wsaaPymt2);
						}
					}
				}
			}
			regpenqIO.setFunction(varcom.nextr);
		}

	/*protected void checkOverlap2510()
	{
		if (isEQ(regpenqIO.getCancelDate(), varcom.vrcmMaxDate)) {
			if ((isGT(sv.firstPaydate, regpenqIO.getFinalPaydate()))
			|| (isLT(sv.finalPaydate, regpenqIO.getFirstPaydate()))) {
				wsaaOverlap = "N";
			}
			else {
				wsaaOverlap = "Y";
			}
		}
		else {
			wsaaOverlap = "N";
		}
		EXIT
	}*/

	protected void J200UpdateComponent() {

		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(regpIO.getChdrcoy());
		covrIO.setChdrnum(regpIO.getChdrnum());
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		covrIO.setValidflag("2");
		covrIO.setCurrto(datcon1rec.intDate);
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);

		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		covrIO.setStatcode(livclmrec.setriskstat);
		covrIO.setPstatcode(livclmrec.setpremstat);
		covrIO.setValidflag("1");
		covrIO.setCurrfrom(datcon1rec.intDate);
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setTranno(chdrlifIO.getTranno());
		covrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		deductPremium();


	}

	protected void parak2010()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(regpIO.getChdrcoy());
		covrIO.setChdrnum(regpIO.getChdrnum());
		covrIO.setFunction(varcom.begn);
//		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");
	}

	protected void callk2050() {

		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrIO.getStatuz(), varcom.endp)) {
			return ;
		}

		if ((isNE(covrIO.getChdrcoy(), regpIO.getChdrcoy())) || (isNE(covrIO.getChdrnum(), regpIO.getChdrnum()))) {
			covrIO.setStatuz(varcom.endp);
			return;
		}

		if (isNE(covrIO.getValidflag(), "1")) {
			covrIO.setFunction(varcom.nextr);
			goTo(GotoLabel.callK2050);
		}
	covrIO.setValidflag("2");
	covrIO.setCurrto(datcon1rec.intDate);
	covrIO.setFunction(varcom.rewrt);
	SmartFileCode.execute(appVars, covrIO);
	if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}


	wsbbCrtable = regpIO.getCrtable().toString();
	if (isEQ(covrIO.getCrtable(), wsbbCrtable)) {		
		covrIO.setStatcode(livclmrec.setriskstatall);
		covrIO.setPstatcode(livclmrec.setpremstatall);
	}
	if (isNE(covrIO.getCrtable(), wsbbCrtable)) {
		if (isEQ(covrIO.getStatcode(), livclmrec.checkriskstat)) {
			covrIO.setStatcode(livclmrec.setriskstatall);
		}
		if (isEQ(covrIO.getPstatcode(), livclmrec.checkpremstat)) {
			covrIO.setPstatcode(livclmrec.setpremstatall);
		}
	}



		covrIO.setValidflag("1");
		covrIO.setCurrfrom(datcon1rec.intDate);
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setTranno(chdrlifIO.getTranno());
		covrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		covrIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callK2050);
	}

	
	protected void deductPremium()
	{
		chdrlifIO.setChdrpfx(chdrrgpIO.getChdrpfx());
		chdrlifIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		chdrlifIO.setChdrnum(chdrrgpIO.getChdrnum());
		chdrlifIO.setValidflag(chdrrgpIO.getValidflag());
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}

		setPrecision(chdrlifIO.getSinstamt01(), 2);
		chdrlifIO.setSinstamt01(sub(chdrlifIO.getSinstamt01(),covrIO.getInstprem()));

		setPrecision(chdrlifIO.getSinstamt06(), 2);
		chdrlifIO.setSinstamt06((add(add(add(add(chdrlifIO.getSinstamt01(), chdrlifIO.getSinstamt02()), chdrlifIO.getSinstamt03()), chdrlifIO.getSinstamt04()), chdrlifIO.getSinstamt05())));

		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}

		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		payrIO.setChdrnum(chdrrgpIO.getChdrnum());
		payrIO.setValidflag("1");
		payrIO.setPayrseqno(wsaaPayrseqno);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getStatuz());
			fatalError600();
		}

		setPrecision(payrIO.getSinstamt01(), 2);
		payrIO.setSinstamt01(sub(payrIO.getSinstamt01(),covrIO.getInstprem()));

		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06((add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()), payrIO.getSinstamt04()), payrIO.getSinstamt05())));

		payrIO.setFunction(varcom.rewrt);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

	protected void checkcltype2041() {

		// if Claim type is spaces then return.

		 if (isEQ(sv.cltype, " ")) {
		    return;
		 }

		 // if the WSSPCOMN flag is not = 'C' or 'M', then exit.

		 if (isNE(wsspcomn.flag, "C")
		 && (isNE(wsspcomn.flag, "M"))) {
			return;
		 }

		// if the coverage code is VPTD, then perform the validations.

		 if (isEQ(sv.crtable, "VPTD")) {
			regpenqIO.initialize();
			regpenqIO.setChdrcoy(regpIO.getChdrcoy());
			regpenqIO.setChdrnum(regpIO.getChdrnum());
			regpenqIO.setLife(regpIO.getLife());
			regpenqIO.setCoverage(regpIO.getCoverage());
			regpenqIO.setRider(regpIO.getRider());
			regpenqIO.setRgpynum(99999);
			regpenqIO.setStatuz(varcom.oK);
			regpenqIO.setFunction(varcom.begn);

			regpenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			regpenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");

			while ( !(isEQ(regpenqIO.getStatuz(), varcom.endp))) {
				validateCltype2500();
		 }

		 //Check the initial validations on the claim type.
		 //if there are validation errors, then skip validating the dates.

		 if(isEQ(sv.cltype, "SP")) {
		   if(isEQ(isFPPresent, false))
			 {
			  sv.cltypeErr.set(errorsInner.zp31);
			  wsspcomn.edterror.set("Y");
			  isValidationerror = true;
			  }
		   }


		 if(isEQ(sv.cltype, "T1")
		 &&(isEQ(isSPPresent, false))
		 &&(isEQ(isFPPresent, false)))
		 {
		   if(isEQ(isFPPresent, false)) {
		   sv.cltypeErr.set(errorsInner.zp31);
		   wsspcomn.edterror.set("Y");
		   isValidationerror = true;
		   }
		 }

		 if (isEQ(isValidationerror, true)) {
		 return;
		 }

		 //When there are no validation errors, then perform the calculation
		 //of dates for the Second pay and the Third pay


		 if (isEQ(sv.cltype, "SP")
			|| (isEQ(sv.cltype, "T1"))) {
		    	regpenqIO.initialize();
				regpenqIO.setChdrcoy(regpIO.getChdrcoy());
				regpenqIO.setChdrnum(regpIO.getChdrnum());
				regpenqIO.setLife(regpIO.getLife());
				regpenqIO.setCoverage(regpIO.getCoverage());
				regpenqIO.setRider(regpIO.getRider());
				regpenqIO.setRgpynum(99999);
				regpenqIO.setStatuz(varcom.oK);
				regpenqIO.setFunction(varcom.begn);

				regpenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				regpenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
				while ( !(isEQ(regpenqIO.getStatuz(), varcom.endp))) {
					calculatePaydate2501();
		      }
		    }
		  }
		}

		protected void validateCltype2500() {
			 validateCltype2501();
		}


		 protected void validateCltype2501() {

		 //Call the REGPENQIO

		 SmartFileCode.execute(appVars, regpenqIO);

		 if ((isNE(regpenqIO.getStatuz(), varcom.oK))
		 && (isNE(regpenqIO.getStatuz(), varcom.endp)))
		 {
			syserrrec.params.set(regpenqIO.getParams());
			fatalError600();
		 }

		 //If REGPENQ records are not matched with REGP, then set the statuz to ENDP.
		 if (isNE(regpenqIO.getChdrcoy(), regpIO.getChdrcoy())
		 && (isNE(regpenqIO.getChdrnum(), regpIO.getChdrnum()))
		 && (isNE(regpenqIO.getLife(), regpIO.getLife()))
		 && (isNE(regpenqIO.getCoverage(), regpIO.getCoverage()))
		 && (isNE(regpenqIO.getRider(), regpIO.getRider()))
		 && (isEQ(regpenqIO.getStatuz(), varcom.endp))) {
			regpenqIO.setStatuz(varcom.endp);
			return;
		 }

		 //If REGPQNQ statuz is ENDP, then exit. Else set the REGPENQ function to NEXTR.
		 if (isEQ(regpenqIO.getStatuz(), varcom.endp)) {
			return ; }
		 else {
			regpenqIO.setFunction(varcom.nextr);
		 }

		 // if the screen Claim type is 'FP' or 'SP' or 'T1', and REGPENQ already hold the same
		 // reason, then set duplicate claim error.

		 if (isEQ(sv.cltype, "FP")
		 && (isEQ(regpenqIO.getPayreason(), "FP"))
		 && (isNE(wsspcomn.flag, "M"))) {
		  sv.filllErr.set(errorsInner.g882);
		  isValidationerror = true;
		  return;
		 }

		if (isEQ(sv.cltype, "SP")
		&& (isEQ(regpenqIO.getPayreason(), "SP"))
		&& (isNE(wsspcomn.flag, "M"))) {
			sv.filllErr.set(errorsInner.g882);
			isValidationerror = true;
			return;
		 }

		 if (isEQ(sv.cltype, "T1")
		 && (isEQ(regpenqIO.getPayreason(), "T1"))
		 && (isNE(wsspcomn.flag, "M"))) {
		 sv.filllErr.set(errorsInner.g882);
		 isValidationerror = true;
		 return;
		 }

		 //if the current claim is 'SP', then there should be an  'FP' claim already done on the
		//policy.

		 if (isEQ(sv.cltype, "SP")
		 && (isEQ(isFPPresent, false))) {
		 if (isEQ(regpenqIO.getPayreason(), "FP")) {
		     isFPPresent=true;
			 regpenqIO.setStatuz(varcom.endp);
			 return;
		   }
		 }

		 if(isEQ(sv.cltype, "T1")
		 && (isEQ(regpenqIO.getPayreason(), "FP"))) {
			isFPPresent = true;
			regpenqIO.setStatuz(varcom.endp);
			return;
		 }

		 if(isEQ(sv.cltype, "T1")
		 && (isEQ(regpenqIO.getPayreason(), "SP"))) {
		   isSPPresent = true;
		   regpenqIO.setStatuz(varcom.endp);
		   return;
		   }
		 }

		protected void calculatePaydate2501()
		{
			 calculatePaydate2502();
		}

		 protected void calculatePaydate2502()
		 {

		 SmartFileCode.execute(appVars, regpenqIO);
		 if ((isNE(regpenqIO.getStatuz(), varcom.oK))
		 && (isNE(regpenqIO.getStatuz(), varcom.endp)))
		 {
		 syserrrec.params.set(regpenqIO.getParams());
		 fatalError600();
		 }

		 //If REGPENQ records are not matched with REGP, then set the statuz to ENDP.
		 if (isNE(regpenqIO.getChdrcoy(), regpIO.getChdrcoy())
		 && (isNE(regpenqIO.getChdrnum(), regpIO.getChdrnum()))
		 && (isNE(regpenqIO.getLife(), regpIO.getLife()))
		 && (isNE(regpenqIO.getCoverage(), regpIO.getCoverage()))
		 && (isNE(regpenqIO.getRider(), regpIO.getRider()))
		 && (isEQ(regpenqIO.getStatuz(), varcom.endp)))	{
			regpenqIO.setStatuz(varcom.endp);
			return;
		 }

		//If REGPQNQ statuz is ENDP, then exit. Else set the REGPENQ function to NEXTR.
		 if (isEQ(regpenqIO.getStatuz(), varcom.endp)) {
			return ; }
		 else {
			regpenqIO.setFunction(varcom.nextr);
		 }


		 if (isEQ(regpenqIO.getPayreason(), "FP")
		 && (isEQ(sv.cltype, "SP")))
		 {
		    datcon2rec.initialize();
			datcon2rec.freqFactor.set(6);
			datcon2rec.frequency.set("12");
			datcon2rec.intDate1.set(regpenqIO.getFirstPaydate());
			datcon2rec.intDate2.set(ZERO);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);

		 if (isNE(datcon2rec.statuz, varcom.oK)) {
		    syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		 }

		 if(!isGT(sv.firstPaydate, datcon2rec.intDate2)) {
		   sv.cltypeErr.set(errorsInner.rfo2);
		   wsspcomn.edterror.set("Y");
		   return;
		   }
		 }


		 if (isEQ(regpenqIO.getPayreason(), "SP")
		 && (isEQ(sv.cltype, "T1")))
		 {
		 datcon2rec.initialize();
		 datcon2rec.freqFactor.set(12);
		 datcon2rec.frequency.set("12");
		 datcon2rec.intDate1.set(regpenqIO.getFirstPaydate());
		 datcon2rec.intDate2.set(ZERO);
		 callProgram(Datcon2.class, datcon2rec.datcon2Rec);

		 if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		 }


		 if(!isGT(sv.firstPaydate, datcon2rec.intDate2)) {
		    sv.cltypeErr.set(errorsInner.rfo2);
		    wsspcomn.edterror.set("Y");
		    return;
		     }
		   }
		 }

	
	protected void validateAusFollow2079()
	{
		/* Validate Follow-Up Indicator.*/
		if ((isNE(sv.fupflg, "+"))
		&& (isNE(sv.fupflg, "X"))
		&& (isNE(sv.fupflg, SPACES))) {
			sv.fupflgErr.set(errorsInner.h118);
		}
		/* Validate Annuity Details Indicator*/
		if ((isNE(sv.anntind, "+"))
		&& (isNE(sv.anntind, "X"))
		&& (isNE(sv.anntind, SPACES))) {
			sv.anntindErr.set(errorsInner.h118);
		}
		/* Validate Bank Details Indicator.*/
		if ((isNE(sv.ddind, "+"))
		&& (isNE(sv.ddind, "X"))
		&& (isNE(sv.ddind, SPACES))) {
			sv.ddindErr.set(errorsInner.h118);
		}
		/* Validate Accident Indicator.                                    */
		if ((isNE(sv.agclmstz, "+"))
		&& (isNE(sv.agclmstz, "X"))
		&& (isNE(sv.agclmstz, SPACES))) {
			sv.agclmstzErr.set(errorsInner.h118);
		}
		if ((isNE(wsaaPayclt, SPACES))
		&& (isNE(sv.payclt, SPACES))) {
			if (isNE(wsaaPayclt, sv.payclt)) {
				if (isEQ(t6694rec.bankreq, "Y")) {
					regpIO.setBankkey(SPACES);
					regpIO.setBankacckey(SPACES);
					sv.ddind.set("X");
				}
				else {
					if (isEQ(t6694rec.bankreq, "N")
					|| isEQ(t6694rec.bankreq, " ")) {
						regpIO.setBankkey(SPACES);
						regpIO.setBankacckey(SPACES);
						sv.ddind.set(SPACES);
					}
				}
			}
		}
		if ((isEQ(t6694rec.bankreq, "Y"))
		&& (isEQ(sv.ddind, SPACES))) {
			sv.ddind.set("X");
		}
		/*  Accumulate all claims (REGPENQ) which do not have a*/
		/*  status of terminated and have an overlapping period*/
		/*  with the new claim. We will not attempt to do an*/
		/*  accumulation if this is the first claim on the contract.*/
		/*  The accumulation ONLY takes into account claims on this*/
		/*  component that have the same Reason Code.*/
		if ((isGT(sv.rgpynum, 1))
		&& (isNE(wsspcomn.flag, "I"))) {
			wsaaPymt.set(ZERO);
			wsaaPymt2.set(ZERO);
			regpenqIO.setChdrcoy(regpIO.getChdrcoy());
			regpenqIO.setChdrnum(regpIO.getChdrnum());
			regpenqIO.setLife(regpIO.getLife());
			regpenqIO.setCoverage(regpIO.getCoverage());
			regpenqIO.setRider(regpIO.getRider());
			regpenqIO.setRgpynum(99999);
			regpenqIO.setStatuz(varcom.oK);
			regpenqIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi
			regpenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			regpenqIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");

			wsaaTotPymt.set(ZERO);
			while ( !(isEQ(regpenqIO.getStatuz(), varcom.endp))) {
				/*    COMPUTE WSAA-PYMT = WSAA-PYMT + S6677-PYMT                */
				/*    COMPUTE WSAA-TOT-PYMT = WSAA-TOT-PYMT + S6677-PYMT   <006>*/
				accumRegp2500();
			}
	        //ILIFE-1195 STARTS
			//if (isNE(sv.regpayfreq, "00")) {
				compute(wsaaPymt, 2).set(add(wsaaPymt, sv.pymt));
				compute(wsaaTotPymt, 2).set(add(wsaaTotPymt, sv.pymt));
			//}
		        //ILIFE-1195 ENDS
		}
		else {
			/*    MOVE S6677-PYMT            TO WSAA-PYMT                   */		
				wsaaPymt.set(sv.pymt);
				wsaaTotPymt.set(sv.pymt); 		
		}
		/* Obtain the maximum override percentage value*/
		/*COMPUTE WSAA-MAX-PRCNT-VALUE   =                             */
		/*                  (WSAA-SUMINS2 * T6696-MXOVRPCT  / 100).    */
		/* COMPUTE WSAA-MAX-PRCNT-VALUE   =                        <006>*/
		/*                   (WSAA-SUMINS * T6696-MXOVRPCT  / 100).<006>*/
		compute(wsaaMaxPrcntValue, 2).set((div(mult(wsaaSumins2, t6696rec.mxovrpct), 100)));
		/* Check if the total claim sum in the period specified*/
		/*  is higher than the maximum override percentage value*/
		if (isGT(wsaaPymt, wsaaMaxPrcntValue)) {
			if (isEQ(wsaaIsWop, "Y")
			&& isEQ(tr517rec.zrwvflg01, "Y")) {
				/*NEXT_SENTENCE*/
			}
			else {
				sv.totalamt.set(wsaaPymt);
				/*   MOVE G527                  TO S6677-TOTALAMT-ERR          */
				sv.totalamtErr.set(errorsInner.g515);
				sv.msgclaim.set(wsaaClaimReasMess);
				sv.msgclaimOut[varcom.nd.toInt()].set(SPACES);
				sv.totalamtOut[varcom.nd.toInt()].set(SPACES);
				//goTo(GotoLabel.checkForErrors2080);
			}
		}
		else {
			sv.totalamtOut[varcom.nd.toInt()].set("Y");
			sv.msgclaimOut[varcom.nd.toInt()].set("Y");
		}
		/* Check that the total Sum of all claims is not greater           */
		/* than the sum insured.                                           */
		/* IF WSAA-TOT-PYMT               > WSAA-SUMINS            <006>*/
		/* Adding additional Validation from VPMS to check if Max Valid livclmrec.Maxvalid is Y or ' '. 
		 Therefore Commenting the changes and include the validation only for VAMR*/		
		if (isGT(wsaaTotPymt, wsaaSumins2)) {
			if (isEQ(wsaaIsWop, "Y")
			&& isEQ(tr517rec.zrwvflg01, "Y")) {
				/*NEXT_SENTENCE*/
			}
			else {
				sv.totalamt.set(wsaaTotPymt);				
				if (isEQ(livclmrec.Maxvalid, " ")) {
					sv.totalamtErr.set(errorsInner.g527);
				}
				sv.msgclaim.set(wsaaClaimCompMess);
				sv.msgclaimOut[varcom.nd.toInt()].set(SPACES);
				sv.totalamtOut[varcom.nd.toInt()].set(SPACES);
			}
		}
		else {
			sv.totalamtOut[varcom.nd.toInt()].set("Y");
			sv.msgclaimOut[varcom.nd.toInt()].set("Y");
		}
	}
	//ILIFE-8299 : END
	protected void  protectScr2410CustomerSpecific(){
		
	}
	protected void 	updateRegpayCltCustomerSpecific3010(){
		
	}
	protected void validate2021CustomerSPecific(){
		
	}
	//MLIL-1938
	protected void checkLottery() {}
}
