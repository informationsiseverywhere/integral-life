package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:21:53
 * Description:
 * Copybook name: UTRSMATKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Utrsmatkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData utrsmatFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData utrsmatKey = new FixedLengthStringData(256).isAPartOf(utrsmatFileKey, 0, REDEFINE);
  	public FixedLengthStringData utrsmatChdrcoy = new FixedLengthStringData(1).isAPartOf(utrsmatKey, 0);
  	public FixedLengthStringData utrsmatChdrnum = new FixedLengthStringData(8).isAPartOf(utrsmatKey, 1);
  	public FixedLengthStringData utrsmatCoverage = new FixedLengthStringData(2).isAPartOf(utrsmatKey, 9);
  	public FixedLengthStringData utrsmatRider = new FixedLengthStringData(2).isAPartOf(utrsmatKey, 11);
  	public FixedLengthStringData utrsmatUnitVirtualFund = new FixedLengthStringData(4).isAPartOf(utrsmatKey, 13);
  	public FixedLengthStringData utrsmatUnitType = new FixedLengthStringData(1).isAPartOf(utrsmatKey, 17);
  	public PackedDecimalData utrsmatPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(utrsmatKey, 18);
  	public FixedLengthStringData utrsmatLife = new FixedLengthStringData(2).isAPartOf(utrsmatKey, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(233).isAPartOf(utrsmatKey, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(utrsmatFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		utrsmatFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}