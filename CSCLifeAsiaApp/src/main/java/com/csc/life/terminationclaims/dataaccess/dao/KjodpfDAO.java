package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Kjodpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface KjodpfDAO extends BaseDAO<Kjodpf>{
	void insertKjodpfList(List<Kjodpf> pfList);
	public Kjodpf getKjodpfRecord(String chdrnum, String coverage, String rider);
}
