/*
 * File: P6691.java
 * Date: 30 August 2009 0:53:15
 * Author: Quipoz Limited
 * 
 * Class transformed from P6691.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.SvltsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.screens.S6691ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
*                  FULL BONUS SURRENDER ENQUIRY.
*
*  This transaction, Full Bonus Surrender, is selected from the
*  surrender sub-menu  S5245/P5245. This program allows the user
*  to enquire on bonus surrenders for a component.
*  The transaction may process a single policy or the whole plan.
*  If any of the policy components are broken out, then whole
*  plan cannot be selected.
*  This transaction cannot be selected if any loans are in
*  existence. This decision is taken at sub-menu stage.
*
*  Bonus surrender is only applicable to TRADITIONAL TYPE
*  components.
*
*  The user must register the correct amount to surrender to each
*  component if the total is being spread across several
*  components.
*
*  The program uses an effective date passed from S5245 and an
*  amount of bonus to be surrendered applicable to this
*  component.
*
*  NOTE : The Effective Date is passed in using WSSP-CURRFROM.
*
*  The COVR record stored in P6351 is used to process the
*  component. The COVR file is not actually read.
*
*  The calculation program to be used is read from T6598
*  using a method obtained from T6640 (keyed by CRTABLE).
*  A surrender detail record is then written to be picked up
*  subsequently by the AT processing module.
*
*  For this particular component, the RESERVE value is then
*  calculated using the Reserve Calculation routine from T6598.
*  This is called using the SURC linkage area and SURC-STATUS is
*  set to 'BONS' to indicate that this is a bonus only surrender.
*  The Bonus Reserve Calculation routine will return the BONUS
*  value for this component (from the ACBL file) in
*  SURC-ESTIMATED-VAL and the RESERVE value in SURC-ACTUAL-VAL.
*
*  If processing a WHOLE-PLAN then the figures returned will
*  be accumulations from all the policy component records
*  pertaining to the current COVERAGE/RIDER component.
*
*  If processing a single policy, then the calculation
*  routine will return the value from that policy.
*
*  NOTE - SURC-ESTIMATED-VAL is used differently in UNIT LINKING
*         routines. For BONUS Reserve calculations this field is
*         not relevant and hence it is used to return the BONUS
*         value in this manner.
*
*  The details are displayed on the screen and the user must
*  enter an amount to surrender (not exceeding the TOTAL BONUS
*  VALUE).
*
*  The user may additionally request that payment be made in
*  a different currency to that of the Contract Header (default).
*
*  From the Total Bonus, the requested amount to surrender and
*  the Total Bonus Reserve, the Bonus Surrender Reserve Value
*  is calculated. This is done whenever the amount to surrender
*  is changed.
*
*  The Bonus Surrender Reserve value is then converted to the
*  appropriate Currency.
*
*  The details are then redisplayed if the CALC PF key is
*  pressed otherwise processing continues.
*
*  The details may be changed and displayed any number of times
*  as long as the CALC PF Key is used.
*
*  If the details are changed but ENTER is used, the changed
*  values will be used for further processing.
*
*  For a whole plan bonus surrender, this process is repeated
*  for all component under this COVERAGE/RIDER.
*
*  For a single component surrender processing is carried out
*  only once.
*
*  FILES READ.
*  ----------
*        COVRMJA - Coverage File
*        CHDRMJA - Contract Header File
*        LIFEMJA - Life File
*        ACBL    - Account Balance File
*        CLTS    - Client File
*        DESC    - Description File
*        ITEM    - Item File
*
*  TABLES READ.
*  -----------
*        T6640   - Traditional Component Methods Table
*        T5688   - Contract Type Descriptions Table
*        T3588   - Risk Status Descriptions Table
*        T3623   - Premium Status Descriptions Table
*        T6598   - Calculation and Processing Subroutines
*
*  PROCESSING.
*  ----------
*
*  INITIALISE.
*  ----------
*
*  If returning from program further down the stack (POINTER =
*  '*') skip this section.
*
*  Read the  Contract  header  (function  RETRV)  and  read  the
*  relevant data necessary for obtaining the status description,
*  the Owner,  the  Life-assured,  Joint-life, CCD, Paid-to-date
*  and the Bill-to-date.
*
*  RETRV the "Plan" or  the  policy  to  be  worked  on from the
*  COVRMJA  I/O  module,  it was stored from program P6351.
*  If the Plan suffix for the key "kept" is zero, the whole Plan
*  is to be surrendered, otherwise, a single policy is to be
*  be surrendered.
*
*  LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To obtain the life assured and joint-life details (if any) do
*  the following;-
*
*      - READR  the   life  details  using  LIFESUR  (for  this
*           contract  number,  joint life number '00').  Format
*           the name accordingly.
*      - READR the  joint-life  details using LIFESUR (for this
*           contract  number,  joint life number '01').  Format
*           the name accordingly.
*
*
*  Obtain the  necessary  descriptions  by  calling 'DESCIO' and
*  output the descriptions of the statuses to the screen.
*
*  Format Name
*
*      Read the  client  details  record  and  use the relevant
*           copybook in order to format the required names.
*
*      For this coverage/rider :-
*
*           - call  the  surrender  calculation  subroutine  as
*                defined on  T6640,  this  method  is  used  to
*                access  T6598,  which contains the subroutines
*                necessary   for   the  surrender  calculation.
*
*  Linkage area passed to the surrender calculation subroutine:-
*
*        - company
*        - contract header number
*        - suffix
*        - life number
*        - joint-life number
*        - coverage
*        - rider
*        - crtable
*        - language
*        - estimated value
*        - actual value
*        - currency
*        - element code
*        - description
*        - type code
*        - status
*
*  Use the SURC linkage area with SURC-STATUS set to 'BONS'.
*
*      If the policy selected is part of a summary then the
*      amounts returned must be divided by "n" ("n" being
*      the number of policies in the plan).
*
*  SURC-ESTIMATED-VAL contains the BONUS VALUE.
*  SURC-ACTUAL-VAL    contains the BONUS RESERVE VALUE.
*
*  Set Screen values.
*
*  RECEIVE SCREEN.
*  --------------
*
*  If returning from program further down the stack (POINTER =
*  '*') skip this section.
*
*  Validation
*  ----------
*
*  If the KILL PF Key was entered, then skip the remainder of the
*  validation and exit from the program.
*
*  Amount to surrender
*      Must be entered and cannot exceed Bonus Value.
*
*  Currency
*      Validated by the I/O module.
*      A blank entry defaults to the contract currency.
*
*  Letter Print Request
*      A letter to eveidence the enquiry may be requested.
*
*  Process Screen.
*  --------------
*
*  If the Bonus Amount to surrender has changed then the new
*  reserve value is calculated as follows :
*
*    (Amt to Surrender / Total Bonus ) * Total Bonus Reserve
*
*  eg. Total Bonus = $1000.00
*      Total Bonus Reserve = $500.00
*      Amt to Surr = $250.00
*
*    Bonus Amt To Surr Reserve = (250 / 1000) * 500 = $125.00
*
*  If the currency code was changed then convert reserve amount
*  to the new currency.
*
*  Processing is always carried out in this order ie. Reserve
*  recalculation followed by currency conversion.
*
*  Only if the CALC PF Key function key is used is the screen
*  redisplayed with the new values otherwise processing continues
*  assuming the recalculated values.
*
*  UPDATING
*  --------
*
*  If returning from program further down the stack (POINTER =
*  '*') skip this section.
*
*  If the KILL PF Key function  key  was  pressed,  skip  the
*  updating and exit from the program.
*
*  If a letter was requested then write a LETC record using the
*  letter type read from T6634 and write the details of the
*  the surrender to a surrender detail letter record, SVLT.
*
*  NEXT PROGRAM.
*  ------------
*
*  Add 1 to the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P6691 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6691");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaProcessingType = new FixedLengthStringData(1);
	private Validator wholePlan = new Validator(wsaaProcessingType, "1");
	private Validator singleComponent = new Validator(wsaaProcessingType, "2");
	private PackedDecimalData wsaaAcblCurrentBalance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaStoredReserve = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaSurrType = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSvltShortdss = new FixedLengthStringData(10);
	private PackedDecimalData wsaaReserve = new PackedDecimalData(17, 4);
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3);
	private PackedDecimalData wsaaStoredSurrVal = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaSvltSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(206);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 0);
	private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 10);
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData filler2 = new FixedLengthStringData(184).isAPartOf(wsaaTransactionRec, 22, FILLER).init(SPACES);

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaLetokeys, 0);
		/* TABLES */
	private static final String t6640 = "T6640";
	private static final String t5688 = "T5688";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t6598 = "T6598";
	private static final String tr384 = "TR384";
		/* FORMATS */
//	private static final String covrmjarec = "COVRMJAREC";
	private static final String lifemjarec = "LIFEMJAREC";
	private static final String cltsrec = "CLTSREC";
	private static final String itemrec = "ITEMREC";
	private static final String svltsurrec = "SVLTSURREC";
	private AcblTableDAM acblIO = new AcblTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
//	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Covrpf covrmjaIO;
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private SvltsurTableDAM svltsurIO = new SvltsurTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private T6640rec t6640rec = new T6640rec();
	private T6598rec t6598rec = new T6598rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6691ScreenVars sv = ScreenProgram.getScreenVars( S6691ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private ExternalisedRules er = new ExternalisedRules();
	
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1099, 
		checkForErrors2080, 
		exit2090
	}

	public P6691() {
		super();
		screenVars = sv;
		new ScreenModel("S6691", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			start1000();
			setScreen1020();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start1000()
	{
		/*  Is control returning from another program further down*/
		/*  the stack.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1099);
		}
		/*  Initialise the  Screen.*/
		sv.dataArea.set(SPACES);
		wsaaStoredCurrency.set(SPACES);
		wsaaSurrType.set(SPACES);
		wsaaSvltShortdss.set(SPACES);
		wsaaStoredSurrVal.set(ZERO);
		sv.bonusReserveValue.set(ZERO);
		sv.bonusValue.set(ZERO);
		sv.bonusValueSurrender.set(ZERO);
		sv.plansfx.set(ZERO);
		sv.numpols.set(ZERO);
		sv.bonusDecDate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.rcdate.set(varcom.vrcmMaxDate);
		wsaaBatckey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatckey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatckey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		/*  Set todays date.*/
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				syserrrec.statuz.set(datcon1rec.statuz);
				fatalError600();
			}
			else {
				wsaaToday.set(datcon1rec.intDate);
			}
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		/*  Initialise table numeric items in SVLT rec.*/
		for (wsaaSvltSub.set(1); !(isGT(wsaaSvltSub, 10)); wsaaSvltSub.add(1)){
			svltsurIO.setEmv(wsaaSvltSub, 0);
			svltsurIO.setActvalue(wsaaSvltSub, 0);
		}
		wsaaSvltSub.set(ZERO);
		wsaaTransactionRec.set(wsspcomn.tranid);
		/*  Retrieve the Coverage record stored by P6351.*/
		covrmjaIO = covrpfDAO.getCacheObject(new Covrpf()); 
		if(covrmjaIO == null) {
			syserrrec.params.set(chdrmjaIO.getChdrnum());
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
//		covrmjaIO.setFormat(covrmjarec);
//		covrmjaIO.setFunction(varcom.retrv);
//		SmartFileCode.execute(appVars, covrmjaIO);
//		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
//			syserrrec.params.set(covrmjaIO.getParams());
//			syserrrec.statuz.set(covrmjaIO.getStatuz());
//			fatalError600();
//		}
		/*  Store the coverage details.*/
		wsaaCovrChdrcoy.set(covrmjaIO.getChdrcoy());
		wsaaCovrChdrnum.set(covrmjaIO.getChdrnum());
		wsaaCovrLife.set(covrmjaIO.getLife());
		wsaaCovrCoverage.set(covrmjaIO.getCoverage());
		wsaaCovrRider.set(covrmjaIO.getRider());
		/*  Retrieve the Contract Header record stored by P6351.*/
		chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		/*  Obtain the Surrender Bonus Calc Method from T6640.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(wsspcomn.currfrom);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t6640)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getCrtable());
			syserrrec.statuz.set(errorsInner.g588);
			fatalError600();
		}
		else {
			t6640rec.t6640Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t6640rec.surrenderBonusMethod, SPACES)) {
			syserrrec.params.set(covrmjaIO.getCrtable());
			syserrrec.statuz.set(errorsInner.h151);
			fatalError600();
		}
		/*  Obtain the Surrender Bonus Routine from T6598.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFormat(itemrec);
		itemIO.setItemtabl(t6598);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(t6640rec.surrenderBonusMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		if (isEQ(t6598rec.calcprog, SPACES)) {
			syserrrec.params.set(t6640rec.surrenderBonusMethod);
			syserrrec.statuz.set(errorsInner.h021);
			fatalError600();
		}
		/*  Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.cntdesc.fill("?");
		}
		else {
			sv.cntdesc.set(descIO.getLongdesc());
		}
		/*  Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			descIO.setStatuz(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.rstatdesc.fill("?");
		}
		else {
			sv.rstatdesc.set(descIO.getShortdesc());
		}
		/*  Obtain the Premium Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premStatDesc.fill("?");
		}
		else {
			sv.premStatDesc.set(descIO.getShortdesc());
		}
		/*  Load the Contract Owner details.*/
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		sv.life.set(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		/*  Read CLTS file using first life details.*/
		cltsIO.setClntnum(chdrmjaIO.getCownnum());
		sv.cownum.set(chdrmjaIO.getCownnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		/*  Load the First-Life details to screen*/
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(covrmjaIO.getChdrnum());
		lifemjaIO.setLife(covrmjaIO.getLife());
		sv.life.set(covrmjaIO.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		/*  Read CLTS file using first life details.*/
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/*  Check for existence of JOINT-LIFE Details.*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
		/*  If record not found for joint life then this is single life.*/
		/*  If single life then move spaces to joint life details. If*/
		/*  joint life life case then read CLTS file and call PLAINNAME.*/
		if (isEQ(lifemjaIO.getStatuz(), varcom.mrnf)) {
			sv.jlife.set(SPACES);
			sv.jlifename.set(SPACES);
		}
		else {
			sv.jlife.set(lifemjaIO.getLifcnum());
			cltsIO.setClntnum(lifemjaIO.getLifcnum());
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				syserrrec.statuz.set(cltsIO.getStatuz());
				fatalError600();
			}
			else {
				plainname();
				sv.jlifename.set(wsspcomn.longconfname);
			}
		}
		/*  If COVRMJA-PLAN-SUFFIX is zeroes then the whole plan is to be*/
		/*  processed.*/
		if (isEQ(covrmjaIO.getPlanSuffix(), ZERO)) {
			wsaaProcessingType.set("1");
		}
		else {
			sv.plansfx.set(covrmjaIO.getPlanSuffix());
			wsaaProcessingType.set("2");
		}
		/*  Get the details of the policy to be surrendered.*/
		policyLoad5000();
		/*  In POLICY-LOAD, the COVR details are read for WHOLE PLAN cases*/
		/*  only. If ENDP is returned then no COVR records have been*/
		/*  found.*/
		if (covrmjaIO == null) {
			syserrrec.params.set(wsaaCovrKey);
			syserrrec.statuz.set(varcom.endp);
			fatalError600();
		}
	}

protected void setScreen1020()
	{
		/*    Set screen fields*/
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.numpols.set(chdrmjaIO.getPolinc());
		sv.coverage.set(covrmjaIO.getCoverage());
		sv.rider.set(covrmjaIO.getRider());
		sv.ptdate.set(chdrmjaIO.getPtdate());
		sv.btdate.set(chdrmjaIO.getBtdate());
		sv.bonusDecDate.set(covrmjaIO.getUnitStatementDate());
		sv.rcdate.set(covrmjaIO.getCrrcd());
		sv.effdate.set(wsspcomn.currfrom);
		sv.paycurr.set(chdrmjaIO.getCntcurr());
		wsaaStoredCurrency.set(chdrmjaIO.getCntcurr());
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*  Is control returning from another program further down         */
		/*  the stack.                                                     */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*  If no bonus value, put out error message and protect           */
		/*  Input fields.                                                  */
		if (isEQ(sv.bonusValue, 0)) {
			sv.bonvalsurrOut[varcom.pr.toInt()].set("Y");
			sv.bonusvalueErr.set(errorsInner.h154);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2000();
					validate2010();
					checkForChanges2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2000()
	{
		/*    CALL 'S6691IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6691-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*         GO TO 2099-EXIT.                                        */
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2010()
	{
//	ILIFE-1260 
		/*    Validate fields*/
		/*  A bonus value to be surrendered must be entered.*/
		/*if (isGT(sv.bonusValue, 0)) {
			if (isEQ(sv.bonusValueSurrender, 0)) {
				sv.bonvalsurrErr.set(errorsInner.h153);
			}
		}*/
		/*  Bonus value to surrender may not exceed Total amount of*/
		/*  bonus.*/
		if (isGT(sv.bonusValueSurrender, sv.bonusValue)) {
			sv.bonvalsurrErr.set(errorsInner.h152);
		}
		if (isNE(sv.bonusValueSurrender, ZERO)) {
			zrdecplrec.amountIn.set(sv.bonusValueSurrender);
			a000CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.bonusValueSurrender)) {
				sv.bonvalsurrErr.set(errorsInner.rfik);
			}
		}
		if (isNE(sv.letterPrintFlag, "Y")
		&& isNE(sv.letterPrintFlag, "y")
		&& isNE(sv.letterPrintFlag, "N")
		&& isNE(sv.letterPrintFlag, "n")
		&& isNE(sv.letterPrintFlag, SPACES)) {
			sv.letprtflagErr.set(errorsInner.g941);
		}
		if (isEQ(sv.letterPrintFlag, "Y")
		|| isEQ(sv.letterPrintFlag, "y")) {
			readT66342300();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.checkForErrors2080);
		}
		/*  If Currency is blank, default to CHDR currency.*/
		if (isEQ(sv.paycurr, SPACES)) {
			sv.paycurr.set(chdrmjaIO.getCntcurr());
		}
	}

protected void checkForChanges2020()
	{
		/*  Check for a change to Bonus Value to Surrender. If so*/
		/*  calculate the reserve and store the new value.*/
		/*  New Reserve value will be calculated in original CHDR*/
		/*  currency, this may now be different but not necessarily*/
		/*  on the most recent screen display ie. if the currency*/
		/*  was not changed this time round, then the current*/
		/*  currency would be the same as the stored currency but*/
		/*  the reserve is calculated in the original contract*/
		/*  currency. Unless a check is made for a change (on some*/
		/*  previous display) for a change in currency from the*/
		/*  original one then the Reserve will be incorrectly*/
		/*  displayed.*/
		if (isNE(sv.bonusValueSurrender, wsaaStoredSurrVal)) {
			recalculateReserve2100();
			sv.bonusReserveValue.set(wsaaReserve);
			wsaaStoredSurrVal.set(sv.bonusValueSurrender);
			if (isEQ(wsspcomn.edterror, "Y")) {
				goTo(GotoLabel.checkForErrors2080);
			}
			/*  Has currency changed from original currency ?*/
			if (isNE(sv.paycurr, chdrmjaIO.getCntcurr())) {
				conlinkrec.currIn.set(chdrmjaIO.getCntcurr());
				conlinkrec.currOut.set(sv.paycurr);
				readjustCurrencies2200();
				sv.bonusReserveValue.set(conlinkrec.amountOut);
				wsaaStoredCurrency.set(sv.paycurr);
				if (isEQ(wsspcomn.edterror, "Y")) {
					goTo(GotoLabel.checkForErrors2080);
				}
			}
		}
		else {
			/*  If currency denomination only has changed, call XCVRT to*/
			/*  covert to the new currency and store the new currency.*/
			if (isNE(sv.paycurr, wsaaStoredCurrency)) {
				conlinkrec.currIn.set(wsaaStoredCurrency);
				conlinkrec.currOut.set(sv.paycurr);
				readjustCurrencies2200();
				sv.bonusReserveValue.set(conlinkrec.amountOut);
				wsaaStoredCurrency.set(sv.paycurr);
				if (isEQ(wsspcomn.edterror, "Y")) {
					goTo(GotoLabel.checkForErrors2080);
				}
			}
		}
		/*  Redisplay the screen only if requested.*/
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void recalculateReserve2100()
	{
		/*START*/
		/*  Recalculate the reserve value from the new Amt to Surrender.*/
		compute(wsaaReserve, 4).set(mult((div(sv.bonusValueSurrender, sv.bonusValue)), wsaaStoredReserve));
		zrdecplrec.amountIn.set(wsaaReserve);
		a000CallRounding();
		wsaaReserve.set(zrdecplrec.amountOut);
		/*EXIT*/
	}

protected void readjustCurrencies2200()
	{
		start2200();
	}

protected void start2200()
	{
		/*  Convert the Bonus Reserve value to the new currency.*/
		conlinkrec.amountIn.set(sv.bonusReserveValue);
		conlinkrec.function.set("CVRT");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrmjaIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			scrnparams.errorCode.set(conlinkrec.statuz);
			wsspcomn.edterror.set("Y");
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		a000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

protected void readT66342300()
	{
		start2300();
	}

protected void start2300()
	{
		/*  Get the Letter type from T6634.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		/* MOVE T6634                      TO ITEM-ITEMTABL.            */
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from contract type & transaction code.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmjaIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(errorsInner.g437);
			fatalError600();
		}
		/* MOVE ITEM-GENAREA               TO T6634-T6634-REC.          */
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF  T6634-LETTER-TYPE           = SPACES                     */
		if (isEQ(tr384rec.letterType, SPACES)) {
			sv.letprtflagErr.set(errorsInner.h394);
		}
	}

protected void update3000()
	{
		/*START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, "KILL")) {
			return ;
		}
		if (isEQ(sv.bonusValue, ZERO)) {
			return ;
		}
		if (isEQ(sv.letterPrintFlag, "Y")
		|| isEQ(sv.letterPrintFlag, "y")) {
			letterRecs6000();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*START*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void policyLoad5000()
	{
		/*LOAD-POLICY*/
		/*  Set screen attributes to display "to 1" when processing*/
		/*  summary record.*/
		if (singleComponent.isTrue()) {
			sv.plansfxOut[varcom.hi.toInt()].set("Y");
			sv.plansfx.set(covrmjaIO.getPlanSuffix());
		}
		/*  Get Reserve value and Total Bonus Value.*/
		getReserve5100();
		sv.bonusValue.set(wsaaAcblCurrentBalance);
		/*EXIT*/
	}

protected void getReserve5100()
	{
		start5100();
	}

protected void start5100()
	{
		srcalcpy.currcode.set(chdrmjaIO.getCntcurr());
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrmjaIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrmjaIO.getChdrnum());
		srcalcpy.lifeLife.set(covrmjaIO.getLife());
		srcalcpy.lifeJlife.set(covrmjaIO.getJlife());
		srcalcpy.covrCoverage.set(covrmjaIO.getCoverage());
		srcalcpy.covrRider.set(covrmjaIO.getRider());
		srcalcpy.crtable.set(covrmjaIO.getCrtable());
		srcalcpy.crrcd.set(covrmjaIO.getCrrcd());
		srcalcpy.status.set("BONS");
		srcalcpy.pstatcode.set(covrmjaIO.getPstatcode());
		srcalcpy.polsum.set(chdrmjaIO.getPolsum());
		srcalcpy.ptdate.set(chdrmjaIO.getPtdate());
		srcalcpy.effdate.set(wsspcomn.currfrom);
		srcalcpy.language.set(wsspcomn.language);
		srcalcpy.type.set("F");
		/*  Set Plan Suffix as appropriate. If surrendering a*/
		/*  notional policy, the calculation routine must get its*/
		/*  information from the summary ACBL record.*/
		srcalcpy.planSuffix.set(covrmjaIO.getPlanSuffix());
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			/*IVE-796 RUL Product - Partial Surrender Calculation started*/
			//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString())))
			{
				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			}
			else
			{
		 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
				Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

				vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
				vpxsurcrec.function.set("INIT");
				callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
				srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
				vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
				vpmfmtrec.initialize();
				vpmfmtrec.amount02.set(wsaaEstimateTot);			

				callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrmjaIO);//VPMS call
				
				
				if(isEQ(srcalcpy.type,"L"))
				{
					vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
					callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
					srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
					srcalcpy.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpy.type,"C"))
				{
					srcalcpy.status.set(varcom.endp);
				}
				else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
				{
					srcalcpy.status.set(varcom.endp);
				}
				else
				{
					srcalcpy.status.set(varcom.oK);
				}
			}

			/*IVE-796 RUL Product - Partial Surrender Calculation end*/
			if (isNE(srcalcpy.status, varcom.oK)
			&& isNE(srcalcpy.status, varcom.endp)) {
				syserrrec.statuz.set(srcalcpy.status);
				fatalError600();
			}

		wsaaStoredReserve.add(srcalcpy.actualVal);
		wsaaAcblCurrentBalance.add(srcalcpy.estimatedVal);
		wsaaSurrType.set(srcalcpy.type);
		wsaaSvltShortdss.set(srcalcpy.description);
		/*  If notional policy then divide the summary record figures*/
		/*  by number of policies summarised.*/
		if (singleComponent.isTrue()
		&& isLTE(covrmjaIO.getPlanSuffix(), chdrmjaIO.getPolsum())) {
			compute(wsaaAcblCurrentBalance, 2).set(div(wsaaAcblCurrentBalance, chdrmjaIO.getPolsum()));
			compute(wsaaStoredReserve, 2).set(div(wsaaStoredReserve, chdrmjaIO.getPolsum()));
		}
	}
		zrdecplrec.amountIn.set(wsaaAcblCurrentBalance);
		a000CallRounding();
		wsaaAcblCurrentBalance.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(wsaaStoredReserve);
		a000CallRounding();
		wsaaStoredReserve.set(zrdecplrec.amountOut);
}

protected void letterRecs6000()
	{
		letcRecord6020();
		svltRecord6040();
	}

	/**
	* <pre>
	*  Write LETC via LETRQST and SVLT details record.
	* </pre>
	*/
protected void letcRecord6020()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.          */
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(sv.effdate);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.otherKeys.set(SPACES);
		letrqstrec.rdocpfx.set(chdrmjaIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdocnum.set(sv.chdrnum);
		letrqstrec.chdrnum.set(sv.chdrnum);
		letrqstrec.branch.set(wsspcomn.branch);
		/*  MOVE S6691-CHDRNUM          TO WSAA-CHDRNUM.                 */
		/*  MOVE WSAA-LETOKEYS          TO LETRQST-OTHER-KEYS.           */
		letrqstrec.function.set("ADD");
		/* IF  T6634-LETTER-TYPE       NOT = SPACES                     */
		if (isNE(tr384rec.letterType, SPACES)) {
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz, varcom.oK)) {
				syserrrec.params.set(letrqstrec.params);
				syserrrec.statuz.set(letrqstrec.statuz);
				fatalError600();
			}
		}
	}

protected void svltRecord6040()
	{
		svltsurIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		svltsurIO.setChdrnum(sv.chdrnum);
		svltAlreadyExists6200();
		wsaaSvltSub.add(1);
		if (isLTE(wsaaSvltSub, 10)) {
			svltsurIO.setCoverage(wsaaSvltSub, sv.coverage);
			svltsurIO.setRider(wsaaSvltSub, sv.rider);
			svltsurIO.setCnstcur(wsaaSvltSub, sv.paycurr);
			svltsurIO.setEmv(wsaaSvltSub, sv.bonusValueSurrender);
			svltsurIO.setActvalue(wsaaSvltSub, sv.bonusReserveValue);
			/*   MOVE WSAA-SURR-TYPE     TO SVLTSUR-TYPES (WSAA-SVLT-SUB)    */
			/*   MOVE WSAA-SVLT-SHORTDSS TO SVLTSUR-SHORTDSS (WSAA-SVLT-SUB) */
			svltsurIO.setType(wsaaSvltSub, wsaaSurrType);
			svltsurIO.setShortds(wsaaSvltSub, wsaaSvltShortdss);
		}
		/*  Store general Contract details in SVLT record area.*/
		svltsurIO.setPlanSuffix(sv.plansfx);
		svltsurIO.setCnttype(chdrmjaIO.getCnttype());
		svltsurIO.setCownnum(chdrmjaIO.getCownnum());
		svltsurIO.setOccdate(chdrmjaIO.getOccdate());
		svltsurIO.setPtdate(chdrmjaIO.getPtdate());
		svltsurIO.setTransactionDate(wsaaTransactionDate);
		svltsurIO.setTransactionTime(wsaaTransactionTime);
		svltsurIO.setUser(wsaaUser);
		svltsurIO.setTermid(wsaaTermid);
		svltsurIO.setValidflag("1");
		svltsurIO.setFunction(varcom.writr);
		svltsurIO.setFormat(svltsurrec);
		SmartFileCode.execute(appVars, svltsurIO);
		if (isNE(svltsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(svltsurIO.getParams());
			syserrrec.statuz.set(svltsurIO.getStatuz());
			fatalError600();
		}
	}

protected void svltAlreadyExists6200()
	{
		start6200();
	}

protected void start6200()
	{
		/*  Check to see if SVLT record already exists and if so mark it*/
		/*  as valid flag 2 and rewrite.*/
		svltsurIO.setFunction(varcom.readr);
		svltsurIO.setFormat(svltsurrec);
		SmartFileCode.execute(appVars, svltsurIO);
		if (isNE(svltsurIO.getStatuz(), varcom.oK)
		&& isNE(svltsurIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(svltsurIO.getParams());
			syserrrec.statuz.set(svltsurIO.getStatuz());
			fatalError600();
		}
		if (isEQ(svltsurIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		/*  Record already exists, READH, update valid flag and rewrite.*/
		svltsurIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, svltsurIO);
		if (isNE(svltsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(svltsurIO.getParams());
			syserrrec.statuz.set(svltsurIO.getStatuz());
			fatalError600();
		}
		svltsurIO.setValidflag("2");
		svltsurIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, svltsurIO);
		if (isNE(svltsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(svltsurIO.getParams());
			syserrrec.statuz.set(svltsurIO.getStatuz());
			fatalError600();
		}
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.paycurr);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData g437 = new FixedLengthStringData(4).init("G437");
	private FixedLengthStringData g588 = new FixedLengthStringData(4).init("G588");
	private FixedLengthStringData g941 = new FixedLengthStringData(4).init("G941");
	private FixedLengthStringData h021 = new FixedLengthStringData(4).init("H021");
	private FixedLengthStringData h151 = new FixedLengthStringData(4).init("H151");
	private FixedLengthStringData h152 = new FixedLengthStringData(4).init("H152");
	private FixedLengthStringData h153 = new FixedLengthStringData(4).init("H153");
	private FixedLengthStringData h154 = new FixedLengthStringData(4).init("H154");
	private FixedLengthStringData h394 = new FixedLengthStringData(4).init("H394");
	private FixedLengthStringData rfik = new FixedLengthStringData(4).init("RFIK");
}
}
