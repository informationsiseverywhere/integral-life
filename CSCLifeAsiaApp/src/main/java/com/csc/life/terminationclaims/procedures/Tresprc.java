/*
 * File: Tresprc.java
 * Date: 30 August 2009 2:45:39
 * Author: Quipoz Limited
 * 
 * Class transformed from TRESPRC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.procedures.Anpyintcal;
import com.csc.life.contractservicing.recordstructures.Annypyintcalcrec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*         TRADITIONAL LIFE SURRENDER PROCESSING PROGRAM.
*
*   The program PROPPRC is a copy of this one and so any
*   changes made here may well need to be mirrored in that
*   program.
*
*   This subroutine is used as the processing routine for
*   traditional type component surrenders.
*
*   It is used for Temporary Term Assurances.
*
*   It is called from the surrender processing AT module P5084AT.
*
*   It is called once per contract using the SURPCPY linkage
*   area.
*
*   It accesses information stored in the SURRENDER HEADER file
*   (SURH) and the SURRENDER DETAIL file (SURD) to write
*   accounting entries to the ACCOUNT MOVEMENTS file (ACMV).
*
*   A BEGN on the SURD file gets the first surrender record.
*   The SURH file is first read to get the contract level
*   details. The SURD file is then processed sequentially to
*   get surrender records for each of the components on the
*   contract. There may be a Sum Assured record and a Bonus
*   record for each component.
*
*   For each SURD record read, an account movement record is
*   written to the ACMV file.
*
*   When all SURD records have been processed for this contract,
*   write an ACMV record for the Adjustment value read from the
*   SURH record.
*   Then write a balancing ACMV record for the total of all
*   records previously processed.
*
*   Return control to the calling program.
*
*   FILE READS.
*   ----------
*
*   Surrender Header file for :
*   - Contract level details.
*   - Adjustment amount entered at time of surrender.
*
*   Surrender Detail file for :
*   - details of individual surrender amounts.
*
*   PROCESSING.
*   ----------
*
*   INITIAL VALUES.
*   --------------
*
*   - Read T5688 to establish whether contract or component
*     level accounting for this contract type.
*
*   - Read T5645 to get the accounting rules for this
*     subroutine.
*
*   - Read T1688 to get description of the transaction
*     which initiated the surrender.
*
*   - Initial read of SURD file.
*
*   - Read SURH file.
*
*   - Set up ACMV fields which are common to all components on
*     this contract.
*
*   PROCESSING LOOP
*   ---------------
*
*   - Read SURD file sequentially for all surrender records
*     associated with this contract.
*
*   - For each Record :
*
*   - There is no requirement to distinguish between 'S' type
*     records (Sum Assured) and 'B' type records (Reversionary
*     Bonus).
*
*   - Write an ACMV record.
*     - Fields common to all ACMV's
*
*       LIFA-FUNCTION         - 'PSTW'
*       LIFA-BATCCOY          - Contract Company from linkage
*       LIFA-BATCKEY          - Batch Key details from linkage
*       LIFA-RDOCNUM          - Contract Number from linkage
*       LIFA-TRANNO           - Transaction Number from SURD
*       LIFA-JRNSEQ           - Start from 1, increment by 1
*       LIFA-RLDGCOY          - Contract Company from linkage
*       LIFA-ORIGCURR         - Contract Currency from linkage
*       LIFA-TRANREF          - Contract Number from linkage
*       LIFA-TRANDESC         - Tran. Code description from T1688
*       LIFA-CRATE            - 0
*       LIFA-ACCTAMT          - 0
*       LIFA-GENLCOY          - Contract Company from linkage
*       LIFA-GENLCUR          - Contract Currency from linkage
*       LIFA-POSTYEAR         - Spaces
*       LIFA-POSTMONTH        - Spaces
*       LIFA-EFFDATE          - Effective Surr. Date from linkage
*       LIFA-RCAMT            - 0
*       LIFA-FRCDATE          - Max-date
*       LIFA-TRANSACTION-DATE - Effective Date from linkage
*       LIFA-TRANSACTION-TIME - Time of transaction
*       LIFA-USER             - Transaction User from linkage
*       LIFA-TERMID           - Transaction Term-ID from linkage
*       LIFA-SUBSTITUTE-CODE  - 1 - Contract Type
*                               2 - 5 not set
*     - Fields varying :
*
*       LIFA-SACSCODE         - Account Code from T5645
*       LIFA-RLDGACCT         - If Component Level Accounting :
*                                 Full component Key
*                               If Contract Level Accounting :
*                                 Contract Number
*       LIFA-SACSTYP          - Account Type from T5645
*       LIFA-ORIGAMT          - Amount of posting from SURD rec.
*       LIFA-GLCODE           - General Ledger Code from T5645
*       LIFA-GLSIGN           - General Ledger Sign from T5645
*       LIFA-CONTOT           - Control Total from T5645
*       LIFA-SUBSTITUTE-CODE  - 6 - Component Type
*
*   - Accounting Rules from T5645
*
*     Line 1 : Sum Assured Surrender Value for contract level
*     Line 2 : Sum Assured Surrender Value for component level
*     Line 3 : Adjustment per contract from SURH record
*     Line 4 : Balance of all totalled (suspense)
*
*   - Write ACMV for contract adjustment.
*
*   - Write balancing ACMV for the total.
*
*   SET EXIT VARIABLES.
*   ------------------
*   set linkage variables as follows :
*
*   To indicate that processing complete :
*
*   - STATUS -        'ENDP'
*
*****************************************************************
* </pre>
*/
public class Tresprc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("TRESPRC");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* ERRORS */
	private String e308 = "E308";
	private String h150 = "H150";
	private String h134 = "H134";
	private String e044 = "E044";
	private String surdclmrec = "SURDCLMREC";
	private String surhclmrec = "SURHCLMREC";
		/* TABLES */
	private String t5645 = "T5645";
	private String t5688 = "T5688";
	private String t1688 = "T1688";

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30);
	private PackedDecimalData wsaaAccumValue = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaSequenceNo = new ZonedDecimalData(13, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaAcctLevel = new FixedLengthStringData(1);
	private Validator componentLevelAccounting = new Validator(wsaaAcctLevel, "Y");

	private FixedLengthStringData wsaaEndOfContFlag = new FixedLengthStringData(1).init("N");
	private Validator endOfContract = new Validator(wsaaEndOfContFlag, "Y");
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
		/*Surrender Claims Detail Record*/
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
		/*Full Surrender header record*/
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private Surpcpy surpcpy = new Surpcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();
	private boolean endowFlag = false;
	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private Zraepf zraepf = new Zraepf();
	public PackedDecimalData interestAmount = new PackedDecimalData(17, 2);
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private List<Acblpf> acblenqListLPAE = null;
	private List<Acblpf> acblenqListLPAS = null;
	private BigDecimal accAmtLPAE = new BigDecimal(0);
	private BigDecimal accAmtLPAS = new BigDecimal(0);
	private Annypyintcalcrec annypyintcalcrec = new Annypyintcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private static final String PRPRO001 = "PRPRO001"; 

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		seExit9090, 
		dbExit9190
	}

	public Tresprc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		surpcpy.surrenderRec = convertAndSetParam(surpcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		/*START*/
		initialise2000();
		while ( !(endOfContract.isTrue())) {
			processSurdRecords3000();
		}
		
		endOfContract4000();
		exitProgram();
	}

protected void initialise2000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaProg);
		varcom.vrcmTime.set(getCobolTime());
		wsaaEndOfContFlag.set("N");
		wsaaAccumValue.set(ZERO);
		endowFlag = FeaConfg.isFeatureExist("2", PRPRO001, appVars, "IT");
		readTables2100();
		initialSurdRead2200();
		readSurhRecord2300();
		initialAcmvFields2400();
		if(endowFlag){
			zraepf=zraepfDAO.getItemByContractNum(surpcpy.chdrnum.toString());
			
		}
		/*EXIT*/
	}

protected void updateZrae()
{
    List<Zraepf> zraepfList;
    List<String> chdrnumList = new ArrayList<String>();
    chdrnumList.add(surdclmIO.getChdrnum().toString());
    zraepfList = zraepfDAO.searchZraepfRecord(chdrnumList, surdclmIO.getChdrcoy().toString());
    
    if(zraepfList!= null && !zraepfList.isEmpty()){
    	 zraepfDAO.updateValidflag(zraepfList);	
     }
    Zraepf Zraepftmp = new Zraepf();
		if (zraepfList != null && !zraepfList.isEmpty()) {
			Zraepftmp = new Zraepf(zraepfList.get(0));
			Zraepftmp.setApcaplamt(new BigDecimal(0));
			Zraepftmp.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
			Zraepftmp.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
			Zraepftmp.setFlag("2");
			Zraepftmp.setValidflag("1");
			Zraepftmp.setTranno(surdclmIO.getTranno().toInt());
			Zraepftmp.setApintamt(BigDecimal.ZERO);
			Zraepftmp.setTotamnt(0);
			zraepfDAO.insertZraeRecord(Zraepftmp);
		}
}

private void endowmentposting() {
	if(zraepf.getChdrnum().length() > 0)
		calculateInterest();	

	acblenqListLPAE = acblpfDAO.getAcblenqRecord(
			"2", StringUtils.rightPad(surdclmIO.getChdrnum().toString(), 16),t5645rec.sacscode06.toString(),t5645rec.sacstype06.toString());

	acblenqListLPAS =acblpfDAO.getAcblenqRecord(
			"2", StringUtils.rightPad(surdclmIO.getChdrnum().toString(), 16),t5645rec.sacscode07.toString(),t5645rec.sacstype07.toString());
	
	if (acblenqListLPAE != null && !acblenqListLPAE.isEmpty()) {
		accAmtLPAE = acblenqListLPAE.get(0).getSacscurbal();
		accAmtLPAE = new BigDecimal(ZERO.toString()).subtract(accAmtLPAE);	
	}
	if (acblenqListLPAS != null && !acblenqListLPAS.isEmpty()) {
		accAmtLPAS =  acblenqListLPAS.get(0).getSacscurbal();
		accAmtLPAS = new BigDecimal(ZERO.toString()).subtract(accAmtLPAS);
	}
			
	if(isNE(accAmtLPAE, 0))
	{
		lifacmvrec1.tranno.set(surdclmIO.getTranno());
		lifacmvrec1.rldgacct.set(surdclmIO.getChdrnum());
		lifacmvrec1.sacscode.set(t5645rec.sacscode06);// LP AE
		lifacmvrec1.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec1.glcode.set(t5645rec.glmap06);
		lifacmvrec1.glsign.set(t5645rec.sign06);
		lifacmvrec1.contot.set(t5645rec.cnttot06);
		lifacmvrec1.origamt.set(accAmtLPAE); 
		lifacmvrec1.acctamt.set(accAmtLPAE);
		wsaaAccumValue.add(accAmtLPAE.doubleValue());
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			return ; 
		}
		lifacmvrec1.sacscode.set(t5645rec.sacscode07);// LP AS
		lifacmvrec1.sacstyp.set(t5645rec.sacstype07);
		lifacmvrec1.glcode.set(t5645rec.glmap07);
		lifacmvrec1.glsign.set(t5645rec.sign07);
		lifacmvrec1.contot.set(t5645rec.cnttot07);
		accAmtLPAS=	add(accAmtLPAS,interestAmount).getbigdata();
		wsaaAccumValue.add(accAmtLPAS.doubleValue());
		lifacmvrec1.origamt.set(accAmtLPAS);
		lifacmvrec1.acctamt.set(accAmtLPAS);
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
		}
	}
	updateZrae();
	
}

private void calculateInterest() {
	annypyintcalcrec.intcalcRec.set(SPACES);
	annypyintcalcrec.chdrcoy.set(surdclmIO.getChdrcoy());
	annypyintcalcrec.chdrnum.set(surdclmIO.getChdrnum());
	annypyintcalcrec.cnttype.set(surhclmIO.cnttype);
	annypyintcalcrec.interestTo.set(surhclmIO.getEffdate());
	annypyintcalcrec.interestFrom.set(zraepf.getAplstintbdte());
	annypyintcalcrec.annPaymt.set(zraepf.getApcaplamt());
	annypyintcalcrec.lastCaplsnDate.set(zraepf.getAplstcapdate());		
	annypyintcalcrec.interestAmount.set(ZERO);
	annypyintcalcrec.currency.set(zraepf.getPaycurr());
	annypyintcalcrec.transaction.set("ENDOWMENT");
	if (isLT(annypyintcalcrec.interestFrom,annypyintcalcrec.interestTo)) {
		callProgram(Anpyintcal.class, annypyintcalcrec.intcalcRec);
		if (isNE(annypyintcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(annypyintcalcrec.statuz);
			systemError9000();	
		}
	}
	/* MOVE INTC-INTEREST-AMOUNT    TO ZRDP-AMOUNT-IN.              */
	/* PERFORM 5000-CALL-ROUNDING.                                  */
	/* MOVE ZRDP-AMOUNT-OUT         TO INTC-INTEREST-AMOUNT.        */
	if (isNE(annypyintcalcrec.interestAmount, 0)) {
		zrdecplrec.amountIn.set(annypyintcalcrec.interestAmount);
		callRounding5100();
		annypyintcalcrec.interestAmount.set(zrdecplrec.amountOut);
		interestAmount.add(annypyintcalcrec.interestAmount);
	}
	
}

private void callRounding5100() {
	/*CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(surdclmIO.getChdrcoy());
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(zraepf.getPaycurr());
	zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		systemError9000();	
	}
	/*EXIT*/
}

protected void readTables2100()
	{
		accountingLevel2110();
		accountingRules2120();
		transactionCodeDesc2130();
	}

protected void accountingLevel2110()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(surpcpy.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(surpcpy.cnttype);
		itdmIO.setItmfrm(surpcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError9000();
		}
		if (isNE(itdmIO.getItemcoy(),surpcpy.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),surpcpy.cnttype)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surpcpy.cnttype);
			syserrrec.statuz.set(e308);
			systemError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaAcctLevel.set(t5688rec.comlvlacc);
	}

protected void accountingRules2120()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(surpcpy.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(wsaaProg);
			syserrrec.statuz.set(h134);
			systemError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void transactionCodeDesc2130()
	{
		wsaaBatckey.set(surpcpy.batckey);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(surpcpy.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setLanguage(surpcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(wsaaBatckey.batcBatctrcde);
			syserrrec.statuz.set(e044);
			systemError9000();
		}
		wsaaLongdesc.set(descIO.getLongdesc());
	}

protected void initialSurdRead2200()
	{
		start2200();
	}

protected void start2200()
	{
		surdclmIO.setParams(SPACES);
		surdclmIO.setChdrcoy(surpcpy.chdrcoy);
		surdclmIO.setChdrnum(surpcpy.chdrnum);
		surdclmIO.setTranno(surpcpy.tranno);
		surdclmIO.setPlanSuffix(surpcpy.planSuffix);
		surdclmIO.setLife(surpcpy.life);
		surdclmIO.setCoverage(surpcpy.coverage);
		surdclmIO.setRider(surpcpy.rider);
		surdclmIO.setCrtable(surpcpy.crtable);
		surdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		surdclmIO.setFormat(surdclmrec);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)
		&& isNE(surdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			syserrrec.statuz.set(surdclmIO.getStatuz());
			dbError9100();
		}
		if (isNE(surdclmIO.getChdrcoy(),surpcpy.chdrcoy)
		|| isNE(surdclmIO.getChdrnum(),surpcpy.chdrnum)
		|| isEQ(surdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surpcpy.chdrnum);
			syserrrec.statuz.set(h150);
			dbError9100();
		}
	}

protected void readSurhRecord2300()
	{
		start2300();
	}

protected void start2300()
	{
		surhclmIO.setParams(SPACES);
		surhclmIO.setChdrcoy(surdclmIO.getChdrcoy());
		surhclmIO.setChdrnum(surdclmIO.getChdrnum());
		surhclmIO.setTranno(surdclmIO.getTranno());
		surhclmIO.setPlanSuffix(surdclmIO.getPlanSuffix());
		surhclmIO.setFunction(varcom.readr);
		surhclmIO.setFormat(surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(surhclmIO.getParams());
			syserrrec.statuz.set(surhclmIO.getStatuz());
			dbError9100();
		}
	}

protected void initialAcmvFields2400()
	{
		start2400();
	}

protected void start2400()
	{
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batccoy.set(surpcpy.chdrcoy);
		lifacmvrec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifacmvrec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifacmvrec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifacmvrec1.rdocnum.set(surpcpy.chdrnum);
		lifacmvrec1.rldgcoy.set(surpcpy.chdrcoy);
		lifacmvrec1.origcurr.set(surpcpy.cntcurr);
		lifacmvrec1.tranref.set(surpcpy.chdrnum);
		lifacmvrec1.trandesc.set(wsaaLongdesc);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.genlcoy.set(surpcpy.chdrcoy);
		lifacmvrec1.genlcur.set(SPACES);
		lifacmvrec1.effdate.set(surpcpy.effdate);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.transactionDate.set(surpcpy.effdate);
		lifacmvrec1.transactionTime.set(varcom.vrcmTime);
		lifacmvrec1.user.set(surpcpy.user);
		lifacmvrec1.termid.set(surpcpy.termid);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.substituteCode[1].set(surpcpy.cnttype);
	}

protected void processSurdRecords3000()
	{
		if (isNE(surdclmIO.fieldType, 'E')) {
			start3000();
		} else {
			if (endowFlag)
				endowmentposting();
		}
		readNextSurdclmio();
	}

	private void readNextSurdclmio() {
		surdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK) && isNE(surdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			syserrrec.statuz.set(surdclmIO.getStatuz());
			dbError9100();
		}
		if (isNE(surdclmIO.getChdrcoy(), surpcpy.chdrcoy) || isNE(surdclmIO.getChdrnum(), surpcpy.chdrnum)
				|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
			wsaaEndOfContFlag.set("Y");
		}
	}

protected void start3000()
	{
		lifacmvrec1.origamt.set(surdclmIO.getActvalue());
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.tranno.set(surdclmIO.getTranno());
		if (componentLevelAccounting.isTrue()) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode02);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec1.glcode.set(t5645rec.glmap02);
			lifacmvrec1.glsign.set(t5645rec.sign02);
			lifacmvrec1.contot.set(t5645rec.cnttot02);
			wsaaRldgChdrnum.set(surpcpy.chdrnum);
			wsaaPlan.set(surdclmIO.getPlanSuffix());
			wsaaRldgLife.set(surpcpy.life);
			wsaaRldgCoverage.set(surpcpy.coverage);
			wsaaRldgRider.set(surpcpy.rider);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode01);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec1.glcode.set(t5645rec.glmap01);
			lifacmvrec1.glsign.set(t5645rec.sign01);
			lifacmvrec1.contot.set(t5645rec.cnttot01);
			lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
		}
		wsaaAccumValue.add(surdclmIO.getActvalue());
		lifacmvrec1.rldgacct.set(SPACES);
		if (componentLevelAccounting.isTrue()) {
			wsaaRldgChdrnum.set(surpcpy.chdrnum);
			wsaaPlan.set(surpcpy.planSuffix);
			wsaaRldgLife.set(surpcpy.life);
			wsaaRldgCoverage.set(surpcpy.coverage);
			wsaaRldgRider.set(surpcpy.rider);
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec1.rldgacct.set(wsaaRldgacct);
		}
		else {
			lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
		}
		lifacmvrec1.substituteCode[6].set(surpcpy.crtable);
		if (isNE(lifacmvrec1.origamt,0)) {
			postAcmvRecord5000();
		}
		
	}

protected void endOfContract4000()
	{
		start4000();
	}

protected void start4000()
	{
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
		lifacmvrec1.sacscode.set(t5645rec.sacscode03);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec1.glcode.set(t5645rec.glmap03);
		lifacmvrec1.glsign.set(t5645rec.sign03);
		lifacmvrec1.contot.set(t5645rec.cnttot03);
		lifacmvrec1.origamt.set(surhclmIO.getOtheradjst());
		wsaaAccumValue.add(surhclmIO.getOtheradjst());
		if (isNE(lifacmvrec1.origamt,0)) {
			postAcmvRecord5000();
		}
		lifacmvrec1.rldgacct.set(SPACES);
		lifacmvrec1.rldgacct.set(surpcpy.chdrnum);
		lifacmvrec1.sacscode.set(t5645rec.sacscode05);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype05);
		lifacmvrec1.glcode.set(t5645rec.glmap05);
		lifacmvrec1.glsign.set(t5645rec.sign05);
		lifacmvrec1.contot.set(t5645rec.cnttot05);
		lifacmvrec1.origamt.set(surhclmIO.getTaxamt());
		wsaaAccumValue.subtract(surhclmIO.getTaxamt());
		if (isNE(lifacmvrec1.origamt,0)) {
			postAcmvRecord5000();
		}
		lifacmvrec1.sacscode.set(t5645rec.sacscode04);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec1.glcode.set(t5645rec.glmap04);
		lifacmvrec1.glsign.set(t5645rec.sign04);
		lifacmvrec1.contot.set(t5645rec.cnttot04);
		lifacmvrec1.origamt.set(wsaaAccumValue);
		if (isNE(lifacmvrec1.origamt,0)) {
			postAcmvRecord5000();
		}
		surpcpy.status.set(varcom.endp);
	}

protected void postAcmvRecord5000()
	{
		/*START*/
		wsaaSequenceNo.add(1);
		lifacmvrec1.jrnseq.set(wsaaSequenceNo);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			systemError9000();
		}
		/*EXIT*/
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		surpcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		surpcpy.status.set(varcom.bomb);
		exitProgram();
	}
}
