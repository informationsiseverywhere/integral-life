package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr582screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 4, 17, 18, 23, 5, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr582screensfl";
		lrec.subfileClass = Sr582screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 11;
		lrec.pageSubfile = 10;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 10, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr582ScreenVars sv = (Sr582ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr582screenctlWritten, sv.Sr582screensflWritten, av, sv.sr582screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr582ScreenVars screenVars = (Sr582ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.crtabled.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.rgpynum.setClassString("");
		screenVars.diagcde.setClassString("");
		screenVars.shortdesc.setClassString("");
		screenVars.crrcdDisp.setClassString("");
		screenVars.gcadmdtDisp.setClassString("");
		screenVars.dischdtDisp.setClassString("");
		screenVars.incurdtDisp.setClassString("");
		screenVars.zdoctor.setClassString("");
		screenVars.givname.setClassString("");
		screenVars.zmedprv.setClassString("");
		screenVars.cdesc.setClassString("");
		screenVars.zrsumin.setClassString("");
	}

/**
 * Clear all the variables in Sr582screenctl
 */
	public static void clear(VarModel pv) {
		Sr582ScreenVars screenVars = (Sr582ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.crtable.clear();
		screenVars.crtabled.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.rgpynum.clear();
		screenVars.diagcde.clear();
		screenVars.shortdesc.clear();
		screenVars.crrcdDisp.clear();
		screenVars.crrcd.clear();
		screenVars.gcadmdtDisp.clear();
		screenVars.gcadmdt.clear();
		screenVars.dischdtDisp.clear();
		screenVars.dischdt.clear();
		screenVars.incurdtDisp.clear();
		screenVars.incurdt.clear();
		screenVars.zdoctor.clear();
		screenVars.givname.clear();
		screenVars.zmedprv.clear();
		screenVars.cdesc.clear();
		screenVars.zrsumin.clear();
	}
}
