/*
 * File: P6307at.java
 * Date: 30 August 2009 0:42:52
 * Author: Quipoz Limited
 *
 * Class transformed from P6307AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.procedures.Brkout;
import com.csc.life.contractservicing.recordstructures.Brkoutrec;
import com.csc.life.general.tablestructures.T6005rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.PtshclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.PtsdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.PtshpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Ptsdpf;
import com.csc.life.terminationclaims.dataaccess.model.Ptshpf;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  P6307AT - Part Surrender AT Module.
*  -----------------------------------
*
*  This AT module  is  called  by  the  Part  Surrender  Claims
*  program  P6307,  (through  programming  switching)  and  the
*  Contract number is passed in the AT parameters area  as  the
*  "Primary  Key".  Note:  where  surrender  is mentioned, part
*  surrender is implied.
*
*  NOTE:
*
*  - where "NEW SUMMARISED" is mentioned it implies  the  value
*  contained  in the suffix field on the surrender claim header
*  record PTSHCLM.
*
*  - where "OLD SUMMARISED" is mentioned it implies  the  value
*  contained  in  the  "POLSUM"  field  on  the contract header
*  record CHDRPTS.
*
*  - if a policy within a summarised group of policies is  part
*  surrendered,  then  these  policies  must be "broken-out" in
*  order that the  relevant  part  may  be  processed  and  the
*  remaining  policies re-summarised. See the subroutine called
*  to handle breakout.
*
*  AT PROCESSING
*
*  Get today's date for DATCON1.
*
*  Check the return status codes  for  each  of  the  following
*  records being updated.
*
*  Read  the  Surrender  claim header record (PTSHCLM) for this
*  contract.
*
*  Read the Contract Header "RETRV"  in  order  to  obtain  the
*  TRANNO.
*
*  Call  the  breakout  subroutine  (BREAKOUT)  passing  it the
*  following fields in the linkage:
*
*            - Company
*            - Contract number
*            - Old summarised number
*            - New summarised number
*            - Effective date
*            - Statuz
*
*
*  PROCESS COMPONENTS
*
*  Process all the PTSHCLM records.
*
*  If the PTSHCLM suffix = '0000', then  for  all  the  COVRPTS
*  records present, keyed:
*
*  COY, CHDR, SUFFIX, LIFE, COVERAGE, RIDER:-
*
*  - write a new record as follows:
*
*  -  update the transaction number with the transaction number
*  from the contract header and  update  the  transaction  date
*  with the Surrender Effective Date (default is today's date).
*
*  If  the  PTSHCLM  suffix  is  not = '0000', then for all the
*  COVRPTS records present, (keyed: COY,  CHDR,  SUFFIX,  LIFE,
*  RIDER):
*
*  If  the COVR is a summary and the total "new summarised"  is
*  less than the total "old summarised"  (see  note  at  start)
*  then:
*
*  - rewrite a new summary record as follows:
*
*  -   the  new  COVR  "summary"  replaces  the  previous  COVR
*  "summary", but the values are  in  the  ratio  of (the   NEW
*  summary  value  divided  by  the  OLD  values). Note, that a
*  summarised COVR is not added if the  NEW  summary  value  is
*  zero.
*
*  - write the remaining records as follows:
*
*  -  each  remaining  COVR  record must reflect the percentage
*  change that was made in  the  summary  record,  i.e. a  COVR
*  record  is  written  from  the  old  suffix  down to the new
*  summarised value + 1,  with  the  value  of  (1  divided  by
*  "old").
*
*  -  if  surrendered compare against PTSHCLM (i.e. attempt  to
*  read PTSHCLM).
*
*  If the COVR is a not a summary and Part Surrendered
*
*  - rewrite a new record as follows:
*
*  - update the transaction number with the transaction  number
*  from  the  contract  header  and update the transaction date
*  with the Surrender Effective Date (default is today's date).
*
*  - sum the regular premium amount.
*
*
*  PROCESS PART SURRENDER CLAIM DETAIL RECORDS (PTSDCLM)
*
*  Read all the PTSDCLM records for this contract.
*
*  Access T5687 and for the surrender method  found,  read  the
*  table  (T6598)  behind this method and access the 'Surrender
*  Processing' subroutine.
*
*  DOWHILE
*       there are PTSDCLM records for this  contract/life  Call
*       the "Surrender Processing Subroutine (T5687/T6598)"
*       READH the INCR file, with the SURD KEY
*       UPDATE the INCR record with Validflag '2' and P/Statcode
*  ENDDO
*
*  Linkage area passed to the Surrender processing subroutine:
*
*             - company
*             - contract header number
*             - suffix
*             - life number
*             - joint-life number
*             - coverage
*             - rider
*             - crtable
*             - effective date
*             - estimated value
*             - percentage
*             - actual value
*             - currency
*             - element code
*             - type code
*             - status
*
*
*  PROCESS CONTRACT HEADER
*
*  Read  the  contract  header  (CHDRLIF)  for the contract and
*  update as follows:
*
*       - if the "new summarised total" is less than  the  "old
*       summarised  total", then overwrite the "OLD" value with
*       the "NEW" value.
*
*       - rewrite a new version of CHDRPTS as follows:-
*
*       - update the transaction number by incrementing  by  +1
*       and  update  the  transaction  date  with the Surrender
*       Claim Effective Date (default is today's date).
*
*       - set 'Effective From  Date'  to  'Surrender  Effective
*       Date' (today's date).
*
*       - set 'Effective To Date' to VRCM-MAX-DATE.
*
*
*  GENERAL HOUSE-KEEPING
*
*  1) Write a PTRN transaction record:
*
*       - contract key from contract header
*       - transaction number from contract header
*       - transaction effective date equals today's date
*       - batch key information from AT linkage.
*
*  2)  Update  the  batch  header  by  calling  BATCUP  with  a
*  function of WRITS and the following parameters:
*
*       - transaction count equals 1
*       - all other amounts zero
*       - batch key from AT linkage
*
*  3) Release contract SFTLOCK.
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are COVRSTS and COVRSTA.
* COVRSTS allow only validflag 1 records, and COVRSTA
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through its life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as its item name, the transaction code and
* the contract status of a contract, e.g. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, e.g. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, e.g. AB, will be moved to
* the STTR record. The same principle applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to COVRSTA records, and
* current categories refer to COVRSTS records. AGCMSTS
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the AGCMSTS, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out up to and including the TRANNOR number.
*
*
*
*****************************************************
* </pre>
*/
public class P6307at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P6307AT");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(200);
	protected PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 0);
	protected PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 4);
	protected PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransArea, 8);
	protected FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 16);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(ZERO);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaBappmeth = new FixedLengthStringData(4);
	//private PackedDecimalData wsaaZbinstprem = new PackedDecimalData(17, 2);
	//private PackedDecimalData wsaaZlinstprem = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
		/* ERRORS */
	private static final String f294 = "F294";
	private static final String e966 = "E966";

	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();

	private IncrTableDAM incrIO = new IncrTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	//private PtsdclmTableDAM ptsdclmIO = new PtsdclmTableDAM();
	private PtshclmTableDAM ptshclmIO = new PtshclmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	protected Varcom varcom = new Varcom();
	private Brkoutrec brkoutrec = new Brkoutrec();
	private T5679rec t5679rec = new T5679rec();
	protected T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T7508rec t7508rec = new T7508rec();
	private T6005rec t6005rec = new T6005rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Surpcpy surpcpy = new Surpcpy();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	protected Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private SysrSyserrRecInner sysrSyserrRecInner = new SysrSyserrRecInner();
	private PtshpfDAO ptshDao =getApplicationContext().getBean("ptshpfDAO",PtshpfDAO.class);
	protected Ptshpf ptshObj=null;
	private List<Ptshpf> ptshList=null;
	private PtsdpfDAO ptsdDao =getApplicationContext().getBean("ptsdpfDAO",PtsdpfDAO.class);
	private Ptsdpf ptsdObj=null;
	private List<Ptsdpf> ptsdList=null;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected List<Itempf> t5687List;
	private List<Itempf> t6005List;
	private List<Itempf> t6598List;
	//ILIFE6594
	private List<Itempf> t5679List;
	private List<Itempf> t7508List;
	protected List<Covrpf> covrRecord;
	
	protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected Covrpf covrpf;
	protected List<Covrpf> covrUpdateRecord;
	protected List<Covrpf> covrInsertRecord;
	private BigDecimal wsaaLocalZbinstprem = BigDecimal.ZERO;
	private BigDecimal wsaaLocalZlinstprem = BigDecimal.ZERO;
	private String strCompany= new String();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		gotLastRecord1124
	}

	public P6307at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initSmartTable(){
	t5687List = itemDAO.getAllitems("IT", atmodrec.company.toString().trim(), "T5687");
	t6005List = itemDAO.getAllitems("IT", atmodrec.company.toString().trim(), "T6005");
	t6598List = itemDAO.getAllitems("IT", atmodrec.company.toString().trim(), "T6598");
	t5679List = itemDAO.getAllitems("IT", atmodrec.company.toString().trim(), "T5679");
	t7508List = itemDAO.getAllitems("IT", atmodrec.company.toString().trim(), "T7508");
}
protected void mainline0000()
	{
		/*MAINLINE*/
		strCompany=atmodrec.company.toString().trim();
		initSmartTable();
		initialise1000();
		
         	for(int intptshCounter=0;intptshCounter<ptshList.size();intptshCounter++){
			ptshObj=ptshList.get(intptshCounter);
			process2000();
		}
		endOfDrivingFile4000();
		a000Statistics();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(sysrSyserrRecInner.sysrStatuz, varcom.bomb)) {
			return ;
		}
		sysrSyserrRecInner.sysrSyserrStatuz.set(sysrSyserrRecInner.sysrStatuz);
		if (isNE(sysrSyserrRecInner.sysrSyserrType, "2")) {
			sysrSyserrRecInner.sysrSyserrType.set("1");
		}
		callProgram(Syserr.class, sysrSyserrRecInner.sysrSyserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		readContractHeader1050();
		/*PERFORM 1100-READ-SURRENDER-HEADER.                          */
		callBreakout1120();
		readStatusCodes1150();
		callDatcons1200();
		/*EXIT*/
	}

protected void readContractHeader1050()
	{
		readContract1051();
	}

protected void readContract1051()
	{
		/*  retrieve the contract header and store the transaction number*/
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransArea.set(atmodrec.transArea);
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(strCompany);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction("READR");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		wsaaTranno.set(chdrlifIO.getTranno());
	}

	/**
	* <pre>
	* Call the BRKOUT subroutine to breakout COVR + AGCM
	* INCR (Increase Pending) records are similarly broken out.   *
	* </pre>
	*/
protected void callBreakout1120()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					go1121();
					read1122();
				//case gotLastRecord1124:
					//gotLastRecord1124();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go1121(){
		/* We have to find out the smallest plan suffix of the surrenders  */
		/* (if more than one) since the last transaction. Because of the   */
		/* key structure of PTSHHCLM, we have to read the whole file       */
		/* until contract break to find out what the smallest plan suffix  */
		/* is.                                                             */
		/* Example: If we have a plan of 10 summarised policies and if     */
		/*          we select policy number 10, 9, 8, and 7 to be          */
		/*          surrenderd. we would have the following records in     */
		/*          this order on the file:-                               */
		/* Company  Cont. No. Tran. No  Suffix                             */
		/*   01     00000123    00001    0010                              */
		/*   01     00000123    00002    0009                              */
		/*   01     00000123    00003    0008                              */
		/*   01     00000123    00004    0007                              */
		/* Now you see why!                                                */
	ptshObj=new Ptshpf();
	ptshObj.setChdrcoy(strCompany);
	ptshObj.setChdrnum(wsaaPrimaryChdrnum.toString().trim());
	ptshObj.setTranno(wsaaTranno.toInt());
	ptshList=ptshDao.serachPtshRecord(ptshObj);
}

protected void read1122(){
	for(int intptshCounter=0;intptshCounter<ptshList.size();intptshCounter++){
		ptshObj=ptshList.get(intptshCounter);
		wsaaPlanSuffix.set(ptshObj.getPlanSuffix());
	}
	gotLastRecord1124();
}

protected void gotLastRecord1124()
	{
		//ptshclmIO.setPlanSuffix(wsaaPlanSuffix);
	    ptshObj.setPlanSuffix(wsaaPlanSuffix.getbigdata());
		if(ptshObj.getPlanSuffix().equals(chdrlifIO.getPolsum().getbigdata())
		|| ptshObj.getPlanSuffix().equals(BigDecimal.ZERO)
		|| isEQ(chdrlifIO.getPolsum(), 1)){
			return;
		}
		brkoutrec.brkOldSummary.set(chdrlifIO.getPolsum());
		compute(brkoutrec.brkNewSummary, 0).set(ptshObj.getPlanSuffix().subtract(BigDecimal.ONE));
		brkoutrec.brkChdrnum.set(chdrlifIO.getChdrnum());
		brkoutrec.brkChdrcoy.set(chdrlifIO.getChdrcoy());
		brkoutrec.brkBatctrcde.set(wsaaBatckey.batcBatctrcde);
		brkoutrec.brkStatuz.set(SPACES);
		callProgram(Brkout.class, brkoutrec.outRec);
		if (isNE(brkoutrec.brkStatuz, "****")) {
			sysrSyserrRecInner.sysrParams.set(brkoutrec.brkStatuz);
			xxxxFatalError();
		}

	}

	/**
	* <pre>
	* Read the status codes for the contract type.                *
	* </pre>
	*/
protected void readStatusCodes1150(){
	String keyItemitem = wsaaBatckey.batcBatctrcde.toString().trim();
	Iterator<Itempf> itemIterator = t5679List.iterator();
	while (itemIterator.hasNext()) {
		Itempf itempf = itemIterator.next();
		if (itempf.getItemitem().trim().equals(keyItemitem)) {
			t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			return;
		}
	}
}

	/**
	* <pre>
	* Call DATCON3 subroutine.                                    *
	* </pre>
	*/
protected void callDatcons1200()
	{
		callDatcons1201();
		datcon11205();
	}

protected void callDatcons1201()
	{
		/* Get the frequency Factor from DATCON3 for Regular premiums.*/
		if (isEQ(chdrlifIO.getBillfreq(), "00")) {
			datcon3rec.freqFactor.set(1);
			return ;
		}
		datcon3rec.intDate1.set(chdrlifIO.getCcdate());
		datcon3rec.intDate2.set(chdrlifIO.getBillcd());
		datcon3rec.frequency.set(chdrlifIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(datcon3rec.datcon3Rec);
			xxxxFatalError();
		}
	}

protected void datcon11205()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
		/*EXIT*/
	}

	/**
	* <pre>
	* Do everything.                                              *
	* </pre>
	*/
protected void process2000()
	{
		go2001();
	}

protected void go2001()
	{
		/*  store fields of the SURrender header for use later when*/
		/*  calling the SURrender processing sub routine*/
		surpcpy.effdate.set(ptshObj.getEffdate());
		surpcpy.cnttype.set(ptshObj.getCnttype());
		if (ptshObj.getPlanSuffix().equals(BigDecimal.ZERO)) {
			processWholePlan2050();
		}
		else {
			partPlanSurrender3100();
		}
	}

protected void processWholePlan2050()
	{
		/*PROCESS*/
		processCoversWhole2400();
		/*EXIT*/
	}

	/**
	* <pre>
	* Process the related COVERS.                                 *
	* </pre>
	*/
protected void processCoversWhole2400()
	{
	 covrRecord = new LinkedList<Covrpf>();
	 covrUpdateRecord=new LinkedList<Covrpf>();
	 covrInsertRecord=new LinkedList<Covrpf>();

	 
	 covrRecord.addAll(covrpfDAO.getCovrsurByComAndNum(strCompany, chdrlifIO.getChdrnum().toString().trim()));
	 for (int intCounter=0;intCounter<covrRecord.size();intCounter++){
		 covrpf=covrRecord.get(intCounter);
		 if(covrpf.getValidflag().equals("1")){
			 updateComponent2450();
		 }
	 }
	 if(!covrUpdateRecord.isEmpty()){
		 covrpfDAO.updateCovrBulk(covrUpdateRecord);
	 }
	 //ILIFE-4620 
	 for (int intCounter=0;intCounter<covrUpdateRecord.size();intCounter++){
		 covrpf=covrUpdateRecord.get(intCounter);
		 updateComponent2452();
	 }
	 if(!covrInsertRecord.isEmpty()){
		 covrpfDAO.insertCovrBulk(covrInsertRecord);
	 }
		/*EXIT*/
	}

protected void updateComponent2450() {
	covrpf.setValidflag("2");
	covrpf.setCurrto(ptshObj.getEffdate().intValue());
	covrUpdateRecord.add(covrpf);
}
protected void updateComponent2452(){
	Iterator<Itempf> itemIterator = t5687List.iterator();
	while(itemIterator.hasNext()){
			Itempf itempf=itemIterator.next();
			if(itempf.getItemitem().trim().equals(covrpf.getCrtable().trim())){/* IJTI-1523 */
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				break;
			}
	}
	covrpf.setValidflag("1");
	covrpf.setCurrto(varcom.vrcmMaxDate.toInt());
	covrpf.setCurrfrom(ptshObj.getEffdate().intValue());
	covrpf.setTranno(ptshObj.getTranno());
	covrpf.setTransactionDate(wsaaTransactionDate.toInt());
	covrpf.setTransactionTime(wsaaTransactionTime.toInt());
	covrpf.setUser(wsaaUser.toInt());
	covrpf.setTermid(wsaaTermid.toString().trim());
	covrpf.setRiskprem(BigDecimal.ZERO);//ILIFE-7956
	covrpf.setZclstate(covrpf.getZclstate());
	covrInsertRecord.add(covrpf);
}
	/**
	* <pre>
	* Process the lives.                                          *
	* </pre>
	*/
protected void processLives2700()
	{
		/*LIVES*/
		lifeIO.setParams(SPACES);
		lifeIO.setChdrnum(chdrlifIO.getChdrnum());
		lifeIO.setChdrcoy(atmodrec.company);
		lifeIO.setFormat(formatsInner.liferec);
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction("BEGN");
		while ( !(isEQ(lifeIO.getStatuz(), varcom.endp))) {
			updateLives2800();
		}

		/*EXIT*/
	}

	/**
	* <pre>
	* Update life records.                                        *
	* </pre>
	*/
protected void updateLives2800(){
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)
		&& isNE(lifeIO.getStatuz(), varcom.endp)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(lifeIO.getStatuz(), varcom.endp)
		|| isNE(lifeIO.getChdrnum(), ptshclmIO.getChdrnum())
		|| isNE(atmodrec.company, lifeIO.getChdrcoy())) {
			lifeIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(lifeIO.getValidflag(), "1")) {
			lifeIO.setFunction(varcom.nextr);
			return ;
		}
		lifeIO.setTransactionDate(wsaaTransactionDate);
		lifeIO.setTransactionTime(wsaaTransactionTime);
		lifeIO.setUser(wsaaUser);
		lifeIO.setTermid(wsaaTermid);
		lifeIO.setValidflag("2");
		lifeIO.setCurrto(ptshclmIO.getEffdate());
		lifeIO.setFunction(varcom.updat);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			xxxxFatalError();
		}
		/*MOVE WSAA-TRANNO            TO LIFE-TRANNO.                  */
		lifeIO.setTranno(ptshclmIO.getTranno());
		lifeIO.setValidflag("1");
		lifeIO.setCurrto(varcom.vrcmMaxDate);
		lifeIO.setCurrfrom(ptshclmIO.getEffdate());
		if (isEQ(lifeIO.getJlife(), "00")
		|| isEQ(lifeIO.getJlife(), "  ")) {
			lifeIO.setStatcode(t5679rec.setLifeStat);
		}
		else {
			lifeIO.setStatcode(t5679rec.setJlifeStat);
		}
		lifeIO.setFunction(varcom.writr);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifeIO.getParams());
			xxxxFatalError();
		}
		lifeIO.setFunction(varcom.nextr);
	}

protected void partPlanSurrender3100(){
		/*READ*/
		/* Read coverages.*/
		/* MOVE SPACES                 TO COVRSUR-PARAMS.               */
		/* MOVE ATMD-COMPANY           TO COVRSUR-CHDRCOY.              */
		/* MOVE CHDRLIF-CHDRNUM        TO COVRSUR-CHDRNUM.              */
		/* MOVE PTSHCLM-PLAN-SUFFIX    TO COVRSUR-PLAN-SUFFIX.          */
		/* MOVE BEGN                   TO COVRSUR-FUNCTION.             */
		/* PERFORM 3300-UPDATE-COMPONENT UNTIL COVRSUR-STATUZ = ENDP.   */

	covrRecord = new LinkedList<Covrpf>();
	 covrUpdateRecord=new LinkedList<Covrpf>();
	 covrInsertRecord=new LinkedList<Covrpf>();
	
	
	
	 covrRecord.addAll(covrpfDAO.getCovrsurByComAndNum(strCompany, chdrlifIO.getChdrnum().toString().trim()));
	for (int intCounter=0;intCounter<covrRecord.size();intCounter++){
		 covrpf=covrRecord.get(intCounter);
		 if(covrpf.getValidflag().equals("1")){
			 updateComponent3300();
		 }
	 }
	 if(!covrUpdateRecord.isEmpty()){
		 covrpfDAO.updateCovrBulk(covrUpdateRecord);
	 }
	 for (int intCounter=0;intCounter<covrRecord.size();intCounter++){
		 covrpf=covrRecord.get(intCounter);
		 updateComponent3302();
	 }
	 if(!covrInsertRecord.isEmpty()){
		 covrpfDAO.insertCovrBulk(covrInsertRecord);
	 }
		/*EXIT*/
	}

protected void updateComponent3300(){
		/* Delete all this section as it uses COVRSUR. It will be replaced */
		/* by COVRBRK. It is easier to remove all these lines and          */
		/* replace them by the ones that are required.                     */
		/* CALL 'COVRSURIO'            USING COVRSUR-PARAMS.            */
		/* IF COVRSUR-STATUZ           NOT = O-K AND                    */
		/*                             NOT = ENDP                       */
		/*    MOVE COVRSUR-PARAMS      TO SYSR-PARAMS                   */
		/*    PERFORM XXXX-FATAL-ERROR.                                 */
		/* IF COVRSUR-CHDRCOY          NOT = ATMD-COMPANY OR            */
		/*    COVRSUR-CHDRNUM          NOT = CHDRLIF-CHDRNUM OR         */
		/*    COVRSUR-PLAN-SUFFIX      NOT = PTSHCLM-PLAN-SUFFIX OR     */
		/*    COVRSUR-STATUZ           = ENDP                           */
		/*    MOVE ENDP TO COVRSUR-STATUZ                               */
		/*    GO TO 3309-EXIT.                                          */
		/* We only want to process valid flag 1 records                    */
		/* IF COVRSUR-VALIDFLAG           = '2'                 <A06731>*/
		/*    MOVE NEXTR               TO COVRSUR-FUNCTION      <A06731>*/
		/*    GO TO 3309-EXIT                                   <A06731>*/
		/* END-IF.                                              <A06731>*/
		/* Update the Covr record with a valid flag of 2*/
		/* MOVE '2'                    TO COVRSUR-VALIDFLAG.            */
		/*MOVE PTSHCLM-EFFDATE        TO COVRSUR-CRRCD.                */
		/* MOVE PTSHCLM-EFFDATE        TO COVRSUR-CURRTO.       <A06731>*/
		/* MOVE UPDAT                  TO COVRSUR-FUNCTION.             */
		/* MOVE COVRSURREC             TO COVRSUR-FORMAT.               */
		/* CALL 'COVRSURIO'            USING COVRSUR-PARAMS.            */
		/* IF COVRSUR-STATUZ           NOT = O-K                        */
		/*    MOVE COVRSUR-PARAMS      TO SYSR-PARAMS                   */
		/*    PERFORM XXXX-FATAL-ERROR.                                 */
		/* Access the table to initialise the COVR fields.*/
		/* MOVE SPACES                 TO ITDM-DATA-AREA.               */
		/* MOVE ATMD-COMPANY           TO ITDM-ITEMCOY.                 */
		/* MOVE T5687                  TO ITDM-ITEMTABL.                */
		/* MOVE COVRSUR-CRTABLE        TO ITDM-ITEMITEM.                */
		/* MOVE COVRSUR-CRRCD          TO ITDM-ITMFRM.                  */
		/* MOVE BEGN                   TO ITDM-FUNCTION.                */
		/* CALL 'ITDMIO' USING         ITDM-PARAMS.                     */
		/* IF ITDM-STATUZ              NOT = O-K AND NOT = ENDP         */
		/*    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   */
		/*    PERFORM XXXX-FATAL-ERROR.                                 */
		/* IF ITDM-ITEMCOY             NOT = ATMD-COMPANY  OR           */
		/*    ITDM-ITEMTABL            NOT = T5687 OR                   */
		/*    ITDM-ITEMITEM            NOT = COVRSUR-CRTABLE OR         */
		/*    ITDM-STATUZ              = ENDP                           */
		/*    MOVE COVRSUR-CRTABLE     TO ITDM-ITEMITEM         <A06731>*/
		/*    MOVE ITDM-PARAMS         TO SYSR-PARAMS                   */
		/*    MOVE F294                TO SYSR-STATUZ           <A06731>*/
		/*    PERFORM XXXX-FATAL-ERROR                                  */
		/* ELSE                                                         */
		/*    MOVE ITDM-GENAREA        TO T5687-T5687-REC.              */
		/* MOVE '1'                    TO COVRSUR-VALIDFLAG.            */
		/* MOVE VRCM-MAX-DATE          TO COVRSUR-CURRTO.               */
		/* MOVE PTSHCLM-EFFDATE        TO COVRSUR-CURRFROM.             */
		/*MOVE CHDRLIF-TRANNO         TO COVRSUR-TRANNO.               */
		/* MOVE PTSHCLM-TRANNO         TO COVRSUR-TRANNO.       <A06731>*/
		/*    MOVE T5679-SET-COV-RISK-STAT TO COVRSUR-STATCODE.*/
		/*    IF  COVRSUR-RIDER           = '00' OR SPACES*/
		/*        MOVE T5679-SET-COV-RISK-STAT TO COVRSUR-STATCODE*/
		/*    ELSE*/
		/*        MOVE T5679-SET-RID-RISK-STAT TO COVRSUR-STATCODE.*/
		/*    IF  CHDRLIF-BILLFREQ            NOT = '00'*/
		/*        AND T5687-SINGLE-PREM-IND   NOT = 'Y'*/
		/*        IF COVRSUR-RIDER            = SPACES OR '00'*/
		/*           MOVE T5679-SET-COV-PREM-STAT TO COVRSUR-PSTATCODE*/
		/*        ELSE*/
		/*           MOVE T5679-SET-RID-PREM-STAT TO COVRSUR-PSTATCODE*/
		/*    ELSE*/
		/*        IF COVRSUR-RIDER            = SPACES OR '00'*/
		/*           MOVE T5679-SET-SNGP-COV-STAT TO COVRSUR-PSTATCODE*/
		/*        ELSE*/
		/*           MOVE T5679-SET-SNGP-COV-STAT TO COVRSUR-PSTATCODE.*/
		/* MOVE WSAA-TRANSACTION-DATE      TO COVRSUR-TRANSACTION-DATE. */
		/* MOVE WSAA-TRANSACTION-TIME      TO COVRSUR-TRANSACTION-TIME. */
		/* MOVE WSAA-USER                  TO COVRSUR-USER.             */
		/* MOVE WSAA-TERMID                TO COVRSUR-TERMID.           */
		/* MOVE WRITR                      TO COVRSUR-FUNCTION.         */
		/* MOVE COVRSURREC                 TO COVRSUR-FORMAT.           */
		/* CALL 'COVRSURIO'                USING COVRSUR-PARAMS.        */
		/* IF COVRSUR-STATUZ               NOT = O-K                    */
		/*    MOVE COVRSUR-PARAMS          TO SYSR-PARAMS               */
		/*    PERFORM XXXX-FATAL-ERROR.                                 */
		/* MOVE NEXTR                       TO COVRSUR-FUNCTION.        */
		/* Use the logical COVRBRK instead of COVRSUR.                     */
	
	covrpf.setValidflag("2");
	covrpf.setCurrto(ptshObj.getEffdate().intValue());
	covrUpdateRecord.add(covrpf);	
}
protected void updateComponent3302(){
	Iterator<Itempf> itemIterator = t5687List.iterator();
	while(itemIterator.hasNext()){
			Itempf itempf=itemIterator.next();
			if(itempf.getItemitem().trim().equals(covrpf.getCrtable().trim())){/* IJTI-1523 */
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				break;
			}
	}
	getBapPayMthd3400();
	covrpf.setValidflag("1");
	covrpf.setCurrto(varcom.vrcmMaxDate.toInt());
	covrpf.setCurrfrom(ptshObj.getEffdate().intValue());
	covrpf.setTranno(ptshObj.getTranno());
	covrpf.setTransactionDate(wsaaTransactionDate.toInt());
	covrpf.setTransactionTime(wsaaTransactionTime.toInt());
	covrpf.setUser(wsaaUser.toInt());
	covrpf.setTermid(wsaaTermid.toString().trim());
	covrpf.setPayrseqno(1);
	covrpf.setBappmeth(wsaaBappmeth.toString().trim());
	covrpf.setZbinstprem(wsaaLocalZbinstprem);
	covrpf.setZlinstprem(wsaaLocalZlinstprem);
	covrpf.setZclstate(covrpf.getZclstate());
	covrInsertRecord.add(covrpf);
}
protected void getBapPayMthd3400()
	{
		start3401();
		getInstPrem3405();
	}

protected void start3401()
	{
	Iterator<Itempf> itemIterator = t6005List.iterator();
	while(itemIterator.hasNext()){
			Itempf itempf=itemIterator.next();
			if(itempf.getItemitem().trim().equals(covrpf.getCrtable().trim())){/* IJTI-1523 */
				t6005rec.t6005Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				break;
			}
	}
		if (isEQ(t6005rec.ind, "1")){
			wsaaBappmeth.set(t6005rec.bappmeth01);
		}
		else if (isEQ(t6005rec.ind, "2")){
			wsaaBappmeth.set(t6005rec.bappmeth02);
		}
		else if (isEQ(t6005rec.ind, "3")){
			wsaaBappmeth.set(t6005rec.bappmeth03);
		}
		else if (isEQ(t6005rec.ind, "4")){
			wsaaBappmeth.set(t6005rec.bappmeth04);
		}
		else if (isEQ(t6005rec.ind, "5")){
			wsaaBappmeth.set(t6005rec.bappmeth05);
		}
		else{
			wsaaBappmeth.set(SPACES);
		}
	}

protected void getInstPrem3405(){

		if (isEQ(covrpf.getInstprem(), ZERO)) {
			wsaaLocalZbinstprem=covrpf.getSingp();
		}
		else {
			wsaaLocalZbinstprem=covrpf.getInstprem();
		}
	}

	/**
	* <pre>
	* Housekeeping prior to termination.                          *
	* </pre>
	*/
protected void endOfDrivingFile4000()
	{
		/*HOUSEKEEPING-TERMINATE*/
		processPtsdclmRecs4100();
		/*    PERFORM 4300-WRITE-NEW-CONT-HEADER.                          */
		ptrnTransaction4400();
		writeNewContHeader4300();
		batchHeader4500();
		dryProcessing6000();
		releaseSoftlock4600();
		/*EXIT*/
	}

	/**
	* <pre>
	* Process related SURrender details records                   *
	* </pre>
	*/
protected void processPtsdclmRecs4100(){

	ptsdObj=new Ptsdpf();
	
	ptsdObj.setChdrcoy(strCompany);
	ptsdObj.setChdrnum(chdrlifIO.getChdrnum().toString().trim());
	ptsdObj.setTranno(wsaaTranno.toInt());
	ptsdList=ptsdDao.serachPtsdRecord(ptsdObj);
	for(int intpstdRowCounter=0;intpstdRowCounter<ptsdList.size();intpstdRowCounter++){
		ptsdObj=ptsdList.get(intpstdRowCounter);
		updateSurrenders4150();
	}
}

protected void updateSurrenders4150(){
		wsaaTranno.set(ptsdObj.getTranno());
		readTables4200();
		incrCheck5000();
		surpcpy.planSuffix.set(ptsdObj.getPlanSuffix());
		surpcpy.crtable.set(ptsdObj.getCrtable());
		surpcpy.chdrcoy.set(ptsdObj.getChdrcoy());
		surpcpy.chdrnum.set(ptsdObj.getChdrnum());
		surpcpy.life.set(ptsdObj.getLife());
		surpcpy.jlife.set(ptsdObj.getJlife());
		surpcpy.coverage.set(ptsdObj.getCoverage());
		surpcpy.rider.set(ptsdObj.getRider());
		surpcpy.fund.set(ptsdObj.getVirtualFund());
		surpcpy.status.set(SPACES);
		surpcpy.tranno.set(ptsdObj.getTranno());
		surpcpy.estimatedVal.set(ptsdObj.getEstMatValue());
		surpcpy.actualVal.set(ptsdObj.getActvalue());
		surpcpy.type.set(ptsdObj.getFieldType());
		surpcpy.currcode.set(ptsdObj.getCurrcd());
		surpcpy.percreqd.set(ptsdObj.getPercreqd());
		/* MOVE PTSDCLM-CURRCD         TO SURP-CNTCURR.                 */
		surpcpy.cntcurr.set(chdrlifIO.getCntcurr());
		surpcpy.batckey.set(wsaaBatckey.batcFileKey);
		surpcpy.termid.set(wsaaTermid);
		surpcpy.date_var.set(wsaaTransactionDate);
		surpcpy.time.set(wsaaTransactionTime);
		surpcpy.user.set(wsaaUser);
		surpcpy.status.set("****");
		surpcpy.language.set(atmodrec.language);
		if (isNE(surpcpy.type, "C")) {
			callSurrenderMethod4250();
		}		
	}

protected void readTables4200(){
	Iterator<Itempf> itemIterator = t5687List.iterator();
	while(itemIterator.hasNext()){
			Itempf itempf=itemIterator.next();
			if(itempf.getItemitem().trim().equals(ptsdObj.getCrtable().trim())){/* IJTI-1523 */
				t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				break;
			}
	}
	itemIterator = t6598List.iterator();
	while(itemIterator.hasNext()){
			Itempf itempf=itemIterator.next();
			if(itempf.getItemitem().trim().equals(t5687rec.partsurr.toString().trim())){
				t6598rec.t6598Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				return;
			}
	}
}

protected void callSurrenderMethod4250()
	{
		/*READ*/
		callProgram(t6598rec.procesprog, surpcpy.surrenderRec);
		if (isEQ(surpcpy.status, varcom.bomb)) {
			sysrSyserrRecInner.sysrStatuz.set(surpcpy.status);
			xxxxFatalError();
		}
		if (isEQ(surpcpy.status, varcom.endp)) {
			return ;
		}
		if (isNE(surpcpy.status, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(surpcpy.status);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void writeNewContHeader4300(){
		/*    Read CHDR & hold ready for update*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFunction("READH");
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		/*  update the contract header*/
		/* TRANNO has already been updated in P5089.                       */
		/*ADD 1 TO CHDRLIF-TRANNO.                                     */
		/* Update new POLSUM.                                              */
		/*    IF PTSHCLM-PLAN-SUFFIX        > CHDRLIF-POLSUM OR            */
		/*       PTSHCLM-PLAN-SUFFIX        = ZERO                         */
		if (isGT(wsaaPlanSuffix, chdrlifIO.getPolsum())
		|| isEQ(wsaaPlanSuffix, ZERO)
		|| isEQ(chdrlifIO.getPolsum(), 1)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isLT(brkoutrec.brkNewSummary, 2)) {
				chdrlifIO.setPolsum(ZERO);
			}
			else {
				chdrlifIO.setPolsum(brkoutrec.brkNewSummary);
			}
		}
		chdrlifIO.setTranno(wsaaTranno);
		chdrlifIO.setFunction("REWRT");
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			xxxxFatalError();
		}
		return ;
	}

	/**
	* <pre>
	* Write PTRN transaction.                                     *
	* </pre>
	*/
protected void ptrnTransaction4400(){
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTransactionDate(0);
		ptrnIO.setTransactionTime(0);
		ptrnIO.setUser(0);
		ptrnIO.setDataKey(atmodrec.batchKey);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		/* MOVE WSAA-TODAY             TO PTRN-PTRNEFF.                 */
		/*  Re-read the PTSHCLM record to get the effective date for    */
		/*  this transaction. It could be different for separate        */
		/*  policy transactions done on the same contract under the     */
		/*  same transaction. Note that whilst the PLNSFX is part of    */
		/*  key to the PTSHCLM it is not required as TRANNOs are        */
		/*  unique.                                                     */
		ptshclmIO.setChdrcoy(atmodrec.company);
		ptshclmIO.setChdrnum(wsaaPrimaryChdrnum);
		ptshclmIO.setTranno(ptrnIO.getTranno());
		ptshclmIO.setPlanSuffix(ZERO);
		ptshclmIO.setFunction("BEGN");
		ptshclmIO.setFormat(formatsInner.ptshclmrec);
		SmartFileCode.execute(appVars, ptshclmIO);
		if (isNE(ptshclmIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptshclmIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(ptshclmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(ptshclmIO.getChdrcoy(), atmodrec.company)
		|| isNE(ptshclmIO.getChdrnum(), wsaaPrimaryChdrnum)
		|| isNE(ptshclmIO.getTranno(), ptrnIO.getTranno())) {
			sysrSyserrRecInner.sysrParams.set(chdrlifIO.getParams());
			sysrSyserrRecInner.sysrStatuz.set(e966);
			xxxxFatalError();
		}
		ptrnIO.setPtrneff(ptshclmIO.getEffdate());
		/* MOVE PTSHCLM-EFFDATE        TO PTRN-DATESUB.         <LA5184>*/
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setTermid(wsaaTermid);
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(ptrnIO.getParams());
			xxxxFatalError();
		}
		/* Write a PTRN for each surrender(set of details).                */
		/* CHDRLIF-TRANNO = first set.                                     */
		/* WSAA-TRANNO = last set.                                         */
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		if (isGT(chdrlifIO.getTranno(), wsaaTranno)) {
			/*NEXT_SENTENCE*/
		}
		else {
			ptrnTransaction4400();
			return ;
		}
	}

	/**
	* <pre>
	* Write batch header transaction.                             *
	* </pre>
	*/
protected void batchHeader4500()
	{
		/*BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(batcuprec.batcupRec);
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Release Soft locked record.                                 *
	* </pre>
	*/
protected void releaseSoftlock4600()
	{
		releaseSoftlock4601();
	}

protected void releaseSoftlock4601()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrStatuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void incrCheck5000()
	{
		para5010();
	}

protected void para5010()
	{
		incrIO.setDataArea(SPACES);
		incrIO.setChdrcoy(ptsdObj.getChdrcoy());
		incrIO.setChdrnum(ptsdObj.getChdrnum());
		incrIO.setLife(ptsdObj.getLife());
		incrIO.setCoverage(ptsdObj.getCoverage());
		incrIO.setRider(ptsdObj.getRider());
		incrIO.setPlanSuffix(ptsdObj.getPlanSuffix());
		incrIO.setFunction(varcom.readh);
		incrIO.setFormat(formatsInner.incrrec);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), varcom.oK)
		&& isNE(incrIO.getStatuz(), varcom.mrnf)) {
			sysrSyserrRecInner.sysrParams.set(incrIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(incrIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		incrIO.setValidflag("2");
		if (isNE(t5679rec.setCovPremStat, SPACES)) {
			incrIO.setPstatcode(t5679rec.setCovPremStat);
		}
		if (isNE(t5679rec.setCovRiskStat, SPACES)) {
			incrIO.setStatcode(t5679rec.setCovRiskStat);
		}
		incrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(incrIO.getParams());
			xxxxFatalError();
		}
	}

protected void dryProcessing6000(){
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		//ILIFE-6594
		wsaaT7508Cnttype.set(chdrlifIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		String keyItemitem = wsaaT7508Key.toString().trim();
		Iterator<Itempf> itemIterator = t7508List.iterator();
		while (itemIterator.hasNext()) {
			Itempf itempf = itemIterator.next();
			if (itempf.getItemitem().trim().equals(keyItemitem)) {
				t7508rec.t7508Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				return;
			}else{
				wsaaT7508Cnttype.set("***");
				if (itempf.getItemitem().trim().equals(keyItemitem)) {
					t7508rec.t7508Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					return;
				}else{
					return;
				}
			}
		}
		
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrlifIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(drypDryprcRecInner.drypDryprcRec);
			sysrSyserrRecInner.sysrStatuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void a000Statistics(){
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		/*MOVE CHDRPTS-CHDRCOY        TO LIFS-CHDRCOY.            <012>*/
		/*MOVE CHDRPTS-CHDRNUM        TO LIFS-CHDRNUM.            <012>*/
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(chdrlifIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			sysrSyserrRecInner.sysrParams.set(lifsttrrec.lifsttrRec);
			sysrSyserrRecInner.sysrStatuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
	private FixedLengthStringData ptshclmrec = new FixedLengthStringData(10).init("PTSHCLMREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData incrrec = new FixedLengthStringData(10).init("INCRREC");
}
/*
 * Class transformed  from Data Structure SYSR-SYSERR-REC--INNER
 */
private static final class SysrSyserrRecInner {

		/* System records.*/
	private FixedLengthStringData sysrSyserrRec = new FixedLengthStringData(528);
	private FixedLengthStringData sysrMsgarea = new FixedLengthStringData(512).isAPartOf(sysrSyserrRec, 5);
	private FixedLengthStringData sysrSyserrType = new FixedLengthStringData(1).isAPartOf(sysrMsgarea, 50);
	private FixedLengthStringData sysrDbparams = new FixedLengthStringData(305).isAPartOf(sysrMsgarea, 51);
	private FixedLengthStringData sysrParams = new FixedLengthStringData(305).isAPartOf(sysrDbparams, 0, REDEFINE);
	private FixedLengthStringData sysrDbFunction = new FixedLengthStringData(5).isAPartOf(sysrParams, 0);
	private FixedLengthStringData sysrDbioStatuz = new FixedLengthStringData(4).isAPartOf(sysrParams, 5);
	private FixedLengthStringData sysrLevelId = new FixedLengthStringData(15).isAPartOf(sysrParams, 9);
	private FixedLengthStringData sysrGenDate = new FixedLengthStringData(8).isAPartOf(sysrLevelId, 0);
	private ZonedDecimalData sysrGenyr = new ZonedDecimalData(4, 0).isAPartOf(sysrGenDate, 0).setUnsigned();
	private ZonedDecimalData sysrGenmn = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 4).setUnsigned();
	private ZonedDecimalData sysrGendy = new ZonedDecimalData(2, 0).isAPartOf(sysrGenDate, 6).setUnsigned();
	private ZonedDecimalData sysrGenTime = new ZonedDecimalData(6, 0).isAPartOf(sysrLevelId, 8).setUnsigned();
	private FixedLengthStringData sysrVn = new FixedLengthStringData(1).isAPartOf(sysrLevelId, 14);
	private FixedLengthStringData sysrDataLocation = new FixedLengthStringData(1).isAPartOf(sysrParams, 24);
	private FixedLengthStringData sysrIomod = new FixedLengthStringData(10).isAPartOf(sysrParams, 25);
	private BinaryData sysrRrn = new BinaryData(9, 0).isAPartOf(sysrParams, 35);
	private FixedLengthStringData sysrFormat = new FixedLengthStringData(10).isAPartOf(sysrParams, 39);
	private FixedLengthStringData sysrDataKey = new FixedLengthStringData(256).isAPartOf(sysrParams, 49);
	private FixedLengthStringData sysrSyserrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 356);
	private FixedLengthStringData sysrStatuz = new FixedLengthStringData(4).isAPartOf(sysrMsgarea, 360);
	private FixedLengthStringData sysrNewTech = new FixedLengthStringData(1).isAPartOf(sysrSyserrRec, 517).init("Y");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(sysrSyserrRec, 518).init("ITEMREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

		/* COPY UBBLALLPAR.                                             */
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
