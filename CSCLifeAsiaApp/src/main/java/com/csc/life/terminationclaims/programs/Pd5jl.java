package com.csc.life.terminationclaims.programs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.CrelpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Crelpf;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.contractservicing.recordstructures.Pd5h5rec;
import com.csc.life.newbusiness.dataaccess.dao.BnfypfDAO;
import com.csc.life.newbusiness.dataaccess.dao.CpbnfypfDAO;
import com.csc.life.newbusiness.dataaccess.model.Bnfypf;
import com.csc.life.newbusiness.dataaccess.model.Cpbnfypf;
import com.csc.life.newbusiness.tablestructures.Tr52yrec;
import com.csc.life.newbusiness.tablestructures.Tr52zrec;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5662rec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.screens.Sd5jlScreenVars;
import com.csc.smart.dataaccess.dao.CcfkpfDAO;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BaseScreenData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;

public class Pd5jl extends ScreenProgCS {

    public static final String ROUTINE = QPUtilities.getThisClass();
    public int numberOfParameters = 0;
    private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pd5jl");
    private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
    private int ix = 0;
    private int iy = 0;
    private int iz = 0;
    private int wsaaCount = 0;

    private FixedLengthStringData wsaaMultBnfy = new FixedLengthStringData(20);
    protected ZonedDecimalData[] wsaaBnfyCount = ZDArrayPartOfStructure(10, 2, 0, wsaaMultBnfy, 0, UNSIGNED_TRUE);
    protected int ib = 0;
    private String wsaaCrtBnfyreln = " ";
    protected int wsaaPctSub = 0;
    protected FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8).init(SPACES);
    private static final int wsaaSubfileSize = 30;
    private ZonedDecimalData wsaaIndex = new ZonedDecimalData(5, 0).setUnsigned();
    private int wsaaNofRead;
    private int wsaaLastTranno = 0;

    private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
    private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
    private FixedLengthStringData wsaaOkeyBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaLetokeys, 1);
    private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 5).setUnsigned();

    /*01  WSAA-ITEM-T6634.                                     <PCPPRT>*/
    private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
    private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
    private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);

    private FixedLengthStringData wsaaFound = new FixedLengthStringData(1).init(SPACES);
    protected Validator found = new Validator(wsaaFound, "Y");
    private Validator notFound = new Validator(wsaaFound, "N");

    private FixedLengthStringData wsaaBeneTypeFound = new FixedLengthStringData(1).init("N");
    private Validator beneTypeFound = new Validator(wsaaBeneTypeFound, "Y");

    private FixedLengthStringData wsaaValidRelation = new FixedLengthStringData(1).init(SPACES);
    private Validator validRelation = new Validator(wsaaValidRelation, "Y");

    private FixedLengthStringData wsaaSelfRelation = new FixedLengthStringData(1).init(SPACES);
    private Validator selfRelation = new Validator(wsaaSelfRelation, "Y");
    private Validator noSelfRelation = new Validator(wsaaSelfRelation, "N");

    private FixedLengthStringData wsaaBatcKey = new FixedLengthStringData(22);
    private FixedLengthStringData wsaaBatcBatctrcde = new FixedLengthStringData(5).isAPartOf(wsaaBatcKey, 17);

    private FixedLengthStringData wsaaMandatorys = new FixedLengthStringData(30);
    private FixedLengthStringData[] wsaaMandatory = FLSArrayPartOfStructure(30, 1, wsaaMandatorys, 0);

    private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(3);
    private Validator wsaaAdd = new Validator(wsaaUpdateFlag, "ADD");
    private Validator wsaaDelete = new Validator(wsaaUpdateFlag, "DEL");
    private Validator wsaaModify = new Validator(wsaaUpdateFlag, "MOD");
    protected ZonedDecimalData wsbbTableSub = new ZonedDecimalData(3, 0).setUnsigned();
    private ZonedDecimalData wsbbTableRsub = new ZonedDecimalData(3, 0).setUnsigned();

    private FixedLengthStringData wsaaClntArray = new FixedLengthStringData(420);
    /*                             OCCURS 10.                       */
    protected FixedLengthStringData[] wsaaClient = FLSArrayPartOfStructure(30, 10, wsaaClntArray, 0);
    protected FixedLengthStringData[] wsaaType = FLSArrayPartOfStructure(30, 2, wsaaClntArray, 300);
    protected FixedLengthStringData[] wsaaSequence = FLSArrayPartOfStructure(30, 2, wsaaClntArray, 360);

    private FixedLengthStringData wsaaPctTypes = new FixedLengthStringData(90);
    protected FixedLengthStringData[] wsaaPctType = FLSArrayPartOfStructure(10, 2, wsaaPctTypes, 0);
    protected FixedLengthStringData[] wsaaPctSeq = FLSArrayPartOfStructure(10, 2, wsaaPctTypes, 20);
    protected ZonedDecimalData[] wsaaPctTot = ZDArrayPartOfStructure(10, 5, 2, wsaaPctTypes, 40, UNSIGNED_TRUE);
    protected int wsaaDuprSub = 0;
    private String wsaaPtrnFlag = "";
    protected Datcon2rec datcon2rec = new Datcon2rec();
    private int wsaaRrn = 0;
    /* TABLES */
    private static final String t3584 = "T3584";
    private static final String t5662 = "T5662";
    protected static final String tr52y = "TR52Y";
    private static final String tr52z = "TR52Z";
    private static final String chdrlnbrec = "CHDRLNBREC";

    private static final String chdrclmrec = "CHDRCLMREC";
    protected static final String chdrenqrec = "CHDRENQREC";
    private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
    private Batckey wsaaBatckey = new Batckey();
    protected Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
    private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
    private Cltrelnrec cltrelnrec = new Cltrelnrec();
    private T5662rec t5662rec = new T5662rec();
    private Tr384rec tr384rec = new Tr384rec();
    protected Tr52yrec tr52yrec = new Tr52yrec();
    protected Tr52zrec tr52zrec = new Tr52zrec();
    private Sftlockrec sftlockrec = new Sftlockrec();
    protected Datcon1rec datcon1rec = new Datcon1rec();
    private Letrqstrec letrqstrec = new Letrqstrec();
    private Bldenrlrec bldenrlrec = new Bldenrlrec();
    protected Sd5jlScreenVars sv = ScreenProgram.getScreenVars(Sd5jlScreenVars.class);
    private ErrorsInner errorsInner = new ErrorsInner();
    private WsbbStackArrayInner wsbbStackArrayInner = new WsbbStackArrayInner();
    protected ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
    protected ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
    private CrelpfDAO crelpfDAO = getApplicationContext().getBean("crelpfDAO", CrelpfDAO.class);
    private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
    private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
    private CpbnfypfDAO cpbnfypfDAO = getApplicationContext().getBean("cpbnfypfDAO", CpbnfypfDAO.class);
    private BabrpfDAO babrpfDAO = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class);
    private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
    private CcfkpfDAO ccfkpfDAO = getApplicationContext().getBean("ccfkpfDAO", CcfkpfDAO.class);
    private BnfypfDAO bnfypfDAO = getApplicationContext().getBean("bnfypfDAO", BnfypfDAO.class);
    protected Itempf itempf = null;
    private Descpf descpf = null;
    protected Clntpf clntpf = null;
    private Crelpf crelpf = null;
    private Ptrnpf ptrnpf = null;
    protected Lifepf lifepf = null;
    private Clrrpf clrrpf = null;
    private Cpbnfypf cpbnfypf = null;
    private Bnfypf bnfypf = null;
    private Cpbnfypf instCpbnfypf = null;
    private Babrpf babrpf = null;
    private Clbapf clbapf = null;
    private List<Bnfypf> bnfypfList = null;
    private List<Itempf> itdmpfList = null;
    private List<Cpbnfypf> cpbnfypfList = null;

    private String P5256 = "P5256";
    private String P5318 = "P5318";
    private String btype = "DB";
    private String sequence01 = "01";
    private PackedDecimalData wsaaActualTotVal = new PackedDecimalData(17, 2).init(0);
    private PackedDecimalData wsaaTotVal = new PackedDecimalData(17, 2).init(0);
    private BigDecimal wsaaBnypc = new BigDecimal(0);


    /*Claims Contract Header*/
    //private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
    private Chdrpf chdrpf = new Chdrpf();
    private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);

    private Batckey wsaaBatckey1 = new Batckey();
    private Pd5h5rec ptemprec = new Pd5h5rec();
    int wsaaX = 0;
    int wsaaY = 0;
    private int len = 0;
    private int lenRel = 0;
    private StringBuffer bankAccKeybuffer = null;

    private Optswchrec optswchrec = new Optswchrec();
    private static final String t669 = "T669";

    /**
     * Contains all possible labels used by goTo action.
     */
    protected enum GotoLabel implements GOTOInterface {
        DEFAULT,
        exit2190,
        b790Exit,
        switch4170
    }

    public Pd5jl() {
        super();
        screenVars = sv;
        new ScreenModel("Sd5jl", AppVars.getInstance(), sv);
    }


    protected FixedLengthStringData getWsaaProg() {
        return wsaaProg;
    }

    protected FixedLengthStringData getWsaaVersion() {
        return wsaaVersion;
    }


    /**
     * The mainline method is the default entry point to the class
     */
    public void mainline(Object... parmArray) {
        sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
        scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
        wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
        wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
        try {
            super.mainline();
        } catch (COBOLExitProgramException e) {
            // Expected exception for control flow purposes
        }
    }

    public void processBo(Object... parmArray) {
        sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
        sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
        scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
        wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
        wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

        try {
            processBoMainline(sv, sv.dataArea, parmArray);
        } catch (COBOLExitProgramException e) {
            // Expected exception for control flow purposes
        }
    }

    protected void plainname() {
        /*PLAIN-100*/
        wsspcomn.longconfname.set(SPACES);
        if (clntpf.getClttype().trim().equals("C")) {
            corpname();
            return;
        }
        if (clntpf.getGivname() != null & !clntpf.getGivname().trim().equals("")) {
            StringUtil stringVariable1 = new StringUtil();
            stringVariable1.addExpression(clntpf.getSurname(), "  ");
            stringVariable1.addExpression(", ");
            stringVariable1.addExpression(clntpf.getGivname(), "  ");
            stringVariable1.setStringInto(wsspcomn.longconfname);
        } else {
            wsspcomn.longconfname.set(clntpf.getSurname());
        }
        /*PLAIN-EXIT*/
    }


    protected void corpname() {
        /*PAYEE-1001*/
        wsspcomn.longconfname.set(SPACES);
        /* STRING CLTS-SURNAME         DELIMITED SIZE                   */
        /*        CLTS-GIVNAME         DELIMITED '  '                   */
        StringUtil stringVariable1 = new StringUtil();
        stringVariable1.addExpression(clntpf.getLsurname(), "  ");
        stringVariable1.addExpression(" ");
        stringVariable1.addExpression(clntpf.getLgivname(), "  ");
        stringVariable1.setStringInto(wsspcomn.longconfname);
        /*CORP-EXIT*/
    }

    /**
     * <pre>
     *  END OF CONFNAME **********************************************
     *      INITIALISE FIELDS FOR SHOWING ON SCREEN
     * </pre>
     */
    protected void initialise1000() {
        initialise1010();
        retrieveHeader1030();
        readContractLongdesc1040();
        loadSubfileFields1060();
    }


    protected void initialise1010() {
        if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
            return;
        }
        wsaaBatckey.set(wsspcomn.batchkey);
        varcom.vrcmTranid.set(wsspcomn.tranid);
        wsaaTotVal.set(sv.totalAmount);
        sv.dataArea.set(SPACES);
        sv.subfileArea.set(SPACES);
        wsaaMandatorys.set(SPACES);
        ix = 0;
        iy = 0;
        iz = 0;
        wsaaCount = 0;
        wsaaMultBnfy.set(ZERO);
        ib = 0;
        /* Get today's date.*/
        datcon1rec.function.set(varcom.tday);
        Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
        if (isNE(datcon1rec.statuz, varcom.oK)) {
            syserrrec.params.set(datcon1rec.datcon1Rec);
            syserrrec.statuz.set(datcon1rec.statuz);
            fatalError600();
        }
        /* Initialise screen fields.*/
        /* MOVE SPACES                 TO Sd5jl-BNYRLN                  */
        sv.cltreln.set(SPACES);
        sv.bnycd.set(SPACES);
        sv.bnysel.set(SPACES);
        sv.relto.set(SPACES);
        sv.revcflg.set(SPACES);
        sv.clntsname.set(SPACES);
        sv.bnypc.set(ZERO);
        sv.amount.set(ZERO);
        sv.sequence.set(SPACES);
        scrnparams.function.set(varcom.sclr);
        processScreen("SD5JL", sv);
        if (isNE(scrnparams.statuz, varcom.oK)) {
            syserrrec.statuz.set(scrnparams.statuz);
            fatalError600();
        }
        scrnparams.subfileRrn.set(1);

        initOptswch1200();

        if (isEQ(wsaaBatckey.batcBatctrcde, t669)) {
            sv.disabledFlag.set("Y");
        }
    }

    protected void initOptswch1200() {
        start1210();
    }

    protected void start1210() {
        optswchrec.optsFunction.set(varcom.init);
        optswchrec.optsLanguage.set(wsspcomn.language);
        optswchrec.optsCompany.set(wsspcomn.company);
        varcom.vrcmTranid.set(wsspcomn.tranid);
        optswchrec.optsUser.set(varcom.vrcmUser);
        optswchrec.optsCallingProg.set(wsaaProg);
        optswchrec.optsDteeff.set(datcon1rec.intDate);
        callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
        if (isNE(optswchrec.optsStatuz, varcom.oK)) {
            syserrrec.params.set(optswchrec.rec);
            syserrrec.statuz.set(optswchrec.optsStatuz);
            fatalError600();
        }
    }

    protected void retrieveHeader1030() {
        chdrpf = chdrpfDAO.getCacheObject(chdrpf);
        if(null==chdrpf) {
//            chdrclmIO.setFunction(varcom.retrv);
//            chdrclmIO.setFormat(formatsInner.chdrclmrec);
//            SmartFileCode.execute(appVars, chdrclmIO);
//            if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
//                syserrrec.params.set(chdrclmIO.getParams());
//                fatalError600();
//            }
//            else {
                chdrpf = chdrpfDAO.getChdrpf(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum()); /* IJTI-1479 */
                if(null==chdrpf) {
                    fatalError600();
                }else {
                    chdrpfDAO.setCacheObject(chdrpf);
                }
 //           }
        }
    }

    protected void readContractLongdesc1040() {
        /* Get the Contract Type description.*/
        descpf = descDAO.getdescData("IT", "T5688", chdrpf.getCnttype(), chdrpf.getChdrcoy().toString(), wsspcomn.language.toString());
        sv.chdrnum.set(chdrpf.getChdrnum());
        sv.cnttype.set(chdrpf.getCnttype());
        if (descpf == null) {
            sv.ctypedes.set(SPACES);
        } else {
            sv.ctypedes.set(descpf.getLongdesc());
        }
        /*  Read CLTS to get contract owner long name                      */
        clntpf = new Clntpf();
        clntpf.setClntpfx(fsupfxcpy.clnt.toString());
        clntpf.setClntcoy(wsspcomn.fsuco.toString());
        clntpf.setClntnum(chdrpf.getCownnum());
        sv.cownnum.set(chdrpf.getCownnum());
        clntpf = clntpfDAO.selectClient(clntpf);
        plainname();
        sv.ownername.set(wsspcomn.longconfname);
        /*  Read LIFE to get life insured                                  */
        lifepf = lifepfDAO.getLifeEnqRecord(wsspcomn.company.toString(), chdrpf.getChdrnum(), "01", "00");
        if (lifepf == null) {
            syserrrec.params.set(wsspcomn.company.toString().concat(chdrpf.getChdrnum()).concat("0100"));
            syserrrec.statuz.set("MRNF");
            fatalError600();
        }
        /*  Read CLTS to get life insured long name                        */
        clntpf = new Clntpf();
        clntpf.setClntpfx(fsupfxcpy.clnt.toString());
        clntpf.setClntcoy(wsspcomn.fsuco.toString());
        clntpf.setClntnum(lifepf.getLifcnum());
        sv.lifcnum.set(lifepf.getLifcnum());
        clntpf = clntpfDAO.selectClient(clntpf);
        plainname();
        sv.linsname.set(wsspcomn.longconfname);
        /*  Read Contract Beneficiary Mandatory Table - TR52Z.             */
        itempf = new Itempf();
        itempf.setItempfx(smtpfxcpy.item.toString());
        itempf.setItemcoy(wsspcomn.company.toString());
        itempf.setItemtabl(tr52z);
        itempf.setItemitem(chdrpf.getCnttype());
        itempf = itemDAO.getItempfRecord(itempf);
        if (itempf == null) {
            itempf = new Itempf();
            itempf.setItempfx(smtpfxcpy.item.toString());
            itempf.setItemcoy(wsspcomn.company.toString());
            itempf.setItemtabl(tr52z);
            itempf.setItemitem("***");

        }
        if (itempf == null) {
            tr52zrec.tr52zRec.set(SPACES);
        } else {
            tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
        }
        sv.chdrnum.set(chdrpf.getChdrnum());
        sv.cnttype.set(chdrpf.getCnttype());
        /* Load any beneficiary records into the working storage array*/
        /* prior to loading them into the subfile.*/
        loadStack1100();
    }

    protected void loadSubfileFields1060() {
        moveStackToScreen1400();
        /*EXIT*/
    }

    protected void loadStack1100() {
        wsbbStackArrayInner.wsbbStackArray.set(SPACES);
        for (wsaaIndex.set(1); !(isGT(wsaaIndex, wsaaSubfileSize)); wsaaIndex.add(1)) {
            initialZero1300();
        }
        /**
         * at first time,we need to check in table CPBNFYPF If the CHDRNUL is exist,If it is exis,means we have added row from table CPBNFYPF,else not.
         */
        cpbnfypfList = new ArrayList<Cpbnfypf>();
        int resultchdr = cpbnfypfDAO.getByChdrnum(chdrpf.getChdrnum());
        if (resultchdr <= 0) {
            addDefaultBeneficiaries1200();
        } else {
            cpbnfypfList = cpbnfypfDAO.getCpbnfyByCoyAndNumAndSeq(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), sequence01);
        }

        if (cpbnfypfList != null && cpbnfypfList.size() > 0) {
            for (wsaaNofRead = 1; wsaaNofRead <= cpbnfypfList.size(); wsaaNofRead++) {
                cpbnfypf = cpbnfypfList.get(wsaaNofRead - 1);
                loadBeneficiaries1200();
            }
        }
    }

    protected void addDefaultBeneficiaries1200() {
        bnfypfList = bnfypfDAO.getBnfymnaByCoyAndNumAndBtype(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), btype);
        // add default to the new
        for (Bnfypf bnfypf : bnfypfList) {
            Cpbnfypf cpbnfypf = new Cpbnfypf();
            cpbnfypf.setBankacckey(bnfypf.getBankacckey());
            cpbnfypf.setBankkey(bnfypf.getBankkey());
            cpbnfypf.setBnycd(bnfypf.getBnycd());
            cpbnfypf.setBnyclt(bnfypf.getBnyclt());
            cpbnfypf.setBnypc(bnfypf.getBnypc());
            cpbnfypf.setBnyrln(bnfypf.getBnyrln());
            cpbnfypf.setBnytype(bnfypf.getBnytype());
            cpbnfypf.setChdrcoy(bnfypf.getChdrcoy());
            cpbnfypf.setChdrnum(bnfypf.getChdrnum());
            cpbnfypf.setCltreln(bnfypf.getCltreln());
            cpbnfypf.setCurrfr(bnfypf.getCurrfr());
            cpbnfypf.setCurrto(bnfypf.getCurrto());
            cpbnfypf.setDatime(bnfypf.getDatime());
            cpbnfypf.setEffdate(bnfypf.getEffdate());
            cpbnfypf.setEnddate(bnfypf.getEnddate());
            cpbnfypf.setJobnm(bnfypf.getJobnm());
            cpbnfypf.setPaymmeth(bnfypf.getPaymmeth());
            cpbnfypf.setRelto(bnfypf.getRelto());
            cpbnfypf.setRevcflg(bnfypf.getRevcflg());
            cpbnfypf.setSelfind(bnfypf.getSelfind());
            cpbnfypf.setSequence(bnfypf.getSequence());
            cpbnfypf.setTermid(bnfypf.getTermid());
            cpbnfypf.setTranno(bnfypf.getTranno());
            cpbnfypf.setTrdt(bnfypf.getTrdt());
            cpbnfypf.setTrtm(bnfypf.getTrtm());
            cpbnfypf.setUniqueNumber(bnfypf.getUniqueNumber());
            cpbnfypf.setUserT(bnfypf.getUserT());
            cpbnfypf.setUsrprf(bnfypf.getUsrprf());
            cpbnfypf.setValidflag(bnfypf.getValidflag());
            cpbnfypf.setAmount(mult(wsaaTotVal, cpbnfypf.getBnypc().divide(new BigDecimal(100))).getbigdata());
            cpbnfypfList.add(cpbnfypf);
            cpbnfypfDAO.insertCpbnfypf(cpbnfypf);
        }

    }

    protected void loadBeneficiaries1200() {
        clntpf = new Clntpf();
        clntpf.setClntpfx("CN");
        clntpf.setClntcoy(wsspcomn.fsuco.toString());
        clntpf.setClntnum(cpbnfypf.getBnyclt()); /* IJTI-1479 */
        clntpf = clntpfDAO.selectClient(clntpf);
        plainname();
        wsbbStackArrayInner.wsbbBnysel[wsaaNofRead].set(cpbnfypf.getBnyclt());
        wsbbStackArrayInner.wsbbClntnm[wsaaNofRead].set(wsspcomn.longconfname);
        /* MOVE BNFYMNA-BNYRLN         TO WSBB-BNYRLN(WSAA-NOF-READ).   */
        wsbbStackArrayInner.wsbbBnyrln[wsaaNofRead].set(cpbnfypf.getCltreln());
        wsbbStackArrayInner.wsbbBnytype[wsaaNofRead].set(cpbnfypf.getBnytype());
        wsbbStackArrayInner.wsbbBnycd[wsaaNofRead].set(cpbnfypf.getBnycd());
        wsbbStackArrayInner.wsbbBnypc[wsaaNofRead].set(cpbnfypf.getBnypc());
        if (cpbnfypf.getAmount() == null) {
            wsbbStackArrayInner.wsbbAmount[wsaaNofRead].set(mult(wsaaTotVal, cpbnfypf.getBnypc().divide(new BigDecimal(100))));
        } else {
            wsbbStackArrayInner.wsbbAmount[wsaaNofRead].set(cpbnfypf.getAmount());
        }
        wsbbStackArrayInner.wsbbEffdate[wsaaNofRead].set(cpbnfypf.getEffdate());
        wsbbStackArrayInner.wsbbEnddate[wsaaNofRead].set(cpbnfypf.getEnddate());
        wsbbStackArrayInner.wsbbRelto[wsaaNofRead].set(cpbnfypf.getRelto());
        wsbbStackArrayInner.wsbbRevcflg[wsaaNofRead].set(cpbnfypf.getRevcflg());
        wsbbStackArrayInner.wsbbSelfind[wsaaNofRead].set(cpbnfypf.getSelfind());
        wsbbStackArrayInner.wsbbpaymthbf[wsaaNofRead].set(cpbnfypf.getPaymmeth());
        wsbbStackArrayInner.wsbbSequence[wsaaNofRead].set(cpbnfypf.getSequence());//ICIL-372
        wsbbStackArrayInner.wsbbBankkey[wsaaNofRead].set(cpbnfypf.getBankkey());
        wsbbStackArrayInner.wsbbBankacckey[wsaaNofRead].set(cpbnfypf.getBankacckey());
        if (cpbnfypf.getBankkey() != null && !cpbnfypf.getBankkey().trim().equals("")) {
            babrpf = babrpfDAO.searchBankkey(cpbnfypf.getBankkey());
            wsbbStackArrayInner.wsbbBankdesc[wsaaNofRead].set(babrpf.getBankdesc());
        }
    }

    protected void initialZero1300() {
        wsbbStackArrayInner.wsbbBnypc[wsaaIndex.toInt()].set(ZERO);
        wsbbStackArrayInner.wsbbAmount[wsaaIndex.toInt()].set(ZERO);
        wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbRelto[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbRevcflg[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbBnyrln[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbBnycd[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbClntnm[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbEffdate[wsaaIndex.toInt()].set(varcom.vrcmMaxDate);
        wsbbStackArrayInner.wsbbEnddate[wsaaIndex.toInt()].set(varcom.vrcmMaxDate);
        wsbbStackArrayInner.wsbbSelfind[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbSequence[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbpaymthbf[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbBankkey[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbBankacckey[wsaaIndex.toInt()].set(SPACES);
        wsbbStackArrayInner.wsbbBankdesc[wsaaIndex.toInt()].set(SPACES);
    }

    protected void moveStackToScreen1400() {
        /*PARA*/
        scrnparams.function.set(varcom.sclr);
        processScreen("SD5JL", sv);
        if (isNE(scrnparams.statuz, varcom.oK)) {
            syserrrec.statuz.set(scrnparams.statuz);
            fatalError600();
        }
        for (wsaaIndex.set(1); !(isGT(wsaaIndex, wsaaSubfileSize)); wsaaIndex.add(1)) {
            moveToScreen1500();
        }
        /*EXIT*/
    }

    protected void moveToScreen1500() {
        sv.bnyselOut[varcom.pr.toInt()].set(SPACES);
        sv.bnytypeOut[varcom.pr.toInt()].set(SPACES);
        sv.cltrelnOut[varcom.pr.toInt()].set(SPACES);
        sv.bnycdOut[varcom.pr.toInt()].set(SPACES);
        sv.bnypcOut[varcom.pr.toInt()].set(SPACES);
        sv.amountOut[varcom.pr.toInt()].set(SPACES);
        sv.effdateOut[varcom.pr.toInt()].set(SPACES);
        sv.enddateOut[varcom.pr.toInt()].set(SPACES);
        sv.sequenceOut[varcom.pr.toInt()].set(SPACES);
        sv.bankacckeyOut[varcom.pr.toInt()].set(SPACES);
        sv.bankkeyOut[varcom.pr.toInt()].set(SPACES);
        /* MOVE WSBB-BNYRLN(WSAA-INDEX) TO Sd5jl-BNYRLN.                */
        sv.cltreln.set(wsbbStackArrayInner.wsbbBnyrln[wsaaIndex.toInt()]);
        sv.bnycd.set(wsbbStackArrayInner.wsbbBnycd[wsaaIndex.toInt()]);
        sv.bnypc.set(wsbbStackArrayInner.wsbbBnypc[wsaaIndex.toInt()]);
        sv.amount.set(wsbbStackArrayInner.wsbbAmount[wsaaIndex.toInt()]);
        sv.bnytype.set(wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()]);
        sv.relto.set(wsbbStackArrayInner.wsbbRelto[wsaaIndex.toInt()]);
        sv.revcflg.set(wsbbStackArrayInner.wsbbRevcflg[wsaaIndex.toInt()]);
        sv.bnysel.set(wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()]);
        /* MOVE WSBB-CLNTNM(WSAA-INDEX) TO Sd5jl-CLNTNM.                */
        sv.clntsname.set(wsbbStackArrayInner.wsbbClntnm[wsaaIndex.toInt()]);
        sv.effdate.set(wsbbStackArrayInner.wsbbEffdate[wsaaIndex.toInt()]);
        sv.enddate.set(wsbbStackArrayInner.wsbbEnddate[wsaaIndex.toInt()]);
        sv.paymthbf.set(wsbbStackArrayInner.wsbbpaymthbf[wsaaIndex.toInt()]);
        sv.sequence.set(wsbbStackArrayInner.wsbbSequence[wsaaIndex.toInt()]);
        sv.bankkey.set(wsbbStackArrayInner.wsbbBankkey[wsaaIndex.toInt()]);
        sv.bkrelackey.set(wsbbStackArrayInner.wsbbBankacckey[wsaaIndex.toInt()]);
        sv.bnkcdedsc.set(wsbbStackArrayInner.wsbbBankdesc[wsaaIndex.toInt()]);
        if (sv.bkrelackey != null && !sv.bkrelackey.toString().trim().equals("")) {
            len = sv.bkrelackey.toString().trim().length();
            if (len > 4) {
                bankAccKeybuffer = new StringBuffer("");
                for (int i = 0; i < len - 4; i++) {
                    bankAccKeybuffer.append("*");
                }
                bankAccKeybuffer.append(sv.bkrelackey.toString().trim().substring(len - 4, len));
                sv.bankacckey.set(bankAccKeybuffer.toString());
            } else {
                sv.bankacckey.set(sv.bkrelackey);
            }

        } else {
            sv.bankacckey.set(SPACES);
        }
        scrnparams.subfileRrn.set(wsaaIndex);
        if (isEQ(wsbbStackArrayInner.wsbbRevcflg[wsaaIndex.toInt()], "N")) {
            sv.bnyselOut[varcom.pr.toInt()].set("Y");
            sv.bnytypeOut[varcom.pr.toInt()].set("Y");
            sv.cltrelnOut[varcom.pr.toInt()].set("Y");
            sv.bnycdOut[varcom.pr.toInt()].set("Y");
            sv.bnypcOut[varcom.pr.toInt()].set("Y");
            sv.amountOut[varcom.pr.toInt()].set("Y");
            sv.effdateOut[varcom.pr.toInt()].set("Y");
            sv.enddateOut[varcom.pr.toInt()].set("Y");
            sv.sequenceOut[varcom.pr.toInt()].set("Y");
        }
        scrnparams.function.set(varcom.sadd);
        processScreen("SD5JL", sv);
        if (isNE(scrnparams.statuz, varcom.oK)) {
            syserrrec.statuz.set(scrnparams.statuz);
            fatalError600();
        }
    }

    /**
     * <pre>
     *     RETRIEVE SCREEN FIELDS AND EDIT
     * </pre>
     */
    protected void preScreenEdit() {
        /*PRE-START*/
        if (isEQ(wsspcomn.flag, "I")) {
            scrnparams.function.set(varcom.prot);
        }
        scrnparams.subfileRrn.set(1);
        wsspcomn.edterror.set(varcom.oK);
        return;
        /*PRE-EXIT*/
    }

    protected void screenEdit2000() {
        screenIo2010();
        validateScreen2010();
    }

    protected void screenIo2010() {
        /*    CALL 'Sd5jlIO'              USING SCRN-SCREEN-PARAMS         */
        /*                                      Sd5jl-DATA-AREA            */
        /*                                      Sd5jl-SUBFILE-AREA.        */
        /* Screen errors are now handled in the calling program.           */
        /*    PERFORM 200-SCREEN-ERRORS.                                   */
        wsspcomn.edterror.set(varcom.oK);
    }

    protected void validateScreen2010() {
        if (isEQ(scrnparams.statuz, varcom.kill)) {
            return;
        }
        if (isEQ(wsspcomn.flag, "I")) {
            return;
        }
        if (isEQ(scrnparams.statuz, varcom.calc)) {
            wsspcomn.edterror.set("Y");
        }
        /* Validate subfile fields.*/
        wsaaClntArray.set(SPACES);
        wsaaMultBnfy.set(ZERO);
        wsaaActualTotVal.set(ZERO);
        initialize(wsaaPctTypes);
        wsaaPctSub = 1;
        scrnparams.function.set(varcom.sstrt);
        scrnparams.statuz.set(varcom.oK);
        while (!(isEQ(scrnparams.statuz, varcom.endp))) {
            validateSubfile2100();
        }
        if (isNE(wsaaActualTotVal, wsaaTotVal)) {
            scrnparams.errorCode.set(errorsInner.rrm1);
            wsspcomn.edterror.set("Y");
        }
        /* Calculate default share percentages if none are entered.        */
        /* IF WSAA-BNY-CNT          NOT = ZERO    AND           <V76L01>*/
        /*    WSAA-BNYPC-TOTAL          = ZERO    AND           <V76L01>*/
        /*    Sd5jl-ERROR-SUBFILE       = SPACE                 <V76L01>*/
        /*    COMPUTE WSAA-QUOTA        = 100 / WSAA-BNY-CNT    <V76L01>*/
        /*    MOVE O-K                 TO SCRN-STATUZ           <V76L01>*/
        /*    MOVE SSTRT               TO SCRN-FUNCTION         <V76L01>*/
        /*    PERFORM 2400-DEFAULT-SHARE                        <V76L01>*/
        /*       UNTIL SCRN-STATUZ      = ENDP                  <V76L01>*/
        /* END-IF.                                                      */
        /* If entered, share percentages must be entered against           */
        /* all beneficiaries.                                              */
        /*  IF WSAA-NO-DETS         NOT = SPACE  AND            <V76L01>*/
        /*     WSAA-BNYPC-TOTAL         > ZERO                  <V76L01>*/
        /*     MOVE E518               TO SCRN-ERROR-CODE       <V76L01>*/
        /*     MOVE 'Y'                TO WSSP-EDTERROR         <V76L01>*/
        /* END-IF.                                              <V76L01>*/
        /* If the percentage share does not total 100%, display an*/
        /* error.*/
        /*  IF WSAA-BNYPC-TOTAL     NOT = 100                           */
        /*     AND BENEFICIARY-EXISTS                                   */
        /*      MOVE E631              TO SCRN-ERROR-CODE               */
        /*      MOVE 'Y'               TO WSSP-EDTERROR                 */
        /* END-IF.                                                      */
        /*   Check for mandatory beneficiary type in TR52Z.                */
        a100MandatoryBnfy();
        /*   Check for percentage under each beneficiary type.             */
        for (ix = 1; !(isGT(ix, 10)); ix++) {
            if (isNE(wsaaPctTot[ix], 100)
                    && isNE(wsaaPctTot[ix], ZERO)) {
                scrnparams.errorCode.set(errorsInner.e631);
                wsspcomn.edterror.set("Y");
            }
        }
    }

    protected void validateSubfile2100() {
        try {
            validateSubfile2120();
            updateErrorIndicators2170();
        } catch (GOTOException e) {
            /* Expected exception for control flow purposes. */
        }
    }

    protected void callDatcon21600() {
        /*PARA*/
        datcon2rec.frequency.set("01");
        callProgram(Datcon2.class, datcon2rec.datcon2Rec);
        if (isEQ(datcon2rec.statuz, varcom.bomb)) {
            syserrrec.statuz.set(datcon2rec.statuz);
            fatalError600();
        }
        /*EXIT*/
    }

    protected void validateSubfile2120() {
        processScreen("SD5JL", sv);

        if (isNE(scrnparams.statuz, varcom.oK)
                && isNE(scrnparams.statuz, varcom.endp)) {
            syserrrec.statuz.set(scrnparams.statuz);
            fatalError600();
        }
        if (isEQ(scrnparams.statuz, varcom.endp)) {
            goTo(GotoLabel.exit2190);
        }

        if (isEQ(sv.bnysel, SPACES)) {
            sv.clntsname.set(SPACES);
        }

        /* If details are blanked out, go on to the next record.*/
        /* IF Sd5jl-BNYRLN              = SPACES                        */
        if (isEQ(sv.cltreln, SPACES)
                && isEQ(sv.bnycd, SPACES)
                && isEQ(sv.bnypc, ZERO)
                && isEQ(sv.amount, ZERO)
                && isEQ(sv.bnysel, SPACES)
                && isEQ(sv.effdate, varcom.vrcmMaxDate)
                && isEQ(sv.bnytype, SPACES)
                && isEQ(sv.sequence, SPACES)
                && isEQ(sv.paymthbf, SPACES)
                && isEQ(sv.bankacckey, SPACES)) {
            sv.bankkey.set(SPACES);
            sv.bkrelackey.set(SPACES);
            sv.bnkcdedsc.set(SPACES);
            scrnparams.function.set(varcom.srdn);
            goTo(GotoLabel.exit2190);
        }
        /* Check relationship. Validation check is replaced by T3584    */
        /* MOVE SPACES                 TO DESC-DATA-KEY.                */
        /* MOVE 'IT'                   TO DESC-DESCPFX.                 */
        /* MOVE WSSP-COMPANY           TO DESC-DESCCOY.                 */
        /* MOVE T5663                  TO DESC-DESCTABL.                */
        /* MOVE Sd5jl-BNYRLN           TO DESC-DESCITEM.                */
        /* MOVE WSSP-LANGUAGE          TO DESC-LANGUAGE.                */
        /* MOVE READR                  TO DESC-FUNCTION.                */
        /* CALL 'DESCIO'               USING DESC-PARAMS.               */
        /* IF DESC-STATUZ           NOT = O-K                           */
        /*    AND                   NOT = MRNF                          */
        /*     MOVE DESC-STATUZ        TO SYSR-STATUZ                   */
        /*     PERFORM 600-FATAL-ERROR                                  */
        /* END-IF.                                                      */
        /* IF DESC-STATUZ               = MRNF                          */
        /*     MOVE E468               TO Sd5jl-BNYRLN-ERR              */
        /* END-IF.                                                      */
        /* Check beneficiary.*/
        checkBeneficiary2200();
        /* Check the percentage/code fields.*/
        checkShare2300();
        /* IF Sd5jl-BNYRLN         NOT =  SPACES AND            <V76L01>*/
        setEffectiveDate();
        /* valid claim payees amount*/
        validClaimPayeesAmount();

        if (isEQ(sv.bnytype, SPACES)) {
            sv.bnytypeErr.set(errorsInner.e186);
        }
        if (isNE(sv.bnytype, btype)) {
            sv.bnytypeErr.set(errorsInner.rrm9);
        }
        if (isEQ(sv.cltreln, SPACES)) {
            sv.cltrelnErr.set(errorsInner.e186);
        }

//		if (isEQ(sv.bnypc, ZERO)) {
//			sv.bnypcErr.set(errorsInner.e186);
//		}

        if (isEQ(sv.sequence, SPACES) && (isNE(sv.bnypc,ZERO) || isNE(sv.amount,ZERO))) {
            sv.sequenceErr.set(errorsInner.rrm0);
        }
        if (isEQ(sv.effdate, varcom.vrcmMaxDate)
                || isEQ(sv.effdate, ZERO)) {
            sv.effdateErr.set(errorsInner.h046);
        }
        if (isEQ(sv.bankacckey, "EROR")) {//ICIL-384
            sv.paymthbfErr.set(errorsInner.rrbs);
            sv.bankacckey.set(SPACES);
        }
        if (isEQ(sv.paymthbf, "C") || isEQ(sv.paymthbf, "4")) {
            if (isEQ(sv.bankacckey, SPACES)) {
                sv.bankacckeyErr.set(errorsInner.rrbr);
            }
        }
        if (isNE(sv.paymthbf, SPACES) && isNE(sv.paymthbf, "C") && isNE(sv.paymthbf, "4")) {
            if (isNE(sv.bankacckey, SPACES) && isNE(sv.bankacckey, "EROR")) {
                sv.bankacckeyErr.set(errorsInner.e570);
            }
        }
        if (isNE(sv.bankacckey, SPACES) && isNE(sv.bankacckey, "EROR")) {
            if (isEQ(sv.paymthbf, SPACES)) {
                sv.paymthbfErr.set(errorsInner.rrbs);
            }
        }

        /*ICIL-11  end*/
        /* The Beneficiary effective date cannot be less than the RCD      */
        if (isLT(sv.effdate, chdrpf.getOccdate())) {
            sv.effdateErr.set(errorsInner.h359);
        }
        if (isNE(sv.enddate, SPACES) && isNE(sv.enddate, varcom.vrcmMaxDate) && isLT(sv.enddate, sv.effdate)) {
            sv.enddateErr.set(errorsInner.rfwr);
        } else {
            if (isEQ(sv.bnytype, "BD")) {
                datcon2rec.intDate1.set(sv.effdate);
                datcon2rec.freqFactor.set(3);
                callDatcon21600();
                if (isEQ(sv.enddate, SPACES) || isEQ(sv.enddate, varcom.vrcmMaxDate)) {
                    sv.enddate.set(datcon2rec.intDate2);
                } else if (isNE(sv.enddate, SPACES) && isNE(sv.enddate, varcom.vrcmMaxDate) && isLT(sv.enddate, datcon2rec.intDate2)) {
                    sv.enddateErr.set(errorsInner.rfwq);
                }
            }
        }
        validBankExist();

        if (isNE(sv.errorSubfile, SPACES)) {
            wsspcomn.edterror.set("Y");
        }
    }

    protected void validClaimPayeesAmount() {
        if (isEQ(sv.sequence, sequence01)) {
            //Claim Payees Registration
            if (isEQ(sv.bnypc, ZERO)) {
                if (isEQ(sv.amount, ZERO)) {
                    sv.amountErr.set(errorsInner.rrlz);
                } else {
                    wsaaActualTotVal.add(sv.amount);
                }
            } else {
                wsaaBnypc.add(BigDecimal.valueOf(sv.bnypc.toDouble() / 100));
                sv.amount.set(mult(wsaaTotVal, BigDecimal.valueOf(sv.bnypc.toDouble() / 100)));
                wsaaActualTotVal.add(sv.amount);
            }
        } else {
            if (isNE(sv.sequence, SPACES) && (isNE(sv.amount, ZERO) || (isNE(sv.bnypc, SPACES) && isNE(sv.bnypc, ZERO)))) {
                sv.amountErr.set(errorsInner.rrm0);
            }
        }
    }

    protected void validBankExist() {
        if (isNE(sv.paymthbf, "C") && isNE(sv.paymthbf, "4")) {
            return;
        }

        if (sv.bankacckey == null || isEQ(sv.bankacckey, SPACES)) {
            return;
        }
        len = sv.bankacckey.toString().trim().length();
        lenRel = sv.bkrelackey.toString().trim().length();
        if (sv.bankacckey.toString().indexOf("*") == -1 && isNE(sv.bankacckey, sv.bkrelackey)) {
            sv.bkrelackey.set(sv.bankacckey);
        } else if (sv.bankacckey.toString().indexOf("*") != -1
                && isNE(sv.bankacckey.toString().trim().substring(len - 4, len), sv.bkrelackey.toString().trim().substring(lenRel - 4, lenRel))) {
            if (isEQ(sv.paymthbf, "C")) {
                sv.bankacckeyErr.set(errorsInner.rfh7);
            } else if (isEQ(sv.paymthbf, "4")) {
                sv.bankacckeyErr.set(errorsInner.f826);
            }
            return;
        } else if (sv.bankacckey.toString().indexOf("*") != -1 && len != lenRel) {
            if (isEQ(sv.paymthbf, "C")) {
                sv.bankacckeyErr.set(errorsInner.rfh7);
            } else if (isEQ(sv.paymthbf, "4")) {
                sv.bankacckeyErr.set(errorsInner.f826);
            }
            return;
        }

        clbapf = new Clbapf();
        clbapf.setClntcoy(wsspcomn.fsuco.toString());
        clbapf.setClntpfx("CN");
        clbapf.setClntnum(sv.bnysel.toString().trim());

        if (isEQ(sv.paymthbf, "C")) {
            clbapf.setClntrel("CC");
            clbapf.setBsortcde(sv.bankkey.toString());
            clbapf.setBankacckey(ccfkpfDAO.retreiveReferenceNumber(sv.bkrelackey.toString().trim()));
            if (clbapf.getBankacckey().equals("")) {
                sv.bankacckeyErr.set(errorsInner.rfh7);
                return;
            }
        } else {
            clbapf.setBankkey(sv.bankkey.toString());
            clbapf.setClntrel("CB");
            clbapf.setBankacckey(sv.bkrelackey.toString());
        }

        if (!clbapfDAO.isExistClbapf(clbapf)) {
            if (isEQ(sv.paymthbf, "C")) {
                sv.bankacckeyErr.set(errorsInner.rfh7);
            } else if (isEQ(sv.paymthbf, "4")) {
                sv.bankacckeyErr.set(errorsInner.f826);
            }
        }


    }

    protected void updateErrorIndicators2170() {
        scrnparams.function.set(varcom.supd);
        processScreen("SD5JL", sv);
        if (isNE(scrnparams.statuz, varcom.oK)) {
            syserrrec.statuz.set(scrnparams.statuz);
            fatalError600();
        }
        scrnparams.function.set(varcom.srdn);
    }

    protected void checkBeneficiary2200() {

        if (isEQ(sv.bnysel, SPACES)) {
            sv.clntsname.set(SPACES);
            sv.bnyselErr.set(errorsInner.f691);
            return;
        }
        if (isEQ(sv.bnytype, SPACES)) {
            sv.bnytypeErr.set(errorsInner.e186);
            return;
        }
//        if (isEQ(sv.sequence, SPACES)) {
//            sv.sequenceErr.set(errorsInner.e186);
//            sv.sequenceOut[Varcom.pr.toInt()].set("Y");
//            return;
//        }

        clntpf = new Clntpf();
        clntpf.setClntpfx("CN");
        clntpf.setClntcoy(wsspcomn.fsuco.toString());
        clntpf.setClntnum(sv.bnysel.toString().trim());  //Code change for ILIFE-6180 deployed with ILIFE-6304
        clntpf = clntpfDAO.selectClient(clntpf);
        if (clntpf == null) {
            /*     MOVE E339               TO Sd5jl-BNYSEL-ERR              */
            sv.bnyselErr.set(errorsInner.e058);
            return;
        } else {
            plainname();
        }
        /* Do not allow selection of a dead client                         */
        if (isNE(clntpf.getCltdod(), varcom.vrcmMaxDate)) {
            sv.bnyselErr.set(errorsInner.f782);
            return;
        }
        /* MOVE WSSP-LONGCONFNAME      TO Sd5jl-CLNTNM.                 */
        sv.clntsname.set(wsspcomn.longconfname);

        if (isNE(sv.cltreln, SPACES)) {
            a200ReadT3584();
        }
        b100CallTr52y();
        sv.relto.set(tr52yrec.relto);
        getOwnerNumber();

        if (isEQ(sv.relto, "L")) {
            wsaaClntnum.set(lifepf.getLifcnum());
        }
        chkAlrdyRlnshp2600();
        /* Check for a duplicate client number.*/
        /* PERFORM VARYING WSAA-DUPR-SUB FROM 1 BY 1                    */
        /*    UNTIL WSAA-DUPR-SUB      > SCRN-SUBFILE-RRN               */
        /*     IF Sd5jl-BNYSEL         = WSAA-CLIENT(WSAA-DUPR-SUB)     */
        /*         MOVE E048          TO Sd5jl-BNYSEL-ERR               */
        /*     END-IF                                                   */
        /* END-PERFORM.                                                 */
        for (wsaaDuprSub = 1; !(isGT(wsaaDuprSub, scrnparams.subfileRrn)); wsaaDuprSub++) {
            if (isEQ(sv.bnysel, wsaaClient[wsaaDuprSub])
                    && isEQ(sv.bnytype, wsaaType[wsaaDuprSub])
                    && isNE(sv.bnysel, SPACES)) {
                sv.bnyselErr.set(errorsInner.e048);
                wsspcomn.edterror.set("Y");
                return;
            }
        }
        /* Check the relationship against blank and allow self flag        */
        /* in TR52Z.                                                       */
        if (isEQ(sv.cltreln, SPACES)) {
            b300ReadTr52z();
            if (found.isTrue()
                    && isNE(tr52zrec.selfind[wsbbTableSub.toInt()], "Y")) {
                sv.cltrelnErr.set(errorsInner.e186);
            }
        }
        wsaaClient[scrnparams.subfileRrn.toInt()].set(sv.bnysel);
        if (isNE(sv.bnytype, SPACES)
                && isNE(sv.bnysel, SPACES)) {
            wsaaType[scrnparams.subfileRrn.toInt()].set(sv.bnytype);
            wsaaSequence[scrnparams.subfileRrn.toInt()].set(sv.sequence);
        }
        ib = 0;
        for (wsaaPctSub = 1; !(isGT(wsaaPctSub, 10)); wsaaPctSub++) {
            if (isEQ(sv.bnytype, wsaaPctType[wsaaPctSub]) && isEQ(sv.sequence, this.wsaaPctSeq[wsaaPctSub])) {
                wsaaPctTot[wsaaPctSub].add(sv.bnypc);
                wsaaBnfyCount[wsaaPctSub].add(1);
                ib = wsaaPctSub;
                wsaaPctSub = 11;
            } else {
                if (isEQ(wsaaPctType[wsaaPctSub], SPACES)) {
                    wsaaPctType[wsaaPctSub].set(sv.bnytype);
                    wsaaPctSeq[wsaaPctSub].set(sv.sequence);
                    wsaaPctTot[wsaaPctSub].set(sv.bnypc);
                    wsaaBnfyCount[wsaaPctSub].set(1);
                    ib = wsaaPctSub;
                    wsaaPctSub = 11;
                }
            }
        }
        if (isGT(wsaaBnfyCount[ib], 1)
                && isEQ(tr52yrec.multind, "N")) {
            sv.bnyselErr.set(errorsInner.rfk6);
            wsspcomn.edterror.set("Y");
        }
        if (isNE(sv.relto, "N")) {
            b200MandRlsp();
        }
    }

    protected void checkShare2300() {
        /*READ-T5662*/
        /* IF Sd5jl-BNYCD               = SPACES                        */
        /*    AND Sd5jl-BNYPC           = 0                             */
        /*     MOVE E518              TO Sd5jl-BNYCD-ERR                */
        /*                               Sd5jl-BNYPC-ERR                */
        /*     GO TO 2349-EXIT                                          */
        /* END-IF.                                                      */
        /* Read Beneficiary Codes table.*/
        if (isNE(sv.bnycd, SPACES)) {
            readT56622350();
        }
        if (isGT(sv.bnypc, 100)) {
            sv.bnypcErr.set(errorsInner.f348);
        }
        /*EXIT*/
    }

    protected void readT56622350() {
        itdmpfList = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), t5662, sv.bnycd.toString(), chdrpf.getOccdate());

        if (itdmpfList == null || itdmpfList.size() == 0) {
            sv.bnycdErr.set(errorsInner.e470);
            return;
        }
        t5662rec.t5662Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
        sv.bnypc.set(t5662rec.bnypc);
    }

    /**
     * <pre>
     * 2400-DEFAULT-SHARE SECTION.                              <V76L01>
     * 2400-START.                                              <V76L01>
     *                                                         <V76L01>
     *    CALL 'Sd5jlIO'           USING SCRN-SCREEN-PARAMS    <V76L01>
     *                                   Sd5jl-DATA-AREA       <V76L01>
     *                                   Sd5jl-SUBFILE-AREA.   <V76L01>
     *                                                         <V76L01>
     *    IF SCRN-STATUZ           NOT = O-K   AND             <V76L01>
     *       SCRN-STATUZ           NOT = ENDP                  <V76L01>
     *       MOVE SCRN-STATUZ         TO SYSR-STATUZ           <V76L01>
     *       PERFORM 600-FATAL-ERROR                           <V76L01>
     *    END-IF.                                              <V76L01>
     *                                                         <V76L01>
     *    IF SCRN-STATUZ               = ENDP                  <V76L01>
     *  The exit paragraph name will be standardised to 2090 -EXIT.
     *       GO TO 2400-EXIT
     *       GO TO 2090-EXIT                                   <V76L01>
     *    END-IF.                                              <V76L01>
     *                                                         <V76L01>
     *    IF Sd5jl-BNYSEL          NOT = SPACE                 <V76L01>
     *       MOVE WSAA-QUOTA          TO Sd5jl-BNYPC           <V76L01>
     *       ADD  WSAA-QUOTA          TO WSAA-BNYPC-TOTAL      <V76L01>
     *       MOVE SUPD                TO SCRN-FUNCTION         <V76L01>
     *                                                         <V76L01>
     *       CALL 'Sd5jlIO'        USING SCRN-SCREEN-PARAMS    <V76L01>
     *                                   Sd5jl-DATA-AREA       <V76L01>
     *                                   Sd5jl-SUBFILE-AREA    <V76L01>
     *                                                         <V76L01>
     *       IF SCRN-STATUZ        NOT = O-K                   <V76L01>
     *          MOVE SCRN-STATUZ      TO SYSR-STATUZ           <V76L01>
     *          PERFORM 600-FATAL-ERROR                        <V76L01>
     *       END-IF                                            <V76L01>
     *    END-IF.                                              <V76L01>
     *                                                         <V76L01>
     *    MOVE SRDN                   TO SCRN-FUNCTION.        <V76L01>
     *                                                         <V76L01>
     *  The exit paragraph name will be standardised to 2090 -EXIT.
     * 2400-EXIT.
     * 2090-EXIT.                                               <V76L01>
     *    EXIT.                                                <V76L01>
     * </pre>
     */
    protected void chkAlrdyRlnshp2600() {

        crelpf = crelpfDAO.getMcrlByCoyAndNum(wsspcomn.fsuco.toString(), wsaaClntnum.toString(), wsspcomn.fsuco.toString(), sv.bnysel.toString());
        if (crelpf != null && isEQ(sv.cltreln, SPACES)) {
            sv.cltreln.set(crelpf.getCltreln());
            return;
        }
        if (crelpf != null && isNE(sv.cltreln, crelpf.getCltreln()) && isNE(sv.cltreln, SPACES)) {
            sv.cltrelnErr.set(errorsInner.rfk0);
            wsspcomn.edterror.set("Y");
        }
    }

    protected void a100MandatoryBnfy() {
        /*  Check for Mandatory Beneficiary's relation using TR52Z.        */
        wsaaMandatorys.set(SPACES);
        wsaaFound.set(SPACES);
        wsaaCount = 0;
        iz = 0;
        itempf = new Itempf();
        itempf.setItempfx(smtpfxcpy.item.toString());
        itempf.setItemcoy(wsspcomn.company.toString());
        itempf.setItemtabl(tr52z);
        itempf.setItemitem(chdrpf.getCnttype());
        itempf = itemDAO.getItempfRecord(itempf);

        if (itempf == null) {
            itempf = new Itempf();
            itempf.setItempfx(smtpfxcpy.item.toString());
            itempf.setItemcoy(wsspcomn.company.toString());
            itempf.setItemtabl(tr52z);
            itempf.setItemitem("***");
            itempf = itemDAO.getItempfRecord(itempf);
        }
        if (itempf != null) {
            tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
        } else {
            tr52zrec.tr52zRec.set(SPACES);
        }
        for (ix = 1; !(isGT(ix, 10)); ix++) {
            if (isNE(tr52zrec.bnytype[ix], SPACES)
                    && isEQ(tr52zrec.manopt[ix], "Y")) {
                iz++;
                for (iy = 1; !(isGT(iy, 30)
                        || isEQ(wsaaMandatory[iz], "Y")); iy++) {
                    wsaaMandatory[iz].set("N");
                    if (isEQ(tr52zrec.bnytype[ix], wsaaType[iy])) {
                        wsaaMandatory[iz].set("Y");
                    }
                }
            }
        }
        if (ix > 10) {
            ix = 1;
            while (!(notFound.isTrue())) {
                a300ContinueCheck();
            }

        }
        wsaaCount = wsaaCount + inspectTallyAll(wsaaMandatorys, "N");
        if (isGT(wsaaCount, 0)) {
            sv.chdrnumErr.set(errorsInner.rfk2);
            wsspcomn.edterror.set("Y");
        }
    }

    protected void a200ReadT3584() {
        descpf = descDAO.getdescData(smtpfxcpy.item.toString(), t3584, sv.cltreln.toString(), wsspcomn.fsuco.toString(), wsspcomn.language.toString());
        if (descpf == null) {
            sv.cltrelnErr.set(errorsInner.w038);
        }
    }

    protected void a300ContinueCheck() {
        if (isEQ(tr52zrec.gitem, SPACES)) {
            wsaaFound.set("N");
            return;
        }
        itempf.setItemcoy(wsspcomn.company.toString());
        itempf.setItemtabl(tr52z);
        itempf.setItemitem(tr52zrec.gitem.toString());
        itempf = itemDAO.getItempfRecord(itempf);
        if (itempf != null) {
            tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
        } else {
            tr52zrec.tr52zRec.set(SPACES);
        }
        for (ix = 1; ix <= 10; ix++) {
            if (isNE(tr52zrec.bnytype[ix], SPACES)
                    && isEQ(tr52zrec.manopt[ix], "Y")) {
                iz++;
                for (iy = 1; !(iy > 30 || isEQ(wsaaMandatory[iz], "Y")); iy++) {
                    wsaaMandatory[iz].set("N");
                    if (isEQ(tr52zrec.bnytype[ix], wsaaType[iy])) {
                        wsaaMandatory[iz].set("Y");
                    }
                }
            }
        }
    }

    protected void b100CallTr52y() {
        /* Read Beneficiary Type Table - TR52Y.                            */
        itempf = new Itempf();
        itempf.setItempfx(smtpfxcpy.item.toString());
        itempf.setItemcoy(wsspcomn.company.toString());
        itempf.setItemtabl(tr52y);
        itempf.setItemitem(sv.bnytype.toString());
        itempf = itemDAO.getItempfRecord(itempf);         //MLIL-230(Base Issue)
        if (itempf != null) {
            tr52yrec.tr52yRec.set(StringUtil.rawToString(itempf.getGenarea()));
        } else {
            tr52yrec.tr52yRec.set(SPACES);
        }
    }

    protected void b200MandRlsp() {
        /*  Check for Mandatory Beneficiary's relation using TR52Z.        */
        wsaaCrtBnfyreln = "N";
        wsaaBeneTypeFound.set("N");
        wsaaValidRelation.set("N");
        if (isNE(sv.bnytype, SPACES)) {
            b300ReadTr52z();
        }
        if (isEQ(wsaaCrtBnfyreln, "N")
                && validRelation.isTrue()
                && isNE(sv.bnysel, wsaaClntnum)) {
            sv.cltrelnErr.set(errorsInner.rfk1);
        }
        if (beneTypeFound.isTrue()
                && isEQ(tr52zrec.selfind[wsbbTableSub.toInt()], "Y")) {
            if (isEQ(sv.bnysel, wsaaClntnum)) {
                sv.cltreln.set("SELF");
            } else {
                if (isEQ(sv.cltreln, "SELF")) {
                    sv.bnyselErr.set(errorsInner.rfk1);
                }
            }
        }
        if (beneTypeFound.isTrue()
                && isNE(tr52zrec.selfind[wsbbTableSub.toInt()], "Y")) {
            if (isEQ(sv.bnysel, wsaaClntnum)
                    && noSelfRelation.isTrue()) {
                sv.bnyselErr.set(errorsInner.rfk3);
            }
        }
        if (beneTypeFound.isTrue()) {
            wsbbStackArrayInner.wsbbSelfind[scrnparams.subfileRrn.toInt()].set(tr52zrec.selfind[wsbbTableSub.toInt()]);
        } else {
            wsbbStackArrayInner.wsbbSelfind[scrnparams.subfileRrn.toInt()].set(SPACES);
        }
        if (isEQ(wsaaCrtBnfyreln, "Y")) {
            wsbbStackArrayInner.wsbbRevcflg[scrnparams.subfileRrn.toInt()].set(tr52zrec.revcflg[wsbbTableRsub.toInt()]);
        } else {
            wsbbStackArrayInner.wsbbRevcflg[scrnparams.subfileRrn.toInt()].set(SPACES);
        }
    }

    protected void b300ReadTr52z() {
        wsaaFound.set("Y");
        /* Read the beneficiary type details from TR52Z for the            */
        /* contract type held on CHDRMNA.                                  */
        itempf = new Itempf();
        itempf.setItempfx(smtpfxcpy.item.toString());
        itempf.setItemcoy(wsspcomn.company.toString());
        itempf.setItemtabl(tr52z);
        itempf.setItemitem(chdrpf.getCnttype()); /* IJTI-1479 */
        itempf = itemDAO.getItempfRecord(itempf);

        if (itempf == null) {
            itempf = new Itempf();
            itempf.setItempfx(smtpfxcpy.item.toString());
            itempf.setItemcoy(wsspcomn.company.toString());
            itempf.setItemtabl(tr52z);
            itempf.setItemitem("***");
            itempf = itemDAO.getItempfRecord(itempf);
        }

        if (itempf != null) {
            tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
        } else {
            tr52zrec.tr52zRec.set(SPACES);
        }

        /* Find the benef type in the table by checking the current table  */
        /* and if not found checking subsequent tables if they exist.      */
        wsbbTableSub.set(1);
        wsbbTableRsub.set(1);
        while (!(isEQ(tr52zrec.bnytype[wsbbTableSub.toInt()], sv.bnytype)
                || isNE(wsaaFound, "Y"))) {
            b400FindBnytype();
        }

        if (isEQ(sv.bnytype, tr52zrec.bnytype[wsbbTableSub.toInt()])) {
            beneTypeFound.setTrue();
        }
        /* Compute the subscript for the first relat and find the relat.   */
        /* Note the position of the bene type(found already above)         */
        /* determines the start position of the associated relationship.   */
        if (isNE(sv.cltreln, SPACES)
                && beneTypeFound.isTrue()) {
            compute(wsbbTableRsub, 0).set(add(mult((sub(wsbbTableSub, 1)), 5), 1));
            while (!(isEQ(tr52zrec.cltreln[wsbbTableRsub.toInt()], sv.cltreln)
                    || notFound.isTrue())) {
                b700FindRelation();
            }

        }
        if (isNE(sv.cltreln, SPACES)) {
            if (isEQ(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])) {
                validRelation.setTrue();
                wsaaCrtBnfyreln = "Y";
            }
        }
        if (isNE(sv.cltreln, SPACES)) {
            wsaaSelfRelation.set("N");
            if (isEQ(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])
                    && isEQ(tr52zrec.cltreln[wsbbTableRsub.toInt()], "SELF")) {
                selfRelation.setTrue();
            }
        }
    }

    protected void b400FindBnytype() {
        /*B410-BEGIN*/
        /* Actually find the BNYTYPE.                                      */
        for (wsbbTableSub.set(1); !(isGT(wsbbTableSub, 10)
                || isEQ(tr52zrec.bnytype[wsbbTableSub.toInt()], sv.bnytype)); wsbbTableSub.add(1)) {
            b500Nothing();
        }
        /* If the type has not been found look in the continuation table   */
        if (isGT(wsbbTableSub, 10)
                || isNE(tr52zrec.bnytype[wsbbTableSub.toInt()], sv.bnytype)) {
            wsbbTableSub.set(1);
            b600ContinuationTable();
        }
        /*B490-EXIT*/
    }

    protected void b500Nothing() {
        /*B510-ABSOLUTLY-NOTHING*/
        /** Dummy section used to reference the TR52Z table.                */
        /*B590-EXIT*/
    }

    protected void b600ContinuationTable() {
        /* Read the beneficiary type details from continuation table       */
        /* Last entry on the table is found and the beneficiary type       */
        /* looked for has not been found.                                  */
        if (isEQ(tr52zrec.gitem, SPACES)) {
            wsaaFound.set("N");
            return;
        }
        itempf.setItemcoy(wsspcomn.company.toString());
        itempf.setItemtabl(tr52z);
        itempf.setItemitem(tr52zrec.gitem.toString());
        if (itempf != null) {
            tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
        } else {
            tr52zrec.tr52zRec.set(SPACES);
        }
    }

    protected void b700FindRelation() {
        try {
            while (!(isEQ(wsbbTableRsub, 50)
                    || isEQ(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])
                    || notFound.isTrue())) {
                b770SearchLine();
            }

            /*  If not found on this table entry, try the next one.            */
            if (isNE(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])) {
                wsbbTableSub.set(1);
                wsbbTableRsub.set(1);
                b600ContinuationTable();
            }
            goTo(GotoLabel.b790Exit);
        } catch (GOTOException e) {
            /* Expected exception for control flow purposes. */
        }
    }


    /**
     * <pre>
     *  Check a single line of riders (performed paragraphs).
     * </pre>
     */
    protected void b770SearchLine() {
        for (int loopVar1 = 0; !(loopVar1 == 5); loopVar1 += 1) {
            b780CheckElement();
        }
        if (isNE(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])) {
            if (isNE(wsbbTableSub, 10)) {
                wsbbTableSub.add(1);
                if (isNE(tr52zrec.bnytype[wsbbTableSub.toInt()], SPACES)) {
                    wsaaFound.set("N");
                }
            }
        }
    }

    protected void b780CheckElement() {
        if (isNE(tr52zrec.cltreln[wsbbTableRsub.toInt()], SPACES)) {
            validRelation.setTrue();
        }
        if (isNE(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])
                && isNE(wsbbTableRsub, 50)) {
            wsbbTableRsub.add(1);
        }
    }

    /**
     * <pre>
     *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
     * </pre>
     */
    protected void update3000() {
        cpbnfypf = new Cpbnfypf();
        cpbnfypf.setChdrcoy(chdrpf.getChdrcoy().toString().charAt(0));
        cpbnfypf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
        wsaaPtrnFlag = "N";
        scrnparams.statuz.set(varcom.oK);
        scrnparams.function.set(varcom.sstrt);
        while (!(isEQ(scrnparams.statuz, varcom.endp))) {
            update3100();
        }
    }


    protected void update3100() {
        processScreen("SD5JL", sv);
        if (isNE(scrnparams.statuz, varcom.oK)
                && isNE(scrnparams.statuz, varcom.endp)) {
            syserrrec.statuz.set(scrnparams.statuz);
            fatalError600();
        }
        if (isEQ(scrnparams.statuz, varcom.endp)) {
            return;
        }
        checkChange3200();

        if (wsaaAdd.isTrue()) {
            addRecord3300();
        }
        if (wsaaDelete.isTrue()) {
            deleteRecord3400();
        }
        if (wsaaModify.isTrue()) {
            modifyRecord3500();
        }
        scrnparams.function.set(varcom.srdn);
    }

    protected void checkChange3200() {

        /* MOVE SPACES                TO WSAA-UPDATE-FLAG.             */
        wsaaUpdateFlag.set(SPACES);
        wsaaRrn = scrnparams.subfileRrn.toInt();

        /* Check amount */
        cpbnfypf = new Cpbnfypf();
        cpbnfypf.setChdrcoy(chdrpf.getChdrcoy());
        cpbnfypf.setChdrnum(chdrpf.getChdrnum());
        cpbnfypf.setBnytype(wsbbStackArrayInner.wsbbBnytype[wsaaRrn].toString());
        cpbnfypf.setBnyclt(wsbbStackArrayInner.wsbbBnysel[wsaaRrn].toString().trim());
        cpbnfypf.setTranno(chdrpf.getTranno());
        List<Cpbnfypf> cpbnfypfList = cpbnfypfDAO.getCpbnfypfByKey(cpbnfypf);
        if (cpbnfypfList != null && cpbnfypfList.size() > 0) {
            if(cpbnfypfList.get(0).getAmount() == null || isNE(cpbnfypfList.get(0).getAmount().doubleValue(),BigDecimal.valueOf(sv.amount.toDouble()))){
                wsaaUpdateFlag.set("MOD");
                return;
            }
        }

        /* Check if nothing has actually changed.*/
        if (isEQ(sv.bnysel, wsbbStackArrayInner.wsbbBnysel[wsaaRrn])
                && isEQ(sv.cltreln, wsbbStackArrayInner.wsbbBnyrln[wsaaRrn])
                && isEQ(sv.bnycd, wsbbStackArrayInner.wsbbBnycd[wsaaRrn])
                && isEQ(sv.bnypc, wsbbStackArrayInner.wsbbBnypc[wsaaRrn])
                && isEQ(sv.effdate, wsbbStackArrayInner.wsbbEffdate[wsaaRrn])
                && isEQ(sv.bnytype, wsbbStackArrayInner.wsbbBnytype[wsaaRrn])
                && isEQ(sv.sequence, wsbbStackArrayInner.wsbbSequence[wsaaRrn])
                && isEQ(sv.paymthbf, wsbbStackArrayInner.wsbbpaymthbf[wsaaRrn])
                && isEQ(sv.bankkey, wsbbStackArrayInner.wsbbBankkey[wsaaRrn])
                && isEQ(sv.bkrelackey, wsbbStackArrayInner.wsbbBankacckey[wsaaRrn])
                && isEQ(sv.amount, wsbbStackArrayInner.wsbbAmount[wsaaRrn])) {
            return;
        }
        /* Check if this is a new record.*/
        if (isNE(sv.bnysel, SPACES)
                && isEQ(wsbbStackArrayInner.wsbbBnysel[wsaaRrn], SPACES)) {
            /*     MOVE 'ADD'              TO WSAA-UPDATE-FLAG              */
            wsaaUpdateFlag.set("ADD");
            return;
        }
        /* Check if this is a record which has been deleted.*/
        if (isNE(wsbbStackArrayInner.wsbbBnysel[wsaaRrn], SPACES)
                && isEQ(sv.bnysel, SPACES)) {
            /*     MOVE 'DEL'              TO WSAA-UPDATE-FLAG              */
            wsaaUpdateFlag.set("DEL");
            return;
        }

        if (isEQ(sv.cltreln, SPACES)
                && isEQ(sv.bnycd, SPACES)
                && isEQ(sv.bnypc, ZERO)
                && isEQ(sv.bnysel, SPACES)
                && isEQ(sv.effdate, varcom.vrcmMaxDate)
                && isEQ(sv.bnytype, SPACES)
                && isEQ(sv.sequence, SPACES)) {
            return;
        }
        /* If it is none of the above, it must be a modification.*/
        /* MOVE 'MOD'                  TO WSAA-UPDATE-FLAG.             */
        wsaaUpdateFlag.set("MOD");
    }

    protected void addRecord3300() {
        /* Write a new beneficiary record.*/
        cpbnfypf.setChdrcoy(chdrpf.getChdrcoy().toString().charAt(0));
        cpbnfypf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
        cpbnfypf.setBnyclt(sv.bnysel.toString());
        /* MOVE Sd5jl-BNYRLN           TO BNFYMNA-BNYRLN.               */
        cpbnfypf.setCltreln(sv.cltreln.toString());
        cpbnfypf.setBnycd(sv.bnycd.toString().charAt(0));
        cpbnfypf.setBnytype(sv.bnytype.toString());
        cpbnfypf.setRelto(sv.relto.toString().charAt(0));
        cpbnfypf.setBnypc(BigDecimal.valueOf(sv.bnypc.toDouble()));
        cpbnfypf.setSelfind(wsbbStackArrayInner.wsbbSelfind[wsaaRrn].toString().charAt(0));
        cpbnfypf.setRevcflg(wsbbStackArrayInner.wsbbRevcflg[wsaaRrn].toString().charAt(0));
        cpbnfypf.setTranno(chdrpf.getTranno());
        cpbnfypf.setValidflag("1".charAt(0));
        cpbnfypf.setCurrfr(datcon1rec.intDate.toInt());
        /*                                BNFYMNA-EFFDATE.              */
        cpbnfypf.setEffdate(sv.effdate.toInt());
        cpbnfypf.setEnddate(sv.enddate.toInt());
        cpbnfypf.setCurrto(varcom.vrcmMaxDate.toInt());
        cpbnfypf.setTermid(varcom.vrcmTermid.toString());
        cpbnfypf.setUserT(varcom.vrcmUser.toInt());
        cpbnfypf.setTrtm(varcom.vrcmTime.toInt());
        cpbnfypf.setTrdt(varcom.vrcmDate.toInt());
        if (sv.sequence.length() < 2) {
            cpbnfypf.setSequence("0".concat(sv.sequence.toString()));
        } else {
            cpbnfypf.setSequence(sv.sequence.toString());
        }
        cpbnfypf.setPaymmeth(sv.paymthbf.toString());
        cpbnfypf.setBankkey(sv.bankkey.toString());
        cpbnfypf.setBankacckey(sv.bkrelackey.toString());
        cpbnfypf.setAmount(BigDecimal.valueOf(sv.amount.toDouble()));
        if (cpbnfypfDAO.insertCpbnfypf(cpbnfypf) <= 0) {
            syserrrec.statuz.set("MRNF");
            syserrrec.params.set(chdrpf.getChdrcoy().toString().concat(chdrpf.getChdrnum())); /* IJTI-1479 */
            fatalError600();
        }
    }

    protected void deleteRecord3400() {
        cpbnfypf = new Cpbnfypf();
        cpbnfypf.setChdrcoy(chdrpf.getChdrcoy().toString().charAt(0));
        cpbnfypf.setChdrnum(chdrpf.getChdrnum()); /* IJTI-1479 */
        cpbnfypf.setBnytype(wsbbStackArrayInner.wsbbBnytype[wsaaRrn].toString());
        cpbnfypf.setBnyclt(wsbbStackArrayInner.wsbbBnysel[wsaaRrn].toString());
        cpbnfypf.setTranno(chdrpf.getTranno());
        List<Cpbnfypf> list = cpbnfypfDAO.getCpbnfymnaByKeyAndTranno(cpbnfypf);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                cpbnfypf = list.get(i);
                callBnfymna3420();
            }
        }
    }

    /**
     * <pre>
     * *** PERFORM 3600-CALL-BNFYMNAIO.
     * </pre>
     */
    protected void callBnfymna3420() {
        cpbnfypf.setValidflag("2".charAt(0));
        cpbnfypf.setCurrto(datcon1rec.intDate.toInt());
        if (cpbnfypfDAO.updateCpbnfymnaByUnqueNum(cpbnfypf) <= 0) {
            syserrrec.statuz.set("MRNF");
            syserrrec.params.set(chdrpf.getChdrcoy().toString().concat(chdrpf.getChdrnum())); /* IJTI-1479 */
            fatalError600();
        }
        /* MOVE REWRT                  TO BNFYMNA-FUNCTION.             */
        wsaaLastTranno = cpbnfypf.getTranno();
    }


    protected void modifyRecord3500() {
        /* Validflag 2 old beneficiary record.*/
        cpbnfypf = new Cpbnfypf();
        cpbnfypf.setChdrcoy(chdrpf.getChdrcoy());
        cpbnfypf.setChdrnum(chdrpf.getChdrnum());
        cpbnfypf.setBnytype(wsbbStackArrayInner.wsbbBnytype[wsaaRrn].toString());
        cpbnfypf.setBnyclt(wsbbStackArrayInner.wsbbBnysel[wsaaRrn].toString().trim());
        cpbnfypf.setTranno(chdrpf.getTranno());
        List<Cpbnfypf> list = cpbnfypfDAO.getCpbnfymnaByKeyAndTranno(cpbnfypf);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                cpbnfypf = list.get(i);
                callBnfymna3520();
            }
        }
        /* MOVE READH                  TO BNFYMNA-FUNCTION.             */
    }

    /**
     * <pre>
     * *** PERFORM 3600-CALL-BNFYMNAIO.
     * </pre>
     */
    protected void callBnfymna3520() {
        cpbnfypf.setValidflag("2".charAt(0));
        cpbnfypf.setCurrto(datcon1rec.intDate.toInt());
        cpbnfypfDAO.updateCpbnfymnaByUnqueNum(cpbnfypf);
        /* MOVE REWRT                  TO BNFYMNA-FUNCTION.             */
        wsaaLastTranno = cpbnfypf.getTranno();
        /* Write new beneficiary record.*/
        instCpbnfypf = new Cpbnfypf();
        instCpbnfypf.setChdrcoy(chdrpf.getChdrcoy());
        instCpbnfypf.setChdrnum(chdrpf.getChdrnum());
        instCpbnfypf.setBnytype(sv.bnytype.toString());
        instCpbnfypf.setBnyclt(sv.bnysel.toString());
        /* MOVE Sd5jl-BNYRLN           TO BNFYMNA-BNYRLN.               */
        instCpbnfypf.setCltreln(sv.cltreln.toString());
        instCpbnfypf.setRelto(sv.relto.toString().charAt(0));
        instCpbnfypf.setBnycd(sv.bnycd.charat(0));
        instCpbnfypf.setBnypc(sv.bnypc.getbigdata());
        instCpbnfypf.setSelfind(wsbbStackArrayInner.wsbbSelfind[wsaaRrn].charat(0));
        instCpbnfypf.setRevcflg(wsbbStackArrayInner.wsbbRevcflg[wsaaRrn].charat(0));
        instCpbnfypf.setTranno(chdrpf.getTranno());
        instCpbnfypf.setCurrfr(datcon1rec.intDate.toInt());
        instCpbnfypf.setCurrto(varcom.vrcmMaxDate.toInt());
        instCpbnfypf.setEffdate(sv.effdate.toInt());
        instCpbnfypf.setEnddate(sv.enddate.toInt());
        instCpbnfypf.setValidflag("1".charAt(0));
        instCpbnfypf.setTermid(varcom.vrcmTermid.toString());
        instCpbnfypf.setUserT(varcom.vrcmUser.toInt());
        instCpbnfypf.setTrtm(varcom.vrcmTime.toInt());
        instCpbnfypf.setTrdt(varcom.vrcmDate.toInt());
        if (sv.sequence.length() < 2) {
            instCpbnfypf.setSequence("0".concat(sv.sequence.toString()));
        } else {
            instCpbnfypf.setSequence(sv.sequence.toString());
        }
        instCpbnfypf.setPaymmeth(sv.paymthbf.toString());
        instCpbnfypf.setBankkey(sv.bankkey.toString());
        instCpbnfypf.setBankacckey(sv.bkrelackey.toString());
        instCpbnfypf.setAmount(BigDecimal.valueOf(sv.amount.toDouble()));
        if (cpbnfypfDAO.insertCpbnfypf(instCpbnfypf) <= 0) {
            syserrrec.statuz.set("MRNF");
            syserrrec.params.set(chdrpf.getChdrcoy().toString().concat(chdrpf.getChdrnum())); /* IJTI-1479 */
            fatalError600();
        }
    }
    /**
     * <pre>
     *     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
     * </pre>
     */
    protected void whereNext4000() {
        nextProgram4010();
    }

    protected void nextProgram4010() {
        optswchCall4100();
    }

    protected void optswchCall4100() {
        GotoLabel nextMethod = GotoLabel.DEFAULT;
        while (true) {
            try {
                switch (nextMethod) {
                    case DEFAULT:
                        call4110();
                        lineSwitching4120();
                    case switch4170:
                        switch4170();
                }
                break;
            } catch (GOTOException e) {
                nextMethod = (GotoLabel) e.getNextMethod();
            }
        }
    }

    protected void switch4170() {
        optswchrec.optsItemCompany.set(wsspcomn.company);
        optswchrec.optsFunction.set("STCK");
        callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
        if (isNE(optswchrec.optsStatuz, varcom.oK)
                && isNE(optswchrec.optsStatuz, varcom.endp)) {
            syserrrec.params.set(optswchrec.rec);
            syserrrec.statuz.set(optswchrec.optsStatuz);
            fatalError600();
        }
        if (isEQ(optswchrec.optsStatuz, varcom.oK)) {
            wsspcomn.programPtr.add(1);
            scrnparams.subfileRrn.set(wsaaRrn);
        } else {
            wsspcomn.nextprog.set(scrnparams.scrname);
        }
    }

    protected void lineSwitching4120() {
        if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
            scrnparams.subfileRrn.set(ZERO);
        } else {
            scrnparams.subfileRrn.set(wsaaRrn);
        }
        scrnparams.statuz.set(varcom.oK);
        scrnparams.function.set(varcom.sstrt);
        processScreen("SD5JL", sv);
        if (isNE(scrnparams.statuz, varcom.oK)
                && isNE(scrnparams.statuz, varcom.endp)) {
            syserrrec.statuz.set(scrnparams.statuz);
            fatalError600();
        }
        if (isEQ(scrnparams.statuz, varcom.endp)) {
            goTo(GotoLabel.switch4170);
        }
        scrnparams.function.set(varcom.supd);
        processScreen("SD5JL", sv);
        if (isNE(scrnparams.statuz, varcom.oK)
                && isNE(scrnparams.statuz, varcom.endp)) {
            syserrrec.statuz.set(scrnparams.statuz);
            fatalError600();
        }
    }

    protected void call4110() {
        wsspcomn.nextprog.set(wsaaProg);
    }

    protected void loadProgram4400() {
        /*RESTORE1*/
        wsspcomn.secProg[wsaaX].set(ptemprec.progOut[wsaaY]);
        wsaaX++;
        wsaaY++;
        /*EXIT1*/
    }

    /*
     * Class transformed  from Data Structure WSBB-STACK-ARRAY--INNER
     */
    private static final class WsbbStackArrayInner {

        private FixedLengthStringData wsbbStackArray = new FixedLengthStringData(4560);

        private FixedLengthStringData wsbbBnytypeArray = new FixedLengthStringData(60).isAPartOf(wsbbStackArray, 0);
        private FixedLengthStringData[] wsbbBnytype = FLSArrayPartOfStructure(30, 2, wsbbBnytypeArray, 0);
        private FixedLengthStringData wsbbBnyrlnArray = new FixedLengthStringData(120).isAPartOf(wsbbStackArray, 60);
        /*                             OCCURS 10.                       */
        private FixedLengthStringData[] wsbbBnyrln = FLSArrayPartOfStructure(30, 4, wsbbBnyrlnArray, 0);
        private FixedLengthStringData wsbbReltoArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 180);
        private FixedLengthStringData[] wsbbRelto = FLSArrayPartOfStructure(30, 1, wsbbReltoArray, 0);
        private FixedLengthStringData wsbbRevcflgArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 210);
        private FixedLengthStringData[] wsbbRevcflg = FLSArrayPartOfStructure(30, 1, wsbbRevcflgArray, 0);
        private FixedLengthStringData wsbbBnycdArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 240);
        /*                             OCCURS 10.                       */
        private FixedLengthStringData[] wsbbBnycd = FLSArrayPartOfStructure(30, 1, wsbbBnycdArray, 0);
        private FixedLengthStringData wsbbBnypcArray = new FixedLengthStringData(150).isAPartOf(wsbbStackArray, 270);
        /*                             OCCURS 10.                       */
        private ZonedDecimalData[] wsbbBnypc = ZDArrayPartOfStructure(30, 5, 2, wsbbBnypcArray, 0, UNSIGNED_TRUE);

        private FixedLengthStringData wsbbBnyselArray = new FixedLengthStringData(300).isAPartOf(wsbbStackArray, 420);
        /*                             OCCURS 10.                       */
        private FixedLengthStringData[] wsbbBnysel = FLSArrayPartOfStructure(30, 10, wsbbBnyselArray, 0);
        private FixedLengthStringData wsbbClntnmArray = new FixedLengthStringData(900).isAPartOf(wsbbStackArray, 720);
        /*                             OCCURS 10.                       */
        private FixedLengthStringData[] wsbbClntnm = FLSArrayPartOfStructure(30, 30, wsbbClntnmArray, 0);
        private FixedLengthStringData wsbbEffdateArray = new FixedLengthStringData(240).isAPartOf(wsbbStackArray, 1620);
        /*                             OCCURS 10.                       */
        private PackedDecimalData[] wsbbEffdate = PDArrayPartOfStructure(30, 8, 0, wsbbEffdateArray, 0);
        private FixedLengthStringData wsbbEnddateArray = new FixedLengthStringData(240).isAPartOf(wsbbStackArray, 1860);
        /*                             OCCURS 10.               <V76L01>*/
        private PackedDecimalData[] wsbbEnddate = PDArrayPartOfStructure(30, 8, 0, wsbbEnddateArray, 0);

        private FixedLengthStringData wsbbChangeArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2100);
        /*                             OCCURS 30.                       */
        private FixedLengthStringData[] wsbbChange = FLSArrayPartOfStructure(30, 1, wsbbChangeArray, 0);
        private FixedLengthStringData wsbbSelfindArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2130);
        private FixedLengthStringData[] wsbbSelfind = FLSArrayPartOfStructure(30, 1, wsbbSelfindArray, 0);
        /*ICIL-11*/
        private FixedLengthStringData wsbbSequenceArray = new FixedLengthStringData(60).isAPartOf(wsbbStackArray, 2160);
        private FixedLengthStringData[] wsbbSequence = FLSArrayPartOfStructure(30, 2, wsbbSequenceArray, 0);
        private FixedLengthStringData wsbbpaymthbfArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2220);
        private FixedLengthStringData[] wsbbpaymthbf = FLSArrayPartOfStructure(30, 1, wsbbpaymthbfArray, 0);
        private FixedLengthStringData wsbbBankkeyArray = new FixedLengthStringData(300).isAPartOf(wsbbStackArray, 2250);
        private FixedLengthStringData[] wsbbBankkey = FLSArrayPartOfStructure(30, 10, wsbbBankkeyArray, 0);
        private FixedLengthStringData wsbbBankacckeyArray = new FixedLengthStringData(600).isAPartOf(wsbbStackArray, 2550);
        private FixedLengthStringData[] wsbbBankacckey = FLSArrayPartOfStructure(30, 20, wsbbBankacckeyArray, 0);
        private FixedLengthStringData wsbbBankdescArray = new FixedLengthStringData(900).isAPartOf(wsbbStackArray, 3150);
        private FixedLengthStringData[] wsbbBankdesc = FLSArrayPartOfStructure(30, 30, wsbbBankdescArray, 0);


        private FixedLengthStringData wsbbAmountArray = new FixedLengthStringData(510).isAPartOf(wsbbStackArray, 4050);
        /*                             OCCURS 10.                       */
        private ZonedDecimalData[] wsbbAmount = ZDArrayPartOfStructure(30, 17, 2, wsbbAmountArray, 0, UNSIGNED_TRUE);
    }

    /*
     * Class transformed  from Data Structure ERRORS--INNER
     */
    private static final class ErrorsInner {
        /* ERRORS */
        private String e048 = "E048";
        private String e186 = "E186";
        private String e631 = "E631";
        private String e058 = "E058";
        private String e470 = "E470";
        private String f348 = "F348";
        private String f691 = "F691";
        private String h046 = "H046";
        private String h359 = "H359";
        private String f782 = "F782";
        private String rfk0 = "RFK0";
        private String rfk1 = "RFK1";
        private String rfk2 = "RFK2";
        private String rfk3 = "RFK3";
        private String rfk6 = "RFK6";
        private String w038 = "W038";
        private String rfwq = "RFWQ";
        private String rfwr = "RFWR";
        private String h093 = "H093";
        private String rrbr = "RRBR";//ICIL-11
        private String rrbs = "RRBS";//ICIL-11
        private String e570 = "E570";//ICIL-11
        private String rfh7 = "RFH7";//ICIL-11
        private String f826 = "F826";//ICIL-11
        private String rrlz = "RRLZ";
        private String rrm0 = "RRM0";
        private String rrm1 = "RRM1";
        private String rrm9 = "RRM9";
    }

    protected void setEffectiveDate() {
        if (isNE(sv.cltreln, SPACES)
                && isNE(sv.bnytype, SPACES)
                && isNE(sv.bnycd, SPACES)
                && isNE(sv.bnypc, ZERO)
                && isNE(sv.bnysel, SPACES)
                && (isEQ(sv.effdate, varcom.vrcmMaxDate)
                || isEQ(sv.effdate, ZERO))) {
            sv.effdate.set(datcon1rec.intDate);
        }
    }
    protected void getOwnerNumber() {
        if (isEQ(sv.relto, "O")
                || isEQ(sv.relto, "B")) {
            wsaaClntnum.set(chdrpf.getCownnum());
        }
    }
}
