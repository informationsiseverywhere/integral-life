/*
 * File: Revbsurr.java
 * Date: 30 August 2009 2:06:04
 * Author: Quipoz Limited
 * 
 * Class transformed from REVBSURR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*      -----------------------------------
*           BONUS SURRENDER REVERSAL
*      -----------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*  The routine is used to reverse a Bonus Surrender.
*
*  All ACMVs that were posted in the forward transaction are
*  reversed.
*  The details of which contract to process originate from
*  the Policy Transaction File (PTRN) and are passed through
*  in the linkage.
*
*  The routine is driven by :
*    - The Company to which this Contract belongs.
*    - The Contract Header Number.
*    - The Transaction Number used in the original
*      surrender transaction.
*
*  PROCESSING
*  ----------
*
*  - Get Today's date.
*
*  - Read the Contract Header details.
*
*  - Get the Transaction Code description from T1688 using
*    Transaction Code passed in through Linkage.
*
*  - BEGN on the ACMV file. This will get to the first
*    accounting record which has this Transaction Number.
*
*  - Now find the first record under this number which has the
*    appropriate Transaction Code (the code of the original
*    surrender transaction).
*
*  MAIN PROCESSING LOOP.
*  --------------------
*
*  PROCESS UNTIL ACMV-STATUZ = ENDP
*
*  - Post a new ACMV with :
*
*              ORIGAMT = previous value * -1.
*              TRANSACTION-CODE = Tran Code of the Reversing
*                                 transaction (passed in through
*                                 Linkage).
*              TRANDESC = Description of the reversing Tran Code.
*              TRANSACTION NUMBER = Previous Contract Header
*                                   Tran Number + 1.
*
*  - Find the next ACMV to process by reading the file
*    sequentially until a record of the same transaction code
*    is read.
*
*    If a new Company, Contract Header Number or Transaction
*    Number is encountered, move ENDP to ACMV-STATUZ and
*    effectively exit the processing loop.
*
*  END OF MAIN PROCESSING LOOP.
*
*****************************************************************
* </pre>
*/
public class Revbsurr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVBSURR";
	private FixedLengthStringData wsaaRevTrandesc = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* FORMATS */
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String acmvrevrec = "ACMVREVREC";
	private static final String arcmrec = "ARCMREC";

	private FixedLengthStringData wsaaAcmvFlag = new FixedLengthStringData(1);
	private Validator nextAcmvFound = new Validator(wsaaAcmvFlag, "Y");
	private Validator nextAcmvNotFound = new Validator(wsaaAcmvFlag, "N");
		/* TABLES */
	private static final String t1688 = "T1688";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Reverserec reverserec = new Reverserec();

	public Revbsurr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*START*/
		initialise1000();
		process2000();
		processAcmvOptical2010();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaAcmvFlag.set("N");
		/*  Get todays date from DATCON1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*  Read in the contract header details*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError9000();
		}
		/*  Get transaction code description for reversal transaction.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setDesctabl(t1688);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		wsaaRevTrandesc.set(descIO.getLongdesc());
	}

protected void process2000()
	{
		start2000();
	}

protected void start2000()
	{
		/*  Get to first ACMV record for this CHDRNUM/TRANNO*/
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setBatctrcde(SPACES);
		SmartFileCode.execute(appVars, acmvrevIO);
		if ((isNE(acmvrevIO.getStatuz(),varcom.oK))
		&& (isNE(acmvrevIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(acmvrevIO.getParams());
			fatalError9000();
		}
		if ((isNE(acmvrevIO.getRldgcoy(),reverserec.company))
		|| (isNE(acmvrevIO.getRdocnum(),reverserec.chdrnum))
		|| (isNE(acmvrevIO.getTranno(),reverserec.tranno))) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		else {
			if ((isEQ(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde))) {
				wsaaAcmvFlag.set("Y");
			}
		}
		/*  Provided that a record for this CHDRNUM/TRANNO has been found,*/
		/*  now look for the first one with the correct transaction code.*/
		if (isNE(acmvrevIO.getStatuz(),varcom.endp)
		&& nextAcmvNotFound.isTrue()) {
			findNextAcmv3000();
		}
		/*  Process all the records.*/
		while ( !(isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
			reverseAcmvRecs2200();
		}
		
	}

protected void processAcmvOptical2010()
	{
		start2010();
	}

protected void start2010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			fatalError9000();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			fatalError9000();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs2200();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				fatalError9000();
			}
		}
	}

protected void reverseAcmvRecs2200()
	{
		start2200();
	}

protected void start2200()
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.termid.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(),-1));
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(chdrlifIO.getChdrnum());
		lifacmvrec.trandesc.set(wsaaRevTrandesc);
		/* MOVE WSAA-TODAY             TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		/* MOVE '99999999'             TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
		findNextAcmv3000();
	}

protected void findNextAcmv3000()
	{
			start3001();
		}

protected void start3001()
	{
		/*  Read down ACMV file for next record with appropriate*/
		/*  transaction code.*/
		wsaaAcmvFlag.set("N");
		while ( !((isEQ(acmvrevIO.getStatuz(),varcom.endp))
		|| nextAcmvFound.isTrue())) {
			acmvrevIO.setFunction(varcom.nextr);
			/*      CALL 'ACMVREVIO' USING ACMVREV-PARAMS                    */
			if (isEQ(wsaaIoCall,"CP")) {
				FixedLengthStringData groupTEMP = acmvrevIO.getParams();
				callProgram(Acmvrevcp.class, groupTEMP);
				acmvrevIO.setParams(groupTEMP);
			}
			else {
				SmartFileCode.execute(appVars, acmvrevIO);
			}
			if ((isNE(acmvrevIO.getStatuz(),varcom.oK))
			&& (isNE(acmvrevIO.getStatuz(),varcom.endp))) {
				syserrrec.params.set(acmvrevIO.getParams());
				fatalError9000();
			}
			if ((isNE(acmvrevIO.getRldgcoy(),reverserec.company))
			|| (isNE(acmvrevIO.getRdocnum(),reverserec.chdrnum))
			|| (isNE(acmvrevIO.getTranno(),reverserec.tranno))
			|| (isEQ(acmvrevIO.getStatuz(),varcom.endp))) {
				acmvrevIO.setStatuz(varcom.endp);
			}
			else {
				if ((isEQ(acmvrevIO.getBatctrcde(),reverserec.oldBatctrcde))) {
					wsaaAcmvFlag.set("Y");
				}
			}
		}
		
	}

protected void fatalError9000()
	{
					fatalErrors9000();
					errorProg9000();
				}

protected void fatalErrors9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg9000()
	{
		reverserec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
