/*
 * File: Ph595.java
 * Date: 30 August 2009 1:10:03
 * Author: Quipoz Limited
 *
 * Class transformed from PH595.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.screens.Sh595ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                    NON-FORFEITURE OPTION
*
*This is a minor alteration,  to  change  the  Non-forfeiture
*option on the contract header using details captured from
*screen/program PH595.
*
* Initialise
* ----------
*
* Read CHDRMNA (RETRV)  in  order to obtain the contract header
* information.
*
* Read  the 'DESCIO' in  order to obtain the description of the
* risk and premium statuses.
*
* In order to format  the  required  names  the  client details
* record is read and  the  relevant copybook for formatting the
* names must be included in the program.
*
* Validation
* ----------
*
* If KILL is requested, then skip the validation and exit.
*
* Check the  returned  screen  for  any  errors,  if errors are
* present, then highlight.
*
* Non-forfeiture option validation:
*
*      - new non-forfeiture option must be entered.
*
*      - new and old option must not be the same.
*
*      - reason code must be entered.
*
*Updating
*--------
*
* Updating occurs  with  the  updating  of  the contract header
* record, update contract additional information and write reason
* log file.
*
* If the 'KILL' function key was pressed, skip the updating.
*
*
*  CONTRACT HEADER UPDATE
*
*      - rewrite (WRITS) the contract header record.
*
*  CONTRACT ADDITIONAL INFORMATION (HPAD) WRITE
*
*      - set the new non-forfeiture option code to HPAD record
*
*      - rewrite (REWRT) the HPAD record.
*
*  TRANSACTION REASON LOG (RESN) WRITE
*
*      - set the reason code and description to RESN record
*
*      - rewrite (WRITE) the RESN record.
*
* GENERAL HOUSEKEEPING
*
*      - Write a PTRN record (if updating the CHDR):
*
*           - contract key from contract header
*           - transaction number from WSSP
*           - transaction effective date from VRCM-DATE
*           - batch key information WSSP batchkey
*
*      - Update the header by calling BATCUP with a function of
*           WRITS and the following parameters:
*
*           - transaction count equals 1
*           - all other amounts equal to zero
*           - batch key from WSSP
*
*      - Release contract SFTLOCK.
*
* Next Program
* ------------
*
* Add 1 to the program pointer and exit.
*                                                                     *
*****************************************************************
* </pre>
*/
public class Ph595 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH595");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 8);
		/* ERRORS */
	private static final String e186 = "E186";
	private static final String e304 = "E304";
	private static final String hl58 = "HL58";
	private static final String hl57 = "HL57";
		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5688 = "T5688";
	private static final String t5500 = "T5500";
	private static final String tr384 = "TR384";
	private static final String th586 = "TH586";
	private static final String th584 = "TH584";
	private static final String ptrnrec = "PTRNREC";
	private static final String resnrec = "RESNREC";
	private static final String chdrmnarec = "CHDRMNAREC";
	private static final String hpadrec = "HPADREC";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	protected ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private ResnTableDAM resnIO = new ResnTableDAM();
	private Batckey wsaaBatcKey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Tr384rec tr384rec = new Tr384rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wssplife wssplife = new Wssplife();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	protected Sh595ScreenVars sv = ScreenProgram.getScreenVars( Sh595ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		reascd2050,
		checkForErrors2080
	}

	public Ph595() {
		super();
		screenVars = sv;
		new ScreenModel("Sh595", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		blankOutFields1020();
	}

protected void initialise1010()
	{
		wsaaBatcKey.set(wsspcomn.batchkey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}

protected void blankOutFields1020()
	{
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		/* Retrieve contract fields from I/O module*/
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmnaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		/*  Load all fields from the Contract Header to the Screen*/
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.ownersel.set(chdrmnaIO.getCownnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		sv.ptdate.set(chdrmnaIO.getPtdate());
		/*  Look up premium status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmnaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.premStatDesc.set(descIO.getShortdesc());
		}
		else {
			sv.premStatDesc.fill("?");
		}
		/*  Look up Risk status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmnaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.statdsc.set(descIO.getShortdesc());
		}
		else {
			sv.statdsc.fill("?");
		}
		/*   Look up all other descriptions*/
		/*       - owner name*/
		if (isNE(sv.ownersel, SPACES)) {
			formatClientName1700();
		}
		formatNfftDetails5000();
	}

	/**
	* <pre>
	*      RETRIEVE OWNER DETAILS AND SET SCREEN
	* </pre>
	*/
protected void formatClientName1700()
	{
		readClientRecord1710();
	}

protected void readClientRecord1710()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.ownersel);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.ownerselErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					nonForfeit2020();
				case reascd2050:
					reascd2050();
				case checkForErrors2080:
					checkForErrors2080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SH595IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SH595-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* IF  SH595-ZNFOPT-02         = SPACES*/
		/*     MOVE E186               TO SH595-ZNFOPT02-ERR.*/
		if (isEQ(sv.reasoncd, SPACES)) {
			sv.reasoncdErr.set(e186);
		}
		if (isEQ(sv.znfopt02, sv.znfopt01)) {
			sv.znfopt02Err.set(hl58);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.znfopt02, SPACES)) {
			goTo(GotoLabel.reascd2050);
		}
	}

protected void nonForfeit2020()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		/* MOVE T9504                  TO DESC-DESCTABL.                */
		descIO.setDesctabl(th586);
		descIO.setDescitem(sv.znfopt02);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.znfoptd02.set(descIO.getLongdesc());
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(th584);
		itemIO.getItemitem().setSub1String(1, 3, sv.znfopt02);
		itemIO.getItemitem().setSub1String(4, 3, chdrmnaIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.znfopt02Err.set(hl57);
		}
	}

protected void reascd2050()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5500);
		descIO.setDescitem(sv.reasoncd);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)
		&& isEQ(sv.resndesc, SPACES)) {
			sv.resndesc.set(descIO.getLongdesc());
		}
	}

protected void checkForErrors2080()
	{
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
		writeReasncd3050();
		relsfl3070();
	}

protected void updateDatabase3010()
	{
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmnaIO.setTranid(varcom.vrcmCompTranid);
		/*  Update contract header fields as follows*/
		chdrmnaIO.setFunction(varcom.keeps);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setFunction(varcom.writs);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		/* Re-write the HPAD record for this contract.*/
		hpadIO.setFunction(varcom.readh);
		hpadIO.setFormat(hpadrec);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		hpadIO.setZnfopt(sv.znfopt02);
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		/* Write a PTRN record.*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		wsaaBatcKey.set(wsspcomn.batchkey);
		ptrnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmnaIO.getChdrnum());
		ptrnIO.setTranno(chdrmnaIO.getTranno());
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		writeLetter6000();
		/* Update the batch header using BATCUP.*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(wsspcomn.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.statuz.set(ZERO);
		batcuprec.function.set(SPACES);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError600();
		}
	}

protected void writeReasncd3050()
	{
		/* Create a RESN record if reason code or narrative was entered*/
		if (isNE(sv.resndesc, SPACES)
		|| isNE(sv.reasoncd, SPACES)) {
			resnIO.setDataKey(SPACES);
			resnIO.setChdrcoy(chdrmnaIO.getChdrcoy());
			resnIO.setChdrnum(chdrmnaIO.getChdrnum());
			resnIO.setTranno(chdrmnaIO.getTranno());
			resnIO.setTrancde(wsaaBatcKey.batcBatctrcde);
			resnIO.setReasoncd(sv.reasoncd);
			resnIO.setResndesc(sv.resndesc);
			resnIO.setTransactionDate(varcom.vrcmDate);
			resnIO.setTransactionTime(varcom.vrcmTime);
			resnIO.setUser(varcom.vrcmUser);
			resnIO.setFormat(resnrec);
			resnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, resnIO);
			if (isNE(resnIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(resnIO.getParams());
				fatalError600();
			}
		}
	}

protected void relsfl3070()
	{
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void formatNfftDetails5000()
	{
		start5010();
	}

	/**
	* <pre>
	*    This section formats the current non-forfeiture option    *
	* </pre>
	*/
protected void start5010()
	{
		hpadIO.setChdrcoy(chdrmnaIO.getChdrcoy());
		hpadIO.setChdrnum(chdrmnaIO.getChdrnum());
		hpadIO.setFunction(varcom.readr);
		hpadIO.setFormat(hpadrec);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)
		&& isNE(hpadIO.getStatuz(), varcom.endp)
		&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		if (isEQ(hpadIO.getStatuz(), varcom.endp)
		|| isEQ(hpadIO.getStatuz(), varcom.mrnf)) {
			sv.znfopt01.set(SPACES);
			return ;
		}
		sv.znfopt01.set(hpadIO.getZnfopt());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		/* MOVE T9504                  TO DESC-DESCTABL.                */
		descIO.setDesctabl(th586);
		descIO.setDescitem(sv.znfopt01);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.znfoptd01.set(descIO.getLongdesc());
		}
	}

protected void writeLetter6000()
	{
		readTr3846010();
	}

	/**
	* <pre>
	*6010-READ-T6634.
	* </pre>
	*/
protected void readTr3846010()
	{
		/*  Get the Letter type from T6634.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmnaIO.getChdrcoy());
		/* MOVE T6634                      TO ITEM-ITEMTABL.            */
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from contract type & transaction code.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmnaIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatcKey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* MOVE ITEM-GENAREA               TO T6634-T6634-REC.          */
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF  T6634-LETTER-TYPE           = SPACES                     */
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/*  Write LETC via LETRQST.*/
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.          */
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.rdocpfx.set(fsupfxcpy.chdr);
		letrqstrec.rdoccoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.branch.set(chdrmnaIO.getCntbranch());
		letrqstrec.otherKeys.set(SPACES);
		/* STRING                          CHDRMNA-CHDRNUM*/
		/*                                 WSKY-BATC-BATCTRCDE*/
		/* DELIMITED BY                    SPACE*/
		/* INTO                            LETRQST-OTHER-KEYS.*/
		wsaaOtherKeys.set(SPACES);
		wsaaChdrnum.set(chdrmnaIO.getChdrnum());
		wsaaLanguage.set(wsspcomn.language);
		letrqstrec.otherKeys.set(wsaaOtherKeys);
		letrqstrec.function.set("ADD");
		/* IF  T6634-LETTER-TYPE       NOT = SPACES                     */
		if (isNE(tr384rec.letterType, SPACES)) {
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz, varcom.oK)) {
				syserrrec.params.set(letrqstrec.params);
				syserrrec.statuz.set(letrqstrec.statuz);
				fatalError600();
			}
		}
	}
}
