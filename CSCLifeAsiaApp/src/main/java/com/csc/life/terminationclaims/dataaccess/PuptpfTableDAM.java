package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PuptpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:11
 * Class transformed from PUPTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PuptpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 111;
	public FixedLengthStringData puptrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData puptpfRecord = puptrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(puptrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(puptrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(puptrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(puptrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(puptrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(puptrec);
	public FixedLengthStringData pumeth = DD.pumeth.copy().isAPartOf(puptrec);
	public PackedDecimalData newsuma = DD.newsuma.copy().isAPartOf(puptrec);
	public PackedDecimalData fundAmount = DD.fundamnt.copy().isAPartOf(puptrec);
	public PackedDecimalData pupfee = DD.pupfee.copy().isAPartOf(puptrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(puptrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(puptrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(puptrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(puptrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(puptrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(puptrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(puptrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PuptpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PuptpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PuptpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PuptpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PuptpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PuptpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PuptpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PUPTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"PUMETH, " +
							"NEWSUMA, " +
							"FUNDAMNT, " +
							"PUPFEE, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     pumeth,
                                     newsuma,
                                     fundAmount,
                                     pupfee,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		pumeth.clear();
  		newsuma.clear();
  		fundAmount.clear();
  		pupfee.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPuptrec() {
  		return puptrec;
	}

	public FixedLengthStringData getPuptpfRecord() {
  		return puptpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPuptrec(what);
	}

	public void setPuptrec(Object what) {
  		this.puptrec.set(what);
	}

	public void setPuptpfRecord(Object what) {
  		this.puptpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(puptrec.getLength());
		result.set(puptrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}