package com.csc.life.terminationclaims.dataaccess.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name = "ZHLCPF", schema = "VM1DTA")
public class Zhlcpf implements java.io.Serializable {
	@Id
	
	
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private String crtable;
	private String validflag;
	private int tranno;
	private int trdt;
	private int trtm;
	private int usr;
	private BigDecimal actvalue;
	private BigDecimal zclmadjst;
	private BigDecimal zhldclmv;
	private BigDecimal zhldclma;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;
	

	public Zhlcpf(){
	}
	
	public Zhlcpf(String chdrcoy, String chdrnum, String life, String jlife, String coverage, String rider, String crtable, String validflag, int tranno, int trdt, int trtm,
			int usr, BigDecimal actvalue, BigDecimal zclmadjst, BigDecimal zhldclmv, BigDecimal zhldclma, String usrprf, String jobnm, Timestamp datime){
	
	this.chdrcoy = chdrcoy;
	this.chdrnum = chdrnum;
	this.life = life;
	this.jlife = jlife;
	this.coverage = coverage;
	this.rider = rider;
	this.crtable = crtable;
	this.validflag = validflag;
	this.tranno = tranno;
	this.trdt = trdt;
	this.trtm = trtm;
	this.usr = usr;
	this.actvalue = actvalue;
	this.zclmadjst = zclmadjst;
	this.zhldclmv = zhldclmv;
	this.zhldclma = zhldclma;
	this.usrprf = usrprf;
	this.jobnm = jobnm;
	this.datime = new Timestamp(datime.getTime());//IJTI-314
	}
	
	/*public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}*/
	
	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	

	public String getLife(){
		return life;
	}
	public void setLife(String life){
		this.life = life;
	}
	
	public String getJlife(){
		return jlife;
	}
	public void setJlife(String jlife){
		this.jlife = jlife;
	}
	
	
	public String getCoverage(){
		return coverage;
	}
	public void setCoverage(String coverage){
		this.coverage = coverage;
	}
	
	public String getRider(){
		return rider;
	}
	public void setRider(String rider){
		this.rider = rider;
	}
	
	
	public String getCrtable(){
		return crtable;
	}
	public void setCrtable(String crtable){
		this.crtable = crtable;
	}
	
	
	public String getValidflag(){
		return validflag;
	}
	public void setValidflag(String validflag){
		this.validflag = validflag;
	}
	
	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	
	
	public int getTrdt() {
		return trdt;
	}

	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}
	
	
	public int getTrtm() {
		return trtm;
	}

	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}
	
	
	public int getUsr() {
		return usr;
	}

	public void setUsr(int usr) {
		this.usr = usr;
	}
	
	
	public BigDecimal getActvalue() {
		return actvalue;
	}

	public void setActvalue(BigDecimal actvalue) {
		this.actvalue = actvalue;
	}
	
	
	public BigDecimal getZclmadjst() {
		return zclmadjst;
	}

	public void setZclmadjst(BigDecimal zclmadjst) {
		this.zclmadjst = zclmadjst;
	}
	
	
	public BigDecimal getZhldclmv() {
		return zhldclmv;
	}

	public void setZhldclmv(BigDecimal zhldclmv) {
		this.zhldclmv = zhldclmv;
	}
	
	
	public BigDecimal getZhldclma() {
		return zhldclma;
	}

	public void setZhldclma(BigDecimal zhldclma) {
		this.zhldclma = zhldclma;
	}
	

	public String getUsrprf() {
		return this.usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	
	
	public String getJobnm() {
		return this.jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	
	
public Timestamp getDatime() {
		return new Timestamp(this.datime.getTime());//IJTI-316
	}

	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}

	
	public String toString() {

		return "Zhlcpf info::: " + 
		chdrcoy+":"+
		chdrnum+":"+
		life+":"+
		jlife+":"+
		coverage+":"+
        rider+":"+
        crtable+":"+
        validflag+":"+
        tranno+":"+
        trdt+":"+
        trtm+":"+
        usr+":"+
        actvalue+":"+
        zclmadjst+":"+
        zhldclmv+":"+
        zhldclma+":"+
        usrprf+":"+
        jobnm+":" +
        datime;

	}
	
	
	
}
