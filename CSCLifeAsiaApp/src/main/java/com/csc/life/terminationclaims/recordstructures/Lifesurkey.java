package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:09
 * Description:
 * Copybook name: LIFESURKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifesurkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifesurFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifesurKey = new FixedLengthStringData(256).isAPartOf(lifesurFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifesurChdrcoy = new FixedLengthStringData(1).isAPartOf(lifesurKey, 0);
  	public FixedLengthStringData lifesurChdrnum = new FixedLengthStringData(8).isAPartOf(lifesurKey, 1);
  	public FixedLengthStringData lifesurLife = new FixedLengthStringData(2).isAPartOf(lifesurKey, 9);
  	public FixedLengthStringData lifesurJlife = new FixedLengthStringData(2).isAPartOf(lifesurKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(lifesurKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifesurFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifesurFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}