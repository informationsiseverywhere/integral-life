package com.csc.life.terminationclaims.dataaccess.dao;


import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Matdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MatdpfDAO extends BaseDAO<Matdpf> {
	
	public boolean insertMatdpf(List<Matdpf> matdpfList);
	public List<Matdpf> getMatdRecord(Integer chdrcoy,String chdrnum, Integer tranno);
	public List<Matdpf> getmatdclmListRecord(String chdrcoy,String chdrnum, int tranno,int planSuffix);
	public List<Matdpf> getmatdClmRecord(String chdrcoy, String chdrnum);
	public boolean insertMatdpfRecord(Matdpf matdpf) ;
}
