/*
 * File: Ph539.java
 * Date: 30 August 2009 1:06:49
 * Author: Quipoz Limited
 *
 * Class transformed from PH539.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.regularprocessing.tablestructures.T5399rec;
import com.csc.life.terminationclaims.procedures.Calcfee;
import com.csc.life.terminationclaims.screens.Sh539ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.life.terminationclaims.tablestructures.Th584rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This is the transaction screen for ETI Processing and
* quotation. The lower part of the screen lists the
* components that are eligible for the ETI Non-forfeiture option.
*
* The items shown for each coverage include the existing and
* the new Sum Insured, the total Surrender Value, the amount
* to be refunded, if any, the length of the Extended Term in
* terms of Years and Days and the old risk cessation date
* before ETI and the new risk cessation date after ETI.
*
*
* Initialise
*     - Preload Frequently Used Tables
*     - Prepare Header section
*     - Prepare Subfile section
*       - evaluate the contract in-force period and determine
*         the automatic premium non-forfeiture rule applicable
*         for each component.
*       - calculate no of overdue days
*       - calculate the new values for coverage impacted
*         (new SI, surrender value, new risk cessation date,
*          extended term in terms of years and days, cash
*          value refund)
*       - write a subfile line for each eligible component
*     - Preload confirmation message
*
* Processing
*     - if it is an online quotation, exit program.
*     - Otherwise, update the coverage and riders with new values
*       and stati (both premium status and risk status default
*       from the applicable non-forfeiture rule)
*       - if surrender allow in the first in-force year,
*         non-forfeiture processing can be default to contract
*         level in case of no special component level non-
*         forfeiture definition on that rider and absense of
*         cash dividend detail for that component.
*     - lapse the existing paid-up addition of the processing
*       components
*     - remove corresponding contract underwriting record if any.
*     - write letter request control record optionally.
*
*
*****************************************************************
* </pre>
*/
public class Ph539 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH539");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSystemDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaTr386Id = new FixedLengthStringData(3).isAPartOf(wsaaTr386Key, 6);

	private FixedLengthStringData wsaaCovrStatusTable = new FixedLengthStringData(192);
	private FixedLengthStringData[] wsaaCovrRec = FLSArrayPartOfStructure(48, 4, wsaaCovrStatusTable, 0);
	private FixedLengthStringData[] wsaaCovrRist = FLSDArrayPartOfArrayStructure(2, wsaaCovrRec, 0);
	private FixedLengthStringData[] wsaaCovrPrst = FLSDArrayPartOfArrayStructure(2, wsaaCovrRec, 2);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIfMonths = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaOverdueDays = new ZonedDecimalData(3, 0).setUnsigned();
	private String wsaaFound = "";

	private FixedLengthStringData wsaaCovrRskMatch = new FixedLengthStringData(1).init("N");
	private Validator covrRskMatch = new Validator(wsaaCovrRskMatch, "Y");

	private FixedLengthStringData wsaaCovrPrmMatch = new FixedLengthStringData(1).init("N");
	private Validator covrPrmMatch = new Validator(wsaaCovrPrmMatch, "Y");

	private FixedLengthStringData wsaaEstmOn = new FixedLengthStringData(1).init(SPACES);
	private Validator estmStatuz = new Validator(wsaaEstmOn, "Y");
		/* WSAA-T6597 */
	private FixedLengthStringData wsaaT6597Subr = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6597Rstat = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT6597Pstat = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT6597LapsSubr = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6597LapsRstat = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaT6597LapsPstat = new FixedLengthStringData(2);
	//private static final int wsaaT5399Size = 100;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5399Size = 1000;

	private FixedLengthStringData wsaaT5399Key2 = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaAuthCode = new FixedLengthStringData(4).isAPartOf(wsaaT5399Key2, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5399Key2, 4);
	private FixedLengthStringData wsaaTh506Nfo = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaTh584Method = new FixedLengthStringData(4);
		/* WSAA-ETI */
	private ZonedDecimalData wsaaTotEtiYears = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaTotEtiDays = new ZonedDecimalData(3, 0).setUnsigned();
	//private static final int wsaaT5687Size = 500;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5687Size = 1000;
	private PackedDecimalData wsaaT5687IxMax = new PackedDecimalData(5, 0);

		/* WSAA-T5687-ARRAY
		   03  WSAA-T5687-REC          OCCURS 200                       */
	private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray (1000, 21);
	private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 0, SPACES);
	private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0);
	private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(17, wsaaT5687Rec, 4);
	private FixedLengthStringData[] wsaaT5687NonForMethod = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 0);
	private PackedDecimalData[] wsaaT5687Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5687Data, 4);
	private FixedLengthStringData[] wsaaT5687Bbmeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 9);
	private FixedLengthStringData[] wsaaT5687Pumeth = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 13);
	//private static final int wsaaT6647Size = 500;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6647Size = 1000;
	private PackedDecimalData wsaaT6647IxMax = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaT6647Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6647AuthCode = new FixedLengthStringData(4).isAPartOf(wsaaT6647Item, 0);

		/* WSAA-T6647-ARRAY */
	private FixedLengthStringData[] wsaaT6647Rec = FLSInittedArray (1000, 19);
	private FixedLengthStringData[] wsaaT6647Key = FLSDArrayPartOfArrayStructure(8, wsaaT6647Rec, 0, SPACES);
	private FixedLengthStringData[] wsaaT6647Batctrcde = FLSDArrayPartOfArrayStructure(4, wsaaT6647Key, 0);
	private FixedLengthStringData[] wsaaT6647Cnttype = FLSDArrayPartOfArrayStructure(4, wsaaT6647Key, 4);
	private FixedLengthStringData[] wsaaT6647Data = FLSDArrayPartOfArrayStructure(11, wsaaT6647Rec, 8);
	private PackedDecimalData[] wsaaT6647Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6647Data, 0);
	private FixedLengthStringData[] wsaaT6647Aloind = FLSDArrayPartOfArrayStructure(1, wsaaT6647Data, 5);
	private FixedLengthStringData[] wsaaT6647Efdcode = FLSDArrayPartOfArrayStructure(2, wsaaT6647Data, 6);
	private ZonedDecimalData[] wsaaT6647SeqNo = ZDArrayPartOfArrayStructure(3, 0, wsaaT6647Data, 8);

	private FixedLengthStringData wsaaTh584Keys = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTh584Nfo = new FixedLengthStringData(3).isAPartOf(wsaaTh584Keys, 0);
	private FixedLengthStringData wsaaTh584Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh584Keys, 3);

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItem, 3);

	private FixedLengthStringData wsaaOkeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkey1 = new FixedLengthStringData(1).isAPartOf(wsaaOkeys, 0);
	private ZonedDecimalData wsaaPrevTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaOkeys, 1).setUnsigned();

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);
	private static final String f781 = "F781";
	private static final String g381 = "G381";
	private static final String h791 = "H791";
	private static final String hl38 = "HL38";
	private static final String stnf = "STNF";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String cltsrec = "CLTSREC";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String descrec = "DESCREC";
	private static final String hcsdrec = "HCSDREC";
	private static final String hpuarec = "HPUAREC";
	private static final String itemrec = "ITEMREC";
	private static final String lifemjarec = "LIFEMJAREC";
	private static final String payrrec = "PAYRREC";
	private static final String ptrnrec = "PTRNREC";
		/*    COPY DRYPRCREC.                                      <DRYAPL>
		    COPY DRYPRCLNK.                                      <DRYAPL>*/
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private IntegerData wsaaCovrIx = new IntegerData();
	private IntegerData wsaaT5399Ix = new IntegerData();
	private IntegerData wsaaT5399CrskIx = new IntegerData();
	private IntegerData wsaaT5399CprmIx = new IntegerData();
	private IntegerData wsaaT5399SrskIx = new IntegerData();
	private IntegerData wsaaT5399SprmIx = new IntegerData();
	private IntegerData wsaaT5687Ix = new IntegerData();
	private IntegerData wsaaT6647Ix = new IntegerData();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Batckey wsaaBatckey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private T5399rec t5399rec = new T5399rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6597rec t6597rec = new T6597rec();
	private T6647rec t6647rec = new T6647rec();
	private Th584rec th584rec = new Th584rec();
	private Th506rec th506rec = new Th506rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Tr386rec tr386rec = new Tr386rec();
	private T7508rec t7508rec = new T7508rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Sh539ScreenVars sv = ScreenProgram.getScreenVars( Sh539ScreenVars.class);
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaT5399ArrayInner wsaaT5399ArrayInner = new WsaaT5399ArrayInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		subfile1080,
		nextCovrmja1280,
		exit1290,
		nextCovrmja3180,
		exit3190,
		exit3690,
		a200Delet
	}

	public Ph539() {
		super();
		screenVars = sv;
		new ScreenModel("Sh539", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
				case subfile1080:
					subfile1080();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*    Access today's date using DATCON1.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		wsaaTime.set(getCobolTime());
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("SH539", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.crrcd.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.crrcd.set(ZERO);
		wsaaTotEtiYears.set(ZERO);
		wsaaTotEtiDays.set(ZERO);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		/*  Load all the products for overdue from T5399.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5399);
		itemIO.setItemitem(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL");

		wsaaT5399Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT53991550();
		}

		/*    Load unit linked contract details from T6647.*/
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		itdmIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItempfx("IT");
		wsaaT6647Ix.set(1);
		while ( !(isEQ(itdmIO.getStatuz(), varcom.endp))) {
			loadT66471100();
		}

		/*    Load the non-forfeiture methods from T5687.*/
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL");

		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItempfx("IT");
		wsaaT5687Ix.set(1);
		while ( !(isEQ(itdmIO.getStatuz(), varcom.endp))) {
			loadT56871200();
		}

		/* Retrieve Contract Header details which have been stored in*/
		/* the CHDRMJA I/O MODULE and load the corresponding screen fields*/
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		wsaaItemCnttype.set(chdrmjaIO.getCnttype());
		/* Read the PAYR file to retrieve the Billing information*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		payrIO.setChdrnum(chdrmjaIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/*    Set screen fields*/
		sv.ptdate.set(chdrmjaIO.getPtdate());
		sv.btdate.set(chdrmjaIO.getBtdate());
		sv.billfreq.set(chdrmjaIO.getBillfreq());
		sv.register.set(chdrmjaIO.getRegister());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.crrcd.set(chdrmjaIO.getOccdate());
		sv.mop.set(payrIO.getBillchnl());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/* Get Billing Frequency description from T3590*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3590);
		descIO.setDescitem(chdrmjaIO.getBillfreq());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.bilfrqdesc.fill("?");
		}
		else {
			sv.bilfrqdesc.set(descIO.getLongdesc());
		}
		/* Load the First-Life details to screen*/
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setLife("01");
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for existance of JOINT-LIFE Details.*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFormat(lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			sv.jlife.set(SPACES);
			sv.jlifename.set(SPACES);
			goTo(GotoLabel.subfile1080);
		}
		sv.jlife.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		else {
			plainname();
			sv.jlifename.set(wsspcomn.longconfname);
		}
	}

protected void subfile1080()
	{
		/* Read TH506 to obtain NFO*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.setItemtabl(tablesInner.th506);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		/*    MOVE TH506-HNFOPT-01        TO WSAA-TH506-NFO.               */
		wsaaTh506Nfo.set(th506rec.znfopt01);
		/*    IF TH506-HNFOPT-01          = SPACES                         */
		if (isEQ(th506rec.znfopt01, SPACES)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* Calculate in-force duration*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrmjaIO.getOccdate());
		datcon3rec.intDate2.set(chdrmjaIO.getPtdate());
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaIfMonths.set(datcon3rec.freqFactor);
		/* Set up COVRMJA key from CHDRMJA*/
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setLife(SPACES);
		covrmjaIO.setCoverage(SPACES);
		covrmjaIO.setRider(SPACES);
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		/*    Load first page of subfile*/
		wsaaEstmOn.set(SPACES);
		/* PERFORM  1250-LOAD-SUBFILE UNTIL COVRMJA-STATUZ = ENDP OR    */
		/*                            WSAA-COUNT = SH539-SUBFILE-PAGE.  */
		while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
			loadSubfile1250();
		}

		/* Set confirmation message*/
		/*--- Read TR386 table to get screen literals                      */
		itemIO.setFormat(itemrec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		wsaaTr386Id.set(SPACES);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		if (isEQ(wsspcomn.flag, "I")) {
			/*      MOVE TR386-PROGDESC-1   TO SH539-TRANDSC                 */
			sv.trandsc.set(tr386rec.progdesc01);
			/*****     MOVE END-QUO-MSG        TO SH539-TRANDSC                 */
		}
		else {
			if (isEQ(wsaaTotEtiYears, ZERO)
			&& isEQ(wsaaTotEtiDays, ZERO)) {
				/*         MOVE TR386-PROGDESC-2  TO SH539-TRANDSC              */
				sv.trandsc.set(tr386rec.progdesc02);
				/*****         MOVE ABD-ETI-MSG    TO SH539-TRANDSC                 */
			}
			else {
				/*          MOVE TR386-PROGDESC-3  TO SH539-TRANDSC              */
				sv.trandsc.set(tr386rec.progdesc03);
				/*****         MOVE PRO-ETI-MSG    TO SH539-TRANDSC                 */
			}
		}
		scrnparams.subfileRrn.set(1);
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	* </pre>
	*/
protected void loadSubfile1250()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					startLoad1250();
				case nextCovrmja1280:
					nextCovrmja1280();
				case exit1290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void startLoad1250()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			covrmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.oK)) {
			if (isEQ(th506rec.yearInforce, 0)) {
				readHcsd1300();
				if (isEQ(hcsdIO.getStatuz(), varcom.mrnf)) {
					goTo(GotoLabel.nextCovrmja1280);
				}
			}
			/*     ELSE*/
			readTh5841400();
			if (isEQ(wsaaTh584Method, SPACES)) {
				syserrrec.statuz.set(hl38);
				fatalError600();
			}
			readT65971500();
			setupSubfile1600();
			setupOvrduerec1720();
			ovrduerec.function.set("OLNPT");
			callProgram(wsaaT6597Subr, ovrduerec.ovrdueRec);
			/*         IF OVRD-STATUZ  = O-K                                */
			/*            MOVE OVRD-NEW-SUMINS      TO ZRDP-AMOUNT-IN       */
			/*            PERFORM B000-CALL-ROUNDING                        */
			/*            MOVE ZRDP-AMOUNT-OUT      TO OVRD-NEW-SUMINS      */
			/*            MOVE OVRD-SURRENDER-VALUE TO ZRDP-AMOUNT-IN       */
			/*            PERFORM B000-CALL-ROUNDING                        */
			/*            MOVE ZRDP-AMOUNT-OUT      TO OVRD-SURRENDER-VALUE */
			/*            MOVE OVRD-CV-REFUND       TO ZRDP-AMOUNT-IN       */
			/*            PERFORM B000-CALL-ROUNDING                        */
			/*            MOVE ZRDP-AMOUNT-OUT      TO OVRD-CV-REFUND       */
			/*         END-IF                                               */
			if (isEQ(ovrduerec.statuz, varcom.oK)
			|| isEQ(ovrduerec.statuz, "ESTM")) {
				if (isNE(ovrduerec.newSumins, 0)) {
					zrdecplrec.amountIn.set(ovrduerec.newSumins);
					b000CallRounding();
					ovrduerec.newSumins.set(zrdecplrec.amountOut);
				}
				if (isNE(ovrduerec.surrenderValue, 0)) {
					zrdecplrec.amountIn.set(ovrduerec.surrenderValue);
					b000CallRounding();
					ovrduerec.surrenderValue.set(zrdecplrec.amountOut);
				}
				if (isNE(ovrduerec.cvRefund, 0)) {
					zrdecplrec.amountIn.set(ovrduerec.cvRefund);
					b000CallRounding();
					ovrduerec.cvRefund.set(zrdecplrec.amountOut);
				}
				sv.hetinsi.set(ovrduerec.newSumins);
				sv.hsurval.set(ovrduerec.surrenderValue);
				sv.etiyear.set(ovrduerec.etiYears);
				sv.etidays.set(ovrduerec.etiDays);
				sv.hrefval.set(ovrduerec.cvRefund);
				sv.zrdate.set(ovrduerec.newRiskCessDate);
				wsaaTotEtiYears.add(ovrduerec.etiYears);
				wsaaTotEtiDays.add(ovrduerec.etiDays);
				if (isEQ(ovrduerec.statuz, "ESTM")) {
					wsaaEstmOn.set("Y");
				}
			}
			else {
				if (isEQ(ovrduerec.statuz, "LAPS")
				|| isEQ(ovrduerec.statuz, "OMIT")) {
					if (isNE(ovrduerec.surrenderValue, 0)) {
						zrdecplrec.amountIn.set(ovrduerec.surrenderValue);
						b000CallRounding();
						ovrduerec.surrenderValue.set(zrdecplrec.amountOut);
					}
					if (isNE(ovrduerec.cvRefund, 0)) {
						zrdecplrec.amountIn.set(ovrduerec.cvRefund);
						b000CallRounding();
						ovrduerec.cvRefund.set(zrdecplrec.amountOut);
					}
					sv.hetinsi.set(covrmjaIO.getVarSumInsured());
					sv.hsurval.set(ovrduerec.surrenderValue);
					sv.etiyear.set(ovrduerec.etiYears);
					sv.etidays.set(ovrduerec.etiDays);
					sv.hrefval.set(ovrduerec.cvRefund);
					sv.zrdate.set(ovrduerec.newRiskCessDate);
				}
				else {
					syserrrec.statuz.set(ovrduerec.statuz);
					fatalError600();
				}
			}
			writeSubfile1800();
			/*****     END-IF*/
		}
	}

protected void nextCovrmja1280()
	{
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void readHcsd1300()
	{
		start1310();
	}

protected void start1310()
	{
		/*  Read HCSD file using key information from SURD record.*/
		hcsdIO.setParams(SPACES);
		hcsdIO.setChdrcoy(covrmjaIO.getChdrcoy());
		hcsdIO.setChdrnum(covrmjaIO.getChdrnum());
		hcsdIO.setLife(covrmjaIO.getLife());
		hcsdIO.setCoverage(covrmjaIO.getCoverage());
		hcsdIO.setRider(covrmjaIO.getRider());
		hcsdIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)
		&& isNE(hcsdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			fatalError600();
		}
	}

protected void readTh5841400()
	{
		start1410();
	}

protected void start1410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.th584);
		wsaaTh584Nfo.set(wsaaTh506Nfo);
		wsaaTh584Crtable.set(covrmjaIO.getCrtable());
		itemIO.setItemitem(wsaaTh584Keys);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			th584rec.th584Rec.set(itemIO.getGenarea());
			wsaaTh584Method.set(th584rec.nonForfeitMethod);
		}
	}

protected void readT65971500()
	{
		start1510();
	}

protected void start1510()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(tablesInner.t6597);
		itdmIO.setItemitem(wsaaTh584Method);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemtabl(), tablesInner.t6597)
		|| isNE(wsaaTh584Method, itdmIO.getItemitem())
		|| isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaTh584Method);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g381);
			fatalError600();
		}
		t6597rec.t6597Rec.set(itdmIO.getGenarea());
		wsaaFound = "N";
		wsaaT6597Subr.set(SPACES);
		wsaaT6597LapsSubr.set(SPACES);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 3)
		|| isEQ(wsaaFound, "Y")); wsaaSub.add(1)){
			if (isLTE(wsaaIfMonths, t6597rec.durmnth[wsaaSub.toInt()])) {
				wsaaT6597Subr.set(t6597rec.premsubr[wsaaSub.toInt()]);
				wsaaT6597Rstat.set(t6597rec.crstat[wsaaSub.toInt()]);
				wsaaT6597Pstat.set(t6597rec.cpstat[wsaaSub.toInt()]);
				wsaaFound = "Y";
			}
		}
		wsaaT6597LapsSubr.set(t6597rec.premsubr04);
		wsaaT6597LapsRstat.set(t6597rec.crstat04);
		wsaaT6597LapsPstat.set(t6597rec.cpstat04);
		if (isNE(wsaaFound, "Y")
		|| isEQ(wsaaT6597Subr, SPACES)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(g381);
			fatalError600();
		}
	}

protected void loadT53991550()
	{
		t53991551();
	}

protected void t53991551()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getItemtabl(), tablesInner.t5399)
		|| isNE(itemIO.getItemcoy(), wsspcomn.company)
		|| isEQ(itemIO.getStatuz(), varcom.endp)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		if (isGT(wsaaT5399Ix, wsaaT5399Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t5399);
			fatalError600();
		}
		t5399rec.t5399Rec.set(itemIO.getGenarea());
		wsaaT5399ArrayInner.wsaaT5399Key[wsaaT5399Ix.toInt()].set(itemIO.getItemitem());
		wsaaT5399ArrayInner.wsaaT5399RiskStats[wsaaT5399Ix.toInt()].set(t5399rec.covRiskStats);
		wsaaT5399ArrayInner.wsaaT5399PremStats[wsaaT5399Ix.toInt()].set(t5399rec.covPremStats);
		wsaaT5399ArrayInner.wsaaT5399SetRiskStats[wsaaT5399Ix.toInt()].set(t5399rec.setCnRiskStats);
		wsaaT5399ArrayInner.wsaaT5399SetPremStats[wsaaT5399Ix.toInt()].set(t5399rec.setCnPremStats);
		itemIO.setFunction(varcom.nextr);
		wsaaT5399Ix.add(1);
	}

protected void setupSubfile1600()
	{
		/*START*/
		sv.life.set(covrmjaIO.getLife());
		sv.coverage.set(covrmjaIO.getCoverage());
		sv.rider.set(covrmjaIO.getRider());
		if (isEQ(covrmjaIO.getVarSumInsured(), ZERO)) {
			sv.hetiosi.set(covrmjaIO.getSumins());
		}
		else {
			sv.hetiosi.set(covrmjaIO.getVarSumInsured());
		}
		sv.riskCessDate.set(covrmjaIO.getRiskCessDate());
		/*EXIT*/
	}

protected void setupOvrduerec1720()
	{
		start1720();
	}

protected void start1720()
	{
		ovrduerec.ovrdueRec.set(SPACES);
		ovrduerec.newPremCessDate.set(ZERO);
		ovrduerec.newRiskCessDate.set(ZERO);
		ovrduerec.newRerateDate.set(ZERO);
		ovrduerec.cvRefund.set(ZERO);
		ovrduerec.surrenderValue.set(ZERO);
		ovrduerec.etiYears.set(ZERO);
		ovrduerec.etiDays.set(ZERO);
		ovrduerec.newBonusDate.set(ZERO);
		ovrduerec.riskCessDate.set(ZERO);
		searchT66471040();
		searchT56871050();
		ovrduerec.language.set(wsspcomn.language);
		ovrduerec.chdrcoy.set(covrmjaIO.getChdrcoy());
		ovrduerec.chdrnum.set(covrmjaIO.getChdrnum());
		ovrduerec.life.set(covrmjaIO.getLife());
		ovrduerec.coverage.set(covrmjaIO.getCoverage());
		ovrduerec.rider.set(covrmjaIO.getRider());
		ovrduerec.riskCessDate.set(covrmjaIO.getRiskCessDate());
		ovrduerec.planSuffix.set(ZERO);
		ovrduerec.tranno.set(chdrmjaIO.getTranno());
		ovrduerec.tranno.add(1);
		ovrduerec.cntcurr.set(payrIO.getCntcurr());
		ovrduerec.effdate.set(wsaaToday);
		ovrduerec.outstamt.set(payrIO.getOutstamt());
		ovrduerec.ptdate.set(payrIO.getPtdate());
		ovrduerec.btdate.set(payrIO.getBtdate());
		ovrduerec.ovrdueDays.set(wsaaOverdueDays);
		ovrduerec.agntnum.set(chdrmjaIO.getAgntnum());
		ovrduerec.cownnum.set(chdrmjaIO.getCownnum());
		ovrduerec.trancode.set(wsaaBatckey.batcBatctrcde);
		ovrduerec.acctyear.set(wsaaBatckey.batcBatcactyr);
		ovrduerec.acctmonth.set(wsaaBatckey.batcBatcactmn);
		ovrduerec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ovrduerec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		ovrduerec.user.set(payrIO.getUser());
		ovrduerec.company.set(chdrmjaIO.getCowncoy());
		ovrduerec.tranDate.set(wsaaToday);
		ovrduerec.tranTime.set(wsaaTime);
		ovrduerec.termid.set(varcom.vrcmTermid);
		ovrduerec.crtable.set(covrmjaIO.getCrtable());
		ovrduerec.pumeth.set(wsaaT5687Pumeth[wsaaT5687Ix.toInt()]);
		ovrduerec.billfreq.set(payrIO.getBillfreq());
		ovrduerec.instprem.set(ZERO);
		ovrduerec.sumins.set(ZERO);
		ovrduerec.crrcd.set(ZERO);
		ovrduerec.newSumins.set(ZERO);
		ovrduerec.premCessDate.set(ZERO);
		ovrduerec.cnttype.set(chdrmjaIO.getCnttype());
		ovrduerec.occdate.set(chdrmjaIO.getOccdate());
		ovrduerec.polsum.set(chdrmjaIO.getPolsum());
		/*    Call the CALCFEE subroutine to calculate OVRD-PUPFEE.*/
		callProgram(Calcfee.class, ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz, varcom.oK)) {
			syserrrec.params.set(ovrduerec.ovrdueRec);
			syserrrec.statuz.set(ovrduerec.statuz);
			fatalError600();
		}
		ovrduerec.planSuffix.set(covrmjaIO.getPlanSuffix());
		ovrduerec.instprem.set(covrmjaIO.getInstprem());
		ovrduerec.sumins.set(covrmjaIO.getSumins());
		ovrduerec.crrcd.set(covrmjaIO.getCrrcd());
		ovrduerec.premCessDate.set(covrmjaIO.getPremCessDate());
	}

protected void callLapsSubr1750()
	{
		/*START*/
		setupOvrduerec1720();
		ovrduerec.function.set(SPACES);
		callProgram(wsaaT6597LapsSubr, ovrduerec.ovrdueRec);
		/*EXIT*/
	}

protected void writeSubfile1800()
	{
		/*WRITE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("SH539", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void searchT66471040()
	{
		/*T6647*/
		/*    Search table T6647 to find correct dated item.*/
		for (wsaaT6647Ix.set(wsaaT6647IxMax); !(isEQ(wsaaT6647Ix, 0)
		|| (isEQ(chdrmjaIO.getCnttype(), wsaaT6647Cnttype[wsaaT6647Ix.toInt()])
		&& isGTE(chdrmjaIO.getOccdate(), wsaaT6647Itmfrm[wsaaT6647Ix.toInt()]))); wsaaT6647Ix.add(-1))
{
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsaaT6647Ix, 0)) {
			ovrduerec.aloind.set(SPACES);
			ovrduerec.efdcode.set(SPACES);
			ovrduerec.procSeqNo.set(ZERO);
		}
		else {
			ovrduerec.aloind.set(wsaaT6647Aloind[wsaaT6647Ix.toInt()]);
			ovrduerec.efdcode.set(wsaaT6647Efdcode[wsaaT6647Ix.toInt()]);
			ovrduerec.procSeqNo.set(wsaaT6647SeqNo[wsaaT6647Ix.toInt()]);
		}
		/*EXIT*/
	}

protected void searchT56871050()
	{
		t56871050();
		calc1060();
	}

protected void t56871050()
	{
		for (wsaaT5687Ix.set(wsaaT5687IxMax); !(isEQ(wsaaT5687Ix, 0)
		|| (isEQ(wsaaT5687Crtable[wsaaT5687Ix.toInt()], covrmjaIO.getCrtable())
		&& isGTE(wsaaToday, wsaaT5687Itmfrm[wsaaT5687Ix.toInt()]))); wsaaT5687Ix.add(-1))
{
			/*CONTINUE_STMT*/
		}
		if (isEQ(wsaaT5687Ix, 0)) {
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.t5687);
			itemIO.setItemitem(covrmjaIO.getCrtable());
			itemIO.setFunction(varcom.readr);
			itemIO.setStatuz(f781);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void calc1060()
	{
		/*    Call DATCON3 to calculate the no. of overdue days.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(payrIO.getPtdate());
		datcon3rec.intDate2.set(wsaaToday);
		datcon3rec.frequency.set("DY");
		datcon3rec.freqFactor.set(0);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaOverdueDays.set(datcon3rec.freqFactor);
	}

protected void loadT66471100()
	{
		start1100();
	}

protected void start1100()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		wsaaT6647Item.set(itdmIO.getItemitem());
		if (isNE(itdmIO.getItemtabl(), tablesInner.t6647)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(wsaaT6647AuthCode, wsaaBatckey.batcBatctrcde)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
			return ;
		}
		if (isGT(wsaaT6647Ix, wsaaT6647Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t6647);
			fatalError600();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		wsaaT6647Key[wsaaT6647Ix.toInt()].set(itdmIO.getItemitem());
		wsaaT6647Itmfrm[wsaaT6647Ix.toInt()].set(itdmIO.getItmfrm());
		wsaaT6647Aloind[wsaaT6647Ix.toInt()].set(t6647rec.aloind);
		wsaaT6647Efdcode[wsaaT6647Ix.toInt()].set(t6647rec.efdcode);
		wsaaT6647SeqNo[wsaaT6647Ix.toInt()].set(t6647rec.procSeqNo);
		itdmIO.setFunction(varcom.nextr);
		wsaaT6647IxMax.set(wsaaT6647Ix);
		wsaaT6647Ix.add(1);
	}

protected void loadT56871200()
	{
		start1210();
	}

protected void start1210()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			if (isEQ(itdmIO.getFunction(), varcom.begn)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			else {
				itdmIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isGT(wsaaT5687Ix, wsaaT5687Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tablesInner.t5687);
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		wsaaT5687Crtable[wsaaT5687Ix.toInt()].set(itdmIO.getItemitem());
		wsaaT5687Itmfrm[wsaaT5687Ix.toInt()].set(itdmIO.getItmfrm());
		wsaaT5687Pumeth[wsaaT5687Ix.toInt()].set(t5687rec.pumeth);
		wsaaT5687NonForMethod[wsaaT5687Ix.toInt()].set(t5687rec.nonForfeitMethod);
		itdmIO.setFunction(varcom.nextr);
		wsaaT5687IxMax.set(wsaaT5687Ix);
		wsaaT5687Ix.add(1);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* If ETI processing routine returns 'ESTM' statuz, estimation     */
		/* only.                                                           */
		if (estmStatuz.isTrue()) {
			sv.flag.set(SPACES);
			sv.flagOut[varcom.nd.toInt()].set(SPACES);
		}
		else {
			sv.flagOut[varcom.nd.toInt()].set("Y");
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'SH539IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         SH539-DATA-AREA                         */
		/*                         SH539-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		/**    Validate fields*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SH539", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}

		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/**    Validate subfile fields*/
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SH539", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SH539", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.flag, "I")
		|| (isEQ(wsaaTotEtiYears, ZERO)
		&& isEQ(wsaaTotEtiDays, ZERO))) {
			return ;
		}
		initialize(wsaaCovrStatusTable);
		wsaaCovrIx.set(1);
		/* Set up COVRMJA key from CHDRMJA*/
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		/*    Loop coverage file to perform ETI processing.*/
		covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrmjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");

		while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
			loopCovrmja3100();
		}

		maintainChdrmjaPtrn3200();
		a200DelUnderwritting();
		writeLetter3600();
		dryProcessing8000();
	}

protected void loopCovrmja3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3100();
				case nextCovrmja3180:
					nextCovrmja3180();
				case exit3190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3100()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		//ILIFE-959, added a validation
		if ( isEQ(covrmjaIO.getStatuz(), varcom.endp)
		|| isNE(covrmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			covrmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.oK)) {
			if (isEQ(th506rec.yearInforce, 0)) {
				readHcsd1300();
				if (isEQ(hcsdIO.getStatuz(), varcom.mrnf)) {
					h100LapseProcess();
					goTo(GotoLabel.nextCovrmja3180);
				}
			}
			/*     ELSE*/
			readTh5841400();
			readT65971500();
			setupOvrduerec1720();
			ovrduerec.function.set("OLPT");
			callProgram(wsaaT6597Subr, ovrduerec.ovrdueRec);
			/*         IF OVRD-STATUZ  = O-K                                */
			/*            MOVE OVRD-NEW-SUMINS      TO ZRDP-AMOUNT-IN       */
			/*            PERFORM B000-CALL-ROUNDING                        */
			/*            MOVE ZRDP-AMOUNT-OUT      TO OVRD-NEW-SUMINS      */
			/*            MOVE OVRD-SURRENDER-VALUE TO ZRDP-AMOUNT-IN       */
			/*            PERFORM B000-CALL-ROUNDING                        */
			/*            MOVE ZRDP-AMOUNT-OUT      TO OVRD-SURRENDER-VALUE */
			/*            MOVE OVRD-CV-REFUND       TO ZRDP-AMOUNT-IN       */
			/*            PERFORM B000-CALL-ROUNDING                        */
			/*            MOVE ZRDP-AMOUNT-OUT      TO OVRD-CV-REFUND       */
			/*         END-IF                                               */
			if (isEQ(ovrduerec.statuz, varcom.oK)
			|| isEQ(ovrduerec.statuz, "ESTM")) {
				if (isNE(ovrduerec.newSumins, 0)) {
					zrdecplrec.amountIn.set(ovrduerec.newSumins);
					b000CallRounding();
					ovrduerec.newSumins.set(zrdecplrec.amountOut);
				}
				if (isNE(ovrduerec.surrenderValue, 0)) {
					zrdecplrec.amountIn.set(ovrduerec.surrenderValue);
					b000CallRounding();
					ovrduerec.surrenderValue.set(zrdecplrec.amountOut);
				}
				if (isNE(ovrduerec.cvRefund, 0)) {
					zrdecplrec.amountIn.set(ovrduerec.cvRefund);
					b000CallRounding();
					ovrduerec.cvRefund.set(zrdecplrec.amountOut);
				}
				h200BasicNewCovr();
			}
			else {
				if (isEQ(ovrduerec.statuz, "LAPS")) {
					if (isNE(ovrduerec.newSumins, 0)) {
						zrdecplrec.amountIn.set(ovrduerec.newSumins);
						b000CallRounding();
						ovrduerec.newSumins.set(zrdecplrec.amountOut);
					}
					if (isNE(ovrduerec.surrenderValue, 0)) {
						zrdecplrec.amountIn.set(ovrduerec.surrenderValue);
						b000CallRounding();
						ovrduerec.surrenderValue.set(zrdecplrec.amountOut);
					}
					if (isNE(ovrduerec.cvRefund, 0)) {
						zrdecplrec.amountIn.set(ovrduerec.cvRefund);
						b000CallRounding();
						ovrduerec.cvRefund.set(zrdecplrec.amountOut);
					}
					setupOvrduerec1720();
					callProgram(wsaaT6597LapsSubr, ovrduerec.ovrdueRec);
					h300BasicLapsNewCovr();
				}
				else {
					syserrrec.statuz.set(ovrduerec.statuz);
					fatalError600();
				}
			}
			h500UpdateHpua();
			/*****     END-IF*/
		}
	}

protected void nextCovrmja3180()
	{
		if (isGT(wsaaCovrIx, 48)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		wsaaCovrPrst[wsaaCovrIx.toInt()].set(covrmjaIO.getPstatcode());
		wsaaCovrRist[wsaaCovrIx.toInt()].set(covrmjaIO.getStatcode());
		wsaaCovrIx.add(1);
		setPrecision(covrmjaIO.getTranno(), 0);
		covrmjaIO.setTranno(add(1, chdrmjaIO.getTranno()));
		writeCovrmja5200();
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void maintainChdrmjaPtrn3200()
	{
		rewriteChdrmja3210();
		writeNewChdrmja3250();
		writePtrn3270();
	}

protected void rewriteChdrmja3210()
	{
		chdrmjaIO.setValidflag("2");
		chdrmjaIO.setCurrto(wsaaToday);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
		chdrmjaIO.setFunction(varcom.writs);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void writeNewChdrmja3250()
	{
		setT5399Status3300();
		chdrmjaIO.setValidflag("1");
		setPrecision(chdrmjaIO.getTranno(), 0);
		chdrmjaIO.setTranno(add(chdrmjaIO.getTranno(), 1));
		chdrmjaIO.setCurrfrom(wsaaToday);
		chdrmjaIO.setCurrto(varcom.vrcmMaxDate);
		chdrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void writePtrn3270()
	{
		/*    Write a new PTRN.*/
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());
		ptrnIO.setValidflag("1");
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setTermid(ovrduerec.termid);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setDatesub(wsaaToday);
		wsaaSystemDate.set(getCobolDate());
		ptrnIO.setTransactionDate(wsaaSystemDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setUser(999999);
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void setT5399Status3300()
	{
		start3310();
	}

protected void start3310()
	{
		/*    Read T5679.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Search T5399*/
		wsaaCovrRskMatch.set("N");
		wsaaCovrPrmMatch.set("N");
		wsaaAuthCode.set(wsaaBatckey.batcBatctrcde);
		wsaaCnttype.set(chdrmjaIO.getCnttype());
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5399ArrayInner.wsaaT5399Rec);
		as1.setIndices(wsaaT5399Ix);
		as1.addSearchKey(wsaaT5399ArrayInner.wsaaT5399Key, wsaaT5399Key2, true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.t5399);
			itemIO.setItemitem(wsaaT5399Key2);
			itemIO.setFunction(varcom.readr);
			itemIO.setStatuz(stnf);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*  We need to process the coverage risk statuses and premium*/
		/*  statuses in two separate parts.  Using T5399 as the driving*/
		/*  file, find the first match between risk status and coverage*/
		/*  risk status.*/
		for (wsaaT5399CrskIx.set(1); !(isGT(wsaaT5399CrskIx, 12)
		|| covrRskMatch.isTrue()); wsaaT5399CrskIx.add(1)){
			readRist3500();
		}
		/*  Use T5679 as a 'catch all' if the T5399 hierarchy does not*/
		/*  contain the coverage risk.*/
		if (!covrRskMatch.isTrue()) {
			chdrmjaIO.setStatcode(t5679rec.setCnRiskStat);
		}
		/*  Perform the same processing as above with the premium statcode.*/
		for (wsaaT5399CprmIx.set(1); !(isGT(wsaaT5399CprmIx, 12)
		|| covrPrmMatch.isTrue()); wsaaT5399CprmIx.add(1)){
			readPrst3400();
		}
		if (!covrPrmMatch.isTrue()) {
			chdrmjaIO.setPstatcode(t5679rec.setCnPremStat);
		}
	}

protected void readPrst3400()
	{
		start3410();
	}

protected void start3410()
	{
		if (isEQ(wsaaT5399ArrayInner.wsaaT5399PremStat[wsaaT5399Ix.toInt()][wsaaT5399CprmIx.toInt()], "  ")) {
			return ;
		}
		wsaaCovrIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaCovrIx, wsaaCovrRec.length); wsaaCovrIx.add(1)){
				if (isEQ(wsaaT5399ArrayInner.wsaaT5399PremStat[wsaaT5399Ix.toInt()][wsaaT5399CprmIx.toInt()], wsaaCovrPrst[wsaaCovrIx.toInt()])) {
					wsaaT5399SprmIx.set(wsaaT5399CprmIx);
					chdrmjaIO.setPstatcode(wsaaT5399ArrayInner.wsaaT5399SetPremStat[wsaaT5399Ix.toInt()][wsaaT5399SprmIx.toInt()]);
					wsaaCovrPrmMatch.set("Y");
					break searchlabel1;
				}
			}
			/*CONTINUE_STMT*/
		}
	}

protected void readRist3500()
	{
		start3510();
	}

protected void start3510()
	{
		if (isEQ(wsaaT5399ArrayInner.wsaaT5399RiskStat[wsaaT5399Ix.toInt()][wsaaT5399CrskIx.toInt()], "  ")) {
			return ;
		}
		wsaaCovrIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaCovrIx, wsaaCovrRec.length); wsaaCovrIx.add(1)){
				if (isEQ(wsaaT5399ArrayInner.wsaaT5399RiskStat[wsaaT5399Ix.toInt()][wsaaT5399CrskIx.toInt()], wsaaCovrRist[wsaaCovrIx.toInt()])) {
					wsaaT5399SrskIx.set(wsaaT5399CrskIx);
					chdrmjaIO.setStatcode(wsaaT5399ArrayInner.wsaaT5399SetRiskStat[wsaaT5399Ix.toInt()][wsaaT5399SrskIx.toInt()]);
					wsaaCovrRskMatch.set("Y");
					break searchlabel1;
				}
			}
			/*CONTINUE_STMT*/
		}
	}

protected void writeLetter3600()
	{
		try {
			readT66343610();
			setUpParm3630();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readT66343610()
	{
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(wsaaItem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			/*     PERFORM 3700-READ-T6634-AGAIN                    <PCPPRT>*/
			readTr384Again3700();
		}
		else {
			/*     MOVE ITEM-GENAREA            TO T6634-T6634-REC  <PCPPRT>*/
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
		/* IF   T6634-LETTER-TYPE          = SPACE              <PCPPRT>*/
		if (isEQ(tr384rec.letterType, SPACES)) {
			goTo(GotoLabel.exit3690);
		}
	}

	/**
	* <pre>
	*3620-GET-ENDORSE.
	*    MOVE 'NEXT '                TO ALNO-FUNCTION.          <P008>
	*    MOVE 'EN'                   TO ALNO-PREFIX.            <P008>
	*    MOVE WSKY-BATC-BATCBRN      TO ALNO-GENKEY.            <P008>
	*    MOVE WSSP-COMPANY           TO ALNO-COMPANY.           <P008>
	*    CALL 'ALOCNO' USING ALNO-ALOCNO-REC.                   <P008>
	*    IF ALNO-STATUZ              NOT = O-K                  <P008>
	*       MOVE ALNO-STATUZ         TO SYSR-STATUZ             <P008>
	*       MOVE 'ALOCNO'            TO SYSR-PARAMS             <P008>
	*       PERFORM 600-FATAL-ERROR.                            <P008>
	* </pre>
	*/
protected void setUpParm3630()
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmjaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		/*    MOVE 'EN'                   TO LETRQST-RDOCPFX.        <P008>*/
		/*    MOVE WSSP-COMPANY           TO LETRQST-RDOCCOY.        <P008>*/
		/*    MOVE ALNO-ALOC-NO           TO LETRQST-RDOCNUM.        <P008>*/
		letrqstrec.rdocpfx.set(chdrmjaIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrmjaIO.getChdrnum());
		/*   MOVE SPACE                  TO WSAA-OKEYS.                   */
		wsaaOkey1.set(wsspcomn.language);
		letrqstrec.otherKeys.set(wsaaOkeys);
		letrqstrec.clntcoy.set(chdrmjaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmjaIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmjaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmjaIO.getTranno());
		letrqstrec.branch.set(chdrmjaIO.getCntbranch());
		letrqstrec.function.set("ADD");
		/*    CALL 'HLETRQS' USING LETRQST-PARAMS.                   <P008>*/
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*3700-READ-T6634-AGAIN SECTION.                           <PCPPRT>
	* </pre>
	*/
protected void readTr384Again3700()
	{
		start3710();
	}

protected void start3710()
	{
		wsaaItemCnttype.set("***");
		wsaaItemBatctrcde.set(wsaaBatckey.batcBatctrcde);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		/* MOVE T6634                       TO ITEM-ITEMTABL.   <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(wsaaItem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		else {
			/*     MOVE ITEM-GENAREA            TO T6634-T6634-REC  <PCPPRT>*/
			tr384rec.tr384Rec.set(itemIO.getGenarea());
		}
	}

protected void h100LapseProcess()
	{
		h101Start();
	}

protected void h101Start()
	{
		readTh5841400();
		if (isEQ(wsaaTh584Method, SPACES)) {
			searchT56871050();
			wsaaTh584Method.set(wsaaT5687NonForMethod[wsaaT5687Ix.toInt()]);
		}
		readT65971500();
		setupOvrduerec1720();
		callProgram(wsaaT6597Subr, ovrduerec.ovrdueRec);
		if (isEQ(ovrduerec.statuz, varcom.oK)) {
			h400RiderNewCovr();
		}
		else {
			syserrrec.statuz.set(ovrduerec.statuz);
			fatalError600();
		}
	}

protected void h200BasicNewCovr()
	{
		h201Start();
	}

protected void h201Start()
	{
		updateCovrmja5000();
		covrmjaIO.setValidflag("1");
		setPrecision(covrmjaIO.getTranno(), 0);
		covrmjaIO.setTranno(add(covrmjaIO.getTranno(), 1));
		covrmjaIO.setCurrfrom(wsaaToday);
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setPstatcode(wsaaT6597Pstat);
		covrmjaIO.setStatcode(wsaaT6597Rstat);
		covrmjaIO.setSingp(ovrduerec.newSingp);
		if (isEQ(ovrduerec.statuz, "ESTM")) {
			return ;
		}
		covrmjaIO.setRiskCessDate(ovrduerec.newRiskCessDate);
		covrmjaIO.setPremCessDate(ovrduerec.newPremCessDate);
		covrmjaIO.setSumins(ovrduerec.newSumins);
		covrmjaIO.setVarSumInsured(ZERO);
		covrmjaIO.setRerateDate(ovrduerec.newRerateDate);
		if (isNE(ovrduerec.newBonusDate, 0)) {
			covrmjaIO.setUnitStatementDate(ovrduerec.newBonusDate);
		}
	}

protected void h300BasicLapsNewCovr()
	{
		h301Start();
	}

protected void h301Start()
	{
		updateCovrmja5000();
		covrmjaIO.setValidflag("1");
		setPrecision(covrmjaIO.getTranno(), 0);
		covrmjaIO.setTranno(add(covrmjaIO.getTranno(), 1));
		covrmjaIO.setCurrfrom(wsaaToday);
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setPstatcode(wsaaT6597LapsPstat);
		covrmjaIO.setStatcode(wsaaT6597LapsRstat);
		if (isNE(ovrduerec.newRiskCessDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newRiskCessDate, ZERO)) {
			covrmjaIO.setRiskCessDate(ovrduerec.newRiskCessDate);
		}
		if (isNE(ovrduerec.newPremCessDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newPremCessDate, ZERO)) {
			covrmjaIO.setPremCessDate(ovrduerec.newPremCessDate);
		}
	}

protected void h400RiderNewCovr()
	{
		h401Start();
	}

protected void h401Start()
	{
		updateCovrmja5000();
		covrmjaIO.setValidflag("1");
		setPrecision(covrmjaIO.getTranno(), 0);
		covrmjaIO.setTranno(add(covrmjaIO.getTranno(), 1));
		covrmjaIO.setCurrfrom(wsaaToday);
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setPstatcode(wsaaT6597Pstat);
		covrmjaIO.setStatcode(wsaaT6597Rstat);
		if (isNE(ovrduerec.newRiskCessDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newRiskCessDate, ZERO)) {
			covrmjaIO.setRiskCessDate(ovrduerec.newRiskCessDate);
		}
		if (isNE(ovrduerec.newPremCessDate, varcom.vrcmMaxDate)
		&& isNE(ovrduerec.newPremCessDate, ZERO)) {
			covrmjaIO.setPremCessDate(ovrduerec.newPremCessDate);
		}
	}

protected void h500UpdateHpua()
	{
		h501Start();
	}

protected void h501Start()
	{
		hpuaIO.setDataKey(SPACES);
		hpuaIO.setChdrcoy(covrmjaIO.getChdrcoy());
		hpuaIO.setChdrnum(covrmjaIO.getChdrnum());
		hpuaIO.setLife(covrmjaIO.getLife());
		hpuaIO.setCoverage(covrmjaIO.getCoverage());
		hpuaIO.setRider(covrmjaIO.getRider());
		hpuaIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFormat(hpuarec);
		hpuaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		hpuaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hpuaIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER","PLNSFX");

		while ( !(isEQ(hpuaIO.getStatuz(), varcom.endp))) {
			maintainHpua5100();
		}

	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void updateCovrmja5000()
	{
		update5010();
	}

protected void update5010()
	{
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(wsaaToday);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		wsaaPrevTranno.set(covrmjaIO.getTranno());
		covrmjaIO.setFunction(varcom.writs);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
	}

protected void maintainHpua5100()
	{
		start5110();
	}

protected void start5110()
	{
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)
		&& isNE(hpuaIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hpuaIO.getParams());
			fatalError600();
		}
		if (isNE(covrmjaIO.getChdrcoy(), hpuaIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), hpuaIO.getChdrnum())
		|| isNE(covrmjaIO.getLife(), hpuaIO.getLife())
		|| isNE(covrmjaIO.getCoverage(), hpuaIO.getCoverage())
		|| isNE(covrmjaIO.getRider(), hpuaIO.getRider())
		|| isNE(covrmjaIO.getPlanSuffix(), hpuaIO.getPlanSuffix())) {
			hpuaIO.setStatuz(varcom.endp);
		}
		if (isNE(hpuaIO.getStatuz(), varcom.oK)) {
			hpuaIO.setStatuz(varcom.endp);
			return ;
		}
		/* Invalidate the existing PUA records.*/
		hpuaIO.setValidflag("2");
		hpuaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpuaIO.getParams());
			fatalError600();
		}
		hpuaIO.setFunction(varcom.writs);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpuaIO.getParams());
			fatalError600();
		}
		/* Write a new PUA record to denote it is lapsed.*/
		hpuaIO.setValidflag("1");
		setPrecision(hpuaIO.getTranno(), 0);
		hpuaIO.setTranno(add(1, chdrmjaIO.getTranno()));
		hpuaIO.setPstatcode(wsaaT6597LapsPstat);
		hpuaIO.setRstatcode(wsaaT6597LapsRstat);
		hpuaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpuaIO.getParams());
			fatalError600();
		}
		setPrecision(hpuaIO.getPuAddNbr(), 0);
		hpuaIO.setPuAddNbr(add(hpuaIO.getPuAddNbr(), 1));
		hpuaIO.setFunction(varcom.begn);
	}

protected void writeCovrmja5200()
	{
		/*START*/
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrmjaIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(wsspcomn.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(chdrmjaIO.getChdrnum());
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsspcomn.fsuco);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrmjaIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrmjaIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrmjaIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrmjaIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrmjaIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrmjaIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrmjaIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypOccdate.set(chdrmjaIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypAplsupto.set(payrIO.getAplspto());
		noSource("DRYPROCES", drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void a200DelUnderwritting()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a200Ctrl();
				case a200Delet:
					a200Delet();
					a200Next();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a200Ctrl()
	{
		lifemjaIO.setRecKeyData(SPACES);
		lifemjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		lifemjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		lifemjaIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		lifemjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifemjaIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		lifemjaIO.setFormat(lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (!(isEQ(lifemjaIO.getStatuz(), varcom.oK)
		&& isEQ(chdrmjaIO.getChdrcoy(), lifemjaIO.getChdrcoy())
		&& isEQ(chdrmjaIO.getChdrnum(), lifemjaIO.getChdrnum()))) {
			lifemjaIO.setStatuz(varcom.endp);
			syserrrec.params.set(lifemjaIO.getParams());
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			fatalError600();
		}
	}

protected void a200Delet()
	{
		initialize(crtundwrec.parmRec);
		/* MOVE SH539-LIFENUM          TO UNDW-CLNTNUM.        <LFA1062>*/
		crtundwrec.clntnum.set(lifemjaIO.getLifcnum());
		crtundwrec.coy.set(chdrmjaIO.getChdrcoy());
		crtundwrec.currcode.set(chdrmjaIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrmjaIO.getChdrnum());
		crtundwrec.cnttyp.set(chdrmjaIO.getCnttype());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}
	}

protected void a200Next()
	{
		lifemjaIO.setFunction(varcom.nextr);
		lifemjaIO.setFormat(lifemjarec);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)) {
			return ;
		}
		if (isEQ(chdrmjaIO.getChdrcoy(), lifemjaIO.getChdrcoy())
		&& isEQ(chdrmjaIO.getChdrnum(), lifemjaIO.getChdrnum())) {
			goTo(GotoLabel.a200Delet);
		}
		/*A200-EXIT*/
	}

protected void b000CallRounding()
	{
		/*B100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.cntcurr);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*B900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-T5399-ARRAY--INNER
 */
private static final class WsaaT5399ArrayInner {

		/* WSAA-T5399-ARRAY */
	private FixedLengthStringData[] wsaaT5399Rec = FLSInittedArray (230, 103);//IBPLIFE-3150
	private FixedLengthStringData[] wsaaT5399Key = FLSDArrayPartOfArrayStructure(7, wsaaT5399Rec, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT5399AuthCode = FLSDArrayPartOfArrayStructure(4, wsaaT5399Key, 0);
	private FixedLengthStringData[] wsaaT5399Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5399Key, 4);
	private FixedLengthStringData[] wsaaT5399Data = FLSDArrayPartOfArrayStructure(96, wsaaT5399Rec, 7);
	private FixedLengthStringData[] wsaaT5399RiskStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 0); 
	private FixedLengthStringData[][] wsaaT5399RiskStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaT5399RiskStats, 0);
	private FixedLengthStringData[] wsaaT5399PremStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 24);
	private FixedLengthStringData[][] wsaaT5399PremStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaT5399PremStats, 0);
	private FixedLengthStringData[] wsaaT5399SetRiskStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 48);
	private FixedLengthStringData[][] wsaaT5399SetRiskStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaT5399SetRiskStats, 0);
	private FixedLengthStringData[] wsaaT5399SetPremStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 72);
	private FixedLengthStringData[][] wsaaT5399SetPremStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaT5399SetPremStats, 0);
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t5399 = new FixedLengthStringData(6).init("T5399");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6597 = new FixedLengthStringData(5).init("T6597");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData th584 = new FixedLengthStringData(5).init("TH584");
	private FixedLengthStringData th506 = new FixedLengthStringData(5).init("TH506");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
	private FixedLengthStringData tr386 = new FixedLengthStringData(5).init("TR386");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
