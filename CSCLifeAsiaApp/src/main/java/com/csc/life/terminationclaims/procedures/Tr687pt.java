/*
 * File: Tr687pt.java
 * Date: 30 August 2009 2:44:11
 * Author: Quipoz Limited
 * 
 * Class transformed from TR687PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.terminationclaims.tablestructures.Tr687rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR687.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr687pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(42).isAPartOf(wsaaPrtLine001, 34, FILLER).init("Hospital Benefits                    SR687");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(8);
	private FixedLengthStringData filler9 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 0, FILLER).init("    Code");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(79);
	private FixedLengthStringData filler10 = new FixedLengthStringData(79).isAPartOf(wsaaPrtLine005, 0, FILLER).init("    Benefit  Amt/Life Time  Amt/Annual    Max Ben    No.   Deductibles  CoPaymt");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(77);
	private FixedLengthStringData filler11 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 0, FILLER).init("  1");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 4);
	private FixedLengthStringData filler12 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine006, 14).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine006, 28).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine006, 42).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 53).setPattern("ZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine006, 60).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 70, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 74).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(77);
	private FixedLengthStringData filler18 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  2");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 4);
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine007, 14).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine007, 28).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine007, 42).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 53).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine007, 60).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 70, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 74).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(77);
	private FixedLengthStringData filler25 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 0, FILLER).init("  3");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 4);
	private FixedLengthStringData filler26 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine008, 14).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine008, 28).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine008, 42).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 53).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine008, 60).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 70, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 74).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(77);
	private FixedLengthStringData filler32 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  4");
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 4);
	private FixedLengthStringData filler33 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine009, 14).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine009, 28).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine009, 42).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 53).setPattern("ZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine009, 60).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 70, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 74).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(77);
	private FixedLengthStringData filler39 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 0, FILLER).init("  5");
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 4);
	private FixedLengthStringData filler40 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine010, 14).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine010, 28).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine010, 42).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 53).setPattern("ZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine010, 60).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 70, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 74).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(77);
	private FixedLengthStringData filler46 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 0, FILLER).init("  6");
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 4);
	private FixedLengthStringData filler47 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine011, 14).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine011, 28).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine011, 42).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 53).setPattern("ZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine011, 60).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 70, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 74).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(78);
	private FixedLengthStringData filler53 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine012, 0, FILLER).init(" Annual Aggregated Deductible:");
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(10, 0).isAPartOf(wsaaPrtLine012, 32).setPattern("ZZZZZZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine012, 42, FILLER).init("  Min Sum Assured:");
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(17, 0).isAPartOf(wsaaPrtLine012, 61).setPattern("ZZZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(75);
	private FixedLengthStringData filler55 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" Prem & Coverage Timing Factor:");
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(6, 0).isAPartOf(wsaaPrtLine013, 32).setPattern("ZZZZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 38, FILLER).init(SPACES);
	private FixedLengthStringData filler57 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine013, 44, FILLER).init("Life Coverage Timing Factor:");
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 73).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(71);
	private FixedLengthStringData filler58 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler59 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine014, 44, FILLER).init("Continuation Item:");
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 63);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr687rec tr687rec = new Tr687rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr687pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr687rec.tr687Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo010.set(tr687rec.benfamt01);
		fieldNo017.set(tr687rec.benfamt02);
		fieldNo024.set(tr687rec.benfamt03);
		fieldNo031.set(tr687rec.benfamt04);
		fieldNo038.set(tr687rec.benfamt05);
		fieldNo045.set(tr687rec.benfamt06);
		fieldNo011.set(tr687rec.nofday01);
		fieldNo018.set(tr687rec.nofday02);
		fieldNo025.set(tr687rec.nofday03);
		fieldNo032.set(tr687rec.nofday04);
		fieldNo039.set(tr687rec.nofday05);
		fieldNo046.set(tr687rec.nofday06);
		fieldNo013.set(tr687rec.copay01);
		fieldNo020.set(tr687rec.copay02);
		fieldNo027.set(tr687rec.copay03);
		fieldNo034.set(tr687rec.copay04);
		fieldNo041.set(tr687rec.copay05);
		fieldNo048.set(tr687rec.copay06);
		fieldNo007.set(tr687rec.hosben01);
		fieldNo014.set(tr687rec.hosben02);
		fieldNo021.set(tr687rec.hosben03);
		fieldNo028.set(tr687rec.hosben04);
		fieldNo035.set(tr687rec.hosben05);
		fieldNo042.set(tr687rec.hosben06);
		fieldNo008.set(tr687rec.amtlife01);
		fieldNo015.set(tr687rec.amtlife02);
		fieldNo022.set(tr687rec.amtlife03);
		fieldNo029.set(tr687rec.amtlife04);
		fieldNo036.set(tr687rec.amtlife05);
		fieldNo043.set(tr687rec.amtlife06);
		fieldNo009.set(tr687rec.amtyear01);
		fieldNo016.set(tr687rec.amtyear02);
		fieldNo023.set(tr687rec.amtyear03);
		fieldNo030.set(tr687rec.amtyear04);
		fieldNo037.set(tr687rec.amtyear05);
		fieldNo044.set(tr687rec.amtyear06);
		fieldNo012.set(tr687rec.gdeduct01);
		fieldNo019.set(tr687rec.gdeduct02);
		fieldNo026.set(tr687rec.gdeduct03);
		fieldNo033.set(tr687rec.gdeduct04);
		fieldNo040.set(tr687rec.gdeduct05);
		fieldNo047.set(tr687rec.gdeduct06);
		fieldNo049.set(tr687rec.aad);
		fieldNo051.set(tr687rec.premunit);
		fieldNo050.set(tr687rec.zssi);
		fieldNo052.set(tr687rec.factor);
		fieldNo053.set(tr687rec.contitem);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
