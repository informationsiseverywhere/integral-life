/*
 * File: P6762.java
 * Date: 30 August 2009 0:56:28
 * Author: Quipoz Limited
 * 
 * Class transformed from P6762.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.procedures.T6762pt;
import com.csc.life.terminationclaims.screens.S6762ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6762rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P6762 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6762");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T6762rec t6762rec = new T6762rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6762ScreenVars sv = ScreenProgram.getScreenVars( S6762ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P6762() {
		super();
		screenVars = sv;
		new ScreenModel("S6762", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t6762rec.t6762Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
	}

protected void generalArea1045()
	{
		sv.regpayExcpCsfl.set(t6762rec.regpayExcpCsfl);
		sv.regpayExcpExpd.set(t6762rec.regpayExcpExpd);
		sv.regpayExcpIchs.set(t6762rec.regpayExcpIchs);
		sv.regpayExcpIcos.set(t6762rec.regpayExcpIcos);
		sv.regpayExcpInrv.set(t6762rec.regpayExcpInrv);
		sv.regpayExcpInsf.set(t6762rec.regpayExcpInsf);
		sv.regpayExcpIpst.set(t6762rec.regpayExcpIpst);
		sv.regpayExcpLtma.set(t6762rec.regpayExcpLtma);
		sv.regpayExcpNopr.set(t6762rec.regpayExcpNopr);
		sv.regpayExcpTinf.set(t6762rec.regpayExcpTinf);
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(t6762rec.t6762Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.regpayExcpCsfl,t6762rec.regpayExcpCsfl)) {
			t6762rec.regpayExcpCsfl.set(sv.regpayExcpCsfl);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.regpayExcpExpd,t6762rec.regpayExcpExpd)) {
			t6762rec.regpayExcpExpd.set(sv.regpayExcpExpd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.regpayExcpIchs,t6762rec.regpayExcpIchs)) {
			t6762rec.regpayExcpIchs.set(sv.regpayExcpIchs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.regpayExcpIcos,t6762rec.regpayExcpIcos)) {
			t6762rec.regpayExcpIcos.set(sv.regpayExcpIcos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.regpayExcpInrv,t6762rec.regpayExcpInrv)) {
			t6762rec.regpayExcpInrv.set(sv.regpayExcpInrv);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.regpayExcpInsf,t6762rec.regpayExcpInsf)) {
			t6762rec.regpayExcpInsf.set(sv.regpayExcpInsf);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.regpayExcpIpst,t6762rec.regpayExcpIpst)) {
			t6762rec.regpayExcpIpst.set(sv.regpayExcpIpst);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.regpayExcpLtma,t6762rec.regpayExcpLtma)) {
			t6762rec.regpayExcpLtma.set(sv.regpayExcpLtma);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.regpayExcpNopr,t6762rec.regpayExcpNopr)) {
			t6762rec.regpayExcpNopr.set(sv.regpayExcpNopr);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.regpayExcpTinf,t6762rec.regpayExcpTinf)) {
			t6762rec.regpayExcpTinf.set(sv.regpayExcpTinf);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T6762pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
