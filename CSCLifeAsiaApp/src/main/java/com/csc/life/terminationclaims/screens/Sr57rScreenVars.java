package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for Sr57r
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class Sr57rScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(958);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(382).isAPartOf(dataArea, 0);
	public FixedLengthStringData causeofdth = DD.causeofdth.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,4);
	public ZonedDecimalData dtofdeath = DD.dtofdeath.copyToZonedDecimal().isAPartOf(dataFields,7);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,15);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,23);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,53);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,70);
	public ZonedDecimalData totclaim = DD.totclaim.copyToZonedDecimal().isAPartOf(dataFields,74);
	public FixedLengthStringData asterisk = DD.asterisk.copy().isAPartOf(dataFields,91);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,92);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,100);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,108);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,111);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,119);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,149);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,157);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,204);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,212);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,259);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,269);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,277);
	public FixedLengthStringData astrsk = DD.astrsk.copy().isAPartOf(dataFields,287);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,288);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,296);
	public FixedLengthStringData causeofdthdsc = DD.causeofdthdsc.copy().isAPartOf(dataFields,343);
	public FixedLengthStringData claimnumber = DD.claimnumber.copy().isAPartOf(dataFields,373);
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 382);
	public FixedLengthStringData causeofdthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData dtofdeathErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData totclaimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData asteriskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData causeofdthdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData claimnumberErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(432).isAPartOf(dataArea, 526);
	public FixedLengthStringData[] causeofdthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] dtofdeathOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] totclaimOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] asteriskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] causeofdthdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] claimnumberOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(254);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(108).isAPartOf(subfileArea, 0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,2);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(subfileFields,6);
	public FixedLengthStringData lcendesc = DD.lcendesc.copy().isAPartOf(subfileFields,10);
	public ZonedDecimalData zrlsclmv = DD.zrlsclmv.copyToZonedDecimal().isAPartOf(subfileFields,40);
	public ZonedDecimalData zrlsclma = DD.zrlsclma.copyToZonedDecimal().isAPartOf(subfileFields,57);
	public ZonedDecimalData zhldclmv = DD.zhldclmv.copyToZonedDecimal().isAPartOf(subfileFields,74);
	public ZonedDecimalData zhldclma = DD.zhldclma.copyToZonedDecimal().isAPartOf(subfileFields,91);
	
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 108);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData lcendescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData zrlsclmvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData zrlsclmaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData zhldclmvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData zhldclmaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(108).isAPartOf(subfileArea, 144);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] lcendescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] zrlsclmvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] zrlsclmaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] zhldclmvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] zhldclmaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 252);
	
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData dtofdeathDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData Sr57rscreensflWritten = new LongData(0);
	public LongData Sr57rscreenctlWritten = new LongData(0);
	public LongData Sr57rscreenWritten = new LongData(0);
	public LongData Sr57rprotectWritten = new LongData(0);
	public GeneralTable sr57rscreensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr57rscreensfl;
	}

	public Sr57rScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dtofdeathOut,new String[] {"01","09","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"02","10","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(causeofdthOut,new String[] {"03","09","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(otheradjstOut,new String[] {"07","09","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"05","09","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(longdescOut,new String[] {"06","09","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zhldclmvOut,new String[] {"08","09","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zhldclmaOut,new String[] {"11","09","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		fieldIndMap.put(claimnumberOut,new String[] {"31",null, "-31","24", null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {coverage, rider, life, crtable, lcendesc, zrlsclmv, zrlsclma, zhldclmv, zhldclma};
		screenSflOutFields = new BaseData[][] {coverageOut, riderOut, lifeOut, crtableOut, lcendescOut, zrlsclmvOut, zrlsclmaOut, zhldclmvOut, zhldclmaOut};
		screenSflErrFields = new BaseData[] {coverageErr, riderErr, lifeErr, crtableErr, lcendescErr, zrlsclmvErr, zrlsclmaErr, zhldclmvErr, zhldclmaErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, ptdate, btdate, asterisk, dtofdeath, effdate, causeofdth, otheradjst, reasoncd, longdesc, currcd, totclaim, claimnumber};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, ptdateOut, btdateOut, asteriskOut, dtofdeathOut, effdateOut, causeofdthOut, otheradjstOut, reasoncdOut, longdescOut, currcdOut, totclaimOut, claimnumberOut };
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, ptdateErr, btdateErr, asteriskErr, dtofdeathErr, effdateErr, causeofdthErr, otheradjstErr, reasoncdErr, longdescErr, currcdErr, totclaimErr, claimnumberErr};
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, dtofdeath, effdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, dtofdeathErr, effdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, dtofdeathDisp, effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr57rscreen.class;
		screenSflRecord = Sr57rscreensfl.class;
		screenCtlRecord = Sr57rscreenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr57rprotect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr57rscreenctl.lrec.pageSubfile);
	}
}
