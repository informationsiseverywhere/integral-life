package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:15
 * Description:
 * Copybook name: T6694REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6694rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6694Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData bankreq = new FixedLengthStringData(1).isAPartOf(t6694Rec, 0);
  	public FixedLengthStringData contreq = new FixedLengthStringData(1).isAPartOf(t6694Rec, 1);
  	public FixedLengthStringData debcred = new FixedLengthStringData(1).isAPartOf(t6694Rec, 2);
  	public FixedLengthStringData glact = new FixedLengthStringData(14).isAPartOf(t6694Rec, 3);
  	public FixedLengthStringData payeereq = new FixedLengthStringData(1).isAPartOf(t6694Rec, 17);
  	public FixedLengthStringData reqntype = new FixedLengthStringData(1).isAPartOf(t6694Rec, 18);
  	public FixedLengthStringData sacscode = new FixedLengthStringData(2).isAPartOf(t6694Rec, 19);
  	public FixedLengthStringData sacstype = new FixedLengthStringData(2).isAPartOf(t6694Rec, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(477).isAPartOf(t6694Rec, 23, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6694Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6694Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}