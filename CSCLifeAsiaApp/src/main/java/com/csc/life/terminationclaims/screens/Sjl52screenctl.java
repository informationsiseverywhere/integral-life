package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl52screenctl extends ScreenRecord{

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 10, 13, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sjl52screensfl";
		lrec.subfileClass = Sjl52screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 101;
		lrec.pageSubfile = 100;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 6, 1, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl52ScreenVars sv = (Sjl52ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl52screenctlWritten, sv.Sjl52screensflWritten, av, sv.Sjl52screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl52ScreenVars screenVars = (Sjl52ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.conStatus.setClassString("");
		screenVars.claimFrom.setClassString("");
		screenVars.conFrom.setClassString("");
		screenVars.conOwner.setClassString("");
		screenVars.conDateDisp.setClassString("");
		screenVars.claimTypeList.setClassString("");
		screenVars.clttwo.setClassString("");
	}

/**
 * Clear all the variables in Sjl52screenctl
 */
	public static void clear(VarModel pv) {
		Sjl52ScreenVars screenVars = (Sjl52ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.conStatus.clear();
		screenVars.claimFrom.clear();
		screenVars.conFrom.clear();
		screenVars.conOwner.clear();
		screenVars.conDateDisp.clear();
		screenVars.claimTypeList.clear();
		screenVars.clttwo.clear();
	}
}
