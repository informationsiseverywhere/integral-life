package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.NohspfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Nohspf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * 
 * @author ehyrat
 * DAOImpl related table NOHSPF
 *
 */
public class NohspfDAOImpl extends BaseDAOImpl<Nohspf> implements NohspfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(NohspfDAOImpl.class);

	@Override
	public boolean insertNohspf(Nohspf nohspf) {
		StringBuilder sb = new StringBuilder("INSERT INTO NOHSPF(NOTIHYCOY,NOTIHSNUM,TRANSNO,TRANSCODE,TRANSDATE,TRANSDESC,EFFECTDATE,USRPRF,JOBNM,DATIME) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?)");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
        boolean result = false;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, nohspf.getNotihycoy());
			ps.setString(2, nohspf.getNotihsnum());
			ps.setString(3, nohspf.getTransno());
			ps.setString(4, nohspf.getTranscode());
			ps.setString(5, nohspf.getTransdate());
			ps.setString(6, nohspf.getTransdesc());
			ps.setInt(7, nohspf.getEffectdate());
			ps.setString(8, getUsrprf());
			ps.setString(9, getJobnm());
			ps.setTimestamp(10, getDatime());
			ps.executeUpdate();
			result = true;
		}catch(SQLException e) {
			LOGGER.error("insertNohspf()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("insertNohspf()", ex);//IJTI-1561
			throw new RuntimeException(ex);
		}finally {
			close(ps, null);			
		}
		
		return result;
	}


	@Override
	public List<Nohspf> getNotificationHistoryRecord(String notihscnum) {
		StringBuilder sqlClntSelect1 = new StringBuilder(
                "SELECT NOTIHSNUM,TRANSNO,TRANSCODE,TRANSDATE,TRANSDESC,EFFECTDATE,USRPRF FROM NOHSPF WHERE NOTIHSNUM=? ORDER BY TRANSNO DESC");
		 ResultSet sqlclntpf1rs = null;
	        PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
	        List<Nohspf> resultList = new ArrayList<Nohspf>();
		 try {
			 	psClntSelect.setString(1, notihscnum.trim());
	            sqlclntpf1rs = executeQuery(psClntSelect);
	            while (sqlclntpf1rs.next()) {
	            	Nohspf nohspf = new Nohspf();
	            	nohspf.setNotihsnum(sqlclntpf1rs.getString(1));
	            	nohspf.setTransno(sqlclntpf1rs.getString(2));
	            	nohspf.setTranscode(sqlclntpf1rs.getString(3));
	            	nohspf.setTransdate(sqlclntpf1rs.getString(4));
	            	nohspf.setTransdesc(sqlclntpf1rs.getString(5));
	            	nohspf.setEffectdate(sqlclntpf1rs.getInt(6));
	            	nohspf.setUsrprf(sqlclntpf1rs.getString(7));
	            	resultList.add(nohspf);
	            }
	        } catch (SQLException e) {
	            LOGGER.error("getNotificationHistoryRecord()", e);//IJTI-1561
	            throw new SQLRuntimeException(e);
	        } catch(Exception ex){
			LOGGER.error("getNotificationHistoryRecord()", ex);//IJTI-1561
			throw new RuntimeException(ex);
		} finally {
	            close(psClntSelect, sqlclntpf1rs);
	        }
		return resultList;
	}


	@Override
	public String getMaxTransnum(String notihsnum) {
		
		String notifinum="";
		StringBuilder sqlClntSelect1 = new StringBuilder(
                "SELECT MAX(TRANSNO) ");
        sqlClntSelect1.append("FROM NOHSPF WHERE NOTIHSNUM = ?");
      
        ResultSet sqlclntpf1rs = null;
        PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
        try {
        	psClntSelect.setString(1, notihsnum.trim());
            sqlclntpf1rs = executeQuery(psClntSelect);

            if (sqlclntpf1rs.next()) {
            	notifinum=sqlclntpf1rs.getString(1);
            	
            }
        } catch (SQLException e) {
            LOGGER.error("getMaxTransnum()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psClntSelect, sqlclntpf1rs);
        }
        return notifinum;	
		
		
	}


	@Override
	public List<Nohspf> getCombainedNotificationHistoryRecord(String notihsnum) {
		StringBuilder sqlClntSelect1 = new StringBuilder(
                "select b.NOTIFINUM,b.TRANSNO,a.TRANSCODE,a.TRANSDATE,a.TRANSDESC,a.EFFECTDATE,a.USRPRF from NOHSPF a, NOTIPF b ");
		sqlClntSelect1.append("where a.TRANSNO=b.TRANSNO and a.NOTIHSNUM = b.NOTIFINUM and b.NOTIFINUM = ?");
		 ResultSet sqlclntpf1rs = null;
	        PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
	        List<Nohspf> resultList = new ArrayList<Nohspf>();
		 try {
			 	psClntSelect.setString(1, notihsnum.trim());
	            sqlclntpf1rs = executeQuery(psClntSelect);
	            while (sqlclntpf1rs.next()) {
	            	Nohspf nohspf = new Nohspf();
	            	nohspf.setNotihsnum(sqlclntpf1rs.getString(1));
	            	nohspf.setTransno(sqlclntpf1rs.getString(2));
	            	nohspf.setTranscode(sqlclntpf1rs.getString(3));
	            	nohspf.setTransdate(sqlclntpf1rs.getString(4));
	            	nohspf.setTransdesc(sqlclntpf1rs.getString(5));
	            	nohspf.setEffectdate(sqlclntpf1rs.getInt(6));
	            	nohspf.setUsrprf(sqlclntpf1rs.getString(7));
	            	resultList.add(nohspf);
	            }
	        } catch (SQLException e) {
	            LOGGER.error("getCombainedNotificationHistoryRecord()", e);//IJTI-1561
	            throw new SQLRuntimeException(e);
	        } catch(Exception ex){
			LOGGER.error("getCombainedNotificationHistoryRecord()", ex);//IJTI-1561
			throw new RuntimeException(ex);
		} finally {
	            close(psClntSelect, sqlclntpf1rs);
	        }
		return resultList;
	}
}
