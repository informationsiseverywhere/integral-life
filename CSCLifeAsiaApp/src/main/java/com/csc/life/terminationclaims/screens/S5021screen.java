package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5021screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5021ScreenVars sv = (S5021ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5021screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5021ScreenVars screenVars = (S5021ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.cntdesc.setClassString("");
		screenVars.cownum.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.rstatdesc.setClassString("");
		screenVars.premStatDesc.setClassString("");
		screenVars.rcdateDisp.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.jlifename.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.plansfx.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.bonusDecDateDisp.setClassString("");
		screenVars.bonusValue.setClassString("");
		screenVars.life.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.bonusValueSurrender.setClassString("");
		screenVars.bonusReserveValue.setClassString("");
		screenVars.paycurr.setClassString("");
		screenVars.numpols.setClassString("");
	}

/**
 * Clear all the variables in S5021screen
 */
	public static void clear(VarModel pv) {
		S5021ScreenVars screenVars = (S5021ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.cnttype.clear();
		screenVars.cntdesc.clear();
		screenVars.cownum.clear();
		screenVars.lifenum.clear();
		screenVars.jlife.clear();
		screenVars.chdrnum.clear();
		screenVars.rstatdesc.clear();
		screenVars.premStatDesc.clear();
		screenVars.rcdateDisp.clear();
		screenVars.rcdate.clear();
		screenVars.ownername.clear();
		screenVars.lifename.clear();
		screenVars.jlifename.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.plansfx.clear();
		screenVars.cntcurr.clear();
		screenVars.bonusDecDateDisp.clear();
		screenVars.bonusDecDate.clear();
		screenVars.bonusValue.clear();
		screenVars.life.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.bonusValueSurrender.clear();
		screenVars.bonusReserveValue.clear();
		screenVars.paycurr.clear();
		screenVars.numpols.clear();
	}
}
