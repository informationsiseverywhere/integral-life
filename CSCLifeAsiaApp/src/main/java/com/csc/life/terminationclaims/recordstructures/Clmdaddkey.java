package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:43
 * Description:
 * Copybook name: CLMDADDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Clmdaddkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData clmdaddFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData clmdaddKey = new FixedLengthStringData(64).isAPartOf(clmdaddFileKey, 0, REDEFINE);
  	public FixedLengthStringData clmdaddChdrcoy = new FixedLengthStringData(1).isAPartOf(clmdaddKey, 0);
  	public FixedLengthStringData clmdaddChdrnum = new FixedLengthStringData(8).isAPartOf(clmdaddKey, 1);
  	public FixedLengthStringData clmdaddLife = new FixedLengthStringData(2).isAPartOf(clmdaddKey, 9);
  	public FixedLengthStringData clmdaddCoverage = new FixedLengthStringData(2).isAPartOf(clmdaddKey, 11);
  	public FixedLengthStringData clmdaddRider = new FixedLengthStringData(2).isAPartOf(clmdaddKey, 13);
  	public FixedLengthStringData clmdaddVirtualFund = new FixedLengthStringData(4).isAPartOf(clmdaddKey, 15);
  	public FixedLengthStringData clmdaddFieldType = new FixedLengthStringData(1).isAPartOf(clmdaddKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(clmdaddKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(clmdaddFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		clmdaddFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}