package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:21
 * Description:
 * Copybook name: T6762REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6762rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6762Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData regpayExcpCsfl = new FixedLengthStringData(4).isAPartOf(t6762Rec, 0);
  	public FixedLengthStringData regpayExcpExpd = new FixedLengthStringData(4).isAPartOf(t6762Rec, 4);
  	public FixedLengthStringData regpayExcpIchs = new FixedLengthStringData(4).isAPartOf(t6762Rec, 8);
  	public FixedLengthStringData regpayExcpIcos = new FixedLengthStringData(4).isAPartOf(t6762Rec, 12);
  	public FixedLengthStringData regpayExcpInrv = new FixedLengthStringData(4).isAPartOf(t6762Rec, 16);
  	public FixedLengthStringData regpayExcpInsf = new FixedLengthStringData(4).isAPartOf(t6762Rec, 20);
  	public FixedLengthStringData regpayExcpIpst = new FixedLengthStringData(4).isAPartOf(t6762Rec, 24);
  	public FixedLengthStringData regpayExcpLtma = new FixedLengthStringData(4).isAPartOf(t6762Rec, 28);
  	public FixedLengthStringData regpayExcpNopr = new FixedLengthStringData(4).isAPartOf(t6762Rec, 32);
  	public FixedLengthStringData regpayExcpTinf = new FixedLengthStringData(4).isAPartOf(t6762Rec, 36);
  	public FixedLengthStringData filler = new FixedLengthStringData(460).isAPartOf(t6762Rec, 40, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6762Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6762Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}