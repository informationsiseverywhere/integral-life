/*
 * File: Dccan5.java
 * Date: 29 August 2009 22:45:39
 * Author: Quipoz Limited
 * 
 * Class transformed from DCCAN5.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  DCCAN5 - Death Claim Calculation Subroutine No. 5
*  -------------------------------------------------
*
*  This program is an item entry on T6598,  the death claim
*  subroutine method table.
*
*  The  following  linkage  information  is passed  to this
*  subroutine:-
*
*             - DEATHREC
*
*  The  calculation  is  for  Deferred  Annuity  components
*  that have NOT vested.
*
*  The  death  value  returned  in  this  case will  be  an
*  accumulation  of  all  premiums  paid  to date  for  the
*  component being processed.
*
*  This is determined by getting the Account Balance (ACBL)
*  for the component passed  in the call  to  this program.
*  NB. Component Level accounting must have been running on
*      this component.
*
*  If  Revenue Accounting  debits the Accumulated  Premiums
*  account then this is NOT included  back  in to the total
*  premiums paid calculated by this program. This is to  be
*  accounted  for  by  the  Adjustment  field  on the Death
*  Register screen manually.
*
*  If  the fee is to be included in the total premiums then
*  this  too  must  be  accounted  for  manually  via   the
*  adjustment field on the Register screen. This is because
*  the  fee  is  accounted  for  at  a  contract  level not
*  component level.
*
*  EXAMPLE
*  -------
*
*  New Business  Debit  LP S    100
*                Credit LE PM   100 (Premium Account)
*  Collection    Debit  LP S    100
*   #1           Credit LE PM   100 (Premium Account)
*  Collection    Debit  LP S    100
*   #2           Credit LE PM   100 (Premium Account)
*
*  Death Claim  in  third year will have a 300 ACBL balance
*  in  LE PM.  Therefore,  T5645 would only need a one line
*  entry for LE PM.
*
*****************************************************************
* </pre>
*/
public class Dccan5 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "DCCAN5";
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaSwitch, " ");

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* TABLES */
	private String t5645 = "T5645";
	private String t5687 = "T5687";
		/* FORMATS */
	private String acblrec = "ACBLREC";
	private String covrrec = "COVRREC";
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Deathrec deathrec = new Deathrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1099, 
		exit1199, 
		exit1299, 
		exit2299, 
		exit610
	}

	public Dccan5() {
		super();
	}

public void mainline(Object... parmArray)
	{
		deathrec.deathRec = convertAndSetParam(deathrec.deathRec, parmArray, 0);
		try {
			startSubr1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr1000()
	{
		try {
			para1010();
		}
		catch (GOTOException e){
		}
		finally{
			exit1099();
		}
	}

protected void para1010()
	{
		deathrec.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		deathrec.estimatedVal.set(ZERO);
		deathrec.actualVal.set(ZERO);
		deathrec.element.set(SPACES);
		deathrec.description.set(SPACES);
		if (firstTime.isTrue()) {
			begnCovr1100();
		}
		else {
			nextrCovr1200();
		}
		wsaaSwitch.set("N");
		if (isEQ(covrIO.getStatuz(),varcom.endp)) {
			deathrec.status.set(varcom.endp);
			wsaaSwitch.set(SPACES);
			goTo(GotoLabel.exit1099);
		}
		
		//ilife-2546 starts
		if (isNE(covrIO.getValidflag(),"1")) {
		      goTo(GotoLabel.exit1199);
		}
		//ilife-2546 ends
		totalPremsPaid2000();
	}

protected void exit1099()
	{
		exitProgram();
	}

protected void begnCovr1100()
	{
		try {
			begn1110();
		}
		catch (GOTOException e){
		}
	}

protected void begn1110()
	{
	covrIO.setDataArea(SPACES);
	covrIO.setChdrcoy(deathrec.chdrChdrcoy);
	covrIO.setChdrnum(deathrec.chdrChdrnum);
	covrIO.setLife(deathrec.lifeLife);
	covrIO.setCoverage(deathrec.covrCoverage);
	covrIO.setRider(deathrec.covrRider);
	covrIO.setPlanSuffix(ZERO);
	covrIO.setFormat(covrrec);
	covrIO.setFunction(varcom.begn);

	//performance improvement --  atiwari23 
	covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
	
	SmartFileCode.execute(appVars, covrIO);
	if (isNE(covrIO.getStatuz(),varcom.oK)
	&& isNE(covrIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(covrIO.getParams());
		fatalError600();
	}
	if ((isNE(covrIO.getChdrcoy(),deathrec.chdrChdrcoy)
	|| isNE(covrIO.getChdrnum(),deathrec.chdrChdrnum)
	|| isNE(covrIO.getLife(),deathrec.lifeLife)
	|| isNE(covrIO.getCoverage(),deathrec.covrCoverage)
	|| isNE(covrIO.getRider(),deathrec.covrRider)
	|| isEQ(covrIO.getStatuz(),varcom.endp))) {
		covrIO.setStatuz(varcom.endp);
		goTo(GotoLabel.exit1199);
	}
	
	
}
	
	

protected void nextrCovr1200()
	{
		try {
			nextr1210();
		}
		catch (GOTOException e){
		}
	}

protected void nextr1210()
	{//performance improvement --  atiwari23 
	covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");

		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if ((isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if ((isNE(covrIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(covrIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(covrIO.getLife(),deathrec.lifeLife)
		|| isNE(covrIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(covrIO.getRider(),deathrec.covrRider)
		|| isEQ(covrIO.getStatuz(),varcom.endp))) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1299);
		}
		
		// ILIFE-2546 starts
		if (isNE(covrIO.getValidflag(),"1")) {
			covrIO.setStatuz(varcom.endp);
		      goTo(GotoLabel.exit1199);
		}
		//ILIFE-2546 ends
	}





protected void totalPremsPaid2000()
	{
		para2010();
	}

protected void para2010()
	{
		readT56452100();
		deathrec.fieldType.set("S");
		deathrec.valueType.set("S");
		wsaaPlan.set(covrIO.getPlanSuffix());
		deathrec.element.set(wsaaPlansuff);
		descIO.setDescitem(deathrec.crtable);
		descIO.setDesctabl(t5687);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(deathrec.chdrChdrcoy);
		descIO.setLanguage(deathrec.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			deathrec.description.set(descIO.getShortdesc());
		}
		else {
			deathrec.description.fill("?");
		}
		for (wsaaSub.set(1); !(isEQ(t5645rec.sacscode[wsaaSub.toInt()],SPACES)); wsaaSub.add(1)){
			accumulate2200();
		}
	}

protected void readT56452100()
	{
		para2110();
	}

protected void para2110()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(deathrec.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void accumulate2200()
	{
		para2210();
	}

protected void para2210()
	{
		acblIO.setParams(SPACES);
		acblIO.setStatuz(varcom.oK);
		acblIO.setRldgcoy(deathrec.chdrChdrcoy);
		wsaaRldgChdrnum.set(deathrec.chdrChdrnum);
		wsaaRldgLife.set(deathrec.lifeLife);
		wsaaRldgCoverage.set(deathrec.covrCoverage);
		wsaaRldgRider.set(deathrec.covrRider);
		wsaaRldgPlanSuffix.set(deathrec.element);
		acblIO.setRldgacct(wsaaRldgacct);
		acblIO.setOrigcurr(SPACES);
		acblIO.setSacscode(t5645rec.sacscode[wsaaSub.toInt()]);
		acblIO.setSacstyp(t5645rec.sacstype[wsaaSub.toInt()]);
		acblIO.setFunction(varcom.begn);
		acblIO.setFormat(acblrec);
		while ( !(isEQ(acblIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23 
			acblIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			acblIO.setFitKeysSearch("RLDGCOY");
			
			callAcbl2250();
		}
		
		compute(deathrec.actualVal, 2).set(mult(deathrec.actualVal,-1));
	}

protected void callAcbl2250()
	{
		try {
			para2260();
		}
		catch (GOTOException e){
		}
	}

protected void para2260()
	{
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		acblIO.setFunction(varcom.nextr);
		wsaaRldgacct.set(acblIO.getRldgacct());
		if ((isNE(acblIO.getRldgcoy(),deathrec.chdrChdrcoy)
		|| isNE(wsaaRldgChdrnum,deathrec.chdrChdrnum)
		|| isNE(wsaaRldgLife,deathrec.lifeLife)
		|| isNE(wsaaRldgCoverage,deathrec.covrCoverage)
		|| isNE(wsaaRldgRider,deathrec.covrRider)
		|| isNE(wsaaRldgPlanSuffix,deathrec.element)
		|| isEQ(acblIO.getStatuz(),varcom.endp))) {
			acblIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2299);
		}
		if ((isEQ(acblIO.getSacscode(),t5645rec.sacscode[wsaaSub.toInt()])
		&& isEQ(acblIO.getSacstyp(),t5645rec.sacstype[wsaaSub.toInt()]))) {
			deathrec.actualVal.add(acblIO.getSacscurbal());
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error601();
				}
				case exit610: {
					exit610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error601()
	{
		if (isEQ(syserrrec.statuz,"BOMB")) {
			goTo(GotoLabel.exit610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit610()
	{
		deathrec.status.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
