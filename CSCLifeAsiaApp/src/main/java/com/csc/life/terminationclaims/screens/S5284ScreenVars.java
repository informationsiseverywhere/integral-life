package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5284
 * @version 1.0 generated on 30/08/09 06:38
 * @author Quipoz
 */
public class S5284ScreenVars extends SmartVarModel { 
	//ILIFE-1137
	//added for death claim flexibility BRSO
	//public FixedLengthStringData dataArea = new FixedLengthStringData(1006);
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	//public FixedLengthStringData dataFields = new FixedLengthStringData(478).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData asterisk = DD.asterisk.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData astrsk = DD.astrsk.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData causeofdth = DD.causeofdth.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,14);
	public ZonedDecimalData clamamt = DD.clamamt.copyToZonedDecimal().isAPartOf(dataFields,22);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,42);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,80);
	public ZonedDecimalData dtofdeath = DD.dtofdeath.copyToZonedDecimal().isAPartOf(dataFields,83);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,91);
	public ZonedDecimalData intday = DD.intday.copyToZonedDecimal().isAPartOf(dataFields,99);
	public ZonedDecimalData interest = DD.interest.copyToZonedDecimal().isAPartOf(dataFields,104);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,117);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,125);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,180);
	public ZonedDecimalData net = DD.net.copyToZonedDecimal().isAPartOf(dataFields,227);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,244);
	public ZonedDecimalData ofcharge = DD.ofcharge.copyToZonedDecimal().isAPartOf(dataFields,252);
	public ZonedDecimalData otheradjst = DD.otheradjst.copyToZonedDecimal().isAPartOf(dataFields,265);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,282);
	public FixedLengthStringData paycurr = DD.paycurr.copy().isAPartOf(dataFields,329);
	public ZonedDecimalData policyloan = DD.policyloan.copyToZonedDecimal().isAPartOf(dataFields,332);
	public ZonedDecimalData proceeds = DD.proceeds.copyToZonedDecimal().isAPartOf(dataFields,349);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,366);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,376);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,384);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,388);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,438);
	public ZonedDecimalData totclaim = DD.totclaim.copyToZonedDecimal().isAPartOf(dataFields,448);
	public ZonedDecimalData zrcshamt = DD.zrcshamt.copyToZonedDecimal().isAPartOf(dataFields,465);
	//ILIFE-1137
	//added for death claim flexibility BRSO
	public ZonedDecimalData susamt = DD.susamt.copyToZonedDecimal().isAPartOf(dataFields,478);
	public ZonedDecimalData nextinsamt = DD.nextinsamt.copyToZonedDecimal().isAPartOf(dataFields,495);
	
	public ZonedDecimalData zhldclmv = DD.zhldclmv.copyToZonedDecimal().isAPartOf(dataFields,512);//BRD-34
	public ZonedDecimalData zhldclma = DD.zhldclma.copyToZonedDecimal().isAPartOf(dataFields,529);//BRD-34
	public FixedLengthStringData bnfying = DD.bnfying.copy().isAPartOf(dataFields,546);//CLM-015
	public ZonedDecimalData confirm = DD.confirm.copyToZonedDecimal().isAPartOf(dataFields,547);//CLM-015
	//added for death claim flexibility BRSO
	
	// CML-004
	public ZonedDecimalData itstrate = DD.intrat.copyToZonedDecimal().isAPartOf(dataFields,548);
	public FixedLengthStringData claimnumber = DD.claimnumber.copy().isAPartOf(dataFields,556);
	public FixedLengthStringData aacct = DD.aacct.copy().isAPartOf(dataFields,565);
	public FixedLengthStringData claimnotes = DD.claimnotes.copy().isAPartOf(dataFields,579);
	public FixedLengthStringData investres = DD.investres.copy().isAPartOf(dataFields,580);
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,581);
	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 478);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData asteriskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData astrskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData causeofdthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData clamamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData dtofdeathErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData intdayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData interestErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData netErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ofchargeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData otheradjstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData paycurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData policyloanErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData proceedsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData totclaimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData zrcshamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	//ILIFE-1137
	//added for death claim flexibility BRSO
	public FixedLengthStringData susamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData nextinsamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	
	public FixedLengthStringData zhldclmvErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);//BRD-34
	public FixedLengthStringData zhldclmaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);//BRD-34
	public FixedLengthStringData bnfyingErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData confirmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	//added for death claim
	
	public FixedLengthStringData itstrateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData claimnumberErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData aacctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
    public FixedLengthStringData claimnotesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
    public FixedLengthStringData investresErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
    public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(396).isAPartOf(dataArea, 610);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] asteriskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] astrskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] causeofdthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] clamamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] dtofdeathOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] intdayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] interestOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] netOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ofchargeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] otheradjstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] paycurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] policyloanOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] proceedsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] totclaimOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] zrcshamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	//ILIFE-1137
	public FixedLengthStringData[] susamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] nextinsamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);	
	
	public FixedLengthStringData[] zhldclmvOut = FLSArrayPartOfStructure(12, 1, outputIndicators,420 );//BRD-34
	public FixedLengthStringData[] zhldclmaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);//BRD-34
	public FixedLengthStringData[] bnfyingOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] confirmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] itstrateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);  
	public FixedLengthStringData[] claimnumberOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] aacctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] claimnotesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	 public FixedLengthStringData[] investresOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	 public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dtofdeathDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5284screenWritten = new LongData(0);
	public LongData S5284protectWritten = new LongData(0);	
	public static final int[] pfInds = new int[] {9, 17, 22, 4, 18, 23, 5, 15, 6, 24, 16, 1, 2, 3, 21, 20, 10};

	public boolean hasSubfile() {
		return false;
	}


	public S5284ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(netOut,new String[] {null, null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(intdayOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(interestOut,new String[] {null, null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ofchargeOut,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(proceedsOut,new String[] {null, null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paycurrOut,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnfyingOut,new String[] {"21",null, "-21","22", null, null, null, null, null, null, null, null});
		//ILIFE-1137
		//modified for deat
		fieldIndMap.put(itstrateOut,new String[] {"97","92","-97","93", null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimnumberOut,new String[] {"31",null, "-31","24", null, null, null, null, null, null, null, null});
		fieldIndMap.put(aacctOut,new String[] {"25",null, "-25","26", null, null, null, null, null, null, null, null});
	    fieldIndMap.put(claimnotesOut,new String[] {"27",null, "-27","28", null, null, null, null, null, null, null, null});
	    fieldIndMap.put(investresOut,new String[] {"29",null, "-29","30", null, null, null, null, null, null, null, null});
	        
		/*screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, dtofdeath, policyloan, effdate, causeofdth, otheradjst, currcd, clamamt, reasoncd, resndesc, net, intday, interest, ofcharge, proceeds, totclaim, asterisk, astrsk, paycurr, zrcshamt, susamt, nextinsamt, zhldclmv, zhldclma};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, dtofdeathOut, policyloanOut, effdateOut, causeofdthOut, otheradjstOut, currcdOut, clamamtOut, reasoncdOut, resndescOut, netOut, intdayOut, interestOut, ofchargeOut, proceedsOut, totclaimOut, asteriskOut, astrskOut, paycurrOut, zrcshamtOut, susamtOut, nextinsamtOut,zhldclmvOut, zhldclmaOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, dtofdeathErr, policyloanErr, effdateErr, causeofdthErr, otheradjstErr, currcdErr, clamamtErr, reasoncdErr, resndescErr, netErr, intdayErr, interestErr, ofchargeErr, proceedsErr, totclaimErr, asteriskErr, astrskErr, paycurrErr, zrcshamtErr, susamtErr, nextinsamtErr, zhldclmvErr, zhldclmaErr};*/
		/*
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, dtofdeath, policyloan, effdate, causeofdth, otheradjst, currcd, clamamt, reasoncd, resndesc, net, intday, interest, ofcharge, proceeds, totclaim, asterisk, astrsk, paycurr, zrcshamt};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, dtofdeathOut, policyloanOut, effdateOut, causeofdthOut, otheradjstOut, currcdOut, clamamtOut, reasoncdOut, resndescOut, netOut, intdayOut, interestOut, ofchargeOut, proceedsOut, totclaimOut, asteriskOut, astrskOut, paycurrOut, zrcshamtOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, dtofdeathErr, policyloanErr, effdateErr, causeofdthErr, otheradjstErr, currcdErr, clamamtErr, reasoncdErr, resndescErr, netErr, intdayErr, interestErr, ofchargeErr, proceedsErr, totclaimErr, asteriskErr, astrskErr, paycurrErr, zrcshamtErr};
		*/
		/*screenDateFields = new BaseData[] {occdate, ptdate, btdate, dtofdeath, effdate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, dtofdeathErr, effdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, dtofdeathDisp, effdateDisp};*/
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5284screen.class;
		protectRecord = S5284protect.class;
	}
	
	public int getDataAreaSize()
	{
		return 1302;
	}
	
	public int getDataFieldsSize()
	{
		return 582;
	}
	public int getErrorIndicatorSize()
	{
		return 180;
	}
	public int getOutputFieldSize()
	{
		return 540;
	}
	
	public BaseData[] getscreenFields()
	{
		return new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, dtofdeath, policyloan, effdate, causeofdth, otheradjst, currcd, clamamt, reasoncd, resndesc, net, intday, interest, ofcharge, proceeds, totclaim, asterisk, astrsk, paycurr, zrcshamt, susamt, nextinsamt, zhldclmv, zhldclma, bnfying, confirm, itstrate,claimnumber,aacct,claimnotes,investres,fupflg};
	}
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, dtofdeathOut, policyloanOut, effdateOut, causeofdthOut, otheradjstOut, currcdOut, clamamtOut, reasoncdOut, resndescOut, netOut, intdayOut, interestOut, ofchargeOut, proceedsOut, totclaimOut, asteriskOut, astrskOut, paycurrOut, zrcshamtOut, susamtOut, nextinsamtOut,zhldclmvOut, zhldclmaOut, bnfyingOut, confirmOut, itstrateOut,claimnumberOut,aacctOut,claimnotesOut,investresOut,fupflgOut};
	}
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, dtofdeathErr, policyloanErr, effdateErr, causeofdthErr, otheradjstErr, currcdErr, clamamtErr, reasoncdErr, resndescErr, netErr, intdayErr, interestErr, ofchargeErr, proceedsErr, totclaimErr, asteriskErr, astrskErr, paycurrErr, zrcshamtErr, susamtErr, nextinsamtErr, zhldclmvErr, zhldclmaErr, bnfyingErr, confirmErr, itstrateErr,claimnumberErr,aacctErr,claimnotesErr,investresErr,fupflgErr};
	}
	
	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {occdate, ptdate, btdate, dtofdeath, effdate};
	}
	
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, dtofdeathDisp, effdateDisp};
	}
	

	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {occdateErr, ptdateErr, btdateErr, dtofdeathErr, effdateErr};
	}
	

	public static int[] getScreenSflPfInds()
	{
		return pfInds;
	}


}
