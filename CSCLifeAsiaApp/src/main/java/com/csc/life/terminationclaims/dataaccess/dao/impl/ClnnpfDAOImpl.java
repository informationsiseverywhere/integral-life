package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.ClnnpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * 
 * @author ehyrat
 * DAOImpl related table CLNNPF
 *
 */
public class ClnnpfDAOImpl extends BaseDAOImpl<Clnnpf> implements ClnnpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(ClnnpfDAOImpl.class);

	@Override
	public boolean insertClnnpf(Clnnpf clnnpf) {
		Timestamp ts = Timestamp.valueOf(clnnpf.getDatime());
		StringBuilder sb = new StringBuilder("INSERT INTO CLNNPF(CLNNCOY,NOTIFINUM,LIFENUM,CLAIMANT,RELATIONSHIP,NOTIFINOTES,USRPRF,JOBNM, DATIME, CLAIMNO) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?)");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
        boolean result = false;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, clnnpf.getClnncoy());
			ps.setString(2, clnnpf.getNotifinum());
			ps.setString(3, clnnpf.getLifenum());
			ps.setString(4, clnnpf.getClaimant());
			ps.setString(5, clnnpf.getRelationship());
			ps.setString(6, clnnpf.getNotificationNote());
			ps.setString(7, getUsrprf());			    
			ps.setString(8, getJobnm());		
			ps.setTimestamp(9, ts);
			ps.setString(10,clnnpf.getClaimno());
			ps.executeUpdate();
			result = true;
		}catch(SQLException e) {
			LOGGER.error("insertClnnpf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("insertClnnpf()", ex); /* IJTI-1479 */
			throw new RuntimeException(ex);
		}
		finally {
			close(ps, null);			
		}
		
		return result;
	}
	
	@Override
	public Clnnpf getClnnpfBySeq(long uniqueNumber) {
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER,NOTIFINOTES,DATIME,USRPRF FROM CLNNPF WHERE UNIQUE_NUMBER = ?");
		LOGGER.info(sb.toString());
		ResultSet sqlclntpf1rs = null;
	    PreparedStatement psClntSelect = getPrepareStatement(sb.toString());
	    Clnnpf Clnnpf = null;
		 try {
			 	psClntSelect.setLong(1, uniqueNumber);
	            sqlclntpf1rs = executeQuery(psClntSelect);
	            while (sqlclntpf1rs.next()) {
	            		Clnnpf = new Clnnpf();
	            		Clnnpf.setUniqueNumber(sqlclntpf1rs.getLong(1));
	            		Clnnpf.setNotificationNote(sqlclntpf1rs.getString(2));
	            		Clnnpf.setDatime(sqlclntpf1rs.getString(3));
	            		Clnnpf.setUsrprf(sqlclntpf1rs.getString(4));
	            }
	        } catch (SQLException e) {
			LOGGER.error("getClnnpfList()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        }catch(Exception ex){
			LOGGER.error("getClnnpfList()", ex); /* IJTI-1479 */
				ex.printStackTrace();
			} finally {
	            close(psClntSelect, sqlclntpf1rs);
	        }
		return Clnnpf;
	}

	@Override
	public List<Clnnpf> getClnnpfList(String clnncoy, String notifinum, String claimno) {
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER,NOTIFINUM,NOTIFINOTES,DATIME,USRPRF,LIFENUM FROM CLNNPF WHERE CLNNCOY = ? AND (NOTIFINUM = ? AND CLAIMNO = ?) ORDER BY DATIME DESC");	//IBPLIFE-906
		LOGGER.info(sb.toString());
		ResultSet sqlclntpf1rs = null;
	    PreparedStatement psClntSelect = getPrepareStatement(sb.toString());
	    List<Clnnpf> resultList = new ArrayList<Clnnpf>();
	    try {
	    psClntSelect.setString(1, clnncoy);
	    psClntSelect.setString(2, notifinum);
	    psClntSelect.setString(3, claimno);
	            sqlclntpf1rs = executeQuery(psClntSelect);
	            while (sqlclntpf1rs.next()) {
	            	   Clnnpf Clnnpf = new Clnnpf();
	            		Clnnpf.setUniqueNumber(sqlclntpf1rs.getLong(1));
	            		Clnnpf.setNotifinum(sqlclntpf1rs.getString(2));
	            		Clnnpf.setNotificationNote(sqlclntpf1rs.getString(3));
	            		Clnnpf.setDatime(sqlclntpf1rs.getString(4));
	            		Clnnpf.setUsrprf(sqlclntpf1rs.getString(5));
	            		Clnnpf.setLifenum(sqlclntpf1rs.getString(6));	//IBPLIFE-906
	            		resultList.add(Clnnpf);
	            }
	        } catch (SQLException e) {
			LOGGER.error("getClnnpfList()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        }catch(Exception ex){
			LOGGER.error("getClnnpfList()", ex); /* IJTI-1479 */
				ex.printStackTrace();
			} finally {
	            close(psClntSelect, sqlclntpf1rs);
	        }
		return resultList;
	}

	@Override
	public int removeClnnpfRecord(long uniqueNumber) {
		StringBuilder sb = new StringBuilder("DELETE FROM CLNNPF WHERE UNIQUE_NUMBER = ?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		int executeUpdate;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setLong(1, uniqueNumber);
			executeUpdate = ps.executeUpdate();
		}catch(SQLException e) {
			LOGGER.error("deleteClnnpf()", e); /* IJTI-1479 */	
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("deleteClnnpf()", ex); /* IJTI-1479 */	
			throw new RuntimeException(ex);
		}finally {
			close(ps, null);			
		}
		return executeUpdate;
	}

	@Override

	public int updateClnnpf(Clnnpf Clnnpf) {
		Timestamp ts = Timestamp.valueOf(Clnnpf.getDatime());
		StringBuilder sb = new StringBuilder("UPDATE CLNNPF SET NOTIFINOTES=?, DATIME=?  WHERE UNIQUE_NUMBER = ?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		int executeUpdate;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Clnnpf.getNotificationNote());
			ps.setTimestamp(2, ts);
			ps.setLong(3, Clnnpf.getUniqueNumber());
			executeUpdate = ps.executeUpdate();
		}catch(SQLException e) {
			LOGGER.error("updateClnnpf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("updateClnnpf()", ex); /* IJTI-1479 */
			throw new RuntimeException(ex);
		}finally {
			close(ps, null);			
		}
		return executeUpdate;
	}
	
	@Override
	public int updateClnnpfClaimno(String claimno, String notifinum) {
		StringBuilder sb = new StringBuilder("UPDATE CLNNPF SET CLAIMNO = ? WHERE NOTIFINUM=?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		int executeUpdate;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, claimno.trim());
			ps.setString(2, notifinum.trim());
			executeUpdate = ps.executeUpdate();
		}catch(SQLException e) {
			LOGGER.error("updateClnnpfClaimno()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("updateClnnpfClaimno()", ex); /* IJTI-1479 */	
			throw new RuntimeException(ex);
		}finally {
			close(ps, null);			
		}
		return executeUpdate;
	}

}
