/*
 * File: P5240.java
 * Date: 30 August 2009 0:22:04
 * Author: Quipoz Limited
 *
 * Class transformed from P5240.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.RegtlnbTableDAM;
import com.csc.life.terminationclaims.screens.S5240ScreenVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  P5240 - Temporary Regular Payments Bank Details.
*  ------------------------------------------------
*
*  This program will provide a window in  which  the  user  may
*  enter  details of the bank account through which the payment
*  will be made.
*
*  If the user has selected a  payment  type  whose  rules  say
*  that  Bank  Details are required then the system will window
*  automatically to this program.
*
*  Initialise.
*  -----------
*
*  Retrieve the current payment details from  REGTLNB.  Store
*  the currency  in  a  hidden screen field so that it is
*  available for further windowing.
*
*  If the current bank number and account number are not  blank
*  display  the  bank  details  from  BABR  and  client account
*  details from CLBL.
*
*  Validation.
*  -----------
*
*  If in enquiry mode, (WSSP-FLAG is 'I'), protect  the  screen
*  prior to output.
*
*  Display the screen with the I/O module.
*
*  If  'KILL'  is  requested  or  in  Enquiry  mode,  skip  the
*  validation.
*
*  Validate the individual fields as follows:
*
*  Check that the bank identification exists on  BABR  and  the
*  combination  of  bank  and account number exist on CLBL. The
*  account currency must match with the payment  currency  from
*  REGTLNB.
*
*  Obtain the bank and account descriptions for confirmation.
*
*  If  'CALC'  was  requested  re-display the screen to display
*  the bank/branch and account descriptions.
*
*  Updating.
*  ---------
*
*  Skip the updating if 'KILL' was pressed  or  if  in  enquiry
*  mode.
*
*  Store  the  bank  branch  code  and client account number in
*  REGTLNB.
*
*  Perform a KEEPS on REGTLNB.
*
*  Where Next.
*  -----------
*
*  Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P5240 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5240");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsbbBankdesc = new FixedLengthStringData(60);
	private FixedLengthStringData wsbbBankdescLine1 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 0);
	private FixedLengthStringData wsbbBankdescLine2 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 30);
		/* ERRORS */
	private String e186 = "E186";
	private String e756 = "E756";
	private String g600 = "G600";
	private String h130 = "H130";
	private String curs = "CURS";
	private String regtlnbrec = "REGTLNBREC";
		/*Bank/Branch Name File*/
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*Logical File: Client/Bank Account Record*/
	private ClblTableDAM clblIO = new ClblTableDAM();
		/*Regular Payment Temporary Record New Bus*/
	private RegtlnbTableDAM regtlnbIO = new RegtlnbTableDAM();
	private Wssplife wssplife = new Wssplife();
	private S5240ScreenVars sv = ScreenProgram.getScreenVars( S5240ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1090,
		preExit,
		checkForErrors2080,
		exit2090,
		exit3090
	}

	public P5240() {
		super();
		screenVars = sv;
		new ScreenModel("S5240", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		regtlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, regtlnbIO);
		if (isNE(regtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regtlnbIO.getParams());
			fatalError600();
		}
		sv.payrnum.set(regtlnbIO.getPayclt());
		sv.numsel.set(regtlnbIO.getPayclt());
		sv.currcode.set(regtlnbIO.getCurrcd());
		if (isEQ(regtlnbIO.getBankkey(),SPACES)
		&& isEQ(regtlnbIO.getBankacckey(),SPACES)) {
			goTo(GotoLabel.exit1090);
		}
		clblIO.setBankkey(regtlnbIO.getBankkey());
		sv.bankkey.set(regtlnbIO.getBankkey());
		clblIO.setBankacckey(regtlnbIO.getBankacckey());
		sv.bankacckey.set(regtlnbIO.getBankacckey());
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.flag,"I"))
		&& (isNE(wsspcomn.flag,"D"))) {
			if (isNE(clblIO.getClntnum(),sv.payrnum)) {
				sv.bankkey.set(SPACES);
				sv.bankacckey.set(SPACES);
				sv.facthous.set(SPACES);
				goTo(GotoLabel.exit1090);
			}
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)
		&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		if (isEQ(babrIO.getStatuz(),varcom.oK)) {
			wsbbBankdesc.set(babrIO.getBankdesc());
			sv.bankdesc.set(wsbbBankdescLine1);
			sv.branchdesc.set(wsbbBankdescLine2);
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if ((isEQ(wsspcomn.flag,"I"))
		|| (isEQ(wsspcomn.flag,"D"))) {
			scrnparams.function.set("PROT");
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if ((isNE(scrnparams.statuz,varcom.kill))
		&& (isNE(scrnparams.statuz,varcom.calc))
		&& (isNE(scrnparams.statuz,varcom.oK))) {
			scrnparams.errorCode.set(curs);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validate2020()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.bankkey,SPACES)) {
			sv.bankkeyErr.set(e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if ((isNE(babrIO.getStatuz(),varcom.oK))
		&& (isNE(babrIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(e756);
			goTo(GotoLabel.checkForErrors2080);
		}
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		if (isEQ(sv.bankacckey,SPACES)) {
			sv.bankacckeyErr.set(e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if ((isNE(clblIO.getStatuz(),varcom.oK))
		&& (isNE(clblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isEQ(clblIO.getStatuz(),varcom.mrnf)) {
			sv.bankacckeyErr.set(g600);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(regtlnbIO.getCurrcd(),clblIO.getCurrcode())) {
			sv.bankacckeyErr.set(h130);
			goTo(GotoLabel.checkForErrors2080);
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		regtlnbIO.setBankkey(sv.bankkey);
		regtlnbIO.setBankacckey(sv.bankacckey);
		regtlnbIO.setFormat(regtlnbrec);
		regtlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, regtlnbIO);
		if (isNE(regtlnbIO.getStatuz(),varcom.oK)) {
			regtlnbIO.setParams(regtlnbIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
