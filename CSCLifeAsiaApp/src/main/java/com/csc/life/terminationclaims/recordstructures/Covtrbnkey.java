package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:30
 * Description:
 * Copybook name: COVTRBNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covtrbnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covtrbnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covtrbnKey = new FixedLengthStringData(64).isAPartOf(covtrbnFileKey, 0, REDEFINE);
  	public FixedLengthStringData covtrbnChdrcoy = new FixedLengthStringData(1).isAPartOf(covtrbnKey, 0);
  	public FixedLengthStringData covtrbnChdrnum = new FixedLengthStringData(8).isAPartOf(covtrbnKey, 1);
  	public FixedLengthStringData covtrbnLife = new FixedLengthStringData(2).isAPartOf(covtrbnKey, 9);
  	public FixedLengthStringData covtrbnCoverage = new FixedLengthStringData(2).isAPartOf(covtrbnKey, 11);
  	public FixedLengthStringData covtrbnRider = new FixedLengthStringData(2).isAPartOf(covtrbnKey, 13);
  	public PackedDecimalData covtrbnSeqnbr = new PackedDecimalData(3, 0).isAPartOf(covtrbnKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(covtrbnKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covtrbnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covtrbnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}