/*
 * File: Dcc002.java
 * Date: 29 August 2009 22:45:19
 * Author: Quipoz Limited
 * 
 * Class transformed from DCC002.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrndthTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsdccTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  CALCULATE BID VALUE OF UNITS.
*
*          DEATH CALCULATION METHOD 2.
*          ---------------------------
*
*          Returns the Bid Value of the investment units.
*
*          This program is an item entry on T6598, the death claim
*          subroutine method table. This is the calculation process
*          for obtaining the Bid value of the investments the amount
*          is returned in the currency of the fund.
*
*          The following is the linkage information passed to this
*          subroutine:-
*
*                     - company
*                     - contract header number
*                     - life number
*                     - joint-life number
*                     - coverage
*                     - rider
*                     - crtable
*                     - effective date
*                     - language
*                     - estimated value
*                     - actual value
*                     - currency
*                     - element code
*                     - description
*                     - type code
*                     - status
*
*            PROCESSING.
*            ----------
*            If it is the first time through this routine:-
*
*          N.B. - accumulate all records/amounts for the same fund,
*          including initial and accumulation.
*
*          - set-up the key and BEGN option  and read fund UTRS
*                  keyed:  Coy, Chdr, Life, Jlife, Coverage, Rider
*
*              - for each fund on component get the now bid price
*                  read the VPRC (prices file), keyed on:
*                  - Virtual fund, Unit type and Effective-date.
*
*              - calc value to estimate value
*                multiply the number-of-units by the bid price
*                returned.
*
*              - return amt, fund, desc, type = f, ccy fund
*          else
*              - set status = ENDP.
*
*          These fields are returned to the main transaction for
*          further processing.
*
*****************************************************************
* </pre>
*/
public class Dcc002 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "DCC002";
	protected String g094 = "G094";
		/* TABLES */
	private String t5515 = "T5515";
	private String hitralorec = "HITRALOREC";
	private String utrndthrec = "UTRNDTHREC";

	private FixedLengthStringData wsaaUtype = new FixedLengthStringData(1).init(SPACES);
	private Validator initialUnits = new Validator(wsaaUtype, "I");
	private PackedDecimalData wsaaInitialBidPrice = new PackedDecimalData(9, 5).init(0);
	protected PackedDecimalData wsaaAccumBidPrice = new PackedDecimalData(9, 5).init(0);
	private PackedDecimalData wsaaInitialAmountX = new PackedDecimalData(17, 5).init(0);
	private PackedDecimalData wsaaInitialAmount = new PackedDecimalData(17, 5).init(0);
	protected PackedDecimalData wsaaAccumAmountX = new PackedDecimalData(17, 5).init(0);
	protected PackedDecimalData wsaaAccumAmount = new PackedDecimalData(17, 5).init(0);
	protected FixedLengthStringData wsaaVirtualFund = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaUnitType = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaVirtualFundKey = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaUnitTypeKey = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaUtrnVirtualFund = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaUtrnUnitType = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaUtrnChdrcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaUtrnChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaUtrnCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaUtrnRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaUtrnLife = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaSwitch, "Y");
	protected Deathrec deathrec = new Deathrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Unprocessed HITR record file*/
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
		/*Interest Bearing Transaction Summary*/
	private HitsTableDAM hitsIO = new HitsTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected Syserrrec syserrrec = new Syserrrec();
	private T5515rec t5515rec = new T5515rec();
		/*Unit Transactions Death Claim*/
	private UtrndthTableDAM utrndthIO = new UtrndthTableDAM();
		/*Unit Transactions for Death Claim Calcs*/
	private UtrsdccTableDAM utrsdccIO = new UtrsdccTableDAM();
	private Varcom varcom = new Varcom();
		/*VPRCPF view for now price for units*/
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readAccum122, 
		exit125, 
		exit390, 
		doCheck410, 
		exit490, 
		exit505, 
		checkUtrn610, 
		utrnCompare630, 
		resetDetails640, 
		exit690, 
		a190Exit, 
		a320DoCheck, 
		a390Exit, 
		a420CheckHitr, 
		a430HitrCompare, 
		a440ResetDetails, 
		a490Exit, 
		exit9020
	}

	public Dcc002() {
		super();
	}

public void mainline(Object... parmArray)
	{
		deathrec.deathRec = convertAndSetParam(deathrec.deathRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (firstTime.isTrue()) {
			wsaaChdrcoy.set(deathrec.chdrChdrcoy);
			wsaaUtrnChdrcoy.set(deathrec.chdrChdrcoy);
			wsaaChdrnum.set(deathrec.chdrChdrnum);
			wsaaUtrnChdrnum.set(deathrec.chdrChdrnum);
			wsaaCoverage.set(deathrec.covrCoverage);
			wsaaUtrnCoverage.set(deathrec.covrCoverage);
			wsaaRider.set(deathrec.covrRider);
			wsaaUtrnRider.set(deathrec.covrRider);
			wsaaLife.set(deathrec.lifeLife);
			wsaaUtrnLife.set(deathrec.lifeLife);
			wsaaAccumAmount.set(ZERO);
			wsaaInitialAmount.set(ZERO);
			deathrec.estimatedVal.set(ZERO);
			deathrec.actualVal.set(ZERO);
			deathrec.valueType.set(SPACES);
			wsaaUtrnVirtualFund.set(SPACES);
			wsaaUtrnUnitType.set(SPACES);
			wsaaUnitType.set(SPACES);
			wsaaVirtualFund.set(SPACES);
			wsaaVirtualFundKey.set(deathrec.virtualFundStore);
			wsaaUnitTypeKey.set(deathrec.unitTypeStore);
			deathrec.status.set(varcom.oK);
			wsaaSwitch.set("N");
		}
		if (isEQ(deathrec.endf,"Y")) {
			deathrec.status.set(varcom.endp);
		}
		if (isEQ(deathrec.status,varcom.oK)) {
			if (isEQ(deathrec.endf,"I")) {
				a100ReadInterestBearing();
			}
			else {
				readFunds500();
			}
		}
		if (isEQ(deathrec.status,varcom.endp)) {
			wsaaSwitch.set("Y");
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readVprnudl120()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					read121();
				}
				case readAccum122: {
					readAccum122();
				}
				case exit125: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read121()
	{
		if (isEQ(wsaaInitialAmount,ZERO)) {
			wsaaInitialAmountX.set(ZERO);
			goTo(GotoLabel.readAccum122);
		}
		vprnudlIO.setDataArea(SPACES);
		vprnudlIO.setCompany(deathrec.chdrChdrcoy);
		vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
		vprnudlIO.setUnitType("I");
		vprnudlIO.setEffdate(deathrec.effdate);
		vprnudlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY", "VRTFND", "ULTYPE");
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund,vprnudlIO.getUnitVirtualFund())
		|| isNE(deathrec.chdrChdrcoy,vprnudlIO.getCompany())
		|| isNE(vprnudlIO.getUnitType(),"I")
		|| isEQ(vprnudlIO.getStatuz(),varcom.endp)) {
			deathrec.status.set(varcom.mrnf);
			vprnudlIO.setFunction(varcom.begn);
			vprnudlIO.setStatuz(varcom.endp);
			vprnudlIO.setCompany(deathrec.chdrChdrcoy);
			vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
			vprnudlIO.setUnitType("I");
			vprnudlIO.setEffdate(deathrec.effdate);
			syserrrec.statuz.set(g094);
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		wsaaInitialBidPrice.set(vprnudlIO.getUnitBidPrice());
		compute(wsaaInitialAmountX, 6).setRounded(mult(wsaaInitialBidPrice,wsaaInitialAmount));
	}

protected void readAccum122()
	{
		if (isEQ(wsaaAccumAmount,ZERO)) {
			wsaaAccumAmountX.set(ZERO);
			goTo(GotoLabel.exit125);
		}
		vprnudlIO.setDataArea(SPACES);
		vprnudlIO.setCompany(deathrec.chdrChdrcoy);
		vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
		vprnudlIO.setUnitType("A");
		vprnudlIO.setEffdate(deathrec.effdate);
		vprnudlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		vprnudlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprnudlIO.setFitKeysSearch("COMPANY", "VRTFND", "ULTYPE");
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund,vprnudlIO.getUnitVirtualFund())
		|| isNE(vprnudlIO.getUnitType(),"A")
		|| isNE(deathrec.chdrChdrcoy,vprnudlIO.getCompany())
		|| isEQ(vprnudlIO.getStatuz(),varcom.endp)) {
			deathrec.status.set(varcom.mrnf);
			vprnudlIO.setFunction(varcom.begn);
			vprnudlIO.setStatuz(varcom.endp);
			vprnudlIO.setCompany(deathrec.chdrChdrcoy);
			vprnudlIO.setUnitVirtualFund(wsaaVirtualFund);
			vprnudlIO.setUnitType("A");
			vprnudlIO.setEffdate(deathrec.effdate);
			syserrrec.statuz.set(g094);
			syserrrec.params.set(vprnudlIO.getParams());
			fatalError9000();
		}
		wsaaAccumBidPrice.set(vprnudlIO.getUnitBidPrice());
		compute(wsaaAccumAmountX, 6).setRounded(mult(wsaaAccumBidPrice,wsaaAccumAmount));
	}

protected void checkEof200()
	{
		/*EOF*/
		if (initialUnits.isTrue()) {
			deathrec.estimatedVal.set(wsaaInitialAmountX);
			deathrec.fieldType.set("I");
		}
		else {
			deathrec.fieldType.set("A");
			deathrec.estimatedVal.set(wsaaAccumAmountX);
		}
		deathrec.valueType.set("F");
		deathrec.element.set(wsaaVirtualFund);
		readTables300();
		/*EXIT*/
	}

protected void readTables300()
	{
		try {
			go300();
		}
		catch (GOTOException e){
		}
	}

protected void go300()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescitem(wsaaVirtualFund);
		descIO.setDesctabl(t5515);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(deathrec.chdrChdrcoy);
		descIO.setLanguage(deathrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			deathrec.description.set(descIO.getLongdesc());
		}
		else {
			deathrec.description.fill("?");
		}
		deathrec.element.set(wsaaVirtualFund);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(deathrec.chdrChdrcoy);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(wsaaVirtualFund);
		itdmIO.setItmfrm("99999999");
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError9000();
		}
		if (isNE(wsaaVirtualFund,itdmIO.getItemitem())
		|| isNE(deathrec.chdrChdrcoy,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			deathrec.currcode.set(SPACES);
			goTo(GotoLabel.exit390);
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
			deathrec.currcode.set(t5515rec.currcode);
		}
	}

protected void checkUnprocessedUtrns400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para401();
				}
				case doCheck410: {
					doCheck410();
				}
				case exit490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para401()
	{
		deathrec.status.set(varcom.oK);
		utrndthIO.setChdrcoy(deathrec.chdrChdrcoy);
		utrndthIO.setChdrnum(deathrec.chdrChdrnum);
		utrndthIO.setLife(deathrec.lifeLife);
		utrndthIO.setCoverage(deathrec.covrCoverage);
		utrndthIO.setRider(deathrec.covrRider);
		utrndthIO.setPlanSuffix(ZERO);
		utrndthIO.setProcSeqNo(ZERO);
		utrndthIO.setTranno(ZERO);
	}

protected void doCheck410()
	{
		SmartFileCode.execute(appVars, utrndthIO);
		if ((isNE(utrndthIO.getStatuz(),varcom.oK))
		&& (isNE(utrndthIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrndthIO.getParams());
			fatalError9000();
		}
		if ((isNE(utrndthIO.getChdrcoy(),deathrec.chdrChdrcoy))
		|| (isNE(utrndthIO.getChdrnum(),deathrec.chdrChdrnum))
		|| (isNE(utrndthIO.getLife(),deathrec.lifeLife))
		|| (isNE(utrndthIO.getCoverage(),deathrec.covrCoverage))
		|| (isNE(utrndthIO.getRider(),deathrec.covrRider))
		|| (isEQ(utrndthIO.getStatuz(),varcom.endp))) {
			utrndthIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit490);
		}
		deathrec.element.set(utrndthIO.getUnitVirtualFund());
		if (isEQ(utrndthIO.getSvp(),1)) {
			if (isEQ(utrndthIO.getUnitType(),"I")) {
				wsaaInitialAmount.add(utrndthIO.getContractAmount());
				wsaaUtype.set("I");
			}
			else {
				wsaaAccumAmount.add(utrndthIO.getContractAmount());
				wsaaUtype.set("A");
			}
		}
		utrndthIO.setFunction(varcom.nextr);
		goTo(GotoLabel.doCheck410);
	}

protected void readFunds500()
	{
		try {
			para501();
		}
		catch (GOTOException e){
		}
	}

protected void para501()
	{
		utrsdccIO.setParams(SPACES);
		utrsdccIO.setChdrcoy(wsaaChdrcoy);
		utrsdccIO.setChdrnum(wsaaChdrnum);
		utrsdccIO.setLife(wsaaLife);
		utrsdccIO.setCoverage(wsaaCoverage);
		utrsdccIO.setRider(wsaaRider);
		utrsdccIO.setUnitVirtualFund(wsaaVirtualFundKey);
		utrsdccIO.setUnitType(wsaaUnitTypeKey);
		utrsdccIO.setPlanSuffix(ZERO);
		utrsdccIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, utrsdccIO);
		if (isNE(utrsdccIO.getStatuz(),varcom.oK)
		&& isNE(utrsdccIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsdccIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsdccIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(utrsdccIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(utrsdccIO.getLife(),deathrec.lifeLife)
		|| isNE(utrsdccIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(utrsdccIO.getRider(),deathrec.covrRider)
		|| isEQ(utrsdccIO.getStatuz(),varcom.endp)) {
			deathrec.status.set(varcom.endp);
			utrsdccIO.setStatuz(varcom.endp);
			deathrec.endf.set("I");
			wsaaSwitch.set("Y");
			noUtrsRecords600();
			if (isEQ(deathrec.status,varcom.endp)) {
				deathrec.virtualFundStore.set(SPACES);
				deathrec.status.set(varcom.oK);
				goTo(GotoLabel.exit505);
			}
			else {
				wsaaVirtualFund.set(wsaaUtrnVirtualFund);
				wsaaUnitType.set(wsaaUtrnUnitType);
				readVprnudl120();
				checkEof200();
				deathrec.virtualFundStore.set(SPACES);
				goTo(GotoLabel.exit505);
			}
		}
		wsaaUnitType.set(utrsdccIO.getUnitType());
		wsaaVirtualFund.set(utrsdccIO.getUnitVirtualFund());
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		deathrec.estimatedVal.set(ZERO);
		deathrec.actualVal.set(ZERO);
		while ( !(isEQ(utrsdccIO.getStatuz(),varcom.endp)
		|| isNE(utrsdccIO.getUnitVirtualFund(),wsaaVirtualFund)
		|| isNE(utrsdccIO.getUnitType(),wsaaUnitType))) {
			accumAmount510();
		}
		
		if (isEQ(utrsdccIO.getStatuz(),varcom.oK)
		|| isEQ(utrsdccIO.getStatuz(),varcom.endp)) {
			readVprnudl120();
			checkEof200();
			deathrec.element.set(wsaaVirtualFund);
			deathrec.fieldType.set(wsaaUnitType);
			wsaaLife.set(utrsdccIO.getLife());
			wsaaRider.set(utrsdccIO.getRider());
			wsaaCoverage.set(utrsdccIO.getCoverage());
			wsaaChdrnum.set(utrsdccIO.getChdrnum());
			deathrec.virtualFundStore.set(utrsdccIO.getUnitVirtualFund());
			deathrec.unitTypeStore.set(utrsdccIO.getUnitType());
			if (isEQ(deathrec.endf,"I")) {
				deathrec.virtualFundStore.set(SPACES);
			}
			wsaaSwitch.set("Y");
		}
	}

protected void accumAmount510()
	{
		para511();
	}

protected void para511()
	{
		if (isEQ(utrsdccIO.getUnitType(),"I")) {
			wsaaInitialAmount.add(utrsdccIO.getCurrentDunitBal());
			wsaaUtype.set("I");
		}
		else {
			wsaaAccumAmount.add(utrsdccIO.getCurrentDunitBal());
			wsaaUtype.set("A");
		}
		utrndthIO.setParams(SPACES);
		utrndthIO.setUnitVirtualFund(utrsdccIO.getUnitVirtualFund());
		utrndthIO.setUnitType(utrsdccIO.getUnitType());
		utrndthIO.setProcSeqNo(ZERO);
		utrndthIO.setTranno(ZERO);
		utrndthIO.setFormat(utrndthrec);
		utrndthIO.setFunction(varcom.begn);
		while ( !(isEQ(utrndthIO.getStatuz(),varcom.endp))) {
			checkUnprocessedUtrns400();
		}
		
		utrsdccIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsdccIO);
		if (isNE(utrsdccIO.getStatuz(),varcom.oK)
		&& isNE(utrsdccIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsdccIO.getParams());
			fatalError9000();
		}
		if (isNE(utrsdccIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(utrsdccIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(utrsdccIO.getLife(),deathrec.lifeLife)
		|| isNE(utrsdccIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(utrsdccIO.getRider(),deathrec.covrRider)
		|| isEQ(utrsdccIO.getStatuz(),varcom.endp)) {
			utrsdccIO.setStatuz(varcom.endp);
			deathrec.endf.set("I");
			wsaaSwitch.set("Y");
		}
	}

protected void noUtrsRecords600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para601();
				}
				case checkUtrn610: {
					checkUtrn610();
				}
				case utrnCompare630: {
					utrnCompare630();
				}
				case resetDetails640: {
					resetDetails640();
					nextr660();
				}
				case exit690: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para601()
	{
		utrndthIO.setParams(SPACES);
		utrndthIO.setChdrcoy(wsaaUtrnChdrcoy);
		utrndthIO.setChdrnum(wsaaUtrnChdrnum);
		utrndthIO.setLife(wsaaUtrnLife);
		utrndthIO.setCoverage(wsaaUtrnCoverage);
		utrndthIO.setRider(wsaaUtrnRider);
		utrndthIO.setUnitVirtualFund(wsaaUtrnVirtualFund);
		utrndthIO.setUnitType(wsaaUtrnUnitType);
		utrndthIO.setPlanSuffix(ZERO);
		utrndthIO.setProcSeqNo(ZERO);
		utrndthIO.setTranno(ZERO);
		utrndthIO.setFormat(utrndthrec);
		utrndthIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrndthIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrndthIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
	}

protected void checkUtrn610()
	{
		SmartFileCode.execute(appVars, utrndthIO);
		if ((isNE(utrndthIO.getStatuz(),varcom.oK))
		&& (isNE(utrndthIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(utrndthIO.getParams());
			fatalError9000();
		}
		if ((isNE(utrndthIO.getChdrcoy(),wsaaUtrnChdrcoy))
		|| (isNE(utrndthIO.getChdrnum(),wsaaUtrnChdrnum))
		|| (isNE(utrndthIO.getLife(),wsaaUtrnLife))
		|| (isNE(utrndthIO.getCoverage(),wsaaUtrnCoverage))
		|| (isNE(utrndthIO.getRider(),wsaaUtrnRider))
		|| (isEQ(utrndthIO.getStatuz(),varcom.endp))) {
			utrndthIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit690);
		}
		if ((isNE(wsaaUtrnVirtualFund,SPACES))
		&& (isNE(wsaaUtrnUnitType,SPACES))) {
			goTo(GotoLabel.utrnCompare630);
		}
		else {
			goTo(GotoLabel.resetDetails640);
		}
	}

protected void utrnCompare630()
	{
		if ((isNE(wsaaUtrnVirtualFund,utrndthIO.getUnitVirtualFund()))
		&& (isNE(wsaaUtrnUnitType,utrndthIO.getUnitType()))) {
			goTo(GotoLabel.exit690);
		}
	}

protected void resetDetails640()
	{
		wsaaUtrnVirtualFund.set(utrndthIO.getUnitVirtualFund());
		wsaaUtrnUnitType.set(utrndthIO.getUnitType());
		wsaaUtrnChdrcoy.set(utrndthIO.getChdrcoy());
		wsaaUtrnChdrnum.set(utrndthIO.getChdrnum());
		wsaaUtrnLife.set(utrndthIO.getLife());
		wsaaUtrnCoverage.set(utrndthIO.getCoverage());
		wsaaUtrnRider.set(utrndthIO.getRider());
		if (isEQ(utrndthIO.getSvp(),1)) {
			if (isEQ(utrndthIO.getUnitType(),"I")) {
				wsaaInitialAmount.add(utrndthIO.getContractAmount());
				wsaaUtype.set("I");
			}
			else {
				wsaaAccumAmount.add(utrndthIO.getContractAmount());
				wsaaUtype.set("A");
			}
		}
		deathrec.status.set(varcom.oK);
	}

protected void nextr660()
	{
		utrndthIO.setFunction(varcom.nextr);
		goTo(GotoLabel.checkUtrn610);
	}

protected void a100ReadInterestBearing()
	{
		try {
			a110Para();
		}
		catch (GOTOException e){
		}
	}

protected void a110Para()
	{
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(wsaaChdrcoy);
		hitsIO.setChdrnum(wsaaChdrnum);
		hitsIO.setLife(wsaaLife);
		hitsIO.setCoverage(wsaaCoverage);
		hitsIO.setRider(wsaaRider);
		hitsIO.setPlanSuffix(ZERO);
		hitsIO.setZintbfnd(wsaaVirtualFundKey);
		hitsIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(hitsIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(hitsIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(hitsIO.getLife(),deathrec.lifeLife)
		|| isNE(hitsIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(hitsIO.getRider(),deathrec.covrRider)
		|| isEQ(hitsIO.getStatuz(),varcom.endp)) {
			deathrec.endf.set("Y");
			deathrec.status.set(varcom.endp);
			a400NoHitsRecords();
			if (isNE(deathrec.status,varcom.endp)) {
				wsaaVirtualFund.set(wsaaUtrnVirtualFund);
				a200CheckEof();
			}
			goTo(GotoLabel.a190Exit);
		}
		wsaaAccumAmount.set(ZERO);
		wsaaInitialAmount.set(ZERO);
		deathrec.estimatedVal.set(ZERO);
		deathrec.actualVal.set(ZERO);
		wsaaVirtualFund.set(hitsIO.getZintbfnd());
		while ( !(isEQ(hitsIO.getStatuz(),varcom.endp)
		|| isNE(hitsIO.getZintbfnd(),wsaaVirtualFund))) {
			a120AccumInterestBearing();
		}
		
		if (isEQ(hitsIO.getStatuz(),varcom.endp)
		|| isEQ(hitsIO.getStatuz(),varcom.oK)) {
			a200CheckEof();
			wsaaVirtualFund.set(hitsIO.getZintbfnd());
			deathrec.virtualFundStore.set(hitsIO.getZintbfnd());
			wsaaLife.set(hitsIO.getLife());
			wsaaRider.set(hitsIO.getRider());
			wsaaCoverage.set(hitsIO.getCoverage());
			wsaaChdrnum.set(hitsIO.getChdrnum());
			wsaaSwitch.set("Y");
		}
	}

protected void a120AccumInterestBearing()
	{
		a121Para();
	}

protected void a121Para()
	{
		wsaaAccumAmount.add(hitsIO.getZcurprmbal());
		hitraloIO.setParams(SPACES);
		hitraloIO.setZintbfnd(hitsIO.getZintbfnd());
		hitraloIO.setFormat(hitralorec);
		hitraloIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitraloIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitraloIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		while ( !(isEQ(hitraloIO.getStatuz(),varcom.endp))) {
			a300CheckUnprocessedHitrs();
		}
		
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError9000();
		}
		if (isNE(hitsIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(hitsIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(hitsIO.getLife(),deathrec.lifeLife)
		|| isNE(hitsIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(hitsIO.getRider(),deathrec.covrRider)
		|| isEQ(hitsIO.getStatuz(),varcom.endp)) {
			hitsIO.setStatuz(varcom.endp);
			deathrec.endf.set("Y");
		}
	}

protected void a200CheckEof()
	{
		/*A210-CHECK*/
		deathrec.estimatedVal.set(wsaaAccumAmount);
		deathrec.fieldType.set("D");
		readTables300();
		/*A290-EXIT*/
	}

protected void a300CheckUnprocessedHitrs()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a310Hitr();
				}
				case a320DoCheck: {
					a320DoCheck();
				}
				case a390Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a310Hitr()
	{
		hitraloIO.setChdrcoy(deathrec.chdrChdrcoy);
		hitraloIO.setChdrnum(deathrec.chdrChdrnum);
		hitraloIO.setLife(deathrec.lifeLife);
		hitraloIO.setCoverage(deathrec.covrCoverage);
		hitraloIO.setRider(deathrec.covrRider);
		hitraloIO.setPlanSuffix(ZERO);
		hitraloIO.setProcSeqNo(ZERO);
		hitraloIO.setTranno(ZERO);
		hitraloIO.setEffdate(ZERO);
	}

protected void a320DoCheck()
	{
		SmartFileCode.execute(appVars, hitraloIO);
		if (isNE(hitraloIO.getStatuz(),varcom.oK)
		&& isNE(hitraloIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitraloIO.getParams());
			fatalError9000();
		}
		if (isNE(hitraloIO.getChdrcoy(),deathrec.chdrChdrcoy)
		|| isNE(hitraloIO.getChdrnum(),deathrec.chdrChdrnum)
		|| isNE(hitraloIO.getLife(),deathrec.lifeLife)
		|| isNE(hitraloIO.getCoverage(),deathrec.covrCoverage)
		|| isNE(hitraloIO.getRider(),deathrec.covrRider)
		|| isEQ(hitraloIO.getStatuz(),varcom.endp)) {
			hitraloIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a390Exit);
		}
		deathrec.element.set(hitraloIO.getZintbfnd());
		if (isEQ(hitraloIO.getSvp(),ZERO)) {
			wsaaAccumAmount.add(hitraloIO.getContractAmount());
		}
		hitraloIO.setFunction(varcom.nextr);
		goTo(GotoLabel.a320DoCheck);
	}

protected void a400NoHitsRecords()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a410Hits();
				}
				case a420CheckHitr: {
					a420CheckHitr();
				}
				case a430HitrCompare: {
					a430HitrCompare();
				}
				case a440ResetDetails: {
					a440ResetDetails();
					a480Nextr();
				}
				case a490Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a410Hits()
	{
		hitraloIO.setParams(SPACES);
		hitraloIO.setChdrcoy(wsaaUtrnChdrcoy);
		hitraloIO.setChdrnum(wsaaUtrnChdrnum);
		hitraloIO.setLife(wsaaUtrnLife);
		hitraloIO.setCoverage(wsaaUtrnCoverage);
		hitraloIO.setRider(wsaaUtrnRider);
		hitraloIO.setZintbfnd(wsaaUtrnVirtualFund);
		hitraloIO.setPlanSuffix(ZERO);
		hitraloIO.setProcSeqNo(ZERO);
		hitraloIO.setTranno(ZERO);
		hitraloIO.setEffdate(ZERO);
		hitraloIO.setFormat(hitralorec);
		hitraloIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitraloIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitraloIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
	}

protected void a420CheckHitr()
	{
		SmartFileCode.execute(appVars, hitraloIO);
		if (isNE(hitraloIO.getStatuz(),varcom.oK)
		&& isNE(hitraloIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitraloIO.getParams());
			fatalError9000();
		}
		if (isNE(hitraloIO.getChdrcoy(),wsaaUtrnChdrcoy)
		|| isNE(hitraloIO.getChdrnum(),wsaaUtrnChdrnum)
		|| isNE(hitraloIO.getLife(),wsaaUtrnLife)
		|| isNE(hitraloIO.getCoverage(),wsaaUtrnCoverage)
		|| isNE(hitraloIO.getRider(),wsaaUtrnRider)
		|| isEQ(hitraloIO.getStatuz(),varcom.endp)) {
			hitraloIO.setStatuz(varcom.endp);
			goTo(GotoLabel.a490Exit);
		}
		if (isNE(wsaaUtrnVirtualFund,SPACES)) {
			goTo(GotoLabel.a430HitrCompare);
		}
		else {
			goTo(GotoLabel.a440ResetDetails);
		}
	}

protected void a430HitrCompare()
	{
		if (isNE(wsaaUtrnVirtualFund,hitraloIO.getZintbfnd())) {
			goTo(GotoLabel.a490Exit);
		}
	}

protected void a440ResetDetails()
	{
		wsaaUtrnVirtualFund.set(hitraloIO.getZintbfnd());
		wsaaUtrnChdrcoy.set(hitraloIO.getChdrcoy());
		wsaaUtrnChdrnum.set(hitraloIO.getChdrnum());
		wsaaUtrnLife.set(hitraloIO.getLife());
		wsaaUtrnCoverage.set(hitraloIO.getCoverage());
		wsaaUtrnRider.set(hitraloIO.getRider());
		if (isEQ(hitraloIO.getSvp(),1)) {
			wsaaAccumAmount.add(hitraloIO.getContractAmount());
		}
		deathrec.status.set(varcom.oK);
	}

protected void a480Nextr()
	{
		hitraloIO.setFunction(varcom.nextr);
		goTo(GotoLabel.a420CheckHitr);
	}

protected void fatalError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					error9010();
				}
				case exit9020: {
					exit9020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void error9010()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit9020);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9020()
	{
		deathrec.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
