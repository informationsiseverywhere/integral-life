package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.HsudpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Hsudpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HsudpfDAOImpl extends BaseDAOImpl<Hsudpf> implements HsudpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(HsudpfDAOImpl.class);
	@Override
	public void insertHsudpfList(List<Hsudpf> pfList) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO HSUDPF(CHDRCOY,CHDRNUM,TRANNO,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,PLNSFX,HACTVAL,HEMV,HCNSTCUR,TYPE_T,USRPRF,JOBNM,DATIME) ");
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for (Hsudpf pf : pfList) {
				int i = 1;
				ps.setString(i++, pf.getChdrcoy());
				ps.setString(i++, pf.getChdrnum());
				ps.setInt(i++, pf.getTranno());
				ps.setString(i++, pf.getLife());
				ps.setString(i++, pf.getJlife());
				ps.setString(i++, pf.getCoverage());
				ps.setString(i++, pf.getRider());
				ps.setString(i++, pf.getCrtable());
				ps.setInt(i++, pf.getPlanSuffix());
				ps.setBigDecimal(i++, pf.getHactval());
				ps.setBigDecimal(i++, pf.getHemv());
				ps.setString(i++, pf.getHcnstcur());
				ps.setString(i++, pf.getFieldType());
				ps.setString(i++, this.getUsrprf());
				ps.setString(i++, this.getJobnm());
				ps.setTimestamp(i++, this.getDatime());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertSurdpfList()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}		
}
