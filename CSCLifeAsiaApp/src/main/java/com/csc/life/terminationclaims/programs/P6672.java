/*
 * File: P6672.java
 * Date: 30 August 2009 0:50:25
 * Author: Quipoz Limited
 * 
 * Class transformed from P6672.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.interestbearing.dataaccess.HitrrnlTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.screens.S6672ScreenVars;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnuddTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcchk;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcchkrec;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*REMARKS.
*
*
*  The submenu program is  automatically  produced  via  Smart,
*  with additional validation required as follows:
*
*
*  Validation
*  ----------
*
*  The contract number must be entered.
*
*  The  contract  number must exist on the contract header file
*  (CHDR).
*
*  The paid to date must equal the  billed  to  date;  if  not,
*  issue an error and disallow selection.
*
*  The  billing  frequency cannot equal zeros (ie. the contract
*  is a  single  premium  only,  hence  there  are  no  premium
*  instalments  and  nothing to 'pay up'); if it does, issue an
*  error and disallow selection.
*
*  Read the Component Status  Table  (T5679)  for  the  submenu
*  transaction.   The contract must have a Contract Risk Status
*  and a Contract Premium Status that matches one of  those  on
*  the table entry.
*
*
*  Update
*  ------
*  Keep  and  softlock  the contract header record for the next
*  programs.
*
*
*****************************************************************
* </pre>
*/
public class P6672 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6672");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).setUnsigned();
	private String wsaaValidStatus = "";
	private String wsaaLapseOk = "";

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1).init(" ");
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t5729 = "T5729";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String itemrec = "ITEMREC";
	private static final String utrnuddrec = "UTRNUDDREC";
	private static final String hitrrnlrec = "HITRRNLREC";
	//private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private HitrrnlTableDAM hitrrnlIO = new HitrrnlTableDAM();
	//private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnuddTableDAM utrnuddIO = new UtrnuddTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcchkrec batcchkrec = new Batcchkrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T5729rec t5729rec = new T5729rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private S6672ScreenVars sv = getLScreenVars();
	private ErrorsInner errorsInner = new ErrorsInner();
	
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	
	//added for ILB-500 start 
	private Itempf itempf = new Itempf();
	protected Chdrpf chdrpf = new Chdrpf();
	
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	//end

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		validate2200, 
		pass2250, 
		verifyBatchControl2300, 
		exit2090, 
		loopCovrmja2650, 
		exit2690, 
		skipSoftlock3150, 
		continue3400, 
		exit3900
	}

	public P6672() {
		super();
		screenVars = sv;
		new ScreenModel("S6672", AppVars.getInstance(), sv);
	}
	protected S6672ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6672ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					wsspcomn.edterror.set(varcom.oK);
					validateAction2100();
				case validate2200: 
					validate2200();
				case pass2250: 
					pass2250();
				case verifyBatchControl2300: 
					verifyBatchControl2300();
					batchProgs2310();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateAction2100()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}
		/*    GO TO 2200-VALIDATE.                                      */
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			goTo(GotoLabel.validate2200);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.verifyBatchControl2300);
		}
	}

protected void validate2200()
	{
		/*    Validate fields*/
		if (isEQ(sv.chdrsel, SPACES)) {
			sv.chdrselErr.set(errorsInner.g667);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}
		/* Read the contract header details.*/
		/*chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmjaIO.getStatuz(), varcom.mrnf)) { commented for ILB-500 */
		
		//ILB-500 start
		chdrpf.setChdrcoy(wsspcomn.company.toString().charAt(0));
		chdrpf.setChdrnum(sv.chdrsel.toString());
		List<Chdrpf> chdrpfList = chdrpfDAO.getChdrmjaRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if (null==chdrpfList) {
			fatalError600();
		}
		if (chdrpfList.isEmpty()) {
		//end
			sv.chdrselErr.set(errorsInner.f259);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}else {
			chdrpf=chdrpfList.get(0);
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*                                                         <D9604> */
		/*  Dont check BT and PT date for flexible premium contract<D9604> */
		/*                                                         <D9604> */
		wsaaFlexiblePremium.set("N");
		/*modified for ILB 500
		 * itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5729);
		itemIO.setItemitem(chdrmjaIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			t5729rec.t5729Rec.set(itemIO.getGenarea());
			wsaaFlexiblePremium.set("Y");
			goTo(GotoLabel.pass2250);
		}*/
		//ILB-500 start
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5729);
		itempf.setItemitem(chdrpf.getCnttype());/* IJTI-1523 */
		//ILIFE-8174 start
		
		//List<Itempf> itemList = this.itemDAO.getItempfData(itempf);
		//ILIFE-8254 start
//		List<Itempf> itemList = this.itemDAO.getAllItemitem("IT",wsspcomn.company.toString(), "T5679", subprogrec.transcd.toString()); commented for ILIFE-8254
		List<Itempf> itemList = this.itemDAO.getAllItemitem("IT",wsspcomn.company.toString(), itempf.getItemtabl(), itempf.getItemitem()); 		
		if (null==itemList) {
			fatalError600();
		}
		//end
		//ILIFE-8174 end
		if (!itemList.isEmpty()) {
			t5729rec.t5729Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
			wsaaFlexiblePremium.set("Y");
			goTo(GotoLabel.pass2250);
		}
		//end 
		/*     The paid to date must equal the billed to date*/
		
	}

protected void pass2250()
	{
		/*     The billing frequency cannot equal zeros*/
		/* IF CHDRMJA-BILLFREQ         = '00'                           */
		/*    MOVE H014                TO S6672-CHDRSEL-ERR             */
		/*    MOVE 'Y'                 TO WSSP-EDTERROR                 */
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*    GO 2900-EXIT.                                             */
		/*    GO 2090-EXIT.                                             */
		/* Read the valid statii from table T5679.*/
		/*ILB-500
		 * itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());*/
	
		//ILB-500 start
	
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5679);
		itempf.setItemitem(subprogrec.transcd.toString());
		//ILIFE-8174 start 
		List<Itempf> itemList = this.itemDAO.getAllItemitem("IT",wsspcomn.company.toString(), "T5679", subprogrec.transcd.toString());
		
		if (null==itemList || itemList.isEmpty()) {
			fatalError600();
		}
		//ILIFE-8174 end
		t5679rec.t5679Rec.set(StringUtil.rawToString(itemList.get(0).getGenarea()));
		//end
		
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			headerStatuzCheck2910();
		}
		
		if (isEQ(sv.chdrselErr,SPACES) && isNE(wsaaValidStatus, "Y")) {
			sv.chdrselErr.set(errorsInner.e767);
		}
		if (isEQ(sv.chdrselErr,SPACES) && isNE(chdrpf.getBtdate(), chdrpf.getPtdate())) {
			sv.chdrselErr.set(errorsInner.h013);
			 
		}
		wsaaLapseOk = "N";
		chkValidLapse2600();
		if (isEQ(sv.chdrselErr,SPACES) && isEQ(wsaaLapseOk, "N")) {
			sv.chdrselErr.set(errorsInner.e663);
		}
		/*  Check if there are outstanding UTRN records for the            */
		/*  contract.                                                      */
		utrnuddIO.setChdrcoy(chdrpf.getChdrcoy());
		utrnuddIO.setChdrnum(chdrpf.getChdrnum());
		utrnuddIO.setLife("00");
		utrnuddIO.setCoverage("00");
		utrnuddIO.setRider("00");
		utrnuddIO.setPlanSuffix(ZERO);
		utrnuddIO.setFormat(utrnuddrec);
		utrnuddIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrnuddIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnuddIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, utrnuddIO);
		if (isNE(utrnuddIO.getStatuz(), varcom.oK)
		&& isNE(utrnuddIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(utrnuddIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrpf.getChdrcoy(), utrnuddIO.getChdrcoy())
		&& isEQ(chdrpf.getChdrnum(), utrnuddIO.getChdrnum())
		&& isNE(utrnuddIO.getStatuz(), varcom.endp)) {
			sv.chdrselErr.set(errorsInner.i030);
		}
		/*    Check that the contract has no unprocessed HITRs pending.    */
		hitrrnlIO.setParams(SPACES);
		hitrrnlIO.setChdrcoy(wsspcomn.company);
		hitrrnlIO.setChdrnum(chdrpf.getChdrnum());
		hitrrnlIO.setFormat(hitrrnlrec);
		hitrrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitrrnlIO);
		if (isNE(hitrrnlIO.getStatuz(), varcom.oK)
		&& isNE(hitrrnlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hitrrnlIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.chdrselErr,SPACES) &&  isEQ(hitrrnlIO.getStatuz(), varcom.oK)) {
			sv.chdrselErr.set(errorsInner.hl08);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* Disallow whole-plan processing where a contract has already     */
		/* been broken out by setting wssp-flag for reference in p6350     */
		if (isNE(chdrpf.getPolsum(), chdrpf.getPolinc())) {
			wsspcomn.flag.set("L");
		}
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*    GO TO 2900-EXIT.                                             */
		goTo(GotoLabel.exit2090);
	}

protected void verifyBatchControl2300()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(errorsInner.e073);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2900-EXIT.                                         */
			goTo(GotoLabel.exit2090);
		}
		bcbprogrec.transcd.set(subprogrec.transcd);
	}

protected void batchProgs2310()
	{
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2900-EXIT.                                         */
			return ;
		}
		wsspcomn.secProg[1].set(bcbprogrec.nxtprog1);
		wsspcomn.secProg[2].set(bcbprogrec.nxtprog2);
		wsspcomn.secProg[3].set(bcbprogrec.nxtprog3);
		wsspcomn.secProg[4].set(bcbprogrec.nxtprog4);
	}

protected void chkValidLapse2600()
 {
        Map<String, List<Itempf>> t5687Map = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "T5687");
        List<String> crtableList = covrpfDAO.searchCovrCrtable(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());/* IJTI-1523 */
        if (crtableList != null && crtableList.size() > 0) {
            for (String crtable : crtableList) {
                if (!t5687Map.containsKey(crtable)) {
                    syserrrec.params.set("t5687:" + crtable);
                    syserrrec.statuz.set(errorsInner.f294);
                    fatalError600();
                }
                List<Itempf> items = t5687Map.get(crtable);
               // BigDecimal ptdate = chdrmjaIO.getPtdate().getbigdata();ILB-500
                BigDecimal ptdate = BigDecimal.valueOf(chdrpf.getPtdate());
                for (Itempf item : items) {
                    if (item.getItmfrm().compareTo(ptdate) <= 0) {
                        t5687rec.t5687Rec.set(StringUtil.rawToString(item.getGenarea()));
                        if (isNE(t5687rec.nonForfeitMethod, SPACES)) {
                            wsaaLapseOk = "Y";
                            return;
                        }
                    }
                }
            }
        }
    }

	/**
	* <pre>
	*     CHECK THE CONTRACT HEADER STATUS AGAINST T5679.
	* </pre>
	*/
protected void headerStatuzCheck2910()
	{
		/*HEADER-STATUZ-CHECK*/
		/*IF T5679-CN-RISK-STAT(WSAA-INDEX) NOT = SPACE                */
		/*    IF (T5679-CN-RISK-STAT(WSAA-INDEX) = CHDRMJA-STATCODE)   */
		/*   AND (T5679-CN-PREM-STAT(WSAA-INDEX) = CHDRMJA-PSTATCODE)  */
		/*        MOVE 'Y'            TO WSAA-VALID-STATUS             */
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*        GO TO 2990-EXIT.                                     */
		/*        GO TO 2090-EXIT.                                     */
		if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()], chdrpf.getStatcode())) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				//if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()], chdrmjaIO.getPstatcode())) {ILB-500
				if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()], chdrpf.getPstcde())) {
					
					wsaaValidStatus = "Y";
				}
			}
		}
		/*EXIT1*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
 {
        updateWssp3100();
        if (isNE(sv.errorIndicators, SPACES)) {
            wsspcomn.edterror.set("Y");
        }
        if (isEQ(scrnparams.statuz, "BACH") || isNE(subprogrec.bchrqd, "Y")) {
            return;
        }
        if (updateBatchControl3200()) {
            continue3400();
        }
    }

protected void updateWssp3100()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		/*  Update WSSP Key details*/
		/*  Keep the contract header record.*/
		/*chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}commented for ILB-500*/
		//added for ILB-500
		chdrpfDAO.setCacheObject(chdrpf);
		//end
		/* Softlock the contract header record.*/
		if (isEQ(scrnparams.deviceInd, "*RMT")) {
			return;
		}
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		/* MOVE S6672-CHDRSEL          TO SFTL-ENTITY.                  */
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
		}
	}

protected boolean updateBatchControl3200()
	{
		batcchkrec.batcchkRec.set(SPACES);
		batcchkrec.function.set("CHECK");
		batcchkrec.batchkey.set(wsspcomn.batchkey);
		batcchkrec.tranid.set(wsspcomn.tranid);
		callProgram(Batcchk.class, batcchkrec.batcchkRec);
		if (isEQ(batcchkrec.statuz, varcom.oK)) {
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			return false;
		}
		if (isEQ(batcchkrec.statuz, varcom.dupr)) {
			sv.actionErr.set(errorsInner.e132);
			wsspcomn.edterror.set("Y");
			return false;
		}
		if (isEQ(batcchkrec.statuz, "INAC")) {
			batcdorrec.function.set("ACTIV");
			wsspcomn.batchkey.set(batcchkrec.batchkey);
			return true;
		}
		if (isNE(batcchkrec.statuz, varcom.mrnf)) {
			sv.actionErr.set(batcchkrec.statuz);
			wsspcomn.edterror.set("Y");
			return false;
		}
		
		batcdorrec.function.set("AUTO");
		return true;
	}


protected void continue3400()
	{
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
			wsspcomn.edterror.set("Y");
			rollback();
			return ;
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.set(1);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e132 = new FixedLengthStringData(4).init("E132");
	private FixedLengthStringData e717 = new FixedLengthStringData(4).init("E717");
	private FixedLengthStringData f259 = new FixedLengthStringData(4).init("F259");
	private FixedLengthStringData g667 = new FixedLengthStringData(4).init("G667");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData h013 = new FixedLengthStringData(4).init("H013");
	private FixedLengthStringData e663 = new FixedLengthStringData(4).init("E663");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData i030 = new FixedLengthStringData(4).init("I030");
	private FixedLengthStringData hl08 = new FixedLengthStringData(4).init("HL08");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
}
}
