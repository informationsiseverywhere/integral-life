/*
 * File: P5029.java
 * Date: 29 August 2009 23:58:17
 * Author: Quipoz Limited
 * 
 * Class transformed from P5029.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.screens.S5029ScreenVars;
//import com.csc.smart.dataaccess.DescTableDAM;
//import com.csc.smart.dataaccess.ItdmTableDAM;
//import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P5029 - Plan Policy Selection for Maturity/Expiry.
* --------------------------------------------------
*
* This screen,  S5029  is used to select a  Plan  (i.e.  number  of
* policies  is   greater than one) or one or many policies within a
* Plan.  If  it  is  not  a  Plan,   then    this  screen  will  be
* bypassed  and the policy for selection is defaulted to the one on
* file.
*
* Initialise
* ----------
*
* If the current stack entry is '*', then skip this section.
*
* Clear the subfile ready for loading.
*
* Read CHDRMAT (RETRV)  in  order to  obtain  the  contract  header
* information.   If  the  number of policies in the plan is zero or
* one then  Plan-processing  does not apply. If there is any  other
* numeric    value   present,  this  value will indicate the number
* of    policies    in    the  Plan.  If  Plan  processing  is  not
* applicable, do not load the screen (i.e. skip this section).
*
* For  Plan  processing,    the  number of policies within the Plan
* are  displayed  in  descending  sequence.    The  first  position
* remains    blank  (spaces)  and if this option is selected by the
* user, the entire Plan is selected.
*
* The status/description  for  each of the policies is obtained  as
* follows:-
*
* Read    the    first "COVR" record for this Policy and output the
* coverage  and premium  status  codes.  Look-up    the    coverage
* status  description  for  the  subfile  entry  and  output on the
* screen.
*
* Obtain  the  status codes required by reading table  T5679,  i.e.
* the   'Component Statuses For Transactions'.  This table is keyed
* by transaction number.
*
* Check the status code of the coverage  and  premium  against  the
* code  applicable    to   it  on T5679. If the codes do not match,
* then protect the selection field on that policy.
*
* While  loading  the  subfile,  if it is  only  possible  for  the
* user    to    select 1 policy, skip looking up any other details,
* displaying the screen etc., and process as  if  the  entire  Plan
* was originally selected for processing.
*
* LIFE DETAILS
*
* Obtain the life assured and joint-life details (if any):
*
*      - read  the life details using LIFEMAT (life number from
*               CHDRMAT, joint life number '00').  Look up the name
*               from the  client  details  (CLTS)  and  format as a
*               "confirmation name".
*
*      - read the joint life details using LIFEMAT (life number
*               from CHDRMAT,  joint  life number '01').  If found,
*               look up the name from the client details (CLTS) and
*               format as a "confirmation name".
*
* Obtain  the    necessary   descriptions  by  calling 'DESCIO' and
* output the descriptions of the statuses to the screen.
*
* Output the remaining relevant  data to  the  screen,  the  status
* descriptions,  the  Owner  number  and name (look-up the CLTS and
* format the name), CCD, Paid-to-date and the Bill-to-date.
*
* In all cases, load all  pages required in  the  subfile  and  set
* the subfile indicator to no.
*
* The above details are returned to the user.
*
*
* Validation
* ----------
*
* If    Plan    processing  is  not  applicable, or only one policy
* remains  for  selection, the screen is  not  displayed,  so  skip
* this section.
*
* Skip    this    section  if  returning  from  a  Policy selection
* (current stack position action flag = '*').
*
*     If 'KILL'  was entered, then skip the remainder of the valid-
*     ation and blank out the correct program on the stack and exit
*     from the program.
*
* Read all modified records  and  if this is  a  Plan  (see  above)
* and    the    first  subfile  entry  is selected (i.e. select the
* complete   Plan)  then  no  other  selection  must    be    made.
* Otherwise    this is an error and return the screen and request a
* re-selection.
*
* If  a  Plan  is  being processed and some  of  the  policies  are
* protected    (i.e.  cannot  be selected) and the user selects the
* remaining policies, then this selection is  treated  as  if   the
* entire Plan was selected.
*
* Updating
* --------
*
* This program does no updating.
*
*
* Next Program
* ------------
*
* If not returning from a policy selection:
*
* If part maturing/expiring over the entire Plan, "KEEPS" the   key
* of the first coverage/rider with a suffix of 0. Call  "GENSSW"
* with an action of "A" load the returned programs into the program
* stack. Add 1 to the program pointer and exit.
*
* Otherwise, one or  many  policies are being matured/expired.
*
* Call "GENSSW" with  an  action  of  "B"  and  load  the  returned
* programs  into the program stack. Set the stack option to '*' and
* start reading the subfile from the beginning.
*
* When a selected policy  is  found,   "KEEPS"  its  COVR  details,
* add 1 to the program pointer and exit.
*
* When  the  end  of  the subfile is reached, call "GENSSW" with an
* action of "E" and load the returned  programs  into  the  program
* stack. Add 1 to the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5029 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5029");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(ZERO);
	private ZonedDecimalData wsaaSavePlanSuffix = new ZonedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaPoliciesProtected = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaCovrTimes = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaSelected = new PackedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0).init(ZERO);
	private ZonedDecimalData wsccSub1 = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaPlan = new FixedLengthStringData(1).init(SPACES);
	private Validator planNotSelected = new Validator(wsaaPlan, " ");
	private Validator planSelected = new Validator(wsaaPlan, "Y");
	private ZonedDecimalData wsccSub2 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaChdrmatTranno = new ZonedDecimalData(5, 0);
	private PackedDecimalData wsaaActualWritten = new PackedDecimalData(3, 0).init(ZERO);

	private FixedLengthStringData wsaaDispS5029 = new FixedLengthStringData(1).init(SPACES);
	private Validator dispS5029Screen = new Validator(wsaaDispS5029, "Y");
	private String wsaaValidStatus = "";
		/* FORMATS */
	private String chdrmatrec = "CHDRMATREC";
	private String covrmatrec = "COVRMATREC";
		/* TABLES */
	private String t5679 = "T5679";
	private String t3623 = "T3623";
	private String t3588 = "T3588";
	private String t5682 = "T5682";
	private String t5688 = "T5688";
		/* ERRORS */
	private String g633 = "G633";
	private String g634 = "G634";
	private String e304 = "E304";
	private String h080 = "H080";
	private String h093 = "H093";
		/*Contract Header Maturities*/
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Gensswrec gensswrec = new Gensswrec();
	private T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5029ScreenVars sv = ScreenProgram.getScreenVars( S5029ScreenVars.class);

	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	
	private Itempf itempf = null;
	private List<Covrpf> covrpfList =new ArrayList<Covrpf>(); 
	private Covrpf covrpf;
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	
	Iterator<Covrpf> iterator ; 
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private List<Lifepf> lifepfList =new ArrayList<Lifepf>(); 
	private Lifepf lifepf;
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	Iterator<Lifepf> iteratorLife ;
	private Chdrpf chdrpf;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1009, 
		loop1152, 
		writeSubfileRec1153, 
		loop1202, 
		writeSubfileRec1230, 
		loopRisk1402, 
		initialPrem1405, 
		loopPrem1407, 
		exit1409, 
		readJlife1610, 
		exit1690, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		bypass4020, 
		exit4090, 
		exit4110
	}

	public P5029() {
		super();
		screenVars = sv;
		new ScreenModel("S5029", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1002();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1002()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaDispS5029.set("Y");
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1009);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		scrnparams.function.set(varcom.sclr);
		processScreen("S5029", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.planSuffix.set(ZERO);
		chdrpf = new Chdrpf();
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf){
		chdrmatIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmatIO);
		if (isNE(chdrmatIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmatIO.getParams());
			fatalError600();
		}
			else {
				chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(wsspcomn.company.toString(),chdrmatIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
			}
		} 
		wsaaActualWritten.set(ZERO);
		wsaaSavePlanSuffix.set(ZERO);
		if (isLTE(chdrpf.getPolinc(),1)) {
			wsaaActualWritten.add(1);
			goTo(GotoLabel.exit1009);
		}
		checkStatus1100();
		if (isNE(chdrpf.getPolsum(),chdrpf.getPolinc())) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		sv.planSuffix.set(0);
		sv.statdesc.set("Whole Plan Select");
		scrnparams.function.set(varcom.sadd);
		processScreen("S5029", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		compute(wsaaCovrTimes, 0).set(sub(chdrpf.getPolinc(),chdrpf.getPolsum()));
		wsaaPlanSuffix.set(chdrpf.getPolinc());
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaCovrTimes);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			loadCovrSubfile1200();
		}
		if (isNE(chdrpf.getPolinc(),ZERO)) {
			processSummary1150();
		}
		fillScreen1600();
	}

protected void checkStatus1100()
	{
		readStatusTable1110();
	}

protected void readStatusTable1110()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5679);
		itempf.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void processSummary1150()
	{
					go1151();
	}

protected void go1151()
	{
		wsaaValidStatus = "N";
		covrpfList = covrpfDAO.getCovrsurByComAndNum(wsspcomn.company.toString(),chdrpf.getChdrnum());//IJTI-1485
		if(covrpfList.isEmpty())
			fatalError600();
		iterator =   covrpfList.iterator();
		if (iterator.hasNext()) 
			covrpf = iterator.next();				
	}

protected void loop1152()
	{
		sv.stycvr.set(covrpf.getStatcode());
		sv.pstatcode.set(covrpf.getPstatcode());
		descpf=descDAO.getdescData("IT", t5682, covrpf.getStatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1485
		if (descpf==null) {
			sv.statdesc.fill("?");
		}
		else {
			sv.statdesc.set(descpf.getLongdesc());
		}
		chkComponentsStatii1400();
		if (isEQ(wsaaValidStatus,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("N");
			//goTo(GotoLabel.writeSubfileRec1153);
			writeSubfileRec1153();
			return;
		}
		if(covrpf.getPlanSuffix() == 0) {
			if(iterator.hasNext())
				covrpf = iterator.next();
			//goTo(GotoLabel.loop1152);
			loop1152();
			return;
		}
		sv.selectOut[varcom.pr.toInt()].set("Y");
		writeSubfileRec1153();
		return;
	}

protected void writeSubfileRec1153()
	{
	 PackedDecimalData loopEndVar2 = new PackedDecimalData(7, 0);
		loopEndVar2.set(chdrpf.getPolsum());
		for (int loopVar2 = 0; !(isEQ(loopVar2,loopEndVar2.toInt())); loopVar2 += 1){
			loadSummarySubfile1250();
		}
		/*EXIT*/
	}

protected void loadCovrSubfile1200()
	{
					initialize1200();
	}

protected void initialize1200()
	{
		wsaaValidStatus = "N";
		covrpfList.clear();
		covrpfList = covrpfDAO.SearchRecordsforMatd(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum(),wsaaPlanSuffix.toInt());//IJTI-1485
		if(null == covrpfList){
			fatalError600();
		}
		iterator =   covrpfList.iterator();
		if (iterator.hasNext()) 
			covrpf = iterator.next();		
		loop1202();
		return;
	}

protected void loop1202()
	{
		if(covrpfList.size() < 1){
			sv.selectOut[varcom.pr.toInt()].set("Y");
			//goTo(GotoLabel.writeSubfileRec1230);
			writeSubfileRec1230();
			return;
		}
		sv.planSuffix.set(wsaaPlanSuffix);
		sv.stycvr.set(covrpf.getStatcode());
		sv.pstatcode.set(covrpf.getPstatcode());
		descpf=descDAO.getdescData("IT", t5682, covrpf.getStatcode(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf!=null) { //IJTI-462
			sv.statdesc.set(descpf.getLongdesc());
		}
		else {
			sv.statdesc.fill("?");
		}
		chkComponentsStatii1400();
		if (isEQ(wsaaValidStatus,"Y")) {
			wsaaSavePlanSuffix.set(sv.planSuffix);
			wsaaActualWritten.add(1);
		}
		if (isEQ(wsaaValidStatus,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("N");
			//goTo(GotoLabel.writeSubfileRec1230);
			writeSubfileRec1230();
			return;
		}
		if(iterator.hasNext()){
			covrpf = iterator.next();
			//goTo(GotoLabel.loop1202);
			loop1202();
			return;
		}
		sv.selectOut[varcom.pr.toInt()].set("Y");
		writeSubfileRec1230();
		return;
	}

protected void writeSubfileRec1230()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5029", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.subtract(1);
		/*EXIT*/
	}

protected void loadSummarySubfile1250()
	{
		/*GO*/
		sv.planSuffix.set(wsaaPlanSuffix);
		if (isNE(sv.selectOut[varcom.pr.toInt()],"Y")) {
			wsaaSavePlanSuffix.set(sv.planSuffix);
			wsaaActualWritten.add(1);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5029", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaPlanSuffix.subtract(1);
		/*EXIT*/
	}

protected void findDesc1300()
	{
		/*READ*/

		/*EXIT*/
	}

protected void chkComponentsStatii1400()
	{
					initialRisk1400();
	}

protected void initialRisk1400()
	{
		wsaaSub.set(ZERO);
		loopRisk1402();
		return;
	}

protected void loopRisk1402()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			//goTo(GotoLabel.exit1409);
			return;
		}
		if (isEQ(covrpf.getStatcode(),t5679rec.covRiskStat[wsaaSub.toInt()])) {
			//goTo(GotoLabel.initialPrem1405);
			initialPrem1405();
			return;
		}
		//goTo(GotoLabel.loopRisk1402);
		loopRisk1402();
		return;
	}

protected void initialPrem1405()
	{
		wsaaSub.set(ZERO);
		loopPrem1407();
		return;
	}

protected void loopPrem1407()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)) {
			//goTo(GotoLabel.exit1409);
			return;
		}
		if (isEQ(covrpf.getPstatcode(),t5679rec.covPremStat[wsaaSub.toInt()])) {
			wsaaValidStatus = "Y";
			//goTo(GotoLabel.exit1409);
			return;
		}
		//goTo(GotoLabel.loopPrem1407);
		loopPrem1407();
		return;
	}

protected void fillScreen1600()
	{

					headings1610();

	}

protected void headings1610()
	{
		sv.occdate.set(chdrpf.getOccdate());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		descpf=descDAO.getdescData("IT", t5688, chdrpf.getCnttype(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1485
		if (descpf==null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		sv.cownnum.set(chdrpf.getCownnum());
		cltsIO.setClntnum(chdrpf.getCownnum());
		getClientDetails1700();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownernumErr.set(e304);
			sv.ownernum.set(SPACES);
		}
		else {
			plainname();
			sv.ownernum.set(wsspcomn.longconfname);
		}
		sv.btdate.set(chdrpf.getBtdate());
		sv.ptdate.set(chdrpf.getPtdate());
		descpf=descDAO.getdescData("IT", t3623, chdrpf.getStatcode(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1485
		if (descpf==null) {
			sv.rstate.fill("?");
		}
		else {
			sv.rstate.set(descpf.getShortdesc());
		}
		descpf=descDAO.getdescData("IT", t3588, chdrpf.getPstcde(), wsspcomn.company.toString(), wsspcomn.language.toString());//IJTI-1485
		if (descpf!=null) {
			sv.pstate.set(descpf.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		lifepf = new Lifepf();
		lifepfList = lifepfDAO.getLifematRecords(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), covrpf.getLife());//IJTI-1485
		if(null == lifepfList)
			fatalError600();
		iteratorLife = lifepfList.iterator();

		if(iteratorLife.hasNext())
		{
			lifepf = iteratorLife.next();
			if(lifepf.getJlife() != "00"){
				// goTo(GotoLabel.readJlife1610);
				readJlife1610();
				return;
		}
			sv.lifcnum.set(lifepf.getLifcnum());
			cltsIO.setClntnum(lifepf.getLifcnum());
		getClientDetails1700();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
	}
		readJlife1610();
		return;
	}

protected void readJlife1610()
	{
		if(iteratorLife.hasNext())
		{
			lifepf = iteratorLife.next();
			sv.jlifcnum.set(lifepf.getLifcnum());
			cltsIO.setClntnum(lifepf.getLifcnum());
		getClientDetails1700();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.jlinsnameErr.set(e304);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
	}
		else{
			sv.jlifcnum.set("*NONE");
			sv.jlinsname.set(SPACES);
			//goTo(GotoLabel.exit1690);
			return;
		}
	}
protected void getClientDetails1700()
	{
		/*READ*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		wsaaSelected.set(0);
		wsaaPlan.set(SPACES);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (isEQ(wsaaActualWritten,1)
				|| isLTE(chdrpf.getPolinc(),1)) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (!dispS5029Screen.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateSubfile2010()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5029", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.select,SPACES)) {
			wsaaPlan.set("Y");
		}
		if (isNE(sv.select,SPACES)
				&& isLTE(chdrpf.getPolinc(),1)) {
			scrnparams.errorCode.set(h080);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		scrnparams.subfileRrn.set(1);
		if (planSelected.isTrue()
		&& isGT(wsaaSelected,1)) {
			scrnparams.errorCode.set(g633);
			wsspcomn.edterror.set("Y");
		}
		if (planNotSelected.isTrue()
		&& isEQ(wsaaSelected,0)) {
			scrnparams.errorCode.set(g634);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2600()
	{
		/*VALIDATION*/
		if (isNE(sv.select,SPACES)) {
			wsaaSelected.add(1);
		}
		/*READ-NEXT-RECORD*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5029", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{

					nextProgram4010();

	}

protected void nextProgram4010()
	{
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			//goTo(GotoLabel.bypass4020);
			bypass4020();
			return;
		}
		if (isLTE(chdrpf.getPolinc(),1)
		|| isLTE(wsaaActualWritten,1)) {
			keepsCovr4400();
			gensswrec.function.set("A");
			callGenssw4100();
			//goTo(GotoLabel.exit4090);
			return;
		}
		if (!dispS5029Screen.isTrue()) {
			keepsCovr4400();
			gensswrec.function.set("A");
			callGenssw4100();
			//goTo(GotoLabel.exit4090);
			return;
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5029", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.select,SPACES)) {
			keepsCovr4400();
			gensswrec.function.set("A");
			callGenssw4100();
			//goTo(GotoLabel.exit4090);
			return;
		}
		bypass4020();
		return;
	}

protected void bypass4020()
	{
		if (isEQ(sv.select,SPACES)) {
			while ( !(isNE(sv.select,SPACES)
			|| isEQ(scrnparams.statuz,varcom.endp))) {
				readSubfile4800();
			}
			
		}
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			gensswrec.function.set("E");
			callGenssw4100();
			//goTo(GotoLabel.exit4090);
			return;
		}
		keepsCovr4400();
		gensswrec.function.set("B");
		callGenssw4100();
		sv.select.set(SPACES);
	}

protected void callGenssw4100()
	{
		try {
			callGenssw4110();
		}
		catch (GOTOException e){
		}
	}

protected void callGenssw4110()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,varcom.oK)
		&& isNE(gensswrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4110);
		}
		compute(wsccSub1, 0).set(add(1,wsspcomn.programPtr));
		wsccSub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			gensToWsspProgs4300();
		}
		if (isEQ(gensswrec.function,"B")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		}
		else {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		}
		wsspcomn.programPtr.add(1);
	}

protected void gensToWsspProgs4300()
	{
		/*GENS-TO-WSSP-PROGS*/
		wsspcomn.secProg[wsccSub1.toInt()].set(gensswrec.progOut[wsccSub2.toInt()]);
		wsccSub1.add(1);
		wsccSub2.add(1);
		/*EXIT*/
	}

protected void keepsCovr4400()
	{
		go4410();
	}

protected void go4410()
	{
		covrpf = new Covrpf();
		if (isLTE(wsaaActualWritten,1)) {
			covrpf.setPlanSuffix(wsaaSavePlanSuffix.toInt());
		}
		else {
			if (isLTE(sv.planSuffix,chdrpf.getPolsum())) {
				covrpf.setPlanSuffix(0);
			}
			else {
				covrpf.setPlanSuffix(sv.planSuffix.toInt());
			}
		}
		covrpfList = covrpfDAO.SearchRecordsforMatd(wsspcomn.company.toString(),chdrpf.getChdrnum(),covrpf.getPlanSuffix());//IJTI-1485

		if(null == covrpfList){
			fatalError600();
		}
		iterator = covrpfList.iterator();
		if(iterator.hasNext()){
			covrpf = iterator.next();
		}
		if (isLTE(wsaaActualWritten,1)) {
			covrpf.setPlanSuffix(wsaaSavePlanSuffix.toInt());
		}
		else {
			covrpf.setPlanSuffix(sv.planSuffix.toInt());
		}
		covrpfDAO.setCacheObject(covrpf);
	}

protected void readSubfile4800()
	{
		/*READ*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5029", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
