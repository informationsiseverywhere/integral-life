package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:39
 * Description:
 * Copybook name: MATCCPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Matccpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData maturityRec = new FixedLengthStringData(getMaturityRecSize());
  	public FixedLengthStringData chdrChdrcoy = new FixedLengthStringData(1).isAPartOf(maturityRec, 0);
  	public FixedLengthStringData chdrChdrnum = new FixedLengthStringData(8).isAPartOf(maturityRec, 1);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(maturityRec, 9);
  	public PackedDecimalData polsum = new PackedDecimalData(4, 0).isAPartOf(maturityRec, 12);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(maturityRec, 15);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(maturityRec, 17);
  	public FixedLengthStringData covrCoverage = new FixedLengthStringData(2).isAPartOf(maturityRec, 19);
  	public FixedLengthStringData covrRider = new FixedLengthStringData(2).isAPartOf(maturityRec, 21);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(maturityRec, 23);
  	public ZonedDecimalData crrcd = new ZonedDecimalData(8, 0).isAPartOf(maturityRec, 27);
  	public ZonedDecimalData ptdate = new ZonedDecimalData(8, 0).isAPartOf(maturityRec, 35);
  	public ZonedDecimalData effdate = new ZonedDecimalData(8, 0).isAPartOf(maturityRec, 43);
  	public ZonedDecimalData convUnits = new ZonedDecimalData(8, 0).isAPartOf(maturityRec, 51);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(maturityRec, 59);
  	public PackedDecimalData estimatedVal = new PackedDecimalData(17, 2).isAPartOf(maturityRec, 60);
  	public PackedDecimalData actualVal = new PackedDecimalData(17, 2).isAPartOf(maturityRec, 69);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(maturityRec, 78);
  	public FixedLengthStringData chdrCurr = new FixedLengthStringData(3).isAPartOf(maturityRec, 81);
  	public FixedLengthStringData chdrType = new FixedLengthStringData(3).isAPartOf(maturityRec, 84);
  	public FixedLengthStringData matCalcMeth = new FixedLengthStringData(4).isAPartOf(maturityRec, 87);
  	public FixedLengthStringData pstatcode = new FixedLengthStringData(2).isAPartOf(maturityRec, 91);
  	public FixedLengthStringData element = new FixedLengthStringData(1).isAPartOf(maturityRec, 93);
  	public FixedLengthStringData description = new FixedLengthStringData(30).isAPartOf(maturityRec, 94);
  	public PackedDecimalData singp = new PackedDecimalData(17, 2).isAPartOf(maturityRec, 124);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(maturityRec, 133);
  	public FixedLengthStringData type = new FixedLengthStringData(1).isAPartOf(maturityRec, 135);
  	public FixedLengthStringData fund = new FixedLengthStringData(4).isAPartOf(maturityRec, 136);
  	public FixedLengthStringData batckey = new FixedLengthStringData(64).isAPartOf(maturityRec, 140);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(maturityRec, 204);
  	public ZonedDecimalData planSwitch = new ZonedDecimalData(1, 0).isAPartOf(maturityRec, 208).setUnsigned();
  	public FixedLengthStringData endf = new FixedLengthStringData(1).isAPartOf(maturityRec, 209);
    //ILIFE-7396 start
  	public PackedDecimalData vpmCalcResult = new PackedDecimalData(17, 2).isAPartOf(maturityRec, 210);
    //ILIFE-7396 end
	public void initialize() {
		COBOLFunctions.initialize(maturityRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		maturityRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

	public int getMaturityRecSize()
	{
		return 219;
	}

}