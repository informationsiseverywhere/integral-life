package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.LaptpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Laptpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class LaptpfDAOImpl extends BaseDAOImpl<Laptpf> implements LaptpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(LaptpfDAOImpl.class);	
	
	public Laptpf getLaptByKey(String chdrcoy, String chdrnum, String life, String coverage, String rider, int plnsfx){
		
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,NEWSUMA,TERMID,TRDT,TRTM,USER_T,USRPRF,JOBNM ");
		sb.append("FROM LAPTPF WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? ");
		sb.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER ASC");	
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		Laptpf laptpf = null;
		
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, coverage);
			ps.setString(5, rider);
			ps.setInt(6, plnsfx);
			rs = ps.executeQuery();
			if(rs.next()) {
				laptpf = new Laptpf();
				laptpf.setUnique_number(rs.getLong("UNIQUE_NUMBER"));
				laptpf.setChdrcoy(rs.getString("CHDRCOY"));
				laptpf.setChdrnum(rs.getString("CHDRNUM"));
				laptpf.setLife(rs.getString("LIFE"));
				laptpf.setCoverage(rs.getString("COVERAGE"));
				laptpf.setRider(rs.getString("RIDER"));
				laptpf.setPlnsfx(rs.getInt("PLNSFX"));
				laptpf.setNewsuma(rs.getBigDecimal("NEWSUMA"));
				laptpf.setTermid(rs.getString("TERMID"));
				laptpf.setTrdt(rs.getInt("TRDT"));
				laptpf.setTrtm(rs.getInt("TRTM"));
				laptpf.setUser_t(rs.getInt("USER_T"));
				laptpf.setUsrprf(rs.getString("USRPRF"));
				laptpf.setJobnm(rs.getString("JOBNM"));
			}
			
		}catch(SQLException e) {
			LOGGER.error("getLaptByKey()".concat(e.getMessage()));	
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		
		return laptpf;
		
	}
	
	public boolean updtOrInstLaptByKey(Laptpf laptpf) {
		if(updatLaptByKey(laptpf)>0) {
			return true;
		}else {
			return insertLaptpf(laptpf);
		}

	}
	
	public int updatLaptByKey(Laptpf laptpf){
		StringBuilder sb = new StringBuilder("UPDATE LAPTPF SET NEWSUMA=?, TERMID=? , TRDT=?, TRTM=?, USER_T=?, USRPRF=?, JOBNM=?, DATIME=? ");
		sb.append("WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
        int result = 0;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setBigDecimal(1, laptpf.getNewsuma());
			ps.setString(2, laptpf.getTermid());
			ps.setInt(3, laptpf.getTrdt());
			ps.setInt(4, laptpf.getTrtm());
			ps.setInt(5, laptpf.getUser_t());
			ps.setString(6, getUsrprf());			    
			ps.setString(7, getJobnm());		
			ps.setTimestamp(8, getDatime());
			ps.setString(9, laptpf.getChdrcoy());
			ps.setString(10, laptpf.getChdrnum());
			ps.setString(11, laptpf.getLife());
			ps.setString(12, laptpf.getCoverage());
			ps.setString(13, laptpf.getRider());
			ps.setInt(14, laptpf.getPlnsfx());
			
			result = ps.executeUpdate();
			
		}catch(SQLException e) {
			LOGGER.error("updatLaptByKey()".concat(e.getMessage()));	
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}
		
		return result;
		
	}
	
	public boolean insertLaptpf(Laptpf laptpf) {
		StringBuilder sb = new StringBuilder("INSERT INTO LAPTPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,NEWSUMA,TERMID,TRDT,TRTM,USER_T,USRPRF,JOBNM, DATIME) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?, ?)");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
        boolean result = false;
		try {
			ps = getPrepareStatement(sb.toString());
		
			ps.setString(1, laptpf.getChdrcoy());
			ps.setString(2, laptpf.getChdrnum());
			ps.setString(3, laptpf.getLife());
			ps.setString(4, laptpf.getCoverage());
			ps.setString(5, laptpf.getRider());
			ps.setInt(6, laptpf.getPlnsfx());
			ps.setBigDecimal(7, laptpf.getNewsuma());
			ps.setString(8, laptpf.getTermid());
			ps.setInt(9, laptpf.getTrdt());
			ps.setInt(10, laptpf.getTrtm());
			ps.setInt(11, laptpf.getUser_t());
			ps.setString(12, getUsrprf());			    
			ps.setString(13, getJobnm());		
			ps.setTimestamp(14, getDatime());
			ps.executeUpdate();
			result = true;
		}catch(SQLException e) {
			LOGGER.error("insertLaptpf()".concat(e.getMessage()));	
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}
		
		return result;
	}
	
}
