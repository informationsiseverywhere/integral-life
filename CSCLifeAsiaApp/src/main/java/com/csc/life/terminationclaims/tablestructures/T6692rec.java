package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:13
 * Description:
 * Copybook name: T6692REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6692rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6692Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData rgpytype = new FixedLengthStringData(2).isAPartOf(t6692Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(t6692Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6692Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6692Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}