package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6002
 * @version 1.0 generated on 30/08/09 06:50
 * @author Quipoz
 */
public class S6002ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(967);
	public FixedLengthStringData dataFields = new FixedLengthStringData(439).isAPartOf(dataArea, 0);
	public FixedLengthStringData bilfrqdesc = DD.bilfrqdesc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,30);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,32);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,64);
	public ZonedDecimalData crrcd = DD.crrcd.copyToZonedDecimal().isAPartOf(dataFields,66);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,108);
	public ZonedDecimalData fundAmount = DD.fundamnt.copyToZonedDecimal().isAPartOf(dataFields,138);
	public FixedLengthStringData jlifename = DD.jlifename.copy().isAPartOf(dataFields,155);
	public FixedLengthStringData jlife = DD.jlifenum.copy().isAPartOf(dataFields,202);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,210);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,212);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,259);
	public ZonedDecimalData newinsval = DD.newinsval.copyToZonedDecimal().isAPartOf(dataFields,267);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,284);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,288);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,292);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(dataFields,302);
	public FixedLengthStringData pstatdesc = DD.pstatdesc.copy().isAPartOf(dataFields,304);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,334);
	public ZonedDecimalData pufee = DD.pufee.copyToZonedDecimal().isAPartOf(dataFields,342);
	public FixedLengthStringData pumeth = DD.pumeth.copy().isAPartOf(dataFields,359);
	public FixedLengthStringData pumethdesc = DD.pumethdesc.copy().isAPartOf(dataFields,363);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,393);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,396);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(dataFields,398);
	public FixedLengthStringData statdesc = DD.statdesc.copy().isAPartOf(dataFields,400);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,424);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 439);
	public FixedLengthStringData bilfrqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData crrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData fundamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData newinsvalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData pstatdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData pufeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData pumethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData pumethdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData statcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData statdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(396).isAPartOf(dataArea, 571);
	public FixedLengthStringData[] bilfrqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] crrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] fundamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] newinsvalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] pstatdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] pufeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] pumethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] pumethdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] statcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] statdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crrcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S6002screenWritten = new LongData(0);
	public LongData S6002protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6002ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(newinsvalOut,new String[] {null, "10",null, null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, lifenum, lifename, jlife, jlifename, life, coverage, rider, crtable, crtabdesc, ptdate, btdate, crrcd, pumethdesc, pstatdesc, billfreq, pumeth, statcode, pstatcode, bilfrqdesc, statdesc, numpols, planSuffix, sumin, fundAmount, newinsval, pufee};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, lifenumOut, lifenameOut, jlifenumOut, jlifenameOut, lifeOut, coverageOut, riderOut, crtableOut, crtabdescOut, ptdateOut, btdateOut, crrcdOut, pumethdescOut, pstatdescOut, billfreqOut, pumethOut, statcodeOut, pstatcodeOut, bilfrqdescOut, statdescOut, numpolsOut, plnsfxOut, suminOut, fundamntOut, newinsvalOut, pufeeOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, lifenumErr, lifenameErr, jlifenumErr, jlifenameErr, lifeErr, coverageErr, riderErr, crtableErr, crtabdescErr, ptdateErr, btdateErr, crrcdErr, pumethdescErr, pstatdescErr, billfreqErr, pumethErr, statcodeErr, pstatcodeErr, bilfrqdescErr, statdescErr, numpolsErr, plnsfxErr, suminErr, fundamntErr, newinsvalErr, pufeeErr};
		screenDateFields = new BaseData[] {ptdate, btdate, crrcd};
		screenDateErrFields = new BaseData[] {ptdateErr, btdateErr, crrcdErr};
		screenDateDispFields = new BaseData[] {ptdateDisp, btdateDisp, crrcdDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6002screen.class;
		protectRecord = S6002protect.class;
	}

}
