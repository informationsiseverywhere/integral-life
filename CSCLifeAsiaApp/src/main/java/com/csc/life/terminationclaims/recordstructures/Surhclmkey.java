package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:57
 * Description:
 * Copybook name: SURHCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Surhclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData surhclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData surhclmKey = new FixedLengthStringData(64).isAPartOf(surhclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData surhclmChdrcoy = new FixedLengthStringData(1).isAPartOf(surhclmKey, 0);
  	public FixedLengthStringData surhclmChdrnum = new FixedLengthStringData(8).isAPartOf(surhclmKey, 1);
  	public PackedDecimalData surhclmTranno = new PackedDecimalData(5, 0).isAPartOf(surhclmKey, 9);
  	public PackedDecimalData surhclmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(surhclmKey, 12);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(surhclmKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(surhclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		surhclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}