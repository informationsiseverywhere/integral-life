package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:32
 * Description:
 * Copybook name: ACBLCLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acblclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acblclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acblclmKey = new FixedLengthStringData(64).isAPartOf(acblclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData acblclmRldgcoy = new FixedLengthStringData(1).isAPartOf(acblclmKey, 0);
  	public FixedLengthStringData acblclmSacscode = new FixedLengthStringData(2).isAPartOf(acblclmKey, 1);
  	public FixedLengthStringData acblclmSacstyp = new FixedLengthStringData(2).isAPartOf(acblclmKey, 3);
  	public FixedLengthStringData acblclmRldgacct = new FixedLengthStringData(16).isAPartOf(acblclmKey, 5);
  	public FixedLengthStringData acblclmOrigcurr = new FixedLengthStringData(3).isAPartOf(acblclmKey, 21);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(acblclmKey, 24, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acblclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acblclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}