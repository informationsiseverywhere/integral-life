package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import com.csc.life.terminationclaims.dataaccess.dao.ClnnpfDAO;
import com.csc.life.terminationclaims.screens.Sa621ScreenVars;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pa621 extends ScreenProgCS {

	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA621");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sa621ScreenVars sv = ScreenProgram.getScreenVars( Sa621ScreenVars.class);
	private ClnnpfDAO clnnpfDAO= getApplicationContext().getBean("clnnpfDAO",ClnnpfDAO.class);
	private Clnnpf clnnpf = new Clnnpf();
	public static final String CLMPREFIX = "CLMNTF";
	private String combine = "";
	
	public Pa621() {
		super();
		screenVars = sv;
		new ScreenModel("Sa621", AppVars.getInstance(), sv);
	}
	
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}
	
	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray){
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000(){
	sv.dataArea.set(SPACES);
	if(isNE(wsspcomn.wsaanotificationNum,SPACES))
		sv.notifinum.set(wsspcomn.wsaanotificationNum.toString());
	else
		sv.notifinum.set(SPACES);	
	if (isEQ(wsspcomn.flag,"I") || isEQ(wsspcomn.flag,"M") || isEQ(wsspcomn.flag,"D")) {
		clnnpf = clnnpfDAO.getCacheObject(clnnpf);
		if(clnnpf ==null){
			syserrrec.params.set(wsspcomn.company.toString().concat(wsspcomn.wsaanotificationNum.toString()));
			fatalError600();
		}
		else{
			sv.notes.set(clnnpf.getNotificationNote().trim());
		}
	}
 }

protected void preScreenEdit(){
	/*PRE-START*/
	/*    This section will handle any action required on the screen **/
	/*    before the screen is painted.                              **/
	if (isEQ(wsspcomn.flag,"I") || isEQ(wsspcomn.flag,"D")) {
		scrnparams.function.set(varcom.prot);
	}
	return ;
	/*PRE-EXIT*/
}
/**
* <pre>
*     RETRIEVE SCREEN FIELDS AND EDIT
* </pre>
*/
protected void screenEdit2000(){
		screenIo2010();
		exit2090();
}

protected void screenIo2010(){
	wsspcomn.edterror.set(varcom.oK);
	if (isEQ(scrnparams.statuz, varcom.kill)) {
		exit2090();
	}
	if (isEQ(scrnparams.statuz, varcom.calc)) {
		wsspcomn.edterror.set("Y");
		exit2090();
	}
	/*VALIDATE*/
	if (isEQ(wsspcomn.flag,"I") || isEQ(wsspcomn.flag,"D")) {
		return ;
	}
	/*OTHER*/
	
}

protected void exit2090(){
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}

protected void update3000(){
	
	if (isEQ(scrnparams.statuz, varcom.kill)) {
		return;
	}
	if (isEQ(wsspcomn.flag,"I")) {
		return ;
	}
	if(isNE(sv.notes, SPACES)){
		if (isEQ(wsspcomn.flag,"M")) {
			clnnpf.setNotificationNote(sv.notes.toString().trim());
			clnnpf.setDatime(setupDatime());
			clnnpfDAO.updateClnnpf(clnnpf);
		}
		else if (isEQ(wsspcomn.flag,"D")) {
			clnnpfDAO.removeClnnpfRecord(clnnpf.getUniqueNumber());
		}
		else{
			
			clnnpf = new Clnnpf();
			clnnpf.setClnncoy(wsspcomn.company.toString());
			clnnpf.setNotifinum(sv.notifinum.toString().trim().replace(CLMPREFIX, ""));
			clnnpf.setClaimno(wsspcomn.wsaaclaimno.toString().trim());
			clnnpf.setLifenum(wsspcomn.chdrCownnum.toString());
			clnnpf.setClaimant(wsspcomn.wsaaclaimant.toString());
			clnnpf.setRelationship(wsspcomn.wsaarelationship.toString());
			clnnpf.setNotificationNote(sv.notes.toString().trim());
			clnnpf.setDatime(setupDatime());
			clnnpfDAO.insertClnnpf(clnnpf);
			
		}
	}
}
protected String setupDatime(){
	try{
	Date parse=new SimpleDateFormat("MMM dd yyyy HH:mm:ss").parse(sv.datime.substring(4,24));
	combine =new SimpleDateFormat("yyyy-MM-dd").format(parse)+" "+new SimpleDateFormat("HH:mm:ss").format(parse);	
	}
	catch(ParseException e){
		e.printStackTrace();
	}
	return combine;
}
protected void whereNext4000(){
	/*NEXT-PROGRAM*/
	wsspcomn.programPtr.add(1);		
	/*EXIT*/
	}
}
