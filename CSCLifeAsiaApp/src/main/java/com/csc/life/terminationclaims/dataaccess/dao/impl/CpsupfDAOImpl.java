package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.terminationclaims.dataaccess.dao.CpsupfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cpsupf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;


public class CpsupfDAOImpl extends BaseDAOImpl<Cpsupf> implements CpsupfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(CpsupfDAOImpl.class);

	@Override
	public void insertCpsupfRecord(Cpsupf cpsupf) {
		
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO CPSUPF (CHDRPFX,CHDRCOY,CHDRNUM,TRANNO,BATCTRCDE,RIDER,CRTABLE,CNTTYPE,COMPRSKCODE,COMPPREMCODE,COMPSUMSS,COMPBILPRM,REFPRM,REFADJUST,REFAMT,STATUS,PAYRNUM,REQNTYPE,BANKACCKEY,BANKKEY,BANKDESC,COMPSTATUS,VALIDFLAG,LIFE,COVERAGE,PLNSFX,USRPRF,JOBNM,DATIME)");
		sql.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		try {
				int i = 1;
				ps.setString(i++, cpsupf.getChdrpfx());
				ps.setString(i++, cpsupf.getChdrcoy());
				ps.setString(i++, cpsupf.getChdrnum());
				ps.setInt(i++, cpsupf.getTranno());
				ps.setString(i++, cpsupf.getBatctrcde());
				ps.setString(i++, cpsupf.getRider());
				ps.setString(i++, cpsupf.getCrtable());
				ps.setString(i++, cpsupf.getCnttype());
				ps.setString(i++, cpsupf.getComprskcode());
				ps.setString(i++, cpsupf.getComppremcode());
				ps.setDouble(i++, cpsupf.getCompsumss());
				ps.setDouble(i++, cpsupf.getCompbilprm());
				ps.setDouble(i++, cpsupf.getRefprm());
				ps.setDouble(i++, cpsupf.getRefadjust());
				ps.setDouble(i++, cpsupf.getRefamt());
				ps.setString(i++, cpsupf.getStatus());
				ps.setString(i++, cpsupf.getPayrnum());
				ps.setString(i++, cpsupf.getReqntype());
				ps.setString(i++, cpsupf.getBankacckey());
				ps.setString(i++, cpsupf.getBankkey());
				ps.setString(i++, cpsupf.getBankdesc());
				ps.setString(i++, cpsupf.getCompstatus());
				ps.setString(i++, cpsupf.getValidflag());
				ps.setString(i++, cpsupf.getLife());
				ps.setString(i++, cpsupf.getCoverage());
				ps.setInt(i++, cpsupf.getPlnsfx());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				
				ps.executeUpdate();
				
		} catch (SQLException e) {
			LOGGER.error("insertCpsupfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
		
	}

	@Override
	public void updateCpsupfRecord(Cpsupf cpsupf) {
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE CPSUPF SET VALIDFLAG='2'");
		sb.append(" WHERE CHDRNUM=? AND COVERAGE=? AND RIDER=? AND STATUS=? AND VALIDFLAG=?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());	
						    
			    ps.setString(1, cpsupf.getChdrnum());
			    ps.setString(2, cpsupf.getCoverage());
			    ps.setString(3, cpsupf.getRider());
			    ps.setString(4, "R");	
			    ps.setString(5, "1");	
			   
			    ps.executeUpdate();					
			
		}catch (SQLException e) {
			LOGGER.error("updateCpsupf()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
	}

	@Override
	public void deleteCpsupfRecord(Cpsupf cpsupf) {// for register Reversal

		 StringBuilder sql_delete = new StringBuilder("DELETE FROM CPSUPF WHERE CHDRNUM=? AND CRTABLE=? AND STATUS=?");
		 	int flag ;
		 	PreparedStatement psDelete = null;
	        ResultSet rs = null;
	        try {
	        	psDelete = getPrepareStatement(sql_delete.toString());
	        	
	        	psDelete.setString(1, cpsupf.getChdrnum().trim());
	        	psDelete.setString(2, cpsupf.getCrtable().trim());
	        	psDelete.setString(3, cpsupf.getStatus().trim());
	        	      		          
	        	flag = psDelete.executeUpdate();
	          
	        } catch (SQLException e) {
	            LOGGER.error("delete Cpsupf()", e);//IJTI-1561
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psDelete, rs);
	        }
			
	}
	


	@Override
	public Cpsupf getCpsupfItem(String chdrnum, String crtable) {
		Cpsupf cpsupf = new Cpsupf();
      	
      	StringBuilder sql = new StringBuilder("SELECT * FROM CPSUPF");
      //ILB-523
      //sql.append(" WHERE RTRIM(LTRIM(CHDRNUM))=? AND RTRIM(LTRIM(CRTABLE))=? AND STATUS=? "); 
      	sql.append(" WHERE CHDRNUM=? AND CRTABLE=? AND STATUS=? ");
      	PreparedStatement ps=null;
      	ResultSet rs=null;
      	
      	try {
      		ps=getPrepareStatement(sql.toString());
      		 //ILB-523
      		/*ps.setString(1, chdrnum.trim());
      		ps.setString(2, crtable.trim());*/
      		ps.setString(1, StringUtil.fillSpace(chdrnum.trim(), DD.chdrnum.length));
      		ps.setString(2, StringUtil.fillSpace(crtable.trim(), DD.crtable.length));
      		ps.setString(3, "R");
      		rs=ps.executeQuery();
        while (rs.next()) {
        	cpsupf.setChdrpfx(rs.getString("CHDRPFX"));
        	cpsupf.setChdrcoy(rs.getString("CHDRCOY"));
        	cpsupf.setChdrnum(rs.getString("CHDRNUM"));
        	cpsupf.setTranno(rs.getInt("TRANNO"));
        	cpsupf.setBatctrcde(rs.getString("BATCTRCDE"));
        	cpsupf.setRider(rs.getString("RIDER"));
        	cpsupf.setCrtable(rs.getString("CRTABLE"));
        	cpsupf.setCnttype(rs.getString("CNTTYPE"));
        	cpsupf.setComprskcode(rs.getString("COMPRSKCODE"));
        	cpsupf.setComppremcode(rs.getString("COMPPREMCODE"));
        	cpsupf.setCompsumss(rs.getDouble("COMPSUMSS"));
        	cpsupf.setCompbilprm(rs.getDouble("COMPBILPRM"));
        	cpsupf.setRefprm(rs.getDouble("REFPRM"));
        	cpsupf.setRefadjust(rs.getDouble("REFADJUST"));
        	cpsupf.setRefamt(rs.getDouble("REFAMT"));
        	cpsupf.setStatus(rs.getString("STATUS"));
        	cpsupf.setPayrnum(rs.getString("PAYRNUM"));
        	cpsupf.setReqntype(rs.getString("REQNTYPE"));
        	cpsupf.setBankacckey(rs.getString("BANKACCKEY"));
        	cpsupf.setBankkey(rs.getString("BANKKEY"));
        	cpsupf.setBankdesc(rs.getString("BANKDESC"));
        	cpsupf.setCompstatus(rs.getString("COMPSTATUS"));
        	cpsupf.setLife(rs.getString("LIFE"));
        	cpsupf.setCoverage(rs.getString("COVERAGE"));	        	
        	cpsupf.setPlnsfx(rs.getInt("PLNSFX"));
        	
            }
        } catch (SQLException e) {
        LOGGER.error("getItemByContractNum()", e);//IJTI-1561
        throw new SQLRuntimeException(e);
    } finally {
        close(ps, rs);
    }
    return cpsupf;
}


	@Override
	public List<Cpsupf> getCpsupfRcdList(String chdrcoy, String chdrnum, String Status) {
		Cpsupf cpsupf = null;
		PreparedStatement psCpsuSelect = null;
		ResultSet sqlcpsupf1rs = null;
		StringBuilder sqlCpsuSelectBuilder = new StringBuilder();
		List<Cpsupf> list = new ArrayList<Cpsupf>();
		sqlCpsuSelectBuilder.append("SELECT * FROM CPSUPF");
		sqlCpsuSelectBuilder.append(" WHERE  CHDRCOY = ? AND CHDRNUM = ? AND STATUS=? ");		
		
		psCpsuSelect = getPrepareStatement(sqlCpsuSelectBuilder.toString());
		
		try {
			psCpsuSelect.setString(1, chdrcoy.trim());
			psCpsuSelect.setString(2, chdrnum.trim());
			psCpsuSelect.setString(3, Status.trim());
		
			sqlcpsupf1rs = executeQuery(psCpsuSelect);
			while (sqlcpsupf1rs.next()) {
				
				cpsupf = new Cpsupf();
				
				cpsupf.setChdrpfx(sqlcpsupf1rs.getString("CHDRPFX"));
	        	cpsupf.setChdrcoy(sqlcpsupf1rs.getString("CHDRCOY"));
	        	cpsupf.setChdrnum(sqlcpsupf1rs.getString("CHDRNUM"));
	        	cpsupf.setTranno(sqlcpsupf1rs.getInt("TRANNO"));
	        	cpsupf.setBatctrcde(sqlcpsupf1rs.getString("BATCTRCDE"));
	        	cpsupf.setRider(sqlcpsupf1rs.getString("RIDER"));
	        	cpsupf.setCrtable(sqlcpsupf1rs.getString("CRTABLE"));
	        	cpsupf.setCnttype(sqlcpsupf1rs.getString("CNTTYPE"));
	        	cpsupf.setComprskcode(sqlcpsupf1rs.getString("COMPRSKCODE"));
	        	cpsupf.setComppremcode(sqlcpsupf1rs.getString("COMPPREMCODE"));
	        	cpsupf.setCompsumss(sqlcpsupf1rs.getDouble("COMPSUMSS"));
	        	cpsupf.setCompbilprm(sqlcpsupf1rs.getDouble("COMPBILPRM"));
	        	cpsupf.setRefprm(sqlcpsupf1rs.getDouble("REFPRM"));
	        	cpsupf.setRefadjust(sqlcpsupf1rs.getDouble("REFADJUST"));
	        	cpsupf.setRefamt(sqlcpsupf1rs.getDouble("REFAMT"));
	        	cpsupf.setStatus(sqlcpsupf1rs.getString("STATUS"));
	        	cpsupf.setPayrnum(sqlcpsupf1rs.getString("PAYRNUM"));
	        	cpsupf.setReqntype(sqlcpsupf1rs.getString("REQNTYPE"));
	        	cpsupf.setBankacckey(sqlcpsupf1rs.getString("BANKACCKEY"));
	        	cpsupf.setBankkey(sqlcpsupf1rs.getString("BANKKEY"));
	        	cpsupf.setBankdesc(sqlcpsupf1rs.getString("BANKDESC"));
	        	cpsupf.setCompstatus(sqlcpsupf1rs.getString("COMPSTATUS"));
	        	cpsupf.setValidflag(sqlcpsupf1rs.getString("VALIDFLAG"));
	        	cpsupf.setLife(sqlcpsupf1rs.getString("LIFE"));
	        	cpsupf.setCoverage(sqlcpsupf1rs.getString("COVERAGE"));	        	
	        	cpsupf.setPlnsfx(sqlcpsupf1rs.getInt("PLNSFX"));	        	
				
				list.add(cpsupf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchCovrRecordForContract()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psCpsuSelect, sqlcpsupf1rs);
		}
		return list;
	}
	
		@Override
	public void updateCpsupf(Cpsupf cpsupf) {
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE CPSUPF SET VALIDFLAG='1' ");
		sb.append("WHERE CHDRNUM=? AND CRTABLE=? AND STATUS=?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
				ps = getPrepareStatement(sb.toString());	
			    ps.setString(1, cpsupf.getChdrnum());
			    ps.setString(2, cpsupf.getCrtable());
			    ps.setString(3, cpsupf.getStatus()); 
			   
			    ps.executeUpdate();					
			
		}catch (SQLException e) {
			LOGGER.error("updateCpsupf()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		
	}
	
	
	@Override
	public List<Cpsupf> getCpsubyTranno(String chdrnum, String tranno) {
		/*Cpsupf cpsupf = new Cpsupf();*/
		List<Cpsupf> cpsupfList = new ArrayList<Cpsupf>();
      	
      	StringBuilder sql = new StringBuilder("SELECT * FROM CPSUPF");
      	sql.append(" WHERE CHDRNUM= ? AND TRANNO=? AND STATUS='A' AND VALIDFLAG='1' ");
      	PreparedStatement ps=null;
      	ResultSet rs=null;
      	
      	try {
      		ps=getPrepareStatement(sql.toString());
      		ps.setString(1, chdrnum.trim());
      		ps.setString(2, tranno.trim());
      		rs = executeQuery(ps);
      		System.out.print("result set" + rs);
        while (rs.next()) {
		Cpsupf result = new Cpsupf();
        	result.setChdrpfx(rs.getString("CHDRPFX"));
        	result.setChdrcoy(rs.getString("CHDRCOY"));
        	result.setChdrnum(rs.getString("CHDRNUM"));
        	result.setTranno(rs.getInt("TRANNO"));
        	result.setBatctrcde(rs.getString("BATCTRCDE"));
        	result.setRider(rs.getString("RIDER"));
        	result.setCrtable(rs.getString("CRTABLE"));
        	result.setComprskcode(rs.getString("COMPRSKCODE"));
        	result.setComppremcode(rs.getString("COMPPREMCODE"));
        	result.setCompsumss(rs.getDouble("COMPSUMSS"));
        	result.setCompbilprm(rs.getDouble("COMPBILPRM"));
        	result.setRefprm(rs.getDouble("REFPRM"));
        	result.setRefadjust(rs.getDouble("REFADJUST"));
        	result.setRefamt(rs.getDouble("REFAMT"));
        	result.setStatus(rs.getString("STATUS"));
        	result.setPayrnum(rs.getString("PAYRNUM"));
        	result.setReqntype(rs.getString("REQNTYPE"));
        	result.setBankacckey(rs.getString("BANKACCKEY"));
        	result.setBankkey(rs.getString("BANKKEY"));
        	result.setBankdesc(rs.getString("BANKDESC"));
        	result.setCompstatus(rs.getString("COMPSTATUS"));
        	cpsupfList.add(result);
        	
            }
        } catch (SQLException e) {
        LOGGER.error("getCpsupfbyTranno()", e);//IJTI-1561
        throw new SQLRuntimeException(e);
    } finally {
        close(ps, rs);
    }
    return cpsupfList;
}


}