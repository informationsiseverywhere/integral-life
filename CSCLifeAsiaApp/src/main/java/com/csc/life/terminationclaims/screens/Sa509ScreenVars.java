package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR50D
 * @version 1.0 generated on 30/08/09 07:14
 * @author Quipoz
 */
public class Sa509ScreenVars extends SmartVarModel { 


public FixedLengthStringData dataArea = new FixedLengthStringData(244); //ILIFE-2472 
	public FixedLengthStringData dataFields = new FixedLengthStringData(116).isAPartOf(dataArea, 0); //ILIFE-2472
	
	//upper part
	public FixedLengthStringData optdsc = DD.optdsc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData notifinum = DD.aacct.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,37);
	public FixedLengthStringData claimant = DD.claimant.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData clamnme = DD.clamnme.copy().isAPartOf(dataFields,92);
	public FixedLengthStringData relation = DD.relationwithowner.copy().isAPartOf(dataFields,111);
	public FixedLengthStringData indxflg = DD.indxflg.copy().isAPartOf(dataFields,115);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 116);
	public FixedLengthStringData optdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData notifinumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData claimantErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData clamnmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData relationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData indxflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 148);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] notifinumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] claimantOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] clamnmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] relationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] indxflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	//lower part
	//lower part
	public FixedLengthStringData subfileArea = new FixedLengthStringData(3147);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(3032).isAPartOf(subfileArea, 0);
	//Select
	public ZonedDecimalData select = DD.select.copyToZonedDecimal().isAPartOf(subfileFields,0);
	//Sequence Number
	public PackedDecimalData seqnoen = DD.seqnoen.copy().isAPartOf(subfileFields,1);
	public PackedDecimalData seqnogp = DD.seqnogp.copy().isAPartOf(subfileFields,4);
	//Investigation Result
	public FixedLengthStringData accdesc = DD.accdesc1.copy().isAPartOf(subfileFields,7);
	//Date and Time
	public ZonedDecimalData effdates = DD.effdates.copyToZonedDecimal().isAPartOf(subfileFields,3007);
	public FixedLengthStringData acctime = DD.acctime.copy().isAPartOf(subfileFields,3017);
	//Userid
	public FixedLengthStringData userid = DD.userid.copy().isAPartOf(subfileFields,3022);
			
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 3032);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData seqnoenErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData seqnogpErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData accdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData effdatesErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData acctimeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
			
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 3060);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] seqnoenOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] seqnogpOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] accdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] effdatesOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] acctimeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
			
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 3144);
				
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	public FixedLengthStringData effdatesDisp = new FixedLengthStringData(10);
		
	public LongData Sa509screensflWritten = new LongData(0);
	public LongData Sa509screenctlWritten = new LongData(0);
	public LongData Sa509screenWritten = new LongData(0);
	public LongData Sa509protectWritten = new LongData(0);
	public GeneralTable sa509screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}
	public GeneralTable getScreenSubfileTable() {
		return sa509screensfl;
	}
	public Sa509ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01","02","-01","13",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdscOut,new String[] {"03","25","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(notifinumOut,new String[] {"96","40","-96","44", "90", null, null, null, null, null, null, null});	
		fieldIndMap.put(lifcnumOut,new String[] {"97","41","-97","45", "91", null, null, null, null, null, null, null});		
		fieldIndMap.put(lifenameOut,new String[] {"98","42","-97","45", "92", null, null, null, null, null, null, null});		
		fieldIndMap.put(claimantOut,new String[] {"99","43","-98","46", "93", null, null, null, null, null, null, null});	
		fieldIndMap.put(clamnmeOut,new String[] {"101","45","-100","48", "95", null, null, null, null, null, null, null});		
		fieldIndMap.put(relationOut,new String[] {"104","48","-103","51", "98", null, null, null, null, null, null, null});		
		fieldIndMap.put(accdescOut,new String[] {"122","65","-120","68", "115", null, null, null, null, null, null, null});
		fieldIndMap.put(indxflgOut,new String[] {"05","26","-05","04", null, null, null, null, null, null, null, null});
		//ILIFE-1138 STARTS
		screenSflFields = new BaseData[] {select, seqnoen, seqnogp, accdesc, effdates,acctime, userid};
		screenSflOutFields = new BaseData[][] {selectOut, seqnoenOut, seqnogpOut, accdescOut, effdatesOut,acctimeOut, useridOut};
		screenSflErrFields = new BaseData[] {selectErr, seqnoenErr, seqnogpErr, accdescErr, effdatesErr,acctimeErr, useridErr};
		//ILIFE-1138 ENDS
		screenSflDateFields = new BaseData[] {effdates};
		screenSflDateErrFields = new BaseData[] {effdatesErr};
		screenSflDateDispFields = new BaseData[] {effdatesDisp};
		
		screenFields = new BaseData[] {optdsc,notifinum,lifcnum,lifename,claimant,clamnme,relation,indxflg};
		screenOutFields = new BaseData[][] {optdscOut,notifinumOut,lifcnumOut,lifenameOut,claimantOut,clamnmeOut,relationOut,indxflgOut};
		screenErrFields = new BaseData[] {optdscErr,notifinumErr,lifcnumErr,lifenameErr,claimantErr,clamnmeErr,relationErr,indxflgErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sa509screen.class;
		screenSflRecord = Sa509screensfl.class;
		screenCtlRecord = Sa509screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sa509protect.class;
	}
	
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sa509screenctl.lrec.pageSubfile);
	}
	
}
