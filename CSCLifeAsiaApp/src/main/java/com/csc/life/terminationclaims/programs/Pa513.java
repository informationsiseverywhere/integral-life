/**
 * Pa513 —— Notification History from Pa512 hyperlink
 */
package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart.procedures.Genssw;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.dao.NohspfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Nohspf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.life.terminationclaims.screens.Sa513ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.dao.CrelpfDAO;
import com.csc.fsu.clients.dataaccess.model.Crelpf;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The details of  the  contract  being  enquired  upon  will be
*     stored in the  CHDRENQ  I/O  module. Retrieve the details and
*     set up the header portion of the screen.
*
*     Look up the following descriptions and names:
*
*          Contract Type, (CNTTYPE) - long description from T5688,
*
*          Contract Status,  (STATCODE)  -  short  description from
*          T3623,
*
*          Premium Status,  (PSTATCODE)  -  short  description from
*          T3588,
*
*          Servicing branch, - long description from T1692,
*
*          The owner's client (CLTS) details.
*
*          The joint owner's  client  (CLTS) details if they exist.
*
*     The PAYR file is read for the contract,followed by T3620.
*     If the contract requires bank details(i.e Direct Debit with
****  T3620-DDIND NOT = SPACES),then the Mandate file is read
****  to obtain the Mandate detalis.
*     T3620-DDIND NOT = SPACES or Credit Card with T3620-CRCIND
*     NOT = SPACES),then the Mandate file is read to obtain the
*     Mandate detalis.
*
*     Set up the  Factoring  House, (CHDR-FACTHOUS), on the screen.
*     Valid Factoring House  codes  are held on T3684. (See GETDESC
*     for an example of how to obtain the long description from the
*     DESC data-set. Do  not  call  GETDESC,  the  appropriate code
*     should be moved into the mainline.
*
*     Set up the  Bank  Code,  (CHDR-BANKKEY),  on  the  screen and
*     obtain the  corresponding  description  from the Bank Details
*     data-set BABR.  Read  BABR  with  a  key  of CHDR-BANKKEY and
*     obtain BABR-BANKDESC.
*
*     Set up the  Account  Number, (CHDR-BANKACCKEY), on the screen
*     and  obtain  the  correeponding  description  from  the  CLBL
*     data-set.  Read  CLBL   with   a   key  of  CHDR-BANKKEY  and
*     CHDR-BANKACCKEY and obtain CLBL-BANKADDDSC and CLBL-CURRCODE.
*
*     If the Assignee client, (CHDR-ASGNNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the usual way.
*
*     If the Despatch client, (CHDR-DESPNUM), is non-blank then use
*     it to access CLTS and obtain the address and postcode. Format
*     the name in the ususal way.
*
*
*
* Validation
* ----------
*
*     Skip this section  if  returning  from  an optional selection
*     (current stack position action flag = '*').
*
*     The only validation  required  is  of  the  indicator field -
*     CONBEN. This may be space or 'X'.
*
*
* Updating
* --------
*
*     There is no updating in this program.
*
*
* Next Program
* ------------
*
*     If "CF11" was  pressed  move  spaces  to  the current program
*     position in the program stack and exit.
*
*     If returning from  processing  a  selection  further down the
*     program stack, (current  stack  action  field  will  be '*'),
*     restore the next  8  programs  from  the  WSSP and remove the
*     asterisk.
*
*     If nothing was  selected move '*' to the current stack action
*     field, add 1 to the program pointer and exit.
*
*     If a selection has  been found use GENSWCH to locate the next
*     program(s)  to process  the  selection,  (up  to  8).  Use  a
*     function of 'A'. Save  the  next  8 programs in the stack and
*     replace them with  the  ones  returned from GENSWCH. Place an
*     asterisk in the  current  stack  action  field,  add 1 to the
*     program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class Pa513 extends ScreenProgCS {
	private static final Logger LOGGER = LoggerFactory.getLogger(Pa513.class);
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final String CLMPREFIX = "CLMNTF";
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA513");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);

	
	private Chdrpf chdrpf = new Chdrpf();

	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sa513ScreenVars sv = getPScreenVars() ;
	private FormatsInner formatsInner = new FormatsInner();
	private Clntpf clntpf;
	private Clexpf clexpf;
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	
	private NohspfDAO nohspfDAO = getApplicationContext().getBean("nohspfDAO", NohspfDAO.class);
	private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ClexpfDAO clexpfDAO = getApplicationContext().getBean("clexpfDAO", ClexpfDAO.class);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private CrelpfDAO crelpfDAO = getApplicationContext().getBean("crelpfDAO", CrelpfDAO.class);
	private Crelpf crelpf = null;	
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private Fluppf fluppf = null ;
	/* WSAA-MISCELLANEOUS-FLAGS */
	private ZonedDecimalData wsaaSelectedlife = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSelected = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSelectFlagLife = new FixedLengthStringData(1);
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");
	boolean rpuConfig = false;
	private static final String g931 = "G931";
	protected static final String h093 = "H093";
	protected static final String rrng = "RRNG";
	private Batckey wsaaBatchkey = new Batckey();
	private Subprogrec subprogrec = new Subprogrec();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	protected static final String chdrenqrec = "CHDRENQREC";
	protected ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	boolean rpuMetNotFund;
	int subFileCount;
	private Notipf notipf=new Notipf();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator planLevel = new Validator(wsaaPlanproc, "Y");
	 protected static final String e186 = "E186";
	 String inctype="DC";
	 String inctype1="RP";
	 String rgpytype="HS";
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2690, 
		exit2090
	}
	
	public Pa513() {
		super();
		screenVars = sv;
		new ScreenModel("Sa513", AppVars.getInstance(), sv);
	}

	protected Sa513ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sa513ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
			LOGGER.info(e.getMessage());
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
			LOGGER.info(e.getMessage());
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}


protected void initialise1010()
	{
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*      GO TO 1090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		/* Clear the WORKING STORAGE.*/
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaImmexitFlag.set(SPACES);
		wsaaSelectFlag.set(SPACES);
		wsaaSelected.set(ZERO);
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		/* Clear the Subfile.*/
		wsaaMiscellaneousInner.wsaaSelected.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlansuff.set(ZERO);
		wsaaMiscellaneousInner.wsaaPlanSuffix.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("Sa513", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
		wsaaSelectFlagLife.set(SPACES);
		scrnparams.subfileRrn.set(1);
		
		wsaaToday.set(datcon1rec.intDate);
		wsaaSelectFlagLife.set(SPACES);
		sv.claimant.set(wsspcomn.wsaaclaimant);
		getClaimName1015();
		sv.relation.set(wsspcomn.wsaarelationship);
		//auto genarate Notifinum number
		sv.notifnum.set(wsspcomn.wsaanotificationNum);
		getLifeAssSection1020();
		getNotificationHistory1030();
	}
	
	public void getClaimName1015() {
		try{
			clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), sv.claimant.getData());/* IJTI-1523 */
			if(clntpf != null){
			sv.clamnme.set((clntpf.getSurname()!= null?clntpf.getSurname().trim(): " ")+","+(clntpf.getSurname()!= null?clntpf.getGivname().trim(): " "));/* IJTI-1523 */
			}else{
				chdrenqIO.setFormat(formatsInner.chdrenqrec);
				fatalError600();
			}
		}catch(Exception e){
			LOGGER.info(e.getMessage());
		}
	}
	
	public void getNotificationHistory1030(){
		List<Nohspf> notificationHistoryList = nohspfDAO.getNotificationHistoryRecord(wsspcomn.wsaanotificationNum.toString().trim().replace(CLMPREFIX, ""));
//		List<Nohspf> notificationHistoryList = nohspfDAO.getCombainedNotificationHistoryRecord(wsspcomn.wsaanotificationNum.toString().trim());
		for (Nohspf nohspf : notificationHistoryList) {
			sv.subfileArea.set(SPACES);
			sv.subfileFields.set(SPACES);
			// get Transaction Number
			sv.tranno.set(nohspf.getTransno());
			convertTransno(sv.tranno.toString().trim());
			sv.trdate.set(nohspf.getTransdate());
			sv.effdates.set(nohspf.getEffectdate());
			sv.trancd.set(nohspf.getTranscode());
			sv.trandes.set(nohspf.getTransdesc());
			sv.userid.set(nohspf.getUsrprf());
			addToSubfile1500();
		}
	}
	
	public void convertTransno(String transno) {
		String totalTransno = "0000";
		totalTransno = totalTransno.substring(0,totalTransno.length()-transno.length())+transno;
		sv.tranno.set(totalTransno);
	}

	private void getLifeAssSection1020() {
		try{
			clntpf = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), wsspcomn.chdrCownnum.toString());
			if(clntpf != null){
			sv.lifcnum.set(wsspcomn.chdrCownnum);
			sv.lifename.set((clntpf.getSurname()!= null?clntpf.getSurname().trim(): " ")+","+(clntpf.getSurname()!= null?clntpf.getGivname().trim(): " "));/* IJTI-1523 */
			}else{
				chdrenqIO.setFormat(formatsInner.chdrenqrec);
				fatalError600();
			}
		}catch(Exception e){
			LOGGER.info(e.getMessage());
		}
	}
	
	
	protected void addToSubfile1500()
	{
		/*ADD-LINE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("Sa513", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
		syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
	

protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* IF   WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                  */
		/*      MOVE O-K               TO WSSP-EDTERROR                 */
		/*      GO TO 2090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
		
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
				LOGGER.info(e.getMessage());
			}
		}
	
	}

	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			//goTo(GotoLabel.exit2090);
			return;
		}
		wsspcomn.edterror.set(varcom.oK);
		/*    Catering for F11.                                    <V4L011>*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			exit2090();
		}
		if(isEQ(scrnparams.statuz, "CALC")){
			wsspcomn.edterror.set("Y");			
			}
			
		if (isEQ(wsspcomn.flag, "I")) {
			/*       PERFORM 2100-OPTSWCH-CHCK                         <V4L011>*/
			/*       PERFORM 2200-CHECK-SCROLLING                              */
			//goTo(GotoLabel.exit2090);
			return;
		}	
		scrnparams.subfileRrn.set(1);
		
	}

protected void exit2090(){
	
	if (isNE(sv.errorSubfile, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}


protected void validate2020()
	{
	 if (isEQ(scrnparams.statuz, varcom.kill)) {
         return;
     }
     if (isEQ(wsspcomn.flag, "I")) {
         return;
     }
     if (isEQ(scrnparams.statuz, varcom.calc)) {
         wsspcomn.edterror.set("Y");
     }
}

public void validateSubfile2030(){
	subfileStart();	
	if(isNE(wsaaSelectFlagLife,"Y")){
		wsaaSelectedlife.set(ZERO);
	}
	subFileCount=0;
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
		validateSubfile2600();
		if(rpuMetNotFund) break;
		scrnparams.function.set(varcom.supd);
		subfileIo4400();
		scrnparams.function.set(varcom.srdn);
		subfileIo4400();
	} 		
    //g931 if there is no selection
    if(!rpuMetNotFund){
    if (isEQ(wsaaMiscellaneousInner.wsaaSelected,0) && isEQ(rpuConfig,false)) {
    	scrnparams.errorCode.set(g931);
    	wsspcomn.edterror.set("Y");
    	}	
    }
 }
     
protected void subfileIo4400() {

	processScreen("Sa513", sv);
	if (isNE(scrnparams.statuz, varcom.oK) && isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}

protected void validateSubfile2600()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: 
				readNextRecord2610();
				updateErrorIndicators2650();
			case exit2690: 
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
			LOGGER.info(e.getMessage());
		}
	}
}

protected void updateErrorIndicators2650()
{
if (isNE(sv.errorSubfile,SPACES)) {
	wsspcomn.edterror.set("Y");
}
else {
	wsspcomn.edterror.set(varcom.oK);
	//goTo(GotoLabel.exit2690);
	return;
}
	sv.select.set(SPACES);
	sv.selectOut[varcom.pr.toInt()].set("Y");		
	scrnparams.function.set(varcom.supd);
	processScreen("Sa513", sv);	
}

protected void readNextRecord2610()
{

	/* If record has space in the select AND it has already*/
	/* been processed then count it as a valid selection then*/
	/* exit. This is to force the user to only have a single*/
	/* selection given subsequent program limitations.*/
	
		if (isNE(sv.select,SPACES)) {
			wsaaMiscellaneousInner.wsaaSelected.add(1);
		}
}

protected void subfileStart()	
{
	/*VALIDATE-SUBFILE*/
	scrnparams.function.set(varcom.sstrt);
	processScreen("Sa513", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateWssp3010();
		return ;
	}

protected void updateWssp3010()
{
	wsspcomn.sbmaction.set(scrnparams.action);
	wsaaBatchkey.set(wsspcomn.batchkey);
	wsspcomn.batchkey.set(wsaaBatchkey);
	if (isEQ(scrnparams.statuz, "BACH")) {
		//goTo(GotoLabel.exit3090);
		return;
	}
	/*  Update WSSP Key details*/
	wsspcomn.flag.set("I");
}

protected void keeps3070()
{
	/*   Store the contract header for use by the transaction programs*/
	//ILB-459 start
	chdrpfDAO.setCacheObject(chdrpf);
	//ILB-459 end
}


	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
{
	nextProgram4010();
	
	
	/*NEXT-PROGRAM
	wsspcomn.programPtr.add(1);
	EXIT*/
}

protected void continue4080()
{
	/* Set up for execution of the component specific programs to be*/
	/* executed. Note that this includes a set of follow up records*/
	/* for each payment created according to table settings.*/
//	setUpForComponents4300();
	/* Read the  Program  table T5671 for  the components programs*/
	/* to be executed next.*/
	programTables4400();
	/*UPDATE-SCREEN*/
	screenUpdate4600();
}

protected void screenUpdate4600()
{
	/*PARA*/
	scrnparams.function.set(varcom.supd);
	processScreen("Sa513", sv);		
}

protected void programTables4400()
{
		try {
			readProgramTable4410();
		}
		catch (GOTOException e){
			LOGGER.info(e.getMessage());
		}
}

protected void readProgramTable4410()
{
	/* Read the  Program  table T5671 for  the components programs to*/
	/* be executed next.*/
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
	stringVariable1.addExpression(wsaaMiscellaneousInner.wsaaCrtable);
	
	/* Reset the Action to '*' signifying action desired to the next*/
	/* program.*/
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
	wsspcomn.nextprog.set(wsaaProg);
	
	
}

protected void wayout4800()
{
	/*PROGRAM-EXIT*/
	/* If control is passed to this  part of the 4000 section on the*/
	/*   way out of the program, i.e. after  screen  I/O,  then  the*/
	/*   current stack position action  flag will be  blank.  If the*/
	/*   4000-section  is  being   performed  after  returning  from*/
	/*   processing another program then  the current stack position*/
	/*   action flag will be '*'.*/
	/* If 'KILL' has been requested, (CF11), then move spaces to the*/
	/*   current program entry in the program stack and exit.*/
	if (isEQ(scrnparams.statuz, "KILL")) {
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		return ;
	}
	/* Add 1 to the program pointer and exit.*/
	wsspcomn.programPtr.add(1);
	/*EXIT*/
}

protected void nextProgram4010()
{
		if (isEQ(scrnparams.statuz,"KILL")) {
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsaaMiscellaneousInner.wsaaSub.set(1);
		wsspcomn.secActn[wsaaMiscellaneousInner.wsaaSub.toInt()].set(SPACES);
		//goTo(GotoLabel.exit4090);
		return;
	}
	if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
		initSelection4100();
	}
	else {
		scrnparams.function.set(varcom.srdn);
	}
	wsaaSelectFlag.set("N");
	wsaaImmexitFlag.set("N");
	while ( !(wsaaSelection.isTrue()
	|| wsaaImmExit.isTrue())) {
		next4200();
	}
	
	if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
		if (wsaaImmExit.isTrue()) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			//goTo(GotoLabel.exit4090);
			return;
		}
	}


		if(isEQ(wsaaSelectFlag,"N")){
			if (wsaaImmExit.isTrue()) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				wsspcomn.programPtr.add(1);
				scrnparams.subfileRrn.set(1);
				wsspcomn.nextprog.set(wsaaProg);
				//goTo(GotoLabel.exit4090);
				return;
			}
		}
		if(isEQ(wsaaSelectFlag,"Y")){
			//ICIL-1502 jumpt to respective screen with corresonding data
			/**
			 * get the corresponding data with transno which connect Nohspf and Notipf
			 */
			wsspcomn.WsspCoinsuranceFlag.set("T");
			wsspcomn.tranno.set(sv.tranno.getData().trim());/* IJTI-1523 */
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(1);
			wsspcomn.nextprog.set(wsaaProg);
			//goTo(GotoLabel.exit4090);
			return;
		}
		continue4080();
}

protected void next4200()
{
	try {
		nextRec4210();
		ifSubfileEndp4220();
	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
		LOGGER.info(e.getMessage());
	}
}

protected void nextRec4210()
{
	/* Read next subfile record sequentially.*/
	processScreen("Sa513", sv);
	if (isNE(scrnparams.statuz, varcom.oK)
	&& isNE(scrnparams.statuz, varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}

protected void ifSubfileEndp4220()
{
	/* Check for the end of the Subfile.*/
	/* If end of Subfile re-load the Saved four programs to Return*/
	/*   to initial stack order.*/
	if (isEQ(scrnparams.statuz, varcom.endp)) {
		reloadProgsWssp4230();
//		goTo(GotoLabel.exit4290);
	}
	/* Exit if a selection has been found. NOTE that we space*/
	/* and protect the selection as the subsequent transactions*/
	/* cannot handle multiple entry.*/
//	sv.select.set("1");
	if (isNE(sv.select, SPACES)) {
		sv.select.set(SPACES);
		wsaaSelectFlagLife.set("Y");
		wsaaSelectFlag.set("Y");
	}
	scrnparams.function.set(varcom.srdn);
//	goTo(GotoLabel.exit4290);
}

protected void reloadProgsWssp4230()
{
	/* Set Flag for immediate exit as the next action is to go to the*/
	/* next program in the stack.*/
	/* Set Subscripts and call Loop.*/
	wsaaImmexitFlag.set("Y");
	compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
	wsaaMiscellaneousInner.wsaaSub2.set(1);
	for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
		loop34240();
	}
//	goTo(GotoLabel.exit4290);
}

protected void loop34240()
{
	/* Re-load the Saved four programs from Working Storage to the*/
	/* WSSP Program Stack.*/
	wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()].set(wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()]);
	wsaaMiscellaneousInner.wsaaSub1.add(1);
	wsaaMiscellaneousInner.wsaaSub2.add(1);
}

protected void initSelection4100()
{
	try {
		saveNextProgs4110();
	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
		LOGGER.info(e.getMessage());
	}
}

protected void saveNextProgs4110()
{
	/* Save the  next  four  programs  after  P  into  Working*/
	/* Storage for re-instatement at the end of the Subfile.*/
	compute(wsaaMiscellaneousInner.wsaaSub1, 0).set(add(1, wsspcomn.programPtr));
	wsaaMiscellaneousInner.wsaaSub2.set(1);
	for (int loopVar1 = 0; !(loopVar1 == 4); loopVar1 += 1){
		loop14120();
	}
	/* First Read of the Subfile for a Selection.*/
	scrnparams.function.set(varcom.sstrt);
	/* Go to the first  record  in  the Subfile to  find the First*/
	/* Selection.*/
//	goTo(GotoLabel.exit4190);
}

protected void loop14120()
{
	/* This loop will load the next four  programs from WSSP Stack*/
	/* into a Working Storage Save area.*/
	wsaaSecProg[wsaaMiscellaneousInner.wsaaSub2.toInt()].set(wsspcomn.secProg[wsaaMiscellaneousInner.wsaaSub1.toInt()]);
	wsaaMiscellaneousInner.wsaaSub1.add(1);
	wsaaMiscellaneousInner.wsaaSub2.add(1);
}

protected void saveProgram4100()
{
	/*SAVE*/
	wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
} 
protected void restoreProgram4100()
{
	/*PARA*/
	wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
	sub1.add(1);
	sub2.add(1);
	/*EXIT*/
}

protected void callGenssw4300()
{
	callSubroutine4310();
}

protected void callSubroutine4310()
{
	callProgram(Genssw.class, gensswrec.gensswRec);
	if (isNE(gensswrec.statuz, varcom.oK)
	&& isNE(gensswrec.statuz, varcom.mrnf)) {
		syserrrec.params.set(gensswrec.gensswRec);
		syserrrec.statuz.set(gensswrec.statuz);
		fatalError600();
	}
	/* If an entry on T1675 was not found by genswch redisplay the scre*/
	/* with an error and the options and extras indicator*/
	/* with its initial load value*/
	if (isEQ(gensswrec.statuz, varcom.mrnf)) {
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*     MOVE V045                TO SCRN-ERROR-CODE               */
		scrnparams.errorCode.set(h093);//ILB-459
		wsspcomn.nextprog.set(scrnparams.scrname);
		return ;
	}
	/*    load from gensw to wssp*/
	compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
	wsaaY.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		loadProgram4400();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
}
protected void checkFollowups1850() {
	//ILIFE-6296 by wli31
	String chdrnum = (chdrpf.getChdrnum()!= null?chdrpf.getChdrnum(): " ");//ILB-459
	String chdrcoy = (chdrpf.getChdrcoy()!=null?chdrpf.getChdrcoy().toString():" ");//ILB-459
	boolean recFound = fluppfDAO.checkFluppfRecordByChdrnum(chdrcoy, chdrnum);
}
protected void restoreProgram4200()
{
	/*RESTORE*/
	wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT*/
}

protected void loadProgram4400()
{
	/*RESTORE1*/
	wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
	wsaaX.add(1);
	wsaaY.add(1);
	/*EXIT1*/
}
public Chdrpf getChdrpf() {
		return chdrpf;
	}

	public void setChdrpf(Chdrpf chdrpf) {
		this.chdrpf = chdrpf;
	}

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
    private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
}


	protected void prepareName(String firstName, String lastName) {
		String fullName = "";
		if (isNE(firstName, SPACES)) {
			fullName = getStringUtil().plainName(firstName, lastName, ",");
			 
		} else {
			fullName = lastName;
		}
		wsspcomn.longconfname.set(fullName);
	}

	/**
	 * can be overriden to change the its behaviour
	 * @return
	 */
	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}
	
	public Subprogrec getSubprogrec() {
		return subprogrec;
	}
	public void setSubprogrec(Subprogrec subprogrec) {
		this.subprogrec = subprogrec;
	}
	
	/*
	 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
	 */
	private static final class WsaaMiscellaneousInner { 

			/* WSAA-MISCELLANEOUS */
		private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).init(1);
		private PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
		private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
		private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(4, 0).setUnsigned();
		private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).setUnsigned();
		private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
		private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0);
		private ZonedDecimalData wsaaSelected = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	}
	
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 
		 private FixedLengthStringData RRNJ = new FixedLengthStringData(4).init("RRNJ");
		 private FixedLengthStringData RRNG = new FixedLengthStringData(4).init("RRNG");
	}

}


