package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sh5c3ScreenVars extends SmartVarModel{
	
	public FixedLengthStringData dataArea = new FixedLengthStringData(92);
	public FixedLengthStringData dataFields = new FixedLengthStringData(28).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData efdate = DD.efdate.copyToZonedDecimal().isAPartOf(dataFields,11);
	public FixedLengthStringData claimnmber = DD.claimnmber.copy().isAPartOf(dataFields,19);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 28);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData efdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData claimnmberErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 44);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] efdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] claimnmberOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData efdateDisp = new FixedLengthStringData(10);

	public LongData Sh5c3screenWritten = new LongData(0);
	public LongData Sh5c3protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh5c3ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		fieldIndMap.put(chdrselOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimnmberOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(efdateOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {chdrsel, efdate, action, claimnmber};
		screenOutFields = new BaseData[][] {chdrselOut, efdateOut, actionOut, claimnmberOut};
		screenErrFields = new BaseData[] {chdrselErr, efdateErr, actionErr, claimnmberErr};
		screenDateFields = new BaseData[] {efdate};
		screenDateErrFields = new BaseData[] {efdateErr};
		screenDateDispFields = new BaseData[] {efdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = Sh5c3screen.class;
		protectRecord = Sh5c3protect.class;
	}


}
