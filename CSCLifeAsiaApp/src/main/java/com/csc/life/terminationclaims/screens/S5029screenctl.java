package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5029screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "S5029screensfl";
		lrec.subfileClass = S5029screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 10;
		lrec.pageSubfile = 9;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 11, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5029ScreenVars sv = (S5029ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5029screenctlWritten, sv.S5029screensflWritten, av, sv.s5029screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5029ScreenVars screenVars = (S5029ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownernum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.btdateDisp.setClassString("");
		screenVars.ptdateDisp.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.lifcnum.setClassString("");
		screenVars.linsname.setClassString("");
		screenVars.jlifcnum.setClassString("");
		screenVars.jlinsname.setClassString("");
	}

/**
 * Clear all the variables in S5029screenctl
 */
	public static void clear(VarModel pv) {
		S5029ScreenVars screenVars = (S5029ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.rstate.clear();
		screenVars.chdrnum.clear();
		screenVars.cownnum.clear();
		screenVars.ownernum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.btdateDisp.clear();
		screenVars.btdate.clear();
		screenVars.ptdateDisp.clear();
		screenVars.ptdate.clear();
		screenVars.pstate.clear();
		screenVars.lifcnum.clear();
		screenVars.linsname.clear();
		screenVars.jlifcnum.clear();
		screenVars.jlinsname.clear();
	}
}
