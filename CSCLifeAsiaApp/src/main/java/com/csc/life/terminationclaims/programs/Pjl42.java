package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmdpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmhpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.CrsvpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmdpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmhpf;
import com.csc.life.terminationclaims.dataaccess.model.Crsvpf;
import com.csc.life.terminationclaims.recordstructures.Deathrec;
import com.csc.life.terminationclaims.screens.Sjl42ScreenVars;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pjl42 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl42.class);
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL42");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaStoredCurrency2 = new FixedLengthStringData(3).init(SPACES);
	private ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	protected FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	protected FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaDeadLife = new FixedLengthStringData(8);
	protected T5687rec t5687rec = new T5687rec();
	protected T6598rec t6598rec = new T6598rec();
	/* TABLES */
	protected ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	protected Sjl42ScreenVars sv =getLScreenVars();
	boolean CMDTH010Permission  = false;
	private static final String feaConfigPreRegistartion= "CMDTH010";
	protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected List<Covrpf> covrpfList;
	protected Covrpf covrpf = null;
	protected ItemDAO itempfDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected List<Itempf> itempfList;
	protected Itempf itempf = null;
	protected Deathrec deathrec = getDeathrec();
	private String  t6640Item;	
	private T5688rec t5688rec = new T5688rec();
	protected ClntpfDAO clntpfDao = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private CrsvpfDAO crsvpfDAO = getApplicationContext().getBean("crsvpfDAO", CrsvpfDAO.class);
	private String  t5688 = "T5688";	
	private Clmdpf clmdclm = null;
	protected Clntpf cltspf = new Clntpf();
	protected Batckey wsaaBatckey = new Batckey();
	protected LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	protected Lifepf lifepf = new Lifepf();	
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private Fluppf fluppf = null ;
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	protected Chdrpf chdrpf = new Chdrpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO" , CattpfDAO.class);
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	protected ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	protected Descpf descpf = null;
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	protected Gensswrec gensswrec = new Gensswrec();
	protected Sftlockrec sftlockrec = new Sftlockrec();
	protected FormatsInner formatsInner = new FormatsInner();
	private static final String SPLIT_SIGN = "_";
	protected static final String e304 = "E304";
	protected static final String e186 = "E186";
	protected static final String g620 = "G620";
	protected static final String h093 = "H093";
	protected static final String rfs8 = "RFS8";
	protected static final String jl25 = "JL25";
	protected static final String jl26 = "JL26";
	protected static final String jl60 = "JL60";
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class);

	public Deathrec getDeathrec() {
		return new Deathrec();
	}

	protected enum GotoLabel implements GOTOInterface { 
		DEFAULT,
		exit3090	
	}

	public Pjl42() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl42", AppVars.getInstance(), sv);
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	protected Sjl42ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sjl42ScreenVars.class);
	}

	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void initialise1000()
	{
		initialise1010();
		continue1030();
	}

	protected void initialise1010()
	{
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", feaConfigPreRegistartion, appVars, "IT");
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			/*exit*/
		}		
		/* Get Todays Date and pass this as the effective date             */
		/* to the Death Claim Calculation subroutine, this                 */
		/* date will be used there to calculate the fund prices            */
		sv.dataArea.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		deathrec.effdate.set(datcon1rec.intDate);
		wsaaToday.set(datcon1rec.intDate);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/* Skip this section if returning from an optional selection,*/
		/* i.e. check the stack action flag equals '*'.*/
		/* MOVE WSSP-BATCHKEY          TO WSAA-BATCKEY.                 */
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*    GO TO 1090-EXIT.                                          */
		/* Initialise Working Storage fields.                              */
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		wsaaLife.set(SPACES);
		wsaaJlife.set(SPACES);
		wsaaStoredCurrency.set(SPACES);
		wsaaStoredCurrency2.set(SPACES);
		wsaaDeadLife.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);

		/* Dummy subfile initalisation for prototype - replace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("SJL42", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);

		/*    Dummy field initilisation for prototype version.*/

		sv.estMatValue.set(ZERO);
		sv.actvalue.set(ZERO);
		sv.dtofdeath.set(varcom.vrcmMaxDate);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);

		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrclmIO.setFunction(varcom.retrv);
			chdrclmIO.setFormat(formatsInner.chdrclmrec);
			SmartFileCode.execute(appVars, chdrclmIO);
			if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrclmIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrclmIO.getChdrcoy().toString(), chdrclmIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		sv.occdate.set(chdrpf.getOccdate());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		Hpadpf hpadpf = hpadpfDAO.getHpadData(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if(hpadpf!=null) {
			sv.riskcommdte.set(hpadpf.getRskcommdate());
		}
		descpf = this.getDescData("T5688", chdrpf.getCnttype().trim());
		if (descpf != null) {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrpf.getCownnum());

		List<String> clntnumList = new ArrayList();
		clntnumList.add(chdrpf.getCownnum());

		sv.btdate.set(chdrpf.getBtdate());
		sv.ptdate.set(chdrpf.getPtdate());
		/*    Retrieve contract status from T3623*/

		descpf = this.getDescData("T3623", chdrpf.getStatcode().trim());/* IJTI-1386 */
		if (descpf != null) {
			sv.rstate.set(descpf.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/

		descpf = this.getDescData("T3588", chdrpf.getPstcde().trim());/* IJTI-1386 */
		if (descpf != null) {
			sv.pstate.set(descpf.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}

		lifepf = new Lifepf();
		lifepf.setChdrcoy(chdrpf.getChdrcoy().toString());//ILB-459
		lifepf.setChdrnum(chdrpf.getChdrnum());//ILB-459
		lifepf = lifepfDAO.selectLifepfRecord(lifepf).get(0);

		wsaaLife.set(lifepf.getLife());
		wsaaJlife.set(lifepf.getJlife());
		wsaaDeadLife.set(lifepf.getLifcnum());
		if (isEQ(lifepf.getJlife(), "01")) {
			sv.astrsk.set("*");
			sv.asterisk.set(SPACES);
		}
		else {
			sv.astrsk.set(" ");
			sv.asterisk.set("*");
		}
		/*    Read the first life as the life retrieved above*/
		/*    may have been a joint life.*/

		List<Lifepf> lifepfList = lifepfDAO.selectLifeDetails(lifepf);

		/* Move the last record read to the screen and get the*/
		/* description.*/
		if(lifepfList==null){
			fatalError600();
		}
		sv.lifcnum.set(lifepfList.get(0).getLifcnum());
		for(Lifepf lifepf:lifepfList){
			clntnumList.add(lifepf.getLifcnum());
		}
		Map<String,Clntpf> cltspfMap = clntpfDao.searchClntRecord("CN", wsspcomn.fsuco.toString(), clntnumList);
		if(cltspfMap.containsKey(wsspcomn.fsuco.toString()+ getSplitSign() +chdrpf.getCownnum())){
			cltspf = cltspfMap.get(wsspcomn.fsuco.toString()+ getSplitSign() +chdrpf.getCownnum());
			plainname();
			sv.ownername.set(wsspcomn.longconfname);}
		else{
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		if(cltspfMap.containsKey(wsspcomn.fsuco.toString()+ getSplitSign() +lifepfList.get(0).getLifcnum())){
			cltspf = cltspfMap.get(wsspcomn.fsuco.toString()+ getSplitSign() +lifepfList.get(0).getLifcnum());
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		else{
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		/*    look for joint life.*/
		if(lifepfList.size()>1 && lifepfList.get(1).getJlife() == "01"){
			sv.jlifcnum.set(lifepfList.get(1).getLifcnum());
			if(cltspfMap.containsKey(wsspcomn.fsuco.toString()+ getSplitSign() +lifepfList.get(1).getLifcnum())){
				cltspf = cltspfMap.get(wsspcomn.fsuco.toString()+ getSplitSign() +lifepfList.get(1).getLifcnum());
				plainname();
				sv.jlinsname.set(wsspcomn.longconfname);
				continue1030();
			}
			else{
				sv.jlinsnameErr.set(e304);
				sv.jlinsname.set(SPACES);
			}
		}	
		else{
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
		}
		/* Read Cattpf Record */
		Cattpf cattpf = cattpfDAO.selectRecords(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());

		if(cattpf.equals(SPACES)){
			sv.chdrnumErr.set(jl60);
		}
		else{

			sv.dtofdeath.set(cattpf.getDtofdeath());
			sv.effdate.set(cattpf.getEffdate());
			sv.causeofdth.set(cattpf.getCauseofdth());
			sv.claim.set(cattpf.getClaim());
			sv.claimStat.set(cattpf.getClamstat());
			sv.claimTyp.set(cattpf.getClamtyp());
		}

		

		if (isEQ(wsspcomn.flag, "I")) {
			sv.dtofdeathOut[varcom.pr.toInt()].set("Y");
			sv.causeofdthOut[varcom.pr.toInt()].set("Y");
			sv.effdateOut[varcom.pr.toInt()].set("Y");
		}
		/*  This next line has been removed from the above IF to         */
		/*  ensure that the field is always protected.                   */

	}		
	protected Descpf getDescData(String desctable, String descitem){
		Descpf descpf=descDAO.getdescData("IT", desctable, descitem, wsspcomn.company.toString().trim(), wsspcomn.language.toString().trim());
		return descpf;
	}

	public String getSplitSign() {
		return SPLIT_SIGN;
	}
	public StringUtil getStringUtil() {
		return stringUtil;
	}
	protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltspf.getClttype(), "C")) {
			corpname();

		} else if (isNE(cltspf.getGivname(), SPACES)) {
			String firstName = cltspf.getGivname();
			String lastName = cltspf.getSurname();
			String delimiter = ",";
			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			wsspcomn.longconfname.set(fullName);

		} else {
			wsspcomn.longconfname.set(cltspf.getSurname());
		}
		/*PLAIN-EXIT*/
	}
	protected void corpname()
	{
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		String firstName = cltspf.getLgivname();
		String lastName = cltspf.getLsurname();
		String delimiter = "";
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		/*CORP-EXIT*/
	}

	protected void continue1030()
	{		
		covrpf = new Covrpf();
		covrpf.setChdrcoy(lifepf.getChdrcoy());
		covrpf.setChdrnum(lifepf.getChdrnum());
		covrpf.setLife(lifepf.getLife());
		covrpf.setPlanSuffix(0);
		covrpf.setValidflag("1");
		covrpfList = covrpfDAO.selectCovrRecord(covrpf);

		if(covrpfList != null && covrpfList.size() > 0)
		{
			t6640Item = covrpfList.get(0).getCrtable();
			for(Covrpf covr : covrpfList)
			{
				wsaaStoredCoverage.set(covr.getCoverage());
				wsaaStoredRider.set(covr.getRider());			
				wsaaStoredCurrency.set(covr.getPremCurrency());
				processComponents1500(covr);
			}	
		}
		else{
			t6640Item = " ";
		}
		checkFollowups1850();
	}

	protected void processComponents1500(Covrpf covr)
	{
		readT56871650(covr);
		if (isEQ(lifepf.getJlife(), "00")
				|| isEQ(lifepf.getJlife(), "  ")) {
			if (isNE(covr.getJlife(), "01")) {
				selectCoverRider1700(covr);
			}
		}
		if (isEQ(lifepf.getJlife(), "01")) {
			if ((isEQ(covr.getJlife(), "01")
					|| isEQ(t5687rec.jlifePresent, "Y"))) {
				selectCoverRider1700(covr);
			}
		}
		wsaaStoredCoverage.set(covr.getCoverage());
		wsaaStoredRider.set(covr.getRider());
	}
	protected void selectCoverRider1700(Covrpf covr)
	{
		addFields1720(covr);
	}
	protected void addFields1720(Covrpf covr)
	{
		readT65981750(covr);
		sv.coverage.set(covr.getCoverage());
		sv.hcover.set(covr.getCoverage());
		if (isEQ(covr.getRider(), "00")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(covr.getRider());
		}
		if (isNE(covr.getRider(), "00")
				&& isNE(covr.getRider(), "  ")) {
			sv.coverage.set(SPACES);
		}
		sv.cnstcur.set(covr.getPremCurrency());
		sv.hcnstcur.set(covr.getPremCurrency());
		/*    MOVE CLMDCLM-CRTABLE       TO S5318-CRTABLE.*/
		sv.hcrtable.set(covr.getCrtable());

		descpf = this.getDescData("T5687", covr.getCrtable().trim());
		if (isNE(deathrec.description, SPACES)) {
			if (descpf != null) {
				sv.shortds.set(descpf.getShortdesc());
			}
			else {
				sv.shortds.fill("?");
			}
		}
		if (isNE(deathrec.description, SPACES)) {
			sv.shortds.set(deathrec.description);
		}
		sv.liencd.set(covr.getLiencd());
		deathrec.estimatedVal.set(ZERO);
		deathrec.actualVal.set(ZERO);
		deathrec.chdrChdrcoy.set(covr.getChdrcoy());
		deathrec.chdrChdrnum.set(covr.getChdrnum());
		deathrec.lifeLife.set(covr.getLife());
		deathrec.lifeJlife.set(covr.getJlife());
		deathrec.covrCoverage.set(covr.getCoverage());
		deathrec.covrRider.set(covr.getRider());
		deathrec.crtable.set(covr.getCrtable());
		deathrec.status.set(SPACES);
		deathrec.endf.set(SPACES);
		deathrec.batckey.set(wsaaBatckey);
		deathrec.element.set(SPACES);
		deathrec.fieldType.set(SPACES);
		deathrec.virtualFundStore.set(SPACES);
		deathrec.unitTypeStore.set(SPACES);
		deathrec.processInd.set(SPACES);
		deathrec.language.set(wsspcomn.language);
		while ( !(isEQ(deathrec.status, varcom.endp))) {
			callDeathMethod1800(covr);
		}
	}
	protected void callDeathMethod1800(Covrpf covr)
	{
		read1810(covr);
		if (isEQ(deathrec.status, varcom.endp)) {
			return;
		}
		setUpScreenFields1850(covr);
	}
	protected void read1810(Covrpf covr)
	{
		deathrec.element.set(SPACES);
		deathrec.currcode.set(chdrpf.getCntcurr());
		if (isEQ(t6598rec.calcprog, SPACES)) {
			deathrec.status.set(varcom.endp);
			setUpScreenFields1850(covr);
		}
		callProgram(t6598rec.calcprog, deathrec.deathRec);
		if (isEQ(deathrec.status, varcom.bomb)) {
			syserrrec.params.set(deathrec.deathRec);
			syserrrec.statuz.set(deathrec.status);
			fatalError600();
		}
		if (isNE(deathrec.status, varcom.oK)
				&& isNE(deathrec.status, varcom.endp)) {
			syserrrec.params.set(deathrec.deathRec);
			syserrrec.statuz.set(deathrec.status);
			fatalError600();
		}
		if (isEQ(deathrec.status, varcom.endp)) {
			return;
		}
		zrdecplcPojo.setCurrency(deathrec.currcode.toString());
		zrdecplcPojo.setAmountIn(deathrec.estimatedVal.getbigdata());
		callRounding5000();
		deathrec.estimatedVal.set(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setCurrency(deathrec.currcode.toString());
		zrdecplcPojo.setAmountIn(deathrec.actualVal.getbigdata());//ILIFE-7679
		callRounding5000();
		deathrec.actualVal.set(zrdecplcPojo.getAmountOut());
	}
	protected void setUpScreenFields1850(Covrpf covr)
	{
		if (isEQ(deathrec.currcode, SPACES)) {
			sv.cnstcur.set(covr.getPremCurrency());
			sv.hcnstcur.set(covr.getPremCurrency());
		}
		else {
			sv.cnstcur.set(deathrec.currcode);
			sv.hcnstcur.set(deathrec.currcode);
		}
		sv.htype.set(deathrec.fieldType);
		sv.fieldType.set(deathrec.fieldType);
		if(isEQ(deathrec.element,"00"))
			sv.vfund.set(SPACES);
		else
			sv.vfund.set(deathrec.element);
		sv.vfund.set(deathrec.element);
		sv.estMatValue.set(deathrec.estimatedVal);
		sv.hemv.set(deathrec.estimatedVal);
		sv.actvalue.set(deathrec.actualVal);
		sv.hactval.set(deathrec.actualVal);
		descpf = this.getDescData("T5687", deathrec.crtable.toString().trim());
		if (descpf != null) {
			sv.shortds.set(descpf.getShortdesc());
		}
		else {
			sv.shortds.fill("?");
		}
		if (isNE(sv.vfund, SPACES)) {
			descpf = this.getDescData("T5515", sv.vfund.toString().trim());
			if (descpf != null) {
				sv.shortds.set(descpf.getShortdesc());
			}
			else {
				sv.shortds.fill("?");
			}
		}
		if (isNE(deathrec.crtable, SPACES)) {
			sv.shortds.set(deathrec.crtable);
		}
		if (isEQ(covr.getPremCurrency(), wsaaStoredCurrency)) {
			wsaaEstimateTot.add(deathrec.estimatedVal);
			wsaaActualTot.add(deathrec.actualVal);
		}
		if (isEQ(deathrec.fieldType, "S")) {
		}
		else {
			if (isEQ(deathrec.estimatedVal, ZERO)
					&& isEQ(deathrec.actualVal, ZERO)
					&& isEQ(deathrec.processInd, SPACES)) {
				return ;
			}
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("SJL42", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	protected void callRounding5000()
	{
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setCompany(wsspcomn.company.toString());
		zrdecplcPojo.setStatuz(Varcom.oK.toString());
		zrdecplcPojo.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			fatalError600();
		}
	}

	protected void readT65981750(Covrpf covr)
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(t5687rec.dcmeth.toString());
		itempf.setItemtabl("T6598");
		itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate()));
		itempf.setItmto(new BigDecimal(0));
		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {				
				t6598rec.t6598Rec.set(StringUtil.rawToString(it.getGenarea()));
			}
		} 
	}

	protected void checkFollowups1850() {

		String chdrnum = (chdrpf.getChdrnum()!= null?chdrpf.getChdrnum().trim(): " ");
		String chdrcoy = (chdrpf.getChdrcoy()!=null?chdrpf.getChdrcoy().toString().trim():" ");
		boolean recFound = fluppfDAO.checkFluppfRecordByChdrnum(chdrcoy, chdrnum);
		if (!recFound) {
			sv.fupflg.set(SPACES);
		}  else {
			sv.fupflg.set("+");
		}
	}

	protected void readT56871650(Covrpf covr)
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(covr.getCrtable());
		itempf.setItemtabl("T5687");
		itempf.setItmfrm(new BigDecimal(covr.getCrrcd()));
		itempf.setItmto(new BigDecimal(covr.getCrrcd()));
		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {				
				t5687rec.t5687Rec.set(StringUtil.rawToString(it.getGenarea()));
			}
		} 
	}
	protected void preScreenEdit()
	{

		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

	protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
			checkForErrors2080();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			exitProgram();
		}
	}

	protected void validate2020(){

		if (isNE(sv.fupflg, "X")
				&& isNE(sv.fupflg, "+")
				&& isNE(sv.fupflg, " ")
				&& isNE(sv.fupflg, "?")) {
			sv.fupflgErr.set(g620);
			wsspcomn.edterror.set("Y");
		}

	}

	protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	 * <pre>
	 *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3010();
					updateCattpf();
					updatePtrn();
					writeChdrpf();
					writePtrn();
                   
					//ILJ-383 Starts
					if(CMDTH010Permission){
						deleteClmReserve3610();
					}
					releaseSoftlock();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
	}

	protected void updateCattpf(){

		cattpfDAO.deleteRcdByChdrnum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
	}
	
	protected void updatePtrn() {
		
		ptrnpfDAO.updateValidPtrnrevRecord(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum() ,"TASO");
	}
	
	protected void writeChdrpf(){
		List<Chdrpf> chdrList =   new ArrayList<Chdrpf>();
		Chdrpf chdrpfIO = new Chdrpf();
		chdrpfIO.setChdrcoy(chdrpf.getChdrcoy());
		chdrpfIO.setChdrnum(chdrpf.getChdrnum());	
		chdrpfIO.setTranno(chdrpf.getTranno()+1);
		chdrList.add(chdrpf);
		boolean b =  chdrpfDAO.updateChdrTranno(chdrList);
	}
	
	protected void writePtrn(){
		Ptrnpf ptrnpf = new Ptrnpf();
		ptrnpf.setChdrpfx("CH");
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());
		ptrnpf.setTranno(chdrpf.getTranno());		
		ptrnpf.setTrdt(varcom.vrcmDate.toInt());
		ptrnpf.setTrtm(varcom.vrcmTime.toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setUserT(varcom.vrcmUser.toInt());
		ptrnpf.setPtrneff(datcon1rec.intDate.toInt());
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());
		ptrnpf.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
		ptrnpf.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
		ptrnpf.setBatcbatch(wsaaBatckey.batcBatcbatch.toString());
		ptrnpf.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
		ptrnpf.setBatccoy(wsaaBatckey.batcBatccoy.toString());
		ptrnpf.setBatcpfx(wsaaBatckey.batcBatcpfx.toString());
		ptrnpf.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		ptrnpfDAO.insertPtrnPF(ptrnpf);
	}
	
	protected void deleteClmReserve3610(){
		List<Crsvpf> crsvpf = crsvpfDAO.getCrsvpfRecord(chdrpf.getChdrnum(), "L", "1", "TASO");
		if(!crsvpf.isEmpty()){
			crsvpfDAO.deleteCrsvpfRecord(crsvpf.get(0).getUniqueNumber());
			crsvpf.clear();
			
		}
	 }
	
	protected void releaseSoftlock(){
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}
	
	 //ILJ-383 ends
	protected void whereNext4000()
	{
		nextProgram4010();
	}

	protected void nextProgram4010()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		/*  If first time into this section (stack action blank)*/
		/*  save next eight programs in stack*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		if (isEQ(sv.fupflg, "?")) {
			checkFollowups1850();
		}
		if (isEQ(sv.fupflg, "X")) {
			gensswrec.function.set("A");
			sv.fupflg.set("?");
			callGenssw4300();
			return ;
		}
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		/*  If current stack action is * then re-display screen*/
		/*     (in this case, some other option(s) were requested)*/
		/*  Otherwise continue as normal.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

	protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

	protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

	protected void callGenssw4300()
	{
		/*CALL-SUBROUTINE*/
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/*    load from gensw to wssp*/
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	protected void loadProgram4400()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

	private static final class FormatsInner {
		/* FORMATS */private FixedLengthStringData chdrclmrec = new FixedLengthStringData(10).init("CHDRCLMREC");

	}



}