package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh580screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 17, 22, 5, 18, 23, 24, 15, 16, 1, 2, 3, 21, 20}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {18, 23, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh580ScreenVars sv = (Sh580ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh580screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh580ScreenVars screenVars = (Sh580ScreenVars)pv;
		screenVars.sumins01.setClassString("");
		screenVars.otheradjst.setClassString("");
		screenVars.sumins02.setClassString("");
		screenVars.estimateTotalValue.setClassString("");
		screenVars.instprem01.setClassString("");
		screenVars.clamant.setClassString("");
		screenVars.instprem02.setClassString("");
		screenVars.reqntype.setClassString("");
		screenVars.payee.setClassString(""); //ICIL-254
		screenVars.payeename.setClassString(""); //ICIL-254
		screenVars.bankacckey.setClassString(""); //ICIL-254
        screenVars.bankdesc.setClassString(""); //ICIL-254
        screenVars.bankkey.setClassString(""); //ICIL-254
        screenVars.crdtcrd.setClassString(""); //ICIL-254
        screenVars.effdateDisp.setClassString(""); //ICIL-254
	}

/**
 * Clear all the variables in Sh580screen
 */
	public static void clear(VarModel pv) {
		Sh580ScreenVars screenVars = (Sh580ScreenVars) pv;
		screenVars.sumins01.clear();
		screenVars.otheradjst.clear();
		screenVars.sumins02.clear();
		screenVars.estimateTotalValue.clear();
		screenVars.instprem01.clear();
		screenVars.clamant.clear();
		screenVars.instprem02.clear();
		screenVars.reqntype.clear();
		screenVars.payee.clear(); //ICIL-254
		screenVars.payeename.clear(); //ICIL-254
		screenVars.bankacckey.clear(); //ICIL-254
        screenVars.bankdesc.clear(); //ICIL-254
        screenVars.bankkey.clear(); //ICIL-254
        screenVars.crdtcrd.clear(); //ICIL-254
        screenVars.effdate.clear(); //ICIL-254
        screenVars.effdateDisp.clear(); //ICIL-254
	}
}
