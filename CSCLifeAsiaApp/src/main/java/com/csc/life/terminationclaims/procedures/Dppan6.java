/*
 * File: Dppan6.java
 * Date: 29 August 2009 22:46:52
 * Author: Quipoz Limited
 * 
 * Class transformed from DPPAN6.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Dthcpy;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* OVERVIEW
* ========
*
* This is  a  new  subroutine  which  forms  part  of  the  9405
* Annuities  Development.   It will be called from T6598 as part
* of the death claim processing within the AT request.
*
* The purpose of  this  subroutine  is  to  terminate  all  REGPs
* attached  to  the  component  being  terminated  by  the death
* claim.  REGPs will be written with a valid-flag of '2' and new
* REGPs with the status set to the value on T6693 for the  death
* claim transaction.
*
* A   CLMD   record  will  have  been  written  in  the  on-line
* registration with a value of zero, so there is  no  accounting
* to be done within this routine.
*
* INPUTS AND OUTPUTS
* ==================
*
* Inputs
*
* ITDM
* T6693
* REGPREV
*
* Outputs
*
* REGPREV
* Error     H144
*
*****************************************************************
* </pre>
*/
public class Dppan6 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "DPPAN6";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-STORAGE-AREAS-AND-SUBS */
	private FixedLengthStringData wsaaNewRegpStatus = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaTableStatusFlag = new FixedLengthStringData(1);
	private Validator statusOk = new Validator(wsaaTableStatusFlag, "Y");
	private Validator statusNotOk = new Validator(wsaaTableStatusFlag, "N");
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
		/* ERRORS */
	private static final String h144 = "H144";
		/* TABLES */
	private static final String t6693 = "T6693";
		/* FORMATS */
	private static final String regprec = "REGPREC";
	private static final String regpclmrec = "REGPCLMREC";
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private RegpclmTableDAM regpclmIO = new RegpclmTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T6693rec t6693rec = new T6693rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Dthcpy dthcpy = new Dthcpy();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Dppan6() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dthcpy.deathRec = convertAndSetParam(dthcpy.deathRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*STARTS*/
		initialize1000();
		if (isNE(regpIO.getStatuz(),varcom.endp)) {
			readT66932000();
			while ( !(isEQ(regpIO.getStatuz(),varcom.endp))) {
				processRegp3000();
			}
			
		}
		exitProgram();
	}

protected void initialize1000()
	{
		starts1010();
	}

protected void starts1010()
	{
		statusNotOk.setTrue();
		dthcpy.status.set(varcom.oK);
		syserrrec.subrname.set(wsaaSubr);
		wsaaItemitem.set(SPACES);
		wsaaBatckey.set(dthcpy.batckey);
		/* Read ahead the REGP records. If nothing is found no*/
		/* further processing is required and exit the program.*/
		regpIO.setChdrcoy(dthcpy.chdrChdrcoy);
		regpIO.setChdrnum(dthcpy.chdrChdrnum);
		regpIO.setLife(dthcpy.lifeLife);
		regpIO.setCoverage(dthcpy.covrCoverage);
		regpIO.setRider(dthcpy.covrRider);
		regpIO.setRgpynum(ZERO);
		/* MOVE BEGNH                  TO REGP-FUNCTION.                */
		regpIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		regpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		
		regpIO.setFormat(regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)
		&& isNE(regpIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(regpIO.getChdrcoy(),dthcpy.chdrChdrcoy)
		|| isNE(regpIO.getChdrnum(),dthcpy.chdrChdrnum)
		|| isNE(regpIO.getLife(),dthcpy.lifeLife)
		|| isNE(regpIO.getCoverage(),dthcpy.covrCoverage)
		|| isNE(regpIO.getRider(),dthcpy.covrRider))) {
			/*     MOVE REWRT              TO REGP-FUNCTION                 */
			/*     CALL 'REGPIO'           USING REGP-PARAMS                */
			/*     IF REGP-STATUZ       NOT = O-K                           */
			/*        MOVE REGP-PARAMS     TO SYSR-PARAMS                   */
			/*        PERFORM 600-FATAL-ERROR                               */
			/*     END-IF                                                   */
			regpIO.setStatuz(varcom.endp);
		}
	}

protected void readT66932000()
	{
		starts2010();
	}

protected void starts2010()
	{
		/* The status of REGP is checked if present on T6693.  This is*/
		/* done twice. The first time with RGPYSTAT & CRTABLE as the*/
		/* item key, and the second (if not found) with **** as the*/
		/* item key. If, after the second check, the item has not been*/
		/* found, then set the error code and perform fatal error.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(regpIO.getRgpystat(), SPACES);
		stringVariable1.addExpression(regpIO.getCrtable(), SPACES);
		stringVariable1.setStringInto(wsaaItemitem);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(regpIO.getChdrcoy());
		itdmIO.setItemtabl(t6693);
		itdmIO.setItmfrm(regpIO.getCertdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	



		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(),regpIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t6693)
		|| isNE(itdmIO.getItemitem(),wsaaItemitem)
		|| isEQ(itdmIO.getStatuz(),varcom.endp))) {
			nextStatusCheck2100();
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		t6693Search2200();
	}

protected void nextStatusCheck2100()
	{
		starts2100();
	}

protected void starts2100()
	{
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(regpIO.getRgpystat(), SPACES);
		stringVariable1.addExpression("****", SPACES);
		stringVariable1.setStringInto(wsaaItemitem);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(regpIO.getChdrcoy());
		itdmIO.setItemtabl(t6693);
		itdmIO.setItmfrm(regpIO.getCertdate());
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	



		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if ((isNE(itdmIO.getItemcoy(),regpIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t6693)
		|| isNE(itdmIO.getItemitem(),wsaaItemitem)
		|| isEQ(itdmIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h144);
			fatalError600();
		}
	}

protected void t6693Search2200()
	{
		/*STARTS*/
		for (wsaaIndex.set(1); !(statusOk.isTrue()
		|| isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			if (isEQ(wsaaBatckey.batcBatctrcde,t6693rec.trcode[wsaaIndex.toInt()])) {
				wsaaNewRegpStatus.set(t6693rec.rgpystat[wsaaIndex.toInt()]);
				statusOk.setTrue();
			}
		}
		if (statusNotOk.isTrue()) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h144);
			fatalError600();
		}
		/*EXIT*/
	}

protected void processRegp3000()
	{
		starts3000();
	}

protected void starts3000()
	{
		/* Rewrite the REGP with valid flag 2.*/
		regpIO.setValidflag("2");
		regpIO.setRecvdDate(varcom.vrcmMaxDate);
		regpIO.setIncurdt(varcom.vrcmMaxDate);
		regpIO.setFormat(regprec);
		/* MOVE REWRT                  TO REGP-FUNCTION                 */
		regpIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		/* Write a new REGP with valid flag 1 and the new status.*/
		setupFields3100();
		regpclmIO.setValidflag("1");
		regpclmIO.setFormat(regpclmrec);
		regpclmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, regpclmIO);
		if (isNE(regpclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpclmIO.getParams());
			fatalError600();
		}
		/* Get the next REGP.*/
		regpIO.setFunction(varcom.nextr);
		//performance improvement --  atiwari23 
		regpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		regpIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		regpIO.setFormat(regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)
		&& isNE(regpIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if ((isNE(regpIO.getChdrcoy(),dthcpy.chdrChdrcoy)
		|| isNE(regpIO.getChdrnum(),dthcpy.chdrChdrnum)
		|| isNE(regpIO.getLife(),dthcpy.lifeLife)
		|| isNE(regpIO.getCoverage(),dthcpy.covrCoverage)
		|| isNE(regpIO.getRider(),dthcpy.covrRider))) {
			/*     MOVE REWRT              TO REGP-FUNCTION                 */
			/*     CALL 'REGPIO'           USING REGP-PARAMS                */
			/*     IF REGP-STATUZ       NOT = O-K                           */
			/*        MOVE REGP-PARAMS     TO SYSR-PARAMS                   */
			/*        PERFORM 600-FATAL-ERROR                               */
			/*     END-IF                                                   */
			regpIO.setStatuz(varcom.endp);
		}
	}

protected void setupFields3100()
	{
		para3110();
	}

protected void para3110()
	{
		/* Set up the REGP logical file's fields from the REGP*/
		/* fields ready to be inserted as a new record.*/
		regpclmIO.setChdrcoy(regpIO.getChdrcoy());
		regpclmIO.setChdrnum(regpIO.getChdrnum());
		regpclmIO.setLife(regpIO.getLife());
		regpclmIO.setCoverage(regpIO.getCoverage());
		regpclmIO.setRider(regpIO.getRider());
		regpclmIO.setPlanSuffix(regpIO.getPlanSuffix());
		regpclmIO.setRgpynum(regpIO.getRgpynum());
		regpclmIO.setValidflag(regpIO.getValidflag());
		regpclmIO.setTranno(dthcpy.tranno);
		regpclmIO.setSacscode(regpIO.getSacscode());
		regpclmIO.setSacstype(regpIO.getSacstype());
		regpclmIO.setGlact(regpIO.getGlact());
		regpclmIO.setDebcred(regpIO.getDebcred());
		regpclmIO.setDestkey(regpIO.getDestkey());
		regpclmIO.setPaycoy(regpIO.getPaycoy());
		regpclmIO.setPayclt(regpIO.getPayclt());
		regpclmIO.setRgpymop(regpIO.getRgpymop());
		regpclmIO.setRegpayfreq(regpIO.getRegpayfreq());
		regpclmIO.setCurrcd(regpIO.getCurrcd());
		regpclmIO.setPymt(regpIO.getPymt());
		regpclmIO.setPrcnt(regpIO.getPrcnt());
		regpclmIO.setTotamnt(regpIO.getTotamnt());
		regpclmIO.setRgpystat(wsaaNewRegpStatus);
		regpclmIO.setPayreason(regpIO.getPayreason());
		regpclmIO.setClaimevd(regpIO.getClaimevd());
		regpclmIO.setBankkey(regpIO.getBankkey());
		regpclmIO.setBankacckey(regpIO.getBankacckey());
		regpclmIO.setCrtdate(regpIO.getCrtdate());
		regpclmIO.setAprvdate(regpIO.getAprvdate());
		regpclmIO.setFirstPaydate(regpIO.getFirstPaydate());
		regpclmIO.setNextPaydate(regpIO.getNextPaydate());
		regpclmIO.setRevdte(regpIO.getRevdte());
		regpclmIO.setLastPaydate(regpIO.getLastPaydate());
		regpclmIO.setFinalPaydate(regpIO.getFinalPaydate());
		regpclmIO.setAnvdate(regpIO.getAnvdate());
		regpclmIO.setCancelDate(regpIO.getCancelDate());
		regpclmIO.setRecvdDate(regpIO.getRecvdDate());
		regpclmIO.setIncurdt(regpIO.getIncurdt());
		regpclmIO.setRgpytype(regpIO.getRgpytype());
		regpclmIO.setCrtable(regpIO.getCrtable());
		regpclmIO.setCertdate(regpIO.getCertdate());
		regpclmIO.setTermid(regpIO.getTermid());
		regpIO.setUser(dthcpy.user);
		regpIO.setTransactionDate(dthcpy.date_var);
		regpIO.setTransactionTime(dthcpy.time);
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fatalError601();
				case errorProg610: 
					errorProg610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		dthcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
