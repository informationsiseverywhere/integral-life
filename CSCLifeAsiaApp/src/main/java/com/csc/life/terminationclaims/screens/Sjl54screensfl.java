package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.TableModel.Subfile.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl54screensfl extends Subfile{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28}; 
	public static int maxRecords = 16;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 16, 1, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl54ScreenVars sv = (Sjl54ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sjl54screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sjl54screensfl, 
			sv.Sjl54screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sjl54ScreenVars sv = (Sjl54ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sjl54screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sjl54ScreenVars sv = (Sjl54ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sjl54screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		if (ind3.isOn() && sv.Sjl54screensflWritten.gt(0))
		{
			sv.sjl54screensfl.setCurrentIndex(0);
			sv.Sjl54screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sjl54ScreenVars sv = (Sjl54ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sjl54screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl54ScreenVars screenVars = (Sjl54ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqenum.setFieldName("seqenum");
				screenVars.clttwo.setFieldName("clttwo");
				screenVars.clntID.setFieldName("clntID");
				screenVars.babrdc.setFieldName("babrdc");
				screenVars.accdesc.setFieldName("accdesc");
				screenVars.bankacckey.setFieldName("bankacckey");
				screenVars.bankaccdsc.setFieldName("bankaccdsc");
				screenVars.prcent.setFieldName("prcent");
				screenVars.pymt.setFieldName("pymt");
				screenVars.select.setFieldName("select");
				screenVars.select.setFieldName("cheqpaynum");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.seqenum.set(dm.getField("seqenum"));
			screenVars.clttwo.set(dm.getField("clttwo"));
			screenVars.clntID.set(dm.getField("clntID"));
			screenVars.babrdc.set(dm.getField("babrdc"));
			screenVars.accdesc.set(dm.getField("accdesc"));
			screenVars.bankacckey.set(dm.getField("bankacckey"));
			screenVars.bankaccdsc.set(dm.getField("bankaccdsc"));
			screenVars.prcent.set(dm.getField("prcent"));
			screenVars.pymt.set(dm.getField("pymt"));
			screenVars.select.set(dm.getField("select"));
			screenVars.select.set(dm.getField("cheqpaynum"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl54ScreenVars screenVars = (Sjl54ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqenum.setFieldName("seqenum");
				screenVars.clttwo.setFieldName("clttwo");
				screenVars.clntID.setFieldName("clntID");
				screenVars.babrdc.setFieldName("babrdc");
				screenVars.accdesc.setFieldName("accdesc");
				screenVars.bankacckey.setFieldName("bankacckey");
				screenVars.bankaccdsc.setFieldName("bankaccdsc");
				screenVars.prcent.setFieldName("prcent");
				screenVars.pymt.setFieldName("pymt");
				screenVars.select.setFieldName("select");
				screenVars.select.setFieldName("cheqpaynum");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("seqenum").set(screenVars.seqenum);
			dm.getField("clttwo").set(screenVars.clttwo);
			dm.getField("clntID").set(screenVars.clntID);
			dm.getField("babrdc").set(screenVars.babrdc);
			dm.getField("accdesc").set(screenVars.accdesc);
			dm.getField("bankacckey").set(screenVars.bankacckey);
			dm.getField("bankaccdsc").set(screenVars.bankaccdsc);
			dm.getField("prcent").set(screenVars.prcent);
			dm.getField("pymt").set(screenVars.pymt);
			dm.getField("select").set(screenVars.select);
			dm.getField("cheqpaynum").set(screenVars.cheqpaynum);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sjl54screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sjl54ScreenVars screenVars = (Sjl54ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.seqenum.clearFormatting();
		screenVars.clttwo.clearFormatting();
		screenVars.clntID.clearFormatting();
		screenVars.babrdc.clearFormatting();
		screenVars.accdesc.clearFormatting();
		screenVars.bankacckey.clearFormatting();
		screenVars.bankaccdsc.clearFormatting();
		screenVars.prcent.clearFormatting();
		screenVars.pymt.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.cheqpaynum.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sjl54ScreenVars screenVars = (Sjl54ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.seqenum.setClassString("");
		screenVars.clttwo.setClassString("");
		screenVars.clntID.setClassString("");
		screenVars.babrdc.setClassString("");
		screenVars.accdesc.setClassString("");
		screenVars.bankacckey.setClassString("");
		screenVars.bankaccdsc.setClassString("");
		screenVars.prcent.setClassString("");
		screenVars.pymt.setClassString("");
		screenVars.select.setClassString("");
		screenVars.cheqpaynum.setClassString("");
	}

/**
 * Clear all the variables in Sjl54screensfl
 */
	public static void clear(VarModel pv) {
		Sjl54ScreenVars screenVars = (Sjl54ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.seqenum.clear();
		screenVars.clttwo.clear();
		screenVars.clntID.clear();
		screenVars.babrdc.clear();
		screenVars.accdesc.clear();
		screenVars.bankacckey.clear();
		screenVars.bankaccdsc.clear();
		screenVars.prcent.clear();
		screenVars.pymt.clear();
		screenVars.select.clear();
		screenVars.cheqpaynum.clear();
	}
}
