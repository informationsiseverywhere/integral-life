package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:21
 * Description:
 * Copybook name: CHDRPAYKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrpaykey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrpayFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrpayKey = new FixedLengthStringData(64).isAPartOf(chdrpayFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrpayChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrpayKey, 0);
  	public FixedLengthStringData chdrpayChdrnum = new FixedLengthStringData(8).isAPartOf(chdrpayKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrpayKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrpayFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrpayFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}