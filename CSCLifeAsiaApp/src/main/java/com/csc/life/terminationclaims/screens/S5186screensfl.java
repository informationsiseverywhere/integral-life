package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:41
 * @author Quipoz
 */
public class S5186screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 9, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 13, 1, 2, 11, 3, 12, 21, 20, 10}; 
	public static int maxRecords = 14;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {8, 4, 5, 6, 7, 2, 3, 10, 9, 11}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5186ScreenVars sv = (S5186ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5186screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5186screensfl, 
			sv.S5186screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5186ScreenVars sv = (S5186ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5186screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5186ScreenVars sv = (S5186ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5186screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5186screensflWritten.gt(0))
		{
			sv.s5186screensfl.setCurrentIndex(0);
			sv.S5186screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5186ScreenVars sv = (S5186ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5186screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5186ScreenVars screenVars = (S5186ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hrgpynum.setFieldName("hrgpynum");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.select.setFieldName("select");
				screenVars.asterisk.setFieldName("asterisk");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.plansuff.setFieldName("plansuff");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.elemdesc.setFieldName("elemdesc");
				//ILIFE-1138 STARTS
				screenVars.covrriskstat.setFieldName("covrriskstat");
				screenVars.covrpremstat.setFieldName("covrpremstat");
				//ILIFE-1138 ENDS
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hrgpynum.set(dm.getField("hrgpynum"));
			screenVars.hcrtable.set(dm.getField("hcrtable"));
			screenVars.select.set(dm.getField("select"));
			screenVars.asterisk.set(dm.getField("asterisk"));
			screenVars.life.set(dm.getField("life"));
			screenVars.jlife.set(dm.getField("jlife"));
			screenVars.coverage.set(dm.getField("coverage"));
			screenVars.rider.set(dm.getField("rider"));
			screenVars.plansuff.set(dm.getField("plansuff"));
			screenVars.shortdesc.set(dm.getField("shortdesc"));
			screenVars.elemdesc.set(dm.getField("elemdesc"));
			//ILIFE-1138 STARTS
			screenVars.covrriskstat.set(dm.getField("covrriskstat"));
			screenVars.covrpremstat.set(dm.getField("covrpremstat"));
			//ILIFE-1138 ENDS
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5186ScreenVars screenVars = (S5186ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hrgpynum.setFieldName("hrgpynum");
				screenVars.hcrtable.setFieldName("hcrtable");
				screenVars.select.setFieldName("select");
				screenVars.asterisk.setFieldName("asterisk");
				screenVars.life.setFieldName("life");
				screenVars.jlife.setFieldName("jlife");
				screenVars.coverage.setFieldName("coverage");
				screenVars.rider.setFieldName("rider");
				screenVars.plansuff.setFieldName("plansuff");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.elemdesc.setFieldName("elemdesc");
				//ILIFE-1138 STARTS
				screenVars.covrriskstat.setFieldName("covrriskstat");
				screenVars.covrpremstat.setFieldName("covrpremstat");
				//ILIFE-1138 ENDS
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hrgpynum").set(screenVars.hrgpynum);
			dm.getField("hcrtable").set(screenVars.hcrtable);
			dm.getField("select").set(screenVars.select);
			dm.getField("asterisk").set(screenVars.asterisk);
			dm.getField("life").set(screenVars.life);
			dm.getField("jlife").set(screenVars.jlife);
			dm.getField("coverage").set(screenVars.coverage);
			dm.getField("rider").set(screenVars.rider);
			dm.getField("plansuff").set(screenVars.plansuff);
			dm.getField("shortdesc").set(screenVars.shortdesc);
			dm.getField("elemdesc").set(screenVars.elemdesc);
			//ILIFE-1138 STARTS
			dm.getField("covrriskstat").set(screenVars.covrriskstat);
			dm.getField("covrpremstat").set(screenVars.covrpremstat);
			//ILIFE-1138 ENDS
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5186screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5186ScreenVars screenVars = (S5186ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hrgpynum.clearFormatting();
		screenVars.hcrtable.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.asterisk.clearFormatting();
		screenVars.life.clearFormatting();
		screenVars.jlife.clearFormatting();
		screenVars.coverage.clearFormatting();
		screenVars.rider.clearFormatting();
		screenVars.plansuff.clearFormatting();
		screenVars.shortdesc.clearFormatting();
		screenVars.elemdesc.clearFormatting();
		//ILIFE-1138 STARTS
		screenVars.covrriskstat.clearFormatting();
		screenVars.covrpremstat.clearFormatting();
		//ILIFE-1138 ENDS
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5186ScreenVars screenVars = (S5186ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hrgpynum.setClassString("");
		screenVars.hcrtable.setClassString("");
		screenVars.select.setClassString("");
		screenVars.asterisk.setClassString("");
		screenVars.life.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.coverage.setClassString("");
		screenVars.rider.setClassString("");
		screenVars.plansuff.setClassString("");
		screenVars.shortdesc.setClassString("");
		screenVars.elemdesc.setClassString("");
		//ILIFE-1138 STARTS
		screenVars.covrriskstat.setClassString("");
		screenVars.covrpremstat.setClassString("");
		//ILIFE-1138 ENDS
	}

/**
 * Clear all the variables in S5186screensfl
 */
	public static void clear(VarModel pv) {
		S5186ScreenVars screenVars = (S5186ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hrgpynum.clear();
		screenVars.hcrtable.clear();
		screenVars.select.clear();
		screenVars.asterisk.clear();
		screenVars.life.clear();
		screenVars.jlife.clear();
		screenVars.coverage.clear();
		screenVars.rider.clear();
		screenVars.plansuff.clear();
		screenVars.shortdesc.clear();
		screenVars.elemdesc.clear();
		//ILIFE-1138 STARTS
		screenVars.covrriskstat.clear();
		screenVars.covrpremstat.clear();
		//ILIFE-1138 ENDS
	}
}
