package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:14
 * Description:
 * Copybook name: T6693REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6693rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6693Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData rgpystats = new FixedLengthStringData(24).isAPartOf(t6693Rec, 0);
  	public FixedLengthStringData[] rgpystat = FLSArrayPartOfStructure(12, 2, rgpystats, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(rgpystats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rgpystat01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData rgpystat02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData rgpystat03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData rgpystat04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData rgpystat05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData rgpystat06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData rgpystat07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData rgpystat08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData rgpystat09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData rgpystat10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData rgpystat11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData rgpystat12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData trcodes = new FixedLengthStringData(48).isAPartOf(t6693Rec, 24);
  	public FixedLengthStringData[] trcode = FLSArrayPartOfStructure(12, 4, trcodes, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(48).isAPartOf(trcodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData trcode01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData trcode02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData trcode03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData trcode04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData trcode05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData trcode06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData trcode07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData trcode08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData trcode09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData trcode10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData trcode11 = new FixedLengthStringData(4).isAPartOf(filler1, 40);
  	public FixedLengthStringData trcode12 = new FixedLengthStringData(4).isAPartOf(filler1, 44);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(428).isAPartOf(t6693Rec, 72, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6693Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6693Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}