package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.SurdpfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Surdpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class SurdpfDAOImpl extends BaseDAOImpl<Surdpf> implements SurdpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(SurdpfDAOImpl.class);
	@Override
	public Map<String, List<Surdpf>> getSurdpfMap(String coy, List<String> chdrnumList) {
        StringBuilder sb = new StringBuilder("");
        sb.append("SELECT CHDRNUM,CHDRCOY,PLNSFX,TRANNO,TERMID,TRDT,TRTM,USER_T,EFFDATE,CURRCD,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,SHORTDS,LIENCD,TYPE_T,ACTVALUE,EMV,VRTFUND,COMMCLAW,UNIQUE_NUMBER ");
        sb.append("FROM VM1DTA.SURDPF WHERE CHDRCOY=? AND ");
        sb.append(getSqlInStr("CHDRNUM", chdrnumList));
        sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, TRANNO ASC, PLNSFX ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, CRTABLE ASC, UNIQUE_NUMBER DESC ");        
        LOGGER.info(sb.toString());	
        PreparedStatement ps = getPrepareStatement(sb.toString());
        ResultSet rs = null;
        Map<String, List<Surdpf>> surdpfMap = new HashMap<String, List<Surdpf>>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);
            while (rs.next()) {
            	Surdpf pf = new Surdpf();
            	pf.setChdrnum(rs.getString("CHDRNUM"));
            	pf.setChdrcoy(rs.getString("CHDRCOY"));
            	pf.setPlnsfx(rs.getInt("PLNSFX"));
            	pf.setTranno(rs.getInt("TRANNO"));
            	pf.setTermid(rs.getString("TERMID"));
            	pf.setTrdt(rs.getInt("TRDT"));
            	pf.setTrtm(rs.getInt("TRTM"));
            	pf.setUserT(rs.getInt("USER_T"));
            	pf.setEffdate(rs.getInt("EFFDATE"));
            	pf.setCurrcd(rs.getString("CURRCD"));
              	pf.setLife(rs.getString("LIFE"));
            	pf.setJlife(rs.getString("JLIFE"));
            	pf.setCoverage(rs.getString("COVERAGE"));
            	pf.setRider(rs.getString("RIDER"));
            	pf.setCrtable(rs.getString("CRTABLE"));
            	pf.setShortds(rs.getString("SHORTDS"));
            	pf.setLiencd(rs.getString("LIENCD"));
            	pf.setTypeT(rs.getString("TYPE_T"));
            	pf.setActvalue(rs.getBigDecimal("ACTVALUE"));
            	pf.setEmv(rs.getBigDecimal("EMV"));
            	pf.setVrtfund(rs.getString("VRTFUND"));
            	pf.setCommclaw(rs.getBigDecimal("COMMCLAW"));
            	pf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
                if (surdpfMap.containsKey(pf.getChdrnum())) {
                	surdpfMap.get(pf.getChdrnum()).add(pf);
                } else {
                    List<Surdpf> pfList = new ArrayList<Surdpf>();
                    pfList.add(pf);
                    surdpfMap.put(pf.getChdrnum(), pfList);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("getSurdpfMap()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return surdpfMap;

	}
	@Override
	public void insertSurdpfList(List<Surdpf> pfList) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO SURDPF(CHDRNUM,CHDRCOY,PLNSFX,TRANNO,TERMID,TRDT,TRTM,USER_T,EFFDATE,CURRCD,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,SHORTDS,LIENCD,TYPE_T,ACTVALUE,EMV,VRTFUND,USRPRF,JOBNM,DATIME,OTHERADJST,COMMCLAW) ");
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for (Surdpf pf : pfList) {
				int i = 1;
				ps.setString(i++, pf.getChdrnum());
				ps.setString(i++, pf.getChdrcoy());
				ps.setInt(i++, pf.getPlnsfx());
				ps.setInt(i++, pf.getTranno());
				ps.setString(i++, pf.getTermid());
				ps.setInt(i++, pf.getTrdt());
				ps.setInt(i++, pf.getTrtm());
				ps.setInt(i++, pf.getUserT());
				ps.setInt(i++, pf.getEffdate());
				ps.setString(i++, pf.getCurrcd());
				ps.setString(i++, pf.getLife());
				ps.setString(i++, pf.getJlife());
				ps.setString(i++, pf.getCoverage());
				ps.setString(i++, pf.getRider());
				ps.setString(i++, pf.getCrtable());
				ps.setString(i++, pf.getShortds());
				ps.setString(i++, pf.getLiencd());
				ps.setString(i++, pf.getTypeT());
				ps.setBigDecimal(i++, pf.getActvalue());
				ps.setBigDecimal(i++, pf.getEmv());
				ps.setString(i++, pf.getVrtfund());				
				ps.setString(i++, this.getUsrprf());
				ps.setString(i++, this.getJobnm());
				ps.setTimestamp(i++, this.getDatime());
				ps.setBigDecimal(i++, pf.getOtheradjst());
				ps.setBigDecimal(i++, pf.getCommclaw());
				
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertSurdpfList()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	//ILIFE-4406
	public List<Surdpf> searchSurdclmRecord(String chdrcoy, String chdrnum,String life,String rider, String coverage,int planSuffix, int tranno) {
		List<Surdpf> surdclmResult = new ArrayList<Surdpf>();
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT CHDRCOY,CHDRNUM,CURRCD,ACTVALUE,LIFE,COVERAGE,RIDER,PLNSFX,CRTABLE,JLIFE,TRANNO ");
		sb.append("FROM VM1DTA.SURDCLM WHERE CHDRCOY=? AND CHDRNUM=? AND LIFE=? AND COVERAGE=? AND RIDER=? AND PLNSFX=? AND TRANNO=?  ");
		sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			ps.setString(4, coverage);
			ps.setString(5, rider);
			ps.setInt(6, planSuffix);
			ps.setInt(7, tranno);
			rs = executeQuery(ps);
			Surdpf surdclm = null;
			while (rs.next()) {
				surdclm = new Surdpf();
				surdclm.setChdrcoy(rs.getString(1));
				surdclm.setChdrnum(rs.getString(2));
				surdclm.setCurrcd(rs.getString(3));
				surdclm.setActvalue(rs.getBigDecimal(4));
				surdclm.setLife(rs.getString(5));
				surdclm.setCoverage(rs.getString(6));
				surdclm.setRider(rs.getString(7));
				surdclm.setPlnsfx(rs.getInt(8));
				surdclm.setCrtable(rs.getString(9));
				surdclm.setJlife(rs.getString(10));
				surdclm.setTranno(rs.getInt(10));
				surdclmResult.add(surdclm);
			}

		} catch (SQLException e) {
			LOGGER.error("SurdpfDAOImpl()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return surdclmResult;
	}
	public void insertSurdclmpfDataBulk(List<Surdpf> surdpfList){
		if (surdpfList == null || surdpfList.size() == 0) {
			return;
		}

		String stmt = "INSERT INTO VM1DTA.HSUDPF(CHDRCOY,CHDRNUM,PLNSFX,TRANNO,TERMID,TRDT,TRTM,USER_T,EFFDATE,CURRCD,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,SHORTDS,LIENCD,TYPE_T,ACTVALUE,EMV,VRTFUND USRPRF, JOBNM, DATIME)"
				+ " value(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pssurdnsert = getPrepareStatement(stmt);
		try {
			for (Surdpf s : surdpfList) {
				pssurdnsert.setString(1, s.getChdrcoy());
				pssurdnsert.setString(2, s.getChdrnum());
				pssurdnsert.setInt(3, s.getPlnsfx());
				pssurdnsert.setInt(4, s.getTranno());
				pssurdnsert.setString(5, s.getTermid());
				pssurdnsert.setInt(6, s.getTrdt());
				pssurdnsert.setInt(7,s.getTrtm());
				pssurdnsert.setInt(8,s.getUserT());
				pssurdnsert.setInt(9, s.getEffdate());
				pssurdnsert.setString(10, s.getCurrcd());
				pssurdnsert.setString(11, s.getLife());
				pssurdnsert.setString(12, s.getJlife());
				pssurdnsert.setString(13, s.getCoverage());
				pssurdnsert.setString(14, s.getRider());
				pssurdnsert.setString(15, s.getCrtable());
				pssurdnsert.setString(16, s.getShortds());
				pssurdnsert.setString(17, s.getLiencd());
				pssurdnsert.setString(18, s.getTypeT());
				pssurdnsert.setBigDecimal(19, s.getActvalue());
				pssurdnsert.setBigDecimal(20, s.getEmv());
				pssurdnsert.setString(21, s.getVrtfund());
				pssurdnsert.setString(22, getJobnm());
				pssurdnsert.setString(23, getUsrprf());
				pssurdnsert.setTimestamp(24,new Timestamp(System.currentTimeMillis()));
				pssurdnsert.addBatch();
			}
			pssurdnsert.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertSurdclmpfDataBulk()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(pssurdnsert, null);
		}
	
	}
	public void insertSurdpfRecord(Surdpf s){
		
		String stmt = "INSERT INTO VM1DTA.SURDPF(CHDRCOY,CHDRNUM,PLNSFX,TRANNO,TERMID,TRDT,TRTM,USER_T,EFFDATE,CURRCD,LIFE,JLIFE,COVERAGE,RIDER,CRTABLE,SHORTDS,LIENCD,TYPE_T,ACTVALUE,EMV,VRTFUND, USRPRF, JOBNM, DATIME)"
				+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pssurdnsert = getPrepareStatement(stmt);
		try {
			
				pssurdnsert.setString(1, s.getChdrcoy());
				pssurdnsert.setString(2, s.getChdrnum());
				pssurdnsert.setInt(3, s.getPlnsfx());
				pssurdnsert.setInt(4, s.getTranno());
				pssurdnsert.setString(5, s.getTermid());
				pssurdnsert.setInt(6, s.getTrdt());
				pssurdnsert.setInt(7,s.getTrtm());
				pssurdnsert.setInt(8,s.getUserT());
				pssurdnsert.setInt(9, s.getEffdate());
				pssurdnsert.setString(10, s.getCurrcd());
				pssurdnsert.setString(11, s.getLife());
				pssurdnsert.setString(12, s.getJlife());
				pssurdnsert.setString(13, s.getCoverage());
				pssurdnsert.setString(14, s.getRider());
				pssurdnsert.setString(15, s.getCrtable());
				pssurdnsert.setString(16, s.getShortds());
				pssurdnsert.setString(17, s.getLiencd());
				pssurdnsert.setString(18, s.getTypeT());
				pssurdnsert.setBigDecimal(19, s.getActvalue());
				pssurdnsert.setBigDecimal(20, s.getEmv());
				pssurdnsert.setString(21, s.getVrtfund());
				pssurdnsert.setString(22, getJobnm());
				pssurdnsert.setString(23, getUsrprf());
				pssurdnsert.setTimestamp(24,new Timestamp(System.currentTimeMillis()));
				pssurdnsert.addBatch();
				int[] rowCount=pssurdnsert.executeBatch();	
			
			
		} catch (SQLException e) {
			LOGGER.error("insertSurdpfrecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(pssurdnsert, null);
		}
	
	}
	@Override
	public List<Surdpf> searchSurdclmRecordList(String chdrcoy, String chdrnum, int tranno) {
		List<Surdpf> surdclmResult = new ArrayList<Surdpf>();
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT CHDRCOY,CHDRNUM,CURRCD,ACTVALUE,LIFE,COVERAGE,RIDER,PLNSFX,CRTABLE,JLIFE,TRANNO,EFFDATE,OTHERADJST,TYPE_T ");
		sb.append("FROM VM1DTA.SURDPF WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=?  ");
		sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);			
			ps.setInt(3, tranno);
			rs = executeQuery(ps);
			Surdpf surdclm = null;
			while (rs.next()) {
				surdclm = new Surdpf();
				surdclm.setChdrcoy(rs.getString(1));
				surdclm.setChdrnum(rs.getString(2));
				surdclm.setCurrcd(rs.getString(3));
				surdclm.setActvalue(rs.getBigDecimal(4));
				surdclm.setLife(rs.getString(5));
				surdclm.setCoverage(rs.getString(6));
				surdclm.setRider(rs.getString(7));
				surdclm.setPlnsfx(rs.getInt(8));
				surdclm.setCrtable(rs.getString(9));
				surdclm.setJlife(rs.getString(10));
				surdclm.setTranno(rs.getInt(11));
				surdclm.setEffdate(rs.getInt(12));
				surdclm.setOtheradjst(rs.getBigDecimal(13));
				surdclm.setTypeT(rs.getString(14));
				surdclmResult.add(surdclm);
			}

		} catch (SQLException e) {
			LOGGER.error("SurdpfDAOImpl()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return surdclmResult;
	}

}
