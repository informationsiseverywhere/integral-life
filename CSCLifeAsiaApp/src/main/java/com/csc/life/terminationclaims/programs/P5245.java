/*
 * File: P5245.java
 * Date: 30 August 2009 0:22:12
 * Author: Quipoz Limited
 * 
 * Class transformed from P5245.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Datcon3rec; //ICIL-297
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.RplnpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rplnpf;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec; //ICIL-297
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;//ICIL-297
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;//ICIL-297
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO; //ICIL-297
import com.csc.life.productdefinition.dataaccess.model.Lifepf;  //ICIL-297
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.Td5gfrec;   //ICIL-297
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.screens.S5245ScreenVars;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator; //ICIL-297
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData; //ICIL-297
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;


/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*     Full Surrender Sub Menu.
*
* Validation
* ----------
*
*  Key 1 - Contract number (CHDRSUR)
*
*       Y = mandatory, must exist on file.
*            - CHDRSUR  -  Life  new  business  contract  header
*                 logical view.
*            - must be correct status for transaction (T5679).
*            - MUST BE CORRECT BRANCH.
*
*       N = IRREVELANT.
*
*       Blank = irrelevant.
*
*
* Updating
* --------
*
*  Set up WSSP-FLAG:
*       - always "M".                                                (002
*
*  For maintenance transactions  (WSSP-FLAG  set to  'M' above),
*  soft lock the contract header (call SFTLOCK). If the contract
*  is already locked, display an error message.
*
*****************************************************************
* </pre>
*/
public class P5245 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5245");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private String wsaaSurrOk = "";
	private String wsaaBsurrOk = "";
	private FixedLengthStringData wsaaFreelookBasisChn = new FixedLengthStringData(1); //ICIL-297
	private Validator issueDateChnBasis = new Validator(wsaaFreelookBasisChn, "1"); //ICIL-297
	private Validator ackBasis = new Validator(wsaaFreelookBasisChn, "2"); //ICIL-297
	private Validator deemedBasis = new Validator(wsaaFreelookBasisChn, "3"); //ICIL-297
	private Validator minAckDeemedBasis = new Validator(wsaaFreelookBasisChn, "4"); //ICIL-297
	private ZonedDecimalData wsaaFreelookDate = new ZonedDecimalData(8, 0).setUnsigned(); //ICIL-297
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).setUnsigned(); //ICIL-297
	private ZonedDecimalData wsaaTdayno = new ZonedDecimalData(3, 0).setUnsigned(); //ICIL-297
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t6640 = "T6640";
	private static final String td5gf = "TD5GF"; //ICIL-297
		/* ERRORS */
	private static final String rlcb = "RLCB";  //ICIL-297
	private static final String rlaj = "RLAJ";  //ICIL-297
	private static final String rrgi = "RRGI";  //ICIL-297
		/* FORMATS */
	private static final String chdrsurrec = "CHDRSURREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String covrsurrec = "COVRSURREC";
	private static final String loanrec = "LOANREC";
		/* Dummy user wssp. Replace with required version.
		    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	protected ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LoanTableDAM loanIO = new LoanTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6640rec t6640rec = new T6640rec();
	private Td5gfrec td5gfrec = new Td5gfrec();      // ICIL-297
	private Agecalcrec agecalcrec = new Agecalcrec(); //ICIL-297
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec(); //ICIL-297
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private S5245ScreenVars sv = getLScreenVars();
	private ErrorsInner errorsInner = new ErrorsInner();
	protected Chdrpf chdrpf = new Chdrpf();
	protected ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); //ICIL-297
	private Hpadpf hpadpf; //ICIL-297
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class); //ICIL-297
	private Itempf itempf = null; //ICIL-297
	protected LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class); //ICIL-297
	protected Lifepf lifepf = new Lifepf(); //ICIL-297
	boolean cscom004Permission = false; // ICIL-297
	//ICIL-14 Start
	boolean isLoanConfig = false;
	private RplnpfDAO rplnpfDAO = getApplicationContext().getBean("rplnpfDAO", RplnpfDAO.class);
	private Rplnpf rplnpf = new Rplnpf();
	//ICIL-14 End
	boolean csulk005Permission = false;    //ILIFE-7809
	private Th506rec th506rec = new Th506rec(); // ILIFE-7809
	private static final String th506 = "TH506"; //  ILIFE-7809
	private FixedLengthStringData wsaaSurrenderMethod = new FixedLengthStringData(4).init("SC14");
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		validateKey22220, 
		exit2290, 
		search2510, 
		exit12090, 
		loopCovrsur2975, 
		exit2979, 
		loopCovrsur2985, 
		exit2989, 
		keeps3070, 
		batching3080, 
		exit3090
	}
	protected S5245ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5245ScreenVars.class);
	}

	public P5245() {
		super();
		screenVars = sv;
		new ScreenModel("S5245", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
	    cscom004Permission  = FeaConfg.isFeatureExist("2", "CSCOM004", appVars, "IT");     // ICIL-297
	    csulk005Permission = FeaConfg.isFeatureExist("2", "CSULK005", appVars, "IT");        //ILIFE-7809
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		sv.effdate.set(varcom.vrcmMaxDate);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		
		//ICIL-14 
		isLoanConfig = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "CSLND002", appVars, "IT");
			
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'S5245IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5245-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
				if (isNE(sv.chdrselErr, SPACES)) {
					return ;
				}
				/*           PERFORM 2600-CHECK-UTRNS                              */
				/*     Condition added to prevent contract validation for enquiry  */
				/*        PERFORM 2600-CHK-FOR-LOANS                       <012>*/
				/*        PERFORM 2700-VALIDATE-EFFDATE                    <008>*/
				/*        IF  S5245-CHDRSEL-ERR = SPACES                   <009>*/
				/*        AND S5245-EFFDATE-ERR = SPACES                   <015>*/
				/*        PERFORM 2960-CHK-VALID-ACTION                    <009>*/
				/*        END-IF                                           <009>*/
				/*           MOVE 'Y'            TO WSAA-SUM-FLAG             <020>*/
				/*           PERFORM 5100-CHECK-SUM-CONTRACT                  <020>*/
				/*           IF     WSAA-SUM-FLAG NOT = SPACE                 <020>*/
				/*              AND S5245-EFFDATE     = VRCM-MAX-DATE         <020>*/
				/*                  PERFORM 5200-CHECK-SUM-EFFDATE            <020>*/
				/*           END-IF                                           <020>*/
				if (isNE(scrnparams.action, "I")) {
					chkForLoans2600();
					validateEffdate2700();
					if (isEQ(sv.chdrselErr, SPACES)
					&& isEQ(sv.effdateErr, SPACES)) {
						chkValidAction2960();
					}
				}
			}
			else {
				verifyBatchControl2900();
			}
		}
		//ICIL-14 Start
				if(isEQ(isLoanConfig,true)){
					rplnpf = rplnpfDAO.getRploanRecord(sv.chdrsel.toString().trim(),"P");
					if(rplnpf!=null) {
						if(isEQ(sv.action, "A")){					
							sv.actionErr.set(errorsInner.rrfh);	
						}			
					}
				}
		//ICIL-14 End
				
	}

	/**
	* <pre>
	**Check if surrender details exist only if there is a policy <005>
	**and the option chosen is option B.                         <005>
	********IF CHDRSUR-STATUZ     NOT = MRNF AND                 <005>
	***********S5245-ACTION       = 'B'                          <005>
	************PERFORM 2300-CHECK-SURRENDER.                    <005>
	* </pre>
	*/
protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateKey12210();
				case validateKey22220: 
					validateKey22220();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1, SPACES)) {
			goTo(GotoLabel.validateKey22220);
		}
		chdrsurIO.setChdrcoy(wsspcomn.company);
		chdrsurIO.setChdrnum(sv.chdrsel);
		chdrsurIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrsurIO);
		}
		else {
			chdrsurIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)
		&& isNE(chdrsurIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrsurIO.getStatuz(), varcom.mrnf)
		&& isEQ(subprogrec.key1, "Y")) {
			sv.chdrselErr.set(errorsInner.e544);
		}
		if (isEQ(chdrsurIO.getStatuz(), varcom.oK)
		&& isEQ(subprogrec.key1, "N")) {
			sv.chdrselErr.set(errorsInner.f918);
		}
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				goTo(GotoLabel.exit2290);
			}
		}
		/*    For transactions on policies,*/
		/*       check the contract selected is of the correct status*/
		/*       and from correct branch.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)) {
			checkStatus2400();
		}
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)
		&& isNE(wsspcomn.branch, chdrsurIO.getCntbranch())) {
			sv.chdrselErr.set(errorsInner.e455);
		}
	}

protected void validateKey22220()
	{
		if (isEQ(subprogrec.key2, SPACES)
		|| isEQ(subprogrec.key2, "N")) {
			return ;
		}
		chdrsurIO.setChdrcoy(wsspcomn.company);
		chdrsurIO.setChdrnum(sv.chdrsel);
		chdrsurIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrsurIO);
		}
		else {
			chdrsurIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)
		&& isNE(chdrsurIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrsurIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.e544);
		}
	}

	/**
	* <pre>
	*2300-CHECK-SURRENDER SECTION.                               <005>
	******************************                               <005>
	*2300-PARA.                                                  <005>
	*****MOVE SPACES                 TO SURHCLM-DATA-AREA.       <005>
	*****MOVE CHDRSUR-CHDRCOY        TO SURHCLM-CHDRCOY.         <005>
	*****MOVE CHDRSUR-CHDRNUM        TO SURHCLM-CHDRNUM.         <005>
	*****MOVE CHDRSUR-TRANNO         TO SURHCLM-TRANNO.          <005>
	*****MOVE ZERO                   TO SURHCLM-PLAN-SUFFIX.     <005>
	*****MOVE 'BEGN'                 TO SURHCLM-FUNCTION.        <005>
	*****PERFORM 2950-PROCESS-SURHCLM.                           <005>
	*2300-EXIT.                                                  <005>
	*****EXIT.                                                   <005>
	* </pre>
	*/
protected void checkStatus2400()
	{
		readStatusTable2410();
	}

protected void readStatusTable2410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaSub.set(ZERO);
		lookForStat2500();
	}

protected void lookForStat2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case search2510: 
					search2510();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void search2510()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			sv.chdrselErr.set(errorsInner.e767);
			wsspcomn.edterror.set("Y");
		}
		else {
			if (isNE(chdrsurIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.search2510);
			}
		}
		/*EXIT*/
	}

protected void chkForLoans2600()
	{
		start2600();
	}

protected void start2600()
	{
		if (isEQ(scrnparams.action, "C")
		|| isEQ(scrnparams.action, "D")) {
			loanIO.setDataArea(SPACES);
			loanIO.setChdrcoy(chdrsurIO.getChdrcoy());
			loanIO.setChdrnum(chdrsurIO.getChdrnum());
			loanIO.setLoanNumber(ZERO);
			loanIO.setFunction(varcom.begn);
			//performance improvement --  Niharika Modi 
			loanIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			loanIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
			loanIO.setFormat(loanrec);
			SmartFileCode.execute(appVars, loanIO);
			if (isNE(loanIO.getStatuz(), varcom.oK)
			&& isNE(loanIO.getStatuz(), varcom.endp)) {
				syserrrec.statuz.set(loanIO.getParams());
				syserrrec.statuz.set(loanIO.getStatuz());
				fatalError600();
			}
			if (isEQ(loanIO.getChdrcoy(), chdrsurIO.getChdrcoy())
			&& isEQ(loanIO.getChdrnum(), chdrsurIO.getChdrnum())
			&& isEQ(loanIO.getStatuz(), varcom.oK)) {
				wsspcomn.edterror.set("Y");
				sv.actionErr.set(errorsInner.t063);
			}
		}
	}

	/**
	* <pre>
	/                                                            <003>
	* </pre>
	*/
protected void validateEffdate2700()
	{
		/*START*/
		if (isNE(sv.effdateErr, SPACES)) {
			return ;
		}
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)) {
			sv.effdate.set(wsaaToday);
		}
		else {
			if (isGT(sv.effdate, wsaaToday)) {
				/*           AND WSAA-SUM-FLAG        = SPACE                 <020>*/
				sv.effdateErr.set(errorsInner.f073);
			}
			else {
				if (isLT(sv.effdate, chdrsurIO.getOccdate())) {
					sv.effdateErr.set(errorsInner.f616);
				}
			}
		}
		wsspcomn.currfrom.set(sv.effdate);
		/*EXIT*/
	}

	/**
	* <pre>
	*2600-EXIT.                                                  <014>
	*    EXIT.                                                   <014>
	* </pre>
	*/
protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			/*      MOVE E073               TO S5245-ACTION-ERR.             */
			sv.actionErr.set(errorsInner.e073);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*2950-PROCESS-SURHCLM SECTION.                               <005>
	******************************                               <005>
	*2950-PARA.                                                  <005>
	*****                                                        <005>
	*****CALL 'SURHCLMIO'              USING SURHCLM-PARAMS.     <005>
	*****                                                        <005>
	*****IF CHDRSUR-CHDRCOY           = SURHCLM-CHDRCOY AND      <005>
	*****   CHDRSUR-CHDRNUM           = SURHCLM-CHDRNUM AND      <005>
	*****   CHDRSUR-TRANNO            = SURHCLM-TRANNO           <005>
	*****   MOVE ENDP                  TO SURHCLM-STATUZ         <005>
	*****   GO TO 2950-EXIT.                                     <005>
	*****                                                        <005>
	*****IF CHDRSUR-CHDRCOY     NOT   = SURHCLM-CHDRCOY OR       <005>
	*****   CHDRSUR-CHDRNUM     NOT   = SURHCLM-CHDRNUM OR       <005>
	*****   CHDRSUR-TRANNO       NOT  = SURHCLM-TRANNO OR        <005>
	*****   SURHCLM-STATUZ            = ENDP                     <005>
	*****   MOVE ENDP                  TO SURHCLM-STATUZ         <005>
	*****   MOVE H111                  TO S5245-CHDRSEL-ERR      <005>
	*****   MOVE 'Y'                   TO WSSP-EDTERROR          <005>
	*****ELSE                                                    <005>
	*****   MOVE NEXTR                 TO SURHCLM-FUNCTION       <005>
	*****   GO TO 2950-PARA.                                     <005>
	*****                                                        <005>
	*2950-EXIT.                                                  <005>
	*****EXIT.                                                   <005>
	* </pre>
	*/
protected void chkValidAction2960()
	{
	    /*INIT*/
		if (isEQ(scrnparams.action, "A")
		|| isEQ(scrnparams.action, "B")
		|| isEQ(scrnparams.action, "E")) {
			wsaaSurrOk = "N";
			chkValidSurr2970();
			if (isEQ(wsaaSurrOk, "N")) {
				sv.chdrselErr.set(errorsInner.e656);
			}
		}
		if (isEQ(scrnparams.action, "C")
		|| isEQ(scrnparams.action, "D")) {
			wsaaBsurrOk = "N";
			chkValidBsurr2980();
			if (isEQ(wsaaBsurrOk, "N")) {
				sv.chdrselErr.set(errorsInner.e657);
			}
		}
	    /*ICIL-297 start*/
		if(cscom004Permission){
			if ((isEQ(sv.chdrselErr, SPACES)) 
			&& (isEQ(scrnparams.action, "A"))) {
				freelookCheckCHN();					
			}	
		}
	    /*ICIL-297 end*/
		/*EXIT*/
		
		if(csulk005Permission)
  		{	
		
		readTh506(); // ILIFE-7809
  		}
	}

// ILIFE-7809 Starts
protected void readTh506() 
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(th506);/* IJTI-1523 */
	itempf.setItemitem(chdrsurIO.getCnttype().toString());
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null == itempf)
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(th506);/* IJTI-1523 */
		itempf.setItemitem("***");
	}
	if (null != itempf) {
		th506rec.th506Rec.set(StringUtil.rawToString(itempf.getGenarea()));
}
	else {
		th506rec.th506Rec.set(SPACES);
	}
	chdrpf = chdrpfDAO.getchdrRecord(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString());
	datcon3rec.intDate2.set(sv.effdate);
	datcon3rec.intDate1.set(chdrpf.getOccdate());
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isLT(datcon3rec.freqFactor,th506rec.yearInforce) && isNE(t5687rec.svMethod,wsaaSurrenderMethod)) {
		sv.effdateErr.set(errorsInner.rrmu);
		wsspcomn.edterror.set("Y");
		return ;
	}
	}
//ILIFE-7809 Ends
//*CIL-297 start*/
protected void freelookCheckCHN() 	
{
	/* Validate the cooling off period for freelook cancellation       */
	/* based on the rule defined in TD5GF.                             */
	getCoolOff();
	wsaaFreelookBasisChn.set(td5gfrec.ratebas);       
	if (!(issueDateChnBasis.isTrue()
	|| ackBasis.isTrue()
	|| deemedBasis.isTrue()		
	|| minAckDeemedBasis.isTrue())) {
		sv.chdrselErr.set(rlcb);
		return ;
	}
	hpadpf=hpadpfDAO.getHpadData(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString());
	/* issue date*/
	if (issueDateChnBasis.isTrue()) {
		wsaaFreelookDate.set(hpadpf.getHissdte());
	}
	
	/* ack */
	if (ackBasis.isTrue()) {
		if (isNE(hpadpf.getPackdate(), varcom.vrcmMaxDate)
				&& isNE(hpadpf.getPackdate(), ZERO)) {
					wsaaFreelookDate.set(hpadpf.getPackdate());
				}
				else {
					sv.chdrselErr.set(rlaj);
					return ;
				}
	}
	
	/* deemed */
	if (deemedBasis.isTrue()) {
		if (isNE(hpadpf.getDeemdate(), varcom.vrcmMaxDate)
				&& isNE(hpadpf.getDeemdate(), ZERO)) {
					wsaaFreelookDate.set(hpadpf.getDeemdate());
				}
				else {
					sv.chdrselErr.set(rlaj);
					return ;
				}
	}
	
	/* min- ack or deemed */
	if (minAckDeemedBasis.isTrue()) {
		if (isGT(hpadpf.getDeemdate(), hpadpf.getPackdate())) {
			if (isNE(hpadpf.getPackdate(), varcom.vrcmMaxDate)
					&& isNE(hpadpf.getPackdate(), ZERO)) {
						wsaaFreelookDate.set(hpadpf.getPackdate());
					}
		}
		else {
			if (isNE(hpadpf.getDeemdate(), varcom.vrcmMaxDate)
					&& isNE(hpadpf.getDeemdate(), ZERO)) {
						wsaaFreelookDate.set(hpadpf.getDeemdate());
					}
					else {
						sv.chdrselErr.set(rlaj);
						return ;
					}
		}

	}

	datcon3rec.datcon3Rec.set(SPACES);
	datcon3rec.intDate1.set(wsaaFreelookDate);
	datcon3rec.intDate2.set(wsaaToday);
	datcon3rec.frequency.set("DY");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}

	if (isLT(datcon3rec.freqFactor, wsaaTdayno) && isNE(t5687rec.svMethod,wsaaSurrenderMethod)) {
		sv.chdrselErr.set(rrgi);
	}
}   

protected void getCoolOff() 
{
	lifepf = lifepfDAO.getLifeRecord(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString(), "01", "00");
	if (lifepf== null ) {
		syserrrec.params.set(lifepf);
		fatalError600();
	}
	
	initialize(agecalcrec.agecalcRec);
	agecalcrec.function.set("CALCP");
	agecalcrec.language.set(wsspcomn.language);
	agecalcrec.intDate1.set(lifepf.getCltdob());
	agecalcrec.intDate2.set(wsaaToday);
	agecalcrec.company.set(wsspcomn.fsuco);
	callProgram(Agecalc.class, agecalcrec.agecalcRec);
	if (isNE(agecalcrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(agecalcrec.statuz);
		fatalError600();
	}
	wsaaAnb.set(agecalcrec.agerating);

	readTd5gf1100();

		if (isEQ(lifepf.getCltsex(),td5gfrec.gender01)  && isGTE(wsaaAnb,td5gfrec.frmage01) && isLTE(wsaaAnb,td5gfrec.toage01)) {
			td5gfrec.cooloff.set(td5gfrec.coolingoff1);
			wsaaTdayno.set(td5gfrec.coolingoff);
			
		}
		else {
			if (isEQ(lifepf.getCltsex(),td5gfrec.gender02) && isGTE(wsaaAnb,td5gfrec.frmage02) && isLTE(wsaaAnb,td5gfrec.toage02)) {
				td5gfrec.cooloff.set(td5gfrec.coolingoff2);
				wsaaTdayno.set(td5gfrec.coolingoff);
			}
			else {
				if (isEQ(lifepf.getCltsex(),td5gfrec.gender03) && isGTE(wsaaAnb,td5gfrec.frmage03) && isLTE(wsaaAnb,td5gfrec.toage03)) {
					td5gfrec.cooloff.set(td5gfrec.coolingoff3);
					wsaaTdayno.set(td5gfrec.coolingoff);
				}
				
				else {
					if (isEQ(lifepf.getCltsex(),td5gfrec.gender04) && isGTE(wsaaAnb,td5gfrec.frmage04) && isLTE(wsaaAnb,td5gfrec.toage04)) {
						td5gfrec.cooloff.set(td5gfrec.coolingoff4);
						wsaaTdayno.set(td5gfrec.coolingoff);
					}
				}
						
			}
		}
}

protected void readTd5gf1100() 
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(td5gf);/* IJTI-1523 */
	itempf.setItemitem(chdrsurIO.getCnttype().toString() + chdrsurIO.getSrcebus().toString());
	itempf = itempfDAO.getItempfRecord(itempf);
	
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(td5gf);/* IJTI-1523 */
		itempf.setItemitem(chdrsurIO.getCnttype().toString() + "**");
		itempf = itempfDAO.getItempfRecord(itempf);	
	}
	
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(td5gf);/* IJTI-1523 */
		itempf.setItemitem("*****");
		itempf = itempfDAO.getItempfRecord(itempf);	
	}
	
	if (itempf != null) {
		td5gfrec.td5gfRec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	else {
		td5gfrec.td5gfRec.set(SPACES);
	}
	
}

/*ICIL-297 end*/

protected void chkValidSurr2970()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readCovrsur2971();
				case loopCovrsur2975: 
					loopCovrsur2975();
					readT56872976();
				case exit2979: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readCovrsur2971()
	{
		covrsurIO.setParams(SPACES);
		covrsurIO.setChdrcoy(chdrsurIO.getChdrcoy());
		covrsurIO.setChdrnum(chdrsurIO.getChdrnum());
		covrsurIO.setPlanSuffix(ZERO);
		covrsurIO.setFormat(covrsurrec);
		covrsurIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	}

protected void loopCovrsur2975()
	{
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)
		&& isNE(covrsurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if (isNE(covrsurIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(covrsurIO.getChdrnum(), chdrsurIO.getChdrnum())
		|| isEQ(covrsurIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit2979);
		}
	}

protected void readT56872976()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrsurIO.getCrtable());
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrsurIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(covrsurIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.f294);
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (isNE(t5687rec.svMethod, SPACES)) {
			wsaaSurrOk = "Y";
			return ;
		}
		covrsurIO.setFunction(varcom.nextr);
		goTo(GotoLabel.loopCovrsur2975);
	}

protected void chkValidBsurr2980()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readCovrsur2981();
				case loopCovrsur2985: 
					loopCovrsur2985();
					readT66402986();
				case exit2989: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readCovrsur2981()
	{
		covrsurIO.setParams(SPACES);
		covrsurIO.setChdrcoy(chdrsurIO.getChdrcoy());
		covrsurIO.setChdrnum(chdrsurIO.getChdrnum());
		covrsurIO.setPlanSuffix(ZERO);
		covrsurIO.setFormat(covrsurrec);
		covrsurIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		covrsurIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrsurIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
	}

protected void loopCovrsur2985()
	{
		SmartFileCode.execute(appVars, covrsurIO);
		if (isNE(covrsurIO.getStatuz(), varcom.oK)
		&& isNE(covrsurIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrsurIO.getParams());
			fatalError600();
		}
		if (isNE(covrsurIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(covrsurIO.getChdrnum(), chdrsurIO.getChdrnum())
		|| isEQ(covrsurIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit2989);
		}
	}

protected void readT66402986()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(covrsurIO.getCrtable());
		itdmIO.setItmfrm(sv.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t6640)
		|| isNE(itdmIO.getItemitem(), covrsurIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			/* No need to give an error if no item found on this table,        */
			/* Traditional Components only exist on this table.                */
			return ;
		}
		t6640rec.t6640Rec.set(itdmIO.getGenarea());
		if (isNE(t6640rec.surrenderBonusMethod, SPACES)) {
			wsaaBsurrOk = "Y";
			return ;
		}
		covrsurIO.setFunction(varcom.nextr);
		goTo(GotoLabel.loopCovrsur2985);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
				case keeps3070: 
					keeps3070();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details*/
		if (isEQ(scrnparams.action, "I")
		|| isEQ(scrnparams.action, "B")
		|| isEQ(scrnparams.action, "D")) {
			wsspcomn.flag.set("I");
			chdrenqIO.setChdrcoy(wsspcomn.company);
			chdrenqIO.setChdrnum(sv.chdrsel);
			chdrenqIO.setFunction(varcom.reads);
			chdrenqIO.setFormat(chdrenqrec);
			chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
			chdrpfDAO.setCacheObject(chdrpf);
			SmartFileCode.execute(appVars, chdrenqIO);
			goTo(GotoLabel.keeps3070);
		}
		else {
			/*       MOVE 'M'                    TO WSSP-FLAG.            <010>*/
			wsspcomn.flag.set("N");
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.batching3080);
		}
		/*    Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		/* MOVE S5245-CHDRSEL          TO SFTL-ENTITY                   */
		sftlockrec.entity.set(chdrsurIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
		/*    For existing proposals, add one to the transaction number.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			setPrecision(chdrsurIO.getTranno(), 0);
			chdrsurIO.setTranno(add(chdrsurIO.getTranno(), 1));
		}
	}

protected void keeps3070()
	{
		/*   Store the contract header for use by the transaction programs*/
		if (isEQ(scrnparams.action, "C")
		|| isEQ(scrnparams.action, "D")) {
			chdrmjaIO.setChdrcoy(chdrsurIO.getChdrcoy());
			chdrmjaIO.setChdrnum(chdrsurIO.getChdrnum());
			chdrmjaIO.setFunction(varcom.reads);
			chdrmjaIO.setFormat(chdrmjarec);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.batching3080);
		}
		chdrsurIO.setFunction("KEEPS");
		chdrsurIO.setFormat(chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
		/**5100-CHECK-SUM-CONTRACT SECTION.                            <020>*/
		/** Loop round all the coverages and FLAG as a SUM contract if      */
		/** all the coverages use SUM ..........                            */
		/**5110-READ-COVRSUR.                                          <020>*/
		/**     MOVE SPACES                TO COVRSUR-PARAMS.          <020>*/
		/**     MOVE CHDRSUR-CHDRCOY       TO COVRSUR-CHDRCOY.         <020>*/
		/**     MOVE CHDRSUR-CHDRNUM       TO COVRSUR-CHDRNUM.         <020>*/
		/**     MOVE ZEROES                TO COVRSUR-PLAN-SUFFIX.     <020>*/
		/**     MOVE COVRSURREC            TO COVRSUR-FORMAT.          <020>*/
		/**     MOVE BEGN                  TO COVRSUR-FUNCTION.        <020>*/
		/**                                                            <020>*/
		/**5120-LOOP-COVRSUR.                                          <020>*/
		/**    CALL 'COVRSURIO' USING      COVRSUR-PARAMS.             <020>*/
		/**                                                            <020>*/
		/**    IF COVRSUR-STATUZ           NOT = O-K                   <020>*/
		/**    AND COVRSUR-STATUZ          NOT = ENDP                  <020>*/
		/**        MOVE COVRSUR-PARAMS     TO SYSR-PARAMS              <020>*/
		/**        PERFORM 600-FATAL-ERROR.                            <020>*/
		/**                                                            <020>*/
		/**    IF COVRSUR-CHDRCOY          NOT = CHDRSUR-CHDRCOY       <020>*/
		/**    OR COVRSUR-CHDRNUM          NOT = CHDRSUR-CHDRNUM       <020>*/
		/**    OR COVRSUR-STATUZ           = ENDP                      <020>*/
		/**        GO TO 5190-EXIT.                                    <020>*/
		/**                                                            <020>*/
		/** Check if the coverage uses the calculation package SUM.         */
		/** All coverages on the contract must use SUM if SUM processing    */
		/** is to be applicable. Note that flag set to non-space denotes    */
		/** a SUM product & a single coverage outside will set to space.    */
		/**    IF WSAA-SUM-FLAG NOT = SPACE                            <020>*/
		/**       MOVE SPACES              TO ITEM-PARAMS              <020>*/
		/**       MOVE 'IT'                TO ITEM-ITEMPFX             <020>*/
		/**       MOVE T5602               TO ITEM-ITEMTABL            <020>*/
		/**       MOVE COVRSUR-CHDRCOY     TO ITEM-ITEMCOY             <020>*/
		/**       MOVE COVRSUR-CRTABLE     TO ITEM-ITEMITEM            <020>*/
		/**       MOVE ITEMREC             TO ITEM-FORMAT              <020>*/
		/**       MOVE READR               TO ITEM-FUNCTION            <020>*/
		/**       CALL 'ITEMIO' USING ITEM-PARAMS                      <020>*/
		/**                                                            <020>*/
		/**       IF ITEM-STATUZ NOT = O-K                             <020>*/
		/**          MOVE SPACE            TO WSAA-SUM-FLAG.           <020>*/
		/**          GO TO 5190-EXIT.                                  <020>*/
		/**                                                            <020>*/
		/**    MOVE NEXTR                  TO COVRSUR-FUNCTION.        <020>*/
		/**    GO TO 5120-LOOP-COVRSUR.                                <020>*/
		/**                                                            <020>*/
		/**5190-EXIT.                                                  <020>*/
		/**    EXIT.                                                   <020>*/
		/**                                                            <020>*/
		/**5200-CHECK-SUM-EFFDATE SECTION.                             <020>*/
		/**5210-START.                                                 <020>*/
		/** Set the default date for a SUM product ......                   */
		/**    MOVE CHDRSUR-PTDATE        TO S5245-EFFDATE.            <020>*/
		/**    MOVE SPACES                TO DTC2-DATCON2-REC.         <020>*/
		/**    MOVE '12'                  TO DTC2-FREQUENCY.           <020>*/
		/**                                                            <020>*/
		/** As of 10/3/95 default to the paid to date if in the future.<020>*/
		/*************IF S5245-EFFDATE > WSAA-TODAY                         */
		/*************** MOVE -1           TO DTC2-FREQ-FACTOR              */
		/*************** PERFORM 5200-GET-EFFDATE UNTIL                     */
		/***************      S5245-EFFDATE < WSAA-TODAY                    */
		/*************** MOVE DTC2-INT-DATE-1 TO S5245-EFFDATE              */
		/*************ELSE                                                  */
		/**              IF S5245-EFFDATE < WSAA-TODAY                 <020>*/
		/**                 MOVE 1         TO DTC2-FREQ-FACTOR         <020>*/
		/**                 PERFORM 5300-GET-EFFDATE UNTIL             <020>*/
		/**                         S5245-EFFDATE > WSAA-TODAY.        <020>*/
		/**5290-EXIT.                                                  <020>*/
		/**    EXIT.                                                   <020>*/
		/**                                                            <020>*/
		/**5300-GET-EFFDATE SECTION.                                   <020>*/
		/**5310-START.                                                 <020>*/
		/** Use DATCON2 to add or subtract a month until the date is less   */
		/** than or greater than today's date ........                      */
		/** Where it is less than take the last date, where greater than    */
		/** use the higher date returned ...                                */
		/**                                                            <020>*/
		/**    MOVE S5245-EFFDATE          TO DTC2-INT-DATE-1.         <020>*/
		/**    MOVE ZERO                   TO DTC2-INT-DATE-2.         <020>*/
		/**                                                            <020>*/
		/**    CALL 'DATCON2' USING DTC2-DATCON2-REC.                  <020>*/
		/**    IF DTC2-STATUZ NOT = O-K                                <020>*/
		/**       MOVE DTC2-DATCON2-REC    TO SYSR-PARAMS              <020>*/
		/**       MOVE DTC2-STATUZ         TO SYSR-STATUZ              <020>*/
		/**       PERFORM 600-FATAL-ERROR.                             <020>*/
		/**                                                            <020>*/
		/** Move the new date to the screen effective date....              */
		/**    MOVE DTC2-INT-DATE-2        TO S5245-EFFDATE.           <020>*/
		/**                                                            <020>*/
		/**5390-EXIT.                                                  <020>*/
		/**    EXIT.                                                   <020>*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData f918 = new FixedLengthStringData(4).init("F918");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData f616 = new FixedLengthStringData(4).init("F616");
	private FixedLengthStringData e656 = new FixedLengthStringData(4).init("E656");
	private FixedLengthStringData e657 = new FixedLengthStringData(4).init("E657");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData t063 = new FixedLengthStringData(4).init("T063");
	private FixedLengthStringData rrfh = new FixedLengthStringData(4).init("RRFH");/*ICIL-14*/
	private FixedLengthStringData e944 = new FixedLengthStringData(4).init("E944");/*ICIL-14*/
	private FixedLengthStringData rrmu = new FixedLengthStringData(4).init("RRMU"); // ILIFE-7809
	
}
}