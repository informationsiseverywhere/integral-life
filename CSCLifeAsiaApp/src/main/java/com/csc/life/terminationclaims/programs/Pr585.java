/*
 * File: Pr585.java
 * Date: 30 August 2009 1:47:52
 * Author: Quipoz Limited
 * 
 * Class transformed from PR585.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.procedures.Tr585pt;
import com.csc.life.terminationclaims.screens.Sr585ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Tr585rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   Accident Benefit Plan
*   ---------------------
*
*       DO NOT COMPILE THIS TABLE SCREEN WITH *XDS
*       COMPILE USING *DSPF IF ANY CHANGES TO THIS SCREEN
*
*
*   IMPORTANT NOTE - Please take note
*   ==============
*   In order to capture more information, and not exceeds the
*   limit of GENAREA 500 characters, the description of Accident
*   Benefit set as output field.
*
***********************************************************************
* </pre>
*/
public class Pr585 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR585");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/* WSAA-VARIABLES */
	private FixedLengthStringData wsaaBenDesc = new FixedLengthStringData(30);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0);
		/* TABLES */
	private String tr584 = "TR584";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr585rec tr585rec = new Tr585rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr585ScreenVars sv = ScreenProgram.getScreenVars( Sr585ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public Pr585() {
		super();
		screenVars = sv;
		new ScreenModel("Sr585", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
					confirmationFields1050();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setParams(SPACES);
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr585rec.tr585Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		tr585rec.amtfld01.set(ZERO);
		tr585rec.amtfld02.set(ZERO);
		tr585rec.amtfld03.set(ZERO);
		tr585rec.amtfld04.set(ZERO);
		tr585rec.amtfld05.set(ZERO);
		tr585rec.amtfld06.set(ZERO);
		tr585rec.amtfld07.set(ZERO);
		tr585rec.amtfld08.set(ZERO);
		tr585rec.amtfld09.set(ZERO);
		tr585rec.amtfld10.set(ZERO);
		tr585rec.amtfld11.set(ZERO);
		tr585rec.amtfld12.set(ZERO);
		tr585rec.amtfld13.set(ZERO);
		tr585rec.amtfld14.set(ZERO);
		tr585rec.amtfld15.set(ZERO);
		tr585rec.dfclmpct01.set(ZERO);
		tr585rec.dfclmpct02.set(ZERO);
		tr585rec.dfclmpct03.set(ZERO);
		tr585rec.dfclmpct04.set(ZERO);
		tr585rec.dfclmpct05.set(ZERO);
		tr585rec.dfclmpct06.set(ZERO);
		tr585rec.dfclmpct07.set(ZERO);
		tr585rec.dfclmpct08.set(ZERO);
		tr585rec.dfclmpct09.set(ZERO);
		tr585rec.dfclmpct10.set(ZERO);
		tr585rec.dfclmpct11.set(ZERO);
		tr585rec.dfclmpct12.set(ZERO);
		tr585rec.dfclmpct13.set(ZERO);
		tr585rec.dfclmpct14.set(ZERO);
		tr585rec.dfclmpct15.set(ZERO);
		tr585rec.mxbenunt01.set(ZERO);
		tr585rec.mxbenunt02.set(ZERO);
		tr585rec.mxbenunt03.set(ZERO);
		tr585rec.mxbenunt04.set(ZERO);
		tr585rec.mxbenunt05.set(ZERO);
		tr585rec.mxbenunt06.set(ZERO);
		tr585rec.mxbenunt07.set(ZERO);
		tr585rec.mxbenunt08.set(ZERO);
		tr585rec.mxbenunt09.set(ZERO);
		tr585rec.mxbenunt10.set(ZERO);
		tr585rec.mxbenunt11.set(ZERO);
		tr585rec.mxbenunt12.set(ZERO);
		tr585rec.mxbenunt13.set(ZERO);
		tr585rec.mxbenunt14.set(ZERO);
		tr585rec.mxbenunt15.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.acdbens.set(tr585rec.acdbens);
		sv.amtflds.set(tr585rec.amtflds);
		sv.benfreqs.set(tr585rec.benfreqs);
		sv.contitem.set(tr585rec.contitem);
		sv.dfclmpcts.set(tr585rec.dfclmpcts);
		sv.gcdblinds.set(tr585rec.gcdblinds);
		sv.mxbenunts.set(tr585rec.mxbenunts);
	}

protected void confirmationFields1050()
	{
		for (ix.set(1); !(isGT(ix,15)); ix.add(1)){
			if (isNE(sv.acdben[ix.toInt()],SPACES)) {
				getBenefitDesc2100();
				sv.ditdsc[ix.toInt()].set(wsaaBenDesc);
			}
			else {
				sv.ditdsc[ix.toInt()].set(SPACES);
			}
		}
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
		for (ix.set(1); !(isGT(ix,15)); ix.add(1)){
			if (isNE(sv.acdben[ix.toInt()],SPACES)) {
				getBenefitDesc2100();
				sv.ditdsc[ix.toInt()].set(wsaaBenDesc);
			}
			else {
				sv.ditdsc[ix.toInt()].set(SPACES);
			}
		}
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void getBenefitDesc2100()
	{
		start2110();
	}

protected void start2110()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tr584);
		descIO.setDescitem(sv.acdben[ix.toInt()]);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaBenDesc.set(descIO.getLongdesc());
		}
		else {
			wsaaBenDesc.fill("?");
		}
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(tr585rec.tr585Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.acdbens,tr585rec.acdbens)) {
			tr585rec.acdbens.set(sv.acdbens);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.amtflds,tr585rec.amtflds)) {
			tr585rec.amtflds.set(sv.amtflds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.benfreqs,tr585rec.benfreqs)) {
			tr585rec.benfreqs.set(sv.benfreqs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.contitem,tr585rec.contitem)) {
			tr585rec.contitem.set(sv.contitem);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.dfclmpcts,tr585rec.dfclmpcts)) {
			tr585rec.dfclmpcts.set(sv.dfclmpcts);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.gcdblinds,tr585rec.gcdblinds)) {
			tr585rec.gcdblinds.set(sv.gcdblinds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.mxbenunts,tr585rec.mxbenunts)) {
			tr585rec.mxbenunts.set(sv.mxbenunts);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tr585pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
