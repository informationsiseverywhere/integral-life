/*
 * File: Tradsur.java
 * Date: 30 August 2009 2:45:00
 * Author: Quipoz Limited
 * 
 * Class transformed from TRADSUR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.Iterator; //ILB-1029
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;

import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO; //ILB-1029
import com.csc.life.productdefinition.dataaccess.model.Covrpf; //ILB-1029
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.tablestructures.T5617rec;
import com.csc.life.terminationclaims.tablestructures.T5618rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   TRADITIONAL LIFE SURRENDER CALCULATION USING SURRENDER
*   VALUE FACTORS FROM TABLES USING NO RESERVE CALCULATIONS.
*
*
*  This program calculates the surrender the Sum Assured
*  and Bonus Surrender Values using factors on T5617/T5618
*  respectively. It is based on structure of PRESERV.
*
*  For the first time through the Sum Assured Surrender value
*  is calculated.  For the Second time through the bonus
*  Surrender Value is calculated.
*
*  The program may be called to process either both of these
*  or bonus only - could be called from Bonus Surrender and
*  not Full Surrender i.e. Second Time through only.
*
*  To establish which 'pass' the program is currently doing
*  the program uses SURC-ENDF.  If this is 'B' it is the
*  second time through, if spaces it is the first time through
*  otherwise it is an error.
*
*   To do the BONUS reserve only, the program must be called
*   with the SURC-STATUS = 'BONS'. If this is received by the
*   program, it immediately sets SURC-ENDF to 'B' to indicate
*   this is the SECOND-TIME-THROUGH.
*
*   The program can calculate these values for either a single
*   component or for the whole-plan.
*   This is established by examination of SURC-PLAN-SUFFIX.
*   If this is zero, then process the whole-plan, otherwise
*   process the single policy for the value passed.
*   For single component processing, the policy may be broken
*   out or it may be NOTIONAL, i.e. part of the summarised
*   record. The program establishes this before reading the
*   coverage file.
*
*   If this is a FULL SURRENDER, then a figure for the SUM
*   ASSURED will be calculated           for either the single
*   policy requested or for the whole plan by accumulation
*   of the figures for all the policy records pertaining to
*   this COVERAGE/RIDER (including the summary record).
*   This will then be repeated for the BONUS value.
*
*   The SUM ASSURED is lifted directly from the component
*   coverage and the REVERSIONARY BONUS from the ACCOUNT BALANCE
*   file. The factor returned from the tables
*   then applied to this figure.
*   Where a value   is required for a single NOTIONAL policy,
*   the figure for the summary record is passed and this will
*   be divided by the number of policies summarised subsequently.
*
*   The tables used are:- T5617 for sum assured factors and
*   is T5618 for Bonus factors, zero is returned to the
*   calling program.
*   The tables are keyed on component/term.
*   Then depending on the premium duration a factor is
*   retrieved.  Where premium duration falls between factors ie
*   part years we interpolate between the lower and higher
*   values to get the exact current duration value.
*
*****************************************************************
* </pre>
*/
public class Tradsur extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "TRADSUR";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT5617Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5617Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5617Key, 0);
	private FixedLengthStringData wsaaT5617Term = new FixedLengthStringData(2).isAPartOf(wsaaT5617Key, 4);
		/* WSAA-DURATION */
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaSurrenderVal = new PackedDecimalData(18, 9);
	private PackedDecimalData wsaaSurrenderFactor = new PackedDecimalData(18, 9);
		/* WSAA-EXACT-VALUES */
	private PackedDecimalData wsaaExactValue = new PackedDecimalData(18, 10);
		/* WSAA-VALUES */
	private PackedDecimalData wsaaLowValue = new PackedDecimalData(18, 10);
	private PackedDecimalData wsaaHighValue = new PackedDecimalData(18, 10);
		/* WSAA-FRACTIONS-DURATIONS */
	private ZonedDecimalData wsaaFractionDuration = new ZonedDecimalData(6, 5);
		/* WSAA-IN-FORCE-DURATIONS */
	private ZonedDecimalData wsaaInforceDur = new ZonedDecimalData(11, 5);
	private ZonedDecimalData wsaaDurLowInteger = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaDurHighInteger = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaRunIndicator = new FixedLengthStringData(1);
	private Validator firstTimeThrough = new Validator(wsaaRunIndicator, " ");
	private Validator secondTimeThrough = new Validator(wsaaRunIndicator, "B");

	private FixedLengthStringData wsaaAcblKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAcblChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaAcblKey, 0);
	private FixedLengthStringData wsaaAcblLife = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 8);
	private FixedLengthStringData wsaaAcblCoverage = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 10);
	private FixedLengthStringData wsaaAcblRider = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 12);
	private FixedLengthStringData wsaaAcblPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaAcblKey, 14);
	private PackedDecimalData wsaaAcblCurrentBalance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSurrenderTot = new PackedDecimalData(18, 3);
	private PackedDecimalData wsaaBonusTot = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaProcessingType = new FixedLengthStringData(1);
	private Validator wholePlan = new Validator(wsaaProcessingType, "1");
	private Validator singleComponent = new Validator(wsaaProcessingType, "2");
	private Validator notionalSingleComponent = new Validator(wsaaProcessingType, "3");
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
		/* ERRORS */
	private String g344 = "G344";
	private String h134 = "H134";
	private String h135 = "H135";
	private String h053 = "H053";
	private String h155 = "H155";
	private String i192 = "I192";
		/* FORMATS */
	private String acblrec = "ACBLREC";
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String t5645 = "T5645";
	private String t5687 = "T5687";
	private String t3695 = "T3695";
	private String t5617 = "T5617";
	private String t5618 = "T5618";
		/*Subsidiary account balance*/
	private AcblTableDAM acblIO = new AcblTableDAM();
		/*COVR layout for trad. reversionary bonus*/

	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	private Srcalcpy srcalcpy = new Srcalcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5617rec t5617rec = new T5617rec();
	private T5618rec t5618rec = new T5618rec();
	private T5645rec t5645rec = new T5645rec();
	private Varcom varcom = new Varcom();
	ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	List<Itempf> listT5617 = new ArrayList<Itempf>();
	
	private ExternalisedRules er = new ExternalisedRules();
	Vpxsurcrec vpxsurcRec=null;
 //ILB-1029 start
	 private Covrpf covrtrb = new Covrpf();
	 private List<Covrpf> covrpfList = new ArrayList<>();
	 private Iterator<Covrpf> covrpfIter;
	 private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
 //ILB-1029 end	

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit3199, 
		exit4099, 
		exit5099, 
		exit5419, 
		exit5429, 
		seExit9090, 
		dbExit9190
	}

	public Tradsur() {
		super();
	}

public void mainline(Object... parmArray)
	{
		srcalcpy.surrenderRec = convertAndSetParam(srcalcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		/*MAIN*/
		vpxsurcRec=new Vpxsurcrec();
		int count=0;
	
		initialise2000();
 //ILB-1029 start
		if(covrpfList != null && !(covrpfList.isEmpty())){ 
			 covrpfIter = covrpfList.iterator();
			  while (covrpfIter.hasNext()) {
			obtainInitVals3000();
			readAcbl4000();
			
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMTRADSUR") 
					&& er.isExternalized(srcalcpy.cnttype.toString(), covrtrb.getCrtable()))) {
				calcSurrenderValue5000();
			}
			else {
				switch(count){
					case 0:
						setCovrTrbVal00(covrtrb);
						break;
					case 1:
						setCovrTrbVal01(covrtrb);
						break;
					case 2:
						setCovrTrbVal02(covrtrb);
						break;
					case 3:
						setCovrTrbVal03(covrtrb);
						break;
					case 4:
						setCovrTrbVal04(covrtrb);
						break;
					case 5:
						setCovrTrbVal05(covrtrb);
						break;
					case 6:
						setCovrTrbVal06(covrtrb);
						break;
					case 7:
						setCovrTrbVal07(covrtrb);
						break;
					case 8:
						setCovrTrbVal08(covrtrb);
						break;
					case 9:
						setCovrTrbVal09(covrtrb);
						break;
					default : {
						break;
					}
				}
				count=count+1;
				
			}
 //ILB-1029 end
			if (singleComponent.isTrue()
			|| notionalSingleComponent.isTrue()) {
				break;
			}
			else {
			
				covrtrb = covrpfIter.next();
			}
		  }
		}
		if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMTRADSUR") 
				&& er.isExternalized(srcalcpy.cnttype.toString(), covrtrb.getCrtable()))) {
			vpxsurcRec.covrcnt.set(count);
			if (secondTimeThrough.isTrue()) {
				srcalcpy.type.set("B");
			}
			else {
				srcalcpy.type.set("");
			}
			callProgram("VPMTRADSUR", srcalcpy.surrenderRec, vpxsurcRec);
			if(secondTimeThrough.isTrue()) {
				wsaaSurrenderTot.set(srcalcpy.actualVal1);
				wsaaBonusTot.set(srcalcpy.estimatedVal);
			}
			else {
				wsaaSurrenderTot.set(srcalcpy.actualVal);
			}
		}
		
		setExitVariables7000();
		/*RETURN*/
		exitProgram();
	}

protected void setCovrTrbVal00(Covrpf covrtrbio) {
	vpxsurcRec.covrrcomdate00.set(covrtrbio.getCrrcd());
	vpxsurcRec.covrrcesdate00.set(covrtrbio.getRiskCessDate());
	vpxsurcRec.covrpcesdate00.set(covrtrbio.getPremCessDate());
	vpxsurcRec.covrsumins00.set(covrtrbio.getSumins());
	vpxsurcRec.acblsacscurbal00.set(wsaaAcblCurrentBalance);
}
protected void setCovrTrbVal01(Covrpf covrtrbio) {
	vpxsurcRec.covrrcomdate01.set(covrtrbio.getCrrcd());
	vpxsurcRec.covrrcesdate01.set(covrtrbio.getRiskCessDate());
	vpxsurcRec.covrpcesdate01.set(covrtrbio.getPremCessDate());
	vpxsurcRec.covrsumins01.set(covrtrbio.getSumins());
	vpxsurcRec.acblsacscurbal01.set(wsaaAcblCurrentBalance);
}
protected void setCovrTrbVal02(Covrpf covrtrbio) {
	vpxsurcRec.covrrcomdate02.set(covrtrbio.getCrrcd());
	vpxsurcRec.covrrcesdate02.set(covrtrbio.getRiskCessDate());
	vpxsurcRec.covrpcesdate02.set(covrtrbio.getPremCessDate());
	vpxsurcRec.covrsumins02.set(covrtrbio.getSumins());
	vpxsurcRec.acblsacscurbal02.set(wsaaAcblCurrentBalance);
}
protected void setCovrTrbVal03(Covrpf covrtrbio) {
	vpxsurcRec.covrrcomdate03.set(covrtrbio.getCrrcd());
	vpxsurcRec.covrrcesdate03.set(covrtrbio.getRiskCessDate());
	vpxsurcRec.covrpcesdate03.set(covrtrbio.getPremCessDate());
	vpxsurcRec.covrsumins03.set(covrtrbio.getSumins());
	vpxsurcRec.acblsacscurbal03.set(wsaaAcblCurrentBalance);
}
protected void setCovrTrbVal04(Covrpf covrtrbio) {
	vpxsurcRec.covrrcomdate04.set(covrtrbio.getCrrcd());
	vpxsurcRec.covrrcesdate04.set(covrtrbio.getRiskCessDate());
	vpxsurcRec.covrpcesdate04.set(covrtrbio.getPremCessDate());
	vpxsurcRec.covrsumins04.set(covrtrbio.getSumins());
	vpxsurcRec.acblsacscurbal04.set(wsaaAcblCurrentBalance);
}
protected void setCovrTrbVal05(Covrpf covrtrbio) {
	vpxsurcRec.covrrcomdate05.set(covrtrbio.getCrrcd());
	vpxsurcRec.covrrcesdate05.set(covrtrbio.getRiskCessDate());
	vpxsurcRec.covrpcesdate05.set(covrtrbio.getPremCessDate());
	vpxsurcRec.covrsumins05.set(covrtrbio.getSumins());
	vpxsurcRec.acblsacscurbal05.set(wsaaAcblCurrentBalance);
}
protected void setCovrTrbVal06(Covrpf covrtrbio) {
	vpxsurcRec.covrrcomdate06.set(covrtrbio.getCrrcd());
	vpxsurcRec.covrrcesdate06.set(covrtrbio.getRiskCessDate());
	vpxsurcRec.covrpcesdate06.set(covrtrbio.getPremCessDate());
	vpxsurcRec.covrsumins06.set(covrtrbio.getSumins());
	vpxsurcRec.acblsacscurbal06.set(wsaaAcblCurrentBalance);
}
protected void setCovrTrbVal07(Covrpf covrtrbio) {
	vpxsurcRec.covrrcomdate07.set(covrtrbio.getCrrcd());
	vpxsurcRec.covrrcesdate07.set(covrtrbio.getRiskCessDate());
	vpxsurcRec.covrpcesdate07.set(covrtrbio.getPremCessDate());
	vpxsurcRec.covrsumins07.set(covrtrbio.getSumins());
	vpxsurcRec.acblsacscurbal07.set(wsaaAcblCurrentBalance);
}
protected void setCovrTrbVal08(Covrpf covrtrbio) {
	vpxsurcRec.covrrcomdate08.set(covrtrbio.getCrrcd());
	vpxsurcRec.covrrcesdate08.set(covrtrbio.getRiskCessDate());
	vpxsurcRec.covrpcesdate08.set(covrtrbio.getPremCessDate());
	vpxsurcRec.covrsumins08.set(covrtrbio.getSumins());
	vpxsurcRec.acblsacscurbal08.set(wsaaAcblCurrentBalance);
}
protected void setCovrTrbVal09(Covrpf covrtrbio) {
	vpxsurcRec.covrrcomdate09.set(covrtrbio.getCrrcd());
	vpxsurcRec.covrrcesdate09.set(covrtrbio.getRiskCessDate());
	vpxsurcRec.covrpcesdate09.set(covrtrbio.getPremCessDate());
	vpxsurcRec.covrsumins09.set(covrtrbio.getSumins());
	vpxsurcRec.acblsacscurbal09.set(wsaaAcblCurrentBalance);
}


protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(srcalcpy.status,"BONS")) {
			wsaaRunIndicator.set("B");
		}
		else {
			wsaaRunIndicator.set(srcalcpy.endf);
		}
		if (!firstTimeThrough.isTrue()
		&& !secondTimeThrough.isTrue()) {
			syserrrec.params.set(srcalcpy.endf);
			syserrrec.statuz.set(h155);
			systemError9000();
		}
		wsaaSurrenderTot.set(ZERO);
		wsaaBonusTot.set(ZERO);
		if (isEQ(srcalcpy.planSuffix,ZERO)) {
			wsaaProcessingType.set("1");
		}
		else {
			if (isLTE(srcalcpy.planSuffix,srcalcpy.polsum)
			&& isNE(srcalcpy.polsum,1)) {
				wsaaProcessingType.set("3");
			}
			else {
				wsaaProcessingType.set("2");
			}
		}
		readFiles2100();
		if (firstTimeThrough.isTrue()) {
			getSaDescription2400();
		}
		else {
			getRbDescAndAmt2500();
			srcalcpy.type.set("B");
		}
	}

protected void readFiles2100()
	{
		start2100();
	}

protected void start2100()
	{
	 //ILB-1029 start
		covrpfList = covrpfDAO.getcovrtrbRecord(srcalcpy.chdrChdrcoy.toString(), srcalcpy.chdrChdrnum.toString(), srcalcpy.lifeLife.toString(), srcalcpy.covrCoverage.toString(), srcalcpy.covrRider.toString());
		if (notionalSingleComponent.isTrue()) {
			covrtrb.setPlanSuffix(0);
		}
		if (covrpfList == null || covrpfList.isEmpty()) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(srcalcpy.chdrChdrcoy);
			stringVariable1.append(srcalcpy.chdrChdrnum);
			stringVariable1.append(srcalcpy.lifeLife);
			stringVariable1.append(srcalcpy.covrCoverage);
			stringVariable1.append(srcalcpy.covrRider);
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(g344);
			dbError9100();
		}
		covrpfIter = covrpfList.iterator();
		if(covrpfIter.hasNext()){
		covrtrb = covrpfIter.next();
		} 
		
		if ((singleComponent.isTrue()
		&& isNE(covrtrb.getPlanSuffix(),srcalcpy.planSuffix))
		|| (notionalSingleComponent.isTrue()
		&& isNE(covrtrb.getPlanSuffix(),0))) {
			wsaaPlan.set(srcalcpy.planSuffix);
			StringBuilder stringVariable2 = new StringBuilder();
			stringVariable2.append(srcalcpy.chdrChdrcoy);
			stringVariable2.append(srcalcpy.chdrChdrnum);
			stringVariable2.append(srcalcpy.lifeLife);
			stringVariable2.append(srcalcpy.covrCoverage);
			stringVariable2.append(srcalcpy.covrRider);
			stringVariable2.append(wsaaPlan);
			syserrrec.params.setLeft(stringVariable2.toString());
			syserrrec.statuz.set(g344);
			dbError9100();
		}
	}
 //ILB-1029 end
protected void getSaDescription2400()
	{
		start2400();
	}

protected void start2400()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(srcalcpy.crtable);
		descIO.setLanguage(srcalcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(h053);
			syserrrec.params.set(descIO.getParams());
			systemError9000();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			srcalcpy.description.fill("?");
		}
		else {
			srcalcpy.description.set(descIO.getShortdesc());
		}
	}

protected void getRbDescAndAmt2500()
	{
		start2500();
	}

protected void start2500()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError9000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(h134);
			systemError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(srcalcpy.chdrChdrcoy);
		descIO.setDesctabl(t3695);
		descIO.setDescitem(t5645rec.sacstype07);            //ILIFE-8985
		descIO.setLanguage(srcalcpy.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(h135);
			systemError9000();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			srcalcpy.description.fill("?");
		}
		else {
			srcalcpy.description.set(descIO.getShortdesc());
		}
	}

protected void obtainInitVals3000()
	{
		/*START*/
		yearsInForce3100();
		calculateTerm3200();
		/*EXIT*/
	}

protected void yearsInForce3100()
	{
		try {
			premiumDuration3100();
			premiumPaidUp3120();
		}
		catch (GOTOException e){
		}
	}

protected void premiumDuration3100()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.crrcd);
		datcon3rec.intDate2.set(srcalcpy.ptdate);
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		compute(datcon3rec.freqFactor, 5).set(div(datcon3rec.freqFactor,12));
		wsaaInforceDur.set(datcon3rec.freqFactor);
		wsaaDurLowInteger.set(wsaaInforceDur);
		compute(wsaaDurHighInteger, 0).set(add(1,wsaaDurLowInteger));
		compute(wsaaFractionDuration, 5).set(sub(wsaaInforceDur,wsaaDurLowInteger));
	}

protected void premiumPaidUp3120()
	{
		if (isLTE(srcalcpy.effdate,covrtrb.getPremCessDate())) {
			goTo(GotoLabel.exit3199);
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(srcalcpy.crrcd);
		datcon3rec.intDate2.set(srcalcpy.effdate);
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		compute(datcon3rec.freqFactor, 5).set(div(datcon3rec.freqFactor,12));
		wsaaInforceDur.set(datcon3rec.freqFactor);
		wsaaDurLowInteger.set(wsaaInforceDur);
		compute(wsaaDurHighInteger, 0).set(add(1,wsaaDurLowInteger));
		compute(wsaaFractionDuration, 5).set(sub(wsaaInforceDur,wsaaDurLowInteger));
	}

protected void calculateTerm3200()
	{
		start3200();
	}

protected void start3200()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrb.getCrrcd());
		datcon3rec.intDate2.set(covrtrb.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaTerm.set(datcon3rec.freqFactor);
	}

protected void readAcbl4000()
	{
		try {
			start4000();
		}
		catch (GOTOException e){
		}
	}

protected void start4000()
	{
		if (firstTimeThrough.isTrue()) {
			wsaaAcblCurrentBalance.set(0);
			goTo(GotoLabel.exit4099);
		}
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(srcalcpy.chdrChdrcoy);
		acblIO.setSacscode(t5645rec.sacscode07);  //ILIFE-8985          
		wsaaAcblChdrnum.set(srcalcpy.chdrChdrnum);
		wsaaAcblLife.set(srcalcpy.lifeLife);
		wsaaAcblCoverage.set(srcalcpy.covrCoverage);
		wsaaAcblRider.set(srcalcpy.covrRider);
		wsaaPlan.set(covrtrb.getPlanSuffix()); //ILB-1029
		wsaaAcblPlanSuffix.set(wsaaPlansuff);
		acblIO.setRldgacct(wsaaAcblKey);
		acblIO.setOrigcurr(srcalcpy.currcode);
		acblIO.setSacstyp(t5645rec.sacstype07);    //ILIFE-8985
		acblIO.setFunction(varcom.readr);
		acblIO.setFormat(acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if ((isNE(acblIO.getStatuz(),varcom.oK)
		&& isNE(acblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(acblIO.getParams());
			syserrrec.statuz.set(acblIO.getStatuz());
			dbError9100();
		}
		if (isEQ(acblIO.getStatuz(),varcom.mrnf)) {
			wsaaAcblCurrentBalance.set(0);
		}
		else {
			wsaaAcblCurrentBalance.set(acblIO.getSacscurbal());
		}
		wsaaBonusTot.add(wsaaAcblCurrentBalance);
	}

protected void calcSurrenderValue5000()
	{
		try {
			start5000();
		}
		catch (GOTOException e){
		}
	}

protected void start5000()
	{
		wsaaSurrenderVal.set(0);
		if (secondTimeThrough.isTrue()
		&& isEQ(wsaaAcblCurrentBalance,0)) {
			goTo(GotoLabel.exit5099);
		}
		calculateSurrValue5400();
	}

protected void calculateSurrValue5400()
	{
		/*START*/
		if (firstTimeThrough.isTrue()) {
			getSaFactor5410();
		}
		else {
			getBonusFactor5420();
		}
		if (firstTimeThrough.isTrue()) {
			compute(wsaaSurrenderVal, 9).set(mult(wsaaSurrenderFactor,covrtrb.getSumins())); //ILB-1029
		}
		else {
			compute(wsaaSurrenderVal, 9).set(mult(wsaaSurrenderFactor,wsaaAcblCurrentBalance));
		}
		wsaaSurrenderTot.addRounded(wsaaSurrenderVal);
		/*EXIT*/
	}

protected void getSaFactor5410()
	{
		try {
			start5410();
		}
		catch (GOTOException e){
		}
	}

protected void start5410()
	{
	wsaaT5617Crtable.set(srcalcpy.crtable);
	wsaaT5617Term.set(wsaaTerm);
	
	listT5617 = itemDAO.getAllItemitemByDateFrm("IT",srcalcpy.chdrChdrcoy.toString(), t5617,wsaaT5617Key.toString(), srcalcpy.effdate.toInt());
		
		if(!listT5617.isEmpty()) {
			t5617rec.t5617Rec.set(StringUtil.rawToString(listT5617.get(0).getGenarea()));
			
			if (isNE(wsaaDurLowInteger,ZERO)) {
				wsaaLowValue.set(t5617rec.insprm[wsaaDurLowInteger.toInt()]);
			}
			else {
				wsaaLowValue.set(ZERO);
			}
			if (isEQ(wsaaDurLowInteger,ZERO)
			&& isEQ(t5617rec.insprm[wsaaDurHighInteger.toInt()],ZERO)) {
				wsaaSurrenderFactor.set(ZERO);
				goTo(GotoLabel.exit5419);
			}
			if (isNE(wsaaDurLowInteger,ZERO)
			&& isEQ(t5617rec.insprm[wsaaDurLowInteger.toInt()],ZERO)) {
				wsaaSurrenderFactor.set(ZERO);
				goTo(GotoLabel.exit5419);
			}
			wsaaHighValue.set(t5617rec.insprm[wsaaDurHighInteger.toInt()]);
			compute(wsaaExactValue, 11).setRounded(add(mult(wsaaFractionDuration,(sub(wsaaHighValue,wsaaLowValue))),wsaaLowValue));
			compute(wsaaSurrenderFactor, 10).set(div(wsaaExactValue,t5617rec.unit));
		}
		else {
			srcalcpy.status.set(varcom.mrnf);
			exitProgram();
		}
		
	}

protected void getBonusFactor5420()
	{
		try {
			start5420();
		}
		catch (GOTOException e){
		}
	}

protected void start5420()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		itdmIO.setItemtabl(t5618);
		wsaaT5617Crtable.set(srcalcpy.crtable);
		wsaaT5617Term.set(wsaaTerm);
		itdmIO.setItemitem(wsaaT5617Key);
		itdmIO.setItmfrm(srcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),srcalcpy.chdrChdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5618)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(srcalcpy.crtable);
			syserrrec.statuz.set(i192);
			dbError9100();
		}
		else {
			t5618rec.t5618Rec.set(itdmIO.getGenarea());
		}
		if (isNE(wsaaDurLowInteger,ZERO)) {
			wsaaLowValue.set(t5618rec.insprm[wsaaDurLowInteger.toInt()]);
		}
		else {
			wsaaLowValue.set(ZERO);
		}
		if (isEQ(wsaaDurLowInteger,ZERO)
		&& isEQ(t5618rec.insprm[wsaaDurHighInteger.toInt()],ZERO)) {
			wsaaSurrenderFactor.set(ZERO);
			goTo(GotoLabel.exit5429);
		}
		if (isNE(wsaaDurLowInteger,ZERO)
		&& isEQ(t5618rec.insprm[wsaaDurLowInteger.toInt()],ZERO)) {
			wsaaSurrenderFactor.set(ZERO);
			goTo(GotoLabel.exit5429);
		}
		wsaaHighValue.set(t5618rec.insprm[wsaaDurHighInteger.toInt()]);
		compute(wsaaExactValue, 11).setRounded(add(mult(wsaaFractionDuration,(sub(wsaaHighValue,wsaaLowValue))),wsaaLowValue));
		compute(wsaaSurrenderFactor, 10).set(div(wsaaExactValue,t5618rec.unit));
	}


protected void setExitVariables7000()
	{
		start7000();
	}

protected void start7000()
	{
		srcalcpy.actualVal.setRounded(wsaaSurrenderTot);
		srcalcpy.estimatedVal.set(0);
		if (firstTimeThrough.isTrue()) {
			srcalcpy.status.set("****");
			srcalcpy.endf.set("B");
			srcalcpy.type.set("S");
		}
		else {
			if (isEQ(srcalcpy.status,"BONS")) {
				srcalcpy.estimatedVal.set(wsaaBonusTot);
			}
			srcalcpy.status.set("ENDP");
			srcalcpy.endf.set(SPACES);
			srcalcpy.type.set("B");
		}
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		srcalcpy.status.set(varcom.bomb);
		exitProgram();
	}
}
