/*
 * File: Pr582.java
 * Date: 30 August 2009 1:47:33
 * Author: Quipoz Limited
 *
 * Class transformed from PR582.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.recordstructures.Wsspdocs;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.ZmpvTableDAM;
import com.csc.life.terminationclaims.dataaccess.AcldTableDAM;
import com.csc.life.terminationclaims.dataaccess.AclhTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.screens.Sr582ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Tr585rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Itdmkey;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* This program handles Create, Update, Delete and Inquiry of Accident
* benefit claims. Depending on option(1=Create, 2=Modify, 3=Delete and
* 4=Inquiry) selected by user, next program PR583 will handle the requ-
* ired functionality. Also, this pragram creates a record in ACLH file.
*
***********************************************************************
* </pre>
*/
public class Pr582 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(SmartFileCode.class);

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR582");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTr585Itemitem = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaTr585Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTr585Itemitem, 0);
	private FixedLengthStringData wsaaSlt = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaStoreFlag = new FixedLengthStringData(1);
	private String wsaaTable = "";
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8);
	private String wsaaPayeeName = "PYNMN";
	private FixedLengthStringData wsaaScrnStatuz = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaTotPayable = new ZonedDecimalData(12, 2).setUnsigned();
		/* WSAA-VARIABLES */
	private ZonedDecimalData ix = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaBenDesclong = new FixedLengthStringData(29);
	private FixedLengthStringData wsaaShrtDesc = new FixedLengthStringData(29);
	private String g624 = "G624";
	private String w121 = "W121";
		/* TABLES */
	private String t5687 = "T5687";
	private String tr584 = "TR584";
	private String tr585 = "TR585";
	private String covrrec = "COVRREC";
	private String itdmrec = "ITEMREC   ";
	private String descrec = "DESCREC";
	private String aclhrec = "ACLHREC";
	private String acldrec = "ACLDREC";
	private String zmpvrec = "ZMPVREC";
		/*Accident Claim Detail File*/
	private AcldTableDAM acldIO = new AcldTableDAM();
		/*Accident Claim Header*/
	private AclhTableDAM aclhIO = new AclhTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Claims Life Record*/
	private LifeclmTableDAM lifeclmIO = new LifeclmTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Optswchrec optswchrec = new Optswchrec();
		/*Regular Payments File*/
	private RegpTableDAM regpIO = new RegpTableDAM();

	/*Medical Provider Details File*/
	private ZmpvTableDAM zmpvIO = new ZmpvTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Itdmkey wsaaItdmkey = new Itdmkey();

	private Tr585rec tr585rec = new Tr585rec();
	private Wsspdocs wsspdocs = new Wsspdocs();
	private Sr582ScreenVars sv = ScreenProgram.getScreenVars( Sr582ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readCovr1040,
		loadSubfile1050,
		exit1090,
		payeeExit,
		validateSubfile2060,
		optswch4080,
		exit4090
	}

	public Pr582() {
		super();
		screenVars = sv;
		new ScreenModel("Sr582", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspdocs.userArea = convertAndSetParam(wsspdocs.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
					retrieveCovrmja1020();
					readAclh1030();
				case readCovr1040:
					readCovr1040();
				case loadSubfile1050:
					loadSubfile1050();
				case exit1090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			/* Perform a RLSE on ACLH file*/
			aclhIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, aclhIO);
			if (isNE(aclhIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(aclhIO.getParams());
				syserrrec.statuz.set(aclhIO.getStatuz());
				fatalError600();
			}
			wsspcomn.flag.set(wsaaStoreFlag);
			if (isEQ(wsaaSlt,"1")
			|| isEQ(wsaaSlt,"2")
			|| isEQ(wsaaSlt,"3")
			|| isEQ(wsaaSlt,"4")) {
				sv.sel.set(SPACES);
				scrnparams.function.set(varcom.sclr);
				processScreen("SR582", sv);
				if (isNE(scrnparams.statuz,varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					syserrrec.params.set(scrnparams.screenParams);
					fatalError600();
				}
				scrnparams.subfileRrn.set(1);
				goTo(GotoLabel.readCovr1040);
			}
			wsaaSlt.set(SPACES);
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaScrnStatuz.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR582", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.acdben.set(SPACES);
		sv.bendesc.set(SPACES);
		sv.benfreq.set(SPACES);
		sv.gcdblind.set(SPACES);
		sv.chdrnum.set(SPACES);
		sv.crtable.set(SPACES);
		sv.crtabled.set(SPACES);
		sv.lifenum.set(SPACES);
		sv.lifename.set(SPACES);
		sv.diagcde.set(SPACES);
		sv.shortdesc.set(SPACES);
		sv.zdoctor.set(SPACES);
		sv.givname.set(SPACES);
		sv.zmedprv.set(SPACES);
		sv.cdesc.set(SPACES);
		sv.tclmamt.set(ZERO);
		sv.rgpynum.set(ZERO);
		sv.amtaout.set(ZERO);
		sv.gcnetpy.set(ZERO);
		wsaaTotPayable.set(ZERO);
		sv.mxbenunt.set(ZERO);
		sv.acbenunt.set(ZERO);
		sv.zrsumin.set(ZERO);
		sv.actexp.set(ZERO);
		sv.crrcd.set(varcom.maxdate);
		sv.dischdt.set(varcom.maxdate);
		sv.gcadmdt.set(varcom.maxdate);
		sv.incurdt.set(varcom.maxdate);
		screenInitOptswch1700();
	}

	/**
	* <pre>
	*    Set screen fields
	* </pre>
	*/
protected void retrieveCovrmja1020()
	{
		/* Retrieve from REGP....*/
		regpIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			syserrrec.statuz.set(regpIO.getStatuz());
			fatalError600();
		}
		sv.rgpynum.set(regpIO.getRgpynum());
		sv.chdrnum.set(regpIO.getChdrnum());
		sv.crtable.set(regpIO.getCrtable());
		sv.incurdt.set(regpIO.getIncurdt());
		/*  Read T5687 description to get Coverage/Rider type*/
		/*  description.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(regpIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.crtabled.fill("?");
		}
		else {
			sv.crtabled.set(descIO.getLongdesc());
		}
		/* Retrieve from COVTRBN....*/
		covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.mrnf)) {
			covrmjaIO.setChdrcoy(regpIO.getChdrcoy());
			covrmjaIO.setChdrnum(regpIO.getChdrnum());
			covrmjaIO.setLife(regpIO.getLife());
			covrmjaIO.setCoverage(regpIO.getCoverage());
			covrmjaIO.setRider(regpIO.getRider());
			covrmjaIO.setPlanSuffix(ZERO);
			covrmjaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				syserrrec.statuz.set(covrmjaIO.getStatuz());
				fatalError600();
			}
		}
		sv.crrcd.set(covrmjaIO.getCrrcd());
		sv.zrsumin.set(covrmjaIO.getSumins());
		/* Life Assured.*/
		lifeclmIO.setRecKeyData(SPACES);
		lifeclmIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lifeclmIO.setChdrnum(covrmjaIO.getChdrnum());
		lifeclmIO.setLife(covrmjaIO.getLife());
		lifeclmIO.setJlife(covrmjaIO.getJlife());
		lifeclmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeclmIO);
		if (isNE(lifeclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifeclmIO.getParams());
			syserrrec.statuz.set(lifeclmIO.getStatuz());
			fatalError600();
		}
		sv.lifenum.set(lifeclmIO.getLifcnum());
		cltsIO.setClntnum(lifeclmIO.getLifcnum());
		/* Get Life Assured name*/
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
	}

protected void readAclh1030()
	{
		aclhIO.setChdrcoy(regpIO.getChdrcoy());
		aclhIO.setChdrnum(regpIO.getChdrnum());
		aclhIO.setLife(regpIO.getLife());
		aclhIO.setCoverage(regpIO.getCoverage());
		aclhIO.setRider(regpIO.getRider());
		aclhIO.setRgpynum(regpIO.getRgpynum());
		aclhIO.setFunction(varcom.readr);
		aclhIO.setFormat(aclhrec);
		SmartFileCode.execute(appVars, aclhIO);
		if (isEQ(aclhIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.readCovr1040);
		}
		if (isNE(aclhIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(aclhIO.getStatuz());
			syserrrec.params.set(aclhIO.getParams());
			fatalError600();
		}
		if (isEQ(aclhIO.getStatuz(),varcom.oK)) {
			sv.incurdt.set(aclhIO.getIncurdt());
			sv.gcadmdt.set(aclhIO.getGcadmdt());
			sv.dischdt.set(aclhIO.getDischdt());
			sv.diagcde.set(aclhIO.getDiagcde());
			sv.zdoctor.set(aclhIO.getZdoctor());
			sv.zmedprv.set(aclhIO.getZmedprv());
			sv.tclmamt.set(aclhIO.getTclmamt());
			/* Get Diagnosis Description*/
			if (isNE(sv.diagcde,SPACES)) {
				wsaaTable = "TR50H";
				wsaaItem.set(sv.diagcde);
				a200TableDesc();
				sv.shortdesc.set(wsaaShrtDesc);
			}
			/* Get Doctor Description*/
			if (isNE(sv.zdoctor,SPACES)) {
				a300DoctorDesc();
			}
			/* Get Provider Description*/
			if (isNE(sv.zmedprv,SPACES)) {
				a400ProvDesc();
			}
		}
	}

protected void readCovr1040()
	{
		a100CallCovr();
		if (isEQ(covrIO.getStatuz(),varcom.mrnf)) {
			covrIO.setRecNonKeyData(SPACES);
		}
		else {
			wsaaItdmkey.itdmItemcoy.set(covrIO.getChdrcoy());
			wsaaTr585Crtable.set(covrIO.getCrtable());
			wsaaItdmkey.itdmItemitem.set(wsaaTr585Itemitem);
			goTo(GotoLabel.loadSubfile1050);
		}
	}

protected void loadSubfile1050()
	{
		/*    Load first page of subfile*/
		wsaaItdmkey.itdmItemtabl.set(tr585);
		itdmIO.setDataArea(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsaaItdmkey.itdmItemcoy);
		itdmIO.setItemtabl(wsaaItdmkey.itdmItemtabl);
		itdmIO.setItemitem(wsaaItdmkey.itdmItemitem);
		itdmIO.setItmfrm(regpIO.getCrtdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsaaItdmkey.itdmItemcoy)
		|| isNE(itdmIO.getItemtabl(),tr585)
		|| isNE(itdmIO.getItemitem(),wsaaItdmkey.itdmItemitem)) {
			itdmIO.setStatuz(varcom.endp);
			blankSubfile1400();
			return ;
		}
		tr585rec.tr585Rec.set(itdmIO.getGenarea());
		loadSubfile1800();
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsaaSlt.set(SPACES);
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void blankSubfile1400()
	{
		/*BEGIN*/
		sv.acdben.set(SPACES);
		sv.bendesc.set(SPACES);
		sv.benfreq.set(SPACES);
		sv.gcdblind.set(SPACES);
		sv.shortdesc.set(SPACES);
		sv.givname.set(SPACES);
		sv.cdesc.set(SPACES);
		sv.amtaout.set(ZERO);
		sv.gcnetpy.set(ZERO);
		sv.mxbenunt.set(ZERO);
		sv.actexp.set(ZERO);
		sv.acbenunt.set(ZERO);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR582", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadSubfile1800()
	{
		/*BEGN*/
		wsaaTotPayable.set(ZERO);
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			loadSubfile1200();
		}

		scrnparams.subfileRrn.set(1);
		sv.tclmamt.set(wsaaTotPayable);
		/*EXIT*/
	}

protected void loadSubfile1200()
	{
		try {
			begn1210();
			contitem1230();
		}
		catch (GOTOException e){
		}
	}

protected void begn1210()
	{
		for (ix.set(1); !(isGT(ix,10)); ix.add(1)){
			if (isNE(tr585rec.acdben[ix.toInt()],SPACES)) {
				readAcld1600();
				sv.sel.set(" ");
				sv.acdben.set(tr585rec.acdben[ix.toInt()]);
				benefitDesc1300();
				sv.bendesc.set(wsaaBenDesclong);
				if (isNE(tr585rec.dfclmpct[ix.toInt()],ZERO)) {
					compute(sv.amtaout, 2).set(div(mult(tr585rec.dfclmpct[ix.toInt()],sv.zrsumin),100));
					sv.benfreq.set(tr585rec.benfreq[ix.toInt()]);
					sv.mxbenunt.set(tr585rec.mxbenunt[ix.toInt()]);
				}
				else {
					sv.amtaout.set(tr585rec.amtfld[ix.toInt()]);
				}
				sv.gcdblind.set(tr585rec.gcdblind[ix.toInt()]);
				wsaaTotPayable.add(sv.gcnetpy);
				/* Update hidden fields*/
				scrnparams.function.set(varcom.sadd);
				processScreen("SR582", sv);
				if (isNE(scrnparams.statuz,varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
		}
	}

protected void contitem1230()
	{
		if (isEQ(tr585rec.contitem,SPACES)) {
			itdmIO.setStatuz(varcom.endp);
			return ;
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsaaItdmkey.itdmItemcoy);
		itdmIO.setItemtabl(wsaaItdmkey.itdmItemtabl);
		itdmIO.setItemitem(tr585rec.contitem);
		itdmIO.setItmfrm(regpIO.getCrtdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsaaItdmkey.itdmItemcoy)
		|| isNE(itdmIO.getItemtabl(),tr585)
		|| isNE(itdmIO.getItemitem(),tr585rec.contitem)) {
			itdmIO.setStatuz(varcom.endp);
		}
		tr585rec.tr585Rec.set(itdmIO.getGenarea());
	}

protected void benefitDesc1300()
	{
			begn1310();
		}

protected void begn1310()
	{
		if (isEQ(tr585rec.acdben[ix.toInt()], " ")) {
			return ;
		}
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tr584);
		descIO.setDescitem(tr585rec.acdben[ix.toInt()]);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaBenDesclong.set(descIO.getLongdesc());
		}
		else {
			wsaaBenDesclong.fill("?");
		}
	}

protected void readAcld1600()
	{
		begn1610();
	}

protected void begn1610()
	{
		acldIO.setChdrcoy(regpIO.getChdrcoy());
		acldIO.setChdrnum(regpIO.getChdrnum());
		acldIO.setLife(regpIO.getLife());
		acldIO.setCoverage(regpIO.getCoverage());
		acldIO.setRider(regpIO.getRider());
		acldIO.setAcdben(tr585rec.acdben[ix.toInt()]);
		acldIO.setRgpynum(regpIO.getRgpynum());
		acldIO.setFunction(varcom.readr);
		acldIO.setFormat(acldrec);
		SmartFileCode.execute(appVars, acldIO);
		if (isNE(acldIO.getStatuz(),varcom.oK)
		&& isNE(acldIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acldIO.getParams());
			syserrrec.statuz.set(acldIO.getStatuz());
			fatalError600();
		}
		if (isEQ(acldIO.getStatuz(),varcom.oK)) {
			sv.actexp.set(acldIO.getActexp());
			sv.acbenunt.set(acldIO.getAcbenunt());
			sv.gcnetpy.set(acldIO.getGcnetpy());
		}
		else {
			sv.actexp.set(ZERO);
			sv.acbenunt.set(ZERO);
			sv.gcnetpy.set(ZERO);
		}
	}

protected void screenInitOptswch1700()
	{
		optswch1700();
	}

protected void optswch1700()
	{
		/* Call OPTSWCH to load OPTDSC*/
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)) {
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")) {
			protectScr2100();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					validateScreen2010();
					checkForErrors2050();
				case validateSubfile2060:
					validateSubfile2060();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsaaScrnStatuz.set(scrnparams.statuz);
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsaaScrnStatuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateScreen2010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.validateSubfile2060);
		}
		/*    Validate fields*/
		/* Get Diagnosis Description*/
		if (isNE(sv.diagcde,SPACES)) {
			wsaaTable = "TR50H";
			wsaaItem.set(sv.diagcde);
			a200TableDesc();
			sv.shortdesc.set(wsaaShrtDesc);
		}
		/* Get Doctor Description*/
		if (isNE(sv.zdoctor,SPACES)) {
			a300DoctorDesc();
		}
		/* Get Provider Description*/
		if (isNE(sv.zmedprv,SPACES)) {
			a400ProvDesc();
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR582", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}

		if (isEQ(wsaaSlt, " ")
		&& isEQ(sv.tclmamt,ZERO)) {
			sv.tclmamtErr.set(g624);
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void protectScr2100()
	{
		/*PROTECT*/
		sv.diagcdeOut[varcom.pr.toInt()].set("Y");
		sv.zdoctorOut[varcom.pr.toInt()].set("Y");
		sv.zmedprvOut[varcom.pr.toInt()].set("Y");
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		validation2610();
		updateErrorIndicators2670();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/*    Validate subfile fields*/
		if (isEQ(sv.sel,"1")
		|| isEQ(sv.sel,"2")
		|| isEQ(sv.sel,"3")
		|| isEQ(sv.sel,"4")
		|| isEQ(sv.sel, " ")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.selErr.set(w121);
		}
		if (isEQ(wsspcomn.flag,"C")
		|| isEQ(wsspcomn.flag,"M")) {
			if (isNE(sv.actexp,ZERO)
			&& isNE(sv.gcnetpy,ZERO)) {
				if (isEQ(sv.sel,"1")) {
					sv.selErr.set(w121);
				}
			}
			if (isEQ(sv.actexp,ZERO)
			&& isEQ(sv.gcnetpy,ZERO)) {
				if (isEQ(sv.sel,"2")
				|| isEQ(sv.sel,"3")) {
					sv.selErr.set(w121);
				}
			}
		}
		if (isEQ(sv.actexp,ZERO)
		&& isEQ(sv.gcnetpy,ZERO)) {
			if (isEQ(sv.sel,"4")) {
				sv.selErr.set(w121);
			}
		}
		if (isEQ(wsspcomn.flag,"I")) {
			if (isEQ(sv.sel,"1")
			|| isEQ(sv.sel,"2")
			|| isEQ(sv.sel,"3")) {
				sv.selErr.set(w121);
			}
		}
		if (isEQ(sv.errorSubfile,SPACES)) {
			if (isNE(sv.sel,SPACES)
			&& isEQ(sv.sel,"1")
			|| isEQ(sv.sel,"2")
			|| isEQ(sv.sel,"3")
			|| isEQ(sv.sel,"4")) {
				wsaaSlt.set(sv.sel);
				wsspdocs.subfileRrn.set(scrnparams.subfileRrn);
				wsspdocs.subfileEnd.set(scrnparams.subfileEnd);
			}
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR582", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SR582", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		/*  Update database files as required*/
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		if (isEQ(aclhIO.getStatuz(),varcom.mrnf)) {
			//IBPLIFE-2838 Unlock the tableDAM 
			aclhIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, aclhIO);
			
			aclhIO.setStatuz(varcom.oK);
			aclhIO.setDataKey(SPACES);
			aclhIO.setFormat(aclhrec);
			aclhIO.setFunction(varcom.writr);
		}
		// MIBT-336 - Condition added to pick correct write function
				else {
					aclhIO.setStatuz(varcom.oK);
					aclhIO.setDataKey(SPACES);
					aclhIO.setFormat(aclhrec);
					aclhIO.setChdrcoy(regpIO.getChdrcoy());
					aclhIO.setChdrnum(regpIO.getChdrnum());
					aclhIO.setLife(regpIO.getLife());
					aclhIO.setCoverage(regpIO.getCoverage());
					aclhIO.setRider(regpIO.getRider());
					aclhIO.setRgpynum(regpIO.getRgpynum());
					aclhIO.setFunction(varcom.readr);
					SmartFileCode.execute(appVars, aclhIO);
					if (isNE(aclhIO.getStatuz(),varcom.oK)) {
						aclhIO.setFunction(varcom.writr);
					}else{
						aclhIO.setFunction(varcom.writd);	
					}
				}
				//Ends
		aclhIO.setChdrcoy(regpIO.getChdrcoy());
		aclhIO.setChdrnum(regpIO.getChdrnum());
		aclhIO.setLife(regpIO.getLife());
		aclhIO.setCoverage(regpIO.getCoverage());
		aclhIO.setRider(regpIO.getRider());
		aclhIO.setCrtable(sv.crtable);
		aclhIO.setRgpynum(regpIO.getRgpynum());
		aclhIO.setRecvdDate(regpIO.getRecvdDate());
		aclhIO.setIncurdt(sv.incurdt);
		aclhIO.setGcadmdt(sv.gcadmdt);
		aclhIO.setDiagcde(sv.diagcde);
		aclhIO.setDischdt(sv.dischdt);
		aclhIO.setZdoctor(sv.zdoctor);
		aclhIO.setZmedprv(sv.zmedprv);
		aclhIO.setTclmamt(sv.tclmamt);
		SmartFileCode.execute(appVars, aclhIO);
		if (isNE(aclhIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aclhIO.getParams());
			syserrrec.statuz.set(aclhIO.getStatuz());
			fatalError600();
		}
		/* keep  ACLH  file ...*/
		aclhIO.setFormat(aclhrec);
		aclhIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, aclhIO);
		if (isNE(aclhIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aclhIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					nextProgram4010();
				case optswch4080:
					optswch4080();
				case exit4090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.programPtr.subtract(1);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(wsaaSlt, " ")) {
			goTo(GotoLabel.optswch4080);
		}
		/* keep  ACLH  file ...*/
		aclhIO.setFormat(aclhrec);
		aclhIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, aclhIO);
		if (isNE(aclhIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aclhIO.getParams());
			fatalError600();
		}
		/* ...if found, "select" the subfile line to switch to PopUp*/
		/* program.*/
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelOptno.set(wsaaSlt);
		optswchrec.optsSelCode.set(SPACES);
	}

protected void optswch4080()
	{
		/* Switch to next program.*/
		wsspcomn.lastActn.set(SPACES);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz,varcom.oK)
		&& isNE(optswchrec.optsStatuz,varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz,varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
		if (isEQ(wsaaSlt, " ")) {
			return ;
		}
		wsaaStoreFlag.set(wsspcomn.flag);
		wsspcomn.flag.set(wsaaSlt);
	}

protected void a100CallCovr()
	{
		a100ReadCovr();
	}

protected void a100ReadCovr()
	{
		covrIO.setRecKeyData(SPACES);
		covrIO.setChdrcoy(regpIO.getChdrcoy());
		covrIO.setChdrnum(regpIO.getChdrnum());
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFunction(varcom.readr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
	}

protected void a200TableDesc()
	{
		a200Begn();
	}

protected void a200Begn()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(wsaaTable);
		descIO.setDescitem(wsaaItem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaShrtDesc.set(descIO.getShortdesc());
		}
		else {
			wsaaShrtDesc.fill("?");
		}
	}

protected void a300DoctorDesc()
	{
			a300Begn();
		}

protected void a300Begn()
	{
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.zdoctor);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		else {
			namadrsrec.namadrsRec.set(SPACES);
			namadrsrec.clntkey.set(cltsIO.getDataKey());
			namadrsrec.language.set(wsspcomn.language);
			namadrsrec.function.set(wsaaPayeeName);
			callProgram(Namadrs.class, namadrsrec.namadrsRec);
			if (isNE(namadrsrec.statuz,varcom.oK)) {
				syserrrec.params.set(cltsIO.getDataKey());
				syserrrec.statuz.set(namadrsrec.statuz);
				fatalError600();
			}
			sv.givname.set(namadrsrec.name);
		}
	}

protected void a400ProvDesc()
	{
		a400Begn();
	}

protected void a400Begn()
	{
		zmpvIO.setDataArea(SPACES);
		zmpvIO.setStatuz(varcom.oK);
		zmpvIO.setZmedcoy(wsspcomn.company);
		zmpvIO.setZmedprv(sv.zmedprv);
		zmpvIO.setFormat(zmpvrec);
		zmpvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, zmpvIO);
		if (isNE(zmpvIO.getStatuz(),varcom.oK)
		&& isNE(zmpvIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(zmpvIO.getStatuz());
			syserrrec.params.set(zmpvIO.getParams());
			fatalError600();
		}
		if (isEQ(zmpvIO.getStatuz(),varcom.oK)) {
			sv.cdesc.set(zmpvIO.getZmednam());
		}
		else {
			sv.cdesc.fill("?");
		}
	}
}
