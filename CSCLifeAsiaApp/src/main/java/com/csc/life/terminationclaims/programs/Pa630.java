/**
 * Pa630 —— Notification Notes from Sa508 hyperlink
 */
package com.csc.life.terminationclaims.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.dao.InvspfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.life.terminationclaims.screens.Sa630ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.smart.recordstructures.Subprogrec;

public class Pa630 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final String CLMPREFIX = "CLMNTF";
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PA630");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-SEC-PROGS */

	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	private Sa630ScreenVars sv = getPScreenVars() ;
	private Clntpf clntpf;
	
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	protected static final String h093 = "H093";
	private Batckey wsaaBatchkey = new Batckey();
	private Subprogrec subprogrec = new Subprogrec();
	//CML023
	private InvspfDAO invspfDAO= getApplicationContext().getBean("invspfDAO",InvspfDAO.class);
	protected static final String chdrenqrec = "CHDRENQREC";
	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator planLevel = new Validator(wsaaPlanproc, "Y");
	protected static final String e186 = "E186";
	private Invspf invspf = new Invspf();
	private String combine = "";
	
	public Pa630() {
		super();
		screenVars = sv;
		new ScreenModel("Sa630", AppVars.getInstance(), sv);
	}

	protected Sa630ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sa630ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			e.printStackTrace();
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		}catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			e.printStackTrace();
		}catch (Exception ex){
			ex.printStackTrace();
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}


protected void initialise1010()
	{
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*      GO TO 1090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		if(isNE(wsspcomn.wsaanotificationNum,SPACES))
			sv.notifinum.set(wsspcomn.wsaanotificationNum.toString());
		else
			sv.notifinum.set(SPACES);	
		getLifeAssSection1020();
	}
	
	private void getLifeAssSection1020() {
		try{
			if (isEQ(wsspcomn.flag,"I") || isEQ(wsspcomn.flag,"M") || isEQ(wsspcomn.flag,"D")) {
				invspf = invspfDAO.getCacheObject(invspf);
				if(invspf ==null){
					syserrrec.params.set(wsspcomn.company.toString().concat(wsspcomn.wsaanotificationNum.toString()));
					fatalError600();
				}
				else{
					sv.accdesc.set(invspf.getInvestigationResult().trim());
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* IF   WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                  */
		/*      MOVE O-K               TO WSSP-EDTERROR                 */
		/*      GO TO 2090-EXIT.                                        */
		if (isEQ(wsspcomn.flag,"I") || isEQ(wsspcomn.flag,"D")) {
			scrnparams.function.set(varcom.prot);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}

protected void screenEdit2000()
	{
	
		try{
			screenIo2010();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
		
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			return;
			
		}
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
			exit2090();
		}
		/*    Catering for F11.                                    <V4L011>*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			exit2090();
		}
		if (isEQ(wsspcomn.flag,"I") || isEQ(wsspcomn.flag,"D")) {
			return ;
		}	
		
	}

protected void exit2090(){
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
	if (isEQ(scrnparams.statuz, varcom.kill)) {
		return;
	}
	if (isEQ(wsspcomn.flag,"I")) {
		return ;
	}
	if(isNE(sv.accdesc, SPACES)){
		if (isEQ(wsspcomn.flag,"M")) {
			updateInvspf3020();
		}
		else if (isEQ(wsspcomn.flag,"D")) {
			invspfDAO.removeInvspfRecord(invspf.getUniqueNumber());
		}
		else{
			createInvspf3010();
		}
	}
		
}

public void createInvspf3010(){
	Invspf invspf = new Invspf();
	invspf.setInvscoy(wsspcomn.company.toString());
	invspf.setNotifinum(sv.notifinum.toString().trim().replace(CLMPREFIX, ""));
	invspf.setClaimno(wsspcomn.wsaaclaimno.toString().trim());
	invspf.setLifenum(wsspcomn.chdrCownnum.toString());
	invspf.setClaimant(wsspcomn.wsaaclaimant.toString());
	invspf.setRelationship(wsspcomn.wsaarelationship.toString());
	invspf.setInvestigationResult(sv.accdesc.toString().trim());
	invspf.setDatime(setupDatime());
	invspfDAO.insertInvspf(invspf);
}
	
public void updateInvspf3020(){
	invspf.setInvestigationResult(sv.accdesc.toString().trim());
	invspf.setDatime(setupDatime());
	int updateInvspf = invspfDAO.updateInvspf(invspf);
}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected String setupDatime(){
	try{
	Date parse=new SimpleDateFormat("MMM dd yyyy HH:mm:ss").parse(sv.datime.substring(4,24));
	combine =new SimpleDateFormat("yyyy-MM-dd").format(parse)+" "+new SimpleDateFormat("HH:mm:ss").format(parse);	
	}
	catch(ParseException e){
		e.printStackTrace();
	}
	return combine;
}
protected void whereNext4000(){
	/*NEXT-PROGRAM*/
	wsspcomn.programPtr.add(1);		
	/*EXIT*/
	}

	
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 
		 private FixedLengthStringData RRNJ = new FixedLengthStringData(4).init("RRNJ");
	}

}


