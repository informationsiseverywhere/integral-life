package com.csc.life.terminationclaims.dataaccess.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;


@Table(name="Covppf")
public class Covppf implements Serializable {
	@Id
	private long unique_number;
	private String chdrpfx;
	private String chdrcoy;
	private String chdrnum;
	private String validflag;
	private Integer effdate;
	private String trcode;
	private String covrprpse;
	private String crtable;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private String coverage;
	private String rider;
	
	public Covppf() {
	}

	public Covppf(long uniqueNumber) {
		this.unique_number = uniqueNumber;
	}

	public Covppf(long uniqueNumber, String chdrpfx, 
			String chdrcoy, String chdrnum, String validflag, Integer effdate,
			String trcode, String covrprpse, String crtable, String usrprf, String jobnm, Date datime,String coverage,String rider) {
		this.unique_number = uniqueNumber;
		this.chdrcoy = chdrcoy;
		this.chdrnum = chdrnum;
		this.validflag = validflag;
		this.chdrpfx = chdrpfx;
		this.effdate = effdate;
		this.trcode = trcode;
		this.covrprpse = covrprpse;
		this.crtable = crtable;
		this.usrprf = usrprf;
		this.jobnm = jobnm;
		this.datime = datime;
		this.coverage = coverage;
		this.rider = rider;
	}

	public long getUniqueNumber() {
		return this.unique_number;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.unique_number = uniqueNumber;
	}

	public String getChdrcoy() {
		return this.chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return this.chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getValidflag() {
		return this.validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public Integer getEffdate() {
		return this.effdate;
	}

	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}

	public String getChdrpfx() {
		return this.chdrpfx;
	}

	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}

	public String getTranCode() {
		return this.trcode;
	}

	public void setTranCode(String trcode) {
		this.trcode = trcode;
	}

	public String getCovrprpse() {
		return this.covrprpse;
	}

	public void setCovrprpse(String covrprpse) {
		this.covrprpse = covrprpse;
	}

	public String getUsrprf() {
		return this.usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return this.jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public Date getDatime() {
		return this.datime;
	}

	public void setDatime(Date datime) {
		this.datime = datime;
	}
	
	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

}
