package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:15
 * Description:
 * Copybook name: PTSDPENKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ptsdpenkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ptsdpenFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ptsdpenKey = new FixedLengthStringData(64).isAPartOf(ptsdpenFileKey, 0, REDEFINE);
  	public FixedLengthStringData ptsdpenChdrcoy = new FixedLengthStringData(1).isAPartOf(ptsdpenKey, 0);
  	public FixedLengthStringData ptsdpenChdrnum = new FixedLengthStringData(8).isAPartOf(ptsdpenKey, 1);
  	public PackedDecimalData ptsdpenTranno = new PackedDecimalData(5, 0).isAPartOf(ptsdpenKey, 9);
  	public PackedDecimalData ptsdpenPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ptsdpenKey, 12);
  	public FixedLengthStringData ptsdpenLife = new FixedLengthStringData(2).isAPartOf(ptsdpenKey, 15);
  	public FixedLengthStringData ptsdpenCoverage = new FixedLengthStringData(2).isAPartOf(ptsdpenKey, 17);
  	public FixedLengthStringData ptsdpenRider = new FixedLengthStringData(2).isAPartOf(ptsdpenKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(ptsdpenKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ptsdpenFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ptsdpenFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}