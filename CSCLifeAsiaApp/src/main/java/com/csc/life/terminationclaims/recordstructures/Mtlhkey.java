package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:47
 * Description:
 * Copybook name: MTLHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mtlhkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mtlhFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mtlhKey = new FixedLengthStringData(64).isAPartOf(mtlhFileKey, 0, REDEFINE);
  	public FixedLengthStringData mtlhChdrcoy = new FixedLengthStringData(1).isAPartOf(mtlhKey, 0);
  	public FixedLengthStringData mtlhChdrnum = new FixedLengthStringData(8).isAPartOf(mtlhKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(mtlhKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mtlhFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mtlhFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}