package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl55screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {22, 23, 2, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl55ScreenVars sv = (Sjl55ScreenVars) pv;
		clearInds(av,sv.getScreenSflPfInds());
		write(lrec, sv.Sjl55screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl55ScreenVars screenVars = (Sjl55ScreenVars)pv;
		ScreenRecord.setClassStringFormatting(pv);
	}

/**
 * Clear all the variables in Sjl55screen
 */
	public static void clear(VarModel pv) {
		Sjl55ScreenVars screenVars = (Sjl55ScreenVars) pv;
		ScreenRecord.clear(pv);
	}
}
