/*
 * File: P5124.java
 * Date: 30 August 2009 0:09:59
 * Author: Quipoz Limited
 *
 * Class transformed from P5124.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.impl.ChdrpfDAOImpl;
import com.csc.fsu.general.dataaccess.dao.impl.MandpfDAOImpl;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.screens.S5124ScreenVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*  P5124 - Regular Payments Bank Details.
*  --------------------------------------
*
*  This program will provide a window in  which  the  user  may
*  enter  details of the bank account through which the payment
*  will be made.
*
*  If the user has selected a  payment  type  whose  rules  say
*  that  Bank  Details are required then the system will window
*  automatically to this program.
*
*  Initialise.
*  -----------
*
*  Retrieve the current payment details from  REGP.  Store  the
*  currency  in  a  hidden screen field so that it is available
*  for further windowing.
*
*  If the current bank number and account number are not  blank
*  display  the  bank  details  from  BABR  and  client account
*  details from CLBL.
*
*  Validation.
*  -----------
*
*  If in enquiry mode, (WSSP-FLAG is 'I'), protect  the  screen
*  prior to output.
*
*  Display the screen with te I/O module.
*
*  If  'KILL'  is  requested  or  in  Enquiry  mode,  skip  the
*  validation.
*
*  Validate the individual fields as follows:
*
*  Check that the bank identification exists on  BABR  and  the
*  combination  of  bank  and account number exist on CLBL. The
*  account currency must match with the payment  currency  from
*  REGP.
*
*  Obtain the bank and account descriptions for confirmation.
*
*  If  'CALC'  was  requested  re-display the screen to display
*  the bank/branch and account descriptions.
*
*  Updating.
*  ---------
*
*  Skip the updating if 'KILL' was pressed  or  if  in  enquiry
*  mode.
*
*  Store  the  bank  branch  code  and client account number in
*  REGP.
*
*  Perform a KEEPS on REGP>
*
*  Where Next.
*  -----------
*
*  Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P5124 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5)
			.init("P5124");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2)
			.init("03");

	protected FixedLengthStringData wsbbBankdesc = new FixedLengthStringData(60);
	protected FixedLengthStringData wsbbBankdescLine1 = new FixedLengthStringData(
			30).isAPartOf(wsbbBankdesc, 0);
	protected FixedLengthStringData wsbbBankdescLine2 = new FixedLengthStringData(
			30).isAPartOf(wsbbBankdesc, 30);
		/* ERRORS */
	private String e186 = "E186";
	private String e756 = "E756";
	private String g600 = "G600";
	private String h130 = "H130";
	private String curs = "CURS";
	private String regprec = "REGPREC";
		/*Bank/Branch Name File*/
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*Regular Payments View.*/


		/*Logical File: Client/Bank Account Record*/
	private ClblTableDAM clblIO = new ClblTableDAM();
		/*Regular Payments File*/

	private RegpTableDAM regpIO = new RegpTableDAM();
	private Wssplife wssplife = new Wssplife();
	private S5124ScreenVars sv = ScreenProgram
			.getScreenVars(S5124ScreenVars.class);


	// CML-010
	boolean CMRPY008Permission = false;

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1090,
		preExit,
		checkForErrors2080,
		exit2090,
		exit3090
	}

	public P5124() {
		super();
		screenVars = sv;
		new ScreenModel("S5124", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		// CML010
		CMRPY008Permission = FeaConfg.isFeatureExist("2", "CMRPY008", appVars,
				"IT");
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		regpIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(regpIO.getParams());
			fatalError600();
		}
		
		// CML-010
		ChdrpfDAO chdrpfDAOImpl = new ChdrpfDAOImpl();
		Chdrpf chdrpf = chdrpfDAOImpl.getchdrRecordData(regpIO.getChdrcoy().getData(), regpIO.getChdrnum().getData());

		MandpfDAO mandpfDAOImpl = new MandpfDAOImpl();
		Mandpf mandpf = mandpfDAOImpl.searchMandpfRecordData(
				wsspcomn.fsuco.getData(),
 chdrpf.getCownnum(), "00001",
				"VM1DTA.MANDLNB");

		// if (isEQ(mandlnbIO.getStatuz(), varcom.mrnf)) {
		// // sv.mandrefErr.set("H929");
		// return;
		// }
		// CML010
		/* Move bank account details to corresponding fields */
		if (CMRPY008Permission && mandpf != null && chdrpf != null) {
			sv.bankkey.set(mandpf.getBankkey());
			sv.bankacckey.set(mandpf.getBankacckey());

			babrIO.setBankkey(sv.bankkey);
			babrIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, babrIO);
			if ((isNE(babrIO.getStatuz(), varcom.oK))
					&& (isNE(babrIO.getStatuz(), varcom.mrnf))) {
				syserrrec.params.set(babrIO.getParams());
				fatalError600();
			}
			sv.bankaccdsc.set(clblIO.getBankaccdsc());
			wsbbBankdesc.set(babrIO.getBankdesc());
			sv.bankdesc.set(wsbbBankdescLine1);
			sv.branchdesc.set(wsbbBankdescLine2);		
		}
		// CML-010

		sv.payrnum.set(regpIO.getPayclt());
		sv.numsel.set(regpIO.getPayclt());
		sv.currcode.set(regpIO.getCurrcd());
		
		if (isEQ(regpIO.getBankkey(),SPACES)
		&& isEQ(regpIO.getBankacckey(),SPACES)) {
			goTo(GotoLabel.exit1090);
		}
		clblIO.setBankkey(regpIO.getBankkey());
		sv.bankkey.set(regpIO.getBankkey());
		clblIO.setBankacckey(regpIO.getBankacckey());
		sv.bankacckey.set(regpIO.getBankacckey());
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if ((isNE(wsspcomn.flag,"I"))
		&& (isNE(wsspcomn.flag,"D"))) {
			if (isNE(clblIO.getClntnum(),sv.payrnum)) {
				sv.bankkey.set(SPACES);
				sv.bankacckey.set(SPACES);
				sv.facthous.set(SPACES);
				goTo(GotoLabel.exit1090);
			}
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)
		&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}

	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if ((isEQ(wsspcomn.flag,"I"))
		|| (isEQ(wsspcomn.flag,"D"))) {
			scrnparams.function.set("PROT");
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if ((isNE(scrnparams.statuz,varcom.kill))
		&& (isNE(scrnparams.statuz,varcom.calc))
		&& (isNE(scrnparams.statuz,varcom.oK))) {
			scrnparams.errorCode.set(curs);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validate2020()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.bankkey,SPACES)) {
			sv.bankkeyErr.set(e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if ((isNE(babrIO.getStatuz(),varcom.oK))
		&& (isNE(babrIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(e756);
			goTo(GotoLabel.checkForErrors2080);
		}
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		if (isEQ(sv.bankacckey,SPACES)) {
			sv.bankacckeyErr.set(e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum); //IBPLIFE-14416
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if ((isNE(clblIO.getStatuz(),varcom.oK))
		&& (isNE(clblIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isEQ(clblIO.getStatuz(),varcom.mrnf)) {
			sv.bankacckeyErr.set(g600);
			goTo(GotoLabel.checkForErrors2080);
		}

		if (isNE(regpIO.getCurrcd(),clblIO.getCurrcode())) {
			sv.bankacckeyErr.set(h130);
			goTo(GotoLabel.checkForErrors2080);
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		regpIO.setBankkey(sv.bankkey);
		regpIO.setBankacckey(sv.bankacckey);
		regpIO.setFormat(regprec);
		regpIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(),varcom.oK)) {
			regpIO.setParams(regpIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}