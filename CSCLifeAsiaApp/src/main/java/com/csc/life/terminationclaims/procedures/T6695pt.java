/*
 * File: T6695pt.java
 * Date: 30 August 2009 2:30:44
 * Author: Quipoz Limited
 * 
 * Class transformed from T6695PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.terminationclaims.tablestructures.T6695rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6695.
*
*
*****************************************************************
* </pre>
*/
public class T6695pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6695rec t6695rec = new T6695rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6695pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6695rec.t6695Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(t6695rec.tupyr01);
		generalCopyLinesInner.fieldNo008.set(t6695rec.pctinc01);
		generalCopyLinesInner.fieldNo009.set(t6695rec.tupyr02);
		generalCopyLinesInner.fieldNo010.set(t6695rec.pctinc02);
		generalCopyLinesInner.fieldNo011.set(t6695rec.tupyr03);
		generalCopyLinesInner.fieldNo012.set(t6695rec.pctinc03);
		generalCopyLinesInner.fieldNo013.set(t6695rec.tupyr04);
		generalCopyLinesInner.fieldNo014.set(t6695rec.pctinc04);
		generalCopyLinesInner.fieldNo015.set(t6695rec.tupyr05);
		generalCopyLinesInner.fieldNo016.set(t6695rec.pctinc05);
		generalCopyLinesInner.fieldNo017.set(t6695rec.tupyr06);
		generalCopyLinesInner.fieldNo018.set(t6695rec.pctinc06);
		generalCopyLinesInner.fieldNo019.set(t6695rec.tupyr07);
		generalCopyLinesInner.fieldNo020.set(t6695rec.pctinc07);
		generalCopyLinesInner.fieldNo021.set(t6695rec.tupyr08);
		generalCopyLinesInner.fieldNo022.set(t6695rec.pctinc08);
		generalCopyLinesInner.fieldNo023.set(t6695rec.tupyr09);
		generalCopyLinesInner.fieldNo024.set(t6695rec.pctinc09);
		generalCopyLinesInner.fieldNo025.set(t6695rec.tupyr10);
		generalCopyLinesInner.fieldNo026.set(t6695rec.pctinc10);
		generalCopyLinesInner.fieldNo027.set(t6695rec.tupyr11);
		generalCopyLinesInner.fieldNo028.set(t6695rec.pctinc11);
		generalCopyLinesInner.fieldNo029.set(t6695rec.tupyr12);
		generalCopyLinesInner.fieldNo030.set(t6695rec.pctinc12);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 27, FILLER).init("Regular Claim Indexation Rules              S6695");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(59);
	private FixedLengthStringData filler9 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(44).isAPartOf(wsaaPrtLine004, 15, FILLER).init("Increase benefit by the following parameters");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(50);
	private FixedLengthStringData filler11 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine005, 25, FILLER).init("Term           Percentage");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(49);
	private FixedLengthStringData filler13 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine006, 23, FILLER).init("Up to year        Increase");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(48);
	private FixedLengthStringData filler15 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 26);
	private FixedLengthStringData filler16 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(48);
	private FixedLengthStringData filler17 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 26);
	private FixedLengthStringData filler18 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine008, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(48);
	private FixedLengthStringData filler19 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 26);
	private FixedLengthStringData filler20 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(48);
	private FixedLengthStringData filler21 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 26);
	private FixedLengthStringData filler22 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine010, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(48);
	private FixedLengthStringData filler23 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 26);
	private FixedLengthStringData filler24 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(48);
	private FixedLengthStringData filler25 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 26);
	private FixedLengthStringData filler26 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine012, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(48);
	private FixedLengthStringData filler27 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 26);
	private FixedLengthStringData filler28 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine013, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(48);
	private FixedLengthStringData filler29 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 26);
	private FixedLengthStringData filler30 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine014, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(48);
	private FixedLengthStringData filler31 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 26);
	private FixedLengthStringData filler32 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine015, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(48);
	private FixedLengthStringData filler33 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 26);
	private FixedLengthStringData filler34 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine016, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(48);
	private FixedLengthStringData filler35 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 26);
	private FixedLengthStringData filler36 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine017, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 42).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(48);
	private FixedLengthStringData filler37 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine018, 26);
	private FixedLengthStringData filler38 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine018, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine018, 42).setPattern("ZZZ.ZZ");
}
}
