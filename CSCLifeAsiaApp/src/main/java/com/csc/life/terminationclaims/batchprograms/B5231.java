/*
 * File: B5231.java
 * Date: 29 August 2009 21:01:27
 * Author: Quipoz Limited
 * 
 * Class transformed from B5231.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.annuities.dataaccess.VstlTableDAM;
import com.csc.life.annuities.dataaccess.VstldelTableDAM;
import com.csc.life.annuities.procedures.Vpxvest;
import com.csc.life.annuities.recordstructures.Vpxvestrec;
import com.csc.life.annuities.recordstructures.Vstarec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrmatTableDAM;
import com.csc.life.terminationclaims.recordstructures.P5231par;
import com.csc.life.terminationclaims.reports.R5231Report;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*
* OVERVIEW
* ========
*
* This  is  a  new  batch  program  which forms part of the 9405
* Annuities Development.  It  will  be  run  as  part  of  batch
* schedule  PENDVEST,  which  can  be  run as and when required.
* This will probably form part of the regular  batch  processing
* but is unlikely to be run on a daily basis.
* It  will select any component with a risk cessation date which
* falls between  the  dates  entered  on  the  parameter  prompt
* screen,  S5231.    The  only restriction on the dates input on
* this screen is that the 'To date' is greater  than  the  'From
* date'.
*
* Annuity  components  will be identified by the presence of the
* coverage code on T6625.
*
* VSTLPF.  One record will be written for each CONTRACT that has
* at least one  component  due  to  vest  between  the  dates
* specified.    This  record  will  hold the information for all
* components,   including   the   anticipated   value   of   the
* component(s) at vesting.
*
* In  order to calculate the amounts to be put onto this file, a
* calculation subroutine needs to be called.    To  access  this
* subroutine  firstly, T5687 - Component details table - is read
* with the coverage code in order to find the  Maturity  method.
* The  calculation  methods table, T6598, is then read with this
* maturity method to obtain the calculation subroutine.
*
* Once the Vesting Letter file has been  updated  completely  a
* Letter  Trigger  record,  LETCPF, will be written.  The letter
* type to write is held on the Automatic Letters  table,  T6634.
* The  item  key  to  this  table  is  the CONTRACT type and the
* transaction code for the batch job.  If  there  is  no  letter
* type  on  the extra data screen for this table, do not write a
* LETC record.
*
* A report will also be produced which will detail the component
* due to vest within the  date  range  input  on  the  parameter
* prompt screen.
*
* The  following control totals will be maintained by this batch
* program:
*
*      Number    Description
*      ======    ===========
*       01       Number of records selected
*       02       Number of VSTL records written
*       03       Number of LETC records written
*       04       Number of VSTL records overwritten
*       05       Number of unprocessed records
*
*****************************************************************
* </pre>
*/
public class B5231 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlcovrpf1rs = null;
	private java.sql.PreparedStatement sqlcovrpf1ps = null;
	private java.sql.Connection sqlcovrpf1conn = null;
	private String sqlcovrpf1 = "";
	private R5231Report printerFile = new R5231Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5231");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaIntDatecfrom = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaIntDatecto = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaChdrnumfrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumto = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaSqlChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaLastContract = new FixedLengthStringData(8);
	private String wsaaSameLetter = "";
	private PackedDecimalData wsaaCount = new PackedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).setUnsigned();
	private String wsaaVestingAllow = "";
	private String wsaaPremiumOk = "";
	private String wsaaStatusOk = "";
	private ZonedDecimalData wsaaLowerDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaUpperDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaEvstperd = new ZonedDecimalData(3, 0);
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t5687 = "T5687";
	private static final String t5679 = "T5679";
	private static final String t6598 = "T6598";
	private static final String tr384 = "TR384";
	private static final String t6625 = "T6625";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/* Main, standard page headings*/
	private FixedLengthStringData r5231H01 = new FixedLengthStringData(93);
	private FixedLengthStringData r5231h01O = new FixedLengthStringData(93).isAPartOf(r5231H01, 0);
	private FixedLengthStringData rh01Datecfrom = new FixedLengthStringData(10).isAPartOf(r5231h01O, 0);
	private FixedLengthStringData rh01Datecto = new FixedLengthStringData(10).isAPartOf(r5231h01O, 10);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r5231h01O, 20);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r5231h01O, 21);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(r5231h01O, 51);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(r5231h01O, 61);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(r5231h01O, 63);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrmatTableDAM chdrmatIO = new ChdrmatTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private VstlTableDAM vstlIO = new VstlTableDAM();
	private VstldelTableDAM vstldelIO = new VstldelTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T5687rec t5687rec = new T5687rec();
	private Vstarec vstarec = new Vstarec();
	private P5231par p5231par = new P5231par();
	private T5679rec t5679rec = new T5679rec();
	private T6598rec t6598rec = new T6598rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T6625rec t6625rec = new T6625rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private FormatsInner formatsInner = new FormatsInner();
	private R5231D01Inner r5231D01Inner = new R5231D01Inner();
	private SqlCovrpfInner sqlCovrpfInner = new SqlCovrpfInner();
	private ExternalisedRules er = new ExternalisedRules();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2060, 
		exit2090
	}

	public B5231() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsaaBatckey.batcKey.set(batcdorrec.batchkey);
		wsaaSameLetter = "N";
		wsaaCount.set(1);
		wsaaBatckey.batcBatctrcde.set(bprdIO.getAuthCode());
		printerFile.openOutput();
		p5231par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(bsprIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Branch.set(bsprIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
		datcon1rec.intDate.set(p5231par.datefrm);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Datecfrom.set(datcon1rec.extDate);
		datcon1rec.intDate.set(p5231par.dateto);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Datecto.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaIntDatecfrom.set(p5231par.datefrm);
		wsaaIntDatecto.set(p5231par.dateto);
		wsaaChdrnumfrm.set(p5231par.chdrnum);
		wsaaChdrnumto.set(p5231par.chdrnum1);
		wsaaSqlChdrcoy.set(bsprIO.getCompany());
		if (isEQ(p5231par.chdrnum,SPACES)) {
			wsaaChdrnumfrm.set(ZERO);
		}
		if (isEQ(p5231par.chdrnum1,SPACES)) {
			wsaaChdrnumto.set("99999999");
		}
		sqlcovrpf1 = " SELECT  CHDRCOY, CHDRNUM, LIFE, JLIFE, COVERAGE, RIDER, PLNSFX, STATCODE, PSTATCODE, CRRCD, PRMCUR, CRTABLE, RCESDTE, SUMINS, INSTPREM, SINGP, SICURR, CBCVIN" +
" FROM   " + getAppVars().getTableNameOverriden("COVRPF") + " " +
" WHERE CHDRCOY = ?" +
" AND RCESDTE >= ?" +
" AND RCESDTE <= ?" +
" AND CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" AND VALIDFLAG = '1'" +
" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX";
		/* Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlcovrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.CovrpfTableDAM());
			sqlcovrpf1ps = getAppVars().prepareStatementEmbeded(sqlcovrpf1conn, sqlcovrpf1, "COVRPF");
			getAppVars().setDBString(sqlcovrpf1ps, 1, wsaaSqlChdrcoy);
			getAppVars().setDBNumber(sqlcovrpf1ps, 2, wsaaIntDatecfrom);
			getAppVars().setDBNumber(sqlcovrpf1ps, 3, wsaaIntDatecto);
			getAppVars().setDBString(sqlcovrpf1ps, 4, wsaaChdrnumfrm);
			getAppVars().setDBString(sqlcovrpf1ps, 5, wsaaChdrnumto);
			sqlcovrpf1rs = getAppVars().executeQuery(sqlcovrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case eof2060: 
					eof2060();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		/*  Call the I/O module or do a Standard COBOL read on*/
		/*  the primary file.*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlcovrpf1rs)) {
				getAppVars().getDBObject(sqlcovrpf1rs, 1, sqlCovrpfInner.sqlChdrcoy);
				getAppVars().getDBObject(sqlcovrpf1rs, 2, sqlCovrpfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlcovrpf1rs, 3, sqlCovrpfInner.sqlLife);
				getAppVars().getDBObject(sqlcovrpf1rs, 4, sqlCovrpfInner.sqlJlife);
				getAppVars().getDBObject(sqlcovrpf1rs, 5, sqlCovrpfInner.sqlCoverage);
				getAppVars().getDBObject(sqlcovrpf1rs, 6, sqlCovrpfInner.sqlRider);
				getAppVars().getDBObject(sqlcovrpf1rs, 7, sqlCovrpfInner.sqlPlnsfx);
				getAppVars().getDBObject(sqlcovrpf1rs, 8, sqlCovrpfInner.sqlStatcode);
				getAppVars().getDBObject(sqlcovrpf1rs, 9, sqlCovrpfInner.sqlPstatcode);
				getAppVars().getDBObject(sqlcovrpf1rs, 10, sqlCovrpfInner.sqlCrrcd);
				getAppVars().getDBObject(sqlcovrpf1rs, 11, sqlCovrpfInner.sqlPrmcur);
				getAppVars().getDBObject(sqlcovrpf1rs, 12, sqlCovrpfInner.sqlCrtable);
				getAppVars().getDBObject(sqlcovrpf1rs, 13, sqlCovrpfInner.sqlRcesdte);
				getAppVars().getDBObject(sqlcovrpf1rs, 14, sqlCovrpfInner.sqlSumins);
				getAppVars().getDBObject(sqlcovrpf1rs, 15, sqlCovrpfInner.sqlInstprem);
				getAppVars().getDBObject(sqlcovrpf1rs, 16, sqlCovrpfInner.sqlSingp);
				getAppVars().getDBObject(sqlcovrpf1rs, 17, sqlCovrpfInner.sqlSicurr);
			}
			else {
				goTo(GotoLabel.eof2060);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		goTo(GotoLabel.exit2090);
	}

protected void eof2060()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
			editIt2500();
		}

protected void editIt2500()
	{
		wsspEdterror.set(varcom.oK);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(sqlCovrpfInner.sqlCrtable);
		itdmIO.setItmfrm(bsscIO.getEffectiveDate());
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t6625)
		|| isNE(itdmIO.getItemitem(), sqlCovrpfInner.sqlCrtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(SPACES);
			return ;
		}
		else {
			t6625rec.t6625Rec.set(itdmIO.getGenarea());
		}
		/* A further check is performed to ensure that the Status*/
		/* is allowed. i.e. it is checked against the statii on T5679*/
		/* and if not found WSSP-EDTERROR is set to SPACES and the*/
		/* SECTION is exited:*/
		wsaaVestingAllow = "N";
		wsaaPremiumOk = "N";
		wsaaStatusOk = "N";
		for (wsaaSub.set(1); !(isEQ(wsaaVestingAllow,"Y")
		|| isGT(wsaaSub,12)); wsaaSub.add(1)){
			checkStatus2510();
		}
		if (isNE(wsaaVestingAllow,"Y")) {
			wsspEdterror.set(SPACES);
			return ;
		}
		/* A last check is required before CT01 can be incremented*/
		/* to whether the effective date falls between a range of*/
		/* dates specified on T6625.*/
		/* Set lower date range from lead years, moving the returned*/
		/* value into WSAA-LOWER-DATE.*/
		compute(wsaaEvstperd, 0).set(mult(t6625rec.evstperd,-1));
		datcon2rec.intDate1.set(sqlCovrpfInner.sqlRcesdte);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaEvstperd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaLowerDate.set(datcon2rec.intDatex2);
		/* Set upper date range from lag years, moving the returned*/
		/* value into WSAA-UPPER-DATE.*/
		datcon2rec.intDate1.set(sqlCovrpfInner.sqlRcesdte);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(t6625rec.lvstperd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		wsaaUpperDate.set(datcon2rec.intDatex2);
		if (isLT(bsscIO.getEffectiveDate(),wsaaLowerDate)
		|| isGT(bsscIO.getEffectiveDate(),wsaaUpperDate)) {
			wsspEdterror.set(SPACES);
			return ;
		}
		else {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
		}
		/* The following lines check if there is a new coverage*/
		/* this is important as a new letter is required if it*/
		/* changes.*/
		/* If it is NOT = to LAST-CONTRACT:*/
		/* Read the Contract header to obtain the contract owner,*/
		/* contract currency & contract type. (This must be done at*/
		/* this point as CHDR info is needed in VSTA-VESTING-REC)*/
		/* This section also checks for duplicates on VSTL.*/
		if (isEQ(sqlCovrpfInner.sqlChdrnum, wsaaLastContract)) {
			wsaaSameLetter = "Y";
		}
		else {
			newLetterSetup2520();
		}
		calculateVesting2540();
	}

protected void checkStatus2510()
	{
		/*START*/
		/* Check the risk status:*/
		if (isEQ(sqlCovrpfInner.sqlStatcode, t5679rec.covRiskStat[wsaaSub.toInt()])) {
			wsaaStatusOk = "Y";
		}
		/* Check the premium status:*/
		if (isEQ(sqlCovrpfInner.sqlPstatcode, t5679rec.covPremStat[wsaaSub.toInt()])) {
			wsaaPremiumOk = "Y";
		}
		/* Check that both are ok:*/
		if (isEQ(wsaaStatusOk,"Y")
		&& isEQ(wsaaPremiumOk,"Y")) {
			wsaaVestingAllow = "Y";
		}
		/*EXIT*/
	}

protected void newLetterSetup2520()
	{
		new2520();
	}

protected void new2520()
	{
		wsaaLastContract.set(sqlCovrpfInner.sqlChdrnum);
		wsaaSameLetter = "N";
		chdrmatIO.setParams(SPACES);
		chdrmatIO.setChdrcoy(sqlCovrpfInner.sqlChdrcoy);
		chdrmatIO.setChdrnum(sqlCovrpfInner.sqlChdrnum);
		chdrmatIO.setFormat(formatsInner.chdrmatrec);
		chdrmatIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmatIO);
		if (isNE(chdrmatIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmatIO.getParams());
			fatalError600();
		}
		/* The next set of lines calls VSTLIO to check if data*/
		/* exists already.  Delete any existing VSTL records for*/
		/* this contract and start fresh.*/
		vstlIO.setChdrcoy(sqlCovrpfInner.sqlChdrcoy);
		vstlIO.setChdrnum(sqlCovrpfInner.sqlChdrnum);
		vstlIO.setFunction(varcom.readr);
		vstlIO.setFormat(formatsInner.vstlrec);
		SmartFileCode.execute(appVars, vstlIO);
		if (isNE(vstlIO.getStatuz(),varcom.oK)
		&& isNE(vstlIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(vstlIO.getParams());
			fatalError600();
		}
		/* Delete the VSTL records found.*/
		if (isEQ(vstlIO.getStatuz(),varcom.oK)) {
			vstldelIO.setChdrcoy(sqlCovrpfInner.sqlChdrcoy);
			vstldelIO.setChdrnum(sqlCovrpfInner.sqlChdrnum);
			vstldelIO.setFunction(varcom.begnh);
			vstldelIO.setFormat(formatsInner.vstldelrec);
			SmartFileCode.execute(appVars, vstldelIO);
			if (isNE(vstldelIO.getStatuz(),varcom.oK)
			&& isNE(vstldelIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(vstlIO.getParams());
				fatalError600();
			}
			if (isNE(vstldelIO.getChdrcoy(), sqlCovrpfInner.sqlChdrcoy)
			&& isNE(vstldelIO.getChdrnum(), sqlCovrpfInner.sqlChdrnum)) {
				vstldelIO.setStatuz(varcom.endp);
			}
			while ( !(isEQ(vstldelIO.getStatuz(),varcom.endp))) {
				deleteOldVstls2530();
			}
			
		}
	}

protected void deleteOldVstls2530()
	{
		start2531();
	}

protected void start2531()
	{
		vstldelIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, vstldelIO);
		if (isNE(vstldelIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vstldelIO.getParams());
			fatalError600();
		}
		else {
			contotrec.totno.set(ct04);
			contotrec.totval.set(1);
			callContot001();
		}
		vstldelIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, vstldelIO);
		if (isNE(vstldelIO.getStatuz(),varcom.oK)
		&& isNE(vstldelIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(vstldelIO.getParams());
			fatalError600();
		}
		if (isNE(vstldelIO.getChdrcoy(), sqlCovrpfInner.sqlChdrcoy)
		&& isNE(vstldelIO.getChdrnum(), sqlCovrpfInner.sqlChdrnum)) {
			vstldelIO.setStatuz(varcom.endp);
		}
	}

protected void calculateVesting2540()
	{
			start2540();
		}

protected void start2540()
	{
		/* Table T5687 is read to obtain maturity method:*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(sqlCovrpfInner.sqlCrtable);
		itdmIO.setItmfrm(bsscIO.getEffectiveDate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(), sqlCovrpfInner.sqlCrtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(SPACES);
			contotrec.totno.set(ct05);
			contotrec.totval.set(1);
			callContot001();
			return ;
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		/* Table T6598 is consulted to check for any calculation*/
		/* routines:*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.maturityCalcMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
			callT65982550();
		}
		else {
			wsspEdterror.set(SPACES);
			contotrec.totno.set(ct05);
			contotrec.totval.set(1);
			callContot001();
		}
	}

protected void callT65982550()
	{
		starts2550();
	}

protected void starts2550()
	{
		/* This section calls the maturity calculation routine used*/
		/* by the COVR.*/
		vstarec.vestingRec.set(SPACES);
		vstarec.batckey.set(wsaaBatckey);
		vstarec.chdrChdrcoy.set(sqlCovrpfInner.sqlChdrcoy);
		vstarec.chdrChdrnum.set(sqlCovrpfInner.sqlChdrnum);
		vstarec.planSuffix.set(sqlCovrpfInner.sqlPlnsfx);
		vstarec.lifeLife.set(sqlCovrpfInner.sqlLife);
		vstarec.covrCoverage.set(sqlCovrpfInner.sqlCoverage);
		vstarec.covrRider.set(sqlCovrpfInner.sqlRider);
		vstarec.crtable.set(sqlCovrpfInner.sqlCrtable);
		vstarec.crrcd.set(sqlCovrpfInner.sqlCrrcd);
		vstarec.effdate.set(bsscIO.getEffectiveDate());
		vstarec.chdrCurr.set(chdrmatIO.getCntcurr());
		vstarec.chdrType.set(chdrmatIO.getCnttype());
		vstarec.currcode.set(sqlCovrpfInner.sqlPrmcur);
		vstarec.matCalcMeth.set(t5687rec.maturityCalcMeth);
		vstarec.pstatcode.set(sqlCovrpfInner.sqlPstatcode);
		vstarec.planSwitch.set(1);
		vstarec.status.set(varcom.oK);
		vstarec.language.set(bsscIO.getLanguage());
		/* A Check is done on PLAN-SUFFIX, This is used in*/
		/* T6598-CALCPROG:*/
		if (isNE(sqlCovrpfInner.sqlPlnsfx, 0)) {
			if (isGT(sqlCovrpfInner.sqlPlnsfx, chdrmatIO.getPolsum())) {
				vstarec.planSwitch.set(2);
			}
			else {
				vstarec.planSuffix.set(ZERO);
				vstarec.planSwitch.set(3);
			}
		}
		else {
			vstarec.planSwitch.set(1);
		}
		/* The calculation subroutine defined on T6598 can now be*/
		/* called:*/
		vstarec.status.set(varcom.oK);
		/* IVE-914 LIFE Annuties DAN Product - Vesting Calculation Method VEST(VSTANNC) 
		 * Start * */
		//callProgram(t6598rec.calcprog, vstarec.vestingRec);
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal(t6598rec.calcprog.toString())))) 
		{
			callProgram(t6598rec.calcprog, vstarec.vestingRec);
		}	
		else
		{			
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(vstarec.vestingRec);
			Vpxvestrec vpxvestrec=new Vpxvestrec();
			vpxvestrec.function.set("INIT");
			vstarec.actualVal01.set(ZERO);
			vstarec.actualVal02.set(ZERO);
			vstarec.actualVal03.set(ZERO);
			vstarec.actualVal04.set(ZERO);
			vstarec.actualVal05.set(ZERO);
			vstarec.actualVal06.set(ZERO);
            //ILIFE-4835 start
			vstarec.actvalTotal.set(ZERO);
		    //ILIFE-4835 end
			callProgram(Vpxvest.class, vpmcalcrec.vpmcalcRec,vpxvestrec);
			callProgram(t6598rec.calcprog, vstarec.vestingRec,vpxvestrec);
			
			vstarec.type01.set("S");
			vstarec.type02.set("B");
			vstarec.type03.set("T");
			vstarec.type04.set("X");
			vstarec.type05.set("M");

		}
		/* IVE-914  End * */
		if (isNE(vstarec.status,varcom.oK)) {
			syserrrec.statuz.set(vstarec.status);
			fatalError600();
		}
		/*  MOVE VSTA-ACTUAL-VAL        TO ZRDP-AMOUNT-IN.              */
		/*  PERFORM A000-CALL-ROUNDING.                                 */
		/*  MOVE ZRDP-AMOUNT-OUT        TO VSTA-ACTUAL-VAL.             */
		/*  IF VSTA-ACTUAL-VAL          NOT = 0                  <S19FIX>*/
		/*     MOVE VSTA-ACTUAL-VAL     TO ZRDP-AMOUNT-IN        <S19FIX>*/
		/*     PERFORM A000-CALL-ROUNDING                        <S19FIX>*/
		/*     MOVE ZRDP-AMOUNT-OUT     TO VSTA-ACTUAL-VAL       <S19FIX>*/
		/*  END-IF.                                              <S19FIX>*/
		if (isNE(vstarec.actualVal[1], 0)) {
			zrdecplrec.amountIn.set(vstarec.actualVal[1]);
			a000CallRounding();
			vstarec.actualVal[1].set(zrdecplrec.amountOut);
		}
		/*  MOVE VSTA-ACTVAL-TOTAL      TO ZRDP-AMOUNT-IN.              */
		/*  PERFORM A000-CALL-ROUNDING.                                 */
		/*  MOVE ZRDP-AMOUNT-OUT        TO VSTA-ACTVAL-TOTAL.           */
		if (isNE(vstarec.actvalTotal, 0)) {
			zrdecplrec.amountIn.set(vstarec.actvalTotal);
			a000CallRounding();
			vstarec.actvalTotal.set(zrdecplrec.amountOut);
		}
	}

protected void update3000()
	{
		updates3000();
	}

protected void updates3000()
	{
		/* Write report line for the coverage and then....*/
		/*   convert date into acceptable format:*/
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sqlCovrpfInner.sqlRcesdte);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		r5231D01Inner.rd01Rcesdte.set(datcon1rec.extDate);
		/* The program needs to get the description from table*/
		/* T6625:*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t6625);
		descIO.setDescitem(sqlCovrpfInner.sqlCrtable);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		r5231D01Inner.rd01Descrip.set(descIO.getLongdesc());
		r5231D01Inner.rd01Chdrnum.set(vstarec.chdrChdrnum);
		r5231D01Inner.rd01Coverage.set(vstarec.covrCoverage);
		r5231D01Inner.rd01Crtable.set(vstarec.crtable);
		r5231D01Inner.rd01Sumin.set(ZERO);
		r5231D01Inner.rd01Bonuses.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub,6)
		|| isEQ(vstarec.type[wsaaSub.toInt()],SPACES)); wsaaSub.add(1)){
			if (isEQ(vstarec.type[wsaaSub.toInt()],"S")) {
				r5231D01Inner.rd01Sumin.set(vstarec.actualVal[wsaaSub.toInt()]);
			}
			else {
				r5231D01Inner.rd01Bonuses.add(vstarec.actualVal[wsaaSub.toInt()]);
			}
		}
		r5231D01Inner.rd01Estimated.set(vstarec.actvalTotal);
		r5231D01Inner.rd01Currency.set(vstarec.currcode);
		if (newPageReq.isTrue()) {
			printerFile.printR5231h01(r5231H01, indicArea);
			wsaaOverflow.set("N");
		}
		printerFile.printR5231d01(r5231D01Inner.r5231D01, indicArea);
		/* A check is required to see whether to add the details*/
		/* to a new letter, or the current one.*/
		/* A new letter is required if a new COVR is found, or when*/
		/* 6 or more details have been written:*/
		if (isEQ(wsaaSameLetter,"Y")
		&& isLT(wsaaCount,5)) {
			sameLetter3100();
		}
		else {
			newLetter3200();
		}
	}

protected void sameLetter3100()
	{
		starts3100();
	}

protected void starts3100()
	{
		wsaaCount.add(1);
		/* Read and hold the letter written previously:*/
		vstlIO.setChdrcoy(chdrmatIO.getChdrcoy());
		vstlIO.setChdrnum(chdrmatIO.getChdrnum());
		vstlIO.setFunction(varcom.readh);
		vstlIO.setFormat(formatsInner.vstlrec);
		SmartFileCode.execute(appVars, vstlIO);
		if (isNE(vstlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vstlIO.getParams());
			fatalError600();
		}
		/* Rewrite the letter with the new details:*/
		vstlIO.setVstamnt(wsaaCount, vstarec.actvalTotal);
		vstlIO.setCrtable(wsaaCount, vstarec.crtable);
		vstlIO.setCurrcd(wsaaCount, vstarec.currcode);
		vstlIO.setDescrip(wsaaCount, descIO.getLongdesc());
		vstlIO.setVstdate(wsaaCount, sqlCovrpfInner.sqlRcesdte);
		vstlIO.setFunction(varcom.rewrt);
		vstlIO.setFormat(formatsInner.vstlrec);
		SmartFileCode.execute(appVars, vstlIO);
		if (isNE(vstlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vstlIO.getParams());
			fatalError600();
		}
	}

protected void newLetter3200()
	{
			starts3200();
		}

protected void starts3200()
	{
		/* A new letter is Initialised and Formatted:*/
		for (wsaaCount.set(1); !(isGT(wsaaCount,5)); wsaaCount.add(1)){
			vstlIO.setVstamnt(wsaaCount, ZERO);
			vstlIO.setVstdate(wsaaCount, ZERO);
			vstlIO.setDescrip(wsaaCount, SPACES);
			vstlIO.setCrtable(wsaaCount, SPACES);
			vstlIO.setCurrcd(wsaaCount, SPACES);
		}
		wsaaCount.set(1);
		/* Firstly call client details:*/
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx(chdrmatIO.getCownpfx());
		cltsIO.setClntcoy(chdrmatIO.getCowncoy());
		cltsIO.setClntnum(chdrmatIO.getCownnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(sqlCovrpfInner.sqlRcesdte);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		vstlIO.setCowncoy(chdrmatIO.getCowncoy());
		vstlIO.setCownnum(chdrmatIO.getCownnum());
		vstlIO.setSurname(cltsIO.getSurname());
		vstlIO.setGivname(cltsIO.getGivname());
		vstlIO.setSalut(cltsIO.getSalutl());
		vstlIO.setInitials(cltsIO.getInitials());
		vstlIO.setCltaddr01(cltsIO.getCltaddr01());
		vstlIO.setCltaddr02(cltsIO.getCltaddr02());
		vstlIO.setCltaddr03(cltsIO.getCltaddr03());
		vstlIO.setCltaddr04(cltsIO.getCltaddr04());
		vstlIO.setCltaddr05(cltsIO.getCltaddr05());
		vstlIO.setCltpcode(cltsIO.getCltpcode());
		/* The first component level details are set up here:*/
		vstlIO.setChdrcoy(chdrmatIO.getChdrcoy());
		vstlIO.setChdrnum(chdrmatIO.getChdrnum());
		vstlIO.setCrtable(wsaaCount, vstarec.crtable);
		vstlIO.setVstamnt(wsaaCount, vstarec.actvalTotal);
		vstlIO.setCurrcd(wsaaCount, vstarec.currcode);
		vstlIO.setDescrip(wsaaCount, descIO.getLongdesc());
		vstlIO.setVstdate(wsaaCount, sqlCovrpfInner.sqlRcesdte);
		vstlIO.setFunction(varcom.writr);
		vstlIO.setFormat(formatsInner.vstlrec);
		SmartFileCode.execute(appVars, vstlIO);
		if (isNE(vstlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(vstlIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		/* Table T6634 is read to find the letter type required:*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		/* MOVE T6634                  TO ITEM-ITEMTABL.                */
		itemIO.setItemtabl(tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmatIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* MOVE ITEM-GENAREA           TO T6634-T6634-REC.              */
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* Do not add any LETRQST's if the LETTER-TYPE is blank:*/
		/* IF T6634-LETTER-TYPE          = SPACES                       */
		if (isEQ(tr384rec.letterType,SPACES)) {
			return ;
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(vstlIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.          */
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(bsscIO.getEffectiveDate());
		letrqstrec.clntcoy.set(vstlIO.getCowncoy());
		letrqstrec.chdrcoy.set(vstlIO.getCowncoy());
		letrqstrec.clntnum.set(vstlIO.getCownnum());
		letrqstrec.chdrnum.set(vstlIO.getChdrnum());
		letrqstrec.otherKeys.set(vstlIO.getChdrnum());
		letrqstrec.branch.set(chdrmatIO.getCntbranch());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*   Close the cursor:*/
		getAppVars().freeDBConnectionIgnoreErr(sqlcovrpf1conn, sqlcovrpf1ps, sqlcovrpf1rs);
		/*  Close any open files:*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.currency.set(chdrmatIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData vstlrec = new FixedLengthStringData(10).init("VSTLREC");
	private FixedLengthStringData vstldelrec = new FixedLengthStringData(10).init("VSTLDELREC");
	private FixedLengthStringData chdrmatrec = new FixedLengthStringData(10).init("CHDRMATREC");
}
/*
 * Class transformed  from Data Structure SQL-COVRPF--INNER
 */
private static final class SqlCovrpfInner { 

		/* SQL-COVRPF */
	private FixedLengthStringData sqlCovrrec = new FixedLengthStringData(71);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlCovrrec, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlCovrrec, 1);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 9);
	private FixedLengthStringData sqlJlife = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 11);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 13);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 15);
	private PackedDecimalData sqlPlnsfx = new PackedDecimalData(4, 0).isAPartOf(sqlCovrrec, 17);
	private FixedLengthStringData sqlStatcode = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 20);
	private FixedLengthStringData sqlPstatcode = new FixedLengthStringData(2).isAPartOf(sqlCovrrec, 22);
	private PackedDecimalData sqlCrrcd = new PackedDecimalData(8, 0).isAPartOf(sqlCovrrec, 24);
	private FixedLengthStringData sqlPrmcur = new FixedLengthStringData(3).isAPartOf(sqlCovrrec, 29);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlCovrrec, 32);
	private PackedDecimalData sqlRcesdte = new PackedDecimalData(8, 0).isAPartOf(sqlCovrrec, 36);
	private PackedDecimalData sqlSumins = new PackedDecimalData(17, 2).isAPartOf(sqlCovrrec, 41);
	private PackedDecimalData sqlInstprem = new PackedDecimalData(17, 2).isAPartOf(sqlCovrrec, 50);
	private PackedDecimalData sqlSingp = new PackedDecimalData(17, 2).isAPartOf(sqlCovrrec, 59);
	private FixedLengthStringData sqlSicurr = new FixedLengthStringData(3).isAPartOf(sqlCovrrec, 68);
}
/*
 * Class transformed  from Data Structure R5231-D01--INNER
 */
private static final class R5231D01Inner { 

		/*Detail line - add as many detail and total lines as required.
		           - use redefines to save WS space where applicable.*/
	private FixedLengthStringData r5231D01 = new FixedLengthStringData(110);
	private FixedLengthStringData r5231d01O = new FixedLengthStringData(110).isAPartOf(r5231D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r5231d01O, 0);
	private FixedLengthStringData rd01Life = new FixedLengthStringData(2).isAPartOf(r5231d01O, 8);
	private FixedLengthStringData rd01Coverage = new FixedLengthStringData(2).isAPartOf(r5231d01O, 10);
	private FixedLengthStringData rd01Rider = new FixedLengthStringData(2).isAPartOf(r5231d01O, 12);
	private FixedLengthStringData rd01Crtable = new FixedLengthStringData(4).isAPartOf(r5231d01O, 14);
	private FixedLengthStringData rd01Descrip = new FixedLengthStringData(30).isAPartOf(r5231d01O, 18);
	private ZonedDecimalData rd01Sumin = new ZonedDecimalData(15, 0).isAPartOf(r5231d01O, 48);
	private FixedLengthStringData rd01Rcesdte = new FixedLengthStringData(10).isAPartOf(r5231d01O, 63);
	private FixedLengthStringData rd01Currency = new FixedLengthStringData(3).isAPartOf(r5231d01O, 73);
	private ZonedDecimalData rd01Bonuses = new ZonedDecimalData(17, 2).isAPartOf(r5231d01O, 76);
	private ZonedDecimalData rd01Estimated = new ZonedDecimalData(17, 2).isAPartOf(r5231d01O, 93);
}
}
