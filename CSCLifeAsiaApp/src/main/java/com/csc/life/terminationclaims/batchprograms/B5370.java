/*
 * File: B5370.java
 * Date: 29 August 2009 21:15:36
 * Author: Quipoz Limited
 * 
 * Class transformed from B5370.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.terminationclaims.dataaccess.RegrTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.RegrDAO;
import com.csc.life.terminationclaims.dataaccess.model.Regrpf;
import com.csc.life.terminationclaims.reports.R5370Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*                Regular Payment Reporting
*                =========================
*
*   This program(B5370) is incorporated within a Multi-Threading
*   Batch. B5370 is scheduled to run 3 times after the following
*   programs :-     ^1] B5365 : Payments
*                   ^2] B5367 : Review
*                   ^3] B5369 : Terminate
*   B5370 can produce all of the above 3 reports, simply reading
*   REGRPF (written to in the above 3 progs) & producing the relevent
*   report. The type of report is dependent on parameter03, which
*   can have the following possible values :-  ^1] PAYMENT
*   (which MUST be in uppercase)               ^2] REVIEW
*                                              ^3] TERMINATED
*   Parameter02 contains the progname which is running before B5370.
*   (ie. B5365, B5367 or B5369)
*   Once the progname is different from parameter02, processing
*   will stop.
*
*   *NOTE*
*          Due to the logical key, the records are in the order of
*          non-exception records followed by exception records.
*
*   Control Totals
*   --------------
*     CT01  :  Number of records read
*     CT02  :  Number of Records printed
*     CT03  :  Number of detail reords printed
*     CT04  :  Number of exception records printed
*     Rules :  (1 = 2) & (2 = 3 + 4)
*
*   1000-Section
*   ------------
*     - set up primary file key
*     - retrieve and set up standard report headings.
*     - set wsaa-1st-non-excep-read & wsaa-1st-excep-read to Y
*
*     NB: the above field is used to indicate if a excep or
*         non-excep record has been printed. This is then used
*         to print the appropiate trailer ie no recs or end of rpt.
*
*   2000-Section
*   ------------
*     - read REGR sequentialy, increment control total 1
*     - at end of file, move ENDP to wssp-edterror
*
*   2500-Section
*   ------------
*     - set wssp-edterror to OK
*     - no validation required as this is done by previous program
*
*   3000-Section
*   ------------
*       - check exception code in REGRPF
*
*           if = 'N' then process non-exception (detail) rec
*                write header & detail lines
*                increment control totals 2 & 3
*                set wsaa-1st-non-excep-read to 'N'.
*
*           if = 'Y' then process exception record
*                if wsaa-1st-non-excep = 'Y'
*                   write hdr & trl for non-excep rpt (ie no recs)
*                if wsaa-1st-non-excep = 'N'
*                   write trl for non-excep rpt (ie records found)
*                write header & detail lines
*                increment control totals 2 & 4
*                set wsaa-1st-excep-read to 'N'.
*
*    4000-Section
*    ------------
*        - check fields :- wsaa-1st-non-excep-read
*                          wsaa-1st-excep-read
*
*          depending on the field value, write headers & trailers
*          for non-excep & excep report.
*
*   Error Processing
*   ----------------
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the REGR-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class B5370 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5370Report printerFile = new R5370Report();
	private R5370Report printerFile02 = new R5370Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData printerRec02 = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5370");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final String descrec = "DESCREC";
	
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t6689 = "T6689";
		/* ERRORS */
	private static final String e306 = "E306";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
		/* WSAA-MESSAGES */
	private FixedLengthStringData[] wsaaMessage = FLSInittedArray(25, 50);
		/* WSAA-PRINTER-CONTROL */
	private ZonedDecimalData wsaaLineCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLineCount02 = new ZonedDecimalData(2, 0).setUnsigned();
	private static final int wsaaPageSize = 59;

		/*   Main, standard page headings*/
	private FixedLengthStringData r5370H01 = new FixedLengthStringData(91);
	private FixedLengthStringData r5370h01O = new FixedLengthStringData(91).isAPartOf(r5370H01, 0);
	private FixedLengthStringData r5370Repdate = new FixedLengthStringData(10).isAPartOf(r5370h01O, 0);
	private ZonedDecimalData r5370Jobnum = new ZonedDecimalData(8, 0).isAPartOf(r5370h01O, 10);
	private FixedLengthStringData r5370Company = new FixedLengthStringData(1).isAPartOf(r5370h01O, 18);
	private FixedLengthStringData r5370Companynm = new FixedLengthStringData(30).isAPartOf(r5370h01O, 19);
	private FixedLengthStringData r5370Sdate = new FixedLengthStringData(10).isAPartOf(r5370h01O, 49);
	private FixedLengthStringData r5370Branch = new FixedLengthStringData(2).isAPartOf(r5370h01O, 59);
	private FixedLengthStringData r5370Branchnm = new FixedLengthStringData(30).isAPartOf(r5370h01O, 61);

	private FixedLengthStringData r5370H02 = new FixedLengthStringData(91);
	private FixedLengthStringData r5370h02O = new FixedLengthStringData(91).isAPartOf(r5370H02, 0);
	private FixedLengthStringData r5370Repdate1 = new FixedLengthStringData(10).isAPartOf(r5370h02O, 0);
	private ZonedDecimalData r5370Jobnum1 = new ZonedDecimalData(8, 0).isAPartOf(r5370h02O, 10);
	private FixedLengthStringData r5370Company1 = new FixedLengthStringData(1).isAPartOf(r5370h02O, 18);
	private FixedLengthStringData r5370Companynm1 = new FixedLengthStringData(30).isAPartOf(r5370h02O, 19);
	private FixedLengthStringData r5370Sdate1 = new FixedLengthStringData(10).isAPartOf(r5370h02O, 49);
	private FixedLengthStringData r5370Branch1 = new FixedLengthStringData(2).isAPartOf(r5370h02O, 59);
	private FixedLengthStringData r5370Branchnm1 = new FixedLengthStringData(30).isAPartOf(r5370h02O, 61);


	private FixedLengthStringData r5370D03 = new FixedLengthStringData(2);

	private FixedLengthStringData r5370D04 = new FixedLengthStringData(2);

	private FixedLengthStringData r5370D05 = new FixedLengthStringData(2);

	private FixedLengthStringData r5370D06 = new FixedLengthStringData(2);

	private FixedLengthStringData r5370D07 = new FixedLengthStringData(2);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Regular Payments View.*/
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private RegrTableDAM regrIO = new RegrTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private R5370D01Inner r5370D01Inner = new R5370D01Inner();
	private R5370D02Inner r5370D02Inner = new R5370D02Inner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private P6671par p6671par = new P6671par();
	private RegrDAO regIO = getApplicationContext().getBean("RegrDAO", RegrDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private List<Regrpf> insertRegrpfList = new ArrayList<Regrpf>();
	private Map<String,String> chdrpfMap = null;
	private List<String> chdrnumList=null;
	private int wsaacount;


	public B5370() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
		setUpFields1050();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getSystemParam03(),"PAYMENT")
		&& isNE(bprdIO.getSystemParam03(),"REVIEW")
		&& isNE(bprdIO.getSystemParam03(),"TERMINATED")) {
			syserrrec.params.set(bprdIO.getSystemParam03());
			syserrrec.statuz.set(e306);
			fatalError600();
		}
		
 /*ILIFE-3168 Start */
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());;
		wsaaChdrnumFrom.set(p6671par.chdrnum);
		if (isEQ(p6671par.chdrnum,SPACES)
				&& isEQ(p6671par.chdrnum1,SPACES)) {
					wsaaChdrnumFrom.set(LOVALUE);
					wsaaChdrnumTo.set(HIVALUE);
				}
				else {
					wsaaChdrnumFrom.set(p6671par.chdrnum);
					wsaaChdrnumTo.set(p6671par.chdrnum1);
				}
		
		insertRegrpfList=regIO.getRegrpfList(bprdIO.getSystemParam02().toString(), wsaaChdrnumFrom.toString(), wsaaChdrnumTo.toString());
	    wsaacount=insertRegrpfList.size();
	  //vrathi4
	    if(!insertRegrpfList.isEmpty()) {
	    chdrnumList=new ArrayList<>(); 
	    insertRegrpfList.forEach(list->chdrnumList.add(list.getChdrnum()));
		chdrpfMap=chdrpfDAO.searchRecordByChdrnumList(bsprIO.getCompany().trim(),chdrnumList);
	    }
	    /*ILIFE-3168 End */
		printerFile.openOutput();
		printerFile02.openOutput();
		wsspEdterror.set(varcom.oK);
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		r5370Company.set(bsprIO.getCompany());
		r5370Company1.set(bsprIO.getCompany());
		r5370Companynm.set(descIO.getLongdesc());
		r5370Companynm1.set(descIO.getLongdesc());
		r5370Jobnum.set(bsscIO.getScheduleNumber());
		r5370Jobnum1.set(bsscIO.getScheduleNumber());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(bsprIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		r5370Branch.set(bsprIO.getDefaultBranch());
		r5370Branch1.set(bsprIO.getDefaultBranch());
		r5370Branchnm.set(descIO.getLongdesc());
		r5370Branchnm1.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		r5370Sdate.set(datcon1rec.extDate);
		r5370Sdate1.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(bsscIO.getEffectiveDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		r5370Repdate.set(datcon1rec.extDate);
		r5370Repdate1.set(datcon1rec.extDate);
	}

protected void setUpFields1050()
	{
		wsaaMiscellaneousInner.wsaa1stNonExcepRead.set("Y");
		wsaaMiscellaneousInner.wsaa1stExcepRead.set("Y");
	
	}

protected void readFile2000()
	{
		//ILIFE-3168 Start 
		if (wsaacount > 0) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(wsaacount);
			callProgram(Contot.class, contotrec.contotRec);
			wsspEdterror.set(varcom.oK);
			wsaacount = 0;
		} else {
			wsspEdterror.set(varcom.endp);
		}
		//ILIFE-3168 End 
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
	//ILIFE-3168 Start 
	for(int i=0;i<insertRegrpfList.size();i++){
		regrIO.setChdrcoy(insertRegrpfList.get(i).getChdrcoy());
		regrIO.setChdrnum(insertRegrpfList.get(i).getChdrnum());
		regrIO.setLife(insertRegrpfList.get(i).getLife());
		regrIO.setCoverage(insertRegrpfList.get(i).getCoverage());
		regrIO.setRider(insertRegrpfList.get(i).getRider());
		regrIO.setRgpynum(insertRegrpfList.get(i).getRgpynum());
		regrIO.setPymt(insertRegrpfList.get(i).getPymt());
		regrIO.setCurrcd(insertRegrpfList.get(i).getCurrcd());
		regrIO.setPrcnt(insertRegrpfList.get(i).getPrcnt());
		regrIO.setRgpytype(insertRegrpfList.get(i).getRgpytype());
		regrIO.setPayreason(insertRegrpfList.get(i).getPayreason());
		regrIO.setRgpystat(insertRegrpfList.get(i).getRgpystat());
		regrIO.setRevdte(insertRegrpfList.get(i).getRevdte());
		regrIO.setFirstPaydate(insertRegrpfList.get(i).getFirstPaydate());
		regrIO.setLastPaydate(insertRegrpfList.get(i).getLastPaydate());
		regrIO.setProgname(insertRegrpfList.get(i).getProgname());
		regrIO.setExcode(insertRegrpfList.get(i).getExcode());
		regrIO.setExreport(insertRegrpfList.get(i).getExreport());
		regrIO.setCrtable(insertRegrpfList.get(i).getCrtable());
		regrIO.setTranno(insertRegrpfList.get(i).getTranno());
		regrIO.setUserProfile(insertRegrpfList.get(i).getUserProfile());
		regrIO.setJobName(insertRegrpfList.get(i).getJobName());
		regrIO.setDatime(insertRegrpfList.get(i).getDatime());
		
		//ILIFE-3168 end 	
		if (isEQ(regrIO.getExreport(),SPACES)) {
			nonExceptRpt3100();
		}
		else {
			excepRpt3650();
		}
		
	}
		/*EXIT*/
	}

protected void nonExceptRpt3100()
	{
		/*NON-EXCEPT-RPT*/
		if (isEQ(wsaaMiscellaneousInner.wsaa1stNonExcepRead, "Y")) {
			writeNonExcepHeader5000();
			wsaaMiscellaneousInner.wsaa1stNonExcepRead.set("N");
		}
		writeNonExcepDetail3200();
		/*EXIT*/
	}

protected void writeNonExcepDetail3200()
	{
		wsaaLineCount.add(1);
		if (isGTE(wsaaLineCount,wsaaPageSize)) {
			writeNonExcepHeader5000();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
		moveProcessRptFields3300();
		printerFile.printR5370d01(r5370D01Inner.r5370D01);
	}

protected void moveProcessRptFields3300()
	{
		processConvertCurrency5400();
		/* move regr fields to report fields*/
		r5370D01Inner.r5370Date.set(datcon1rec.extDate);
		r5370D01Inner.r5370Chdrnum.set(regrIO.getChdrnum());
		r5370D01Inner.r5370Life.set(regrIO.getLife());
		r5370D01Inner.r5370Coverage.set(regrIO.getCoverage());
		r5370D01Inner.r5370Rider.set(regrIO.getRider());
		r5370D01Inner.r5370Rgpynum.set(regrIO.getRgpynum());
		r5370D01Inner.r5370Pymnt.set(wsaaMiscellaneousInner.wsaaAmountOut);
		r5370D01Inner.r5370Currcd.set(regrIO.getCurrcd());
		r5370D01Inner.r5370Prcnt.set(regrIO.getPrcnt());
		r5370D01Inner.r5370Rgpytype.set(regrIO.getRgpytype());
		r5370D01Inner.r5370Payreason.set(regrIO.getPayreason());
		r5370D01Inner.r5370Rgpystat.set(regrIO.getRgpystat());

	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void excepRpt3650()
	{
		/*START*/
		/* check to see what type of trailer to print*/
		if (isEQ(wsaaMiscellaneousInner.wsaa1stExcepRead, "Y")) {
			if (isEQ(wsaaMiscellaneousInner.wsaa1stNonExcepRead, "Y")) {
				nonExcepRptNorecs3700();
			}
			else {
				nonExcepRptYesrecs5100();
			}
			writeExcepHeader5200();
			wsaaMiscellaneousInner.wsaa1stExcepRead.set("N");
		}
		writeExcepDetail3800();
		/*EXIT*/
	}

protected void nonExcepRptNorecs3700()
	{
		/*START*/
		writeNonExcepHeader5000();
		writeNonNorecTrl5300();
		/*EXIT*/
	}

protected void writeExcepDetail3800()
	{
		wsaaLineCount02.add(1);
		if (isGTE(wsaaLineCount02,wsaaPageSize)) {
			writeExcepHeader5200();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
		processConvertCurrency5400();
		getExceptionCodeDesc3850();
		/* move regr fields to report fields*/
		r5370D02Inner.r5370Chdrnum.set(regrIO.getChdrnum());
		r5370D02Inner.r5370Life.set(regrIO.getLife());
		r5370D02Inner.r5370Coverage.set(regrIO.getCoverage());
		r5370D02Inner.r5370Rider.set(regrIO.getRider());
		r5370D02Inner.r5370Rgpynum.set(regrIO.getRgpynum());
		r5370D02Inner.r5370Pymnt.set(wsaaMiscellaneousInner.wsaaAmountOut);
		r5370D02Inner.r5370Currcd.set(regrIO.getCurrcd());
		r5370D02Inner.r5370Prcnt.set(regrIO.getPrcnt());
		r5370D02Inner.r5370Rgpytype.set(regrIO.getRgpytype());
		r5370D02Inner.r5370Payreason.set(regrIO.getPayreason());
		r5370D02Inner.r5370Rgpystat.set(regrIO.getRgpystat());
		printerFile02.printR5370d02(r5370D02Inner.r5370D02, indicArea);
	}

protected void getExceptionCodeDesc3850()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t6689);
		descIO.setDescitem(regrIO.getExcode());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			indOn.setTrue(16);
			indOff.setTrue(15);
		}
		else {
			r5370D02Inner.r5370Longdesc.set(descIO.getLongdesc());
			indOn.setTrue(15);
			indOff.setTrue(16);
		}
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		processReportTrailers4100();
		printerFile.close();		
		//ILIFE-1266 - STARTS
		printerFile02.close();
		//ILIFE-1266 - ENDS
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void processReportTrailers4100()
	{
		if (isEQ(wsaaMiscellaneousInner.wsaa1stNonExcepRead, "Y")
		&& isEQ(wsaaMiscellaneousInner.wsaa1stExcepRead, "Y")) {
			/* header & trailer for non-exception rpt with no recs.*/
			writeNonExcepHeader5000();
			writeNonNorecTrl5300();
			/* header & trailer for exception rpt with no recs.*/
			writeExcepHeader5200();
			writeExcepNorecTrl4200();
			return ;
		}
		if (isEQ(wsaaMiscellaneousInner.wsaa1stExcepRead, "Y")) {
			/* write 'end of rpt' trailer for non-excep rpt*/
			nonExcepRptYesrecs5100();
			/* header & trailer for exception rpt with no recs.*/
			writeExcepHeader5200();
			writeExcepNorecTrl4200();
			return ;
		}
		if (isEQ(wsaaMiscellaneousInner.wsaa1stExcepRead, "N")) {
			/* Check new page required.*/
			wsaaLineCount02.add(1);
			if (isGTE(wsaaLineCount02,wsaaPageSize)) {
				writeExcepHeader5200();
			}
			printerFile02.printR5370d03(r5370D03);
		}
	}


protected void writeExcepNorecTrl4200()
	{
		/*START*/
		printerFile02.printR5370d04(r5370D04);
		/*EXIT*/
	}

protected void writeNonExcepHeader5000()
	{
		if (isEQ(bprdIO.getSystemParam03(),"PAYMENT")) {
			indOn.setTrue(11);
			indOff.setTrue(12);
			indOff.setTrue(13);
			//ILIFE-1266 - STARTS
			indOff.setTrue(14);
			//ILIFE-1266 - ENDS
		}
		else {
			if (isEQ(bprdIO.getSystemParam03(),"REVIEW")) {
				indOn.setTrue(12);
				indOff.setTrue(11);
				indOff.setTrue(13);
				//ILIFE-1266 - STARTS
				indOff.setTrue(14);
				//ILIFE-1266 - ENDS
			}
			else {
				indOn.setTrue(13);
				indOff.setTrue(11);
				indOff.setTrue(12);
				//ILIFE-1266 - STARTS
				indOff.setTrue(14);
				//ILIFE-1266 - ENDS
			}
		}
		wsaaLineCount.set(12);
		printerFile.printR5370h01(r5370H01, indicArea);
	}

protected void nonExcepRptYesrecs5100()
	{
		/*START*/
		wsaaLineCount.add(1);
		if (isGTE(wsaaLineCount,wsaaPageSize)) {
			writeNonExcepHeader5000();
		}
		printerFile.printR5370d03(r5370D03);
		/*EXIT*/
	}

protected void writeExcepHeader5200()
	{
		if (isEQ(bprdIO.getSystemParam03(),"PAYMENT")) {
			indOn.setTrue(11);
			indOn.setTrue(14);
			indOff.setTrue(12);
			indOff.setTrue(13);
		}
		else {
			if (isEQ(bprdIO.getSystemParam03(),"REVIEW")) {
				indOn.setTrue(12);
				indOn.setTrue(14);
				//ILIFE-1266 - STARTS
				indOff.setTrue(11);
				//ILIFE-1266 - ENDS
				indOff.setTrue(13);
			}
			else {
				indOn.setTrue(13);
				indOn.setTrue(14);
				indOff.setTrue(11);
				indOff.setTrue(12);
			}
		}
		wsaaLineCount02.set(12);
		printerFile02.printR5370h02(r5370H02, indicArea);
	}

protected void writeNonNorecTrl5300()
	{
		/*START*/
		if (isEQ(bprdIO.getSystemParam03(),"PAYMENT")) {
			printerFile.printR5370d05(r5370D05, indicArea);
		}
		else {
			if (isEQ(bprdIO.getSystemParam03(),"REVIEW")) {
				printerFile.printR5370d06(r5370D06, indicArea);
			}
			else {
				printerFile.printR5370d07(r5370D07, indicArea);
			}
		}
		/*EXIT*/
	}

protected void processConvertCurrency5400()
	{
		/* move appropiate date depending on param03*/
		if (isEQ(bprdIO.getSystemParam03(),"PAYMENT")) {
			wsaaMiscellaneousInner.wsaaNonExcepDate.set(regrIO.getLastPaydate());
		}
		else {
			if (isEQ(bprdIO.getSystemParam03(),"REVIEW")) {
				wsaaMiscellaneousInner.wsaaNonExcepDate.set(regrIO.getRevdte());
			}
			else {
				wsaaMiscellaneousInner.wsaaNonExcepDate.set(regrIO.getFirstPaydate());
			}
		}
		/* convert date format*/
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(wsaaMiscellaneousInner.wsaaNonExcepDate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		String cntCurr="";
		if (chdrpfMap == null || !chdrpfMap .containsKey(regrIO .getChdrnum().toString().trim() )) {
			syserrrec.params.set(regrIO.getChdrnum().toString().trim() );
			syserrrec.statuz.set(varcom.mrnf);
			fatalError600(); 
		}
		else {
			cntCurr = chdrpfMap.get(regrIO.getChdrnum().toString().trim());
		}
		
		if (isNE(regrIO.getCurrcd(),cntCurr)) {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.currOut.set(regrIO.getCurrcd());
			conlinkrec.currIn.set(cntCurr);
			conlinkrec.amountIn.set(regrIO.getPymt());
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(bsprIO.getCompany());
			conlinkrec.cashdate.set(datcon1rec.intDate);
			conlinkrec.function.set("CVRT");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz,varcom.oK)) {
				syserrrec.params.set(descIO.getParams());
				syserrrec.statuz.set(descIO.getStatuz());
				fatalError600();
			}
			if (isNE(conlinkrec.amountOut, ZERO)) {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				a000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
			}
			wsaaMiscellaneousInner.wsaaAmountOut.set(conlinkrec.amountOut);
		}
		else {
			wsaaMiscellaneousInner.wsaaAmountOut.set(regrIO.getPymt());
		}
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(regrIO.getCurrcd());
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner { 
		/* WSAA-MISCELLANEOUS */
	private FixedLengthStringData wsaa1stNonExcepRead = new FixedLengthStringData(1);
	private FixedLengthStringData wsaa1stExcepRead = new FixedLengthStringData(1);
		/*   For non excep rpt, 3 different rpts with 3 diff dates*/
	private ZonedDecimalData wsaaNonExcepDate = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();

		/*   Used for 3 diff report titles & 3 diff column titles*/
	private FixedLengthStringData wsaaReportTitle = new FixedLengthStringData(32);
		/*                            VALUE 'PAYMENTS'.                 */
	private FixedLengthStringData wsaaTitle = new FixedLengthStringData(9).isAPartOf(wsaaReportTitle, 0);
		/*     88   PAYMENT-TRL       VALUE                             
		                            'No Payments Records'.            
		     88   REVIEW-TRL        VALUE                             
		                            'No In Review Records'.           
		     88   TERMINATED-TRL    VALUE                             
		                            'No Records Terminated'.          
		 2 diff possible amts, move to wsaa-amount-out, then to rpt*/
	private ZonedDecimalData wsaaAmountOut = new ZonedDecimalData(13, 2);

		/* if exception code not found on T6689, display msg*/
	private FixedLengthStringData wsaaT6689Msg = new FixedLengthStringData(25);
	private FixedLengthStringData wsaaMsg = new FixedLengthStringData(21).isAPartOf(wsaaT6689Msg, 0);
		/*                     VALUE 'T6689 : MRNF : VAL : '.           */
	private FixedLengthStringData wsaaT6689Item = new FixedLengthStringData(4).isAPartOf(wsaaT6689Msg, 21);
}
/*
 * Class transformed  from Data Structure R5370-D01--INNER
 */
private static final class R5370D01Inner { 

		/*  Detail line - add as many detail and total lines as required.
		              - use redefines to save WS space where applicable.*/
	private FixedLengthStringData r5370D01 = new FixedLengthStringData(56);
	private FixedLengthStringData r5370d01O = new FixedLengthStringData(56).isAPartOf(r5370D01, 0);
	private FixedLengthStringData r5370Chdrnum = new FixedLengthStringData(8).isAPartOf(r5370d01O, 0);
	private FixedLengthStringData r5370Life = new FixedLengthStringData(2).isAPartOf(r5370d01O, 8);
	private FixedLengthStringData r5370Coverage = new FixedLengthStringData(2).isAPartOf(r5370d01O, 10);
	private FixedLengthStringData r5370Rider = new FixedLengthStringData(2).isAPartOf(r5370d01O, 12);
	private ZonedDecimalData r5370Rgpynum = new ZonedDecimalData(5, 0).isAPartOf(r5370d01O, 14);
	private ZonedDecimalData r5370Pymnt = new ZonedDecimalData(13, 2).isAPartOf(r5370d01O, 19);
	private FixedLengthStringData r5370Currcd = new FixedLengthStringData(3).isAPartOf(r5370d01O, 32);
	private ZonedDecimalData r5370Prcnt = new ZonedDecimalData(5, 2).isAPartOf(r5370d01O, 35);
	private FixedLengthStringData r5370Rgpytype = new FixedLengthStringData(2).isAPartOf(r5370d01O, 40);
	private FixedLengthStringData r5370Payreason = new FixedLengthStringData(2).isAPartOf(r5370d01O, 42);
	private FixedLengthStringData r5370Rgpystat = new FixedLengthStringData(2).isAPartOf(r5370d01O, 44);
	private FixedLengthStringData r5370Date = new FixedLengthStringData(10).isAPartOf(r5370d01O, 46);
}
/*
 * Class transformed  from Data Structure R5370-D02--INNER
 */
private static final class R5370D02Inner { 

	private FixedLengthStringData r5370D02 = new FixedLengthStringData(76);
	private FixedLengthStringData r5370d02O = new FixedLengthStringData(76).isAPartOf(r5370D02, 0);
	private FixedLengthStringData r5370Chdrnum = new FixedLengthStringData(8).isAPartOf(r5370d02O, 0);
	private FixedLengthStringData r5370Life = new FixedLengthStringData(2).isAPartOf(r5370d02O, 8);
	private FixedLengthStringData r5370Coverage = new FixedLengthStringData(2).isAPartOf(r5370d02O, 10);
	private FixedLengthStringData r5370Rider = new FixedLengthStringData(2).isAPartOf(r5370d02O, 12);
	private ZonedDecimalData r5370Rgpynum = new ZonedDecimalData(5, 0).isAPartOf(r5370d02O, 14);
	private ZonedDecimalData r5370Pymnt = new ZonedDecimalData(13, 2).isAPartOf(r5370d02O, 19);
	private FixedLengthStringData r5370Currcd = new FixedLengthStringData(3).isAPartOf(r5370d02O, 32);
	private ZonedDecimalData r5370Prcnt = new ZonedDecimalData(5, 2).isAPartOf(r5370d02O, 35);
	private FixedLengthStringData r5370Rgpytype = new FixedLengthStringData(2).isAPartOf(r5370d02O, 40);
	private FixedLengthStringData r5370Payreason = new FixedLengthStringData(2).isAPartOf(r5370d02O, 42);
	private FixedLengthStringData r5370Rgpystat = new FixedLengthStringData(2).isAPartOf(r5370d02O, 44);
	private FixedLengthStringData r5370Longdesc = new FixedLengthStringData(30).isAPartOf(r5370d02O, 46);
}
}
