/*
 * File: Revdcad.java
 * Date: 30 August 2009 2:06:58
 * Author: Quipoz Limited
 * 
 * Class transformed from REVDCAD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.FluprevTableDAM;
import com.csc.life.contractservicing.dataaccess.FuperevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  REVDCAD - Death Claim Adjustment Reversal
*  -----------------------------------------
*
*  This subroutine is called by the Full Contract Reversal program
*  P5155 via Table T6661. The parameter passed to this subroutine
*  is contained in the REVERSEREC copybook.
*
*  REVERSE ACMVS
*  ~~~~~~~~~~~~~
*  Do a BEGNH on the ACMV file for this transaction no. and
*  call LIFACMV to post an equal and opposite ACMV. At the
*  same time accumulate the total ORIGAMT for the Tranno.
*
*  ADJUST CLAIM HEADER
*  --------------------
*  READH on  the  Claim  Header  record  (CLMHCLM)  for the
*  relevant contract.
*
*  REWRiTe the record by subtracting the totalled ORIGAMT from
*  the ACMV's.
*
*  CLAIM DETAIL RECORDS
*  ~~~~~~~~~~~~~~~~~~~~~
*  Claim  details  record are unaffected by the Adjustment
*  Transaction so there is no need to perform any reversal
*  process on these records. However, we must loop around
*  the CLMD's and perform generic processing on each CRTABLE
*  found.
*
*  REVERSE CONTRACT HEADER
*  ~~~~~~~~~~~~~~~~~~~~~~~
*  BEGNH on  the  Contract  Header record (CHDRCLM) for the
*  relevant contract.
*
*  Store the transaction number and status of the contract.
*  Add 1 to the tranno and store.
*
*  This is the valid flag 1 record - there should be at least
*  one valid flag 1 record and one valid flag 2 record
*  following Death Claim Adjustment.
*
*****************************************************************
* </pre>
*/
public class Revdcad extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVDCAD";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";

	private FixedLengthStringData wsaaT5671 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batc = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671, 4);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5671 = "T5671";
	private static final String t5645 = "T5645";
		/* FORMATS */
	private static final String chdrclmrec = "CHDRCLMREC";
	private static final String clmdclmrec = "CLMDCLMREC";
	private static final String clmhclmrec = "CLMHCLMREC";
	private static final String itemrec = "ITEMREC";
	private static final String acmvrevrec = "ACMVREVREC";
	private static final String arcmrec = "ARCMREC";
	private static final String fluprevrec = "FLUPREVREC";
	private static final String fuperevrec = "FUPEREVREC";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM();
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluprevTableDAM fluprevIO = new FluprevTableDAM();
	private FuperevTableDAM fuperevIO = new FuperevTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5671rec t5671rec = new T5671rec();
	private T5645rec t5645rec = new T5645rec();
	private Greversrec greversrec = new Greversrec();
	private Reverserec reverserec = new Reverserec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit3290, 
		exit3390, 
		exit4090, 
		errorProg610
	}

	public Revdcad() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		processAcmv2000();
		processAcmvOptical2100();
		processChdr3000();
		processClmh4000();
		/* Although no base generic subroutine exist for Death Claim Adjust*/
		/* the forward AT module does allow for a generic call. Therefore,*/
		/* the reversal must also cater for it.*/
		processClmd5000();
		/*EXIT*/
		exitProgram();
	}

protected void processAcmv2000()
	{
		start2000();
	}

protected void start2000()
	{
		/* The ORIGAMT is accumulated for all ACMV's found, then the CLMH*/
		/* is updated with the total amount deducted from the amount on*/
		/* the CLMH.*/
		/*    MOVE ZEROES                 TO WSAA-ORIGAMT.                 */
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs2200();
		}
		
	}

protected void processAcmvOptical2100()
	{
		start2110();
	}

protected void start2110()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			fatalError600();
		}
		/* If the PTRN being processed has an Accounting Period Greater*/
		/* than the last period Archived then there is no need to*/
		/* attempt to read the Optical Device.*/
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs2200();
		}
		
		/* If the next policy is found then we must call the COLD API*/
		/* again with a function of CLOSE.*/
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				fatalError600();
			}
		}
	}

protected void reverseAcmvs2200()
	{
		start2210();
	}

protected void start2210()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvrevIO.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvrevIO.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		readT56452300();
		/* There will be 2 contra entries each the same value for the adjus*/
		/* made. Therefore, only select one ACMV entry to determine how muc*/
		/* adjustment was for. Otherwise, if accumulating them you would en*/
		/* up with a total of 0.*/
		/*    IF  ACMVREV-SACSCODE        = T5645-SACSCODE-01              */
		/*    AND ACMVREV-SACSTYP         = T5645-SACSTYPE-01              */
		/*        COMPUTE  WSAA-ORIGAMT   = WSAA-ORIGAMT +                 */
		/*                                  (ACMVREV-ORIGAMT * -1)         */
		/*    END-IF.                                                      */
		lifacmvrec.genlcur.set(acmvrevIO.getGenlcur());
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvrevIO.getTranref());
		lifacmvrec.tranref.set(reverserec.newTranno);
		descIO.setDescitem(reverserec.newTranno);
		getDescription6000();
		lifacmvrec.effdate.set(reverserec.ptrneff);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		acmvrevIO.setFunction(varcom.nextr);
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			fatalError600();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setStatuz(varcom.endp);
		}
	}

protected void readT56452300()
	{
		start2300();
	}

protected void start2300()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5645);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void processChdr3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* Get the first Contract Header record using CHDRCLM logical view*/
		chdrclmIO.setParams(SPACES);
		chdrclmIO.setChdrnum(reverserec.chdrnum);
		chdrclmIO.setChdrcoy(reverserec.company);
		chdrclmIO.setFormat(chdrclmrec);
		/* MOVE BEGNH                  TO CHDRCLM-FUNCTION.             */
		chdrclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
//		chdrclmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(), varcom.oK)
		&& isNE(chdrclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrclmIO.getParams());
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrclmIO.getValidflag(), "1")
		|| isNE(chdrclmIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrclmIO.getChdrcoy(), reverserec.company)
		|| isEQ(chdrclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrclmIO.getParams());
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			fatalError600();
		}
		/* Delete the valid flag 1 record*/
		/* MOVE DELET                  TO CHDRCLM-FUNCTION.             */
		chdrclmIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}
		/* Reverse other records                                           */
		reverseOtherRecs3100();
		/* Get NEXTR CHDR record (a validflag '2' one) and re-instate to  d*/
		/* validflag '1'.*/
		chdrclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(), varcom.oK)
		&& isNE(chdrclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrclmIO.getParams());
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrclmIO.getValidflag(), "2")
		|| isNE(chdrclmIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(chdrclmIO.getChdrcoy(), reverserec.company)
		|| isEQ(chdrclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrclmIO.getParams());
			syserrrec.statuz.set(chdrclmIO.getStatuz());
			fatalError600();
		}
		/* Update the valid flag 2 record*/
		chdrclmIO.setValidflag("1");
		chdrclmIO.setCurrto(varcom.vrcmMaxDate);
		chdrclmIO.setTranno(reverserec.newTranno);
		/* MOVE REWRT                  TO CHDRCLM-FUNCTION.             */
		chdrclmIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}
	}

protected void reverseOtherRecs3100()
	{
		/*DO-REVERSAL*/
		while ( !(isEQ(fluprevIO.getStatuz(), varcom.endp))) {
			reverseFollowups3200();
		}
		
		fuperevIO.setParams(SPACES);
		while ( !(isEQ(fuperevIO.getStatuz(), varcom.endp))) {
			reverseFuperev3300();
		}
		
		/*EXIT*/
	}

protected void reverseFollowups3200()
	{
		try {
			begnh3210();
			delete3250();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void begnh3210()
	{
		fluprevIO.setParams(SPACES);
		fluprevIO.setChdrnum(reverserec.chdrnum);
		fluprevIO.setChdrcoy(reverserec.company);
		fluprevIO.setTranno(reverserec.tranno);
		fluprevIO.setFormat(fluprevrec);
		fluprevIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, fluprevIO);
		if ((isNE(fluprevIO.getStatuz(), varcom.oK))
		&& (isNE(fluprevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprevIO.getParams());
			fatalError600();
		}
		if ((isNE(fluprevIO.getChdrnum(), reverserec.chdrnum))
		|| (isNE(fluprevIO.getChdrcoy(), reverserec.company))
		|| (isNE(fluprevIO.getTranno(), reverserec.tranno))
		|| (isEQ(fluprevIO.getStatuz(), varcom.endp))) {
			fluprevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3290);
		}
	}

protected void delete3250()
	{
		fluprevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fluprevIO);
		if ((isNE(fluprevIO.getStatuz(), varcom.oK))
		&& (isNE(fluprevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fluprevIO.getParams());
			fatalError600();
		}
	}

protected void reverseFuperev3300()
	{
		try {
			begnh3310();
			delete3350();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void begnh3310()
	{
		fuperevIO.setParams(SPACES);
		fuperevIO.setChdrnum(reverserec.chdrnum);
		fuperevIO.setChdrcoy(reverserec.company);
		fuperevIO.setTranno(reverserec.tranno);
		fuperevIO.setFormat(fuperevrec);
		fuperevIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, fuperevIO);
		if ((isNE(fuperevIO.getStatuz(), varcom.oK))
		&& (isNE(fuperevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fuperevIO.getParams());
			fatalError600();
		}
		if ((isNE(fuperevIO.getChdrnum(), reverserec.chdrnum))
		|| (isNE(fuperevIO.getChdrcoy(), reverserec.company))
		|| (isNE(fuperevIO.getTranno(), reverserec.tranno))
		|| (isEQ(fuperevIO.getStatuz(), varcom.endp))) {
			fuperevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3390);
		}
	}

protected void delete3350()
	{
		fuperevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fuperevIO);
		if ((isNE(fuperevIO.getStatuz(), varcom.oK))
		&& (isNE(fuperevIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(fuperevIO.getParams());
			fatalError600();
		}
	}

protected void processClmh4000()
	{
		try {
			readClaimsHeader4000();
			deletRecord4020();
			nextrRecord4030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readClaimsHeader4000()
	{
		clmhclmIO.setParams(SPACES);
		clmhclmIO.setChdrcoy(reverserec.company);
		clmhclmIO.setChdrnum(reverserec.chdrnum);
		clmhclmIO.setFormat(clmhclmrec);
		/*    MOVE READH                  TO CLMHCLM-FUNCTION.             */
		clmhclmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)
		&& isNE(clmhclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmhclmIO.getParams());
			syserrrec.statuz.set(clmhclmIO.getStatuz());
			fatalError600();
		}
		if (isNE(clmhclmIO.getValidflag(), "1")
		|| isNE(clmhclmIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(clmhclmIO.getChdrcoy(), reverserec.company)
		|| isEQ(clmhclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmhclmIO.getParams());
			syserrrec.statuz.set(clmhclmIO.getStatuz());
			fatalError600();
		}
		/* In case, it has no financial impact, no CLMHCLM will generated  */
		if (isLT(clmhclmIO.getTranno(), reverserec.tranno)) {
			goTo(GotoLabel.exit4090);
		}
	}

	/**
	* <pre>
	*4020-REWRT-RECORD.                                               
	* </pre>
	*/
protected void deletRecord4020()
	{
		/* Delete the VALIDFLAG = '1' rec and then reinstate the           */
		/* VALIDFLAG = '2' rec as the active VALIDFLAG = '1' rec.          */
		clmhclmIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
	}

protected void nextrRecord4030()
	{
		/* Get NEXTR CHDR record (a validflag '2' one) and re-instate to   */
		/* validflag '1'.                                                  */
		clmhclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)
		&& isNE(clmhclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmhclmIO.getParams());
			syserrrec.statuz.set(clmhclmIO.getStatuz());
			fatalError600();
		}
		if (isNE(clmhclmIO.getValidflag(), "2")
		|| isNE(clmhclmIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(clmhclmIO.getChdrcoy(), reverserec.company)
		|| isEQ(clmhclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmhclmIO.getParams());
			syserrrec.statuz.set(clmhclmIO.getStatuz());
			fatalError600();
		}
		/* Update the valid flag 2 record                                  */
		clmhclmIO.setValidflag("1");
		clmhclmIO.setTranno(reverserec.newTranno);
		clmhclmIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
		/*    COMPUTE CLMHCLM-OTHERADJST =                                 */
		/*        CLMHCLM-OTHERADJST + (WSAA-ORIGAMT).                     */
		/*    MOVE REVE-NEW-TRANNO        TO CLMHCLM-TRANNO.               */
		/*    MOVE REWRT                  TO CLMHCLM-FUNCTION.             */
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
	}

protected void processClmd5000()
	{
		begnClmd5010();
	}

protected void begnClmd5010()
	{
		clmdclmIO.setParams(SPACES);
		clmdclmIO.setChdrcoy(reverserec.company);
		clmdclmIO.setChdrnum(reverserec.chdrnum);
		clmdclmIO.setFormat(clmdclmrec);
		clmdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		clmdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clmdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.oK)
		&& isNE(clmdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		if ((isNE(clmdclmIO.getChdrnum(), reverserec.chdrnum))
		|| (isNE(clmdclmIO.getChdrcoy(), reverserec.company))
		|| (isEQ(clmdclmIO.getStatuz(), varcom.endp))) {
			clmdclmIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(clmdclmIO.getStatuz(), varcom.endp))) {
			callGeneric5100();
		}
		
	}

protected void callGeneric5100()
	{
		para5110();
	}

protected void para5110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Batc.set(reverserec.oldBatctrcde);
		wsaaT5671Crtable.set(clmdclmIO.getCrtable());
		itemIO.setItemitem(wsaaT5671);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), SPACES)) {
			t5671rec.t5671Rec.set(SPACES);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		initialize(greversrec.reverseRec);
		greversrec.effdate.set(varcom.vrcmMaxDate);
		greversrec.transDate.set(varcom.vrcmMaxDate);
		greversrec.chdrcoy.set(clmdclmIO.getChdrcoy());
		greversrec.chdrnum.set(clmdclmIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.life.set(clmdclmIO.getLife());
		greversrec.coverage.set(clmdclmIO.getCoverage());
		greversrec.rider.set(clmdclmIO.getRider());
		greversrec.planSuffix.set(ZERO);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.effdate.set(reverserec.ptrneff);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callSubprog5200();
		}
		clmdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.oK)
		&& isNE(clmdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		if ((isNE(clmdclmIO.getChdrnum(), reverserec.chdrnum))
		|| (isNE(clmdclmIO.getChdrcoy(), reverserec.company))
		|| (isEQ(clmdclmIO.getStatuz(), varcom.endp))) {
			clmdclmIO.setStatuz(varcom.endp);
		}
	}

protected void callSubprog5200()
	{
		/*GO*/
		greversrec.statuz.set("****");
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(greversrec.statuz);
				fatalError600();
			}
		}
		/*EXIT*/
	}

protected void getDescription6000()
	{
		para6010();
	}

protected void para6010()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setDesctabl(t1688);
		/* MOVE 'E'                    TO DESC-LANGUAGE.                */
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(), varcom.oK))
		&& (isNE(descIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		lifacmvrec.trandesc.set(descIO.getLongdesc());
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					fatalError601();
				case errorProg610: 
					errorProg610();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		reverserec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
