package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.impl.CovrpfDAOImpl;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.terminationclaims.dataaccess.dao.MatypfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Matypf;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.life.regularprocessing.recordstructures.P6671par;

public class Bhsp6 extends Mainb {
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BHSP6");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	/*
	 * These fields are required by MAINB processing and should not be deleted.
	 */
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRiskCessDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaMatyFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaMatyFn, 0, FILLER).init("MATY");
	private FixedLengthStringData wsaaMatyRunid = new FixedLengthStringData(2).isAPartOf(wsaaMatyFn, 4);
	private ZonedDecimalData wsaaMatyJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaMatyFn, 6).setUnsigned();

	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStatus = "";
	private String wsaaCrtableFound = "N";
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6625rec t6625rec = new T6625rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();

	private Chdrpf chdrmat;
	private Matypf matypf;
	private Covrpf covrpf;
	private Payrpf payr;
	private List<Covrpf> covrlist;
	private List<Matypf> insertMatypfList = new ArrayList<Matypf>();

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAOImpl.class);
	private MatypfDAO matyDAO = getApplicationContext().getBean("matypfDAO", MatypfDAO.class);
	private PayrpfDAO payrDAO = getApplicationContext().getBean("payrDAO", PayrpfDAO.class);
	private Map<String, List<Itempf>> t5687Map = new HashMap<String, List<Itempf>>();
	private Map<String, List<Itempf>> t6625Map = new HashMap<String, List<Itempf>>();
	private Map<String, List<Itempf>> t5679Map = new HashMap<String, List<Itempf>>();

	private Iterator<Covrpf> iteratorList;
	private String matyTableName;
	private int wsaacount;
	private boolean noRecordFound;

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private static final Logger LOGGER = LoggerFactory.getLogger(Bhsp6.class);

	
	public Bhsp6() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
			
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void restart0900() {
		/* RESTART */
		/* EXIT */

	}

	protected void initialise1000() {
		initialise1010();
		readT56791100();
	}

	protected void initialise1010() {
		/* Since this program runs from a parameter screen, move the */
		/* internal parameter area to the original parameter copybook */
		/* to retrieve the contract range */
	
		
			wsaaChdrnumFrom.set(LOVALUE);
		
			wsaaChdrnumTo.set(HIVALUE);
		

		/* The next thing we must do is construct the name of the */
		/* temporary file which we will be working with. */
		wsaaMatyRunid.set(bprdIO.getSystemParam04());
		wsaaMatyJobno.set(bsscIO.getScheduleNumber());
		wsaaCompany.set(bsprIO.getCompany().toString());
		matyTableName = wsaaMatyFn.trim();
		noRecordFound = false;
		covrlist = new ArrayList<Covrpf>();
		matyDAO.deleteMatypf(matyTableName);

		wsaaEffdate.set(bsscIO.getEffectiveDate());

		String coy = bsprIO.getCompany().toString();
		String pfx = smtpfxcpy.item.toString();
		t5687Map = itemDAO.loadSmartTable(pfx, coy, "T5687");
		t6625Map = itemDAO.loadSmartTable(pfx, coy, "T6625");
		t5679Map = itemDAO.loadSmartTable(pfx, coy, "T5679");
		if (covrlist.size() > 0)
			covrlist.clear();
		covrlist = covrpfDAO.searchRecordsforEXPY(coy,wsaaChdrnumFrom.toString(),wsaaChdrnumTo.toString(), wsaaEffdate.toInt());
		if (covrlist.isEmpty())
			noRecordFound = true;
		else {
			wsaacount = covrlist.size();
			iteratorList = covrlist.iterator();
		}
	}

	protected void readT56791100() {
		if (t5679Map.containsKey(bprdIO.getAuthCode())) {
			List<Itempf> t5679ItemList = t5679Map.get(bprdIO.getAuthCode());
			Itempf t5679Item = t5679ItemList.get(0);
			t5679rec.t5679Rec.set(StringUtil.rawToString(t5679Item.getGenarea()));
			return;
		}
		fatalError600();
	}

	protected void readFile2000() {
		if (wsaacount > 0) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(wsaacount);
			callProgram(Contot.class, contotrec.contotRec);
			wsspEdterror.set(varcom.oK);
			wsaacount = 0;
			wsaaEof.set("N");
		} else {
			wsspEdterror.set(varcom.endp);
		}

	}

	protected void edit2500() {
		wsspEdterror.set(varcom.oK);
		readCovrData3100();

	}

	protected void readCovrData3100() {
		if (iteratorList.hasNext()) {
			covrpf = new Covrpf();
			covrpf = iteratorList.next();
		} else {
			wsaaEof.set("Y");
		}
	}

	protected void update3000() {
		while (!(endOfFile.isTrue())) {

			wsaaChdrChdrcoy.set(covrpf.getChdrcoy());
			wsaaChdrChdrnum.set(covrpf.getChdrnum());
			processcontract3200();
		}
	}

	protected void processcontract3200() {
		/* READ-CHDRMAT */
		readChdrmatClntnum3100();
		/* CHECK-STATUS */
		CheckChdrStatus3200();
		if (isEQ(wsaaValidStatus, "N")) {
			while (!(isNE(covrpf.getChdrnum(), wsaaChdrChdrnum) || isNE(covrpf.getChdrcoy(), wsaaChdrChdrcoy)
					|| endOfFile.isTrue())) {
				readCovrData3100();
			}
			return;
		}
		while (!(isNE(covrpf.getChdrnum(), wsaaChdrChdrnum) || isNE(covrpf.getChdrcoy(), wsaaChdrChdrcoy)
				|| endOfFile.isTrue())) {
			Processcomponents3300();
		}

	}

	protected void readChdrmatClntnum3100() {
		callChdrmat3300();
	}

	protected void callChdrmat3300() {
		chdrmat = new Chdrpf();
		chdrmat=chdrpfDAO.getchdrRecord(covrpf.getChdrcoy(),covrpf.getChdrnum());
		
		if (chdrmat== null ) {
			syserrrec.params.set(chdrmat);
			fatalError600();
		}
		/* Read the PAYR file */
		payr = new Payrpf();
		payr=payrDAO.getpayrRecord(wsaaChdrChdrcoy.toString(),wsaaChdrChdrnum.toString());
		
		
		if (payr == null) {
			syserrrec.statuz.set("mrnf");
			syserrrec.params.set("payr"+ wsaaChdrChdrnum);
			fatalError600();
		}
	}

	protected void CheckChdrStatus3200() {
				
					initialRisk3200();
					loopRisk3202();
				
			} 
	
	
	
	protected void initialRisk3200() {
		wsaaSub1.set(ZERO);
	}

	protected void loopRisk3202() {
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, 12)) {
			wsaaValidStatus = "N";
			return;
		}
		if (isEQ(chdrmat.getStatcode(), t5679rec.cnRiskStat[wsaaSub1.toInt()])) {
			initialPrem3205();
			if(isEQ(wsaaValidStatus,"Y")){
				return;
			}
		}
		loopRisk3202();
		return;
	}

	protected void initialPrem3205() { 
		wsaaSub1.set(ZERO);
		loopPrem3207();	
	}

	protected void loopPrem3207() {
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, 12)) {
			wsaaValidStatus = "N";
			return;
		}
		if (isEQ(chdrmat.getPstcde(), t5679rec.cnPremStat[wsaaSub1.toInt()])) {
			wsaaValidStatus = "Y";
			return;
		}
		loopPrem3207();
		return;
	}

	protected void Processcomponents3300() {
		processComponent3400();
	}

	protected void processComponent3400() {
		wsaaRider.set(covrpf.getRider());
		wsaaCoverage.set(covrpf.getCoverage());
		wsaaLife.set(covrpf.getLife());
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(covrpf.getRiskCessDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaRiskCessDate.set(datcon1rec.extDate);

		searchTableT56873400();

		wsaaCrtableFound = "N";
		searchTableT66253500();
		if (isEQ(wsaaCrtableFound, "Y")) {
			readNextRecord3006();
			return;
		}
/*		if (isEQ(t5687rec.maturityCalcMeth, SPACES)) {
			readNextRecord3006();
			return;
		}
		*/
		chkComponentStatii3500();
		if (isEQ(wsaaValidStatus, "Y")) {
			matypf = new Matypf();
			matypf.setCownum(chdrmat.getCownnum());/* IJTI-1523 */
			matypf.setChdrcoy(covrpf.getChdrcoy());
			matypf.setChdrnum(covrpf.getChdrnum());
			matypf.setLife(covrpf.getLife());
			matypf.setJlife(covrpf.getJlife());
			matypf.setCoverage(covrpf.getCoverage());
			matypf.setRider(covrpf.getRider());
			matypf.setCrtable(covrpf.getCrtable());
			matypf.setValidflag(covrpf.getValidflag());
			matypf.setTranno(chdrmat.getTranno());
			matypf.setActvalue(BigDecimal.ZERO);
			matypf.setMaturitycalcmeth(t5687rec.maturityCalcMeth.toString());
			matypf.setSinglepremind(t5687rec.singlePremInd.toString());
			insertMatypfList.add(matypf);
			readNextRecord3006();
		}
	}

	protected void searchTableT56873400() {
		if (t5687Map.containsKey(covrpf.getCrtable())) {
			List<Itempf> t5687ItemList = t5687Map.get(covrpf.getCrtable());
			Itempf t5687Item = t5687ItemList.get(0);
			t5687rec.t5687Rec.set(StringUtil.rawToString(t5687Item.getGenarea()));
			return;
		}
		fatalError600();
	}

	protected void searchTableT66253500() {
		if (t6625Map.containsKey(covrpf.getCrtable())) {
			wsaaCrtableFound = "Y";
		}
	}

	protected void chkComponentStatii3500() {
		chkComponentStatii3600();
		if (isEQ(wsaaValidStatus, "N")) {
			readCovrData3100();
			return;
		}
	}

	protected void chkComponentStatii3600() {
		wsaaSub1.set(ZERO);
		loopRisk4402();
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaSub1.set(ZERO);
			loopPrem4407();
		}
	}

	protected void loopRisk4402() {
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, 12)) {
			wsaaValidStatus = "N";
			return;
		}
		if (isEQ(covrpf.getStatcode(), t5679rec.covRiskStat[wsaaSub1.toInt()])) {
			initialPrem4405();
			if(isEQ(wsaaValidStatus,"Y")){
				return;
			}
		}
		loopRisk4402();
		return ;
	}

	protected void initialPrem4405() {
		wsaaSub1.set(ZERO);
		loopPrem4407();
	}

	protected void loopPrem4407() {
		wsaaSub1.add(1);
		if (isGT(wsaaSub1, 12)) {
			wsaaValidStatus = "N";
			return;
		}
		if (isEQ(covrpf.getPstatcode(), t5679rec.covPremStat[wsaaSub1.toInt()])) {
			wsaaValidStatus = "Y";
			return;
		}
		loopPrem4407();
		return;
	}

	protected void readNextRecord3006() {
		while (!(isNE(covrpf.getRider(), wsaaRider) 
			|| isNE(covrpf.getCoverage(), wsaaCoverage)
			|| isNE(covrpf.getLife(), wsaaLife) 
			|| isNE(covrpf.getChdrnum(), wsaaChdrChdrnum)
			|| isNE(covrpf.getChdrcoy(), wsaaChdrChdrcoy)
			|| endOfFile.isTrue())) {
			readCovrData3100();
			

		}

	}

	protected void commit3500() {
		if (!noRecordFound) {
			commitMatyBulkInsert();

		}

	}

	protected void commitMatyBulkInsert() {

		boolean isInsertMatypfTemp = matyDAO.insertMatyTempRecords(insertMatypfList, matyTableName);

		/*
		 * Write a insertMatyTempRecords method matydao that will insert bulk
		 * data.
		 */

		if (!isInsertMatypfTemp) {
			LOGGER.error("Insert Matymx record failed.");
			fatalError600();
		} else
			insertMatypfList.clear();
	}

	@Override
	protected void rollback3600() {
		/* ROLLBACK */
		/* EXIT */

	}

	protected void close4000() {
		/* CLOSE-FILES */
		if (!insertMatypfList.isEmpty()) {
			insertMatypfList.clear();
			insertMatypfList = null;
			if (t5687Map != null) {
				t5687Map.clear();
		    }
			t5687Map = null;
			if (t6625Map != null) {
				t6625Map.clear();
		    }
			t6625Map = null;
			if (t5679Map != null) {
				t5679Map.clear();
		    }
			t5679Map = null;
		
		lsaaStatuz.set(varcom.oK);
		}

	}

}
