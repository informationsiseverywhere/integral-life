/*
 * File: Revmaty.java
 * Date: 30 August 2009 2:09:57
 * Author: Quipoz Limited
 *
 * Class transformed from REVMATY.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.IncrselTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanenqTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.tablestructures.T6633rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.MatdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.MathclmTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  This subroutine is called by the Full Contract Reversal
*  program REVGENAT via Table T6661. The parameter passed to
*  this subroutine is contained in the REVERSEREC copybook.
*
*  REVERSE MATURITY CLAIM HEADER
*
*  Only reverse the contract if reversing from a Maturity/Expiry.
*
*       READH on the Maturity Claim Header record (MATHCLM)
*       for the relevant contract.
*
*       Store the transaction number.
*
*       DELET this record.
*
*  REVERSE CONTRACT HEADER
*
*       READH on  the  Contract  Header record (CHDRLIF) for the
*       relevant contract. Add 1 to the tranno and store.
*
*       DELET this record.
*
*       NEXTR on  the  Contract  Header record (CHDRLIF) for the
*       relevant  contract.  This should read a record with
*       valid flag of '2', if not, then this is an error.
*
*       REWRT this   Contract   Header  record  (CHDRLIF)  after
*       altering  the  valid  flag from '2' to '1' and with
*       the  stored  tranno  as  this  is to re-instate the
*       contract prior to the maturity/expiry.
*
*  REVERSE PAYRLIF
*
*       READH on  the  Payor record (PAYRLIF) for the relevant
*       contract. Add 1 to the tranno and store.
*
*       DELET this record.
*
*       NEXTR on  the  Payor record (PAYRLIF) for the relevant
*       contract.  This should read a record with valid flag of
*       '2', if not, then this is an error.
*
*       REWRT this Payor record (PAYRLIF)after altering the valid
*       flag from '2' to '1' and with the stored as this is to
*       re-instate the  contract prior to the maturity/expiry.
*
*  REVERSE LIVES
*
*  If the entire Plan was matured, then the LIFE has had a
*  status change and its valid flag changed from "1" to "2".
*
*       READH on the Life details record (LIFE) for the
*       relevant contract and the life from the MATH record.
*
*       DELET this record.
*
*       NEXTR on the Life details record (LIFE) should read a
*       record with valid flag of '2', if not, then this is
*       an error.
*
*       REWRT this Life details record (LIFE) after altering
*       the valid flag from '2' to '1' as this is to re-instate
*       the life prior to the maturity/expiry.
*
*       Skip over all other valid flag '2' records until a
*       valid flag  '1'  record  is found for each
*       coverage/rider reinstated, look up generic subroutine
*       on T5671 and call if not blank.
*
*  REVERSE COMPONENTS.
*
*  Read all  the Coverage/Rider records (COVR) for this contract
*  and update each COVR record as follows:
*
*       READH on the COVR records (COVR) for the relevant
*       contract.
*
*       DELET this record.
*
*       NEXTR on the COVR file (COVR) should read a record
*       with valid flag of '2', if not ,then this is an error.
*
*       REWRT this COVR detail record (COVR) after altering
*       the valid flag from '2' to '1' as this is to re-instate
*       the coverage prior to the maturity/expiry.
*
*  REVERSE CLAIM DETAIL RECORDS
*
*       READH on the Maturity Claim details record
*       (MATDCLM) for the relevant contract.
*
*       DELET all records.
*
*  REVERSAL OF UTRNS
*
*  Call the T5671 generic subroutine to reverse Unit
*  transactions for the TRANNO being reversed.
*
*  The subroutine GREVUTRN should be entered in the relevant
*  table entry.
*
*  CONTRACT REVERSAL ACCOUNTING
*
*  Do a BEGNH on the ACMV file for this transaction no. and
*  call LIFACMV to post the reversal to the sub-account.
*
*
*  REVERSAL OF ANY LOANS PAID OFF BY THE MATURITY
*
*       Do a BEGNH on the LOANENQ file for the tranno being
*        reversed and see if any Loans ( contract or APLs )
*        were paid off during the Death Claim processing
*        - check this by comparing the Last-Tranno field with
*          the tranno we are trying to reverse now
*        - if so, we must re-instate the Loans affected
*        Set the LOANENQ record Validflags back to '1' and
*         zeroise the Last-Tranno field
*
* AGENT/GOVERNMENT STATISTICS RECORDS REVERSAL.
*
* All of these records are reversed when REVGENAT, the AT module,
*  calls the statistical subroutine LIFSTTR.
*
* NO processing is therefore required in this subroutine.
*
*
*
*
*****************************************************************
* </pre>
*/
public class Revmaty extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVMATY";

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batc = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaChdrlifTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaPayrlifTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaReversedTranno = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaFullyMatured = new FixedLengthStringData(1).init("Y");
	private Validator fullyMatured = new Validator(wsaaFullyMatured, "Y");
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2);
	private PackedDecimalData wsaaLstInterestDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaNxtInterestDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaLoanNumber = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();
		/* WSAA-C-DATE */
	private ZonedDecimalData wsaaContractDate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-E-DATE */
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
		/* WSAA-L-DATE */
	private ZonedDecimalData wsaaLoanDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaLndt = new FixedLengthStringData(8).isAPartOf(wsaaLoanDate, 0, REDEFINE);
	private ZonedDecimalData wsaaLoanYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaLndt, 0).setUnsigned();
	private FixedLengthStringData wsaaLoanMd = new FixedLengthStringData(4).isAPartOf(wsaaLndt, 4);
	private ZonedDecimalData wsaaLoanMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 0).setUnsigned();
	private ZonedDecimalData wsaaLoanDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaLoanMd, 2).setUnsigned();
		/* WSAA-N-DATE */
	private ZonedDecimalData wsaaNewDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNdate = new FixedLengthStringData(8).isAPartOf(wsaaNewDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNewYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNdate, 0).setUnsigned();
	private FixedLengthStringData wsaaNewMd = new FixedLengthStringData(4).isAPartOf(wsaaNdate, 4);
	private ZonedDecimalData wsaaNewMonth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 0).setUnsigned();
	private ZonedDecimalData wsaaNewDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNewMd, 2).setUnsigned();

	private ZonedDecimalData wsaaDayCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator daysLessThan28 = new Validator(wsaaDayCheck, new ValueRange("00","28"));
	private Validator daysInJan = new Validator(wsaaDayCheck, 31);
	private Validator daysInFeb = new Validator(wsaaDayCheck, 28);
	private Validator daysInApr = new Validator(wsaaDayCheck, 30);
	private static final int wsaaFebruaryDays = 28;
	private static final int wsaaAprilDays = 30;

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";

	private FixedLengthStringData wsaaAcmvTrcde = new FixedLengthStringData(4).init(SPACES);
	private Validator loanInterestBilling = new Validator(wsaaAcmvTrcde, "BA69");
	private Validator loanInterestCapital = new Validator(wsaaAcmvTrcde, "BA68");
		/* TABLES */
	private static final String t5671 = "T5671";
	private static final String t5679 = "T5679";
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t6633 = "T6633";
		/* ERRORS */
	private static final String e723 = "E723";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private IncrselTableDAM incrselIO = new IncrselTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private LoanenqTableDAM loanenqIO = new LoanenqTableDAM();
	private MatdclmTableDAM matdclmIO = new MatdclmTableDAM();
	private MathclmTableDAM mathclmIO = new MathclmTableDAM();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Greversrec greversrec = new Greversrec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T5645rec t5645rec = new T5645rec();
	private T6633rec t6633rec = new T6633rec();
	private Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaMonthCheckInner wsaaMonthCheckInner = new WsaaMonthCheckInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit3009,
		exit3109,
		getNextComponent4106,
		exit4109
	}

	public Revmaty() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		mainRoutine0000();
		exit0009();
	}

protected void mainRoutine0000()
	{
		initialize0010();
		reverseMathclms1000();
		reverseContractHeader2000();
		reversePayrlif2500();
		/*  PERFORM 3000-REVERSE-LIVES.                                  */
		reverseComponents4000();
		if (fullyMatured.isTrue()) {
			reverseLives3000();
		}
		reverseMatdclms5000();
		reverseAcmvs7000();
		processAcmvOptical7010();
		checkLoans10000();
	}

	/**
	* <pre>
	**** PERFORM A000-STATISTICS.
	* </pre>
	*/
protected void exit0009()
	{
		exitProgram();
	}

protected void initialize0010()
	{
		/*INIT*/
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		wsaaBatckey.set(reverserec.batchkey);
		/* Get todays date*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}

protected void reverseMathclms1000()
	{
		readhMathclm1000();
	}

protected void readhMathclm1000()
	{
		mathclmIO.setParams(SPACES);
		mathclmIO.setChdrcoy(reverserec.company);
		mathclmIO.setChdrnum(reverserec.chdrnum);
		mathclmIO.setTranno(reverserec.tranno);
		mathclmIO.setPlanSuffix(ZERO);
		mathclmIO.setFormat(formatsInner.mathclmrec);
		/* MOVE BEGNH                  TO MATHCLM-FUNCTION.             */
		mathclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		mathclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mathclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(), varcom.oK)
		&& isNE(mathclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(mathclmIO.getParams());
			fatalErrors9000();
		}
		if (isNE(mathclmIO.getChdrcoy(), reverserec.company)
		|| isNE(mathclmIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(mathclmIO.getTranno(), reverserec.tranno)
		|| isEQ(mathclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(mathclmIO.getParams());
			fatalErrors9000();
		}
		while ( !(isEQ(mathclmIO.getStatuz(), varcom.endp))) {
			deleteMathclm1010();
		}

	}

protected void deleteMathclm1010()
	{
		/*DELETE-RECORD*/
		mathclmIO.setFormat(formatsInner.mathclmrec);
		/* MOVE DELET                  TO MATHCLM-FUNCTION.             */
		mathclmIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(mathclmIO.getParams());
			fatalErrors9000();
		}
		/*READ-NEXT-RECORD*/
		mathclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getChdrcoy(), reverserec.company)
		|| isNE(mathclmIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(mathclmIO.getTranno(), reverserec.tranno)
		|| isEQ(mathclmIO.getStatuz(), varcom.endp)) {
			mathclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void reverseContractHeader2000()
	{
		begnhChdrlif2000();
		deletChdrlifRecord2002();
		readNextrChdrlif2004();
		rewriteChdrlif2006();
	}

protected void begnhChdrlif2000()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(reverserec.company);
		chdrlifIO.setChdrnum(reverserec.chdrnum);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		/* MOVE BEGNH                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalErrors9000();
		}
		wsaaStatcode.set(chdrlifIO.getStatcode());
		wsaaReversedTranno.set(chdrlifIO.getTranno());
		compute(wsaaChdrlifTranno, 0).set(add(chdrlifIO.getTranno(), 1));
	}

protected void deletChdrlifRecord2002()
	{
		/* MOVE DELET                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalErrors9000();
		}
	}

protected void readNextrChdrlif2004()
	{
		chdrlifIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)
		&& isNE(chdrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalErrors9000();
		}
		if (isNE(chdrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(chdrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(chdrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalErrors9000();
		}
		if (isNE(chdrlifIO.getValidflag(), "2")) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalErrors9000();
		}
	}

protected void rewriteChdrlif2006()
	{
		chdrlifIO.setTranno(wsaaChdrlifTranno);
		chdrlifIO.setValidflag("1");
		/* MOVE REWRT                  TO CHDRLIF-FUNCTION.             */
		chdrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalErrors9000();
		}
		/*EXIT*/
	}

protected void reversePayrlif2500()
	{
		begnhPayrlif2500();
		deletPayrlifRecord2502();
		readNextrPayrlif2504();
		rewritePayrlif2506();
	}

protected void begnhPayrlif2500()
	{
		payrlifIO.setDataArea(SPACES);
		payrlifIO.setChdrcoy(reverserec.company);
		payrlifIO.setChdrnum(reverserec.chdrnum);
		payrlifIO.setPayrseqno(ZERO);
		payrlifIO.setFormat(formatsInner.payrlifrec);
		/* MOVE BEGNH                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrlifIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrlifIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			fatalErrors9000();
		}
		compute(wsaaPayrlifTranno, 0).set(add(payrlifIO.getTranno(), 1));
	}

protected void deletPayrlifRecord2502()
	{
		payrlifIO.setFormat(formatsInner.payrlifrec);
		/* MOVE DELET                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			fatalErrors9000();
		}
	}

protected void readNextrPayrlif2504()
	{
		payrlifIO.setFormat(formatsInner.payrlifrec);
		payrlifIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)
		&& isNE(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			fatalErrors9000();
		}
		if (isNE(payrlifIO.getChdrcoy(), reverserec.company)
		|| isNE(payrlifIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(payrlifIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(payrlifIO.getParams());
			fatalErrors9000();
		}
		if (isNE(payrlifIO.getValidflag(), "2")) {
			syserrrec.params.set(payrlifIO.getParams());
			fatalErrors9000();
		}
	}

protected void rewritePayrlif2506()
	{
		payrlifIO.setTranno(wsaaPayrlifTranno);
		payrlifIO.setValidflag("1");
		payrlifIO.setFormat(formatsInner.payrlifrec);
		/* MOVE REWRT                  TO PAYRLIF-FUNCTION.             */
		payrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrlifIO.getParams());
			fatalErrors9000();
		}
		/*EXIT*/
	}

protected void reverseLives3000()
	{
		try {
			checkWholePlan3000();
			readhLife3002();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkWholePlan3000()
	{
		if (isNE(reverserec.planSuffix, ZERO)) {
			goTo(GotoLabel.exit3009);
		}
	}

protected void readhLife3002()
	{
		lifeIO.setParams(SPACES);
		lifeIO.setChdrcoy(reverserec.company);
		lifeIO.setChdrnum(reverserec.chdrnum);
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFormat(formatsInner.liferec);
		/* MOVE BEGNH                  TO LIFE-FUNCTION.                */
		lifeIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)
		&& isNE(lifeIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalErrors9000();
		}
		if (isNE(lifeIO.getChdrcoy(), reverserec.company)
		|| isNE(lifeIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(lifeIO.getValidflag(), "1")) {
			syserrrec.params.set(lifeIO.getParams());
			fatalErrors9000();
		}
		while ( !(isEQ(lifeIO.getStatuz(), varcom.endp))) {
			deleteAndUpdate3100();
		}

	}

protected void deleteAndUpdate3100()
	{
		try {
			deleteRecord3101();
			readNextrRecord3102();
			readJlife3105();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void deleteRecord3101()
	{
		lifeIO.setFormat(formatsInner.liferec);
		/* MOVE DELET                  TO LIFE-FUNCTION.                */
		lifeIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalErrors9000();
		}
	}

protected void readNextrRecord3102()
	{
		lifeIO.setFormat(formatsInner.liferec);
		lifeIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)
		&& isNE(lifeIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalErrors9000();
		}
		if (isNE(lifeIO.getChdrcoy(), reverserec.company)
		|| isNE(lifeIO.getChdrnum(), reverserec.chdrnum)
		|| isEQ(lifeIO.getStatuz(), varcom.endp)) {
			lifeIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3109);
		}
		if (isNE(lifeIO.getValidflag(), "2")) {
			syserrrec.params.set(lifeIO.getParams());
			fatalErrors9000();
		}
		/* MOVE REVE-TRANS-DATE        TO LIFE-TRANSACTION-DATE         */
		/* MOVE REVE-TRANS-TIME        TO LIFE-TRANSACTION-TIME.        */
		/* MOVE REVE-USER              TO LIFE-USER.                    */
		/* MOVE REVE-TERMID            TO LIFE-TERMID.                  */
		lifeIO.setValidflag("1");
		lifeIO.setCurrto(varcom.vrcmMaxDate);
		lifeIO.setFormat(formatsInner.liferec);
		/* MOVE REWRT                  TO LIFE-FUNCTION.                */
		lifeIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalErrors9000();
		}
	}

protected void readJlife3105()
	{
		lifeIO.setFormat(formatsInner.liferec);
		lifeIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)
		&& isNE(lifeIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalErrors9000();
		}
		if (isEQ(lifeIO.getStatuz(), varcom.endp)
		|| isNE(lifeIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(lifeIO.getChdrcoy(), reverserec.company)) {
			lifeIO.setStatuz(varcom.endp);
			return ;
		}
		while ( !(isEQ(lifeIO.getStatuz(), varcom.endp)
		|| isEQ(lifeIO.getValidflag(), "1"))) {
			getNextLife3200();
		}

	}

protected void getNextLife3200()
	{
		/*READ-NEXTR*/
		lifeIO.setFormat(formatsInner.liferec);
		lifeIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), varcom.oK)
		&& isNE(lifeIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalErrors9000();
		}
		if (isEQ(lifeIO.getStatuz(), varcom.endp)
		|| isNE(lifeIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(lifeIO.getChdrcoy(), reverserec.company)) {
			lifeIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void reverseComponents4000()
	{
		readBegnCovr4000();
	}

protected void readBegnCovr4000()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(reverserec.company);
		covrIO.setChdrnum(reverserec.chdrnum);
		covrIO.setPlanSuffix(reverserec.planSuffix);
		covrIO.setTranno(ZERO);
		covrIO.setFormat(formatsInner.covrrec);
		/* MOVE BEGNH                  TO COVR-FUNCTION.                */
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp)
		|| isEQ(covrIO.getTranno(), reverserec.tranno))) {
			SmartFileCode.execute(appVars, covrIO);
			if (isNE(covrIO.getStatuz(), varcom.oK)
			&& isNE(covrIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(covrIO.getParams());
				fatalErrors9000();
			}
			if (isNE(covrIO.getChdrnum(), reverserec.chdrnum)
			|| isNE(covrIO.getChdrcoy(), reverserec.company)) {
				covrIO.setStatuz(varcom.endp);
				syserrrec.params.set(covrIO.getParams());
				fatalErrors9000();
			}
			covrIO.setFunction(varcom.nextr);
		}

		if (isNE(covrIO.getValidflag(), "1")) {
			syserrrec.params.set(covrIO.getParams());
			fatalErrors9000();
		}
		getStatus4500();
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			deleteAndUpdatComp4100();
		}

	}

protected void deleteAndUpdatComp4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					deleteRecs4100();
				case getNextComponent4106:
					getNextComponent4106();
				case exit4109:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void deleteRecs4100()
	{
		/* Skip over unmature components.*/
		/*  IF T5679-SET-CN-RISK-STAT   NOT = COVR-STATCODE              */
		if (isNE(t5679rec.setCovRiskStat, covrIO.getStatcode())) {
			goTo(GotoLabel.getNextComponent4106);
		}
		/* IF T5679-SET-CN-RISK-STAT   NOT = CHDRLIF-STATCODE      <002>*/
		if (isNE(t5679rec.setCnRiskStat, wsaaStatcode)) {
			wsaaFullyMatured.set("N");
		}
		/* Check for a increase record created by the forward           */
		/* transaction and delete it if one exists.                     */
		checkIncr4300();
		reverseUtrn6000();
		covrIO.setFormat(formatsInner.covrrec);
		/* MOVE DELET                  TO COVR-FUNCTION.                */
		covrIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			fatalErrors9000();
		}
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalErrors9000();
		}
		if ((isEQ(covrIO.getStatuz(), varcom.endp))
		|| (isNE(covrIO.getChdrnum(), chdrlifIO.getChdrnum()))
		|| (isNE(covrIO.getChdrcoy(), reverserec.company))) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit4109);
		}
		if (isNE(covrIO.getValidflag(), "2")) {
			syserrrec.params.set(covrIO.getParams());
			fatalErrors9000();
		}
		/* MOVE REVE-TRANS-DATE        TO COVR-TRANSACTION-DATE.        */
		/* MOVE REVE-TRANS-TIME        TO COVR-TRANSACTION-TIME.        */
		/* MOVE REVE-USER              TO COVR-USER.                    */
		/* MOVE REVE-TERMID            TO COVR-TERMID.                  */
		covrIO.setValidflag("1");
		/* MOVE WSAA-CHDRLIF-TRANNO    TO COVR-TRANNO.                  */
		covrIO.setCurrto(varcom.vrcmMaxDate);
		/* MOVE REWRT                  TO COVR-FUNCTION.                */
		covrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			fatalErrors9000();
		}
	}

protected void getNextComponent4106()
	{
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalErrors9000();
		}
		if ((isEQ(covrIO.getStatuz(), varcom.endp))
		|| (isNE(covrIO.getChdrnum(), reverserec.chdrnum))
		|| (isNE(covrIO.getChdrcoy(), reverserec.company))) {
			covrIO.setStatuz(varcom.endp);
			return ;
		}
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp)
		|| (isEQ(covrIO.getValidflag(), "1")
		&& isEQ(covrIO.getTranno(), reverserec.tranno)))) {
			getNextCovr4200();
		}

	}

protected void getNextCovr4200()
	{
		/*GET-RECS*/
		covrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalErrors9000();
		}
		if ((isEQ(covrIO.getStatuz(), varcom.endp))
		|| (isNE(covrIO.getChdrnum(), reverserec.chdrnum))
		|| (isNE(covrIO.getChdrcoy(), reverserec.company))) {
			covrIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkIncr4300()
	{
		readh4310();
	}

protected void readh4310()
	{
		/* Look for a historic INCR record for the forward transaction. */
		/* If one exists for this component, delete it, thereby         */
		/* reinstating the previous record.                             */
		incrselIO.setDataArea(SPACES);
		incrselIO.setChdrcoy(covrIO.getChdrcoy());
		incrselIO.setChdrnum(covrIO.getChdrnum());
		incrselIO.setLife(covrIO.getLife());
		incrselIO.setCoverage(covrIO.getCoverage());
		incrselIO.setRider(covrIO.getRider());
		incrselIO.setPlanSuffix(covrIO.getPlanSuffix());
		incrselIO.setTranno(reverserec.tranno);
		incrselIO.setFunction(varcom.readh);
		incrselIO.setFormat(formatsInner.incrselrec);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(), varcom.oK)
		&& isNE(incrselIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			fatalErrors9000();
		}
		if (isEQ(incrselIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		incrselIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, incrselIO);
		if (isNE(incrselIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(incrselIO.getStatuz());
			syserrrec.params.set(incrselIO.getParams());
			fatalErrors9000();
		}
	}

protected void getStatus4500()
	{
		readStatusTable4500();
	}

protected void readStatusTable4500()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5679);
		/*  MOVE WSAA-MAT-TRANS-CODE    TO ITEM-ITEMITEM.                */
		itemIO.setItemitem(reverserec.oldBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalErrors9000();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void reverseMatdclms5000()
	{
		/*BEGNH-MATDCLM*/
		matdclmIO.setParams(SPACES);
		matdclmIO.setChdrcoy(reverserec.company);
		matdclmIO.setChdrnum(reverserec.chdrnum);
		matdclmIO.setTranno(reverserec.tranno);
		matdclmIO.setPlanSuffix(ZERO);
		matdclmIO.setFormat(formatsInner.matdclmrec);
		/* MOVE BEGNH                  TO MATDCLM-FUNCTION.             */
		matdclmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		matdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		matdclmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		SmartFileCode.execute(appVars, matdclmIO);
		/*DELETE-MATDCLM-RECORDS*/
		while ( !(isEQ(matdclmIO.getStatuz(), varcom.endp))) {
			deleteMatdclmRecords5100();
		}

		/*EXIT*/
	}

protected void deleteMatdclmRecords5100()
	{
		deleteMatdclms5100();
		nextrMatdclm5102();
	}

protected void deleteMatdclms5100()
	{
		/* MOVE DELET                  TO MATDCLM-FUNCTION.             */
		matdclmIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(matdclmIO.getParams());
			syserrrec.statuz.set(matdclmIO.getStatuz());
			fatalErrors9000();
		}
	}

protected void nextrMatdclm5102()
	{
		matdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, matdclmIO);
		if (isNE(matdclmIO.getStatuz(), varcom.oK)
		&& isNE(matdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(matdclmIO.getParams());
			syserrrec.statuz.set(matdclmIO.getStatuz());
			fatalErrors9000();
		}
		if (isNE(matdclmIO.getChdrcoy(), reverserec.company)
		|| isNE(matdclmIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(matdclmIO.getTranno(), reverserec.tranno)
		|| isEQ(matdclmIO.getStatuz(), varcom.endp)) {
			matdclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void reverseUtrn6000()
	{
		readStatusCodes6000();
	}

protected void readStatusCodes6000()
	{
		itemIO.setDataKey(SPACES);
		wsaaT5671Batc.set(reverserec.oldBatctrcde);
		wsaaT5671Crtable.set(covrIO.getCrtable());
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5671);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalErrors9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		greversrec.chdrcoy.set(covrIO.getChdrcoy());
		greversrec.chdrnum.set(covrIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		/* MOVE REVE-PLAN-SUFFIX       TO GREV-PLAN-SUFFIX.             */
		greversrec.planSuffix.set(covrIO.getPlanSuffix());
		greversrec.life.set(covrIO.getLife());
		greversrec.newTranno.set(wsaaChdrlifTranno);
		greversrec.coverage.set(covrIO.getCoverage());
		greversrec.rider.set(covrIO.getRider());
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.language.set(reverserec.language);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callSubprog6100();
		}
	}

protected void callSubprog6100()
	{
		/*GO*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(greversrec.statuz);
				fatalErrors9000();
			}
		}
		/*EXIT*/
	}

protected void reverseAcmvs7000()
	{
		readSubAcctTab7000();
	}

protected void readSubAcctTab7000()
	{
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			fatalErrors9000();
		}
		if ((isNE(acmvrevIO.getRldgcoy(), reverserec.company))
		|| (isNE(acmvrevIO.getRdocnum(), reverserec.chdrnum))
		|| (isNE(acmvrevIO.getTranno(), reverserec.tranno))) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs7100();
		}

	}

protected void processAcmvOptical7010()
	{
		start7010();
	}

protected void start7010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			fatalErrors9000();
		}
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			fatalErrors9000();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvRecs7100();
		}

		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				fatalErrors9000();
			}
		}
	}

protected void reverseAcmvRecs7100()
	{
		readAcmv7100();
		nextAcmv7104();
	}

protected void readAcmv7100()
	{
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey);
		lifacmvrec.batccoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifacmvrec.tranno.set(wsaaChdrlifTranno);
		lifacmvrec.sacscode.set(acmvrevIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvrevIO.getSacstyp());
		lifacmvrec.glcode.set(acmvrevIO.getGlcode());
		lifacmvrec.glsign.set(acmvrevIO.getGlsign());
		/* MOVE ZERO                   TO LIFA-JRNSEQ.                  */
		lifacmvrec.jrnseq.set(acmvrevIO.getJrnseq());
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.rldgacct.set(acmvrevIO.getRldgacct());
		/* MOVE CHDRLIF-CNTCURR        TO LIFA-ORIGCURR.                */
		lifacmvrec.origcurr.set(acmvrevIO.getOrigcurr());
		compute(lifacmvrec.origamt, 2).set(mult(acmvrevIO.getOrigamt(), -1));
		compute(lifacmvrec.acctamt, 2).set(mult(acmvrevIO.getAcctamt(), -1));
		lifacmvrec.genlcur.set(SPACES);
		/* MOVE ZERO                   TO LIFA-ACCTAMT.                 */
		/* MOVE ZERO                   TO LIFA-CRATE.                   */
		lifacmvrec.crate.set(acmvrevIO.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(reverserec.tranno);
		descIO.setDescitem(reverserec.tranno);
		getDescription8000();
		/* MOVE WSAA-TODAY             TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvrevIO.getEffdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalErrors9000();
		}
	}

protected void nextAcmv7104()
	{
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		acmvrevIO.setFunction(varcom.nextr);
		/*  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            */
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			fatalErrors9000();
		}
		if (isNE(acmvrevIO.getRldgcoy(), reverserec.company)
		|| isNE(acmvrevIO.getRdocnum(), reverserec.chdrnum)
		|| isNE(acmvrevIO.getTranno(), reverserec.tranno)
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void getDescription8000()
	{
		para8000();
	}

protected void para8000()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setDesctabl(t1688);
		/* MOVE 'E'                    TO DESC-LANGUAGE.                */
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalErrors9000();
		}
		lifacmvrec.trandesc.set(descIO.getLongdesc());
	}

protected void fatalErrors9000()
	{
		fatalError9000();
		errorProg9004();
	}

protected void fatalError9000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg9004()
	{
		reverserec.statuz.set(varcom.bomb);
		/*EXIT*/
	}

protected void checkLoans10000()
	{
		/*START*/
		loanenqIO.setParams(SPACES);
		loanenqIO.setChdrcoy(reverserec.company);
		loanenqIO.setChdrnum(reverserec.chdrnum);
		loanenqIO.setLoanNumber(ZERO);
		/* MOVE BEGNH                  TO LOANENQ-FUNCTION.             */
		loanenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		loanenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		loanenqIO.setFormat(formatsInner.loanenqrec);
		while ( !(isEQ(loanenqIO.getStatuz(), varcom.endp))) {
			processLoans10100();
		}

		/*EXIT*/
	}

protected void processLoans10100()
	{
		start10100();
	}

protected void start10100()
	{
		SmartFileCode.execute(appVars, loanenqIO);
		if (isNE(loanenqIO.getStatuz(), varcom.oK)
		&& isNE(loanenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(loanenqIO.getParams());
			syserrrec.statuz.set(loanenqIO.getStatuz());
			fatalErrors9000();
		}
		/* If no Loan records found for this contract, then  EXIT section*/
		if (isNE(reverserec.company, loanenqIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, loanenqIO.getChdrnum())
		|| isEQ(loanenqIO.getStatuz(), varcom.endp)) {
			loanenqIO.setStatuz(varcom.endp);
			return ;
		}
		/* get here, so we have found a Loan record.......*/
		/*  does it need changing ???*/
		/*  check this by comparing the Last Tranno on the Loan record*/
		/* IF it matches, we will update record, otherwise we will NEXTR*/
		if (isEQ(loanenqIO.getLastTranno(), wsaaReversedTranno)) {
			obtainInterestDates10200();
			loanenqIO.setLastIntBillDate(wsaaLstInterestDate);
			loanenqIO.setNextIntBillDate(wsaaNxtInterestDate);
			loanenqIO.setValidflag("1");
			loanenqIO.setLastTranno(ZERO);
			/*     MOVE REWRT              TO LOANENQ-FUNCTION              */
			loanenqIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, loanenqIO);
			if (isNE(loanenqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(loanenqIO.getParams());
				syserrrec.statuz.set(loanenqIO.getStatuz());
				fatalErrors9000();
			}
		}
		loanenqIO.setFunction(varcom.nextr);
	}

protected void obtainInterestDates10200()
	{
		start10200();
	}

protected void start10200()
	{
		readT5645Table10300();
		/* Use entries on the T5645 record read to get Loan interest       */
		/*  ACMVs for this loan number.                                    */
		if (isEQ(loanenqIO.getLoanType(), "P")) {
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstype.set(t5645rec.sacstype01);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode02);
			wsaaSacstype.set(t5645rec.sacstype02);
		}
		wsaaRldgacct.set(SPACES);
		wsaaLstInterestDate.set(ZERO);
		wsaaChdrnum.set(loanenqIO.getChdrnum());
		wsaaLoanNumber.set(loanenqIO.getLoanNumber());
		/*  We will begin reading on the ACMV original surrender           */
		/*  transactions and read backwards to get the movements           */
		/*  before the original surrender (nextp)                          */
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			searchAcmvRecs10400();
		}

		/*  if any interest records for the loan being processed           */
		/*  (before the surrender movements) i.e there is a value          */
		/*  in WSAA-LST-INTEREST-DATE use this otherwise no interest       */
		/*  has been calculated since the loan start date                  */
		if (isEQ(wsaaLstInterestDate, ZERO)) {
			wsaaLstInterestDate.set(loanenqIO.getLoanStartDate());
		}
		/*  calculate next interest date from the last interest            */
		/*  date retrieved (i.e the effective date on the relevant         */
		/*  acmv                                                           */
		readT6633Table10500();
		calcNextInterestDate10600();
	}

protected void readT5645Table10300()
	{
		start10300();
	}

protected void start10300()
	{
		/*  Read the accounting rules table                                */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(loanenqIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalErrors9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void searchAcmvRecs10400()
	{
		start10400();
	}

protected void start10400()
	{
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getStatuz());
			fatalErrors9000();
		}
		if (isNE(acmvrevIO.getRldgcoy(), loanenqIO.getChdrcoy())
		|| isNE(acmvrevIO.getRdocnum(), loanenqIO.getChdrnum())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
			return ;
		}
		/*  Read the accounting rules table                                */
		wsaaAcmvTrcde.set(acmvrevIO.getBatctrcde());
		if (isLT(acmvrevIO.getTranno(), reverserec.tranno)
		&& isEQ(acmvrevIO.getSacscode(), wsaaSacscode)
		&& isEQ(acmvrevIO.getSacstyp(), wsaaSacstype)
		&& isEQ(acmvrevIO.getRldgacct(), wsaaRldgacct)
		&& (loanInterestBilling.isTrue()
		|| loanInterestCapital.isTrue())) {
			wsaaLstInterestDate.set(acmvrevIO.getEffdate());
			acmvrevIO.setStatuz(varcom.endp);
		}
		else {
			acmvrevIO.setFunction(varcom.nextp);
		}
	}

protected void readT6633Table10500()
	{
		start10500();
	}

protected void start10500()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(loanenqIO.getChdrcoy());
		itdmIO.setItemtabl(t6633);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(wsaaLstInterestDate);
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalErrors9000();
		}
		if (isNE(itdmIO.getItemcoy(), loanenqIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t6633)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrlifIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e723);
			fatalErrors9000();
		}
		t6633rec.t6633Rec.set(itdmIO.getGenarea());
	}

protected void calcNextInterestDate10600()
	{
		start10600();
	}

protected void start10600()
	{
		/* Check the interest  details on T6633 in the following           */
		/*  order: i) Calculate interest on Loan anniv ... Y/N             */
		/*        ii) Calculate interest on Policy anniv.. Y/N             */
		/*       iii) Check Int freq & whether a specific Day is chosen    */
		wsaaLoanDate.set(loanenqIO.getLoanStartDate());
		wsaaEffdate.set(wsaaLstInterestDate);
		wsaaContractDate.set(chdrlifIO.getOccdate());
		/* Check for loan anniversary flag set                             */
		/* IF set,                                                         */
		/*    set next interest billing date to be on the next loan anniv  */
		/*     date after the Effective date we are using now.             */
		if (isEQ(t6633rec.loanAnnivInterest, "Y")) {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.intDate1.set(loanenqIO.getLoanStartDate());
			datcon4rec.frequency.set("01");
			datcon4rec.freqFactor.set(1);
			while ( !(isGT(datcon4rec.intDate2, wsaaEffdate))) {
				callDatcon410700();
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}

			wsaaNxtInterestDate.set(datcon4rec.intDate2);
			return ;
		}
		/* Check for contract anniversary flag set                         */
		/* IF set,                                                         */
		/*    set next interest billing date to be on the next contract    */
		/*     anniversary date after the Effective date we are using now. */
		if (isEQ(t6633rec.policyAnnivInterest, "Y")) {
			datcon4rec.datcon4Rec.set(SPACES);
			datcon4rec.intDate2.set(ZERO);
			datcon4rec.intDate1.set(chdrlifIO.getOccdate());
			datcon4rec.frequency.set("01");
			datcon4rec.freqFactor.set(1);
			while ( !(isGT(datcon4rec.intDate2, wsaaEffdate))) {
				callDatcon410700();
				datcon4rec.intDate1.set(datcon4rec.intDate2);
			}

			wsaaNxtInterestDate.set(datcon4rec.intDate2);
			return ;
		}
		/* Get here so the next interest calc. date isn't based on loan    */
		/*  or contract anniversarys.                                      */
		wsaaNewDate.set(ZERO);
		/* check if table T6633 has a fixed day of the month specified     */
		/* ...if not, use the Loan day                                     */
		wsaaDayCheck.set(t6633rec.interestDay);
		wsaaMonthCheckInner.wsaaMonthCheck.set(wsaaLoanMonth);
		if (!daysLessThan28.isTrue()) {
			dateSet10800();
		}
		else {
			if (isNE(t6633rec.interestDay, ZERO)) {
				wsaaNewDay.set(t6633rec.interestDay);
			}
			else {
				wsaaNewDay.set(wsaaLoanDay);
			}
		}
		wsaaNewYear.set(wsaaLoanYear);
		wsaaNewMonth.set(wsaaLoanMonth);
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate2.set(ZERO);
		datcon4rec.intDate1.set(wsaaNewDate);
		datcon4rec.freqFactor.set(1);
		/* check if table T6633 has a fixed frequency for interest calcs,  */
		/* ...if not, use 1 year as the default interest calc. frequency.  */
		if (isEQ(t6633rec.interestFrequency, SPACES)) {
			datcon4rec.frequency.set("01");
		}
		else {
			datcon4rec.frequency.set(t6633rec.interestFrequency);
		}
		while ( !(isGT(datcon4rec.intDate2, wsaaEffdate))) {
			callDatcon410700();
			datcon4rec.intDate1.set(datcon4rec.intDate2);
		}

		wsaaNxtInterestDate.set(datcon4rec.intDate2);
	}

protected void callDatcon410700()
	{
		/*START*/
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			fatalErrors9000();
		}
		/*EXIT*/
	}

protected void dateSet10800()
	{
		/*START*/
		/* we have to check that the date we are going to call Datcon4     */
		/*  with is a valid from-date... ie IF the interest/capn day in    */
		/*  T6633 is > 28, we have to make sure the from-date isn't        */
		/*  something like 31/02/nnnn or 31/06/nnnn                        */
		if (wsaaMonthCheckInner.january.isTrue()
		|| wsaaMonthCheckInner.march.isTrue()
		|| wsaaMonthCheckInner.may.isTrue()
		|| wsaaMonthCheckInner.july.isTrue()
		|| wsaaMonthCheckInner.august.isTrue()
		|| wsaaMonthCheckInner.october.isTrue()
		|| wsaaMonthCheckInner.december.isTrue()) {
			wsaaNewDay.set(wsaaDayCheck);
			return ;
		}
		if (wsaaMonthCheckInner.april.isTrue()
		|| wsaaMonthCheckInner.june.isTrue()
		|| wsaaMonthCheckInner.september.isTrue()
		|| wsaaMonthCheckInner.november.isTrue()) {
			if (daysInJan.isTrue()) {
				wsaaNewDay.set(wsaaAprilDays);
			}
			else {
				wsaaNewDay.set(wsaaDayCheck);
			}
			return ;
		}
		if (wsaaMonthCheckInner.february.isTrue()) {
			wsaaNewDay.set(wsaaFebruaryDays);
		}
		/*EXIT*/
		/**A000-STATISTICS SECTION.                                         */
		/**A010-START.                                                      */
		/***** MOVE REVE-BATCCOY           TO LIFS-BATCCOY.                 */
		/***** MOVE REVE-BATCBRN           TO LIFS-BATCBRN.                 */
		/***** MOVE REVE-BATCACTYR         TO LIFS-BATCACTYR.               */
		/***** MOVE REVE-BATCACTMN         TO LIFS-BATCACTMN.               */
		/***** MOVE REVE-BATCTRCDE         TO LIFS-BATCTRCDE.               */
		/***** MOVE REVE-BATCBATCH         TO LIFS-BATCBATCH.               */
		/***** MOVE CHDRLIF-CHDRCOY        TO LIFS-CHDRCOY.                 */
		/***** MOVE CHDRLIF-CHDRNUM        TO LIFS-CHDRNUM.                 */
		/***** MOVE CHDRLIF-TRANNO         TO LIFS-TRANNO.                  */
		/***** MOVE REVE-TRANNO            TO LIFS-TRANNOR.                 */
		/***** MOVE SPACES                 TO LIFS-AGNTNUM.                 */
		/***** MOVE SPACES                 TO LIFS-OLD-AGNTNUM.             */
		/***** CALL 'LIFSTTR'              USING LIFS-LIFSTTR-REC.          */
		/***** IF LIFS-STATUZ              NOT = O-K                        */
		/*****    MOVE LIFS-LIFSTTR-REC    TO SYSR-PARAMS                   */
		/*****    MOVE LIFS-STATUZ         TO SYSR-STATUZ                   */
		/*****    PERFORM 9000-FATAL-ERRORS.                                */
		/**A090-EXIT.                                                       */
		/***** EXIT.                                                        */
	}
/*
 * Class transformed  from Data Structure WSAA-MONTH-CHECK--INNER
 */
private static final class WsaaMonthCheckInner {

	private ZonedDecimalData wsaaMonthCheck = new ZonedDecimalData(2, 0).setUnsigned();
	private Validator january = new Validator(wsaaMonthCheck, 1);
	private Validator february = new Validator(wsaaMonthCheck, 2);
	private Validator march = new Validator(wsaaMonthCheck, 3);
	private Validator april = new Validator(wsaaMonthCheck, 4);
	private Validator may = new Validator(wsaaMonthCheck, 5);
	private Validator june = new Validator(wsaaMonthCheck, 6);
	private Validator july = new Validator(wsaaMonthCheck, 7);
	private Validator august = new Validator(wsaaMonthCheck, 8);
	private Validator september = new Validator(wsaaMonthCheck, 9);
	private Validator october = new Validator(wsaaMonthCheck, 10);
	private Validator november = new Validator(wsaaMonthCheck, 11);
	private Validator december = new Validator(wsaaMonthCheck, 12);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData mathclmrec = new FixedLengthStringData(10).init("MATHCLMREC");
	private FixedLengthStringData matdclmrec = new FixedLengthStringData(10).init("MATDCLMREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
	private FixedLengthStringData loanenqrec = new FixedLengthStringData(10).init("LOANENQREC");
	private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData incrselrec = new FixedLengthStringData(10).init("INCRSELREC");
}
}
