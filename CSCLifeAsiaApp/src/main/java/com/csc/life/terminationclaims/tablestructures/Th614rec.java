package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:38
 * Description:
 * Copybook name: TH614REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th614rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th614Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData compfreq = new FixedLengthStringData(2).isAPartOf(th614Rec, 0);
  	public FixedLengthStringData interestFrequency = new FixedLengthStringData(2).isAPartOf(th614Rec, 2);
  	public ZonedDecimalData intRate = new ZonedDecimalData(8, 5).isAPartOf(th614Rec, 4);
  	public FixedLengthStringData filler = new FixedLengthStringData(488).isAPartOf(th614Rec, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th614Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th614Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}