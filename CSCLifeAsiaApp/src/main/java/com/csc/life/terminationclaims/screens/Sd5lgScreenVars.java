package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sd5lg
 * 
 * @version 1.0 generated on 16/01/19 06:52
 * @author Quipoz
 */
public class Sd5lgScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(88);
	public FixedLengthStringData dataFields = new FixedLengthStringData(24).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields, 1);
	public FixedLengthStringData submnuprog = DD.submnuprog.copy().isAPartOf(dataFields, 11);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields, 16);// ICIL-254
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 24);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData submnuprogErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 40);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] submnuprogOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sd5lgscreenWritten = new LongData(0);
	public LongData Sd5lgprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sd5lgScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,
				new String[] { "02", null, "-02", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(actionOut,
				new String[] { "04", null, "-04", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(effdateOut,
				new String[] { "03", null, "-03", null, null, null, null, null, null, null, null, null });
		screenFields = new BaseData[] { submnuprog, chdrsel, action, effdate };
		screenOutFields = new BaseData[][] { submnuprogOut, chdrselOut, actionOut, effdateOut };
		screenErrFields = new BaseData[] { submnuprogErr, chdrselErr, actionErr, effdateErr };
		screenDateFields = new BaseData[] { effdate };
		screenDateErrFields = new BaseData[] { effdateErr };
		screenDateDispFields = new BaseData[] { effdateDisp };

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = Sd5lgscreen.class;
		protectRecord = Sd5lgprotect.class;
	}

}
