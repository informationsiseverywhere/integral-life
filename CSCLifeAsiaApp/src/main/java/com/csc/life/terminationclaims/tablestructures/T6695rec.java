package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:15
 * Description:
 * Copybook name: T6695REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6695rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6695Rec = new FixedLengthStringData(508);
  	public FixedLengthStringData pctincs = new FixedLengthStringData(60).isAPartOf(t6695Rec, 0);
  	public ZonedDecimalData[] pctinc = ZDArrayPartOfStructure(12, 5, 2, pctincs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(pctincs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData pctinc01 = new ZonedDecimalData(5, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData pctinc02 = new ZonedDecimalData(5, 2).isAPartOf(filler, 5);
  	public ZonedDecimalData pctinc03 = new ZonedDecimalData(5, 2).isAPartOf(filler, 10);
  	public ZonedDecimalData pctinc04 = new ZonedDecimalData(5, 2).isAPartOf(filler, 15);
  	public ZonedDecimalData pctinc05 = new ZonedDecimalData(5, 2).isAPartOf(filler, 20);
  	public ZonedDecimalData pctinc06 = new ZonedDecimalData(5, 2).isAPartOf(filler, 25);
  	public ZonedDecimalData pctinc07 = new ZonedDecimalData(5, 2).isAPartOf(filler, 30);
  	public ZonedDecimalData pctinc08 = new ZonedDecimalData(5, 2).isAPartOf(filler, 35);
  	public ZonedDecimalData pctinc09 = new ZonedDecimalData(5, 2).isAPartOf(filler, 40);
  	public ZonedDecimalData pctinc10 = new ZonedDecimalData(5, 2).isAPartOf(filler, 45);
  	public ZonedDecimalData pctinc11 = new ZonedDecimalData(5, 2).isAPartOf(filler, 50);
  	public ZonedDecimalData pctinc12 = new ZonedDecimalData(5, 2).isAPartOf(filler, 55);
  	public FixedLengthStringData tupyrs = new FixedLengthStringData(36).isAPartOf(t6695Rec, 60);
  	public FixedLengthStringData[] tupyr = FLSArrayPartOfStructure(12, 3, tupyrs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(tupyrs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData tupyr01 = new FixedLengthStringData(3).isAPartOf(filler1, 0);
  	public FixedLengthStringData tupyr02 = new FixedLengthStringData(3).isAPartOf(filler1, 3);
  	public FixedLengthStringData tupyr03 = new FixedLengthStringData(3).isAPartOf(filler1, 6);
  	public FixedLengthStringData tupyr04 = new FixedLengthStringData(3).isAPartOf(filler1, 9);
  	public FixedLengthStringData tupyr05 = new FixedLengthStringData(3).isAPartOf(filler1, 12);
  	public FixedLengthStringData tupyr06 = new FixedLengthStringData(3).isAPartOf(filler1, 15);
  	public FixedLengthStringData tupyr07 = new FixedLengthStringData(3).isAPartOf(filler1, 18);
  	public FixedLengthStringData tupyr08 = new FixedLengthStringData(3).isAPartOf(filler1, 21);
  	public FixedLengthStringData tupyr09 = new FixedLengthStringData(3).isAPartOf(filler1, 24);
  	public FixedLengthStringData tupyr10 = new FixedLengthStringData(3).isAPartOf(filler1, 27);
  	public FixedLengthStringData tupyr11 = new FixedLengthStringData(3).isAPartOf(filler1, 30);
  	public FixedLengthStringData tupyr12 = new FixedLengthStringData(3).isAPartOf(filler1, 33);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(404).isAPartOf(t6695Rec, 96, FILLER);
  	public FixedLengthStringData contitem = new FixedLengthStringData(8).isAPartOf(t6695Rec, 500);


	public void initialize() {
		COBOLFunctions.initialize(t6695Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6695Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}