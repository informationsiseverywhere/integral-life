package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6597screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6597ScreenVars sv = (S6597ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6597screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6597ScreenVars screenVars = (S6597ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.durmnth01.setClassString("");
		screenVars.cpstat01.setClassString("");
		screenVars.crstat01.setClassString("");
		screenVars.premsubr01.setClassString("");
		screenVars.durmnth02.setClassString("");
		screenVars.premsubr02.setClassString("");
		screenVars.crstat02.setClassString("");
		screenVars.cpstat02.setClassString("");
		screenVars.durmnth03.setClassString("");
		screenVars.premsubr03.setClassString("");
		screenVars.crstat03.setClassString("");
		screenVars.cpstat03.setClassString("");
		screenVars.premsubr04.setClassString("");
		screenVars.crstat04.setClassString("");
		screenVars.cpstat04.setClassString("");
	}

/**
 * Clear all the variables in S6597screen
 */
	public static void clear(VarModel pv) {
		S6597ScreenVars screenVars = (S6597ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.durmnth01.clear();
		screenVars.cpstat01.clear();
		screenVars.crstat01.clear();
		screenVars.premsubr01.clear();
		screenVars.durmnth02.clear();
		screenVars.premsubr02.clear();
		screenVars.crstat02.clear();
		screenVars.cpstat02.clear();
		screenVars.durmnth03.clear();
		screenVars.premsubr03.clear();
		screenVars.crstat03.clear();
		screenVars.cpstat03.clear();
		screenVars.premsubr04.clear();
		screenVars.crstat04.clear();
		screenVars.cpstat04.clear();
	}
}
