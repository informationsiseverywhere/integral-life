package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:22
 * Description:
 * Copybook name: COVRCLRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrclrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrclrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrclrKey = new FixedLengthStringData(64).isAPartOf(covrclrFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrclrChdrcoy = new FixedLengthStringData(1).isAPartOf(covrclrKey, 0);
  	public FixedLengthStringData covrclrChdrnum = new FixedLengthStringData(8).isAPartOf(covrclrKey, 1);
  	public PackedDecimalData covrclrTranno = new PackedDecimalData(5, 0).isAPartOf(covrclrKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(covrclrKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrclrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrclrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}