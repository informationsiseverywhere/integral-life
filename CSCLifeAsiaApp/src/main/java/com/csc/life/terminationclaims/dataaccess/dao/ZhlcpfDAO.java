package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.Date;
import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Zhlcpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;


public interface ZhlcpfDAO extends BaseDAO<Zhlcpf>{
	
	public boolean insertIntoZhlcpf(Zhlcpf zhlcpfDAO);
	public List<Zhlcpf> readZhlcpf(Zhlcpf zhlcpf);
	public List<Zhlcpf> readZhlcpfData(Zhlcpf zhlcpf);
	public boolean updateIntoZhlcpf(Zhlcpf zhlcpf);

	
}
