/*

 * File: Pr679any.java
 * Date: 30 August 2009 1:54:06
 * Author: Quipoz Limited
 * 
 * Class transformed from PR679ANY.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*  This  subroutine  is  called  from  OPTSWCH Table T1661. It
*  determines whether any ANNY records exist for the component
*  being enquired upon in PR679.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr679any extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("PR679ANY");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String covrmjarec = "COVRMJAREC";
	private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
		/*Annuity Details*/
	private AnnyTableDAM annyIO = new AnnyTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		errorProg610
	}

	public Pr679any() {
		super();
	}

public void mainline(Object... parmArray)
	{
		wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 1);
		optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		try {
			performs0010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void performs0010()
	{
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
		covrpf = covrDao.getCacheObject(covrpf);
		if(covrpf==null)
		{
			return;
		}
		annyIO.setChdrcoy(covrpf.getChdrcoy());
		annyIO.setChdrnum(covrpf.getChdrnum());
		annyIO.setLife(covrpf.getLife());
		annyIO.setCoverage(covrpf.getCoverage());
		annyIO.setRider(covrpf.getRider());
		annyIO.setPlanSuffix(ZERO);
		annyIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		annyIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		annyIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(),varcom.oK)
		&& isNE(annyIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(annyIO.getParams());
			syserrrec.statuz.set(annyIO.getStatuz());
			fatalError600();
		}
		if (isNE(annyIO.getChdrcoy(),covrpf.getChdrcoy())
				|| isNE(annyIO.getChdrnum(),covrpf.getChdrnum())
				|| isNE(annyIO.getLife(),covrpf.getLife())
				|| isNE(annyIO.getCoverage(),covrpf.getCoverage())
				|| isNE(annyIO.getRider(),covrpf.getRider())
				|| isEQ(annyIO.getStatuz(),varcom.endp)) {
					annyIO.setStatuz(varcom.endp);
				}
		if (isEQ(annyIO.getStatuz(),varcom.oK)) {
			wsaaChkpStatuz.set("DFND");
		}
		else {
			wsaaChkpStatuz.set("NFND");
			goTo(GotoLabel.exit090);
		}
		covrmjaIO.setChdrcoy(covrpf.getChdrcoy());
		covrmjaIO.setChdrnum(covrpf.getChdrnum());
		covrmjaIO.setLife(covrpf.getLife());
		covrmjaIO.setCoverage(covrpf.getCoverage());
		covrmjaIO.setRider(covrpf.getRider());
		covrmjaIO.setPlanSuffix(covrpf.getPlanSuffix());
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit090);
		}
		covrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		wsaaChkpStatuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
