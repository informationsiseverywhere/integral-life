/*
 * File: P6306.java
 * Date: 30 August 2009 0:42:20
 * Author: Quipoz Limited
 * 
 * Class transformed from P6306.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT; //ICIL-297
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO; //ILIFE-7772
import com.csc.fsu.general.procedures.Datcon3; //ICIL-297
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Datcon3rec; //ICIL-297
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.general.procedures.Agecalc;  //ICIL-297
import com.csc.life.general.recordstructures.Agecalcrec; //ICIL-297
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;//ICIL-297
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;//ICIL-297
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO; //ICIL-297
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;  //ICIL-297
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.Td5gfrec;   //ICIL-297
import com.csc.life.terminationclaims.dataaccess.ChdrptsTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.screens.S6306ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILIFE-7772
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator; //ICIL-297
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData; //ICIL-297
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*     Part Surrender Sub Menu.
*
* Validation
* ----------
*
*  Key 1 - Contract number (CHDRPTS)
*
*       Y = mandatory, must exist on file.
*            - CHDRPTS  -  Life  new  business  contract  header
*                 logical view.
*            - must be correct status for transaction (T5679).
*            - MUST BE CORRECT BRANCH.
*
*       N = IRREVELANT.
*
*       Blank = irrelevant.
*
*
* Updating
* --------
*
*  Set up WSSP-FLAG:
*       - "I" for actions I, otherwise "M".
*  For enqury transactions ("I"), nothing else is required.
*
*  For maintenance transactions  (WSSP-FLAG  set to  'M' above),
*  soft lock the contract header (call SFTLOCK). If the contract
*  is already locked, display an error message.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
*  Include option for traditional part surrender.  This option
*  is not allowed if the paid-to-date is not = the billed-to
*  date.
*
*****************************************************************
* </pre>
*/
public class P6306 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	//public int numberOfParameters = 0;//ILIFE-7772 this came in Findbugs
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6306");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private String wsaaPsurrOk = "";
	private FixedLengthStringData wsaaFreelookBasisChn = new FixedLengthStringData(1); //ICIL-297
	private Validator issueDateChnBasis = new Validator(wsaaFreelookBasisChn, "1"); //ICIL-297
	private Validator ackBasis = new Validator(wsaaFreelookBasisChn, "2"); //ICIL-297
	private Validator deemedBasis = new Validator(wsaaFreelookBasisChn, "3"); //ICIL-297
	private Validator minAckDeemedBasis = new Validator(wsaaFreelookBasisChn, "4"); //ICIL-297
	private ZonedDecimalData wsaaFreelookDate = new ZonedDecimalData(8, 0).setUnsigned(); //ICIL-297
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).setUnsigned(); //ICIL-297
	private ZonedDecimalData wsaaTdayno = new ZonedDecimalData(3, 0).setUnsigned(); //ICIL-297
	    /* TABLES */
	private static final String td5gf = "TD5GF";  // ICIL-297
		/* ERRORS */
	private static final String e070 = "E070";
	private static final String f910 = "F910";
	private static final String e073 = "E073";
	private static final String e544 = "E544";
	private static final String e455 = "E455";
	private static final String e767 = "E767";
	private static final String f918 = "F918";
	private static final String e662 = "E662";
	private static final String g008 = "G008";
	private static final String rlcb = "RLCB";  //ICIL-297
	private static final String rlaj = "RLAJ";  //ICIL-297
	private static final String rrgi = "RRGI";  //ICIL-297
	private static final String f073 = "F073";  //ICIL-254
	private static final String f616 = "F616";  //ICIL-254
		/* FORMATS */
	protected static final String chdrptsrec = "CHDRPTSREC";
	protected static final String chdrsurrec = "CHDRSURREC";
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String chdrenqrec = "CHDRENQREC";
	protected static final String covrmjarec = "COVRMJAREC";
		/* Dummy user wssp. Replace with required version.
		    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();//ILIFE-7968
		/*Contract Header File - Major Alts*/
//	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Contract Header Details - Part Surr.*/
	protected ChdrptsTableDAM chdrptsIO = new ChdrptsTableDAM();//ILIFE-7968
		/*Contract Header - Surrenders*/
	protected ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();//ILIFE-7968
		/*Coverage/Rider details - Major Alts*/
	protected CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private Td5gfrec td5gfrec = new Td5gfrec();      // ICIL-297
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec(); //ICIL-297
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Agecalcrec agecalcrec = new Agecalcrec(); //ICIL-297
	private S6306ScreenVars sv = getLScreenVars();
	private CovrpfDAO covrSurdao =getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class); //ICIL-297
	private Itempf itempf = null; //ICIL-297
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); //ICIL-297
	private Hpadpf hpadpf; //ICIL-297
	private Map<String, List<Itempf>> t5687Map;
	private Covrpf covrSur=null;
	private List<Covrpf> covrSurList=null;
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class); //ICIL-297
	private Lifepf lifepf = new Lifepf(); //ICIL-297
	boolean cscom004Permission = false; // ICIL-297
	//ILIFE-6594 wli31
	//ILIFE-7772 start
	protected Chdrpf chdrpf = new Chdrpf();
	/*protected Chdrpf chdrsurpf = new Chdrpf();
	protected Chdrpf chdrenqpf = new Chdrpf();
	protected Chdrpf chdrmjapf = new Chdrpf();*/
//	protected Covrpf covrmjapf = new Covrpf();
	protected ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO" , CovrpfDAO.class);
	//ILIFE-7772 end 
	public boolean tradpartsurFlag = false;
	private Map<String, List<Itempf>> t5540Map;
	private T5540rec t5540rec = new T5540rec();
	private static final String rrx4 = "RRX4";
	private static final String rrx5 = "RRX5";
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		validateKey22220, 
		exit2290, 
		search2510, 
		loopCovrsur2650, 
		exit2690, 
		exit12090, 
		preKeeps3060, 
		keeps3070, 
		batching3080, 
		exit3090
	}

	public P6306() {
		super();
		screenVars = sv;
		new ScreenModel("S6306", AppVars.getInstance(), sv);
	}

	protected S6306ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6306ScreenVars.class);
	}
	
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		cscom004Permission  = FeaConfg.isFeatureExist("2", "CSCOM004", appVars, "IT");     // ICIL-297
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		if (isEQ(wsaaToday,0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		/*EXIT*/
		tradpartsurFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "SUSUR006", appVars, "IT");
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateEffdate2020();//ICIL-254
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'S6306IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6306-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
				if(isNE(sv.chdrselErr, SPACES)) {//ILB-1164
					return;
				}
				if (isEQ(scrnparams.action,"A")) {
					chkValidAction2300();
				}
				else {
					if (isEQ(sv.chdrselErr,SPACES)
					&& isEQ(scrnparams.action,"B")) {
					    /*ICIL-297 start*/
						if(cscom004Permission){
							freelookCheckCHN();
						}
					    /*ICIL-297 end*/
						if(tradpartsurFlag)
							chkValidAction2300();
						chkChdrDates2700();
					}
				}
			}
			else {
				verifyBatchControl2900();
			}
		}
	}

protected void validateEffdate2020() {
	if(isEQ(sv.effdate, varcom.vrcmMaxDate)) {
		sv.effdate.set(wsaaToday);
	}else {
		if(isGT(sv.effdate, wsaaToday)) {
			sv.effdateErr.set(f073);
		}else {
			if(isLT(sv.effdate, chdrsurIO.getOccdate()))//ILIFE-7968
				{
				sv.effdateErr.set(f616);
			}
		}
	}
	wsspcomn.currfrom.set(sv.effdate);
}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
	
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateKey12210();
				case validateKey22220: 
					validateKey22220();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1,SPACES)) {
			goTo(GotoLabel.validateKey22220);
		}
		//ILIFE-7968 start
		chdrptsIO.setChdrcoy(wsspcomn.company);
		/*                                 CHDRSUR-CHDRCOY.              */
		chdrptsIO.setChdrnum(sv.chdrsel);
		chdrptsIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel,SPACES)) {
			SmartFileCode.execute(appVars, chdrptsIO);
		}
		else {
			chdrptsIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrptsIO.getStatuz(),varcom.oK)
		&& isNE(chdrptsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrptsIO.getStatuz(),varcom.mrnf)
		&& isEQ(subprogrec.key1,"Y")) {
			sv.chdrselErr.set(e544);
			goTo(GotoLabel.exit2290);//ILB-1164
		}
		if (isEQ(chdrptsIO.getStatuz(),varcom.oK)
		&& isEQ(subprogrec.key1,"N")
		&& isEQ(sv.chdrselErr,SPACES)) {
			sv.chdrselErr.set(f918);
		}
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				goTo(GotoLabel.exit2290);
			}
		}
		chdrsurIO.setChdrcoy(wsspcomn.company);
		chdrsurIO.setChdrnum(sv.chdrsel);
		chdrsurIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel,SPACES)) {
			SmartFileCode.execute(appVars, chdrsurIO);
		}
		else {
			chdrsurIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrsurIO.getStatuz(),varcom.oK)
		&& isNE(chdrsurIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)) {
			checkStatus2400();
		}
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)
		&& isNE(wsspcomn.branch,chdrptsIO.getCntbranch())) {
			sv.chdrselErr.set(e455);
		}
		//ILIFE-7968 end
	}

protected void validateKey22220()
	{
		if (isEQ(subprogrec.key2,SPACES)
		|| isEQ(subprogrec.key2,"N")) {
			return ;
		}
		//ILIFE-7968 start
		chdrptsIO.setChdrcoy(wsspcomn.company);
		chdrptsIO.setChdrnum(sv.chdrsel);
		chdrptsIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel,SPACES)) {
			SmartFileCode.execute(appVars, chdrptsIO);
		}
		else {
			chdrptsIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrptsIO.getStatuz(),varcom.oK)
		&& isNE(chdrptsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrptsIO.getStatuz(),varcom.mrnf)) {
			sv.chdrselErr.set(e544);
		}
		//ILIFE-7968 end
	}

protected void chkValidAction2300()
	{
	    /*ICIL-297 start*/
		if(cscom004Permission){
			freelookCheckCHN();
		}
	    /*ICIL-297 end*/
		/*INIT*/
		wsaaPsurrOk = "N";
		chkValidPsurr2600();
		if (isEQ(wsaaPsurrOk,"N")
		&& isEQ(sv.chdrselErr,SPACES)) {
			sv.chdrselErr.set(e662);
		}
		/*EXIT*/
	}

//*CIL-297 start*/
protected void freelookCheckCHN() 	
{
	/* Validate the cooling off period for freelook cancellation       */
	/* based on the rule defined in TD5GF.                             */
	getCoolOff();
	wsaaFreelookBasisChn.set(td5gfrec.ratebas);       
	if (!(issueDateChnBasis.isTrue()
	|| ackBasis.isTrue()
	|| deemedBasis.isTrue()		
	|| minAckDeemedBasis.isTrue())) {
		sv.chdrselErr.set(rlcb);
		return ;
	}
	hpadpf=hpadpfDAO.getHpadData(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString());//ILIFE-7968
	/* issue date*/
	if (issueDateChnBasis.isTrue()) {
		wsaaFreelookDate.set(hpadpf.getHissdte());
	}
	
	/* ack */
	if (ackBasis.isTrue()) {
		if (isNE(hpadpf.getPackdate(), varcom.vrcmMaxDate)
				&& isNE(hpadpf.getPackdate(), ZERO)) {
					wsaaFreelookDate.set(hpadpf.getPackdate());
				}
				else {
					sv.chdrselErr.set(rlaj);
					return ;
				}
	}
	
	/* deemed */
	if (deemedBasis.isTrue()) {
		if (isNE(hpadpf.getDeemdate(), varcom.vrcmMaxDate)
				&& isNE(hpadpf.getDeemdate(), ZERO)) {
					wsaaFreelookDate.set(hpadpf.getDeemdate());
				}
				else {
					sv.chdrselErr.set(rlaj);
					return ;
				}
	}
	
	/* min- ack or deemed */
	if (minAckDeemedBasis.isTrue()) {
		if (isGT(hpadpf.getDeemdate(), hpadpf.getPackdate())) {
			if (isNE(hpadpf.getPackdate(), varcom.vrcmMaxDate)
					&& isNE(hpadpf.getPackdate(), ZERO)) {
						wsaaFreelookDate.set(hpadpf.getPackdate());
					}
		}
		else {
			if (isNE(hpadpf.getDeemdate(), varcom.vrcmMaxDate)
					&& isNE(hpadpf.getDeemdate(), ZERO)) {
						wsaaFreelookDate.set(hpadpf.getDeemdate());
					}
					else {
						sv.chdrselErr.set(rlaj);
						return ;
					}
		}

	}
	if (isEQ(wsaaFreelookDate, varcom.vrcmMaxDate)) {	//ILB-1164
		return;
	}
	datcon3rec.datcon3Rec.set(SPACES);
	datcon3rec.intDate1.set(wsaaFreelookDate);
	datcon3rec.intDate2.set(wsaaToday);
	datcon3rec.frequency.set("DY");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}

	if (isLT(datcon3rec.freqFactor, wsaaTdayno)) {
		sv.chdrselErr.set(rrgi);
	}
}   

protected void getCoolOff() 
{
	lifepf = lifepfDAO.getLifeRecord(chdrsurIO.getChdrcoy().toString(), chdrsurIO.getChdrnum().toString(), "01", "00");//ILIFE-7968
	if (lifepf== null ) {
		syserrrec.params.set(lifepf);
		fatalError600();
	}
	
	initialize(agecalcrec.agecalcRec);
	agecalcrec.function.set("CALCP");
	agecalcrec.language.set(wsspcomn.language);
	agecalcrec.intDate1.set(lifepf.getCltdob());
	agecalcrec.intDate2.set(wsaaToday);
	agecalcrec.company.set(wsspcomn.fsuco);
	callProgram(Agecalc.class, agecalcrec.agecalcRec);
	if (isNE(agecalcrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(agecalcrec.statuz);
		fatalError600();
	}
	wsaaAnb.set(agecalcrec.agerating);

	readTd5gf1100();

		if (isEQ(lifepf.getCltsex(),td5gfrec.gender01)  && isGTE(wsaaAnb,td5gfrec.frmage01) && isLTE(wsaaAnb,td5gfrec.toage01)) {
			td5gfrec.cooloff.set(td5gfrec.coolingoff1);
			wsaaTdayno.set(td5gfrec.coolingoff);
			
		}
		else {
			if (isEQ(lifepf.getCltsex(),td5gfrec.gender02) && isGTE(wsaaAnb,td5gfrec.frmage02) && isLTE(wsaaAnb,td5gfrec.toage02)) {
				td5gfrec.cooloff.set(td5gfrec.coolingoff2);
				wsaaTdayno.set(td5gfrec.coolingoff);
			}
			else {
				if (isEQ(lifepf.getCltsex(),td5gfrec.gender03) && isGTE(wsaaAnb,td5gfrec.frmage03) && isLTE(wsaaAnb,td5gfrec.toage03)) {
					td5gfrec.cooloff.set(td5gfrec.coolingoff3);
					wsaaTdayno.set(td5gfrec.coolingoff);
				}
				
				else {
					if (isEQ(lifepf.getCltsex(),td5gfrec.gender04) && isGTE(wsaaAnb,td5gfrec.frmage04) && isLTE(wsaaAnb,td5gfrec.toage04)) {
						td5gfrec.cooloff.set(td5gfrec.coolingoff4);
						wsaaTdayno.set(td5gfrec.coolingoff);
					}
				}
						
			}
		}
}

protected void readTd5gf1100() 
{
	
	
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(td5gf);/* IJTI-1523 */
	itempf.setItemitem(chdrsurIO.getCnttype().toString() + chdrsurIO.getSrcebus().toString());//ILIFE-7968
	itempf = itempfDAO.getItempfRecord(itempf);
	
	
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(td5gf);/* IJTI-1523 */
		itempf.setItemitem(chdrsurIO.getCnttype().toString() + "**");
		itempf = itempfDAO.getItempfRecord(itempf);	
	}
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(td5gf);/* IJTI-1523 */
		itempf.setItemitem("*****");
		itempf = itempfDAO.getItempfRecord(itempf);	
	}
	
	if (itempf != null) {
		td5gfrec.td5gfRec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	else {
		td5gfrec.td5gfRec.set(SPACES);
	}
	
}
/*ICIL-297 end*/

protected void checkStatus2400(){

	//ILIFE-6594 wli31
	List<Itempf> itempfList = itemDAO.getAllItemitem("IT", wsspcomn.company.toString().trim(), "T5679",subprogrec.transcd.toString().trim());
	if(!itempfList.isEmpty()){
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}else{
		syserrrec.params.set("t5679");
		fatalError600();
		}		

	wsaaSub.set(ZERO);
	lookForStat2500();
}

protected void lookForStat2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case search2510: 
					search2510();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void search2510()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,12)
		&& isEQ(sv.chdrselErr,SPACES)) {
			sv.chdrselErr.set(e767);
			wsspcomn.edterror.set("Y");
		}
		else {
			if (isNE(chdrptsIO.getStatcode(),t5679rec.cnRiskStat[wsaaSub.toInt()]))//ILIFE-7968
				{
				goTo(GotoLabel.search2510);
			}
		}
		/*EXIT*/
	}

protected void chkValidPsurr2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readCovrsur2610();
				case loopCovrsur2650: 
					readT56872670();
				case exit2690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readCovrsur2610()
	{
	covrSur=new Covrpf();
	covrSur.setChdrcoy(chdrsurIO.getChdrcoy().toString().trim());//ILIFE-7968
	covrSur.setChdrnum(chdrsurIO.getChdrnum().toString().trim());//ILIFE-7968
	covrSurList=covrSurdao.selectCovrSurData(covrSur);
	
	}

protected void readT56872670()
	{
	t5687Map = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "T5687");
		List<Itempf> itempfList = new ArrayList<Itempf>();
		Iterator<Covrpf> iterator = covrSurList.iterator();
		while (iterator.hasNext()){
			covrSur=iterator.next();
			String keyItemitem = covrSur.getCrtable().trim();
			if(t5687Map.containsKey(keyItemitem)){
				itempfList=t5687Map.get(keyItemitem);
				Iterator<Itempf> itemIterator = itempfList.iterator();
				while(itemIterator.hasNext()){
						Itempf itempf=itemIterator.next();
							if (itempf.getItmfrm().toString().equals(wsaaToday.toString().trim())) {
								t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
								break;
							}else{
								t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
							}
				}
				if (isNE(t5687rec.partsurr,SPACES)) {
					wsaaPsurrOk = "Y";
					if(tradpartsurFlag)	//ILIFE-8611
						readT5540();
					return ;
				}
			}
		
		}
		
	}

protected void readT5540() {
	t5540Map = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "T5540");
	Iterator<Covrpf> iterator = covrSurList.iterator();
	while (iterator.hasNext()){
		covrSur=iterator.next();
		if(isEQ(covrSur.getRider(),"00")) {	//IBPLIFE-2300
			String keyItemitem = covrSur.getCrtable().trim();
			if(!t5540Map.containsKey(keyItemitem) && isEQ(scrnparams.action,"A") && isEQ(sv.chdrselErr,SPACES))
				sv.chdrselErr.set(rrx4);
			else if(t5540Map.containsKey(keyItemitem) && isEQ(scrnparams.action,"B") && isEQ(sv.chdrselErr,SPACES))
				sv.chdrselErr.set(rrx5);
		}
	}
}

protected void chkChdrDates2700()
	{
		/*CHDR-DATES*/
	//ILIFE-7772 start
	chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
	//chdrmjapf.setFormat(chdrmjarec);
	if(chdrpf==null){
		fatalError600();
		return;
	}
	if (isNE(chdrpf.getBtdate(),chdrpf.getPtdate())) {
		sv.actionErr.set(g008);
	}
	/*	chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(chdrmjaIO.getBtdate(),chdrmjaIO.getPtdate())) {
			sv.actionErr.set(g008);
		}*/
		//ILIFE-7772 end
		/*EXIT*/
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
				case preKeeps3060: 
					preKeeps3060();
				case keeps3070: 
					keeps3070();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.action,"I")) {
			wsspcomn.flag.set("I");
			//ILIFE-7968 start
			chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim());
			chdrpfDAO.setCacheObject(chdrpf);
			if(chdrpf==null){
				fatalError600();
				return;
			}
			
			/*chdrenqIO.setChdrcoy(wsspcomn.company);
			chdrenqIO.setChdrnum(sv.chdrsel);
			chdrenqIO.setFunction(varcom.reads);
			chdrenqIO.setFormat(chdrenqrec);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(),varcom.oK)
			&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(chdrenqIO.getParams());
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				fatalError600();
			}*/
			//ILIFE-7968 end
			else {
				goTo(GotoLabel.keeps3070);
			}
		}
		else {
			wsspcomn.flag.set("M");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.batching3080);
		}
		if (isEQ(scrnparams.deviceInd,"*RMT")) {
			goTo(GotoLabel.preKeeps3060);
		}
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrptsIO.getChdrnum());//ILIFE-7968
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")
		&& isEQ(sv.chdrselErr,SPACES)) {
			sv.chdrselErr.set(f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}
	}

protected void preKeeps3060()
	{
		/*    For existing proposals, add one to the transaction number.*/
	//ILIFE-7968
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			setPrecision(chdrptsIO.getTranno(), 0);
			chdrptsIO.setTranno(add(chdrptsIO.getTranno(),1));
		}
	}

protected void keeps3070()
	{
	//ILIFE-7968 start
		chdrptsIO.setFunction("KEEPS");
		chdrptsIO.setFormat(chdrptsrec);
		SmartFileCode.execute(appVars, chdrptsIO);
		if (isNE(chdrptsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
		chdrsurIO.setFunction("KEEPS");
		chdrsurIO.setFormat(chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrsurIO.getParams());
			fatalError600();
		}
		//ILIFE-7968 end
		if (isEQ(scrnparams.action,"B")) {
			chdrpfDAO.setCacheObject(chdrpf);
			
			/*chdrmjaIO.setFunction("KEEPS");
			chdrmjaIO.setFormat(chdrmjarec);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}*/
			//ILIFE-7772 end
			covrmjaIO.setDataArea(SPACES);
			covrmjaIO.setChdrcoy(wsspcomn.company);
			covrmjaIO.setChdrnum(chdrpf.getChdrnum());/* IJTI-1523 */
			covrmjaIO.setLife("01");
			covrmjaIO.setCoverage("00");
			covrmjaIO.setRider("00");
			covrmjaIO.setPlanSuffix(ZERO);
			covrmjaIO.setFunction(varcom.begn);
			
			//Start Life Performance atiwari23
            covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
            covrmjaIO.setFitKeysSearch("CHDRNUM");
			//end;
			SmartFileCode.execute(appVars, covrmjaIO);
			if ((isNE(covrmjaIO.getStatuz(),varcom.oK))
			&& (isNE(covrmjaIO.getStatuz(),varcom.endp))) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			if (isNE(wsspcomn.company,covrmjaIO.getChdrcoy())
			|| isNE(chdrpf.getChdrnum(),covrmjaIO.getChdrnum())
			|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
			covrmjaIO.setFormat(covrmjarec);
			covrmjaIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
