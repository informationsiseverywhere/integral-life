package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AcldpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:35
 * Class transformed from ACLDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AcldpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 104;
	public FixedLengthStringData acldrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData acldpfRecord = acldrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(acldrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(acldrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(acldrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(acldrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(acldrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(acldrec);
	public PackedDecimalData rgpynum = DD.rgpynum.copy().isAPartOf(acldrec);
	public FixedLengthStringData acdben = DD.acdben.copy().isAPartOf(acldrec);
	public PackedDecimalData datefrm = DD.datefrm.copy().isAPartOf(acldrec);
	public PackedDecimalData dateto = DD.dateto.copy().isAPartOf(acldrec);
	public FixedLengthStringData gcdblind = DD.gcdblind.copy().isAPartOf(acldrec);
	public FixedLengthStringData benfreq = DD.benfreq.copy().isAPartOf(acldrec);
	public PackedDecimalData actexp = DD.actexp.copy().isAPartOf(acldrec);
	public PackedDecimalData acbenunt = DD.acbenunt.copy().isAPartOf(acldrec);
	public PackedDecimalData cvbenunt = DD.cvbenunt.copy().isAPartOf(acldrec);
	public PackedDecimalData gcnetpy = DD.gcnetpy.copy().isAPartOf(acldrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(acldrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(acldrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(acldrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AcldpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AcldpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AcldpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AcldpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AcldpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AcldpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AcldpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ACLDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"CRTABLE, " +
							"RGPYNUM, " +
							"ACDBEN, " +
							"DATEFRM, " +
							"DATETO, " +
							"GCDBLIND, " +
							"BENFREQ, " +
							"ACTEXP, " +
							"ACBENUNT, " +
							"CVBENUNT, " +
							"GCNETPY, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     crtable,
                                     rgpynum,
                                     acdben,
                                     datefrm,
                                     dateto,
                                     gcdblind,
                                     benfreq,
                                     actexp,
                                     acbenunt,
                                     cvbenunt,
                                     gcnetpy,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		crtable.clear();
  		rgpynum.clear();
  		acdben.clear();
  		datefrm.clear();
  		dateto.clear();
  		gcdblind.clear();
  		benfreq.clear();
  		actexp.clear();
  		acbenunt.clear();
  		cvbenunt.clear();
  		gcnetpy.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAcldrec() {
  		return acldrec;
	}

	public FixedLengthStringData getAcldpfRecord() {
  		return acldpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAcldrec(what);
	}

	public void setAcldrec(Object what) {
  		this.acldrec.set(what);
	}

	public void setAcldpfRecord(Object what) {
  		this.acldpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(acldrec.getLength());
		result.set(acldrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}