package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr688screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 17, 4, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr688screensfl";
		lrec.subfileClass = Sr688screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.endSubfileString = "*MORE";
		lrec.sizeSubfile = 8;
		lrec.pageSubfile = 7;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 9, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr688ScreenVars sv = (Sr688ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr688screenctlWritten, sv.Sr688screensflWritten, av, sv.sr688screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr688ScreenVars screenVars = (Sr688ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.benpln.setClassString("");
		screenVars.incurdtDisp.setClassString("");
		screenVars.gcadmdtDisp.setClassString("");
		screenVars.dischdtDisp.setClassString("");
		screenVars.diagcde.setClassString("");
		screenVars.shortdesc.setClassString("");
		screenVars.zdoctor.setClassString("");
		screenVars.givname.setClassString("");
		screenVars.zmedprv.setClassString("");
		screenVars.cdesc.setClassString("");
		screenVars.zunit.setClassString("");
		screenVars.amtlife.setClassString("");
		screenVars.amtyear.setClassString("");
		screenVars.gdeduct.setClassString("");
		screenVars.copay.setClassString("");
	}

/**
 * Clear all the variables in Sr688screenctl
 */
	public static void clear(VarModel pv) {
		Sr688ScreenVars screenVars = (Sr688ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.crtable.clear();
		screenVars.benpln.clear();
		screenVars.incurdtDisp.clear();
		screenVars.incurdt.clear();
		screenVars.gcadmdtDisp.clear();
		screenVars.gcadmdt.clear();
		screenVars.dischdtDisp.clear();
		screenVars.dischdt.clear();
		screenVars.diagcde.clear();
		screenVars.shortdesc.clear();
		screenVars.zdoctor.clear();
		screenVars.givname.clear();
		screenVars.zmedprv.clear();
		screenVars.cdesc.clear();
		screenVars.zunit.clear();
		screenVars.amtlife.clear();
		screenVars.amtyear.clear();
		screenVars.gdeduct.clear();
		screenVars.copay.clear();
	}
}
