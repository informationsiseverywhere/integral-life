/*
 * File: P6002.java
 * Date: 30 August 2009 0:36:00
 * Author: Quipoz Limited
 * 
 * Class transformed from P6002.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.terminationclaims.dataaccess.PuptTableDAM;
import com.csc.life.terminationclaims.procedures.Calcfee;
import com.csc.life.terminationclaims.screens.S6002ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6648rec;
import com.csc.life.terminationclaims.tablestructures.T6651rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnaloTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprcTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*  P6002 - Paid Up Processing Selection        (LMJAPUP)
*  ------------------------------------
*
*  This function will be used to perform Paid Up processing  on
*  a component within a contract.
*
*  The  user  may  have  selected to 'pay-up' either individual
*  policies within the plan or the whole plan.  After this  the
*  actual  component  will be selected.  If individual policies
*  are being paid-up then this screen will be invoked once  for
*  each selected component.
*
*  If  the whole plan has been selected, (COVRMJA-PLAN-SUFFIX =
*  0000), then this screen will again be invoked once for  each
*  component  selected  but  it  must loop around within itself
*  and display  the  screen  once  for  each  matching  COVRMJA
*  record  that  exists  within  the  plan. It will loop around
*  between the 2000 and 4000 sections  until  all  the  COVRMJA
*  records  with  a  key  matching on Company, Contract Number,
*  Life, Coverage and Rider have been  processed.  The  program
*  will  read  all the COVRMJA records from the summary record,
*  if it exists, to the last COVRMJA record  in  the  plan  for
*  the  given  rider  key. The plan suffix being processed will
*  be displayed in the heading portion of the screen  in  order
*  to  show  to  the  user  which  policy  is  currently  being
*  processed.  When  the  summary  record  is  being  processed
*  display  the  range  of policies represented by the summary,
*  e.g. Policy Number: 0001 - 0006.
*
*
*  Initialise
*  ----------
*
*  Skip this section if returning from a program  further  down
*  the stack, (current stack position action flag = '*').
*
*  Perform  a RETRV on the COVRMJA I/O module. This will return
*  the COVRMJA record on the selected policy.
*
*  If COVRMJA-PLAN-SUFFIX = 0000 then the whole plan  is  being
*  processed  and every COVRMJA record whose key matches on the
*  passed Company, Contract Number, Life,  Coverage  and  Rider
*  will be processed.
*
*  If  the COVRMJA-PLAN-SUFFIX is not greater than the value of
*  CHDRMJA-POLSUM then a 'notional' policy is being  processed,
*  i.e. a  policy  has been selected that is at present part of
*  the summarised portion of the contract. If this is the  case
*  then  the COVRMJA summary record will have been saved by the
*  KEEPS command and the values from here  will  be  displayed.
*  The  details  of the contract being processed will be stored
*  in the CHDRMJA I/O  module.    Retrieve  the  details  using
*  RETRV.
*
*  Set  up  the header portion of the screen with the following
*  fields, descriptions and names:
*
*       Contract  Type  (CHDRMJA-CNTTYPE)  -  long  description
*       from T5688
*       Contract  Status (CHDRMJA-STATCODE) - short description
*       from T3623
*       Premium      Status   (CHDRMJA-PSTATCODE)    -    short
*       description from T3588
*       Paid to Date (CHDRMJA-PTDATE)
*       Billed to Date (CHDRMJA-BTDATE)
*       Billing Frequency (CHDRMJA-BILLFREQ)
*       The owner's client (CLTS) details.
*       The first Life Assured details from the LIFE dataset.
*       The  Joint  Life  Assured details from the LIFE dataset
*       if they exist.
*       All  the  client  numbers  should  have   their   names
*       displayed  using  CONFNAME.    Obtain  the confirmation
*       names.
*
*  These screen fields are for display only.
*
*
*  Screen Display and Validation
*  -----------------------------
*
*  Skip this section if returning from a program  further  down
*  the stack, (current stack position action flag = '*').
*
*  Read  the PUPT file, checking for an existing record for the
*  component selected.  If so, proceed to the  'Field  Display'
*  section, displaying the following message:
*   'PUP Already Requested'.
*
*  Check   that   the  coverage/rider  exists  on  the  General
*  Coverage/Rider Details Table:-
*       Read T5687  for  the  coverage/rider  (COVRMJA-CRTABLE)
*       selected.      If  no  table  item  exists,  abort  the
*       program.  If T5687-PUMETH is equal to spaces, issue  an
*       error and disallow pay-up.
*
*  Check  that  the  component  has  been in force at least the
*  number of months specified on the Paid Up  Validation  Rules
*  table:-
*       Read  T6648 using T5687-PUMETH||CHDRMJA-CNTCURR.  If no
*       table item exists, abort the program.    Otherwise  use
*       DATCON3  to  calculate  the  number  of  months between
*       COVRMJA-CRRCD and  PAYR-BTDATE;  if  the  number  of
*       months  calculated is less than the number of months on
*       the  table  (T6648-MINMTHIF)  for   the   corresponding
*       billing  frequency  (CHDRMJA-BILLFREQ)  entry, issue an
*       error and disallow pay-up.
*
*  Check that the sum-assured is greater than or equal  to  the
*  minimum  sum-assured  required  on  the  Paid  Up Validation
*  Rules table:-
*       Use T6648.
*       If the sum-assured is  less  than  that  on  the  table
*       (T6648-MINISUMA)    for   the   corresponding   billing
*       frequency (CHDRMJA-BILLFREQ) entry, issue an error  and
*       disallow pay-up.
*
*  Check  that  the  remaining policy term for the component is
*  at least the number of  months  specified  on  the  Paid  Up
*  Validation Rules table:-
*       Use T6648.
*       Perform  DATCON3  to  calculate  the  number  of months
*       between PAYR-PTDATE and COVRMJA-RISK-CESS-DATE.   If
*       the  number  of  months  calculated  is  less  than the
*       number of months on the table (T6648-MINTERM)  for  the
*       corresponding   billing   frequency  (CHDRMJA-BILLFREQ)
*       entry, issue an error and disallow pay-up.
*
*  Check that the coverage/rider  status  matches  one  of  the
*  pairs on the Component Status Table:-
*       Read  T5679 using the program's transaction code as the
*       item key.
*       If COVRMJA-RIDER = '00',  check  against  the  coverage
*       status, else check against the rider status.
*       Use    the    fields    T5679-RID(COV)-PREM-STAT    and
*       T5679-RID(COV)-RISK-STAT against COVRMJA-PSTATCODE  and
*       COVRMJA-STATCODE.
*       If  the statuses do  not  match one of the pairs on the
*       table entry, issue an error and disallow pay-up.
*
*  The component instalment premium  (COVRMJA-INSTPREM)  cannot
*  equal zero; if it does, issue an error and disallow pay-up.
*
*  Read  T6651  using  a  key  of  paid  up  processing  method
*  concatenated with CNTCURR, giving  the  paid  up  processing
*  rules for the item.
*  Check  that the fee calculated is not greater than the total
*  value of the funds:
*
*  Read  T6598  using  PUPT-PUMETH  to  find  the   Calculation
*  Subroutine Name.
*  If  T6598  Calculation  Subroutine  Name  field  is  spaces,
*  zeroise the fee-charged and  do  not  attempt  to  call  the
*  subroutine
*  else
*  Call  the  Paid  Up  Processing Calculation Subroutine, with
*  the following linkage fields:
*       Company - CHDRCOY
*       Contract Number - CHDRNUM
*       Life - LIFE
*       Coverage - COVERAGE
*       Rider - RIDER
*       Coverage/Rider Type - COVRMJA-CRTABLE
*       Paid Up Method - T5687-PUMETH
*       Contract Currency - CHDRMJA-CNTCURR
*       Billing Frequency - CHDRMJA-BILLFREQ
*       Instalment Premium - COVRMJA-INSTPREM
*       Sum Assured - COVRMJA-SUMINS
*       Billed to Date - CHDRMJA-BTDATE
*       Coverage/Rider Risk Commencement Date - COVRMJA-CRRCD
*       Premium Cessation Date - COVRMJA-PREM-CESS-DATE
*       New Sum Assured - set to zeros (will be returned)
*       Fee - set to zeros (will be returned).
*
*  Initialise the total-components-funds field.
*
*  Set up UTRS with a key of:-
*       Company,
*       Contract Header Number,
*       Life,
*       Coverage,
*       Rider,
*       Plan Suffix,
*       and spaces in Fund and Unit Type.
*
*  Perform a BEGN on UTRS.
*  If  the  current  unit  balance  (UTRS-CURRENT-UNIT-BAL)  is
*  zeros, read the next record (NEXTR).
*
*  The  first  time  through or on a change of fund, read table
*  T5515  (Unit Linked  Fund  Table)  to  determine  the   Fund
*  currency  AND  obtain  the  bid (if T6651-BIDOFFER = 'B') or
*  offer (if T6651-BIDOFFER = 'O') price from VPRC. The key  is
*  Fund, Type (which will always be 'A'), and CHDRMJA-BTDATE.
*
*  If  T6651-ADJUSTIU  =  'Y', and the Unit Type = 'I', use the
*  total  number  of  units  to   pro-rata   against   as   the
*  Accumulated  Real  Units plus the Initial Real Units for the
*  fund
*  else
*  If T6651-ADJUSTIU  =  'N',  ignore  any  Initial  Units  and
*  NEXTR; i.e. only pro-rata against the Accumulated Real Units
*  for the fund.
*
*  Value of Units - multiply the total  number  of  real  units
*  for   the   fund   (as  accumulated  above),  by  the  price
*  (VPRC-UNIT-BID/OFFER-PRICE), giving the value of  the  units
*  in fund currency.
*
*  Perform a NEXTR.
*  If  the  current  unit  balance  (UTRS-CURRENT-UNIT-BAL)  is
*  zeros, read the next record (NEXTR).
*
*  On a change of fund and if the fund  currency  differs  from
*  the   payment   currency   then  a  currency  conversion  is
*  necessary.
*       Call the subroutine 'XCVRT' with a function of  'REAL',
*       the   From  Currency  as  the  Fund  Currency,  the  To
*       Currency as the payment  currency  and  the  calculated
*       amount  in  fund  currency  as  the  From  Amount.  The
*       effective date (BTDATE)  of  the  transaction  is  also
*       passed  to  XCVRT.   This will return the amount in the
*       required currency.
*  Add  the   amount   in   the   payment   currency   to   the
*  total-components-funds field.
*
*  If  the  key  (apart  from  fund or type) changes, perform a
*  final currency conversion check.
*
*  If the calculated fee is non-zero and is  greater  than  the
*  total  value  of  the  units, display a message stating that
*  there are insufficient funds to pay for  the  fee  incurred,
*  and disallow pay-up.
*
*  Field Display
*  -------------
*  Display the following component/rider level fields:
*       Existing  Sum  Assured  (COVRMJA-SUMINS)  - if the plan
*       suffix is  zeros,  the  sum-assured  will  need  to  be
*       divided  by the number of policies currently summarised
*       (CHDRMJA-POLSUM)
*       New Sum Assured  (PUPT-SUMINS  or  calculated  New  Sum
*       Assured returned from subroutine)
*       Value   of   Funds   (PUPT-FUND-VALUE   or   calculated
*       'total-components-funds' field)
*       Fee (PUPT-FEE-CHARGED or calculated Fee  returned  from
*       subroutine)
*       Risk Commencement Date (COVRMJA-CRRCD)
*       Paid  Up  Method (T5687-PUMETH) - long description from
*       T6598
*       Component  Risk  Status   (COVRMJA-STATCODE)   -   long
*       description from T5682
*       Component  Premium  Status  (COVRMJA-PSTATCODE)  - long
*       description from T5681
*  Display the screen, with all fields protected.
*
*  Note for future modification:-
*  If the sum-assured is allowed to be changed,  unprotect  the
*  sum-assured field on the screen.
*
*  If 'KILL' is requested, exit this section.
*
*
*  Updating
*  --------
*  Skip  this  section if returning from a program further down
*  the stack (current stack position action  flag  =  '*'),  or
*  'KILL' was requested.
*
*  Write  one  PUPT  record  for  the  component to be paid up,
*  using the UPDAT function.
*
*  Set the following fields on the PUPT:
*
*  CHDRCOY   -|
*  CHDRNUM    |
*  LIFE       |
*  COVERAGE   |---key from COVRMJA record
*  RIDER      |
*  PLNSFX    -|
*  Paid Up Processing Method - T5687-PUMETH
*  New Sum Assured
*  Value of the Funds
*  Fee-charged
*  Termid
*  Transaction Date
*  Transaction Time
*  User
*
*
*  Next Program
*  ------------
*  If 'KILL' was requested move spaces  to  the  current  stack
*  action  and  program  fields and do not add 1 to the program
*  pointer.  Then exit.
*
*  If the current stack action field is '*'  then  re-load  the
*  next  8  positions in the program stack from the saved area,
*  and move spaces to WSSP-SEC-ACTION(current position).
*
*  If processing the whole Plan, (COVRMJA-PLAN-SUFFIX =  0000),
*  then  the  program will read the next COVRMJA record for the
*  matching Company, Contract Number, Life, Coverage and  Rider
*  and  loop back around to the 2000 section and re-display the
*  details from the next COVRMJA record as they were first  set
*  up  by  the  first  pass through the 1000 section. This will
*  continue until all the matching COVRMJA  records  have  been
*  processed.
*
*  Add 1 to the current program pointer and exit.
*
*
*  Tables Used
*  -----------
*
*  T3623 - Contract Risk Status           Key: Risk Code
*  T3588 - Contract Premium Status        Key: Premium Code
*  T5515 - Unit Linked Fund               Key: Unit Linked Fund
*  T5679 - Component Status               Key: Transaction Code
*  T5681 - Component Premium Status       Key: Premium Code
*  T5682 - Component Risk Status          Key: Risk Code
*  T5687 - General Coverage/Rider Details Key: Coverage/Rider
*                                              Code
*  T5688 - Contract Structure             Key: Contract Type
*  T6598 - Paid Up Calculation Method     Key: Paid Up Method
*  T6648 - Paid Up Validation Rules       Key: Paid Up
*                                              Method||Currency
*  T6651 - Paid Up Processing Rules       Key: Paid Up
*                                              Method||Currency
*
*  T6648 - Paid Up Processing Validation
*  -------------------------------------
*  A  new  table  to  hold  parameter  values used to determine
*  whether the component is able to be paid up.
*
*  The table will  be  keyed  on  paid  up  calculation  method
*  concatenated with currency.
*
*  The table will be a dated table.
*
*  Parameters   are   as   follows,   in  a  matrix  with  five
*  occurrences by billing frequency:
*       - minimum number of months in force
*            MINMTHIF       PIC 9(3)
*       - minimum sum assured amount
*            MINISUMA       PIC S9(11)V99
*       - minimum remaining policy term (in months)
*            MINTERM        PIC 9(4)
*
*
*
*****************************************************************
* </pre>
*/
public class P6002 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6002");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaValidStatuz = "N";
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(3, 0).init(ZERO);
	private PackedDecimalData wsaaFee = new PackedDecimalData(17, 5);
	private FixedLengthStringData wsaaPreviousFund = new FixedLengthStringData(4).init(SPACES);

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator wsaaWholePlan = new Validator(wsaaPlanproc, "Y");
	private Validator wsaaSgleComponent = new Validator(wsaaPlanproc, "N");

	private FixedLengthStringData wsaaGotFreq = new FixedLengthStringData(1);
	private Validator gotFreq = new Validator(wsaaGotFreq, "Y");
		/* WSAA-FREQUENCIES */
	private FixedLengthStringData wsaaFreq1 = new FixedLengthStringData(10).init("0001020412");
		/* WSAA-SCRN-ERRORS */
	private FixedLengthStringData wsaaRiderErr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCoverageErr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPumethErr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPumeth1Err = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPumeth2Err = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaScrnErr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFundamntErr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCntcurrErr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPufeeErr = new FixedLengthStringData(1);
		/* WSAA-COVR-KEY */
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaKey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaKeyItem1 = new FixedLengthStringData(4).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaaKeyItem2 = new FixedLengthStringData(3).isAPartOf(wsaaKey, 4);

		/* WSAA-PRO-RATA-TABLE */
	private FixedLengthStringData[] wsaaProRataItem = FLSInittedArray (40, 31);
	private FixedLengthStringData[] wsaaFund = FLSDArrayPartOfArrayStructure(4, wsaaProRataItem, 0);
	private FixedLengthStringData[] wsaaFundCurr = FLSDArrayPartOfArrayStructure(3, wsaaProRataItem, 4);
	private PackedDecimalData[] wsaaFundUnits = PDArrayPartOfArrayStructure(16, 5, wsaaProRataItem, 7);
	private PackedDecimalData[] wsaaFundAcumValue = PDArrayPartOfArrayStructure(17, 5, wsaaProRataItem, 22);
	private PackedDecimalData wsaaFundValue = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaTotalFundValue = new PackedDecimalData(18, 5);
	private PackedDecimalData wsaaCurrentUnitBal = new PackedDecimalData(16, 5);

	private FixedLengthStringData wsaaEndOfFund = new FixedLengthStringData(1).init("N");
	private Validator endOfFund = new Validator(wsaaEndOfFund, "Y");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaAlreadyPaidup = new FixedLengthStringData(1).init("N");
	private Validator alreadyPaidup = new Validator(wsaaAlreadyPaidup, "Y");

	private FixedLengthStringData wsaaDbparams = new FixedLengthStringData(97);
	private FixedLengthStringData filler2 = new FixedLengthStringData(15).isAPartOf(wsaaDbparams, 5, FILLER).init("ON SIZE ERROR");
	private FixedLengthStringData wsaaErrSection = new FixedLengthStringData(20).isAPartOf(wsaaDbparams, 20);
	private PackedDecimalData wsaaOvrdPupfee = new PackedDecimalData(17, 2);

		/* WSAA-IB-PRO-RATA-TABLE */
	private FixedLengthStringData[] wsaaIbProRataItem = FLSInittedArray (40, 16);
	private FixedLengthStringData[] wsaaIbFund = FLSDArrayPartOfArrayStructure(4, wsaaIbProRataItem, 0);
	private FixedLengthStringData[] wsaaIbFundCurr = FLSDArrayPartOfArrayStructure(3, wsaaIbProRataItem, 4);
	private PackedDecimalData[] wsaaIbFundAcumValue = PDArrayPartOfArrayStructure(17, 5, wsaaIbProRataItem, 7);
	private IntegerData fundIndex = new IntegerData();
	private IntegerData ibFund = new IntegerData();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PuptTableDAM puptIO = new PuptTableDAM();
	private UtrnaloTableDAM utrnaloIO = new UtrnaloTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private VprcTableDAM vprcIO = new VprcTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T5515rec t5515rec = new T5515rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T6648rec t6648rec = new T6648rec();
	private T6651rec t6651rec = new T6651rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6002ScreenVars sv = ScreenProgram.getScreenVars( S6002ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	//ILIFE-8137 
    private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpfList;
	

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		display2050, 
		exit2090, 
		continue2110, 
		readUtrs2250, 
		setFields2300, 
		display2350, 
		calc5225, 
		readNextUtrs5250, 
		a150Calc, 
		a190Exit
	}

	public P6002() {
		super();
		screenVars = sv;
		new ScreenModel("S6002", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
			initialise1100();
			setScreen1200();
		skipJlife1050();
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		wsaaRiderErr.set(SPACES);
		wsaaCoverageErr.set(SPACES);
		wsaaPumethErr.set(SPACES);
		wsaaPumeth1Err.set(SPACES);
		wsaaPumeth2Err.set(SPACES);
		wsaaScrnErr.set(SPACES);
		wsaaFundamntErr.set(SPACES);
		wsaaCntcurrErr.set(SPACES);
		wsaaPufeeErr.set(SPACES);
		wsaaBatckey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatckey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatckey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		if (isEQ(wsaaToday,0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				syserrrec.statuz.set(datcon1rec.statuz);
				fatalError600();
			}
			else {
				wsaaToday.set(datcon1rec.intDate);
			}
		}
		sv.btdate.set(ZERO);
		sv.ptdate.set(ZERO);
		sv.sumin.set(ZERO);
		sv.pufee.set(ZERO);
		sv.crrcd.set(ZERO);
		sv.numpols.set(ZERO);
		sv.planSuffix.set(ZERO);
		sv.fundAmount.set(ZERO);
		sv.newinsval.set(ZERO);
		wsaaFundValue.set(ZERO);
		puptIO.setFundAmount(ZERO);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		wsaaBatckey.set(wsspcomn.batchkey);
		syserrrec.subrname.set(wsaaProg);
		/*covrmjaIO.setFormat(formatsInner.covrmjarec);
		covrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		
		//ILIFE-8137
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
		}
		else {
			covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
					covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}
		}
	}
		
		wsaaCovrChdrcoy.set(covrpf.getChdrcoy());
		wsaaCovrChdrnum.set(covrpf.getChdrnum());
		wsaaCovrLife.set(covrpf.getLife());
		wsaaCovrCoverage.set(covrpf.getCoverage());
		wsaaCovrRider.set(covrpf.getRider());
		/* If COVRMJA-PLAN-SUFFIX is zeroes then the whole plan is to be*/
		/*    processed*/
		if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
			/*    MOVE 9999                TO COVRMJA-PLAN-SUFFIX           */
			//covrmjaIO.setFunction(varcom.begn);
			/*       MOVE 'Y'                 TO S6002-PLNSFX-OUT(HI)*/
			wsaaPlanproc.set("Y");
		}
		else {
			wsaaPlanproc.set("N");
			//covrmjaIO.setFunction(varcom.readr);
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		policyLoad5000();
		/* Retrieve Contract Header details which have been stored in*/
		/* the CHDRMJA I/O MODULE and load the corresponding screen fields*/
		//ILIFE-8137
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		/* Read the PAYR file to retrieve the Billing information          */
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrpf.getChdrcoy());
		payrIO.setChdrnum(chdrpf.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

protected void setScreen1200()
	{
		/*    Set screen fields*/
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		sv.crtable.set(covrpf.getCrtable());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.btdate.set(chdrpf.getBtdate());
		sv.billfreq.set(chdrpf.getBillfreq());
		sv.register.set(chdrpf.getReg());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.numpols.set(chdrpf.getPolinc());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/* Get Billing Frequency description from T3590*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3590);
		descIO.setDescitem(chdrpf.getBillfreq());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.bilfrqdesc.fill("?");
		}
		else {
			sv.bilfrqdesc.set(descIO.getLongdesc());
		}
		/* Get Coverage description  from T5687*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if ((isNE(descIO.getStatuz(),varcom.oK))
		&& (isNE(descIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.crtabdesc.fill("?");
		}
		else {
			sv.crtabdesc.set(descIO.getLongdesc());
		}
		/* Load the First-Life details to screen*/
		lifemjaIO.setDataKey(SPACES);
		lifemjaIO.setChdrcoy(wsspcomn.company);
		lifemjaIO.setChdrnum(covrpf.getChdrnum());
		lifemjaIO.setLife(covrpf.getLife());
		sv.life.set(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFormat(formatsInner.cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		/* Check for existance of JOINT-LIFE Details.*/
		lifemjaIO.setJlife("01");
		lifemjaIO.setFormat(formatsInner.lifemjarec);
		lifemjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(),varcom.oK)) {
			sv.jlife.set(SPACES);
			sv.jlifename.set(SPACES);
			/*    GO TO 1900-EXIT.                                          */
			return ;
		}
		sv.jlife.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		else {
			plainname();
			sv.jlifename.set(wsspcomn.longconfname);
		}
	}

protected void skipJlife1050()
	{
		setUpScreen2900();
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section.
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*                                                     <D509CS>*/
		/*MOVE O-K                    TO WSSP-EDTERROR.        <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF S6002-ERROR-INDICATORS   NOT = SPACES             <D509CS>*/
		/*   GO TO 2350-DISPLAY.                               <D509CS>*/
		/*                                                     <D509CS>*/
		/*MOVE ZEROES                 TO WSAA-TOTAL-FUND-VALUE.<D509CS>*/
		/*PERFORM 2600-INIT-TABLE     VARYING FUND-INDEX       <D509CS>*/
		/*                            FROM 1 BY 1              <D509CS>*/
		/*                            UNTIL FUND-INDEX > 40.   <D509CS>*/
		/*MOVE 1                      TO WSAA-SUB2.            <D509CS>*/
		/*                                                     <D509CS>*/
		/*2100-VALIDATE.                                           <D509CS>*/
		/*                                                     <D509CS>*/
		/*    Validate fields                                              */
		/* allow one pup per day                                           */
		/*IF CHDRMJA-CURRFROM         = ZEROS                  <D509CS>*/
		/*   GO TO  2110-CONTINUE.                             <D509CS>*/
		/*  MOVE -1                     TO DTC2-FREQ-FACTOR.             */
		/*  MOVE 'DY'                   TO DTC2-FREQUENCY.               */
		/*  MOVE CHDRMJA-CURRFROM       TO DTC2-INT-DATE-1.              */
		/*  CALL 'DATCON2' USING DTC2-DATCON2-REC.                       */
		/*  IF DTC2-STATUZ           NOT = O-K                           */
		/*     MOVE DTC2-DATCON2-REC TO SYSR-PARAMS                      */
		/*     MOVE DTC2-STATUZ      TO SYSR-STATUZ                      */
		/*     PERFORM 600-FATAL-ERROR.                                  */
		/*  IF DTC2-INT-DATE-2 = WSAA-TODAY                              */
		/*     MOVE H066                TO S6002-CHDRNUM-ERR             */
		/*     MOVE 'Y'                 TO WSSP-EDTERROR                 */
		/*     GO TO 2350-DISPLAY.                                       */
		/*2110-CONTINUE.                                           <D509CS>*/
		/*MOVE WSSP-COMPANY           TO PUPT-CHDRCOY.         <D509CS>*/
		/*MOVE COVRMJA-CHDRNUM        TO PUPT-CHDRNUM.         <D509CS>*/
		/*MOVE COVRMJA-LIFE           TO PUPT-LIFE.            <D509CS>*/
		/*MOVE COVRMJA-COVERAGE       TO PUPT-COVERAGE.        <D509CS>*/
		/*MOVE COVRMJA-RIDER          TO PUPT-RIDER.           <D509CS>*/
		/*MOVE COVRMJA-PLAN-SUFFIX    TO PUPT-PLAN-SUFFIX.     <D509CS>*/
		/*MOVE PUPTREC                TO PUPT-FORMAT.          <D509CS>*/
		/*MOVE  READR                 TO PUPT-FUNCTION.        <D509CS>*/
		/*CALL 'PUPTIO'               USING PUPT-PARAMS.       <D509CS>*/
		/*IF PUPT-STATUZ              = O-K                    <D509CS>*/
		/*   MOVE H009                TO S6002-CHDRSTATUS-ERR  <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2350-DISPLAY.                               <D509CS>*/
		/* Check that the Coverage/Rider exists on T5687                   */
		/*MOVE SPACES                 TO ITDM-DATA-KEY.        <D509CS>*/
		/*MOVE WSSP-COMPANY           TO ITDM-ITEMCOY.         <D509CS>*/
		/*MOVE T5687                  TO ITDM-ITEMTABL.        <D509CS>*/
		/*MOVE COVRMJA-CRTABLE        TO ITDM-ITEMITEM.        <D509CS>*/
		/*MOVE WSAA-TODAY             TO ITDM-ITMFRM.          <D509CS>*/
		/*MOVE ITEMREC                TO ITDM-FORMAT.          <D509CS>*/
		/*MOVE BEGN                   TO ITDM-FUNCTION.        <D509CS>*/
		/*                                                     <D509CS>*/
		/*CALL 'ITDMIO' USING ITDM-PARAMS.                     <D509CS>*/
		/*IF ITDM-STATUZ              NOT = O-K AND            <D509CS>*/
		/*   ITDM-STATUZ              NOT = ENDP               <D509CS>*/
		/*   MOVE ITDM-PARAMS         TO SYSR-PARAMS           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF ITDM-ITEMCOY             NOT = WSSP-COMPANY OR    <D509CS>*/
		/*   ITDM-ITEMTABL            NOT = T5687        OR    <D509CS>*/
		/*   ITDM-ITEMITEM            NOT = COVRMJA-CRTABLE OR <D509CS>*/
		/*   ITDM-STATUZ              = ENDP                   <D509CS>*/
		/*   MOVE F050                TO S6002-RIDER-ERR       <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2350-DISPLAY                                <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   MOVE ITDM-GENAREA        TO T5687-T5687-REC.      <D509CS>*/
		/* If paid up method not found in T5687 then display error         */
		/* message and abort program.                                      */
		/*IF T5687-PUMETH             = SPACES                 <D509CS>*/
		/* MOVE H016                TO S6002-PUMETH-ERR              */
		/*   MOVE I027                TO S6002-PUMETH-ERR      <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/* GO TO 2300-SET-FIELDS.                                    */
		/*   GO TO 2350-DISPLAY.                               <D509CS>*/
		/* Check that the Component has been in-force at least the number  */
		/*  of months specified on T6648. If the number of months between  */
		/*  COVRMJA-CRRD and CHDRMJA-BTDATE is less then T6648-MINMYHIF    */
		/*  for the corresponding CHDRMJA-BILLFREQ pay-up is disallowed.   */
		/*MOVE SPACES                 TO ITDM-DATA-KEY.        <D509CS>*/
		/*MOVE WSSP-COMPANY           TO ITDM-ITEMCOY.         <D509CS>*/
		/*MOVE T5687-PUMETH           TO WSAA-KEY-ITEM1.       <D509CS>*/
		/*MOVE CHDRMJA-CNTCURR        TO WSAA-KEY-ITEM2.       <D509CS>*/
		/*MOVE WSAA-KEY               TO ITDM-ITEMITEM.        <D509CS>*/
		/*MOVE T6648                  TO ITDM-ITEMTABL.        <D509CS>*/
		/*MOVE ITEMREC                TO ITDM-FORMAT.          <D509CS>*/
		/*MOVE WSAA-TODAY             TO ITDM-ITMFRM.                  */
		/*MOVE CHDRMJA-PTDATE         TO ITDM-ITMFRM.          <D509CS>*/
		/*MOVE BEGN                   TO ITDM-FUNCTION.        <D509CS>*/
		/*CALL 'ITDMIO'               USING ITDM-PARAMS.       <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF ITDM-STATUZ              NOT = O-K AND            <D509CS>*/
		/*   ITDM-STATUZ              NOT = ENDP               <D509CS>*/
		/*   MOVE ITDM-PARAMS         TO SYSR-PARAMS           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF ITDM-ITEMCOY             NOT = WSSP-COMPANY OR    <D509CS>*/
		/*   ITDM-ITEMTABL            NOT = T6648        OR    <D509CS>*/
		/*   ITDM-ITEMITEM            NOT = WSAA-KEY     OR    <D509CS>*/
		/*   ITDM-STATUZ              = ENDP                   <D509CS>*/
		/*   MOVE ITDM-PARAMS         TO SYSR-PARAMS           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR                           <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   MOVE ITDM-GENAREA        TO T6648-T6648-REC.      <D509CS>*/
		/*                                                     <D509CS>*/
		/*MOVE 'CMDF'                 TO DTC3-FUNCTION.        <D509CS>*/
		/*MOVE '12'                   TO DTC3-FREQUENCY.       <D509CS>*/
		/*MOVE COVRMJA-CRRCD          TO DTC3-INT-DATE-1.      <D509CS>*/
		/*MOVE PAYR-BTDATE            TO DTC3-INT-DATE-2.      <D509CS>*/
		/*MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-2.      <D509CS>*/
		/*CALL 'DATCON3'              USING  DTC3-DATCON3-REC. <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF DTC3-STATUZ              NOT = O-K                <D509CS>*/
		/*   MOVE DTC3-DATCON3-REC    TO SYSR-PARAMS           <D509CS>*/
		/*   MOVE DTC3-STATUZ         TO SYSR-STATUZ           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF CHDRMJA-BILLFREQ         = '00'                   <D509CS>*/
		/*   MOVE 1                   TO WSAA-SUB1             <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   IF CHDRMJA-BILLFREQ      = '01'                   <D509CS>*/
		/*      MOVE 2                TO WSAA-SUB1             <D509CS>*/
		/*   ELSE                                              <D509CS>*/
		/*      IF CHDRMJA-BILLFREQ   = '02'                   <D509CS>*/
		/*         MOVE 3             TO WSAA-SUB1             <D509CS>*/
		/*      ELSE                                           <D509CS>*/
		/*         IF CHDRMJA-BILLFREQ = '04'                  <D509CS>*/
		/*            MOVE 4          TO WSAA-SUB1             <D509CS>*/
		/*         ELSE                                        <D509CS>*/
		/*            MOVE 5          TO WSAA-SUB1.            <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF DTC3-FREQ-FACTOR < T6648-MINMTHIF(WSAA-SUB1)      <D509CS>*/
		/*   MOVE H010                TO S6002-COVERAGE-ERR    <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2300-SET-FIELDS.                            <D509CS>*/
		/* Check that Sum-Assured is >= Min-Sum-Assured on T6648           */
		/*IF COVRMJA-SUMINS < T6648-MINISUMA(WSAA-SUB1)        <D509CS>*/
		/*   MOVE H011                TO S6002-COVERAGE-ERR    <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2300-SET-FIELDS.                            <D509CS>*/
		/* Check that the remaining policy term is at least as long as     */
		/* specified on T6648                                              */
		/*MOVE 'CMDF'                 TO DTC3-FUNCTION.        <D509CS>*/
		/*MOVE '12'                   TO DTC3-FREQUENCY.       <D509CS>*/
		/*MOVE COVRMJA-CRRCD          TO DTC3-INT-DATE-1.              */
		/*MOVE CHDRMJA-PTDATE         TO DTC3-INT-DATE-2.              */
		/*MOVE CHDRMJA-BTDATE         TO DTC3-INT-DATE-1.      <D509CS>*/
		/*MOVE COVRMJA-PREM-CESS-DATE TO DTC3-INT-DATE-2.      <D509CS>*/
		/*MOVE PAYR-PTDATE            TO DTC3-INT-DATE-1.      <D509CS>*/
		/*MOVE COVRMJA-RISK-CESS-DATE TO DTC3-INT-DATE-2.      <D509CS>*/
		/*CALL 'DATCON3'              USING  DTC3-DATCON3-REC. <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF DTC3-STATUZ              NOT = O-K                <D509CS>*/
		/*   MOVE DTC3-DATCON3-REC    TO SYSR-PARAMS           <D509CS>*/
		/*   MOVE DTC3-STATUZ         TO SYSR-STATUZ           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF DTC3-FREQ-FACTOR < T6648-MINTERM(WSAA-SUB1)       <D509CS>*/
		/*   MOVE H012                TO S6002-PTDATE-ERR      <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2300-SET-FIELDS.                            <D509CS>*/
		/* Check that the Covr/Rider status is on T5679                    */
		/*MOVE SPACES                 TO ITEM-DATA-KEY.        <D509CS>*/
		/*MOVE 'IT'                   TO ITEM-ITEMPFX.         <D509CS>*/
		/*MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.         <D509CS>*/
		/*MOVE WSKY-BATC-BATCTRCDE    TO ITEM-ITEMITEM.        <D509CS>*/
		/*MOVE T5679                  TO ITEM-ITEMTABL.        <D509CS>*/
		/*MOVE ITEMREC                TO ITEM-FORMAT.          <D509CS>*/
		/*MOVE READR                  TO ITEM-FUNCTION.        <D509CS>*/
		/*CALL 'ITEMIO'               USING ITEM-PARAMS.       <D509CS>*/
		/*IF ITEM-STATUZ              NOT = O-K AND            <D509CS>*/
		/*   ITEM-STATUZ              NOT = MRNF               <D509CS>*/
		/*   MOVE ITEM-PARAMS         TO SYSR-PARAMS           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*IF ITEM-STATUZ              = MRNF                   <D509CS>*/
		/*   MOVE F050                TO S6002-COVERAGE-ERR    <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2350-DISPLAY                                <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   MOVE ITEM-GENAREA        TO T5679-T5679-REC.      <D509CS>*/
		/*MOVE 1                      TO WSAA-SUB1.                    */
		/*2150-CHECK-COVR.                                         <D509CS>*/
		/*MOVE 'N'                    TO WSAA-VALID-STATUZ.    <D509CS>*/
		/*MOVE 'N'                    TO WSAA-ALREADY-PAIDUP.  <D509CS>*/
		/*IF COVRMJA-RIDER            = '00'                   <D509CS>*/
		/*   PERFORM 2450-VALIDATE-COVR                        <D509CS>*/
		/*   VARYING WSAA-SUB1 FROM 1 BY 1                     <D509CS>*/
		/*   UNTIL WSAA-SUB1 > 12    OR WSAA-VALID-STATUZ = 'Y'<D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   PERFORM 2500-VALIDATE-RIDR                        <D509CS>*/
		/*   VARYING WSAA-SUB1 FROM 1 BY 1                     <D509CS>*/
		/*   UNTIL WSAA-SUB1 > 12    OR WSAA-VALID-STATUZ = 'Y'<D509CS>*/
		/*                                                     <D509CS>*/
		/*IF WSAA-VALID-STATUZ        = 'N'                    <D509CS>*/
		/*   MOVE I028                TO S6002-COVERAGE-ERR    <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2300-SET-FIELDS.                            <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF (T5679-SET-CN-PREM-STAT NOT = SPACES) AND         <D509CS>*/
		/*   (T5679-SET-CN-PREM-STAT     = COVRMJA-PSTATCODE)  <D509CS>*/
		/*   IF (T5679-SET-CN-RISK-STAT NOT = SPACES) AND      <D509CS>*/
		/*      (T5679-SET-CN-RISK-STAT     = COVRMJA-STATCODE)<D509CS>*/
		/*      MOVE 'Y'              TO WSAA-ALREADY-PAIDUP   <D509CS>*/
		/*      GO TO 2400-EXIT.                               <D509CS>*/
		/*                                                     <D509CS>*/
		/*                                                     <D509CS>*/
		/*  Btdate must = Ptdate or pay-up is disallowed                   */
		/*                                                     <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF CHDRMJA-PTDATE           NOT = CHDRMJA-BTDATE     <D509CS>*/
		/*   MOVE H013                TO S6002-BTDATE-ERR      <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2300-SET-FIELDS.                            <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF CHDRMJA-BILLFREQ         = '00'                   <D509CS>*/
		/*   MOVE H014                TO S6002-BILLFREQ-ERR    <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2300-SET-FIELDS.                            <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF COVRMJA-INSTPREM         = ZERO                   <D509CS>*/
		/* MOVE U019                TO S6002-FUNDSVAL-ERR    <D509CS>*/
		/*    MOVE G818                TO S6002-FUNDSVAL-ERR       <012>*/
		/*   MOVE G818                TO S6002-FUNDAMNT-ERR    <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2300-SET-FIELDS.                            <D509CS>*/
		/*                                                     <D509CS>*/
		/*                                                     <D509CS>*/
		/* Get the Paid-Up processing rules from T6651                     */
		/*                                                     <D509CS>*/
		/*                                                     <D509CS>*/
		/*MOVE SPACES                 TO ITDM-DATA-KEY.        <D509CS>*/
		/*MOVE WSSP-COMPANY           TO ITDM-ITEMCOY.         <D509CS>*/
		/*MOVE T5687-PUMETH           TO WSAA-KEY-ITEM1.       <D509CS>*/
		/*MOVE CHDRMJA-CNTCURR        TO WSAA-KEY-ITEM2.       <D509CS>*/
		/*MOVE WSAA-KEY               TO ITDM-ITEMITEM.        <D509CS>*/
		/*MOVE T6651                  TO ITDM-ITEMTABL.        <D509CS>*/
		/*MOVE ITEMREC                TO ITDM-FORMAT.          <D509CS>*/
		/*MOVE WSAA-TODAY             TO ITDM-ITMFRM.                  */
		/*MOVE CHDRMJA-BTDATE         TO ITDM-ITMFRM.          <D509CS>*/
		/*MOVE BEGN                   TO ITDM-FUNCTION.        <D509CS>*/
		/*                                                     <D509CS>*/
		/*CALL 'ITDMIO'               USING ITDM-PARAMS.       <D509CS>*/
		/*IF ITDM-STATUZ              NOT = O-K AND            <D509CS>*/
		/*   ITDM-STATUZ              NOT = ENDP               <D509CS>*/
		/*   MOVE ITDM-PARAMS         TO SYSR-PARAMS           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF ITDM-ITEMCOY             NOT = WSSP-COMPANY       <D509CS>*/
		/*OR ITDM-ITEMTABL            NOT = T6651              <D509CS>*/
		/*OR ITDM-ITEMITEM            NOT = WSAA-KEY           <D509CS>*/
		/*OR ITDM-STATUZ              = ENDP                   <D509CS>*/
		/*   MOVE H020                TO S6002-PUMETH-ERR      <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2350-DISPLAY                                <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   MOVE ITDM-GENAREA        TO T6651-T6651-REC.      <D509CS>*/
		/*                                                     <D509CS>*/
		/*2200-FEE-CHECK.                                          <D509CS>*/
		/*                                                     <D509CS>*/
		/* Read T6598 to get the Calculation subroutine name               */
		/*MOVE SPACES                 TO ITEM-DATA-KEY.        <D509CS>*/
		/*MOVE 'IT'                   TO ITEM-ITEMPFX.         <D509CS>*/
		/*MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.         <D509CS>*/
		/*MOVE T5687-PUMETH           TO ITEM-ITEMITEM.        <D509CS>*/
		/*MOVE T6598                  TO ITEM-ITEMTABL.        <D509CS>*/
		/*MOVE ITEMREC                TO ITEM-FORMAT.          <D509CS>*/
		/*MOVE READR                  TO ITEM-FUNCTION.        <D509CS>*/
		/*CALL 'ITEMIO'               USING ITEM-PARAMS.       <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF ITEM-STATUZ              NOT = O-K AND            <D509CS>*/
		/*   ITEM-STATUZ              NOT = MRNF               <D509CS>*/
		/*   MOVE ITEM-PARAMS         TO SYSR-PARAMS           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF ITEM-STATUZ              = MRNF                   <D509CS>*/
		/*   MOVE H021                TO S6002-PUMETH-ERR      <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2350-DISPLAY                                <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   MOVE ITEM-GENAREA        TO T6598-T6598-REC.      <D509CS>*/
		/* If T6598 Calculation subroutine name is spaces zeroise the      */
		/*    fee-charge and do not call the subroutine else               */
		/*    call the paid up processing subroutine                       */
		/*     Even though the subroutine is no longer called              */
		/*     we perform the Fee calculation section                      */
		/*     as we would normally, the subroutine has been               */
		/*     replaced by as section.                                     */
		/*IF T6598-CALCPROG           = SPACES                 <D509CS>*/
		/*   MOVE ZEROES              TO PUPT-PUPFEE           <D509CS>*/
		/*                               S6002-PUFEE           <D509CS>*/
		/*                               OVRD-PUPFEE           <D509CS>*/
		/*   GO TO 2250-READ-UTRS                              <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   MOVE ZEROS               TO OVRD-PUPFEE           <D509CS>*/
		/*   PERFORM 2700-CALC-FEE                             <D509CS>*/
		/*   MOVE OVRD-PUPFEE         TO S6002-PUFEE.          <D509CS>*/
		/*    MOVE SPACES                 TO PUPC-PUPCALC-REC.             */
		/*    MOVE CHDRMJA-CHDRCOY        TO PUPC-CHDR-CHDRCOY.            */
		/*    MOVE CHDRMJA-CHDRNUM        TO PUPC-CHDR-CHDRNUM.            */
		/*    MOVE COVRMJA-LIFE           TO PUPC-LIFE-LIFE.               */
		/*    MOVE COVRMJA-COVERAGE       TO PUPC-COVR-COVERAGE.           */
		/*    MOVE COVRMJA-RIDER          TO PUPC-COVR-RIDER.              */
		/*    MOVE COVRMJA-CRTABLE        TO PUPC-CRTABLE.                 */
		/*    MOVE T5687-PUMETH           TO PUPC-PUMETH.                  */
		/*    MOVE CHDRMJA-CNTCURR        TO PUPC-CNTCURR.                 */
		/*    MOVE CHDRMJA-BILLFREQ       TO PUPC-BILLFREQ.                */
		/*    MOVE COVRMJA-INSTPREM       TO PUPC-INSTPREM.                */
		/*    MOVE COVRMJA-SUMINS         TO PUPC-SUMINS.                  */
		/*    MOVE CHDRMJA-BTDATE         TO PUPC-BTDATE.                  */
		/*    MOVE COVRMJA-CRRCD          TO PUPC-CRRCD.                   */
		/*    MOVE COVRMJA-PREM-CESS-DATE TO PUPC-PREM-CESS-DATE.          */
		/*    MOVE ZEROES                 TO PUPC-FEE-CHARGED              */
		/*                                   PUPC-NEW-SUMINS.              */
		/*    MOVE 'CMDF'                 TO PUPC-FUNCTION.                */
		/*    CALL  T6598-CALCPROG        USING PUPC-PUPCALC-REC.          */
		/*    IF PUPC-STATUZ              NOT = O-K                        */
		/*       MOVE PUPC-PUPCALC-REC    TO SYSR-PARAMS                   */
		/*       PERFORM 600-FATAL-ERROR.                                  */
		/*    MOVE PUPC-FEE-CHARGED       TO S6002-PUFEE.                  */
		/*2250-READ-UTRS.                                          <D509CS>*/
		/* Set up UTRS and perform a BEGN                                  */
		/*  MOVE SPACE                  TO UTRS-PARAMS.                  */
		/*  IF CHDRMJA-POLSUM > ZERO                                     */
		/*     MOVE ZEROES              TO UTRS-PLAN-SUFFIX              */
		/*  ELSE                                                         */
		/*     MOVE COVRMJA-PLAN-SUFFIX TO UTRS-PLAN-SUFFIX.             */
		/*MOVE ZEROS                  TO UTRS-PLAN-SUFFIX.     <D509CS>*/
		/*                                                     <D509CS>*/
		/*MOVE CHDRMJA-CHDRCOY        TO UTRS-CHDRCOY.         <D509CS>*/
		/*MOVE CHDRMJA-CHDRNUM        TO UTRS-CHDRNUM.         <D509CS>*/
		/*MOVE COVRMJA-LIFE           TO UTRS-LIFE.            <D509CS>*/
		/*MOVE COVRMJA-COVERAGE       TO UTRS-COVERAGE.        <D509CS>*/
		/*MOVE COVRMJA-RIDER          TO UTRS-RIDER.           <D509CS>*/
		/*MOVE SPACES                 TO UTRS-UNIT-VIRTUAL-FUND<D509CS>*/
		/*MOVE SPACES                 TO UTRS-UNIT-TYPE.       <D509CS>*/
		/*MOVE BEGN                   TO UTRS-FUNCTION.        <D509CS>*/
		/*MOVE UTRSREC                TO UTRS-FORMAT.          <D509CS>*/
		/*CALL 'UTRSIO'               USING UTRS-PARAMS.       <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF UTRS-STATUZ              NOT = O-K AND            <D509CS>*/
		/*   UTRS-STATUZ              NOT = ENDP               <D509CS>*/
		/*   MOVE UTRS-PARAMS         TO SYSR-PARAMS           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*    If No UTRS record found then we wish to display a message    */
		/*    informing the user that there are no unit holdings for this  */
		/*    Contract.                                                    */
		/*IF CHDRMJA-CHDRCOY          NOT = UTRS-CHDRCOY  OR   <D509CS>*/
		/*   CHDRMJA-CHDRNUM          NOT = UTRS-CHDRNUM  OR   <D509CS>*/
		/*   COVRMJA-LIFE             NOT = UTRS-LIFE     OR   <D509CS>*/
		/*   COVRMJA-COVERAGE         NOT = UTRS-COVERAGE OR   <D509CS>*/
		/*   COVRMJA-RIDER            NOT = UTRS-RIDER    OR   <D509CS>*/
		/*   UTRS-STATUZ                  = ENDP               <D509CS>*/
		/*   MOVE E494                TO SCRN-ERROR-CODE       <D509CS>*/
		/*   MOVE ENDP                TO UTRS-STATUZ           <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO                    2350-DISPLAY.            <D509CS>*/
		/* The screen must be displayed otherwise we LOOP.                 */
		/*   GO                       TO 2400-EXIT.            <D509CS>*/
		/*    MOVE ENDP                TO UTRS-STATUZ.                  */
		/*  wsaa-current-unit-bal is now calculated as each UTRS is      */
		/*  read i.e 5200 section                                        */
		/*  IF COVRMJA-PLAN-SUFFIX      NOT = ZERO                       */
		/*     DIVIDE UTRS-CURRENT-UNIT-BAL BY CHDRMJA-POLSUM            */
		/*     GIVING WSAA-CURRENT-UNIT-BAL                              */
		/*  ELSE                                                         */
		/*     MOVE UTRS-CURRENT-UNIT-BAL TO WSAA-CURRENT-UNIT-BAL.      */
		/* MOVE ZEROES                 TO S6002-FUNDSVAL.               */
		/*MOVE ZEROES                 TO S6002-FUND-AMOUNT.    <D509CS>*/
		/*                                                     <D509CS>*/
		/*PERFORM 5200-CALC-FUND-VALUES                        <D509CS>*/
		/*        UNTIL UTRS-STATUZ   = ENDP.                  <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF WSSP-EDTERROR            = 'Y'                    <D509CS>*/
		/*   GO TO 2300-SET-FIELDS.                            <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF WSAA-FUND-VALUE          = ZERO                   <D509CS>*/
		/*     MOVE H022                TO S6002-FUNDSVAL-ERR    <D509CS>*/
		/*   MOVE H022                TO S6002-FUNDAMNT-ERR    <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR         <D509CS>*/
		/*   GO TO 2300-SET-FIELDS.                            <D509CS>*/
		/*                                                     <D509CS>*/
		/* MOVE WSAA-TOTAL-FUND-VALUE  TO S6002-FUNDSVAL.               */
		/*MOVE WSAA-TOTAL-FUND-VALUE  TO S6002-FUND-AMOUNT.    <D509CS>*/
		/*  IF WSAA-WHOLE-PLAN         OR COVRMJA-PLAN-SUFFIX NOT = ZERO */
		/*     MOVE COVRMJA-SUMINS      TO S6002-SUMINS                  */
		/*  ELSE                                                         */
		/*     DIVIDE COVRMJA-SUMINS    BY CHDRMJA-POLSUM                */
		/*     GIVING S6002-SUMINS.                                      */
		/*     Sum insured is also been generated incorrectly if         */
		/*     we are looking at summarized records.                     */
		/* IF WSAA-WHOLE-PLAN                                  <D509CS>*/
		/*        MOVE COVRMJA-SUMINS     TO S6002-SUMINS                  */
		/*    MOVE COVRMJA-SUMINS     TO S6002-SUMIN           <D509CS>*/
		/* ELSE                                                <D509CS>*/
		/*    IF COVRMJA-PLAN-SUFFIX  > CHDRMJA-POLSUM         <D509CS>*/
		/*           MOVE COVRMJA-SUMINS  TO S6002-SUMINS                  */
		/*       MOVE COVRMJA-SUMINS  TO S6002-SUMIN           <D509CS>*/
		/*    ELSE                                             <D509CS>*/
		/*       DIVIDE COVRMJA-SUMINS BY CHDRMJA-POLSUM       <D509CS>*/
		/*           GIVING S6002-SUMINS.                                  */
		/*       GIVING S6002-SUMIN.                           <D509CS>*/
		/* IF PUPC-FEE-CHARGED         NOT = ZERO                       */
		/*IF OVRD-PUPFEE              NOT = ZERO               <D509CS>*/
		/*   PERFORM 5300-VALIDATE-PUPC-FEE                    <D509CS>*/
		/*    VARYING FUND-INDEX      FROM 1 BY 1              <D509CS>*/
		/*     UNTIL FUND-INDEX >     40      OR               <D509CS>*/
		/*     WSAA-FUND(FUND-INDEX)  = SPACE OR               <D509CS>*/
		/*     WSSP-EDTERROR          = 'Y'.                   <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF WSSP-EDTERROR            = 'Y'                    <D509CS>*/
		/*   GO TO 2300-SET-FIELDS.                            <D509CS>*/
		/*                                                     <D509CS>*/
		/*2300-SET-FIELDS.                                         <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF T6598-CALCPROG           = SPACES                 <D509CS>*/
		/*   MOVE COVRMJA-SUMINS      TO S6002-NEWINSVAL       <D509CS>*/
		/*   MOVE ZEROES              TO S6002-PUFEE           <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/* MOVE PUPC-NEW-SUMINS     TO S6002-NEWINSVAL               */
		/* MOVE PUPC-FEE-CHARGED    TO S6002-PUFEE                   */
		/* MOVE ZEROS               TO S6002-NEWINSVAL          <011>*/
		/* MOVE OVRD-PUPFEE         TO S6002-PUFEE.             <011>*/
		/*   MOVE OVRD-PUPFEE         TO S6002-PUFEE,          <D509CS>*/
		/*                               WSAA-OVRD-PUPFEE      <D509CS>*/
		/*   PERFORM 2800-CALC-NEW-SUMINS                      <D509CS>*/
		/*   MOVE OVRD-NEW-SUMINS     TO S6002-NEWINSVAL.      <D509CS>*/
		/*                                                     <D509CS>*/
		/*MOVE COVRMJA-CRRCD          TO S6002-CRRCD.          <D509CS>*/
		/*                                                     <D509CS>*/
		/*                                                     <D509CS>*/
		/* Display Paid-up method and get the                      <D509CS>*/
		/*     Paid-up Method Description from T5698               <D509CS>*/
		/*                                                     <D509CS>*/
		/*MOVE T5687-PUMETH           TO S6002-PUMETH.         <D509CS>*/
		/*MOVE SPACES                 TO DESC-DATA-KEY.        <D509CS>*/
		/*MOVE 'IT'                   TO DESC-DESCPFX.         <D509CS>*/
		/*MOVE WSSP-COMPANY           TO DESC-DESCCOY.         <D509CS>*/
		/*MOVE T6598                  TO DESC-DESCTABL.        <D509CS>*/
		/*MOVE T5687-PUMETH           TO DESC-DESCITEM.        <D509CS>*/
		/*MOVE WSSP-LANGUAGE          TO DESC-LANGUAGE.        <D509CS>*/
		/*MOVE DESCREC                TO DESC-FORMAT.          <D509CS>*/
		/*MOVE READR                  TO DESC-FUNCTION.        <D509CS>*/
		/*CALL 'DESCIO'               USING DESC-PARAMS.       <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF DESC-STATUZ              NOT = O-K AND            <D509CS>*/
		/*   DESC-STATUZ              NOT = MRNF               <D509CS>*/
		/*   MOVE DESC-PARAMS         TO SYSR-PARAMS           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF DESC-STATUZ              = MRNF                   <D509CS>*/
		/*   MOVE ALL '?'             TO S6002-PUMETHDESC      <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   MOVE DESC-LONGDESC       TO S6002-PUMETHDESC.     <D509CS>*/
		/* Display Risk-status and get the                                 */
		/*     Component Risk-status-Desc from T5682                       */
		/*MOVE COVRMJA-STATCODE       TO S6002-STATCODE.       <D509CS>*/
		/*MOVE SPACES                 TO DESC-DATA-KEY.        <D509CS>*/
		/*MOVE 'IT'                   TO DESC-DESCPFX.         <D509CS>*/
		/*MOVE WSSP-COMPANY           TO DESC-DESCCOY.         <D509CS>*/
		/*MOVE T5682                  TO DESC-DESCTABL.        <D509CS>*/
		/*MOVE COVRMJA-STATCODE       TO DESC-DESCITEM.        <D509CS>*/
		/*MOVE WSSP-LANGUAGE          TO DESC-LANGUAGE.        <D509CS>*/
		/*MOVE DESCREC                TO DESC-FORMAT.          <D509CS>*/
		/*MOVE READR                  TO DESC-FUNCTION.        <D509CS>*/
		/*CALL 'DESCIO'               USING DESC-PARAMS.       <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF DESC-STATUZ              NOT = O-K AND            <D509CS>*/
		/*   DESC-STATUZ              NOT = MRNF               <D509CS>*/
		/*   MOVE DESC-PARAMS         TO SYSR-PARAMS           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF DESC-STATUZ              = MRNF                   <D509CS>*/
		/*   MOVE ALL '?'             TO S6002-STATDESC        <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   MOVE DESC-LONGDESC       TO S6002-STATDESC.       <D509CS>*/
		/* Display the Prem-status and get the                             */
		/*     Component PREM-STATUS-DESC from T5681                       */
		/*MOVE COVRMJA-PSTATCODE      TO S6002-PSTATCODE.      <D509CS>*/
		/*MOVE SPACES                 TO DESC-DATA-KEY.        <D509CS>*/
		/*MOVE 'IT'                   TO DESC-DESCPFX.         <D509CS>*/
		/*MOVE WSSP-COMPANY           TO DESC-DESCCOY.         <D509CS>*/
		/*MOVE T5681                  TO DESC-DESCTABL.        <D509CS>*/
		/*MOVE COVRMJA-PSTATCODE      TO DESC-DESCITEM.        <D509CS>*/
		/*MOVE WSSP-LANGUAGE          TO DESC-LANGUAGE.        <D509CS>*/
		/*MOVE DESCREC                TO DESC-FORMAT.          <D509CS>*/
		/*MOVE READR                  TO DESC-FUNCTION.        <D509CS>*/
		/*CALL 'DESCIO'               USING DESC-PARAMS.       <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF DESC-STATUZ              NOT = O-K AND            <D509CS>*/
		/*   DESC-STATUZ              NOT = MRNF               <D509CS>*/
		/*   MOVE DESC-PARAMS         TO SYSR-PARAMS           <D509CS>*/
		/*   PERFORM 600-FATAL-ERROR.                          <D509CS>*/
		/*                                                     <D509CS>*/
		/*IF DESC-STATUZ              = MRNF                   <D509CS>*/
		/*   MOVE ALL '?'             TO S6002-PSTATDESC       <D509CS>*/
		/*ELSE                                                 <D509CS>*/
		/*   MOVE DESC-LONGDESC       TO S6002-PSTATDESC.      <D509CS>*/
		/*2350-DISPLAY.                                            <D509CS>*/
		/*IF S6002-ERROR-INDICATORS   NOT = SPACES             <D509CS>*/
		/*   MOVE 'Y'                 TO WSSP-EDTERROR.        <D509CS>*/
		/* Protect is set on each individual field rather then on the      */
		/*  whole screen because for future modification S6002-NEWSUMINS   */
		/*  on certain pay-up method calculations will be allowed to be    */
		/*  altered.                                                       */
		/*IF T6651-OVRSUMA            NOT = 'Y'                <D509CS>*/
		/*   MOVE 'Y'                 TO S6002-NEWINSVAL-OUT(PR<D509CS>*/
		/*MOVE 'Y'                    TO S6002-BILFRQDESC-OUT(P<D509CS>*/
		/*MOVE 'Y'                    TO S6002-BILLFREQ-OUT(PR)<D509CS>*/
		/*MOVE 'Y'                    TO S6002-BTDATE-OUT(PR). <D509CS>*/
		/*MOVE 'Y'                    TO S6002-CHDRNUM-OUT(PR).<D509CS>*/
		/*MOVE 'Y'                    TO S6002-CHDRSTATUS-OUT(P<D509CS>*/
		/*MOVE 'Y'                    TO S6002-CNTCURR-OUT(PR).<D509CS>*/
		/*MOVE 'Y'                    TO S6002-CNTTYPE-OUT(PR).<D509CS>*/
		/*MOVE 'Y'                    TO S6002-COVERAGE-OUT(PR)<D509CS>*/
		/*MOVE 'Y'                    TO S6002-CRRCD-OUT(PR).  <D509CS>*/
		/*MOVE 'Y'                    TO S6002-CRTABDESC-OUT(PR<D509CS>*/
		/*MOVE 'Y'                    TO S6002-CRTABLE-OUT(PR).<D509CS>*/
		/*MOVE 'Y'                    TO S6002-CTYPEDES-OUT(PR)<D509CS>*/
		/*MOVE 'Y'                    TO S6002-FUNDSVAL-OUT(PR)<D509CS>*/
		/*MOVE 'Y'                    TO S6002-FUNDAMNT-OUT(PR)<D509CS>*/
		/*MOVE 'Y'                    TO S6002-JLIFENAME-OUT(PR<D509CS>*/
		/*MOVE 'Y'                    TO S6002-JLIFENUM-OUT(PR)<D509CS>*/
		/*MOVE 'Y'                    TO S6002-LIFE-OUT(PR).   <D509CS>*/
		/*MOVE 'Y'                    TO S6002-LIFENAME-OUT(PR)<D509CS>*/
		/*MOVE 'Y'                    TO S6002-LIFENUM-OUT(PR).<D509CS>*/
		/*MOVE 'Y'                    TO S6002-NUMPOLS-OUT(PR).<D509CS>*/
		/*MOVE 'Y'                    TO S6002-PLNSFX-OUT(PR). <D509CS>*/
		/*MOVE 'Y'                    TO S6002-PREMSTATUS-OUT(P<D509CS>*/
		/*MOVE 'Y'                    TO S6002-PSTATCODE-OUT(PR<D509CS>*/
		/*MOVE 'Y'                    TO S6002-PSTATDESC-OUT(PR<D509CS>*/
		/*MOVE 'Y'                    TO S6002-PTDATE-OUT(PR). <D509CS>*/
		/*MOVE 'Y'                    TO S6002-PUFEE-OUT(PR).  <D509CS>*/
		/*MOVE 'Y'                    TO S6002-PUMETH-OUT(PR). <D509CS>*/
		/*MOVE 'Y'                    TO S6002-PUMETHDESC-OUT(P<D509CS>*/
		/*MOVE 'Y'                    TO S6002-REGISTER-OUT(PR)<D509CS>*/
		/*MOVE 'Y'                    TO S6002-RIDER-OUT(PR).  <D509CS>*/
		/*MOVE 'Y'                    TO S6002-STATCODE-OUT(PR)<D509CS>*/
		/*MOVE 'Y'                    TO S6002-STATDESC-OUT(PR)<D509CS>*/
		/*    MOVE 'Y'                    TO S6002-SUMINS-OUT(PR).         */
		/*MOVE 'Y'                    TO S6002-SUMIN-OUT(PR).  <D509CS>*/
		/* PERFORM 2900-SET-UP-SCREEN.                          <LA5128>*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
		try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					checkUtrns2120();
					checkCovr2030();
				case display2050: 
					display2050();
				case exit2090: 
				}
				break;
		}
		catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*2050-START.                                              <D509CS>
	* </pre>
	*/
protected void screenIo2010()
	{
		/*    CALL 'S6002IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                S6002-DATA-AREA.                 */
		/* Catering for F11*/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.edterror.set(varcom.oK);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2400-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.display2050);
		}
		puptIO.setChdrcoy(wsspcomn.company);
		puptIO.setChdrnum(covrpf.getChdrnum());
		puptIO.setLife(covrpf.getLife());
		puptIO.setCoverage(covrpf.getCoverage());
		puptIO.setRider(covrpf.getRider());
		puptIO.setPlanSuffix(covrpf.getPlanSuffix());
		puptIO.setFormat(formatsInner.puptrec);
		puptIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, puptIO);
		if (isEQ(puptIO.getStatuz(), varcom.oK)) {
			sv.chdrstatusErr.set(errorsInner.h009);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		if (isEQ(wsaaRiderErr, "Y")) {
			sv.riderErr.set(errorsInner.f050);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		/* If paid up method not found in T5687 then display error         */
		/* message and abort program.                                      */
		if (isEQ(wsaaPumeth1Err, "Y")) {
			sv.pumethErr.set(errorsInner.i027);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		/*  Check that the Component has been in-force at least the no.    */
		/*  of months specified on T6648. If the number of months between  */
		/*  COVRMJA-CRRD and CHDRMJA-BTDATE is less then T6648-MINMTHIF    */
		/*  for the corresponding CHDRMJA-BILLFREQ pay-up is disallowed.   */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		wsaaKeyItem1.set(t5687rec.pumeth);
		wsaaKeyItem2.set(chdrpf.getCntcurr());
		itdmIO.setItemitem(wsaaKey);
		itdmIO.setItemtabl(tablesInner.t6648);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setItmfrm(chdrpf.getPtdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6648)
		|| isNE(itdmIO.getItemitem(), wsaaKey)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		else {
			t6648rec.t6648Rec.set(itdmIO.getGenarea());
		}
		datcon3rec.function.set("CMDF");
		datcon3rec.frequency.set("12");
		datcon3rec.intDate1.set(covrpf.getCrrcd());
		datcon3rec.intDate2.set(payrIO.getBtdate());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		if (isEQ(chdrpf.getBillfreq(), "00")) {
			wsaaSub1.set(1);
		}
		else {
			if (isEQ(chdrpf.getBillfreq(), "01")) {
				wsaaSub1.set(2);
			}
			else {
				if (isEQ(chdrpf.getBillfreq(), "02")) {
					wsaaSub1.set(3);
				}
				else {
					if (isEQ(chdrpf.getBillfreq(), "04")) {
						wsaaSub1.set(4);
					}
					else {
						wsaaSub1.set(5);
					}
				}
			}
		}
		if (isLT(datcon3rec.freqFactor, t6648rec.minmthif[wsaaSub1.toInt()])) {
			sv.coverageErr.set(errorsInner.h010);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		/* Check that Sum-Assured is >= Min-Sum-Assured on T6648           */
		if (isLT(covrpf.getSumins(), t6648rec.minisuma[wsaaSub1.toInt()])) {
			sv.coverageErr.set(errorsInner.h011);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		/* Check that the remaining policy term is at least as long        */
		/* specified on T6648                                              */
		datcon3rec.function.set("CMDF");
		datcon3rec.frequency.set("12");
		datcon3rec.intDate1.set(payrIO.getPtdate());
		datcon3rec.intDate2.set(covrpf.getRiskCessDate());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		if (isLT(datcon3rec.freqFactor, t6648rec.minterm[wsaaSub1.toInt()])) {
			sv.ptdateErr.set(errorsInner.h012);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
	}

protected void checkUtrns2120()
	{
		/* Check if outstanding UTRNs exist.  If they do, display a        */
		/* message and disallow 'make paid up'                             */
		utrnaloIO.setRecKeyData(SPACES);
		utrnaloIO.setRecNonKeyData(SPACES);
		utrnaloIO.setStatuz(varcom.oK);
		utrnaloIO.setChdrcoy(wsspcomn.company);
		utrnaloIO.setChdrnum(wsaaCovrChdrnum);
		utrnaloIO.setLife(wsaaCovrLife);
		utrnaloIO.setCoverage(wsaaCovrCoverage);
		utrnaloIO.setRider(wsaaCovrRider);
		utrnaloIO.setFunction(varcom.begn);
		while ( !(isEQ(utrnaloIO.getStatuz(), varcom.endp))) {
			readUtrnalo5600();
		}
		
		if (isEQ(wsaaCoverageErr, "Y")) {
			sv.coverageErr.set(errorsInner.f050);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
	}

protected void checkCovr2030()
	{
		wsaaValidStatuz = "N";
		wsaaAlreadyPaidup.set("N");
		if (isEQ(covrpf.getRider(), "00")) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1, 12)
			|| isEQ(wsaaValidStatuz, "Y")); wsaaSub1.add(1)){
				validateCovr2450();
			}
		}
		else {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1, 12)
			|| isEQ(wsaaValidStatuz, "Y")); wsaaSub1.add(1)){
				validateRidr2500();
			}
		}
		if (isEQ(wsaaValidStatuz, "N")) {
			sv.coverageErr.set(errorsInner.i028);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		if ((isNE(t5679rec.setCnPremStat, SPACES))
		&& (isEQ(t5679rec.setCnPremStat, covrpf.getPstatcode()))) {
			if ((isNE(t5679rec.setCnRiskStat, SPACES))
			&& (isEQ(t5679rec.setCnRiskStat, covrpf.getStatcode()))) {
				wsaaAlreadyPaidup.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}
		if (isEQ(chdrpf.getBillfreq(), "00")) {
			sv.billfreqErr.set(errorsInner.h014);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		if (isEQ(covrpf.getInstprem(), ZERO)) {
			sv.fundamntErr.set(errorsInner.g818);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		if (isEQ(wsaaPumethErr, "Y")) {
			sv.pumethErr.set(errorsInner.h020);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		if (isEQ(wsaaPumeth2Err, "Y")) {
			sv.pumethErr.set(errorsInner.h021);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		if (isEQ(wsaaScrnErr, "Y")) {
			scrnparams.errorCode.set(errorsInner.e494);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		if (isEQ(wsaaCntcurrErr, "Y")) {
			sv.cntcurrErr.set(errorsInner.h386);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		if (isEQ(wsaaFundamntErr, "Y")) {
			sv.fundamntErr.set(errorsInner.h022);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
		if (isEQ(wsaaPufeeErr, "Y")) {
			sv.pufeeErr.set(errorsInner.h019);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.display2050);
		}
	}

protected void display2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	* Sections Performed by the 2000 Section
	* </pre>
	*/
protected void validateCovr2450()
	{
		/*COVR*/
		/*IF T5679-RID-PREM-STAT(WSAA-SUB1) NOT = COVRMJA-PSTATCODE    */
		/*ADD 1                    TO WSAA-SUB1                     */
		/*ELSE                                                         */
		/*MOVE 'Y'                 TO WSAA-VALID-STATUZ.            */
		/*IF (T5679-COV-PREM-STAT(WSAA-SUB1) NOT = COVRMJA-PSTATCODE)<0*/
		/*AND (T5679-COV-RISK-STAT(WSAA-SUB1) NOT = COVRMJA-STATCODE)<0*/
		/*   ADD 1                    TO WSAA-SUB1                <007>*/
		/*ELSE                                                    <007>*/
		/*   MOVE 'Y'                 TO WSAA-VALID-STATUZ.       <007>*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub1.toInt()],covrpf.getStatcode())) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub1.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaSub1.toInt()],covrpf.getPstatcode())) {
					wsaaValidStatuz = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void validateRidr2500()
	{
		/*RIDR*/
		/*IF T5679-RID-RISK-STAT(WSAA-SUB1) NOT = COVRMJA-STATCODE     */
		/*ADD 1                    TO WSAA-SUB1                     */
		/*ELSE                                                         */
		/*MOVE 'Y'                 TO WSAA-VALID-STATUZ.            */
		/*IF (T5679-RID-PREM-STAT(WSAA-SUB1) NOT = COVRMJA-PSTATCODE)<0*/
		/*AND (T5679-RID-RISK-STAT(WSAA-SUB1) NOT = COVRMJA-STATCODE)<0*/
		/*   ADD 1                    TO WSAA-SUB1                <007>*/
		/*ELSE                                                    <007>*/
		/*   MOVE 'Y'                 TO WSAA-VALID-STATUZ.       <007>*/
		if (isEQ(t5679rec.ridRiskStat[wsaaSub1.toInt()],covrpf.getStatcode())) {
			for (wsaaSub1.set(1); !(isGT(wsaaSub1,12)
			|| isEQ(wsaaValidStatuz,"Y")); wsaaSub1.add(1)){
				if (isEQ(t5679rec.ridPremStat[wsaaSub1.toInt()],covrpf.getPstatcode())) {
					wsaaValidStatuz = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void initTable2600()
	{
		/*START*/
		wsaaFund[fundIndex.toInt()].set(SPACES);
		wsaaFundCurr[fundIndex.toInt()].set(SPACES);
		wsaaFundUnits[fundIndex.toInt()].set(ZERO);
		wsaaFundAcumValue[fundIndex.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void calcFee2700()
	{
		start2700();
	}

protected void start2700()
	{
		/*    SET UP THE CALL TO CALCFEE USING OVRDUEREC TO                */
		/*    CALCULATE PAID UP FEE                                        */
		ovrduerec.ovrdueRec.set(SPACES);
		ovrduerec.language.set(wsspcomn.language);
		ovrduerec.chdrcoy.set(chdrpf.getChdrcoy());
		ovrduerec.chdrnum.set(chdrpf.getChdrnum());
		ovrduerec.life.set(covrpf.getLife());
		ovrduerec.coverage.set(covrpf.getCoverage());
		ovrduerec.rider.set(covrpf.getRider());
		ovrduerec.planSuffix.set(ZERO);
		ovrduerec.tranno.set(chdrpf.getTranno());
		ovrduerec.cntcurr.set(chdrpf.getCntcurr());
		ovrduerec.effdate.set(chdrpf.getOccdate());
		ovrduerec.outstamt.set(chdrpf.getOutstamt());
		ovrduerec.ptdate.set(chdrpf.getPtdate());
		ovrduerec.btdate.set(chdrpf.getBtdate());
		ovrduerec.ovrdueDays.set(ZERO);
		ovrduerec.agntnum.set(chdrpf.getAgntnum());
		ovrduerec.cownnum.set(chdrpf.getCownnum());
		ovrduerec.trancode.set(wsaaBatckey.batcBatctrcde);
		ovrduerec.acctyear.set(wsaaBatckey.batcBatcactyr);
		ovrduerec.acctmonth.set(wsaaBatckey.batcBatcactmn);
		ovrduerec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ovrduerec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		ovrduerec.user.set(varcom.vrcmUser);
		/* MOVE WSSP-COMPANY           TO OVRD-COMPANY.            <011>*/
		ovrduerec.company.set(chdrpf.getCowncoy());
		ovrduerec.tranDate.set(varcom.vrcmDate);
		ovrduerec.tranTime.set(varcom.vrcmTime);
		/*  MOVE VRCM-TERM              TO OVRD-TERMID.                  */
		ovrduerec.termid.set(varcom.vrcmTermid);
		ovrduerec.crtable.set(covrpf.getCrtable());
		ovrduerec.pumeth.set(t5687rec.pumeth);
		ovrduerec.billfreq.set(chdrpf.getBillfreq());
		ovrduerec.instprem.set(covrpf.getInstprem());
		ovrduerec.sumins.set(covrpf.getSumins());
		ovrduerec.crrcd.set(covrpf.getCrrcd());
		ovrduerec.premCessDate.set(covrpf.getPremCessDate());
		ovrduerec.cnttype.set(chdrpf.getCnttype());
		ovrduerec.occdate.set(chdrpf.getOccdate());
		ovrduerec.aloind.set(SPACES);
		ovrduerec.efdcode.set(SPACES);
		/* MOVE SPACES                 TO OVRD-PROC-SEQ-NO.        <011>*/
		ovrduerec.procSeqNo.set(ZERO);
		ovrduerec.newSumins.set(ZERO);
		ovrduerec.description.set(SPACES);
		ovrduerec.actualVal.set(ZERO);
		ovrduerec.polsum.set(ZERO);
		ovrduerec.runDate.set(wsaaToday);
		/* Call the CALCFEE subroutine in order to calculate               */
		/* OVRD-PUPFEE                                                     */
		callProgram(Calcfee.class, ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz,varcom.oK)) {
			syserrrec.statuz.set(ovrduerec.statuz);
			syserrrec.params.set(ovrduerec.ovrdueRec);
			fatalError600();
		}
		zrdecplrec.amountIn.set(ovrduerec.pupfee);
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		b000CallRounding();
		ovrduerec.pupfee.set(zrdecplrec.amountOut);
	}

protected void calcNewSumins2800()
	{
		start2800();
	}

protected void start2800()
	{
		/*    Set up the OVRD-OVRDUE-REC to pass to T6598-CALCPROG.        */
		ovrduerec.ovrdueRec.set(SPACES);
		ovrduerec.language.set(wsspcomn.language);
		ovrduerec.chdrcoy.set(chdrpf.getChdrcoy());
		ovrduerec.chdrnum.set(chdrpf.getChdrnum());
		ovrduerec.life.set(covrpf.getLife());
		ovrduerec.coverage.set(covrpf.getCoverage());
		ovrduerec.rider.set(covrpf.getRider());
		ovrduerec.planSuffix.set(ZERO);
		ovrduerec.tranno.set(chdrpf.getTranno());
		ovrduerec.cntcurr.set(chdrpf.getCntcurr());
		ovrduerec.effdate.set(chdrpf.getOccdate());
		ovrduerec.outstamt.set(chdrpf.getOutstamt());
		ovrduerec.ptdate.set(chdrpf.getPtdate());
		ovrduerec.btdate.set(chdrpf.getBtdate());
		ovrduerec.ovrdueDays.set(ZERO);
		ovrduerec.agntnum.set(chdrpf.getAgntnum());
		ovrduerec.cownnum.set(chdrpf.getCownnum());
		ovrduerec.trancode.set(wsaaBatckey.batcBatctrcde);
		ovrduerec.acctyear.set(wsaaBatckey.batcBatcactyr);
		ovrduerec.acctmonth.set(wsaaBatckey.batcBatcactmn);
		ovrduerec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ovrduerec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		ovrduerec.user.set(varcom.vrcmUser);
		/* MOVE WSSP-COMPANY           TO OVRD-COMPANY.            <021>*/
		ovrduerec.tranDate.set(varcom.vrcmDate);
		ovrduerec.tranTime.set(varcom.vrcmTime);
		ovrduerec.termid.set(varcom.vrcmTermid);
		ovrduerec.crtable.set(covrpf.getCrtable());
		ovrduerec.pumeth.set(t5687rec.pumeth);
		ovrduerec.billfreq.set(chdrpf.getBillfreq());
		ovrduerec.instprem.set(covrpf.getInstprem());
		ovrduerec.sumins.set(covrpf.getSumins());
		ovrduerec.crrcd.set(covrpf.getCrrcd());
		ovrduerec.premCessDate.set(covrpf.getPremCessDate());
		ovrduerec.pupfee.set(ZERO);
		ovrduerec.cnttype.set(chdrpf.getCnttype());
		ovrduerec.occdate.set(chdrpf.getOccdate());
		ovrduerec.aloind.set(SPACES);
		ovrduerec.efdcode.set(SPACES);
		ovrduerec.procSeqNo.set(ZERO);
		ovrduerec.newSumins.set(ZERO);
		ovrduerec.description.set(SPACES);
		ovrduerec.actualVal.set(ZERO);
		ovrduerec.polsum.set(ZERO);
		/* Call the T6598-CALCPROG subroutine in order to calculate        */
		/* OVRD-NEW-SUMINS.                                                */
		callProgram(t6598rec.calcprog, ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz,varcom.oK)) {
			syserrrec.statuz.set(ovrduerec.statuz);
			syserrrec.params.set(ovrduerec.ovrdueRec);
			fatalError600();
		}
		zrdecplrec.amountIn.set(ovrduerec.newSumins);
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		b000CallRounding();
		ovrduerec.sumins.set(zrdecplrec.amountOut);
	}

protected void setUpScreen2900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2910();
				case continue2110: 
					continue2110();
					checkUtrns12120();
					feeCheck2200();
				case readUtrs2250: 
					readUtrs2250();
				case setFields2300: 
					setFields2300();
				case display2350: 
					display2350();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	****************************                              <D509CS>
	* </pre>
	*/
protected void para2910()
	{
		/* MOVE O-K                    TO WSSP-EDTERROR.        <LA5128>*/
		/* IF S6002-ERROR-INDICATORS   NOT = SPACES             <LA5128>*/
		/*    GO TO 2350-DISPLAY.                               <LA5128>*/
		wsaaTotalFundValue.set(ZERO);
		for (fundIndex.set(1); !(isGT(fundIndex,40)); fundIndex.add(1)){
			initTable2600();
		}
		for (ibFund.set(1); !(isGT(ibFund,40)); ibFund.add(1)){
			a300InitIbTable();
		}
		wsaaSub2.set(1);
		/*VALIDATE*/
		/*                                                         <D509CS>*/
		/*    Validate fields                                      <D509CS>*/
		/*                                                         <D509CS>*/
		/*                                                       <D509CS>*/
		/* allow one pup per day                                   <D509CS>*/
		/*                                                       <D509CS>*/
		/*                                                         <D509CS>*/
		if (isEQ(chdrpf.getCurrfrom(),ZERO)) {
			goTo(GotoLabel.continue2110);
		}
	}

protected void continue2110()
	{
		/* MOVE WSSP-COMPANY           TO PUPT-CHDRCOY.         <LA5128>*/
		/* MOVE COVRMJA-CHDRNUM        TO PUPT-CHDRNUM.         <LA5128>*/
		/* MOVE COVRMJA-LIFE           TO PUPT-LIFE.            <LA5128>*/
		/* MOVE COVRMJA-COVERAGE       TO PUPT-COVERAGE.        <LA5128>*/
		/* MOVE COVRMJA-RIDER          TO PUPT-RIDER.           <LA5128>*/
		/* MOVE COVRMJA-PLAN-SUFFIX    TO PUPT-PLAN-SUFFIX.     <LA5128>*/
		/* MOVE PUPTREC                TO PUPT-FORMAT.          <LA5128>*/
		/* MOVE  READR                 TO PUPT-FUNCTION.        <LA5128>*/
		/* CALL 'PUPTIO'               USING PUPT-PARAMS.       <LA5128>*/
		/* IF PUPT-STATUZ              = O-K                    <LA5128>*/
		/*    MOVE H009                TO S6002-CHDRSTATUS-ERR  <LA5128>*/
		/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
		/*    GO TO 2350-DISPLAY.                               <LA5128>*/
		/*                                                         <D509CS>*/
		/* Check that the Coverage/Rider exists on T5687           <D509CS>*/
		/*                                                         <D509CS>*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),covrpf.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaRiderErr.set("Y");
			/*    MOVE F050                TO S6002-RIDER-ERR       <LA5128>*/
			/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
			goTo(GotoLabel.display2350);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		/*                                                         <D509CS>*/
		/* If paid up method not found in T5687 then display error <D509CS>*/
		/* message and abort program.                              <D509CS>*/
		/*                                                         <D509CS>*/
		/* IF T5687-PUMETH             = SPACES                 <LA5128>*/
		/*    MOVE I027                TO S6002-PUMETH-ERR      <LA5128>*/
		/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
		/*    GO TO 2350-DISPLAY.                               <LA5128>*/
		if (isEQ(t5687rec.pumeth,SPACES)) {
			wsaaPumeth1Err.set("Y");
			goTo(GotoLabel.display2350);
		}
	}

	/**
	* <pre>
	*                                                         <D509CS>
	* Check that the Component has been in-force at least the <D509CS>
	*  of months specified on T6648. If the number of months b<D509CS>
	*  COVRMJA-CRRD and CHDRMJA-BTDATE is less then T6648-MINM<D509CS>
	*  for the corresponding CHDRMJA-BILLFREQ pay-up is disall<D509CS>
	*                                                         <D509CS>
	**** MOVE SPACES                 TO ITDM-DATA-KEY.        <LA5128>
	**** MOVE WSSP-COMPANY           TO ITDM-ITEMCOY.         <LA5128>
	**** MOVE T5687-PUMETH           TO WSAA-KEY-ITEM1.       <LA5128>
	**** MOVE CHDRMJA-CNTCURR        TO WSAA-KEY-ITEM2.       <LA5128>
	**** MOVE WSAA-KEY               TO ITDM-ITEMITEM.        <LA5128>
	**** MOVE T6648                  TO ITDM-ITEMTABL.        <LA5128>
	**** MOVE ITEMREC                TO ITDM-FORMAT.          <LA5128>
	**** MOVE CHDRMJA-PTDATE         TO ITDM-ITMFRM.          <LA5128>
	**** MOVE BEGN                   TO ITDM-FUNCTION.        <LA5128>
	**** CALL 'ITDMIO'               USING ITDM-PARAMS.       <LA5128>
	**** IF ITDM-STATUZ              NOT = O-K AND            <LA5128>
	****    ITDM-STATUZ              NOT = ENDP               <LA5128>
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS           <LA5128>
	****    PERFORM 600-FATAL-ERROR.                          <LA5128>
	**** IF ITDM-ITEMCOY             NOT = WSSP-COMPANY OR    <LA5128>
	****    ITDM-ITEMTABL            NOT = T6648        OR    <LA5128>
	****    ITDM-ITEMITEM            NOT = WSAA-KEY     OR    <LA5128>
	****    ITDM-STATUZ              = ENDP                   <LA5128>
	****    MOVE ITDM-PARAMS         TO SYSR-PARAMS           <LA5128>
	****    PERFORM 600-FATAL-ERROR                           <LA5128>
	**** ELSE                                                 <LA5128>
	****    MOVE ITDM-GENAREA        TO T6648-T6648-REC.      <LA5128>
	**** MOVE 'CMDF'                 TO DTC3-FUNCTION.        <LA5128>
	**** MOVE '12'                   TO DTC3-FREQUENCY.       <LA5128>
	**** MOVE COVRMJA-CRRCD          TO DTC3-INT-DATE-1.      <LA5128>
	**** MOVE PAYR-BTDATE            TO DTC3-INT-DATE-2.      <LA5128>
	**** CALL 'DATCON3'              USING  DTC3-DATCON3-REC. <LA5128>
	**** IF DTC3-STATUZ              NOT = O-K                <LA5128>
	****    MOVE DTC3-DATCON3-REC    TO SYSR-PARAMS           <LA5128>
	****    MOVE DTC3-STATUZ         TO SYSR-STATUZ           <LA5128>
	****    PERFORM 600-FATAL-ERROR.                          <LA5128>
	**** IF CHDRMJA-BILLFREQ         = '00'                   <LA5128>
	****    MOVE 1                   TO WSAA-SUB1             <LA5128>
	**** ELSE                                                 <LA5128>
	****    IF CHDRMJA-BILLFREQ      = '01'                   <LA5128>
	****       MOVE 2                TO WSAA-SUB1             <LA5128>
	****    ELSE                                              <LA5128>
	****       IF CHDRMJA-BILLFREQ   = '02'                   <LA5128>
	****          MOVE 3             TO WSAA-SUB1             <LA5128>
	****       ELSE                                           <LA5128>
	****          IF CHDRMJA-BILLFREQ = '04'                  <LA5128>
	****             MOVE 4          TO WSAA-SUB1             <LA5128>
	****          ELSE                                        <LA5128>
	****             MOVE 5          TO WSAA-SUB1.            <LA5128>
	**** IF DTC3-FREQ-FACTOR < T6648-MINMTHIF(WSAA-SUB1)      <LA5128>
	****    MOVE H010                TO S6002-COVERAGE-ERR    <LA5128>
	****    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>
	****    GO TO 2300-SET-FIELDS.                            <LA5128>
	*                                                         <D509CS>
	* Check that Sum-Assured is >= Min-Sum-Assured on T6648   <D509CS>
	*                                                         <D509CS>
	**** IF COVRMJA-SUMINS < T6648-MINISUMA(WSAA-SUB1)        <LA5128>
	****    MOVE H011                TO S6002-COVERAGE-ERR    <LA5128>
	****    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>
	****    GO TO 2300-SET-FIELDS.                            <LA5128>
	*                                                         <D509CS>
	* Check that the remaining policy term is at least as long<D509CS>
	* specified on T6648                                      <D509CS>
	*                                                         <D509CS>
	**** MOVE 'CMDF'                 TO DTC3-FUNCTION.        <LA5128>
	**** MOVE '12'                   TO DTC3-FREQUENCY.       <LA5128>
	**** MOVE PAYR-PTDATE            TO DTC3-INT-DATE-1.      <LA5128>
	**** MOVE COVRMJA-RISK-CESS-DATE TO DTC3-INT-DATE-2.      <LA5128>
	**** CALL 'DATCON3'              USING  DTC3-DATCON3-REC. <LA5128>
	**** IF DTC3-STATUZ              NOT = O-K                <LA5128>
	****    MOVE DTC3-DATCON3-REC    TO SYSR-PARAMS           <LA5128>
	****    MOVE DTC3-STATUZ         TO SYSR-STATUZ           <LA5128>
	****    PERFORM 600-FATAL-ERROR.                          <LA5128>
	**** IF DTC3-FREQ-FACTOR < T6648-MINTERM(WSAA-SUB1)       <LA5128>
	****    MOVE H012                TO S6002-PTDATE-ERR      <LA5128>
	****    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>
	****    GO TO 2300-SET-FIELDS.                            <LA5128>
	* </pre>
	*/
protected void checkUtrns12120()
	{
		/* Check if outstanding UTRNs exist.  If they do, display a        */
		/* message and disallow 'make paid up'                             */
		/* INITIALIZE UTRNALOREC-KEY-DATA                       <LA5128>*/
		/*            UTRNALOREC-NON-KEY-DATA.                  <LA5128>*/
		/* MOVE WSSP-COMPANY           TO UTRNALO-CHDRCOY.      <LA5128>*/
		/* MOVE WSAA-COVR-CHDRNUM      TO UTRNALO-CHDRNUM.      <LA5128>*/
		/* MOVE BEGN                   TO UTRNALO-FUNCTION.     <LA5128>*/
		/* PERFORM 5600-READ-UTRNALO                            <LA5128>*/
		/*    UNTIL UTRNALO-STATUZ = ENDP.                      <LA5128>*/
		/*                                                         <D509CS>*/
		/* Check that the Covr/Rider status is on T5679            <D509CS>*/
		/*                                                         <D509CS>*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaCoverageErr.set("Y");
			/*    MOVE F050                TO S6002-COVERAGE-ERR    <LA5128>*/
			/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
			goTo(GotoLabel.display2350);
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		/*2150-CHECK-COVR.                                         <LA5128>*/
		/* MOVE 'N'                    TO WSAA-VALID-STATUZ.    <LA5128>*/
		/* MOVE 'N'                    TO WSAA-ALREADY-PAIDUP.  <LA5128>*/
		/* IF COVRMJA-RIDER            = '00'                   <LA5128>*/
		/*    PERFORM 2450-VALIDATE-COVR                        <LA5128>*/
		/*    VARYING WSAA-SUB1 FROM 1 BY 1                     <LA5128>*/
		/*    UNTIL WSAA-SUB1 > 12    OR WSAA-VALID-STATUZ = 'Y'<LA5128>*/
		/* ELSE                                                 <LA5128>*/
		/*    PERFORM 2500-VALIDATE-RIDR                        <LA5128>*/
		/*    VARYING WSAA-SUB1 FROM 1 BY 1                     <LA5128>*/
		/*    UNTIL WSAA-SUB1 > 12    OR WSAA-VALID-STATUZ = 'Y'<LA5128>*/
		/* IF WSAA-VALID-STATUZ        = 'N'                    <LA5128>*/
		/*    MOVE I028                TO S6002-COVERAGE-ERR    <LA5128>*/
		/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
		/*    GO TO 2300-SET-FIELDS.                            <LA5128>*/
		/* IF (T5679-SET-CN-PREM-STAT NOT = SPACES) AND         <LA5128>*/
		/*    (T5679-SET-CN-PREM-STAT     = COVRMJA-PSTATCODE)  <LA5128>*/
		/*    IF (T5679-SET-CN-RISK-STAT NOT = SPACES) AND      <LA5128>*/
		/*       (T5679-SET-CN-RISK-STAT     = COVRMJA-STATCODE)<LA5128>*/
		/*       MOVE 'Y'              TO WSAA-ALREADY-PAIDUP   <LA5128>*/
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*          GO TO 2990-EXIT.                                       */
		/*       GO TO 2090-EXIT.                               <LA5128>*/
		/*                                                         <D509CS>*/
		/*  Btdate must = Ptdate or pay-up is disallowed           <D509CS>*/
		/*                                                         <D509CS>*/
		/* IF CHDRMJA-PTDATE           NOT = CHDRMJA-BTDATE     <LA5128>*/
		/*    MOVE H013                TO S6002-BTDATE-ERR      <LA5128>*/
		/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
		/*    GO TO 2300-SET-FIELDS.                            <LA5128>*/
		/* IF CHDRMJA-BILLFREQ         = '00'                   <LA5128>*/
		/*    MOVE H014                TO S6002-BILLFREQ-ERR    <LA5128>*/
		/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
		/*    GO TO 2300-SET-FIELDS.                            <LA5128>*/
		/* IF COVRMJA-INSTPREM         = ZERO                   <LA5128>*/
		/*    MOVE G818                TO S6002-FUNDAMNT-ERR    <LA5128>*/
		/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
		/*    GO TO 2300-SET-FIELDS.                            <LA5128>*/
		/*                                                         <D509CS>*/
		/* Get the Paid-Up processing rules from T6651             <D509CS>*/
		/*                                                         <D509CS>*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		wsaaKeyItem1.set(t5687rec.pumeth);
		wsaaKeyItem2.set(chdrpf.getCntcurr());
		itdmIO.setItemitem(wsaaKey);
		itdmIO.setItemtabl(tablesInner.t6651);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setItmfrm(chdrpf.getBtdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6651)
		|| isNE(itdmIO.getItemitem(),wsaaKey)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaPumethErr.set("Y");
			/*    MOVE H020                TO S6002-PUMETH-ERR      <LA5128>*/
			/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
			goTo(GotoLabel.display2350);
		}
		else {
			t6651rec.t6651Rec.set(itdmIO.getGenarea());
		}
	}

protected void feeCheck2200()
	{
		/*                                                         <D509CS>*/
		/* Read T6598 to get the Calculation subroutine name       <D509CS>*/
		/*                                                         <D509CS>*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemitem(t5687rec.pumeth);
		itemIO.setItemtabl(tablesInner.t6598);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaPumeth2Err.set("Y");
			/*    MOVE H021                TO S6002-PUMETH-ERR      <LA5128>*/
			/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
			goTo(GotoLabel.display2350);
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
		/*                                                         <D509CS>*/
		/* If T6598 Calculation subroutine name is spaces zeroise t<D509CS>*/
		/*    fee-charge and do not call the subroutine else       <D509CS>*/
		/*    call the paid up processing subroutine               <D509CS>*/
		/*                                                         <D509CS>*/
		/*                                                    <D509CS>*/
		/*                                                        <D509CS>*/
		/*     Even though the subroutine is no longer called      <D509CS>*/
		/*     we perform the Fee calculation section              <D509CS>*/
		/*     as we would normally, the subroutine has been       <D509CS>*/
		/*     replaced by as section.                             <D509CS>*/
		/*                                                        <D509CS>*/
		/*                                                    <D509CS>*/
		if (isEQ(t6598rec.calcprog,SPACES)) {
			puptIO.setPupfee(ZERO);
			sv.pufee.set(ZERO);
			ovrduerec.pupfee.set(ZERO);
			goTo(GotoLabel.readUtrs2250);
		}
		else {
			ovrduerec.pupfee.set(ZERO);
			calcFee2700();
			sv.pufee.set(ovrduerec.pupfee);
		}
	}

protected void readUtrs2250()
	{
		/*                                                         <D509CS>*/
		/* Set up UTRS and perform a BEGN                          <D509CS>*/
		/*                                                         <D509CS>*/
		utrsIO.setRecKeyData(SPACES);
		utrsIO.setRecNonKeyData(SPACES);
		utrsIO.setStatuz(varcom.oK);
		utrsIO.setPlanSuffix(ZERO);
		utrsIO.setChdrcoy(chdrpf.getChdrcoy());
		utrsIO.setChdrnum(chdrpf.getChdrnum());
		utrsIO.setLife(covrpf.getLife());
		utrsIO.setCoverage(covrpf.getCoverage());
		utrsIO.setRider(covrpf.getRider());
		utrsIO.setUnitVirtualFund(SPACES);
		utrsIO.setUnitType(SPACES);
		utrsIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		utrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		utrsIO.setFormat(formatsInner.utrsrec);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(),utrsIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),utrsIO.getChdrnum())
		|| isNE(covrpf.getLife(),utrsIO.getLife())
		|| isNE(covrpf.getCoverage(),utrsIO.getCoverage())
		|| isNE(covrpf.getRider(),utrsIO.getRider())
		|| isEQ(utrsIO.getStatuz(),varcom.endp)) {
			utrsIO.setStatuz(varcom.endp);
		}
		hitsIO.setParams(SPACES);
		hitsIO.setFunction(varcom.begn);

		//performance improvement --  Niharika Modi 
		hitsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitsIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		hitsIO.setChdrcoy(chdrpf.getChdrcoy());
		hitsIO.setChdrnum(chdrpf.getChdrnum());
		hitsIO.setLife(covrpf.getLife());
		hitsIO.setCoverage(covrpf.getCoverage());
		hitsIO.setRider(covrpf.getRider());
		hitsIO.setPlanSuffix(ZERO);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		|| isNE(hitsIO.getChdrcoy(),chdrpf.getChdrcoy())
		|| isNE(hitsIO.getChdrnum(),chdrpf.getChdrnum())
		|| isNE(hitsIO.getLife(),covrpf.getLife())
		|| isNE(hitsIO.getCoverage(),covrpf.getCoverage())
		|| isNE(hitsIO.getRider(),covrpf.getRider())) {
			hitsIO.setStatuz(varcom.endp);
		}
		if (isEQ(utrsIO.getStatuz(),varcom.endp)
		&& isEQ(hitsIO.getStatuz(),varcom.endp)) {
			wsaaScrnErr.set("Y");
			/*    MOVE E494                TO SCRN-ERROR-CODE       <LA5128>*/
			utrsIO.setStatuz(varcom.endp);
			/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
			goTo(GotoLabel.display2350);
		}
		/* The screen must be displayed otherwise we LOOP.         <D509CS>*/
		sv.fundAmount.set(ZERO);
		while ( !(isEQ(utrsIO.getStatuz(),varcom.endp))) {
			calcFundValues5200();
		}
		
		/* IF WSSP-EDTERROR            = 'Y'                    <LA5128>*/
		/*    GO TO 2300-SET-FIELDS.                            <LA5128>*/
		if (isEQ(wsaaCntcurrErr, "Y")) {
			goTo(GotoLabel.setFields2300);
		}
		while ( !(isEQ(hitsIO.getStatuz(),varcom.endp))) {
			a100CalcIbFundValues();
		}
		
		/* IF WSAA-FUND-VALUE          = ZERO                           */
		if (isEQ(wsaaTotalFundValue,ZERO)) {
			wsaaFundamntErr.set("Y");
			/*    MOVE H022                TO S6002-FUNDAMNT-ERR    <LA5128>*/
			/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
			goTo(GotoLabel.setFields2300);
		}
		sv.fundAmount.set(wsaaTotalFundValue);
		/*                                                       <D509CS>*/
		/*     Sum insured is also been generated incorrectly if <D509CS>*/
		/*     we are looking at summarized records.             <D509CS>*/
		/*                                                       <D509CS>*/
		if (wsaaWholePlan.isTrue()) {
			sv.sumin.set(covrpf.getSumins());
		}
		else {
			if (isGT(covrpf.getPlanSuffix(),chdrpf.getPolsum())) {
				sv.sumin.set(covrpf.getSumins());
			}
			else {
				compute(sv.sumin, 2).set(div(covrpf.getSumins(),chdrpf.getPolsum()));
			}
		}
		if (isNE(ovrduerec.pupfee,ZERO)) {
			for (fundIndex.set(1); !(isGT(fundIndex,40)
			|| isEQ(wsaaFund[fundIndex.toInt()],SPACES)
			|| isEQ(wsaaPufeeErr, "Y")); fundIndex.add(1)){
				validatePupcFee5300();
			}
		}
		if (isNE(ovrduerec.pupfee,ZERO)) {
			for (ibFund.set(1); !(isGT(ibFund,40)
			|| isEQ(wsaaIbFund[ibFund.toInt()],SPACES)
			|| isEQ(wsaaPufeeErr, "Y")); ibFund.add(1)){
				a200ValidateIbPupcFee();
			}
		}
	}

	/**
	* <pre>
	**** IF WSSP-EDTERROR            = 'Y'                    <LA5128>
	****    GO TO 2300-SET-FIELDS.                            <LA5128>
	* </pre>
	*/
protected void setFields2300()
	{
		if (isEQ(t6598rec.calcprog,SPACES)) {
			sv.newinsval.set(covrpf.getSumins());
			sv.pufee.set(ZERO);
		}
		else {
			sv.pufee.set(ovrduerec.pupfee);
			wsaaOvrdPupfee.set(ovrduerec.pupfee);
			calcNewSumins2800();
			sv.newinsval.set(ovrduerec.newSumins);
		}
		sv.crrcd.set(covrpf.getCrrcd());
		/*                                                         <D509CS>*/
		/* Display Paid-up method and get the                      <D509CS>*/
		/*     Paid-up Method Description from T5698               <D509CS>*/
		/*                                                         <D509CS>*/
		sv.pumeth.set(t5687rec.pumeth);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t6598);
		descIO.setDescitem(t5687rec.pumeth);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pumethdesc.fill("?");
		}
		else {
			sv.pumethdesc.set(descIO.getLongdesc());
		}
		/*                                                         <D509CS>*/
		/* Display Risk-status and get the                         <D509CS>*/
		/*     Component Risk-status-Desc from T5682               <D509CS>*/
		/*                                                         <D509CS>*/
		sv.statcode.set(covrpf.getStatcode());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5682);
		descIO.setDescitem(covrpf.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.statdesc.fill("?");
		}
		else {
			sv.statdesc.set(descIO.getLongdesc());
		}
		/*                                                         <D509CS>*/
		/* Display the Prem-status and get the                     <D509CS>*/
		/*     Component PREM-STATUS-DESC from T5681               <D509CS>*/
		/*                                                         <D509CS>*/
		sv.pstatcode.set(covrpf.getPstatcode());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5681);
		descIO.setDescitem(covrpf.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.pstatdesc.fill("?");
		}
		else {
			sv.pstatdesc.set(descIO.getLongdesc());
		}
	}

protected void display2350()
	{
		/* IF S6002-ERROR-INDICATORS   NOT = SPACES             <LA5128>*/
		/*    MOVE 'Y'                 TO WSSP-EDTERROR.        <LA5128>*/
		/*                                                         <D509CS>*/
		/* Protect is set on each individual field rather then on t<D509CS>*/
		/*  whole screen because for future modification S6002-NEWS<D509CS>*/
		/*  on certain pay-up method calculations will be allowed t<D509CS>*/
		/*  altered.                                               <D509CS>*/
		/*                                                         <D509CS>*/
		if (isNE(t6651rec.ovrsuma,"Y")) {
			sv.newinsvalOut[varcom.pr.toInt()].set("Y");
		}
		sv.bilfrqdescOut[varcom.pr.toInt()].set("Y");
		sv.billfreqOut[varcom.pr.toInt()].set("Y");
		sv.btdateOut[varcom.pr.toInt()].set("Y");
		sv.chdrnumOut[varcom.pr.toInt()].set("Y");
		sv.chdrstatusOut[varcom.pr.toInt()].set("Y");
		sv.cntcurrOut[varcom.pr.toInt()].set("Y");
		sv.cnttypeOut[varcom.pr.toInt()].set("Y");
		sv.coverageOut[varcom.pr.toInt()].set("Y");
		sv.crrcdOut[varcom.pr.toInt()].set("Y");
		sv.crtabdescOut[varcom.pr.toInt()].set("Y");
		sv.crtableOut[varcom.pr.toInt()].set("Y");
		sv.ctypedesOut[varcom.pr.toInt()].set("Y");
		sv.fundamntOut[varcom.pr.toInt()].set("Y");
		sv.jlifenameOut[varcom.pr.toInt()].set("Y");
		sv.jlifenumOut[varcom.pr.toInt()].set("Y");
		sv.lifeOut[varcom.pr.toInt()].set("Y");
		sv.lifenameOut[varcom.pr.toInt()].set("Y");
		sv.lifenumOut[varcom.pr.toInt()].set("Y");
		sv.numpolsOut[varcom.pr.toInt()].set("Y");
		sv.plnsfxOut[varcom.pr.toInt()].set("Y");
		sv.premstatusOut[varcom.pr.toInt()].set("Y");
		sv.pstatcodeOut[varcom.pr.toInt()].set("Y");
		sv.pstatdescOut[varcom.pr.toInt()].set("Y");
		sv.ptdateOut[varcom.pr.toInt()].set("Y");
		sv.pufeeOut[varcom.pr.toInt()].set("Y");
		sv.pumethOut[varcom.pr.toInt()].set("Y");
		sv.pumethdescOut[varcom.pr.toInt()].set("Y");
		sv.registerOut[varcom.pr.toInt()].set("Y");
		sv.riderOut[varcom.pr.toInt()].set("Y");
		sv.statcodeOut[varcom.pr.toInt()].set("Y");
		sv.statdescOut[varcom.pr.toInt()].set("Y");
		sv.suminOut[varcom.pr.toInt()].set("Y");
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
			updateDatabase3100();
		}

protected void updateDatabase3100()
	{
		if (isEQ(wsaaAlreadyPaidup,"Y")) {
			return ;
		}
		/* Catering for F11*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		/*  Write a PUPT record for the component to be paid-up*/
		puptIO.setDataKey(SPACES);
		puptIO.setChdrcoy(covrpf.getChdrcoy());
		puptIO.setChdrnum(covrpf.getChdrnum());
		puptIO.setLife(covrpf.getLife());
		puptIO.setCoverage(covrpf.getCoverage());
		puptIO.setRider(covrpf.getRider());
		puptIO.setPlanSuffix(covrpf.getPlanSuffix());
		puptIO.setPumeth(t5687rec.pumeth);
		puptIO.setNewsuma(sv.newinsval);
		/*    MOVE PUPC-NEW-SUMINS        TO PUPT-NEWSUMA.*/
		/* MOVE WSAA-FUND-VALUE        TO PUPT-FUND-AMOUNT.*/
		puptIO.setFundAmount(wsaaTotalFundValue);
		/* MOVE PUPC-FEE-CHARGED       TO PUPT-PUPFEE.                  */
		/* MOVE OVRD-PUPFEE            TO PUPT-PUPFEE.             <011>*/
		puptIO.setPupfee(wsaaOvrdPupfee);
		/*  MOVE VRCM-TERM              TO PUPT-TERMID.                  */
		puptIO.setTermid(varcom.vrcmTermid);
		puptIO.setTransactionDate(varcom.vrcmDate);
		puptIO.setTransactionTime(varcom.vrcmTime);
		puptIO.setUser(varcom.vrcmUser);
		puptIO.setFormat(formatsInner.puptrec);
		puptIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, puptIO);
		if (isNE(puptIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(puptIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
			nextProgram4010();
		}

protected void nextProgram4010()
	{
		if (isEQ(scrnparams.statuz,"KILL")) {
			wayout4300();
			return ;
		}
		wsspcomn.nextprog.set(wsaaProg);
		/* If we are processing a Whole Plan selection then we must*/
		/*   process all Policies for the selected Coverage/Rider,*/
		/*   Looping back to the 2000-section after having loaded the*/
		/*   screen for the next Policy*/
		/* Else we exit to the next program in the stack.*/
		if (wsaaWholePlan.isTrue()) {
			//covrmjaIO.setFunction(varcom.nextr);
			policyLoad5000();
			if (covrpf != null) {
				wsspcomn.nextprog.set(wsaaProg);
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
				wayout4300();
			}
			else {
				wsspcomn.nextprog.set(scrnparams.scrname);
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			}
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wayout4300();
		}
	}

protected void wayout4300()
	{
		/*PARA*/
		/* If control is passed to this  part of the 4000 section on the*/
		/* way  out of the program,  ie.  after  screen  I/O,  then  the*/
		/* current stack position action  flag  will  be  blank.  If the*/
		/* 4000  section  is   being   performed  after  returning  from*/
		/* processing another program  then  the  current stack position*/
		/* action flag will be '*'.*/
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/*    spaces to  the current program entry in the program*/
		/*    stack and exit.*/
		if (isEQ(scrnparams.statuz,"KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void policyLoad5000()
	{
		/*LOAD-POLICY*/
		if (wsaaWholePlan.isTrue()) {
			readCovr5100();
				if (covrpf == null) {
					return;
				}
		}
		/* The header is set to show which selection is being actioned.*/
		if (wsaaWholePlan.isTrue()) {
			if (isEQ(covrpf.getPlanSuffix(),ZERO)) {
				sv.planSuffix.set(ZERO);
				/************MOVE 'Y'              TO S6002-PLNSFX-OUT(HI)*/
			}
			else {
				sv.planSuffix.set(covrpf.getPlanSuffix());
			}
		}
		/*EXIT*/
	}

protected void readCovr5100()
	{
		/*READ-COVRMJA*/
		//ILIFE-8137
		covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
		for (Covrpf covr:covrpfList) {
			if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
					|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
					|| isNE(covr.getLife(),wsaaCovrLife)
					|| isNE(covr.getCoverage(),wsaaCovrCoverage)
					|| isNE(covr.getRider(),wsaaCovrRider)
					|| (!covrpfList.isEmpty())) {
						return;
					}
			covrpf=covr;
		}
		/*SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covrpf.getChdrcoy(),wsaaCovrChdrcoy)
		|| isNE(covrpf.getChdrnum(),wsaaCovrChdrnum)
		|| isNE(covrpf.getLife(),wsaaCovrLife)
		|| isNE(covrpf.getCoverage(),wsaaCovrCoverage)
		|| isNE(covrpf.getRider(),wsaaCovrRider)
		|| isEQ(covrmjaIO.getStatuz(),varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
		}*/
		/*EXIT*/
	}

protected void calcFundValues5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5200();
				case calc5225: 
					calc5225();
				case readNextUtrs5250: 
					readNextUtrs5250();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5200()
	{
		/*  For each UTRS read put a value in wsaa-current-unit-bal      */
		wsaaCurrentUnitBal.set(0);
		if (isEQ(covrpf.getPlanSuffix(),0)) {
			wsaaCurrentUnitBal.set(utrsIO.getCurrentDunitBal());
		}
		else {
			if (isGT(covrpf.getPlanSuffix(),chdrpf.getPolsum())
			&& isEQ(covrpf.getPlanSuffix(),utrsIO.getPlanSuffix())) {
				wsaaCurrentUnitBal.set(utrsIO.getCurrentDunitBal());
			}
			else {
				if (isLTE(covrpf.getPlanSuffix(),chdrpf.getPolsum())
				&& isEQ(utrsIO.getPlanSuffix(),0)) {
					compute(wsaaCurrentUnitBal, 6).setRounded(div(utrsIO.getCurrentUnitBal(),chdrpf.getPolsum()));
				}
			}
		}
		if (isEQ(wsaaCurrentUnitBal,ZERO)) {
			goTo(GotoLabel.readNextUtrs5250);
		}
		/* If the Unit-Type is I and T5515-ADJUSTIU is Y then include all*/
		/*    the units in the accumulation otherwise just add the*/
		/*    accumulated units.*/
		if (isEQ(utrsIO.getUnitType(),"I")
		&& isEQ(t6651rec.adjustiu,"N")) {
			goTo(GotoLabel.readNextUtrs5250);
		}
		fundIndex.set(1);
		 searchlabel1:
		{
			for (; isLT(fundIndex,wsaaProRataItem.length); fundIndex.add(1)){
				if (isEQ(wsaaFund[fundIndex.toInt()],utrsIO.getUnitVirtualFund())) {
					wsaaSub2.set(fundIndex);
					break searchlabel1;
				}
				if (isEQ(wsaaFund[fundIndex.toInt()],SPACES)) {
					wsaaFund[fundIndex.toInt()].set(utrsIO.getUnitVirtualFund());
					wsaaSub2.set(fundIndex);
					break searchlabel1;
				}
			}
		}
		/* If first time thru or change of fund read T5515 to determine*/
		/*  currency of Virtual Fund and obtain Bid or Offer Price*/
		if (isEQ(utrsIO.getUnitVirtualFund(),wsaaPreviousFund)) {
			goTo(GotoLabel.calc5225);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemitem(utrsIO.getUnitVirtualFund());
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(),utrsIO.getUnitVirtualFund())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaCntcurrErr.set("Y");
			goTo(GotoLabel.readNextUtrs5250);
			/*****    MOVE H386                TO S6002-CNTCURR-ERR             */
			/*****    MOVE 'Y'                 TO WSSP-EDTERROR                 */
			/*****    GO TO 5290-EXIT                                           */
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		wsaaFundCurr[fundIndex.toInt()].set(t5515rec.currcode);
		/* Read VPRC to get the bid-price (T6651-BIDOFFER = B) or*/
		/*                      offer-price (T6651-BIDOFFER = O)*/
		vprcIO.setParams(SPACES);
		vprcIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		/*  MOVE 'A'                    TO VPRC-UNIT-TYPE.               */
		vprcIO.setUnitType(utrsIO.getUnitType());
		vprcIO.setEffdate(chdrpf.getBtdate());
		vprcIO.setJobno(ZERO);
		vprcIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		vprcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		vprcIO.setFitKeysSearch("VRTFND","ULTYPE");
		vprcIO.setFormat(formatsInner.vprcrec);
		SmartFileCode.execute(appVars, vprcIO);
		if (isNE(vprcIO.getStatuz(),varcom.oK)
		|| isNE(vprcIO.getUnitVirtualFund(),utrsIO.getUnitVirtualFund())
		|| isNE(vprcIO.getUnitType(),utrsIO.getUnitType())) {
			syserrrec.params.set(vprcIO.getParams());
			fatalError600();
		}
		wsaaPreviousFund.set(utrsIO.getUnitVirtualFund());
	}

protected void calc5225()
	{
		calcValReal5500();
		if (isNE(chdrpf.getCntcurr(),t5515rec.currcode)) {
			convertCurrency5400();
		}
		wsaaFundAcumValue[wsaaSub2.toInt()].add(wsaaFundValue);
		wsaaTotalFundValue.add(wsaaFundValue);
	}

protected void readNextUtrs5250()
	{
		utrsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(),utrsIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),utrsIO.getChdrnum())
		|| isNE(covrpf.getLife(),utrsIO.getLife())
		|| isNE(covrpf.getCoverage(),utrsIO.getCoverage())
		|| isNE(covrpf.getRider(),utrsIO.getRider())) {
			/*     COVRMJA-RIDER            NOT = UTRS-RIDER    OR           */
			/*     COVRMJA-PLAN-SUFFIX      NOT = UTRS-PLAN-SUFFIX           */
			utrsIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void validatePupcFee5300()
	{
		/*START*/
		/*COMPUTE WSAA-FEE ROUNDED                                     */
		/* = OVRD-PUPFEE      * WSAA-FUND-ACUM-VALUE(FUND-INDEX)  <011>*/
		/* = PUPC-FEE-CHARGED * WSAA-FUND-ACUM-VALUE(FUND-INDEX)       */
		/* / WSAA-FUND-VALUE.                                          */
		/* Calculate the Fund fee as a percentage of the total fee         */
		compute(wsaaFee, 6).setRounded(mult(ovrduerec.pupfee,(div(wsaaFundAcumValue[fundIndex.toInt()],wsaaTotalFundValue))));
		if (wsaaFee.isTruncated()) {
			syserrrec.subrname.set(wsaaProg);
			wsaaErrSection.set("5300 SECTION");
			syserrrec.params.set(wsaaDbparams);
			syserrrec.statuz.set("COMP");
			fatalError600();
		}
		if (isGT(wsaaFee,wsaaFundAcumValue[fundIndex.toInt()])) {
			/*    MOVE 'Y'                 TO WSSP-EDTERROR                 */
			/*    MOVE H019                TO S6002-PUFEE-ERR.              */
			wsaaPufeeErr.set("Y");
		}
		/*EXIT*/
	}

protected void convertCurrency5400()
	{
		start5400();
	}

protected void start5400()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountIn.set(wsaaFundValue);
		/* MOVE 'REAL'                 TO CLNK-FUNCTION.                */
		conlinkrec.function.set("SURR");
		/* MOVE PUPC-CNTCURR           TO CLNK-CURR-OUT.                */
		conlinkrec.currOut.set(chdrpf.getCntcurr());
		conlinkrec.currIn.set(t5515rec.currcode);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		/* MOVE PUPC-BTDATE            TO CLNK-CASHDATE.                */
		conlinkrec.cashdate.set(chdrpf.getBtdate());
		conlinkrec.company.set(wsspcomn.company);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		b000CallRounding();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		wsaaFundValue.set(conlinkrec.amountOut);
	}

protected void calcValReal5500()
	{
		/*CALC*/
		wsaaFundValue.set(0);
		if (isEQ(t6651rec.bidoffer,"B")) {
			compute(wsaaFundValue, 5).set(mult(vprcIO.getUnitBidPrice(),wsaaCurrentUnitBal));
		}
		if (isEQ(t6651rec.bidoffer,"O")) {
			compute(wsaaFundValue, 5).set(mult(vprcIO.getUnitOfferPrice(),wsaaCurrentUnitBal));
		}
		zrdecplrec.amountIn.set(wsaaFundValue);
		zrdecplrec.currency.set(t5515rec.currcode);
		b000CallRounding();
		wsaaFundValue.set(zrdecplrec.amountOut);
		/*EXIT*/
	}

protected void readUtrnalo5600()
	{
		/*READ-UTRNALO*/
		SmartFileCode.execute(appVars, utrnaloIO);
		if (isNE(utrnaloIO.getStatuz(),varcom.oK)
		&& isNE(utrnaloIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(utrnaloIO.getStatuz());
			syserrrec.params.set(utrnaloIO.getParams());
			fatalError600();
		}
		if (isNE(utrnaloIO.getChdrcoy(),wsspcomn.company)
		|| isNE(utrnaloIO.getChdrnum(),wsaaCovrChdrnum)
		|| isNE(utrnaloIO.getLife(), wsaaCovrLife)
		|| isNE(utrnaloIO.getCoverage(), wsaaCovrCoverage)
		|| isNE(utrnaloIO.getRider(), wsaaCovrRider)
		|| isEQ(utrnaloIO.getStatuz(),varcom.endp)) {
			utrnaloIO.setStatuz(varcom.endp);
		}
		else {
			sv.coverageErr.set(errorsInner.h355);
			wsspcomn.edterror.set("Y");
			utrnaloIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void a100CalcIbFundValues()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a110Start();
				case a150Calc: 
					a150Calc();
				case a190Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a110Start()
	{
		ibFund.set(1);
		 searchlabel1:
		{
			for (; isLT(ibFund,wsaaIbProRataItem.length); ibFund.add(1)){
				if (isEQ(wsaaIbFund[ibFund.toInt()],hitsIO.getZintbfnd())) {
					wsaaSub2.set(ibFund);
					break searchlabel1;
				}
				if (isEQ(wsaaIbFund[ibFund.toInt()],SPACES)) {
					wsaaIbFund[ibFund.toInt()].set(hitsIO.getZintbfnd());
					wsaaSub2.set(ibFund);
					break searchlabel1;
				}
			}
		}
		if (isEQ(hitsIO.getZintbfnd(),wsaaPreviousFund)) {
			goTo(GotoLabel.a150Calc);
		}
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemitem(hitsIO.getZintbfnd());
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5515)
		|| isNE(itdmIO.getItemitem(),hitsIO.getZintbfnd())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaCntcurrErr.set("Y");
			/*    MOVE H386                TO S6002-CNTCURR-ERR     <LA5128>*/
			/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
			goTo(GotoLabel.a190Exit);
		}
		else {
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		wsaaIbFundCurr[ibFund.toInt()].set(t5515rec.currcode);
		wsaaPreviousFund.set(hitsIO.getZintbfnd());
	}

protected void a150Calc()
	{
		wsaaFundValue.set(hitsIO.getZcurprmbal());
		if (isNE(chdrpf.getCntcurr(),t5515rec.currcode)) {
			convertCurrency5400();
		}
		wsaaFundAcumValue[wsaaSub2.toInt()].add(wsaaFundValue);
		wsaaTotalFundValue.add(wsaaFundValue);
		hitsIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)
		&& isNE(hitsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(),hitsIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(),hitsIO.getChdrnum())
		|| isNE(covrpf.getLife(),hitsIO.getLife())
		|| isNE(covrpf.getCoverage(),hitsIO.getCoverage())
		|| isNE(covrpf.getRider(),hitsIO.getRider())) {
			hitsIO.setStatuz(varcom.endp);
		}
	}

protected void a200ValidateIbPupcFee()
	{
		/*A210-START*/
		/* Calculate the Fund fee as a percentage of the total fee         */
		compute(wsaaFee, 6).setRounded(mult(ovrduerec.pupfee,(div(wsaaIbFundAcumValue[ibFund.toInt()],wsaaTotalFundValue))));
		if (wsaaFee.isTruncated()) {
			syserrrec.subrname.set(wsaaProg);
			wsaaErrSection.set("A200 SECTION");
			syserrrec.params.set(wsaaDbparams);
			syserrrec.statuz.set("COMP");
			fatalError600();
		}
		if (isGT(wsaaFee,wsaaIbFundAcumValue[ibFund.toInt()])) {
			/*    MOVE 'Y'                 TO WSSP-EDTERROR         <LA5128>*/
			/*    MOVE H019                TO S6002-PUFEE-ERR       <LA5128>*/
			wsaaPufeeErr.set("Y");
		}
		/*A290-EXIT*/
	}

protected void a300InitIbTable()
	{
		/*A310-INIT*/
		wsaaIbFund[ibFund.toInt()].set(SPACES);
		wsaaIbFundCurr[ibFund.toInt()].set(SPACES);
		wsaaIbFundAcumValue[ibFund.toInt()].set(ZERO);
		/*A390-EXIT*/
	}

protected void b000CallRounding()
	{
		/*B100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*B900-EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e494 = new FixedLengthStringData(24).init("E494");
	private FixedLengthStringData f050 = new FixedLengthStringData(4).init("F050");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h009 = new FixedLengthStringData(4).init("H009");
	private FixedLengthStringData h010 = new FixedLengthStringData(4).init("H010");
	private FixedLengthStringData h011 = new FixedLengthStringData(4).init("H011");
	private FixedLengthStringData h012 = new FixedLengthStringData(4).init("H012");
	private FixedLengthStringData h014 = new FixedLengthStringData(4).init("H014");
	private FixedLengthStringData h019 = new FixedLengthStringData(4).init("H019");
	private FixedLengthStringData h020 = new FixedLengthStringData(4).init("H020");
	private FixedLengthStringData h021 = new FixedLengthStringData(4).init("H021");
	private FixedLengthStringData h022 = new FixedLengthStringData(4).init("H022");
	private FixedLengthStringData h355 = new FixedLengthStringData(4).init("H355");
	private FixedLengthStringData h386 = new FixedLengthStringData(4).init("H386");
	private FixedLengthStringData i027 = new FixedLengthStringData(4).init("I027");
	private FixedLengthStringData i028 = new FixedLengthStringData(4).init("I028");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t3588 = new FixedLengthStringData(5).init("T3588");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t3623 = new FixedLengthStringData(5).init("T3623");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5681 = new FixedLengthStringData(5).init("T5681");
	private FixedLengthStringData t5682 = new FixedLengthStringData(5).init("T5682");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6598 = new FixedLengthStringData(5).init("T6598");
	private FixedLengthStringData t6648 = new FixedLengthStringData(5).init("T6648");
	private FixedLengthStringData t6651 = new FixedLengthStringData(5).init("T6651");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC   ");
	private FixedLengthStringData lifemjarec = new FixedLengthStringData(10).init("LIFEMJAREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC   ");
	private FixedLengthStringData puptrec = new FixedLengthStringData(10).init("PUPTREC   ");
	private FixedLengthStringData utrsrec = new FixedLengthStringData(10).init("UTRSREC   ");
	private FixedLengthStringData vprcrec = new FixedLengthStringData(10).init("VPRCREC   ");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
}
}
