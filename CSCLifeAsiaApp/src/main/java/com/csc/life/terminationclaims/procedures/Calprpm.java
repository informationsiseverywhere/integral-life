package com.csc.life.terminationclaims.procedures;


import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.recordstructures.Incrsrec;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.terminationclaims.recordstructures.Calprpmrec;
import com.csc.life.terminationclaims.tablestructures.Td5j2rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Calprpm extends COBOLConvCodeModel {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private static final String wsaaSubrname = "CALPRPM";
	private static final String t5687 = "T5687";
	private static final String t5675 = "T5675";
	private static final String t5679 = "T5679";
	private static final String tr52d = "TR52D";
	private static final String t6658 = "T6658";
	private static final String t5655 = "T5655";
	private static final String tr517 = "TR517";
	private static final String t5688 = "T5688";
	private static final String t5674 = "T5674";
	private Calprpmrec calprpmrec = new Calprpmrec();
	private Premiumrec premiumrec = new Premiumrec();
	private BinaryData wsaaJointAge = new BinaryData(5, 0);
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private T5679rec t5679rec = new T5679rec();
	private Tr517rec tr517rec = new Tr517rec();
	private Tr52drec tr52drec = new Tr52drec();
	private T5688rec t5688rec = new T5688rec();
	private T5687rec t5687rec = new T5687rec();
	private T5675rec t5675rec = new T5675rec();
	private T5655rec t5655rec = new T5655rec();
	private T6658rec t6658rec = new T6658rec();
	private T5674rec t5674rec = new T5674rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Syserrrec syserrrec = new Syserrrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private LextpfDAO lextpfDAO =  getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private ZonedDecimalData wsaaFirstIncrDate = new ZonedDecimalData(8, 0).init(99999999);
	private FixedLengthStringData wsaaT5675Item =new FixedLengthStringData(8);
	private PackedDecimalData wsaaTax = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);
	private Tr52erec tr52erec = new Tr52erec();
	private String tr52e = "TR52E";	
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Itempf itempf = null;
	private Chdrpf chdrpf = null;
	private FixedLengthStringData wsaaPremMeth= new FixedLengthStringData(4);
	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private FixedLengthStringData wsaaMainCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaMainCoverage = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaMainCessdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaMainPcessdte = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaMainMortclass = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaMainLife = new FixedLengthStringData(2);
	private BinaryData wsaaMainAge = new BinaryData(5, 0);
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaZbinstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaZlinstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaRateFrom = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaBillDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDurationInt = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDurationRem = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaRerateType = new FixedLengthStringData(1);
	private Validator trueRerate = new Validator(wsaaRerateType, "1");
	private Validator lextRerate = new Validator(wsaaRerateType, "2");
	private ZonedDecimalData wsaaLastRrtDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaCrtableMatch = new FixedLengthStringData(1).init("N");
	private Validator crtableMatch = new Validator(wsaaCrtableMatch, "Y");
	private PackedDecimalData wsaaEarliestRerateDate = new PackedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaIndex1 = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0);
	private ExternalisedRules er = new ExternalisedRules();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private BigDecimal wsaaCovrSumins = BigDecimal.ZERO;
	private boolean endFlag = false;
	private Payrpf payrpf = null;
	private Lifepf lifepf = null;
	private Annypf annypf = null;
	private Covrpf covrpf = null;
	private static final String td5j2 = "TD5J2";
	private Td5j2rec td5j2rec = new Td5j2rec();
	private HashMap<Long,List<Covrpf>> reratedPremKeeps  = new LinkedHashMap<>();
	private ZonedDecimalData wsaaj = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaau = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaasub = new ZonedDecimalData(11, 5).setUnsigned();
	private PackedDecimalData wsaaLastsum = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaLastinst = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZbinst = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZlinst = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOldCpiDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaareratesub = new ZonedDecimalData(11, 5).setUnsigned();
	private PackedDecimalData wsaaOriginst01 = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOriginst02 = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOriginst03= new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOrigsum = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaCovrCpiDate = new PackedDecimalData(8, 0);
	private Incrpf incrpf = null;
	private boolean changeExist = false;
	private static final String ta85 = "TA85";
	private static final String ta86 = "TA86";
	private boolean noMethod = false;
	private boolean isIncreaseAllowed = false;
	private Incrsrec incrsrec = new Incrsrec();
	private Covrpf covrpfobj = null;
	private boolean prmhldtrad = false;//ILIFE-8509
	private static final String tr7d = "TR7D";
	private static final String tr7b = "TR7B";
	private boolean stampDutyflag = false;
	private boolean dialdownFlag = false;
	private boolean premiumflag = false;
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Vpxlextrec vpxlextrec = new Vpxlextrec();
	private Vpxacblrec vpxacblrec=new Vpxacblrec();
	private AppVars appvars = null;
	private Incrpf incrIO;
	private ZonedDecimalData wsaaZstpduty01 = new ZonedDecimalData(17, 2);

	public Calprpm() {
		super();
	}

public void mainline(Object... parmArray)
	{
	//wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 2);
	incrIO = (Incrpf) convertAndSetParam(incrIO, parmArray, 3);
	appvars = (COBOLAppVars) convertAndSetParam(appVars, parmArray, 2);
	reratedPremKeeps = (HashMap<Long,List<Covrpf>>) convertAndSetParam(reratedPremKeeps, parmArray, 1);
	calprpmrec.calprpmRec = convertAndSetParam(calprpmrec.calprpmRec, parmArray, 0);
		try {
			main1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main1000()
	{
		setGlobalVars(appvars);//ILIFE-8509
		start1000();
		exit090();
	}

protected void start1000(){	
	calprpmrec.statuz.set("****");
	readCovr();
}
protected void readCovr(){
	chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(calprpmrec.chdrcoy.toString(), calprpmrec.chdrnum.toString());
	if (chdrpf == null ) {
		syserrrec.params.set(calprpmrec.chdrcoy.toString().concat(calprpmrec.chdrnum.toString()));
		fatalError600();
	}
	wsaaBillDate.set(ZERO);
	wsaaFirstIncrDate.set(varcom.vrcmMaxDate);
	calcLeadDays();
	getPayrpf();
	wsaaInstPrem.set(ZERO);
	wsaaZbinstprem.set(ZERO);
	wsaaZlinstprem.set(ZERO);
	wsaaMainCrtable.set(SPACES);
	changeExist = false;
	covrpf = covrpfDAO.getCovrByUniqueNum(calprpmrec.uniqueno.toLong());
	if(isNE(covrpf.getValidflag(),"1")){
		return;
	}
	readT5687(covrpf);
    readT6658();
	//ILIFE-8509
    prmhldtrad = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "CSOTH010", appVars, "IT");
    stampDutyflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
    if(!prmhldtrad)
		readTd5j2(covrpf.getCrtable());/* IJTI-1386 */
	calprpmrec.prem.set(covrpf.getInstprem());
	wsaaLastinst.set(covrpf.getInstprem());
	wsaaLastsum.set(covrpf.getSumins());
	wsaaZbinst.set(covrpf.getZbinstprem());
	wsaaZlinst.set(covrpf.getZlinstprem());
	if(isEQ(calprpmrec.transcode,ta86) || isEQ(calprpmrec.transcode,ta85) || (prmhldtrad && (isEQ(calprpmrec.transcode,tr7b) || isEQ(calprpmrec.transcode,tr7d))))//ILIFE-8509
		getIncr();
	if(!prmhldtrad) {
		if(isNE(td5j2rec.td5j2Rec,SPACES) && isEQ(td5j2rec.shortTerm,"Y")){
			calcShortPrem();
			if(isGTE(wsaaInstPrem, ZERO))
				calprpmrec.rerateprem.set(wsaaInstPrem);
		}
		else{
		   calprpmrec.prem.set(calprpmrec.increasePrem);
		}
	}
	else {
		if((covrpf.getReinstated()==null || covrpf.getReinstated().trim().isEmpty() || !("Y".equals(covrpf.getReinstated()))) && isNE(incrsrec.increaseRec, SPACES))
			updateIncr();
		calprpmrec.prem.set(calprpmrec.increasePrem);
		if(isGTE(wsaaInstPrem, ZERO))
			calprpmrec.rerateprem.set(wsaaInstPrem);
		if(isNE(calprpmrec.mode, "T"))
			calprpmrec.zstpduty01.set(incrIO.getZstpduty01() == null || incrIO.getZstpduty01().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : incrIO.getZstpduty01());
	}
}

protected void callDatcon5500()
{
	datcon3rec.function.set("CMDF");
	datcon3rec.frequency.set(chdrpf.getBillfreq());
    datcon3rec.intDate2.set(calprpmrec.nextPTDate);
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	else {
		wsaareratesub.set(datcon3rec.freqFactor);
	}
	
	/*EXIT*/
}

protected void calcLeadDays(){
	
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(calprpmrec.chdrcoy.toString());
	itempf.setItemtabl(t5655);/* IJTI-1386 */
	itempf.setItemitem(calprpmrec.transcode + chdrpf.getCnttype());/* IJTI-1386 */
	
	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf == null ) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(calprpmrec.chdrcoy.toString());
		itempf.setItemtabl(t5655);/* IJTI-1386 */
		itempf.setItemitem(calprpmrec.transcode + "***");
		
		itempf = itempfDAO.getItempfRecord(itempf);
		if(itempf == null )
			return;
	}
	t5655rec.t5655Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	
	datcon2rec.frequency.set("DY");
	datcon2rec.intDate1.set(calprpmrec.reinstDate);
	datcon2rec.freqFactor.set(t5655rec.leadDays);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz, varcom.oK)) {
		calprpmrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	else {
		wsaaBillDate.set(datcon2rec.intDate2);
	}
}

protected void readTd5j2(String crtable)
{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(calprpmrec.chdrcoy.toString());
		itempf.setItemtabl(td5j2);/* IJTI-1386 */
		itempf.setItemitem(crtable);
		
		itempf = itempfDAO.getItempfRecord(itempf);
		if (itempf == null ) {
			td5j2rec.td5j2Rec.set(SPACES);
			return;
		}
		td5j2rec.td5j2Rec.set(StringUtil.rawToString(itempf.getGenarea()));			
}

protected void getIncr(){
	
	wsaaOldCpiDate.set(calprpmrec.lastPTDate);
	if(prmhldtrad)//ILIFE-8509
		wsaaOldCpiDate.set(covrpf.getCpiDate());
	incrsrec.set(SPACES);
	if(isEQ(covrpf.getCpiDate(), ZERO) || (isEQ(covrpf.getCpiDate(),varcom.vrcmMaxDate))){
		isIncreaseAllowed = false;
	}
	
	if(isLT(covrpf.getCpiDate(),wsaaBillDate) && isNE(covrpf.getIndexationInd(),'P')){
		isIncreaseAllowed = true;
		wsaaCovrCpiDate.set(covrpf.getCpiDate());
	}
	while(isLT(wsaaOldCpiDate, calprpmrec.nextPTDate)){
		if(!noMethod && isIncreaseAllowed && isLTE(wsaaCovrCpiDate,wsaaOldCpiDate)){
			if(prmhldtrad) {//ILIFE-8509
				performRerate();
				continue;
			}
			calcIncrPrem();
	   }
		else{
			calprpmrec.increasePrem.add(wsaaLastinst);
		}
		if(!prmhldtrad)
			performRerate();
	}
}
protected void performRerate() {
	wsaaInstPrem.set(ZERO);
	wsaaZbinstprem.set(ZERO);
	wsaaZlinstprem.set(ZERO);
	wsaaMainCrtable.set(SPACES);
	if (isNE(covrpf.getRerateDate(),ZERO)){
		if (isNE(wsaaBillDate,ZERO) && isLTE(covrpf.getRerateDate(),wsaaBillDate) && isGTE(wsaaOldCpiDate, covrpf.getRerateDate())) {
			validateCovr(covrpf);
		}
		if(prmhldtrad && isNE(calprpmrec.mode, "T"))
			calcIncrPrem();
		if(isNE(wsaaInstPrem, ZERO)){
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, calprpmrec.prem));
			compute(wsaaZlinstprem, 2).set(sub(wsaaZlinstprem, calprpmrec.prem));
		}
		if(isGTE(wsaaInstPrem, ZERO))
			calprpmrec.rerateprem.add(wsaaInstPrem);
		if(isGTE(wsaaZlinstprem,ZERO))
			calprpmrec.zlinstprem.add(wsaaZlinstprem);
	}
	covrpfobj = new Covrpf(covrpf);
	covrpfobj.setInstprem(wsaaLastinst.getbigdata());
	covrpfobj.setZbinstprem(wsaaZbinst.getbigdata());
	covrpfobj.setSumins(wsaaLastsum.getbigdata());
	covrpfobj.setCrrcd(wsaaOldCpiDate.toInt());
	if(isGTE(wsaaInstPrem, ZERO))
		covrpfobj.setInstprem(add(covrpfobj.getInstprem(), wsaaInstPrem).getbigdata());
	if (reratedPremKeeps.containsKey(covrpfobj.getUniqueNumber())) {
		reratedPremKeeps.get(covrpfobj.getUniqueNumber()).add(covrpfobj); 
    } else {
        List<Covrpf> covrList = new ArrayList<Covrpf>();
        covrList.add(covrpfobj);
        reratedPremKeeps.put(covrpfobj.getUniqueNumber(), covrList);
    } 
	wsaaOldCpiDate.set(getnextDate(chdrpf.getBillfreq(),wsaaOldCpiDate.toInt()));/* IJTI-1386 */
}
protected int getnextDate(String factor, int date){
	
	datcon2rec.datcon2Rec.set(SPACES);
	datcon2rec.intDate1.set(date); 
	datcon2rec.frequency.set("01");
	datcon2rec.freqFactor.set(factor);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz,varcom.oK)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		syserrrec.params.set(datcon2rec.datcon2Rec);
		fatalError600();
	}
	return datcon2rec.intDate2.toInt();
}

protected void calcIncrPrem(){
	
	incrsrec.origsum.set(ZERO);
	incrsrec.lastsum.set(ZERO);
	incrsrec.newsum.set(ZERO);
	incrsrec.originst01.set(ZERO);
	incrsrec.lastinst01.set(ZERO);
	incrsrec.newinst01.set(ZERO);
	incrsrec.currinst01.set(ZERO);
	incrsrec.originst02.set(ZERO);
	incrsrec.lastinst02.set(ZERO);
	incrsrec.newinst02.set(ZERO);
	incrsrec.currinst02.set(ZERO);
	incrsrec.originst03.set(ZERO);
	incrsrec.lastinst03.set(ZERO);
	incrsrec.newinst03.set(ZERO);
	incrsrec.currinst03.set(ZERO);
	incrsrec.pctinc.set(ZERO);
    incrsrec.currinst01.set(wsaaLastinst);
    incrsrec.currinst02.set(wsaaZbinst);
    incrsrec.currinst03.set(wsaaZlinst);
    incrsrec.currsum.set(wsaaLastsum);
	incrsrec.originst01.set(wsaaOriginst01);
    incrsrec.originst02.set(wsaaOriginst02);
    incrsrec.originst03.set(wsaaOriginst03);
    incrsrec.origsum.set(wsaaOrigsum);
    
    incrsrec.lastinst01.set(wsaaLastinst);
    incrsrec.lastinst02.set(wsaaZbinst);
    incrsrec.lastinst03.set(wsaaZlinst);
    incrsrec.lastsum.set(wsaaLastsum);
    read3201();
	incrsrec.function.set(SPACES);
	incrsrec.statuz.set(SPACES);
    incrsrec.chdrcoy.set(covrpf.getChdrcoy());
    incrsrec.chdrnum.set(covrpf.getChdrnum());
    incrsrec.coverage.set(covrpf.getCoverage());
    incrsrec.life.set(covrpf.getLife());
    incrsrec.rider.set(covrpf.getRider());
    incrsrec.plnsfx.set(covrpf.getPlanSuffix());
    incrsrec.effdate.set(wsaaOldCpiDate);
    incrsrec.crtable.set(covrpf.getCrtable());
    incrsrec.rcesdte.set(covrpf.getRiskCessDate());
    incrsrec.mortcls.set(covrpf.getMortcls());
	incrsrec.billfreq.set(payrpf.getBillfreq());
	incrsrec.mop.set(payrpf.getBillchnl());
	incrsrec.occdate.set(chdrpf.getOccdate());
    incrsrec.cntcurr.set(covrpf.getPremCurrency());
	incrsrec.language.set(calprpmrec.language);
	if (isEQ(t6658rec.maxpcnt, ZERO)) {
		incrsrec.maxpcnt.set(+99.999);
	}
	else {
		incrsrec.maxpcnt.set(t6658rec.maxpcnt);
	}
	incrsrec.minpcnt.set(t6658rec.minpcnt);
	incrsrec.fixdtrm.set(t6658rec.fixdtrm);
	incrsrec.annvmeth.set(t5687rec.anniversaryMethod);
	if(stampDutyflag) {//ILIFE-8509
		incrsrec.zstpduty01.set(ZERO);
		incrsrec.zstpduty02.set(ZERO);
		incrsrec.zstpduty03.set(ZERO);
	}
	if(!prmhldtrad) {
		incrsrec.autoincreaseindicator.set("P");
		callProgramX(t6658rec.premsubr, incrsrec.increaseRec); //ILIFE-8302
	}
	else {
		incrsrec.simpleInd.set(t6658rec.simpleInd);
		incrsrec.autoincreaseindicator.set("C");
		incrsrec.compoundInd.set(t6658rec.compoundInd);
		incrsrec.calcprem.set(wsaaInstPrem);
		incrsrec.inputPrevPrem.set(ZERO);
		incrsrec.currinst01.set(wsaaInstPrem);
	    incrsrec.currinst02.set(wsaaZbinstprem);
	    incrsrec.currinst03.set(wsaaZlinstprem);
	    //incrsrec.currsum.set(wsaaLastsum); //check
		incrsrec.originst01.set(wsaaInstPrem);
	    incrsrec.originst02.set(wsaaZbinstprem);
	    incrsrec.originst03.set(wsaaZlinstprem);
	    //incrsrec.origsum.set(wsaaOrigsum); //check
	    
	    incrsrec.lastinst01.set(wsaaInstPrem);
	    incrsrec.lastinst02.set(wsaaZbinstprem);
	    incrsrec.lastinst03.set(wsaaZlinstprem);
	    //incrsrec.lastsum.set(wsaaLastsum); //check
		initPremiumrec(covrpf);
		premiumrec.function.set("INCR");
		callProgram(t6658rec.premsubr, incrsrec.increaseRec,premiumrec,vpxlextrec, vpxacblrec);
		if (isNE(incrsrec.newsum,ZERO)) {
			callProgram(t6658rec.premsubr,incrsrec.increaseRec,premiumrec,vpxlextrec, vpxacblrec);
		}
	}

	premiumrec.riskPrem.set(BigDecimal.ZERO);
	
	if (isEQ(incrsrec.statuz,varcom.bomb)) {
		syserrrec.statuz.set(incrsrec.statuz);
		syserrrec.params.set(incrsrec.increaseRec);
		fatalError600();
	}
	if (isEQ(incrsrec.newsum,ZERO)) {
		incrsrec.newsum.set(incrsrec.currsum);
	}
	if (isEQ(incrsrec.newinst01,ZERO)) {
		incrsrec.newinst01.set(incrsrec.currinst01);
	}
	if (isEQ(incrsrec.newinst02,ZERO)) {
		incrsrec.newinst02.set(incrsrec.currinst02);
	}
	if (isEQ(incrsrec.newinst03,ZERO)) {
		incrsrec.newinst03.set(incrsrec.currinst03);
	}
	if(stampDutyflag) {//ILIFE-8509
		if(isNE(incrsrec.newinst01, ZERO) && isNE(incrsrec.zstpduty01, ZERO)) {
			incrsrec.newinst01.set(add(incrsrec.newinst01, incrsrec.zstpduty01));
		}
		if(isNE(incrsrec.newinst02, ZERO) && isNE(incrsrec.zstpduty02, ZERO)) {
			incrsrec.newinst02.set(add(incrsrec.newinst02, incrsrec.zstpduty02));
		}
		if(isNE(incrsrec.newinst03, ZERO) && isNE(incrsrec.zstpduty03, ZERO)) {
			incrsrec.newinst03.set(add(incrsrec.newinst03, incrsrec.zstpduty03));
		}
	}
	
	if(isNE(incrsrec.newinst01,ZERO)){
		calprpmrec.increasePrem.add(incrsrec.newinst01);
		calprpmrec.prem.set(incrsrec.newinst01);
		wsaaLastinst.set(incrsrec.newinst01);
		wsaaLastsum.set(incrsrec.newsum);
		wsaaZbinst.set(incrsrec.newinst02);
		wsaaZlinst.set(incrsrec.newinst03);
		calprpmrec.zbinstprem.set(wsaaZbinst);
		calprpmrec.zlinstprem.set(wsaaZlinst);
		calprpmrec.increaseSum.set(incrsrec.newsum);
		changeExist = true;	
	}
	wsaaCovrCpiDate.set(getnextDate(t6658rec.billfreq.toString(),wsaaCovrCpiDate.toInt()));
}
protected void calc6005()
{
	premiumrec.premiumRec.set(SPACES);
	premiumrec.function.set("INCR");
	premiumrec.chdrChdrcoy.set(incrsrec.chdrcoy);
	premiumrec.chdrChdrnum.set(incrsrec.chdrnum);
	premiumrec.lifeLife.set(incrsrec.life);
	premiumrec.lifeJlife.set("00");
	premiumrec.covrCoverage.set(incrsrec.coverage);
	premiumrec.covrRider.set(incrsrec.rider);
	premiumrec.crtable.set(incrsrec.crtable);
	premiumrec.effectdt.set(incrsrec.effdate);
	premiumrec.termdate.set(incrsrec.rcesdte);
	premiumrec.language.set(incrsrec.language);
	wsaaLastRrtDate.set(incrsrec.effdate);
	getLifeDetails7000();
	checkJointLife3270();
	readT5675();
	datcon3rec.intDate1.set(incrsrec.effdate);
	datcon3rec.intDate2.set(premiumrec.termdate);
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz,varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	datcon3rec.freqFactor.add(0.99999);
	premiumrec.duration.set(datcon3rec.freqFactor);
	premiumrec.currcode.set(incrsrec.cntcurr);
	premiumrec.sumin.set(incrsrec.origsum);
	premiumrec.mortcls.set(incrsrec.mortcls);
	premiumrec.billfreq.set(incrsrec.billfreq);
	premiumrec.mop.set(incrsrec.mop);
	premiumrec.ratingdate.set(incrsrec.occdate);
	premiumrec.reRateDate.set(incrsrec.occdate);
	premiumrec.calcPrem.set(incrsrec.currinst01);
	premiumrec.calcBasPrem.set(ZERO);
	premiumrec.calcLoaPrem.set(ZERO);
	if (isEQ(t5675rec.premsubr,SPACES)) {
		incrsrec.newinst01.set(incrsrec.currinst01);
	}
	getAnny11000();
}


protected void read3201()
{
	if(changeExist){
		return;
	}
	incrpf = incrpfDAO.getIncrData(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlanSuffix());/* IJTI-1386 */        
    if(incrpf == null){
    	incrsrec.originst01.set(covrpf.getInstprem());
        incrsrec.originst02.set(covrpf.getZbinstprem());
        incrsrec.originst03.set(covrpf.getZlinstprem());
        incrsrec.origsum.set(covrpf.getSumins());
        wsaaOriginst01.set(covrpf.getInstprem());
        wsaaOriginst02.set(covrpf.getZbinstprem());
        wsaaOriginst03.set(covrpf.getZlinstprem());
        wsaaOrigsum.set(covrpf.getSumins());
	   return;
    }
	if (isEQ(incrpf.getRefusalFlag(), SPACES)) {
		incrsrec.lastinst01.set(incrpf.getNewinst());
		incrsrec.currinst01.set(incrpf.getNewinst());
		incrsrec.lastinst02.set(incrpf.getZbnewinst());
		incrsrec.currinst02.set(incrpf.getZbnewinst());
		incrsrec.lastinst03.set(incrpf.getZlnewinst());
		incrsrec.currinst03.set(incrpf.getZlnewinst());
		incrsrec.lastsum.set(incrpf.getNewsum());
		incrsrec.currsum.set(incrpf.getNewsum());
		
    if (isEQ(covrpf.getInstprem(), incrsrec.originst01)
               && isEQ(covrpf.getSumins(), incrsrec.origsum)) {
        incrsrec.originst01.set(incrpf.getOrigInst());
        incrsrec.originst02.set(incrpf.getZboriginst());
        incrsrec.originst03.set(incrpf.getZboriginst());
        incrsrec.origsum.set(incrpf.getOrigSum());
        wsaaOriginst01.set(incrpf.getOrigInst());
        wsaaOriginst02.set(incrpf.getZboriginst());
        wsaaOriginst03.set(incrpf.getZboriginst());
        wsaaOrigsum.set(incrpf.getOrigSum());
      }
    }
}

protected void getLifeDetails7000(){
	lifepf = lifepfDAO.getLifeEnqRecord(calprpmrec.chdrcoy.toString(),calprpmrec.chdrnum.toString(), covrpf.getLife(), "00");/* IJTI-1386 */
	if(lifepf == null){
		syserrrec.statuz.set(calprpmrec.chdrcoy.toString().concat(calprpmrec.chdrnum.toString()).concat(covrpf.getLife()));/* IJTI-1386 */
		fatalError600();
	}
	calculateAnb3260(lifepf);
	premiumrec.lage.set(wsaaAnb);
	premiumrec.lsex.set(lifepf.getCltsex());
}

protected void calcShortPrem(){
	
	callDatcon4500();
	compute(wsaasub, 2).set(div(wsaaj,wsaau));
	if(isGT(wsaasub,1)){
		wsaasub.set(1);
	}
	compute(calprpmrec.prem, 2).set(mult(calprpmrec.prem, wsaasub));
}

protected void callDatcon4500()
{
	/*GO*/
	while(isGT(calprpmrec.reinstDate,calprpmrec.lastPTDate)){
		datcon2rec.datcon2Rec.set(SPACES);
		calprpmrec.ptdlapse.set(calprpmrec.lastPTDate);
		
		datcon2rec.frequency.set(chdrpf.getBillfreq());
		datcon2rec.intDate1.set(calprpmrec.lastPTDate);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		else {
			calprpmrec.lastPTDate.set(datcon2rec.intDate2);
		}
	}
	
	datcon3rec.frequency.set("DY");
	datcon3rec.intDate1.set(calprpmrec.reinstDate);
	datcon3rec.intDate2.set(calprpmrec.nextPTDate);
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	else {
		wsaaj.set(datcon3rec.freqFactor);
	}
	datcon3rec.frequency.set("DY");
	datcon3rec.intDate1.set(calprpmrec.ptdlapse); /*PTDate just before reinstatement date*/
	datcon3rec.intDate2.set(calprpmrec.nextPTDate);
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	else {
		wsaau.set(datcon3rec.freqFactor);
	}

	
	/*EXIT*/
}

protected void validateCovr(Covrpf covrpf){
	/* Validate the new coverage status against T5679.*/
	wsaaValidStatus.set("N");
	
	readT5679();
	readTr517();
	if (isNE(tr517rec.tr517Rec,SPACES)) {
		if(isEQ(calprpmrec.mode,"T")){
			startWoplPrem(covrpf);
			return;
		}
		else{
			calprpmrec.mode.set("Y");
			exit090();
		}
	}
	validate3000(covrpf);	
	if (isEQ(wsaaValidStatus,"Y")) {
		calcRerate(covrpf);
	}
 }

protected void validate3000(Covrpf covrpf){
	wsaaValidStatus.set("N");
	  if ((isNE(covrpf.getCoverage(),SPACES)) && ((isEQ(covrpf.getRider(),SPACES)) || (isEQ(covrpf.getRider(),"00")))) {
 		  
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12) || isEQ(wsaaValidStatus,"Y")) ; wsaaIndex.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],covrpf.getStatcode())) {
				for (wsaaIndex1.set(1); !(isGT(wsaaIndex1,12)) ; wsaaIndex1.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaIndex1.toInt()],covrpf.getPstatcode())) {
							wsaaIndex1.set(13);
							wsaaValidStatus.set("Y");
					}		
			    }
		     }
	    }
	  }
	  if ((isNE(covrpf.getCoverage(),SPACES)) && ((isNE(covrpf.getRider(),SPACES)) && (isNE(covrpf.getRider(),"00")))) {
		  
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12) || isEQ(wsaaValidStatus,"Y")) ; wsaaIndex.add(1)){
			if (isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()],covrpf.getStatcode())){
				for (wsaaIndex1.set(1); !(isGT(wsaaIndex1,12)) ; wsaaIndex1.add(1)){
					if (isEQ(t5679rec.ridPremStat[wsaaIndex1.toInt()],covrpf.getPstatcode())) {
							wsaaIndex1.set(13);
							wsaaValidStatus.set("Y");
					}		
			    }
		    }
		}
	}
}



protected void readT5679()
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(calprpmrec.chdrcoy.toString());
	itempf.setItemtabl(t5679);/* IJTI-1386 */
	itempf.setItemitem(calprpmrec.transcode.toString());
	
	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf == null ) {
		return;
	}
	t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	
}
protected void readTr517()
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(calprpmrec.chdrcoy.toString());
	itempf.setItemtabl(tr517);/* IJTI-1386 */
	itempf.setItemitem(covrpf.getCrtable());/* IJTI-1386 */
	itempf.setItmfrm(new BigDecimal(covrpf.getCrrcd()));
	itempf.setItmto(new BigDecimal(covrpf.getCrrcd()));
	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf == null ) {
		tr517rec.tr517Rec.set(SPACES);
	}
	else{
	tr517rec.tr517Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
}


protected void calcRerate(Covrpf covrpf){

	start300(covrpf);
	getPrem3300(covrpf);
}

protected void readT5687(Covrpf covrpf){
	
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(calprpmrec.chdrcoy.toString());
	itempf.setItemtabl(t5687);/* IJTI-1386 */
	itempf.setItemitem(covrpf.getCrtable());/* IJTI-1386 */

	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf == null ) {
		syserrrec.params.set(itempf.getItemtabl() + itempf.getItemitem()); 
		fatalError600();
	}
	t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
}

protected void readT5675(){
	
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(calprpmrec.chdrcoy.toString());
	itempf.setItemtabl(t5675.trim());/* IJTI-1386 */
	itempf.setItemitem(wsaaT5675Item.toString().trim());
	
	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf == null ) {
		syserrrec.params.set(itempf.getItemtabl() + itempf.getItemitem()); 
		fatalError600();
	}
	t5675rec.t5675Rec.set(StringUtil.rawToString(itempf.getGenarea()));
}

protected void readT6658(){
	
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(calprpmrec.chdrcoy.toString());
	itempf.setItemtabl(t6658);/* IJTI-1386 */
	itempf.setItemitem(t5687rec.anniversaryMethod.toString());

	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf == null ) {
		t6658rec.t6658Rec.set(SPACES);
		noMethod = true;
		return;
	}
	t6658rec.t6658Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	noMethod = false;
}

protected void start300(Covrpf covrpf){
	/*  Default to a TRUE rerate.*/
	wsaaRerateType.set("1");
	if (isEQ(covrpf.getPremCessDate(),covrpf.getRerateDate())) {
	/*	wsaaFullyPaid = "Y";*/
		return;
	}
	readT5687(covrpf);
	if (isEQ(t5687rec.premmeth,SPACES)) {
		return;
	}
	else{
		if(prmhldtrad) {//ILIFE-8509
			initPremiumrec(covrpf);
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))) 
				callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			else {
				premiumrec.setPmexCall.set("Y");
				premiumrec.inputPrevPrem.set(ZERO);//ILIFE-8537
				premiumrec.updateRequired.set("N");
				if (isEQ(covrpf.getPlanSuffix(),0) && isNE(chdrpf.getPolinc(),1)) {
					if(isEQ(calprpmrec.mode,"T"))
						compute(premiumrec.sumin, 3).setRounded(div(wsaaCovrSumins,chdrpf.getPolsum()));
					else
					compute(premiumrec.sumin, 3).setRounded(div(wsaaLastsum,chdrpf.getPolsum()));
				}
				else {
					if(isEQ(calprpmrec.mode,"T"))
						premiumrec.sumin.set(wsaaCovrSumins);
					else
					premiumrec.sumin.set(wsaaLastsum);
				}
				compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin,chdrpf.getPolinc()));
				if(("PMEX").equals(t5675rec.premsubr.toString().trim()))
					callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec, reratedPremKeeps);
			}
			if(stampDutyflag) {
				premiumrec.calcPrem.add(premiumrec.zstpduty01);
				wsaaZstpduty01.set(premiumrec.zstpduty01);
			}
			return;
		}
		setRerateType3410(covrpf);
		premiumrec.premiumRec.set(SPACES);
		/*premiumrec.lsex.set(lifepf.getCltsex());*/
		getLifeDetails7000();
		checkJointLife3270();
		/* Read T5675 for premium calculation subroutine call.*/
		readT5675();
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(wsaaT5675Item);
		}
		premiumrec.crtable.set(covrpf.getCrtable());
		premiumrec.chdrChdrcoy.set(covrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrpf.getChdrnum());
		premiumrec.lifeLife.set(covrpf.getLife());
		premiumrec.lifeJlife.set(covrpf.getJlife());
		premiumrec.covrRider.set(covrpf.getRider());
		premiumrec.covrCoverage.set(covrpf.getCoverage());
		premiumrec.mop.set(payrpf.getBillchnl());
		premiumrec.billfreq.set(payrpf.getBillfreq());
		premiumrec.effectdt.set(wsaaLastRrtDate);
		premiumrec.mortcls.set(covrpf.getMortcls());
		premiumrec.currcode.set(covrpf.getPremCurrency());
		premiumrec.termdate.set(covrpf.getPremCessDate());
		premiumrec.ratingdate.set(covrpf.getRerateFromDate());
		premiumrec.reRateDate.set(covrpf.getRerateDate());
		premiumrec.calcPrem.set(wsaaLastinst); 
		premiumrec.calcBasPrem.set(wsaaZbinst);
		premiumrec.calcLoaPrem.set(wsaaZlinst);
		
		datcon3rec.intDate1.set(premiumrec.effectdt.toString());
		datcon3rec.intDate2.set(premiumrec.termdate.toString());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			calprpmrec.statuz.set(datcon3rec.statuz);
			return ;
		}
		compute(premiumrec.duration, 5).set(add(datcon3rec.freqFactor,.99999));
		if (isEQ(covrpf.getPlanSuffix(),0) && isNE(chdrpf.getPolinc(),1)) {
			if(isEQ(calprpmrec.mode,"T")){
				compute(premiumrec.sumin, 3).setRounded(div(wsaaCovrSumins,chdrpf.getPolsum()));
			}
			else{
			compute(premiumrec.sumin, 3).setRounded(div(wsaaLastsum,chdrpf.getPolsum()));
			}
		}
		else {
			if(isEQ(calprpmrec.mode,"T")){
				premiumrec.sumin.set(wsaaCovrSumins);
			}
			else{
			premiumrec.sumin.set(wsaaLastsum);
			}
		}
		
		compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin,chdrpf.getPolinc()));
		premiumrec.function.set("CALC");
		/* Get any Annuity values required for the calculation.  If the*/
		/* coverage is not an annuity, initialise these values.*/
		getAnny11000();
		premiumrec.language.set(calprpmrec.language);
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{			
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			premiumrec.cnttype.set(chdrpf.getCnttype());	
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		
		if (isNE(premiumrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(premiumrec.statuz);
			syserrrec.params.set(premiumrec.premiumRec);
			fatalError600();
		}
	}
}

private void getPayrpf() {
	payrpf = payrpfDAO.getPayrRecordByCoyAndNumAndSeq(calprpmrec.chdrcoy.toString(), calprpmrec.chdrnum.toString());

	if (payrpf==null) {
		syserrrec.params.set(calprpmrec.chdrcoy.toString().concat(calprpmrec.chdrnum.toString()));
		fatalError600();
	}
}


protected void checkJointLife3270() {
	
	Lifepf lifelnb = lifepfDAO.getLifelnbRecord(calprpmrec.chdrcoy.toString(),calprpmrec.chdrnum.toString(), covrpf.getLife(),"01");/* IJTI-1386 */
	if (lifelnb != null) {
		wsaaT5675Item.set(t5687rec.jlPremMeth);
		premiumrec.jlsex.set(lifelnb.getCltsex());
		calculateAnb3260(lifelnb);
		premiumrec.jlage.set(wsaaAnb);
		premiumrec.premMethod.set(t5687rec.jlPremMeth);
	} else {
		wsaaT5675Item.set(t5687rec.premmeth);
		premiumrec.jlsex.set(SPACES);
		premiumrec.jlage.set(ZERO);
		premiumrec.premMethod.set(t5687rec.premmeth);
	}
}

protected void calculateAnb3260(Lifepf lifelnb){
	
	wsaaAnb.set(ZERO);
	agecalcrec.function.set("CALCP");
	agecalcrec.language.set(calprpmrec.language);
	agecalcrec.cnttype.set(chdrpf.getCnttype());
	agecalcrec.intDate1.set(lifelnb.getCltdob());
	agecalcrec.intDate2.set(wsaaLastRrtDate.toString()); 
	agecalcrec.company.set("9");
	callProgram(Agecalc.class, agecalcrec.agecalcRec);
	if (isNE(agecalcrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(agecalcrec.statuz);
		fatalError600();
	}
	wsaaAnb.set(agecalcrec.agerating);
}

protected void setRerateType3410(Covrpf covrpf){
	wsaaDurationInt.set(ZERO);
	if (isNE(t5687rec.rtrnwfreq,NUMERIC)
	|| isEQ(t5687rec.rtrnwfreq,0)) {
		wsaaRerateType.set("2");
	}
	else {
		datcon3rec.intDate1.set(covrpf.getCrrcd());
		datcon3rec.intDate2.set(covrpf.getRerateDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			calprpmrec.statuz.set(datcon3rec.statuz);
			return ;
		}
		
		compute(wsaaDurationInt, 5).setDivide(datcon3rec.freqFactor, (t5687rec.rtrnwfreq));
		wsaaDurationRem.setRemainder(wsaaDurationInt);
		if (isNE(wsaaDurationRem,0)) {
			checkLextRerate3420();
		}
	}
	
	if (lextRerate.isTrue()) {
		datcon2rec.intDate1.set(covrpf.getCrrcd());
		compute(datcon2rec.freqFactor, 0).set((mult(t5687rec.rtrnwfreq, wsaaDurationInt)));
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			calprpmrec.statuz.set(datcon2rec.statuz);
			return ;
		}
		wsaaLastRrtDate.set(datcon2rec.intDate2);
	}
	else {
		wsaaLastRrtDate.set(covrpf.getRerateDate());
	}
}

protected void checkLextRerate3420() {
	
	Lextpf lextbrr = new Lextpf();
	lextbrr.setChdrcoy(calprpmrec.chdrcoy.toString());
	lextbrr.setChdrnum(calprpmrec.chdrnum.toString());
	lextbrr.setLife(covrpf.getLife());/* IJTI-1386 */
	lextbrr.setCoverage(covrpf.getCoverage());/* IJTI-1386 */
	lextbrr.setRider(covrpf.getRider());/* IJTI-1386 */
	lextbrr.setExtCessDate(covrpf.getRerateDate());
	lextbrr = lextpfDAO.getLextbrrRecord(lextbrr);
	if (lextbrr != null) {
		wsaaRerateType.set("2");
	}
}

protected void getAnny11000(){
	
    annypf = annypfDAO.getAnnyRecordByAnnyKey(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(), covrpf.getCoverage(), covrpf.getRider(), covrpf.getPlanSuffix());

	if (annypf != null) {
		premiumrec.freqann.set(annypf.getFreqann());
		premiumrec.advance.set(annypf.getAdvance());
		premiumrec.arrears.set(annypf.getArrears());
		premiumrec.guarperd.set(annypf.getGuarperd());
		premiumrec.intanny.set(annypf.getIntanny());
		premiumrec.capcont.set(annypf.getCapcont());
		premiumrec.withprop.set(annypf.getWithprop());
		premiumrec.withoprop.set(annypf.getWithoprop());
		premiumrec.ppind.set(annypf.getPpind());
		premiumrec.nomlife.set(annypf.getNomlife());
		premiumrec.dthpercn.set(annypf.getDthpercn());
		premiumrec.dthperco.set(annypf.getDthperco());
	}
	else {
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
	}
}

protected void c000CallRounding(){
	/*C100-CALL*/
	zrdecplrec.function.set(SPACES);
	zrdecplrec.company.set(calprpmrec.chdrcoy.toString());
	zrdecplrec.statuz.set(varcom.oK);
	zrdecplrec.currency.set(chdrpf.getBillcurr());
	zrdecplrec.batctrcde.set(calprpmrec.transcode);
	callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
	if (isNE(zrdecplrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(zrdecplrec.statuz);
		syserrrec.params.set(zrdecplrec.zrdecplRec);
		fatalError600();
	}
	/*C900-EXIT*/
}

protected void getPrem3300(Covrpf covrpf){
	
					if (isNE(t5687rec.premmeth,SPACES)) {
						compute(wsaaInstPrem, 3).setRounded(div(premiumrec.calcPrem,chdrpf.getPolinc()));
						compute(wsaaZbinstprem, 3).setRounded(div(premiumrec.calcBasPrem,chdrpf.getPolinc()));
						compute(wsaaZlinstprem, 3).setRounded(div(premiumrec.calcLoaPrem,chdrpf.getPolinc()));
						if (isEQ(covrpf.getPlanSuffix(),0)
						&& isNE(chdrpf.getPolinc(),1)) {
							compute(wsaaInstPrem, 3).setRounded(mult(wsaaInstPrem,chdrpf.getPolsum()));
							compute(wsaaZbinstprem, 3).setRounded(mult(wsaaZbinstprem,chdrpf.getPolsum()));
							compute(wsaaZlinstprem, 3).setRounded(mult(wsaaZlinstprem,chdrpf.getPolsum()));
						}
						if (isNE(wsaaInstPrem, 0)) {
							zrdecplrec.amountIn.set(wsaaInstPrem.getbigdata());
							c000CallRounding();
							wsaaInstPrem.set(zrdecplrec.amountOut);
						}
						if (isNE(wsaaZbinstprem, 0)) {
							zrdecplrec.amountIn.set(wsaaZbinstprem.getbigdata());
							c000CallRounding();
							wsaaZbinstprem.set(zrdecplrec.amountOut);
						}
						if (isNE(wsaaZlinstprem, 0)) {
							zrdecplrec.amountIn.set(wsaaZlinstprem.getbigdata());
							c000CallRounding();
							wsaaZlinstprem.set(zrdecplrec.amountOut);
						}
						if (prmhldtrad && isNE(wsaaZstpduty01, 0)) {
							zrdecplrec.amountIn.set(wsaaZstpduty01.getbigdata());
							c000CallRounding();
							wsaaZstpduty01.set(zrdecplrec.amountOut);
						}
				
				     }
		}		


protected void exit090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
	/*FATAL*/
	syserrrec.subrname.set(wsaaSubrname);
	if (isNE(syserrrec.statuz, SPACES)
	|| isNE(syserrrec.syserrStatuz, SPACES)) {
		syserrrec.syserrType.set("1");
	}
	else {
		syserrrec.syserrType.set("2");
	}
	callProgram(Syserr.class, syserrrec.syserrRec);
	/*EXIT-PROGRAM*/
	exitProgram();
	}
protected void startWoplPrem(Covrpf covrpf){
	readTr52d();
	processWop3a10(covrpf);						
	calcRerate(covrpf);
	if(prmhldtrad) {
		calprpmrec.increaseSum.set(premiumrec.sumin);
		calprpmrec.zstpduty01.set(wsaaZstpduty01);
	}
}
protected void readTr52d(){
	
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(calprpmrec.chdrcoy.toString());
		itempf.setItemtabl(tr52d);/* IJTI-1386 */
		itempf.setItemitem(chdrpf.getReg());/* IJTI-1386 */
		
		itempf = itempfDAO.getItempfRecord(itempf);
		if (itempf == null ) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(calprpmrec.chdrcoy.toString());
			itempf.setItemtabl(tr52d);/* IJTI-1386 */
			itempf.setItemitem("***");
			itempf = itempfDAO.getItempfRecord(itempf);
			if (itempf == null ) {
				syserrrec.params.set(itempf.getItemtabl() + itempf.getItemitem()); 
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
}
protected void processWop3a10(Covrpf covrpf){
	if (isGT(covrpf.getCpiDate(),covrpf.getRerateDate())) {
		wsaaEarliestRerateDate.set(covrpf.getRerateDate());
	}
	else {
		wsaaEarliestRerateDate.set(covrpf.getCpiDate());
	}
	wsaaCovrSumins = BigDecimal.ZERO;
	List<Covrpf> covrList = covrpfDAO.getCovrByComAndNum(calprpmrec.chdrcoy.toString(), calprpmrec.chdrnum.toString());
	/* Determine if the WOP is life insured specific.*/
	if (isNE(tr517rec.zrwvflg02, "Y")) {
		for (Covrpf c : covrList) {
			if (isNE(tr517rec.zrwvflg02, "Y") && isNE(c.getLife(), covrpf.getLife())) {
				break;
			}
			wsaaCrtableMatch.set("N");
			check3b22(c);
			if (endFlag) {
				break;
			}
		}
		endFlag = false;
		for (Covrpf c : covrList) {
			if (isNE(tr517rec.zrwvflg02, "Y") && isNE(c.getLife(), covrpf.getLife())) {
				break;
			}
			wsaaValidStatus.set(SPACES);
			validate3000(c);
			if (isNE(wsaaValidStatus, "Y")) {
				continue;
			}
			wsaaCrtableMatch.set("N");
			check3a22(c, covrpf.getLife());
			
		}
	}
	else {
		for (Covrpf c : covrList) {
			if (isNE(tr517rec.zrwvflg02, "Y") && isNE(c.getLife(), covrpf.getLife())) {
				break;
			}
			wsaaCrtableMatch.set("N");
			check3b22(c);
			if (endFlag) {
				break;
			}

		}
		endFlag = false;
		for (Covrpf c : covrList) {
			if (isNE(tr517rec.zrwvflg02, "Y") && isNE(c.getLife(), covrpf.getLife())) {
				break;
			}
			wsaaValidStatus.set(SPACES);
			validate3000(c);
			if (isNE(wsaaValidStatus, "Y")) {
				continue;
			}
			wsaaCrtableMatch.set("N");
			check3a22(c, covrpf.getLife());
			}
		} 
	if (isEQ(tr517rec.zrwvflg03,"Y")) {
		calcFee3b10(covrpf.getCoverage());
	}
}
protected void calcFee3b10(String coverage)
{
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(chdrpf.getChdrcoy().toString());
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItmto(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItemtabl(t5688);
	itempf.setItemitem(chdrpf.getCnttype());/* IJTI-1386 */
	itempf.setValidflag("1");
	List<Itempf> itempfList = itemDAO.findByItemDates(itempf);		 

	if ((itempfList == null || itempfList.size() == 0 )){
		syserrrec.params.set(itempf.getItemtabl() + itempf.getItemitem()); 
		fatalError600();
	}
	else{
		t5688rec.t5688Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}
	if (isEQ(t5688rec.feemeth,SPACES)) {
		return ;
	}
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(calprpmrec.chdrcoy.toString());
	itempf.setItemtabl(t5674);/* IJTI-1386 */
	itempf.setItemitem(t5688rec.feemeth.toString());

	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf == null ) {
		t5674rec.commsubr.set(SPACES);
	}
	t5674rec.t5674Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	if (isEQ(t5674rec.commsubr,SPACES)) {
		return ;
	}
	/* Having got this far, management fees are applicable.*/
	mgfeelrec.mgfeelRec.set(SPACES);
	mgfeelrec.effdate.set(ZERO);
	mgfeelrec.mgfee.set(ZERO);
	mgfeelrec.cnttype.set(chdrpf.getCnttype());
	mgfeelrec.billfreq.set(chdrpf.getBillfreq());
	mgfeelrec.effdate.set(chdrpf.getOccdate());
	mgfeelrec.cntcurr.set(chdrpf.getCntcurr());
	mgfeelrec.company.set(chdrpf.getChdrcoy().toString());
	callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
	if (isNE(mgfeelrec.statuz,varcom.oK)
	&& isNE(mgfeelrec.statuz,varcom.endp)) {
		syserrrec.statuz.set(mgfeelrec.statuz);
		syserrrec.params.set(mgfeelrec.mgfeelRec);
		fatalError600();
	}
	if (isEQ(coverage, "01")
	&& isEQ(tr517rec.zrwvflg03,"Y")) {
		wsaaCovrSumins = wsaaCovrSumins.add(mgfeelrec.mgfee.getbigdata());
		if (isNE(mgfeelrec.mgfee, ZERO)) {
			checkCalcContTax7100();
		}
	}
}
protected void checkCalcContTax7100()
{	
	wsaaTax.set(0);	
	if (isEQ(tr52drec.txcode, SPACES)) {
		return ;
	}
	/* Read table TR52E                                                */
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrpf.getCnttype());
	wsaaTr52eCrtable.set("****");
	a500ReadTr52e();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		a500ReadTr52e();
	}
	if (isNE(tr52erec.taxind02, "Y")) {
		return ;
	}
	/* Call TR52D tax subroutine                                       */
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
	txcalcrec.chdrnum.set(chdrpf.getChdrnum());
	txcalcrec.life.set(SPACES);
	txcalcrec.coverage.set(SPACES);
	txcalcrec.rider.set(SPACES);
	txcalcrec.crtable.set(SPACES);
	txcalcrec.planSuffix.set(ZERO);
	txcalcrec.cnttype.set(chdrpf.getCnttype());
	txcalcrec.register.set(chdrpf.getReg());
	txcalcrec.taxrule.set(wsaaTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(chdrpf.getCntcurr());
	wsaaCntCurr.set(chdrpf.getCntcurr());
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	txcalcrec.amountIn.set(mgfeelrec.mgfee);
	txcalcrec.effdate.set(chdrpf.getOccdate());
	txcalcrec.transType.set("CNTF");
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, Varcom.oK)) {
		syserrrec.params.set(txcalcrec.linkRec);
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTax.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTax.add(txcalcrec.taxAmt[2]);
		}
	}
	wsaaCovrSumins = wsaaCovrSumins.add(wsaaTax.getbigdata());
}

protected void check3b22(Covrpf covrlnbIO)
{
	for (wsaaIndex.set(1); !(isGT(wsaaIndex,50)
	|| crtableMatch.isTrue()); wsaaIndex.add(1)){
		if (isEQ(covrlnbIO.getCrtable(),tr517rec.ctable[wsaaIndex.toInt()])) {
			crtableMatch.setTrue();
		}
	}
	if (!crtableMatch.isTrue()
	&& isNE(tr517rec.contitem,SPACES)) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(calprpmrec.chdrcoy.toString());
		itempf.setItemtabl(tr517);/* IJTI-1386 */
		itempf.setItemitem(tr517rec.contitem.toString());

		itempf = itempfDAO.getItempfRecord(itempf);
		if (itempf == null ) {
			tr517rec.tr517Rec.set(SPACES);
		}
		tr517rec.tr517Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isNE(tr517rec.tr517Rec,SPACES)) {
			check3b22(covrlnbIO);
		}
	}
	if (crtableMatch.isTrue()) {
		wsaaEarliestRerateDate.set(varcom.vrcmMaxDate);
		endFlag = true;
		return;
	}
}
protected void check3a22(Covrpf covrlnbIO, String covrrnlLife)
{
	for (wsaaIndex.set(1); !(isGT(wsaaIndex,50)
	|| crtableMatch.isTrue()); wsaaIndex.add(1)){
		if (isEQ(covrlnbIO.getCrtable(),tr517rec.ctable[wsaaIndex.toInt()])) {
			crtableMatch.setTrue();
		}
	}
	if (!crtableMatch.isTrue()
	&& isNE(tr517rec.contitem,SPACES)) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(calprpmrec.chdrcoy.toString());
		itempf.setItemtabl(tr517);/* IJTI-1386 */
		itempf.setItemitem(tr517rec.contitem.toString());

		itempf = itempfDAO.getItempfRecord(itempf);
		if (itempf == null ) {
			tr517rec.tr517Rec.set(SPACES);
		}
		tr517rec.tr517Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isNE(tr517rec.tr517Rec,SPACES)) {
			check3a22(covrlnbIO, covrrnlLife);
			return ;
		}
	}
	if (crtableMatch.isTrue()
	&& (isEQ(tr517rec.zrwvflg02,"Y")
	|| isEQ(covrlnbIO.getLife(),covrrnlLife))) {
		if (isLT(covrlnbIO.getRerateDate(),wsaaEarliestRerateDate)
		&& isGT(covrlnbIO.getRerateDate(),0)) {
			wsaaEarliestRerateDate.set(covrlnbIO.getRerateDate());
		}
		if (isLT(covrlnbIO.getCpiDate(),wsaaEarliestRerateDate)
		&& isGT(covrlnbIO.getCpiDate(),0)) {
			wsaaEarliestRerateDate.set(covrlnbIO.getCpiDate());
		}
		readIncr3a30(covrlnbIO);
	}
}
protected void readIncr3a30(Covrpf covrlnbIO)
{
	Incrpf i = read3a31(covrlnbIO);
	accSumins3a38(covrlnbIO, i);
}
protected Incrpf read3a31(Covrpf covrlnbIO)
{
	readT5687(covrlnbIO);
	Incrpf incrmjaIO = null;
	/* If no anniversary method exists on T5687 just accumulate*/
	/* the INSTPREM of this component for WOP sumins*/
	if (isEQ(t5687rec.anniversaryMethod,SPACES)) {
		return incrmjaIO;
	}
	if (isEQ(t6658rec.billfreq,"00")
			|| isEQ(covrlnbIO.getCpiDate(),varcom.vrcmMaxDate)) {
				return incrmjaIO;
			}
	
	incrmjaIO = incrpfDAO.getIncrData(covrlnbIO.getChdrcoy(),covrlnbIO.getChdrnum(),covrlnbIO.getLife(), covrlnbIO.getCoverage(), covrlnbIO.getRider(), covrlnbIO.getPlanSuffix());/* IJTI-1386 */
	if(incrmjaIO!=null && isNE(incrmjaIO.getCrrcd(),wsaaOldCpiDate)){
		incrmjaIO = null;
		return incrmjaIO;
	}
	if (incrmjaIO!=null) {
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.freqFactor.set(t6658rec.billfreq);
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(incrmjaIO.getCrrcd());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		if (isEQ(wsaaFirstIncrDate, varcom.vrcmMaxDate)) {
			wsaaEarliestRerateDate.set(datcon2rec.intDate2);
			wsaaFirstIncrDate.set(datcon2rec.intDate2);
		}
		else {
			if (isLT(datcon2rec.intDate2, wsaaFirstIncrDate)) {
				wsaaEarliestRerateDate.set(datcon2rec.intDate2);
				wsaaFirstIncrDate.set(datcon2rec.intDate2);
			}
			else {
				wsaaEarliestRerateDate.set(wsaaFirstIncrDate);
			}
		}
	}
	return incrmjaIO;
}
protected void accSumins3a38(Covrpf covrlnbIO, Incrpf incrmjaIO)
{
	if (isEQ(tr517rec.zrwvflg04,"Y")) {
		if (isEQ(covrlnbIO.getRider(),"00")) {
			if (incrmjaIO == null) {
				if(reratedPremKeeps.get(covrlnbIO.getUniqueNumber()) != null){
					 for(Covrpf c:reratedPremKeeps.get(covrlnbIO.getUniqueNumber())){
						if(isEQ(c.getCrrcd(), wsaaOldCpiDate)){
							wsaaCovrSumins = c.getSumins();
							break;
						}
					}	
				}		
				wsaaMainCrtable.set(covrlnbIO.getCrtable());
				wsaaMainCoverage.set(covrlnbIO.getCoverage());
				wsaaMainCessdate.set(covrlnbIO.getRiskCessDate());
				wsaaMainPcessdte.set(covrlnbIO.getPremCessDate());
				wsaaMainMortclass.set(covrlnbIO.getMortcls());
				wsaaMainLife.set(covrlnbIO.getLife());
			}
			else {
				wsaaCovrSumins.add(incrmjaIO.getNewinst());
			}
		}
		else {
			if (incrmjaIO == null) {
				if(reratedPremKeeps.get(covrlnbIO.getUniqueNumber()) != null){
					 for(Covrpf c:reratedPremKeeps.get(covrlnbIO.getUniqueNumber())){
						if(isEQ(c.getCrrcd(), wsaaOldCpiDate)){
							wsaaCovrSumins = wsaaCovrSumins.subtract(c.getSumins());
							break;
						}
					}	
				}			
				a200CalcBenefitAmount(covrlnbIO);
			}
			else {
				wsaaCovrSumins = wsaaCovrSumins.subtract(incrmjaIO.getNewinst());
			}
		}
	}
	else {
		if (incrmjaIO == null) {
			if(reratedPremKeeps.get(covrlnbIO.getUniqueNumber()) != null){
				 for(Covrpf c:reratedPremKeeps.get(covrlnbIO.getUniqueNumber())){
					if(isEQ(c.getCrrcd(), wsaaOldCpiDate)){
						wsaaCovrSumins = wsaaCovrSumins.add(c.getInstprem()); /*Covers updated rerate premiums and tax*/
						break;
					}
				}	
			}		
			if (covrlnbIO.getInstprem().compareTo(BigDecimal.ZERO)>0)						
				checkCalcCompTax7000(covrlnbIO,incrmjaIO);
		}
		else {
			/*           ADD INCRREF-NEWINST         TO WSAA-COVR-SUMINS       */
			wsaaCovrSumins = wsaaCovrSumins.add(incrmjaIO.getNewinst());
			if (incrmjaIO.getNewinst().compareTo(BigDecimal.ZERO)>0)						
				checkCalcCompTax7000(covrlnbIO,incrmjaIO);
		}
	}
}
protected void checkCalcCompTax7000(Covrpf covrlnbIO,Incrpf incrmjaIO)
{
	wsaaTax.set(0);
	if (isEQ(tr52drec.txcode, SPACES)) {
		return ;
	}
	
	/* Read table TR52E                                                */
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrpf.getCnttype());
	wsaaTr52eCrtable.set(covrlnbIO.getCrtable());
	a500ReadTr52e();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrpf.getCnttype());
		wsaaTr52eCrtable.set("****");
		a500ReadTr52e();
	}
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		a500ReadTr52e();
	}
	/* Call TR52D tax subroutine                                       */
	if (isNE(tr52erec.taxind01, "Y")) {
		return ;
	}
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
	txcalcrec.chdrnum.set(chdrpf.getChdrnum());
	txcalcrec.life.set(covrlnbIO.getLife());
	txcalcrec.coverage.set(covrlnbIO.getCoverage());
	txcalcrec.rider.set(covrlnbIO.getRider());
	txcalcrec.planSuffix.set(ZERO);
	txcalcrec.crtable.set(covrlnbIO.getCrtable());
	txcalcrec.cnttype.set(chdrpf.getCnttype());
	txcalcrec.register.set(chdrpf.getReg());
	txcalcrec.taxrule.set(wsaaTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(chdrpf.getCntcurr());
	wsaaCntCurr.set(chdrpf.getCntcurr());
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.effdate.set(chdrpf.getOccdate());
	txcalcrec.transType.set("PREM");
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	if ((incrmjaIO == null && (covrlnbIO.getInstprem().compareTo(BigDecimal.ZERO)>0))
			|| (incrmjaIO.getNewinst().compareTo(BigDecimal.ZERO)>0)) { 
		if (isEQ(tr52erec.zbastyp, "Y")) {
			if(incrmjaIO == null){
				if(reratedPremKeeps.get(covrlnbIO.getUniqueNumber()) != null){
					 for(Covrpf c:reratedPremKeeps.get(covrlnbIO.getUniqueNumber())){
						if(isEQ(c.getCrrcd(), wsaaOldCpiDate)){
							txcalcrec.amountIn.set(c.getZbinstprem());
							break;
						}
					}	
				}	
			}
			else 
				txcalcrec.amountIn.set(incrmjaIO.getZbnewinst());
		}
		else {
			if(incrmjaIO == null) {
				if(reratedPremKeeps.get(covrlnbIO.getUniqueNumber()) != null){
					 for(Covrpf c:reratedPremKeeps.get(covrlnbIO.getUniqueNumber())){
						if(isEQ(c.getCrrcd(), wsaaOldCpiDate)){
							txcalcrec.amountIn.set(c.getInstprem());			
							break;
						}
					}	
				}		
			}
			else
				txcalcrec.amountIn.set(incrmjaIO.getNewinst());
		}
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}	
	
	wsaaCovrSumins = wsaaCovrSumins.add(wsaaTax.getbigdata());
}
	
protected void a500ReadTr52e()
{
	tr52erec.tr52eRec.set(SPACES);
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(chdrpf.getChdrcoy().toString());
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItmto(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItemtabl(tr52e);
	itempf.setItemitem( wsaaTr52eKey.toString());
	itempf.setValidflag("1");
	List<Itempf> itempfList = itemDAO.findByItemDates(itempf);		 

	if ((itempfList == null || itempfList.size() == 0 )
			&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
		syserrrec.params.set(wsaaTr52eKey);
		syserrrec.statuz.set("MRNF");
		fatalError600();
	}
	if (itempfList != null && itempfList.size() != 0 ) {
		tr52erec.tr52eRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}	
}

protected void a200CalcBenefitAmount(Covrpf covrlnbIO)
{
	a200Ctrl(covrlnbIO);
	a210ByPass(covrlnbIO);
	a220ByPass();
}
protected void a200Ctrl(Covrpf covrlnbIO)
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(chdrpf.getChdrcoy().toString());
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItmto(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItemtabl(t5687);
	itempf.setItemitem(wsaaMainCrtable.toString());
	itempf.setValidflag("1");
	List<Itempf> itempfList = itemDAO.findByItemDates(itempf);		 

	if (itempfList == null || itempfList.size() == 0 ){
		initialize(t5687rec.t5687Rec);
	}
	t5687rec.t5687Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
}

protected void a210ByPass(Covrpf covrlnbIO)
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(calprpmrec.chdrcoy.toString());
	itempf.setItemtabl(t5675.trim());/* IJTI-1386 */
	itempf.setItemitem(t5687rec.premmeth.toString().trim());
	
	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf != null ) {
		t5675rec.t5675Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaPremMeth.set(t5687rec.premmeth);
	}
	else{
		/*premReqd.setTrue();*/
		t5675rec.premsubr.set(SPACES);
		return;
	}
	/*if (isEQ(t5675rec.premsubr,SPACES)) {
		wsaaPremStatuz.set("Y");
	}
	else {
		wsaaPremStatuz.set("N");
	}*/
	a300CallPremiumCalc(covrlnbIO);
	/*  This is the second call using the main component*/
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(chdrpf.getChdrcoy().toString());
	itempf.setItmfrm(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItmto(new BigDecimal(chdrpf.getOccdate().toString()));
	itempf.setItemtabl(t5687);
	itempf.setItemitem(covrlnbIO.getCrtable());/* IJTI-1386 */
	itempf.setValidflag("1");
	List<Itempf> itempfList = itemDAO.findByItemDates(itempf);		 

	if (itempfList == null || itempfList.size() == 0 ){
		initialize(t5687rec.t5687Rec);
		//a220ByPass(covrlnbIO);
	}
	t5687rec.t5687Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
}

protected void a220ByPass()
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(calprpmrec.chdrcoy.toString());
	itempf.setItemtabl(t5675.trim());/* IJTI-1386 */
	itempf.setItemitem(t5687rec.premmeth.toString().trim());
	
	itempf = itempfDAO.getItempfRecord(itempf);
	if (itempf != null ) {
		t5675rec.t5675Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaPremMeth.set(t5687rec.premmeth);
	}
	else{
	/*	premReqd.setTrue();*/
		t5675rec.premsubr.set(SPACES);
		return;
	}
}
protected void a300CallPremiumCalc(Covrpf covrlnbIO)
{
	if(prmhldtrad) {//ILIFE-8509
		initPremiumrec(covrlnbIO);
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))) 
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		else {
			premiumrec.setPmexCall.set("Y");
			premiumrec.inputPrevPrem.set(ZERO);//ILIFE-8537
			premiumrec.updateRequired.set("N");
			if(("PMEX").equals(t5675rec.premsubr.toString().trim()))
				callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		if(stampDutyflag)
			premiumrec.calcPrem.add(premiumrec.zstpduty01);
		return;
	}
	/*  Read Main Life file*/
	Lifepf  lifelnbIO = null;
	lifelnbIO = lifepfDAO.getLifeEnqRecord(calprpmrec.chdrcoy.toString(),calprpmrec.chdrnum.toString(), wsaaMainLife.toString(), "00");
	if(lifelnbIO == null){
		syserrrec.statuz.set(calprpmrec.chdrcoy.toString().concat(calprpmrec.chdrnum.toString()).concat(wsaaMainLife.toString()));
		fatalError600();
	}
	wsaaMainAge.set(lifelnbIO.getAnbAtCcd());
	wsaaAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
	wsaaSex.set(lifelnbIO.getCltsex());
	wsaaJointAge.set(ZERO);
	wsbbSex.set(SPACES);
	/*  Read Join Life file*/
	lifelnbIO = lifepfDAO.getLifelnbRecord(calprpmrec.chdrcoy.toString(),calprpmrec.chdrnum.toString(), wsaaMainLife.toString(),"01");
	if (lifelnbIO != null) {
		wsaaJointAge.set(lifelnbIO.getAnbAtCcd());
		wsbbSex.set(lifelnbIO.getCltsex());
	} 
	initialize(premiumrec.premiumRec);
	premiumrec.function.set("CALC");
	premiumrec.crtable.set(wsaaMainCrtable);
	premiumrec.chdrChdrcoy.set(chdrpf.getChdrcoy().toString());
	premiumrec.chdrChdrnum.set(chdrpf.getChdrnum());
	premiumrec.lifeLife.set(lifelnbIO.getLife());
	premiumrec.lifeJlife.set("00");
	premiumrec.covrCoverage.set(wsaaMainCoverage);
	premiumrec.covrRider.set("00");
	premiumrec.effectdt.set(chdrpf.getOccdate());
	premiumrec.termdate.set(wsaaMainPcessdte);
	premiumrec.currcode.set(chdrpf.getCntcurr());
	premiumrec.lsex.set(wsaaSex);
	premiumrec.lage.set(wsaaAnbAtCcd);
	premiumrec.lage.set(wsaaMainAge);
	premiumrec.jlsex.set(wsbbSex);
	premiumrec.jlage.set(wsaaJointAge);
	datcon3rec.intDate1.set(premiumrec.effectdt);
	datcon3rec.intDate2.set(premiumrec.termdate);
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz,varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	datcon3rec.freqFactor.add(0.99999);
	premiumrec.duration.set(datcon3rec.freqFactor);
	/*  (wsaa-sumin already adjusted for plan processing)*/
	premiumrec.cnttype.set(chdrpf.getCnttype());
	datcon3rec.intDate1.set(premiumrec.effectdt);
	datcon3rec.intDate2.set(wsaaMainCessdate);
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz,varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	datcon3rec.freqFactor.add(0.99999);
	premiumrec.riskCessTerm.set(datcon3rec.freqFactor);
	/* MOVE ZEROES                 TO CPRM-BEN-CESS-TERM.*/
	premiumrec.sumin.set(wsaaCovrSumins);
	premiumrec.mortcls.set(wsaaMainMortclass);
	premiumrec.billfreq.set(chdrpf.getBillfreq());
	premiumrec.mop.set(chdrpf.getBillchnl());
	premiumrec.ratingdate.set(chdrpf.getOccdate());
	premiumrec.reRateDate.set(chdrpf.getOccdate());
	premiumrec.language.set(calprpmrec.language);
	
	if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
	{
		callProgram(t5675rec.premsubr, premiumrec.premiumRec);
	}
	else{
	Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
	Vpxlextrec vpxlextrec = new Vpxlextrec();
	vpxlextrec.function.set("INIT");
	callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
	
	Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
	vpxchdrrec.function.set("INIT");
	callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
	premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
	
	premiumrec.premMethod.set(wsaaPremMeth);
	callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxchdrrec);
	}
	if (isNE(premiumrec.statuz,varcom.oK)) {
		syserrrec.params.set(premiumrec.premiumRec);
		syserrrec.statuz.set(premiumrec.statuz);
		fatalError600();
	}
	wsaaCovrSumins = premiumrec.calcPrem.getbigdata();
}

protected void callReadRCVDPF(){//ILIFE-8509
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	/* IJTI-1386 START*/
	rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
	rcvdPFObject.setChdrnum(covrpf.getChdrnum());
	rcvdPFObject.setLife(covrpf.getLife());
	rcvdPFObject.setCoverage(covrpf.getCoverage());
	rcvdPFObject.setRider(covrpf.getRider());
	rcvdPFObject.setCrtable(covrpf.getCrtable());
	/* IJTI-1386 END*/
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
}

protected void initPremiumrec(Covrpf covr) {//ILIFE-8509
	premiumrec.premiumRec.set(SPACES);
	premiumrec.chdrChdrcoy.set(covr.getChdrcoy());
	premiumrec.chdrChdrnum.set(covr.getChdrnum());
	premiumrec.lifeLife.set(covr.getLife());
	premiumrec.lifeJlife.set(covr.getJlife());
	premiumrec.covrCoverage.set(covr.getCoverage());
	premiumrec.covrRider.set(covr.getRider());
	premiumrec.crtable.set(covr.getCrtable());
	premiumrec.effectdt.set(wsaaOldCpiDate);
	premiumrec.termdate.set(covr.getPremCessDate());
	premiumrec.language.set(calprpmrec.language);
	wsaaLastRrtDate.set(wsaaOldCpiDate);
	getLifeDetails7000();
	checkJointLife3270();
	datcon3rec.intDate1.set(wsaaOldCpiDate);
	datcon3rec.intDate2.set(premiumrec.termdate);
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz,varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	datcon3rec.freqFactor.add(0.99999);
	premiumrec.duration.set(datcon3rec.freqFactor);
	premiumrec.currcode.set(chdrpf.getCntcurr());
	premiumrec.sumin.set(covr.getSumins());
	premiumrec.mortcls.set(covr.getMortcls());
	premiumrec.billfreq.set(payrpf.getBillfreq());
	premiumrec.mop.set(payrpf.getBillchnl());
	premiumrec.ratingdate.set(chdrpf.getOccdate());
	premiumrec.reRateDate.set(chdrpf.getOccdate());
	premiumrec.calcPrem.set(ZERO);
	premiumrec.calcBasPrem.set(ZERO);
	premiumrec.calcLoaPrem.set(ZERO);
	readT5675();
	if (isEQ(t5675rec.premsubr,SPACES)) {
		incrsrec.newinst01.set(incrsrec.currinst01);
	}
	getAnny11000();
	Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
	vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
	vpxlextrec.function.set("INIT");
	callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
	Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
	vpxchdrrec.function.set("INIT");
	callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
	premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
	premiumrec.cnttype.set(chdrpf.getCnttype());	
	callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
	premiumrec.premMethod.set(wsaaT5675Item);
	dialdownFlag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
	premiumflag = FeaConfg.isFeatureExist(chdrpf.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
	if(premiumflag || dialdownFlag){
		callReadRCVDPF();
		if(rcvdPFObject == null)
			premiumrec.dialdownoption.set(100);
		else {
			if(rcvdPFObject.getDialdownoption().startsWith("0"))/* IJTI-1386 */
				premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption().substring(1));/* IJTI-1386 */
			else
				premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());/* IJTI-1386 */
			if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("")){
				if(rcvdPFObject.getPrmbasis().equals("S"))
					premiumrec.prmbasis.set("Y");
				else
					premiumrec.prmbasis.set(SPACES);
			}
			premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod() != null ? rcvdPFObject.getWaitperiod() : SPACES);
			premiumrec.bentrm.set(rcvdPFObject.getBentrm() != null ? rcvdPFObject.getBentrm() : SPACES);
			premiumrec.poltyp.set(rcvdPFObject.getPoltyp() != null ? rcvdPFObject.getPoltyp() : SPACES);
		}
	}
	premiumrec.tpdtype.set(null != covr.getTpdtype() ? (covr.getTpdtype().trim() != null ? (covr.getTpdtype().trim().length() >= 4 ? 
			String.valueOf(covr.getTpdtype().trim().charAt(4)) : SPACES) :SPACES) : SPACES);
	premiumrec.rstate01.set(null != covr.getZclstate() ? covr.getZclstate().trim() : SPACES);
	premiumrec.cownnum.set(chdrpf.getCownnum());
	premiumrec.occdate.set(chdrpf.getOccdate());
	premiumrec.prevSumIns.set(ZERO);
}

protected void updateIncr() {
	incrIO.setTranno(calprpmrec.tranno.toInt());
	incrIO.setValidflag("1");
    incrIO.setStatcode(covrpf.getStatcode());
    incrIO.setPstatcode(covrpf.getPstatcode());
    incrIO.setCrtable(covrpf.getCrtable());
    incrIO.setChdrcoy(incrsrec.chdrcoy.toString());
    incrIO.setChdrnum(incrsrec.chdrnum.toString());
    incrIO.setLife(incrsrec.life.toString());
    incrIO.setJlife(covrpf.getJlife());
    incrIO.setCoverage(incrsrec.coverage.toString());
    incrIO.setRider(incrsrec.rider.toString());
    incrIO.setPlnsfx(incrsrec.plnsfx.toInt());
    incrIO.setCrrcd(incrsrec.effdate.toInt());
    incrIO.setCrtable(incrsrec.crtable.toString());
    incrIO.setAnniversaryMethod(incrsrec.annvmeth.toString());
    incrIO.setBasicCommMeth(t5687rec.basicCommMeth.toString());
    incrIO.setBascpy(t5687rec.bascpy.toString());
    incrIO.setRnwcpy(t5687rec.rnwcpy.toString());
    incrIO.setSrvcpy(t5687rec.srvcpy.toString());
    incrIO.setOrigSum(incrsrec.origsum.getbigdata());
    incrIO.setLastSum(incrsrec.lastsum.getbigdata());
    incrIO.setNewsum(incrsrec.newsum.getbigdata());
    incrIO.setOrigInst(incrsrec.originst01.getbigdata());
    incrIO.setLastInst(incrsrec.lastinst01.getbigdata());
    incrIO.setNewinst(incrsrec.newinst01.getbigdata());
    incrIO.setZboriginst(incrsrec.originst02.getbigdata());
    incrIO.setZblastinst(incrsrec.lastinst02.getbigdata());
    incrIO.setZbnewinst(incrsrec.newinst02.getbigdata());
    incrIO.setZloriginst(incrsrec.originst03.getbigdata());
    incrIO.setZllastinst(incrsrec.lastinst03.getbigdata());
    incrIO.setZlnewinst(incrsrec.newinst03.getbigdata());
    incrIO.setPctinc(incrsrec.pctinc.toInt());
    incrIO.setRefusalFlag(SPACES.toString());
    incrIO.setCeaseInd(SPACES.toString());
    if(stampDutyflag) {
    	String vpmModule = "INCRSUM";
    	if((vpmModule).equals((t6658rec.premsubr != null && isNE(t6658rec.premsubr, SPACES)) ? t6658rec.premsubr.toString().trim() : false)) {
    		incrIO.setZstpduty01(incrsrec.zstpduty01.getbigdata());
    	}
    	else {
        	if(incrsrec.zstpduty03.getbigdata().compareTo(BigDecimal.ZERO) != 0)
        		incrIO.setZstpduty01(incrsrec.zstpduty03.getbigdata());
        	else
        		incrIO.setZstpduty01(incrsrec.zstpduty02.getbigdata());
    	}
    }
    incrIO.setTransactionDate(calprpmrec.reinstDate.toInt());
    incrIO.setTransactionTime(Integer.parseInt(getCobolTime()));
    incrIO.setUser(0);
	varcom.vrcmTranid.set(SPACES);
    incrIO.setTermid(varcom.vrcmTermid.toString());
}

}
