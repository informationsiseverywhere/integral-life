/*

 * File: Pr678.java
 * Date: 30 August 2009 1:53:42
 * Author: Quipoz Limited
 * 
 * Class transformed from PR678.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.terminationclaims.dataaccess.CovtrbnTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.screens.Sr678ScreenVars;
import com.csc.life.terminationclaims.tablestructures.Tr687rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Itdmkey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*               WAIVER OF HOSPITAL BENEFIT
*               ==========================
*
*  This enquiry screen is activated from Hospital benefit Component
*  Entry / Enqruiy Program. The Benefit Details is retrieved from
*  Table TR687.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr678 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR678");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTr687Itemitem = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr687Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTr687Itemitem, 0);
	private FixedLengthStringData wsaaTr687Benpln = new FixedLengthStringData(2).isAPartOf(wsaaTr687Itemitem, 4);

	private FixedLengthStringData wsaaVariables = new FixedLengthStringData(37);
	private ZonedDecimalData ix = new ZonedDecimalData(5, 0).isAPartOf(wsaaVariables, 0).setUnsigned();
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(2, 0).isAPartOf(wsaaVariables, 5).setUnsigned();
	private FixedLengthStringData wsaaBenDesc = new FixedLengthStringData(30).isAPartOf(wsaaVariables, 7);
		/* TABLES */
	private String tr687 = "TR687";
	private String tr50a = "TR50A";
	private String itdmrec = "ITEMREC   ";
	private String descrec = "DESCREC   ";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
	private CovrpfDAO covrDao = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
		/*Coverage/rider transactions - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
		/*Coverage transactions - regular benefit*/
	private CovtrbnTableDAM covtrbnIO = new CovtrbnTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Hospital Benefit File Logical*/
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Tr687rec tr687rec = new Tr687rec();
	private Itdmkey wsaaItdmkey = new Itdmkey();
	private Sr678ScreenVars sv = ScreenProgram.getScreenVars( Sr678ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		loadSubfile1050, 
		exit1090, 
		exit1290, 
		exit1390, 
		preExit
	}

	public Pr678() {
		super();
		screenVars = sv;
		new ScreenModel("Sr678", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					retrieveCovr1020();
				}
				case loadSubfile1050: {
					loadSubfile1050();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		initialize(wsaaVariables);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR678", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.zunit.set(ZERO);
		sv.factor.set(ZERO);
		sv.premunit.set(ZERO);
		sv.zssi.set(ZERO);
		sv.aad.set(ZERO);
		sv.itmfrm.set(varcom.vrcmMaxDate);
		sv.itmto.set(varcom.vrcmMaxDate);
	}

protected void retrieveCovr1020()
	{
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		hbnfIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(),varcom.oK)
		&& isNE(hbnfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		wsaaTr687Benpln.set(hbnfIO.getBenpln());
		covtrbnIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtrbnIO);
		if (isNE(covtrbnIO.getStatuz(),varcom.oK)
		&& isNE(covtrbnIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covtrbnIO.getParams());
			fatalError600();
		}
		if (isNE(covtrbnIO.getStatuz(),varcom.mrnf)) {
			covtrbnIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, covtrbnIO);
			if (isNE(covtrbnIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(covtrbnIO.getParams());
				fatalError600();
			}
			wsaaItdmkey.itdmItemcoy.set(covtrbnIO.getChdrcoy());
			wsaaTr687Crtable.set(covtrbnIO.getCrtable());
			wsaaTr687Benpln.set(hbnfIO.getBenpln());
			wsaaItdmkey.itdmItemitem.set(wsaaTr687Itemitem);
			goTo(GotoLabel.loadSubfile1050);
		}
		covtmjaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isNE(covtmjaIO.getStatuz(),varcom.oK)
		&& isNE(covtmjaIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covtmjaIO.getParams());
			fatalError600();
		}
		if (isNE(covtmjaIO.getStatuz(),varcom.mrnf)) {
			wsaaItdmkey.itdmItemcoy.set(covtmjaIO.getChdrcoy());
			wsaaTr687Crtable.set(covtmjaIO.getCrtable());
			wsaaTr687Benpln.set(hbnfIO.getBenpln());
			wsaaItdmkey.itdmItemitem.set(wsaaTr687Itemitem);
			goTo(GotoLabel.loadSubfile1050);
		}
		/*ilife-3396 Improve the performance of  Contract enquiry- TEN transaction   */
		covrpf = covrDao.getCacheObject(covrpf);
		wsaaItdmkey.itdmItemcoy.set(covrpf.getChdrcoy());
		wsaaTr687Crtable.set(covrpf.getCrtable());
		wsaaTr687Benpln.set(hbnfIO.getBenpln());
		wsaaItdmkey.itdmItemitem.set(wsaaTr687Itemitem);
	}

protected void loadSubfile1050()
	{
		wsaaItdmkey.itdmItemtabl.set(tr687);
		itdmIO.setDataArea(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsaaItdmkey.itdmItemcoy);
		itdmIO.setItemtabl(wsaaItdmkey.itdmItemtabl);
		itdmIO.setItemitem(wsaaItdmkey.itdmItemitem);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsaaItdmkey.itdmItemcoy)
		|| isNE(itdmIO.getItemtabl(),tr687)
		|| isNE(itdmIO.getItemitem(),wsaaItdmkey.itdmItemitem)) {
			itdmIO.setStatuz(varcom.endp);
			blankSubfile1400();
			goTo(GotoLabel.exit1090);
		}
		tr687rec.tr687Rec.set(itdmIO.getGenarea());
		setUpNonSubfile1100();
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			loadSubfile1200();
		}
		
		scrnparams.subfileRrn.set(1);
	}

protected void setUpNonSubfile1100()
	{
		/*BEGIN*/
		sv.crtable.set(wsaaTr687Crtable);
		sv.benpln.set(wsaaTr687Benpln);
		sv.itmfrm.set(itdmIO.getItmfrm());
		sv.itmto.set(itdmIO.getItmto());
		sv.zssi.set(tr687rec.zssi);
		sv.premunit.set(tr687rec.premunit);
		sv.factor.set(tr687rec.factor);
		sv.zunit.set(hbnfIO.getZunit());
		sv.aad.set(tr687rec.aad);
		/*EXIT*/
	}

protected void loadSubfile1200()
	{
		try {
			begn1210();
			contitem1230();
		}
		catch (GOTOException e){
		}
	}

protected void begn1210()
	{
		for (ix.set(1); !(isGT(ix,6)); ix.add(1)){
			if (isNE(tr687rec.hosben[ix.toInt()],SPACES)
			|| isNE(tr687rec.amtlife[ix.toInt()],ZERO)
			|| isNE(tr687rec.amtyear[ix.toInt()],ZERO)
			|| isNE(tr687rec.gdeduct[ix.toInt()],ZERO)
			|| isNE(tr687rec.copay[ix.toInt()],ZERO)
			|| isEQ(ix,1)) {
				wsaaSeqno.add(1);
				sv.seqno.set(wsaaSeqno);
				sv.hosben.set(tr687rec.hosben[ix.toInt()]);
				compute(sv.amtlife, 0).set(mult(tr687rec.amtlife[ix.toInt()],sv.zunit));
				compute(sv.amtyear, 0).set(mult(tr687rec.amtyear[ix.toInt()],sv.zunit));
				compute(sv.benfamt, 0).set(mult(tr687rec.benfamt[ix.toInt()],sv.zunit));
				sv.gdeduct.set(tr687rec.gdeduct[ix.toInt()]);
				sv.nofday.set(tr687rec.nofday[ix.toInt()]);
				sv.copay.set(tr687rec.copay[ix.toInt()]);
				benefitDesc1300();
				sv.benefits.set(wsaaBenDesc);
				scrnparams.function.set(varcom.sadd);
				processScreen("SR678", sv);
				if (isNE(scrnparams.statuz,varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
		}
	}

protected void contitem1230()
	{
		if (isEQ(tr687rec.contitem,SPACES)) {
			itdmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(wsaaItdmkey.itdmItemcoy);
		itdmIO.setItemtabl(wsaaItdmkey.itdmItemtabl);
		itdmIO.setItemitem(tr687rec.contitem);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),wsaaItdmkey.itdmItemcoy)
		|| isNE(itdmIO.getItemtabl(),tr687)
		|| isNE(itdmIO.getItemitem(),tr687rec.contitem)) {
			itdmIO.setStatuz(varcom.endp);
		}
		tr687rec.tr687Rec.set(itdmIO.getGenarea());
	}

protected void benefitDesc1300()
	{
		try {
			begn1310();
		}
		catch (GOTOException e){
		}
	}

protected void begn1310()
	{
		if (isEQ(tr687rec.hosben[ix.toInt()],SPACES)) {
			wsaaBenDesc.fill(SPACES);
			goTo(GotoLabel.exit1390);
		}
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tr50a);
		descIO.setDescitem(tr687rec.hosben[ix.toInt()]);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaBenDesc.set(descIO.getLongdesc());
		}
		else {
			wsaaBenDesc.fill("?");
		}
	}

protected void blankSubfile1400()
	{
		begin1410();
	}

protected void begin1410()
	{
		sv.crtable.set(wsaaTr687Crtable);
		sv.benpln.set(wsaaTr687Benpln);
		sv.itmfrm.set(varcom.vrcmMaxDate);
		sv.itmto.set(varcom.vrcmMaxDate);
		sv.zunit.set(ZERO);
		sv.aad.set(ZERO);
		sv.zssi.set(ZERO);
		sv.premunit.set(ZERO);
		sv.factor.set(ZERO);
		sv.seqno.set(ZERO);
		sv.hosben.set(SPACES);
		sv.amtlife.set(ZERO);
		sv.amtyear.set(ZERO);
		sv.benefits.set(SPACES);
		sv.benfamt.set(ZERO);
		sv.nofday.set(ZERO);
		sv.gdeduct.set(ZERO);
		sv.copay.set(ZERO);
		scrnparams.function.set(varcom.sadd);
		processScreen("SR678", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SR678", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

protected void validation2610()
	{
		/*UPDATE-ERROR-INDICATORS*/
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR678", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR678", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
