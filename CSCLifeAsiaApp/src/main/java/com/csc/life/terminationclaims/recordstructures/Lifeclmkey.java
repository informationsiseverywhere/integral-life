package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:06
 * Description:
 * Copybook name: LIFECLMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifeclmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifeclmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lifeclmKey = new FixedLengthStringData(64).isAPartOf(lifeclmFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifeclmChdrcoy = new FixedLengthStringData(1).isAPartOf(lifeclmKey, 0);
  	public FixedLengthStringData lifeclmChdrnum = new FixedLengthStringData(8).isAPartOf(lifeclmKey, 1);
  	public FixedLengthStringData lifeclmLife = new FixedLengthStringData(2).isAPartOf(lifeclmKey, 9);
  	public FixedLengthStringData lifeclmJlife = new FixedLengthStringData(2).isAPartOf(lifeclmKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(lifeclmKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifeclmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifeclmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}