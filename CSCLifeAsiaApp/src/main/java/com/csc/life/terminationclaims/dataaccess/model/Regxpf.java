package com.csc.life.terminationclaims.dataaccess.model;


public class Regxpf{
	
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int rgpynum;
	private int planSuffix;
	private int tranno;
	private int nextPaydate;
	private String rgpytype;
	private String crtable;
	private String validflag;
	private int finalPaydate;
	private int revdte;
	private String rgpystat;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getRgpynum() {
		return rgpynum;
	}
	public void setRgpynum(int rgpynum) {
		this.rgpynum = rgpynum;
	}
	public int getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getNextPaydate() {
		return nextPaydate;
	}
	public void setNextPaydate(int nextPaydate) {
		this.nextPaydate = nextPaydate;
	}
	public String getRgpytype() {
		return rgpytype;
	}
	public void setRgpytype(String rgpytype) {
		this.rgpytype = rgpytype;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public int getFinalPaydate() {
		return finalPaydate;
	}
	public void setFinalPaydate(int finalPaydate) {
		this.finalPaydate = finalPaydate;
	}
	public int getRevdte() {
		return revdte;
	}
	public void setRevdte(int revdte) {
		this.revdte = revdte;
	}
	public String getRgpystat() {
		return rgpystat;
	}
	public void setRgpystat(String rgpystat) {
		this.rgpystat = rgpystat;
	}
}