package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Td5herec  extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData td5heRec = new FixedLengthStringData(50);
		public ZonedDecimalData minpayamnt = new ZonedDecimalData(9, 0).isAPartOf(td5heRec, 0);
		public ZonedDecimalData maxpayamnt = new ZonedDecimalData(9, 0).isAPartOf(td5heRec, 9);
	  	public FixedLengthStringData filler1 = new FixedLengthStringData(32).isAPartOf(td5heRec, 18, FILLER);


		public void initialize() {
			COBOLFunctions.initialize(td5heRec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			td5heRec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}

}
