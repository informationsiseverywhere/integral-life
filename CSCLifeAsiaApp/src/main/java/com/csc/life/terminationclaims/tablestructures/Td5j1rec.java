package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Td5j1rec extends ExternalData {


	  //*******************************
	  //Attribute Declarations
	  //*******************************
	  
	  	public FixedLengthStringData td5j1Rec = new FixedLengthStringData(500);
	  	public ZonedDecimalData allowperiod = new ZonedDecimalData(3, 0).isAPartOf(td5j1Rec, 0);
	  	public ZonedDecimalData daycheck = new ZonedDecimalData(3, 0).isAPartOf(td5j1Rec, 3);
		public FixedLengthStringData subroutine = new FixedLengthStringData(7).isAPartOf(td5j1Rec, 6);
	  	public FixedLengthStringData filler = new FixedLengthStringData(487).isAPartOf(td5j1Rec, 13, FILLER);


		public void initialize() {
			COBOLFunctions.initialize(td5j1Rec);
		}	

		
		public FixedLengthStringData getBaseString() {
	  		if (baseString == null) {
	   			baseString = new FixedLengthStringData(getLength());
	   			td5j1Rec.isAPartOf(baseString, true);
	   			baseString.resetIsAPartOfOffset();
	  		}
	  		return baseString;
		}


}
