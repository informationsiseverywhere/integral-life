package com.csc.life.terminationclaims.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AclhTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:09
 * Class transformed from ACLH.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AclhTableDAM extends AclhpfTableDAM {

	public AclhTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ACLH");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", RGPYNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "JOBNM, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "CRTABLE, " +
		            "RGPYNUM, " +
		            "ZCLMRECD, " +
		            "INCURDT, " +
		            "GCADMDT, " +
		            "DISCHDT, " +
		            "DIAGCDE, " +
		            "ZDOCTOR, " +
		            "ZMEDPRV, " +
		            "TCLMAMT, " +
		            "USRPRF, " +
		            "DATIME, " +
		            "LIFE, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "RGPYNUM DESC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "RGPYNUM ASC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               jobName,
                               coverage,
                               rider,
                               crtable,
                               rgpynum,
                               recvdDate,
                               incurdt,
                               gcadmdt,
                               dischdt,
                               diagcde,
                               zdoctor,
                               zmedprv,
                               tclmamt,
                               userProfile,
                               datime,
                               life,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(238);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getRgpynum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, rgpynum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller180 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller70.setInternal(rgpynum.toInternal());
	nonKeyFiller180.setInternal(life.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(118);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getJobName().toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getCrtable().toInternal()
					+ nonKeyFiller70.toInternal()
					+ getRecvdDate().toInternal()
					+ getIncurdt().toInternal()
					+ getGcadmdt().toInternal()
					+ getDischdt().toInternal()
					+ getDiagcde().toInternal()
					+ getZdoctor().toInternal()
					+ getZmedprv().toInternal()
					+ getTclmamt().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal()
					+ nonKeyFiller180.toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, recvdDate);
			what = ExternalData.chop(what, incurdt);
			what = ExternalData.chop(what, gcadmdt);
			what = ExternalData.chop(what, dischdt);
			what = ExternalData.chop(what, diagcde);
			what = ExternalData.chop(what, zdoctor);
			what = ExternalData.chop(what, zmedprv);
			what = ExternalData.chop(what, tclmamt);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, nonKeyFiller180);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getRgpynum() {
		return rgpynum;
	}
	public void setRgpynum(Object what) {
		setRgpynum(what, false);
	}
	public void setRgpynum(Object what, boolean rounded) {
		if (rounded)
			rgpynum.setRounded(what);
		else
			rgpynum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getRecvdDate() {
		return recvdDate;
	}
	public void setRecvdDate(Object what) {
		setRecvdDate(what, false);
	}
	public void setRecvdDate(Object what, boolean rounded) {
		if (rounded)
			recvdDate.setRounded(what);
		else
			recvdDate.set(what);
	}	
	public PackedDecimalData getIncurdt() {
		return incurdt;
	}
	public void setIncurdt(Object what) {
		setIncurdt(what, false);
	}
	public void setIncurdt(Object what, boolean rounded) {
		if (rounded)
			incurdt.setRounded(what);
		else
			incurdt.set(what);
	}	
	public PackedDecimalData getGcadmdt() {
		return gcadmdt;
	}
	public void setGcadmdt(Object what) {
		setGcadmdt(what, false);
	}
	public void setGcadmdt(Object what, boolean rounded) {
		if (rounded)
			gcadmdt.setRounded(what);
		else
			gcadmdt.set(what);
	}	
	public PackedDecimalData getDischdt() {
		return dischdt;
	}
	public void setDischdt(Object what) {
		setDischdt(what, false);
	}
	public void setDischdt(Object what, boolean rounded) {
		if (rounded)
			dischdt.setRounded(what);
		else
			dischdt.set(what);
	}	
	public FixedLengthStringData getDiagcde() {
		return diagcde;
	}
	public void setDiagcde(Object what) {
		diagcde.set(what);
	}	
	public FixedLengthStringData getZdoctor() {
		return zdoctor;
	}
	public void setZdoctor(Object what) {
		zdoctor.set(what);
	}	
	public FixedLengthStringData getZmedprv() {
		return zmedprv;
	}
	public void setZmedprv(Object what) {
		zmedprv.set(what);
	}	
	public PackedDecimalData getTclmamt() {
		return tclmamt;
	}
	public void setTclmamt(Object what) {
		setTclmamt(what, false);
	}
	public void setTclmamt(Object what, boolean rounded) {
		if (rounded)
			tclmamt.setRounded(what);
		else
			tclmamt.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		rgpynum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		jobName.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		crtable.clear();
		nonKeyFiller70.clear();
		recvdDate.clear();
		incurdt.clear();
		gcadmdt.clear();
		dischdt.clear();
		diagcde.clear();
		zdoctor.clear();
		zmedprv.clear();
		tclmamt.clear();
		userProfile.clear();
		datime.clear();
		nonKeyFiller180.clear();		
	}


}