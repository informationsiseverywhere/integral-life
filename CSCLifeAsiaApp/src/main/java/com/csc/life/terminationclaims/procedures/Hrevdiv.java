/*
 * File: Hrevdiv.java
 * Date: 29 August 2009 22:55:19
 * Author: Quipoz Limited
 * 
 * Class transformed from HREVDIV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisrevTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*       DIVIDEND AND DIVIDEND INTEREST REVERSAL FOR ETI & RPU
*
*  This subroutine is called from REVODUE via T5671 as a generic
*  subroutine. It attempts to reverse the dividend and interest
*  withdrawal at ETI & RPU processing.
*
*  The routine is driven by :
*    - The Company to which this Contract belongs.
*    - The Contract Header Number.
*    - The Transaction Number used in the original
*      ETI/RPU processing transaction.
*
*
*  - Locate HDIV records and delete them. Write new ones with
*    zero capitalised TRANNO.
*
*  - Locate HDIS records and delete the current validflag 1's,
*    re-instate validflag 2 to 1.
*
*
*
*****************************************************************
* </pre>
*/
public class Hrevdiv extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaStmtNo = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaStmtDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaBalance = new PackedDecimalData(17, 2);
		/* FORMATS */
	private String hdisrec = "HDISREC";
	private String hdivrec = "HDIVREC";
	private String hdisrevrec = "HDISREVREC";
	private String hdivrevrec = "HDIVREVREC";
		/* ERRORS */
	private String hl15 = "HL15";
	private Greversrec greversrec = new Greversrec();
		/*Dividend & Interest Summary Logical*/
	private HdisTableDAM hdisIO = new HdisTableDAM();
		/*Cash Dividend Alloc Summary (Reversal)*/
	private HdisrevTableDAM hdisrevIO = new HdisrevTableDAM();
		/*Dividend Allocation Trans Details Logica*/
	private HdivTableDAM hdivIO = new HdivTableDAM();
		/*Cash Dividend Alloc Transaction(Reversal*/
	private HdivrevTableDAM hdivrevIO = new HdivrevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		seExit9090, 
		dbExit9190
	}

	public Hrevdiv() {
		super();
	}

public void mainline(Object... parmArray)
	{
		greversrec.reverseRec = convertAndSetParam(greversrec.reverseRec, parmArray, 0);
		try {
			main0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main0000()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		setupHdivrev1000();
		while ( !(isEQ(hdivrevIO.getStatuz(),varcom.endp))) {
			maintainHdiv3000();
		}
		
		setupHdisrev4000();
		while ( !(isEQ(hdisrevIO.getStatuz(),varcom.endp))) {
			maintainHdis5000();
		}
		
		/*EXIT*/
		exitProgram();
	}

protected void setupHdivrev1000()
	{
		/*START*/
		hdivrevIO.setDataKey(SPACES);
		hdivrevIO.setChdrcoy(greversrec.chdrcoy);
		hdivrevIO.setChdrnum(greversrec.chdrnum);
		hdivrevIO.setTranno(greversrec.tranno);
		hdivrevIO.setFunction(varcom.begn);
		hdivrevIO.setFormat(hdivrevrec);
		/*EXIT*/
	}

protected void maintainHdiv3000()
	{

	//performance improvement --  atiwari23 
	hdivrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	
	
		start3010();
	}

protected void start3010()
	{
		SmartFileCode.execute(appVars, hdivrevIO);
		if (isNE(hdivrevIO.getStatuz(),varcom.oK)
		&& isNE(hdivrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdivrevIO.getParams());
			dbError9100();
		}
		if (isNE(hdivrevIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(hdivrevIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(hdivrevIO.getTranno(),greversrec.tranno)) {
			hdivrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(hdivrevIO.getStatuz(),varcom.oK)) {
			if (isNE(hdivrevIO.getBatcbatch(),"00000")) {
				if (isEQ(hdivrevIO.getDivdStmtNo(),ZERO)) {
					hdivrevIO.setFunction(varcom.keeps);
					callHdivrevio6000();
					hdivrevIO.setFunction(varcom.delts);
					callHdivrevio6000();
				}
				else {
					writeHdiv6100();
				}
			}
		}
		hdivrevIO.setFunction(varcom.nextr);
	}

protected void setupHdisrev4000()
	{
		/*START*/
		hdisrevIO.setDataKey(SPACES);
		hdisrevIO.setChdrcoy(greversrec.chdrnum);
		hdisrevIO.setChdrnum(greversrec.chdrnum);
		hdisrevIO.setTranno(greversrec.tranno);
		hdisrevIO.setFunction(varcom.begn);
		hdisrevIO.setFormat(hdisrevrec);
		/*EXIT*/
	}

protected void maintainHdis5000()
	{
		start5010();
	}

protected void start5010()
	{
	//performance improvement --  atiwari23 
	hdisrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	
	
		SmartFileCode.execute(appVars, hdisrevIO);
		if (isNE(hdisrevIO.getStatuz(),varcom.oK)
		&& isNE(hdisrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hdisrevIO.getParams());
			dbError9100();
		}
		if (isNE(hdisrevIO.getChdrcoy(),greversrec.chdrcoy)
		|| isNE(hdisrevIO.getChdrnum(),greversrec.chdrnum)
		|| isNE(hdisrevIO.getTranno(),greversrec.tranno)) {
			hdisrevIO.setStatuz(varcom.endp);
		}
		if (isEQ(hdisrevIO.getStatuz(),varcom.oK)) {
			hdisIO.setParams(SPACES);
			hdisIO.setRrn(hdisrevIO.getRrn());
			hdisIO.setFormat(hdisrec);
			hdisIO.setFunction(varcom.readd);
			callHdisio6200();
			wsaaStmtNo.set(hdisIO.getDivdStmtNo());
			wsaaStmtDate.set(hdisIO.getDivdStmtDate());
			wsaaBalance.set(hdisIO.getBalAtStmtDate());
			hdisIO.setFunction(varcom.deltd);
			callHdisio6200();
			hdisIO.setFunction(varcom.begn);
			//performance improvement --  atiwari23 
			hdisIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
			
			SmartFileCode.execute(appVars, hdisIO);
			if (isNE(hdisIO.getStatuz(),varcom.oK)
			&& isNE(hdisIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(hdisIO.getParams());
				dbError9100();
			}
			if (isEQ(hdisIO.getStatuz(),varcom.endp)
			|| isNE(hdisIO.getChdrcoy(),hdisrevIO.getChdrcoy())
			|| isNE(hdisIO.getChdrnum(),hdisrevIO.getChdrnum())
			|| isNE(hdisIO.getLife(),hdisrevIO.getLife())
			|| isNE(hdisIO.getCoverage(),hdisrevIO.getCoverage())
			|| isNE(hdisIO.getRider(),hdisrevIO.getRider())
			|| isNE(hdisIO.getPlanSuffix(),hdisrevIO.getPlanSuffix())) {
				hdisrevIO.setStatuz(varcom.endp);
			}
			else {
				if (isNE(hdisIO.getValidflag(),"2")) {
					hdisrevIO.setStatuz(hl15);
					syserrrec.params.set(hdisrevIO.getParams());
					dbError9100();
				}
				hdisIO.setValidflag("1");
				hdisIO.setDivdStmtNo(wsaaStmtNo);
				hdisIO.setDivdStmtDate(wsaaStmtDate);
				hdisIO.setBalAtStmtDate(wsaaBalance);
				hdisIO.setFormat(hdisrec);
				hdisIO.setFunction(varcom.writd);
				callHdisio6200();
				hdisrevIO.setFunction(varcom.nextr);
			}
		}
	}

protected void callHdivrevio6000()
	{
		/*START*/
		SmartFileCode.execute(appVars, hdivrevIO);
		if (isNE(hdivrevIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdivrevIO.getParams());
			dbError9100();
		}
		/*EXIT*/
	}

protected void writeHdiv6100()
	{
		start6110();
	}

protected void start6110()
	{
		hdivIO.setDataArea(hdivrevIO.getDataArea());
		hdivIO.setChdrcoy(hdivrevIO.getChdrcoy());
		hdivIO.setChdrnum(hdivrevIO.getChdrnum());
		hdivIO.setLife(hdivrevIO.getLife());
		hdivIO.setCoverage(hdivrevIO.getCoverage());
		hdivIO.setRider(hdivrevIO.getRider());
		hdivIO.setPlanSuffix(hdivrevIO.getPlanSuffix());
		hdivIO.setPuAddNbr(hdivrevIO.getPuAddNbr());
		hdivIO.setEffdate(hdivrevIO.getEffdate());
		hdivIO.setJlife(hdivrevIO.getJlife());
		hdivIO.setCntcurr(hdivrevIO.getCntcurr());
		hdivIO.setDivdType(hdivrevIO.getDivdType());
		hdivIO.setDivdRate(hdivrevIO.getDivdRate());
		hdivIO.setDivdRtEffdt(hdivrevIO.getDivdRtEffdt());
		hdivIO.setZdivopt(hdivrevIO.getZdivopt());
		hdivIO.setZcshdivmth(hdivrevIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(hdivrevIO.getDivdOptprocTranno());
		hdivIO.setDivdCapTranno(hdivrevIO.getDivdCapTranno());
		hdivIO.setDivdIntCapDate(hdivrevIO.getDivdIntCapDate());
		hdivIO.setTranno(greversrec.newTranno);
		hdivIO.setDivdStmtNo(ZERO);
		setPrecision(hdivIO.getDivdAmount(), 2);
		hdivIO.setDivdAmount(mult(hdivIO.getDivdAmount(),-1));
		hdivIO.setDivdAllocDate(greversrec.effdate);
		wsaaBatckey.batcFileKey.set(greversrec.batckey);
		hdivIO.setBatccoy(wsaaBatckey.batcBatccoy);
		hdivIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		hdivIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		hdivIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		hdivIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		hdivIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		hdivIO.setFormat(hdivrec);
		hdivIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdivIO.getParams());
			dbError9100();
		}
	}

protected void callHdisio6200()
	{
		/*START*/
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hdisIO.getParams());
			dbError9100();
		}
		/*EXIT*/
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		greversrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
