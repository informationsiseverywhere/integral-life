package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:44
 * Description:
 * Copybook name: REGPLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Regplnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData regplnbFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData regplnbKey = new FixedLengthStringData(256).isAPartOf(regplnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData regplnbChdrcoy = new FixedLengthStringData(1).isAPartOf(regplnbKey, 0);
  	public FixedLengthStringData regplnbChdrnum = new FixedLengthStringData(8).isAPartOf(regplnbKey, 1);
  	public FixedLengthStringData regplnbLife = new FixedLengthStringData(2).isAPartOf(regplnbKey, 9);
  	public FixedLengthStringData regplnbCoverage = new FixedLengthStringData(2).isAPartOf(regplnbKey, 11);
  	public FixedLengthStringData regplnbRider = new FixedLengthStringData(2).isAPartOf(regplnbKey, 13);
  	public PackedDecimalData regplnbPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(regplnbKey, 15);
  	public PackedDecimalData regplnbRgpynum = new PackedDecimalData(5, 0).isAPartOf(regplnbKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(235).isAPartOf(regplnbKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(regplnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		regplnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}