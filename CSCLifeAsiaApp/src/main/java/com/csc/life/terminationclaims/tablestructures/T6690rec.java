package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:12
 * Description:
 * Copybook name: T6690REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6690rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6690Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData rgpytype = new FixedLengthStringData(2).isAPartOf(t6690Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(t6690Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6690Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6690Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}