package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6680
 * @version 1.0 generated on 30/08/09 06:57
 * @author Quipoz
 */
public class S6680ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1488);
	public FixedLengthStringData dataFields = new FixedLengthStringData(640).isAPartOf(dataArea, 0);
	public ZonedDecimalData anvdate = DD.anvdate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData aprvdate = DD.aprvdate.copyToZonedDecimal().isAPartOf(dataFields,8);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,16);
	public ZonedDecimalData cancelDate = DD.canceldate.copyToZonedDecimal().isAPartOf(dataFields,24);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData claimcur = DD.claimcur.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData claimevd = DD.claimevd.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData clmcurdsc = DD.clmcurdsc.copy().isAPartOf(dataFields,61);
	public FixedLengthStringData clmdesc = DD.clmdesc.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData cltype = DD.cltype.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,103);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,114);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(dataFields,118);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,126);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,156);
	public FixedLengthStringData currds = DD.currds.copy().isAPartOf(dataFields,159);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,169);
	public FixedLengthStringData destkey = DD.destkey.copy().isAPartOf(dataFields,170);
	public ZonedDecimalData finalPaydate = DD.epaydate.copyToZonedDecimal().isAPartOf(dataFields,180);
	public ZonedDecimalData firstPaydate = DD.fpaydate.copyToZonedDecimal().isAPartOf(dataFields,188);
	public FixedLengthStringData frqdesc = DD.frqdesc.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,206);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,207);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,215);
	public ZonedDecimalData lastPaydate = DD.lpaydate.copyToZonedDecimal().isAPartOf(dataFields,262);
	public ZonedDecimalData nextPaydate = DD.npaydate.copyToZonedDecimal().isAPartOf(dataFields,270);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,278);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,286);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(dataFields,333);
	public FixedLengthStringData payenme = DD.payenme.copy().isAPartOf(dataFields,341);
	public ZonedDecimalData prcnt = DD.prcnt.copyToZonedDecimal().isAPartOf(dataFields,388);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,393);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,403);
	public ZonedDecimalData pymt = DD.pymt.copyToZonedDecimal().isAPartOf(dataFields,411);
	public FixedLengthStringData regpayfreq = DD.regpayfreq.copy().isAPartOf(dataFields,428);
	public ZonedDecimalData revdte = DD.revdte.copyToZonedDecimal().isAPartOf(dataFields,430);
	public FixedLengthStringData rgpymop = DD.rgpymop.copy().isAPartOf(dataFields,438);
	public ZonedDecimalData rgpynum = DD.rgpynum.copyToZonedDecimal().isAPartOf(dataFields,439);
	public FixedLengthStringData rgpyshort = DD.rgpyshort.copy().isAPartOf(dataFields,444);
	public FixedLengthStringData rgpystat = DD.rgpystat.copy().isAPartOf(dataFields,454);
	public FixedLengthStringData rgpytypesd = DD.rgpytypesd.copy().isAPartOf(dataFields,456);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,466);
	public FixedLengthStringData statdsc = DD.statdsc.copy().isAPartOf(dataFields,476);
	public ZonedDecimalData totamnt = DD.totamnt.copyToZonedDecimal().isAPartOf(dataFields,486);
	public ZonedDecimalData zrsumin = DD.zrsumin.copyToZonedDecimal().isAPartOf(dataFields,504);
	// CML009
	public ZonedDecimalData adjustamt = DD.adjamt.copyToZonedDecimal().isAPartOf(dataFields,517);
	public ZonedDecimalData netclaimamt = DD.netamt.copyToZonedDecimal().isAPartOf(dataFields,529);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,546);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields, 550);
	public ZonedDecimalData pymtAdj = DD.pymt.copyToZonedDecimal().isAPartOf(dataFields, 600);
	// clm008
	public FixedLengthStringData claimnumber = DD.claimnumber.copy().isAPartOf(dataFields,617); 
	public FixedLengthStringData aacct = DD.aacct.copy().isAPartOf(dataFields,626); 
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(212).isAPartOf(dataArea, 640);
	public FixedLengthStringData anvdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData aprvdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData canceldateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData claimcurErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData claimevdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData clmcurdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData clmdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData currdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData destkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData epaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData fpaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData frqdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData lpaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData npaydateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData paycltErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData payenmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData prcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData pymtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData regpayfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData revdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData rgpymopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData rgpynumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData rgpyshortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData rgpystatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData rgpytypesdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData statdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData totamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData zrsuminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	// CML009
	public FixedLengthStringData adjustamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData netClaimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData pymtAdjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	// clm008
	public FixedLengthStringData claimnumberErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData aacctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(636).isAPartOf(dataArea, 852);
	public FixedLengthStringData[] anvdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] aprvdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] canceldateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] claimcurOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] claimevdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] clmcurdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] clmdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] currdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] destkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] epaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] fpaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] frqdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] lpaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] npaydateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] paycltOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] payenmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] prcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] pymtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] regpayfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] revdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] rgpymopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] rgpynumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] rgpyshortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] rgpystatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] rgpytypesdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] statdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] totamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] zrsuminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	// CML009
	public FixedLengthStringData[] adjustamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] netclaimamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1,outputIndicators, 588);
	public FixedLengthStringData[] pymtAdjOut = FLSArrayPartOfStructure(12, 1,outputIndicators, 600);
	// clm008
	public FixedLengthStringData[] claimnumberOut = FLSArrayPartOfStructure(12, 1,outputIndicators, 612);
	public FixedLengthStringData[] aacctOut = FLSArrayPartOfStructure(12, 1,outputIndicators, 624);
	
	public FixedLengthStringData cmoth008flag = new FixedLengthStringData(1); //CLM008
		
		
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData anvdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData aprvdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData cancelDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData finalPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData firstPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData lastPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextPaydateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData revdteDisp = new FixedLengthStringData(10);

	public LongData S6680screenWritten = new LongData(0);
	public LongData S6680protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6680ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(canceldateOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupflgOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		
		
		// CML009
		fieldIndMap.put(reasoncdOut, new String[] { "07", "49", "-07", null,
				null, null, null, null, null, null, null, null });
		fieldIndMap.put(netclaimamtOut, new String[] { "09", "50", "-09", null,
				null, null, null, null, null, null, null, null });
		fieldIndMap.put(adjustamtOut, new String[] { "08", "69", "69", null,
				null, null, null, null, null, null, null, null });
		fieldIndMap.put(resndescOut, new String[] { "10", "70", "70", null,
				null, null, null, null, null, null, null, null });
		fieldIndMap.put(pymtAdjOut, new String[] { "44", "67", "-44", null,
				null, null, null, null, null, null, null, null });
		fieldIndMap.put(zrsuminOut,new String[] {null,"01", null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pymtOut,new String[] {null,"02", null,null, null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] { chdrnum, cnttype, ctypedes, lifcnum,
				linsname, cownnum, ownername, occdate, rstate, pstate, ptdate,
				btdate, currcd, currds, rgpynum, rgpytypesd, rgpystat, statdsc,
				cltype, clmdesc, claimevd, rgpymop, rgpyshort, regpayfreq,
				frqdesc, payclt, payenme, prcnt, destkey, pymt, totamnt,
				claimcur, clmcurdsc, aprvdate, crtdate, revdte, firstPaydate,
				lastPaydate, nextPaydate, anvdate, finalPaydate, cancelDate,
				fupflg, ddind, crtable, zrsumin, adjustamt, netclaimamt,
				reasoncd, resndesc, pymtAdj,claimnumber, aacct };
		screenOutFields = new BaseData[][] { chdrnumOut, cnttypeOut,
				ctypedesOut, lifcnumOut, linsnameOut, cownnumOut, ownernameOut,
				occdateOut, rstateOut, pstateOut, ptdateOut, btdateOut,
				currcdOut, currdsOut, rgpynumOut, rgpytypesdOut, rgpystatOut,
				statdscOut, cltypeOut, clmdescOut, claimevdOut, rgpymopOut,
				rgpyshortOut, regpayfreqOut, frqdescOut, paycltOut, payenmeOut,
				prcntOut, destkeyOut, pymtOut, totamntOut, claimcurOut,
				clmcurdscOut, aprvdateOut, crtdateOut, revdteOut, fpaydateOut,
				lpaydateOut, npaydateOut, anvdateOut, epaydateOut,
				canceldateOut, fupflgOut, ddindOut, crtableOut, zrsuminOut,
				adjustamtOut, netclaimamtOut, reasoncdOut, resndescOut, pymtOut,claimnumberOut, aacctOut };
		screenErrFields = new BaseData[] { chdrnumErr, cnttypeErr, ctypedesErr,
				lifcnumErr, linsnameErr, cownnumErr, ownernameErr, occdateErr,
				rstateErr, pstateErr, ptdateErr, btdateErr, currcdErr,
				currdsErr, rgpynumErr, rgpytypesdErr, rgpystatErr, statdscErr,
				cltypeErr, clmdescErr, claimevdErr, rgpymopErr, rgpyshortErr,
				regpayfreqErr, frqdescErr, paycltErr, payenmeErr, prcntErr,
				destkeyErr, pymtErr, totamntErr, claimcurErr, clmcurdscErr,
				aprvdateErr, crtdateErr, revdteErr, fpaydateErr, lpaydateErr,
				npaydateErr, anvdateErr, epaydateErr, canceldateErr, fupflgErr,
				ddindErr, crtableErr, zrsuminErr, adjustamtErr, netClaimErr,
				reasoncdErr, resndescErr, pymtErr,claimnumberErr, aacctErr };
		screenDateFields = new BaseData[] {occdate, ptdate, btdate, aprvdate, crtdate, revdte, firstPaydate, lastPaydate, nextPaydate, anvdate, finalPaydate, cancelDate};
		screenDateErrFields = new BaseData[] {occdateErr, ptdateErr, btdateErr, aprvdateErr, crtdateErr, revdteErr, fpaydateErr, lpaydateErr, npaydateErr, anvdateErr, epaydateErr, canceldateErr};
		screenDateDispFields = new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, aprvdateDisp, crtdateDisp, revdteDisp, firstPaydateDisp, lastPaydateDisp, nextPaydateDisp, anvdateDisp, finalPaydateDisp, cancelDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6680screen.class;
		protectRecord = S6680protect.class;
	}

}
