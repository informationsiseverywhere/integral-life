/*
 * File: Ph520.java
 * Date: 30 August 2009 1:05:19
 * Author: Quipoz Limited
 * 
 * Class transformed from PH520.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* Traditional Part Processing End Processing.
*
* This program will perform the traditional part surrender by
* calling the subroutines from T6598, which will in turn write
* the necessary accounting entries.  There is no screen attached
* to this program.
* (This program is a cross between P5084AT, full surrender AT,
*  and P5022AT, bonus surrender AT).
*
* 1000-Initialise.
* ----------------
* - retrieve CHDRMJA.
*
* 2000-screen-edit section.
* -------------------------
* - no processing required.
*
* 3000-update section.
* --------------------
* - read and hold surrender header, SURHCLM.
* - loop through SURDCLM until end-of-file or change in contract:
*     .  read T5687 using SURDCLM-CRTABLE as key
*     .  read T6598 with T5687-SV-METHOD as the key
*     .  set up linkage for processing routine in T6598
*     . call T6598-PROCESPROG
* - delete SURHCLM record
*
* 4000-where-next section.
* ------------------------
* - add 1 to wssp-program-ptr.
*
*****************************************************************
* </pre>
*/
public class Ph520 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH520");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
		/* ERRORS */
	private String f259 = "F259";
	private String g713 = "G713";
	private String h053 = "H053";
		/* TABLES */
	private String t5687 = "T5687";
	private String t6598 = "T6598";
		/* FORMATS */
	private String chdrmjarec = "CHDRMJAREC";
	private String itemrec = "ITEMREC";
	private String surdclmrec = "SURDCLMREC";
	private String surhclmrec = "SURHCLMREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Contract Header File - Major Alts*/
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Surrender Claims Detail Record*/
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
		/*Full Surrender header record*/
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private Surpcpy surpcpy = new Surpcpy();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		exit3090, 
		nextSurdclm3180, 
		exit3190, 
		exit3290
	}

	public Ph520() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		chdrmjaIO.setFunction(varcom.retrv);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		wsaaTranno.set(ZERO);
		wsaaBatckey.set(wsspcomn.batchkey);
		varcom.vrcmTranid.set(wsspcomn.tranid);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		surhclmIO.setDataKey(SPACES);
		surhclmIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		surhclmIO.setChdrnum(chdrmjaIO.getChdrnum());
		compute(wsaaTranno, 0).set(add(1,chdrmjaIO.getTranno()));
		surhclmIO.setTranno(wsaaTranno);
		surhclmIO.setPlanSuffix(0);
		surhclmIO.setFormat(surhclmrec);
		surhclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		surhclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surhclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(),varcom.oK)
		&& isNE(surhclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		if (isEQ(surhclmIO.getStatuz(),varcom.endp)
		|| isNE(surhclmIO.getChdrcoy(),chdrmjaIO.getChdrcoy())
		|| isNE(surhclmIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isNE(surhclmIO.getTranno(),wsaaTranno)) {
			syserrrec.statuz.set(f259);
			surhclmIO.setStatuz(f259);
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		surhclmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
		surdclmIO.setDataKey(SPACES);
		surdclmIO.setChdrcoy(surhclmIO.getChdrcoy());
		surdclmIO.setChdrnum(surhclmIO.getChdrnum());
		surdclmIO.setTranno(surhclmIO.getTranno());
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setFormat(surdclmrec);
		surdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		while ( !(isEQ(surdclmIO.getStatuz(),varcom.endp))) {
			callSurdclm3100();
		}
		
		surdclmIO.setParams(SPACES);
		surdclmIO.setChdrcoy(surhclmIO.getChdrcoy());
		surdclmIO.setChdrnum(surhclmIO.getChdrnum());
		surdclmIO.setTranno(surhclmIO.getTranno());
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setFormat(surdclmrec);
		surdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
		while ( !(isEQ(surdclmIO.getStatuz(),varcom.endp))) {
			deleteSurdclm3200();
		}
		
		surhclmIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(surhclmIO.getParams());
			fatalError600();
		}
	}

protected void callSurdclm3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					surdclm3100();
				}
				case nextSurdclm3180: {
					nextSurdclm3180();
				}
				case exit3190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void surdclm3100()
	{
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)
		&& isNE(surdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			fatalError600();
		}
		if (isEQ(surdclmIO.getStatuz(),varcom.endp)
		|| isNE(surdclmIO.getChdrcoy(),surhclmIO.getChdrcoy())
		|| isNE(surdclmIO.getChdrnum(),surhclmIO.getChdrnum())
		|| isNE(surdclmIO.getTranno(),surhclmIO.getTranno())) {
			surdclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3190);
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(surdclmIO.getChdrcoy());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(surdclmIO.getCrtable());
		itdmIO.setItmfrm(chdrmjaIO.getOccdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),surdclmIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),surdclmIO.getCrtable())) {
			syserrrec.statuz.set(h053);
			itdmIO.setStatuz(h053);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (isEQ(t5687rec.svMethod,SPACES)) {
			itdmIO.setStatuz(h053);
			syserrrec.statuz.set(h053);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(surdclmIO.getChdrcoy());
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(g713);
			itemIO.setStatuz(g713);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6598rec.t6598Rec.set(itemIO.getGenarea());
		if (isEQ(t6598rec.procesprog,SPACES)) {
			syserrrec.statuz.set(g713);
			itemIO.setStatuz(g713);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		initialize(surpcpy.surrenderRec);
		surpcpy.chdrcoy.set(surdclmIO.getChdrcoy());
		surpcpy.chdrnum.set(surdclmIO.getChdrnum());
		surpcpy.life.set(surdclmIO.getLife());
		surpcpy.jlife.set(surdclmIO.getJlife());
		surpcpy.planSuffix.set(surdclmIO.getPlanSuffix());
		surpcpy.fund.set(surdclmIO.getVirtualFund());
		surpcpy.coverage.set(surdclmIO.getCoverage());
		surpcpy.rider.set(surdclmIO.getRider());
		surpcpy.crtable.set(surdclmIO.getCrtable());
		surpcpy.effdate.set(surhclmIO.getEffdate());
		surpcpy.estimatedVal.set(surdclmIO.getEstMatValue());
		surpcpy.actualVal.set(surdclmIO.getActvalue());
		if (isEQ(surhclmIO.getCurrcd(),SPACES)) {
			surpcpy.currcode.set(surdclmIO.getCurrcd());
			surpcpy.cntcurr.set(chdrmjaIO.getCntcurr());
		}
		else {
			surpcpy.currcode.set(surhclmIO.getCurrcd());
			surpcpy.cntcurr.set(surdclmIO.getCurrcd());
		}
		surpcpy.cnttype.set(surhclmIO.getCnttype());
		surpcpy.type.set("P");
		surpcpy.status.set(varcom.oK);
		surpcpy.batckey.set(wsaaBatckey.batcFileKey);
		surpcpy.tranno.set(surdclmIO.getTranno());
		surpcpy.termid.set(varcom.vrcmTermid);
		surpcpy.date_var.set(varcom.vrcmDate);
		surpcpy.time.set(varcom.vrcmTime);
		surpcpy.user.set(varcom.vrcmUser);
		surpcpy.language.set(wsspcomn.language);
		callProgram(t6598rec.procesprog, surpcpy.surrenderRec);
		if (isEQ(surpcpy.status,varcom.endp)) {
			goTo(GotoLabel.nextSurdclm3180);
		}
		if (isNE(surpcpy.status,varcom.oK)) {
			syserrrec.statuz.set(surpcpy.status);
			syserrrec.params.set(surpcpy.surrenderRec);
			fatalError600();
		}
	}

protected void nextSurdclm3180()
	{
		surdclmIO.setFunction(varcom.nextr);
	}

protected void deleteSurdclm3200()
	{
		try {
			surdclm13100();
		}
		catch (GOTOException e){
		}
	}

protected void surdclm13100()
	{
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)
		&& isNE(surdclmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(surdclmIO.getParams());
			fatalError600();
		}
		if (isEQ(surdclmIO.getStatuz(),varcom.endp)
		|| isNE(surdclmIO.getChdrcoy(),surhclmIO.getChdrcoy())
		|| isNE(surdclmIO.getChdrnum(),surhclmIO.getChdrnum())
		|| isNE(surdclmIO.getTranno(),surhclmIO.getTranno())) {
			surdclmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3290);
		}
		surdclmIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(surdclmIO.getParams());
			fatalError600();
		}
		surdclmIO.setFunction(varcom.nextr);
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
