
/*
 * File: P5220.java
 * Date: 30 August 2009 0:18:54
 * Author: Quipoz Limited
 * 
 * Class transformed from P5220.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.dataaccess.AnntlnbTableDAM;
import com.csc.life.terminationclaims.screens.S5220ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* OVERVIEW
* This program is part of the 9405 Annuities  Development.    It
* is  called  from  the  existing  New Business Transaction if a
* user is creating,  modifying  or  enquiring  upon  an  Annuity
* coverage  on a contract at proposal stage.  The processing for
* this  program  is  initiated   by   a   selection   field   on
* the Regular Benefit Generic Screen, P5125.
*
* This  program  allows  the  user  to  either create, modify or
* enquire upon  Annuity  Details  data  which  is  held  on  the
* Annuity  Temporary  File,  ANNT.  If  the user is enquiring on
* Annuities data, the Annuity Details Screen is  displayed  with
* all  fields  protected otherwise all annuity detail fields are
* amendable and the screen is  redisplayed  until  it  is  error
* free or the user exits from the transaction.
*
* When  the  screen  is displayed for the first time a number of
* the fields default from the  values  set  up  on  the  Annuity
* Component  Edit  Rules  Table T6625.  Once the ANNT record has
* been created the information  to  display  on  the  screen  is
* retrieved  from this file and if any changes are made the ANNT
* record should be changed to reflect them.
*
* If a client number is entered in the nominated life  field  it
* must  be    the  client number of either the life or the joint
* life for the component.
*
*****************************************************************
* </pre>
*/
public class P5220 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5220");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaRegtImpact = "";
		/* ERRORS */
	protected static final String e925 = "E925";
	private static final String f045 = "F045";
	private static final String f046 = "F046";
	private static final String f047 = "F047";
	private static final String f048 = "F048";
	private static final String f049 = "F049";
	private static final String f064 = "F064";
	private static final String f090 = "F090";
	private static final String h347 = "H347";
	private static final String h147 = "H147";
		/* TABLES */
	protected static final String t6625 = "T6625";
		/* FORMATS */
	private static final String anntlnbrec = "ANNTLNBREC";
	private static final String cltsrec = "CLTSREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private static final String itdmrec = "ITEMREC";
	protected AnntlnbTableDAM anntlnbIO = new AnntlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private T6625rec t6625rec = new T6625rec();
	private Wssplife wssplife = new Wssplife();
	private S5220ScreenVars sv = getLScreenVars(); // getScreenVars(
													// S5220ScreenVars.class);

	protected S5220ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5220ScreenVars.class);
	}

	public P5220() {
		super();
		screenVars = sv;
		new ScreenModel("S5220", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void largename()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaRegtImpact = "N";
		covtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(covtlnbIO.getChdrnum());
		sv.life.set(covtlnbIO.getLife());
		sv.coverage.set(covtlnbIO.getCoverage());
		sv.rider.set(covtlnbIO.getRider());
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife(ZERO);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("01");
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
		}
		else {
			sv.jlifcnum.set(lifelnbIO.getLifcnum());
			cltsIO.setFunction(varcom.readr);
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntpfx("CN");
			cltsIO.setClntnum(lifelnbIO.getLifcnum());
			cltsIO.setFormat(cltsrec);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
		anntlnbIO.setFunction(varcom.readr);
		anntlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		anntlnbIO.setChdrnum(covtlnbIO.getChdrnum());
		anntlnbIO.setLife(covtlnbIO.getLife());
		anntlnbIO.setCoverage(covtlnbIO.getCoverage());
		anntlnbIO.setRider(covtlnbIO.getRider());
		anntlnbIO.setSeqnbr(covtlnbIO.getSeqnbr());
		anntlnbIO.setFormat(anntlnbrec);
		SmartFileCode.execute(appVars, anntlnbIO);
		if (isNE(anntlnbIO.getStatuz(),varcom.oK)
		&& isNE(anntlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(anntlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.flag,"I")
		&& (isEQ(anntlnbIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(anntlnbIO.getParams());
			syserrrec.statuz.set(anntlnbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(anntlnbIO.getStatuz(),varcom.mrnf)) {
			t6625Read1100();
			sv.freqann.set(t6625rec.freqann);
			sv.guarperd.set(t6625rec.guarperd);
			sv.dthpercn.set(t6625rec.dthpercn);
			sv.dthperco.set(t6625rec.dthperco);
			sv.arrears.set(SPACES);
			sv.advance.set(SPACES);
			sv.ppind.set(SPACES);
			sv.withprop.set(SPACES);
			sv.withoprop.set(SPACES);
			sv.nomlife.set(SPACES);
			sv.intanny.set(ZERO);
			sv.capcont.set(ZERO);
		}
		else {
			sv.freqann.set(anntlnbIO.getFreqann());
			sv.guarperd.set(anntlnbIO.getGuarperd());
			sv.dthpercn.set(anntlnbIO.getDthpercn());
			sv.dthperco.set(anntlnbIO.getDthperco());
			sv.arrears.set(anntlnbIO.getArrears());
			sv.advance.set(anntlnbIO.getAdvance());
			sv.ppind.set(anntlnbIO.getPpind());
			sv.withprop.set(anntlnbIO.getWithprop());
			sv.withoprop.set(anntlnbIO.getWithoprop());
			sv.nomlife.set(anntlnbIO.getNomlife());
			sv.intanny.set(anntlnbIO.getIntanny());
			sv.capcont.set(anntlnbIO.getCapcont());
		}
	}

protected void t6625Read1100()
	{
		t6625Start1110();
	}

protected void t6625Start1110()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(covtlnbIO.getEffdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");

		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t6625)
		|| isNE(covtlnbIO.getCrtable(),itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(covtlnbIO.getCrtable());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h147);
			fatalError600();
		}
		else {
			t6625rec.t6625Rec.set(itdmIO.getGenarea());
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    If the user is enquiring about annuities data - protect the  */
		/*    screen:                                                      */
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"C")
		|| isEQ(wsspcomn.flag, "M")
		|| isEQ(wsspcomn.flag, "P")) {
			validateReqd2100();
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateReqd2100()
	{
		valStart2110();
	}

protected void valStart2110()
	{
		if (isEQ(sv.freqann,SPACES)) {
			sv.freqannErr.set(e925);
		}
		if (isEQ(sv.advance,SPACES)
		&& isEQ(sv.arrears,SPACES)) {
			sv.arrearsErr.set(f045);
			sv.advanceErr.set(f045);
		}
		else {
			if (isNE(sv.advance,SPACES)
			&& isNE(sv.arrears,SPACES)) {
				sv.arrearsErr.set(f047);
				sv.advanceErr.set(f047);
			}
		}
		if (isEQ(sv.withprop,SPACES)
		&& isEQ(sv.withoprop,SPACES)) {
			sv.withpropErr.set(f046);
			sv.withopropErr.set(f046);
		}
		else {
			if (isNE(sv.withprop,SPACES)
			&& isNE(sv.withoprop,SPACES)) {
				sv.withpropErr.set(f047);
				sv.withopropErr.set(f047);
			}
		}
		if (isNE(sv.advance,SPACES)
		&& isNE(sv.withprop,SPACES)) {
			sv.advanceErr.set(f048);
			sv.withpropErr.set(f048);
		}
		if (isGT(sv.capcont,wssplife.bigAmt)) {
			sv.capcontErr.set(f090);
		}
		if (isNE(sv.nomlife,SPACES)) {
			begnLifelnb2200();
		}
		if (isGT(sv.dthpercn,100)
		|| isGT(sv.dthperco,100)) {
			if (isGT(sv.dthpercn,100)) {
				sv.dthpercnErr.set(h347);
			}
			else {
				if (isGT(sv.dthperco,100)) {
					sv.dthpercoErr.set(h347);
				}
			}
		}
		if ((isGT(sv.dthpercn,0)
		|| isGT(sv.dthperco,0))
		&& isEQ(sv.nomlife,SPACES)) {
			sv.nomlifeErr.set(f064);
		}
	}

protected void begnLifelnb2200()
	{
		begnStart2210();
	}

protected void begnStart2210()
	{
		lifelnbIO.setFunction(varcom.begn);
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife(ZERO);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		while ( !(isEQ(lifelnbIO.getLifcnum(),sv.nomlife)
		|| isEQ(lifelnbIO.getStatuz(),varcom.endp)
		|| isNE(lifelnbIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(),covtlnbIO.getChdrnum())
		|| isNE(lifelnbIO.getLife(),covtlnbIO.getLife()))) {
			nextrLifelnb2300();
		}
		
		if (isEQ(lifelnbIO.getStatuz(),varcom.endp)
		|| isNE(lifelnbIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(),covtlnbIO.getChdrnum())
		|| isNE(lifelnbIO.getLife(),covtlnbIO.getLife())) {
			sv.nomlifeErr.set(f049);
		}
	}

protected void nextrLifelnb2300()
	{
		/*NEXTR-START*/
		lifelnbIO.setFunction(varcom.nextr);
		lifelnbIO.setFormat(lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.flag,"C")
		|| isEQ(wsspcomn.flag, "M")
		|| isEQ(wsspcomn.flag, "P")) {
			updateReqd3100();
		}
		if (isEQ(wsaaRegtImpact,"Y")) {
			wssplife.fuptype.set("Y");
		}
		/*EXIT*/
	}

protected void updateReqd3100()
	{
		updateStart3110();
	}

protected void updateStart3110()
	{
		anntlnbIO.setFunction(varcom.readh);
		anntlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		anntlnbIO.setChdrnum(covtlnbIO.getChdrnum());
		anntlnbIO.setLife(covtlnbIO.getLife());
		anntlnbIO.setCoverage(covtlnbIO.getCoverage());
		anntlnbIO.setRider(covtlnbIO.getRider());
		anntlnbIO.setSeqnbr(covtlnbIO.getSeqnbr());
		anntlnbIO.setFormat(anntlnbrec);
		SmartFileCode.execute(appVars, anntlnbIO);
		if (isNE(anntlnbIO.getStatuz(),varcom.oK)
		&& isNE(anntlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(anntlnbIO.getParams());
			syserrrec.statuz.set(anntlnbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(anntlnbIO.getStatuz(),varcom.oK)) {
			checkRegtImpact3400();
		}
		anntlnbIO.setFreqann(sv.freqann);
		anntlnbIO.setGuarperd(sv.guarperd);
		anntlnbIO.setDthpercn(sv.dthpercn);
		anntlnbIO.setDthperco(sv.dthperco);
		anntlnbIO.setArrears(sv.arrears);
		anntlnbIO.setAdvance(sv.advance);
		anntlnbIO.setPpind(sv.ppind);
		anntlnbIO.setWithprop(sv.withprop);
		anntlnbIO.setWithoprop(sv.withoprop);
		anntlnbIO.setNomlife(sv.nomlife);
		anntlnbIO.setIntanny(sv.intanny);
		anntlnbIO.setCapcont(sv.capcont);
		anntlnbIO.setPlanSuffix(ZERO);
		if (isEQ(anntlnbIO.getStatuz(),varcom.mrnf)) {
			writrAnntlnb3200();
		}
		else {
			rewrtAnntlnb3300();
		}
	}

protected void writrAnntlnb3200()
	{
		/*WRITR-START*/
		anntlnbIO.setFunction(varcom.writr);
		anntlnbIO.setFormat(anntlnbrec);
		SmartFileCode.execute(appVars, anntlnbIO);
		if (isNE(anntlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(anntlnbIO.getParams());
			syserrrec.statuz.set(anntlnbIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void rewrtAnntlnb3300()
	{
		/*REWRT-START*/
		anntlnbIO.setFunction(varcom.rewrt);
		anntlnbIO.setFormat(anntlnbrec);
		SmartFileCode.execute(appVars, anntlnbIO);
		if (isNE(anntlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(anntlnbIO.getParams());
			syserrrec.statuz.set(anntlnbIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkRegtImpact3400()
	{
		/*CHECK-REGT-START*/
		if (isNE(sv.freqann,anntlnbIO.getFreqann())) {
			wsaaRegtImpact = "Y";
		}
		if (isNE(sv.guarperd,anntlnbIO.getGuarperd())) {
			wsaaRegtImpact = "Y";
		}
		if (isEQ(sv.arrears,SPACES)
		&& isNE(anntlnbIO.getArrears(),SPACES)) {
			wsaaRegtImpact = "Y";
		}
		if (isEQ(sv.advance,SPACES)
		&& isNE(anntlnbIO.getAdvance(),SPACES)) {
			wsaaRegtImpact = "Y";
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
