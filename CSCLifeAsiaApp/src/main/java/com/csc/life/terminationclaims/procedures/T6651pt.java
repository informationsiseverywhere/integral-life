/*
 * File: T6651pt.java
 * Date: 30 August 2009 2:29:02
 * Author: Quipoz Limited
 * 
 * Class transformed from T6651PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.terminationclaims.tablestructures.T6651rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6651.
*
*
*****************************************************************
* </pre>
*/
public class T6651pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Paid Up Processing Rules                   S6651");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(37);
	private FixedLengthStringData filler9 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 25, FILLER).init("Paid Up Fees");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(73);
	private FixedLengthStringData filler11 = new FixedLengthStringData(73).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Freq                Flat       %             Minimum      Maximum");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(73);
	private FixedLengthStringData filler12 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(67).isAPartOf(wsaaPrtLine006, 6, FILLER).init("-------------------------------------------------------------------");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(73);
	private FixedLengthStringData filler14 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  00 |");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 7).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 35).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(73);
	private FixedLengthStringData filler18 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 0, FILLER).init("  01 |");
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 7).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 35).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(73);
	private FixedLengthStringData filler22 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  02 |");
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 7).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 35).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(73);
	private FixedLengthStringData filler26 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 0, FILLER).init("  04 |");
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 7).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 35).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(73);
	private FixedLengthStringData filler30 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 0, FILLER).init("  12 |");
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 7).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 27).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 35).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 55).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(44);
	private FixedLengthStringData filler34 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler35 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine012, 5, FILLER).init("Adjust Initial Units (Y/N):");
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 43);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(44);
	private FixedLengthStringData filler36 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler37 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine013, 5, FILLER).init("Use Bid or Offer Price (B/O):");
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 43);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(44);
	private FixedLengthStringData filler38 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler39 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine014, 5, FILLER).init("Allow override of Sum Assured (Y/N):");
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 43);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6651rec t6651rec = new T6651rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6651pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6651rec.t6651Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t6651rec.puffamt02);
		fieldNo008.set(t6651rec.feepc02);
		fieldNo009.set(t6651rec.pufeemin02);
		fieldNo010.set(t6651rec.pufeemax02);
		fieldNo011.set(t6651rec.puffamt03);
		fieldNo012.set(t6651rec.feepc03);
		fieldNo013.set(t6651rec.pufeemin03);
		fieldNo014.set(t6651rec.pufeemax03);
		fieldNo015.set(t6651rec.puffamt04);
		fieldNo016.set(t6651rec.feepc04);
		fieldNo017.set(t6651rec.pufeemin04);
		fieldNo018.set(t6651rec.pufeemax04);
		fieldNo019.set(t6651rec.puffamt05);
		fieldNo020.set(t6651rec.feepc05);
		fieldNo021.set(t6651rec.pufeemin05);
		fieldNo022.set(t6651rec.pufeemax05);
		fieldNo023.set(t6651rec.puffamt06);
		fieldNo024.set(t6651rec.feepc06);
		fieldNo025.set(t6651rec.pufeemin06);
		fieldNo026.set(t6651rec.pufeemax06);
		fieldNo027.set(t6651rec.adjustiu);
		fieldNo028.set(t6651rec.bidoffer);
		fieldNo029.set(t6651rec.ovrsuma);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
