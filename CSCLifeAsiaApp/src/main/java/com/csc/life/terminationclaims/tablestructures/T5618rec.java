package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:25
 * Description:
 * Copybook name: T5618REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5618rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5618Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData insprms = new FixedLengthStringData(300).isAPartOf(t5618Rec, 0);
  	public ZonedDecimalData[] insprm = ZDArrayPartOfStructure(50, 6, 0, insprms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(300).isAPartOf(insprms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData insprm01 = new ZonedDecimalData(6, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData insprm02 = new ZonedDecimalData(6, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData insprm03 = new ZonedDecimalData(6, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData insprm04 = new ZonedDecimalData(6, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData insprm05 = new ZonedDecimalData(6, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData insprm06 = new ZonedDecimalData(6, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData insprm07 = new ZonedDecimalData(6, 0).isAPartOf(filler, 36);
  	public ZonedDecimalData insprm08 = new ZonedDecimalData(6, 0).isAPartOf(filler, 42);
  	public ZonedDecimalData insprm09 = new ZonedDecimalData(6, 0).isAPartOf(filler, 48);
  	public ZonedDecimalData insprm10 = new ZonedDecimalData(6, 0).isAPartOf(filler, 54);
  	public ZonedDecimalData insprm11 = new ZonedDecimalData(6, 0).isAPartOf(filler, 60);
  	public ZonedDecimalData insprm12 = new ZonedDecimalData(6, 0).isAPartOf(filler, 66);
  	public ZonedDecimalData insprm13 = new ZonedDecimalData(6, 0).isAPartOf(filler, 72);
  	public ZonedDecimalData insprm14 = new ZonedDecimalData(6, 0).isAPartOf(filler, 78);
  	public ZonedDecimalData insprm15 = new ZonedDecimalData(6, 0).isAPartOf(filler, 84);
  	public ZonedDecimalData insprm16 = new ZonedDecimalData(6, 0).isAPartOf(filler, 90);
  	public ZonedDecimalData insprm17 = new ZonedDecimalData(6, 0).isAPartOf(filler, 96);
  	public ZonedDecimalData insprm18 = new ZonedDecimalData(6, 0).isAPartOf(filler, 102);
  	public ZonedDecimalData insprm19 = new ZonedDecimalData(6, 0).isAPartOf(filler, 108);
  	public ZonedDecimalData insprm20 = new ZonedDecimalData(6, 0).isAPartOf(filler, 114);
  	public ZonedDecimalData insprm21 = new ZonedDecimalData(6, 0).isAPartOf(filler, 120);
  	public ZonedDecimalData insprm22 = new ZonedDecimalData(6, 0).isAPartOf(filler, 126);
  	public ZonedDecimalData insprm23 = new ZonedDecimalData(6, 0).isAPartOf(filler, 132);
  	public ZonedDecimalData insprm24 = new ZonedDecimalData(6, 0).isAPartOf(filler, 138);
  	public ZonedDecimalData insprm25 = new ZonedDecimalData(6, 0).isAPartOf(filler, 144);
  	public ZonedDecimalData insprm26 = new ZonedDecimalData(6, 0).isAPartOf(filler, 150);
  	public ZonedDecimalData insprm27 = new ZonedDecimalData(6, 0).isAPartOf(filler, 156);
  	public ZonedDecimalData insprm28 = new ZonedDecimalData(6, 0).isAPartOf(filler, 162);
  	public ZonedDecimalData insprm29 = new ZonedDecimalData(6, 0).isAPartOf(filler, 168);
  	public ZonedDecimalData insprm30 = new ZonedDecimalData(6, 0).isAPartOf(filler, 174);
  	public ZonedDecimalData insprm31 = new ZonedDecimalData(6, 0).isAPartOf(filler, 180);
  	public ZonedDecimalData insprm32 = new ZonedDecimalData(6, 0).isAPartOf(filler, 186);
  	public ZonedDecimalData insprm33 = new ZonedDecimalData(6, 0).isAPartOf(filler, 192);
  	public ZonedDecimalData insprm34 = new ZonedDecimalData(6, 0).isAPartOf(filler, 198);
  	public ZonedDecimalData insprm35 = new ZonedDecimalData(6, 0).isAPartOf(filler, 204);
  	public ZonedDecimalData insprm36 = new ZonedDecimalData(6, 0).isAPartOf(filler, 210);
  	public ZonedDecimalData insprm37 = new ZonedDecimalData(6, 0).isAPartOf(filler, 216);
  	public ZonedDecimalData insprm38 = new ZonedDecimalData(6, 0).isAPartOf(filler, 222);
  	public ZonedDecimalData insprm39 = new ZonedDecimalData(6, 0).isAPartOf(filler, 228);
  	public ZonedDecimalData insprm40 = new ZonedDecimalData(6, 0).isAPartOf(filler, 234);
  	public ZonedDecimalData insprm41 = new ZonedDecimalData(6, 0).isAPartOf(filler, 240);
  	public ZonedDecimalData insprm42 = new ZonedDecimalData(6, 0).isAPartOf(filler, 246);
  	public ZonedDecimalData insprm43 = new ZonedDecimalData(6, 0).isAPartOf(filler, 252);
  	public ZonedDecimalData insprm44 = new ZonedDecimalData(6, 0).isAPartOf(filler, 258);
  	public ZonedDecimalData insprm45 = new ZonedDecimalData(6, 0).isAPartOf(filler, 264);
  	public ZonedDecimalData insprm46 = new ZonedDecimalData(6, 0).isAPartOf(filler, 270);
  	public ZonedDecimalData insprm47 = new ZonedDecimalData(6, 0).isAPartOf(filler, 276);
  	public ZonedDecimalData insprm48 = new ZonedDecimalData(6, 0).isAPartOf(filler, 282);
  	public ZonedDecimalData insprm49 = new ZonedDecimalData(6, 0).isAPartOf(filler, 288);
  	public ZonedDecimalData insprm50 = new ZonedDecimalData(6, 0).isAPartOf(filler, 294);
  	public ZonedDecimalData unit = new ZonedDecimalData(9, 0).isAPartOf(t5618Rec, 300);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(191).isAPartOf(t5618Rec, 309, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5618Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5618Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}