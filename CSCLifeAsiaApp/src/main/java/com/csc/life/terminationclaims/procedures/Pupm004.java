/*
 * File: Pupm004.java
 * Date: 30 August 2009 2:02:52
 * Author: Quipoz Limited
 * 
 * Class transformed from PUPM004.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   This  subroutine  calculates  the  paid-up  Sum Assured as a
*   proportion of the original Sum Assured based on the original
*   term and the current term in force.
*
*
*   The Paid Up Sum Assured is defined as follows :
*
*         PUSA = Sum Assured * T / N
*
*   where :
*
*      -  Sum Assured is the Original Sum Assured from the
*         Coverage File
*      -  T is the premium term in force (up to the PAID TO DATE)
*      -  N is the original premium term
*
*   For this Policy/Component :
*
*      -  Read Coverage File using the actual plan suffix passed
*         in the linkage (PRESERV).
*      -  Calculate the Original Premium Term (N) as the
*         difference between Premium Cessation Date and Coverage/
*         Rider Risk Commencement date.
*      -  Calculate the Premium Term in Force (T) as the
*         difference between the Paid to Date and the Coverage/
*         Rider Risk Commencement Date.
*      -  Calculate the PUSA according to the formula given
*         above and pass back the value in OVRD-ACTUAL-VAL.
*
*
*****************************************************************
* </pre>
*/
public class Pupm004 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "PUPM004";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private FixedLengthStringData wsaaPlanSuffix = new FixedLengthStringData(4);
		/* WSAA-EXACT-VALUES */
	private ZonedDecimalData wsaaOrigTermN = new ZonedDecimalData(11, 5);
	protected ZonedDecimalData wsaaPremTermT = new ZonedDecimalData(11, 5);
	protected PackedDecimalData wsaaPusa = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaSumAssured = new PackedDecimalData(17, 2);

	private ZonedDecimalData wsaaPlanSwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator wholePlan = new Validator(wsaaPlanSwitch, "1");
	private Validator partPlan = new Validator(wsaaPlanSwitch, "2");
	private Validator summaryPartPlan = new Validator(wsaaPlanSwitch, "3");
		/* ERRORS */
	private String e792 = "E792";
	private String g344 = "G344";
	private String h053 = "H053";
		/* FORMATS */
	private String covrtrbrec = "COVRTRBREC";
	private String payrrec = "PAYRREC   ";
		/* TABLES */
	private String t5687 = "T5687";
		/*COVR layout for trad. reversionary bonus*/
	protected CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
	protected Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected Ovrduerec ovrduerec = new Ovrduerec();
		/*Payor Details Logical File*/
	protected PayrTableDAM payrIO = new PayrTableDAM();
	protected Syserrrec syserrrec = new Syserrrec();
	protected Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		seExit9090, 
		dbExit9190
	}

	public Pupm004() {
		super();
	}

public void mainline(Object... parmArray)
	{
		ovrduerec.ovrdueRec = convertAndSetParam(ovrduerec.ovrdueRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		/*MAIN*/
		initialise2000();
		calculatePusa3000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise2000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		if (isEQ(ovrduerec.planSuffix,ZERO)) {
			wsaaPlanSwitch.set(1);
		}
		else {
			if (isLTE(ovrduerec.planSuffix,ovrduerec.polsum)) {
				wsaaPlanSwitch.set(3);
			}
			else {
				wsaaPlanSwitch.set(2);
			}
		}
		readFiles2100();
		getSaDescription2400();
		/*EXIT*/
	}

protected void readFiles2100()
	{
		readCovrtrb2100();
		readPayr2110();
	}

protected void readCovrtrb2100()
	{
		covrtrbIO.setDataArea(SPACES);
		covrtrbIO.setChdrcoy(ovrduerec.chdrcoy);
		covrtrbIO.setChdrnum(ovrduerec.chdrnum);
		covrtrbIO.setLife(ovrduerec.life);
		covrtrbIO.setCoverage(ovrduerec.coverage);
		covrtrbIO.setRider(ovrduerec.rider);
		covrtrbIO.setPlanSuffix(ovrduerec.planSuffix);
		if (summaryPartPlan.isTrue()) {
			covrtrbIO.setPlanSuffix(0);
		}
		covrtrbIO.setFormat(covrtrbrec);
		covrtrbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)
		&& isNE(covrtrbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covrtrbIO.getParams());
			dbError9100();
		}
		if (isEQ(covrtrbIO.getStatuz(),varcom.mrnf)) {
			wsaaPlanSuffix.set(ovrduerec.planSuffix);
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(ovrduerec.chdrcoy.toString());
			stringVariable1.append(ovrduerec.chdrnum.toString());
			stringVariable1.append(ovrduerec.life.toString());
			stringVariable1.append(ovrduerec.coverage.toString());
			stringVariable1.append(ovrduerec.rider.toString());
			stringVariable1.append(wsaaPlanSuffix.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(g344);
			dbError9100();
		}
	}

protected void readPayr2110()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(ovrduerec.chdrcoy);
		payrIO.setChdrnum(ovrduerec.chdrnum);
		payrIO.setPayrseqno(ZERO);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		payrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)
		&& isNE(payrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			dbError9100();
		}
		if (isEQ(payrIO.getStatuz(),varcom.mrnf)
		|| isNE(payrIO.getChdrcoy(),ovrduerec.chdrcoy)
		|| isNE(payrIO.getChdrnum(),ovrduerec.chdrnum)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(ovrduerec.chdrcoy.toString());
			stringVariable1.append(ovrduerec.chdrnum.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			syserrrec.statuz.set(e792);
			dbError9100();
		}
	}

protected void getSaDescription2400()
	{
		start2400();
	}

protected void start2400()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ovrduerec.chdrcoy);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(ovrduerec.crtable);
		descIO.setLanguage(ovrduerec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			systemError9000();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(h053);
			syserrrec.params.set(descIO.getParams());
			systemError9000();
		}
		if (isEQ(descIO.getShortdesc(),SPACES)) {
			ovrduerec.description.fill("?");
		}
		else {
			ovrduerec.description.set(descIO.getShortdesc());
		}
	}

protected void calculatePusa3000()
	{
		calculateN3000();
		calculateT3010();
		calculatePusa3020();
	}

protected void calculateN3000()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(covrtrbIO.getPremCessDate());
		datcon3rec.frequency.set(payrIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		wsaaOrigTermN.set(datcon3rec.freqFactor);
	}

protected void calculateT3010()
	{
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon3rec.intDate2.set(ovrduerec.effdate);
		datcon3rec.frequency.set(payrIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError9000();
		}
		wsaaPremTermT.set(datcon3rec.freqFactor);
	}

protected void calculatePusa3020()
	{
		wsaaSumAssured.set(covrtrbIO.getSumins());
		if (summaryPartPlan.isTrue()) {
			compute(wsaaSumAssured, 3).setRounded(div(covrtrbIO.getSumins(),ovrduerec.polsum));
		}
		compute(wsaaPusa, 6).setRounded(mult(wsaaSumAssured,(div(wsaaPremTermT,wsaaOrigTermN))));
		ovrduerec.actualVal.set(wsaaPusa);
		ovrduerec.newSumins.set(wsaaPusa);
		/*EXIT*/
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		ovrduerec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		ovrduerec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
