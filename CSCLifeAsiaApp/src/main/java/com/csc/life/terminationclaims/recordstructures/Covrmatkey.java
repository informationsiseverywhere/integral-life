package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:23
 * Description:
 * Copybook name: COVRMATKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrmatkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrmatFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrmatKey = new FixedLengthStringData(64).isAPartOf(covrmatFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrmatChdrcoy = new FixedLengthStringData(1).isAPartOf(covrmatKey, 0);
  	public FixedLengthStringData covrmatChdrnum = new FixedLengthStringData(8).isAPartOf(covrmatKey, 1);
  	public PackedDecimalData covrmatPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrmatKey, 9);
  	public FixedLengthStringData covrmatLife = new FixedLengthStringData(2).isAPartOf(covrmatKey, 12);
  	public FixedLengthStringData covrmatCoverage = new FixedLengthStringData(2).isAPartOf(covrmatKey, 14);
  	public FixedLengthStringData covrmatRider = new FixedLengthStringData(2).isAPartOf(covrmatKey, 16);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrmatKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrmatFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrmatFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}