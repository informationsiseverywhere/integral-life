package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.InvspfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * 
 * @author hxu32
 * DAOImpl related table NOTIPF
 *
 */
public class InvspfDAOImpl extends BaseDAOImpl<Invspf> implements InvspfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(InvspfDAOImpl.class);

	@Override
	public boolean insertInvspf(Invspf invspf) {
		Timestamp ts = Timestamp.valueOf(invspf.getDatime());
		StringBuilder sb = new StringBuilder("INSERT INTO INVSPF(INVSCOY,NOTIFINUM,LIFENUM,CLAIMANT,RELATIONSHIP,INVESRESULT,USRPRF,JOBNM, DATIME, CLAIMNO) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?)");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
        boolean result = false;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, invspf.getInvscoy());
			ps.setString(2, invspf.getNotifinum());
			ps.setString(3, invspf.getLifenum());
			ps.setString(4, invspf.getClaimant());
			ps.setString(5, invspf.getRelationship());
			ps.setString(6, invspf.getInvestigationResult());
			ps.setString(7, getUsrprf());			    
			ps.setString(8, getJobnm());		
			ps.setTimestamp(9, ts);
			ps.setString(10,invspf.getClaimno());
			ps.executeUpdate();
			result = true;
		}catch(SQLException e) {
			LOGGER.error("insertInvspf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("insertInvspf()", ex); /* IJTI-1479 */
			throw new RuntimeException(ex);
		}finally {
			close(ps, null);			
		}
		
		return result;
	}



	@Override
	public int removeInvspfRecord(long uniqueNumber) {
		StringBuilder sb = new StringBuilder("DELETE FROM INVSPF WHERE UNIQUE_NUMBER = ?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		int executeUpdate;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setLong(1, uniqueNumber);
			executeUpdate = ps.executeUpdate();
		}catch(SQLException e) {
			LOGGER.error("deleteInvspf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("deleteInvspf()", ex); /* IJTI-1479 */
			throw new RuntimeException(ex);
		}finally {
			close(ps, null);			
		}
		return executeUpdate;
	}

	@Override
	public int updateInvspf(Invspf invspf) {
		Timestamp ts = Timestamp.valueOf(invspf.getDatime());
		StringBuilder sb = new StringBuilder("UPDATE INVSPF SET INVESRESULT=? , DATIME=? WHERE UNIQUE_NUMBER = ?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		int executeUpdate;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, invspf.getInvestigationResult());
			ps.setTimestamp(2, ts);
			ps.setLong(3, invspf.getUniqueNumber());
			executeUpdate = ps.executeUpdate();
		}catch(SQLException e) {
			LOGGER.error("updateInvspf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("updateInvspf()", ex); /* IJTI-1479 */
			throw new RuntimeException(ex);
		}finally {
			close(ps, null);			
		}
		return executeUpdate;
	}
	@Override
	public List<Invspf> getInvspfList(String clnncoy, String notifinum, String claimno) {
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER,NOTIFINUM,INVESRESULT,DATIME,USRPRF FROM INVSPF WHERE INVSCOY = ? AND (NOTIFINUM = ? OR CLAIMNO = ?) ORDER BY DATIME DESC");
		LOGGER.info(sb.toString());
		ResultSet sqlclntpf1rs = null;
	    PreparedStatement psClntSelect = getPrepareStatement(sb.toString());
	    List<Invspf> resultList = new ArrayList<Invspf>();
	    try {
	    psClntSelect.setString(1, clnncoy);
	    psClntSelect.setString(2, notifinum);
	    psClntSelect.setString(3, claimno);
	            sqlclntpf1rs = executeQuery(psClntSelect);
	            while (sqlclntpf1rs.next()) {
	            		Invspf invspf = new Invspf();
	            		invspf.setUniqueNumber(sqlclntpf1rs.getLong(1));
	            		invspf.setNotifinum(sqlclntpf1rs.getString(2));
	            		invspf.setInvestigationResult(sqlclntpf1rs.getString(3));
	            		invspf.setDatime(sqlclntpf1rs.getString(4));
	            		invspf.setUsrprf(sqlclntpf1rs.getString(5));
	            		resultList.add(invspf);
	            }
	        } catch (SQLException e) {
			LOGGER.error("getinvspfList()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        }catch(Exception ex){
			LOGGER.error("getinvspfList()", ex); /* IJTI-1479 */
				ex.printStackTrace();
			} finally {
	            close(psClntSelect, sqlclntpf1rs);
	        }
		return resultList;
	}
	
	@Override
	public int updateInvspfClaimno(String claimno, String notifinum) {
		StringBuilder sb = new StringBuilder("UPDATE INVSPF SET CLAIMNO = ?  WHERE NOTIFINUM=?");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		int executeUpdate;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, claimno.trim());
			ps.setString(2, notifinum.trim());
			executeUpdate = ps.executeUpdate();
		}catch(SQLException e) {
			LOGGER.error("updateInvspfClaimno()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}catch(Exception ex){
			LOGGER.error("updateInvspfClaimno()", ex); /* IJTI-1479 */	
			throw new RuntimeException(ex);
		}finally {
			close(ps, null);			
		}
		return executeUpdate;
	}

	//IBPLIFE-2338 Starts
	@Override
	public List<Invspf> getInvspfByNotifinum(String clnncoy, String notifinum) {
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER,NOTIFINUM,INVESRESULT,DATIME,USRPRF FROM INVSPF WHERE INVSCOY = ? AND NOTIFINUM = ? ORDER BY DATIME DESC");
		LOGGER.info(sb.toString());
		ResultSet sqlclntpf1rs = null;
	    PreparedStatement psClntSelect = getPrepareStatement(sb.toString());
	    List<Invspf> resultList = new ArrayList<Invspf>();
	    try {
		    psClntSelect.setString(1, clnncoy);
		    psClntSelect.setString(2, notifinum);
	            sqlclntpf1rs = executeQuery(psClntSelect);
	            while (sqlclntpf1rs.next()) {
	            		Invspf invspf = new Invspf();
	            		invspf.setUniqueNumber(sqlclntpf1rs.getLong(1));
	            		invspf.setNotifinum(sqlclntpf1rs.getString(2));
	            		invspf.setInvestigationResult(sqlclntpf1rs.getString(3));
	            		invspf.setDatime(sqlclntpf1rs.getString(4));
	            		invspf.setUsrprf(sqlclntpf1rs.getString(5));
	            		resultList.add(invspf);
	            }
	        } catch (SQLException e) {
	        	LOGGER.error("getinvspfList()", e);
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psClntSelect, sqlclntpf1rs);
	        }
		return resultList;
	}
	//IBPLIFE-2338 End

}
