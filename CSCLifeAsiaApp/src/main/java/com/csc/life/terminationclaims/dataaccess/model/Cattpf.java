package com.csc.life.terminationclaims.dataaccess.model;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Id;
import javax.persistence.Table;
@Table(name="Cattpf")
public class Cattpf {
	@Id
	private long unique_number;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private String claim;
	private BigDecimal clamamt;
	private String clamtyp;
	private String clamstat;
	private Integer tranno;
	private String trcode;
	private Integer dtofdeath;
	private String causeofdth;
	private Integer effdate;
	private String lifcnum;
	private String seqenum;
	private String usrprf;
	private String jobnm;
	private Date datime;
	private Integer doceffdate;
	private String flag;
	private Integer kpaydate;
	private Integer intbasedt;
	private String fulfilltrm;
	private String kariflag;
	private String intcalflag;
	private String validflag;
	private Integer condte;//ILJ-487

	public Cattpf() {
	}

	public Cattpf(long uniqueNumber) {
		this.unique_number = uniqueNumber;
	}

	public Cattpf(long uniqueNumber, String chdrcoy,
			String chdrnum, String life, String jlife,
			String coverage, String rider,
			String claim, BigDecimal clamamt, String clamtyp, String clamstat,
			Integer tranno, String trcode, Integer dtofdeath,
			String causeofdth, Integer effdate, String lifcnum, String seqenum,
			String usrprf, String jobnm, Date datime,
			Integer doceffdate,
			String flag, Integer kpaydate, Integer intbasedt,
			String fulfilltrm, String kariflag,
			String intcalflag, String validflag) {
		this.unique_number = uniqueNumber;
		this.chdrcoy = chdrcoy;
		this.chdrnum = chdrnum;
		this.validflag = validflag;
		this.life = life;
		this.jlife = jlife;
		this.coverage = coverage;
		this.rider = rider;
		this.claim = claim;
		this.clamamt = clamamt;
		this.clamtyp = clamtyp;
		this.clamstat = clamstat;
		this.tranno = tranno;
		this.trcode = trcode;
		this.dtofdeath = dtofdeath;
		this.causeofdth = causeofdth;
		this.effdate = effdate;
		this.lifcnum = lifcnum;
		this.seqenum = seqenum;
		this.usrprf = usrprf;
		this.jobnm = jobnm;
		this.datime = datime;
		this.doceffdate = doceffdate;
		this.flag = flag;
		this.kpaydate = kpaydate;
		this.intbasedt = intbasedt;
		this.fulfilltrm = fulfilltrm;
		this.kariflag = kariflag;
		this.intcalflag = intcalflag;
		this.validflag = validflag;
	}

	public long getUnique_number() {
		return unique_number;
	}

	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getLife() {
		return life;
	}

	public void setLife(String life) {
		this.life = life;
	}

	public String getJlife() {
		return jlife;
	}

	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	public String getClaim() {
		return claim;
	}

	public void setClaim(String claim) {
		this.claim = claim;
	}

	public BigDecimal getClamamt() {
		return clamamt;
	}

	public void setClamamt(BigDecimal clamamt) {
		this.clamamt = clamamt;
	}

	public String getClamtyp() {
		return clamtyp;
	}

	public void setClamtyp(String clamtyp) {
		this.clamtyp = clamtyp;
	}

	public String getClamstat() {
		return clamstat;
	}

	public void setClamstat(String clamstat) {
		this.clamstat = clamstat;
	}

	public Integer getTranno() {
		return tranno;
	}

	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}

	public String getTrcode() {
		return trcode;
	}

	public void setTrcode(String trcode) {
		this.trcode = trcode;
	}

	public Integer getDtofdeath() {
		return dtofdeath;
	}

	public void setDtofdeath(Integer dtofdeath) {
		this.dtofdeath = dtofdeath;
	}

	public String getCauseofdth() {
		return causeofdth;
	}

	public void setCauseofdth(String causeofdth) {
		this.causeofdth = causeofdth;
	}

	public Integer getEffdate() {
		return effdate;
	}

	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}

	public String getLifcnum() {
		return lifcnum;
	}

	public void setLifcnum(String lifcnum) {
		this.lifcnum = lifcnum;
	}

	public String getSeqenum() {
		return seqenum;
	}

	public void setSeqenum(String seqenum) {
		this.seqenum = seqenum;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public Date getDatime() {
		return datime;
	}

	public void setDatime(Date datime) {
		this.datime = datime;
	}

	public Integer getDoceffdate() {
		return doceffdate;
	}

	public void setDoceffdate(Integer doceffdate) {
		this.doceffdate = doceffdate;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Integer getKpaydate() {
		return kpaydate;
	}

	public void setKpaydate(Integer kpaydate) {
		this.kpaydate = kpaydate;
	}

	public Integer getIntbasedt() {
		return intbasedt;
	}

	public void setIntbasedt(Integer intbasedt) {
		this.intbasedt = intbasedt;
	}

	public String getFulfilltrm() {
		return fulfilltrm;
	}

	public void setFulfilltrm(String fulfilltrm) {
		this.fulfilltrm = fulfilltrm;
	}

	public String getKariflag() {
		return kariflag;
	}

	public void setKariflag(String kariflag) {
		this.kariflag = kariflag;
	}

	public String getIntcalflag() {
		return intcalflag;
	}

	public void setIntcalflag(String intcalflag) {
		this.intcalflag = intcalflag;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public Integer getCondte() {
		return condte;
	}

	public void setCondte(Integer condte) {
		this.condte = condte;
	}
	
}
