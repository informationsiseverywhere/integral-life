package com.csc.life.terminationclaims.recordstructures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class PaymentValdrec extends ExternalData {

	private static final long serialVersionUID = 1L;
	private String cntType;
	private int paymentDate;
	private String paymentMethod;
	private int numOfPayments;
	private BigDecimal totalPymtAmount; 
	private List<String> recPercent = new ArrayList<>();
	private List<String> pymtAmount = new ArrayList<>();
	private String[] recPercentArray;
	private String[] pymtAmountArray;
	private List<String> outRecPercent = new ArrayList<>();
	private List<String> outPymtAmount = new ArrayList<>();
	private String outTotalRecPercent;
	private String outTotalPymtAmount;
	
	
	
	public String getCntType() {
		return cntType;
	}

	public void setCntType(String cntType) {
		this.cntType = cntType;
	}

	public int getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(int paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public int getNumOfPayments() {
		return numOfPayments;
	}

	public void setNumOfPayments(int numOfPayments) {
		this.numOfPayments = numOfPayments;
	}

	public BigDecimal getTotalPymtAmount() {
		return totalPymtAmount;
	}

	public void setTotalPymtAmount(BigDecimal totalPymtAmount) {
		this.totalPymtAmount = totalPymtAmount;
	}

	public List<String> getRecPercent() {
		return recPercent;
	}

	public void setRecPercent(List<String> recPercent) {
		this.recPercent = recPercent;
	}

	public List<String> getPymtAmount() {
		return pymtAmount;
	}

	public void setPymtAmount(List<String> pymtAmount) {
		this.pymtAmount = pymtAmount;
	}

	public List<String> getOutRecPercent() {
		return outRecPercent;
	}

	public void setOutRecPercent(List<String> outRecPercent) {
		this.outRecPercent = outRecPercent;
	}

	public List<String> getOutPymtAmount() {
		return outPymtAmount;
	}

	public void setOutPymtAmount(List<String> outPymtAmount) {
		this.outPymtAmount = outPymtAmount;
	}

	public String getOutTotalRecPercent() {
		return outTotalRecPercent;
	}

	public void setOutTotalRecPercent(String outTotalRecPercent) {
		this.outTotalRecPercent = outTotalRecPercent;
	}

	public String getOutTotalPymtAmount() {
		return outTotalPymtAmount;
	}

	public void setOutTotalPymtAmount(String outTotalPymtAmount) {
		this.outTotalPymtAmount = outTotalPymtAmount;
	}
	
	

	public String[] getRecPercentArray() {
		return recPercentArray;
	}

	public void setRecPercentArray(String[] recPercentArray) {
		this.recPercentArray = recPercentArray;
	}

	public String[] getPymtAmountArray() {
		return pymtAmountArray;
	}

	public void setPymtAmountArray(String[] pymtAmountArray) {
		this.pymtAmountArray = pymtAmountArray;
	}

	@Override
	public void initialize() {
		
	}

	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

	
}
