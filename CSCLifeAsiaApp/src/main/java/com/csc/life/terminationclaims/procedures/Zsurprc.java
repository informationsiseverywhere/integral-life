package com.csc.life.terminationclaims.procedures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.life.terminationclaims.recordstructures.Surpcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.util.QPUtilities;

public class Zsurprc extends SMARTCodeModel {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "ZSURPRC";
	
	private Syserrrec syserrrec = new Syserrrec();
	private Surpcpy surpcpy = new Surpcpy();
	private Varcom varcom = new Varcom();
	
	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);

	public Zsurprc() {
		super();
	}
	
	public void mainline(Object... parmArray)
	{
		surpcpy.surrenderRec = convertAndSetParam(surpcpy.surrenderRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

	protected void mainline1000()
	{
		/*MAIN*/
		initialise2000();
		updateZrae();
		exitProgram();
	}

	protected void initialise2000()
	{
		syserrrec.subrname.set(wsaaSubr);
		surpcpy.status.set("ENDP");
	}
	
	protected void updateZrae()
	{
	    List<Zraepf> zraepfList;
	    List<String> chdrnumList = new ArrayList<String>();
	    chdrnumList.add(surpcpy.chdrnum.toString());
	    zraepfList = zraepfDAO.searchZraepfRecord(chdrnumList, surpcpy.chdrcoy.toString());
	    
	    if(zraepfList!= null && !zraepfList.isEmpty()){
	    	 zraepfDAO.updateValidflag(zraepfList);	
	     }
	    Zraepf Zraepftmp = new Zraepf();
		if (zraepfList != null && !zraepfList.isEmpty()) {
			Zraepftmp = new Zraepf(zraepfList.get(0));
			Zraepftmp.setApcaplamt(new BigDecimal(0));
			Zraepftmp.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
			Zraepftmp.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
			Zraepftmp.setFlag("2");
			Zraepftmp.setValidflag("1");
			Zraepftmp.setTranno(surpcpy.tranno.toInt());
			Zraepftmp.setApintamt(BigDecimal.ZERO);
			Zraepftmp.setTotamnt(0);
			zraepfDAO.insertZraeRecord(Zraepftmp);
		}
	}
}
