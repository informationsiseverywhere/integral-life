package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6678screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static int maxRecords = 16;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {2, 3, 4, 5, 30, 32, 33, 34, 38, 40, 41, 42, 43, 44}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 10, 3, 45}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6678ScreenVars sv = (S6678ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6678screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6678screensfl, 
			sv.S6678screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6678ScreenVars sv = (S6678ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6678screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6678ScreenVars sv = (S6678ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6678screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6678screensflWritten.gt(0))
		{
			sv.s6678screensfl.setCurrentIndex(0);
			sv.S6678screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6678ScreenVars sv = (S6678ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6678screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6678ScreenVars screenVars = (S6678ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.updteflag.setFieldName("updteflag");
				screenVars.hseqno.setFieldName("hseqno");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.language.setFieldName("language");
				screenVars.indic.setFieldName("indic");
				screenVars.zitem.setFieldName("zitem");
				screenVars.fupcdes.setFieldName("fupcdes");
				screenVars.fupstat.setFieldName("fupstat");
				screenVars.fupremdtDisp.setFieldName("fupremdtDisp");
				screenVars.crtdateDisp.setFieldName("crtdateDisp");
				screenVars.fupremk.setFieldName("fupremk");
				screenVars.sfflg.setFieldName("sfflg");
				screenVars.fuptype.setFieldName("fuptype");
				screenVars.fuprcvdDisp.setFieldName("fuprcvdDisp");
				screenVars.exprdateDisp.setFieldName("exprdateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.updteflag.set(dm.getField("updteflag"));
			screenVars.hseqno.set(dm.getField("hseqno"));
			screenVars.crtuser.set(dm.getField("crtuser"));
			screenVars.language.set(dm.getField("language"));
			screenVars.indic.set(dm.getField("indic"));
			screenVars.zitem.set(dm.getField("zitem"));
			screenVars.fupcdes.set(dm.getField("fupcdes"));
			screenVars.fupstat.set(dm.getField("fupstat"));
			screenVars.fupremdtDisp.set(dm.getField("fupremdtDisp"));
			screenVars.crtdateDisp.set(dm.getField("crtdateDisp"));
			screenVars.fupremk.set(dm.getField("fupremk"));
			screenVars.sfflg.set(dm.getField("sfflg"));
			screenVars.fuptype.set(dm.getField("fuptype"));
			screenVars.fuprcvdDisp.set(dm.getField("fuprcvdDisp"));
			screenVars.exprdateDisp.set(dm.getField("exprdateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6678ScreenVars screenVars = (S6678ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.updteflag.setFieldName("updteflag");
				screenVars.hseqno.setFieldName("hseqno");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.language.setFieldName("language");
				screenVars.indic.setFieldName("indic");
				screenVars.zitem.setFieldName("zitem");
				screenVars.fupcdes.setFieldName("fupcdes");
				screenVars.fupstat.setFieldName("fupstat");
				screenVars.fupremdtDisp.setFieldName("fupremdtDisp");
				screenVars.crtdateDisp.setFieldName("crtdateDisp");
				screenVars.fupremk.setFieldName("fupremk");
				screenVars.sfflg.setFieldName("sfflg");
				screenVars.fuptype.setFieldName("fuptype");
				screenVars.fuprcvdDisp.setFieldName("fuprcvdDisp");
				screenVars.exprdateDisp.setFieldName("exprdateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("updteflag").set(screenVars.updteflag);
			dm.getField("hseqno").set(screenVars.hseqno);
			dm.getField("crtuser").set(screenVars.crtuser);
			dm.getField("language").set(screenVars.language);
			dm.getField("indic").set(screenVars.indic);
			dm.getField("zitem").set(screenVars.zitem);
			dm.getField("fupcdes").set(screenVars.fupcdes);
			dm.getField("fupstat").set(screenVars.fupstat);
			dm.getField("fupremdtDisp").set(screenVars.fupremdtDisp);
			dm.getField("crtdateDisp").set(screenVars.crtdateDisp);
			dm.getField("fupremk").set(screenVars.fupremk);
			dm.getField("sfflg").set(screenVars.sfflg);
			dm.getField("fuptype").set(screenVars.fuptype);
			dm.getField("fuprcvdDisp").set(screenVars.fuprcvdDisp);
			dm.getField("exprdateDisp").set(screenVars.exprdateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6678screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6678ScreenVars screenVars = (S6678ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.updteflag.clearFormatting();
		screenVars.hseqno.clearFormatting();
		screenVars.crtuser.clearFormatting();
		screenVars.language.clearFormatting();
		screenVars.indic.clearFormatting();
		screenVars.zitem.clearFormatting();
		screenVars.fupcdes.clearFormatting();
		screenVars.fupstat.clearFormatting();
		screenVars.fupremdtDisp.clearFormatting();
		screenVars.crtdateDisp.clearFormatting();
		screenVars.fupremk.clearFormatting();
		screenVars.sfflg.clearFormatting();
		screenVars.fuptype.clearFormatting();
		screenVars.fuprcvdDisp.clearFormatting();
		screenVars.exprdateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6678ScreenVars screenVars = (S6678ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.updteflag.setClassString("");
		screenVars.hseqno.setClassString("");
		screenVars.crtuser.setClassString("");
		screenVars.language.setClassString("");
		screenVars.indic.setClassString("");
		screenVars.zitem.setClassString("");
		screenVars.fupcdes.setClassString("");
		screenVars.fupstat.setClassString("");
		screenVars.fupremdtDisp.setClassString("");
		screenVars.crtdateDisp.setClassString("");
		screenVars.fupremk.setClassString("");
		screenVars.sfflg.setClassString("");
		screenVars.fuptype.setClassString("");
		screenVars.fuprcvdDisp.setClassString("");
		screenVars.exprdateDisp.setClassString("");
	}

/**
 * Clear all the variables in S6678screensfl
 */
	public static void clear(VarModel pv) {
		S6678ScreenVars screenVars = (S6678ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.updteflag.clear();
		screenVars.hseqno.clear();
		screenVars.crtuser.clear();
		screenVars.language.clear();
		screenVars.indic.clear();
		screenVars.zitem.clear();
		screenVars.fupcdes.clear();
		screenVars.fupstat.clear();
		screenVars.fupremdtDisp.clear();
		screenVars.fupremdt.clear();
		screenVars.crtdateDisp.clear();
		screenVars.crtdate.clear();
		screenVars.fupremk.clear();
		screenVars.sfflg.clear();
		screenVars.fuptype.clear();
		screenVars.fuprcvdDisp.clear();
		screenVars.fuprcvd.clear();
		screenVars.exprdateDisp.clear();
		screenVars.exprdate.clear();
	}
}
