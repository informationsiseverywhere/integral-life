package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr678screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static int maxRecords = 7;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 13, 2, 76}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr678ScreenVars sv = (Sr678ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr678screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr678screensfl, 
			sv.Sr678screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr678ScreenVars sv = (Sr678ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr678screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr678ScreenVars sv = (Sr678ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr678screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr678screensflWritten.gt(0))
		{
			sv.sr678screensfl.setCurrentIndex(0);
			sv.Sr678screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr678ScreenVars sv = (Sr678ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr678screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr678ScreenVars screenVars = (Sr678ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.benefits.setFieldName("benefits");
				screenVars.nofday.setFieldName("nofday");
				screenVars.hosben.setFieldName("hosben");
				screenVars.copay.setFieldName("copay");
				screenVars.benfamt.setFieldName("benfamt");
				screenVars.seqno.setFieldName("seqno");
				screenVars.gdeduct.setFieldName("gdeduct");
				screenVars.amtlife.setFieldName("amtlife");
				screenVars.amtyear.setFieldName("amtyear");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.benefits.set(dm.getField("benefits"));
			screenVars.nofday.set(dm.getField("nofday"));
			screenVars.hosben.set(dm.getField("hosben"));
			screenVars.copay.set(dm.getField("copay"));
			screenVars.benfamt.set(dm.getField("benfamt"));
			screenVars.seqno.set(dm.getField("seqno"));
			screenVars.gdeduct.set(dm.getField("gdeduct"));
			screenVars.amtlife.set(dm.getField("amtlife"));
			screenVars.amtyear.set(dm.getField("amtyear"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr678ScreenVars screenVars = (Sr678ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.benefits.setFieldName("benefits");
				screenVars.nofday.setFieldName("nofday");
				screenVars.hosben.setFieldName("hosben");
				screenVars.copay.setFieldName("copay");
				screenVars.benfamt.setFieldName("benfamt");
				screenVars.seqno.setFieldName("seqno");
				screenVars.gdeduct.setFieldName("gdeduct");
				screenVars.amtlife.setFieldName("amtlife");
				screenVars.amtyear.setFieldName("amtyear");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("benefits").set(screenVars.benefits);
			dm.getField("nofday").set(screenVars.nofday);
			dm.getField("hosben").set(screenVars.hosben);
			dm.getField("copay").set(screenVars.copay);
			dm.getField("benfamt").set(screenVars.benfamt);
			dm.getField("seqno").set(screenVars.seqno);
			dm.getField("gdeduct").set(screenVars.gdeduct);
			dm.getField("amtlife").set(screenVars.amtlife);
			dm.getField("amtyear").set(screenVars.amtyear);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr678screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr678ScreenVars screenVars = (Sr678ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.benefits.clearFormatting();
		screenVars.nofday.clearFormatting();
		screenVars.hosben.clearFormatting();
		screenVars.copay.clearFormatting();
		screenVars.benfamt.clearFormatting();
		screenVars.seqno.clearFormatting();
		screenVars.gdeduct.clearFormatting();
		screenVars.amtlife.clearFormatting();
		screenVars.amtyear.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr678ScreenVars screenVars = (Sr678ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.benefits.setClassString("");
		screenVars.nofday.setClassString("");
		screenVars.hosben.setClassString("");
		screenVars.copay.setClassString("");
		screenVars.benfamt.setClassString("");
		screenVars.seqno.setClassString("");
		screenVars.gdeduct.setClassString("");
		screenVars.amtlife.setClassString("");
		screenVars.amtyear.setClassString("");
	}

/**
 * Clear all the variables in Sr678screensfl
 */
	public static void clear(VarModel pv) {
		Sr678ScreenVars screenVars = (Sr678ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.benefits.clear();
		screenVars.nofday.clear();
		screenVars.hosben.clear();
		screenVars.copay.clear();
		screenVars.benfamt.clear();
		screenVars.seqno.clear();
		screenVars.gdeduct.clear();
		screenVars.amtlife.clear();
		screenVars.amtyear.clear();
	}
}
