/*
 * File: Pr676.java
 * Date: 30 August 2009 1:53:04
 * Author: Quipoz Limited
 * 
 * Class transformed from PR676.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.tablestructures.T6005rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtridTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnntTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovtrbnTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.terminationclaims.recordstructures.Pr676cpy;
import com.csc.life.terminationclaims.screens.Sr676ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.terminationclaims.tablestructures.Tr686rec;
import com.csc.life.terminationclaims.tablestructures.Tr687rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*      HOSPITAL BENEFIT & WAIVER
*
* This screen/program PR676 is used to capture the coverage and
* rider details for Hospital Benefit & Waiver. The program is
* retrofitted from PNM51 which in turn had been cloned from P5125.
*
* Initialise
* ----------
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* Read CHDRLNB (RETRV)  in  order to obtain the contract header
* information.  If  the  number of policies in the plan is zero
* or  one  then Plan-processing does not apply. If there is any
* other  numeric  value,  this  value  indicates  the number of
* policies in the Plan.
*
* If Plan processing is  not  to be performed, then protect and
* non-display the following three fields:-
*
* a) No. of Policies in Plan (default to 1)
* b) No. Available (default to 1)
* c) No. Applicable (default to 1)
*
* and processing for Variations will not occur.
*
* The key  for Coverage/rider to be worked on will be available
* from  COVTLNB.  This is obtained by using the RETRV function.
* Check if there are any transactions by doing a BEGN  (to read
* the Coverage/rider summary) using the COVTRBN logical view.
*
* The COVTRBN key will be:-
*
*  Company, Contract-no, Life, Coverage-no, rider-no, sequence-no
*
* If  Plan  processing is to occur, then calculate the "credit"
* as follows:
*
*  - subtract  the  'No  of  policies in the Plan' from the
*       first  COVTRBN  record  read  above (if no COVTRBN,
*       this is zero) from the 'No of policies in the Plan'
*       (from the Contract-Header).
*
*  - a positive  credit means that additional policies must
*       be added to the plan.
*
*  - a negative  credit  means  that  some policies must be
*       removed from the plan.
*
* If this is a single policy plan
*    if it is the first time (no COVTRBN records)
*     or there is no credit
*   - plan processing is not required.
*
* Set  the  number of  policies  available  to  the  number  of
* policies on the plan.  If  COVTRBN  records are to be written
* for the first time  (as determined above), default the number
* applicable to the number available.
*
* Read  the  contract  definition  details  from  T5688 for the
* contract  type  held  on  CHDRLNB. Access the version of this
* item for the original commencement date of the risk.
*
* Read  the  general  coverage/rider details from T5687 and the
* traditional/term  edit rules from T5606 for the coverage type
* held  on  COVTLNB.  Access  the version of this  item for the
* original commencement date of the risk.
*
* Read the  tolerance  limit  from  T5667  (key  as for T5606).
* Although this  is  a dated table,  just  read  the latest one
* (using ITEM).
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*  - read  the life details using LIFELNB (life number from
*       COVTLNB, joint life number '00').  Look up the name
*       from the  client  details  (CLTS)  and  format as a
*       "confirmation name".
*
*  - read the joint life details using LIFELNB (life number
*       from COVTLNB,  joint  life number '01').  If found,
*       look up the name from the client details (CLTS) and
*       format as a "confirmation name".
*
* To  determine  which premium calculation method to use decide
* whether or  not it is a Single or Joint-life case (if it is a
* joint  life  case,  the  joint  life record was found above).
* Then:
*
*  - if the benefit billing method is not blank, non-display
*       and protect the premium field (so do not bother with
*       the rest of this).
*
*  - if it  is  a  single-life  case, use the single-method
*       from  T5687. The age to be used for validation will
*       be the age of the main life.
*
*  - if the joint-life indicator (from T5687) is blank, and
*       if  it  is  a Joint-life case, use the joint-method
*       from  T5687. The age to be used for validation will
*       be the age of the main life.
*
*  - if the  Joint-life  indicator  is  'N',  then  use the
*       Single-method.  But, if there is a joint-life (this
*       must be  a  rider  to have got this far) prompt for
*       the joint  life  indicator  to determine which life
*       the rider is to attach to.  In all other cases, the
*       joint life  indicator  should  be non-displayed and
*       protected.  The  age to be used for validation will
*       be the age  of the main or joint life, depending on
*       the one selected.
*
*  - use the  premium-method  selected  from  T5687, if not
*       blank,  to access T5675.  This gives the subroutine
*       to use for the calculation.
*
* COVERAGE/RIDER DETAILS
*
* The fields to  be displayed on the screen are determined from
* the entry in table T5606 read earlier as follows:
*
*  - if  the  maximum  and  minimum   benefit  amounts  are
*       both  zero,  non-display  and protect  the  benefit
*       amount.  If  the  minimum  and maximum are both the
*       same, display the amount protected. Otherwise allow
*       input.
*
*  - if  the amount is to be displayed/entered, look up the
*       benefit frequency short description (T3590).
*
*  - if all   the   valid   mortality  classes  are  blank,
*       non-display  and  protect  this  field. If there is
*       only  one  mortality  class, display and protect it
*       (no validation will be required).
*
*  - if all the valid lien codes are blank, non-display and
*       protect  this field. Otherwise, this is an optional
*       field.
*
*  - if the  cessation  AGE  section  on  T5606  is  blank,
*       protect the two age related fields.
*
*  - if the  cessation  TERM  section  on  T5606  is blank,
*       protect the two term related fields.
*
*  - using  the  age  next  birthday  (ANB at RCD) from the
*       applicable  life  (see above), look up Issue Age on
*       the AGE and TERM  sections.  If  the  age fits into
*       a "slot" in  one  of these sections,  and the  risk
*       cessation  limits   are  the  same,   default   and
*       protect the risk cessation fields. Also do the same
*       for the premium  cessation  details.  In this case,
*       also  calculate  the  risk  and  premium  cessation
*       dates.
*
* OPTIONS AND EXTRAS
*
* If options and extras are  not  allowed (as defined by T5606)
* non-display and protect the fields.
*
* Otherwise,  read the  options  and  extras  details  for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the Options/Extras indicator (to show that there are some).
*
* REASSURANCE
*
* If Reassurance is not allowed ( T5687 Coverage/Rider details)
* non-display and protect the fields.
*
* Otherwise,  read  the  Reassurance  RACT  file for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the Reassurance  indicator (to show that there are some).
*
* ENQUIRY MODE
*
* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to
* protect  all  input  capable  fields  except  the  indicators
* controlling  where  to  switch  to  next  (options and extras
* indicator).
*
* Validation
* ----------
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* If 'KILL'  is  requested  and  the  current credit is zero or
* equal to the number of policies in the plan (all or nothing),
* then skip the  validation.  Otherwise,  highlight  this as an
* error and then skip the remainder of the validation.
*
* If  in  enquiry  mode,  skip  all field validation EXCEPT the
* options/extras indicator.
*
* Before  the  premium amount is calculated, the screen must be
* valid.  So  all  editing  is  completed before the premium is
* calculated.
*
* Table  T5606  (previously read) is used to obtain the editing
* rules.  Edit the screen according to the rules defined by the
* help. In particular:-
*
*  1) Check the benefit amount,  if applicable, against the
*       limits.
*
*  2) Check the consistency of the risk age and term fields
*       and  premium  age and term fields. Either, risk age
*       and  premium  age  must  be  used  or risk term and
*       premium  term  must  be  used.  They  must  not  be
*       combined. Note that  these  only need validating if
*       they were not defaulted.
*       NOTE: Risk age  and  Prem  term  may  be mixed i.e.
*       validation no longer requires the use of  risk  age
*       and prem age or risk term and prem term.
*
*  3) Mortality-Class,  if the mortality class appears on a
*       coverage/rider  screen  it  is  a  compulsory field
*       because it will  be used in calculating the premium
*       amount. The mortality class entered must one of the
*       ones in the edit rules table.
*
* Calculate the following:-
*
*       - risk cessation date
*       - premium cessation date
*
* OPTIONS AND EXTRAS
*
* If  options/extras already exist, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed. If
* options  and  extras  are  requested,  DO  NOT  CALCULATE THE
* PREMIUM THIS TIME AROUND.
*
* REASSURANCE
*
* If  Reassurance already exists, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed.
*
* PREMIUM CALCULATION
*
* The  premium amount is  required  on  all  products  and  all
* validation  must  be  successfully  completed  before  it  is
* calculated. If there is  no premium  method defined (i.e. the
* relevant code was blank), the premium amount must be entered.
* Otherwise, it is optional and always calculated.
*
* To calculate  it,  call  the  relevant calculation subroutine
* worked out above passing:
*
*       - 'CALC' for the function
*       - Coverage code (crtable)
*       - Company
*       - Contract number
*       - Life number
*       - Joint life number
*            (if  the   screen  indicator  is  set  to  'J'
*            ,otherwise pass '00')
*       - Coverage number
*       - Rider number (0 for coverages)
*       - Effective date (Premium / Risk)
*       - Termination date (i.e. term cessation date)
*       - Currency
*       - Benefit amount
*       - Calculation "Basis" (i.e. mortality code)
*       - Payment frequency (from the contract header)
*       - Method of payment (from the contract header)
*       - Premium amount ("instalment")
*       - Return Status
*       - Rating Date (effective date for tables)
*
*  Subroutine may look up:
*
*       -  Life and Joint-life details
*       -  Options/Extras
*
* Having calculated it, the  entered value, if any, is compared
* with it to check that  it  is within acceptable limits of the
* automatically calculated figure.  If  it  is  less  than  the
* amount calculated and  within  tolerance, then  the  manually
* entered amount is allowed.  If  the entered value exceeds the
* calculated one, the calculated value is used.
*
* To check the tolerance amount against the limit read above.
*
* If 'CALC' was entered then re-display the screen.
*
* Updating
* --------
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* Updating occurs with  the Creation, Deletion or Updating of a
* COVTRBN transaction record.
*
* If the 'KILL' function key was pressed or if in enquiry mode,
* skip the updating.
*
* Before  updating any  records,  calculate  the  new  "credit"
* amount.
*
*  - add to  the  current  credit  amount,  the  number  of
*       policies previously  applicable  from  the  COVTRBN
*       record (zero if  there was no COVTRBN) and subtract
*       the number applicable entered on the screen.
*
*  - a positive  credit means that additional policies must
*       be added to the plan.
*
*  - a negative  credit  means  that  some policies must be
*       removed from the plan.
*
* Before creating a  new COVTRBN record initialise the coverage
* fields.
*
* If the number  applicable  is greater than zero, write/update
* the COVTRBN record.  If the number applicable is zero, delete
* the COVTRBN record.
*
*
* Next Program
* ------------
*
* The  first  thing  to   consider   is   the  handling  of  a
* Reassurance request. If the indicator is 'X', a request to
* visit Reassurance has been made. In this case:
*
*  - change the Reassurance request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with  an  action  of 'C' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  Reassurance   indicator  will  be  '?'.  To
* handle the return from Reassurance:
*
*  - calculate  the  premium  as  described  above  in  the
*       'Validation' section, and  check  that it is within
*       the tolerance limit,
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
* Similarly, we do the same processing to handle an
* options/extras request. If the indicator is 'X', a request to
* visit options and extras has been made. In this case:
*
*  - change the options/extras request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with  an  action  of 'A' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  options/extras  indicator  will  be  '?'.  To
* handle the return from options and extras:
*
*  - calculate  the  premium  as  described  above  in  the
*       'Validation' section, and  check  that it is within
*       the tolerance limit,
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
* The  following  processing  for  the  4000  section  will  be
* standard  for  all  scrolling  generic  component  processing
* programs.
*
* If control is passed to this  part of the 4000 section on the
* way  out of the program,  i.e. after  screen  I/O,  then  the
* current stack position action  flag  will  be  blank.  If the
* 4000  section  is   being   performed  after  returning  from
* processing another program, then  the  current stack position
* action flag will be '*'.
*
* Processing on the way out:
*
*  A) If one of the Roll  keys  has  been  pressed  and the
*       'Number  Applicable'   has  been  changed  set  the
*       current select action field  to  '*',  add 1 to the
*       program pointer and exit.
*
*  B) If one of the Roll  keys  has  been  pressed  and the
*       'Number Applicable' has  not been changed then loop
*       round  within   the   program   and   display   the
*       next/previous   screen   as   requested  (including
*       reads).   When   displaying   a   previous  record,
*       calculate  the  number  available  as  the  current
*       number plus the  number  applicable  to  the record
*       about to be  displayed.  When  displaying  the next
*       record, or a blank  screen  to  enter a new record,
*       calculate  the  number  available  as  the  current
*       number less the  number  applicable  on the current
*       screen. If displaying  a  blank  screen  for  a new
*       record, default the number applicable to the number
*       available. If this is less than zero, default it to
*       zero.
*
*  C) If 'Enter' has been  pressed and "Credit" is non-zero
*       set the current select  action  field to '*', add 1
*       to the program pointer and exit.
*
*  D) If 'Enter' has been  pressed and "Credit" is zero add
*       1 to the program pointer and exit.
*
*  E) If 'KILL'  has  been  requested,  then move spaces to
*       the current program entry in the program stack  and
*       exit.
*
* Processing on the way in:
*
*  A) If one of the Roll  keys  has  been pressed then loop
*       round within the  program  and  display the next or
*       previous  screen  as  requested  (including  reads,
*       available count etc., see above).
*
*  B) If 'Enter'  has  been  pressed  and  "Credit"  has  a
*       positive value then  loop  round within the program
*       and  display the next screen.  Calculate  available
*       count as the current available  less current screen
*       applicable. Set the number applicable to the number
*       available, but not less than zero.
*
*  C) If 'Enter'  has  been  pressed  and  "Credit"  has  a
*       negative  value,  loop  round  within  the  program
*       displaying the  details  from the first transaction
*       record,  with  the  'Number Applicable' highlighted
*       and give  a  message  indicating that more policies
*       have been defined than are on the Contract Header.
*
*
*  ANNUITY DETAILS SELECT SCREEN
*  =============================
*
*  As part of the 9405 Annuities Development a new field,
*  Annuity Details, is displayed if the component being
*  created, modified or enquired is an annuity component.
*
*  These components are identified by their coverage code
*  (CRTABLE) being a valid item on the Annuity Component Edit
*  Rules Table (T6625). The new field should only be displayed
*  if the coverage code is on this table.
*
*  If the details have not already been created for this
*  component, an X is displayed  in ANNTIND, otherwise a +
*  is displayed.
*
*  Once the details have been created, the regular benefits
*  component screen is redisplayed.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr676 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR676");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected ZonedDecimalData wsaaSumins = new ZonedDecimalData(17, 2);
	protected ZonedDecimalData wsaaMinsumins = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaBenCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaPayrBillfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaPovrInstamnt = new PackedDecimalData(17, 2);
	private final int wsaaMaxOcc = 8;
	private final int wsaaMaxMort = 6;
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaKillFlag = new FixedLengthStringData(1);
	private Validator forcedKill = new Validator(wsaaKillFlag, "Y");

	private FixedLengthStringData wsaaCtrltime = new FixedLengthStringData(1);
	private Validator firsttime = new Validator(wsaaCtrltime, "Y");
	private Validator nonfirst = new Validator(wsaaCtrltime, "N");
	private String premReqd = "N";

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator nonplan = new Validator(wsaaPlanproc, "N");
	protected Validator plan = new Validator(wsaaPlanproc, "Y");

	private FixedLengthStringData wsaaLifeind = new FixedLengthStringData(1);
	private Validator jointlif = new Validator(wsaaLifeind, "J");
	private Validator singlif = new Validator(wsaaLifeind, "S");

	private FixedLengthStringData wsaaInsertUpdateFlag = new FixedLengthStringData(1);/*ILIFE-3470 by pmujavadiya*/
	private Validator callingfirst=new Validator(wsaaInsertUpdateFlag, "Y");
	private Validator callingnext=new Validator(wsaaInsertUpdateFlag, "N");
	
	private FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	private Validator firstTaxCalc = new Validator(wsaaFirstTaxCalc, "Y");
	private Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");
	private PackedDecimalData wsaaNumavail = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaCredit = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaWorkCredit = new PackedDecimalData(5, 0);
	protected PackedDecimalData wsaaSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaWopFlag = new FixedLengthStringData(1);
	private Validator wsaaWopFound = new Validator(wsaaWopFlag, "Y");
	private Validator wsaaWopNotFound = new Validator(wsaaWopFlag, "N");
	private ZonedDecimalData wsddRiskCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddBenCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddRiskCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddBenCessTerm = new ZonedDecimalData(3, 0);
		/* WSAA-MAIN-LIFE-DETS */
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
		/* WSBB-JOINT-LIFE-DETS */
	private PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsbbCltdob = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);

	private FixedLengthStringData wsbbTr687Itemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsbbTr687Crtable = new FixedLengthStringData(4).isAPartOf(wsbbTr687Itemitem, 0);
	private FixedLengthStringData wsbbTr687Benpln = new FixedLengthStringData(2).isAPartOf(wsbbTr687Itemitem, 4);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzRiskCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzBenCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzRiskCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzBenCessTerm = new PackedDecimalData(11, 5);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler1, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler3, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaHbnfLivesnoChar = new FixedLengthStringData(1);
	protected ZonedDecimalData wsaaHbnfLivesnoNum = new ZonedDecimalData(1, 0).isAPartOf(wsaaHbnfLivesnoChar, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaLivesno = new FixedLengthStringData(1);
	private Validator validLivesno = new Validator(wsaaLivesno, "1","2","3","4");

	private FixedLengthStringData wsaaWaiverprem = new FixedLengthStringData(1);
	private Validator validWaiverprem = new Validator(wsaaWaiverprem, "Y","N");

	private FixedLengthStringData wsaaCovtlnbParams = new FixedLengthStringData(219);
	private FixedLengthStringData wsaaCovtlnbDataKey = new FixedLengthStringData(64).isAPartOf(wsaaCovtlnbParams, 49);
	private FixedLengthStringData wsaaCovtlnbCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovtlnbDataKey, 11);

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private AnntTableDAM anntIO = new AnntTableDAM();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected CltsTableDAM cltsIO = new CltsTableDAM();
	protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private CovtrbnTableDAM covtrbnIO = new CovtrbnTableDAM();
	private CovtridTableDAM covtridIO = new CovtridTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	protected HbnfTableDAM hbnfIO = new HbnfTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	protected LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PovrTableDAM povrIO = new PovrTableDAM();
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	private Freqcpy freqcpy = new Freqcpy();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Gensswrec gensswrec = new Gensswrec();
	private T2240rec t2240rec = new T2240rec();
	private T5606rec t5606rec = new T5606rec();
	private T5667rec t5667rec = new T5667rec();
	private T5671rec t5671rec = new T5671rec();
	protected T5675rec t5675rec = new T5675rec();
	private T5687rec t5687rec = new T5687rec();
	protected T5688rec t5688rec = new T5688rec();
	private T6005rec t6005rec = new T6005rec();
	private Tr687rec tr687rec = new Tr687rec();
	private Tr686rec tr686rec = new Tr686rec();
	private Tr517rec tr517rec = new Tr517rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	protected Premiumrec premiumrec = getPremiumrec();
	private Pr676cpy pr676cpy = new Pr676cpy();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Wssplife wssplife = new Wssplife();
	private Sr676ScreenVars sv = getPScreenVars();//ScreenProgram.getScreenVars( Sr676ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaDefaultsInner wsaaDefaultsInner = new WsaaDefaultsInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private Exclpf exclpf=null;
	private boolean exclFlag = false;
	private Itempf itempf = null;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

	private FixedLengthStringData wsaaOccupationClass = new FixedLengthStringData(2);
	private int fupno = 0;
	private List<Fluppf> fluplnbList = new ArrayList<Fluppf>();
	private T5661rec t5661rec = new T5661rec();
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaCount = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private Ta610rec ta610rec = new Ta610rec();
	boolean isFollowUpRequired  = false;
	private Fluppf fluppf;
	boolean NBPRP056Permission  = false;
	private T3644rec t3644rec = new T3644rec();
	private static final String tA610 = "TA610";
	private static final String t3644 = "T3644";
	private static final String t5661 = "T5661";
	private static final String NBPRP056="NBPRP056";
	private List<Fluppf> fluppfAvaList =  new ArrayList<Fluppf>();	//ICIL-1494
	private List<Itempf> ta610List = new ArrayList<Itempf>();	//ICIL-1494

/**
 * Contains all possible labels used by goTo action.
 */
	
	private boolean incomeProtectionflag = false;
	private boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	private String occuptationCode="";
	private boolean loadingFlag = false;/* BRD-306 */
	private boolean waitperiodFlag=false;
	private boolean bentrmFlag=false;
	private boolean poltypFlag=false;
	private boolean prmbasisFlag=false;
	private boolean dialdownFlag = false;
	private boolean stampdutyFlag  = false;
	//ILJ-43
	private boolean contDtCalcFlag = false;
	private String cntDteFeature = "NBPRP113";
	//end
	public Premiumrec getPremiumrec() {
		return new Premiumrec();
	}
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont1010, 
		cont1012, 
		cont1015, 
		premmeth1020, 
		cont1030, 
		exit1090, 
		readCovtrid1310, 
		setDefault1380, 
		benCessTerm1410, 
		exit1490, 
		riskCessTerm1510, 
		exit1540, 
		premCessTerm1560, 
		exit1590, 
		exit1790, 
		nextColumn1820, 
		moveDefaults1850, 
		preExit, 
		redisplay2480, 
		exit2490, 
		checkRcessFields2530, 
		ageAnniversary2541, 
		term2542, 
		termExact2543, 
		check2544, 
		checkOccurance2545, 
		checkTermFields2550, 
		checkComplete2555, 
		checkLiencd2570, 
		loop2575, 
		checkMore2580, 
		exit2590, 
		readNext2651, 
		calc2710, 
		exit2790, 
		exit3490, 
		exit3790, 
		cont4710, 
		cont4715, 
		cont4717, 
		cont4720, 
		exit4790, 
		rolu4805, 
		cont4810, 
		cont4820, 
		readCovtrbn4830, 
		cont4835, 
		cont4837, 
		cont4840, 
		exit4890, 
		callTaxSubr6150, 
		exit6190
	}

	public Pr676() {
		super();
		screenVars = sv;
		new ScreenModel("Sr676", AppVars.getInstance(), sv);
	}
	
	protected Sr676ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sr676ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {//TMLII-268
		/* Initialization*/
		scrnparams.version.set(getWsaaVersion());
		wsspcomn.version.set(getWsaaVersion());
		scrnparams.errorline.set(varcom.vrcmMsg);
		//TMLII-268
			processBoMainline(sv, sv.dataArea, parmArray);
			//ILIFE-1344 Start vchawda
			if(isEQ(wsspcomn.sectionno, "2000")){
				if(isEQ(sv.instprmErr, errorsInner.f254)){
					sv.instprmErr.set(SPACES);
					if(isEQ(sv.errorIndicators, SPACES)){
					wsspcomn.edterror.set(varcom.oK);
					}
				}
			}
			//ILIFE-1344 End
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1001();
				case cont1010: 
					cont1010();
					plan1010();
				case cont1012: 
					cont1012();
				case cont1015: 
					cont1015();
				case premmeth1020: 
					premmeth1020();
				case cont1030: 
					cont1030();
					cont1060();
					prot1070();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1001()
	{
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");//

		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			if (isNE(wsspcomn.flag, "I")) {  //MTL130
				 sv.instPrem.set(0);		//MTL130
				 calcPremium2700();			//MTL130
				 sv.instprmErr.set(" ");    //MTL130
			}
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		// ILJ-43
		if (!contDtCalcFlag) {
			//sv.riskCessAgeOut[varcom.nd.toInt()].set("Y");
			sv.contDtCalcScreenflag.set("N");
		}
		else
		{
			sv.contDtCalcScreenflag.set("Y");	
		}
		// end
		premiumrec.premiumRec.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		wssplife.fupno.set(ZERO);
		/*    Initialise & Setup default values for screen fields.*/
		sv.anbAtCcd.set(0);
		sv.instPrem.set(0);
		sv.zbinstprem.set(0);
		sv.zlinstprem.set(0);
		/*BRD-306 START */
		sv.loadper.set(0);
		sv.adjustageamt.set(0);
		sv.rateadj.set(0);
		sv.fltmort.set(0);
		sv.premadj.set(0);
		/*BRD-306 END */
		sv.waitperiod.set(SPACES);
		sv.bentrm.set(SPACES);
		sv.prmbasis.set(SPACES);
		sv.poltyp.set(SPACES);
		sv.dialdownoption.set(SPACES);
		sv.numapp.set(0);
		sv.numavail.set(0);
		sv.premCessAge.set(0);
		sv.premCessTerm.set(0);
		sv.polinc.set(0);
		sv.riskCessAge.set(0);
		sv.riskCessTerm.set(0);
		sv.sumin.set(0);
		sv.taxamt.set(0);
		sv.benCessAge.set(0);
		sv.benCessTerm.set(0);
		wssplife.fuptype.set("N");
		wsaaCtrltime.set("N");
		wsaaInsertUpdateFlag.set("Y");/*ILIFE-3470 by pmujavadiya*/
		sv.frqdescOut[varcom.nd.toInt()].set("Y");
		sv.premCessDate.set(varcom.vrcmMaxDate);
		sv.riskCessDate.set(varcom.vrcmMaxDate);
		sv.benCessDate.set(varcom.vrcmMaxDate);
		sv.waiverprem.set("N");		
		/*    Initialise WSSP field for use through linkage.*/
		wssplife.bigAmt.set(ZERO);
		wssplife.occdate.set(ZERO);
		wsaaTaxamt.set(ZERO);
		wsaaFirstTaxCalc.set("Y");
		prmbasisFlag=false;
		poltypFlag=false;
		bentrmFlag=false;
		waitperiodFlag=false;
		/* Read CHDRLNB (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*BRD-306 START */
		sv.cnttype.set(chdrlnbIO.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*BRD-306 END */
		/* Read T2240 for age definition.                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRLNB-CNTTYPE        TO ITEM-ITEMITEM.                */
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*    MOVE '***'               TO ITEM-ITEMITEM                 */
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else { //MTL002
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}//MTL002
			}
		}
		/* Read TR52D for Taxcode                                          */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlnbIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");		//ILIFE-4225
			sv.taxindOut[varcom.pr.toInt()].set("N");
		}
		/* Read the PAYR record to get the Billing Details.*/
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* If Plan processing is  not  to be performed, then protect and*/
		/* non-display the following three fields:-*/
		/*  a) No. of Policies in Plan (default to 1)*/
		/*  b) No. Available (default to 1)*/
		/*  c) No. Applicable (default to 1)*/
		/* and processing for Variations will not occur.*/
		if (isEQ(chdrlnbIO.getPolinc(), 0)) {
			wsaaPlanproc.set("N");
		}
		else {
			wsaaPlanproc.set("Y");
		}
		if (nonplan.isTrue()) {
			sv.polinc.set(1);
			sv.numavail.set(1);
			sv.numapp.set(1);
			sv.polincOut[varcom.nd.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
		}
		/* The key  for Coverage/rider to be worked on will be available*/
		/* from  COVTLNB.  This is obtained by using the RETRV function.*/
		covtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getRider(), "00")) {
			wsaaSumins.set(covtlnbIO.getSumins());
			goTo(GotoLabel.cont1010);
		}
		/*    If we are dealing with a coverage we should skip this part*/
		wsaaCovtlnbParams.set(covtlnbIO.getParams());
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setDataKey(wsaaCovtlnbDataKey);
		covtlnbIO.setRider(ZERO);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.endp)
		&& isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isNE(covtlnbIO.getCoverage(), wsaaCovtlnbCoverage)) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
			wsaaKillFlag.set("Y");
			goTo(GotoLabel.exit1090);
		}
		/*    Keep the SA for the base plan for checking of minimum SA*/
		/*    for Base Plan if it is Hospital Benefit.*/
		wsaaSumins.set(covtlnbIO.getSumins());
		/* TO HAVE GOT THIS FAR MEANS A COVERAGE MUST EXIST*/
		/* FOR THE RIDER WE ARE EITHER ACCESSING OR CREATING.*/
		covtlnbIO.setParams(wsaaCovtlnbParams);
		/*  Read TR686 for waiver code*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr686);
		itemIO.setItemitem(covtlnbIO.getCrtable());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr686rec.tr686Rec.set(itemIO.getGenarea());
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			tr686rec.waivercode.set(SPACES);
		}
		
		//ILIFE-4214
		
				stampdutyFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
				if(!stampdutyFlag) {
					sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
				}
				
				if (isEQ(wsaaToday, 0)) {
					datcon1rec.datcon1Rec.set(SPACES);
					datcon1rec.function.set(varcom.tday);
					Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
					wsaaToday.set(datcon1rec.intDate);
				}
		
	}

protected void cont1010()
	{
		/* Check if there are any transactions by doing a BEGN  (to read*/
		/* the Coverage/rider summary) using the COVTRBN logical view.*/
		/* The COVTRBN key is :-*/
		covtrbnIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtrbnIO.setChdrnum(covtlnbIO.getChdrnum());
		covtrbnIO.setLife(covtlnbIO.getLife());
		covtrbnIO.setCoverage(covtlnbIO.getCoverage());
		covtrbnIO.setRider(covtlnbIO.getRider());
		covtrbnIO.setSeqnbr(ZERO);
		covtrbnIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covtrbnIO);
		if (isNE(covtrbnIO.getStatuz(), varcom.oK)
		&& isNE(covtrbnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtrbnIO.getParams());
			fatalError600();
		}
		if (isNE(covtrbnIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covtrbnIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covtrbnIO.getLife(), covtlnbIO.getLife())
		|| isNE(covtrbnIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covtrbnIO.getRider(), covtlnbIO.getRider())) {
			covtrbnIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtrbnIO.getStatuz(), varcom.endp)) {
			covtrbnIO.setNonKey(SPACES);
			covtrbnIO.setAnbccd(1, 0);
			covtrbnIO.setAnbccd(2, 0);
			covtrbnIO.setSingp(0);
			covtrbnIO.setZbinstprem(0);
			covtrbnIO.setZlinstprem(0);
			covtrbnIO.setInstprem(0);
			covtrbnIO.setNumapp(0);
			covtrbnIO.setPremCessAge(0);
			covtrbnIO.setPremCessTerm(0);
			covtrbnIO.setPolinc(0);
			covtrbnIO.setRiskCessAge(0);
			covtrbnIO.setRiskCessTerm(0);
			covtrbnIO.setSumins(0);
			covtrbnIO.setBenCessAge(0);
			covtrbnIO.setBenCessTerm(0);
			wsaaCtrltime.set("Y");
		}
	}

protected void plan1010()
	{
	
		if (firsttime.isTrue()) {
			covtrbnIO.setPolinc(ZERO);
			covtrbnIO.setNumapp(ZERO);
			covtrbnIO.setSeqnbr(1);
		}
		if (firsttime.isTrue()) {
			covtlnbIO.setSeqnbr(covtrbnIO.getSeqnbr());
			covtlnbIO.setFunction(varcom.keeps);
			covtlnbIO.setFormat(formatsInner.covtlnbrec);
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
		}
		/* If  Plan  processing is to occur, then calculate the "credit"*/
		/* as follows:*/
		/* - subtract  the  'No  of  policies in the Plan' from the*/
		/*   first  COVTRBN  record  read  above (if no COVTRBN,*/
		/*       this is zero) from the 'No of policies in the Plan'*/
		/*       (from the Contract-Header).*/
		if (plan.isTrue()) {
			compute(wsaaCredit, 0).set((sub(chdrlnbIO.getPolinc(), covtrbnIO.getPolinc())));
		}
		/* If this is a single policy plan*/
		/*    if it is the first time (no COVTRBN records)*/
		/*     or there is no credit*/
		/*   - plan processing is not required.*/
		if (isEQ(chdrlnbIO.getPolinc(), 1)
		&& (isEQ(covtrbnIO.getPolinc(), 0)
		|| isEQ(covtrbnIO.getPolinc(), 1))) {
			wsaaPlanproc.set("N");
			sv.polincOut[varcom.nd.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
		}
	
		dialdownFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011 
		incomeProtectionflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
		premiumflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(incomeProtectionflag || premiumflag || dialdownFlag){
			callReadRCVDPF();
		}
		if(!incomeProtectionflag){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
			sv.poltypOut[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.waitperiodOut[varcom.nd.toInt()].set("N");
			sv.bentrmOut[varcom.nd.toInt()].set("N");
			sv.poltypOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getWaitperiod()!=null && !rcvdPFObject.getWaitperiod().trim().equals("") && isEQ(sv.waitperiod,SPACES)){
					sv.waitperiod.set(rcvdPFObject.getWaitperiod());
				}
				if(rcvdPFObject.getPoltyp()!=null && !rcvdPFObject.getPoltyp().trim().equals("") && isEQ(sv.poltyp,SPACES)){
					sv.poltyp.set(rcvdPFObject.getPoltyp());
				}
				if(rcvdPFObject.getBentrm()!=null && !rcvdPFObject.getBentrm().trim().equals("") && isEQ(sv.bentrm,SPACES)){
					sv.bentrm.set(rcvdPFObject.getBentrm());
				}
			}
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		//BRD-NBP-011 starts				
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
					}
				}
			}
		//BRD-NBP-011 ends
		exclFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.exclindOut[varcom.nd.toInt()].set("Y");
		}
		else{
			checkExcl();
		}
		
		/* Set  the  number of  policies  available  to  the  number  of*/
		/* policies on the plan.  If  COVTRBN  records are to be written*/
		/* for the first time  (as determined above), default the number*/
		/* applicable to the number available.*/
		wsaaFirstSeqnbr.set(covtrbnIO.getSeqnbr());
		wsaaNumavail.set(chdrlnbIO.getPolinc());
		if (firsttime.isTrue()) {
			covtrbnIO.setNumapp(wsaaNumavail);
			goTo(GotoLabel.cont1012);
		}
		sv.riskCessDate.set(covtrbnIO.getRiskCessDate());
		sv.premCessDate.set(covtrbnIO.getPremCessDate());
		sv.benCessDate.set(covtrbnIO.getBenCessDate());
		sv.riskCessAge.set(covtrbnIO.getRiskCessAge());
		sv.premCessAge.set(covtrbnIO.getPremCessAge());
		sv.benCessAge.set(covtrbnIO.getBenCessAge());
		sv.riskCessTerm.set(covtrbnIO.getRiskCessTerm());
		sv.premCessTerm.set(covtrbnIO.getPremCessTerm());
		sv.benCessTerm.set(covtrbnIO.getBenCessTerm());
		if (isNE(covtrbnIO.getSingp(), ZERO)) {
			sv.instPrem.set(covtrbnIO.getSingp());
			if (isEQ(covtrbnIO.getInstprem(), 0)) {
				sv.zbinstprem.set(covtrbnIO.getZbinstprem());
				sv.zlinstprem.set(covtrbnIO.getZlinstprem());
			}
		}
		else {
			sv.zbinstprem.set(covtrbnIO.getZbinstprem());
			sv.zlinstprem.set(covtrbnIO.getZlinstprem());
			sv.instPrem.set(covtrbnIO.getInstprem());
		}
		/*BRD-306 START */
		sv.loadper.set(covtrbnIO.getLoadper());
		sv.rateadj.set(covtrbnIO.getRateadj());
		sv.fltmort.set(covtrbnIO.getFltmort());
		sv.premadj.set(covtrbnIO.getPremadj());
		sv.adjustageamt.set(covtrbnIO.getAgeadj());
		/*BRD-306 END */
		
		loadingFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		/* BRD-306 ends */
		sv.sumin.set(covtrbnIO.getSumins());
		sv.mortcls.set(covtrbnIO.getMortcls());
		sv.liencd.set(covtrbnIO.getLiencd());
		if (isEQ(covtrbnIO.getJlife(), "01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
		sv.bappmeth.set(covtrbnIO.getBappmeth());
		
		getPurePrem1011CustomeSpecific();
	}
protected void checkExcl()
{
	exclpf=exclpfDAO.readRecord(covtlnbIO.getChdrcoy().toString(), covtlnbIO.getChdrnum().toString(),covtlnbIO.getCrtable().toString(),covtlnbIO.getLife().toString(),covtlnbIO.getCoverage().toString(),covtlnbIO.getRider().toString());
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
}
protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	rcvdPFObject.setChdrcoy(covtrbnIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covtrbnIO.getChdrnum().toString());
	rcvdPFObject.setLife(covtrbnIO.getLife().toString());
	rcvdPFObject.setCoverage(covtrbnIO.getCoverage().toString());
	rcvdPFObject.setRider(covtrbnIO.getRider().toString());
	rcvdPFObject.setCrtable(covtrbnIO.getCrtable().toString());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
}

protected void cont1012()
	{
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covtrbnIO.getNumapp());
		/* Read  the  contract  definition  details  from  T5688 for the*/
		/* contract  type  held  on  CHDRLNB. Access the version of this*/
		/* item for the original commencement date of the risk.*/
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5688, itdmIO.getItemtabl())
		|| isNE(chdrlnbIO.getCnttype(), itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/* Read  the  general  coverage/rider details from T5687 and the*/
		/* traditional/term  edit rules from T5606 for the coverage type*/
		/* held  on  COVTLNB.  Access  the version of this  item for the*/
		/* original commencement date of the risk.*/
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5687, itdmIO.getItemtabl())
		|| isNE(covtlnbIO.getCrtable(), itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			t5687rec.t5687Rec.set(SPACES);
			scrnparams.errorCode.set(errorsInner.f294);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		checkRacd1100();
		/* The following lines checks wether a window to Annuity*/
		/* Details is required. This is done by referring to*/
		/* Table T6625*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6625);
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		/* Check that the record is either found or at EOF*/
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/* Check if the BEGN point is the correct record*/
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t6625, itdmIO.getItemtabl())
		|| isNE(covtlnbIO.getCrtable(), itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		/* Check if the record is on the table*/
		getAnnt5000();
		if (isEQ(itdmIO.getStatuz(), varcom.oK)) {
			sv.anntindOut[varcom.nd.toInt()].set("N");
			sv.numappOut[varcom.pr.toInt()].set("Y");
			sv.numapp.set(chdrlnbIO.getPolinc());
			if (isEQ(anntIO.getStatuz(), varcom.mrnf)) {
				sv.anntind.set("X");
				sv.anntindOut[varcom.pr.toInt()].set("Y");
			}
			else {
				sv.anntind.set("+");
			}
		}
		else {
			sv.anntindOut[varcom.nd.toInt()].set("Y");
			sv.anntindOut[varcom.pr.toInt()].set("Y");
		}
		setupBonus1200();
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covtlnbIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1700();
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(covtlnbIO.getCrtable());
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		itdmIO.setItemtabl(tablesInner.t5606);
		if (isEQ(t5671rec.pgm[1], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
		wsbbCurrency.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5606, itdmIO.getItemtabl())
		|| isNE(wsbbTranCurrency, itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)) {
			t5606rec.t5606Rec.set(itdmIO.getGenarea());
		}
		else {
			t5606rec.t5606Rec.set(SPACES);
			t5606rec.ageIssageFrms.fill("0");
			t5606rec.ageIssageTos.fill("0");
			t5606rec.termIssageFrms.fill("0");
			t5606rec.termIssageTos.fill("0");
			t5606rec.premCessageFroms.fill("0");
			t5606rec.premCessageTos.fill("0");
			t5606rec.premCesstermFroms.fill("0");
			t5606rec.premCesstermTos.fill("0");
			t5606rec.riskCessageFroms.fill("0");
			t5606rec.riskCessageTos.fill("0");
			t5606rec.riskCesstermFroms.fill("0");
			t5606rec.riskCesstermTos.fill("0");
			t5606rec.benCessageFroms.fill("0");
			t5606rec.benCessageTos.fill("0");
			t5606rec.benCesstermFrms.fill("0");
			t5606rec.benCesstermTos.fill("0");
			t5606rec.sumInsMax.set(ZERO);
			t5606rec.sumInsMin.set(ZERO);
			if (isEQ(scrnparams.errorCode, SPACES)) {
				scrnparams.errorCode.set(errorsInner.f344);
			}
		}
		/* Read the latest premium tollerance allowed.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5667);
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(wsbbCurrency);
		itemIO.setItemitem(wsbbT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
		/* LIFE ASSURED AND JOINT LIFE DETAILS*/
		/* To obtain the life assured and joint-life details (if any) do*/
		/* the following;-*/
		/*  - read  the life details using LIFELNB (life number from*/
		/*       COVTLNB, joint life number '00').  Look up the name*/
		/*       from the  client  details  (CLTS)  and  format as a*/
		/*       "confirmation name".*/
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*    Save Main Life details within Working Storage for later use.*/
		wsaaAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		wsaaCltdob.set(lifelnbIO.getCltdob());
		wsaaSex.set(lifelnbIO.getCltsex());
		setOccCodeCustomerSpecific1012();
		/*    Read CLTS record for Life and format name.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		if(isNE(cltsIO.getStatcode(), SPACES)&& (incomeProtectionflag||premiumflag)){
			occuptationCode=cltsIO.getStatcode().toString();
		}
		
		/*  - read the joint life details using LIFELNB (life number*/
		/*       from COVTLNB,  joint  life number '01').  If found,*/
		/*       look up the name from the client details (CLTS) and*/
		/*       format as a "confirmation name".*/
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsbbSex.set(SPACES);
			wsbbAnbAtCcd.set(0);
			goTo(GotoLabel.cont1015);
		}
		wsbbAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		wsbbCltdob.set(lifelnbIO.getCltdob());
		wsbbSex.set(lifelnbIO.getCltsex());
		setJOccCodeCustomerSpecific1012();
		/*    Read CLTS record for Life and format name.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.jlifcnum.set(lifelnbIO.getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void setJOccCodeCustomerSpecific1012(){
	
}
protected void setOccCodeCustomerSpecific1012(){
	
}

protected void cont1015()
	{
		/* To  determine  which premium calculation method to use decide*/
		/* whether or  not  it  is  a Single or Joint-life case (it is a*/
		/* joint  life  case,  the  joint  life record was found above).*/
		/* Then:*/
		/*  - if it  is  a  single-life  case use, the single-method*/
		/*       from  T5687. The age to be used for validation will*/
		/*       be the age of the main life.*/
		/*  - if the joint-life indicator (from T5687) is blank, and*/
		/*       if  it  is  a Joint-life case, use the joint-method*/
		/*       from  T5687. The age to be used for validation will*/
		/*       be the age of the main life.*/
		/*  - if the  Joint-life  indicator  is  'N',  then  use the*/
		/*       Single-method.  But, if there is a joint-life (this*/
		/*       must be  a  rider  to have got this far) prompt for*/
		/*       the joint  life  indicator  to determine which life*/
		/*       the rider is to attach to.  In all other cases, the*/
		/*       joint life  indicator  should  be non-displayed and*/
		/*       protected.  The  age to be used for validation will*/
		/*       be the age  of the main or joint life, depending on*/
		/*       the one selected.*/
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			wsaaLifeind.set("S");
		}
		else {
			wsaaLifeind.set("J");
		}
		if (singlif.isTrue()) {
			itemIO.setItemitem(t5687rec.premmeth);
			sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			goTo(GotoLabel.premmeth1020);
		}
		if (isEQ(t5687rec.jlifePresent, SPACES)) {
			itemIO.setItemitem(t5687rec.jlPremMeth);
			sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			goTo(GotoLabel.premmeth1020);
		}
		/*  For a rider attaching to one life only for a joint life*/
		/*  case, if this is not the first time, set the life selection*/
		/*  indicator.*/
		itemIO.setItemitem(t5687rec.premmeth);
		if (nonfirst.isTrue()) {
			if (isEQ(covtrbnIO.getJlife(), "01")) {
				sv.select.set("J");
			}
			else {
				sv.select.set("L");
			}
		}
	}

protected void premmeth1020()
	{
		/*  - use the  premium-method  selected  from  T5687, if not*/
		/*       blank,  to access T5675.  This gives the subroutine*/
		/*       to use for the calculation.*/
		/*  - if the benefit billing method is not blank, non-display*/
		/*       and protect the premium field (so do not bother with*/
		/*       the rest of this).*/
		premReqd = "N";
		if (isNE(t5687rec.bbmeth, SPACES)) {
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.cont1030);
		}
		if (isEQ(itemIO.getItemitem(), SPACES)) {
			premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void cont1030()
	{
		sv.anbAtCcd.set(wsaaAnbAtCcd);
		sv.chdrnum.set(lifelnbIO.getChdrnum());
		sv.coverage.set(covtlnbIO.getCoverage());
		sv.currcd.set(payrIO.getCntcurr());
		sv.life.set(lifelnbIO.getLife());
		sv.rider.set(covtlnbIO.getRider());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/* The fields to  be displayed on the screen are determined from*/
		/* the entry in table T5606 read earlier as follows:*/
		/*  - if  the  maximum  and  minimum   benefit  amounts  are*/
		/*       both  zero,  non-display  and protect  the  benefit*/
		/*       amount.  If  the  minimum  and maximum are both the*/
		/*       same, display the amount protected. Otherwise allow*/
		/*       input.*/
		/* NOTE - the benefit amt applies to the PLAN, so if it is to*/
		/*        be defaulted, scale it down according to the number*/
		/*        of policies applicable.*/
		if (isEQ(t5606rec.sumInsMax, 0)
		&& isEQ(t5606rec.sumInsMin, 0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax, 0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5606rec.sumInsMax);
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(sv.sumin, sv.numapp), sv.numavail)));
			}
		}
		/*  - if  the amount is to be dispayed/entered,  look up the*/
		/*       benefit frequency short description (T3590).*/
		if (isNE(t5606rec.sumInsMax, 0)
		|| isNE(t5606rec.sumInsMin, 0)) {
			descIO.setDataArea(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(wsspcomn.company);
			descIO.setDesctabl(tablesInner.t3590);
			descIO.setDescitem(t5606rec.benfreq);
			descIO.setLanguage(wsspcomn.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)
			&& isNE(descIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			else {
				if (isEQ(descIO.getStatuz(), varcom.oK)) {
					sv.frqdesc.set(descIO.getShortdesc());
				}
				else {
					sv.frqdesc.fill("?");
				}
			}
		}
		/* If the benefit is a WOP & uses 'SUM' then do not display*/
		/* the benefit amount and description on screen....*/
		/*    PERFORM 5400-CHECK-T5602.                                    */
		/*  - if all   the   valid   mortality  classes  are  blank,*/
		/*       non-display  and  protect  this  field. If there is*/
		/*       only  one  mortality  class, display and protect it*/
		/*       (no validation will be required).*/
		if (isEQ(t5606rec.mortclss, SPACES)) {
			/*sv.mortclsOut[varcom.nd.toInt()].set("Y");*/
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if(isEQ(t5606rec.waitperiod, SPACES)){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
		}
		else{
			waitperiodFlag=true;
		}
		if(isEQ(t5606rec.bentrm, SPACES)){
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
		}
		else{
			bentrmFlag=true;
		}
		if(isEQ(t5606rec.poltyp, SPACES)){
			sv.poltypOut[varcom.nd.toInt()].set("Y");
		}
		else{
			poltypFlag=true;
		}
		if(isEQ(t5606rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		else{
			prmbasisFlag=true;
		}
		if (isNE(t5606rec.mortcls01, SPACES)
		&& isEQ(t5606rec.mortcls02, SPACES)
		&& isEQ(t5606rec.mortcls03, SPACES)
		&& isEQ(t5606rec.mortcls04, SPACES)
		&& isEQ(t5606rec.mortcls05, SPACES)
		&& isEQ(t5606rec.mortcls06, SPACES)) {
			sv.mortcls.set(t5606rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/*  - if all the valid lien codes are blank, non-display and*/
		/*       protect  this field. Otherwise, this is an optional*/
		/*       field.*/
		if (isEQ(t5606rec.liencds, SPACES)) {
			/*sv.liencdOut[varcom.nd.toInt()].set("Y");*/
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		/*  - using  the  age  next  birthday  (ANB at RCD) from the*/
		/*       applicable  life  (see above), look up Issue Age on*/
		/*       the AGE and TERM  sections.  If  the  age fits into*/
		/*       a "slot" in  one  of  these sections,  and the risk*/
		/*       cessation  limits   are  the   same,   default  and*/
		/*       protect the risk cessation fields. Also do the same*/
		/*       for the premium  cessation  details.  In this case,*/
		/*       also  calculate  the  risk  and  premium  cessation*/
		/*       dates.*/
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		if (nonfirst.isTrue()
		&& isEQ(covtrbnIO.getJlife(), "01")) {
			wszzCltdob.set(wsbbCltdob);
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
		}
		checkDefaults1800();
		if ((wsaaDefaultsInner.defaultRa.isTrue()
		|| wsaaDefaultsInner.defaultRt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(t5606rec.eaage, SPACES))) {
			riskCessDate1500();
		}
		if ((wsaaDefaultsInner.defaultPa.isTrue()
		|| wsaaDefaultsInner.defaultPt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(t5606rec.eaage, SPACES))) {
			premCessDate1550();
		}
		if ((wsaaDefaultsInner.defaultBa.isTrue()
		|| wsaaDefaultsInner.defaultBt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(t5606rec.eaage, SPACES))) {
			benCessDate1400();
		}
		if (isNE(t5606rec.eaage, SPACES)) {
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void cont1060()
	{
		/* OPTIONS AND EXTRAS*/
		/* If options and extras are  not  allowed (as defined by T5606)*/
		/* non-display and protect the fields.*/
		/* Otherwise,  read the  options  and  extras  details  for  the*/
		/* current coverage/rider.  If any  records  exist, put a '+' in*/
		/* the Options/Extras indicator (to show that there are some).*/
		if (isEQ(t5606rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			checkLext1900();
		}
		/* Read HBNF file to obtain Plan Code, Number of Lives, Covered*/
		/* Lives Details and Waiver of Premium....*/
		hbnfIO.setRecKeyData(SPACES);
		hbnfIO.setChdrcoy(covtlnbIO.getChdrcoy());
		hbnfIO.setChdrnum(covtlnbIO.getChdrnum());
		hbnfIO.setLife(covtlnbIO.getLife());
		hbnfIO.setCoverage(covtlnbIO.getCoverage());
		hbnfIO.setRider(covtlnbIO.getRider());
		hbnfIO.setFunction(varcom.readr);
		callHbnf1950();
		if (isEQ(hbnfIO.getStatuz(), varcom.mrnf)) {
			hbnfIO.setRecNonKeyData(SPACES);
		}
		if (isNE(hbnfIO.getStatuz(), varcom.mrnf)) {
			sv.mortcls.set(hbnfIO.getMortcls());
			sv.livesno.set(hbnfIO.getLivesno());
			sv.coverdtl.set("+");
			sv.waiverprem.set(hbnfIO.getWaiverprem());
			sv.zunit.set(hbnfIO.getZunit());
			sv.benpln.set(hbnfIO.getBenpln());
		}
		else {
			sv.mortcls.set(SPACES);
			sv.livesno.set(SPACES);
			sv.waiverprem.set(SPACES);
			sv.benpln.set(SPACES);
			sv.coverdtl.set("X");
			sv.zunit.set(ZERO);
		}
		checkWop1300();
	}

	/**
	* <pre>
	* ENQUIRY MODE
	* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to
	* protect  all  input  capable  fields  except  the  indicators
	* controlling  where  to  switch  to  next  (options and extras
	* indicator).
	* </pre>
	*/
protected void prot1070()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.bcessageOut[varcom.pr.toInt()].set("Y");
			sv.bcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.livesnoOut[varcom.pr.toInt()].set("Y");
			sv.zunitOut[varcom.pr.toInt()].set("Y");
			sv.waiverpremOut[varcom.pr.toInt()].set("Y");
			sv.waitperiodOut[varcom.pr.toInt()].set("Y");
			sv.bentrmOut[varcom.pr.toInt()].set("Y");
			sv.poltypOut[varcom.pr.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkRacd1100()
	{
		readRacd1110();
	}
protected void checkIPPmandatory()
{
	if(AppVars.getInstance().getAppConfig().isVpmsEnable())
	{
		if(incomeProtectionflag)
		{
			if((isEQ(sv.waitperiod,SPACES)) && waitperiodFlag )
				sv.waitperiodErr.set(errorsInner.e186);
			
			if((isEQ(sv.bentrm,SPACES)) && bentrmFlag )
				sv.bentrmErr.set(errorsInner.e186);
			
			if((isEQ(sv.poltyp,SPACES)) && poltypFlag )
				sv.poltypErr.set(errorsInner.e186);
		
		}
		
		if(premiumflag)
		{
			if((isEQ(sv.prmbasis,SPACES)) && prmbasisFlag )
				sv.prmbasisErr.set(errorsInner.e186);
		}
	}
}
protected void checkIPPfields()
{
	boolean t5606Flag=false;
	for(int counter=1; counter< t5606rec.waitperiod.length;counter++){
		if(isNE(sv.waitperiod,SPACES)){
			if (isEQ(t5606rec.waitperiod[counter], sv.waitperiod)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.waitperiod,SPACES)){
		sv.waitperiodErr.set("RFUY");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.bentrm.length;counter++){
		if(isNE(sv.bentrm,SPACES)){
			if (isEQ(t5606rec.bentrm[counter], sv.bentrm)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.bentrm,SPACES)){
		sv.bentrmErr.set("RFUZ");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.poltyp.length;counter++){
		if(isNE(sv.poltyp,SPACES)){
			if (isEQ(t5606rec.poltyp[counter], sv.poltyp)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.poltyp,SPACES)){
		sv.poltypErr.set("RFV0");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.prmbasis.length;counter++){
		if(isNE(sv.prmbasis,SPACES)){
			if (isEQ(t5606rec.prmbasis[counter], sv.prmbasis)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.prmbasis,SPACES)){
		sv.prmbasisErr.set("RFV1");// new error code
	}
	
}

protected void readRacd1110()
	{
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		racdlnbIO.setChdrnum(covtlnbIO.getChdrnum());
		racdlnbIO.setLife(covtlnbIO.getLife());
		racdlnbIO.setCoverage(covtlnbIO.getCoverage());
		racdlnbIO.setRider(covtlnbIO.getRider());
		racdlnbIO.setSeqno(ZERO);
		racdlnbIO.setPlanSuffix(ZERO);
		racdlnbIO.setCestype("2");
		racdlnbIO.setFunction(varcom.begn);
		racdlnbIO.setFormat(formatsInner.racdlnbrec);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(), varcom.oK)
		&& isNE(racdlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdlnbIO.getParams());
			syserrrec.statuz.set(racdlnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(racdlnbIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(racdlnbIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(racdlnbIO.getLife(), covtlnbIO.getLife())
		|| isNE(racdlnbIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(racdlnbIO.getRider(), covtlnbIO.getRider())
		|| isNE(racdlnbIO.getPlanSuffix(), ZERO)
		|| isNE(racdlnbIO.getSeqno(), ZERO)
		|| isNE(racdlnbIO.getCestype(), "2")
		|| isEQ(racdlnbIO.getStatuz(), varcom.endp)) {
			if (isEQ(sv.ratypind, "X")) {
				/*NEXT_SENTENCE*/
			}
			else {
				sv.ratypind.set(SPACES);
				sv.ratypindOut[varcom.nd.toInt()].set("Y");
				sv.ratypindOut[varcom.pr.toInt()].set("Y");
			}
		}
		else {
			sv.ratypind.set("+");
			sv.ratypindOut[varcom.nd.toInt()].set("N");
			sv.ratypindOut[varcom.pr.toInt()].set("N");
		}
	}

protected void setupBonus1200()
	{
		para1200();
	}

protected void para1200()
	{
		/* Check if Coverage/Rider is a SUM product. If not SUM product*/
		/* protect field.*/
		/* If SUM product and default exists setup BAPPMETH and protect*/
		/* field. If SUM product and default not setup allow entry.*/
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covtlnbIO.getChdrcoy());
		itemIO.setItemitem(covtlnbIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			return ;
		}
		t6005rec.t6005Rec.set(itemIO.getGenarea());
		if (isEQ(t6005rec.ind, "1")) {
			sv.bappmeth.set(t6005rec.bappmeth01);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "2")) {
			sv.bappmeth.set(t6005rec.bappmeth02);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "3")) {
			sv.bappmeth.set(t6005rec.bappmeth03);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "4")) {
			sv.bappmeth.set(t6005rec.bappmeth04);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "5")) {
			sv.bappmeth.set(t6005rec.bappmeth05);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkWop1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start1300();
				case readCovtrid1310: 
					readCovtrid1310();
					readTr5171320();
				case setDefault1380: 
					setDefault1380();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1300()
	{
		wsaaWopFlag.set("N");
		/*    Check WOP Rider via logical View*/
		covtridIO.setDataKey(SPACES);
		covtridIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtridIO.setChdrnum(covtlnbIO.getChdrnum());
		covtridIO.setLife(covtlnbIO.getLife());
		covtridIO.setCoverage(covtlnbIO.getCoverage());
		covtridIO.setCrtable(SPACES);
		covtridIO.setFunction("BEGN");
	}

protected void readCovtrid1310()
	{
		SmartFileCode.execute(appVars, covtridIO);
		if (isNE(covtridIO.getStatuz(), varcom.oK)
		&& isNE(covtridIO.getStatuz(), varcom.mrnf)
		&& isNE(covtridIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtridIO.getParams());
			fatalError600();
		}
		if (isEQ(covtridIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.setDefault1380);
		}
		if (isNE(covtlnbIO.getChdrcoy(), covtridIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), covtridIO.getChdrnum())
		|| isNE(covtlnbIO.getLife(), covtridIO.getLife())
		|| isNE(covtlnbIO.getCoverage(), covtridIO.getCoverage())) {
			goTo(GotoLabel.setDefault1380);
		}
	}

protected void readTr5171320()
	{
		/*   If the component is found on table TR517, it is a Waiver Of*/
		/*   Premium component*/
		initialize(tr517rec.tr517Rec);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(covtridIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmIO.getItemcoy(), wsspcomn.company)
		&& isEQ(itdmIO.getItemtabl(), tablesInner.tr517)
		&& isEQ(itdmIO.getItemitem(), covtridIO.getCrtable())) {
			wsaaWopFlag.set("Y");
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.setDefault1380);
		}
		covtridIO.setFunction("NEXTR");
		goTo(GotoLabel.readCovtrid1310);
	}

protected void setDefault1380()
	{
		/*  To default for the WOP*/
		if (wsaaWopFound.isTrue()) {
			sv.waiverprem.set("Y");
			wsspcomn.flag.set("Y");
		}
		/*EXIT*/
	}

protected void benCessDate1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1400();
				case benCessTerm1410: 
					benCessTerm1410();
				case exit1490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1400()
	{
		if (isEQ(sv.benCessAge, 0)) {
			goTo(GotoLabel.benCessTerm1410);
		}
		if (isEQ(t5606rec.eaage, "A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.benCessAge, wszzAnbAtCcd));
		}
		if (isEQ(t5606rec.eaage, "E")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.benCessAge);
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.bcesdteErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1490);
		}
		sv.benCessDate.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1490);
	}

protected void benCessTerm1410()
	{
		if (isEQ(sv.benCessTerm, 0)) {
			sv.bcesdteErr.set(errorsInner.e186);
			return ;
		}
		if (isEQ(t5606rec.eaage, "A")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.freqFactor.set(sv.benCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t5606rec.eaage, "E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.benCessTerm, wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(), wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.bcesdteErr.set(datcon2rec.statuz);
			return ;
		}
		sv.benCessDate.set(datcon2rec.intDate2);
	}

protected void riskCessDate1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1500();
				case riskCessTerm1510: 
					riskCessTerm1510();
				case exit1540: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1500()
	{
		if (isEQ(sv.riskCessAge, 0)) {
			goTo(GotoLabel.riskCessTerm1510);
		}
		if (isEQ(t5606rec.eaage, "A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.riskCessAge, wszzAnbAtCcd));
		}
		if (isEQ(t5606rec.eaage, "E")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.riskCessAge);
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1540);
		}
		sv.riskCessDate.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1540);
	}

protected void riskCessTerm1510()
	{
		if (isEQ(sv.riskCessTerm, 0)) {
			sv.rcesdteErr.set(errorsInner.e186);
			return ;
		}
		if (isEQ(t5606rec.eaage, "A")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.freqFactor.set(sv.riskCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t5606rec.eaage, "E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.riskCessTerm, wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(), wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
			return ;
		}
		sv.riskCessDate.set(datcon2rec.intDate2);
	}

protected void premCessDate1550()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1550();
				case premCessTerm1560: 
					premCessTerm1560();
				case exit1590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1550()
	{
		if (isEQ(sv.premCessAge, 0)) {
			goTo(GotoLabel.premCessTerm1560);
		}
		if (isEQ(t5606rec.eaage, "A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge, wszzAnbAtCcd));
		}
		if (isEQ(t5606rec.eaage, "E")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.premCessAge);
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1590);
		}
		sv.premCessDate.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1590);
	}

protected void premCessTerm1560()
	{
		if (isEQ(sv.premCessTerm, 0)) {
			return ;
		}
		if (isEQ(t5606rec.eaage, "A")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.freqFactor.set(sv.premCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t5606rec.eaage, "E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.premCessTerm, wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(), wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
			return ;
		}
		sv.premCessDate.set(datcon2rec.intDate2);
	}

protected void callDatcon21600()
	{
		/*PARA*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadHeading1700()
	{
		try {
			loadScreen1710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadScreen1710()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(30); !(isLT(wsaaX, 1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(30, wsaaX));
		if (isNE(wsaaY, 0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		//wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1730();
		}
		/*   WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		//wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1790);
	}

protected void moveChar1730()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY, wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void checkDefaults1800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					searchTable1810();
				case nextColumn1820: 
					nextColumn1820();
				case moveDefaults1850: 
					moveDefaults1850();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*  - using  the  age  next  birthday  (ANB at RCD) from the
	*       applicable  life  (see above), look up Issue Age on
	*       the AGE and TERM  sections.  If  the  age fits into
	*       a "slot" in  one  of these sections, and the
	*       risk cessation limits  are  the  same,  default and
	*       protect the risk cessation fields. Also do the same
	*       for the premium  cessation  details.  In this case,
	*       also  calculate  the  risk  and  premium  cessation
	*       dates.
	* </pre>
	*/
protected void searchTable1810()
	{
		sub1.set(0);
		wsaaDefaultsInner.wsaaDefaults.set(SPACES);
	}

protected void nextColumn1820()
	{
		sub1.add(1);
		if (isGT(sub1, wsaaMaxOcc)) {
			goTo(GotoLabel.moveDefaults1850);
		}
		if (!wsaaDefaultsInner.nodefRa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.riskCessageFrom[sub1.toInt()], t5606rec.riskCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRa.isTrue()
				&& !wsaaDefaultsInner.defaultRt.isTrue()) {
					wsddRiskCessAge.set(t5606rec.riskCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.premCessageFrom[sub1.toInt()], t5606rec.premCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPa.isTrue()
				&& !wsaaDefaultsInner.defaultPt.isTrue()) {
					wsddPremCessAge.set(t5606rec.premCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefBa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.benCessageFrom[sub1.toInt()], t5606rec.benCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultBa.isTrue()
				&& !wsaaDefaultsInner.defaultBt.isTrue()) {
					wsddBenCessAge.set(t5606rec.benCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultBa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultBa.set("N");
					wsaaDefaultsInner.wsaaDefaultBt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefRt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.riskCesstermFrom[sub1.toInt()], t5606rec.riskCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRt.isTrue()
				&& !wsaaDefaultsInner.defaultRa.isTrue()) {
					wsddRiskCessTerm.set(t5606rec.riskCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.premCesstermFrom[sub1.toInt()], t5606rec.premCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPt.isTrue()
				&& !wsaaDefaultsInner.defaultPa.isTrue()) {
					wsddPremCessTerm.set(t5606rec.premCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefBt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.benCesstermFrm[sub1.toInt()], t5606rec.benCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultBt.isTrue()
				&& !wsaaDefaultsInner.defaultBa.isTrue()) {
					wsddBenCessTerm.set(t5606rec.benCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultBt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultBa.set("N");
					wsaaDefaultsInner.wsaaDefaultBt.set("N");
				}
			}
		}
		goTo(GotoLabel.nextColumn1820);
	}

protected void moveDefaults1850()
	{
		if (wsaaDefaultsInner.defaultRa.isTrue()) {
			sv.riskCessAge.set(wsddRiskCessAge);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultRt.isTrue()) {
			sv.riskCessTerm.set(wsddRiskCessTerm);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPa.isTrue()) {
			sv.premCessAge.set(wsddPremCessAge);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPt.isTrue()) {
			sv.premCessTerm.set(wsddPremCessTerm);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultBa.isTrue()) {
			sv.benCessAge.set(wsddBenCessAge);
			sv.bcessageOut[varcom.pr.toInt()].set("Y");
			sv.bcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultBt.isTrue()) {
			sv.benCessTerm.set(wsddBenCessTerm);
			sv.bcessageOut[varcom.pr.toInt()].set("Y");
			sv.bcesstrmOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkLext1900()
	{
		readLext1910();
	}

protected void readLext1910()
	{
		lextIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lextIO.setChdrnum(covtlnbIO.getChdrnum());
		lextIO.setLife(covtlnbIO.getLife());
		lextIO.setCoverage(covtlnbIO.getCoverage());
		lextIO.setRider(covtlnbIO.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(), lextIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), lextIO.getChdrnum())
		|| isNE(covtlnbIO.getLife(), lextIO.getLife())
		|| isNE(covtlnbIO.getCoverage(), lextIO.getCoverage())
		|| isNE(covtlnbIO.getRider(), lextIO.getRider())) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(), varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
	}

protected void callHbnf1950()
	{
		/*READ-HBNF*/
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)
		&& isNE(hbnfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		try {
			preStart();
			callScreenIo2010();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void preStart()
	{
		/*     SPECIAL EXIT PROCESSING*/
		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (forcedKill.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/* Test the POVR file for the existance of a Premium Breakdown*/
		/* record. If one exists then allow the option to display the*/
		/* select window for enquiry....if not then protect.*/
		/* Test here so that the latest POVR is retreived when either*/
		/* returning from a selection or having calculated new values.*/
		readPovr5300();
		if (isEQ(povrIO.getStatuz(), varcom.endp)
		|| isNE(povrIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(povrIO.getChdrnum(), lifelnbIO.getChdrnum())
		|| isNE(povrIO.getLife(), lifelnbIO.getLife())
		|| isNE(povrIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(povrIO.getRider(), covtlnbIO.getRider())) {
			sv.pbind.set(SPACES);
			sv.pbindOut[varcom.nd.toInt()].set("Y");
			sv.pbindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.pbind.set("+");
			sv.pbindOut[varcom.nd.toInt()].set("N");
			sv.pbindOut[varcom.pr.toInt()].set("N");
			wsaaPayrBillfreq.set(payrIO.getBillfreq());
			wsaaPovrInstamnt.set(ZERO);
			sv.instPrem.set(ZERO);
			for (wsaaSub.set(1); !(isGT(wsaaSub, 25)); wsaaSub.add(1)){
				compute(wsaaPovrInstamnt, 3).setRounded((div(povrIO.getAnnamnt(wsaaSub), wsaaPayrBillfreq)));
				sv.instPrem.add(wsaaPovrInstamnt);
			}
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			checkCalcTax6100();
		}
		//ILIFE-1223 STARTS
		//if (isEQ(wsspcomn.flag, "I") || isEQ(sv.taxamt, ZERO)) {
		//Ticket #ILIFE-1330 start by akhan203 
		/*if (isEQ(sv.taxamt, ZERO)) {
			//Ticket #ILIFE-1330 end
		//ILIFE-1223 ENDS
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.taxindOut[varcom.nd.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("Y");
		}*/
		//ILIFE-1702 STARTS BY SLAKKALA
		if(isEQ(wsspcomn.flag, "I")){
				
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
						
		}
		//ILIFE-1702 ENDS 
	}

protected void callScreenIo2010()
	{
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2001();
					cont2040();
				case redisplay2480: 
					redisplay2480();
				case exit2490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2001()
	{
		/*    CALL 'SR676IO' USING        SCRN-SCREEN-PARAMS*/
		/*                                SR676-DATA-AREA.*/
		/* Screen errors are now handled in the calling program.*/
		/*    PERFORM 200-SCREEN-ERRORS.*/
		wsspcomn.edterror.set(varcom.oK);
		/* If CF11 (KILL) is requested and the current credit is zero or*/
		/* equal to the number of policies in the plan (all or nothing),*/
		/* then skip the  validation.  Otherwise,  highlight  this as an*/
		/* error and then skip the remainder of the validation.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			if (isEQ(wsaaCredit, 0)
			|| isEQ(wsaaCredit, chdrlnbIO.getPolinc())) {
				goTo(GotoLabel.exit2490);
			}
			else {
				sv.numappErr.set(errorsInner.g622);
				goTo(GotoLabel.redisplay2480);
			}
		}
		/*VALIDATE*/
		/* If  in  enquiry  mode,  skip  all field validation EXCEPT the*/
		/* options/extras indicator.*/
		if (isNE(wsspcomn.flag, "I")) {
			editCoverage2500();
		}
	}

protected void cont2040()
	{
		/* Validate for Units....                                          */
		if (isEQ(sv.zunit, ZERO)) {
			sv.zunitErr.set(errorsInner.w067);
		}
		/* Validate for Plan Code....                                      */
		if (isEQ(sv.benpln, SPACES)) {
			sv.benplnErr.set(errorsInner.f646);
			goTo(GotoLabel.redisplay2480);
		}
		/* Validate for valid Lives Number....*/
		wsaaLivesno.set(sv.livesno);
		if (!validLivesno.isTrue()) {
			sv.livesnoErr.set(errorsInner.rl87);
		}
		checkLivesno6000();
		if (isEQ(sv.livesno, 4)) {
			if (isLTE(wsaaHbnfLivesnoNum, 3)) {
				sv.coverdtl.set("X");
			}
		}
		else {
			if (isNE(wsaaHbnfLivesnoChar, sv.livesno)) {
				sv.coverdtl.set("X");
			}
		}
		/* Covered Lives details has to be either 'X' or '+' only....*/
		if (isNE(sv.coverdtl, "+")
		&& isNE(sv.coverdtl, "X")) {
			sv.coverdtlErr.set(errorsInner.np44);
		}
		/* Waiver of Premium has to be either Y/N/' '.*/
		wsaaWaiverprem.set(sv.waiverprem);
		if (!validWaiverprem.isTrue()) {
			sv.waiverpremErr.set(errorsInner.g616);
		}
		if (isEQ(sv.waiverprem, "Y")) {
			if (isEQ(tr686rec.waivercode, SPACES)) {
				sv.waiverpremErr.set(errorsInner.rl84);
			}
		}
		/* Waiver of Premium will not be applied, if there is not WOP*/
		/* rider on the base plan*/
		if (wsaaWopNotFound.isTrue()
		&& isEQ(sv.waiverprem, "Y")) {
			if (isNE(tr686rec.waivercode, SPACES)) {
				sv.waiverpremErr.set(errorsInner.e316);
			}
		}
		/* Waiver of Premium must be applied, if there is WOP*/
		/* rider on the base plan*/
		if (wsaaWopFound.isTrue()
		&& isEQ(sv.waiverprem, "N")) {
			if (isNE(tr686rec.waivercode, SPACES)) {
				sv.waiverpremErr.set(errorsInner.rl86);
			}
		}
		/* View Plan has to be either 'X' or '+' only....*/
		if (isNE(sv.viewplan, " ")
		&& isNE(sv.viewplan, "+")
		&& isNE(sv.viewplan, "X")) {
			sv.viewplanErr.set(errorsInner.g620);
		}
		/* Taxcode ind has to be either 'X' or '+' only....                */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		/* If  options/extras already exist, there will be a '+' in this*/
		/* field.  A  request  to access the details is made by entering*/
		/* 'X'.  No  other  values  (other  than  blank) are allowed. If*/
		/* options  and  extras  are  requested,  DO  NOT  CALCULATE THE*/
		/* PREMIUM THIS TIME AROUND.*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind, "+")
		&& isNE(sv.optextind, "X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		/* Special Term  will not be applied, if there is not WOP*/
		/* rider on the base plan, else display error*/
//		if (wsaaWopNotFound.isTrue()) {
//			if ((isEQ(sv.optextind, "X")
//			|| isEQ(sv.optextind, "+"))) {
//				sv.optextindErr.set(errorsInner.e374);
//			}
//		}
		/* Special Term  should be applied, if there is WOP*/
		/* rider on the base plan, else display error*/
		/* Commenting for the ISSUE -314 as per Naren the code is commented.
		 * COMMENTS:
		 * its not required that special terms be entered for WOPU rider, kindly resolve this issue
		 */
		/*if (wsaaWopFound.isTrue()) {
			if (isEQ(sv.optextind, " ")) {
				sv.optextindErr.set(errorsInner.np44);
			}
		}*/
		/* If  reassurance   already exists, there will be a '+' in this*/
		/* field.  A  request  to access the details is made by entering*/
		/* 'X'.  No  other  values  (other  than  blank) are allowed.*/
		if (isNE(sv.ratypind, " ")
		&& isNE(sv.ratypind, "+")
		&& isNE(sv.ratypind, "X")) {
			sv.ratypindErr.set(errorsInner.g620);
		}
		/* If the item selected prompts Annuity details, 'X' will*/
		/* occur here. If details exist '+' will occur. No other*/
		/* values are allowed.*/
		if (isNE(sv.anntind, " ")
		&& isNE(sv.anntind, "+")
		&& isNE(sv.anntind, "X")) {
			sv.anntindErr.set(errorsInner.g620);
		}
		/* Check the premium breakdown indicator.*/
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind, "+")
		&& isNE(sv.pbind, "X")) {
			sv.pbindErr.set(errorsInner.g620);
		}
		if (exclFlag && isNE(sv.exclind, " ")
				&& isNE(sv.exclind, "+")
				&& isNE(sv.exclind, "X")) {
					sv.exclindErr.set(errorsInner.g620);
				}
		/* Check to see if BONUS APPLICATION METHOD is valid for*/
		/* coverage/rider.*/
		if (isNE(sv.bappmeth, SPACES)
		&& isNE(sv.bappmeth, t6005rec.bappmeth01)
		&& isNE(sv.bappmeth, t6005rec.bappmeth02)
		&& isNE(sv.bappmeth, t6005rec.bappmeth03)
		&& isNE(sv.bappmeth, t6005rec.bappmeth04)
		&& isNE(sv.bappmeth, t6005rec.bappmeth05)) {
			sv.bappmethErr.set(errorsInner.d352);
		}
		/* If everything else is O-K calculate the Installment Premium.*/
		calcSumins2650();
		if(isEQ(sv.dialdownoptionErr,"E034") && !dialdownFlag)
		{
			sv.dialdownoptionErr.set(SPACES);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.redisplay2480);
		}
		if (isEQ(sv.sumin, ZERO)) {
			sv.suminErr.set(errorsInner.h113);
		}
		cont2041CustomerSpecific();
		calcPremium2700();
		if (isNE(sv.optextind, "X")
		&& isNE(sv.anntind, "X")
		&& isNE(sv.coverdtl, "X")
		&& isNE(sv.viewplan, "X")
		&& isEQ(sv.errorIndicators, SPACES)
		&& isNE(wsspcomn.flag, "I")) {
			calcPremium2700();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.redisplay2480);
		}
		compute(sv.zbinstprem, 2).set(sub(sv.instPrem, sv.zlinstprem));
		/* If 'ROLD' was entered,check this is not the first page.*/
		if (isEQ(scrnparams.statuz, varcom.rold)
		&& isEQ(wsaaFirstSeqnbr, covtrbnIO.getSeqnbr())) {
			scrnparams.errorCode.set(errorsInner.e027);
			goTo(GotoLabel.redisplay2480);
		}
		validateOccupationOrOccupationClass();	//ICIL-1494
		/* If 'CALC' was entered then re-display the screen.*/
		if (isNE(scrnparams.statuz, varcom.calc)) {
			goTo(GotoLabel.exit2490);
		}
	}

	protected void cont2041CustomerSpecific() {
		if (isLT(wsaaSumins, wsaaMinsumins)) {
			sv.suminErr.set(errorsInner.rl85);
		}
	}
protected void redisplay2480()
	{
		wsspcomn.edterror.set("Y");
	}

protected void validateOccupationOrOccupationClass() {
	isFollowUpRequired=false;
	NBPRP056Permission  = FeaConfg.isFeatureExist("2",NBPRP056, appVars, "IT");
	if(NBPRP056Permission &&  lifelnbIO != null) {
		readTA610();
		String occupation = lifelnbIO.getOccup().toString();
		if( occupation != null && !occupation.trim().isEmpty()) {
			for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
					&& isNE(ta610rec.occclassdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
				if(isEQ(occupation,ta610rec.occclassdes[wsaaCount.toInt()])){
					isFollowUpRequired = true;
					scrnparams.errorCode.set(errorsInner.rrsu); 
					break;
				}
			}
			
			if(!isFollowUpRequired){
				getOccupationClass2900(occupation);
				for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
						&& isNE(ta610rec.occcode[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
					if(isEQ(wsaaOccupationClass,ta610rec.occcode[wsaaCount.toInt()])){
						isFollowUpRequired = true;
						scrnparams.errorCode.set(errorsInner.rrsu); 
						break;
					}
				}	
			}
			
		}
	}
	
}



private void readTA610() {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(tA610);
	itempf.setItemitem(covtlnbIO.getCrtable().toString());
	itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
	itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
	ta610List = itempfDAO.findByItemDates(itempf);	//ICIL-1494
	if (ta610List.size()>0 && ta610List.get(0).getGenarea()!=null) {
		ta610rec.tA610Rec.set(StringUtil.rawToString(ta610List.get(0).getGenarea()));
	}	  
}

protected void getOccupationClass2900(String occupation) {
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl(t3644);
	   itempf.setItemitem(occupation);
	   itempf = itempfDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
	   wsaaOccupationClass.set(t3644rec.occupationClass.toString());
}
	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void editCoverage2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					editFund2500();
					checkSumin2525();
				case checkRcessFields2530: 
					checkRcessFields2530();
					checkPcessFields2535();
					checkBcessFields2537();
					checkAgeTerm2540();
				case ageAnniversary2541: 
					ageAnniversary2541();
				case term2542: 
					term2542();
				case termExact2543: 
					termExact2543();
				case check2544: 
					check2544();
				case checkOccurance2545: 
					checkOccurance2545();
				case checkTermFields2550: 
					checkTermFields2550();
				case checkComplete2555: 
					checkComplete2555();
				case checkLiencd2570: 
					checkLiencd2570();
				case loop2575: 
					loop2575();
				case checkMore2580: 
					checkMore2580();
				case exit2590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void editFund2500()
	{
		/* If plan processing, and no policies applicable,*/
		/*   skip the validation as this COVTRBN is to be deleted.*/
		if (plan.isTrue()
		&& isEQ(sv.numapp, 0)) {
			goTo(GotoLabel.exit2590);
		}
		if (isEQ(sv.numapp, ZERO)) {
			sv.numappErr.set(errorsInner.l001);
		}
		/* Before  the  premium amount is calculated, the screen must be*/
		/* valid.  So  all  editing  is  completed before the premium is*/
		/* calculated.*/
		/*  1) Check  the  benefit amt,  if  applicable, against the*/
		/*       limits (NB. these apply to the plan, so adjust first).*/
		/*    - if only one benefit amt is allowed, re-calculate plan*/
		/*      level benefit amt (if applicable).*/
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax, 0)) {
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(t5606rec.sumInsMin, sv.numapp), sv.polinc)));
			}
		}
	}

protected void checkSumin2525()
	{
		if (plan.isTrue()) {
			compute(wsaaSumin, 1).setRounded((div(mult(sv.sumin, sv.polinc), sv.numapp)));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)) {
			goTo(GotoLabel.checkRcessFields2530);
		}
	}

	/**
	* <pre>
	*    IF WSAA-SUMIN               < T5606-SUM-INS-MIN
	*        MOVE E416               TO SR676-SUMIN-ERR.
	*    IF WSAA-SUMIN               > T5606-SUM-INS-MAX
	*        MOVE E417               TO SR676-SUMIN-ERR.
	*  2) Check the consistency of the risk age and term fields
	*       and  premium  age and term fields. Either, risk age
	*       and  premium age must be used or risk term and
	*       premium  term  must  be  used.  They  must  not  be
	*       combined. Note that  these  only need validating if
	*       they were not defaulted.
	*     NOTE: Age and Term fields may now be mixed.
	* </pre>
	*/
protected void checkRcessFields2530()
	{
		if (isEQ(sv.select, "J")) {
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
			wszzCltdob.set(wsbbCltdob);
		}
		else {
			wszzAnbAtCcd.set(wsaaAnbAtCcd);
			wszzCltdob.set(wsaaCltdob);
		}
		if (isNE(t5687rec.jlifePresent, SPACES)
		&& jointlif.isTrue()) {
			checkDefaults1800();
		}
		if (isGT(sv.riskCessAge, 0)
		&& isGT(sv.riskCessTerm, 0)) {
			sv.rcessageErr.set(errorsInner.f220);
			sv.rcesstrmErr.set(errorsInner.f220);
		}
		if (isNE(t5606rec.eaage, SPACES)
		&& isEQ(sv.riskCessAge, 0)
		&& isEQ(sv.riskCessTerm, 0)) {
			sv.rcessageErr.set(errorsInner.e560);
			sv.rcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkPcessFields2535()
	{
		if (isGT(sv.premCessAge, 0)
		&& isGT(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)
		&& isEQ(sv.rcessageErr, SPACES)
		&& isEQ(sv.rcesstrmErr, SPACES)) {
			sv.premCessAge.set(sv.riskCessAge);
			sv.premCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5606rec.eaage, SPACES)
		&& isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkBcessFields2537()
	{
		if (isGT(sv.benCessAge, 0)
		&& isGT(sv.benCessTerm, 0)) {
			sv.bcessageErr.set(errorsInner.f220);
			sv.bcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.benCessAge, 0)
		&& isEQ(sv.benCessTerm, 0)
		&& isEQ(sv.rcessageErr, SPACES)
		&& isEQ(sv.rcesstrmErr, SPACES)) {
			sv.benCessAge.set(sv.riskCessAge);
			sv.benCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5606rec.eaage, SPACES)
		&& isEQ(sv.benCessAge, 0)
		&& isEQ(sv.benCessTerm, 0)) {
			sv.bcessageErr.set(errorsInner.e560);
			sv.bcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkAgeTerm2540()
	{
		if ((isNE(sv.rcessageErr, SPACES))
		|| (isNE(sv.rcesstrmErr, SPACES))
		|| (isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))
		|| (isNE(sv.bcessageErr, SPACES))
		|| (isNE(sv.bcesstrmErr, SPACES))) {
			/*        GO TO 2560-CHECK-MORTCLS.                                */
			goTo(GotoLabel.checkLiencd2570);
		}
		if ((isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))
		|| (isNE(sv.rcessageErr, SPACES))
		|| (isNE(sv.rcesstrmErr, SPACES))) {
			/*        GO TO 2560-CHECK-MORTCLS.                                */
			goTo(GotoLabel.checkLiencd2570);
		}
		/*  To get this far, everything must be consistant,*/
		/*   now cross check/calculate the risk and premium cessasion*/
		/*   dates. Cross validate these calculated dates against the*/
		/*   edit table (T5606).*/
		if (isNE(t5606rec.eaage, SPACES)
		|| isEQ(sv.riskCessDate, varcom.vrcmMaxDate)) {
			riskCessDate1500();
		}
		else {
			if (isEQ(sv.rcesdteErr, SPACES)) {
				if (isNE(sv.riskCessAge, 0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.riskCessAge);
					callDatcon21600();
					if (isLT(datcon2rec.intDate2, sv.riskCessDate)) {
						sv.rcessageErr.set(errorsInner.h040);
						sv.rcesdteErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21600();
						if (isGTE(datcon2rec.intDate2, sv.riskCessDate)) {
							sv.rcessageErr.set(errorsInner.h040);
							sv.rcesdteErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.riskCessTerm, 0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.riskCessTerm);
						callDatcon21600();
						if (isLT(datcon2rec.intDate2, sv.riskCessDate)) {
							sv.rcesstrmErr.set(errorsInner.h040);
							sv.rcesdteErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21600();
							if (isGTE(datcon2rec.intDate2, sv.riskCessDate)) {
								sv.rcesstrmErr.set(errorsInner.h040);
								sv.rcesdteErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(t5606rec.eaage, SPACES)
		|| isEQ(sv.premCessDate, varcom.vrcmMaxDate)) {
			premCessDate1550();
		}
		else {
			if (isEQ(sv.pcesdteErr, SPACES)) {
				if (isNE(sv.premCessAge, 0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.premCessAge);
					callDatcon21600();
					if (isLT(datcon2rec.intDate2, sv.premCessDate)) {
						sv.pcessageErr.set(errorsInner.h040);
						sv.pcesdteErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21600();
						if (isGTE(datcon2rec.intDate2, sv.premCessDate)) {
							sv.pcessageErr.set(errorsInner.h040);
							sv.pcesdteErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.premCessTerm, 0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.premCessTerm);
						callDatcon21600();
						if (isLT(datcon2rec.intDate2, sv.premCessDate)) {
							sv.pcesstrmErr.set(errorsInner.h040);
							sv.pcesdteErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21600();
							if (isGTE(datcon2rec.intDate2, sv.premCessDate)) {
								sv.pcesstrmErr.set(errorsInner.h040);
								sv.pcesdteErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(t5606rec.eaage, SPACES)
		|| isEQ(sv.benCessDate, varcom.vrcmMaxDate)) {
			benCessDate1400();
		}
		else {
			if (isEQ(sv.bcesdteErr, SPACES)) {
				if (isNE(sv.benCessAge, 0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.benCessAge);
					callDatcon21600();
					if (isLT(datcon2rec.intDate2, sv.benCessDate)) {
						sv.bcessageErr.set(errorsInner.h040);
						sv.bcesdteErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21600();
						if (isGTE(datcon2rec.intDate2, sv.benCessDate)) {
							sv.bcessageErr.set(errorsInner.h040);
							sv.bcesdteErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.benCessTerm, 0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.benCessTerm);
						callDatcon21600();
						if (isLT(datcon2rec.intDate2, sv.benCessDate)) {
							sv.bcesstrmErr.set(errorsInner.h040);
							sv.bcesdteErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21600();
							if (isGTE(datcon2rec.intDate2, sv.benCessDate)) {
								sv.bcesstrmErr.set(errorsInner.h040);
								sv.bcesdteErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(sv.rcesdteErr, SPACES)
		|| isNE(sv.pcesdteErr, SPACES)
		|| isNE(sv.bcesdteErr, SPACES)) {
			/*       GO TO 2560-CHECK-MORTCLS.                                 */
			goTo(GotoLabel.checkLiencd2570);
		}
		if (isEQ(sv.premCessDate, varcom.vrcmMaxDate)) {
			sv.premCessDate.set(sv.riskCessDate);
		}
		if (isEQ(sv.benCessDate, varcom.vrcmMaxDate)) {
			sv.benCessDate.set(sv.riskCessDate);
		}
		if (isGT(sv.premCessDate, sv.riskCessDate)) {
			sv.pcesdteErr.set(errorsInner.e566);
			sv.rcesdteErr.set(errorsInner.e566);
		}
		if (isNE(sv.rcesdteErr, SPACES)
		|| isNE(sv.pcesdteErr, SPACES)
		|| isNE(sv.bcesdteErr, SPACES)) {
			/*       GO TO 2560-CHECK-MORTCLS.                                 */
			goTo(GotoLabel.checkLiencd2570);
		}
		if (isEQ(t5606rec.eaage, "A")) {
			goTo(GotoLabel.ageAnniversary2541);
		}
		if (isEQ(sv.riskCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		if (isEQ(sv.benCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32600();
			wszzBenCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzBenCessAge.set(sv.benCessAge);
		}
		goTo(GotoLabel.term2542);
	}

protected void ageAnniversary2541()
	{
		if (isEQ(sv.riskCessAge, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
			wszzRiskCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
			wszzPremCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		if (isEQ(sv.benCessAge, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32600();
			wszzBenCessAge.set(datcon3rec.freqFactor);
			wszzBenCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzBenCessAge.set(sv.benCessAge);
		}
	}

protected void term2542()
	{
		if (isEQ(t5606rec.eaage, "E")
		|| isEQ(t5606rec.eaage, " ")) {
			goTo(GotoLabel.termExact2543);
		}
		if (isEQ(sv.riskCessTerm, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		if (isEQ(sv.benCessTerm, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32600();
			wszzBenCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzBenCessTerm.set(sv.benCessTerm);
		}
		goTo(GotoLabel.check2544);
	}

protected void termExact2543()
	{
		if (isEQ(sv.riskCessTerm, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessTerm.set(datcon3rec.freqFactor);
			compute(wszzRiskCessTerm, 5).set(sub(wszzRiskCessTerm, wszzAnbAtCcd));
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
			compute(wszzPremCessTerm, 5).set(sub(wszzPremCessTerm, wszzAnbAtCcd));
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		if (isEQ(sv.benCessTerm, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32600();
			wszzBenCessTerm.set(datcon3rec.freqFactor);
			compute(wszzBenCessTerm, 5).set(sub(wszzBenCessTerm, wszzAnbAtCcd));
		}
		else {
			wszzBenCessTerm.set(sv.benCessTerm);
		}
	}

protected void check2544()
	{
		/*  Assume the dates are invalid until proved otherwise.*/
		sv.rcessageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.bcessageErr.set(errorsInner.d028);
		sv.rcesstrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		sv.bcesstrmErr.set(errorsInner.d029);
		x.set(0);
	}

	/**
	* <pre>
	* Check each possible option.
	* </pre>
	*/
protected void checkOccurance2545()
	{
		x.add(1);
		if (isGT(x, wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2555);
		}
		if ((isEQ(t5606rec.ageIssageFrm[x.toInt()], 0)
		&& isEQ(t5606rec.ageIssageTo[x.toInt()], 0))
		|| isLT(wszzAnbAtCcd, t5606rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5606rec.ageIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkTermFields2550);
		}
		if (isGTE(wszzRiskCessAge, t5606rec.riskCessageFrom[x.toInt()])
		&& isLTE(wszzRiskCessAge, t5606rec.riskCessageTo[x.toInt()])) {
			sv.rcessageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge, t5606rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge, t5606rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
		if (isGTE(wszzBenCessAge, t5606rec.benCessageFrom[x.toInt()])
		&& isLTE(wszzBenCessAge, t5606rec.benCessageTo[x.toInt()])) {
			sv.bcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2550()
	{
		if ((isEQ(t5606rec.termIssageFrm[x.toInt()], 0)
		&& isEQ(t5606rec.termIssageTo[x.toInt()], 0))
		|| isLT(wszzAnbAtCcd, t5606rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5606rec.termIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkOccurance2545);
		}
		if (isGTE(wszzRiskCessTerm, t5606rec.riskCesstermFrom[x.toInt()])
		&& isLTE(wszzRiskCessTerm, t5606rec.riskCesstermTo[x.toInt()])) {
			sv.rcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm, t5606rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm, t5606rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzBenCessTerm, t5606rec.benCesstermFrm[x.toInt()])
		&& isLTE(wszzBenCessTerm, t5606rec.benCesstermTo[x.toInt()])) {
			sv.bcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2545);
	}

protected void checkComplete2555()
	{
		if (isNE(sv.rcesstrmErr, SPACES)
		&& isEQ(sv.riskCessTerm, ZERO)) {
			sv.rcesdteErr.set(sv.rcesstrmErr);
			sv.rcesstrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr, SPACES)
		&& isEQ(sv.premCessTerm, ZERO)) {
			sv.pcesdteErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.bcesstrmErr, SPACES)
		&& isEQ(sv.benCessTerm, ZERO)) {
			sv.bcesdteErr.set(sv.bcesstrmErr);
			sv.bcesstrmErr.set(SPACES);
		}
		if (isNE(sv.rcessageErr, SPACES)
		&& isEQ(sv.riskCessAge, ZERO)) {
			sv.rcesdteErr.set(sv.rcessageErr);
			sv.rcessageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr, SPACES)
		&& isEQ(sv.premCessAge, ZERO)) {
			sv.pcesdteErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
		if (isNE(sv.bcessageErr, SPACES)
		&& isEQ(sv.benCessAge, ZERO)) {
			sv.bcesdteErr.set(sv.bcessageErr);
			sv.bcessageErr.set(SPACES);
		}
	}

	/**
	* <pre>
	*     Mortality Class is no longer a compulsory field, as premium 
	*     calculation is now using new plan code (BENPLN) instead.    
	*2560-CHECK-MORTCLS.                                              
	*  3) Mortality-Class,  if the mortality class appears on a       
	*       coverage/rider  screen  it  is  a  compulsory field       
	*       because it will  be used in calculating the premium       
	*       amount. The mortality class entered must one of the       
	*       ones in the edit rules table.                             
	*    MOVE 0                      TO X.                            
	*2565-LOOP.                                                       
	*    ADD 1                       TO X.                            
	*    IF X                        > WSAA-MAX-MORT                  
	*       MOVE E420                TO SR676-MORTCLS-ERR             
	*       GO TO 2570-CHECK-LIENCD.                                  
	*    IF T5606-MORTCLS (X)        = SPACES  OR                     
	*       T5606-MORTCLS (X)        NOT = SR676-MORTCLS              
	*       GO TO 2565-LOOP.                                          
	* </pre>
	*/
protected void checkLiencd2570()
	{
		if (isEQ(sv.liencd, SPACES)) {
			goTo(GotoLabel.checkMore2580);
		}
		x.set(0);
	}

protected void loop2575()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2580);
		}
		if (isEQ(t5606rec.liencd[x.toInt()], SPACES)
		|| isNE(t5606rec.liencd[x.toInt()], sv.liencd)) {
			goTo(GotoLabel.loop2575);
		}
	}

protected void checkMore2580()
	{
		/* Check joint life selection indicator (if applicable).*/
		if (isNE(sv.selectOut[varcom.pr.toInt()], "Y")) {
			if (isNE(sv.select, SPACES)
			&& isNE(sv.select, "J")
			&& isNE(sv.select, "L")) {
				sv.selectErr.set(errorsInner.h039);
			}
		}
		if (isEQ(sv.numapp, ZERO)
		&& isEQ(sv.numavail, sv.polinc)) {
			sv.numappErr.set(errorsInner.l001);
		}
		wsaaWorkCredit.set(wsaaCredit);
		if (nonfirst.isTrue()) {
			wsaaWorkCredit.add(covtrbnIO.getNumapp());
		}
		wsaaWorkCredit.subtract(sv.numapp);
		if (isLT(wsaaWorkCredit, ZERO)
		&& isGT(sv.numapp, sv.numavail)) {
			sv.numappErr.set(errorsInner.h437);
			wsspcomn.edterror.set("Y");
		}
		if(incomeProtectionflag || premiumflag){
			checkIPPmandatory();
			checkIPPfields();
		}
	}

protected void callDatcon32600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcSumins2650()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2650();
				case readNext2651: 
					readNext2651();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2650()
	{
		/* Calculate the sum insured by reading table TR687, multiply the*/
		/* timing factor with the daily room and board rate.*/
		itdmIO.setParams(SPACES);
		itdmIO.setDataKey(SPACES);
		initialize(wsbbTr687Itemitem);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr687);
		/*    MOVE WSSP-LANGUAGE          TO WSBB-TR687-LANGUAGE.  <V71L09>*/
		wsbbTr687Crtable.set(covtlnbIO.getCrtable());
		/* MOVE SR676-MORTCLS          TO WSBB-TR687-MORTCLS.           */
		wsbbTr687Benpln.set(sv.benpln);
		itdmIO.setItemitem(wsbbTr687Itemitem);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
	}

protected void readNext2651()
	{
		itdmIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr687)
		|| isNE(itdmIO.getItemitem(), wsbbTr687Itemitem)) {
			sv.suminErr.set(errorsInner.rl81);
			sv.benplnErr.set(errorsInner.rl81);
			return ;
		}
		if (isGT(itdmIO.getItmfrm(), chdrlnbIO.getOccdate())
		|| isLT(itdmIO.getItmto(), chdrlnbIO.getOccdate())) {
			itdmIO.setFunction(varcom.nextr);
			goTo(GotoLabel.readNext2651);
		}
		tr687rec.tr687Rec.set(itdmIO.getGenarea());
		compute(sv.sumin, 1).setRounded(mult(mult(tr687rec.factor, tr687rec.premunit), tr687rec.benfamt02));
		wsaaSumin.set(sv.sumin);
		compute(wsaaMinsumins, 2).set(mult(tr687rec.zssi, tr687rec.premunit));
	}

protected void calcPremium2700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2700();
				case calc2710: 
					calc2710();
				case exit2790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2700()
	{
		/* If plan processing, and no plicies applicable,*/
		/*   skip the validation as this COVTRBN is to be deleted.*/
		if (plan.isTrue()
		&& isEQ(sv.numapp, 0)) {
			goTo(GotoLabel.exit2790);
		}
		/* If benefit billed, do not calculate premium*/
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.exit2790);
		}
		/* PREMIUM CALCULATION*/
		/* The  premium amount is  required  on  all  products  and  all*/
		/* validating  must  be  successfully  completed  before  it  is*/
		/* calculated. If there is  no  premium  method defined (i.e the*/
		/* relevant code was blank), the premium amount must be entered.*/
		/* Otherwise, it is optional and always calculated.*/
		/* Note that a premium calculation subroutine may calculate the*/
		/* premium from the benefit amt OR the benefit amt from the*/
		/* premium.*/
		/* To calculate  it,  call  the  relevant calculation subroutine*/
		/* worked out above passing:*/
		if (isEQ(premReqd, "N")) {
			goTo(GotoLabel.calc2710);
		}
		if (isEQ(sv.instPrem, 0)) {
			sv.instprmErr.set(errorsInner.g818);
		}
		goTo(GotoLabel.exit2790);
	}

protected void calc2710()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(covtlnbIO.getCrtable());
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		if (isEQ(sv.select, "J")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.effectdt.set(chdrlnbIO.getOccdate());
		premiumrec.termdate.set(sv.premCessDate);
		premiumrec.currcode.set(chdrlnbIO.getCntcurr());
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			/*     MOVE DTC3-STATUZ        TO SYSR-STATUZ*/
			/*     PERFORM 600-FATAL-ERROR.*/
			sv.pcesdteErr.set(errorsInner.sc72);
			goTo(GotoLabel.exit2790);
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/*  (wsaa-sumin already adjusted for plan processing)*/
		premiumrec.cnttype.set(chdrlnbIO.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.riskCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.benCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaBenCessTerm.set(datcon3rec.freqFactor);
		premiumrec.benCessTerm.set(wsaaBenCessTerm);
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		pr676cpy.mortcls.set(sv.mortcls);
		premiumrec.benpln.set(sv.benpln);
		pr676cpy.benpln.set(sv.benpln);
		pr676cpy.livesno.set(sv.livesno);
		pr676cpy.waiverprem.set(sv.waiverprem);
		pr676cpy.waiverCrtable.set(tr686rec.waivercode);
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrlnbIO.getOccdate());
		premiumrec.reRateDate.set(chdrlnbIO.getOccdate());
		premiumrec.calcPrem.set(sv.instPrem);
		premiumrec.calcBasPrem.set(sv.instPrem);
		premiumrec.calcLoaPrem.set(ZERO);
		if (plan.isTrue()) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.polinc), sv.numapp)));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem, sv.polinc), sv.numapp)));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.polinc), sv.numapp)));
		}
		getAnnt5000();
		/* If the annuity frequency is not being used then pass the*/
		/* benefit frequency to the calculation method using this field.*/
		/*    IF     WSAA-SUMFLAG NOT = SPACE                              */
		/*       AND CPRM-FREQANN     = SPACES                             */
		/*           MOVE T5606-BENFREQ   TO CPRM-FREQANN.                 */
		/*    CALL T5675-PREMSUBR USING   CPRM-PREMIUM-REC.*/
		premiumrec.language.set(wsspcomn.language);
		//IVE-750  start- TRM product - Premium calculation for HBNF rider - PM09 method
		/*ILIFE-2465 Sr676_Hospital & Surgical Benefit Start*/
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, pr676cpy.parmRecord);
		}
		else
		{		
			premiumrec.livesno.set(sv.livesno);
			premiumrec.liveno.set(sv.livesno);
			premiumrec.indic.set(sv.waiverprem);
			premiumrec.ccode.set(pr676cpy.waiverCrtable);
			//premiumrec.totprem.set(sv.instPrem); 
			premiumrec.percent.set(ZERO);
			
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			if(isNE(sv.waitperiod,SPACES)){
				premiumrec.waitperiod.set(sv.waitperiod);
			}
			if(isNE(sv.bentrm,SPACES)){
				premiumrec.bentrm.set(sv.bentrm);
			}
			if(isNE(sv.prmbasis,SPACES)){
				premiumrec.prmbasis.set(sv.prmbasis);
			}
			if(isNE(sv.poltyp,SPACES)){
				premiumrec.poltyp.set(sv.poltyp);
			}
			premiumrec.occpcode.set("");
			if(isNE(sv.dialdownoption,SPACES)){
				if(sv.dialdownoption.toString().startsWith("0"))
					premiumrec.dialdownoption.set(sv.dialdownoption.substring(1));
				else
					premiumrec.dialdownoption.set(sv.dialdownoption);
			}
			else
				premiumrec.dialdownoption.set("100");
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3)); 
			/*Ticket #ILIFE-2782 Start	*/
			premiumrec.loadper.set(ZERO);
			premiumrec.adjustageamt.set(ZERO);
			premiumrec.rateadj.set(ZERO);
			premiumrec.fltmort.set(ZERO);
			premiumrec.premadj.set(ZERO);
			/*Ticket #ILIFE-2782 - End	*/
			callProgram(t5675rec.premsubr, premiumrec.premiumRec,pr676cpy.parmRecord, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*ILIFE-2465 End*/
		//IVE-750 End

		/*SCH                  PR676-PARM-RECORD.*/
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		
		getOccupVpms2011CustomerSpecific();
		/* Adjust premium calculated for plan processing.*/
		if (plan.isTrue()) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.polinc), sv.numapp)));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem, sv.polinc), sv.numapp)));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.numapp), sv.polinc)));
		}
		/* Put possibly calculated benefit amt back on the screen.*/
		if (plan.isTrue()) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin, sv.numapp), sv.polinc)));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		/*BRD-306 START */
		sv.loadper.set(premiumrec.loadper);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		/*BRD-306 END */
		/* Having calculated it, the  entered value, if any, is compared*/
		/* with it to check that  it  is within acceptable limits of the*/
		/* automatically calculated figure.  If  it  is  less  than  the*/
		/* amount calculated and  within  tolerance  then  the  manually*/
		/* entered amount is allowed.  If  the entered value exceeds the*/
		/* calculated one, the calculated value is used.*/
		if (isEQ(sv.instPrem, 0)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2790);
		}
		if (isGTE(sv.instPrem, premiumrec.calcPrem)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2790);
		}
		/* check the tolerance amount against the table entry read above.*/
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem, sv.instPrem));
		sv.instprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 11)
		|| isEQ(sv.instprmErr, SPACES)); wsaaSub.add(1)){
			searchForTolerance2740();
		}
		/* If the premium is within the tolerance limits, then we have to*/
		/* adjust the basic premium (the loaded premium will not change).*/
		if (isEQ(sv.instprmErr, SPACES)) {
			compute(sv.zbinstprem, 2).set(sub(sv.instPrem, sv.zlinstprem));
		}
		goTo(GotoLabel.exit2790);
	}

	/**
	* <pre>
	***  Calculate tolerance Limit and Check whether it is greater
	***  than the maximum tolerance amount in table T5667.
	* </pre>
	*/
protected void searchForTolerance2740()
	{
		/*IF CHDRLNB-BILLFREQ = T5667-FREQ (WSAA-SUB)*/
		if (isEQ(payrIO.getBillfreq(), t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtol[wsaaSub.toInt()])), 100));
			if (isLTE(wsaaDiff, wsaaTol)
			&& isLTE(wsaaDiff, t5667rec.maxAmount[wsaaSub.toInt()])) {
				sv.instprmErr.set(SPACES);
			}
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			
			loadWsspFields3010();
			checkCredits3020();
			lextpf3010CustomerSpecific();
			/*ILIFE-3470 by pmujavadiya*/
			if((incomeProtectionflag || premiumflag || dialdownFlag)&& callingfirst.isTrue()){
			insertAndUpdateRcvdpf();
			wsaaInsertUpdateFlag.set("N");
			}
			updateHbnf3030();
			if (isFollowUpRequired) {
				fluppfAvaList = fluppfDAO.searchFlupRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
				fupno = fluppfAvaList.size();	//ICIL-1494
				createFollowUps3400();
			}
			
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void createFollowUps3400()
{
	try {
		writeFollowUps3430();
	}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}

protected void writeFollowUps3430()
{
	for (wsaaCount.set(1); !(isGT(wsaaCount, 5)) && isNE(ta610rec.fupcdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)) {
		writeFollowUp3500();
	}

fluppfDAO.insertFlupRecord(fluplnbList);
}

protected void writeFollowUp3500()
{
	try {
		fluppf = new Fluppf();
		lookUpStatus3510();
		lookUpDescription3520();
		writeRecord3530();
	}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}

protected void lookUpStatus3510() {
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(ta610rec.fupcdes[wsaaCount.toInt()]);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.e557);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}

protected void lookUpDescription3520()
{ 
	descpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());

}

protected void writeRecord3530()
{
	fupno++;
	
	fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
	fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
	setPrecision(fupno, 0);
	fluppf.setFupNo(fupno);
	fluppf.setLife("01");
	fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
	fluppf.setFupDt(wsaaToday.toInt());
	fluppf.setFupCde(ta610rec.fupcdes[wsaaCount.toInt()].toString());
	fluppf.setFupTyp('P');
	fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	fluppf.setFupRmk(descpf.getLongdesc());/* IJTI-1523 */
	fluppf.setjLife("00");
	fluppf.setTrdt(varcom.vrcmDate.toInt());
	fluppf.setTrtm(varcom.vrcmTime.toInt());
	fluppf.setUserT(varcom.vrcmUser.toInt());
	fluppf.setEffDate(wsaaToday.toInt());
	fluppf.setCrtDate(wsaaToday.toInt());
	fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
	fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
	fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
	fluplnbList.add(fluppf);
}
protected void loadWsspFields3010()
	{
		/*     SPECIAL EXIT PROCESSING*/
		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3490);
		}
		/* Updating occurs with  the Creation, Deletion or Updating of a*/
		/* COVTRBN transaction record.*/
		/* If the 'KILL' function key was pressed or if in enquiry mode,*/
		/* skip the updating.*/
		if (isEQ(scrnparams.statuz, "KILL")
		|| forcedKill.isTrue()) {
			goTo(GotoLabel.exit3490);
		}
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3490);
		}
		/* If premium breakdown selected then exit....*/
		/*    IF SR676-PBIND = 'X' GO TO 3490-EXIT.*/
		/*    IF SR676-COVERDTL = 'X' GO TO 3030-UPDATE-HBNF.*/
		/*    Only bypass the update of the component record if*/
		/*    the Reassurance check box has not been selected as*/
		/*    component details are required by the Reassurance*/
		/*    program.*/
		if (isEQ(sv.optextind, "X")) {
			if (isNE(sv.premCessTerm, ZERO)) {
				wssplife.bigAmt.set(sv.premCessTerm);
				/**          IF SR676-RATYPIND  NOT = 'X'*/
				/**              GO TO 3490-EXIT*/
				/**          END-IF*/
			}
			else {
				datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
				datcon3rec.intDate2.set(sv.premCessDate);
				callDatcon33600();
				wsaaFreqFactor.set(datcon3rec.freqFactor);
				wssplife.fupno.set(wsaaFreqFactor);
				/**             IF SR676-RATYPIND NOT = 'X'*/
				/**                 GO TO 3490-EXIT*/
				/**             END-IF*/
			}
		}
		/* Skip write/update or delete of COVTTRM if TAXIND = 'X'          */
		if (isEQ(sv.taxind, "X")) {
			goTo(GotoLabel.exit3490);
		}
	}

	/**
	* <pre>
	**** If there is an 'X' in the Annuity details SELECT, then
	**** the next program is skipped, as the new credit amounts
	**** cannot be properly calculated without the extra information.
	*    IF SR676-ANNTIND            = 'X'
	*       AND SR676-RATYPIND   NOT = 'X'
	*        GO TO 3490-EXIT
	*    END-IF.
	* </pre>
	*/
protected void checkCredits3020()
	{
		/* Before  updating any  records,  calculate  the  new  "credit"*/
		/* amount.*/
		/*  - add to  the  current  credit  amount,  the  number  of*/
		/*       policies previously  applicable  from  the  COVTRBN*/
		/*       record (zero if  there was no COVTRBN) and subtract*/
		/*       the number applicable entered on the screen.*/
		/*  - a positive  credit means that additional policies must*/
		/*       be added to the plan.*/
		/*  - a negative  credit  means  that  some policies must be*/
		/*       removed from the plan.*/
		if (nonfirst.isTrue()) {
			wsaaCredit.add(covtrbnIO.getNumapp());
		}
		wsaaCredit.subtract(sv.numapp);
		/* If the number  applicable  is greater than zero, write/update*/
		/* the COVTRBN record.  If the number applicable is zero, delete*/
		/* the COVTRBN record.*/
		if (isGT(sv.numapp, 0)) {
			if (firsttime.isTrue()) {
				initcovr3500();
				setupcovtrbn3550();
				covtrbnIO.setFunction(varcom.writr);
				wsaaCtrltime.set("N");
			}
			else {
				setupcovtrbn3550();
				covtrbnIO.setFunction(varcom.writd);
			}
		}
		if (isLTE(sv.numapp, ZERO)) {
			covtrbnIO.setChdrcoy(covtlnbIO.getChdrcoy());
			covtrbnIO.setChdrnum(covtlnbIO.getChdrnum());
			covtrbnIO.setLife(covtlnbIO.getLife());
			covtrbnIO.setCoverage(covtlnbIO.getCoverage());
			covtrbnIO.setRider(covtlnbIO.getRider());
			covtrbnIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, covtrbnIO);
			if (isNE(covtrbnIO.getStatuz(), varcom.oK)
			&& isNE(covtrbnIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(covtrbnIO.getParams());
				fatalError600();
			}
			else {
				if (isEQ(covtrbnIO.getStatuz(), varcom.mrnf)) {
					goTo(GotoLabel.exit3490);
				}
				else {
					covtrbnIO.setFunction(varcom.delet);
					if (isEQ(wsaaFirstSeqnbr, covtrbnIO.getSeqnbr())) {
						wsaaFirstSeqnbr.set(ZERO);
					}
				}
			}
		}
		updateCovtrbn3700();
	}

protected void insertAndUpdateRcvdpf(){
	boolean rcvdUpdateFlag=false;
if(rcvdPFObject!=null){
	rcvdPFObject.setChdrcoy(covtrbnIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covtrbnIO.getChdrnum().toString());
	rcvdPFObject.setCoverage(covtrbnIO.getCoverage().toString());
	rcvdPFObject.setCrtable(covtrbnIO.getCrtable().toString());
	rcvdPFObject.setLife(covtrbnIO.getLife().toString());
	rcvdPFObject.setRider(covtrbnIO.getRider().toString());
	if(rcvdPFObject.getWaitperiod() != null){
		if (isNE(sv.waitperiod, rcvdPFObject.getWaitperiod())) {
			rcvdPFObject.setWaitperiod(sv.waitperiod.toString());
			rcvdUpdateFlag=true;
		}
	}
	if(rcvdPFObject.getPoltyp() != null){
		if (isNE(sv.poltyp, rcvdPFObject.getPoltyp())) {
			rcvdPFObject.setPoltyp(sv.poltyp.toString());
			rcvdUpdateFlag=true;
		}
	}
	if(rcvdPFObject.getPrmbasis()!=null){
		if (isNE(sv.prmbasis, rcvdPFObject.getPrmbasis())) {
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
			rcvdUpdateFlag=true;
		}
	}
	if(rcvdPFObject.getBentrm()!=null){
		if (isNE(sv.bentrm, rcvdPFObject.getBentrm())) {
			rcvdPFObject.setBentrm(sv.bentrm.toString());
			rcvdUpdateFlag=true;
		}
	}
		//BRD-NBP-011 starts
	if(rcvdPFObject.getDialdownoption()!=null){
		if (isNE(sv.dialdownoption, rcvdPFObject.getDialdownoption())) {
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
			rcvdUpdateFlag=true;
		}
	}
		//BRD-NBP-011 ends
		if(rcvdUpdateFlag){
			rcvdDAO.updateIntoRcvdpf(rcvdPFObject);
		}
	}else{ 
		rcvdPFObject=new Rcvdpf();
		rcvdPFObject.setChdrcoy(covtrbnIO.getChdrcoy().toString());
		rcvdPFObject.setChdrnum(covtrbnIO.getChdrnum().toString());
		rcvdPFObject.setCoverage(covtrbnIO.getCoverage().toString());
		rcvdPFObject.setCrtable(covtrbnIO.getCrtable().toString());
		rcvdPFObject.setLife(covtrbnIO.getLife().toString());
		rcvdPFObject.setRider(covtrbnIO.getRider().toString());
		if(isNE(sv.waitperiod,SPACES)){
			rcvdPFObject.setWaitperiod(sv.waitperiod.toString());
		}
		if(isNE(sv.poltyp,SPACES)){
			rcvdPFObject.setPoltyp(sv.poltyp.toString());
		}
		if(isNE(sv.prmbasis,SPACES)){
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
		}
		if(isNE(sv.bentrm,SPACES)){
			rcvdPFObject.setBentrm(sv.bentrm.toString());
		}
		//BRD-NBP-011 starts
		if(isNE(sv.dialdownoption,SPACES)){
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
		}
		//BRD-NBP-011 ends
		rcvdDAO.insertRcvdpfRecord((rcvdPFObject));
	}
	
}

protected void updateHbnf3030()
	{
		updateHbnf3800();
	}

	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void initcovr3500()
	{
		/*PARA*/
		/* Before creating a  new COVTRBN record initialise the coverage*/
		/* fields.*/
		covtrbnIO.setCrtable(covtlnbIO.getCrtable());
		covtrbnIO.setPayrseqno(1);
		covtrbnIO.setRiskCessAge(0);
		covtrbnIO.setPremCessAge(0);
		covtrbnIO.setBenCessAge(0);
		covtrbnIO.setRiskCessTerm(0);
		covtrbnIO.setPremCessTerm(0);
		covtrbnIO.setBenCessTerm(0);
		covtrbnIO.setSumins(0);
		covtrbnIO.setRiskCessDate(varcom.vrcmMaxDate);
		covtrbnIO.setPremCessDate(varcom.vrcmMaxDate);
		covtrbnIO.setBenCessDate(varcom.vrcmMaxDate);
		covtrbnIO.setEffdate(varcom.vrcmMaxDate);
		/*EXIT*/
	}

protected void setupcovtrbn3550()
	{
		para3550();
	}

protected void para3550()
	{
		/* Check for changes in the COVTRBN Record.*/
		wsaaUpdateFlag.set("N");
		if (isNE(sv.riskCessDate, covtrbnIO.getRiskCessDate())) {
			covtrbnIO.setRiskCessDate(sv.riskCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessDate, covtrbnIO.getPremCessDate())) {
			covtrbnIO.setPremCessDate(sv.premCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessDate, covtrbnIO.getBenCessDate())) {
			covtrbnIO.setBenCessDate(sv.benCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessAge, covtrbnIO.getRiskCessAge())) {
			covtrbnIO.setRiskCessAge(sv.riskCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessAge, covtrbnIO.getPremCessAge())) {
			covtrbnIO.setPremCessAge(sv.premCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessAge, covtrbnIO.getBenCessAge())) {
			covtrbnIO.setBenCessAge(sv.benCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessTerm, covtrbnIO.getRiskCessTerm())) {
			covtrbnIO.setRiskCessTerm(sv.riskCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessTerm, covtrbnIO.getPremCessTerm())) {
			covtrbnIO.setPremCessTerm(sv.premCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessTerm, covtrbnIO.getBenCessTerm())) {
			covtrbnIO.setBenCessTerm(sv.benCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.sumin, covtrbnIO.getSumins())) {
			covtrbnIO.setSumins(sv.sumin);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(payrIO.getBillfreq(), covtrbnIO.getBillfreq())) {
			covtrbnIO.setBillfreq(payrIO.getBillfreq());
			wsaaUpdateFlag.set("Y");
		}
		if (isEQ(covtrbnIO.getBillfreq(), "00")) {
			if (isNE(sv.instPrem, covtrbnIO.getSingp())) {
				covtrbnIO.setSingp(sv.instPrem);
				wsaaUpdateFlag.set("Y");
			}
		}
		else {
			if (isNE(sv.instPrem, covtrbnIO.getInstprem())) {
				covtrbnIO.setInstprem(sv.instPrem);
				wsaaUpdateFlag.set("Y");
			}
		}
		if (isNE(sv.zbinstprem, covtrbnIO.getZbinstprem())) {
			covtrbnIO.setZbinstprem(sv.zbinstprem);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zlinstprem, covtrbnIO.getZlinstprem())) {
			covtrbnIO.setZlinstprem(sv.zlinstprem);
			/*BRD-306 START */			
			covtrbnIO.setLoadper(sv.loadper);
			covtrbnIO.setRateadj(sv.rateadj);
			covtrbnIO.setFltmort(sv.fltmort);
			covtrbnIO.setPremadj(sv.premadj);
			covtrbnIO.setAgeadj(sv.adjustageamt);
			
			/*BRD-306 END */
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.mortcls, covtrbnIO.getMortcls())) {
			covtrbnIO.setMortcls(sv.mortcls);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.liencd, covtrbnIO.getLiencd())) {
			covtrbnIO.setLiencd(sv.liencd);
			wsaaUpdateFlag.set("Y");
		}
		if ((isEQ(sv.select, "J")
		&& (isEQ(covtrbnIO.getJlife(), "00")
		|| isEQ(covtrbnIO.getJlife(), "  ")))
		|| ((isEQ(sv.select, " ")
		|| isEQ(sv.select, "L"))
		&& isEQ(covtrbnIO.getJlife(), "01"))) {
			wsaaUpdateFlag.set("Y");
			if (isEQ(sv.select, "J")) {
				covtrbnIO.setJlife("01");
			}
			else {
				covtrbnIO.setJlife("00");
			}
		}
		if (isNE(sv.polinc, covtrbnIO.getPolinc())) {
			covtrbnIO.setPolinc(sv.polinc);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.numapp, covtrbnIO.getNumapp())) {
			covtrbnIO.setNumapp(sv.numapp);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(payrIO.getBillchnl(), covtrbnIO.getBillchnl())) {
			covtrbnIO.setBillchnl(payrIO.getBillchnl());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(chdrlnbIO.getOccdate(), covtrbnIO.getEffdate())) {
			covtrbnIO.setEffdate(chdrlnbIO.getOccdate());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaAnbAtCcd, covtrbnIO.getAnbccd(1))) {
			covtrbnIO.setAnbccd(1, wsaaAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex, covtrbnIO.getSex(1))) {
			covtrbnIO.setSex(1, wsaaSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsbbAnbAtCcd, covtrbnIO.getAnbccd(2))) {
			covtrbnIO.setAnbccd(2, wsbbAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex, covtrbnIO.getSex(2))) {
			covtrbnIO.setSex(2, wsbbSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.bappmeth, covtrbnIO.getBappmeth())) {
			covtrbnIO.setBappmeth(sv.bappmeth);
			wsaaUpdateFlag.set("Y");
		}
		covtrbnIO.setCntcurr(chdrlnbIO.getCntcurr());
	}

protected void callDatcon33600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateCovtrbn3700()
	{
		try {
			para3700();
			para3710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para3700()
	{
		if (isEQ(wsaaUpdateFlag, "N")) {
			goTo(GotoLabel.exit3790);
		}
	}

protected void para3710()
	{
		covtrbnIO.setTermid(varcom.vrcmTermid);
		covtrbnIO.setUser(varcom.vrcmUser);
		covtrbnIO.setTransactionDate(varcom.vrcmDate);
		covtrbnIO.setTransactionTime(varcom.vrcmTime);
		covtrbnIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtrbnIO.setChdrnum(covtlnbIO.getChdrnum());
		covtrbnIO.setLife(covtlnbIO.getLife());
		covtrbnIO.setCoverage(covtlnbIO.getCoverage());
		covtrbnIO.setRider(covtlnbIO.getRider());
		covtrbnIO.setFormat(formatsInner.covtrbnrec);
		SmartFileCode.execute(appVars, covtrbnIO);
		if (isNE(covtrbnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtrbnIO.getParams());
			fatalError600();
		}
		
		updtpurepremCustomerSpecific();
	}

	/**
	* <pre>
	* Update HBNF file with the main life assured as the first client
	* </pre>
	*/
protected void updateHbnf3800()
	{
		para3800();
	}

protected void para3800()
	{
		hbnfIO.setFunction(varcom.rlse);
		callHbnf1950();
		hbnfIO.setRecKeyData(SPACES);
		hbnfIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hbnfIO.setChdrnum(chdrlnbIO.getChdrnum());
		hbnfIO.setLife(covtlnbIO.getLife());
		hbnfIO.setCoverage(covtlnbIO.getCoverage());
		hbnfIO.setRider(covtlnbIO.getRider());
		hbnfIO.setFunction(varcom.readr);
		callHbnf1950();
		if (isEQ(hbnfIO.getStatuz(), varcom.mrnf)) {
			hbnfIO.setRecNonKeyData(SPACES);
		}
		hbnfIO.setClntnum01(sv.lifcnum);
		hbnfIO.setMortcls(sv.mortcls);
		hbnfIO.setLivesno(sv.livesno);
		hbnfIO.setWaiverprem(sv.waiverprem);
		/*    HBNF now include 7 new fields to cater Benefit calculation   */
		/*    up to per component level.                                   */
		hbnfIO.setLife(sv.life);
		hbnfIO.setCoverage(sv.coverage);
		hbnfIO.setRider(sv.rider);
		hbnfIO.setEffdate(chdrlnbIO.getOccdate());
		hbnfIO.setCrtable(covtlnbIO.getCrtable());
		hbnfIO.setBenpln(sv.benpln);
		hbnfIO.setZunit(sv.zunit);
		if (isEQ(hbnfIO.getStatuz(), varcom.oK)) {
			hbnfIO.setFunction(varcom.writd);
		}
		else {
			hbnfIO.setFunction(varcom.writr);
		}
		callHbnf1950();
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		para4000();
	}

protected void para4000()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/*    spaces to  the current program entry in the program*/
		/*    stack and exit.*/
		if (forcedKill.isTrue()) {
			wsaaKillFlag.set("N");
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(sv.pbind, "X")) {
			pbindExe5100();
		}
		else {
			if (isEQ(sv.pbind, "?")) {
				pbindRet5200();
			}
			else {
				if (isEQ(sv.optextind, "X")) {
					optionsExe4500();
				}
				else {
					if (isEQ(sv.optextind, "?")) {
						optionsRet4600();
					}
					else {
						if (isEQ(sv.ratypind, "X")) {
							reassuranceExe4200();
						}
						else {
							if (isEQ(sv.ratypind, "?")) {
								reassuranceRet4300();
							}
							else {
								if (isEQ(sv.anntind, "X")) {
									annuityExe4400();
								}
								else {
									if (isEQ(sv.anntind, "?")) {
										annuityRet4900();
									}
									else {
										if (isEQ(sv.coverdtl, "X")) {
											coverdtlExe4950();
										}
										else {
											if (isEQ(sv.coverdtl, "?")) {
												coverdtlRet4960();
											}
											else {
												if (isEQ(sv.viewplan, "X")) {
													viewplanExe4970();
												}
												else {
													if (isEQ(sv.viewplan, "?")) {
														viewplanRet4980();
													}
													else {
														if (isEQ(sv.taxind, "X")) {
															taxExe5600();
														}
														else {
															if (isEQ(sv.taxind, "?")) {
																taxRet5700();
															}
															else {
																if (isEQ(sv.exclind, "X")) {
																	exclExe6000();
																}
																else {
																	if (isEQ(sv.exclind, "?")) {
																		exclRet6100();
																	}
															
															
															                 else {
																               if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], " ")) {
																	              wayout4700();
																              }
																            else {
																	             wayin4800();
																            }
															                 }
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
protected void exclExe6000(){
	/* - Keep the CHDR/COVT record */
	//PINNACLE-2855
	chdrlnbIO.setFunction("KEEPS");
	chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
	syserrrec.params.set(chdrlnbIO.getParams());
	syserrrec.statuz.set(chdrlnbIO.getStatuz());
	fatalError600();
	}
	covtlnbIO.setFunction("KEEPS");
	covtlnbIO.setFormat(formatsInner.covtlnbrec);
	SmartFileCode.execute(appVars, covtlnbIO);
	if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(covtlnbIO.getParams());
		syserrrec.statuz.set(covtlnbIO.getStatuz());
		fatalError600();
	}
	sv.exclind.set("?");
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
		gensswrec.function.set("G");
		wsspcomn.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		wsspcomn.crtable.set(covtlnbIO.getCrtable().toString());
		
		if(isNE(wsspcomn.flag,"I")){
			wsspcomn.flag.set("X");
			wsspcomn.cmode.set("PS");}
			else
				wsspcomn.cmode.set("IFE");
			gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	
}
protected void exclRet6100(){
	
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	checkExcl();
	
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	wsspcomn.nextprog.set(scrnparams.scrname);
}

protected void reassuranceExe4200()
	{
		para4200();
	}

protected void para4200()
	{
		/*    If the reassurance details have been selected,(value - 'X'),*/
		/*    then set an asterisk in the program stack action field to*/
		/*    ensure that control returns here, set the parameters for*/
		/*    generalised secondary switching and save the original*/
		/*    programs from the program stack.*/
		/*    Set up WSSP-CURRFORM for next programs.*/
		wsspcomn.currfrom.set(chdrlnbIO.getOccdate());
		sv.ratypind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			save4510();
		}
		gensswrec.function.set("B");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	/**
	* <pre>
	*   If a value has been placed in the GENS-FUNCTION then call
	*   the generalised secondary switching module to obtain the
	*   next 8 programs and load them into the program stack.
	* </pre>
	*/
protected void gensww4210()
	{
		para4211();
	}

protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextIO.getStatuz(), varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

protected void reassuranceRet4300()
	{
		/*PARA*/
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			restore4610();
		}
		/*    Only blank out the action if the Annuities check box*/
		/*     has not been selected.*/
		if (isNE(sv.anntind, "X")
		&& isNE(sv.optextind, "X")
		&& isNE(sv.coverdtl, "X")
		&& isNE(sv.viewplan, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		/*    Check if reassurance records have been added.*/
		checkRacd1100();
		/*EXIT*/
	}

protected void annuityExe4400()
	{
		para4400();
	}

protected void para4400()
	{
		/* If the ANNUITY details have been selected,(value - 'X'),*/
		/* THEN A '?' IS MOVED TO THE ANNT-IND SO THAT NEXT TIME THE*/
		/* PROGRAM IS RUN IT MOVES TO THE ANNUITY-RET(URN) SECTION*/
		/* RATHER THAN THIS*/
		sv.anntind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			save4510();
		}
		/* THE C SWITCHES THE PROGRAM TOWARDS P5220 AS DETERMINED*/
		/* IN TABLE T1675*/
		gensswrec.function.set("C");
		gensww4410();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		/*  Move the benefit amount to linkage to check than the*/
		/*  capital content is not greater than the benefit amount*/
		/*  in P5220.*/
		wssplife.bigAmt.set(sv.sumin);
	}

protected void gensww4410()
	{
		para4410();
	}

protected void para4410()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scr*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			sv.anntind.set("X");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextIO.getStatuz(), varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

protected void optionsExe4500()
	{
		para4500();
	}

protected void para4500()
	{
		/* The  first  thing  to   consider   is   the  handling  of  an*/
		/* options/extras request. If the indicator is 'X', a request to*/
		/* visit options and extras has been made. In this case:*/
		/*  - change the options/extras request indicator to '?',*/
		sv.optextind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			save4510();
		}
		/*  - call GENSSWCH with and  action  of 'A' to retrieve the*/
		/*       program switching required,  and  move  them to the*/
		/*       stack,*/
		gensswrec.function.set("A");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/*  - calculate  the  premium  as  described  above  in  the*/
		/*       'Validation' section, and  check  that it is within*/
		/*       the tolerance limit,*/
		sv.optextind.set("+");
		calcPremium2700();
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		/*    Only blank out the action and set the next program if*/
		/*    neither the Annuity nor the Reassurance check box has*/
		/*    been selected.*/
		/* MOVE ' '                 TO WSSP-SEC-ACTN (WSSP-PROGRAM-PTR).*/
		checkLext1900();
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		if (isNE(sv.anntind, "X")
		&& isNE(sv.ratypind, "X")
		&& isNE(sv.optextind, "X")
		&& isNE(sv.coverdtl, "X")
		&& isNE(sv.viewplan, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		/*EXIT*/
	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void wayout4700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4700();
				case cont4710: 
					cont4710();
				case cont4715: 
					cont4715();
				case cont4717: 
					cont4717();
				case cont4720: 
					cont4720();
				case exit4790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4700()
	{
		/* If control is passed to this  part of the 4000 section on the*/
		/* way  out of the program,  ie.  after  screen  I/O,  then  the*/
		/* current stack position action  flag  will  be  blank.  If the*/
		/* 4000  section  is   being   performed  after  returning  from*/
		/* processing another program  then  the  current stack position*/
		/* action flag will be '*'.*/
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/*    spaces to  the current program entry in the program*/
		/*    stack and exit.*/
		/*    IF SCRN-STATUZ              = 'KILL'*/
		/*       MOVE SPACES           TO WSSP-SEC-PROG (WSSP-PROGRAM-PTR)*/
		/*       GO TO 4790-EXIT.*/
		if (isEQ(scrnparams.statuz, varcom.rolu)
		|| isEQ(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.cont4710);
		}
		/*    If 'Enter' has been  pressed and "Credit" is non-zero*/
		/*    set the current select  action  field to '*', add 1*/
		/*    to the program pointer and exit.*/
		if (isNE(wsaaCredit, 0)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		}
		/*    If 'Enter' has been  pressed and "Credit" is zero add*/
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		goTo(GotoLabel.exit4790);
	}

protected void cont4710()
	{
		/*    If one of the Roll keys has been pressed and the*/
		/*    Number Applicable has been changed set the current*/
		/*    select action field to '*',  add 1 to the program*/
		/*    pointer and exit.*/
		if (isNE(sv.numapp, covtrbnIO.getNumapp())) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4790);
		}
		/*    If one of the Roll keys has been pressed and the*/
		/*    Number Applicable has not been changed then loop*/
		/*    round within the program and display the next /*/
		/*    previous screen as requested  (including BEGN).*/
		/*  Save the lase used sequence number in case we roll off the end*/
		wsaaSaveSeqnbr.set(covtrbnIO.getSeqnbr());
		if (isEQ(sv.numapp, 0)) {
			wsaaSaveSeqnbr.subtract(1);
		}
		/*  Cope with case when last record has just been added*/
		if (firsttime.isTrue()) {
			if (isEQ(scrnparams.statuz, varcom.rolu)) {
				covtrbnIO.setStatuz(varcom.endp);
				goTo(GotoLabel.cont4715);
			}
			else {
				covtrbnIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, covtrbnIO);
				if (isNE(covtrbnIO.getStatuz(), varcom.oK)
				&& isNE(covtrbnIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(covtrbnIO.getParams());
					fatalError600();
				}
			}
		}
		/* Read next/previous record*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			covtrbnIO.setFunction(varcom.nextr);
		}
		else {
			covtrbnIO.setFunction(varcom.nextp);
		}
		SmartFileCode.execute(appVars, covtrbnIO);
		if (isNE(covtrbnIO.getStatuz(), varcom.oK)
		&& isNE(covtrbnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtrbnIO.getParams());
			fatalError600();
		}
		if (isNE(covtrbnIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covtrbnIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covtrbnIO.getLife(), covtlnbIO.getLife())
		|| isNE(covtrbnIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covtrbnIO.getRider(), covtlnbIO.getRider())) {
			covtrbnIO.setStatuz(varcom.endp);
		}
	}

protected void cont4715()
	{
		if (isEQ(covtrbnIO.getStatuz(), varcom.endp)) {
			covtrbnIO.setNonKey(SPACES);
			covtrbnIO.setAnbccd(1, 0);
			covtrbnIO.setAnbccd(2, 0);
			covtrbnIO.setSingp(0);
			covtrbnIO.setInstprem(0);
			covtrbnIO.setZbinstprem(0);
			covtrbnIO.setZlinstprem(0);
			covtrbnIO.setNumapp(0);
			covtrbnIO.setPremCessAge(0);
			covtrbnIO.setPremCessTerm(0);
			covtrbnIO.setPolinc(0);
			covtrbnIO.setRiskCessAge(0);
			covtrbnIO.setRiskCessTerm(0);
			covtrbnIO.setSumins(0);
			covtrbnIO.setBenCessAge(0);
			covtrbnIO.setBenCessTerm(0);
			wsaaCtrltime.set("Y");
			goTo(GotoLabel.cont4717);
		}
		wsaaCtrltime.set("N");
		sv.riskCessDate.set(covtrbnIO.getRiskCessDate());
		sv.premCessDate.set(covtrbnIO.getPremCessDate());
		sv.benCessDate.set(covtrbnIO.getBenCessDate());
		sv.riskCessAge.set(covtrbnIO.getRiskCessAge());
		sv.premCessAge.set(covtrbnIO.getPremCessAge());
		sv.benCessAge.set(covtrbnIO.getBenCessAge());
		sv.riskCessTerm.set(covtrbnIO.getRiskCessTerm());
		sv.premCessTerm.set(covtrbnIO.getPremCessTerm());
		sv.benCessTerm.set(covtrbnIO.getBenCessTerm());
		if (isNE(covtrbnIO.getSingp(), ZERO)) {
			sv.instPrem.set(covtrbnIO.getSingp());
			if (isEQ(covtrbnIO.getInstprem(), 0)) {
				sv.zbinstprem.set(covtrbnIO.getZbinstprem());
				sv.zlinstprem.set(covtrbnIO.getZlinstprem());
			}
		}
		else {
			sv.zbinstprem.set(covtrbnIO.getZbinstprem());
			sv.zlinstprem.set(covtrbnIO.getZlinstprem());
			sv.instPrem.set(covtrbnIO.getInstprem());
		}
		/*BRD-306 START */
		sv.loadper.set(covtrbnIO.getLoadper());
		sv.rateadj.set(covtrbnIO.getRateadj());
		sv.fltmort.set(covtrbnIO.getFltmort());
		sv.premadj.set(covtrbnIO.getPremadj());
		sv.adjustageamt.set(covtrbnIO.getAgeadj());
		/*BRD-306 END */
		sv.sumin.set(covtrbnIO.getSumins());
		sv.mortcls.set(covtrbnIO.getMortcls());
		sv.liencd.set(covtrbnIO.getLiencd());
		sv.bappmeth.set(covtrbnIO.getBappmeth());
		/*    KEEPS the new COVTLNB so that it can be retrieved by*/
		/*    subsequent programs.*/
		covtlnbIO.setChdrcoy(covtrbnIO.getChdrcoy());
		covtlnbIO.setChdrnum(covtrbnIO.getChdrnum());
		covtlnbIO.setLife(covtrbnIO.getLife());
		covtlnbIO.setCoverage(covtrbnIO.getCoverage());
		covtlnbIO.setRider(covtrbnIO.getRider());
		covtlnbIO.setSeqnbr(covtrbnIO.getSeqnbr());
		covtlnbIO.setFunction(varcom.keeps);
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
	}

protected void cont4717()
	{
		/*    When displaying a previous record, calculate the*/
		/*    number available as the current number plus the number*/
		/*    applicable to the record about to be displayed.*/
		if (isEQ(covtrbnIO.getFunction(), varcom.nextp)) {
			wsaaNumavail.add(covtrbnIO.getNumapp());
			goTo(GotoLabel.cont4720);
		}
		/*    When displaying the next record, or a blank screen to*/
		/*    enter a new record, calculate the number available as*/
		/*    the current number less the number applicable on the*/
		/*    current screen.*/
		/*    If displaying a blank screen for a new record, default*/
		/*    the number applicable to the number available. If this*/
		/*    is less than zero, default it to zero.*/
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail, 0)) {
			wsaaNumavail.set(0);
		}
		if (firsttime.isTrue()) {
			setPrecision(covtrbnIO.getSeqnbr(), 0);
			covtrbnIO.setSeqnbr(add(1, wsaaSaveSeqnbr));
			covtrbnIO.setNumapp(wsaaNumavail);
		}
	}

protected void cont4720()
	{
		if (isEQ(covtrbnIO.getFunction(), varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr, 0)) {
			wsaaFirstSeqnbr.set(covtrbnIO.getSeqnbr());
		}
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covtrbnIO.getNumapp());
		/* Re-calculate default SI if applicable*/
		/*  then re-calculate default premium if SI is different*/
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax, 0)) {
			compute(sv.sumin, 1).setRounded((div(mult(t5606rec.sumInsMin, sv.numapp), sv.polinc)));
		}
		if (isNE(sv.sumin, covtrbnIO.getSumins())) {
			sv.instPrem.set(0);
			sv.zbinstprem.set(0);
			sv.zlinstprem.set(0);
			/*BRD-306 START */
			sv.loadper.set(0);
			sv.adjustageamt.set(0);
			sv.rateadj.set(0);
			sv.fltmort.set(0);
			sv.premadj.set(0);
			/*BRD-306 END */
			calcPremium2700();
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
		/* Life selection indicator may only be set up on the first*/
		/* screen*/
		if (isNE(t5687rec.jlifePresent, SPACES)
		&& jointlif.isTrue()
		&& isEQ(wsaaFirstSeqnbr, covtrbnIO.getSeqnbr())) {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void wayin4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4800();
				case rolu4805: 
					rolu4805();
				case cont4810: 
					cont4810();
				case cont4820: 
					cont4820();
				case readCovtrbn4830: 
					readCovtrbn4830();
				case cont4835: 
					cont4835();
				case cont4837: 
					cont4837();
				case cont4840: 
					cont4840();
				case exit4890: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4800()
	{
		if (isEQ(scrnparams.statuz, varcom.rolu)
		|| isEQ(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.cont4820);
		}
		if (isEQ(wsaaCredit, 0)) {
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4890);
		}
		if (isLTE(sv.numapp, sv.numavail)
		&& isLT(wsaaCredit, ZERO)) {
			goTo(GotoLabel.rolu4805);
		}
		if (isLT(wsaaCredit, 0)) {
			goTo(GotoLabel.cont4810);
		}
	}

	/**
	* <pre>
	*    If 'Enter' has been pressed and 'Credit' has a
	*    positive value then force the program to "roll up".
	* </pre>
	*/
protected void rolu4805()
	{
		scrnparams.statuz.set(varcom.rolu);
		goTo(GotoLabel.cont4820);
	}

protected void cont4810()
	{
		/*   If 'Enter' has been pressed and "Credit" has a negative*/
		/*   value, loop round within the program displaying the*/
		/*   details from the first transaction record, with the*/
		/*   'Number Applicable' highlighted, and give a message*/
		/*   indicating that more policies have been defined than*/
		/*   are on the Contract Header.*/
		covtrbnIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtrbnIO.setChdrnum(covtlnbIO.getChdrnum());
		covtrbnIO.setLife(covtlnbIO.getLife());
		covtrbnIO.setCoverage(covtlnbIO.getCoverage());
		covtrbnIO.setRider(covtlnbIO.getRider());
		covtrbnIO.setSeqnbr(0);
		/* The Error Message was being moved to the screen and not*/
		/* to the appropriate Screen Field:- SR676-NumApp-Err.*/
		/* This validation has been moved to the 2000 Section.*/
		covtrbnIO.setFunction(varcom.begn);
		goTo(GotoLabel.readCovtrbn4830);
	}

protected void cont4820()
	{
		/*    If one of the Roll keys has been pressed then loop*/
		/*    round within the program and display the next or*/
		/*    previous screen as requested (including  BEGN,*/
		/*    available count etc., see above).*/
		/*  Save the lase used sequence number in case we roll off the end*/
		wsaaSaveSeqnbr.set(covtrbnIO.getSeqnbr());
		if (isEQ(sv.numapp, 0)) {
			wsaaSaveSeqnbr.subtract(1);
		}
		/*  Cope with case when last record has just been added*/
		if (firsttime.isTrue()) {
			if (isEQ(scrnparams.statuz, varcom.rolu)) {
				covtrbnIO.setStatuz(varcom.endp);
				goTo(GotoLabel.cont4835);
			}
			else {
				covtrbnIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, covtrbnIO);
				if (isNE(covtrbnIO.getStatuz(), varcom.oK)
				&& isNE(covtrbnIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(covtrbnIO.getParams());
					fatalError600();
				}
			}
		}
		/* Read next/previous record*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			covtrbnIO.setFunction(varcom.nextr);
		}
		else {
			covtrbnIO.setFunction(varcom.nextp);
		}
	}

protected void readCovtrbn4830()
	{
		SmartFileCode.execute(appVars, covtrbnIO);
		if (isNE(covtrbnIO.getStatuz(), varcom.oK)
		&& isNE(covtrbnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtrbnIO.getParams());
			fatalError600();
		}
		if (isNE(covtrbnIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covtrbnIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covtrbnIO.getLife(), covtlnbIO.getLife())
		|| isNE(covtrbnIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covtrbnIO.getRider(), covtlnbIO.getRider())) {
			covtrbnIO.setStatuz(varcom.endp);
		}
	}

protected void cont4835()
	{
		if (isEQ(covtrbnIO.getStatuz(), varcom.endp)) {
			wsaaCtrltime.set("Y");
			covtrbnIO.setNonKey(SPACES);
			covtrbnIO.setAnbccd(1, 0);
			covtrbnIO.setAnbccd(2, 0);
			covtrbnIO.setSingp(0);
			covtrbnIO.setInstprem(0);
			covtrbnIO.setZbinstprem(0);
			covtrbnIO.setZlinstprem(0);
			covtrbnIO.setNumapp(0);
			covtrbnIO.setPremCessAge(0);
			covtrbnIO.setPremCessTerm(0);
			covtrbnIO.setPolinc(0);
			covtrbnIO.setRiskCessAge(0);
			covtrbnIO.setRiskCessTerm(0);
			covtrbnIO.setSumins(0);
			covtrbnIO.setBenCessAge(0);
			covtrbnIO.setBenCessTerm(0);
			goTo(GotoLabel.cont4837);
		}
		wsaaCtrltime.set("N");
		sv.riskCessDate.set(covtrbnIO.getRiskCessDate());
		sv.premCessDate.set(covtrbnIO.getPremCessDate());
		sv.benCessDate.set(covtrbnIO.getBenCessDate());
		sv.riskCessAge.set(covtrbnIO.getRiskCessAge());
		sv.premCessAge.set(covtrbnIO.getPremCessAge());
		sv.benCessAge.set(covtrbnIO.getBenCessAge());
		sv.riskCessTerm.set(covtrbnIO.getRiskCessTerm());
		sv.premCessTerm.set(covtrbnIO.getPremCessTerm());
		sv.benCessTerm.set(covtrbnIO.getBenCessTerm());
		if (isNE(covtrbnIO.getSingp(), ZERO)) {
			sv.instPrem.set(covtrbnIO.getSingp());
			if (isEQ(covtrbnIO.getInstprem(), 0)) {
				sv.zbinstprem.set(covtrbnIO.getZbinstprem());
				sv.zlinstprem.set(covtrbnIO.getZlinstprem());
			}
		}
		else {
			sv.zbinstprem.set(covtrbnIO.getZbinstprem());
			sv.zlinstprem.set(covtrbnIO.getZlinstprem());
			sv.instPrem.set(covtrbnIO.getInstprem());
		}
		/*BRD-306 START */
		sv.loadper.set(covtrbnIO.getLoadper());
		sv.rateadj.set(covtrbnIO.getRateadj());
		sv.fltmort.set(covtrbnIO.getFltmort());
		sv.premadj.set(covtrbnIO.getPremadj());
		sv.adjustageamt.set(covtrbnIO.getAgeadj());
		/*BRD-306 END */
		sv.sumin.set(covtrbnIO.getSumins());
		sv.mortcls.set(covtrbnIO.getMortcls());
		sv.liencd.set(covtrbnIO.getLiencd());
		sv.bappmeth.set(covtrbnIO.getBappmeth());
	}

protected void cont4837()
	{
		/*    When displaying the first record again,  set the*/
		/*    number available as the number included in the plan*/
		if (isEQ(covtrbnIO.getFunction(), varcom.begn)) {
			wsaaNumavail.set(chdrlnbIO.getPolinc());
			wsaaFirstSeqnbr.set(covtrbnIO.getSeqnbr());
			goTo(GotoLabel.cont4840);
		}
		/*    When displaying a previous record, calculate the*/
		/*    number available as the current number plus the number*/
		/*    applicable to the record about to be displayed.*/
		if (isEQ(covtrbnIO.getFunction(), varcom.nextp)) {
			wsaaNumavail.add(covtrbnIO.getNumapp());
			goTo(GotoLabel.cont4840);
		}
		/*    When displaying the next record, or a blank screen to*/
		/*    enter a new record, calculate the number available as*/
		/*    the current number less the number applicable on the*/
		/*    current screen.*/
		/*    If displaying a blank screen for a new record, default*/
		/*    the number applicable to the number available. If this*/
		/*    is less than zero, default it to zero.*/
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail, 0)) {
			wsaaNumavail.set(0);
		}
		if (firsttime.isTrue()) {
			setPrecision(covtrbnIO.getSeqnbr(), 0);
			covtrbnIO.setSeqnbr(add(1, wsaaSaveSeqnbr));
			covtrbnIO.setNumapp(wsaaNumavail);
		}
		if (firsttime.isTrue()) {
			covtlnbIO.setSeqnbr(covtrbnIO.getSeqnbr());
			covtlnbIO.setFunction(varcom.keeps);
			covtlnbIO.setFormat(formatsInner.covtlnbrec);
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
		}
	}

protected void cont4840()
	{
		if (isEQ(covtrbnIO.getFunction(), varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr, 0)) {
			wsaaFirstSeqnbr.set(covtrbnIO.getSeqnbr());
		}
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covtrbnIO.getNumapp());
		/* Re-calculate default SI if applicable*/
		/*  then re-calculate default premium if SI is different*/
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax, 0)) {
			compute(sv.sumin, 1).setRounded((div(mult(t5606rec.sumInsMin, sv.numapp), sv.polinc)));
		}
		if (isNE(sv.sumin, covtrbnIO.getSumins())) {
			sv.instPrem.set(0);
			sv.zbinstprem.set(0);
			sv.zlinstprem.set(0);
			/*BRD-306 START */
			sv.loadper.set(0);
			sv.adjustageamt.set(0);
			sv.rateadj.set(0);
			sv.fltmort.set(0);
			sv.premadj.set(0);
			/*BRD-306 END */
			calcPremium2700();
		}
		/* Life selection indicator may only be set up on the first*/
		/* screen*/
		if (isNE(t5687rec.jlifePresent, SPACES)
		&& jointlif.isTrue()
		&& isEQ(wsaaFirstSeqnbr, covtrbnIO.getSeqnbr())) {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void annuityRet4900()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/*  - calculate  the  premium  as  described  above  in  the*/
		/*       'Validation' section, and  check  that it is within*/
		/*       the tolerance limit,*/
		sv.anntind.set("+");
		calcPremium2700();
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
		/* Turn the protect switch off:*/
		sv.anntindOut[varcom.pr.toInt()].set("N");
		/*EXIT*/
	}

protected void coverdtlExe4950()
	{
		coverdtl4951();
	}

protected void coverdtl4951()
	{
		/* Covered Lives details (COVERDTL) is a madatory field. If the*/
		/* indicator is 'X', a request to visit options and extras has*/
		/* been made. In this case:*/
		/*  - change the options/extras request indicator to '?',*/
		sv.coverdtl.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar11 = 0; !(loopVar11 == 8); loopVar11 += 1){
			save4510();
		}
		/*  - call GENSSWCH with and  action  of 'D' to retrieve the*/
		/*       program switching required,  and  move  them to the*/
		/*       stack,*/
		pr676cpy.mortcls.set(sv.mortcls);
		pr676cpy.livesno.set(sv.livesno);
		pr676cpy.waiverprem.set(sv.waiverprem);
		pr676cpy.benpln.set(sv.benpln);
		gensswrec.function.set("D");
		hbnfIO.setFunction(varcom.keeps);
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}

		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar12 = 0; !(loopVar12 == 8); loopVar12 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void coverdtlRet4960()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/*  - calculate  the  premium  as  described  above  in  the*/
		/*       'Validation' section, and  check  that it is within*/
		/*       the tolerance limit,*/
		sv.coverdtl.set("+");
		calcPremium2700();
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar13 = 0; !(loopVar13 == 8); loopVar13 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		if (isNE(sv.anntind, "X")
		&& isNE(sv.ratypind, "X")
		&& isNE(sv.optextind, "X")
		&& isNE(sv.coverdtl, "X")
		&& isNE(sv.viewplan, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		/*EXIT*/
	}

protected void viewplanExe4970()
	{
		viewplan4971();
	}

protected void viewplan4971()
	{
		/* Keep the coverage record for use in viewing the plan...*/
		covtrbnIO.setFunction(varcom.keeps);
		covtrbnIO.setFormat(formatsInner.covtrbnrec);
		SmartFileCode.execute(appVars, covtrbnIO);
		if (isNE(covtrbnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtrbnIO.getParams());
			fatalError600();
		}
		/* Keep the hospital benefit record for use in viewing the plan... */
		hbnfIO.setFunction(varcom.keeps);
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		/* If field  is 'X', a request to visit to table PNMextras has*/
		/* been made. In this case:*/
		/*  - change the options/extras request indicator to '?',*/
		sv.viewplan.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar14 = 0; !(loopVar14 == 8); loopVar14 += 1){
			save4510();
		}
		/*  - call GENSSWCH with and  action  of 'D' to retrieve the*/
		/*       program switching required,  and  move  them to the*/
		/*       stack,*/
		pr676cpy.mortcls.set(sv.mortcls);
		pr676cpy.livesno.set(sv.livesno);
		pr676cpy.waiverprem.set(sv.waiverprem);
		pr676cpy.benpln.set(sv.benpln);
		gensswrec.function.set("E");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar15 = 0; !(loopVar15 == 8); loopVar15 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void viewplanRet4980()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/*  - calculate  the  premium  as  described  above  in  the*/
		/*       'Validation' section, and  check  that it is within*/
		/*       the tolerance limit,*/
		sv.viewplan.set("+");
		calcPremium2700();
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar16 = 0; !(loopVar16 == 8); loopVar16 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		if (isNE(sv.anntind, "X")
		&& isNE(sv.ratypind, "X")
		&& isNE(sv.optextind, "X")
		&& isNE(sv.coverdtl, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void getAnnt5000()
	{
		begins5010();
	}

protected void begins5010()
	{
		anntIO.setChdrcoy(covtlnbIO.getChdrcoy());
		anntIO.setChdrnum(covtlnbIO.getChdrnum());
		anntIO.setLife(covtlnbIO.getLife());
		anntIO.setCoverage(covtlnbIO.getCoverage());
		anntIO.setRider(covtlnbIO.getRider());
		anntIO.setSeqnbr(covtlnbIO.getSeqnbr());
		anntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, anntIO);
		if (isNE(anntIO.getStatuz(), varcom.oK)
		&& isNE(anntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(anntIO.getParams());
			syserrrec.statuz.set(anntIO.getStatuz());
			fatalError600();
		}
		if (isEQ(anntIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(anntIO.getFreqann());
			premiumrec.advance.set(anntIO.getAdvance());
			premiumrec.arrears.set(anntIO.getArrears());
			premiumrec.guarperd.set(anntIO.getGuarperd());
			premiumrec.intanny.set(anntIO.getIntanny());
			premiumrec.capcont.set(anntIO.getCapcont());
			premiumrec.withprop.set(anntIO.getWithprop());
			premiumrec.withoprop.set(anntIO.getWithoprop());
			premiumrec.ppind.set(anntIO.getPpind());
			premiumrec.nomlife.set(anntIO.getNomlife());
			premiumrec.dthpercn.set(anntIO.getDthpercn());
			premiumrec.dthperco.set(anntIO.getDthperco());
		}
		else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

protected void pbindExe5100()
	{
		start5110();
	}

protected void start5110()
	{
		/*  - Keep the POVR/CHDR record for Premium Breakdown enquiry.*/
		/*    Ensure we get the latest one.*/
		readPovr5300();
		if (isEQ(povrIO.getStatuz(), varcom.endp)
		|| isNE(povrIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(povrIO.getChdrnum(), lifelnbIO.getChdrnum())
		|| isNE(povrIO.getLife(), lifelnbIO.getLife())
		|| isNE(povrIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(povrIO.getRider(), covtlnbIO.getRider())) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		/* Use standard GENSWITCH table switching using the next option*/
		/* on table T1675.*/
		/*  - change the request indicator to '?',*/
		sv.pbind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar17 = 0; !(loopVar17 == 8); loopVar17 += 1){
			save4510();
		}
		/*  - call GENSSWCH with the next table action to retreive the*/
		/*    next program.*/
		gensswrec.function.set("D");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar18 = 0; !(loopVar18 == 8); loopVar18 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void pbindRet5200()
	{
		start5210();
	}

protected void start5210()
	{
		/* Release the POVR records as no longer required.*/
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/* Note that to have selected this option in the first place then*/
		/* details must exist.......set the flag for re-selection.*/
		sv.pbind.set("+");
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar19 = 0; !(loopVar19 == 8); loopVar19 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		if (isNE(sv.anntind, "X")
		&& isNE(sv.ratypind, "X")
		&& isNE(sv.optextind, "X")
		&& isNE(sv.coverdtl, "X")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void readPovr5300()
	{
		start5310();
	}

protected void start5310()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(covtlnbIO.getChdrcoy());
		povrIO.setChdrnum(lifelnbIO.getChdrnum());
		povrIO.setLife(lifelnbIO.getLife());
		povrIO.setCoverage(covtlnbIO.getCoverage());
		povrIO.setRider(covtlnbIO.getRider());
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)
		&& isNE(povrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	*5400-CHECK-T5602 SECTION.                                        
	*5410-START.                                                      
	* Read the SUM Coverage table & check if the coverage is
	* a WOP. If so then non-display and protect the benefit amount
	* and benefit frequency description on screen.....
	*    MOVE SPACES                 TO ITEM-PARAMS.                  
	*    MOVE 'IT'                   TO ITEM-ITEMPFX.                 
	*    MOVE COVTLNB-CHDRCOY        TO ITEM-ITEMCOY.                 
	*    MOVE T5602                  TO ITEM-ITEMTABL.                
	*    MOVE COVTLNB-CRTABLE        TO ITEM-ITEMITEM.                
	*    MOVE ITEMREC                TO ITEM-FORMAT.                  
	*    MOVE READR                  TO ITEM-FUNCTION.                
	*    CALL 'ITEMIO' USING ITEM-PARAMS.                             
	*    IF ITEM-STATUZ = O-K                                         
	*       MOVE ITEM-GENAREA        TO T5602-T5602-REC               
	*       MOVE 'Y'                 TO WSAA-SUMFLAG                  
	*       IF T5602-INDIC-01 NOT = SPACE                             
	*          MOVE ZERO             TO SR676-SUMIN                   
	*          MOVE 'Y'              TO SR676-SUMIN-OUT (ND)          
	*                                   SR676-SUMIN-OUT (PR)          
	*       END-IF                                                    
	*    ELSE                                                         
	*       MOVE SPACE               TO WSAA-SUMFLAG.                 
	*5490-EXIT.                                                       
	*    EXIT.                                                        
	* </pre>
	*/
protected void taxExe5600()
	{
		start5610();
	}

protected void start5610()
	{
		/* Keep the CHDR/COVT record                                       */
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		covtlnbIO.setZbinstprem(sv.zbinstprem);
		covtlnbIO.setInstprem(sv.instPrem);
		covtlnbIO.setFunction("KEEPS");
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		/* Perform the standard GENSWCH setup  and execution               */
		sv.taxind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar20 = 0; !(loopVar20 == 8); loopVar20 += 1){
			save4510();
		}
		gensswrec.function.set("F");
		gensww4410();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar21 = 0; !(loopVar21 == 8); loopVar21 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet5700()
	{
		/*START*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		sv.taxind.set("+");
		/* Restore the saved programs to the program stack                 */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar22 = 0; !(loopVar22 == 8); loopVar22 += 1){
			restore4610();
		}
		/* Blank  out  the  stack  "action"                                */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/* Set  WSSP-NEXTPROG to  the current screen                       */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void checkLivesno6000()
	{
		start6010();
	}

protected void start6010()
	{
		hbnfIO.setRecKeyData(SPACES);
		wsaaHbnfLivesnoNum.set(ZERO);
		hbnfIO.setChdrcoy(covtlnbIO.getChdrcoy());
		hbnfIO.setChdrnum(covtlnbIO.getChdrnum());
		hbnfIO.setLife(covtlnbIO.getLife());
		hbnfIO.setCoverage(covtlnbIO.getCoverage());
		hbnfIO.setRider(covtlnbIO.getRider());
		hbnfIO.setFunction(varcom.readr);
		callHbnf1950();
		if (isEQ(hbnfIO.getStatuz(), varcom.mrnf)) {
			hbnfIO.setRecNonKeyData(SPACES);
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 10)); wsaaSub.add(1)){
			if (isNE(hbnfIO.getClntnum(wsaaSub), SPACES)) {
				wsaaHbnfLivesnoNum.add(1);
			}
		}
	}

protected void checkCalcTax6100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start6110();
				case callTaxSubr6150: 
					callTaxSubr6150();
				case exit6190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6110()
	{
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.exit6190);
		}
		if (notFirstTaxCalc.isTrue()) {
			goTo(GotoLabel.callTaxSubr6150);
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covtlnbIO.getCrtable());
		readTr52e6200();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e6200();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e6200();
		}
		wsaaFirstTaxCalc.set("N");
	}

protected void callTaxSubr6150()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
			txcalcrec.life.set(covtlnbIO.getLife());
			txcalcrec.coverage.set(covtlnbIO.getCoverage());
			txcalcrec.rider.set(covtlnbIO.getRider());
			txcalcrec.planSuffix.set(ZERO);
			txcalcrec.crtable.set(covtlnbIO.getCrtable());
			txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
			txcalcrec.register.set(chdrlnbIO.getRegister());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
			wsaaCntCurr.set(chdrlnbIO.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(sv.zbinstprem);
			}
			else {
				txcalcrec.amountIn.set(sv.instPrem);
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrlnbIO.getOccdate());
			txcalcrec.tranno.set(chdrlnbIO.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				wsaaTaxamt.set(sv.instPrem);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				sv.taxind.set("+");
				sv.taxamtOut[varcom.nd.toInt()].set("N");
				sv.taxindOut[varcom.nd.toInt()].set("N");
				sv.taxamtOut[varcom.pr.toInt()].set("Y");		//ILIFE-4225
				sv.taxindOut[varcom.pr.toInt()].set("N");
			}
		}
	}

protected void readTr52e6200()
	{
		start6210();
	}

protected void start6210()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), wsspcomn.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}
public ChdrlnbTableDAM getChdrlnbIO() {
	return chdrlnbIO;
}

public void setChdrlnbIO(ChdrlnbTableDAM chdrlnbIO) {
	this.chdrlnbIO = chdrlnbIO;
}
/*
 * Class transformed  from Data Structure WSAA-DEFAULTS--INNER
 */
private static final class WsaaDefaultsInner { 

	private FixedLengthStringData wsaaDefaults = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaDefaultRa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 0);
	private Validator defaultRa = new Validator(wsaaDefaultRa, "Y");
	private Validator nodefRa = new Validator(wsaaDefaultRa, "N");
	private FixedLengthStringData wsaaDefaultPa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 1);
	private Validator defaultPa = new Validator(wsaaDefaultPa, "Y");
	private Validator nodefPa = new Validator(wsaaDefaultPa, "N");
	private FixedLengthStringData wsaaDefaultBa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 2);
	private Validator defaultBa = new Validator(wsaaDefaultBa, "Y");
	private Validator nodefBa = new Validator(wsaaDefaultBa, "N");
	private FixedLengthStringData wsaaDefaultRt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 3);
	private Validator defaultRt = new Validator(wsaaDefaultRt, "Y");
	private Validator nodefRt = new Validator(wsaaDefaultRt, "N");
	private FixedLengthStringData wsaaDefaultPt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 4);
	private Validator defaultPt = new Validator(wsaaDefaultPt, "Y");
	private Validator nodefPt = new Validator(wsaaDefaultPt, "N");
	private FixedLengthStringData wsaaDefaultBt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 5);
	private Validator defaultBt = new Validator(wsaaDefaultBt, "Y");
	private Validator nodefBt = new Validator(wsaaDefaultBt, "N");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData rl86 = new FixedLengthStringData(4).init("RL86");
	private FixedLengthStringData e027 = new FixedLengthStringData(4).init("E027");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e316 = new FixedLengthStringData(4).init("E316");
	private FixedLengthStringData e374 = new FixedLengthStringData(4).init("E374");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f344 = new FixedLengthStringData(4).init("F344");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData g622 = new FixedLengthStringData(4).init("G622");
	private FixedLengthStringData h437 = new FixedLengthStringData(4).init("H437");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h039 = new FixedLengthStringData(4).init("H039");
	private FixedLengthStringData h040 = new FixedLengthStringData(4).init("H040");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData l001 = new FixedLengthStringData(4).init("L001");
	private FixedLengthStringData d028 = new FixedLengthStringData(4).init("D028");
	private FixedLengthStringData d029 = new FixedLengthStringData(4).init("D029");
	private FixedLengthStringData d352 = new FixedLengthStringData(4).init("D352");
	private FixedLengthStringData rl87 = new FixedLengthStringData(4).init("RL87");
	private FixedLengthStringData np44 = new FixedLengthStringData(4).init("NP44");
	private FixedLengthStringData rl81 = new FixedLengthStringData(4).init("RL81");
	private FixedLengthStringData g616 = new FixedLengthStringData(4).init("G616");
	private FixedLengthStringData h113 = new FixedLengthStringData(4).init("H113");
	private FixedLengthStringData rl84 = new FixedLengthStringData(4).init("RL84");
	private FixedLengthStringData sc72 = new FixedLengthStringData(4).init("SC72");
	private FixedLengthStringData rl85 = new FixedLengthStringData(4).init("RL85");
	private FixedLengthStringData w067 = new FixedLengthStringData(4).init("W067");
	private FixedLengthStringData f646 = new FixedLengthStringData(4).init("F646");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	private FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6625 = new FixedLengthStringData(5).init("T6625");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData tr687 = new FixedLengthStringData(5).init("TR687");
	private FixedLengthStringData tr686 = new FixedLengthStringData(5).init("TR686");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	private FixedLengthStringData covtrbnrec = new FixedLengthStringData(10).init("COVTRBNREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData racdlnbrec = new FixedLengthStringData(10).init("RACDLNBREC");
	private FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
	private FixedLengthStringData hbnfrec = new FixedLengthStringData(10).init("HBNFREC");
}

protected void lextpf3010CustomerSpecific(){
	
}
protected void  getOccupVpms2011CustomerSpecific(){
	
	if (isNE(premiumrec.statuz, varcom.oK)) {
	sv.instprmErr.set(premiumrec.statuz);
	goTo(GotoLabel.exit2790);
}
/* When Number Of Units > 1, then times the no. of units with the  */
/* calculated premium                                              */
if (isGT(sv.zunit, 1)) {
	compute(premiumrec.calcBasPrem, 3).setRounded(mult(premiumrec.calcBasPrem, sv.zunit));
	compute(premiumrec.calcLoaPrem, 3).setRounded(mult(premiumrec.calcLoaPrem, sv.zunit));
	compute(premiumrec.calcPrem, 3).setRounded(mult(premiumrec.calcPrem, sv.zunit));
}
}

protected void updtpurepremCustomerSpecific(){}
protected void getPurePrem1011CustomeSpecific(){}
}


