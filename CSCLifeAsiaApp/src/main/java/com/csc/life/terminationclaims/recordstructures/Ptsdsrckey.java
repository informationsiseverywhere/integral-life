package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:16
 * Description:
 * Copybook name: PTSDSRCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ptsdsrckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ptsdsrcFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ptsdsrcKey = new FixedLengthStringData(64).isAPartOf(ptsdsrcFileKey, 0, REDEFINE);
  	public FixedLengthStringData ptsdsrcChdrcoy = new FixedLengthStringData(1).isAPartOf(ptsdsrcKey, 0);
  	public FixedLengthStringData ptsdsrcChdrnum = new FixedLengthStringData(8).isAPartOf(ptsdsrcKey, 1);
  	public PackedDecimalData ptsdsrcTranno = new PackedDecimalData(5, 0).isAPartOf(ptsdsrcKey, 9);
  	public PackedDecimalData ptsdsrcPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ptsdsrcKey, 12);
  	public FixedLengthStringData ptsdsrcLife = new FixedLengthStringData(2).isAPartOf(ptsdsrcKey, 15);
  	public FixedLengthStringData ptsdsrcCoverage = new FixedLengthStringData(2).isAPartOf(ptsdsrcKey, 17);
  	public FixedLengthStringData ptsdsrcRider = new FixedLengthStringData(2).isAPartOf(ptsdsrcKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(ptsdsrcKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ptsdsrcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ptsdsrcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}