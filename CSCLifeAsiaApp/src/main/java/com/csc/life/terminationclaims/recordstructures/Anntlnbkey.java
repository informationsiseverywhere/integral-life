package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:16
 * Description:
 * Copybook name: ANNTLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Anntlnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData anntlnbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData anntlnbKey = new FixedLengthStringData(64).isAPartOf(anntlnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData anntlnbChdrcoy = new FixedLengthStringData(1).isAPartOf(anntlnbKey, 0);
  	public FixedLengthStringData anntlnbChdrnum = new FixedLengthStringData(8).isAPartOf(anntlnbKey, 1);
  	public FixedLengthStringData anntlnbLife = new FixedLengthStringData(2).isAPartOf(anntlnbKey, 9);
  	public FixedLengthStringData anntlnbCoverage = new FixedLengthStringData(2).isAPartOf(anntlnbKey, 11);
  	public FixedLengthStringData anntlnbRider = new FixedLengthStringData(2).isAPartOf(anntlnbKey, 13);
  	public PackedDecimalData anntlnbSeqnbr = new PackedDecimalData(3, 0).isAPartOf(anntlnbKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(anntlnbKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(anntlnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		anntlnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}