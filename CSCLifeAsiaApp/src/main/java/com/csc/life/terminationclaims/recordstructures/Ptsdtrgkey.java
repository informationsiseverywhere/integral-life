package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:16
 * Description:
 * Copybook name: PTSDTRGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ptsdtrgkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ptsdtrgFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ptsdtrgKey = new FixedLengthStringData(64).isAPartOf(ptsdtrgFileKey, 0, REDEFINE);
  	public FixedLengthStringData ptsdtrgChdrcoy = new FixedLengthStringData(1).isAPartOf(ptsdtrgKey, 0);
  	public FixedLengthStringData ptsdtrgChdrnum = new FixedLengthStringData(8).isAPartOf(ptsdtrgKey, 1);
  	public PackedDecimalData ptsdtrgTranno = new PackedDecimalData(5, 0).isAPartOf(ptsdtrgKey, 9);
  	public PackedDecimalData ptsdtrgPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(ptsdtrgKey, 12);
  	public FixedLengthStringData ptsdtrgLife = new FixedLengthStringData(2).isAPartOf(ptsdtrgKey, 15);
  	public FixedLengthStringData ptsdtrgCoverage = new FixedLengthStringData(2).isAPartOf(ptsdtrgKey, 17);
  	public FixedLengthStringData ptsdtrgRider = new FixedLengthStringData(2).isAPartOf(ptsdtrgKey, 19);
  	public FixedLengthStringData ptsdtrgVirtualFund = new FixedLengthStringData(4).isAPartOf(ptsdtrgKey, 21);
  	public FixedLengthStringData ptsdtrgFieldType = new FixedLengthStringData(1).isAPartOf(ptsdtrgKey, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(ptsdtrgKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ptsdtrgFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ptsdtrgFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}