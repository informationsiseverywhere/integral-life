package com.csc.life.terminationclaims.screens;

import com.csc.smart400framework.SmartVarModel;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import com.csc.common.DD;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sjl52ScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(227);  
	public FixedLengthStringData dataFields = new FixedLengthStringData(115).isAPartOf(dataArea, 0);
	public FixedLengthStringData conStatus =  DD.conStatus.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData claimFrom =  DD.claimnumber.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData conOwner =  DD.conOwner.copy().isAPartOf(dataFields,39);
	public ZonedDecimalData conDate = DD.srdate.copyToZonedDecimal().isAPartOf(dataFields,89);
	public FixedLengthStringData claimTypeList = DD.inctype.copy().isAPartOf(dataFields,97);
	public FixedLengthStringData conFrom = DD.chdrnum.copy().isAPartOf(dataFields,99);
	public FixedLengthStringData clttwo = DD.clttwo.copy().isAPartOf(dataFields, 107);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 115);
	public FixedLengthStringData conStatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData claimFromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData conOwnerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData conDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData claimTypeListErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData conFromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData clttwoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 143);
	public FixedLengthStringData[] conStatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] claimFromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] conOwnerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] conDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] claimTypeListOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] conFromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] clttwoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(223);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(108).isAPartOf(subfileArea, 0);
	public ZonedDecimalData seqenum = DD.seqno.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData claimNum = DD.claimnumber.copy().isAPartOf(subfileFields,2);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,11);
	public FixedLengthStringData clntId = DD.conOwner.copy().isAPartOf(subfileFields,19);
	public ZonedDecimalData contactDate = DD.srdate.copyToZonedDecimal().isAPartOf(subfileFields,69);
	public FixedLengthStringData claimType = DD.claimType.copy().isAPartOf(subfileFields,77);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,107);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 108);
	public FixedLengthStringData seqenumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData claimNumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData clntIdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData contactDateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData claimTypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea,136);
	public FixedLengthStringData[] seqenumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] claimNumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] clntIdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] contactDateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] claimTypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea,220);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public FixedLengthStringData contactDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData conDateDisp = new FixedLengthStringData(10);
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	public FixedLengthStringData cnclScreenFlag = new FixedLengthStringData(1); //IBPLIFE-3582
	
	public LongData Sjl52screensflWritten = new LongData(0);
	public LongData Sjl52screenctlWritten = new LongData(0);
	public LongData Sjl52screenWritten = new LongData(0);
	public LongData Sjl52windowWritten = new LongData(0);
	public LongData Sjl52protectWritten = new LongData(0);
	public GeneralTable Sjl52screensfl = new GeneralTable(AppVars.getInstance());

	public GeneralTable getScreenSubfileTable() {
		return Sjl52screensfl;
	}

	public Sjl52ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		fieldIndMap.put(conStatusOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimFromOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(conOwnerOut,new String[] {"03","12", "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(conFromOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimTypeListOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(seqenumOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimNumOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntIdOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimTypeOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clttwoOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {conStatus, claimFrom, conFrom, conOwner, claimTypeList, conDate, clttwo};
		screenOutFields = new BaseData[][] {conStatusOut, claimFromOut, conFromOut, conOwnerOut, claimTypeListOut, conDateOut, clttwoOut};
		screenErrFields = new BaseData[] {conStatusErr, claimFromErr, conFromErr, conOwnerErr, claimTypeListErr, conDateErr, clttwoErr};
		
		screenDateFields = new BaseData[] {conDate};
		screenDateErrFields = new BaseData[] {conDateErr};
		screenDateDispFields = new BaseData[] {conDateDisp};
		
		screenSflFields = new BaseData[] { seqenum, claimNum, chdrnum, clntId, contactDate, claimType, select};
		screenSflOutFields = new BaseData[][] {seqenumOut, claimNumOut, chdrnumOut, clntIdOut, contactDateOut, claimTypeOut, selectOut};
		screenSflErrFields = new BaseData[] {seqenumErr, claimNumErr, chdrnumErr, clntIdErr, contactDateErr, claimTypeErr, selectErr};
		
		screenSflDateFields = new BaseData[] {contactDate};
		screenSflDateErrFields = new BaseData[] {contactDateErr};
		screenSflDateDispFields = new BaseData[] {contactDateDisp};
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sjl52screen.class;
		screenSflRecord = Sjl52screensfl.class;
		screenCtlRecord = Sjl52screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sjl52protect.class;
		
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl52screenctl.lrec.pageSubfile);
	}

	private long maxRow;
	
	public long getMaxRow() {
		return maxRow;
	}

	public void setMaxRow(long maxRow) {
		this.maxRow = maxRow;
	}
	
	public boolean hasSubfile() {
		return true;
	}
}
