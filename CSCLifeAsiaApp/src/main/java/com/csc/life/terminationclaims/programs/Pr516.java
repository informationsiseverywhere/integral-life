/*
 * File: Pr516.java
 * Date: 30 August 2009 1:34:43
 * Author: Quipoz Limited
 * 
 * Class transformed from PR516.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.procedures.Chkrlmt;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.tablestructures.T6005rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnntTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovtrbnTableDAM;
import com.csc.life.terminationclaims.screens.Sr516ScreenVars;
import com.csc.life.terminationclaims.tablestructures.T5606rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtunlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*      WAIVER OF PREMIUM  GENERIC COMPONENT
*
* This screen/program PR516 is used to capture the coverage and
* rider details for waiver of premium generic components. This
* module has been cloned from P5125. Redundant code has been
* removed and comments have been cleaned up.
*
* Initialise
* ----------
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* Read CHDRLNB (RETRV)  in  order to obtain the contract header
* information.  If  the  number of policies in the plan is zero
* or  one  then Plan-processing does not apply. If there is any
* other  numeric  value,  this  value  indicates  the number of
* policies in the Plan.
*
* If Plan processing is  not  to be performed, then protect and
* non-display the following three fields:-
*
* a) No. of Policies in Plan (default to 1)
* b) No. Available (default to 1)
* c) No. Applicable (default to 1)
*
* and processing for Variations will not occur.
*
* The key  for Coverage/rider to be worked on will be available
* from  COVTLNB.  This is obtained by using the RETRV function.
* Check if there are any transactions by doing a BEGN  (to read
* the Coverage/rider summary) using the COVTRBN logical view.
*
* The COVTRBN key will be:-
*
*  Company, Contract-no, Life, Coverage-no, rider-no, sequence-no
*
* If  Plan  processing is to occur, then calculate the "credit"
* as follows:
*
*  - subtract  the  'No  of  policies in the Plan' from the
*       first  COVTRBN  record  read  above (if no COVTRBN,
*       this is zero) from the 'No of policies in the Plan'
*       (from the Contract-Header).
*
*  - a positive  credit means that additional policies must
*       be added to the plan.
*
*  - a negative  credit  means  that  some policies must be
*       removed from the plan.
*
* If this is a single policy plan
*    if it is the first time (no COVTRBN records)
*     or there is no credit
*   - plan processing is not required.
*
* Set  the  number of  policies  available  to  the  number  of
* policies on the plan.  If  COVTRBN  records are to be written
* for the first time  (as determined above), default the number
* applicable to the number available.
*
* Read  the  contract  definition  details  from  T5688 for the
* contract  type  held  on  CHDRLNB. Access the version of this
* item for the original commencement date of the risk.
*
* Read  the  general  coverage/rider details from T5687 and the
* traditional/term  edit rules from T5606 for the coverage type
* held  on  COVTLNB.  Access  the version of this  item for the
* original commencement date of the risk.
*
* Read the  tolerance  limit  from  T5667  (key  as for T5606).
* Although this  is  a dated table,  just  read  the latest one
* (using ITEM).
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*  - read  the life details using LIFELNB (life number from
*       COVTLNB, joint life number '00').  Look up the name
*       from the  client  details  (CLTS)  and  format as a
*       "confirmation name".
*
*  - read the joint life details using LIFELNB (life number
*       from COVTLNB,  joint  life number '01').  If found,
*       look up the name from the client details (CLTS) and
*       format as a "confirmation name".
*
* To  determine  which premium calculation method to use decide
* whether or  not it is a Single or Joint-life case (if it is a
* joint  life  case,  the  joint  life record was found above).
* Then:
*
*  - if the benefit billing method is not blank, non-display
*       and protect the premium field (so do not bother with
*       the rest of this).
*
*  - if it  is  a  single-life  case, use the single-method
*       from  T5687. The age to be used for validation will
*       be the age of the main life.
*
*  - if the joint-life indicator (from T5687) is blank, and
*       if  it  is  a Joint-life case, use the joint-method
*       from  T5687. The age to be used for validation will
*       be the age of the main life.
*
*  - if the  Joint-life  indicator  is  'N',  then  use the
*       Single-method.  But, if there is a joint-life (this
*       must be  a  rider  to have got this far) prompt for
*       the joint  life  indicator  to determine which life
*       the rider is to attach to.  In all other cases, the
*       joint life  indicator  should  be non-displayed and
*       protected.  The  age to be used for validation will
*       be the age  of the main or joint life, depending on
*       the one selected.
*
*  - use the  premium-method  selected  from  T5687, if not
*       blank,  to access T5675.  This gives the subroutine
*       to use for the calculation.
*
* COVERAGE/RIDER DETAILS
*
* The fields to  be displayed on the screen are determined from
* the entry in table T5606 read earlier as follows:
*
*  - if  the  maximum  and  minimum   benefit  amounts  are
*       both  zero,  non-display  and protect  the  benefit
*       amount.  If  the  minimum  and maximum are both the
*       same, display the amount protected. Otherwise allow
*       input.
*
*  - if  the amount is to be displayed/entered, look up the
*       benefit frequency short description (T3590).
*
*  - if all   the   valid   mortality  classes  are  blank,
*       non-display  and  protect  this  field. If there is
*       only  one  mortality  class, display and protect it
*       (no validation will be required).
*
*  - if all the valid lien codes are blank, non-display and
*       protect  this field. Otherwise, this is an optional
*       field.
*
*  - if the  cessation  AGE  section  on  T5606  is  blank,
*       protect the two age related fields.
*
*  - if the  cessation  TERM  section  on  T5606  is blank,
*       protect the two term related fields.
*
*  - using  the  age  next  birthday  (ANB at RCD) from the
*       applicable  life  (see above), look up Issue Age on
*       the AGE and TERM  sections.  If  the  age fits into
*       a "slot" in  one  of these sections,  and the  risk
*       cessation  limits   are  the  same,   default   and
*       protect the risk cessation fields. Also do the same
*       for the premium  cessation  details.  In this case,
*       also  calculate  the  risk  and  premium  cessation
*       dates.
*
* OPTIONS AND EXTRAS
*
* If options and extras are  not  allowed (as defined by T5606)
* non-display and protect the fields.
*
* Otherwise,  read the  options  and  extras  details  for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the Options/Extras indicator (to show that there are some).
*
* REASSURANCE
*
* If Reassurance is not allowed ( T5687 Coverage/Rider details)
* non-display and protect the fields.
*
* Otherwise,  read  the  Reassurance  RACT  file for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the Reassurance  indicator (to show that there are some).
*
* ENQUIRY MODE
*
* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to
* protect  all  input  capable  fields  except  the  indicators
* controlling  where  to  switch  to  next  (options and extras
* indicator).
*
* Validation
* ----------
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* If 'KILL'  is  requested  and  the  current credit is zero or
* equal to the number of policies in the plan (all or nothing),
* then skip the  validation.  Otherwise,  highlight  this as an
* error and then skip the remainder of the validation.
*
* If  in  enquiry  mode,  skip  all field validation EXCEPT the
* options/extras indicator.
*
* Before  the  premium amount is calculated, the screen must be
* valid.  So  all  editing  is  completed before the premium is
* calculated.
*
* Table  T5606  (previously read) is used to obtain the editing
* rules.  Edit the screen according to the rules defined by the
* help. In particular:-
*
*  1) Check the benefit amount,  if applicable, against the
*       limits.
*
*  2) Check the consistency of the risk age and term fields
*       and  premium  age and term fields. Either, risk age
*       and  premium  age  must  be  used  or risk term and
*       premium  term  must  be  used.  They  must  not  be
*       combined. Note that  these  only need validating if
*       they were not defaulted.
*       NOTE: Risk age  and  Prem  term  may  be mixed i.e.
*       validation no longer requires the use of  risk  age
*       and prem age or risk term and prem term.
*
*  3) Mortality-Class,  if the mortality class appears on a
*       coverage/rider  screen  it  is  a  compulsory field
*       because it will  be used in calculating the premium
*       amount. The mortality class entered must one of the
*       ones in the edit rules table.
*
*  4) If the sum assured amount is zero - which could be the
*     case if we are processing a WOP component. Here the module
*     will automatically calculate the WOP sum assured which
*     is the accumulation of all the premiums of the components
*     for which a waiver is applicable and the policy fee
*     (as specified on table TR517).
*
* Calculate the following:-
*
*       - risk cessation date
*       - premium cessation date
*
* OPTIONS AND EXTRAS
*
* If  options/extras already exist, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed. If
* options  and  extras  are  requested,  DO  NOT  CALCULATE THE
* PREMIUM THIS TIME AROUND.
*
* REASSURANCE
*
* If  Reassurance already exists, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed.
*
* PREMIUM CALCULATION
*
* The  premium amount is  required  on  all  products  and  all
* validation  must  be  successfully  completed  before  it  is
* calculated. If there is  no premium  method defined (i.e. the
* relevant code was blank), the premium amount must be entered.
* Otherwise, it is optional and always calculated.
*
* To calculate  it,  call  the  relevant calculation subroutine
* worked out above passing:
*
*       - 'CALC' for the function
*       - Coverage code (crtable)
*       - Company
*       - Contract number
*       - Life number
*       - Joint life number
*            (if  the   screen  indicator  is  set  to  'J'
*            ,otherwise pass '00')
*       - Coverage number
*       - Rider number (0 for coverages)
*       - Effective date (Premium / Risk)
*       - Termination date (i.e. term cessation date)
*       - Currency
*       - Benefit amount
*       - Calculation "Basis" (i.e. mortality code)
*       - Payment frequency (from the contract header)
*       - Method of payment (from the contract header)
*       - Premium amount ("instalment")
*       - Return Status
*       - Rating Date (effective date for tables)
*
*  Subroutine may look up:
*
*       -  Life and Joint-life details
*       -  Options/Extras
*
* Having calculated it, the  entered value, if any, is compared
* with it to check that  it  is within acceptable limits of the
* automatically calculated figure.  If  it  is  less  than  the
* amount calculated and  within  tolerance, then  the  manually
* entered amount is allowed.  If  the entered value exceeds the
* calculated one, the calculated value is used.
*
* To check the tolerance amount against the limit read above.
*
* If 'CALC' was entered then re-display the screen.
*
* Updating
* --------
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* Updating occurs with  the Creation, Deletion or Updating of a
* COVTRBN transaction record.
*
* If the 'KILL' function key was pressed or if in enquiry mode,
* skip the updating.
*
* Before  updating any  records,  calculate  the  new  "credit"
* amount.
*
*  - add to  the  current  credit  amount,  the  number  of
*       policies previously  applicable  from  the  COVTRBN
*       record (zero if  there was no COVTRBN) and subtract
*       the number applicable entered on the screen.
*
*  - a positive  credit means that additional policies must
*       be added to the plan.
*
*  - a negative  credit  means  that  some policies must be
*       removed from the plan.
*
* Before creating a  new COVTRBN record initialise the coverage
* fields.
*
* If the number  applicable  is greater than zero, write/update
* the COVTRBN record.  If the number applicable is zero, delete
* the COVTRBN record.
*
*
* Next Program
* ------------
*
* The  first  thing  to   consider   is   the  handling  of  a
* Reassurance request. If the indicator is 'X', a request to
* visit Reassurance has been made. In this case:
*
*  - change the Reassurance request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with  an  action  of 'C' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  Reassurance   indicator  will  be  '?'.  To
* handle the return from Reassurance:
*
*  - calculate  the  premium  as  described  above  in  the
*       'Validation' section, and  check  that it is within
*       the tolerance limit,
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
* Similarly, we do the same processing to handle an
* options/extras request. If the indicator is 'X', a request to
* visit options and extras has been made. In this case:
*
*  - change the options/extras request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with  an  action  of 'A' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  options/extras  indicator  will  be  '?'.  To
* handle the return from options and extras:
*
*  - calculate  the  premium  as  described  above  in  the
*       'Validation' section, and  check  that it is within
*       the tolerance limit,
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
* The  following  processing  for  the  4000  section  will  be
* standard  for  all  scrolling  generic  component  processing
* programs.
*
* If control is passed to this  part of the 4000 section on the
* way  out of the program,  i.e. after  screen  I/O,  then  the
* current stack position action  flag  will  be  blank.  If the
* 4000  section  is   being   performed  after  returning  from
* processing another program, then  the  current stack position
* action flag will be '*'.
*
* Processing on the way out:
*
*  A) If one of the Roll  keys  has  been  pressed  and the
*       'Number  Applicable'   has  been  changed  set  the
*       current select action field  to  '*',  add 1 to the
*       program pointer and exit.
*
*  B) If one of the Roll  keys  has  been  pressed  and the
*       'Number Applicable' has  not been changed then loop
*       round  within   the   program   and   display   the
*       next/previous   screen   as   requested  (including
*       reads).   When   displaying   a   previous  record,
*       calculate  the  number  available  as  the  current
*       number plus the  number  applicable  to  the record
*       about to be  displayed.  When  displaying  the next
*       record, or a blank  screen  to  enter a new record,
*       calculate  the  number  available  as  the  current
*       number less the  number  applicable  on the current
*       screen. If displaying  a  blank  screen  for  a new
*       record, default the number applicable to the number
*       available. If this is less than zero, default it to
*       zero.
*
*  C) If 'Enter' has been  pressed and "Credit" is non-zero
*       set the current select  action  field to '*', add 1
*       to the program pointer and exit.
*
*  D) If 'Enter' has been  pressed and "Credit" is zero add
*       1 to the program pointer and exit.
*
*  E) If 'KILL'  has  been  requested,  then move spaces to
*       the current program entry in the program stack  and
*       exit.
*
* Processing on the way in:
*
*  A) If one of the Roll  keys  has  been pressed then loop
*       round within the  program  and  display the next or
*       previous  screen  as  requested  (including  reads,
*       available count etc., see above).
*
*  B) If 'Enter'  has  been  pressed  and  "Credit"  has  a
*       positive value then  loop  round within the program
*       and  display the next screen.  Calculate  available
*       count as the current available  less current screen
*       applicable. Set the number applicable to the number
*       available, but not less than zero.
*
*  C) If 'Enter'  has  been  pressed  and  "Credit"  has  a
*       negative  value,  loop  round  within  the  program
*       displaying the  details  from the first transaction
*       record,  with  the  'Number Applicable' highlighted
*       and give  a  message  indicating that more policies
*       have been defined than are on the Contract Header.
*
*
*  ANNUITY DETAILS SELECT SCREEN
*  =============================
*
*  As part of the 9405 Annuities Development a new field,
*  Annuity Details, is displayed if the component being
*  created, modified or enquired is an annuity component.
*
*  These components are identified by their coverage code
*  (CRTABLE) being a valid item on the Annuity Component Edit
*  Rules Table (T6625). The new field should only be displayed
*  if the coverage code is on this table.
*
*  If the details have not already been created for this
*  component, an X is displayed  in ANNTIND, otherwise a +
*  is displayed.
*
*  Once the details have been created, the regular benefits
*  component screen is redisplayed.
*
***********************************************************************
*
* 29/11/97    DUNC  SMART 9503 Conv for Client/Server.        <S9503>
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pr516 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR516");
	protected FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	protected ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	protected ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	protected ZonedDecimalData wsaaBenCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	protected ZonedDecimalData wsaaPayrBillfreq = new ZonedDecimalData(2, 0).setUnsigned();
	protected PackedDecimalData wsaaPovrInstamnt = new PackedDecimalData(17, 2);
	protected final int wsaaMaxOcc = 8;
	protected final int wsaaMaxMort = 6;
	protected FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);

	protected FixedLengthStringData wsaaKillFlag = new FixedLengthStringData(1);
	protected Validator forcedKill = new Validator(wsaaKillFlag, "Y");

	protected FixedLengthStringData wsaaCtrltime = new FixedLengthStringData(1);
	protected Validator firsttime = new Validator(wsaaCtrltime, "Y");
	protected Validator nonfirst = new Validator(wsaaCtrltime, "N");
	protected String premReqd = "N";

	protected FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	protected Validator nonplan = new Validator(wsaaPlanproc, "N");
	protected Validator plan = new Validator(wsaaPlanproc, "Y");

	protected FixedLengthStringData wsaaLifeind = new FixedLengthStringData(1);
	protected Validator jointlif = new Validator(wsaaLifeind, "J");
	protected Validator singlif = new Validator(wsaaLifeind, "S");

	protected FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	protected Validator firstTaxCalc = new Validator(wsaaFirstTaxCalc, "Y");
	protected Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");
	protected PackedDecimalData wsaaNumavail = new PackedDecimalData(4, 0);
	protected PackedDecimalData wsaaCredit = new PackedDecimalData(4, 0);
	protected PackedDecimalData wsaaWorkCredit = new PackedDecimalData(5, 0);
	protected PackedDecimalData wsaaSumin = new PackedDecimalData(13, 2);
	protected PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	protected PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	protected PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
	protected ZonedDecimalData wsddRiskCessAge = new ZonedDecimalData(3, 0);
	protected ZonedDecimalData wsddPremCessAge = new ZonedDecimalData(3, 0);
	protected ZonedDecimalData wsddBenCessAge = new ZonedDecimalData(3, 0);
	protected ZonedDecimalData wsddRiskCessTerm = new ZonedDecimalData(3, 0);
	protected ZonedDecimalData wsddPremCessTerm = new ZonedDecimalData(3, 0);
	protected ZonedDecimalData wsddBenCessTerm = new ZonedDecimalData(3, 0);
		/* WSAA-MAIN-LIFE-DETS */
	protected PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	protected PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
		/* WSBB-JOINT-LIFE-DETS */
	protected PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	protected PackedDecimalData wsbbCltdob = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsbbSex = new FixedLengthStringData(1);
		/* WSZZ-RATED-LIFE-DETS */
	protected PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	protected PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	protected PackedDecimalData wszzRiskCessAge = new PackedDecimalData(11, 5);
	protected PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	protected PackedDecimalData wszzBenCessAge = new PackedDecimalData(11, 5);
	protected PackedDecimalData wszzRiskCessTerm = new PackedDecimalData(11, 5);
	protected PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
	protected PackedDecimalData wszzBenCessTerm = new PackedDecimalData(11, 5);
		/* WSAA-SEC-PROGS */
	protected FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	protected FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	protected FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	protected FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	protected FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	protected FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	protected FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	protected FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	protected FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	protected FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);
		/* WSAA-END-HEX */
	protected PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	protected FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	protected FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler, 0);
		/* WSAA-START-HEX */
	protected PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	protected FixedLengthStringData filler2 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	protected FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler2, 0);

	protected FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	protected FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);

	protected FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	protected FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
	protected PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	protected PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	protected PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	protected PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	protected PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);

	protected FixedLengthStringData wsaaCovtlnbParams = new FixedLengthStringData(219);
	protected FixedLengthStringData wsaaCovtlnbDataKey = new FixedLengthStringData(64).isAPartOf(wsaaCovtlnbParams, 49);
	protected FixedLengthStringData wsaaCovtlnbCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovtlnbDataKey, 11);
	protected PackedDecimalData wsaaWaiveSumins = new PackedDecimalData(13, 2);
	protected String wsaaWaiveIt = "";
	protected String wsaaWaiveCont = "";

	protected FixedLengthStringData wsaaZrwvflgs = new FixedLengthStringData(3);
	protected FixedLengthStringData[] wsaaZrwvflg = FLSArrayPartOfStructure(3, 1, wsaaZrwvflgs, 0);
	protected FixedLengthStringData wsaaTr517Rec = new FixedLengthStringData(250);
	protected FixedLengthStringData wsaaMainCrtable = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaMainCoverage = new FixedLengthStringData(2);
	protected ZonedDecimalData wsaaMainCessdate = new ZonedDecimalData(8, 0).setUnsigned();
	protected ZonedDecimalData wsaaMainPcessdte = new ZonedDecimalData(8, 0).setUnsigned();
	protected FixedLengthStringData wsaaMainMortclass = new FixedLengthStringData(1);

	protected FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	protected FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);

	protected FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	protected FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	protected FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	protected FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	protected FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	protected AnntTableDAM anntIO = new AnntTableDAM();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected CltsTableDAM cltsIO = new CltsTableDAM();
	protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	protected CovtrbnTableDAM covtrbnIO = new CovtrbnTableDAM();
	protected CovtunlTableDAM covtunlIO = new CovtunlTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	protected ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected LextTableDAM lextIO = new LextTableDAM();
	protected LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	protected PayrTableDAM payrIO = new PayrTableDAM();
	protected PovrTableDAM povrIO = new PovrTableDAM();
	protected RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	protected Freqcpy freqcpy = new Freqcpy();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	protected Datcon3rec datcon3rec = new Datcon3rec();
	protected Gensswrec gensswrec = new Gensswrec();
	protected T5606rec t5606rec = new T5606rec();
	protected T5667rec t5667rec = new T5667rec();
	protected T5671rec t5671rec = new T5671rec();
	protected T5675rec t5675rec = new T5675rec();
	protected T5687rec t5687rec = new T5687rec();
	protected T5688rec t5688rec = new T5688rec();
	protected T5674rec t5674rec = new T5674rec();
	protected T6005rec t6005rec = new T6005rec();
	protected Tr517rec tr517rec = new Tr517rec();
	protected T2240rec t2240rec = new T2240rec();
	protected Tr52drec tr52drec = new Tr52drec();
	protected Tr52erec tr52erec = new Tr52erec();
	protected Mgfeelrec mgfeelrec = new Mgfeelrec();
	protected Premiumrec premiumrec = new Premiumrec();
	
	
	protected Chkrlrec chkrlrec = new Chkrlrec();
	protected Txcalcrec txcalcrec = new Txcalcrec();
	protected Wssplife wssplife = new Wssplife();
	protected Sr516ScreenVars sv =getPScreenVars() ;// ScreenProgram.getScreenVars( Sr516ScreenVars.class);
	protected ErrorsInner errorsInner = new ErrorsInner();
	protected FormatsInner formatsInner = new FormatsInner();
	protected TablesInner tablesInner = new TablesInner();
	protected WsaaDefaultsInner wsaaDefaultsInner = new WsaaDefaultsInner();
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	protected ExternalisedRules er = new ExternalisedRules();
	protected boolean stampDutyflag = false;
	protected String stateCode="";
	protected boolean incomeProtectionflag = false;
	protected boolean premiumflag = false;
	protected RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	protected Rcvdpf rcvdPFObject= new Rcvdpf();
	protected String occuptationCode="";
	protected boolean loadingFlag = false;/* BRD-306 */
	protected boolean waitperiodFlag=false;
	protected boolean bentrmFlag=false;
	protected boolean poltypFlag=false;
	protected boolean prmbasisFlag=false;
	protected boolean dialdownFlag = false;
	protected ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	protected Exclpf exclpf=null;
	protected boolean exclFlag = false;
	private com.csc.smart400framework.dataaccess.model.Clntpf clnt;//ILIFE-8502
	private com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao;	//ILIFE-8502

	protected PackedDecimalData wsaaTax = new PackedDecimalData(17, 2).init(0);
	
	/*ILIFE-7934 : Start*/
	protected boolean mulProdflag = false;
	protected static final String IL_PROD_SETUP_FEATURE_ID="NBPRP096";
	protected static final String OIR = "OIR";
	protected static final String OIS = "OIS";
	protected static final String SIR = "SIR";
	protected static final String SIS = "SIS";
	/*ILIFE-7934 : End*/
	private Itempf itempf = new Itempf();
  	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
  	
	
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

	private FixedLengthStringData wsaaOccupationClass = new FixedLengthStringData(2);
	private int fupno = 0;
	private PackedDecimalData wsaaCount = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private List<Fluppf> fluplnbList = new ArrayList<Fluppf>();
	private T5661rec t5661rec = new T5661rec();
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private Ta610rec ta610rec = new Ta610rec();
	boolean isFollowUpRequired  = false;
	private Fluppf fluppf;
	boolean NBPRP056Permission  = false;
	private T3644rec t3644rec = new T3644rec();
	private static final String tA610 = "TA610";
	private static final String t3644 = "T3644";
	private static final String t5661 = "T5661";
	private static final String NBPRP056="NBPRP056";
	private List<Fluppf> fluppfAvaList =  new ArrayList<Fluppf>();	//ICIL-1494
	private List<Itempf> ta610List = new ArrayList<Itempf>();	//ICIL-1494
	//ILJ-43
	private boolean contDtCalcFlag = false;
	private String cntDteFeature = "NBPRP113";
	//end
	private PackedDecimalData wsaaCommissionPrem = new PackedDecimalData(17, 2).init(0).setUnsigned();   //IBPLIFE-5237
	
/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont1010, 
		cont1012, 
		cont1015, 
		premmeth1020, 
		cont1030, 
		exit1090, 
		benCessTerm1410, 
		exit1490, 
		riskCessTerm1510, 
		exit1540, 
		premCessTerm1560, 
		exit1590, 
		exit1790, 
		nextColumn1820, 
		moveDefaults1850, 
		preExit, 
		redisplay2480, 
		exit2490, 
		checkRcessFields2530, 
		ageAnniversary2541, 
		term2542, 
		termExact2543, 
		check2544, 
		checkOccurance2545, 
		checkTermFields2550, 
		checkComplete2555, 
		checkMortcls2560, 
		loop2565, 
		checkLiencd2570, 
		loop2575, 
		checkMore2580, 
		exit2590, 
		calc2710, 
		exit2790, 
		exit3490, 
		exit3790, 
		cont4710, 
		cont4715, 
		cont4717, 
		cont4720, 
		exit4790, 
		rolu4805, 
		cont4810, 
		cont4820, 
		readCovtrbn4830, 
		cont4835, 
		cont4837, 
		cont4840, 
		exit4890, 
		callCovtunlio6110, 
		check6120, 
		nextr6189, 
		exit6190, 
		a450CallTaxSubr, 
		a490Exit
	}

	public Pr516() {
		super();
		screenVars = sv;
		new ScreenModel("Sr516", AppVars.getInstance(), sv);
	}
	protected Sr516ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sr516ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			//TMLI-268
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			//TMLII-268
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1001();
				case cont1010: 
					cont1010();
					plan1010();
				case cont1012: 
					cont1012();
				case cont1015: 
					cont1015();
				case premmeth1020: 
					premmeth1020();
				case cont1030: 
					cont1030();
					cont1060();
					prot1070();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1001()
	{	
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");//ILJ-43
		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			if (isNE(wsspcomn.flag, "I")) {  //MTL130
				 sv.instPrem.set(0);		//MTL130
				 calcPremium2700();			//MTL130
				 sv.instprmErr.set(" ");    //MTL130
			}
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		// ILJ-43
		if (!contDtCalcFlag) {
			sv.riskCessAgeOut[varcom.nd.toInt()].set("Y");
		}
		// end
		premiumrec.premiumRec.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		wssplife.fupno.set(ZERO);
		/*    Initialise & Setup default values for screen fields.*/
		sv.anbAtCcd.set(0);
		sv.instPrem.set(0);
		sv.zbinstprem.set(0);
		sv.zlinstprem.set(0); 
		/*BRD-306 START */
		sv.loadper.set(0);
		sv.adjustageamt.set(0);
		sv.rateadj.set(0);
		sv.fltmort.set(0);
		sv.premadj.set(0);
	
		/*BRD-306 END */
		sv.zstpduty01.set(ZERO);
		sv.waitperiod.set(SPACES);
		sv.bentrm.set(SPACES);
		sv.prmbasis.set(SPACES);
		sv.poltyp.set(SPACES);
		sv.dialdownoption.set(SPACES);
		sv.numapp.set(0);
		sv.numavail.set(0);
		sv.premCessAge.set(0);
		sv.premCessTerm.set(0);
		sv.polinc.set(0);
		sv.riskCessAge.set(0);
		sv.riskCessTerm.set(0);
		sv.zrsumin.set(0);
		sv.taxamt.set(0);
		sv.benCessAge.set(0);
		sv.benCessTerm.set(0);
		wssplife.fuptype.set("N");
		wsaaCtrltime.set("N");
		sv.premCessDate.set(varcom.vrcmMaxDate);
		sv.riskCessDate.set(varcom.vrcmMaxDate);
		sv.benCessDate.set(varcom.vrcmMaxDate);
		prmbasisFlag=false;
		poltypFlag=false;
		bentrmFlag=false;
		waitperiodFlag=false;
		/*    Initialise WSSP field for use through linkage.*/
		wssplife.bigAmt.set(ZERO);
		wssplife.occdate.set(ZERO);
		wsaaTaxamt.set(ZERO);
		wsaaFirstTaxCalc.set("Y");
		/* Read CHDRLNB (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*BRD-306 START */
		sv.cnttype.set(chdrlnbIO.getCnttype());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5688);
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*BRD-306 END */
		/* Read T2240 for age definition.                                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tablesInner.t2240);
		/* MOVE CHDRLNB-CNTTYPE        TO ITEM-ITEMITEM.        <AGEBTH>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaT2240Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(tablesInner.t2240);
			/*     MOVE '***'              TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itemIO.setItemitem(wsaaT2240Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
		}
		t2240rec.t2240Rec.set(itemIO.getGenarea());
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				else { //MTL002
					if (isNE(t2240rec.agecode04,SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
			}
		}
		sv.zagelitOut[varcom.hi.toInt()].set("N");
		/* Read TR52D for Taxcode.                                         */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlnbIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");		//ILIFE-4225
			sv.taxindOut[varcom.pr.toInt()].set("N");
		}
		/* Read the PAYR record to get the Billing Details.*/
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction("READR");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* If Plan processing is  not  to be performed, then protect and*/
		/* non-display the following three fields:-*/
		/*  a) No. of Policies in Plan (default to 1)*/
		/*  b) No. Available (default to 1)*/
		/*  c) No. Applicable (default to 1)*/
		/* and processing for Variations will not occur.*/
		if (isEQ(chdrlnbIO.getPolinc(), 0)) {
			wsaaPlanproc.set("N");
		}
		else {
			wsaaPlanproc.set("Y");
		}
		if (nonplan.isTrue()) {
			sv.polinc.set(1);
			sv.numavail.set(1);
			sv.numapp.set(1);
			sv.polincOut[varcom.nd.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
		}
		/* The key  for Coverage/rider to be worked on will be available*/
		/* from  COVTLNB.  This is obtained by using the RETRV function.*/
		covtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getRider(), "00")) {
			goTo(GotoLabel.cont1010);
		}
		/*    If we are dealing with a coverage we should skip this part*/
		wsaaCovtlnbParams.set(covtlnbIO.getParams());
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setDataKey(wsaaCovtlnbDataKey);
		covtlnbIO.setRider(ZERO);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.endp)
		&& isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isNE(covtlnbIO.getCoverage(), wsaaCovtlnbCoverage)) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
			wsaaKillFlag.set("Y");
			goTo(GotoLabel.exit1090);
		}
		/* TO HAVE GOT THIS FAR MEANS A COVERAGE MUST EXIST*/
		/* FOR THE RIDER WE ARE EITHER ACCESSING OR CREATING.*/
		covtlnbIO.setParams(wsaaCovtlnbParams);
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.datcon1Rec.set(SPACES);
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
	}

protected void cont1010()
	{
		/* Check if there are any transactions by doing a BEGN  (to read*/
		/* the Coverage/rider summary) using the COVTRBN logical view.*/
		/* The COVTRBN key is :-*/
		covtrbnIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtrbnIO.setChdrnum(covtlnbIO.getChdrnum());
		covtrbnIO.setLife(covtlnbIO.getLife());
		covtrbnIO.setCoverage(covtlnbIO.getCoverage());
		covtrbnIO.setRider(covtlnbIO.getRider());
		covtrbnIO.setSeqnbr(ZERO);
		covtrbnIO.setFunction(varcom.begn);
		covtrbnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtrbnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

		SmartFileCode.execute(appVars, covtrbnIO);
		if (isNE(covtrbnIO.getStatuz(), varcom.oK)
		&& isNE(covtrbnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtrbnIO.getParams());
			fatalError600();
		}
		if (isNE(covtrbnIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covtrbnIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covtrbnIO.getLife(), covtlnbIO.getLife())
		|| isNE(covtrbnIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covtrbnIO.getRider(), covtlnbIO.getRider())) {
			covtrbnIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtrbnIO.getStatuz(), varcom.endp)) {
			covtrbnIO.setNonKey(SPACES);
			covtrbnIO.setAnbccd(1, 0);
			covtrbnIO.setAnbccd(2, 0);
			covtrbnIO.setSingp(0);
			covtrbnIO.setInstprem(0);
			covtrbnIO.setZbinstprem(0);
			covtrbnIO.setZlinstprem(0);
			covtrbnIO.setNumapp(0);
			covtrbnIO.setPremCessAge(0);
			covtrbnIO.setPremCessTerm(0);
			covtrbnIO.setPolinc(0);
			covtrbnIO.setRiskCessAge(0);
			covtrbnIO.setRiskCessTerm(0);
			covtrbnIO.setSumins(0);
			covtrbnIO.setBenCessAge(0);
			covtrbnIO.setBenCessTerm(0);
			wsaaCtrltime.set("Y");
		}
	}

protected void plan1010()
	{
		if (firsttime.isTrue()) {
			covtrbnIO.setPolinc(ZERO);
			covtrbnIO.setNumapp(ZERO);
			covtrbnIO.setSeqnbr(1);
		}
		if (firsttime.isTrue()) {
			covtlnbIO.setSeqnbr(covtrbnIO.getSeqnbr());
			covtlnbIO.setFunction(varcom.keeps);
			covtlnbIO.setFormat(formatsInner.covtlnbrec);
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
		}
		/* If  Plan  processing is to occur, then calculate the "credit"*/
		/* as follows:*/
		/* - subtract  the  'No  of  policies in the Plan' from the*/
		/*   first  COVTRBN  record  read  above (if no COVTRBN,*/
		/*       this is zero) from the 'No of policies in the Plan'*/
		/*       (from the Contract-Header).*/
		if (plan.isTrue()) {
			compute(wsaaCredit, 0).set((sub(chdrlnbIO.getPolinc(), covtrbnIO.getPolinc())));
		}
		/* If this is a single policy plan*/
		/*    if it is the first time (no COVTRBN records)*/
		/*     or there is no credit*/
		/*   - plan processing is not required.*/
		if (isEQ(chdrlnbIO.getPolinc(), 1)
		&& (isEQ(covtrbnIO.getPolinc(), 0)
		|| isEQ(covtrbnIO.getPolinc(), 1))) {
			wsaaPlanproc.set("N");
			sv.polincOut[varcom.nd.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
		}
		dialdownFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		incomeProtectionflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP02", appVars, "IT");
		premiumflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");
		if(incomeProtectionflag || premiumflag || dialdownFlag){
			callReadRCVDPF();
		}
		if(!incomeProtectionflag){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
			sv.poltypOut[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.waitperiodOut[varcom.nd.toInt()].set("N");
			sv.bentrmOut[varcom.nd.toInt()].set("N");
			sv.poltypOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getWaitperiod()!=null && !rcvdPFObject.getWaitperiod().trim().equals("") && isEQ(sv.waitperiod,SPACES)){
					sv.waitperiod.set(rcvdPFObject.getWaitperiod());
				}
				if(rcvdPFObject.getPoltyp()!=null && !rcvdPFObject.getPoltyp().trim().equals("") && isEQ(sv.poltyp,SPACES)){
					sv.poltyp.set(rcvdPFObject.getPoltyp());
				}
				if(rcvdPFObject.getBentrm()!=null && !rcvdPFObject.getBentrm().trim().equals("") && isEQ(sv.bentrm,SPACES)){
					sv.bentrm.set(rcvdPFObject.getBentrm());
				}
			}
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		//BRD-NBP-011 starts				
		if(!dialdownFlag)
			sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
					}
				}
		}
		//BRD-NBP-011 ends
		exclFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.exclindOut[varcom.nd.toInt()].set("Y");
		}
		else{
			checkExcl();
		}

		/* Set  the  number of  policies  available  to  the  number  of*/
		/* policies on the plan.  If  COVTRBN  records are to be written*/
		/* for the first time  (as determined above), default the number*/
		/* applicable to the number available.*/
		wsaaFirstSeqnbr.set(covtrbnIO.getSeqnbr());
		wsaaNumavail.set(chdrlnbIO.getPolinc());
		/* BRD-306 starts */
		loadingFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		/* BRD-306 ends */
		if (firsttime.isTrue()) {
			covtrbnIO.setNumapp(wsaaNumavail);
			goTo(GotoLabel.cont1012);
		}
		sv.riskCessDate.set(covtrbnIO.getRiskCessDate());
		sv.premCessDate.set(covtrbnIO.getPremCessDate());
		sv.benCessDate.set(covtrbnIO.getBenCessDate());
		sv.riskCessAge.set(covtrbnIO.getRiskCessAge());
		sv.premCessAge.set(covtrbnIO.getPremCessAge());
		sv.benCessAge.set(covtrbnIO.getBenCessAge());
		sv.riskCessTerm.set(covtrbnIO.getRiskCessTerm());
		sv.premCessTerm.set(covtrbnIO.getPremCessTerm());
		sv.benCessTerm.set(covtrbnIO.getBenCessTerm());
		if(isEQ(scrnparams.deviceInd, "*RMT")){
			sv.instPrem.set(0);
		} else {
			if (isNE(covtrbnIO.getSingp(), ZERO)) {
				sv.instPrem.set(covtrbnIO.getSingp());
			}
			else {
				sv.instPrem.set(covtrbnIO.getInstprem());
			}
		}
		sv.zbinstprem.set(covtrbnIO.getZbinstprem());
		sv.zlinstprem.set(covtrbnIO.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtrbnIO.getLoadper());		
		sv.rateadj.set(covtrbnIO.getRateadj());
		sv.fltmort.set(covtrbnIO.fltmort);
		sv.premadj.set(covtrbnIO.getPremadj());
		sv.adjustageamt.set(covtrbnIO.getAgeadj());
		/*BRD-306 END */
		stampDutyflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.zstpduty01Out[varcom.nd.toInt()].set("N");
			if(isNE(covtrbnIO.getZstpduty01(),ZERO) && isEQ(sv.zstpduty01,ZERO)){
				sv.zstpduty01.set(covtrbnIO.getZstpduty01());
			}
		}

		sv.zrsumin.set(covtrbnIO.getSumins());
		sv.mortcls.set(covtrbnIO.getMortcls());
		sv.liencd.set(covtrbnIO.getLiencd());
		if (isEQ(covtrbnIO.getJlife(), "01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
		sv.bappmeth.set(covtrbnIO.getBappmeth());
	}
protected void checkExcl()
{
	exclpf=exclpfDAO.readRecord(covtlnbIO.getChdrcoy().toString(), covtlnbIO.getChdrnum().toString(),covtlnbIO.getCrtable().toString(),covtlnbIO.getLife().toString(),covtlnbIO.getCoverage().toString(),covtlnbIO.getRider().toString());
	if (exclpf==null) {
		sv.exclind.set(" ");
	}
	else {
		sv.exclind.set("+");
	}
}

protected void callReadRCVDPF(){
	if(rcvdPFObject == null)
		rcvdPFObject= new Rcvdpf();
	rcvdPFObject.setChdrcoy(covtrbnIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covtrbnIO.getChdrnum().toString());
	rcvdPFObject.setLife(covtrbnIO.getLife().toString());
	rcvdPFObject.setCoverage(covtrbnIO.getCoverage().toString());
	rcvdPFObject.setRider(covtrbnIO.getRider().toString());
	rcvdPFObject.setCrtable(covtrbnIO.getCrtable().toString());
	rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
	if(rcvdPFObject == null)
		sv.dialdownoption.set(100);
	}

protected void cont1012()
	{
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covtrbnIO.getNumapp());
		/* Read  the  contract  definition  details  from  T5688 for the*/
		/* contract  type  held  on  CHDRLNB. Access the version of this*/
		/* item for the original commencement date of the risk.*/
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5688, itdmIO.getItemtabl())
		|| isNE(chdrlnbIO.getCnttype(), itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/* Read  the  general  coverage/rider details from T5687 and the*/
		/* traditional/term  edit rules from T5606 for the coverage type*/
		/* held  on  COVTLNB.  Access  the version of this  item for the*/
		/* original commencement date of the risk.*/
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5687, itdmIO.getItemtabl())
		|| isNE(covtlnbIO.getCrtable(), itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			t5687rec.t5687Rec.set(SPACES);
			scrnparams.errorCode.set(errorsInner.f294);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		checkRacd1100();
		/* The following lines checks wether a window to Annuity*/
		/* Details is required. This is done by referring to*/
		/* Table T6625*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6625);
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		/* Check that the record is either found or at EOF*/
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		/* Check if the BEGN point is the correct record*/
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t6625, itdmIO.getItemtabl())
		|| isNE(covtlnbIO.getCrtable(), itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		/* Check if the record is on the table*/
		getAnnt5000();
		if (isEQ(itdmIO.getStatuz(), varcom.oK)) {
			sv.anntindOut[varcom.nd.toInt()].set("N");
			sv.numappOut[varcom.pr.toInt()].set("Y");
			sv.numapp.set(chdrlnbIO.getPolinc());
			if (isEQ(anntIO.getStatuz(), varcom.mrnf)) {
				sv.anntind.set("X");
				sv.anntindOut[varcom.pr.toInt()].set("Y");
			}
			else {
				sv.anntind.set("+");
			}
		}
		else {
			sv.anntindOut[varcom.nd.toInt()].set("Y");
			sv.anntindOut[varcom.pr.toInt()].set("Y");
		}
		setupBonus1200();
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t5687);
		descIO.setDescitem(covtlnbIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1700();
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5671);
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(covtlnbIO.getCrtable());
		itemIO.setItemitem(wsbbTranCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		itdmIO.setItemtabl(tablesInner.t5606);
		if (isEQ(t5671rec.pgm[1], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
		wsbbCurrency.set(payrIO.getCntcurr());
		itdmIO.setItemitem(wsbbTranCurrency);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5606, itdmIO.getItemtabl())
		|| isNE(wsbbTranCurrency, itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)) {
			t5606rec.t5606Rec.set(itdmIO.getGenarea());
		}
		else {
			t5606rec.t5606Rec.set(SPACES);
			t5606rec.ageIssageFrms.fill("0");
			t5606rec.ageIssageTos.fill("0");
			t5606rec.termIssageFrms.fill("0");
			t5606rec.termIssageTos.fill("0");
			t5606rec.premCessageFroms.fill("0");
			t5606rec.premCessageTos.fill("0");
			t5606rec.premCesstermFroms.fill("0");
			t5606rec.premCesstermTos.fill("0");
			t5606rec.riskCessageFroms.fill("0");
			t5606rec.riskCessageTos.fill("0");
			t5606rec.riskCesstermFroms.fill("0");
			t5606rec.riskCesstermTos.fill("0");
			t5606rec.benCessageFroms.fill("0");
			t5606rec.benCessageTos.fill("0");
			t5606rec.benCesstermFrms.fill("0");
			t5606rec.benCesstermTos.fill("0");
			t5606rec.sumInsMax.set(ZERO);
			t5606rec.sumInsMin.set(ZERO);
			if (isEQ(scrnparams.errorCode, SPACES)) {
				scrnparams.errorCode.set(errorsInner.f344);
			}
		}
		/* Read the latest premium tollerance allowed.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5667);
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(wsbbCurrency);
		itemIO.setItemitem(wsbbT5667Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
		/* LIFE ASSURED AND JOINT LIFE DETAILS*/
		/* To obtain the life assured and joint-life details (if any) do*/
		/* the following;-*/
		/*  - read  the life details using LIFELNB (life number from*/
		/*       COVTLNB, joint life number '00').  Look up the name*/
		/*       from the  client  details  (CLTS)  and  format as a*/
		/*       "confirmation name".*/
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*    Save Main Life details within Working Storage for later use.*/
		wsaaAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		wsaaCltdob.set(lifelnbIO.getCltdob());
		wsaaSex.set(lifelnbIO.getCltsex());
		/*ILIFE-7934 : Starts*/
		mulProdflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), IL_PROD_SETUP_FEATURE_ID, appVars, "IT");
		if(mulProdflag && (chdrlnbIO.getCnttype().equals(OIS) || chdrlnbIO.getCnttype().equals(OIR)
				|| chdrlnbIO.getCnttype().equals(SIS) || chdrlnbIO.getCnttype().equals(SIR))){//ILIFE-8574
			sv.mortcls.set(lifelnbIO.getSmoking());
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/*ILIFE-7934 : Ends*/
		/*    Read CLTS record for Life and format name.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		stampDutyflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if((cltsIO.getClntStateCd()!= null) && stampDutyflag){
			stateCode=cltsIO.getClntStateCd().substring(3).trim();
		}
		if(isNE(cltsIO.getStatcode(), SPACES)&& (incomeProtectionflag || premiumflag)){
			occuptationCode=cltsIO.getStatcode().toString();
		}
		/*  - read the joint life details using LIFELNB (life number*/
		/*       from COVTLNB,  joint  life number '01').  If found,*/
		/*       look up the name from the client details (CLTS) and*/
		/*       format as a "confirmation name".*/
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsbbSex.set(SPACES);
			wsbbAnbAtCcd.set(0);
			goTo(GotoLabel.cont1015);
		}
		wsbbAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		wsbbCltdob.set(lifelnbIO.getCltdob());
		wsbbSex.set(lifelnbIO.getCltsex());
		/*    Read CLTS record for Life and format name.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.jlifcnum.set(lifelnbIO.getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void cont1015()
	{
		/* To  determine  which premium calculation method to use decide*/
		/* whether or  not  it  is  a Single or Joint-life case (it is a*/
		/* joint  life  case,  the  joint  life record was found above).*/
		/* Then:*/
		/*  - if it  is  a  single-life  case use, the single-method*/
		/*       from  T5687. The age to be used for validation will*/
		/*       be the age of the main life.*/
		/*  - if the joint-life indicator (from T5687) is blank, and*/
		/*       if  it  is  a Joint-life case, use the joint-method*/
		/*       from  T5687. The age to be used for validation will*/
		/*       be the age of the main life.*/
		/*  - if the  Joint-life  indicator  is  'N',  then  use the*/
		/*       Single-method.  But, if there is a joint-life (this*/
		/*       must be  a  rider  to have got this far) prompt for*/
		/*       the joint  life  indicator  to determine which life*/
		/*       the rider is to attach to.  In all other cases, the*/
		/*       joint life  indicator  should  be non-displayed and*/
		/*       protected.  The  age to be used for validation will*/
		/*       be the age  of the main or joint life, depending on*/
		/*       the one selected.*/
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			wsaaLifeind.set("S");
		}
		else {
			wsaaLifeind.set("J");
		}
		if (singlif.isTrue()) {
			itemIO.setItemitem(t5687rec.premmeth);
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			goTo(GotoLabel.premmeth1020);
		}
		if (isEQ(t5687rec.jlifePresent, SPACES)) {
			itemIO.setItemitem(t5687rec.jlPremMeth);
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			goTo(GotoLabel.premmeth1020);
		}
		/*  For a rider attaching to one life only for a joint life*/
		/*  case, if this is not the first time, set the life selection*/
		/*  indicator.*/
		itemIO.setItemitem(t5687rec.premmeth);
		if (nonfirst.isTrue()) {
			if (isEQ(covtrbnIO.getJlife(), "01")) {
				sv.select.set("J");
			}
			else {
				sv.select.set("L");
			}
		}
	}

protected void premmeth1020()
	{
		/*  - use the  premium-method  selected  from  T5687, if not*/
		/*       blank,  to access T5675.  This gives the subroutine*/
		/*       to use for the calculation.*/
		/*  - if the benefit billing method is not blank, non-display*/
		/*       and protect the premium field (so do not bother with*/
		/*       the rest of this).*/
		premReqd = "N";
		if (isNE(t5687rec.bbmeth, SPACES)) {
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.cont1030);
		}
		if (isEQ(itemIO.getItemitem(), SPACES)) {
			premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void cont1030()
	{
		sv.anbAtCcd.set(wsaaAnbAtCcd);
		sv.chdrnum.set(lifelnbIO.getChdrnum());
		sv.coverage.set(covtlnbIO.getCoverage());
		sv.currcd.set(payrIO.getCntcurr());
		sv.life.set(lifelnbIO.getLife());
		sv.rider.set(covtlnbIO.getRider());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		/* The fields to  be displayed on the screen are determined from*/
		/* the entry in table T5606 read earlier as follows:*/
		/*  - if  the  maximum  and  minimum   benefit  amounts  are*/
		/*       both  zero,  non-display  and protect  the  benefit*/
		/*       amount.  If  the  minimum  and maximum are both the*/
		/*       same, display the amount protected. Otherwise allow*/
		/*       input.*/
		/* NOTE - the benefit amt applies to the PLAN, so if it is to*/
		/*        be defaulted, scale it down according to the number*/
		/*        of policies applicable.*/
		if (isEQ(t5606rec.sumInsMax, 0)
		&& isEQ(t5606rec.sumInsMin, 0)) {
			sv.zrsuminOut[varcom.nd.toInt()].set("Y");
			sv.zrsuminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax, 0)) {
			sv.zrsuminOut[varcom.pr.toInt()].set("Y");
			sv.zrsumin.set(t5606rec.sumInsMax);
			if (plan.isTrue()) {
				compute(sv.zrsumin, 3).setRounded((div(mult(sv.zrsumin, sv.numapp), sv.numavail)));
			}
		}
		/*       benefit frequency short description (T3590).*/
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(tablesInner.t3590);
		descIO.setDescitem(payrIO.getBillfreq());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		else {
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.frqdesc.set(descIO.getShortdesc());
			}
			else {
				sv.frqdesc.fill("?");
			}
		}
		/*  - if all   the   valid   mortality  classes  are  blank,*/
		/*       non-display  and  protect  this  field. If there is*/
		/*       only  one  mortality  class, display and protect it*/
		/*       (no validation will be required).*/
		if (isEQ(t5606rec.mortclss, SPACES)) {
			/*sv.mortclsOut[varcom.nd.toInt()].set("Y");*/
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		if(isEQ(t5606rec.waitperiod, SPACES)){
			sv.waitperiodOut[varcom.nd.toInt()].set("Y");
		}
		else{
			waitperiodFlag=true;
		}
		if(isEQ(t5606rec.bentrm, SPACES)){
			sv.bentrmOut[varcom.nd.toInt()].set("Y");
		}
		else{
			bentrmFlag=true;
		}
		if(isEQ(t5606rec.poltyp, SPACES)){
			sv.poltypOut[varcom.nd.toInt()].set("Y");
		}
		else{
			poltypFlag=true;
		}
		if(isEQ(t5606rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		else{
			prmbasisFlag=true;
		}
		if (isNE(t5606rec.mortcls01, SPACES)
		&& isEQ(t5606rec.mortcls02, SPACES)
		&& isEQ(t5606rec.mortcls03, SPACES)
		&& isEQ(t5606rec.mortcls04, SPACES)
		&& isEQ(t5606rec.mortcls05, SPACES)
		&& isEQ(t5606rec.mortcls06, SPACES)) {
			sv.mortcls.set(t5606rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/*  - if all the valid lien codes are blank, non-display and*/
		/*       protect  this field. Otherwise, this is an optional*/
		/*       field.*/
		if (isEQ(t5606rec.liencds, SPACES)) {
			//sv.liencdOut[varcom.nd.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		/*  - using  the  age  next  birthday  (ANB at RCD) from the*/
		/*       applicable  life  (see above), look up Issue Age on*/
		/*       the AGE and TERM  sections.  If  the  age fits into*/
		/*       a "slot" in  one  of  these sections,  and the risk*/
		/*       cessation  limits   are  the   same,   default  and*/
		/*       protect the risk cessation fields. Also do the same*/
		/*       for the premium  cessation  details.  In this case,*/
		/*       also  calculate  the  risk  and  premium  cessation*/
		/*       dates.*/
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		if (nonfirst.isTrue()
		&& isEQ(covtrbnIO.getJlife(), "01")) {
			wszzCltdob.set(wsbbCltdob);
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
		}
		checkDefaults1800();
		if ((wsaaDefaultsInner.defaultRa.isTrue()
		|| wsaaDefaultsInner.defaultRt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(t5606rec.eaage, SPACES))) {
			riskCessDate1500();
		}
		if ((wsaaDefaultsInner.defaultPa.isTrue()
		|| wsaaDefaultsInner.defaultPt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(t5606rec.eaage, SPACES))) {
			premCessDate1550();
		}
		if ((wsaaDefaultsInner.defaultBa.isTrue()
		|| wsaaDefaultsInner.defaultBt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(t5606rec.eaage, SPACES))) {
			benCessDate1400();
		}
		if (isNE(t5606rec.eaage, SPACES)) {
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void cont1060()
	{
		/* OPTIONS AND EXTRAS*/
		/* If options and extras are  not  allowed (as defined by T5606)*/
		/* non-display and protect the fields.*/
		/* Otherwise,  read the  options  and  extras  details  for  the*/
		/* current coverage/rider.  If any  records  exist, put a '+' in*/
		/* the Options/Extras indicator (to show that there are some).*/
		if (isEQ(t5606rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			/*    PERFORM 1900-CHECK-LEXT.                                  */
			checkLext1900();
			if (isEQ(wsspcomn.flag, "I")
			&& isEQ(sv.optextind, SPACES)) {
				sv.optextindOut[varcom.pr.toInt()].set("Y");
			}
		}
	}

	/**
	* <pre>
	* ENQUIRY MODE
	* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to
	* protect  all  input  capable  fields  except  the  indicators
	* controlling  where  to  switch  to  next  (options and extras
	* indicator).
	* </pre>
	*/
protected void prot1070()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			sv.zrsuminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.bcessageOut[varcom.pr.toInt()].set("Y");
			sv.bcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instprmOut[varcom.pr.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.waitperiodOut[varcom.pr.toInt()].set("Y");
			sv.poltypOut[varcom.pr.toInt()].set("Y");
			sv.bentrmOut[varcom.pr.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");
		}
	}

	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void checkRacd1100()
	{
		readRacd1110();
	}

protected void readRacd1110()
	{
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		racdlnbIO.setChdrnum(covtlnbIO.getChdrnum());
		racdlnbIO.setLife(covtlnbIO.getLife());
		racdlnbIO.setCoverage(covtlnbIO.getCoverage());
		racdlnbIO.setRider(covtlnbIO.getRider());
		racdlnbIO.setSeqno(ZERO);
		racdlnbIO.setPlanSuffix(ZERO);
		racdlnbIO.setCestype("2");
		racdlnbIO.setFunction(varcom.begn);
		racdlnbIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		racdlnbIO.setFormat(formatsInner.racdlnbrec);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(), varcom.oK)
		&& isNE(racdlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(racdlnbIO.getParams());
			syserrrec.statuz.set(racdlnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(racdlnbIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(racdlnbIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(racdlnbIO.getLife(), covtlnbIO.getLife())
		|| isNE(racdlnbIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(racdlnbIO.getRider(), covtlnbIO.getRider())
		|| isNE(racdlnbIO.getPlanSuffix(), ZERO)
		|| isNE(racdlnbIO.getSeqno(), ZERO)
		|| isNE(racdlnbIO.getCestype(), "2")
		|| isEQ(racdlnbIO.getStatuz(), varcom.endp)) {
			if (isEQ(sv.ratypind, "X")) {
				/*NEXT_SENTENCE*/
			}
			else {
				sv.ratypind.set(SPACES);
				sv.ratypindOut[varcom.nd.toInt()].set("Y");
				sv.ratypindOut[varcom.pr.toInt()].set("Y");
			}
		}
		else {
			sv.ratypind.set("+");
			sv.ratypindOut[varcom.nd.toInt()].set("N");
			sv.ratypindOut[varcom.pr.toInt()].set("N");
		}
	}

protected void setupBonus1200()
	{
		para1200();
	}

protected void para1200()
	{
		/* Check if Coverage/Rider is a SUM product. If not SUM product*/
		/* protect field.*/
		/* If SUM product and default exists setup BAPPMETH and protect*/
		/* field. If SUM product and default not setup allow entry.*/
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t6005);
		itemIO.setItemcoy(covtlnbIO.getChdrcoy());
		itemIO.setItemitem(covtlnbIO.getCrtable());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			return ;
		}
		t6005rec.t6005Rec.set(itemIO.getGenarea());
		if (isEQ(t6005rec.ind, "1")) {
			sv.bappmeth.set(t6005rec.bappmeth01);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "2")) {
			sv.bappmeth.set(t6005rec.bappmeth02);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "3")) {
			sv.bappmeth.set(t6005rec.bappmeth03);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "4")) {
			sv.bappmeth.set(t6005rec.bappmeth04);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "5")) {
			sv.bappmeth.set(t6005rec.bappmeth05);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void benCessDate1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1400();
				case benCessTerm1410: 
					benCessTerm1410();
				case exit1490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1400()
	{
		if (isEQ(sv.benCessAge, 0)) {
			goTo(GotoLabel.benCessTerm1410);
		}
		if (isEQ(t5606rec.eaage, "A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.benCessAge, wszzAnbAtCcd));
		}
		if (isEQ(t5606rec.eaage, "E")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.benCessAge);
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.bcesdteErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1490);
		}
		sv.benCessDate.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1490);
	}

protected void benCessTerm1410()
	{
		if (isEQ(sv.benCessTerm, 0)) {
			sv.bcesdteErr.set(errorsInner.e186);
			return ;
		}
		if (isEQ(t5606rec.eaage, "A")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.freqFactor.set(sv.benCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t5606rec.eaage, "E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.benCessTerm, wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(), wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.bcesdteErr.set(datcon2rec.statuz);
			return ;
		}
		sv.benCessDate.set(datcon2rec.intDate2);
	}

protected void riskCessDate1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1500();
				case riskCessTerm1510: 
					riskCessTerm1510();
				case exit1540: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1500()
	{
		if (isEQ(sv.riskCessAge, 0)) {
			goTo(GotoLabel.riskCessTerm1510);
		}
		if (isEQ(t5606rec.eaage, "A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.riskCessAge, wszzAnbAtCcd));
		}
		if (isEQ(t5606rec.eaage, "E")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.riskCessAge);
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1540);
		}
		sv.riskCessDate.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1540);
	}

protected void riskCessTerm1510()
	{
		if (isEQ(sv.riskCessTerm, 0)) {
			sv.rcesdteErr.set(errorsInner.e186);
			return ;
		}
		if (isEQ(t5606rec.eaage, "A")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.freqFactor.set(sv.riskCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t5606rec.eaage, "E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.riskCessTerm, wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(), wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
			return ;
		}
		sv.riskCessDate.set(datcon2rec.intDate2);
	}

protected void premCessDate1550()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1550();
				case premCessTerm1560: 
					premCessTerm1560();
				case exit1590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1550()
	{
		if (isEQ(sv.premCessAge, 0)) {
			goTo(GotoLabel.premCessTerm1560);
		}
		if (isEQ(t5606rec.eaage, "A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge, wszzAnbAtCcd));
		}
		if (isEQ(t5606rec.eaage, "E")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.premCessAge);
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1590);
		}
		sv.premCessDate.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1590);
	}

protected void premCessTerm1560()
	{
		if (isEQ(sv.premCessTerm, 0)) {
			return ;
		}
		if (isEQ(t5606rec.eaage, "A")
		|| isEQ(t5606rec.eaage, SPACES)) {
			datcon2rec.freqFactor.set(sv.premCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t5606rec.eaage, "E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.premCessTerm, wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(), wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
			return ;
		}
		sv.premCessDate.set(datcon2rec.intDate2);
	}

protected void callDatcon21600()
	{
		/*PARA*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadHeading1700()
	{
		try {
			loadScreen1710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadScreen1710()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(30); !(isLT(wsaaX, 1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(30, wsaaX));
		if (isNE(wsaaY, 0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		/*wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);*/
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1730();
		}
		/*   WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		/*wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);*/
		sv.crtabtitl.set(wsaaHeading);
		goTo(GotoLabel.exit1790);
	}

protected void moveChar1730()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY, wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void checkDefaults1800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					searchTable1810();
				case nextColumn1820: 
					nextColumn1820();
				case moveDefaults1850: 
					moveDefaults1850();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*  - using  the  age  next  birthday  (ANB at RCD) from the
	*       applicable  life  (see above), look up Issue Age on
	*       the AGE and TERM  sections.  If  the  age fits into
	*       a "slot" in  one  of these sections, and the
	*       risk cessation limits  are  the  same,  default and
	*       protect the risk cessation fields. Also do the same
	*       for the premium  cessation  details.  In this case,
	*       also  calculate  the  risk  and  premium  cessation
	*       dates.
	* </pre>
	*/
protected void searchTable1810()
	{
		sub1.set(0);
		wsaaDefaultsInner.wsaaDefaults.set(SPACES);
	}

protected void nextColumn1820()
	{
		sub1.add(1);
		if (isGT(sub1, wsaaMaxOcc)) {
			goTo(GotoLabel.moveDefaults1850);
		}
		if (!wsaaDefaultsInner.nodefRa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.riskCessageFrom[sub1.toInt()], t5606rec.riskCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRa.isTrue()
				&& !wsaaDefaultsInner.defaultRt.isTrue()) {
					wsddRiskCessAge.set(t5606rec.riskCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.premCessageFrom[sub1.toInt()], t5606rec.premCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPa.isTrue()
				&& !wsaaDefaultsInner.defaultPt.isTrue()) {
					wsddPremCessAge.set(t5606rec.premCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefBa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.benCessageFrom[sub1.toInt()], t5606rec.benCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultBa.isTrue()
				&& !wsaaDefaultsInner.defaultBt.isTrue()) {
					wsddBenCessAge.set(t5606rec.benCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultBa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultBa.set("N");
					wsaaDefaultsInner.wsaaDefaultBt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefRt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.riskCesstermFrom[sub1.toInt()], t5606rec.riskCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRt.isTrue()
				&& !wsaaDefaultsInner.defaultRa.isTrue()) {
					wsddRiskCessTerm.set(t5606rec.riskCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.premCesstermFrom[sub1.toInt()], t5606rec.premCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPt.isTrue()
				&& !wsaaDefaultsInner.defaultPa.isTrue()) {
					wsddPremCessTerm.set(t5606rec.premCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefBt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5606rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5606rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5606rec.benCesstermFrm[sub1.toInt()], t5606rec.benCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultBt.isTrue()
				&& !wsaaDefaultsInner.defaultBa.isTrue()) {
					wsddBenCessTerm.set(t5606rec.benCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultBt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultBa.set("N");
					wsaaDefaultsInner.wsaaDefaultBt.set("N");
				}
			}
		}
		goTo(GotoLabel.nextColumn1820);
	}

protected void moveDefaults1850()
	{
		if (wsaaDefaultsInner.defaultRa.isTrue()) {
			sv.riskCessAge.set(wsddRiskCessAge);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultRt.isTrue()) {
			sv.riskCessTerm.set(wsddRiskCessTerm);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPa.isTrue()) {
			sv.premCessAge.set(wsddPremCessAge);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPt.isTrue()) {
			sv.premCessTerm.set(wsddPremCessTerm);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultBa.isTrue()) {
			sv.benCessAge.set(wsddBenCessAge);
			sv.bcessageOut[varcom.pr.toInt()].set("Y");
			sv.bcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultBt.isTrue()) {
			sv.benCessTerm.set(wsddBenCessTerm);
			sv.bcessageOut[varcom.pr.toInt()].set("Y");
			sv.bcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(covtlnbIO.getSumins(), ZERO) || isEQ(wsspcomn.flag,"M")) {
			calcWaiveSumin6000();
			addPolicyFee7000();
			sv.zrsumin.set(wsaaWaiveSumins);
		}
	}

protected void checkLext1900()
	{
		readLext1910();
	}

protected void readLext1910()
	{
		lextIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lextIO.setChdrnum(covtlnbIO.getChdrnum());
		lextIO.setLife(covtlnbIO.getLife());
		lextIO.setCoverage(covtlnbIO.getCoverage());
		lextIO.setRider(covtlnbIO.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.begn);
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(), lextIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), lextIO.getChdrnum())
		|| isNE(covtlnbIO.getLife(), lextIO.getLife())
		|| isNE(covtlnbIO.getCoverage(), lextIO.getCoverage())
		|| isNE(covtlnbIO.getRider(), lextIO.getRider())) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(), varcom.endp)) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		try {
			preStart();
			callScreenIo2010();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void preStart()
	{
		/*     SPECIAL EXIT PROCESSING                                     */
		/* Skip this section  if  returning  from  an optional selection   */
		/* (current stack position action flag = '*').                     */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (forcedKill.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/* Test the POVR file for the existance of a Premium Breakdown     */
		/* record. If one exists then allow the option to display the      */
		/* select window for enquiry....if not then protect.               */
		/* Test here so that the latest POVR is retreived when either      */
		/* returning from a selection or having calculated new values.     */
		readPovr5300();
		if (isEQ(povrIO.getStatuz(), varcom.endp)
		|| isNE(povrIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(povrIO.getChdrnum(), lifelnbIO.getChdrnum())
		|| isNE(povrIO.getLife(), lifelnbIO.getLife())
		|| isNE(povrIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(povrIO.getRider(), covtlnbIO.getRider())) {
			sv.pbind.set(SPACES);
			sv.pbindOut[varcom.nd.toInt()].set("Y");
			sv.pbindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.pbind.set("+");
			sv.pbindOut[varcom.nd.toInt()].set("N");
			sv.pbindOut[varcom.pr.toInt()].set("N");
			wsaaPayrBillfreq.set(payrIO.getBillfreq());
			wsaaPovrInstamnt.set(ZERO);
			sv.instPrem.set(ZERO);
			for (wsaaSub.set(1); !(isGT(wsaaSub, 25)); wsaaSub.add(1)){
				compute(wsaaPovrInstamnt, 3).setRounded((div(povrIO.getAnnamnt(wsaaSub), wsaaPayrBillfreq)));
				sv.instPrem.add(wsaaPovrInstamnt);
			}
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			a400CheckCalcTax();
		}
		/*if (isEQ(sv.taxamt, ZERO)) {
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.taxindOut[varcom.nd.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("Y");
		}*/
		//ILIFE-1702 STARTS BY SLAKKALA	
		if(isEQ(wsspcomn.flag, "I")){
			sv.taxamtOut[varcom.pr.toInt()].set("Y");					
		}
		//ILIFE-1702 ENDS 
		stampDutyflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
		}else{
			if(isNE(covtrbnIO.getZstpduty01(),ZERO) && isEQ(sv.zstpduty01,ZERO)){
				sv.zstpduty01.set(covtrbnIO.getZstpduty01());
			}
			
		}
	}

protected void callScreenIo2010()
	{
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2001();
					cont2040();
				case redisplay2480: 
					redisplay2480();
				case exit2490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2001()
	{
		/*    CALL 'SR516IO' USING        SCRN-SCREEN-PARAMS               */
		/*                                SR516-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If CF11 (KILL) is requested and the current credit is zero or*/
		/* equal to the number of policies in the plan (all or nothing),*/
		/* then skip the  validation.  Otherwise,  highlight  this as an*/
		/* error and then skip the remainder of the validation.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			if (isEQ(wsaaCredit, 0)
			|| isEQ(wsaaCredit, chdrlnbIO.getPolinc())) {
				goTo(GotoLabel.exit2490);
			}
			else {
				sv.numappErr.set(errorsInner.g622);
				goTo(GotoLabel.redisplay2480);
			}
		}
		/*VALIDATE*/
		a100CheckLimit();
		/* If  in  enquiry  mode,  skip  all field validation EXCEPT the*/
		/* options/extras indicator.*/
		if (isNE(wsspcomn.flag, "I")) {
			editCoverage2500();
		}
	}

protected void cont2040()
	{
		/* If  options/extras already exist, there will be a '+' in this*/
		/* field.  A  request  to access the details is made by entering*/
		/* 'X'.  No  other  values  (other  than  blank) are allowed. If*/
		/* options  and  extras  are  requested,  DO  NOT  CALCULATE THE*/
		/* PREMIUM THIS TIME AROUND.*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind, "+")
		&& isNE(sv.optextind, "X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		/* If  reassurance   already exists, there will be a '+' in this*/
		/* field.  A  request  to access the details is made by entering*/
		/* 'X'.  No  other  values  (other  than  blank) are allowed.*/
		if (isNE(sv.ratypind, " ")
		&& isNE(sv.ratypind, "+")
		&& isNE(sv.ratypind, "X")) {
			sv.ratypindErr.set(errorsInner.g620);
		}
		/* If the item selected prompts Annuity details, 'X' will*/
		/* occur here. If details exist '+' will occur. No other*/
		/* values are allowed.*/
		if (isNE(sv.anntind, " ")
		&& isNE(sv.anntind, "+")
		&& isNE(sv.anntind, "X")) {
			sv.anntindErr.set(errorsInner.g620);
		}
		/* Check the Taxcode indicator.                                    */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		if (exclFlag && isNE(sv.exclind, " ")
				&& isNE(sv.exclind, "+")
				&& isNE(sv.exclind, "X")) {
					sv.exclindErr.set(errorsInner.g620);
				}
		
		/* Check the premium breakdown indicator.*/
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind, "+")
		&& isNE(sv.pbind, "X")) {
			sv.pbindErr.set(errorsInner.g620);
		}
		/* Check to see if BONUS APPLICATION METHOD is valid for*/
		/* coverage/rider.*/
		if (isNE(sv.bappmeth, SPACES)
		&& isNE(sv.bappmeth, t6005rec.bappmeth01)
		&& isNE(sv.bappmeth, t6005rec.bappmeth02)
		&& isNE(sv.bappmeth, t6005rec.bappmeth03)
		&& isNE(sv.bappmeth, t6005rec.bappmeth04)
		&& isNE(sv.bappmeth, t6005rec.bappmeth05)) {
			sv.bappmethErr.set(errorsInner.d352);
		}
		/* If everything else is O-K calculate the Installment Premium.*/
		if (isNE(sv.optextind, "X")
		&& isNE(sv.anntind, "X")
		&& isEQ(sv.errorIndicators, SPACES)
		&& isNE(wsspcomn.flag, "I")) {
			calcPremium2700();
		}
		if(isEQ(sv.dialdownoptionErr,"E034")&& !dialdownFlag)
		{
			sv.dialdownoptionErr.set(SPACES);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.redisplay2480);
		}
		if(stampDutyflag){
			compute(sv.zbinstprem, 2).set(sub(sub(sv.instPrem, sv.zlinstprem),sv.zstpduty01));
		}else{
		compute(sv.zbinstprem, 2).set(sub(sv.instPrem, sv.zlinstprem));
		}
		/* If 'ROLD' was entered,check this is not the first page.*/
		if (isEQ(scrnparams.statuz, varcom.rold)
		&& isEQ(wsaaFirstSeqnbr, covtrbnIO.getSeqnbr())) {
			scrnparams.errorCode.set(errorsInner.e027);
			goTo(GotoLabel.redisplay2480);
		}
		validateOccupationOrOccupationClass();	//ICIL-1494
		/* If 'CALC' was entered then re-display the screen.*/
		if (isNE(scrnparams.statuz, varcom.calc)) {
			goTo(GotoLabel.exit2490);
		}
	}

protected void redisplay2480()
	{
		wsspcomn.edterror.set("Y");
	}



protected void validateOccupationOrOccupationClass() {
	isFollowUpRequired=false;
	NBPRP056Permission  = FeaConfg.isFeatureExist("2",NBPRP056, appVars, "IT");
	if(NBPRP056Permission &&  lifelnbIO != null) {
		readTA610();
		String occupation = lifelnbIO.getOccup().toString();
		if( occupation != null && !occupation.trim().isEmpty()) {
			for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
					&& isNE(ta610rec.occclassdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
				if(isEQ(occupation,ta610rec.occclassdes[wsaaCount.toInt()])){
					isFollowUpRequired = true;
					scrnparams.errorCode.set(errorsInner.rrsu); 
					break;
				}
			}
			
			if(!isFollowUpRequired){
				getOccupationClass2900(occupation);
				for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
						&& isNE(ta610rec.occcode[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
					if(isEQ(wsaaOccupationClass,ta610rec.occcode[wsaaCount.toInt()])){
						isFollowUpRequired = true;
						scrnparams.errorCode.set(errorsInner.rrsu); 
						break;
					}
				}	
			}
			
		}
	}
	
}



private void readTA610() {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(tA610);
	itempf.setItemitem(covtlnbIO.getCrtable().toString());
	itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
	itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
	ta610List = itempfDAO.findByItemDates(itempf);	//ICIL-1494
	if (ta610List.size()>0 && ta610List.get(0).getGenarea()!=null) {
		ta610rec.tA610Rec.set(StringUtil.rawToString(ta610List.get(0).getGenarea()));
	}	  
}
protected void getOccupationClass2900(String occupation) {
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl(t3644);
	   itempf.setItemitem(occupation);
	   itempf = itempfDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
	   wsaaOccupationClass.set(t3644rec.occupationClass.toString());
}
	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void editCoverage2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					editFund2500();
					checkSumin2525();
				case checkRcessFields2530: 
					checkRcessFields2530();
					checkPcessFields2535();
					checkBcessFields2537();
					checkAgeTerm2540();
				case ageAnniversary2541: 
					ageAnniversary2541();
				case term2542: 
					term2542();
				case termExact2543: 
					termExact2543();
				case check2544: 
					check2544();
				case checkOccurance2545: 
					checkOccurance2545();
				case checkTermFields2550: 
					checkTermFields2550();
				case checkComplete2555: 
					checkComplete2555();
				case checkMortcls2560: 
					checkMortcls2560();
				case loop2565: 
					loop2565();
				case checkLiencd2570: 
					checkLiencd2570();
				case loop2575: 
					loop2575();
				case checkMore2580: 
					checkMore2580();
				case exit2590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void editFund2500()
	{
		/* If plan processing, and no plicies applicable,*/
		/*   skip the validation as this COVTRBN is to be deleted.*/
		if (plan.isTrue()
		&& isEQ(sv.numapp, 0)) {
			goTo(GotoLabel.exit2590);
		}
		if (isEQ(sv.numapp, ZERO)) {
			sv.numappErr.set(errorsInner.l001);
		}
		/* Before  the  premium amount is calculated, the screen must be*/
		/* valid.  So  all  editing  is  completed before the premium is*/
		/* calculated.*/
		/*  1) Check  the  benefit amt,  if  applicable, against the*/
		/*       limits (NB. these apply to the plan, so adjust first).*/
		/*    - if only one benefit amt is allowed, re-calculate plan*/
		/*      level benefit amt (if applicable).*/
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax, 0)) {
			if (plan.isTrue()) {
				compute(sv.zrsumin, 3).setRounded((div(mult(t5606rec.sumInsMin, sv.numapp), sv.polinc)));
			}
		}
	}

protected void checkSumin2525()
	{
		if (isEQ(sv.zrsumin, ZERO) || isEQ(wsspcomn.flag,"M")) {
			calcWaiveSumin6000();
			addPolicyFee7000();
			sv.zrsumin.set(wsaaWaiveSumins);
		}
		if (plan.isTrue()) {
			compute(wsaaSumin, 3).setRounded((div(mult(sv.zrsumin, sv.polinc), sv.numapp)));
		}
		else {
			wsaaSumin.set(sv.zrsumin);
		}
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)) {
			goTo(GotoLabel.checkRcessFields2530);
		}
		if (isLT(wsaaSumin, t5606rec.sumInsMin)) {
			sv.zrsuminErr.set(errorsInner.e416);
		}
		if (isGT(wsaaSumin, t5606rec.sumInsMax)) {
			sv.zrsuminErr.set(errorsInner.e417);
		}
	}

	/**
	* <pre>
	*  2) Check the consistency of the risk age and term fields
	*       and  premium  age and term fields. Either, risk age
	*       and  premium age must be used or risk term and
	*       premium  term  must  be  used.  They  must  not  be
	*       combined. Note that  these  only need validating if
	*       they were not defaulted.
	*     NOTE: Age and Term fields may now be mixed.
	* </pre>
	*/
protected void checkRcessFields2530()
	{
		if (isEQ(sv.select, "J")) {
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
			wszzCltdob.set(wsbbCltdob);
		}
		else {
			wszzAnbAtCcd.set(wsaaAnbAtCcd);
			wszzCltdob.set(wsaaCltdob);
		}
		if (isNE(t5687rec.jlifePresent, SPACES)
		&& jointlif.isTrue()) {
			checkDefaults1800();
		}
		if (isGT(sv.riskCessAge, 0)
		&& isGT(sv.riskCessTerm, 0)) {
			sv.rcessageErr.set(errorsInner.f220);
			sv.rcesstrmErr.set(errorsInner.f220);
		}
		if (isNE(t5606rec.eaage, SPACES)
		&& isEQ(sv.riskCessAge, 0)
		&& isEQ(sv.riskCessTerm, 0)) {
			sv.rcessageErr.set(errorsInner.e560);
			sv.rcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkPcessFields2535()
	{
		if (isGT(sv.premCessAge, 0)
		&& isGT(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)
		&& isEQ(sv.rcessageErr, SPACES)
		&& isEQ(sv.rcesstrmErr, SPACES)) {
			sv.premCessAge.set(sv.riskCessAge);
			sv.premCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5606rec.eaage, SPACES)
		&& isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkBcessFields2537()
	{
		if (isGT(sv.benCessAge, 0)
		&& isGT(sv.benCessTerm, 0)) {
			sv.bcessageErr.set(errorsInner.f220);
			sv.bcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.benCessAge, 0)
		&& isEQ(sv.benCessTerm, 0)
		&& isEQ(sv.rcessageErr, SPACES)
		&& isEQ(sv.rcesstrmErr, SPACES)) {
			sv.benCessAge.set(sv.riskCessAge);
			sv.benCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5606rec.eaage, SPACES)
		&& isEQ(sv.benCessAge, 0)
		&& isEQ(sv.benCessTerm, 0)) {
			sv.bcessageErr.set(errorsInner.e560);
			sv.bcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkAgeTerm2540()
	{
		if ((isNE(sv.rcessageErr, SPACES))
		|| (isNE(sv.rcesstrmErr, SPACES))
		|| (isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))
		|| (isNE(sv.bcessageErr, SPACES))
		|| (isNE(sv.bcesstrmErr, SPACES))) {
			goTo(GotoLabel.checkMortcls2560);
		}
		if ((isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))
		|| (isNE(sv.rcessageErr, SPACES))
		|| (isNE(sv.rcesstrmErr, SPACES))) {
			goTo(GotoLabel.checkMortcls2560);
		}
		/*  To get this far, everything must be consistant,*/
		/*   now cross check/calculate the risk and premium cessasion*/
		/*   dates. Cross validate these calculated dates against the*/
		/*   edit table (T5606).*/
		if (isNE(t5606rec.eaage, SPACES)
		|| isEQ(sv.riskCessDate, varcom.vrcmMaxDate)) {
			riskCessDate1500();
		}
		else {
			if (isEQ(sv.rcesdteErr, SPACES)) {
				if (isNE(sv.riskCessAge, 0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.riskCessAge);
					callDatcon21600();
					if (isLT(datcon2rec.intDate2, sv.riskCessDate)) {
						sv.rcessageErr.set(errorsInner.h040);
						sv.rcesdteErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21600();
						if (isGTE(datcon2rec.intDate2, sv.riskCessDate)) {
							sv.rcessageErr.set(errorsInner.h040);
							sv.rcesdteErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.riskCessTerm, 0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.riskCessTerm);
						callDatcon21600();
						if (isLT(datcon2rec.intDate2, sv.riskCessDate)) {
							sv.rcesstrmErr.set(errorsInner.h040);
							sv.rcesdteErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21600();
							if (isGTE(datcon2rec.intDate2, sv.riskCessDate)) {
								sv.rcesstrmErr.set(errorsInner.h040);
								sv.rcesdteErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(t5606rec.eaage, SPACES)
		|| isEQ(sv.premCessDate, varcom.vrcmMaxDate)) {
			premCessDate1550();
		}
		else {
			if (isEQ(sv.pcesdteErr, SPACES)) {
				if (isNE(sv.premCessAge, 0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.premCessAge);
					callDatcon21600();
					if (isLT(datcon2rec.intDate2, sv.premCessDate)) {
						sv.pcessageErr.set(errorsInner.h040);
						sv.pcesdteErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21600();
						if (isGTE(datcon2rec.intDate2, sv.premCessDate)) {
							sv.pcessageErr.set(errorsInner.h040);
							sv.pcesdteErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.premCessTerm, 0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.premCessTerm);
						callDatcon21600();
						if (isLT(datcon2rec.intDate2, sv.premCessDate)) {
							sv.pcesstrmErr.set(errorsInner.h040);
							sv.pcesdteErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21600();
							if (isGTE(datcon2rec.intDate2, sv.premCessDate)) {
								sv.pcesstrmErr.set(errorsInner.h040);
								sv.pcesdteErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(t5606rec.eaage, SPACES)
		|| isEQ(sv.benCessDate, varcom.vrcmMaxDate)) {
			benCessDate1400();
		}
		else {
			if (isEQ(sv.bcesdteErr, SPACES)) {
				if (isNE(sv.benCessAge, 0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.benCessAge);
					callDatcon21600();
					if (isLT(datcon2rec.intDate2, sv.benCessDate)) {
						sv.bcessageErr.set(errorsInner.h040);
						sv.bcesdteErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21600();
						if (isGTE(datcon2rec.intDate2, sv.benCessDate)) {
							sv.bcessageErr.set(errorsInner.h040);
							sv.bcesdteErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.benCessTerm, 0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.benCessTerm);
						callDatcon21600();
						if (isLT(datcon2rec.intDate2, sv.benCessDate)) {
							sv.bcesstrmErr.set(errorsInner.h040);
							sv.bcesdteErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21600();
							if (isGTE(datcon2rec.intDate2, sv.benCessDate)) {
								sv.bcesstrmErr.set(errorsInner.h040);
								sv.bcesdteErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(sv.rcesdteErr, SPACES)
		|| isNE(sv.pcesdteErr, SPACES)
		|| isNE(sv.bcesdteErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2560);
		}
		if (isEQ(sv.premCessDate, varcom.vrcmMaxDate)) {
			sv.premCessDate.set(sv.riskCessDate);
		}
		if (isEQ(sv.benCessDate, varcom.vrcmMaxDate)) {
			sv.benCessDate.set(sv.riskCessDate);
		}
		if (isGT(sv.premCessDate, sv.riskCessDate)) {
			sv.pcesdteErr.set(errorsInner.e566);
			sv.rcesdteErr.set(errorsInner.e566);
		}
		if (isNE(sv.rcesdteErr, SPACES)
		|| isNE(sv.pcesdteErr, SPACES)
		|| isNE(sv.bcesdteErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2560);
		}
		/*  Calculate cessasion age and term.*/
		/* MOVE WSZZ-CLTDOB            TO DTC3-INT-DATE-1.              */
		/* MOVE SR516-RISK-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-RISK-CESS-AGE.           */
		/* MOVE SR516-PREM-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-AGE.           */
		/* MOVE SR516-BEN-CESS-DATE    TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-BEN-CESS-AGE.            */
		/* MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-1.              */
		/* MOVE SR516-RISK-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-RISK-CESS-TERM.          */
		/* MOVE SR516-PREM-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-TERM.          */
		/* MOVE SR516-BEN-CESS-DATE    TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-BEN-CESS-TERM.           */
		/*  If age is already entered, do not need to re-calculate it      */
		/*  again.                                                         */
		if (isEQ(t5606rec.eaage, "A")) {
			goTo(GotoLabel.ageAnniversary2541);
		}
		if (isEQ(sv.riskCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		if (isEQ(sv.benCessAge, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32600();
			wszzBenCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzBenCessAge.set(sv.benCessAge);
		}
		goTo(GotoLabel.term2542);
	}

protected void ageAnniversary2541()
	{
		if (isEQ(sv.riskCessAge, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
			wszzRiskCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
			wszzPremCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		if (isEQ(sv.benCessAge, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32600();
			wszzBenCessAge.set(datcon3rec.freqFactor);
			wszzBenCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzBenCessAge.set(sv.benCessAge);
		}
	}

protected void term2542()
	{
		/*  If term is already entered, do not need to re-calculate it     */
		/*  again.                                                         */
		if (isEQ(t5606rec.eaage, "E")
		|| isEQ(t5606rec.eaage, " ")) {
			goTo(GotoLabel.termExact2543);
		}
		if (isEQ(sv.riskCessTerm, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		if (isEQ(sv.benCessTerm, ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32600();
			wszzBenCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzBenCessTerm.set(sv.benCessTerm);
		}
		goTo(GotoLabel.check2544);
	}

protected void termExact2543()
	{
		if (isEQ(sv.riskCessTerm, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			compute(wszzRiskCessTerm, 5).set(sub(datcon3rec.freqFactor, wszzAnbAtCcd));
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			compute(wszzPremCessTerm, 5).set(sub(datcon3rec.freqFactor, wszzAnbAtCcd));
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		if (isEQ(sv.benCessTerm, ZERO)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.benCessDate);
			callDatcon32600();
			compute(wszzBenCessTerm, 5).set(sub(datcon3rec.freqFactor, wszzAnbAtCcd));
		}
		else {
			wszzBenCessTerm.set(sv.benCessTerm);
		}
	}

protected void check2544()
	{
		/*  Assume the dates are invalid until proved otherwise.*/
		sv.rcessageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.bcessageErr.set(errorsInner.d028);
		sv.rcesstrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		sv.bcesstrmErr.set(errorsInner.d029);
		x.set(0);
	}

	/**
	* <pre>
	* Check each possible option.
	* </pre>
	*/
protected void checkOccurance2545()
	{
		x.add(1);
		if (isGT(x, wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2555);
		}
		if ((isEQ(t5606rec.ageIssageFrm[x.toInt()], 0)
		&& isEQ(t5606rec.ageIssageTo[x.toInt()], 0))
		|| isLT(wszzAnbAtCcd, t5606rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5606rec.ageIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkTermFields2550);
		}
		if (isGTE(wszzRiskCessAge, t5606rec.riskCessageFrom[x.toInt()])
		&& isLTE(wszzRiskCessAge, t5606rec.riskCessageTo[x.toInt()])) {
			sv.rcessageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge, t5606rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge, t5606rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
		if (isGTE(wszzBenCessAge, t5606rec.benCessageFrom[x.toInt()])
		&& isLTE(wszzBenCessAge, t5606rec.benCessageTo[x.toInt()])) {
			sv.bcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2550()
	{
		if ((isEQ(t5606rec.termIssageFrm[x.toInt()], 0)
		&& isEQ(t5606rec.termIssageTo[x.toInt()], 0))
		|| isLT(wszzAnbAtCcd, t5606rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5606rec.termIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkOccurance2545);
		}
		if (isGTE(wszzRiskCessTerm, t5606rec.riskCesstermFrom[x.toInt()])
		&& isLTE(wszzRiskCessTerm, t5606rec.riskCesstermTo[x.toInt()])) {
			sv.rcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm, t5606rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm, t5606rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzBenCessTerm, t5606rec.benCesstermFrm[x.toInt()])
		&& isLTE(wszzBenCessTerm, t5606rec.benCesstermTo[x.toInt()])) {
			sv.bcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2545);
	}

protected void checkComplete2555()
	{
		if (isNE(sv.rcesstrmErr, SPACES)
		&& isEQ(sv.riskCessTerm, ZERO)) {
			sv.rcesdteErr.set(sv.rcesstrmErr);
			sv.rcesstrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr, SPACES)
		&& isEQ(sv.premCessTerm, ZERO)) {
			sv.pcesdteErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.bcesstrmErr, SPACES)
		&& isEQ(sv.benCessTerm, ZERO)) {
			sv.bcesdteErr.set(sv.bcesstrmErr);
			sv.bcesstrmErr.set(SPACES);
		}
		if (isNE(sv.rcessageErr, SPACES)
		&& isEQ(sv.riskCessAge, ZERO)) {
			sv.rcesdteErr.set(sv.rcessageErr);
			sv.rcessageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr, SPACES)
		&& isEQ(sv.premCessAge, ZERO)) {
			sv.pcesdteErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
		if (isNE(sv.bcessageErr, SPACES)
		&& isEQ(sv.benCessAge, ZERO)) {
			sv.bcesdteErr.set(sv.bcessageErr);
			sv.bcessageErr.set(SPACES);
		}
	}

protected void checkMortcls2560()
	{
		/*  3) Mortality-Class,  if the mortality class appears on a*/
		/*       coverage/rider  screen  it  is  a  compulsory field*/
		/*       because it will  be used in calculating the premium*/
		/*       amount. The mortality class entered must one of the*/
		/*       ones in the edit rules table.*/
	/*Condition added by pmujavadiya for ILIFE-4485*/
	if(isNE(t5606rec.mortclss,SPACES) && isEQ(sv.mortcls,SPACES)){
		sv.mortclsErr.set(errorsInner.e186);
	goTo(GotoLabel.checkLiencd2570);
	}
		x.set(0);
	}

protected void loop2565()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.mortclsErr.set(errorsInner.e420);
			goTo(GotoLabel.checkLiencd2570);
		}
		if (/*isEQ(t5606rec.mortcls[x.toInt()], SPACES)
		||*/ isNE(t5606rec.mortcls[x.toInt()], sv.mortcls)) {
			goTo(GotoLabel.loop2565);
		}
	}
protected void checkIPPmandatory()
{
	if(AppVars.getInstance().getAppConfig().isVpmsEnable())
	{
		if(incomeProtectionflag)
		{
			if((isEQ(sv.waitperiod,SPACES)) && waitperiodFlag )
				sv.waitperiodErr.set(errorsInner.e186);
			
			if((isEQ(sv.bentrm,SPACES)) && bentrmFlag )
				sv.bentrmErr.set(errorsInner.e186);
			
			if((isEQ(sv.poltyp,SPACES)) && poltypFlag )
				sv.poltypErr.set(errorsInner.e186);
		
		}
		
		if(premiumflag)
		{
			if((isEQ(sv.prmbasis,SPACES)) && prmbasisFlag )
				sv.prmbasisErr.set(errorsInner.e186);
		}
	}
}
protected void checkIPPfields()
{
	boolean t5606Flag=false;
	for(int counter=1; counter< t5606rec.waitperiod.length;counter++){
		if(isNE(sv.waitperiod,SPACES)){
			if (isEQ(t5606rec.waitperiod[counter], sv.waitperiod)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.waitperiod,SPACES)){
		sv.waitperiodErr.set("RFUY");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.bentrm.length;counter++){
		if(isNE(sv.bentrm,SPACES)){
			if (isEQ(t5606rec.bentrm[counter], sv.bentrm)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.bentrm,SPACES)){
		sv.bentrmErr.set("RFUZ");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.poltyp.length;counter++){
		if(isNE(sv.poltyp,SPACES)){
			if (isEQ(t5606rec.poltyp[counter], sv.poltyp)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.poltyp,SPACES)){
		sv.poltypErr.set("RFV0");// new error code
	}
	t5606Flag=false;
	for(int counter=1; counter<t5606rec.prmbasis.length;counter++){
		if(isNE(sv.prmbasis,SPACES)){
			if (isEQ(t5606rec.prmbasis[counter], sv.prmbasis)){
				t5606Flag=true;
				break;
			}
		}
	}
	if(!t5606Flag && isNE(sv.prmbasis,SPACES)){
		sv.prmbasisErr.set("RFV1");// new error code
	}
	
}

protected void checkLiencd2570()
	{
		if (isEQ(sv.liencd, SPACES)) {
			goTo(GotoLabel.checkMore2580);
		}
		x.set(0);
		
	}

protected void loop2575()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2580);
		}
		if (isEQ(t5606rec.liencd[x.toInt()], SPACES)
		|| isNE(t5606rec.liencd[x.toInt()], sv.liencd)) {
			goTo(GotoLabel.loop2575);
		}
	}

protected void checkMore2580()
	{
		/* Check joint life selection indicator (if applicable).*/
		if (isNE(sv.selectOut[varcom.pr.toInt()], "Y")) {
			if (isNE(sv.select, SPACES)
			&& isNE(sv.select, "J")
			&& isNE(sv.select, "L")) {
				sv.selectErr.set(errorsInner.h039);
			}
		}
		if (isEQ(sv.numapp, ZERO)
		&& isEQ(sv.numavail, sv.polinc)) {
			sv.numappErr.set(errorsInner.l001);
		}
		wsaaWorkCredit.set(wsaaCredit);
		if (nonfirst.isTrue()) {
			wsaaWorkCredit.add(covtrbnIO.getNumapp());
		}
		wsaaWorkCredit.subtract(sv.numapp);
		if (isLT(wsaaWorkCredit, ZERO)
		&& isGT(sv.numapp, sv.numavail)) {
			sv.numappErr.set(errorsInner.h437);
			wsspcomn.edterror.set("Y");
		}
		if(incomeProtectionflag || premiumflag){
			checkIPPmandatory();
			checkIPPfields();//ILIFE-3348
		}
	}

protected void callDatcon32600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcPremium2700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2700();
				case calc2710: 
					calc2710();
				case exit2790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2700()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2790);
		}
		/* If plan processing, and no plicies applicable,*/
		/*   skip the validation as this COVTRBN is to be deleted.*/
		if (plan.isTrue()
		&& isEQ(sv.numapp, 0)) {
			goTo(GotoLabel.exit2790);
		}
		/* If benefit billed, do not calculate premium*/
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.exit2790);
		}
		/* PREMIUM CALCULATION*/
		/* The  premium amount is  required  on  all  products  and  all*/
		/* validating  must  be  successfully  completed  before  it  is*/
		/* calculated. If there is  no  premium  method defined (i.e the*/
		/* relevant code was blank), the premium amount must be entered.*/
		/* Otherwise, it is optional and always calculated.*/
		/* Note that a premium calculation subroutine may calculate the*/
		/* premium from the benefit amt OR the benefit amt from the*/
		/* premium.*/
		/* To calculate  it,  call  the  relevant calculation subroutine*/
		/* worked out above passing:*/
		if (isEQ(premReqd, "N")) {
			goTo(GotoLabel.calc2710);
		}
		if (isEQ(sv.instPrem, 0)) {
			sv.instprmErr.set(errorsInner.g818);
		}
		goTo(GotoLabel.exit2790);
	}

protected void calc2710()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(covtlnbIO.getCrtable());
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		if (isEQ(sv.select, "J")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.effectdt.set(chdrlnbIO.getOccdate());
		premiumrec.termdate.set(sv.premCessDate);
		premiumrec.currcode.set(chdrlnbIO.getCntcurr());
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/*  (wsaa-sumin already adjusted for plan processing)*/
		premiumrec.cnttype.set(chdrlnbIO.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.riskCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.benCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaBenCessTerm.set(datcon3rec.freqFactor);
		premiumrec.benCessTerm.set(wsaaBenCessTerm);
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrlnbIO.getOccdate());
		premiumrec.reRateDate.set(chdrlnbIO.getOccdate());
		premiumrec.calcPrem.set(sv.instPrem);
		premiumrec.calcBasPrem.set(sv.instPrem);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.commissionPrem.set(ZERO);
		
		//htruong25
		calc2710CustomerSpecific();
		premiumrec.loadper.set(ZERO);
		premiumrec.adjustageamt.set(ZERO);
		premiumrec.rateadj.set(ZERO);
		premiumrec.fltmort.set(ZERO);
		premiumrec.premadj.set(ZERO);
		if (plan.isTrue()) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.polinc), sv.numapp)));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.polinc), sv.numapp)));
		}
		getAnnt5000();
		/* If the annuity frequency is not being used then pass the*/
		/* benefit frequency to the calculation method using this field.*/
		/*    IF     WSAA-SUMFLAG NOT = SPACE                              */
		/*       AND CPRM-FREQANN     = SPACES                             */
		/*           MOVE T5606-BENFREQ   TO CPRM-FREQANN.                 */
		premiumrec.language.set(wsspcomn.language);

		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
		//ILIFE-7061 start	
		//htruong25
		calc2710CustomerSpecific2();
		premiumrec.loadper.set(0);
		premiumrec.adjustageamt.set(0);
		premiumrec.rateadj.set(0);
		premiumrec.fltmort.set(0);
		premiumrec.premadj.set(0);
		//ILIFE-8502-starts
		premiumrec.commTaxInd.set("Y");
		premiumrec.dialdownoption.set(sv.dialdownoption);
		premiumrec.prevSumIns.set(ZERO);
		premiumrec.inputPrevPrem.set(ZERO);
		clntDao = DAOFactory.getClntpfDAO();
		clnt=clntDao.getClientByClntnum(sv.lifcnum.toString());
		if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
			premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
		}else{
			premiumrec.stateAtIncep.set(cltsIO.getClntStateCd());
		}
		premiumrec.setPmexCall.set("Y");
		//ILIFE-8502-ends
		//ILIFE-7061 end
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			if(isNE(sv.waitperiod,SPACES)){
				premiumrec.waitperiod.set(sv.waitperiod);
			}
			if(isNE(sv.bentrm,SPACES)){
				premiumrec.bentrm.set(sv.bentrm);
			}
			if(isNE(sv.prmbasis,SPACES)){
				premiumrec.prmbasis.set(sv.prmbasis);
			}
			if(isNE(sv.poltyp,SPACES)){
				premiumrec.poltyp.set(sv.poltyp);
			}
			premiumrec.occpcode.set(occuptationCode);// TO BE MAPPED LATER GAURAV
			if(isNE(sv.dialdownoption,SPACES)){
				if(sv.dialdownoption.toString().startsWith("0"))
					premiumrec.dialdownoption.set(sv.dialdownoption.substring(1));
				else
					premiumrec.dialdownoption.set(sv.dialdownoption);
			}
			else
				premiumrec.dialdownoption.set("100");
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			if (isEQ(premiumrec.statuz, varcom.bomb)) {
				syserrrec.params.set(premiumrec.premiumRec);
				syserrrec.statuz.set(premiumrec.statuz);
				fatalError600();
			}
			if (isNE(premiumrec.statuz, varcom.oK)) {
				sv.instprmErr.set(premiumrec.statuz);
				goTo(GotoLabel.exit2790);
			}
			/*Ticket #ILIFE-2787 - Sr516_Screen Bomb_WOPL_DAN Start	*/
			premiumrec.loadper.set(0);
			premiumrec.adjustageamt.set(0);
			premiumrec.rateadj.set(0);
			premiumrec.fltmort.set(0);
			premiumrec.premadj.set(0);
			/*Ticket #ILIFE-2787 - End	*/
			if(stampDutyflag){
				sv.zstpduty01.set(ZERO);
				premiumrec.rstate01.set(stateCode.toUpperCase());
				premiumrec.rstate02.set(SPACES);
				premiumrec.zstpduty01.set(ZERO);
				premiumrec.zstpduty02.set(ZERO);
				
				//ILIFE-6382  
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("STAMPDUTY") && (er.isExternalized(chdrlnbIO.cnttype.toString(), covtlnbIO.crtable.toString()))) )
				{
					callProgram("STAMPDUTY", premiumrec.premiumRec,vpxlextrec,vpxacblrec.vpxacblRec);
					sv.zstpduty01.set(premiumrec.zstpduty01);
					
					if (isNE(premiumrec.zstpduty01, ZERO)) {
						compute(premiumrec.calcPrem, 3).set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
					}
				}
			
			}
		}
		/*Ticket #IVE-792 - End*/
		/*Ticket #ILIFE-2005 - End		*/
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.instprmErr.set(premiumrec.statuz);
			goTo(GotoLabel.exit2790);
		}
		/* Adjust premium calculated for plan processing.*/
		if (plan.isTrue()) {
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.numapp), sv.polinc)));
		}
		/* Put possibly calculated benefit amt back on the screen.*/
		if (plan.isTrue()) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.numapp), sv.polinc)));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem, sv.numapp), sv.polinc)));
			compute(sv.zrsumin, 3).setRounded((div(mult(premiumrec.sumin, sv.numapp), sv.polinc)));
		}
		else {
			sv.zrsumin.set(premiumrec.sumin);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		wsaaCommissionPrem.set(premiumrec.commissionPrem);    //IBPLIFE-5237
		/*BRD-306 START */
		sv.loadper.set(premiumrec.loadper);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		if(stampDutyflag)
		sv.zstpduty01.set(premiumrec.zstpduty01); //ILIFE-8502
		/*BRD-306 END */
		/* Having calculated it, the  entered value, if any, is compared*/
		/* with it to check that  it  is within acceptable limits of the*/
		/* automatically calculated figure.  If  it  is  less  than  the*/
		/* amount calculated and  within  tolerance  then  the  manually*/
		/* entered amount is allowed.  If  the entered value exceeds the*/
		/* calculated one, the calculated value is used.*/
		sv.instPrem.set(ZERO);
		if (isEQ(sv.instPrem, 0)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2790);
		}
		if (isGTE(sv.instPrem, premiumrec.calcPrem)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2790);
		}
		/* check the tolerance amount against the table entry read above.*/
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem, sv.instPrem));
		sv.instprmErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 11)
		|| isEQ(sv.instprmErr, SPACES)); wsaaSub.add(1)){
			searchForTolerance2740();
		}
		/* If the premium is within the tolerance limits, then we have to  */
		/* adjust the basic premium (the loaded premium will not change).  */
		if (isEQ(sv.instprmErr, SPACES)) {
			if(stampDutyflag){
				compute(sv.zbinstprem, 2).set(sub(sub(sv.instPrem, sv.zlinstprem),sv.zstpduty01));
			}else{
			compute(sv.zbinstprem, 2).set(sub(sv.instPrem, sv.zlinstprem));
		}
		}
		goTo(GotoLabel.exit2790);
	}

	/**
	* <pre>
	***  Calculate tolerance Limit and Check whether it is greater
	***  than the maximum tolerance amount in table T5667.
	* </pre>
	*/
protected void searchForTolerance2740()
	{
		if (isEQ(payrIO.getBillfreq(), t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtol[wsaaSub.toInt()])), 100));
			if (isLTE(wsaaDiff, wsaaTol)
			&& isLTE(wsaaDiff, t5667rec.maxAmount[wsaaSub.toInt()])) {
				/*         MOVE SPACES          TO SR516-INSTPRM-ERR.            */
				sv.instprmErr.set(SPACES);
				return ;
			}
			if (isNE(t5667rec.maxamt[wsaaSub.toInt()], 0)) {
				compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtoln[wsaaSub.toInt()])), 100));
				if (isLTE(wsaaDiff, wsaaTol)
				&& isLTE(wsaaDiff, t5667rec.maxamt[wsaaSub.toInt()])) {
					sv.instprmErr.set(SPACES);
				}
			}
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			loadWsspFields3010();
			checkCredits3020();
			if(incomeProtectionflag || premiumflag || dialdownFlag){
				insertAndUpdateRcvdpf();
			}
			if (isFollowUpRequired) {
				fluppfAvaList = fluppfDAO.searchFlupRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
				fupno = fluppfAvaList.size();	//ICIL-1494
				createFollowUps3400();
			}
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}
protected void createFollowUps3400()
{
	try {
		writeFollowUps3430();
	}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}

protected void writeFollowUps3430()
{
	for (wsaaCount.set(1); !(isGT(wsaaCount, 5)) && isNE(ta610rec.fupcdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)) {
		boolean entryFlag = false;
		//ILIFE-9209 STARTS
		if(fluppfAvaList!=null && !fluppfAvaList.isEmpty()) {
			for (Fluppf fluppfdata : fluppfAvaList) {
				if(isEQ(fluppfdata.getFupCde(),ta610rec.fupcdes[wsaaCount.toInt()])) 
					entryFlag=true;
			}
				
		}
		//ILIFE-9209 ENDS
		if(!entryFlag) {		//ILIFE-9209 
		writeFollowUp3500();
		}
	}

fluppfDAO.insertFlupRecord(fluplnbList);
}

protected void writeFollowUp3500()
{
	try {		
		fluppf = new Fluppf();
		lookUpStatus3510();
		lookUpDescription3520();
		writeRecord3530();
	}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}

protected void lookUpStatus3510() {
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(ta610rec.fupcdes[wsaaCount.toInt()]);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.e557);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}

protected void lookUpDescription3520()
{ 
	descpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());

}

protected void writeRecord3530()
{
	fupno++;
	
	fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
	fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
	setPrecision(fupno, 0);
	fluppf.setFupNo(fupno);
	fluppf.setLife("01");
	fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
	fluppf.setFupDt(wsaaToday.toInt());
	fluppf.setFupCde(ta610rec.fupcdes[wsaaCount.toInt()].toString());
	fluppf.setFupTyp('P');
	fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	fluppf.setFupRmk(descpf.getLongdesc());/* IJTI-1523 */
	fluppf.setjLife("00");
	fluppf.setTrdt(varcom.vrcmDate.toInt());
	fluppf.setTrtm(varcom.vrcmTime.toInt());
	fluppf.setUserT(varcom.vrcmUser.toInt());
	fluppf.setEffDate(wsaaToday.toInt());
	fluppf.setCrtDate(wsaaToday.toInt());
	fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
	fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
	fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
	fluplnbList.add(fluppf);
}

protected void loadWsspFields3010()
	{
		/*     SPECIAL EXIT PROCESSING*/
		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3490);
		}
		/* Updating occurs with  the Creation, Deletion or Updating of a*/
		/* COVTRBN transaction record.*/
		/* If the 'KILL' function key was pressed or if in enquiry mode,*/
		/* skip the updating.*/
		if (isEQ(scrnparams.statuz, "KILL")
		|| forcedKill.isTrue()) {
			goTo(GotoLabel.exit3490);
		}
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3490);
		}
		/* If premium breakdown selected then exit....*/
		if (isEQ(sv.pbind, "X")) {
			goTo(GotoLabel.exit3490);
		}
		/* If OPTIONS / EXTRAS selected do not write / update or delete*/
		/* record at this stage.*/
		if (isEQ(sv.optextind, "X")) {
			if (isNE(sv.premCessTerm, ZERO)) {
				wssplife.bigAmt.set(sv.premCessTerm);
				goTo(GotoLabel.exit3490);
			}
			else {
				datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
				datcon3rec.intDate2.set(sv.premCessDate);
				callDatcon33600();
				wsaaFreqFactor.set(datcon3rec.freqFactor);
				wssplife.fupno.set(wsaaFreqFactor);
				goTo(GotoLabel.exit3490);
			}
		}
		/* If there is an 'X' in the Annuity details SELECT, then*/
		/* the next program is skipped, as the new credit amounts*/
		/* cannot be properly calculated without the extra information.*/
		if (isEQ(sv.anntind, "X")) {
			goTo(GotoLabel.exit3490);
		}
		/* Skip write/update or delete COVTTRM if TAXIND = 'X'.         */
		if (isEQ(sv.taxind, "X")) {
			goTo(GotoLabel.exit3490);
		}
	}

protected void checkCredits3020()
	{
		/* Before  updating any  records,  calculate  the  new  "credit"*/
		/* amount.*/
		/*  - add to  the  current  credit  amount,  the  number  of*/
		/*       policies previously  applicable  from  the  COVTRBN*/
		/*       record (zero if  there was no COVTRBN) and subtract*/
		/*       the number applicable entered on the screen.*/
		/*  - a positive  credit means that additional policies must*/
		/*       be added to the plan.*/
		/*  - a negative  credit  means  that  some policies must be*/
		/*       removed from the plan.*/
		if (nonfirst.isTrue()) {
			wsaaCredit.add(covtrbnIO.getNumapp());
		}
		wsaaCredit.subtract(sv.numapp);
		/* If the number  applicable  is greater than zero, write/update*/
		/* the COVTRBN record.  If the number applicable is zero, delete*/
		/* the COVTRBN record.*/
		if (isGT(sv.numapp, 0)) {
			if (firsttime.isTrue()) {
				initcovr3500();
				setupcovtrbn3550();
				covtrbnIO.setFunction(varcom.writr);
				wsaaCtrltime.set("N");
			}
			else {
				setupcovtrbn3550();
				covtrbnIO.setFunction(varcom.updat);
			}
		}
		if (isLTE(sv.numapp, ZERO)) {
			covtrbnIO.setChdrcoy(covtlnbIO.getChdrcoy());
			covtrbnIO.setChdrnum(covtlnbIO.getChdrnum());
			covtrbnIO.setLife(covtlnbIO.getLife());
			covtrbnIO.setCoverage(covtlnbIO.getCoverage());
			covtrbnIO.setRider(covtlnbIO.getRider());
			covtrbnIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, covtrbnIO);
			if (isNE(covtrbnIO.getStatuz(), varcom.oK)
			&& isNE(covtrbnIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(covtrbnIO.getParams());
				fatalError600();
			}
			else {
				if (isEQ(covtrbnIO.getStatuz(), varcom.mrnf)) {
					return ;
				}
				else {
					covtrbnIO.setFunction(varcom.delet);
					if (isEQ(wsaaFirstSeqnbr, covtrbnIO.getSeqnbr())) {
						wsaaFirstSeqnbr.set(ZERO);
					}
				}
			}
		}
		updateCovtrbn3700();
	}

	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void initcovr3500()
	{
		/*PARA*/
		/* Before creating a  new COVTRBN record initialise the coverage*/
		/* fields.*/
		covtrbnIO.setCrtable(covtlnbIO.getCrtable());
		covtrbnIO.setPayrseqno(1);
		covtrbnIO.setRiskCessAge(0);
		covtrbnIO.setPremCessAge(0);
		covtrbnIO.setBenCessAge(0);
		covtrbnIO.setRiskCessTerm(0);
		covtrbnIO.setPremCessTerm(0);
		covtrbnIO.setBenCessTerm(0);
		covtrbnIO.setSumins(0);
		covtrbnIO.setRiskCessDate(varcom.vrcmMaxDate);
		covtrbnIO.setPremCessDate(varcom.vrcmMaxDate);
		covtrbnIO.setBenCessDate(varcom.vrcmMaxDate);
		covtrbnIO.setEffdate(varcom.vrcmMaxDate);
		/*EXIT*/
	}

protected void setupcovtrbn3550()
	{
		para3550();
	}

protected void para3550()
	{
		/* Check for changes in the COVTRBN Record.*/
		wsaaUpdateFlag.set("N");
		if (isNE(sv.riskCessDate, covtrbnIO.getRiskCessDate())) {
			covtrbnIO.setRiskCessDate(sv.riskCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessDate, covtrbnIO.getPremCessDate())) {
			covtrbnIO.setPremCessDate(sv.premCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessDate, covtrbnIO.getBenCessDate())) {
			covtrbnIO.setBenCessDate(sv.benCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessAge, covtrbnIO.getRiskCessAge())) {
			covtrbnIO.setRiskCessAge(sv.riskCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessAge, covtrbnIO.getPremCessAge())) {
			covtrbnIO.setPremCessAge(sv.premCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessAge, covtrbnIO.getBenCessAge())) {
			covtrbnIO.setBenCessAge(sv.benCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessTerm, covtrbnIO.getRiskCessTerm())) {
			covtrbnIO.setRiskCessTerm(sv.riskCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessTerm, covtrbnIO.getPremCessTerm())) {
			covtrbnIO.setPremCessTerm(sv.premCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.benCessTerm, covtrbnIO.getBenCessTerm())) {
			covtrbnIO.setBenCessTerm(sv.benCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zrsumin, covtrbnIO.getSumins())) {
			covtrbnIO.setSumins(sv.zrsumin);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(payrIO.getBillfreq(), covtrbnIO.getBillfreq())) {
			covtrbnIO.setBillfreq(payrIO.getBillfreq());
			wsaaUpdateFlag.set("Y");
		}
		if (isEQ(covtrbnIO.getBillfreq(), "00")) {
			if (isNE(sv.instPrem, covtrbnIO.getSingp())) {
				covtrbnIO.setSingp(sv.instPrem);
				wsaaUpdateFlag.set("Y");
			}
		}
		else {
			if (isNE(sv.instPrem, covtrbnIO.getInstprem())) {
				covtrbnIO.setInstprem(sv.instPrem);
				wsaaUpdateFlag.set("Y");
			}
		}
		if (isNE(sv.zbinstprem, covtrbnIO.getZbinstprem())) {
			covtrbnIO.setZbinstprem(sv.zbinstprem);
			wsaaUpdateFlag.set("Y");
		}
		
		if(isNE(wsaaCommissionPrem,ZERO)){
			covtrbnIO.setcommPrem(wsaaCommissionPrem);    //IBPLIFE-5237
			wsaaUpdateFlag.set("Y");
		}
		
		if (isNE(sv.zlinstprem, covtrbnIO.getZlinstprem())) {
			covtrbnIO.setZlinstprem(sv.zlinstprem);
			/*BRD-306 START */			
			covtrbnIO.setLoadper(sv.loadper);
			covtrbnIO.setRateadj(sv.rateadj);
			covtrbnIO.setFltmort(sv.fltmort);
			covtrbnIO.setPremadj(sv.premadj);
			covtrbnIO.setAgeadj(sv.adjustageamt);
			/*BRD-306 END */
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.mortcls, covtrbnIO.getMortcls())) {
			covtrbnIO.setMortcls(sv.mortcls);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.liencd, covtrbnIO.getLiencd())) {
			covtrbnIO.setLiencd(sv.liencd);
			wsaaUpdateFlag.set("Y");
		}
		if ((isEQ(sv.select, "J")
		&& (isEQ(covtrbnIO.getJlife(), "00")
		|| isEQ(covtrbnIO.getJlife(), "  ")))
		|| ((isEQ(sv.select, " ")
		|| isEQ(sv.select, "L"))
		&& isEQ(covtrbnIO.getJlife(), "01"))) {
			wsaaUpdateFlag.set("Y");
			if (isEQ(sv.select, "J")) {
				covtrbnIO.setJlife("01");
			}
			else {
				covtrbnIO.setJlife("00");
			}
		}
		if (isNE(sv.polinc, covtrbnIO.getPolinc())) {
			covtrbnIO.setPolinc(sv.polinc);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.numapp, covtrbnIO.getNumapp())) {
			covtrbnIO.setNumapp(sv.numapp);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(payrIO.getBillchnl(), covtrbnIO.getBillchnl())) {
			covtrbnIO.setBillchnl(payrIO.getBillchnl());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(chdrlnbIO.getOccdate(), covtrbnIO.getEffdate())) {
			covtrbnIO.setEffdate(chdrlnbIO.getOccdate());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaAnbAtCcd, covtrbnIO.getAnbccd(1))) {
			covtrbnIO.setAnbccd(1, wsaaAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex, covtrbnIO.getSex(1))) {
			covtrbnIO.setSex(1, wsaaSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsbbAnbAtCcd, covtrbnIO.getAnbccd(2))) {
			covtrbnIO.setAnbccd(2, wsbbAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex, covtrbnIO.getSex(2))) {
			covtrbnIO.setSex(2, wsbbSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.bappmeth, covtrbnIO.getBappmeth())) {
			covtrbnIO.setBappmeth(sv.bappmeth);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zstpduty01, covtrbnIO.getZstpduty01())&& stampDutyflag ) {
			covtrbnIO.setZstpduty01(sv.zstpduty01);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(stateCode, covtrbnIO.getZclstate())&& stampDutyflag) {
			covtrbnIO.setZclstate(stateCode);
			wsaaUpdateFlag.set("Y");
		}
		covtrbnIO.setCntcurr(chdrlnbIO.getCntcurr());
	}

protected void callDatcon33600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateCovtrbn3700()
	{
		try {
			para3700();
			para3710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para3700()
	{
		if (isEQ(wsaaUpdateFlag, "N")) {
			goTo(GotoLabel.exit3790);
		}
	}

protected void para3710()
	{
		covtrbnIO.setTermid(varcom.vrcmTermid);
		covtrbnIO.setUser(varcom.vrcmUser);
		covtrbnIO.setTransactionDate(varcom.vrcmDate);
		covtrbnIO.setTransactionTime(varcom.vrcmTime);
		covtrbnIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtrbnIO.setChdrnum(covtlnbIO.getChdrnum());
		covtrbnIO.setLife(covtlnbIO.getLife());
		covtrbnIO.setCoverage(covtlnbIO.getCoverage());
		covtrbnIO.setRider(covtlnbIO.getRider());
		covtrbnIO.setFormat(formatsInner.covtrbnrec);
		SmartFileCode.execute(appVars, covtrbnIO);
		if (isNE(covtrbnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtrbnIO.getParams());
			syserrrec.statuz.set(covtrbnIO.getStatuz());
			fatalError600();
		}
	}
protected void insertAndUpdateRcvdpf(){
	boolean rcvdUpdateFlag=false;
if(rcvdPFObject!=null){
	rcvdPFObject.setChdrcoy(covtrbnIO.getChdrcoy().toString());
	rcvdPFObject.setChdrnum(covtrbnIO.getChdrnum().toString());
	rcvdPFObject.setCoverage(covtrbnIO.getCoverage().toString());
	rcvdPFObject.setCrtable(covtrbnIO.getCrtable().toString());
	rcvdPFObject.setLife(covtrbnIO.getLife().toString());
	rcvdPFObject.setRider(covtrbnIO.getRider().toString());
	if(rcvdPFObject.getWaitperiod() != null){
		if (isNE(sv.waitperiod, rcvdPFObject.getWaitperiod())) {
			rcvdPFObject.setWaitperiod(sv.waitperiod.toString());
			rcvdUpdateFlag=true;
		}
	}
	if(rcvdPFObject.getPoltyp() != null){
		if (isNE(sv.poltyp, rcvdPFObject.getPoltyp())) {
			rcvdPFObject.setPoltyp(sv.poltyp.toString());
			rcvdUpdateFlag=true;
		}
	}
	if(rcvdPFObject.getPrmbasis()!=null){
		if (isNE(sv.prmbasis, rcvdPFObject.getPrmbasis())) {
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
			rcvdUpdateFlag=true;
		}
	}
	if(rcvdPFObject.getBentrm()!=null){
		if (isNE(sv.bentrm, rcvdPFObject.getBentrm())) {
			rcvdPFObject.setBentrm(sv.bentrm.toString());
			rcvdUpdateFlag=true;
		}
	}
		//BRD-NBP-011 starts
	if(rcvdPFObject.getDialdownoption()!=null){
		if (isNE(sv.dialdownoption, rcvdPFObject.getDialdownoption())) {
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
			rcvdUpdateFlag=true;
		}
	}
		//BRD-NBP-011 ends
		if(rcvdUpdateFlag){
			rcvdDAO.updateIntoRcvdpf(rcvdPFObject);
		}
	}else{
		rcvdPFObject=new Rcvdpf();
		rcvdPFObject.setChdrcoy(covtrbnIO.getChdrcoy().toString());
		rcvdPFObject.setChdrnum(covtrbnIO.getChdrnum().toString());
		rcvdPFObject.setCoverage(covtrbnIO.getCoverage().toString());
		rcvdPFObject.setCrtable(covtrbnIO.getCrtable().toString());
		rcvdPFObject.setLife(covtrbnIO.getLife().toString());
		rcvdPFObject.setRider(covtrbnIO.getRider().toString());

		if(isNE(sv.waitperiod,SPACES)){
			rcvdPFObject.setWaitperiod(sv.waitperiod.toString());
		}
		if(isNE(sv.poltyp,SPACES)){
			rcvdPFObject.setPoltyp(sv.poltyp.toString());
		}
		if(isNE(sv.prmbasis,SPACES)){
			rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
		}
		if(isNE(sv.bentrm,SPACES)){
			rcvdPFObject.setBentrm(sv.bentrm.toString());
		}
		//BRD-NBP-011 starts
		if(isNE(sv.dialdownoption,SPACES)){
			rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
		}
		//BRD-NBP-011 ends
		rcvdPFObject.setUsrprf(covtrbnIO.getUserProfile().toString());
		rcvdPFObject.setJobnm(covtrbnIO.getJobName().toString());
		//rcvdpf.setDatime(covtrbnIO.getDatime());
		rcvdDAO.insertRcvdpfRecord((rcvdPFObject));
	}
	
}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		para4000();
	}

protected void para4000()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/*    spaces to  the current program entry in the program*/
		/*    stack and exit.*/
		if (forcedKill.isTrue()) {
			wsaaKillFlag.set("N");
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(sv.pbind, "X")) {
			pbindExe5100();
		}
		else {
			if (isEQ(sv.pbind, "?")) {
				pbindRet5200();
			}
			else {
				if (isEQ(sv.optextind, "X")) {
					optionsExe4500();
				}
				else {
					if (isEQ(sv.optextind, "?")) {
						optionsRet4600();
					}
					else {
						if (isEQ(sv.ratypind, "X")) {
							reassuranceExe4200();
						}
						else {
							if (isEQ(sv.ratypind, "?")) {
								reassuranceRet4300();
							}
							else {
								if (isEQ(sv.anntind, "X")) {
									annuityExe4400();
								}
								else {
									if (isEQ(sv.anntind, "?")) {
										annuityRet4900();
									}
									else {
										if (isEQ(sv.taxind, "X")) {
											taxExe5600();
										}
										else {
											if (isEQ(sv.taxind, "?")) {
												taxRet5700();
											}
											else {
												if (isEQ(sv.exclind, "X")) {
													exclExe6000();
												}
												else {
													if (isEQ(sv.exclind, "?")) {
														exclRet6100();
													}
											
											            else {
												           if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], " ")) {
													       wayout4700();
												               }
												                   else {
													                    wayin4800();
												                   }
											            }
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
protected void exclExe6000(){
	/* - Keep the CHDR/COVT record */
	//PINNACLE-2855
	chdrlnbIO.setFunction("KEEPS");
	chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
	syserrrec.params.set(chdrlnbIO.getParams());
	syserrrec.statuz.set(chdrlnbIO.getStatuz());
	fatalError600();
	}
	covtlnbIO.setFunction("KEEPS");
	covtlnbIO.setFormat(formatsInner.covtlnbrec);
	SmartFileCode.execute(appVars, covtlnbIO);
	if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(covtlnbIO.getParams());
		syserrrec.statuz.set(covtlnbIO.getStatuz());
		fatalError600();
	}
	sv.exclind.set("?");
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
		gensswrec.function.set("G");
		wsspcomn.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		wsspcomn.crtable.set(covtlnbIO.getCrtable().toString());
		
		if(isNE(wsspcomn.flag,"I")){
			wsspcomn.flag.set("X");
			wsspcomn.cmode.set("PS");}
			else
				wsspcomn.cmode.set("IFE");
			gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	
}
protected void exclRet6100(){
	
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	checkExcl();
	
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	wsspcomn.nextprog.set(scrnparams.scrname);
}

protected void reassuranceExe4200()
	{
		para4200();
	}

protected void para4200()
	{
		/*    If the reassurance details have been selected,(value - 'X'),*/
		/*    then set an asterisk in the program stack action field to*/
		/*    ensure that control returns here, set the parameters for*/
		/*    generalised secondary switching and save the original*/
		/*    programs from the program stack.*/
		/*    Set up WSSP-CURRFORM for next programs.*/
		wsspcomn.currfrom.set(chdrlnbIO.getOccdate());
		sv.ratypind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			save4510();
		}
		gensswrec.function.set("B");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	/**
	* <pre>
	*   If a value has been placed in the GENS-FUNCTION then call
	*   the generalised secondary switching module to obtain the
	*   next 8 programs and load them into the program stack.
	* </pre>
	*/
protected void gensww4210()
	{
		para4211();
	}

protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextIO.getStatuz(), varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

protected void reassuranceRet4300()
	{
		/*PARA*/
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			restore4610();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*EXIT*/
	}

protected void annuityExe4400()
	{
		para4400();
	}

protected void para4400()
	{
		/* If the ANNUITY details have been selected,(value - 'X'),*/
		/* THEN A '?' IS MOVED TO THE ANNT-IND SO THAT NEXT TIME THE*/
		/* PROGRAM IS RUN IT MOVES TO THE ANNUITY-RET(URN) SECTION*/
		/* RATHER THAN THIS*/
		sv.anntind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			save4510();
		}
		/* THE C SWITCHES THE PROGRAM TOWARDS P5220 AS DETERMINED*/
		/* IN TABLE T1675*/
		gensswrec.function.set("C");
		gensww4410();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		/*  Move the benefit amount to linkage to check than the*/
		/*  capital content is not greater than the benefit amount*/
		/*  in P5220.*/
		wssplife.bigAmt.set(sv.zrsumin);
	}

protected void gensww4410()
	{
		para4410();
	}

protected void para4410()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scr*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			sv.anntind.set("X");
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (isEQ(lextIO.getStatuz(), varcom.endp)) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

protected void optionsExe4500()
	{
		para4500();
	}

protected void para4500()
	{
		/* The  first  thing  to   consider   is   the  handling  of  an*/
		/* options/extras request. If the indicator is 'X', a request to*/
		/* visit options and extras has been made. In this case:*/
		/*  - change the options/extras request indicator to '?',*/
		sv.optextind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			save4510();
		}
		/*  - call GENSSWCH with and  action  of 'A' to retrieve the*/
		/*       program switching required,  and  move  them to the*/
		/*       stack,*/
		gensswrec.function.set("A");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/*  - calculate  the  premium  as  described  above  in  the*/
		/*       'Validation' section, and  check  that it is within*/
		/*       the tolerance limit,*/
		sv.optextind.set("+");
		calcPremium2700();
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		checkLext1900();
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void wayout4700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4700();
				case cont4710: 
					cont4710();
				case cont4715: 
					cont4715();
				case cont4717: 
					cont4717();
				case cont4720: 
					cont4720();
				case exit4790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4700()
	{
		/* If control is passed to this  part of the 4000 section on the*/
		/* way  out of the program,  ie.  after  screen  I/O,  then  the*/
		/* current stack position action  flag  will  be  blank.  If the*/
		/* 4000  section  is   being   performed  after  returning  from*/
		/* processing another program  then  the  current stack position*/
		/* action flag will be '*'.*/
		if (isEQ(scrnparams.statuz, varcom.rolu)
		|| isEQ(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.cont4710);
		}
		/*    If 'Enter' has been  pressed and "Credit" is non-zero*/
		/*    set the current select  action  field to '*', add 1*/
		/*    to the program pointer and exit.*/
		if (isNE(wsaaCredit, 0)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		}
		/*    If 'Enter' has been  pressed and "Credit" is zero add*/
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		goTo(GotoLabel.exit4790);
	}

protected void cont4710()
	{
		/*    If one of the Roll keys has been pressed and the*/
		/*    Number Applicable has been changed set the current*/
		/*    select action field to '*',  add 1 to the program*/
		/*    pointer and exit.*/
		if (isNE(sv.numapp, covtrbnIO.getNumapp())) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4790);
		}
		/*    If one of the Roll keys has been pressed and the*/
		/*    Number Applicable has not been changed then loop*/
		/*    round within the program and display the next /*/
		/*    previous screen as requested  (including BEGN).*/
		/*  Save the lase used sequence number in case we roll off the end*/
		wsaaSaveSeqnbr.set(covtrbnIO.getSeqnbr());
		if (isEQ(sv.numapp, 0)) {
			wsaaSaveSeqnbr.subtract(1);
		}
		/*  Cope with case when last record has just been added*/
		if (firsttime.isTrue()) {
			if (isEQ(scrnparams.statuz, varcom.rolu)) {
				covtrbnIO.setStatuz(varcom.endp);
				goTo(GotoLabel.cont4715);
			}
			else {
				covtrbnIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, covtrbnIO);
				if (isNE(covtrbnIO.getStatuz(), varcom.oK)
				&& isNE(covtrbnIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(covtrbnIO.getParams());
					fatalError600();
				}
			}
		}
		/* Read next/previous record*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			covtrbnIO.setFunction(varcom.nextr);
		}
		else {
			covtrbnIO.setFunction(varcom.nextp);
		}
		SmartFileCode.execute(appVars, covtrbnIO);
		if (isNE(covtrbnIO.getStatuz(), varcom.oK)
		&& isNE(covtrbnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtrbnIO.getParams());
			fatalError600();
		}
		if (isNE(covtrbnIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covtrbnIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covtrbnIO.getLife(), covtlnbIO.getLife())
		|| isNE(covtrbnIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covtrbnIO.getRider(), covtlnbIO.getRider())) {
			covtrbnIO.setStatuz(varcom.endp);
		}
	}

protected void cont4715()
	{
		if (isEQ(covtrbnIO.getStatuz(), varcom.endp)) {
			covtrbnIO.setNonKey(SPACES);
			covtrbnIO.setAnbccd(1, 0);
			covtrbnIO.setAnbccd(2, 0);
			covtrbnIO.setSingp(0);
			covtrbnIO.setInstprem(0);
			covtrbnIO.setZbinstprem(0);
			covtrbnIO.setZlinstprem(0);
			covtrbnIO.setNumapp(0);
			covtrbnIO.setPremCessAge(0);
			covtrbnIO.setPremCessTerm(0);
			covtrbnIO.setPolinc(0);
			covtrbnIO.setRiskCessAge(0);
			covtrbnIO.setRiskCessTerm(0);
			covtrbnIO.setSumins(0);
			covtrbnIO.setBenCessAge(0);
			covtrbnIO.setBenCessTerm(0);
			wsaaCtrltime.set("Y");
			goTo(GotoLabel.cont4717);
		}
		wsaaCtrltime.set("N");
		sv.riskCessDate.set(covtrbnIO.getRiskCessDate());
		sv.premCessDate.set(covtrbnIO.getPremCessDate());
		sv.benCessDate.set(covtrbnIO.getBenCessDate());
		sv.riskCessAge.set(covtrbnIO.getRiskCessAge());
		sv.premCessAge.set(covtrbnIO.getPremCessAge());
		sv.benCessAge.set(covtrbnIO.getBenCessAge());
		sv.riskCessTerm.set(covtrbnIO.getRiskCessTerm());
		sv.premCessTerm.set(covtrbnIO.getPremCessTerm());
		sv.benCessTerm.set(covtrbnIO.getBenCessTerm());
		if (isNE(covtrbnIO.getSingp(), ZERO)) {
			sv.instPrem.set(covtrbnIO.getSingp());
		}
		else {
			sv.instPrem.set(covtrbnIO.getInstprem());
		}
		sv.zbinstprem.set(covtrbnIO.getZbinstprem());
		sv.zlinstprem.set(covtrbnIO.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtrbnIO.getLoadper());
		sv.rateadj.set(covtrbnIO.getRateadj());
		sv.fltmort.set(covtrbnIO.fltmort);
		sv.premadj.set(covtrbnIO.getPremadj());
		sv.adjustageamt.set(covtrbnIO.getAgeadj());
		/*BRD-306 END */
		if(stampDutyflag && isNE(covtrbnIO.getZstpduty01(),ZERO) && isEQ(sv.zstpduty01,ZERO)){
			sv.zstpduty01.set(covtrbnIO.getZstpduty01());
		}
		sv.zrsumin.set(covtrbnIO.getSumins());
		sv.mortcls.set(covtrbnIO.getMortcls());
		sv.liencd.set(covtrbnIO.getLiencd());
		sv.bappmeth.set(covtrbnIO.getBappmeth());
		/*    KEEPS the new COVTLNB so that it can be retrieved by*/
		/*    subsequent programs.*/
		covtlnbIO.setChdrcoy(covtrbnIO.getChdrcoy());
		covtlnbIO.setChdrnum(covtrbnIO.getChdrnum());
		covtlnbIO.setLife(covtrbnIO.getLife());
		covtlnbIO.setCoverage(covtrbnIO.getCoverage());
		covtlnbIO.setRider(covtrbnIO.getRider());
		covtlnbIO.setSeqnbr(covtrbnIO.getSeqnbr());
		covtlnbIO.setFunction(varcom.keeps);
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
	}

protected void cont4717()
	{
		/*    When displaying a previous record, calculate the*/
		/*    number available as the current number plus the number*/
		/*    applicable to the record about to be displayed.*/
		if (isEQ(covtrbnIO.getFunction(), varcom.nextp)) {
			wsaaNumavail.add(covtrbnIO.getNumapp());
			goTo(GotoLabel.cont4720);
		}
		/*    When displaying the next record, or a blank screen to*/
		/*    enter a new record, calculate the number available as*/
		/*    the current number less the number applicable on the*/
		/*    current screen.*/
		/*    If displaying a blank screen for a new record, default*/
		/*    the number applicable to the number available. If this*/
		/*    is less than zero, default it to zero.*/
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail, 0)) {
			wsaaNumavail.set(0);
		}
		if (firsttime.isTrue()) {
			setPrecision(covtrbnIO.getSeqnbr(), 0);
			covtrbnIO.setSeqnbr(add(1, wsaaSaveSeqnbr));
			covtrbnIO.setNumapp(wsaaNumavail);
		}
	}

protected void cont4720()
	{
		if (isEQ(covtrbnIO.getFunction(), varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr, 0)) {
			wsaaFirstSeqnbr.set(covtrbnIO.getSeqnbr());
		}
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covtrbnIO.getNumapp());
		/* Re-calculate default SI if applicable*/
		/*  then re-calculate default premium if SI is different*/
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax, 0)
		&& isNE(wsspcomn.flag, "I")) {
			compute(sv.zrsumin, 3).setRounded((div(mult(t5606rec.sumInsMin, sv.numapp), sv.polinc)));
		}
		if (isNE(sv.zrsumin, covtrbnIO.getSumins())) {
			sv.instPrem.set(0);
			sv.zbinstprem.set(0);
			sv.zlinstprem.set(0);
			/*BRD-306 START */
			sv.loadper.set(0);
			sv.adjustageamt.set(0);
			sv.rateadj.set(0);
			sv.fltmort.set(0);
			sv.premadj.set(0);
			/*BRD-306 END */
			if(stampDutyflag){
				sv.zstpduty01.set(ZERO);
			}
			calcPremium2700();
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
		/* Life selection indicator may only be set up on the first*/
		/* screen*/
		if (isNE(t5687rec.jlifePresent, SPACES)
		&& jointlif.isTrue()
		&& isEQ(wsaaFirstSeqnbr, covtrbnIO.getSeqnbr())) {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void wayin4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4800();
				case rolu4805: 
					rolu4805();
				case cont4810: 
					cont4810();
				case cont4820: 
					cont4820();
				case readCovtrbn4830: 
					readCovtrbn4830();
				case cont4835: 
					cont4835();
				case cont4837: 
					cont4837();
				case cont4840: 
					cont4840();
				case exit4890: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4800()
	{
		if (isEQ(scrnparams.statuz, varcom.rolu)
		|| isEQ(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.cont4820);
		}
		if (isEQ(wsaaCredit, 0)) {
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4890);
		}
		if (isLTE(sv.numapp, sv.numavail)
		&& isLT(wsaaCredit, ZERO)) {
			goTo(GotoLabel.rolu4805);
		}
		if (isLT(wsaaCredit, 0)) {
			goTo(GotoLabel.cont4810);
		}
	}

	/**
	* <pre>
	*    If 'Enter' has been pressed and 'Credit' has a
	*    positive value then force the program to "roll up".
	* </pre>
	*/
protected void rolu4805()
	{
		scrnparams.statuz.set(varcom.rolu);
		goTo(GotoLabel.cont4820);
	}

protected void cont4810()
	{
		/*   If 'Enter' has been pressed and "Credit" has a negative*/
		/*   value, loop round within the program displaying the*/
		/*   details from the first transaction record, with the*/
		/*   'Number Applicable' highlighted, and give a message*/
		/*   indicating that more policies have been defined than*/
		/*   are on the Contract Header.*/
		covtrbnIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtrbnIO.setChdrnum(covtlnbIO.getChdrnum());
		covtrbnIO.setLife(covtlnbIO.getLife());
		covtrbnIO.setCoverage(covtlnbIO.getCoverage());
		covtrbnIO.setRider(covtlnbIO.getRider());
		covtrbnIO.setSeqnbr(0);
		/* The Error Message was being moved to the screen and not*/
		/* to the appropriate Screen Field:- SR516-NumApp-Err.*/
		/* This validation has been moved to the 2000 Section.*/
		covtrbnIO.setFunction(varcom.begn);
		goTo(GotoLabel.readCovtrbn4830);
	}

protected void cont4820()
	{
		/*    If one of the Roll keys has been pressed then loop*/
		/*    round within the program and display the next or*/
		/*    previous screen as requested (including  BEGN,*/
		/*    available count etc., see above).*/
		/*  Save the lase used sequence number in case we roll off the end*/
		wsaaSaveSeqnbr.set(covtrbnIO.getSeqnbr());
		if (isEQ(sv.numapp, 0)) {
			wsaaSaveSeqnbr.subtract(1);
		}
		/*  Cope with case when last record has just been added*/
		if (firsttime.isTrue()) {
			if (isEQ(scrnparams.statuz, varcom.rolu)) {
				covtrbnIO.setStatuz(varcom.endp);
				goTo(GotoLabel.cont4835);
			}
			else {
				covtrbnIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, covtrbnIO);
				if (isNE(covtrbnIO.getStatuz(), varcom.oK)
				&& isNE(covtrbnIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(covtrbnIO.getParams());
					fatalError600();
				}
			}
		}
		/* Read next/previous record*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			covtrbnIO.setFunction(varcom.nextr);
		}
		else {
			covtrbnIO.setFunction(varcom.nextp);
		}
	}

protected void readCovtrbn4830()
	{
		SmartFileCode.execute(appVars, covtrbnIO);
		if (isNE(covtrbnIO.getStatuz(), varcom.oK)
		&& isNE(covtrbnIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtrbnIO.getParams());
			fatalError600();
		}
		if (isNE(covtrbnIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covtrbnIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covtrbnIO.getLife(), covtlnbIO.getLife())
		|| isNE(covtrbnIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covtrbnIO.getRider(), covtlnbIO.getRider())) {
			covtrbnIO.setStatuz(varcom.endp);
		}
	}

protected void cont4835()
	{
		if (isEQ(covtrbnIO.getStatuz(),varcom.endp)) {
			wsaaCtrltime.set("Y");
			covtrbnIO.setNonKey(SPACES);
			covtrbnIO.setAnbccd(1, 0);
			covtrbnIO.setAnbccd(2, 0);
			covtrbnIO.setSingp(0);
			covtrbnIO.setInstprem(0);
			covtrbnIO.setZbinstprem(0);
			covtrbnIO.setZlinstprem(0);
			covtrbnIO.setNumapp(0);
			covtrbnIO.setPremCessAge(0);
			covtrbnIO.setPremCessTerm(0);
			covtrbnIO.setPolinc(0);
			covtrbnIO.setRiskCessAge(0);
			covtrbnIO.setRiskCessTerm(0);
			covtrbnIO.setSumins(0);
			covtrbnIO.setBenCessAge(0);
			covtrbnIO.setBenCessTerm(0);
			goTo(GotoLabel.cont4837);
		}
		wsaaCtrltime.set("N");
		sv.riskCessDate.set(covtrbnIO.getRiskCessDate());
		sv.premCessDate.set(covtrbnIO.getPremCessDate());
		sv.benCessDate.set(covtrbnIO.getBenCessDate());
		sv.riskCessAge.set(covtrbnIO.getRiskCessAge());
		sv.premCessAge.set(covtrbnIO.getPremCessAge());
		sv.benCessAge.set(covtrbnIO.getBenCessAge());
		sv.riskCessTerm.set(covtrbnIO.getRiskCessTerm());
		sv.premCessTerm.set(covtrbnIO.getPremCessTerm());
		sv.benCessTerm.set(covtrbnIO.getBenCessTerm());
		if (isNE(covtrbnIO.getSingp(), ZERO)) {
			sv.instPrem.set(covtrbnIO.getSingp());
		}
		else {
			sv.instPrem.set(covtrbnIO.getInstprem());
		}
		sv.zbinstprem.set(covtrbnIO.getZbinstprem());
		sv.zlinstprem.set(covtrbnIO.getZlinstprem());
		/*BRD-306 START */
		sv.loadper.set(covtrbnIO.getLoadper());
		sv.rateadj.set(covtrbnIO.getRateadj());
		sv.fltmort.set(covtrbnIO.fltmort);
		sv.premadj.set(covtrbnIO.getPremadj());
		sv.adjustageamt.set(covtrbnIO.getAgeadj());
		/*BRD-306 END */
		if(stampDutyflag && isNE(covtrbnIO.getZstpduty01(),ZERO) && isEQ(sv.zstpduty01,ZERO)){
			sv.zstpduty01.set(covtrbnIO.getZstpduty01());
		}
		sv.zrsumin.set(covtrbnIO.getSumins());
		sv.mortcls.set(covtrbnIO.getMortcls());
		sv.liencd.set(covtrbnIO.getLiencd());
		sv.bappmeth.set(covtrbnIO.getBappmeth());
	}

protected void cont4837()
	{
		/*    When displaying the first record again,  set the*/
		/*    number available as the number included in the plan*/
		if (isEQ(covtrbnIO.getFunction(), varcom.begn)) {
			wsaaNumavail.set(chdrlnbIO.getPolinc());
			wsaaFirstSeqnbr.set(covtrbnIO.getSeqnbr());
			goTo(GotoLabel.cont4840);
		}
		/*    When displaying a previous record, calculate the*/
		/*    number available as the current number plus the number*/
		/*    applicable to the record about to be displayed.*/
		if (isEQ(covtrbnIO.getFunction(), varcom.nextp)) {
			wsaaNumavail.add(covtrbnIO.getNumapp());
			goTo(GotoLabel.cont4840);
		}
		/*    When displaying the next record, or a blank screen to*/
		/*    enter a new record, calculate the number available as*/
		/*    the current number less the number applicable on the*/
		/*    current screen.*/
		/*    If displaying a blank screen for a new record, default*/
		/*    the number applicable to the number available. If this*/
		/*    is less than zero, default it to zero.*/
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail, 0)) {
			wsaaNumavail.set(0);
		}
		if (firsttime.isTrue()) {
			setPrecision(covtrbnIO.getSeqnbr(), 0);
			covtrbnIO.setSeqnbr(add(1, wsaaSaveSeqnbr));
			covtrbnIO.setNumapp(wsaaNumavail);
		}
		if (firsttime.isTrue()) {
			covtlnbIO.setSeqnbr(covtrbnIO.getSeqnbr());
			covtlnbIO.setFunction(varcom.keeps);
			covtlnbIO.setFormat(formatsInner.covtlnbrec);
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
		}
	}

protected void cont4840()
	{
		if (isEQ(covtrbnIO.getFunction(), varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr, 0)) {
			wsaaFirstSeqnbr.set(covtrbnIO.getSeqnbr());
		}
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covtrbnIO.getNumapp());
		/* Re-calculate default SI if applicable*/
		/*  then re-calculate default premium if SI is different*/
		if (isEQ(t5606rec.sumInsMax, t5606rec.sumInsMin)
		&& isNE(t5606rec.sumInsMax, 0)
		&& isNE(wsspcomn.flag, "I")) {
			compute(sv.zrsumin, 3).setRounded((div(mult(t5606rec.sumInsMin, sv.numapp), sv.polinc)));
		}
		if (isNE(sv.zrsumin, covtrbnIO.getSumins())) {
			sv.instPrem.set(0);
			sv.zbinstprem.set(0);
			sv.zlinstprem.set(0);
			/*BRD-306 START */
			sv.loadper.set(0);
			sv.adjustageamt.set(0);
			sv.rateadj.set(0);
			sv.fltmort.set(0);
			sv.premadj.set(0);
			/*BRD-306 END */
			if(stampDutyflag){
				sv.zstpduty01.set(ZERO);
			}
			calcPremium2700();
		}
		/* Life selection indicator may only be set up on the first*/
		/* screen*/
		if (isNE(t5687rec.jlifePresent, SPACES)
		&& jointlif.isTrue()
		&& isEQ(wsaaFirstSeqnbr, covtrbnIO.getSeqnbr())) {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void annuityRet4900()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/*  - calculate  the  premium  as  described  above  in  the*/
		/*       'Validation' section, and  check  that it is within*/
		/*       the tolerance limit,*/
		sv.anntind.set("+");
		calcPremium2700();
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
		/* Turn the protect switch off:*/
		sv.anntindOut[varcom.pr.toInt()].set("N");
		/*EXIT*/
	}

protected void getAnnt5000()
	{
		begins5010();
	}

protected void begins5010()
	{
		anntIO.setChdrcoy(covtlnbIO.getChdrcoy());
		anntIO.setChdrnum(covtlnbIO.getChdrnum());
		anntIO.setLife(covtlnbIO.getLife());
		anntIO.setCoverage(covtlnbIO.getCoverage());
		anntIO.setRider(covtlnbIO.getRider());
		anntIO.setSeqnbr(covtlnbIO.getSeqnbr());
		anntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, anntIO);
		if (isNE(anntIO.getStatuz(), varcom.oK)
		&& isNE(anntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(anntIO.getParams());
			syserrrec.statuz.set(anntIO.getStatuz());
			fatalError600();
		}
		if (isEQ(anntIO.getStatuz(), varcom.oK)) {
			premiumrec.freqann.set(anntIO.getFreqann());
			premiumrec.advance.set(anntIO.getAdvance());
			premiumrec.arrears.set(anntIO.getArrears());
			premiumrec.guarperd.set(anntIO.getGuarperd());
			premiumrec.intanny.set(anntIO.getIntanny());
			premiumrec.capcont.set(anntIO.getCapcont());
			premiumrec.withprop.set(anntIO.getWithprop());
			premiumrec.withoprop.set(anntIO.getWithoprop());
			premiumrec.ppind.set(anntIO.getPpind());
			premiumrec.nomlife.set(anntIO.getNomlife());
			premiumrec.dthpercn.set(anntIO.getDthpercn());
			premiumrec.dthperco.set(anntIO.getDthperco());
		}
		else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

protected void pbindExe5100()
	{
		start5110();
	}

protected void start5110()
	{
		/*  - Keep the POVR/CHDR record for Premium Breakdown enquiry.*/
		/*    Ensure we get the latest one.*/
		readPovr5300();
		if (isEQ(povrIO.getStatuz(), varcom.endp)
		|| isNE(povrIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(povrIO.getChdrnum(), lifelnbIO.getChdrnum())
		|| isNE(povrIO.getLife(), lifelnbIO.getLife())
		|| isNE(povrIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(povrIO.getRider(), covtlnbIO.getRider())) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		/* Use standard GENSWITCH table switching using the next option*/
		/* on table T1675.*/
		/*  - change the request indicator to '?',*/
		sv.pbind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar11 = 0; !(loopVar11 == 8); loopVar11 += 1){
			save4510();
		}
		/*  - call GENSSWCH with the next table action to retreive the*/
		/*    next program.*/
		gensswrec.function.set("D");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar12 = 0; !(loopVar12 == 8); loopVar12 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void pbindRet5200()
	{
		start5210();
	}

protected void start5210()
	{
		/* Release the POVR records as no longer required.*/
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/* Note that to have selected this option in the first place then*/
		/* details must exist.......set the flag for re-selection.*/
		sv.pbind.set("+");
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar13 = 0; !(loopVar13 == 8); loopVar13 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void readPovr5300()
	{
		start5310();
	}

protected void start5310()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(covtlnbIO.getChdrcoy());
		povrIO.setChdrnum(lifelnbIO.getChdrnum());
		povrIO.setLife(lifelnbIO.getLife());
		povrIO.setCoverage(covtlnbIO.getCoverage());
		povrIO.setRider(covtlnbIO.getRider());
		povrIO.setPlanSuffix(ZERO);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)
		&& isNE(povrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	*5400-CHECK-T5602 SECTION.                                        
	*5410-START.                                                      
	* Read the SUM Coverage table & check if the coverage is
	* a WOP. If so then non-display and protect the benefit amount
	* and benefit frequency description on screen.....
	*    MOVE SPACES                 TO ITEM-PARAMS.                  
	*    MOVE 'IT'                   TO ITEM-ITEMPFX.                 
	*    MOVE COVTLNB-CHDRCOY        TO ITEM-ITEMCOY.                 
	*    MOVE T5602                  TO ITEM-ITEMTABL.                
	*    MOVE COVTLNB-CRTABLE        TO ITEM-ITEMITEM.                
	*    MOVE ITEMREC                TO ITEM-FORMAT.                  
	*    MOVE READR                  TO ITEM-FUNCTION.                
	*    CALL 'ITEMIO' USING ITEM-PARAMS.                             
	*    IF ITEM-STATUZ = O-K                                         
	*       MOVE ITEM-GENAREA        TO T5602-T5602-REC               
	*       MOVE 'Y'                 TO WSAA-SUMFLAG                  
	*       IF T5602-INDIC-01 NOT = SPACE                             
	*          MOVE ZERO             TO SR516-ZRSUMIN                 
	*          MOVE 'Y'              TO SR516-ZRSUMIN-OUT (ND)        
	*                                   SR516-ZRSUMIN-OUT (PR)        
	*       END-IF                                                    
	*    ELSE                                                         
	*       MOVE SPACE               TO WSAA-SUMFLAG.                 
	*5490-EXIT.                                                       
	*    EXIT.                                                        
	* </pre>
	*/
protected void taxExe5600()
	{
		start5610();
	}

protected void start5610()
	{
		/* Keep the CHDR/COVT record                                       */
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		covtlnbIO.setZbinstprem(sv.zbinstprem);
		covtlnbIO.setInstprem(sv.instPrem);
		covtlnbIO.setFunction("KEEPS");
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		sv.taxind.set("?");
		/* Save the next 8 programs from the program stack,                */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar14 = 0; !(loopVar14 == 8); loopVar14 += 1){
			save4510();
		}
		/* Call GENSSWCH with the next table action to retreive the        */
		/* next program                                                    */
		gensswrec.function.set("F");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar15 = 0; !(loopVar15 == 8); loopVar15 += 1){
			load4530();
		}
		/* Set the current stack "action" to '*', add 1 to the pgm pointer */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet5700()
	{
		/*START*/
		sv.taxind.set("+");
		/* Restore the saved programs to the program stack                 */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar16 = 0; !(loopVar16 == 8); loopVar16 += 1){
			restore4610();
		}
		/* Blank out the stack "action"                                    */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/* Set WSSP-NEXTPROG to the current screen name                    */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void calcWaiveSumin6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para6000();
				case callCovtunlio6110: 
					callCovtunlio6110();
				case check6120: 
					check6120();
				case nextr6189: 
					nextr6189();
				case exit6190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6000()
	{
		wsaaWaiveSumins.set(ZERO);
		readTr5176200();
		wsaaTr517Rec.set(tr517rec.tr517Rec);
		/* MOVE SPACES                 TO COVTUNL-PARAMS.               */
		covtunlIO.setRecKeyData(SPACES);
		covtunlIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covtunlIO.setChdrnum(covtlnbIO.getChdrnum());
		covtunlIO.setLife(covtlnbIO.getLife());
		covtunlIO.setCoverage(covtlnbIO.getCoverage());
		/* MOVE SPACES                 TO COVTUNL-LIFE.                 */
		/* MOVE SPACES                 TO COVTUNL-COVERAGE.             */
		/* MOVE SPACES                 TO COVTUNL-RIDER.                */
		/* MOVE ZEROES                 TO COVTUNL-SEQNBR.               */
		if (isEQ(tr517rec.zrwvflg04, "N")) {
			covtunlIO.setLife(SPACES);
			covtunlIO.setCoverage(SPACES);
			covtunlIO.setRider(SPACES);
			covtunlIO.setSeqnbr(ZERO);
		}
		covtunlIO.setFunction(varcom.begn);
	}

protected void callCovtunlio6110()
	{
		SmartFileCode.execute(appVars, covtunlIO);
		tr517rec.tr517Rec.set(wsaaTr517Rec);
		if (isNE(covtunlIO.getStatuz(), varcom.oK)
		&& isNE(covtunlIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(covtunlIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covtunlIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isEQ(covtunlIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit6190);
		}
		if (isNE(covtunlIO.getLife(), covtlnbIO.getLife())
		&& isEQ(tr517rec.zrwvflg02, "N")) {
			/*        GO TO 6190-EXIT                                          */
			goTo(GotoLabel.nextr6189);
		}
		if (isEQ(tr517rec.zrwvflg04, "Y")) {
			if (isNE(covtunlIO.getCoverage(), covtlnbIO.getCoverage())) {
				covtunlIO.setStatuz(varcom.endp);
				/*      GO TO 6190-EXIT.                                 <LFA1058>*/
				goTo(GotoLabel.exit6190);
			}
		}
		wsaaWaiveIt = "N";
	}

protected void check6120()
	{
		for (sub1.set(1); !(isGT(sub1, 50)
		|| isEQ(wsaaWaiveIt, "Y")); sub1.add(1)){
			if (isEQ(covtunlIO.getCrtable(), tr517rec.ctable[sub1.toInt()])) {
				wsaaWaiveIt = "Y";
			}
		}
		if (isNE(wsaaWaiveIt, "Y")
		&& isNE(tr517rec.contitem, SPACES)) {
			readTr517620c();
			if (isEQ(wsaaWaiveCont, "Y")) {
				goTo(GotoLabel.check6120);
			}
		}
		/*    IF WSAA-WAIVE-IT            = 'Y'                            */
		/*        ADD COVTUNL-INSTPREM    TO WSAA-WAIVE-SUMINS             */
		/*    END-IF.                                                      */
		if (isEQ(tr517rec.zrwvflg04, "Y")
		&& isEQ(wsaaWaiveIt, "Y")) {
			if (isEQ(covtunlIO.getRider(), "00")) {
				/*        ADD  COVTUNL-SUMINS      TO WSAA-WAIVE-SUMINS         */
				wsaaWaiveSumins.set(covtunlIO.getSumins());
				wsaaMainCrtable.set(covtunlIO.getCrtable());
				wsaaMainCoverage.set(covtunlIO.getCoverage());
				wsaaMainCessdate.set(covtunlIO.getRiskCessDate());
				wsaaMainPcessdte.set(covtunlIO.getPremCessDate());
				wsaaMainMortclass.set(covtunlIO.getMortcls());
			}
			else {
				compute(wsaaWaiveSumins, 2).set(sub(wsaaWaiveSumins, covtunlIO.getSumins()));
				a200CalcBenefitAmount();
			}
		}
		else {
			if (isEQ(wsaaWaiveIt, "Y")) {
				wsaaWaiveSumins.add(covtunlIO.getInstprem());
				if (covtunlIO.getInstprem().compareTo(BigDecimal.ZERO)>0)						
					checkCalcCompTax7000();
			}
		}
	}

protected void nextr6189()
	{
		covtunlIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callCovtunlio6110);
	}

protected void checkCalcCompTax7000()
{
	wsaaTax.set(0);
	if (isEQ(tr52drec.txcode, SPACES)) {
		return ;
	}
	
	/* Read table TR52E                                                */
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
	wsaaTr52eCrtable.set(covtunlIO.getCrtable());
	a500ReadTr52e();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		a500ReadTr52e();
	}
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		a500ReadTr52e();
	}
	/* Call TR52D tax subroutine                                       */
	if (isNE(tr52erec.taxind01, "Y")) {
		return ;
	}
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
	txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
	txcalcrec.life.set(covtunlIO.getLife());
	txcalcrec.coverage.set(covtunlIO.getCoverage());
	txcalcrec.rider.set(covtunlIO.getRider());
	txcalcrec.planSuffix.set(ZERO);
	txcalcrec.crtable.set(covtunlIO.getCrtable());
	txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
	txcalcrec.register.set(chdrlnbIO.getRegister());
	txcalcrec.taxrule.set(wsaaTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
	wsaaCntCurr.set(chdrlnbIO.getCntcurr());
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.effdate.set(chdrlnbIO.getOccdate());
	txcalcrec.transType.set("PREM");
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	if (isNE(covtunlIO.getInstprem(), ZERO)) {
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(covtunlIO.getZbinstprem());
		}
		else {
			txcalcrec.amountIn.set(covtunlIO.getInstprem());
		}
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}	// changed by yy for ILIFE-3960
	
	wsaaWaiveSumins.add(wsaaTax);
}

protected void checkCalcContTax7100()
{
	start7110();
}

protected void start7110()
{
	wsaaTax.set(0);
	
	if (isEQ(tr52drec.txcode, SPACES)) {
		return ;
	}
	/* Read table TR52E                                                */
	wsaaTr52eKey.set(SPACES);
	wsaaTr52eTxcode.set(tr52drec.txcode);
	wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
	wsaaTr52eCrtable.set("****");
	a500ReadTr52e();
	if (isEQ(tr52erec.tr52eRec, SPACES)) {
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set("***");
		wsaaTr52eCrtable.set("****");
		a500ReadTr52e();
	}
	if (isNE(tr52erec.taxind02, "Y")) {
		return ;
	}
	/* Call TR52D tax subroutine                                       */
	initialize(txcalcrec.linkRec);
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
	txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
	txcalcrec.life.set(SPACES);
	txcalcrec.coverage.set(SPACES);
	txcalcrec.rider.set(SPACES);
	txcalcrec.crtable.set(covtlnbIO.getCrtable());
	txcalcrec.planSuffix.set(ZERO);
	txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
	txcalcrec.register.set(chdrlnbIO.getRegister());
	txcalcrec.taxrule.set(wsaaTr52eKey);
	wsaaRateItem.set(SPACES);
	txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
	wsaaCntCurr.set(chdrlnbIO.getCntcurr());
	wsaaTxitem.set(tr52erec.txitem);
	txcalcrec.rateItem.set(wsaaRateItem);
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	txcalcrec.amountIn.set(mgfeelrec.mgfee);
	txcalcrec.effdate.set(chdrlnbIO.getOccdate());
	txcalcrec.transType.set("CNTF");
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, Varcom.oK)) {
		syserrrec.params.set(txcalcrec.linkRec);
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaTax.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaTax.add(txcalcrec.taxAmt[2]);
		}
	}
	wsaaWaiveSumins.add(wsaaTax);
}

protected void readTr5176200()
	{
		para6200();
	}

protected void para6200()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(), covtlnbIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
	}

protected void readTr517620c()
	{
		para620c();
	}

protected void para620c()
	{
		wsaaWaiveCont = "N";
		wsaaZrwvflgs.set(tr517rec.zrwvflgs);
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(tr517rec.contitem);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFormat(formatsInner.itdmrec);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(), tr517rec.contitem)
		|| isNE(itdmIO.getStatuz(), varcom.oK)) {
			return ;
		}
		wsaaWaiveCont = "Y";
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		tr517rec.zrwvflgs.set(wsaaZrwvflgs);
	}

protected void addPolicyFee7000()
	{
		start7100();
	}

protected void start7100()
	{
		/*    Reference T5674 to obtain the subroutine required to work*/
		/*    out the Fee amount by the correct method.*/
		if (isNE(tr517rec.zrwvflg03, "Y")
		|| isEQ(t5688rec.feemeth, SPACES)) {
			return ;
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(tablesInner.t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction("READR");
		itemIO.setItemcoy(wsspcomn.company);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		/* Check subroutine NOT = SPACES before attempting call.           */
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		mgfeelrec.billfreq.set(payrIO.getBillfreq());
		mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set(wsspcomn.company);
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, varcom.oK)
		&& isNE(mgfeelrec.statuz, varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			fatalError600();
		}
		/*    IF COVTLNB-COVERAGE         = '01'                  <LFA1058>*/
		/* IF COVTLNB-COVERAGE         = '01' AND               <LA1168>*/
		/*       TR517-ZRWVFLG-04         = 'Y'                   <LFA1058>*/
		/*    TR517-ZRWVFLG-03         = 'Y'                    <LA1168>*/
		wsaaWaiveSumins.add(mgfeelrec.mgfee);
		//htruong25
		start7100CustomerSpecific();

		if (isNE(mgfeelrec.mgfee, ZERO)) {
			checkCalcContTax7100();
		}
	}
protected void start7100CustomerSpecific() {
	
}
protected void a100CheckLimit()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		chkrlrec.clntnum.set(sv.lifcnum);
		chkrlrec.company.set(wsspcomn.company);
		chkrlrec.currcode.set(sv.currcd);
		chkrlrec.cnttype.set(chdrlnbIO.getCnttype());
		chkrlrec.crtable.set(covtlnbIO.getCrtable());
		chkrlrec.sumins.set(sv.zrsumin);
		chkrlrec.life.set(covtlnbIO.getLife());
		chkrlrec.chdrnum.set(chdrlnbIO.getChdrnum());
		chkrlrec.function.set("CHCK");
		callProgram(Chkrlmt.class, chkrlrec.parmRec);
		if (isNE(chkrlrec.statuz, varcom.oK)) {
			if (isEQ(chkrlrec.statuz, errorsInner.e417)) {
				sv.zrsuminErr.set(errorsInner.e417);
			}
			else {
				syserrrec.statuz.set(chkrlrec.statuz);
				syserrrec.params.set(chkrlrec.params);
				syserrrec.iomod.set("CHKRLMT");
				fatalError600();
			}
		}
	}

protected void a200CalcBenefitAmount()
	{
		a200Ctrl();
	}

protected void a200Ctrl()
	{
		/* To read T5687 2 times here, one for the main coverage the other */
		/* read is to det it back to the current rider. Same with T5675.   */
		/*  This is the first read using the main component                */
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(wsaaMainCrtable);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5687, itdmIO.getItemtabl())
		|| isNE(wsaaMainCrtable, itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			t5687rec.t5687Rec.set(SPACES);
			scrnparams.errorCode.set(errorsInner.f294);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		/*  Read table T5675                                               */
		itemIO.setFunction(varcom.readr);
		itemIO.setItemitem(t5687rec.premmeth);
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			premReqd = "Y";
			return ;
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
		a300CallPremiumCalc();
		/*  This is the second call using the main component               */
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covtunlIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company, itdmIO.getItemcoy())
		|| isNE(tablesInner.t5687, itdmIO.getItemtabl())
		|| isNE(covtunlIO.getCrtable(), itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			t5687rec.t5687Rec.set(SPACES);
			scrnparams.errorCode.set(errorsInner.f294);
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		/*  Read table T5675  again                                        */
		itemIO.setFunction(varcom.readr);
		itemIO.setItemitem(t5687rec.premmeth);
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			premReqd = "Y";
			return ;
		}
		/*ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/*ILIFE-3142 End */
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

protected void a300CallPremiumCalc()
	{
		a300Ctrl();
	}

protected void a300Ctrl()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(wsaaMainCrtable);
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(wsaaMainCoverage);
		premiumrec.covrRider.set("00");
		premiumrec.effectdt.set(chdrlnbIO.getOccdate());
		premiumrec.termdate.set(wsaaMainPcessdte);
		premiumrec.currcode.set(chdrlnbIO.getCntcurr());
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/*  (wsaa-sumin already adjusted for plan processing)              */
		premiumrec.cnttype.set(chdrlnbIO.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(wsaaMainCessdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		premiumrec.benCessTerm.set(ZERO);
		premiumrec.sumin.set(wsaaWaiveSumins);
		premiumrec.mortcls.set(wsaaMainMortclass);
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.ratingdate.set(chdrlnbIO.getOccdate());
		premiumrec.reRateDate.set(chdrlnbIO.getOccdate());
		premiumrec.calcPrem.set(ZERO);
		premiumrec.calcBasPrem.set(ZERO);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #IV-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString())))
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{		
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
			/*Ticket #ILIFE-2787 - Sr516_Screen Bomb_WOPL_DAN Start	*/
			premiumrec.loadper.set(0);
			premiumrec.adjustageamt.set(0);
			premiumrec.rateadj.set(0);
			premiumrec.fltmort.set(0);
			premiumrec.premadj.set(0);
			/*Ticket #ILIFE-2787 - End*/
			//Australian Stamp Duty Calculation Externalization - Starts
			if(stampDutyflag){
				sv.zstpduty01.set(ZERO);
				premiumrec.rstate01.set(stateCode.toUpperCase());
				premiumrec.rstate02.set(SPACES);
				premiumrec.zstpduty01.set(ZERO);
				premiumrec.zstpduty02.set(ZERO);
				//ILIFE-6382
				
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("STAMPDUTY"))  && (er.isExternalized(chdrlnbIO.cnttype.toString(), covtlnbIO.crtable.toString())))  
				{
					callProgram("STAMPDUTY", premiumrec.premiumRec,vpxlextrec,vpxacblrec.vpxacblRec);
					sv.zstpduty01.set(premiumrec.zstpduty01);
					
					if (isNE(premiumrec.zstpduty01, ZERO)) {
						compute(premiumrec.calcPrem, 3).set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
					}
				}
				
			}
		}
		/*Ticket #IVE-792 - End*/	

		/*Ticket #ILIFE-2005 - End 
		*/		
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, varcom.oK)) {
			sv.instprmErr.set(premiumrec.statuz);
			return ;
		}
		wsaaWaiveSumins.set(premiumrec.calcPrem);
	}

protected void a400CheckCalcTax()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a410Start();
				case a450CallTaxSubr: 
					a450CallTaxSubr();
				case a490Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a410Start()
	{
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.a490Exit);
		}
		if (notFirstTaxCalc.isTrue()) {
			goTo(GotoLabel.a450CallTaxSubr);
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covtlnbIO.getCrtable());
		a500ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			a500ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			a500ReadTr52e();
		}
		wsaaFirstTaxCalc.set("N");
	}

protected void a450CallTaxSubr()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
			txcalcrec.life.set(covtlnbIO.getLife());
			txcalcrec.coverage.set(covtlnbIO.getCoverage());
			txcalcrec.rider.set(covtlnbIO.getRider());
			txcalcrec.planSuffix.set(ZERO);
			txcalcrec.crtable.set(covtlnbIO.getCrtable());
			txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
			txcalcrec.register.set(chdrlnbIO.getRegister());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
			wsaaCntCurr.set(chdrlnbIO.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(sv.zbinstprem);
			}
			else {
				txcalcrec.amountIn.set(sv.instPrem);
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrlnbIO.getOccdate());
			txcalcrec.tranno.set(chdrlnbIO.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				wsaaTaxamt.set(sv.instPrem);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				sv.taxind.set("+");
				sv.taxamtOut[varcom.nd.toInt()].set("N");
				sv.taxindOut[varcom.nd.toInt()].set("N");
				sv.taxamtOut[varcom.pr.toInt()].set("Y");		//ILIFE-4225
				sv.taxindOut[varcom.pr.toInt()].set("N");
			}
		}
	}

protected void a500ReadTr52e()
	{
		a510Start();
	}

protected void a510Start()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isNE(itdmIO.getItemcoy(), wsspcomn.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (((isEQ(itdmIO.getItemcoy(), wsspcomn.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

public ChdrlnbTableDAM getChdrlnbIO() {
	return chdrlnbIO;
}
public void setChdrlnbIO(ChdrlnbTableDAM chdrlnbIO) {
	this.chdrlnbIO = chdrlnbIO;
}

	protected void calc2710CustomerSpecific() {
		
	}
	protected void calc2710CustomerSpecific2() {
		
	}
	
	protected Premiumrec getPremiumrec() {
		return premiumrec ;
	}
public PackedDecimalData getSub1() {
		return sub1;
	}
	public void setSub1(PackedDecimalData sub1) {
		this.sub1 = sub1;
	}
public Tr517rec getTr517rec() {
		return tr517rec;
	}
	public void setTr517rec(Tr517rec tr517rec) {
		this.tr517rec = tr517rec;
	}
public String getWsaaWaiveIt() {
		return wsaaWaiveIt;
	}
	public void setWsaaWaiveIt(String wsaaWaiveIt) {
		this.wsaaWaiveIt = wsaaWaiveIt;
	}
public CovtunlTableDAM getCovtunlIO() {
		return covtunlIO;
	}
	public void setCovtunlIO(CovtunlTableDAM covtunlIO) {
		this.covtunlIO = covtunlIO;
	}
public String getWsaaWaiveCont() {
		return wsaaWaiveCont;
	}
	public void setWsaaWaiveCont(String wsaaWaiveCont) {
		this.wsaaWaiveCont = wsaaWaiveCont;
	}
public PackedDecimalData getWsaaWaiveSumins() {
		return wsaaWaiveSumins;
	}
	public void setWsaaWaiveSumins(PackedDecimalData wsaaWaiveSumins) {
		this.wsaaWaiveSumins = wsaaWaiveSumins;
	}
public FixedLengthStringData getWsaaMainCrtable() {
		return wsaaMainCrtable;
	}
	public void setWsaaMainCrtable(FixedLengthStringData wsaaMainCrtable) {
		this.wsaaMainCrtable = wsaaMainCrtable;
	}
public FixedLengthStringData getWsaaMainCoverage() {
		return wsaaMainCoverage;
	}
	public void setWsaaMainCoverage(FixedLengthStringData wsaaMainCoverage) {
		this.wsaaMainCoverage = wsaaMainCoverage;
	}
public ZonedDecimalData getWsaaMainCessdate() {
		return wsaaMainCessdate;
	}
	public void setWsaaMainCessdate(ZonedDecimalData wsaaMainCessdate) {
		this.wsaaMainCessdate = wsaaMainCessdate;
	}
public ZonedDecimalData getWsaaMainPcessdte() {
		return wsaaMainPcessdte;
	}
	public void setWsaaMainPcessdte(ZonedDecimalData wsaaMainPcessdte) {
		this.wsaaMainPcessdte = wsaaMainPcessdte;
	}
public FixedLengthStringData getWsaaMainMortclass() {
		return wsaaMainMortclass;
	}
	public void setWsaaMainMortclass(FixedLengthStringData wsaaMainMortclass) {
		this.wsaaMainMortclass = wsaaMainMortclass;
	}
public Mgfeelrec getMgfeelrec() {
		return mgfeelrec;
	}
	public void setMgfeelrec(Mgfeelrec mgfeelrec) {
		this.mgfeelrec = mgfeelrec;
	}
/*
 * Class transformed  from Data Structure WSAA-DEFAULTS--INNER
 */
protected static final class WsaaDefaultsInner { 

	protected FixedLengthStringData wsaaDefaults = new FixedLengthStringData(6);
	protected FixedLengthStringData wsaaDefaultRa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 0);
	protected Validator defaultRa = new Validator(wsaaDefaultRa, "Y");
	protected Validator nodefRa = new Validator(wsaaDefaultRa, "N");
	protected FixedLengthStringData wsaaDefaultPa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 1);
	protected Validator defaultPa = new Validator(wsaaDefaultPa, "Y");
	protected Validator nodefPa = new Validator(wsaaDefaultPa, "N");
	protected FixedLengthStringData wsaaDefaultBa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 2);
	protected Validator defaultBa = new Validator(wsaaDefaultBa, "Y");
	protected Validator nodefBa = new Validator(wsaaDefaultBa, "N");
	protected FixedLengthStringData wsaaDefaultRt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 3);
	protected Validator defaultRt = new Validator(wsaaDefaultRt, "Y");
	protected Validator nodefRt = new Validator(wsaaDefaultRt, "N");
	protected FixedLengthStringData wsaaDefaultPt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 4);
	protected Validator defaultPt = new Validator(wsaaDefaultPt, "Y");
	protected Validator nodefPt = new Validator(wsaaDefaultPt, "N");
	protected FixedLengthStringData wsaaDefaultBt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 5);
	protected Validator defaultBt = new Validator(wsaaDefaultBt, "Y");
	protected Validator nodefBt = new Validator(wsaaDefaultBt, "N");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
protected static final class ErrorsInner { 
		/* ERRORS */
	protected FixedLengthStringData e027 = new FixedLengthStringData(4).init("E027");
	protected FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	protected FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	protected FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	protected FixedLengthStringData e420 = new FixedLengthStringData(4).init("E420");
	protected FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	protected FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	protected FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	protected FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	protected FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	protected FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	protected FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	protected FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	public FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	protected FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	protected FixedLengthStringData f344 = new FixedLengthStringData(4).init("F344");
	protected FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	protected FixedLengthStringData g622 = new FixedLengthStringData(4).init("G622");
	protected FixedLengthStringData h437 = new FixedLengthStringData(4).init("H437");
	protected FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	protected FixedLengthStringData h039 = new FixedLengthStringData(4).init("H039");
	protected FixedLengthStringData h040 = new FixedLengthStringData(4).init("H040");
	protected FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	protected FixedLengthStringData l001 = new FixedLengthStringData(4).init("L001");
	protected FixedLengthStringData d028 = new FixedLengthStringData(4).init("D028");
	protected FixedLengthStringData d029 = new FixedLengthStringData(4).init("D029");
	protected FixedLengthStringData d352 = new FixedLengthStringData(4).init("D352");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
protected static final class TablesInner { 
		/* TABLES */
	protected FixedLengthStringData t3590 = new FixedLengthStringData(5).init("T3590");
	protected FixedLengthStringData t5606 = new FixedLengthStringData(5).init("T5606");
	protected FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	protected FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	protected FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	protected FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	protected FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	protected FixedLengthStringData t6625 = new FixedLengthStringData(5).init("T6625");
	protected FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	protected FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	protected FixedLengthStringData t5674 = new FixedLengthStringData(5).init("T5674");
	protected FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	protected FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	protected FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
protected static final class FormatsInner { 
		/* FORMATS */
	protected FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	protected FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	protected FixedLengthStringData covtrbnrec = new FixedLengthStringData(10).init("COVTRBNREC");
	protected FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	protected FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	protected FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	protected FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	protected FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	protected FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	protected FixedLengthStringData racdlnbrec = new FixedLengthStringData(10).init("RACDLNBREC");
	protected FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
}
}
