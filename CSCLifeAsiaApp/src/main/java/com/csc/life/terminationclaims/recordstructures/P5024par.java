package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:29
 * Description:
 * Copybook name: P5024PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class P5024par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(52);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData chdrnum1 = new FixedLengthStringData(8).isAPartOf(parmRecord, 8);
  	public ZonedDecimalData intDatecfrom = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 16);
  	public ZonedDecimalData intDatecto = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 24);
  	public FixedLengthStringData datecfrom = new FixedLengthStringData(10).isAPartOf(parmRecord, 32);
  	public FixedLengthStringData datecto = new FixedLengthStringData(10).isAPartOf(parmRecord, 42);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}