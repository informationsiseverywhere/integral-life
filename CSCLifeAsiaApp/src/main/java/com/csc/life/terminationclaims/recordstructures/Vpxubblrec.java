package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Vpxubblrec extends ExternalData {

  	public FixedLengthStringData vpxubblRec = new FixedLengthStringData(587);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(vpxubblRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(vpxubblRec, 5);
  	public FixedLengthStringData copybook = new FixedLengthStringData(12).isAPartOf(vpxubblRec, 9);

  	public FixedLengthStringData result = new FixedLengthStringData(115).isAPartOf(vpxubblRec, 21);
  	public FixedLengthStringData resultField = new FixedLengthStringData(35).isAPartOf(result, 0);
  	public FixedLengthStringData resultValue = new FixedLengthStringData(80).isAPartOf(result, 35);

  	public FixedLengthStringData dataRec = new FixedLengthStringData(451).isAPartOf(vpxubblRec, 136);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(dataRec, 0);
  	public ZonedDecimalData crrcd = new ZonedDecimalData(8, 0).isAPartOf(dataRec, 4);
  	public ZonedDecimalData convUnits = new ZonedDecimalData(8, 0).isAPartOf(dataRec, 12);
  	public FixedLengthStringData pstatcode = new FixedLengthStringData(2).isAPartOf(dataRec, 20);
  	public ZonedDecimalData singp = new ZonedDecimalData(17, 2).isAPartOf(dataRec, 22);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(dataRec, 39);
  	public ZonedDecimalData lcltdob = new ZonedDecimalData(8, 0).isAPartOf(dataRec, 43);
  	public ZonedDecimalData jcltdob = new ZonedDecimalData(8,0).isAPartOf(dataRec, 51);
  	public FixedLengthStringData lcltsex = new FixedLengthStringData(1).isAPartOf(dataRec, 59);
  	public FixedLengthStringData jcltsex = new FixedLengthStringData(1).isAPartOf(dataRec, 60);
  	public ZonedDecimalData lage = new ZonedDecimalData(3, 0).isAPartOf(dataRec, 61);
  	public ZonedDecimalData jlage = new ZonedDecimalData(3, 0).isAPartOf(dataRec, 64);
  	public ZonedDecimalData totalCd = new ZonedDecimalData(17, 2).isAPartOf(dataRec, 67);
  	public ZonedDecimalData covrSingp = new ZonedDecimalData(17, 2).isAPartOf(dataRec, 84);

    // For Component Premium Calc
  	public FixedLengthStringData premMeth = new FixedLengthStringData(10).isAPartOf(dataRec, 101);
  	public ZonedDecimalData duration = new ZonedDecimalData(2, 0).isAPartOf(dataRec, 111);
  	public FixedLengthStringData rstaflag = new FixedLengthStringData(2).isAPartOf(dataRec, 113);
  	public FixedLengthStringData reasind = new FixedLengthStringData(1).isAPartOf(dataRec, 115);
  	public ZonedDecimalData count = new ZonedDecimalData(3, 0).isAPartOf(dataRec, 116);
  	public ZonedDecimalData sumin = new ZonedDecimalData(17, 2).isAPartOf(dataRec, 119);
  	// length 8 * 5 = 40
  	//public ZonedDecimalData[] oppc = ZDArrayPartOfStructure(8, 5, 2, dataRec, 136);
  	
  	public PackedDecimalData oppc01 = new PackedDecimalData(5,2).isAPartOf(dataRec, 136);
  	public PackedDecimalData oppc02 = new PackedDecimalData(5,2).isAPartOf(dataRec, 139);
  	public PackedDecimalData oppc03 = new PackedDecimalData(5,2).isAPartOf(dataRec, 142);
  	public PackedDecimalData oppc04 = new PackedDecimalData(5,2).isAPartOf(dataRec, 145);
  	public PackedDecimalData oppc05 = new PackedDecimalData(5,2).isAPartOf(dataRec, 148);
  	public PackedDecimalData oppc06 = new PackedDecimalData(5,2).isAPartOf(dataRec, 151);
  	public PackedDecimalData oppc07 = new PackedDecimalData(5,2).isAPartOf(dataRec, 154);
  	public PackedDecimalData oppc08 = new PackedDecimalData(5,2).isAPartOf(dataRec, 157);

  	
  	// length 8 * 3 = 24
  	//public ZonedDecimalData[] zmortpct = ZDArrayPartOfStructure(8, 3, 0, dataRec, 176);
  	public PackedDecimalData zmortpct01 = new PackedDecimalData(3,0).isAPartOf(dataRec, 160);
	public PackedDecimalData zmortpct02 = new PackedDecimalData(3,0).isAPartOf(dataRec, 162);
	public PackedDecimalData zmortpct03 = new PackedDecimalData(3,0).isAPartOf(dataRec, 164);
	public PackedDecimalData zmortpct04 = new PackedDecimalData(3,0).isAPartOf(dataRec, 166);
	public PackedDecimalData zmortpct05 = new PackedDecimalData(3,0).isAPartOf(dataRec, 168);
	public PackedDecimalData zmortpct06 = new PackedDecimalData(3,0).isAPartOf(dataRec, 170);
	public PackedDecimalData zmortpct07 = new PackedDecimalData(3,0).isAPartOf(dataRec, 172);
	public PackedDecimalData zmortpct08 = new PackedDecimalData(3,0).isAPartOf(dataRec, 174);
  	
  	// length 8 * 2 = 16
  	//public FixedLengthStringData[] opcda = FLSArrayPartOfStructure(8, 2, dataRec, 200);
	public FixedLengthStringData opcda01 = new FixedLengthStringData(2).isAPartOf(dataRec, 176);
	public FixedLengthStringData opcda02 = new FixedLengthStringData(2).isAPartOf(dataRec, 178);
	public FixedLengthStringData opcda03 = new FixedLengthStringData(2).isAPartOf(dataRec, 180);
	public FixedLengthStringData opcda04 = new FixedLengthStringData(2).isAPartOf(dataRec, 182);
	public FixedLengthStringData opcda05 = new FixedLengthStringData(2).isAPartOf(dataRec, 184);
	public FixedLengthStringData opcda06 = new FixedLengthStringData(2).isAPartOf(dataRec, 186);
	public FixedLengthStringData opcda07 = new FixedLengthStringData(2).isAPartOf(dataRec, 188);
	public FixedLengthStringData opcda08 = new FixedLengthStringData(2).isAPartOf(dataRec, 190);
  	// length 8 * 3 = 24
  	//public ZonedDecimalData[] agerate = ZDArrayPartOfStructure(8, 3, 0, dataRec, 216);
	public PackedDecimalData agerate01 = new PackedDecimalData(3,0).isAPartOf(dataRec, 192);
	public PackedDecimalData agerate02 = new PackedDecimalData(3,0).isAPartOf(dataRec, 194);
	public PackedDecimalData agerate03 = new PackedDecimalData(3,0).isAPartOf(dataRec, 196);
	public PackedDecimalData agerate04 = new PackedDecimalData(3,0).isAPartOf(dataRec, 198);
	public PackedDecimalData agerate05 = new PackedDecimalData(3,0).isAPartOf(dataRec, 200);
	public PackedDecimalData agerate06 = new PackedDecimalData(3,0).isAPartOf(dataRec, 202);
	public PackedDecimalData agerate07 = new PackedDecimalData(3,0).isAPartOf(dataRec, 204);
	public PackedDecimalData agerate08 = new PackedDecimalData(3,0).isAPartOf(dataRec, 206);
  	// length 8 * 6 = 48
  	//public ZonedDecimalData[] insprm = ZDArrayPartOfStructure(8, 6, 0, dataRec, 240);
	public PackedDecimalData insprm01 = new PackedDecimalData(6,0).isAPartOf(dataRec, 208);
	public PackedDecimalData insprm02 = new PackedDecimalData(6,0).isAPartOf(dataRec, 212);
	public PackedDecimalData insprm03 = new PackedDecimalData(6,0).isAPartOf(dataRec, 216);
	public PackedDecimalData insprm04 = new PackedDecimalData(6,0).isAPartOf(dataRec, 220);
	public PackedDecimalData insprm05 = new PackedDecimalData(6,0).isAPartOf(dataRec, 224);
	public PackedDecimalData insprm06 = new PackedDecimalData(6,0).isAPartOf(dataRec, 228);
	public PackedDecimalData insprm07 = new PackedDecimalData(6,0).isAPartOf(dataRec, 232);
	public PackedDecimalData insprm08 = new PackedDecimalData(6,0).isAPartOf(dataRec, 236);

  	// For Surrender Calc
  	public FixedLengthStringData chargeCalc = new FixedLengthStringData(4).isAPartOf(dataRec, 240);
  	public ZonedDecimalData modPrem = new ZonedDecimalData(17, 2).isAPartOf(dataRec, 244);
  	public ZonedDecimalData paidPrem = new ZonedDecimalData(17, 2).isAPartOf(dataRec, 261);
  	public ZonedDecimalData recordCount = new ZonedDecimalData(3, 0).isAPartOf(dataRec, 278).setUnsigned();

  	// length 10 * 17 = 170
  	//public ZonedDecimalData[] curduntbal = ZDArrayPartOfStructure(10, 17, 5, dataRec, 329);
  	public PackedDecimalData curduntbal01 = new PackedDecimalData(16,5).isAPartOf(dataRec, 281);
  	public PackedDecimalData curduntbal02 = new PackedDecimalData(16,5).isAPartOf(dataRec, 290);
  	public PackedDecimalData curduntbal03 = new PackedDecimalData(16,5).isAPartOf(dataRec, 299);
  	public PackedDecimalData curduntbal04 = new PackedDecimalData(16,5).isAPartOf(dataRec, 308);
  	public PackedDecimalData curduntbal05 = new PackedDecimalData(16,5).isAPartOf(dataRec, 317);
  	public PackedDecimalData curduntbal06 = new PackedDecimalData(16,5).isAPartOf(dataRec, 326);
  	public PackedDecimalData curduntbal07 = new PackedDecimalData(16,5).isAPartOf(dataRec, 335);
  	public PackedDecimalData curduntbal08 = new PackedDecimalData(16,5).isAPartOf(dataRec, 344);
  	public PackedDecimalData curduntbal09 = new PackedDecimalData(16,5).isAPartOf(dataRec, 353);
  	public PackedDecimalData curduntbal10 = new PackedDecimalData(16,5).isAPartOf(dataRec, 362);
  	//length 10 * 3 = 30
  	//public FixedLengthStringData[] currcode = FLSArrayPartOfStructure(10, 3, dataRec, 499);
  	public FixedLengthStringData currcode01 =  new FixedLengthStringData(3).isAPartOf(dataRec, 371);
  	public FixedLengthStringData currcode02 =  new FixedLengthStringData(3).isAPartOf(dataRec, 374);
  	public FixedLengthStringData currcode03 =  new FixedLengthStringData(3).isAPartOf(dataRec, 377);
  	public FixedLengthStringData currcode04 =  new FixedLengthStringData(3).isAPartOf(dataRec, 380);
  	public FixedLengthStringData currcode05 =  new FixedLengthStringData(3).isAPartOf(dataRec, 383);
  	public FixedLengthStringData currcode06 =  new FixedLengthStringData(3).isAPartOf(dataRec, 386);
  	public FixedLengthStringData currcode07 =  new FixedLengthStringData(3).isAPartOf(dataRec, 389);
  	public FixedLengthStringData currcode08 =  new FixedLengthStringData(3).isAPartOf(dataRec, 392);
  	public FixedLengthStringData currcode09 =  new FixedLengthStringData(3).isAPartOf(dataRec, 395);
  	public FixedLengthStringData currcode10 =  new FixedLengthStringData(3).isAPartOf(dataRec, 398);

  	//length 10 * 4 = 40
  	//public FixedLengthStringData[] fund = FLSArrayPartOfStructure(10, 4, dataRec, 529);
  	public FixedLengthStringData fund01 =  new FixedLengthStringData(4).isAPartOf(dataRec, 401);
  	public FixedLengthStringData fund02 =  new FixedLengthStringData(4).isAPartOf(dataRec, 405);
  	public FixedLengthStringData fund03 =  new FixedLengthStringData(4).isAPartOf(dataRec, 409);
  	public FixedLengthStringData fund04 =  new FixedLengthStringData(4).isAPartOf(dataRec, 413);
  	public FixedLengthStringData fund05 =  new FixedLengthStringData(4).isAPartOf(dataRec, 417);
  	public FixedLengthStringData fund06 =  new FixedLengthStringData(4).isAPartOf(dataRec, 421);
  	public FixedLengthStringData fund07 =  new FixedLengthStringData(4).isAPartOf(dataRec, 425);
  	public FixedLengthStringData fund08 =  new FixedLengthStringData(4).isAPartOf(dataRec, 429);
  	public FixedLengthStringData fund09 =  new FixedLengthStringData(4).isAPartOf(dataRec, 433);
  	public FixedLengthStringData fund10 =  new FixedLengthStringData(4).isAPartOf(dataRec, 437);
  	
  	// length 10 * 1 = 10
//  	public FixedLengthStringData[] type = FLSArrayPartOfStructure(10, 1, dataRec, 569);
  	public FixedLengthStringData type01 =  new FixedLengthStringData(1).isAPartOf(dataRec, 441);
  	public FixedLengthStringData type02 =  new FixedLengthStringData(1).isAPartOf(dataRec, 442);
  	public FixedLengthStringData type03 =  new FixedLengthStringData(1).isAPartOf(dataRec, 443);
  	public FixedLengthStringData type04 =  new FixedLengthStringData(1).isAPartOf(dataRec, 444);
  	public FixedLengthStringData type05 =  new FixedLengthStringData(1).isAPartOf(dataRec, 445);
  	public FixedLengthStringData type06 =  new FixedLengthStringData(1).isAPartOf(dataRec, 446);
  	public FixedLengthStringData type07 =  new FixedLengthStringData(1).isAPartOf(dataRec, 447);
  	public FixedLengthStringData type08 =  new FixedLengthStringData(1).isAPartOf(dataRec, 448);
  	public FixedLengthStringData type09 =  new FixedLengthStringData(1).isAPartOf(dataRec, 449);
  	public FixedLengthStringData type10 =  new FixedLengthStringData(1).isAPartOf(dataRec, 450);

	@Override
	public void initialize() {
		COBOLFunctions.initialize(vpxubblRec);
	}

	@Override
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			vpxubblRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

}
