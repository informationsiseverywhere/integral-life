package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.dao.AgcmpfDAO;
import com.csc.life.agents.dataaccess.model.Agcmpf;
import com.csc.life.contractservicing.dataaccess.dao.CovrpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Covrpf;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.recordstructures.PmexVpxRec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.reassurance.dataaccess.dao.RecopfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.life.terminationclaims.recordstructures.Cmcl001Rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Cmcl001 extends COBOLConvCodeModel {
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "CMCL001";

	private Syserrrec syserrrec = new Syserrrec();
	private Cmcl001Rec cmcl001Rec = new Cmcl001Rec();
	private PmexVpxRec vpxRec = new PmexVpxRec();
	private Map<String, List<String>> inPutParam = new HashMap<>();
	private Map<String, List<List<String>>> inPutParam2 = new HashMap<>();
	private Map<String, List<List<List<String>>>> inPutParam3 = new HashMap<>();
	private List<Covrpf> covrList = new ArrayList<Covrpf>();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAOP6351", CovrpfDAO.class);
	private ItempfDAO itemDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private T5687rec t5687rec = new T5687rec();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private Payrpf payrpf;

	private Map<String, String> outputParam = new HashMap<String, String>();
	private List<String> coverageList = new ArrayList<String>();
	private int countCoverage = 0;
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaRunIndicator = new FixedLengthStringData(1);
	private Validator lastTimeThrough = new Validator(wsaaRunIndicator, "E");
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Aglfpf aglfpf;
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO", AglfpfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO2", AgcmpfDAO.class);
	PayrpfDAO payrpfDao = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Clexpf clexpf;
	private ClexpfDAO clexpfDAO = getApplicationContext().getBean("clexpfDAO", ClexpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private List<Lifepf> lifepfList;
	private RcvdpfDAO rcvdpfDAO = getApplicationContext().getBean("rcvdpfDAO", RcvdpfDAO.class);
	private List<String> clntnumList;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	private RecopfDAO recopfDAO = getApplicationContext().getBean("recopfDAO", RecopfDAO.class);
	
	public Cmcl001() {
		super();
	}

	@Override
	public void mainline(Object... parmArray) {
		cmcl001Rec = (Cmcl001Rec) convertAndSetParam(cmcl001Rec, parmArray, 0);
		try {
			mainline1000();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void mainline1000() {
		if (lastTimeThrough.isTrue()) {
			cmcl001Rec.statuz.set(Varcom.endp);
			return;
		}
		if (isNE(wsaaLife, cmcl001Rec.getLife())) {
			wsaaLife.set(cmcl001Rec.getLife());
			initMap();
			setMap1();
			setMap2();
			readLextpf();
			initialise2000();
		} else {
			if (isEQ(cmcl001Rec.statuz, Varcom.oK)) {
				wsaaRunIndicator.set("E");
				countCoverage = 0;
			}
		}
		setOutgoingvariables();
		if (covrList.size() == countCoverage) {
			cmcl001Rec.statuz.set(Varcom.oK);
		}
		if (countCoverage == coverageList.size()) {
			coverageList.clear();
			outputParam.clear();
			covrList.clear();
		}
	}

	protected void readLextpf() {
		List<Lextpf> lextList;
		Lextpf lextpf = new Lextpf();
		lextpf.setChdrcoy(cmcl001Rec.getChdrcoy());
		lextpf.setChdrnum(cmcl001Rec.getChdrnum());
		lextList = lextpfDAO.getLextpfData(lextpf);
		initCommonLext();
		if (lextList != null && !lextList.isEmpty()) {
			setLextpfData(lextList);
		} else {
			for (int i = 0; i < inPutParam.get("covrId").size(); i++) {
				initLextParamList();
				initLextList();
				for (int j = 0; j < inPutParam.get("covrId").size(); j++) {
					setLextParamLists();
				}
				inPutParam.get("noOfCoverages").add(String.valueOf(inPutParam.get("covrId").size()));
				inPutParam2.get("countParamList")
						.add(new ArrayList<>(Collections.nCopies(inPutParam.get("covrId").size(), String.valueOf(0))));
				setCommonLext();
				setLextParentLists();
			}
			setLextField();
		}
	}

	protected void initCommonLext() {
		inPutParam.put("noOfCoverages", new ArrayList<>());
		inPutParam2.put("countParamList", new ArrayList<List<String>>());
		inPutParam2.put("aidsCoverIndParamList", new ArrayList<List<String>>());
		inPutParam2.put("ncdCodeParamList", new ArrayList<List<String>>());
		inPutParam2.put("claimAutoIncreaseParamList", new ArrayList<List<String>>());
		inPutParam2.put("syndicateCodeParamList", new ArrayList<List<String>>());
		inPutParam2.put("riskExpiryAgeParamList", new ArrayList<List<String>>());

		inPutParam3.put("oppcParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("zmortpctParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("opcdaParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("agerateParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("inspremParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("insprmRsnParentList", new ArrayList<List<List<String>>>());
		inPutParam3.put("oppcRsnParentList", new ArrayList<List<List<String>>>());
	}

	protected void setLextpfData(List<Lextpf> lextList) {
		int countCvg;
		int countLext;
		for (int i = 0; i < inPutParam.get("lifeId").size(); i++) {
			countCvg = 0;
			setCommonLext();
			inPutParam.put("count", new ArrayList<>());
			initLextParamList();
			for (int j = 0; j < inPutParam.get("covrId").size(); j++) {
				countLext = 0;
				initLextList();
				for (int k = 0; k < lextList.size(); k++) {
					if (inPutParam.get("lifeId").get(i).equals(lextList.get(k).getLife())
							&& inPutParam.get("covrId").get(j).equals(lextList.get(k).getCoverage())) {
						inPutParam.get("oppc").set(countLext, String.valueOf(lextList.get(k).getOppc()));
						inPutParam.get("zmortpct").set(countLext,
								String.valueOf(new BigDecimal(lextList.get(k).getZmortpct())));
						inPutParam.get("opcda").set(countLext, lextList.get(k).getOpcda());
						inPutParam.get("agerate").set(countLext,
								String.valueOf(new BigDecimal(lextList.get(k).getAgerate())));
						inPutParam.get("insprem").set(countLext,
								String.valueOf(new BigDecimal(lextList.get(k).getInsprm())));
						inPutParam.get("insprmRsn").set(countLext, SPACE);
						inPutParam.get("oppcRsn").set(countLext, SPACE);
						countLext++;
					}
				}
				inPutParam.get("count").add(String.valueOf(countLext));
				setLextParamLists();
				countCvg++;
			}
			inPutParam.get("noOfCoverages").add(String.valueOf(countCvg));
			inPutParam2.get("countParamList").add(inPutParam.get("count"));
			setLextParentLists();
		}
		setLextField();
	}

	protected void setCommonLext() {
		inPutParam2.get("aidsCoverIndParamList").add(new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam2.get("ncdCodeParamList").add(new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam2.get("claimAutoIncreaseParamList").add(new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam2.get("syndicateCodeParamList").add(new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam2.get("riskExpiryAgeParamList").add(new ArrayList<>(Collections.nCopies(8, SPACE)));
	}

	protected void initLextParamList() {
		inPutParam2.put("oppcParamList", new ArrayList<List<String>>());
		inPutParam2.put("zmortpctParamList", new ArrayList<List<String>>());
		inPutParam2.put("opcdaParamList", new ArrayList<List<String>>());
		inPutParam2.put("agerateParamList", new ArrayList<List<String>>());
		inPutParam2.put("inspremParamList", new ArrayList<List<String>>());
		inPutParam2.put("insprmRsnParamList", new ArrayList<List<String>>());
		inPutParam2.put("oppcRsnParamList", new ArrayList<List<String>>());
	}

	protected void initLextList() {
		inPutParam.put("aidsCoverInd", new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("ncdCode", new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("claimAutoIncrease", new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("syndicateCode", new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("riskExpiryAge", new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("oppc", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("zmortpct", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("opcda", new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("agerate", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("insprem", new ArrayList<>(Collections.nCopies(8, String.valueOf(BigDecimal.ZERO))));
		inPutParam.put("insprmRsn", new ArrayList<>(Collections.nCopies(8, SPACE)));
		inPutParam.put("oppcRsn", new ArrayList<>(Collections.nCopies(8, SPACE)));
	}

	protected void setLextParentLists() {
		inPutParam3.get("oppcParentList").add(inPutParam2.get("oppcParamList"));
		inPutParam3.get("zmortpctParentList").add(inPutParam2.get("zmortpctParamList"));
		inPutParam3.get("opcdaParentList").add(inPutParam2.get("opcdaParamList"));
		inPutParam3.get("agerateParentList").add(inPutParam2.get("agerateParamList"));
		inPutParam3.get("inspremParentList").add(inPutParam2.get("inspremParamList"));
		inPutParam3.get("insprmRsnParentList").add(inPutParam2.get("insprmRsnParamList"));
		inPutParam3.get("oppcRsnParentList").add(inPutParam2.get("oppcRsnParamList"));
	}

	protected void setLextParamLists() {
		inPutParam2.get("oppcParamList").add(inPutParam.get("oppc"));
		inPutParam2.get("zmortpctParamList").add(inPutParam.get("zmortpct"));
		inPutParam2.get("opcdaParamList").add(inPutParam.get("opcda"));
		inPutParam2.get("agerateParamList").add(inPutParam.get("agerate"));
		inPutParam2.get("inspremParamList").add(inPutParam.get("insprem"));
		inPutParam2.get("insprmRsnParamList").add(inPutParam.get("insprmRsn"));
		inPutParam2.get("oppcRsnParamList").add(inPutParam.get("oppcRsn"));
	}

	protected void setLextField() {
		vpxRec.setNoOfCoverages(inPutParam.get("noOfCoverages"));
		vpxRec.setAidsCoverInd(inPutParam2.get("aidsCoverIndParamList"));
		vpxRec.setNcdCode(inPutParam2.get("ncdCodeParamList"));
		vpxRec.setClaimAutoIncrease(inPutParam2.get("claimAutoIncreaseParamList"));
		vpxRec.setSyndicateCode(inPutParam2.get("syndicateCodeParamList"));
		vpxRec.setTotalSumInsured(inPutParam2.get("sumInsParamList"));
		vpxRec.setRiskExpiryAge(inPutParam2.get("riskExpiryAgeParamList"));
		vpxRec.setOppc(inPutParam3.get("oppcParentList"));
		vpxRec.setZmortpct(inPutParam3.get("zmortpctParentList"));
		vpxRec.setOpcda(inPutParam3.get("opcdaParentList"));
		vpxRec.setAgerate(inPutParam3.get("agerateParentList"));
		vpxRec.setInsprm(inPutParam3.get("inspremParentList"));
		vpxRec.setCount(inPutParam2.get("countParamList"));
		vpxRec.setInsprmRsn(inPutParam3.get("insprmRsnParentList"));
		vpxRec.setOppcRsn(inPutParam3.get("oppcRsnParentList"));
	}

	protected void initialise2000() {
		cmcl001Rec.statuz.set(Varcom.oK);
		cmcl001Rec.setAgentClass(aglfpf == null ? "" : aglfpf.getAgentClass());
		cmcl001Rec.setCurrcode(chdrpf.getCntcurr());
		cmcl001Rec.setBillfreq(payrpf.getBillfreq());
		cmcl001Rec.setRatingdate(chdrpf.getOccdate().toString());
		cmcl001Rec.setRstaflag(clexpf == null ? "" : clexpf.getRstaflag());
		cmcl001Rec.setTpdtype(inPutParam2.get("tpdtypeParamList"));
		cmcl001Rec.setCrtable(inPutParam2.get("crtableParamList"));
		cmcl001Rec.setSumin(inPutParam2.get("sumInsParamList"));
		cmcl001Rec.setCovrCoverage(inPutParam2.get("covrIdParamList"));
		cmcl001Rec.setCovrRiderId(inPutParam2.get("riderIdParamList"));
		cmcl001Rec.setDob(inPutParam2.get("dobParamList"));
		cmcl001Rec.setDuration(inPutParam2.get("durationParamList"));
		cmcl001Rec.setRiskComDate(inPutParam2.get("riskComDateParamList"));
		cmcl001Rec.setPrmbasis(inPutParam2.get("prmBasisParamList"));
		cmcl001Rec.setLinkcov(inPutParam2.get("linkcovParamList"));
		cmcl001Rec.setEffdate(inPutParam.get("effdate"));
		cmcl001Rec.setSrcebus(chdrpf.getSrcebus());
		cmcl001Rec.setTermdate(inPutParam2.get("termdateParamList"));
		cmcl001Rec.setIcommtot(inPutParam2.get("icommtotParamList"));
		cmcl001Rec.setCrrcd(chdrpf.getOccdate().toString());
		cmcl001Rec.setMortcls(inPutParam2.get("mortClsParamList"));
		cmcl001Rec.setCovrcd(inPutParam2.get("covrcdParamList"));
		cmcl001Rec.setLsex(inPutParam.get("lSex"));
		cmcl001Rec.setRstate01(inPutParam.get("rstate01"));

		cmcl001Rec.initialise();
		if (AppVars.getInstance().getAppConfig().isVpmsEnable() && ExternalisedRules.isCallExternal("SURCOMMCLAW")) {
			callProgram("SURCOMMCLAW", cmcl001Rec, vpxRec);
		}

		if (isEQ(cmcl001Rec.statuz, Varcom.oK)) {
			int i = 0;
			for (String s : coverageList) {
				if (!(outputParam.containsKey(s))) {
					outputParam.put(s, cmcl001Rec.getPayamnt().get(0).get(i));
				}
				i++;
			}
		} else {
			syserrrec.subrname.set(wsaaSubr);
			cmcl001Rec.statuz.set(cmcl001Rec.statuz);
		}
	}

	private void initMap() {
		inPutParam.put("cnttype", new ArrayList<String>());
		inPutParam.put("crtable", new ArrayList<String>());
		inPutParam.put("covrcd", new ArrayList<String>());
		inPutParam.put("icommtot", new ArrayList<String>());
		inPutParam.put("previcommtot", new ArrayList<String>());
		inPutParam.put("covrCoverage", new ArrayList<String>());
		inPutParam.put("tpdtype", new ArrayList<String>());
		inPutParam.put("mortCls", new ArrayList<String>());
		inPutParam.put("sumIns", new ArrayList<String>());
		inPutParam.put("covrId", new ArrayList<String>());
		inPutParam.put("riderId", new ArrayList<String>());
		inPutParam.put("lSex", new ArrayList<String>());
		inPutParam.put("lifeId", new ArrayList<String>());
		inPutParam.put("dob", new ArrayList<String>());
		inPutParam.put("duration", new ArrayList<String>());
		inPutParam.put("prmBasis", new ArrayList<String>());
		inPutParam.put("linkcov", new ArrayList<String>());
		inPutParam.put("riskComDate", new ArrayList<String>());
		inPutParam.put("effdate", new ArrayList<String>());
		inPutParam.put("termdate", new ArrayList<String>());
		inPutParam.put("rstate01", new ArrayList<String>());

		inPutParam2.put("covrIdParamList", new ArrayList<List<String>>());
		inPutParam2.put("tpdtypeParamList", new ArrayList<List<String>>());
		inPutParam2.put("crtableParamList", new ArrayList<List<String>>());
		inPutParam2.put("covrcdParamList", new ArrayList<List<String>>());
		inPutParam2.put("icommtotParamList", new ArrayList<List<String>>());
		inPutParam2.put("previcommtotParamList", new ArrayList<List<String>>());
		inPutParam2.put("sumInsParamList", new ArrayList<List<String>>());
		inPutParam2.put("riderIdParamList", new ArrayList<List<String>>());
		inPutParam2.put("mortClsParamList", new ArrayList<List<String>>());
		inPutParam2.put("dobParamList", new ArrayList<List<String>>());
		inPutParam2.put("durationParamList", new ArrayList<List<String>>());
		inPutParam2.put("prmBasisParamList", new ArrayList<List<String>>());
		inPutParam2.put("linkcovParamList", new ArrayList<List<String>>());
		inPutParam2.put("riskComDateParamList", new ArrayList<List<String>>());
		inPutParam2.put("termdateParamList", new ArrayList<List<String>>());
	}

	private void setMap1() {
		readChdrpf();
		readLifepf();
		readCovrpf();
		readAglfpf();
		readAgcmpf();
		readPayr();
		readClex();
	}

	private void readAgcmpf() {
		List<Agcmpf> agcmList = agcmpfDAO.searchAgcmdmnRecord(cmcl001Rec.getChdrcoy(), cmcl001Rec.getChdrnum());
		for (Agcmpf agcm : agcmList) {
			if (agcm.getLife().equals(wsaaLife.toString())) {
				inPutParam.get("icommtot").add(agcm.getInitcom().toString());
			}
		}
	}

	private void readPayr() {
		payrpf = payrpfDao.getpayrRecord(cmcl001Rec.getChdrcoy(), cmcl001Rec.getChdrnum());
	}

	private void readChdrpf() {
		chdrpf = chdrpfDAO.getchdrRecord(cmcl001Rec.getChdrcoy(), cmcl001Rec.getChdrnum());
	}

	private void readAglfpf() {
		aglfpf = aglfpfDAO.searchAglflnb(cmcl001Rec.getChdrcoy(), chdrpf.getAgntnum());
	}

	private void readClex() {
		clexpf = clexpfDAO.getClexpfByClntkey("CN", "9", chdrpf.getCownnum());
	}

	private void setMap2() {
		inPutParam2.get("crtableParamList").add(inPutParam.get("crtable"));
		inPutParam2.get("covrcdParamList").add(inPutParam.get("covrcd"));
		inPutParam2.get("icommtotParamList").add(inPutParam.get("icommtot"));
		inPutParam2.get("tpdtypeParamList").add(inPutParam.get("tpdtype"));
		inPutParam2.get("sumInsParamList").add(inPutParam.get("sumIns"));
		inPutParam2.get("covrIdParamList").add(inPutParam.get("covrId"));
		inPutParam2.get("riderIdParamList").add(inPutParam.get("riderId"));
		inPutParam2.get("mortClsParamList").add(inPutParam.get("mortCls"));
		inPutParam2.get("dobParamList").add(inPutParam.get("dob"));
		inPutParam2.get("durationParamList").add(inPutParam.get("duration"));
		inPutParam2.get("prmBasisParamList").add(inPutParam.get("prmBasis"));
		inPutParam2.get("linkcovParamList").add(inPutParam.get("linkcov"));
		inPutParam2.get("riskComDateParamList").add(inPutParam.get("riskComDate"));
		inPutParam2.get("termdateParamList").add(inPutParam.get("termdate"));
	}

	private void readCovrpf() {
		List<Covrpf> covrList1 = covrpfDAO.searchCovrRecordByCoyNumDescUniquNo(cmcl001Rec.getChdrcoy(), cmcl001Rec.getChdrnum());
		if (!covrList1.isEmpty()) {
			for (Covrpf covr : covrList1) {
				if (isLTE(covr.getCrrcd(), cmcl001Rec.getTranEffdate())) {
					covrList.add(covr);
					setLifepf();
					readClntpf();
				}
			}
			for (Covrpf covr : covrList) {
				if (covr.getLife().equals(wsaaLife.toString())) {
					getPremMethod(covr.getCrtable(), covr.getJlife());
					coverageList.add(covr.getCrtable() + covr.getCoverage());
					inPutParam.get("covrCoverage").add(covr.getCoverage());
					inPutParam.get("crtable").add(covr.getCrtable());
					inPutParam.get("covrcd").add(String.valueOf(covr.getCrrcd()));

					if (covr.getTpdtype() != null && !covr.getTpdtype().trim().isEmpty()) {
						inPutParam.get("tpdtype").add(((Character) covr.getTpdtype().charAt(4)).toString());
					} else {
						inPutParam.get("tpdtype").add(SPACE);
					}
					inPutParam.get("sumIns").add("0");
					inPutParam.get("covrId").add(covr.getCoverage());
					inPutParam.get("riderId").add(covr.getRider());
					if (covr.getMortcls() != null && !covr.getMortcls().trim().isEmpty()) {
						inPutParam.get("mortCls").add(covr.getMortcls());
					} else {
						inPutParam.get("mortCls").add(SPACE);
					}
					inPutParam.get("riskComDate").add(String.valueOf(covr.getCurrfrom()));
					inPutParam.get("effdate").add(String.valueOf(covr.getCurrfrom()));
					inPutParam.get("termdate").add(String.valueOf(covr.getPcesDte()));
					calcDuration(covr);
					readRcvdpfData(covr);
					if (covr.getLnkgno() != null && !covr.getLnkgno().trim().isEmpty()) {
						LinkageInfoService linkgService = new LinkageInfoService();
						inPutParam.get("linkcov").add(linkgService.getLinkageInfo(covr.getLnkgno()));
					} else {
						inPutParam.get("linkcov").add(SPACE);
					}
					getRcstFreq(covr);
				}
			}
		}
	}

	private void getPremMethod(String crtable, String jlife) {
		Itempf itempf = itemDAO.findItemByItem("IT", cmcl001Rec.getChdrcoy(), "T5687", crtable);
		if (itempf != null) {
			t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			if (jlife != null && jlife.equals("01")) {
				cmcl001Rec.setPremMethod(t5687rec.jlPremMeth.toString().trim());
			} else {
				cmcl001Rec.setPremMethod(t5687rec.premmeth.toString().trim());
			}
		}
	}

	protected void readLifepf() {
		lifepfList = lifepfDAO.getLifeRecords(cmcl001Rec.getChdrcoy(), cmcl001Rec.getChdrnum(), "1");
	}

	protected void setLifepf() {
		if (!lifepfList.isEmpty()) {
			setLifeDetails(lifepfList);
		}
	}

	protected void setLifeDetails(List<Lifepf> lifepfList) {
		String lifeNo = null;
		int jLifeInd = 0;
		clntnumList = new ArrayList<>();
		for (Lifepf lif : lifepfList) {
			if (lifeNo != null) {
				if (lif.getLife().equals(lifeNo) && lif.getJlife().equals("00")) {
					setLifeData(lif);
				} else if (lif.getLife().equals(lifeNo) && lif.getJlife().equals("01")) {
					inPutParam.get("dob").add(String.valueOf(new BigDecimal(lif.getCltdob())));
					inPutParam.get("lSex").add(lif.getCltsex());
					jLifeInd = 1;
				} else if (!lif.getLife().equals(lifeNo) && jLifeInd != 1) {
					inPutParam.get("dob").add(String.valueOf(new BigDecimal(lif.getCltdob())));
					inPutParam.get("lSex").add(lif.getCltsex());
					inPutParam.get("lifeId").add(lif.getLife());
				} else if (!lif.getLife().equals(lifeNo) && jLifeInd == 1) {
					jLifeInd = 0;
					setLifeData(lif);
				}
				lifeNo = lif.getLife();
			} else {
				inPutParam.get("lSex").add(lif.getCltsex());
				inPutParam.get("dob").add(String.valueOf(new BigDecimal(lif.getCltdob())));
				inPutParam.get("lifeId").add(lif.getLife());
				lifeNo = lif.getLife();
				jLifeInd = 0;
			}
			clntnumList.add(lif.getLifcnum());
		}

	}

	protected void setLifeData(Lifepf lif) {
		inPutParam.get("lifeId").add(lif.getLife());
		inPutParam.get("dob").add(String.valueOf(new BigDecimal(lif.getCltdob())));
		inPutParam.get("lSex").add(lif.getCltsex());
	}

	protected void calcDuration(Covrpf c) {
		datcon3rec.intDate1.set(cmcl001Rec.getTranEffdate());
		datcon3rec.intDate2.set(c.getPcesDte());
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		datcon3rec.freqFactor.add(0.99999);
		inPutParam.get("duration").add(String.valueOf(datcon3rec.freqFactor.toInt()));
	}

	protected void setOutgoingvariables() {
		String matchData = cmcl001Rec.getCovcrtable() + cmcl001Rec.getCovcovrCoverage();
		if (outputParam.containsKey(matchData)) {
			cmcl001Rec.setCommclaw(outputParam.get(matchData));
			countCoverage++;
		}
	}

	protected void readRcvdpfData(Covrpf c) {
		Rcvdpf rcvdpf = new Rcvdpf();
		rcvdpf.setChdrcoy(c.getChdrcoy());
		rcvdpf.setChdrnum(c.getChdrnum());
		rcvdpf.setLife(c.getLife());
		rcvdpf.setCoverage(c.getCoverage());
		rcvdpf.setRider(c.getRider());
		rcvdpf.setCrtable(c.getCrtable());
		rcvdpf = rcvdpfDAO.readRcvdpf(rcvdpf);
		if (rcvdpf != null && rcvdpf.getPrmbasis() != null) {
			if (rcvdpf.getPrmbasis().equals("S")) {
				inPutParam.get("prmBasis").add("Y");
			} else {
				inPutParam.get("prmBasis").add("");
			}
		}
	}
	
	protected void getRcstFreq(Covrpf covr) {
		String rcstfreq = recopfDAO.getCostFreq(covr.getChdrcoy(), covr.getChdrnum(), covr.getLife(), covr.getCoverage(), covr.getRider(), covr.getPlanSuffix());
		if(rcstfreq!=null)
			cmcl001Rec.setCostfreq(rcstfreq);
	}

	protected void readClntpf() {
		Map<String, Clntpf> clntpfMap = clntpfDAO.searchClntRecord("CN", "9", clntnumList);
		if (clntpfMap != null && !clntpfMap.isEmpty()) {
			for (Map.Entry<String, Clntpf> clnt : clntpfMap.entrySet()) {
				inPutParam.get("rstate01").add(clnt.getValue().getClntStateCd() != null
								&& !clnt.getValue().getClntStateCd().trim().isEmpty()
								&& !clnt.getValue().getClntStateCd().trim().substring(3).isEmpty()
										? clnt.getValue().getClntStateCd().trim().substring(3) : "");
			}
		}
	}

	protected void fatalError9000() {
		error9010();
		exit9020();
	}

	protected void error9010() {
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

	protected void exit9020() {
		cmcl001Rec.statuz.set("BOMB");
		/* EXIT */
		exitProgram();
	}
}