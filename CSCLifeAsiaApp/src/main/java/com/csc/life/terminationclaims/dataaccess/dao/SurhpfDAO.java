package com.csc.life.terminationclaims.dataaccess.dao;

import java.util.List;

import com.csc.life.terminationclaims.dataaccess.model.Surhpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface SurhpfDAO extends BaseDAO<Surhpf> {
	void insertSurhpfList(List<Surhpf> pfList);
	List<Surhpf> getSurhpfRecord(String chdrcoy, String chdrnum);
}
