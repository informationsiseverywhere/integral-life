package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sh5c3screen extends ScreenRecord{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 16, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh5c3ScreenVars sv = (Sh5c3ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh5c3screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh5c3ScreenVars screenVars = (Sh5c3ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrsel.setClassString("");
		screenVars.efdateDisp.setClassString("");
		screenVars.claimnmber.setClassString("");
		screenVars.action.setClassString("");
	}

/**
 * Clear all the variables in Sh5c3screen
 */
	public static void clear(VarModel pv) {
		Sh5c3ScreenVars screenVars = (Sh5c3ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrsel.clear();
		screenVars.efdateDisp.clear();
		screenVars.efdate.clear();
		screenVars.claimnmber.clear();
		screenVars.action.clear();
	}

}
