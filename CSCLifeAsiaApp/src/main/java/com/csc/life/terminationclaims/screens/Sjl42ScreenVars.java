package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SJL42
 * @version 1.0 generated on 10/03/20 12:12
 * @author Sparikh8
 */
public class Sjl42ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData causeofdth = DD.causeofdth.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData claim = DD.claimnumber.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData claimTyp = DD.claimTyp.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData claimStat = DD.clamstat.copy().isAPartOf(dataFields,15);
	public ZonedDecimalData dtofdeath = DD.dtofdeath.copyToZonedDecimal().isAPartOf(dataFields,17);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,25);
	public FixedLengthStringData asterisk = DD.asterisk.copy().isAPartOf(dataFields,33);
	public FixedLengthStringData astrsk = DD.astrsk.copy().isAPartOf(dataFields,34);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,35);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,62);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,92);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,100);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,147);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,155);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,202);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,210);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,257);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,267);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,275);
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,285);
	public ZonedDecimalData riskcommdte = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,286);


	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData causeofdthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData claimErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData claimTypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData claimStatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData dtofdeathErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData asteriskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData astrskErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData riskcommdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] causeofdthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] claimOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] claimTypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] claimStatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] dtofdeathOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] asteriskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] astrskOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] riskcommdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(getSubfileAreaSize());
	public FixedLengthStringData subfileFields = new FixedLengthStringData(getSubfileFieldsSize()).isAPartOf(subfileArea, 0);
	public ZonedDecimalData actvalue = DD.actvalue.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData cnstcur = DD.cnstcur.copy().isAPartOf(subfileFields,17);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData estMatValue = DD.emv.copyToZonedDecimal().isAPartOf(subfileFields,22);
	public ZonedDecimalData hactval = DD.hactval.copyToZonedDecimal().isAPartOf(subfileFields,39);
	public FixedLengthStringData hcnstcur = DD.hcnstcur.copy().isAPartOf(subfileFields,56);
	public FixedLengthStringData hcover = DD.hcover.copy().isAPartOf(subfileFields,59);
	public FixedLengthStringData hcrtable = DD.hcrtable.copy().isAPartOf(subfileFields,61);
	public ZonedDecimalData hemv = DD.hemv.copyToZonedDecimal().isAPartOf(subfileFields,65);
	public FixedLengthStringData htype = DD.htype.copy().isAPartOf(subfileFields,82);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(subfileFields,83);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(subfileFields,85);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(subfileFields,87);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(subfileFields,97);
	public FixedLengthStringData vfund = DD.vfund.copy().isAPartOf(subfileFields,98);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(getErrorSubfileSize()).isAPartOf(subfileArea, getSubfileFieldsSize());
	public FixedLengthStringData actvalueErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData cnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData emvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hactvalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hcnstcurErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData hcoverErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData hcrtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData hemvErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData htypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData shortdsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData typeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData vfundErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(getOutputSubfileSize()).isAPartOf(subfileArea, getErrorSubfileSize()+getSubfileFieldsSize());
	public FixedLengthStringData[] actvalueOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] cnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] emvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hactvalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hcnstcurOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] hcoverOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] hcrtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] hemvOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] htypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] typeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] vfundOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, getSubfileFieldsSize()+getErrorSubfileSize()+getOutputSubfileSize());

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData dtofdeathDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskcommdteDisp = new FixedLengthStringData(10);	

	public LongData Sjl42screensflWritten = new LongData(0);
	public LongData Sjl42screenctlWritten = new LongData(0);
	public LongData Sjl42screenWritten = new LongData(0);
	public LongData Sjl42protectWritten = new LongData(0);
	public GeneralTable sjl42screensfl = new GeneralTable(AppVars.getInstance());

	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21};
	public static int[] affectedInds = new int[] {};
	public FixedLengthStringData defintflag = new FixedLengthStringData(1); 

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sjl42screensfl;
	}

	public Sjl42ScreenVars() {
		super();
		initialiseScreenVars();
	}

	{
		screenIndicArea = DD.indicarea.copy();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dtofdeathOut,new String[] {"01","09","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"02","10","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(causeofdthOut,new String[] {"03","09","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimOut,new String[] {"07","09","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimTypOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(claimStatOut,new String[] {"05","09","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(riskcommdteOut,new String[] {"01",null,"-01",null, null, null, null, null, null, null, null, null});

		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenSflFields = getscreenLSflFields();
		screenSflOutFields = getscreenSflOutFields();
		screenSflErrFields = getscreenSflErrFields();

		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sjl42screen.class;
		screenSflRecord = Sjl42screensfl.class;
		screenCtlRecord = Sjl42screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sjl42protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl42screenctl.lrec.pageSubfile);
	}

	public int getDataAreaSize()
	{
		return getDataFieldsSize()+getErrorIndicatorSize()+getOutputFieldSize();
	}

	public int getDataFieldsSize()
	{
		return 294;
	}
	public int getErrorIndicatorSize()
	{
		return 96;
	}
	public int getOutputFieldSize()
	{
		return 288;
	}

	public BaseData[] getscreenFields()
	{

		return new BaseData[] {chdrnum, cnttype, ctypedes, rstate, pstate, occdate, cownnum, ownername, lifcnum, linsname, jlifcnum, jlinsname, ptdate, btdate, asterisk, astrsk, claim, claimTyp, claimStat, effdate, dtofdeath, causeofdth, riskcommdte };

	}

	public BaseData[][] getscreenOutFields()
	{

		return new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, occdateOut, cownnumOut, ownernameOut, lifcnumOut, linsnameOut, jlifcnumOut, jlinsnameOut, ptdateOut, btdateOut, asteriskOut, astrskOut, claimOut, claimTypOut, claimStatOut, effdateOut, dtofdeathOut, causeofdthOut, riskcommdteOut};

	}

	public BaseData[] getscreenErrFields()
	{

		return new BaseData[] {chdrnumErr, cnttypeErr,  ctypedesErr, rstateErr, pstateErr, occdateErr, cownnumErr, ownernameErr, lifcnumErr, linsnameErr, jlifcnumErr, jlinsnameErr, ptdateErr, btdateErr, asteriskErr, astrskErr, claimErr, claimTypErr, claimStatErr, effdateErr, dtofdeathErr,  causeofdthErr, riskcommdteErr};

	}	

	public int getSubfileAreaSize()
	{
		return 344;
	}

	public int getSubfileFieldsSize()
	{
		return 102;
	}

	public int getErrorSubfileSize()
	{
		return 60;
	}

	public int getOutputSubfileSize()
	{
		return 180;
	}

	public BaseData[] getscreenLSflFields()
	{
		return new BaseData[] {htype, hcover, hcrtable, hcnstcur, hemv, hactval, coverage, rider, liencd, actvalue, estMatValue, shortds, vfund, fieldType, cnstcur};
	}

	public BaseData[][] getscreenSflOutFields()
	{
		return new BaseData[][] {htypeOut, hcoverOut, hcrtableOut, hcnstcurOut, hemvOut, hactvalOut, coverageOut, riderOut, liencdOut, actvalueOut, emvOut, shortdsOut, vfundOut, typeOut, cnstcurOut};
	}

	public BaseData[] getscreenSflErrFields()
	{
		return new BaseData[] {htypeErr, hcoverErr, hcrtableErr, hcnstcurErr, hemvErr, hactvalErr, coverageErr, riderErr, liencdErr, actvalueErr, emvErr, shortdsErr, vfundErr, typeErr, cnstcurErr};
	}


	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {occdate, ptdate, btdate, effdate, dtofdeath,riskcommdte };
	}

	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {occdateDisp, ptdateDisp, btdateDisp, effdateDisp, dtofdeathDisp, riskcommdteDisp};
	}


	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {occdateErr, ptdateErr, btdateErr, effdateErr, dtofdeathErr, riskcommdteErr};
	}


	public static int[] getScreenSflPfInds()
	{
		return pfInds;
	}

	public static int[] getScreenSflAffectedInds()
	{
		return affectedInds;
	}
}
