package com.csc.life.terminationclaims.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LaptpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:41
 * Class transformed from LAPTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LaptpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 89;
	public FixedLengthStringData laptrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData laptpfRecord = laptrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(laptrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(laptrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(laptrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(laptrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(laptrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(laptrec);
	public PackedDecimalData newsuma = DD.newsuma.copy().isAPartOf(laptrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(laptrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(laptrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(laptrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(laptrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(laptrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(laptrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(laptrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public LaptpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for LaptpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public LaptpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for LaptpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public LaptpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for LaptpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public LaptpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("LAPTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"NEWSUMA, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     newsuma,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		newsuma.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getLaptrec() {
  		return laptrec;
	}

	public FixedLengthStringData getLaptpfRecord() {
  		return laptpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setLaptrec(what);
	}

	public void setLaptrec(Object what) {
  		this.laptrec.set(what);
	}

	public void setLaptpfRecord(Object what) {
  		this.laptpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(laptrec.getLength());
		result.set(laptrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}