package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5187
 * @version 1.0 generated on 30/08/09 06:37
 * @author Quipoz
 */
public class Sa507ScreenVars extends SmartVarModel { 
	//dataArea
	public FixedLengthStringData dataArea = new FixedLengthStringData(104);
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(56).isAPartOf(dataArea, 0);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData chdrnumflg = DD.fupflg.copy().isAPartOf(dataFields,55);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 56);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 68);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	
	//subfileArea
	public FixedLengthStringData subfileArea = new FixedLengthStringData(253);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(93).isAPartOf(subfileArea, 0);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData cnttype = DD.cntyp.copy().isAPartOf(subfileFields,9);
	public FixedLengthStringData statcode = DD.status.copy().isAPartOf(subfileFields,12);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(subfileFields,14);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(subfileFields,16);
	public FixedLengthStringData cltype = DD.ntfstat.copy().isAPartOf(subfileFields,46);
	public ZonedDecimalData riskCommDate = DD.cmtdate.copyToZonedDecimal().isAPartOf(subfileFields,76);
	public ZonedDecimalData riskCessDate = DD.cmtdate.copyToZonedDecimal().isAPartOf(subfileFields,84);
	public FixedLengthStringData slt = DD.slt.copy().isAPartOf(subfileFields,92);	//ICIL-1286
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 93);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData statcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData pstatcodeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData cltypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData riskCommDateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData riskCessDateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData sltErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 133);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] statcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] pstatcodeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] cltypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] riskCommDateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] riskCessDateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] sltOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 207);
		
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	public FixedLengthStringData riskCommDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	
	public LongData Sa507screensflWritten = new LongData(0);
	public LongData Sa507screenctlWritten = new LongData(0);
	public LongData Sa507screenWritten = new LongData(0);
	public LongData Sa507protectWritten = new LongData(0);
	public GeneralTable sa507screensfl = new GeneralTable(AppVars.getInstance());


	public boolean hasSubfile() {
		return false;
	}


	public Sa507ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"02","04","-02","03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cnttypeOut,new String[] {null, null, null, "06", null, null, null, null, null, null, null, null});
		fieldIndMap.put(statcodeOut,new String[] {null, null, null, "07", null, null, null, null, null, null, null, null});
		fieldIndMap.put(pstatcodeOut,new String[] {null, null, null, "08", null, null, null, null, null, null, null, null});
		fieldIndMap.put(longdescOut,new String[] {"09",null, "09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltypeOut,new String[] {"10",null, "10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(riskCommDateOut,new String[] {null,null,null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(riskCessDateOut,new String[] {null,null,null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumflgOut,new String[] {"11","12", "-11","13", null, null, null, null, null, null, null, null});
		fieldIndMap.put(sltOut,new String[] {"14","30","-14",null, null, null, null, null, null, null, null, null});
		//ILIFE-1138 STARTS
		screenSflFields = new BaseData[] {select, chdrnum, cnttype, statcode, pstatcode, longdesc, cltype, riskCommDate, riskCessDate, slt};
		screenSflOutFields = new BaseData[][] {selectOut, chdrnumOut, cnttypeOut, statcodeOut, pstatcodeOut, longdescOut, cltypeOut, riskCommDateOut, riskCessDateOut, sltOut};
		screenSflErrFields = new BaseData[] {selectErr, chdrnumErr, cnttypeErr, statcodeErr, pstatcodeErr, longdescErr, cltypeErr, riskCessDateErr, riskCessDateErr, sltErr};
		//ILIFE-1138 ENDS
		screenSflDateFields = new BaseData[] {riskCommDate,riskCessDate};
		screenSflDateErrFields = new BaseData[] {riskCommDateErr,riskCessDateErr};
		screenSflDateDispFields = new BaseData[] {riskCommDateDisp,riskCessDateDisp};
		
		screenFields = new BaseData[] {lifcnum, lifename,chdrnumflg};
		screenOutFields = new BaseData[][] {lifcnumOut, lifenameOut,chdrnumflgOut};
		screenErrFields = new BaseData[] {lifcnumErr, lifenameErr,chdrnumflgErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sa507screen.class;
		screenSflRecord = Sa507screensfl.class;
		screenCtlRecord = Sa507screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sa507protect.class;
		
	}
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sa507screenctl.lrec.pageSubfile);
	}
	

}
