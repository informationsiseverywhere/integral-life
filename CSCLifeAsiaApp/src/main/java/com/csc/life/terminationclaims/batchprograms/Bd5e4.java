/*
 * File: Bd5e4.java
 * Date: 29 August 2009 21:04:52
 * Author: Quipoz Limited
 *
 * Class transformed from Bd5e4.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.financials.tablestructures.Td5e6rec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.dao.impl.ZldbpfDAOImpl;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.contractservicing.procedures.Revgenat;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.procedures.Nlgcalc;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.newbusiness.tablestructures.T6799rec;
import com.csc.life.newbusiness.tablestructures.Th565rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;

import com.csc.life.reassurance.dataaccess.CovtcsnTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.dao.Bd5e4DAO;
import com.csc.life.terminationclaims.dataaccess.dao.ZlreinpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.Bd5e4DAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.ZlreinpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Zlreinpf;
import com.csc.life.terminationclaims.tablestructures.Th614rec;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsuframework.core.FeaConfg;

/**
* <pre>
*
*REMARKS.
*
*                   Auto Reinstatement
*                   ------------------
* Overview
* ________
*
* Bd5e4 will perform all the processing for auto reinstating 
* lapsed contracts .
*
*   This is the Lapse Reinstatement Batch program to reinstate
*   lapsed policy. A Reinstatement fee is
*   included which in turns based on the outstanding premium
*   at the time of the reinstatement. This fee is calculated as
*   an annual interest rate, which is applied to the premium
*   at regular frequencies.
*
*   But Reinstatement will not be permitted if there are
*   insufficient funds in the billing currency suspense account
*   to cover both the outstanding premium and fee
*   or if Fee Waiver is 'Y' in TD5E6 table 
*   configuration .
*   
*   A Debit Accounting entry will be post to the Suspense Account 
*   of the policy owner while credit entry for Lapse Reinstatement fee 
*   will be posted.Also a letter request for Lapse Reinstatement will be
*   issued.
*

*****************************************************************
* </pre>
*/
public class Bd5e4 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final Logger LOGGER = LoggerFactory.getLogger(Bd5e4.class);
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BD5E4");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

		/* WSAA-T3629-ARRAY */
	
		/* ERRORS */
	private static final String ivrm = "IVRM";
	
	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("Y");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");

	private FixedLengthStringData wsaaT6654Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Item, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Item, 1);
	
	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
	private FixedLengthStringData wsysChdrkey = new FixedLengthStringData(11).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysChdrkey, 0);
	private ZonedDecimalData wsysCnttype = new ZonedDecimalData(3, 0).isAPartOf(wsysChdrkey, 8).setUnsigned();
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(77).isAPartOf(wsysSystemErrorParams, 11);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);

	private AglfTableDAM aglfIO = new AglfTableDAM();
	
	private ZonedDecimalData wsaaInstOutst = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaToamount = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaToamountBill = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaPremTot = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFee = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaReinstatementFee = new ZonedDecimalData(17, 2);
	private PackedDecimalData wsaaSuspAvail = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaIntRate = new ZonedDecimalData(8, 5).setUnsigned();
	private ZonedDecimalData wsaaNewIntRate = new ZonedDecimalData(11, 5).setUnsigned();
	private ZonedDecimalData wsaaStdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaEnddate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaNxtCapnDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaTolerance = new ZonedDecimalData(7, 2);
	private ZonedDecimalData wsaaTolerance2 = new ZonedDecimalData(7, 2);	
	private PackedDecimalData wsaaPremTax = new PackedDecimalData(17, 2);	
	private PackedDecimalData wsaaFeeTax = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaReinstEffdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaAgtTerminateFlag = new FixedLengthStringData(1);
	private Validator agtTerminated = new Validator(wsaaAgtTerminateFlag, "Y");
	private Validator agtNotTerminated = new Validator(wsaaAgtTerminateFlag, "N");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(6);

	private FixedLengthStringData wsaaFreqMatch = new FixedLengthStringData(1);
	private Validator freqMatch = new Validator(wsaaFreqMatch, "Y");
	private StringBuilder sbTr52eKey = new StringBuilder("");
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();

	
	private LifeTableDAM lifeIO = new LifeTableDAM();
	
	/*Rec variables declaration to store a database record to use in java*/
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon1rec datcon1rec = new Datcon1rec();	
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();	//ILIFE-3997
	private WsaaTransAreaInner wsaaTransAreaInner = new WsaaTransAreaInner();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec2 = new Lifrtrnrec();
	private Reverserec reverserec = new Reverserec();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Th565rec th565rec = new Th565rec();
	private Td5e6rec td5e6rec = new Td5e6rec();
	private T6661rec t6661rec = new T6661rec();
	private T3695rec t3695rec = new T3695rec();
	private T5645rec t5645rec = new T5645rec();
	private Th614rec th614rec = new Th614rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private T5667rec t5667rec = new T5667rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T6654rec t6654rec = new T6654rec();
	
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
		
	/*Batch Upgrade Variables*/
	private String strEffDate;
	private int intBatchID;
	private int intBatchStep;
	private boolean noRecordFound;
	private int intBatchExtractSize;
	private StringUtil stringVariableReinstFee = new StringUtil();
	private boolean wsaaSubStandFlag = false;
	public ZonedDecimalData wsaaLapsePeriod = new ZonedDecimalData(2, 0);
	private PackedDecimalData wsaareinstMaxDate = new PackedDecimalData(8, 0).init(ZERO);
	private ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaPayrBillcd = new PackedDecimalData(8, 0);

	
	/*PF Used / DAO Model*/
	private Zlreinpf zlreinpf = null;
	private Chdrpf chdrpf;
	private Chdrpf chdrpfRev;
	private Ptrnpf ptrnpf;
	private Ptrnpf ptrnpfRev;
	private Ptrnpf ptrnIns;
	private Ptrnpf ptrnpfNlg;
	private Payrpf payrIO ;
	private Payrpf payrRev;
	private Taxdpf taxdIO ;
	private Descpf descpfT1688;
	/*Iterator, Maps and Lists*/
	private List<Zlreinpf> zlreinpfList;	
	private List<String> zlreinpfDelList = new ArrayList<String>();;
	private List<Zlreinpf> RPTTempData = new ArrayList<Zlreinpf>();;
	private List<Zlreinpf> zlreinpfUpList = new ArrayList<Zlreinpf>();
	private Iterator<Zlreinpf> iteratorList;
	private List<Ptrnpf> ptrnList;
	private List<Ptrnpf> ptrnUpList = new ArrayList<>();
	private List<Ptrnpf> ptrnInsList = new ArrayList<>();
	private List<Covrpf> covrpfList;
	private List<Covtpf> covtpfList;
	private List<Taxdpf> taxdInsertList;
	private List<Chdrpf> chdrUpList = new ArrayList<Chdrpf>();
	private List<Payrpf> payrUpList = new ArrayList<Payrpf>();
	private List<Lifepf> lifepfList = new ArrayList<Lifepf>();
	
	/*Smart Table Lists*/
	private Map<String, List<Itempf>> th565ListMap;	
	private Map<String, List<Itempf>> td5e6ListMap;
	private Map<String, List<Itempf>> t6661ListMap;
	private Map<String, List<Acblpf>> acblListMap;
	private Map<String, List<Lextpf>> lextpfMap;
	private Map<String, List<Payrpf>> payrListMap;
	private Map<String, List<Covrpf>> covrListMap;
	private Map<String, List<Itempf>> th614ListMap;
	private Map<String, List<Itempf>> t5645ListMap;
	private Map<String, List<Itempf>> t3695ListMap;
	private Map<String, List<Itempf>> tr52dListMap;
	private Map<String, List<Itempf>> tr52eListMap;
	private Map<String, List<Itempf>> t5667ListMap;
	private Map<String, Descpf> t1688Map;
	private Map<String, List<Itempf>> tr384ListMap;
	private Map<String, List<Itempf>> t6654ListMap;
	
	/*DAO Used*/
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
    private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private Bd5e4DAO bd5e4DAO =  new Bd5e4DAOImpl();// getApplicationContext().getBean("bd5e4DAO", Bd5e4DAO.class);
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
    private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
    private ZlreinpfDAO zlreinpfDAO = new ZlreinpfDAOImpl();// getApplicationContext().getBean("zlreinDAO", ZlreinpfDAO.class);
    private AcblpfDAO acblpfDAO =  getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
    private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
    private TaxdpfDAO taxdpfDAO =  getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
    private LextpfDAO lextpfDAO =  getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
    private CovtpfDAO covtpfDAO =  getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
    private LifepfDAO lifepfDAO =  getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
    private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    
	/*Error Codes Used*/
	private static final String h205 = new String("H205");	
	private static final String f014 = new String("F014");
	private static final String hl67 = new String("HL67");
	private static final String rl01 = new String("RL01");
	private static final String hl68 = new String("HL68");
	private static final String hl69 = new String("HL69");
	private static final String h085 = new String("H085");
	private static final String h134 = new String("H134");
	private static final String h420 = new String("H420");
	private static final String e961 = new String("E961");
	private static final String f247 = new String("F247");
	private static final String e101 = new String("E101");
	private static final String hl66 = new String("HL66");
	private static final String rl00 = new String("RL00");
	/*Batch Codes*/
	private static final String b673 = "B673";
	private static final String t514 = "T514";
	private static final String t575 = "T575";

	
	/*Control Totals*/
	private long ctrCT01;	
	private long ctrCT03;
	private long ctrCT04;
	
	private FixedLengthStringData wsaaTrcde = new FixedLengthStringData(1);
	private Validator trcdeMatch = new Validator(wsaaTrcde, "Y");
	private Validator trcdeNotMatch = new Validator(wsaaTrcde, "N");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	
	private P6671par p6671par = new P6671par();
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	
	/*T table name to store in map*/
	private static final String td5e6 = "TD5E6";
	private static final String th565 = "TH565";
	private static final String t6661 = "T6661";
	private static final String th614 = "TH614";
	private static final String t5645 = "T5645";
	private static final String t3695 = "T3695";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private static final String t1688 = "T1688";
	private static final String tr384 = "TR384";
	private static final String t5667 = "T5667";
	private static final String t6654 = "T6654";
	
	private BigDecimal instprm = BigDecimal.ZERO;
/**
 * Contains all possible labels used by goTo action.
 */
	
	public Bd5e4() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Restarting of this program is handled by MAINB,*/
		/** using a restart method of '3'.*/
		/*EXIT*/
	}

protected void initialise1000(){
	BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
	strEffDate = bsscIO.getEffectiveDate().toString();	
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	wsaaToday.set(datcon1rec.intDate);
	if (isNE(bprdIO.getRestartMethod(), "3")) {
		syserrrec.statuz.set(ivrm);
		fatalError600();
	}
	/*    Since this program runs from a parameter screen move the*/
	/*    internal parameter area to the original parameter copybook*/
	/*    to retrieve the contract range*/
	bupaIO.setDataArea(lsaaBuparec);
	p6671par.parmRecord.set(bupaIO.getParmarea());
	if (isEQ(p6671par.chdrnum, SPACES)
	&& isEQ(p6671par.chdrnum1, SPACES)) {
		wsaaChdrnumFrom.set(LOVALUE);
		wsaaChdrnumTo.set(HIVALUE);
	}
	else {
		wsaaChdrnumFrom.set(p6671par.chdrnum);
		wsaaChdrnumTo.set(p6671par.chdrnum1);
	}	
	if (bprdIO.systemParam01.isNumeric())
		if (bprdIO.systemParam01.toInt() > 0)
			intBatchExtractSize = bprdIO.systemParam01.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	else
		intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();	
		
	intBatchID = 0;
	intBatchStep = 0;
	noRecordFound = false;
	zlreinpfList = new ArrayList<Zlreinpf>();
	
	
	StringBuilder rcptTempTableName = new StringBuilder("ZLREINPF");
	
	bd5e4DAO.populateFailBIRptTmp(rcptTempTableName.toString(),wsaaChdrnumFrom.toString(),wsaaChdrnumTo.toString(),bsscIO.getScheduleNumber().toInt());
	
	int extractRows = bd5e4DAO.populateBd5e4Temp(intBatchExtractSize, rcptTempTableName.toString(),wsaaChdrnumFrom.toString(),wsaaChdrnumTo.toString());	
	
	if (extractRows > 0) {
		if (!performDataChunk()){
			wsspEdterror.set(SPACES);
			return;
		}
		
		if (!zlreinpfList.isEmpty()){
			initialise1010();
			readSmartTables(bsprIO.getCompany().toString().trim());						
		}			
	}
	else{
		LOGGER.info("No Lapsed conatrcts records retrieved for reinstatment.");
		noRecordFound = true;
		return;
	}		
}

protected void initialise1010(){
	th565ListMap = new HashMap<String, List<Itempf>>();
	td5e6ListMap = new HashMap<String, List<Itempf>>();
	t6661ListMap = new HashMap<String, List<Itempf>>();
	t5645ListMap = new HashMap<String, List<Itempf>>();
	t3695ListMap = new HashMap<String, List<Itempf>>();
	th614ListMap = new HashMap<String, List<Itempf>>();
	tr52dListMap = new HashMap<String, List<Itempf>>();
	tr52eListMap = new HashMap<String, List<Itempf>>();
	t5667ListMap = new HashMap<String, List<Itempf>>();
	tr384ListMap = new HashMap<String, List<Itempf>>();
	t6654ListMap = new HashMap<String, List<Itempf>>();
	t1688Map = new HashMap<String, Descpf>();
	
	ctrCT01=0;	
	ctrCT03=0;
	ctrCT04=0;
	
}

private void readSmartTables(String company){		
	td5e6ListMap = itemDAO.loadSmartTable("IT", company, td5e6);
	th565ListMap = itemDAO.loadSmartTable("IT", company, th565);
	t6661ListMap = itemDAO.loadSmartTable("IT", company, t6661);
	t3695ListMap = itemDAO.loadSmartTable("IT", company, t3695);
	t5645ListMap = itemDAO.loadSmartTable("IT", company, t5645);
	th614ListMap = itemDAO.loadSmartTable("IT", company, th614);
	tr52dListMap = itemDAO.loadSmartTable("IT", company, tr52d);
	tr52eListMap = itemDAO.loadSmartTable("IT", company, tr52e);
	t5667ListMap = itemDAO.loadSmartTable("IT", company, t5667);
	t6654ListMap = itemDAO.loadSmartTable("IT", company, t6654);
	tr384ListMap = itemDAO.loadSmartTable("IT", company, tr384);
	t1688Map = descDAO.getItems("IT", bsprIO.getCompany().toString(), t1688, bsscIO.getLanguage().toString());
}



protected void readTd5e6(){
	String keyItemitem = chdrpf.getCnttype();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (td5e6ListMap.containsKey(keyItemitem)){	
		itempfList = td5e6ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					td5e6rec.td5e6Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				td5e6rec.td5e6Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		
		zlreinpf.setErorcde(h205);
		return;	
	}	
}

protected boolean performDataChunk(){
	acblListMap = new HashMap<String, List<Acblpf>>();
	covrListMap = new HashMap<String, List<Covrpf>>();
	payrListMap = new HashMap<String, List<Payrpf>>();
	lextpfMap = new HashMap<String, List<Lextpf>>();
	
	boolean doPerform = true;
	intBatchID = intBatchStep;	//make zlreinpf as null	
	if (zlreinpfList.size() > 0) 
		zlreinpfList.clear();
	zlreinpfList = bd5e4DAO.loadDataByBatch(intBatchID);
	if (zlreinpfList.isEmpty()) 
		wsspEdterror.set(varcom.endp);
	else {
		iteratorList = zlreinpfList.iterator();
		
        Set<String> chdrnumSet = new HashSet<String>();
        Set<String> clntnumSet = new HashSet<String>();
        for(Zlreinpf zlreinpf:zlreinpfList){
        	     chdrnumSet.add(zlreinpf.getChdrnum());           
            if (!clntnumSet.contains(zlreinpf.getOwnnum())) 
            	clntnumSet.add(zlreinpf.getOwnnum());
        }
        
        if (chdrnumSet == null || chdrnumSet.isEmpty())  doPerform = false;

	        List<String> chdrList = new ArrayList<String>(chdrnumSet);
	        List<String> clntList = new ArrayList<String>(clntnumSet);
	        taxdInsertList = new ArrayList<Taxdpf>();
	        acblListMap = acblpfDAO.searchAcblRecord(bsprIO.getCompany().toString().trim(), chdrList);
	        payrListMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(bsprIO.getCompany().trim(),chdrList);
	        covrListMap = covrpfDAO.searchCovrMap(bsprIO.getCompany().toString().trim(), chdrList);
	        lextpfMap = lextpfDAO.searchLextpfMap(bsprIO.getCompany().toString().trim(), chdrList);
	}	
	return doPerform;
}


protected void readFile2000(){
	if (!zlreinpfList.isEmpty()){
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			if (!performDataChunk()) {
				wsspEdterror.set(SPACES);
				return;
			}
			if (!zlreinpfList.isEmpty()){
				if(zlreinpf != null && !(zlreinpf.getErorcde() == null 
						|| zlreinpf.getErorcde().isEmpty())) {
					zlreinpfUpList.add(zlreinpf);
				}
				zlreinpf = new Zlreinpf();
				zlreinpf = iteratorList.next();
				
				ctrCT01++;
			} else {
				if(zlreinpf != null && !(zlreinpf.getErorcde() == null 
						|| zlreinpf.getErorcde().isEmpty())) {
					zlreinpfUpList.add(zlreinpf);
				}
			}
		}else {	
			if(zlreinpf != null && !(zlreinpf.getErorcde() == null 
					|| zlreinpf.getErorcde().isEmpty())) {
				zlreinpfUpList.add(zlreinpf);
			}
			zlreinpf = new Zlreinpf();
			zlreinpf = iteratorList.next();
			ctrCT01++;
		}	
		/*  Set up the key for the SYSR- copybook, should a system error*/
		/*  for this instalment occur.*/
		wsysChdrnum.set(zlreinpf.getChdrnum());
		wsysCnttype.set(zlreinpf.getCnttype());
	}else wsspEdterror.set(varcom.endp);		
}

protected void edit2500() {
	if (isNE(wsysChdrnum, wsaaPrevChdrnum)) {
		wsaaPrevChdrnum.set(wsysChdrnum);		
		chdrpf = chdrpfDAO.getchdrRecord(zlreinpf.getChdrcoy(),zlreinpf.getChdrnum());//IJTI-1410
		if(chdrpf != null)
		{
			if (isEQ(chdrpf.getBillfreq(), "00")) {
				zlreinpf.setErorcde(h085);
				wsaaValidChdr.set("N");
			}	
		}
		if (!validContract.isTrue()) {
			ctrCT03++;
			wsspEdterror.set(SPACES);
			return ;
		}			
		
		softlock2580();
		/*  Log CHDRs locked*/
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			ctrCT04++;
			wsspEdterror.set(SPACES);
			zlreinpf.setErorcde(e101);
		}	
	} else {
		wsspEdterror.set(SPACES);
		return;
	}
	
 }

protected void softlock2580()
 {
		/* Soft lock the contract, if it is to be processed.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(zlreinpf.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(zlreinpf.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
}

protected void update3000(){
	covrpfList = new ArrayList<Covrpf>();	
	readPtrnAndValidate();
	if(zlreinpf != null && !(zlreinpf.getErorcde() == null 
			|| zlreinpf.getErorcde().isEmpty())) {

		wsspEdterror.set(SPACES);
		return ;		
	}	
	readTd5e6();
	if(zlreinpf != null && !(zlreinpf.getErorcde() == null 
			|| zlreinpf.getErorcde().isEmpty())) {

		wsspEdterror.set(SPACES);
		return ;		
	}	
	readPayr();	
	if(zlreinpf != null && !(zlreinpf.getErorcde() == null 
			|| zlreinpf.getErorcde().isEmpty())) {

		wsspEdterror.set(SPACES);
		return ;		
	}	
	datcon4rec.freqFactor.set(payrIO.getBillfreq());
	datcon4rec.frequency.set(payrIO.getBillfreq());
	datcon4rec.intDate1.set(payrIO.getBillcd());
	datcon4rec.billday.set(payrIO.getBillday());
	datcon4rec.billmonth.set(payrIO.getBillmonth());
	callProgram(Datcon4.class, datcon4rec.datcon4Rec);
	if (isNE(datcon4rec.statuz,varcom.oK)) {
		syserrrec.params.set(datcon4rec.datcon4Rec);
		syserrrec.statuz.set(datcon4rec.statuz);
		fatalError600();
	}
	
	if (isGTE(strEffDate,datcon4rec.intDate2)) {
		zlreinpf.setErorcde(hl66);
		wsspEdterror.set(SPACES);
		return ;	
	}
	
	if (isLT(strEffDate,payrIO.getBillcd())) {
		zlreinpf.setErorcde(rl00);
		wsspEdterror.set(SPACES);
		return ;
	}
	wsaaReinstEffdate.set(zlreinpf.getEffdate());	
	readAcbl();
	if(isLTE(wsaaSuspAvail,ZERO.intValue()))
	{
		zlreinpf.setErorcde(e961);		
		wsspEdterror.set(SPACES);
		return ;
	}
	readTh614();
	if(zlreinpf != null && !(zlreinpf.getErorcde() == null 
			|| zlreinpf.getErorcde().isEmpty())) {

		wsspEdterror.set(SPACES);
		return ;		
	}
	datcon3rec.datcon3Rec.set(SPACES);
	datcon3rec.intDate1.set(payrIO.getBillcd());
	datcon3rec.intDate2.set(wsaaReinstEffdate);
	datcon3rec.frequency.set(payrIO.getBillfreq());
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz,"****")) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	wsaaInstOutst.set(datcon3rec.freqFactor);
	wsaaInstOutst.add(1);
	wsaaPremTot.set(0);
	instprm=covrpfDAO.getSumInstPrem(payrIO.getChdrnum().toString());
	compute(wsaaPremTot, 2).set(add(wsaaPremTot,instprm));
	compute(wsaaPremTot, 2).set(add(wsaaPremTot,payrIO.getSinstamt02()));
	compute(wsaaPremTot, 2).set(add(wsaaPremTot,payrIO.getSinstamt03()));
	compute(wsaaPremTot, 2).set(add(wsaaPremTot,payrIO.getSinstamt04()));
	compute(wsaaPremTot, 2).set(add(wsaaPremTot,payrIO.getSinstamt05()));
	//wsaaPremTot.set(payrIO.getSinstamt06());
	compute(wsaaPremTot, 2).set(mult(wsaaPremTot,wsaaInstOutst));
	wsaaReinstatementFee.set(0);
	wsaaFeeTax.set(ZERO);
	if(!td5e6rec.waiver.equals("Y"))
		calculateReinstFee();	
	calcPremTax();	
	compute(wsaaPremTot, 2).set(add(wsaaPremTot, mult(wsaaPremTax, wsaaInstOutst)));	
	calcTotalOS();
	if(zlreinpf != null && !(zlreinpf.getErorcde() == null 
			|| zlreinpf.getErorcde().isEmpty())) {

		wsspEdterror.set(SPACES);
		return ;		
	}	else {
		
		doReversal();
	}
}

protected void readPtrnAndValidate(){
	ptrnList = ptrnpfDAO.getPtrnrevData(zlreinpf.getChdrcoy(),zlreinpf.getChdrnum(),null);
	for (Ptrnpf ptrnpf: ptrnList) {
		if ((isEQ(ptrnpf.getBatctrcde(),b673)) ||
			(isEQ(ptrnpf.getBatctrcde(),t514)) ||
			(isEQ(ptrnpf.getBatctrcde(),t575 ))) {
			    this.ptrnpf = new Ptrnpf();
			    this.ptrnpf = ptrnpf;
				break;
			}
	}
	readTH565();	
	if(zlreinpf != null && (zlreinpf.getErorcde() == null 
			|| zlreinpf.getErorcde().isEmpty())) {
		readT6661(ptrnpf.getBatctrcde());
	}
}

protected void readTH565(){
	String keyItemitem = bprdIO.getAuthCode().toString();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (th565ListMap.containsKey(keyItemitem)){	
		itempfList = th565ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					th565rec.th565Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				th565rec.th565Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {		
		zlreinpf.setErorcde(hl68);				
	}	else {
		trcdeNotMatch.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)
		|| trcdeMatch.isTrue()); wsaaSub.add(1)){
			if (isEQ(ptrnpf.getBatctrcde(),th565rec.ztrcde[wsaaSub.toInt()])) {
				trcdeMatch.setTrue();
			}
		}
		if (trcdeNotMatch.isTrue()) {
			zlreinpf.setErorcde(hl69);
		}
	}
}

protected void readT6661(String itemKey){
	String keyItemitem = itemKey.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t6661ListMap.containsKey(keyItemitem)){	
		itempfList = t6661ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t6661rec.t6661Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t6661rec.t6661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {		
		zlreinpf.setErorcde(f014);				
	}	else {
		if (isNE(t6661rec.contRevFlag,"Y")) {
			zlreinpf.setErorcde(hl67);
		}
	}
}

protected void validateLapseEffdate()
{
 int wsaaTd5e6Sub;	
 for (wsaaTd5e6Sub = 1; wsaaTd5e6Sub <= 5; wsaaTd5e6Sub++) {
        if (isEQ(td5e6rec.freqStat[wsaaTd5e6Sub], chdrpf.getBillfreq())) {
        	checkLextData();
        	if(wsaaSubStandFlag){
        		wsaaLapsePeriod.set(td5e6rec.substdStat[wsaaTd5e6Sub]);
        	}else{
        		wsaaLapsePeriod.set(td5e6rec.stdStat[wsaaTd5e6Sub]);
        	}
        	//calculate new date by adding number of days plus Lapse effective date
        	datcon2rec.datcon2Rec.set(SPACES);
    		datcon2rec.intDate1.set(ptrnpf.getPtrneff());
    		datcon2rec.freqFactor.set(wsaaLapsePeriod);
    		datcon2rec.frequency.set("DY");
    		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
    		if (isNE(datcon2rec.statuz,Varcom.oK)) {
    			syserrrec.statuz.set(datcon2rec.statuz);
    			syserrrec.params.set(datcon2rec.datcon2Rec);
    			fatalError600();
    		}
    		wsaareinstMaxDate.set(datcon2rec.intDate2);
    		/*
    		 * check that the Receipt date is in the range of the Lapse Date and Lapse Date plus Number of Days
    		 * */
    		if(isGTE(bsscIO.getEffectiveDate(), ptrnpf.getPtrneff()) &&
    				isLTE(bsscIO.getEffectiveDate(), wsaareinstMaxDate) ){
    			wsaaValidChdr.set("Y");
    		}    		
    		break;	
        }
    } 	
}
protected void checkLextData()
{	
	if (lextpfMap != null && lextpfMap.containsKey(zlreinpf.getChdrnum())) {
		for (Lextpf c : lextpfMap.get(zlreinpf.getChdrnum())) {
			if (c.getSbstdl().equals("Y")) {
				wsaaSubStandFlag =true;
				break;
			}
		}
	}
}

protected void readT5645(){
	String keyItemitem = wsaaProg.toString();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5645ListMap.containsKey(keyItemitem)){	
		itempfList = t5645ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {		
		zlreinpf.setErorcde(h134);				
	}	else {
		readT3695();
	}
}

protected void readT3695(){
	String keyItemitem = t5645rec.sacstype01.toString().trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t3695ListMap.containsKey(keyItemitem)){	
		itempfList = t3695ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {		
		zlreinpf.setErorcde(h420);				
	}	
}

protected void readTh614(){
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(chdrpf.getCnttype());
	stringVariable1.addExpression(chdrpf.getCntcurr());	
	String keyItemitem = stringVariable1.toString();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (th614ListMap.containsKey(keyItemitem)){	
		itempfList = th614ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(wsaaReinstEffdate.toString()) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){//IJTI-1410
					th614rec.th614Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				th614rec.th614Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {		
		zlreinpf.setErorcde(rl01);				
	}	
}


protected void readTr52d(){
	String keyItemitem = chdrpf.getReg();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr52dListMap.containsKey(keyItemitem)){	
		itempfList = tr52dListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
					tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
				}
			}else{
				tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
				itemFound = true;					
			}				
		}		
	}
	if (!itemFound) {
		keyItemitem = "***".trim();//IJTI-1410
		itempfList = new ArrayList<Itempf>();
		boolean itemFound2 = false;
		if (tr52dListMap.containsKey(keyItemitem)){	
			itempfList = tr52dListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
						tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound2 = true;
					}
				}else{
					tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound2 = true;					
				}				
			}		
		}				
	}	
}

protected boolean readTr52e(String itemKey, Integer effDate){
	String keyItemitem = itemKey.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (tr52eListMap.containsKey(keyItemitem)){	
		itempfList = tr52eListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((effDate >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& effDate <= Integer.parseInt(itempf.getItmto().toString())){
					tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
					break;
				}
			}else{
				tr52erec.tr52eRec.set(StringUtil.rawToString(itempf.getGenarea()));	
				itemFound = true;
				break;
			}				
		}		
	}
	return itemFound;	
}

protected boolean readT5667(String itemKey, Integer effDate){
	String keyItemitem = itemKey.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t5667ListMap.containsKey(keyItemitem)){	
		itempfList = t5667ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((effDate >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& effDate <= Integer.parseInt(itempf.getItmto().toString())){
					t5667rec.t5667Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
					break;
				}
			}else{
				t5667rec.t5667Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
				itemFound = true;
				break;
			}				
		}		
	}
	return itemFound;	
}

/*protected boolean readT6654(String key,Integer effDate) {
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy("2");
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
			if((effDate >= Integer.parseInt(itempf.getItmfrm().toString()) )
					&& effDate <= Integer.parseInt(itempf.getItmto().toString())){
				t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
				return true;	
			}
		}else{
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
			return true;
		}		
	}
	return false;
}*/

protected boolean readT6654(String itemKey, Integer effDate){
	String keyItemitem = itemKey.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (t6654ListMap.containsKey(keyItemitem)){	
		itempfList = t6654ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
				if((effDate >= Integer.parseInt(itempf.getItmfrm().toString()) )
						&& effDate <= Integer.parseInt(itempf.getItmto().toString())){
					t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
					itemFound = true;
					break;
				}
			}else{
				t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));	
				itemFound = true;
				break;
			}				
		}		
	}
	return itemFound;	
}

protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(controlTotalsInner.ct01);
	contotrec.totval.set(ctrCT01);
	callContot001();
	ctrCT01 = 0;
	
	contotrec.totno.set(controlTotalsInner.ct03);
	contotrec.totval.set(ctrCT03);
	callContot001();
	ctrCT03 = 0;
	
	contotrec.totno.set(controlTotalsInner.ct04);
	contotrec.totval.set(ctrCT04);
	callContot001();
	ctrCT04 = 0;
	
}



protected void readPayr(){
	
    if(payrListMap.size() > 0 && payrListMap.containsKey(zlreinpf.getChdrnum())){
        for(Payrpf p : payrListMap.get(zlreinpf.getChdrnum())){
            if(zlreinpf.getChdrcoy().equals(p.getChdrcoy()) 
            		&& 1 == p.getPayrseqno()){
                payrIO = p;
                break;
            }
        }
    }
    if(payrIO == null){
        syserrrec.params.set(zlreinpf.getChdrnum());
		fatalError600();
	}
}

protected void readAcbl(){
	readT5645();
	if(zlreinpf != null && !(zlreinpf.getErorcde() == null 
			|| zlreinpf.getErorcde().isEmpty())) {

		wsspEdterror.set(SPACES);
		return ;		
	}
	wsaaSuspAvail.set(0);
	Acblpf currAcbl = null;
	String chdrnum = zlreinpf.getChdrnum();
	boolean recordFound = false;
    if (acblListMap != null && acblListMap.containsKey(chdrnum)) {
        List<Acblpf> acblpfList = acblListMap.get(chdrnum);
        for (Iterator<Acblpf> iterator = acblpfList.iterator(); iterator.hasNext(); ){
        	Acblpf acbl = iterator.next();
            if ((acbl.getOrigcurr().equals(chdrpf.getBillcurr())) && acbl.getSacscode().equals(t5645rec.sacscode01.toString().trim())
                    && acbl.getSacstyp().trim().equals(t5645rec.sacstype01.toString().trim())) {
            		recordFound = true;
            		currAcbl = acbl;
            		break;
            }        	
        }
    }
    /* Multiply the balance by the sign from T3695.*/
    if (recordFound) {
		if (isEQ(t3695rec.sign, "-")) {
			compute(wsaaSuspAvail, 2).set(mult(currAcbl.getSacscurbal(),-1));
		}
		else {
			wsaaSuspAvail.set(currAcbl.getSacscurbal());
		}    	
    }
    
}

protected void calculateReinstFee(){
	
	wsaaStdate.set(payrIO.getBillcd());
	wsaaEnddate.set(0);
	wsaaIntRate.set(0);
	wsaaNewIntRate.set(ZERO);
	wsaaFee.set(0);		
	while ( !(isGTE(wsaaEnddate,wsaaReinstEffdate))) {
		if (isEQ(th614rec.compfreq,SPACES)) {
			wsaaEnddate.set(wsaaReinstEffdate);
		}
		else {
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.intDate1.set(wsaaStdate);
			datcon2rec.freqFactor.set(1);
			datcon2rec.frequency.set(th614rec.compfreq);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,Varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				fatalError600();
			}
			wsaaEnddate.set(datcon2rec.intDate2);
			wsaaNxtCapnDate.set(datcon2rec.intDate2);
			if (isGT(wsaaEnddate,wsaaReinstEffdate)) {
				wsaaEnddate.set(wsaaReinstEffdate);
			}
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(wsaaStdate);
		datcon3rec.intDate2.set(wsaaEnddate);
		datcon3rec.frequency.set(th614rec.interestFrequency);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaIntRate.set(th614rec.intRate);		
		compute(wsaaNewIntRate, 5).set(div((mult(wsaaIntRate,wsaaPremTot)),100));
		compute(wsaaFee, 5).set(mult(wsaaNewIntRate,datcon3rec.freqFactor));
		zrdecplrec.amountIn.set(wsaaFee);
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		callRounding8000();
		wsaaFee.set(zrdecplrec.amountOut);
		wsaaReinstatementFee.add(wsaaFee);
		if (isLT(wsaaNxtCapnDate,wsaaReinstEffdate)) {	
			wsaaPremTot.add(wsaaFee);
			wsaaStdate.set(wsaaNxtCapnDate);
		}
	}	
}

protected void calcPremTax() {
	
	/* Initialize and read TR52D */
	wsaaPremTax.set(ZERO);
	readTr52d();
	if (isEQ(tr52drec.txcode, SPACES)) {
		return;
	}	
	/* Read all records in COVRENQ under the same contract */

	if (covrListMap != null && covrListMap.containsKey(zlreinpf.getChdrnum())) {
        covrpfList = covrListMap.get(zlreinpf.getChdrnum());
        for (Iterator<Covrpf> iterator = covrpfList.iterator(); iterator.hasNext(); ){
        	Covrpf covrpf = iterator.next();
    		if (covrpf.getValidflag().equals("1") && covrpf.getChdrcoy().equals(zlreinpf.getChdrcoy())) 
    		{
    			if ( isNE(covrpf.getInstprem(), ZERO.intValue())
    					&& isGT(covrpf.getPremCessDate(), chdrpf.getPtdate().intValue())) {
    				calcCompTax(covrpf);
    			}
            }        	
        }                
    }	
	calcCntfeeTax();
}
protected void calcCompTax(Covrpf covrpf)
{
	/*  Read table TR52E.                                              */
	sbTr52eKey = new StringBuilder("");
	sbTr52eKey.append(tr52drec.txcode.toString().trim());
	sbTr52eKey.append(chdrpf.getCnttype().trim());//IJTI-1410
	sbTr52eKey.append(covrpf.getCrtable().trim());//IJTI-1410
	boolean itemFound = readTr52e(sbTr52eKey.toString(), chdrpf.getOccdate());
	
	if (!itemFound){
		sbTr52eKey = new StringBuilder("");
		sbTr52eKey.append(tr52drec.txcode.toString().trim());
		sbTr52eKey.append(chdrpf.getCnttype().trim());//IJTI-1410
		sbTr52eKey.append("****");	
		itemFound = readTr52e(sbTr52eKey.toString(), chdrpf.getOccdate());
		if (!itemFound){
			sbTr52eKey = new StringBuilder("");
			sbTr52eKey.append(tr52drec.txcode.toString().trim());
			sbTr52eKey.append("***");
			sbTr52eKey.append("****");	
			itemFound = readTr52e(sbTr52eKey.toString(), chdrpf.getOccdate());
			if (!itemFound){
				syserrrec.params.set(sbTr52eKey);
				fatalError600();
			}
		}
	}	
	gotTr52e(covrpf);	
}

protected void gotTr52e(Covrpf covrpf)
{
	if (isNE(tr52erec.taxind01, "Y")) {
		return ;
	}
	/* Call TR52D tax subroutine                                       */
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(zlreinpf.getChdrcoy());
	txcalcrec.chdrnum.set(zlreinpf.getChdrnum());
	txcalcrec.life.set(covrpf.getLife());
	txcalcrec.coverage.set(covrpf.getCoverage());
	txcalcrec.rider.set(covrpf.getRider());
	txcalcrec.planSuffix.set(covrpf.getPlanSuffix());
	txcalcrec.crtable.set(covrpf.getCrtable());
	txcalcrec.cnttype.set(chdrpf.getCnttype());
	txcalcrec.register.set(chdrpf.getReg());
	txcalcrec.transType.set("PREM");
	txcalcrec.ccy.set(chdrpf.getCntcurr());
	txcalcrec.effdate.set(wsaaReinstEffdate);
	txcalcrec.taxrule.set(sbTr52eKey);
	txcalcrec.rateItem.set(SPACES);
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(chdrpf.getCntcurr());
	stringVariable1.addExpression(tr52erec.txitem);
	stringVariable1.setStringInto(txcalcrec.rateItem);
	if (isEQ(tr52erec.zbastyp, "Y")) {
		compute(txcalcrec.amountIn, 2).set(sub(covrpf.getInstprem(), covrpf.getZlinstprem()));
	}
	else {
		txcalcrec.amountIn.set(covrpf.getInstprem());
	}
	txcalcrec.tranno.set(ZERO);
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaPremTax.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaPremTax.add(txcalcrec.taxAmt[2]);
		}
	}
}

protected void calcCntfeeTax()
{
	if (isEQ(payrIO.getSinstamt02(), ZERO)) {
		return;
	}
	/* Calculate tax on the contract fee                               */
	/* Read table TR52E                                                */
	/*  Read table TR52E.                                              */
	sbTr52eKey = new StringBuilder("");
	sbTr52eKey.append(tr52drec.txcode.toString().trim());
	sbTr52eKey.append(chdrpf.getCnttype().trim());//IJTI-1410
	sbTr52eKey.append("****");	
	boolean itemFound = readTr52e(sbTr52eKey.toString(), chdrpf.getOccdate());
	if (!itemFound){
		sbTr52eKey = new StringBuilder("");
		sbTr52eKey.append(tr52drec.txcode.toString().trim());
		sbTr52eKey.append("***");
		sbTr52eKey.append("****");	
		itemFound = readTr52e(sbTr52eKey.toString(), chdrpf.getOccdate());
		if (!itemFound){
			syserrrec.params.set(sbTr52eKey);
			fatalError600();
		}
	}
		 
	gotCntfeeTr52e();
			
}

protected void gotCntfeeTr52e()
{
	if (isNE(tr52erec.taxind02, "Y")) {
		return ;
	}
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(chdrpf.getChdrcoy());
	txcalcrec.chdrnum.set(chdrpf.getChdrnum());
	txcalcrec.life.set(SPACES);
	txcalcrec.coverage.set(SPACES);
	txcalcrec.rider.set(SPACES);
	txcalcrec.planSuffix.set(0);
	txcalcrec.crtable.set(SPACES);
	txcalcrec.cnttype.set(chdrpf.getCnttype());
	txcalcrec.register.set(chdrpf.getReg());
	txcalcrec.transType.set("CNTF");
	txcalcrec.ccy.set(chdrpf.getCntcurr());
	txcalcrec.effdate.set(wsaaReinstEffdate);
	txcalcrec.taxrule.set(sbTr52eKey);
	txcalcrec.rateItem.set(SPACES);
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(chdrpf.getCntcurr());
	stringVariable1.addExpression(tr52erec.txitem);
	stringVariable1.setStringInto(txcalcrec.rateItem);
	txcalcrec.amountIn.set(payrIO.getSinstamt02());
	txcalcrec.tranno.set(ZERO);
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaPremTax.add(txcalcrec.taxAmt[1]);
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaPremTax.add(txcalcrec.taxAmt[2]);
		}
	}
}

protected void calcTotalOS()
{
	if(isGT(wsaaReinstatementFee,ZERO))
		calcReintFeeTax();	
	wsaaToamount.set(wsaaPremTot);	
	wsaaToamount.add(wsaaFeeTax);
	wsaaToamount.add(wsaaReinstatementFee);	
	if (isNE(payrIO.getBillcurr(),payrIO.getCntcurr())) {
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountIn.set(wsaaPremTot);
		conlinkrec.statuz.set(SPACES);
		conlinkrec.function.set("SURR");
		conlinkrec.currIn.set(payrIO.getCntcurr());
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(payrIO.getBillcurr());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,Varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		else {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(conlinkrec.currOut);
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);			
			wsaaToamountBill.set(conlinkrec.amountOut);
		}
		conlinkrec.amountIn.set(wsaaReinstatementFee);
		conlinkrec.amountOut.set(ZERO);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,Varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		else {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(conlinkrec.currOut);
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);			
			wsaaToamountBill.add(conlinkrec.amountOut);
		}		
		conlinkrec.amountIn.set(wsaaFeeTax);
		conlinkrec.amountOut.set(ZERO);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, Varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		else {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(conlinkrec.currOut);
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
			wsaaToamountBill.add(conlinkrec.amountOut);
		}
		wsaaToamount.set(wsaaToamountBill);
	}		
	wsaaTolerance.set(0);
	wsaaTolerance2.set(0);	
	if (isGT(wsaaToamount,wsaaSuspAvail)) {
		checkAgent();
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(bprdIO.getAuthCode().toString());
		stringVariable1.addExpression(chdrpf.getBillcurr());			
		if (!readT5667(stringVariable1.toString(),0)) {		
			zlreinpf.setErorcde(f247);	
			return;
		} else {
			calcTolerance();
		}
	}
	wsaaSuspAvail.add(wsaaTolerance);
	
	if (isGT(wsaaToamount,wsaaSuspAvail)) {
		wsaaSuspAvail.subtract(wsaaTolerance);
		wsaaSuspAvail.add(wsaaTolerance2);
		if (isGT(wsaaToamount,wsaaSuspAvail)) {
			zlreinpf.setErorcde(e961);
			return;
		}
	}
	
}
protected void doReversal()
{
	processPTRN();
	
	processChdrPayr();
	housekeeping();
	a000Statistics();	
	
	if(isEQ(chdrpf.getNlgflg(),"Y")) {
		nlgflg3020();
	}
	//ILIFE-3997 End
	if (isGT(wsaaTransAreaInner.wsaaTotalfee, 0)) {
		putTaxdEntry();
		postFee();		
	}
	writeLetc();
	releaseSoftlock();
	//delete after print report
	if(zlreinpfDelList == null)
		zlreinpfDelList = new ArrayList<String>();
	zlreinpfDelList.add(zlreinpf.getChdrnum());
	if(RPTTempData == null)
		RPTTempData = new ArrayList<Zlreinpf>();
	RPTTempData.add(zlreinpf);
}
protected void processPTRN() 
{
	/*atreqrec.atreqRec.set(SPACES);
	atreqrec.acctYear.set(ZERO);
	atreqrec.acctMonth.set(ZERO);
	atreqrec.module.set("PH613AT");
	atreqrec.batchKey.set(batcdorrec.batchkey);
	atreqrec.reqProg.set(wsaaProg);
	atreqrec.reqUser.set(varcom.vrcmUser);
	atreqrec.reqTerm.set(varcom.vrcmTermid);
	atreqrec.reqDate.set(wsaaToday);
	atreqrec.reqTime.set(varcom.vrcmTime);
	atreqrec.language.set(bsscIO.getLanguage().toString());	
	wsaaPrimaryChdrnum.set(zlreinpf.getChdrnum());
	atreqrec.primaryKey.set(wsaaPrimaryKey);	*/
	wsaaTransAreaInner.wsaaFsuCoy.set(bsprIO.getFsuco().toString().trim());
	wsaaTransAreaInner.wsaaTranDate.set(varcom.vrcmDate);
	wsaaTransAreaInner.wsaaTranTime.set(varcom.vrcmTime);
	wsaaTransAreaInner.wsaaUser.set(varcom.vrcmUser);
	wsaaTransAreaInner.wsaaTermid.set(varcom.vrcmTermid);
	wsaaTransAreaInner.wsaaPlnsfx.set(ZERO);
	wsaaTransAreaInner.wsaaSuppressTo.set(ZERO);
	wsaaTransAreaInner.wsaaCfiafiTranno.set(ZERO);
	wsaaTransAreaInner.wsaaBbldat.set(ZERO);
	wsaaTransAreaInner.wsaaCfiafiTranCode.set(SPACES);
	wsaaTransAreaInner.wsaaSupflag.set("N");
	wsaaTransAreaInner.wsaaReinstDate.set(wsaaReinstEffdate);
	wsaaTransAreaInner.wsaaTranCode.set(ptrnpf.getBatctrcde());
	wsaaTransAreaInner.wsaaTranNum.set(ptrnpf.getTranno());
	wsaaTransAreaInner.wsaaTodate.set(ptrnpf.getPtrneff());	
	wsaaTransAreaInner.wsaaTotalfee.set(wsaaReinstatementFee);
	/*atreqrec.transArea.set(wsaaTransAreaInner.wsaaTransArea);
	atreqrec.statuz.set(Varcom.oK);*/
	compute(wsaaNewTranno, 0).set(add(chdrpf.getTranno(), 1));	
	wsaaPayrBillcd.set(payrIO.getBillcd());
	
	for (Ptrnpf ptrnpf: ptrnList) {
		ptrnpfRev = new Ptrnpf(ptrnpf);
		if (isLT(ptrnpfRev.getTranno(), this.ptrnpf.getTranno())) {
			break;
			
		}
		readT6661(ptrnpfRev.getBatctrcde());
		if (isNE(t6661rec.contRevFlag, "N")) {
			callGenericProcessing(ptrnpfRev);							
			ptrnpfRev.setCrtuser(ptrnpfRev.getUsrprf());
			ptrnpfRev.setValidflag("2");
			if(ptrnUpList == null){
				ptrnUpList = new ArrayList<>();
			}
			ptrnUpList.add(ptrnpfRev);
				
		}
	}
}

protected void callGenericProcessing(Ptrnpf ptrnpfRev) {
	
	reverserec.chdrnum.set(zlreinpf.getChdrnum());
	reverserec.company.set(zlreinpf.getChdrcoy());
	reverserec.tranno.set(ptrnpfRev.getTranno());
	reverserec.ptrneff.set(ptrnpfRev.getPtrneff());
	reverserec.oldBatctrcde.set(ptrnpfRev.getBatctrcde());
	reverserec.language.set(bsscIO.getLanguage().toString());
	reverserec.newTranno.set(wsaaNewTranno);
	reverserec.effdate1.set(wsaaTransAreaInner.wsaaTodate);
	reverserec.effdate2.set(wsaaTransAreaInner.wsaaSuppressTo);
	reverserec.batchkey.set(batcdorrec.batchkey);
	reverserec.ptrnBatcpfx.set(ptrnpfRev.getBatcpfx());
	reverserec.ptrnBatccoy.set(ptrnpfRev.getBatccoy());
	reverserec.ptrnBatcbrn.set(ptrnpfRev.getBatcbrn());
	reverserec.ptrnBatcactyr.set(ptrnpfRev.getBatcactyr());
	reverserec.ptrnBatcactmn.set(ptrnpfRev.getBatcactmn());
	reverserec.ptrnBatctrcde.set(ptrnpfRev.getBatctrcde());
	reverserec.ptrnBatcbatch.set(ptrnpfRev.getBatcbatch());
	reverserec.transDate.set(wsaaTransAreaInner.wsaaTranDate);
	reverserec.transTime.set(wsaaTransAreaInner.wsaaTranTime);
	reverserec.user.set(wsaaTransAreaInner.wsaaUser);
	reverserec.termid.set(wsaaTransAreaInner.wsaaTermid);
	reverserec.planSuffix.set(wsaaTransAreaInner.wsaaPlnsfx);
	reverserec.bbldat.set(wsaaTransAreaInner.wsaaBbldat);
	reverserec.statuz.set(varcom.oK);
	if (isNE(t6661rec.subprog01, SPACES)) {
		callProgram(t6661rec.subprog01, reverserec.reverseRec);
		if (isNE(reverserec.statuz, varcom.oK)) {
			syserrrec.params.set(reverserec.reverseRec);
			syserrrec.statuz.set(reverserec.statuz);
			fatalError600();
		}
	}
	reverserec.statuz.set(varcom.oK);
	if (isNE(t6661rec.subprog02, SPACES)) {
		callProgram(t6661rec.subprog02, reverserec.reverseRec);
		if (isNE(reverserec.statuz, varcom.oK)) {
			syserrrec.params.set(reverserec.reverseRec);
			syserrrec.statuz.set(reverserec.statuz);
			fatalError600();
		}
	}
}

protected void processChdrPayr()
{	
	chdrpfRev = chdrpfDAO.getchdrRecord(zlreinpf.getChdrcoy(),zlreinpf.getChdrnum());//IJTI-1410
	payrRev = payrpfDAO.getpayrRecord(zlreinpf.getChdrcoy(),zlreinpf.getChdrnum());//IJTI-1410
	updateFields();
	rewrtChdr();
	rewrtPayr();
}

protected void updateFields()
{	
	if (isNE(payrRev.getBillcd(), wsaaPayrBillcd)) {
		updatePayrNextdate3300();
	}
}

protected void updatePayrNextdate3300()
{
	String key;
	if(BTPRO028Permission) {
		key = payrIO.getBillchnl().trim() + chdrpf.getCnttype().trim() + payrIO.getBillfreq().trim();
		if(readT6654(key, chdrpf.getOccdate())) {
			datcon2rec.intDate1.set(payrIO.getBillcd());
			datcon2rec.frequency.set("DY");
			compute(datcon2rec.freqFactor, 0).set(mult(t6654rec.leadDays, -1));
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);		
			payrRev.setNextdate(datcon2rec.intDate2.toInt());
		}
		else {
			key = payrIO.getBillchnl().trim().concat(chdrpf.getCnttype().trim()).concat("**");
			if(!readT6654(key, chdrpf.getOccdate())) {
				key = payrIO.getBillchnl().trim().concat("*****");
				if(!readT6654(key, chdrpf.getOccdate())) {
					syserrrec.params.set(wsaaT6654Item);
					syserrrec.statuz.set(varcom.mrnf);
					fatalError600();
				}
			}
		}
	}
	else {
		wsaaT6654Item.set(SPACES);
		wsaaT6654Billchnl.set(payrIO.getBillchnl());
		wsaaT6654Cnttype.set(chdrpf.getCnttype());
		readT6654(wsaaT6654Item.toString(),chdrpf.getOccdate());	
		if (readT6654(wsaaT6654Item.toString(),chdrpf.getOccdate())) {		
			datcon2rec.intDate1.set(payrIO.getBillcd());
			datcon2rec.frequency.set("DY");
			compute(datcon2rec.freqFactor, 0).set(mult(t6654rec.leadDays, -1));
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);		
			payrRev.setNextdate(datcon2rec.intDate2.toInt());
		}
		wsaaT6654Cnttype.set("***");
		if (!readT6654(wsaaT6654Item.toString(),chdrpf.getOccdate())) {	
			syserrrec.params.set(wsaaT6654Item);
			syserrrec.statuz.set(varcom.mrnf);
			fatalError600();
		}
	}
}

protected void rewrtChdr()
{	
	chdrpfRev.setTranno(wsaaNewTranno.toInt());
	chdrpfRev.setCurrto(varcom.vrcmMaxDate.toInt());
	varcom.vrcmDate.set(wsaaTransAreaInner.wsaaTranDate);
	varcom.vrcmTime.set(wsaaTransAreaInner.wsaaTranTime);
	varcom.vrcmUser.set(wsaaTransAreaInner.wsaaUser);
	varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
	varcom.vrcmCompTermid.set(wsaaTransAreaInner.wsaaTermid);
	chdrpfRev.setTranid(varcom.vrcmCompTranid.toString());
	if(chdrUpList == null)
		chdrUpList = new ArrayList<Chdrpf>();
	chdrUpList.add(chdrpfRev);
}

protected void rewrtPayr()
{
	
	payrRev.setTranno(wsaaNewTranno.toInt());
	payrRev.setTransactionDate(wsaaTransAreaInner.wsaaTranDate.toInt());
	payrRev.setTransactionTime(wsaaTransAreaInner.wsaaTranTime.toInt());
	payrRev.setTermid(wsaaTransAreaInner.wsaaTermid.toString());
	payrRev.setUser(wsaaTransAreaInner.wsaaUser.toInt());
	if(payrUpList == null)
		payrUpList = new ArrayList<Payrpf>();
	payrUpList.add(payrRev);
}

protected void housekeeping()
{	
	addNewPtrn();
	addNewBatch();	
	a100AddUnderwritting();	
}

protected void addNewPtrn()
{
	ptrnIns = new Ptrnpf();	
	ptrnIns.setTranno(wsaaNewTranno.toInt());
	ptrnIns.setPtrneff(wsaaReinstEffdate.toInt());
	ptrnIns.setDatesub(wsaaToday.toInt());
	ptrnIns.setChdrpfx(chdrpf.getChdrpfx());
	ptrnIns.setChdrcoy(chdrpf.getChdrcoy().toString());
	ptrnIns.setChdrnum(chdrpf.getChdrnum());
	ptrnIns.setTrdt(new BigDecimal(getCobolDate()).intValue());
	ptrnIns.setTrtm(wsaaTransAreaInner.wsaaTranTime.toInt());
	ptrnIns.setUserT(999999);
	ptrnIns.setTermid(wsaaTransAreaInner.wsaaTermid.toString());
	ptrnIns.setBatcpfx(batcdorrec.prefix.toString());
	ptrnIns.setBatccoy(batcdorrec.company.toString());
	ptrnIns.setBatcbrn(batcdorrec.branch.toString());
	ptrnIns.setBatcactyr(batcdorrec.actyear.toInt());
	ptrnIns.setBatctrcde(batcdorrec.trcde.toString());
	ptrnIns.setBatcactmn(batcdorrec.actmonth.toInt());
	ptrnIns.setBatcbatch(batcdorrec.batch.toString());
	ptrnIns.setValidflag("1");
	if(ptrnInsList == null)
		ptrnInsList = new ArrayList<Ptrnpf>();
	
	ptrnInsList.add(ptrnIns);
}

protected void addNewBatch()
{
	/*BATCH-HEADER*/
	batcuprec.function.set(varcom.writs);
	batcuprec.sub.set(ZERO);
	batcuprec.etreqcnt.set(ZERO);
	batcuprec.bcnt.set(ZERO);
	batcuprec.bval.set(ZERO);
	batcuprec.trancnt.set(ZERO);
	batcuprec.ascnt.set(ZERO);
	batcuprec.batcpfx.set("BA");
	batcuprec.batccoy.set(bsprIO.getCompany());
	batcuprec.batcbrn.set(batcdorrec.branch);
	batcuprec.batcactyr.set(bsscIO.getAcctYear());
	batcuprec.batcactmn.set(bsscIO.getAcctMonth());
	batcuprec.batctrcde.set(bprdIO.getAuthCode());
	batcuprec.batcbatch.set(batcdorrec.batch);
	callProgram(Batcup.class, batcuprec.batcupRec);
	if (isNE(batcuprec.statuz, varcom.oK)) {
		syserrrec.params.set(batcuprec.batcupRec);
		syserrrec.statuz.set(batcuprec.statuz);
		fatalError600();
	}
	/*EXIT*/
}

protected void a100AddUnderwritting()
{	
	lifepfList = lifepfDAO.getLifeList(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());//IJTI-1410
	if(lifepfList.size() == 0) {		
		syserrrec.params.set(chdrpf.getChdrcoy().toString()+chdrpf.getChdrnum());//IJTI-1410
		syserrrec.statuz.set(varcom.mrnf);
		fatalError600();
	}
	a200DelUnderwritting();
	
	for (Iterator<Covrpf> iterator = covrpfList.iterator(); iterator.hasNext(); ){
    	Covrpf covrpf = iterator.next();
		if (covrpf.getValidflag().equals("1") && covrpf.getChdrcoy().equals(zlreinpf.getChdrcoy())) 
		{
			initialize(crtundwrec.parmRec);
			crtundwrec.clntnum.set(lifeIO.getLifcnum());
			crtundwrec.coy.set(chdrpf.getChdrcoy());
			crtundwrec.chdrnum.set(chdrpf.getChdrnum());
			crtundwrec.life.set(covrpf.getLife());
			crtundwrec.crtable.set(covrpf.getCrtable());
			crtundwrec.batctrcde.set(bprdIO.getAuthCode());
			crtundwrec.sumins.set(covrpf.getSumins());
			crtundwrec.cnttyp.set(chdrpf.getCnttype());
			crtundwrec.currcode.set(chdrpf.getCntcurr());
			crtundwrec.function.set("ADD");
			callProgram(Crtundwrt.class, crtundwrec.parmRec);
			if (isNE(crtundwrec.status, varcom.oK)) {
				syserrrec.params.set(crtundwrec.parmRec);
				syserrrec.statuz.set(crtundwrec.status);
				syserrrec.iomod.set("CRTUNDWRT");
				fatalError600();
			}
        }        	
    }
	covtpfList = new ArrayList<Covtpf>();
	covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum()) ;//IJTI-1410
	if(covtpfList.size() > 0) {
		for (Covtpf covtpf : covtpfList) {
			initialize(crtundwrec.parmRec);
			crtundwrec.clntnum.set(lifeIO.getLifcnum());
			crtundwrec.coy.set(covtpf.getChdrcoy());
			crtundwrec.chdrnum.set(covtpf.getChdrnum());
			crtundwrec.life.set(covtpf.getLife());
			crtundwrec.crtable.set(covtpf.getCrtable());
			crtundwrec.batctrcde.set(bprdIO.getAuthCode());
			crtundwrec.sumins.set(covtpf.getSumins());
			crtundwrec.cnttyp.set(chdrpf.getCnttype());
			crtundwrec.currcode.set(chdrpf.getCntcurr());
			crtundwrec.function.set("ADD");
			callProgram(Crtundwrt.class, crtundwrec.parmRec);
			if (isNE(crtundwrec.status, varcom.oK)) {
				syserrrec.params.set(crtundwrec.parmRec);
				syserrrec.statuz.set(crtundwrec.status);
				syserrrec.iomod.set("CRTUNDWRT");
				fatalError600();
			}
		}
	}

}

protected void a200DelUnderwritting()
{
	initialize(crtundwrec.parmRec);
	crtundwrec.clntnum.set(lifeIO.getLifcnum());
	crtundwrec.coy.set(chdrpf.getChdrcoy());
	crtundwrec.currcode.set(chdrpf.getCntcurr());
	crtundwrec.chdrnum.set(chdrpf.getChdrnum());
	crtundwrec.cnttyp.set(chdrpf.getCnttype());
	crtundwrec.crtable.fill("*");
	crtundwrec.function.set("DEL");
	callProgram(Crtundwrt.class, crtundwrec.parmRec);
	if (isNE(crtundwrec.status, varcom.oK)) {
		syserrrec.params.set(crtundwrec.parmRec);
		syserrrec.statuz.set(crtundwrec.status);
		syserrrec.iomod.set("CRTUNDWRT");
		fatalError600();
	}
}



protected void a000Statistics()
{	
	lifsttrrec.batccoy.set(batcdorrec.company.toString());
	lifsttrrec.batcbrn.set(batcdorrec.branch.toString());
	lifsttrrec.batcactyr.set(batcdorrec.actyear.toInt());
	lifsttrrec.batcactmn.set(batcdorrec.actmonth.toInt());
	lifsttrrec.batctrcde.set(batcdorrec.trcde.toString());
	lifsttrrec.batcbatch.set(batcdorrec.batch.toString());
	lifsttrrec.chdrcoy.set(chdrpf.getChdrcoy());
	lifsttrrec.chdrnum.set(chdrpf.getChdrnum());
	/* MOVE WSAA-TRANNO            TO LIFS-TRANNO.             <005>*/
	lifsttrrec.tranno.set(wsaaNewTranno);
	/* MOVE PTRNREV-TRANNO         TO LIFS-TRANNOR.            <003>*/
	lifsttrrec.trannor.set(wsaaTransAreaInner.wsaaTranNum);
	lifsttrrec.agntnum.set(SPACES);
	lifsttrrec.oldAgntnum.set(SPACES);
	callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
	if (isNE(lifsttrrec.statuz, varcom.oK)) {
		syserrrec.params.set(lifsttrrec.lifsttrRec);
		syserrrec.statuz.set(lifsttrrec.statuz);
		fatalError600();
	}
}


protected void calcReintFeeTax()
{	
	if ((setPrecision(ZERO, 2)
	&& isEQ(wsaaReinstatementFee, ZERO))
	|| isEQ(tr52drec.txcode, SPACES)) {
		return;
	}
	/* Calculate tax on the Reinstatement fee                               */
	/* Read table TR52E                                                */
	
	sbTr52eKey = new StringBuilder("");
	sbTr52eKey.append(tr52drec.txcode.toString().trim());
	sbTr52eKey.append(chdrpf.getCnttype().trim());//IJTI-1410
	sbTr52eKey.append("****");	
	boolean itemFound = readTr52e(sbTr52eKey.toString(), chdrpf.getOccdate());
	if (!itemFound){
		sbTr52eKey = new StringBuilder("");
		sbTr52eKey.append(tr52drec.txcode.toString().trim());
		sbTr52eKey.append("***");
		sbTr52eKey.append("****");	
		itemFound = readTr52e(sbTr52eKey.toString(), chdrpf.getOccdate());
		if (!itemFound){
			syserrrec.params.set(sbTr52eKey);
			fatalError600();
		}
	}		
	gotTr52e();
}

protected void gotTr52e()
{
	if (isNE(tr52erec.taxind12, "Y")) {
		return ;
	}
	/* Call TR52D tax subroutine                                       */
	txcalcrec.function.set("CALC");
	txcalcrec.statuz.set(Varcom.oK);
	txcalcrec.chdrcoy.set(zlreinpf.getChdrcoy());
	txcalcrec.chdrnum.set(zlreinpf.getChdrnum());
	txcalcrec.life.set(SPACES);
	txcalcrec.coverage.set(SPACES);
	txcalcrec.rider.set(SPACES);
	txcalcrec.planSuffix.set(0);
	txcalcrec.crtable.set(SPACES);
	txcalcrec.cnttype.set(chdrpf.getCnttype());
	txcalcrec.register.set(chdrpf.getReg());
	txcalcrec.txcode.set(tr52drec.txcode);
	txcalcrec.transType.set("RSTF");
	txcalcrec.ccy.set(chdrpf.getCntcurr());
	txcalcrec.effdate.set(wsaaReinstEffdate);
	txcalcrec.taxrule.set(sbTr52eKey);
	txcalcrec.rateItem.set(SPACES);
	stringVariableReinstFee = new StringUtil();
	stringVariableReinstFee.addExpression(chdrpf.getCntcurr());
	stringVariableReinstFee.addExpression(tr52erec.txitem);
	stringVariableReinstFee.setStringInto(txcalcrec.rateItem);
	compute(txcalcrec.amountIn, 2).set(wsaaReinstatementFee);
	txcalcrec.tranno.set(ZERO);
	txcalcrec.taxType[1].set(SPACES);
	txcalcrec.taxType[2].set(SPACES);
	txcalcrec.taxAmt[1].set(ZERO);
	txcalcrec.taxAmt[2].set(ZERO);
	txcalcrec.taxAbsorb[1].set(SPACES);
	txcalcrec.taxAbsorb[2].set(SPACES);
	callProgram(tr52drec.txsubr, txcalcrec.linkRec);
	if (isNE(txcalcrec.statuz, Varcom.oK)) {
		syserrrec.statuz.set(txcalcrec.statuz);
		fatalError600();
	}
	if (isGT(txcalcrec.taxAmt[1], ZERO)
	|| isGT(txcalcrec.taxAmt[2], ZERO)) {
		if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
			wsaaFeeTax.add(txcalcrec.taxAmt[1]);			
		}
		if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
			wsaaFeeTax.add(txcalcrec.taxAmt[2]);			
		}
	}
	taxdIO = new Taxdpf();
	taxdIO.setTaxamt01(txcalcrec.taxAmt[1].getbigdata());
	taxdIO.setTaxamt02(txcalcrec.taxAmt[2].getbigdata());
	taxdIO.setTaxamt03(new BigDecimal(0));
	taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1].toString());
	taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2].toString());
	taxdIO.setTxabsind03(SPACES.toString());
	taxdIO.setTxtype01(txcalcrec.taxType[1].toString());
	taxdIO.setTxtype02(txcalcrec.taxType[2].toString());
	taxdIO.setTxtype03(SPACES.toString());
}
protected void calcTolerance()
{	
	
	wsaaFreqMatch.set(SPACES);
	for (wsaaCnt.set(1); !(isGT(wsaaCnt,11)
	|| freqMatch.isTrue()); wsaaCnt.add(1)){
		if (isEQ(t5667rec.freq[wsaaCnt.toInt()],payrIO.getBillfreq())) {
			freqMatch.setTrue();
			compute(wsaaCnt, 0).set(sub(wsaaCnt,1));
		}
	}
	if (freqMatch.isTrue()) {
		wsaaTolerance.set(payrIO.getSinstamt06());
		wsaaTolerance.set(wsaaToamount);
		compute(wsaaTolerance, 2).set(div(mult(wsaaTolerance,t5667rec.prmtol[wsaaCnt.toInt()]),100));
		zrdecplrec.amountIn.set(wsaaTolerance);
		zrdecplrec.currency.set(payrIO.getBillcurr());
		callRounding8000();
		wsaaTolerance.set(zrdecplrec.amountOut);
		if (isGT(wsaaTolerance,t5667rec.maxAmount[wsaaCnt.toInt()])) {
			wsaaTolerance.set(t5667rec.maxAmount[wsaaCnt.toInt()]);
		}
		if (isNE(t5667rec.maxamt[wsaaCnt.toInt()],0)) {
			if (agtNotTerminated.isTrue()
			|| (agtTerminated.isTrue()
			&& isEQ(t5667rec.sfind,"2"))) {
				wsaaTolerance2.set(payrIO.getSinstamt06());
				wsaaTolerance2.set(wsaaToamount);
				compute(wsaaTolerance2, 2).set(div(mult(wsaaTolerance2,t5667rec.prmtoln[wsaaCnt.toInt()]),100));
				zrdecplrec.amountIn.set(wsaaTolerance2);
				zrdecplrec.currency.set(payrIO.getBillcurr());
				callRounding8000();
				wsaaTolerance2.set(zrdecplrec.amountOut);
				if (isGT(wsaaTolerance2,t5667rec.maxamt[wsaaCnt.toInt()])) {
					wsaaTolerance2.set(t5667rec.maxamt[wsaaCnt.toInt()]);
				}
			}
		}
	}	
}

protected void checkAgent()
{
	wsaaAgtTerminateFlag.set("N");
	aglfIO.setDataKey(SPACES);
	aglfIO.setAgntcoy(chdrpf.getAgntcoy());
	aglfIO.setAgntnum(chdrpf.getAgntnum());
	aglfIO.setFormat(aglfrec);
	aglfIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, aglfIO);
	if (isNE(aglfIO.getStatuz(),Varcom.oK)) {
		syserrrec.statuz.set(aglfIO.getStatuz());
		syserrrec.params.set(aglfIO.getParams());
		fatalError600();
	}
	if (isLT(aglfIO.getDtetrm(),datcon1rec.intDate)
	|| isLT(aglfIO.getDteexp(),datcon1rec.intDate)
	|| isGT(aglfIO.getDteapp(),datcon1rec.intDate)) {
		wsaaAgtTerminateFlag.set("Y");
	}
}


protected void nlgflg3020() {
	ptrnpfNlg= ptrnpfDAO.getPtrnData(zlreinpf.getChdrcoy(),zlreinpf.getChdrnum(),bprdIO.getAuthCode().toString());//IJTI-1410
	if(ptrnpfNlg != null && ptrnpfNlg.getTranno()!=null)
		nlgcalcrec.tranno.set(ptrnpfNlg.getTranno());
	nlgcalcrec.fsuco.set(wsaaTransAreaInner.wsaaFsuCoy);
	nlgcalcrec.chdrcoy.set(zlreinpf.getChdrcoy());
	nlgcalcrec.chdrnum.set(zlreinpf.getChdrnum());
	nlgcalcrec.effdate.set(wsaaToday);
	nlgcalcrec.batctrcde.set(bprdIO.getAuthCode());
	nlgcalcrec.frmdate.set(chdrpf.getOccdate());
	nlgcalcrec.todate.set(varcom.maxdate);
	nlgcalcrec.cnttype.set(chdrpf.getCnttype());
	nlgcalcrec.language.set(bsscIO.getLanguage().toString());
	nlgcalcrec.Cntcurr.set(chdrpf.getCntcurr());
	nlgcalcrec.billchnl.set(chdrpf.getBillchnl());
	nlgcalcrec.billfreq.set(chdrpf.getBillfreq());
	nlgcalcrec.occdate.set(chdrpf.getOccdate());
	nlgcalcrec.function.set("LINST");
	nlgcalcrec.status.set(Varcom.oK);
	callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec);
	if(isNE(nlgcalcrec.status,varcom.oK)) {
		syserrrec.statuz.set(aglfIO.getStatuz());
		syserrrec.params.set(aglfIO.getParams());
		fatalError600();
	}
}
protected void putTaxdEntry()
{
	taxdIO.setChdrcoy(zlreinpf.getChdrcoy());
	taxdIO.setChdrnum(zlreinpf.getChdrnum());
	taxdIO.setTrantype("RSTF");
	taxdIO.setLife(SPACES.toString());
	taxdIO.setCoverage(SPACES.toString());
	taxdIO.setRider(SPACES.toString());
	taxdIO.setPlansfx(ZERO.intValue());
	taxdIO.setEffdate(wsaaReinstEffdate.toInt());
	taxdIO.setInstfrom(varcom.vrcmMaxDate.toInt());
	taxdIO.setBillcd(varcom.vrcmMaxDate.toInt());
	taxdIO.setInstto(varcom.vrcmMaxDate.toInt());
	taxdIO.setTranno(chdrpf.getTranno());
	taxdIO.setBaseamt(wsaaReinstatementFee.getbigdata());	
	taxdIO.setTranref(stringVariableReinstFee.toString())	;
	taxdIO.setPostflg("P");
	taxdInsertList.add(taxdIO);
}

protected void postFee()
{	
	 descpfT1688 = new Descpf();
	 if (t1688Map.containsKey(bprdIO.getAuthCode())) {
         descpfT1688 = t1688Map.get(bprdIO.getAuthCode());
     } else {        
         descpfT1688.setLongdesc("");
     }	
	lifrtrnrec2.batckey.set(batcdorrec.batchkey);
	lifrtrnrec2.rdocnum.set(zlreinpf.getChdrnum());
	lifrtrnrec2.rldgacct.set(zlreinpf.getChdrnum());
	lifrtrnrec2.tranno.set(wsaaNewTranno);
	lifrtrnrec2.jrnseq.set(0);
	lifrtrnrec2.crate.set(0);
	lifrtrnrec2.acctamt.set(0);
	lifrtrnrec2.rcamt.set(0);
	lifrtrnrec2.rldgcoy.set(zlreinpf.getChdrcoy());
	lifrtrnrec2.origcurr.set(chdrpf.getBillcurr());
	lifrtrnrec2.origamt.set(wsaaTransAreaInner.wsaaTotalfee);
	lifrtrnrec2.origamt.add(wsaaFeeTax);
	lifrtrnrec2.trandesc.set(descpfT1688);
	lifrtrnrec2.user.set(wsaaTransAreaInner.wsaaUser);
	lifrtrnrec2.termid.set(wsaaTransAreaInner.wsaaTermid);
	lifrtrnrec2.transactionDate.set(wsaaTransAreaInner.wsaaTranDate);
	lifrtrnrec2.transactionTime.set(wsaaTransAreaInner.wsaaTranTime);
	lifrtrnrec2.genlcur.set(SPACES);
	lifrtrnrec2.genlcoy.set(zlreinpf.getChdrcoy());
	lifrtrnrec2.sacscode.set(t5645rec.sacscode01);
	lifrtrnrec2.sacstyp.set(t5645rec.sacstype01);
	lifrtrnrec2.glcode.set(t5645rec.glmap01);
	lifrtrnrec2.glsign.set(t5645rec.sign01);
	lifrtrnrec2.contot.set(t5645rec.cnttot01);
	lifrtrnrec2.postyear.set(SPACES);
	lifrtrnrec2.postmonth.set(SPACES);
	lifrtrnrec2.effdate.set(wsaaTransAreaInner.wsaaReinstDate);
	lifrtrnrec2.frcdate.set(varcom.vrcmMaxDate);
	lifrtrnrec2.substituteCode[1].set(chdrpf.getCnttype());
	lifrtrnrec2.function.set("PSTW");
	callProgram(Lifrtrn.class, lifrtrnrec2.lifrtrnRec);
	if (isNE(lifrtrnrec2.statuz,varcom.oK)) {
		syserrrec.statuz.set(lifrtrnrec2.statuz);
		syserrrec.params.set(lifrtrnrec2.lifrtrnRec);
		fatalError600();
	}
	lifacmvrec1.batckey.set(batcdorrec.batchkey);
	lifacmvrec1.rldgcoy.set(zlreinpf.getChdrcoy());
	lifacmvrec1.genlcoy.set(zlreinpf.getChdrcoy());
	lifacmvrec1.rdocnum.set(zlreinpf.getChdrnum());
	lifacmvrec1.rldgacct.set(zlreinpf.getChdrnum());
	lifacmvrec1.tranref.set(zlreinpf.getChdrnum());
	lifacmvrec1.tranno.set(wsaaNewTranno);
	lifacmvrec1.jrnseq.set(1);
	lifacmvrec1.crate.set(0);
	lifacmvrec1.acctamt.set(0);
	lifacmvrec1.rcamt.set(0);
	lifacmvrec1.origcurr.set(chdrpf.getBillcurr());
	lifacmvrec1.origamt.set(wsaaTransAreaInner.wsaaTotalfee); 
	lifacmvrec1.origamt.add(wsaaFeeTax);
	lifacmvrec1.trandesc.set(descpfT1688);
	lifacmvrec1.user.set(wsaaTransAreaInner.wsaaUser);
	lifacmvrec1.termid.set(wsaaTransAreaInner.wsaaTermid);
	lifacmvrec1.transactionDate.set(wsaaTransAreaInner.wsaaTranDate);
	lifacmvrec1.transactionTime.set(wsaaTransAreaInner.wsaaTranTime);
	lifacmvrec1.genlcur.set(SPACES);
	datcon2rec.datcon2Rec.set(SPACES);
	datcon2rec.intDate1.set(chdrpf.getOccdate());
	datcon2rec.freqFactor.set(1);
	datcon2rec.frequency.set("01");
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isNE(datcon2rec.statuz,varcom.oK)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		syserrrec.params.set(datcon2rec.datcon2Rec);
		fatalError600();
	}
	if (isLT(wsaaTransAreaInner.wsaaReinstDate, datcon2rec.intDate2)) {
		lifacmvrec1.sacscode.set(t5645rec.sacscode02);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec1.glcode.set(t5645rec.glmap02);
		lifacmvrec1.glsign.set(t5645rec.sign02);
		lifacmvrec1.contot.set(t5645rec.cnttot02);
	}
	else {
		lifacmvrec1.sacscode.set(t5645rec.sacscode03);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec1.glcode.set(t5645rec.glmap03);
		lifacmvrec1.glsign.set(t5645rec.sign03);
		lifacmvrec1.contot.set(t5645rec.cnttot03);
	}
	lifacmvrec1.postyear.set(SPACES);
	lifacmvrec1.postmonth.set(SPACES);
	lifacmvrec1.effdate.set(wsaaTransAreaInner.wsaaReinstDate);
	lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec1.substituteCode[1].set(chdrpf.getCnttype());
	lifacmvrec1.function.set("PSTW");
	callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
	if (isNE(lifacmvrec1.statuz,varcom.oK)) {
		syserrrec.statuz.set(lifacmvrec1.statuz);
		syserrrec.params.set(lifacmvrec1.lifacmvRec);
		fatalError600();
	}
}

protected void writeLetc()
{
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(chdrpf.getCnttype());
	stringVariable1.addExpression(bprdIO.getAuthCode().toString());
	 if (tr384ListMap.containsKey(stringVariable1.toString().trim())) {
         tr384rec.tr384Rec.set(StringUtil.rawToString(tr384ListMap.get(stringVariable1.toString().trim()).get(0).getGenarea()));
     } else {
    	stringVariable1 = new StringUtil();
		stringVariable1.addExpression("***");
		stringVariable1.addExpression(bprdIO.getAuthCode().toString());
         if (tr384ListMap.containsKey(stringVariable1.toString().trim())) {
             tr384rec.tr384Rec.set(StringUtil.rawToString(tr384ListMap.get(stringVariable1.toString().trim()).get(0).getGenarea()));
         } else {
             syserrrec.params.set("tr384:" + stringVariable1.toString().trim());
             fatalError600();
         }
     }	
	letrqstrec.statuz.set(SPACES);
	letrqstrec.letterType.set(tr384rec.letterType);
	letrqstrec.letterRequestDate.set(wsaaToday);
	letrqstrec.clntcoy.set(chdrpf.getCowncoy());
	letrqstrec.clntnum.set(chdrpf.getCownnum());
	letrqstrec.rdocpfx.set(chdrpf.getChdrpfx());
	letrqstrec.requestCompany.set(zlreinpf.getChdrcoy());
	letrqstrec.chdrcoy.set(zlreinpf.getChdrcoy());
	letrqstrec.rdoccoy.set(zlreinpf.getChdrcoy());
	letrqstrec.rdocnum.set(zlreinpf.getChdrnum());
	letrqstrec.chdrnum.set(zlreinpf.getChdrnum());
	letrqstrec.tranno.set(chdrpf.getTranno());
	letrqstrec.despnum.set(chdrpf.getDespnum());
	letrqstrec.branch.set(chdrpf.getCntbranch());
	letcokcpy.recCode.set("LD");
	letcokcpy.ldDate.set(wsaaTransAreaInner.wsaaReinstDate);
	letrqstrec.otherKeys.set(letcokcpy.saveOtherKeys);
	letrqstrec.trcde.set(bprdIO.getAuthCode());
	letrqstrec.function.set("ADD");
	callProgram(Letrqst.class, letrqstrec.params);
	if (isNE(letrqstrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(letrqstrec.statuz);
		syserrrec.params.set(letrqstrec.params);
		fatalError600();
	}
}



protected void releaseSoftlock()
{
	
	/* Release the soft lock on the contract.*/
	sftlockrec.company.set(batcdorrec.company);
	sftlockrec.entity.set(zlreinpf.getChdrnum());
	sftlockrec.enttyp.set("CH");
	sftlockrec.user.set(999999);
	sftlockrec.transaction.set(bprdIO.getAuthCode());
	sftlockrec.statuz.set(SPACES);
	sftlockrec.function.set("UNLK");
	callProgram(Sftlock.class, sftlockrec.sftlockRec);
	if (isNE(sftlockrec.statuz, varcom.oK)) {
		syserrrec.params.set(sftlockrec.sftlockRec);
		syserrrec.statuz.set(sftlockrec.statuz);
		fatalError600();
	}
}

protected void commit3500(){
	if(!noRecordFound) {
		if(zlreinpfUpList != null && !zlreinpfUpList.isEmpty())
		{
			zlreinpfDAO.bulkUpdateZlreinpf(zlreinpfUpList);
			if(RPTTempData == null)
				RPTTempData = new ArrayList<Zlreinpf>();
			RPTTempData.addAll(zlreinpfUpList);
			zlreinpfUpList.clear();
		}
		if(RPTTempData != null && !RPTTempData.isEmpty()) {
			
			bd5e4DAO.populateBIReportTmp(RPTTempData,bsscIO.getScheduleNumber().toInt());
			RPTTempData.clear();
		}
		if(zlreinpfDelList != null && !zlreinpfDelList.isEmpty())
		{
			  
			//Also Delete existing records if anything available in ZLREINPF is crossed lapse days mentioned in TD5E6			
			
			zlreinpfDAO.bulkDeleteZlreinpf(zlreinpfDelList);			
			zlreinpfDelList.clear();
			
		} 
		if(taxdInsertList != null && !taxdInsertList.isEmpty())
			if(!taxdpfDAO.insertTaxdPF(taxdInsertList))
			{
				LOGGER.error("Update TAXDPF record failed.");
				fatalError600();
			}else taxdInsertList.clear();
		if(ptrnUpList != null && !ptrnUpList.isEmpty()){
			if(!ptrnpfDAO.updatePtrnRecReversal(ptrnUpList))
			{
				LOGGER.error("Update PTRNPF record failed.");
				fatalError600();
			}else ptrnUpList.clear();
			
		}
		if(chdrUpList != null && !chdrUpList.isEmpty()){
			if(!chdrpfDAO.updateChdrRecReversal(chdrUpList))
			{
				LOGGER.error("Update CHDRPF record failed.");
				fatalError600();
			}else chdrUpList.clear();
			
		}		
		if(payrUpList != null && !payrUpList.isEmpty()){
			payrpfDAO.updatePayrTranno(payrUpList);			
		}
		if(ptrnInsList != null && !ptrnInsList.isEmpty()){
			if(!ptrnpfDAO.insertPtrnPF(ptrnInsList))
			{
				LOGGER.error("iNSERT PTRNPF record failed.");
				fatalError600();
			}else ptrnInsList.clear();
			
		}
	}
}

protected void rollback3600(){
	
}

protected void close4000(){	
		if(!noRecordFound) {			
				
			th565ListMap.clear();
			th565ListMap = null;
			
			td5e6ListMap.clear();
			td5e6ListMap = null;
			
			t6661ListMap.clear();
			t6661ListMap = null;
			
			t5645ListMap.clear();
			t5645ListMap = null;
			
			t3695ListMap.clear();
			t3695ListMap = null;
			
			th614ListMap.clear();
			th614ListMap = null;
			
			tr52dListMap.clear();
			tr52dListMap = null;
			
			t5667ListMap.clear();
			
			t5667ListMap = null;		
			
			tr384ListMap.clear();
			tr384ListMap = null;
			
			t1688Map.clear();
			t1688Map = null;
			
			acblListMap.clear();
			acblListMap = null;

			lextpfMap.clear();
			lextpfMap=null;
			
			covrListMap.clear();
			covrListMap = null;
			
			payrListMap.clear();
			payrListMap = null;
			
			zlreinpfUpList.clear();
			zlreinpfUpList = null;
			
			zlreinpfDelList.clear();
			zlreinpfDelList = null;
			
			zlreinpfList.clear();
			zlreinpfList = null;
			
			lsaaStatuz.set(varcom.oK);
		}
}




protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}


private static final class ControlTotalsInner {
		/* CONTROL-TOTALS */
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
}

private static final class WsaaTransAreaInner { 

	private FixedLengthStringData wsaaTransArea = new FixedLengthStringData(95);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransArea, 45).setUnsigned();
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 49);
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransArea, 62).setUnsigned();
	private FixedLengthStringData wsaaReinstDate = new FixedLengthStringData(8).isAPartOf(wsaaTransArea, 70);
	private ZonedDecimalData wsaaTotalfee = new ZonedDecimalData(17, 2).isAPartOf(wsaaTransArea, 78);
}


}
