package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:41
 * Description:
 * Copybook name: MATPCPY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Matpcpy extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData maturityRec = new FixedLengthStringData(getMaturityRecSize());
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(maturityRec, 0);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(maturityRec, 1);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(maturityRec, 9);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(maturityRec, 11);
  	public PackedDecimalData planSuffix = new PackedDecimalData(5, 0).isAPartOf(maturityRec, 13);
  	public FixedLengthStringData fund = new FixedLengthStringData(4).isAPartOf(maturityRec, 16);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(maturityRec, 20);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(maturityRec, 22);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(maturityRec, 24);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(maturityRec, 28);
  	public PackedDecimalData estimatedVal = new PackedDecimalData(17, 2).isAPartOf(maturityRec, 33);
  	public PackedDecimalData actualVal = new PackedDecimalData(17, 2).isAPartOf(maturityRec, 42);
  	public PackedDecimalData percreqd = new PackedDecimalData(5, 2).isAPartOf(maturityRec, 51).setUnsigned();
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(maturityRec, 54);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(maturityRec, 57);
  	public FixedLengthStringData element = new FixedLengthStringData(1).isAPartOf(maturityRec, 60);
  	public FixedLengthStringData type = new FixedLengthStringData(1).isAPartOf(maturityRec, 61);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(maturityRec, 62);
  	public FixedLengthStringData batckey = new FixedLengthStringData(64).isAPartOf(maturityRec, 63);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(maturityRec, 127);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(maturityRec, 130);
  	public ZonedDecimalData date_var = new ZonedDecimalData(6, 0).isAPartOf(maturityRec, 134).setUnsigned();
  	public ZonedDecimalData time = new ZonedDecimalData(6, 0).isAPartOf(maturityRec, 140).setUnsigned();
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(maturityRec, 146).setUnsigned();
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(maturityRec, 152);
  	public FixedLengthStringData status = new FixedLengthStringData(4).isAPartOf(maturityRec, 155);


	public void initialize() {
		COBOLFunctions.initialize(maturityRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		maturityRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


	public int getMaturityRecSize()
	{
		return 159;
	}
}