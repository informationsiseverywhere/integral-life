package com.csc.life.terminationclaims.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:11
 * Description:
 * Copybook name: TH548REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th548rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th548Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData divdParticipant = new FixedLengthStringData(1).isAPartOf(th548Rec, 0);
  	public FixedLengthStringData puCvCode = new FixedLengthStringData(4).isAPartOf(th548Rec, 1);
  	public FixedLengthStringData zdivopt = new FixedLengthStringData(4).isAPartOf(th548Rec, 5);
  	public FixedLengthStringData filler = new FixedLengthStringData(491).isAPartOf(th548Rec, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th548Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th548Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}