package com.csc.life.terminationclaims.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:16
 * Description:
 * Copybook name: PUPCOMPREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pupcomprec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData pupcompRec = new FixedLengthStringData(39);
  	public FixedLengthStringData function = new FixedLengthStringData(4).isAPartOf(pupcompRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(pupcompRec, 4);
  	public ZonedDecimalData sumin = new ZonedDecimalData(9, 0).isAPartOf(pupcompRec, 8).setUnsigned();
  	public ZonedDecimalData newinsval = new ZonedDecimalData(12, 2).isAPartOf(pupcompRec, 17).setUnsigned();
  	public ZonedDecimalData pufee = new ZonedDecimalData(9, 2).isAPartOf(pupcompRec, 29).setUnsigned();
  	public FixedLengthStringData ovrsuma = new FixedLengthStringData(1).isAPartOf(pupcompRec, 38);


	public void initialize() {
		COBOLFunctions.initialize(pupcompRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		pupcompRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}