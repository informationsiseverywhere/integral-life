package com.csc.life.terminationclaims.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sd5hqScreenVars  extends SmartVarModel {
	public FixedLengthStringData dataArea = new FixedLengthStringData(132);
	public FixedLengthStringData dataFields = new FixedLengthStringData(62).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,14);
    public FixedLengthStringData calBasis = DD.item.copy().isAPartOf(dataFields, 44);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 52);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData calBasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 72);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] calBasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public LongData Sd5hqscreenWritten = new LongData(0);
	public LongData Sd5hqprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sd5hqScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, item, tabl, longdesc, calBasis};
		screenOutFields = new BaseData[][] {companyOut, itemOut, tablOut, longdescOut, calBasisOut};
		screenErrFields = new BaseData[] {companyErr, itemErr, tablErr, longdescErr, calBasisErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sd5hqscreen.class;
		protectRecord = Sd5hqprotect.class;
	}

}
