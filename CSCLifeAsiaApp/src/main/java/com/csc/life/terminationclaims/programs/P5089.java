/*
 * File: P5089.java
 * Date: 30 August 2009 0:06:24
 * Author: Quipoz Limited
 * 
 * Class transformed from P5089.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.terminationclaims.dataaccess.ChdrptsTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  P5089 - Part Surrender AT Submission Module.
*  --------------------------------------------
*
*  This program is part  of the Part  Surrender  suite  and  is
*  used for program switching, there is no screen attached.
*
*  Initialise
*  ----------
*
*  RETRV the Contract Header record.
*
*  Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
*  Call  the  AT   submission module to submit P6307AT, passing
*  the  Contract    number,  as  the  'primary    key',    this
*  performs  the Part Surrender transaction.
*
*
*****************************************************************
* </pre>
*/
public class P5089 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5089");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData filler1 = new FixedLengthStringData(183).isAPartOf(wsaaTransactionRec, 17, FILLER).init(SPACES);
		/* FORMATS */
	private String chdrptsrec = "CHDRPTSREC";
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Atreqrec atreqrec = new Atreqrec();
		/*Contract Header Details - Part Surr.*/
	private ChdrptsTableDAM chdrptsIO = new ChdrptsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Batckey wsaaBatckey = new Batckey();

	public P5089() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		chdrptsIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrptsIO);
		if (isNE(chdrptsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		chdrptsIO.setFormat(chdrptsrec);
		chdrptsIO.setFunction("WRITS");
		SmartFileCode.execute(appVars, chdrptsIO);
		if (isNE(chdrptsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrptsIO.getParams());
			fatalError600();
		}
	}

protected void screenEdit2000()
	{
		/*GO*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		go3010();
	}

protected void go3010()
	{
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrptsIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P6307AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrptsIO.getChdrnum());
		wsaaFsuco.set(wsspcomn.fsuco);
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
