package com.csc.life.terminationclaims.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN SJL51
 * @version 1.0 generated on 20/07/06 05:41
 */
public class Sjl51screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl51ScreenVars sv = (Sjl51ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl51screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl51ScreenVars screenVars = (Sjl51ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.windowtype.setClassString("");
		screenVars.reqntype.setClassString("");
		screenVars.claimnmber.setClassString("");
		screenVars.paymentNum.setClassString("");
		screenVars.action.setClassString("");
		screenVars.bankcode.setClassString("");
	}

/**
 * Clear all the variables in Sjl51screen
 */
	public static void clear(VarModel pv) {
		Sjl51ScreenVars screenVars = (Sjl51ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.windowtype.clear();
		screenVars.reqntype.clear();
		screenVars.claimnmber.clear();
		screenVars.paymentNum.clear();
		screenVars.action.clear();
		screenVars.bankcode.clear();
	}
}