package com.csc.life.terminationclaims.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * 
 * @author hxu32
 * DAOImpl related table NOTIPF
 *
 */
public class NotipfDAOImpl extends BaseDAOImpl<Notipf> implements NotipfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(NotipfDAOImpl.class);

	@Override
	public boolean insertNotipf(Notipf notipf) {
		StringBuilder sb = new StringBuilder("INSERT INTO NOTIPF(NOTIFICOY,NOTIFINUM,NOTIFICNUM,CLAIMANT,RELATIONCNUM,NOTIFIDATE,INCURDATE,LOCATION,INCIDENTT,DEATHDATE,CAUSEDEATH,RGPYTYPE,CLAIMAT,DOCTOR,MEDICALPD,HOSPITALLE,DIAGNOSIS,ADMINTDATE,DISCHARGEDATE,ACCIDENTDESC,NOTIFISTATUS,CHDRNUM,VALIDFLAG,TRANSNO,USRPRF,JOBNM, DATIME) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
        boolean result = false;
		try {
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, notipf.getNotificoy());
			ps.setString(2, notipf.getNotifinum());
			ps.setString(3, notipf.getNotificnum());
			ps.setString(4, notipf.getClaimant());
			ps.setString(5, notipf.getRelationcnum());
			ps.setInt(6, notipf.getNotifidate());
			ps.setInt(7, notipf.getIncurdate());
			ps.setString(8, notipf.getLocation());
			ps.setString(9, notipf.getIncidentt());
			ps.setInt(10, notipf.getDeathdate());
			ps.setString(11, notipf.getCausedeath());
			ps.setString(12, notipf.getRclaimreason());
			ps.setDouble(13, notipf.getClaimat());
			ps.setString(14, notipf.getDoctor());
			ps.setString(15, notipf.getMedicalpd());
			ps.setString(16, notipf.getHospitalle());
			ps.setString(17, notipf.getDiagnosis());
			ps.setInt(18, notipf.getAdmintdate());
			ps.setInt(19, notipf.getDischargedate());
			ps.setString(20, notipf.getAccidentdesc());
			ps.setString(21, notipf.getNotifistatus());
			ps.setString(22, notipf.getChdrnum());
			ps.setString(23, notipf.getValidFlag());
			ps.setString(24, notipf.getTransno());
			ps.setString(25, getUsrprf());
			ps.setString(26, getJobnm());		
			ps.setTimestamp(27, getDatime());
			ps.executeUpdate();
			result = true;
		}catch(SQLException e) {
			LOGGER.error("insertNotipf()", e); /* IJTI-1479 */	
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);			
		}
		
		return result;
	}

	@Override
	public int getCountByLifeAssNum(String notificnum) {

		int count=0;
		StringBuilder sqlClntSelect1 = new StringBuilder(
                "SELECT COUNT(1) ");
        sqlClntSelect1.append("FROM NOTIPF WHERE NOTIFICNUM = ?");
      
        ResultSet sqlclntpf1rs = null;
        PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
        try {
        	psClntSelect.setString(1, notificnum.trim());
            sqlclntpf1rs = executeQuery(psClntSelect);

            if (sqlclntpf1rs.next()) {
            	count=sqlclntpf1rs.getInt(1);
            	
            }
        } catch (SQLException e) {
			LOGGER.error("getCountByLifeAssNum()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psClntSelect, sqlclntpf1rs);
        }
        return count;	
		
	}

	@Override
	public String getMaxNotifinum() {
		
		String notifinum="";
		StringBuilder sqlClntSelect1 = new StringBuilder(
                "SELECT MAX(NOTIFINUM) ");
        sqlClntSelect1.append("FROM NOTIPF ");
      
        ResultSet sqlclntpf1rs = null;
        PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
        try {
            sqlclntpf1rs = executeQuery(psClntSelect);

            if (sqlclntpf1rs.next()) {
            	notifinum=sqlclntpf1rs.getString(1);
            	
            }
        } catch (SQLException e) {
			LOGGER.error("getMaxNotifinum()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
        } finally {
            close(psClntSelect, sqlclntpf1rs);
        }
        return notifinum;	
		
		
	}
		@Override
	public List<Notipf> getNotifiRecord(String notificnum) {
		StringBuilder sqlClntSelect1 = new StringBuilder(
                "SELECT NOTIFINUM,INCIDENTT,NOTIFIDATE,NOTIFISTATUS,USRPRF,DATIME FROM NOTIPF WHERE NOTIFICNUM=? ORDER BY NOTIFINUM DESC");
		 
		
		ResultSet sqlclntpf1rs = null;
	        PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
	       
	        List<Notipf> resultList = new ArrayList<Notipf>();
		 try {
			 	psClntSelect.setString(1, notificnum.trim());
	            sqlclntpf1rs = executeQuery(psClntSelect);
	            while (sqlclntpf1rs.next()) {
	            	 Notipf notipf = new Notipf();
	            	notipf.setNotificnum(sqlclntpf1rs.getString(1));
	            	notipf.setIncidentt(sqlclntpf1rs.getString(2));
	            	notipf.setNotifidate(sqlclntpf1rs.getInt(3));
	            	notipf.setNotifistatus(sqlclntpf1rs.getString(4));
	            	notipf.setUsrprf(sqlclntpf1rs.getString(5));
	            	notipf.setDatime(sqlclntpf1rs.getString(6));
	            	resultList.add(notipf);
	            }
	        } catch (SQLException e) {
			LOGGER.error("getNotifiRecord()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psClntSelect, sqlclntpf1rs);
	        }
		return resultList;
	}
		
		@Override
		public List<Notipf> getNotifiRecordByValidFlag(String notificnum,String validFlag) {
			StringBuilder sqlClntSelect1 = new StringBuilder(
	                "SELECT NOTIFINUM,INCIDENTT,NOTIFIDATE,NOTIFISTATUS,USRPRF,DATIME FROM NOTIPF WHERE NOTIFICNUM=? AND VALIDFLAG=? ORDER BY NOTIFINUM DESC");
			 
			
			ResultSet sqlclntpf1rs = null;
		        PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
		       
		        List<Notipf> resultList = new ArrayList<Notipf>();
			 try {
				 	psClntSelect.setString(1, notificnum.trim());
				 	psClntSelect.setString(2, validFlag.trim());
		            sqlclntpf1rs = executeQuery(psClntSelect);
		            while (sqlclntpf1rs.next()) {
		            	 Notipf notipf = new Notipf();
		            	notipf.setNotificnum(sqlclntpf1rs.getString(1));
		            	notipf.setIncidentt(sqlclntpf1rs.getString(2));
		            	notipf.setNotifidate(sqlclntpf1rs.getInt(3));
		            	notipf.setNotifistatus(sqlclntpf1rs.getString(4));
		            	notipf.setUsrprf(sqlclntpf1rs.getString(5));
		            	notipf.setDatime(sqlclntpf1rs.getString(6));
		            	resultList.add(notipf);
		            }
		        } catch (SQLException e) {
			LOGGER.error("getNotifiRecord()", e); /* IJTI-1479 */
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(psClntSelect, sqlclntpf1rs);
		        }
			return resultList;
		}

		//updated for IBPLIFE-1702
		@Override
		public Notipf getNotiReByNotifin(String notifinum,String notificoy) {
			StringBuilder sqlClntSelect1 = new StringBuilder(
	                "SELECT NOTIFINUM,CLAIMANT,RELATIONCNUM,NOTIFIDATE,INCURDATE,LOCATION,INCIDENTT,DEATHDATE,CAUSEDEATH,RGPYTYPE,CLAIMAT,DOCTOR,MEDICALPD,HOSPITALLE,DIAGNOSIS,ADMINTDATE,DISCHARGEDATE,ACCIDENTDESC,Notificoy,Notificnum,IncidentT,USRPRF,JOBNM,DATIME,notifistatus,CHDRNUM,TRANSNO,VALIDFLAG  ");
			
			sqlClntSelect1.append("FROM NOTIPF WHERE NOTIFINUM=? and NOTIFICOY=? ");
			ResultSet sqlclntpf1rs = null;
		     PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
		       
		      Notipf notipf=null;

			 try {
				 	  psClntSelect.setString(1,notifinum.trim());
				 	  psClntSelect.setString(2, notificoy.trim());
		            sqlclntpf1rs = executeQuery(psClntSelect);
		            if (sqlclntpf1rs.next()) {
		            	notipf = new Notipf();
		            	notipf.setNotifinum(sqlclntpf1rs.getString(1));
		            	notipf.setClaimant(sqlclntpf1rs.getString(2));
		            	notipf.setRelationcnum(sqlclntpf1rs.getString(3));
		            	notipf.setNotifidate(sqlclntpf1rs.getInt(4));
		            	notipf.setIncurdate(sqlclntpf1rs.getInt(5));
		            	notipf.setLocation(sqlclntpf1rs.getString(6));
		            	notipf.setIncidentt(sqlclntpf1rs.getString(7));
		            	notipf.setDeathdate(sqlclntpf1rs.getInt(8));
		            	notipf.setCausedeath(sqlclntpf1rs.getString(9));
		            	notipf.setRclaimreason(sqlclntpf1rs.getString(10));
		            	notipf.setClaimat(sqlclntpf1rs.getDouble(11));
		            	notipf.setDoctor(sqlclntpf1rs.getString(12));
		            	notipf.setMedicalpd(sqlclntpf1rs.getString(13));
		            	notipf.setHospitalle(sqlclntpf1rs.getString(14));
		            	notipf.setDiagnosis(sqlclntpf1rs.getString(15));
		            	notipf.setAdmintdate(sqlclntpf1rs.getInt(16));
		            	notipf.setDischargedate(sqlclntpf1rs.getInt(17));
		            	notipf.setAccidentdesc(sqlclntpf1rs.getString(18));
		            	notipf.setNotificoy(sqlclntpf1rs.getString(19));
		            	notipf.setNotificnum(sqlclntpf1rs.getString(20));
		            	notipf.setIncidentt(sqlclntpf1rs.getString(21));
		            	notipf.setUsrprf(sqlclntpf1rs.getString(22));
		            	notipf.setJobnm(sqlclntpf1rs.getString(23));
		            	notipf.setDatime(sqlclntpf1rs.getString(24));
		            	notipf.setNotifistatus(sqlclntpf1rs.getString(25));
		            	notipf.setChdrnum(sqlclntpf1rs.getString(26));
		            	notipf.setTransno(sqlclntpf1rs.getString(27));
		            	notipf.setValidFlag(sqlclntpf1rs.getString(28));
		            	
		            }
		        } catch (SQLException e) {
			LOGGER.error("getNotiReByNotifin()", e); /* IJTI-1479 */
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(psClntSelect, sqlclntpf1rs);
		        }
			return notipf;
		}
		
		@Override
		public Notipf getNotiReByNotifinAndValidflag(String notifinum,String notificoy,String validflag) {
			StringBuilder sqlClntSelect1 = new StringBuilder(
	                "SELECT NOTIFINUM,CLAIMANT,RELATIONCNUM,NOTIFIDATE,INCURDATE,LOCATION,INCIDENTT,DEATHDATE,CAUSEDEATH,RGPYTYPE,CLAIMAT,DOCTOR,MEDICALPD,HOSPITALLE,DIAGNOSIS,ADMINTDATE,DISCHARGEDATE,ACCIDENTDESC  ");
			
			sqlClntSelect1.append("FROM NOTIPF WHERE NOTIFINUM=? and NOTIFICOY=? and VALIDFLAG=?");
			ResultSet sqlclntpf1rs = null;
		     PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
		       
		      Notipf notipf=null;

			 try {
				 	  psClntSelect.setString(1,notifinum.trim());
				 	  psClntSelect.setString(2, notificoy.trim());
				 	  psClntSelect.setString(3, validflag.trim());
		            sqlclntpf1rs = executeQuery(psClntSelect);
		            if (sqlclntpf1rs.next()) {
		            	notipf = new Notipf();
		            	notipf.setNotifinum(sqlclntpf1rs.getString(1));
		            	notipf.setClaimant(sqlclntpf1rs.getString(2));
		            	notipf.setRelationcnum(sqlclntpf1rs.getString(3));
		            	notipf.setNotifidate(sqlclntpf1rs.getInt(4));
		            	notipf.setIncurdate(sqlclntpf1rs.getInt(5));
		            	notipf.setLocation(sqlclntpf1rs.getString(6));
		            	notipf.setIncidentt(sqlclntpf1rs.getString(7));
		            	notipf.setDeathdate(sqlclntpf1rs.getInt(8));
		            	notipf.setCausedeath(sqlclntpf1rs.getString(9));
		            	notipf.setRclaimreason(sqlclntpf1rs.getString(10));
		            	notipf.setClaimat(sqlclntpf1rs.getDouble(11));
		            	notipf.setDoctor(sqlclntpf1rs.getString(12));
		            	notipf.setMedicalpd(sqlclntpf1rs.getString(13));
		            	notipf.setHospitalle(sqlclntpf1rs.getString(14));
		            	notipf.setDiagnosis(sqlclntpf1rs.getString(15));
		            	notipf.setAdmintdate(sqlclntpf1rs.getInt(16));
		            	notipf.setDischargedate(sqlclntpf1rs.getInt(17));
		            	notipf.setAccidentdesc(sqlclntpf1rs.getString(18));
		            }
		        } catch (SQLException e) {
			LOGGER.error("getNotiReByNotifinAndValidflag()", e); /* IJTI-1479 */
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(psClntSelect, sqlclntpf1rs);
		        }
			return notipf;
		}

		@Override
		public int updateNotifiStatus(String notifistatus, String notifinum) {
			 String SQL_CHDR_UPDATE = "UPDATE NOTIPF SET NOTIFISTATUS=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE NOTIFINUM=? ";

	            PreparedStatement psChdrUpdate = getPrepareStatement(SQL_CHDR_UPDATE);
	            int count;
	            try {
	                    psChdrUpdate.setString(1, notifistatus.trim());
	                    psChdrUpdate.setString(2, getJobnm());
	                    psChdrUpdate.setString(3, getUsrprf());
	                    psChdrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
						psChdrUpdate.setString(5, notifinum.trim());								
	                    count = psChdrUpdate.executeUpdate();
	            } catch (SQLException e) {
			LOGGER.error("updateNotifiStatus()", e); /* IJTI-1479 */
	                throw new SQLRuntimeException(e);
	            } finally {
	                close(psChdrUpdate, null);
	            }
	        return count;
		}
		
		

		@Override
		public int updateNotipf(Notipf notipf) {
			String SQL_CHDR_UPDATE = "UPDATE NOTIPF SET CLAIMANT=?,RELATIONCNUM=?,INCURDATE=?,LOCATION=?,INCIDENTT=?,DEATHDATE=?,CAUSEDEATH=?,RGPYTYPE=?,CLAIMAT=?,DOCTOR=?,MEDICALPD=?,HOSPITALLE=?,DIAGNOSIS=?,ADMINTDATE=?,DISCHARGEDATE=?,ACCIDENTDESC=?,NOTIFISTATUS=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE NOTIFINUM=? ";

	        PreparedStatement psChdrUpdate = getPrepareStatement(SQL_CHDR_UPDATE);
	        int count;
	        try {
	                psChdrUpdate.setString(1, notipf.getClaimant().trim());
	                psChdrUpdate.setString(2, notipf.getRelationcnum().trim());
	                psChdrUpdate.setInt(3, notipf.getIncurdate());
	                psChdrUpdate.setString(4, notipf.getLocation().trim());
	                psChdrUpdate.setString(5, notipf.getIncidentt().trim());
	                psChdrUpdate.setInt(6, notipf.getDeathdate());
	                psChdrUpdate.setString(7, notipf.getCausedeath().trim());
	                psChdrUpdate.setString(8, notipf.getRclaimreason().trim());
	                psChdrUpdate.setDouble(9, notipf.getClaimat());
	                
	                psChdrUpdate.setString(10, notipf.getDoctor().trim());
	                psChdrUpdate.setString(11, notipf.getMedicalpd().trim());
	    			psChdrUpdate.setString(12, notipf.getHospitalle().trim());
	    			psChdrUpdate.setString(13, notipf.getDiagnosis().trim());
	    			psChdrUpdate.setInt(14, notipf.getAdmintdate());
	    			psChdrUpdate.setInt(15, notipf.getDischargedate());
	    			psChdrUpdate.setString(16, notipf.getAccidentdesc().trim());
	    			psChdrUpdate.setString(17, notipf.getNotifistatus().trim());
	                
	                psChdrUpdate.setString(18, getJobnm().trim());
	                psChdrUpdate.setString(19, getUsrprf().trim());
	                psChdrUpdate.setTimestamp(20, new Timestamp(System.currentTimeMillis()));
					psChdrUpdate.setString(21, notipf.getNotifinum().trim());								
	                count = psChdrUpdate.executeUpdate();
	        } catch (SQLException e) {
			LOGGER.error("updateNotipf()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psChdrUpdate, null);
	        }
	    return count;
		}
		@Override
		public Notipf getStatusByNumAndCnum(String Notificnum, String chdrnum) {
			


			Notipf notipf = null;
			StringBuilder sqlClntSelect1 = new StringBuilder(
	                "SELECT NOTIFISTATUS ");
	        sqlClntSelect1.append("FROM NOTIPF WHERE NOTIFICNUM=? and CHDRNUM=? ORDER BY DATIME DESC");
	      
	        ResultSet sqlclntpf1rs = null;
	        PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
	        try {
	        	psClntSelect.setString(1, Notificnum.trim());
	        	psClntSelect.setString(2, chdrnum.trim());
	            sqlclntpf1rs = executeQuery(psClntSelect);

	            if (sqlclntpf1rs.next()) {
	            	notipf=new Notipf();
	            	notipf.setNotifistatus(sqlclntpf1rs.getString(1));
	            	
	            }
	        } catch (SQLException e) {
			LOGGER.error("getStatusByNumAndCnum()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psClntSelect, sqlclntpf1rs);
	        }
	        return notipf;	
		}
		@Override
		public Notipf getChdrByFinum(String notifinum) {
			Notipf notipf = null;
			StringBuilder sqlClntSelect1 = new StringBuilder(
	                "SELECT CHDRNUM ");
	        sqlClntSelect1.append("FROM NOTIPF  where NOTIFINUM=? ");
	      
	        ResultSet sqlclntpf1rs = null;
	        PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
	        try {
	        	psClntSelect.setString(1, notifinum.trim());
	            sqlclntpf1rs = executeQuery(psClntSelect);

	            if (sqlclntpf1rs.next()) {
	            	notipf=new Notipf();
	            	notipf.setChdrnum(sqlclntpf1rs.getString(1));
	            	
	            }
	        } catch (SQLException e) {
			LOGGER.error("getStatusByNumAndCnum()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psClntSelect, sqlclntpf1rs);
	        }
	        return notipf;	
		}

		@Override
		public int updateNotipfValidFlag(String validflag, String notifinum) {
			String SQL_CHDR_UPDATE = "UPDATE NOTIPF SET VALIDFLAG=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE NOTIFINUM=? ";

            PreparedStatement psChdrUpdate = getPrepareStatement(SQL_CHDR_UPDATE);
            int count;
            try {
                    psChdrUpdate.setString(1, validflag.trim());
                    psChdrUpdate.setString(2, getJobnm());
                    psChdrUpdate.setString(3, getUsrprf());
                    psChdrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
					psChdrUpdate.setString(5, notifinum.trim());								
                    count = psChdrUpdate.executeUpdate();
            } catch (SQLException e) {
			LOGGER.error("updateNotipfValidFlag()", e); /* IJTI-1479 */
                throw new SQLRuntimeException(e);
            } finally {
                close(psChdrUpdate, null);
            }
        return count;
		}
		
		@Override
		public int updateNotipfValidFlagToClose(String validflag, String notifinum) {
			String SQL_CHDR_UPDATE = "UPDATE NOTIPF SET VALIDFLAG=?,JOBNM=?,USRPRF=?,DATIME=?  WHERE NOTIFINUM=? AND VALIDFLAG='1'";

            PreparedStatement psChdrUpdate = getPrepareStatement(SQL_CHDR_UPDATE);
            int count;
            try {
                    psChdrUpdate.setString(1, validflag.trim());
                    psChdrUpdate.setString(2, getJobnm());
                    psChdrUpdate.setString(3, getUsrprf());
                    psChdrUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
					psChdrUpdate.setString(5, notifinum.trim());								
                    count = psChdrUpdate.executeUpdate();
            } catch (SQLException e) {
			LOGGER.error("updateNotipfValidFlag()", e); /* IJTI-1479 */
                throw new SQLRuntimeException(e);
            } finally {
                close(psChdrUpdate, null);
            }
        return count;
		}

		@Override
		public String getMaxTransno(String notifinum) {
			String result="";
			StringBuilder sqlClntSelect1 = new StringBuilder(
	                "SELECT MAX(TRANSNO) ");
	        sqlClntSelect1.append("FROM NOTIPF WHERE NOTIFINUM=?");
	      
	        ResultSet sqlclntpf1rs = null;
	        PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
	        try {
	        	psClntSelect.setString(1, notifinum.trim());
	            sqlclntpf1rs = executeQuery(psClntSelect);

	            if (sqlclntpf1rs.next()) {
	            	result=sqlclntpf1rs.getString(1);
	            	
	            }
	        } catch (SQLException e) {
			LOGGER.error("getMaxTransno()", e); /* IJTI-1479 */
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(psClntSelect, sqlclntpf1rs);
	        }
	        return result;	
			
		}

		@Override
		public Notipf getExactNotipfByTransno(String notifinum, String transno) {
			StringBuilder sqlClntSelect1 = new StringBuilder(
	                "SELECT NOTIFINUM,CLAIMANT,RELATIONCNUM,NOTIFIDATE,INCURDATE,LOCATION,INCIDENTT,DEATHDATE,CAUSEDEATH,RGPYTYPE,CLAIMAT,DOCTOR,MEDICALPD,HOSPITALLE,DIAGNOSIS,ADMINTDATE,DISCHARGEDATE,ACCIDENTDESC  ");
			
			sqlClntSelect1.append("FROM NOTIPF WHERE NOTIFINUM=? AND TRANSNO=?");
			ResultSet sqlclntpf1rs = null;
		     PreparedStatement psClntSelect = getPrepareStatement(sqlClntSelect1.toString());
		       
		      Notipf notipf=null;

			 try {
				 	  psClntSelect.setString(1,notifinum.trim());
				 	  psClntSelect.setString(2,transno.trim());
		            sqlclntpf1rs = executeQuery(psClntSelect);
		            if (sqlclntpf1rs.next()) {
		            	notipf = new Notipf();
		            	notipf.setNotifinum(sqlclntpf1rs.getString(1));
		            	notipf.setClaimant(sqlclntpf1rs.getString(2));
		            	notipf.setRelationcnum(sqlclntpf1rs.getString(3));
		            	notipf.setNotifidate(sqlclntpf1rs.getInt(4));
		            	notipf.setIncurdate(sqlclntpf1rs.getInt(5));
		            	notipf.setLocation(sqlclntpf1rs.getString(6));
		            	notipf.setIncidentt(sqlclntpf1rs.getString(7));
		            	notipf.setDeathdate(sqlclntpf1rs.getInt(8));
		            	notipf.setCausedeath(sqlclntpf1rs.getString(9));
		            	notipf.setRclaimreason(sqlclntpf1rs.getString(10));
		            	notipf.setClaimat(sqlclntpf1rs.getDouble(11));
		            	notipf.setDoctor(sqlclntpf1rs.getString(12));
		            	notipf.setMedicalpd(sqlclntpf1rs.getString(13));
		            	notipf.setHospitalle(sqlclntpf1rs.getString(14));
		            	notipf.setDiagnosis(sqlclntpf1rs.getString(15));
		            	notipf.setAdmintdate(sqlclntpf1rs.getInt(16));
		            	notipf.setDischargedate(sqlclntpf1rs.getInt(17));
		            	notipf.setAccidentdesc(sqlclntpf1rs.getString(18));
		            }
		        } catch (SQLException e) {
			LOGGER.error("getExactNotipfByTransno()", e); /* IJTI-1479 */
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(psClntSelect, sqlclntpf1rs);
		        }
			return notipf;
		}
		
}


