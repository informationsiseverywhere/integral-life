package com.csc.life.terminationclaims.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH538.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh538Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private ZonedDecimalData loanorigam = new ZonedDecimalData(17, 2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData refundfe = new ZonedDecimalData(17, 2);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData surrval = new ZonedDecimalData(17, 2);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh538Report() {
		super();
	}


	/**
	 * Print the XML for Rh538d01
	 */
	public void printRh538d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 9, 3));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 12, 3));
		surrval.setFieldName("surrval");
		surrval.setInternal(subString(recordData, 15, 17));
		loanorigam.setFieldName("loanorigam");
		loanorigam.setInternal(subString(recordData, 32, 17));
		refundfe.setFieldName("refundfe");
		refundfe.setInternal(subString(recordData, 49, 17));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 66, 30));
		printLayout("Rh538d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				cnttype,
				cntcurr,
				surrval,
				loanorigam,
				refundfe,
				descrip
			}
		);

	}

	/**
	 * Print the XML for Rh538h01
	 */
	public void printRh538h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 42, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 52, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 54, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rh538h01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(10);
	}


}
