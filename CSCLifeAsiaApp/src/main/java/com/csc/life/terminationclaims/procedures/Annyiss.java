/*
 * File: Annyiss.java
 * Date: 29 August 2009 20:14:31
 * Author: Quipoz Limited
 * 
 * Class transformed from ANNYISS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnntlnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.AnnylnbTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  OVERVIEW
*  ========
*
*  This subroutine forms part  of  the  9405  Annuities
*  Development.    The  processing  for this program is
*  initiated  by  performing  a  Contract  Issue  on  a
*  previously  entered  Deferred  or  Immediate Annuity
*  New Business proposal. It will be called  via  T5671
*  within  contract  issue AT processing. It is similar
*  to UNLISS for Unit Linked components or TRADISS  for
*  Traditional components.
*  It   will  convert  the  annuity  temporary  records
*  (ANNT) created  in  proposal  screen  S5220,  to
*  enforce  annuity  records   (ANNY)  and  delete  the
*  temporary  records.
*  If  the  entries on the Annuity Component Edit Rules
*  table, T6625, indicate that  the  annuity  component
*  is  'with  profit'  (i.e.  the bonus method field is
*  not spaces) then  the  unit  statment  date  on  the
*  coverage record should be set as follows:
*       Policy anniversary bonuses:
*            Set to the risk commencement date
*       Company anniversary bonuses:
*            Set to 999999999.
*
*
*  INPUTS AND OUTPUTS
*  ==================
*
*  Inputs
*  ======
*  Files
*  ISUALLREC linkage (ISUA-ISUALL-REC)
*       ANNTLNB
*       COVR
*       ITEM
*
*  Tables
*  ======
*       T6625
*
*  Outputs
*  =======
*  Files
*       ANNTLNB
*       COVR
*       ANNYLNB
*
*  Errors
*  ======
*       H147 Item not found on T6625
*       H148 Invalid Bon Ind on T6625
*       H149 No BONUS method on T6625
*
*****************************************************************
* </pre>
*/
public class Annyiss extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "ANNYISS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String h147 = "H147";
	private String h148 = "H148";
	private String h149 = "H149";
		/* FORMATS */
	private String anntlnbrec = "ANNTLNBREC";
	private String annylnbrec = "ANNYLNBREC";
	private String covrrec = "COVRREC";
		/* TABLES */
	private String t6625 = "T6625";
		/*Annuity Details - Temporary*/
	private AnntlnbTableDAM anntlnbIO = new AnntlnbTableDAM();
		/*Annuity Details Life New Business*/
	private AnnylnbTableDAM annylnbIO = new AnnylnbTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Isuallrec isuallrec = new Isuallrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6625rec t6625rec = new T6625rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		errorProg610
	}

	public Annyiss() {
		super();
	}

public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		convertAnntAnny4000();
		readCovrRecord5000();
		updateFields6000();
		rewriteCovrRecord7000();
		exitProgram();
	}

protected void convertAnntAnny4000()
	{
		start4010();
	}

protected void start4010()
	{
		anntlnbIO.setChdrcoy(isuallrec.company);
		anntlnbIO.setChdrnum(isuallrec.chdrnum);
		anntlnbIO.setLife(isuallrec.life);
		anntlnbIO.setCoverage(isuallrec.coverage);
		anntlnbIO.setRider(isuallrec.rider);
		anntlnbIO.setSeqnbr(ZERO);
		anntlnbIO.setFunction(varcom.begn);
		

		
		//performance improvement --  atiwari23 
		anntlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		anntlnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		
		anntlnbIO.setFormat(anntlnbrec);
		SmartFileCode.execute(appVars, anntlnbIO);
		if (isNE(anntlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(anntlnbIO.getParams());
			syserrrec.statuz.set(anntlnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(isuallrec.company,anntlnbIO.getChdrcoy())
		|| isNE(isuallrec.chdrnum,anntlnbIO.getChdrnum())
		|| isNE(isuallrec.life,anntlnbIO.getLife())
		|| isNE(isuallrec.coverage,anntlnbIO.getCoverage())
		|| isNE(isuallrec.rider,anntlnbIO.getRider())) {
			syserrrec.params.set(anntlnbIO.getParams());
			syserrrec.statuz.set(anntlnbIO.getStatuz());
			fatalError600();
		}
		annylnbIO.setChdrcoy(anntlnbIO.getChdrcoy());
		annylnbIO.setChdrnum(anntlnbIO.getChdrnum());
		annylnbIO.setLife(anntlnbIO.getLife());
		annylnbIO.setCoverage(anntlnbIO.getCoverage());
		annylnbIO.setRider(anntlnbIO.getRider());
		annylnbIO.setPlanSuffix(isuallrec.planSuffix);
		annylnbIO.setGuarperd(anntlnbIO.getGuarperd());
		annylnbIO.setFreqann(anntlnbIO.getFreqann());
		annylnbIO.setArrears(anntlnbIO.getArrears());
		annylnbIO.setAdvance(anntlnbIO.getAdvance());
		annylnbIO.setDthpercn(anntlnbIO.getDthpercn());
		annylnbIO.setDthperco(anntlnbIO.getDthperco());
		annylnbIO.setIntanny(anntlnbIO.getIntanny());
		annylnbIO.setWithprop(anntlnbIO.getWithprop());
		annylnbIO.setWithoprop(anntlnbIO.getWithoprop());
		annylnbIO.setPpind(anntlnbIO.getPpind());
		annylnbIO.setCapcont(anntlnbIO.getCapcont());
		annylnbIO.setNomlife(anntlnbIO.getNomlife());
		annylnbIO.setValidflag("1");
		annylnbIO.setTranno(chdrlnbIO.getTranno());
		anntlnbIO.setFunction(varcom.deltd);
		anntlnbIO.setFormat(anntlnbrec);
		SmartFileCode.execute(appVars, anntlnbIO);
		if (isNE(anntlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(anntlnbIO.getParams());
			syserrrec.statuz.set(anntlnbIO.getStatuz());
			fatalError600();
		}
		annylnbIO.setFunction(varcom.writr);
		annylnbIO.setFormat(annylnbrec);
		SmartFileCode.execute(appVars, annylnbIO);
		if (isNE(annylnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(annylnbIO.getParams());
			syserrrec.statuz.set(annylnbIO.getStatuz());
			fatalError600();
		}
	}

protected void readCovrRecord5000()
	{
		start5010();
	}

protected void start5010()
	{
		covrIO.setChdrcoy(isuallrec.company);
		covrIO.setChdrnum(isuallrec.chdrnum);
		covrIO.setLife(isuallrec.life);
		covrIO.setCoverage(isuallrec.coverage);
		covrIO.setRider(isuallrec.rider);
		covrIO.setPlanSuffix(isuallrec.planSuffix);
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
	}

protected void updateFields6000()
	{
		/*START*/
		readT66256100();
		setBonusIndicator6200();
		annivProcessDate6300();
		bonusDeclareDate6400();
		/*EXIT*/
	}

protected void readT66256100()
	{
		start6110();
	}

protected void start6110()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm(covrIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(),t6625)
		|| isNE(itdmIO.getItemitem(),covrIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getCrtable());
			syserrrec.statuz.set(h147);
			fatalError600();
		}
		else {
			t6625rec.t6625Rec.set(itdmIO.getGenarea());
		}
	}

protected void setBonusIndicator6200()
	{
		/*BONUS-INDICATOR*/
		if (isNE(t6625rec.bonalloc,SPACES)
		&& isNE(t6625rec.bonalloc,"P")
		&& isNE(t6625rec.bonalloc,"C")) {
			syserrrec.statuz.set(h148);
			fatalError600();
		}
		/*BONUS-METHOD*/
		if (isNE(t6625rec.bonalloc,SPACES)) {
			if (isEQ(t6625rec.revBonusMeth,SPACES)) {
				syserrrec.statuz.set(h149);
				fatalError600();
			}
		}
		/*SET-BONUS-INDICATOR*/
		covrIO.setBonusInd(t6625rec.bonalloc);
		/*EXIT*/
	}

protected void annivProcessDate6300()
	{
		/*START*/
		covrIO.setAnnivProcDate(99999999);
		datcon2rec.intDate1.set(covrIO.getCrrcd());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		covrIO.setAnnivProcDate(datcon2rec.intDate2);
		/*EXIT*/
	}

protected void bonusDeclareDate6400()
	{
		/*START*/
		if ((isEQ(covrIO.getBonusInd(),"P"))) {
			covrIO.setUnitStatementDate(covrIO.getCrrcd());
		}
		else {
			covrIO.setUnitStatementDate(99999999);
		}
		/*EXIT*/
	}

protected void rewriteCovrRecord7000()
	{
		start7000();
	}

protected void start7000()
	{
		covrIO.setChdrcoy(isuallrec.company);
		covrIO.setChdrnum(isuallrec.chdrnum);
		covrIO.setLife(isuallrec.life);
		covrIO.setCoverage(isuallrec.coverage);
		covrIO.setRider(isuallrec.rider);
		covrIO.setPlanSuffix(isuallrec.planSuffix);
		covrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fatalError601();
				}
				case errorProg610: {
					errorProg610();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fatalError601()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.errorProg610);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void errorProg610()
	{
		isuallrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void exit690()
	{
		exitProgram();
	}
}
