package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;	
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ZhlcpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.ZhlcpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Zhlcpf;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.terminationclaims.screens.Sr57rScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


public class Pr57r extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR57R");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);
	private PackedDecimalData wsaaTemp = new PackedDecimalData(17, 2).init(0);
	
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

	private FixedLengthStringData wsbbCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsbbRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsbbLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsbbSumins = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsbbAdjust = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();	
	private FixedLengthStringData wsaaChanged = new FixedLengthStringData(1).init("N");
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaDeadLife = new FixedLengthStringData(8);
	private static final Logger LOGGER = LoggerFactory.getLogger(ZhlcpfDAOImpl.class);
	
	private PackedDecimalData wsaaClmv = new PackedDecimalData(17, 2).init(0); 
	private PackedDecimalData wsaaClma = new PackedDecimalData(17, 2).init(0);

		/* ERRORS */
	private static final String e304 = "E304";
	
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t5687 = "T5687";
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5548 = "T5548";
	private static final String clmhclmrec = "CLMHCLMREC";
	private static final String ptrnrec = "PTRNREC";
	private T5645rec t5645rec = new T5645rec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM();
	private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeclmTableDAM lifeclmIO = new LifeclmTableDAM();
	private ZhlcpfDAO zhlcpfDAO = new ZhlcpfDAOImpl();
			
		
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Batckey wsaaBatckey = new Batckey();
	
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Gensswrec gensswrec = new Gensswrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr57rScreenVars sv = ScreenProgram.getScreenVars( Sr57rScreenVars.class);
	private WsaaTransactionRecInner wsaaTransactionRecInner = new WsaaTransactionRecInner();
	private ErrorsInner errorsInner = new ErrorsInner();	
	private FormatsInner formatsInner = new FormatsInner();
  	private Zhlcpf zhlcpf = new Zhlcpf(); /*BRD-34 by vpenugonda*/
  	private List<Zhlcpf> ls= null; /*BRD-34 by vpenugonda*/

    //ILB-459
  	private Chdrpf chdrpf = new Chdrpf();
  	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
  //ILJ-49 Starts
  	private boolean cntDteFlag = false;
  	private String cntDteFeature = "NBPRP113";
  	protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO", CattpfDAO.class);
	protected Cattpf cattpf = new Cattpf();
	boolean CMDTH010Permission  = false;
	private static final String feaConfigPreRegistartion= "CMDTH010";
	
	
  	//ILJ-49 End 
  	
	/* MPTD 389 End Arockia*/

/**
 * Contains all possible labels used by goTo action.
 */
 
	public Pr57r() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57r", AppVars.getInstance(), sv);		
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

protected void initialise1000()
	{
	
		initialise1010();
	}

protected void initialise1010()
	{	
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
		
			return ;
		}
		/* Initialise working storage variables.                           */
		callDatcons1300();
		wsaaTransactionRecInner.wsaaClamamtOld.set(ZERO);
		wsaaTransactionRecInner.wsaaClamamtNew.set(ZERO);
		wsaaTransactionRecInner.wsaaOtheradjst.set(ZERO);
		wsaaX.set(ZERO);
		wsaaY.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
						
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", feaConfigPreRegistartion, appVars, "IT");
		scrnparams.function.set(varcom.sclr);
		processScreen("Sr57r", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Dummy field initilisation for prototype version.*/
		sv.totclaim.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.totclaim.set(ZERO);
		sv.dtofdeath.set(varcom.vrcmMaxDate);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		//ILB-459 starts
		/*chdrclmIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}*/
		
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrclmIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrclmIO);
			if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrclmIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrclmIO.getChdrcoy().toString(), chdrclmIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		//ILB-459 ends
		
		/* Currency Code on the screen now defaults to Contract Currency   */

		wsaaTransactionRecInner.wsaaCurrcd.set(chdrpf.getCntcurr());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.occdate.set(chdrpf.getOccdate());
		/*    Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}

		sv.cownnum.set(chdrpf.getCownnum());
		cltsIO.setClntnum(chdrpf.getCownnum());
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.ptdate.set(chdrpf.getPtdate());
		sv.btdate.set(chdrpf.getBtdate());
		sv.currcd.set(chdrpf.getCntcurr());
		
		
		if(!CMDTH010Permission){
			sv.claimnumberOut[varcom.nd.toInt()].set("Y");
		
		}
		
		/* read claim header record*/
		clmhclmIO.setDataArea(SPACES);
		clmhclmIO.setChdrcoy(chdrpf.getChdrcoy());
		clmhclmIO.setChdrnum(chdrpf.getChdrnum());
		clmhclmIO.setFunction(varcom.readh);
		clmhclmIO.setFormat(clmhclmrec);
		SmartFileCode.execute(appVars, clmhclmIO);
		if (isNE(clmhclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmhclmIO.getParams());
			fatalError600();
		}
		if (isEQ(clmhclmIO.getJlife(), "01")) {
			sv.astrsk.set("*");
		}
		else {
			sv.asterisk.set("*");
		}
		sv.effdate.set(clmhclmIO.getEffdate());
		sv.dtofdeath.set(clmhclmIO.getDtofdeath());
		sv.causeofdth.set(clmhclmIO.getCauseofdth());
		if(CMDTH010Permission){
		cattpf = cattpfDAO.selectRecords(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());
		if(null == cattpf){
		syserrrec.params.set(chdrpf.getChdrcoy().toString()+ chdrpf.getChdrnum());
		fatalError600();	
		}
		else{
		sv.claimnumber.set(cattpf.getClaim());
		}
	}
		/*Obtain cause of death description*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5548);
		descIO.setDescitem(sv.causeofdth);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/*         MOVE ALL '?'           TO S6353-CTYPEDES*/
			sv.causeofdthdsc.set(SPACES);
		}
		else {
			sv.causeofdthdsc.set(descIO.getLongdesc());
		}
		
		sv.reasoncd.set(clmhclmIO.getReasoncd());

		sv.longdesc.set(clmhclmIO.getResndesc());
		wsaaTransactionRecInner.wsaaOtheradjst.set(clmhclmIO.getOtheradjst());
		sv.otheradjst.set(clmhclmIO.getOtheradjst());
		lifeclmIO.setDataArea(SPACES);
		lifeclmIO.setChdrcoy(clmhclmIO.getChdrcoy());
		lifeclmIO.setChdrnum(clmhclmIO.getChdrnum());
		lifeclmIO.setLife(clmhclmIO.getLife());
		lifeclmIO.setJlife(SPACES);
		lifeclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		lifeclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		SmartFileCode.execute(appVars, lifeclmIO);
		if (isNE(lifeclmIO.getStatuz(), varcom.oK)
		&& isNE(lifeclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isNE(clmhclmIO.getChdrcoy(), lifeclmIO.getChdrcoy())
		|| isNE(clmhclmIO.getChdrnum(), lifeclmIO.getChdrnum())
		|| isNE(clmhclmIO.getLife(), lifeclmIO.getLife())
		|| isNE(lifeclmIO.getJlife(), "00")
		&& isNE(lifeclmIO.getJlife(), "  ")
		|| isEQ(lifeclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifeclmIO.getLifcnum());
		wsaaDeadLife.set(lifeclmIO.getLifcnum());
		cltsIO.setClntnum(lifeclmIO.getLifcnum());
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		/*    look for joint life.*/
		lifeclmIO.setJlife("01");
		lifeclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeclmIO);
		if ((isNE(lifeclmIO.getStatuz(), varcom.oK))
		&& (isNE(lifeclmIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isEQ(lifeclmIO.getStatuz(), varcom.mrnf)) {
			lifeclmIO.setJlife(SPACES);
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
		} else {
		//	sv.jlifcnum.set(lifeclmIO.getLifcnum());
			cltsIO.setClntnum(lifeclmIO.getLifcnum());
			if (isEQ(clmhclmIO.getJlife(), "01")) {
				wsaaDeadLife.set(lifeclmIO.getLifcnum());
			}
			getClientDetails1400();
			/* Get the confirmation name.*/
			if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
			|| isNE(cltsIO.getValidflag(), 1)) {
				sv.jlinsnameErr.set(e304);
				sv.jlinsname.set(SPACES);
			}
			else {
				plainname();
	//			sv.jlinsname.set(wsspcomn.longconfname);
			}
		}
		continue1030();
	}

protected void continue1030()
	{
		getTotalClaimValue1500();

		compute(sv.totclaim,2).set(add(sv.totclaim,wsaaActualTot));
		
		/*BRD-34   by vpenugonda starts*/
		
		zhlcpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		zhlcpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1523 */
		zhlcpf.setValidflag("1"); 
		ls = zhlcpfDAO.readZhlcpf(zhlcpf);
		
		if(ls == null || (ls != null  && ls.size()==0)) {
			return;
		}
		/*  store currency*/
		wsbbCoverage.set(" ");
		wsbbRider.set(" "); wsbbLife.set(" ");
		wsbbCrtable.set(" ");
		wsbbSumins.set(0);
		wsbbAdjust.set(0);
		
		for(Zhlcpf zhlcpfData: ls)
			/*BRD-34  by vpenugonda ends*/
		 {
			dispSubfile1800(zhlcpfData);
		}
		
		sv.dtofdeathOut[varcom.pr.toInt()].set("Y");
		sv.causeofdthOut[varcom.pr.toInt()].set("Y");
		sv.reasoncdOut[varcom.pr.toInt()].set("Y");
		sv.longdescOut[varcom.pr.toInt()].set("Y");
		sv.currcdOut[varcom.pr.toInt()].set("Y");
		sv.otheradjstOut[varcom.pr.toInt()].set("Y");
		sv.effdateOut[varcom.pr.toInt()].set("Y");
	}

protected void callDatcons1300()
	{
		/*CALL-DATCONS*/
		/*DATCON1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*EXIT*/
	}


protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1400()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void	getTotalClaimValue1500() {

		clmdclmIO.setDataArea(SPACES);
		clmdclmIO.setChdrcoy(chdrpf.getChdrcoy());
		clmdclmIO.setChdrnum(chdrpf.getChdrnum());
		clmdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		clmdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clmdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.endp)
		&& isNE(clmdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), clmdclmIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), clmdclmIO.getChdrnum())
		|| isEQ(clmdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		/*  store currency*/
		wsaaStoredCurrency.set(clmdclmIO.getCnstcur());
		while ( !(isEQ(clmdclmIO.getStatuz(), varcom.endp))) {
			getTotalClaimClmdclm1600();
		}

}

protected void getTotalClaimClmdclm1600()
{
		if (isNE(chdrpf.getChdrcoy(), clmdclmIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), clmdclmIO.getChdrnum())
		|| isEQ(clmdclmIO.getStatuz(), varcom.endp)) {
			clmdclmIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(clmdclmIO.getCnstcur(), wsaaStoredCurrency)) {
			wsaaEstimateTot.add(clmdclmIO.getEstMatValue());
			wsaaActualTot.add(clmdclmIO.getActvalue());
		}
		else {
			wsaaCurrencySwitch.set(1);
		}
		clmdclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.endp)
		&& isNE(clmdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}

	}


protected void dispSubfile1800(Zhlcpf zhlcpf) {
	
		
		sv.coverage.set(zhlcpf.getCoverage());
		sv.rider.set(zhlcpf.getRider());
		if (isEQ(sv.rider, "00")) {
		    sv.rider.set(SPACES);
		}
		if (isNE(sv.rider, "00")
		&& isNE(sv.rider, SPACES)) {
  		    sv.coverage.set(SPACES);
		}
		sv.life.set(zhlcpf.getLife());
		sv.crtable.set(zhlcpf.getCrtable());
		descIO.setDesctabl(t5687);
		descIO.setDescitem(sv.crtable);
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
	  		sv.lcendesc.set(descIO.getLongdesc());
		}
		else {
			sv.lcendesc.fill("?");
		}
		sv.zhldclmv.set(zhlcpf.getZhldclmv());
		sv.zhldclma.set(zhlcpf.getZhldclma());
		sv.zrlsclmv.set(sv.zhldclmv);
		sv.zrlsclma.set(sv.zhldclma);

		scrnparams.function.set(varcom.sadd);
		processScreen("Sr57r", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{

		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}


protected void preScreenEdit()
	{

		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		scrnparams.subfileRrn.set(1);
		return ;

	}

protected void screenEdit2000()
	{   
		screenIo2010();
		validateScreen2010();
		
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
		
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}


protected void validateScreen2010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			
			validateSelectionFields2070();
		}
		sv.currcd.set(chdrpf.getCntcurr());
		readjustSubfile2100();
		
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			return;
		}
		validateSelectionFields2070();
	}

protected void validateSelectionFields2070()
	{
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
			return ;
		}
	
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			return ;
		}
	}

protected void readjustSubfile2100()
	{
		wsbbAdjust.set(0);
		scrnparams.function.set(varcom.sstrt);
		processScreen("Sr57r", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile2200();
		}
				
		if (isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void readSubfile2200()
	{
		go2250();
		updateErrorIndicators2270();
		readNextRecord2280();
	}

protected void go2250()
	{
		//Check if hold claim amount is greated than coverage claim value.

		if (isGT(sv.zrlsclmv, sv.zhldclmv)) {
			sv.zhldclmvErr.set(errorsInner.RFTG);
		}
		if (isGT(sv.zrlsclma, sv.zhldclma)) {
			sv.zhldclmaErr.set(errorsInner.RFTH);
		}
	}

protected void updateErrorIndicators2270()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("Sr57r", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextRecord2280()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("Sr57r", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

	}


protected void update3000()
	{  
	      updateDatabase3010();
	      checkHoldData3020();
		
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
	}

protected void checkHoldData3020()
	{
		wsbbAdjust.set(0);
		wsbbCoverage.set(" ");
		wsbbRider.set(" ");
		wsbbLife.set(" ");
		wsbbCrtable.set(" ");
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("PR57R");		//ILIFE-4102
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrpf.getChdrcoy());
		descIO.setDesctabl(t5645);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem("PR57R");			//ILIFE-4102
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		readrChdrlif3610();
		scrnparams.function.set(varcom.sstrt);
		processScreen("Sr57r", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*BRD-34  by vpenugonda starts*/
		//Read Coverage value to update for release
		
		zhlcpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		zhlcpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1523 */
		zhlcpf.setValidflag("1"); 
		ls = zhlcpfDAO.readZhlcpf(zhlcpf);
		if(ls == null || (ls != null  && ls.size()==0)) {
				return;
			}
		
		
		for (Zhlcpf zhlcpf:ls)
		{
			
			readSubfile3100(zhlcpf);
			
		}
		if (isEQ(wsaaChanged, "Y")) {
			update3500();		
		}
		/*BRD-34  by vpenugonda ends*/
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}

	}


protected void readSubfile3100(Zhlcpf zhlcpf) 
	{
	boolean isInsertSuccess=false;	
	/*BRD-34  by vpenugonda starts*/
		//Update coverage value
		compute(wsaaTemp,2).set(sub(zhlcpf.getZhldclmv(), sv.zrlsclmv));
		
		zhlcpf.setZhldclmv(wsaaTemp.getbigdata());
		compute(wsaaTemp,2).set(sub(zhlcpf.getZhldclma(), sv.zrlsclma));
		
		
		zhlcpf.setZhldclma(wsaaTemp.getbigdata());
		
		if(zhlcpf.getZhldclmv().intValue()==0 && zhlcpf.getZhldclma().intValue() ==0) {
			   zhlcpf.setValidflag("2");
		}
		
	/*	ILIFE-3135*/
	
				isInsertSuccess = zhlcpfDAO.updateIntoZhlcpf(zhlcpf);
			
			if (!isInsertSuccess) {
				 LOGGER.error("Update Zhlcpf record failed.");
				  fatalError600();
			} 
	/*	ILIFE-3135*/
		/*BRD-34  by vpenugonda ends*/
		
		if (isNE(sv.zrlsclmv,0)) {
			wsaaChanged.set("Y");
			if (isNE(sv.coverage, " ")) {
				wsbbCoverage.set(sv.coverage);
			}
			wsbbCrtable.set(sv.crtable);
			wsbbLife.set(sv.life);
			if (isEQ(sv.rider, " ")) {
				wsbbRider.set("00") ;
			}
			else { wsbbRider.set(sv.rider);
			}
			if (isNE(chdrlifIO.getStatcode(), "DH") || (isNE(chdrlifIO.getPstatcode(), "DH"))) {
				postHoldCoverage3900();				
			}
			if (isEQ(chdrlifIO.getStatcode(), "DH") && (isEQ(chdrlifIO.getPstatcode(), "DH"))) {
				postPaymentsCoverage5500();				
			}			
		}	
		if (isNE(sv.zrlsclma,0)) {
			compute(wsbbAdjust,2).set(add(wsbbAdjust, sv.zrlsclma));
			wsaaChanged.set("Y");
		}	
		scrnparams.function.set(varcom.srdn);
		processScreen("Sr57r", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
	}

protected void update3500() {

	updateChdrlif3600();
	ptrnTransaction3700();
	if (isNE(chdrlifIO.getStatcode(), "DH") || (isNE(chdrlifIO.getPstatcode(), "DH"))) {
		//ILIFE-8394
		if (isNE(wsbbAdjust, 0)) {
			postAdjustment3800();
		}
	}

//	Post Accounting if the policy is Death approved to payment suspense.
	if (isEQ(chdrlifIO.getStatcode(), "DH") && (isEQ(chdrlifIO.getPstatcode(), "DH"))) {
		wsaaActualTot.set(0);	
		postPayment5300();	
	}

}

protected void updateChdrlif3600()
	{
		updatChdrlif3630();
		writeChdrlif3650();
	}

protected void readrChdrlif3610()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(chdrpf.getChdrcoy());
		chdrlifIO.setChdrnum(chdrpf.getChdrnum());
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void updatChdrlif3630()
	{
		chdrlifIO.setValidflag("2");
		/* MOVE VRCM-MAX-DATE          TO CHDRLIF-CURRFROM.        <004>*/
		chdrlifIO.setCurrto(datcon1rec.intDate);
		chdrlifIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void writeChdrlif3650()
	{
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(datcon1rec.intDate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		chdrlifIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}


protected void ptrnTransaction3700() {

		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTransactionDate(0);
		ptrnIO.setTransactionTime(0);
		ptrnIO.setUser(0);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}


protected void postAdjustment3800() {

		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsspcomn.batchkey);
		lifacmvrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.rldgacct.set(chdrlifIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(wsbbAdjust);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.effdate.set(datcon1rec.intDate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		lifacmvrec.jrnseq.set(1);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
}

protected void postHoldCoverage3900() {

		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsspcomn.batchkey);
		lifacmvrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.sacscode.set(t5645rec.sacscode03);
		lifacmvrec.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec.glcode.set(t5645rec.glmap03);
		lifacmvrec.glsign.set(t5645rec.sign03);
		lifacmvrec.contot.set(t5645rec.cnttot03);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());

		wsaaRldgChdrnum.set(chdrlifIO.getChdrnum());
		wsaaRldgLife.set(wsbbLife);
		wsaaRldgCoverage.set(wsbbCoverage);
		wsaaRldgRider.set(wsbbRider);
		wsaaRldgPlanSuffix.set("00");
		lifacmvrec.rldgacct.set(wsaaRldgChdrnum);
		
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(sv.zrlsclmv);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		compute(lifacmvrec.tranno,0).set(add(lifacmvrec.tranno,1));
		lifacmvrec.tranref.set(lifacmvrec.tranno);
		lifacmvrec.effdate.set(datcon1rec.intDate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		//lifacmvrec.substituteCode[1].set(" ");
		lifacmvrec.substituteCode[2].set(sv.crtable);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		lifacmvrec.substituteCode[2].set(sv.crtable);
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.contot.set(t5645rec.cnttot04);
		lifacmvrec.jrnseq.set(1);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
}

protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
		/*CALL-SUBROUTINE*/
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/*    load from gensw to wssp*/
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadProgram4400()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.currcd);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void	postPayment5300() {

		scrnparams.function.set(varcom.sstrt);
		processScreen("Sr57r", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile5400();
		}
		if (isGT(wsaaActualTot,0)) {
			//postPaymentsCoverage5500();
			postPaymentSuspense5600();
			/* MPTD 389 Start Arockia*/
			if(isGT(wsaaActualTot, ZERO)){
				/*paymntCRT6500();*/
			}
			/* MPTD 389 End Arockia*/
		}
}	

protected void readSubfile5400()
	{
		if (isNE(sv.zrlsclmv,0)) {
			if (isNE(sv.coverage, " ")) {
				wsbbCoverage.set(sv.coverage);
			}
			wsbbCrtable.set(sv.crtable);
			wsbbLife.set(sv.life);
			if (isEQ(sv.rider, " ")) {
				wsbbRider.set("00") ;
			}
			else { wsbbRider.set(sv.rider);
			}
			compute(wsaaActualTot,2).set(add(wsaaActualTot,sv.zrlsclmv));
		}	
		scrnparams.function.set(varcom.srdn);
		processScreen("Sr57r", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
	}

protected void postPaymentsCoverage5500() {

		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsspcomn.batchkey);
		lifacmvrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.sacscode.set(t5645rec.sacscode05);
		lifacmvrec.sacstyp.set(t5645rec.sacstype05);
		lifacmvrec.glcode.set(t5645rec.glmap05);
		lifacmvrec.glsign.set(t5645rec.sign05);
		lifacmvrec.contot.set(t5645rec.cnttot05);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		wsaaRldgChdrnum.set(chdrlifIO.getChdrnum());
		lifacmvrec.rldgacct.set(chdrlifIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(sv.zrlsclmv);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		compute(lifacmvrec.tranno,0).set(add(lifacmvrec.tranno,1));
		lifacmvrec.tranref.set(lifacmvrec.tranno);
		lifacmvrec.effdate.set(datcon1rec.intDate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		//lifacmvrec.substituteCode[1].set(" ");
		lifacmvrec.substituteCode[6].set(sv.crtable);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
}

protected void postPaymentSuspense5600() {

	if (isGT(wsbbAdjust,0)) {
		postAP5700();
	}
	postDeathClaimSusp5800();

}

protected void postAP5700() {

//Debit if adjustment to be paid out

		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsspcomn.batchkey);
		lifacmvrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.sacscode.set(t5645rec.sacscode06);
		lifacmvrec.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec.glcode.set(t5645rec.glmap06);
		lifacmvrec.glsign.set(t5645rec.sign06);
		lifacmvrec.contot.set(t5645rec.cnttot06);
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.rldgacct.set(chdrlifIO.getChdrnum());
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(wsbbAdjust);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.tranref.set(chdrlifIO.getTranno());
		lifacmvrec.effdate.set(datcon1rec.intDate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		//lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(varcom.vrcmUser);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.transactionDate.set(varcom.vrcmDate);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
			
}

protected void postDeathClaimSusp5800() {

		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.contot.set(ZERO);
		lifrtrnrec.acctamt.set(ZERO);
		lifrtrnrec.frcdate.set(ZERO);
		lifrtrnrec.effdate.set(ZERO);
		lifrtrnrec.origamt.set(ZERO);
		lifrtrnrec.tranno.set(ZERO);
		lifrtrnrec.jrnseq.set(ZERO);
		lifrtrnrec.crate.set(ZERO);
		lifrtrnrec.transactionDate.set(ZERO);
		lifrtrnrec.transactionTime.set(ZERO);
		lifrtrnrec.user.set(ZERO);
		compute(wsaaActualTot,2).set(add(wsaaActualTot, wsbbAdjust));
		lifrtrnrec.origamt.set(wsaaActualTot);		
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(wsspcomn.batchkey);
		lifrtrnrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifrtrnrec.tranno.set(chdrlifIO.getTranno());
		lifrtrnrec.sacscode.set(t5645rec.sacscode07);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype07);
		lifrtrnrec.glcode.set(t5645rec.glmap07);
		lifrtrnrec.glsign.set(t5645rec.sign07);
		lifrtrnrec.contot.set(t5645rec.cnttot07);
		lifrtrnrec.jrnseq.set(ZERO);
		lifrtrnrec.rldgcoy.set(chdrlifIO.getChdrcoy());
		lifrtrnrec.genlcoy.set(chdrlifIO.getChdrcoy());
		lifrtrnrec.rldgacct.set(chdrlifIO.getChdrnum());
		lifrtrnrec.origcurr.set(chdrlifIO.getCntcurr());
		lifrtrnrec.genlcoy.set(wsspcomn.company);
		lifrtrnrec.acctamt.set(ZERO);
		lifrtrnrec.crate.set(ZERO);
		lifrtrnrec.genlcur.set(SPACES);
		lifrtrnrec.acctamt.set(wsaaActualTot);
		lifrtrnrec.postyear.set(SPACES);
		lifrtrnrec.postmonth.set(SPACES);
		lifrtrnrec.tranref.set(chdrlifIO.getTranno());
		lifrtrnrec.trandesc.set(descIO.getShortdesc());
		lifrtrnrec.effdate.set(datcon1rec.intDate);
		lifrtrnrec.frcdate.set("99999999");
		//lifrtrnrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifrtrnrec.termid.set(varcom.vrcmTermid);
		lifrtrnrec.user.set(varcom.vrcmUser);
		lifrtrnrec.transactionTime.set(varcom.vrcmTime);
		lifrtrnrec.transactionDate.set(varcom.vrcmDate);
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();

		}
	}
/* MPTD 389 Starts Arockia*/

/* MPTD 389 End Arockia*/
/*
 * Class transformed  from Data Structure WSAA-TRANSACTION-REC--INNER
 */
private static final class WsaaTransactionRecInner { 

		/*01  WSAA-TRANSACTION-REC.*/
	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(215);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaClamamtOld = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 1);
	private PackedDecimalData wsaaClamamtNew = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 10);
	private PackedDecimalData wsaaOtheradjst = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 19);
	private FixedLengthStringData wsaaCurrcd = new FixedLengthStringData(3).isAPartOf(wsaaTransactionRec, 37);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 40);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 44);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 48);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 52);
	private FixedLengthStringData filler = new FixedLengthStringData(159).isAPartOf(wsaaTransactionRec, 56, FILLER).init(SPACES);
}

/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData RFTG = new FixedLengthStringData(4).init("RFTG");
	private FixedLengthStringData RFTH = new FixedLengthStringData(4).init("RFTH");
}

/*
 * Class transformed  from Data Structure FORMATS_INNER
 */
private static final class FormatsInner { 
	/* FORMATS */
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData zhlcrec = new FixedLengthStringData(10).init("ZHLCREC");
}
}
