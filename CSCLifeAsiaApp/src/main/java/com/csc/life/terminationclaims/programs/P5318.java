/*
 * File: P5318.java
 * Date: 30 August 2009 0:23:59
 * Author: Quipoz Limited
 * 
 * Class transformed from P5318.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.terminationclaims.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.dao.CpbnfypfDAO;
import com.csc.life.newbusiness.dataaccess.model.Cpbnfypf;
//added for death claim flexibility
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.ChdrclmTableDAM; //ILB-459
import com.csc.life.terminationclaims.dataaccess.ClmdaddTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.FlupclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.LifeclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.CattpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClmhpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ClnnpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.CrsvpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.InvspfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.NotipfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ZhlbpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.ZhlcpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.ZhlbpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.dao.impl.ZhlcpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Cattpf;
import com.csc.life.terminationclaims.dataaccess.model.Clmhpf;
import com.csc.life.terminationclaims.dataaccess.model.Clnnpf;
import com.csc.life.terminationclaims.dataaccess.model.Crsvpf;
import com.csc.life.terminationclaims.dataaccess.model.Invspf;
import com.csc.life.terminationclaims.dataaccess.model.Notipf;
import com.csc.life.terminationclaims.dataaccess.model.Zhlbpf;
import com.csc.life.terminationclaims.dataaccess.model.Zhlcpf;
import com.csc.life.terminationclaims.recordstructures.Dthclmflxrec;
import com.csc.life.terminationclaims.screens.S5318ScreenVars;
import com.csc.life.terminationclaims.screens.Sd5jlScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;//ILB-459
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Claims Adjustments/Enquiries
*
*  This transaction,  Claims  Adjustments/Enquiries, is
*  selected from the Claims sub-menu S6319/P6319.
*
*     Initialise
*     ----------
*
*  Check the WSSP-FLAG. If 'I'(enquiry  mode), set indicator
*  to protect  all  input  capable  fields, except the
*  follow-up selection field.
*
*  Read the  Contract  header  (function  RETRV)  and  read
*  the relevant data necessary for obtaining the status
*  description, the Owner, CCD, Paid-to-date and the
*  Bill-to-date.
*
*  Read the Claim Header record (CLMHCLM) for this contract.
*
*      LIFE ASSURED AND JOINT LIFE DETAILS
*
*  To obtain the life assured and joint-life details (if any)
*  do the following;-
*
*  - Obtain  the  life  details  using LIFECLM,(READR) (for
*    this  contract  number, using the life number/joint
*    life  number  from the CLMHCLM record).  Format the
*    name accordingly.
*
*  - Obtain  the  joint-life  details using LIFECLM (READR)
*    (for   this   contract   number,   using  the  life
*    number/joint  life number from the CHLMCLM record).
*    Format the name accordingly.
*
*  - Indicate the 'dead' life by a highlighted 'asterisk'.
*
*
*  Obtain the  necessary  descriptions  by  calling 'DESCIO'
*  and output the descriptions of the statuses to the screen.
*
*  Format Name
*
*  Read the  client  details  record  and  use the relevant
*  copybook in order to format the required names.
*
*  Build the Coverage/Rider review details
*
*  Read the  Claim  details  record  (CLMDCLM) and output a
*  detail line,  for each record for this contract, to
*  the subfile and display the entries.
*
*  Sum the  Estimated  values  and  the  Actual amounts and
*  display them on the screen.
*
*  From the  Claim  header display the following details on
*  the screen:
*
*                - date of death
*                - effective date
*                - cause of death
*                - follow-ups
*                - adjustment reason code
*                - policy loan amount
*                - other adjustments amount
*                - currency
*                - adjustment reason description
*
*  The above details are returned  to  the user and if he
*  wishes to continue and  perform the Claims Adjustments he
*  enters the required amendments at the bottom of the screen,
*  otherwise he opts for 'KILL' to exit from this transaction.
*
*
*     Validation
*     ----------
*
*  If in enquiry mode or 'KILL' was entered, then skip the
*  remainder of the validation and exit from the program.
*
*  Date of Death (optional)
*
*  Check that the Date-of-Death  entered  on  the screen is
*  not less than  the contract commencement date (CCD)
*  and  that  it is  not  greater  than today's date. Also
*  compare  it  against the date-of-death on the claim
*  header record, has it changed or not.
*
*  Effective Date
*
*  Check that the Effective-date entered  on  the screen is
*  not less  than the contract commencement date (CCD)
*  and  not  less than the Date-of-death (if entered).
*  Again check against the claim header record.
*
*  Cause-of-Death (optional)
*
*  Validated by the I/O module and against the value on the
*  claim header record.
*
*  Adjustment reason (optional)
*
*  Validated by the I/O module and against the value on the
*  claim header record.
*
*  Adjustment reason description (optional)
*
*  Check whether an  adjustment  reason was entered or not.
*  If an  adjustment  reason  code  was entered and no
*  adjustment description was entered, then read T5500
*  and output the description found. This is displayed
*  if  'CALC' is  pressed  (redisplay).  If  a  reason
*  description was entered, the system will default to
*  use this value  instead  of accessing T5500 for the
*  description.  Again check the value entered against
*  the value on the claim header record.
*  Currency (optional)
*
*  Validated by the I/O module and against the value on the
*  claim header record.
*
*  A blank entry defaults to the contract currency.
*
*  Other adjustment amount
*
*  A value may or may not be entered
*
*  If 'CALC' is pressed, then the screen is redisplayed and
*  the adjusted  amount  is  included  with  the final
*  total amount, i.e.  if an adjusted amount exists.
*
*  If the    currency    code     was     changed,     then
*  re-calculate/convert  the sum of all of the amounts
*  held  in  the  subfile.  i.e.  the  total Estimated
*  amount and total Actual amount.
*
*  Note, if the  currency amount is altered again and again
*  a saturation  point  would  be reached and no money
*  would be left. To combat this, each time there is a
*  currency change the amount in the original currency
*  is converted to the new currency.
*
*     Updating
*     --------
*
*  If  in  enquiry  mode  or  KILL  was  entered, then skip
*  this section and exit from the program.
*
*  If there are no  changes  from  the  data entered against
*  the data held on the  claim  header  record,  then exit
*  from this section with no update  being  performed  and
*  exit  from the program.
*
*  UPDATE CLAIMS HEADER RECORD.
*
*  Update  the  claim  header record (CLMHCLM) for this
*  contract with  any  data from the screen that may have
*  changed. (keyed by Company, Contract number).
*
*                - company
*                - contract number
*                - transaction
*                - life number
*                - joint-life number
*                - date of death
*                - effective date
*                - cause of death
*                - follow-ups
*                - currency (old currency code is passed
*                            in the AT linkage)
*                - policy loan amount
*                - other adjustments amount (old passed in AT)
*                - adjustment reason code
*                - adjustment reason description
*
*  UPDATE THE CLAIMS DETAILS RECORD.
*
*  If the currency has  been  changed,  then  update each of
*  the CLMDCLM records for  this  contract. Amend the currency
*  field to the new currency and  the amend the amounts to
*  reflect the new converted currency  values.  It  is  keyed
*  by:- Company, Contract no, Coverage, Rider, Element type
*
*                - company
*                - contract number
*                - transaction
*                - life number
*                - joint-life number
*                - coverage
*                - rider
*                - element type (4 character code)
*                - description
*                - lien code
*                - re-insurance indicator
*                - currency
*                - estimated value
*                - actual value
*                - type (1 character code)
*
*  Call 'SFTLOCK' to transfer the contract lock to 'AT'.
*
*  Call the AT  submission module to submit P5318AT, passing
*  the Contract  number, as the 'primary  key',  this
*  performs  the Claims  Adjustments  transaction.  Also pass
*  old currency and old adjustment amount in the transaction
*  area.
*
*  Next Program
*  ------------
*
*  For KILL or 'Enter', add 1 to the program pointer and exit.
*
*
*  Modifications
*  -------------
*
*  Rewrite P5318 with the above processing included.
*
*  Include the AT module P5318AT.
*
*  Include  the  following logical views (access only the
*  fields required).
*
*            - CHDRCLM
*            - LIFECLM
*            - COVRCLM
*            - CLMHCLM
*            - CLMDCLM
*****************************************************************
* </pre>
*/
public class P5318 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	protected FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5318");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	protected FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);

	protected ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator detailsSameCurrency = new Validator(wsaaCurrencySwitch, "0");
	private Validator detailsDifferent = new Validator(wsaaCurrencySwitch, "1");
	private Validator totalCurrDifferent = new Validator(wsaaCurrencySwitch, "2");
	protected String wsaaChanged = "N";
	protected PackedDecimalData wsaaEstimateTot = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaActualTot = new PackedDecimalData(17, 2).init(0);
	protected PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaDeadLife = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(32);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	
	private ZonedDecimalData wsaaHdlAdj = new ZonedDecimalData(17, 2).init(ZERO);//BRD-34
	private ZonedDecimalData wsaaHdlAmt = new ZonedDecimalData(17, 2).init(ZERO);//BRD-34
		/* ERRORS */
	private static final String e304 = "E304";
	private static final String h073 = "H073";
	private static final String f616 = "F616";
	private static final String f990 = "F990";
	private static final String e923 = "E923";
	private static final String g620 = "G620";
	private static final String f910 = "F910";
	protected static final String f572 = "F572";
	private static final String rfik = "RFIK";
	private static final String T064 = "T064";
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5500 = "T5500";
	private static final String clmhclmrec = "CLMHCLMREC";
	private static final String cltsrec = "CLTSREC";
	protected ChdrclmTableDAM chdrclmIO = new ChdrclmTableDAM();
	private ClmdaddTableDAM clmdaddIO = new ClmdaddTableDAM();
	protected ClmdclmTableDAM clmdclmIO = new ClmdclmTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrclmTableDAM covrclmIO = new CovrclmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FlupclmTableDAM flupclmIO = new FlupclmTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeclmTableDAM lifeclmIO = new LifeclmTableDAM();
	private ZhlcpfDAO zhlcpfDAO = new ZhlcpfDAOImpl(); /*BRD-34*/
	private ZhlbpfDAO zhlbpfDAO = new ZhlbpfDAOImpl();/*BRD-34*/
	protected Batckey wsaaBatckey = new Batckey();
	private Atreqrec atreqrec = new Atreqrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected Gensswrec gensswrec = new Gensswrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5318ScreenVars sv = getLScreenVars();
	private WsaaTransactionRecInner wsaaTransactionRecInner = new WsaaTransactionRecInner();
//	MIBT-181
	private ErrorsInner errorsInner = new ErrorsInner();	
	
	//added for death claim flexibility		
	private T5688rec t5688rec = new T5688rec();
	private Dthclmflxrec dthclmflxRec =  new Dthclmflxrec();
	private static final String e308 = "E308";
	//end added for death claim flexibility
	//BRD-34 starts
		private Zhlcpf zhlcpf = new Zhlcpf();
		private Zhlbpf zhlbpf = new Zhlbpf();
	  	private List<Zhlcpf> zhlcpfList= null;
	  	private List<Zhlbpf> zhlbpfList= null;
	//BRD-34 ends
	  	
	  //ILB-459 start
		 protected Chdrpf chdrpf = new Chdrpf();
		 protected ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);
	
	boolean CMDTH006Permission  = false;
	private Sd5jlScreenVars sd5jl = ScreenProgram.getScreenVars(Sd5jlScreenVars.class);
	private ZonedDecimalData wsaaTotalClaim = new ZonedDecimalData(17, 2).init(ZERO);
    private CpbnfypfDAO cpbnfypfDAO = getApplicationContext().getBean("cpbnfypfDAO", CpbnfypfDAO.class);
    private NotipfDAO notipfDAO = getApplicationContext().getBean("NotipfDAO", NotipfDAO.class);
	private Notipf notipf=null;
	private List<Clnnpf> clnnpfList1 = new ArrayList<Clnnpf>();
	private ClnnpfDAO clnnpfDAO= getApplicationContext().getBean("clnnpfDAO",ClnnpfDAO.class);
    private static final String feaConfigDefIntrCalc= "CMOTH002";
	private boolean defintrFlag = false;

	private boolean CMOTH003Permission  = false;
	private static final String feaConfigClmNotify= "CMOTH003";
	public static final String CLMPREFIX = "CLMNTF";
	private List<Invspf> invspfList1 = new ArrayList<Invspf>();
	private InvspfDAO invspfDAO= getApplicationContext().getBean("invspfDAO",InvspfDAO.class);
	private FixedLengthStringData wsaaNotifinum = new FixedLengthStringData(8);

	protected AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class); // ILIFE-8394
	protected Acblpf acblpf; // ILIFE-8394
	protected CattpfDAO cattpfDAO = getApplicationContext().getBean("cattpfDAO",CattpfDAO.class); // ILIFE-8394
	protected Cattpf cattpf; // ILIFE-8394

	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	private CrsvpfDAO crsvpfDAO = getApplicationContext().getBean("crsvpfDAO", CrsvpfDAO.class);
	boolean CMDTH010Permission = false;
	
	protected static final String h903 = "H903";
	protected static final String jl58 = "JL58";
	protected static final String jl67 = "JL67";
	protected static final String rfs8 = "RFS8";//ILJ-434
	private ClmhpfDAO clmhpfDAO = getApplicationContext().getBean("clmhpfDAO", ClmhpfDAO.class);
	private Clmhpf clmhclm = null;
	private Long wsaaUniqueNumber;
/**	
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		continue1030, 
		exit1090, 
		validateSelectionFields2070, 
		exit2090
	}

	public P5318() {
		super();
		screenVars = sv;
		new ScreenModel("S5318", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case continue1030: 
					continue1030();
				case exit1090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		CMDTH006Permission  = FeaConfg.isFeatureExist("2", "CMDTH006", appVars, "IT");
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		defintrFlag = FeaConfg.isFeatureExist("2", feaConfigDefIntrCalc, appVars, "IT");
		CMOTH003Permission  = FeaConfg.isFeatureExist("2",feaConfigClmNotify, appVars, "IT");
		CMDTH010Permission  = FeaConfg.isFeatureExist("2", "CMDTH010", appVars, "IT");
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		/* Initialise working storage variables.                           */
		wsaaTransactionRecInner.wsaaClamamtOld.set(ZERO);
		wsaaTransactionRecInner.wsaaClamamtNew.set(ZERO);
		wsaaTransactionRecInner.wsaaOtheradjst.set(ZERO);
		wsaaX.set(ZERO);
		wsaaY.set(ZERO);
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		sv.zhldclma.set(ZERO);//BRD-34
		sv.zhldclmv.set(ZERO);//BRD-34

		/* Skip this section if returning from an optional selection,*/
		/* i.e. check the stack action flag equals '*'.*/
		/* MOVE WSSP-BATCHKEY          TO WSAA-BATCKEY.                 */
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*    GO TO 1090-EXIT.                                          */
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
		if (!defintrFlag) {
			sv.defintflag.set("N");
		}
		else {
			sv.defintflag.set("Y");
		}
		scrnparams.function.set(varcom.sclr);
		processScreen("S5318", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*    Dummy field initilisation for prototype version.*/
		sv.estimateTotalValue.set(ZERO);
		sv.otheradjst.set(ZERO);
		sv.policyloan.set(ZERO);
		sv.tdbtamt.set(ZERO);
		sv.zrcshamt.set(ZERO);
		sv.clamamt.set(ZERO);
		sv.estMatValue.set(ZERO);
		sv.interest.set(ZERO);
		sv.ofcharge.set(ZERO);
		sv.totclaim.set(ZERO);
		sv.susamt.set(ZERO);
		sv.nextinsamt.set(ZERO);
		sv.actvalue.set(ZERO);
		sv.interestrate.set(ZERO);
		sv.dtofdeath.set(varcom.vrcmMaxDate);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.btdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		sv.contactDate.set(varcom.vrcmMaxDate);
		//ILB-459 start
		/*chdrclmIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrclmIO);
		if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrclmIO.getParams());
			fatalError600();
		}
		*/
		
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrclmIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrclmIO);
			if (isNE(chdrclmIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrclmIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrclmIO.getChdrcoy().toString(), chdrclmIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		//ILB-459 end
		/* Currency Code on the screen now defaults to Contract Currency   */
		sv.currcd.set(chdrpf.getCntcurr());
		wsaaTransactionRecInner.wsaaCurrcd.set(chdrpf.getCntcurr());
		sv.occdate.set(chdrpf.getOccdate());
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setDesctabl(t5688);
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		else {
			sv.ctypedes.fill("?");
		}
		sv.cownnum.set(chdrpf.getCownnum());
		cltsIO.setClntnum(chdrpf.getCownnum());
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		sv.btdate.set(chdrpf.getBtdate());
		sv.ptdate.set(chdrpf.getPtdate());
		/*    Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*  Look up premium status*/
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrpf.getPstcde());
		findDesc1300();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/* read claim header record*/
		
		clmhclm = new Clmhpf();
		clmhclm = clmhpfDAO.getClmhpfH(chdrpf.getChdrcoy().toString(),chdrpf.getChdrnum());
		if(null==clmhclm) {
			fatalError600();
		}
		wsaaUniqueNumber = clmhclm.getUniqueNumber();
		if (isEQ(clmhclm.getJlife(), "01")) {
			sv.astrsk.set("*");
		}
		else {
			sv.asterisk.set("*");
		}
		if(!CMOTH003Permission){
			sv.aacctOut[varcom.nd.toInt()].set("Y");
//			sv.claimnumberOut[varcom.nd.toInt()].set("Y");
			sv.claimnotesOut[varcom.nd.toInt()].set("Y");
			sv.investresOut[varcom.nd.toInt()].set("Y");
		}
		
		if(!CMDTH010Permission && !CMOTH003Permission) {
			sv.claimnumberOut[varcom.nd.toInt()].set("Y");
		}
		
		if(!CMDTH010Permission) {
			sv.claimStatOut[varcom.nd.toInt()].set("Y");
			sv.claimTypOut[varcom.nd.toInt()].set("Y");
			sv.contactDateOut[varcom.nd.toInt()].set("Y");
		}
		
		sv.dtofdeath.set(clmhclm.getDtofdeath());
		sv.effdate.set(clmhclm.getEffdate());
		sv.causeofdth.set(clmhclm.getCauseofdth());
		sv.reasoncd.set(clmhclm.getReasoncd());
		if(CMDTH010Permission){
		cattpf = cattpfDAO.selectRecords(chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum());
		if(null == cattpf.getClaim()){
			sv.contactDate.set(varcom.vrcmMaxDate);
			sv.claimStat.set("PR");
			sv.claimTyp.set("DC");
		}
		else{
		sv.claimnumber.set(cattpf.getClaim());	
		sv.claimStat.set(cattpf.getClamstat());
		sv.claimTyp.set(cattpf.getClamtyp());
		sv.contactDate.set(clmhclm.getCondte().toString());//ILJ-766
		}
		}
		if(defintrFlag && isEQ(wsspcomn.sbmaction, "E")){
			if(null == clmhclm.getInterestrate()) {
				sv.interestrate.set(SPACES);
			}
			else
				sv.interestrate.set(clmhclm.getInterestrate());
		}
		/* MOVE CLMHCLM-RESNDESC       TO S5318-RESNDESC.               */
		sv.longdesc.set(clmhclm.getResndesc());
		/*    MOVE CLMHCLM-POLICYLOAN     TO S5318-POLICYLOAN,             */
		/*                                   WSAA-POLICYLOAN.              */
		sv.policyloan.set(clmhclm.getPolicyloan());
		sv.tdbtamt.set(clmhclm.getTdbtamt());
		sv.zrcshamt.set(clmhclm.getZrcshamt());
		/*    MOVE CLMHCLM-OTHERADJST     TO S5318-OTHERADJST,*/
		/*                                   WSAA-OTHERADJST.*/
		sv.interest.set(clmhclm.getInterest());
		sv.ofcharge.set(clmhclm.getOfcharge());
		/* The WSAA-OTHERADJST field should be set up as what the          */
		/* other adjustments originally were, i.e. whats on the Claim      */
		/* Header File (CLMHCLM) at this point                             */
		wsaaTransactionRecInner.wsaaOtheradjst.set(clmhclm.getOtheradjst());
		sv.otheradjst.set(clmhclm.getOtheradjst());
		/*MOVE CLMHCLM-CURRCD         TO S5318-CURRCD,                 */
		/*WSAA-CURRCD.                  */
		/*    Read the first life as the life retrieved above*/
		/*    may have been a joint life.*/
		lifeclmIO.setDataArea(SPACES);
		lifeclmIO.setChdrcoy(clmhclm.getChdrcoy());
		lifeclmIO.setChdrnum(clmhclm.getChdrnum());
		lifeclmIO.setLife(clmhclm.getLife());
		lifeclmIO.setJlife(SPACES);
		lifeclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		lifeclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifeclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE");
		SmartFileCode.execute(appVars, lifeclmIO);
		if (isNE(lifeclmIO.getStatuz(), varcom.oK)
		&& isNE(lifeclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isNE(clmhclm.getChdrcoy(), lifeclmIO.getChdrcoy())
		|| isNE(clmhclm.getChdrnum(), lifeclmIO.getChdrnum())
		|| isNE(clmhclm.getLife(), lifeclmIO.getLife())
		|| isNE(lifeclmIO.getJlife(), "00")
		&& isNE(lifeclmIO.getJlife(), "  ")
		|| isEQ(lifeclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		/* Move the last record read to the screen and get the*/
		/* description.*/
		sv.lifcnum.set(lifeclmIO.getLifcnum());
		wsaaDeadLife.set(lifeclmIO.getLifcnum());
		cltsIO.setClntnum(lifeclmIO.getLifcnum());
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.linsnameErr.set(e304);
			sv.linsname.set(SPACES);
		}
		else {
			plainname();
			sv.linsname.set(wsspcomn.longconfname);
		}
		/*    look for joint life.*/
		lifeclmIO.setJlife("01");
		lifeclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeclmIO);
		if ((isNE(lifeclmIO.getStatuz(), varcom.oK))
		&& (isNE(lifeclmIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(lifeclmIO.getParams());
			fatalError600();
		}
		if (isEQ(lifeclmIO.getStatuz(), varcom.mrnf)) {
			lifeclmIO.setJlife(SPACES);
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			goTo(GotoLabel.continue1030);
		}
		sv.jlifcnum.set(lifeclmIO.getLifcnum());
		cltsIO.setClntnum(lifeclmIO.getLifcnum());
		if (isEQ(clmhclm.getJlife(), "01")) {
			wsaaDeadLife.set(lifeclmIO.getLifcnum());
		}
		getClientDetails1400();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.jlinsnameErr.set(e304);
			sv.jlinsname.set(SPACES);
		}
		else {
			plainname();
			sv.jlinsname.set(wsspcomn.longconfname);
		}
	}

protected void continue1030()
	{
		/*  find each coverage or rider attached to the death*/
		/*  by reading the CLMDCLM file*/
		clmdclmIO.setDataArea(SPACES);
		clmdclmIO.setChdrcoy(chdrpf.getChdrcoy());
		clmdclmIO.setChdrnum(chdrpf.getChdrnum());
		setLifeAsClmdParam();
		clmdclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		clmdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		clmdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		setLifeAsClmdSearchKey();
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.endp)
		&& isNE(clmdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), clmdclmIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), clmdclmIO.getChdrnum())
		|| isEQ(clmdclmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		/*  store currency*/
		wsaaStoredCurrency.set(clmdclmIO.getCnstcur());
		while ( !(isEQ(clmdclmIO.getStatuz(), varcom.endp))) {
			processClmdclm1500();
		}
		
		/* MOVE ZERO                   TO S5318-POLICYLOAN.             */
		if (detailsSameCurrency.isTrue()) {
			sv.estimateTotalValue.set(wsaaEstimateTot);
			/*      COMPUTE S5318-CLAMAMT = WSAA-ACTUAL-TOT +               */
			/*         S5318-POLICYLOAN + S5318-OTHERADJST                  */
			/* COMPUTE S5318-CLAMAMT = WSAA-ACTUAL-TOT              <CAS1.0>*/
			/*       - S5318-POLICYLOAN + S5318-OTHERADJST          <CAS1.0>*/
			/*BRD-34 starts*/
			//Get Hold Claim Adjustment
			wsaaHdlAdj.set(ZERO);
			wsaaHdlAmt.set(ZERO);
			CalCovAmt1600();
			CalBeneAmt1700();
			sv.zhldclmv.set(wsaaHdlAdj.toDouble() + wsaaHdlAmt.toDouble() );
			sv.zhldclma.set(wsaaHdlAdj);		
			/*BRD-34 ends*/

			compute(sv.clamamt, 2).set(sub(add(add(add(wsaaActualTot, sv.policyloan), sv.otheradjst), sv.zrcshamt), sv.tdbtamt));
			CalcCustomerSpecifc();
			if ((isEQ(wsaaActualTot, ZERO)
			&& isNE(wsaaEstimateTot, ZERO))) {
				/*CONTINUE_STMT*/
			}
			else {
				//ILIFE-1137
				//added for death claim flexibility BRSO
				readTabT5688250();
				if (isNE(t5688rec.dflexmeth,SPACES)){
					dthclmflxRec.chdrChdrcoy.set(chdrpf.getChdrcoy());
					dthclmflxRec.chdrChdrnum.set(chdrpf.getChdrnum());			
					dthclmflxRec.ptdate.set(sv.ptdate);
					if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
						dthclmflxRec.dtofdeath.set(sv.dtofdeath);
					}else{
						datcon1rec.function.set(varcom.tday);
						Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
						wsaaToday.set(datcon1rec.intDate);				
						dthclmflxRec.dtofdeath.set(wsaaToday);
					}					
					dthclmflxRec.btdate.set(sv.btdate);
					dthclmflxRec.cnttype.set(sv.cnttype);
					dthclmflxRec.register.set(chdrpf.getReg());
					dthclmflxRec.cntcurr.set(chdrpf.getCntcurr());
					dthclmflxRec.effdate.set(chdrpf.getCurrfrom());
					callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
					if (isNE(dthclmflxRec.status, varcom.oK)) {
						syserrrec.statuz.set(dthclmflxRec.status);
						syserrrec.params.set(dthclmflxRec.dthclmflxrec);
						fatalError600();
					}	
					sv.susamt.set(dthclmflxRec.susamt);
					sv.nextinsamt.set(dthclmflxRec.nextinsamt);
				}	
				///ILIFE-8394 starts
				else{
					   acblpf = acblDao.getAcblpfRecord(chdrpf.getChdrcoy().toString(), "LP", chdrpf.getChdrnum().trim(), /* IJTI-1523 */
			                   "S", null);
			                   if(acblpf!=null){
			                          BigDecimal suspenseamt = acblpf.getSacscurbal();
			                          sv.susamt.set(suspenseamt);             
			                   }
				}
				//ILIFE-8394 ends
				retrieveSuspenseCustomerSpecifc();		
				//compute(sv.totclaim, 2).set((sub(add(sv.clamamt, sv.interest), sv.ofcharge,sv.susamt,sv.nextinsamt)));
				compute(sv.totclaim, 2).set((sub(add(sv.clamamt, sv.interest), sv.ofcharge,sv.susamt,sv.nextinsamt,wsaaHdlAmt.toDouble(),wsaaHdlAdj.toDouble())));//BRD-34
				customerSpecificPensionCalc();
				sv.susamt.set(mult(sv.susamt,-1));
			}
			sv.currcd.set(wsaaStoredCurrency);
		}
		checkFollowups1850();
		wsaaTransactionRecInner.wsaaClamamtOld.set(sv.clamamt);
		if (isEQ(wsspcomn.flag, "I")) {
			sv.dtofdeathOut[varcom.pr.toInt()].set("Y");
			sv.causeofdthOut[varcom.pr.toInt()].set("Y");
			sv.reasoncdOut[varcom.pr.toInt()].set("Y");
			/*     MOVE 'Y'                TO S5318-LONGDESC-OUT(PR)        */
			sv.longdescOut[varcom.pr.toInt()].set("Y");
			sv.currcdOut[varcom.pr.toInt()].set("Y");
			sv.otheradjstOut[varcom.pr.toInt()].set("Y");
			sv.contactDateOut[varcom.pr.toInt()].set("Y");
		}
		/*  This next line has been removed from the above IF to         */
		/*  ensure that the field is always protected.                   */
		sv.effdateOut[varcom.pr.toInt()].set("Y");
	}
//BRD-34 starts
protected void  CalCovAmt1600() {
	/*	ILIFE-3135*/


	zhlcpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	zhlcpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1523 */
	/*ILIFE-3125 starts*/
	zhlcpf.setValidflag("1");
	zhlcpfList=zhlcpfDAO.readZhlcpfData(zhlcpf);
	
		
	if (zhlcpfList== null || zhlcpfList.size()<=0) {
	
			return ;
		}
	
	/*Iterator<Zhlcpf> lsIterator =zhlcpfList.iterator();
	while(lsIterator.hasNext())
	{
		
		wsaaHdlAdj.add(zhlcpf.getZhldclma().doubleValue());
		wsaaHdlAmt.add(zhlcpf.getZhldclmv().doubleValue());
		lsIterator.next();
	
	}*/
	for(Zhlcpf zhlcpf: zhlcpfList)
	{
		wsaaHdlAdj.add(zhlcpf.getZhldclma().doubleValue());
		wsaaHdlAmt.add(zhlcpf.getZhldclmv().doubleValue());
	}
	/*ILIFE-3125 ends*/
}

protected void  CalBeneAmt1700() {
	
	zhlbpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	zhlbpf.setChdrnum(chdrpf.getChdrnum());/* IJTI-1523 */
	zhlbpf.setValidflag("1");
	zhlbpfList = zhlbpfDAO.readZhlbpfData(zhlbpf);
	if (zhlbpfList==null || zhlbpfList.size()<=0) {
		syserrrec.statuz.set(Varcom.mrnf);
			return ;
		}

	/*Iterator<Zhlbpf> lsIterator =zhlbpfList.iterator();
	while(lsIterator.hasNext())
	{
		
		wsaaHdlAdj.add(zhlbpf.getZhldclma().doubleValue());
		wsaaHdlAmt.add(zhlbpf.getZhldclmv().doubleValue());
		lsIterator.next();
		
		
	}*/
	for(Zhlbpf zhlbpf: zhlbpfList)
	{
		wsaaHdlAdj.add(zhlbpf.getZhldclma().doubleValue());
		wsaaHdlAmt.add(zhlbpf.getZhldclmv().doubleValue());
	}
		
} /*	ILIFE-3135*/
//BRD-34 ENDS

protected void findDesc1300()
	{
		/*READ*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClientDetails1400()
	{
		/*READ*/
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void processClmdclm1500()
	{
		read1510();
	}

protected void read1510()
	{
		SmartFileCode.execute(appVars, clmdclmIO);
		if (isNE(clmdclmIO.getStatuz(), varcom.endp)
		&& isNE(clmdclmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clmdclmIO.getParams());
			fatalError600();
		}
		if (isNE(chdrpf.getChdrcoy(), clmdclmIO.getChdrcoy())
		|| isNE(chdrpf.getChdrnum(), clmdclmIO.getChdrnum())
		|| isEQ(clmdclmIO.getStatuz(), varcom.endp)) {
			clmdclmIO.setStatuz(varcom.endp);
			return ;
		}
		if(CMOTH003Permission){
			sv.claimnumber.set(clmdclmIO.getClaimno());
			if(isNE(clmdclmIO.getClaimnotifino(),SPACES))
				sv.aacct.set(CLMPREFIX+clmdclmIO.getClaimnotifino());
		}
		sv.coverage.set(clmdclmIO.getCoverage());
		sv.hcover.set(clmdclmIO.getCoverage());
		if (isEQ(clmdclmIO.getRider(), "00")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(clmdclmIO.getRider());
		}
		if (isNE(clmdclmIO.getRider(), "00")
		&& isNE(clmdclmIO.getRider(), "  ")) {
			sv.coverage.set(SPACES);
		}
		/*    MOVE CLMDCLM-CRTABLE       TO S5318-CRTABLE.*/
		sv.vfund.set(clmdclmIO.getVirtualFund());
		sv.fieldType.set(clmdclmIO.getFieldType());
		sv.shortds.set(clmdclmIO.getShortds());
		sv.liencd.set(clmdclmIO.getLiencd());
		/*  MOVE CLMDCLM-RIIND         TO S5318-RIIND.                   */
		sv.cnstcur.set(clmdclmIO.getCnstcur());
		sv.hcnstcur.set(clmdclmIO.getCnstcur());
		sv.estMatValue.set(clmdclmIO.getEstMatValue());
		sv.hemv.set(clmdclmIO.getEstMatValue());
		sv.actvalue.set(clmdclmIO.getActvalue());
		sv.hactual.set(clmdclmIO.getActvalue());
		if (isEQ(clmdclmIO.getCnstcur(), wsaaStoredCurrency)) {
			wsaaEstimateTot.add(clmdclmIO.getEstMatValue());
			wsaaActualTot.add(clmdclmIO.getActvalue());
		}
		else {
			wsaaCurrencySwitch.set(1);
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5318", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		clmdclmIO.setFunction(varcom.nextr);
	}

protected void checkFollowups1850()
	{
		read1860();
	}

protected void read1860()
	{
		/*   Check for any follow-ups.*/
		flupclmIO.setDataKey(SPACES);
		flupclmIO.setChdrcoy(chdrpf.getChdrcoy());
		flupclmIO.setChdrnum(chdrpf.getChdrnum());
		flupclmIO.setFupno(ZERO);
		flupclmIO.setFunction(varcom.begn);
		//performance improvement --  Niharika Modi 
		flupclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		flupclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, flupclmIO);
		if ((isNE(flupclmIO.getStatuz(), varcom.oK))
		&& (isNE(flupclmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		if ((isNE(flupclmIO.getChdrcoy(), chdrpf.getChdrcoy()))
		|| (isNE(flupclmIO.getChdrnum(), chdrpf.getChdrnum()))
		|| (isEQ(flupclmIO.getStatuz(), varcom.endp))) {
			sv.fupflg.set(SPACES);
		}
		else {
			/*  There are some follow-ups*/
			sv.fupflg.set("+");
		}
	}

protected void largename() {
	/* LGNM-100 */
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(cltsIO.getClttype(), "C")) {
		corpname();
	} else {
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}
	/* LGNM-EXIT */
}

protected void plainname() {
	/* PLAIN-100 */
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(cltsIO.getClttype(), "C")) {
		corpname();

	} else if (isNE(cltsIO.getGivname(), SPACES)) {
		String firstName = cltsIO.getGivname().toString();
		String lastName = cltsIO.getSurname().toString();
		String delimiter = ",";

		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);

	} else {
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}
	/* PLAIN-EXIT */
}

protected void payeename() {
	/* PAYEE-100 */
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(cltsIO.getClttype(), "C")) {
		corpname();
	} else if (isEQ(cltsIO.getEthorig(), "1")) {
		String firstName = cltsIO.getGivname().toString();
		String lastName = cltsIO.getSurname().toString();
		String delimiter = "";
		String salute = cltsIO.getSalutl().toString();

		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		String fullNameWithSalute = getStringUtil().includeSalute(salute, fullName);

		wsspcomn.longconfname.set(fullNameWithSalute);
	} else {
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
	}
	/* PAYEE-EXIT */
}
protected void corpname() {
	/* PAYEE-1001 */
	wsspcomn.longconfname.set(SPACES);
	/* STRING CLTS-SURNAME DELIMITED SIZE */
	/* CLTS-GIVNAME DELIMITED ' ' */
	String firstName = cltsIO.getLgivname().toString();
	String lastName = cltsIO.getLsurname().toString();
	String delimiter = "";

	// this way we can override StringUtil behaviour in formatName()
	String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
	wsspcomn.longconfname.set(fullName);
	/* CORP-EXIT */
}
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		if (CMDTH006Permission) {
			sv.bnfyingOut[varcom.nd.toInt()].set("N");
		} else {
			sv.bnfyingOut[varcom.nd.toInt()].set("Y");
		}
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateScreen2010();
				case validateSelectionFields2070: 
					validateSelectionFields2070();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5318IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5318-DATA-AREA                         */
		/*                         S5318-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if(CMOTH003Permission){
			if(isNE(sv.aacct,SPACES)){
				wsaaNotifinum.set(sv.aacct.toString().replace(CLMPREFIX, ""));
			}
			else{
				wsaaNotifinum.set(SPACES);
			}
			clnnpfList1 =  clnnpfDAO.getClnnpfList(wsspcomn.company.toString(), wsaaNotifinum.toString(),sv.claimnumber.toString().trim());
			invspfList1 =  invspfDAO.getInvspfList(wsspcomn.company.toString(), wsaaNotifinum.toString(),sv.claimnumber.toString().trim());			
		}
	}

protected void validateScreen2010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.validateSelectionFields2070);
		}
		/*Date of Death field is mandatory for Death Claim Adjustment
		MIBT-181*/
		if(isEQ(sv.dtofdeathDisp, SPACES)){
	    	sv.dtofdeathErr.set(errorsInner.e186);
	    }
		if (isEQ(sv.reasoncd,SPACES) && isEQ(wsspcomn.flag, "M")) {
			sv.reasoncdErr.set(errorsInner.e186);
		}
		if(CMDTH010Permission)
		{
		if(isEQ(sv.contactDateDisp, SPACES)){
	    	sv.contactDateErr.set(errorsInner.e186);
	    }
		//ILJ-434 starts
		if (isEQ(sv.contactDate, 0)) {
			sv.contactDateErr.set(h903);
		} 
		else {
			datcon1rec.function.set(Varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);	
			
			if((sv.contactDate.equals(SPACES))||(sv.contactDate.equals(varcom.vrcmMaxDate)) && isEQ(sv.contactDateDisp, SPACES)){
				sv.contactDateErr.set(errorsInner.e186);
			}

			if(isLT(sv.contactDate, sv.occdate)){
				sv.contactDateErr.set(jl58);
			}

			if(isGT(sv.contactDate,wsaaToday)&&isNE(sv.contactDate,varcom.vrcmMaxDate)){
				sv.contactDateErr.set(rfs8); 
			}
			
			if(isLT(sv.contactDate,sv.dtofdeath)){
				sv.contactDateErr.set(jl67);
			}
		}
		
		if (isEQ(sv.dtofdeath, 0)) {
			sv.dtofdeathErr.set(h903);
		}
		else{
			/*       if date of death entered it must not be less than*/
			/*       the original commencment date and not > than today*/
			if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
				datcon1rec.function.set(varcom.tday);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				wsaaToday.set(datcon1rec.intDate);
				if (isGT(sv.dtofdeath, wsaaToday)) {
					sv.dtofdeathErr.set(e923);
				}
				else {
					if (isLT(sv.dtofdeath, chdrpf.getOccdate())) {
						/*            MOVE U041          TO S5318-DTOFDEATH-ERR.         */
						sv.dtofdeathErr.set(h073);
					}
				}
			}
		}
	}
		/*       effective date must be entered - should be not less than*/
		/*       the ccd and not less than date of death.*/
		if (isLT(sv.effdate, chdrpf.getOccdate())) {
			sv.effdateErr.set(f616);
		}
		else {
			if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
				if (isLT(sv.effdate, sv.dtofdeath)) {
					sv.effdateErr.set(T064);
				}
			}
		}
		if (isNE(sv.reasoncd, SPACES)) {
			/*     IF S5318-RESNDESC = SPACES                               */
			if (isEQ(sv.longdesc, SPACES)) {
				scrnparams.statuz.set(varcom.calc);
				descIO.setDesctabl(t5500);
				descIO.setDescitem(sv.reasoncd);
				findDesc1300();
				if (isEQ(descIO.getStatuz(), varcom.oK)) {
					/*         MOVE DESC-LONGDESC      TO S5318-RESNDESC            */
					sv.longdesc.set(descIO.getLongdesc());
				}
				else {
					/*         MOVE ALL '?'            TO S5318-RESNDESC.           */
					sv.longdesc.fill("?");
				}
			}
		}
		/*      COMPUTE S5318-CLAMAMT = WSAA-ACTUAL-TOT +               */
		/*         S5318-OTHERADJST +                                   */
		/*         S5318-POLICYLOAN.                                    */
		if (isNE(sv.otheradjst, ZERO)) {
			zrdecplrec.amountIn.set(sv.otheradjst);
			callRounding5000();
			if (isNE(zrdecplrec.amountOut, sv.otheradjst)) {
				sv.otheradjstErr.set(rfik);
			}
		}
		compute(sv.clamamt, 2).set(sub(add(add(add(wsaaActualTot, sv.otheradjst), sv.zrcshamt), sv.policyloan), sv.tdbtamt));
		CalcCustomerSpecifc();
		wsaaTransactionRecInner.wsaaClamamtNew.set(sv.clamamt);
		if ((isEQ(wsaaActualTot, ZERO)
		&& isNE(wsaaEstimateTot, ZERO))) {
			/*CONTINUE_STMT*/
		}
		else {
			//ILIFE-1137
			//added for death claim flexibility BRSO
			readTabT5688250();
			if (isNE(t5688rec.dflexmeth,SPACES)){
				dthclmflxRec.chdrChdrcoy.set(chdrpf.getChdrcoy());
				dthclmflxRec.chdrChdrnum.set(chdrpf.getChdrnum());			
				dthclmflxRec.ptdate.set(sv.ptdate);
				if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
					dthclmflxRec.dtofdeath.set(sv.dtofdeath);
				}else{
					datcon1rec.function.set(varcom.tday);
					Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
					wsaaToday.set(datcon1rec.intDate);				
					dthclmflxRec.dtofdeath.set(wsaaToday);
				}				
				dthclmflxRec.btdate.set(sv.btdate);
				dthclmflxRec.cnttype.set(sv.cnttype);
				dthclmflxRec.register.set(chdrpf.getReg());
				dthclmflxRec.cntcurr.set(chdrpf.getCntcurr());
				dthclmflxRec.effdate.set(chdrpf.getCurrfrom());
				
				callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
				if (isNE(dthclmflxRec.status, varcom.oK)) {
					syserrrec.statuz.set(dthclmflxRec.status);
					syserrrec.params.set(dthclmflxRec.dthclmflxrec);
					fatalError600();
				}	
				sv.susamt.set(dthclmflxRec.susamt);
				sv.nextinsamt.set(dthclmflxRec.nextinsamt);
			}
			retrieveSuspenseCustomerSpecifc();	
			//modified for death claim flexibility BRSO
			//compute(sv.totclaim, 2).set((sub(add(sv.clamamt, sv.interest), sv.ofcharge)));
			compute(sv.totclaim, 2).set((sub(add(sv.clamamt, sv.interest), sv.ofcharge, sv.susamt, sv.nextinsamt)));
			customerSpecificPensionCalc();
			sv.susamt.set(mult(sv.susamt,-1));
		}
		/* Default Currency Code on the screen to the Contract Currency    */
		/*IF S5318-CURRCD                 = SPACES                     */
		/*MOVE CHDRCLM-CNTCURR        TO S5318-CURRCD.             */
		sv.currcd.set(chdrpf.getCntcurr());
		//added for death claim flexibility BRSO
		/*  check fields entered are the same as those of the*/
		/*  claims header record.*/
		if ((isNE(sv.effdate, clmhclm.getEffdate()))
		|| (isNE(sv.dtofdeath, clmhclm.getDtofdeath()))
		|| (isNE(sv.otheradjst, clmhclm.getOtheradjst()))
		|| (isNE(sv.causeofdth, clmhclm.getCauseofdth()))
		|| (isNE(sv.reasoncd, clmhclm.getReasoncd()))
		|| (isNE(sv.longdesc, clmhclm.getResndesc()))
		|| (isNE(sv.currcd, clmhclm.getCurrcd()))
		|| (CMDTH010Permission && isNE(sv.contactDate.toInt(), cattpf.getEffdate()))) {//ILJ-383
			wsaaChanged = "Y";
		}
		if (isNE(sv.currcd, wsaaStoredCurrency)) {
			wsaaStoredCurrency.set(sv.currcd);
			wsaaCurrencySwitch.set(3);
			wsspcomn.edterror.set("Y");
			readjustCurrencies2100();
			sv.estimateTotalValue.set(wsaaEstimateTot);
			/*      COMPUTE S5318-CLAMAMT = WSAA-ACTUAL-TOT +*/
			/*         S5318-OTHERADJST   + S5318-POLICYLOAN                */
			/*         S5318-OTHERADJST   - S5318-POLICYLOAN                */
			/*      COMPUTE S5318-CLAMAMT = WSAA-ACTUAL-TOT +               */
			/*         S5318-OTHERADJST   + S5318-POLICYLOAN                */
			compute(sv.clamamt, 2).set(sub(add(add(add(wsaaActualTot, sv.otheradjst), sv.policyloan), sv.zrcshamt), sv.tdbtamt));
			wsaaTransactionRecInner.wsaaClamamtNew.set(sv.clamamt);
		}
		if (isLT(sv.clamamt, ZERO)) {
			sv.otheradjstErr.set(f572);
		}
		if ((isEQ(wsaaActualTot, ZERO)
		&& isNE(wsaaEstimateTot, ZERO))) {
			/*CONTINUE_STMT*/
		}
		else {
			//ILIFE-1137
			//added for death claim flexibility BRSO
			readTabT5688250();
			if (isNE(t5688rec.dflexmeth,SPACES)){
				dthclmflxRec.chdrChdrcoy.set(chdrpf.getChdrcoy());
				dthclmflxRec.chdrChdrnum.set(chdrpf.getChdrnum());			
				dthclmflxRec.ptdate.set(sv.ptdate);
				if (isNE(sv.dtofdeath, varcom.vrcmMaxDate)) {
					dthclmflxRec.dtofdeath.set(sv.dtofdeath);
				}else{
					datcon1rec.function.set(varcom.tday);
					Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
					wsaaToday.set(datcon1rec.intDate);				
					dthclmflxRec.dtofdeath.set(wsaaToday);
				}				
				dthclmflxRec.btdate.set(sv.btdate);
				dthclmflxRec.cnttype.set(sv.cnttype);
				dthclmflxRec.register.set(chdrpf.getReg());
				dthclmflxRec.cntcurr.set(chdrpf.getCntcurr());
				dthclmflxRec.effdate.set(chdrpf.getCurrfrom());
				callProgram(t5688rec.dflexmeth, dthclmflxRec.dthclmflxrec);
				if (isNE(dthclmflxRec.status, varcom.oK)) {
					syserrrec.statuz.set(dthclmflxRec.status);
					syserrrec.params.set(dthclmflxRec.dthclmflxrec);
					fatalError600();
				}	
				sv.susamt.set(dthclmflxRec.susamt);
				sv.nextinsamt.set(dthclmflxRec.nextinsamt);
			}
			retrieveSuspenseCustomerSpecifc();	
			//modified for death claim flexibility BRSO
			//compute(sv.totclaim, 2).set((sub(add(sv.clamamt, sv.interest), sv.ofcharge)));
			compute(sv.totclaim, 2).set((sub(add(sv.clamamt, sv.interest), sv.ofcharge,sv.susamt, sv.nextinsamt)));
			customerSpecificPensionCalc();
			sv.susamt.set(mult(sv.susamt,-1));
		}
		balErrCustomerSpecific2010();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

	protected void validateSelectionFields2070()
	{
		if (isNE(sv.fupflg, "X")
		&& isNE(sv.fupflg, "+")
		&& isNE(sv.fupflg, " ")
		&& isNE(sv.fupflg, "?")) {
			sv.fupflgErr.set(g620);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)
		|| isEQ(scrnparams.statuz, wsaaChanged)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			return ;
		}
		if (isNE(sv.bnfying, "X")
				&& isNE(sv.bnfying, "+")
				&& isNE(sv.bnfying, " ")) {
			sv.bnfyingErr.set(errorsInner.g620);
		}
		if(CMOTH003Permission){
		if (isNE(sv.investres, "X")
				&& isNE(sv.investres, "+")
				&& isNE(sv.investres, " ")) {
			sv.investresErr.set(g620);
		}
		if (isNE(sv.claimnotes, "X")
				&& isNE(sv.claimnotes, "+")
				&& isNE(sv.claimnotes, " ")) {
			sv.claimnotesErr.set(g620);
		}
		}
	}

protected void readjustCurrencies2100()
	{
		/*G0*/
		wsaaEstimateTot.set(ZERO);
		wsaaActualTot.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5318", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile2200();
		}
		
		if (isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void readSubfile2200()
	{
		go2250();
		updateErrorIndicators2270();
		readNextRecord2280();
	}

protected void go2250()
	{
		/*convert the estimated value if it is non zero.*/
		if (isNE(sv.hemv, ZERO)) {
			conlinkrec.amountIn.set(sv.hemv);
			callXcvrt2300();
			sv.estMatValue.set(conlinkrec.amountOut);
			wsaaEstimateTot.add(conlinkrec.amountOut);
		}
		/*convert the actual value if it is non zero.*/
		if (isNE(sv.hactual, ZERO)) {
			conlinkrec.amountIn.set(sv.hactual);
			callXcvrt2300();
			sv.actvalue.set(conlinkrec.amountOut);
			wsaaActualTot.add(conlinkrec.amountOut);
		}
		sv.cnstcur.set(conlinkrec.currOut);
	}

protected void updateErrorIndicators2270()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5318", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextRecord2280()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5318", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callXcvrt2300()
	{
		g02350();
	}

protected void g02350()
	{
		conlinkrec.function.set("CVRT");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(sv.hcnstcur);
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(sv.currcd);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(chdrpf.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, "****")) {
			scrnparams.errorCode.set(conlinkrec.statuz);
			wsspcomn.edterror.set("Y");
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		callRounding5000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
	updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		if (isEQ(sv.bnfying, "X")) {
			return;
		}
	    if (isEQ(sv.fupflg, "X")) {
	    	return;
		}
		if(CMOTH003Permission){
		if (isEQ(sv.investres, "X")) {
			return;
		}
		if (isEQ(sv.claimnotes, "X")) {
			return;
		}
		}
		if (isNE(wsaaChanged, "Y")) {
			return;
		}
		/*   restart the subfile and read each record and update the*/
		/*   claims detail file.*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5318", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			readSubfile3100();
		}
		
		adjustClmhHeader3400();
		//ILJ-383 starts
		if(CMDTH010Permission){
			updateCrsvRecord();
		}
		
		/*  Update the Client Details File with the Date of Death.         */
		updateClient3600();
		if(CMOTH003Permission){
			if(isNE(wsaaNotifinum,SPACES) && isNE(wsspcomn.flag,"I")){
				clnnpfDAO.updateClnnpfClaimno(sv.claimnumber.toString(),wsaaNotifinum.toString());
			}
			if(isNE(wsaaNotifinum,SPACES) && isNE(wsspcomn.flag,"I")){
				invspfDAO.updateInvspfClaimno(sv.claimnumber.toString(),wsaaNotifinum.toString());
			}		
		}
		/*  transfer the contract soft lock to AT*/
		sftlockrec.function.set("TOAT");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrpf.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*  call the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5318AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		/*MOVE VRCM-TERM            TO  ATRT-REQ-TERM.                 */
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrpf.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionRecInner.wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransactionRecInner.wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionRecInner.wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaTransactionRecInner.wsaaUser.set(varcom.vrcmUser);
		/*MOVE VRCM-TERM            TO  WSAA-TERMID.                   */
		wsaaTransactionRecInner.wsaaTermid.set(varcom.vrcmTermid);
		/* WSAA-OTHERADJST should not be set to whats on the screen at     */
		/* this point - it should hold the old 'Other Adjustments' value   */
		/* (got from CLMHCLM in 1000-Initialise setcion)                   */
		/*MOVE S5318-OTHERADJST      TO WSAA-OTHERADJST.          <011>*/
		atreqrec.transArea.set(wsaaTransactionRecInner.wsaaTransactionRec);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
        	
		
	
	}

protected void readSubfile3100()
	{
		/*READ-NEXT-RECORD*/
		/* CLMD data can not be updated from P5318 screen, so no           */
		/* reason to rewrite CLMD rec with new transaction number.         */
		/*    PERFORM 3300-ADJUST-CLMD-DETAIL.                             */
		scrnparams.function.set(varcom.srdn);
		processScreen("S5318", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*3300-ADJUST-CLMD-DETAIL    SECTION.                      <LA4544>
	************************************                      <LA4544>
	*3310-WRITE.                                              <LA4544>
	*                                                         <LA4544>
	*    MOVE SPACES                 TO CLMDADD-DATA-AREA.    <LA4544>
	*    MOVE WSSP-COMPANY           TO CLMDADD-CHDRCOY.      <LA4544>
	*    MOVE CHDRCLM-CHDRNUM        TO CLMDADD-CHDRNUM.      <LA4544>
	*    MOVE LIFECLM-LIFE           TO CLMDADD-LIFE.         <LA4544>
	*    MOVE S5318-VFUND            TO CLMDADD-VIRTUAL-FUND. <LA4544>
	*    MOVE S5318-FIELD-TYPE       TO CLMDADD-FIELD-TYPE.   <LA4544>
	*    MOVE S5318-HCOVER           TO CLMDADD-COVERAGE.     <LA4544>
	*    IF S5318-RIDER              = SPACES                 <LA4544>
	*       MOVE '00'                TO CLMDADD-RIDER         <LA4544>
	*    ELSE                                                 <LA4544>
	*       MOVE S5318-RIDER         TO CLMDADD-RIDER.        <LA4544>
	*    MOVE CLMDADDREC             TO CLMDADD-FORMAT.       <LA4544>
	*    MOVE READH                  TO CLMDADD-FUNCTION.     <LA4544>
	*    CALL 'CLMDADDIO'            USING CLMDADD-PARAMS.    <LA4544>
	*    IF CLMDADD-STATUZ           NOT = O-K                <LA4544>
	*       MOVE CLMDADD-PARAMS      TO SYSR-PARAMS           <LA4544>
	*       PERFORM 600-FATAL-ERROR.                          <LA4544>
	*                                                         <LA4544>
	*    MOVE LIFECLM-JLIFE          TO CLMDADD-JLIFE.        <LA4544>
	*    MOVE WSAA-CRTABLE           TO CLMDADD-CRTABLE.      <LA4544>
	**** MOVE S5318-CRTABLE          TO CLMDADD-CRTABLE.      <LA4544>
	*    MOVE S5318-SHORTDS          TO CLMDADD-SHORTDS.      <LA4544>
	*    MOVE CHDRCLM-TRANNO         TO CLMDADD-TRANNO.       <LA4544>
	*    MOVE S5318-LIENCD           TO CLMDADD-LIENCD.       <LA4544>
	**** MOVE S5318-RIIND            TO CLMDADD-RIIND.        <LA4544>
	*    MOVE S5318-CNSTCUR          TO CLMDADD-CNSTCUR.      <LA4544>
	*    MOVE S5318-EST-MAT-VALUE    TO CLMDADD-EST-MAT-VALUE.<LA4544>
	*    MOVE S5318-ACTVALUE         TO CLMDADD-ACTVALUE.     <LA4544>
	**** MOVE CDTH-TYPE              TO CLMDADD-FIELD-TYPE.   <LA4544>
	**** MOVE 'UPDAT'                TO CLMDADD-FUNCTION.     <LA4544>
	*    MOVE REWRT                  TO CLMDADD-FUNCTION.     <LA4544>
	*    MOVE CLMDADDREC             TO CLMDADD-FORMAT.       <LA4544>
	*    CALL 'CLMDADDIO'            USING CLMDADD-PARAMS.    <LA4544>
	*    IF CLMDADD-STATUZ           NOT = O-K                <LA4544>
	*       MOVE CLMDADD-PARAMS      TO SYSR-PARAMS           <LA4544>
	*       PERFORM 600-FATAL-ERROR.                          <LA4544>
	*                                                         <LA4544>
	*3390-EXIT.                                               <LA4544>
	*     EXIT.                                               <LA4544>
	* </pre>
	*/
protected void adjustClmhHeader3400()
	{
		updatClmhclm3410();
		write3420();
	}

protected void updatClmhclm3410()
	{
		clmhpfDAO.updateClmhpfValidFlag(wsaaUniqueNumber,"2");
	}

	/**
	* <pre>
	*3410-WRITE.                                                      
	* </pre>
	*/
protected void write3420()
	{
		clmhclm.setValidflag("1");
		clmhclm.setCurrcd(sv.currcd.toString());
		clmhclm.setDtofdeath(sv.dtofdeath.toInt());
		clmhclm.setEffdate(sv.effdate.toInt());
		clmhclm.setTranno(chdrpf.getTranno());
		clmhclm.setReasoncd(sv.reasoncd.toString());
		/* MOVE S5318-RESNDESC         TO CLMHCLM-RESNDESC.             */
		clmhclm.setResndesc(sv.longdesc.toString());
		clmhclm.setPolicyloan(sv.policyloan.getbigdata());
		clmhclm.setTdbtamt(sv.tdbtamt.getbigdata());
		clmhclm.setZrcshamt(sv.zrcshamt.getbigdata());
		clmhclm.setCauseofdth(sv.causeofdth.toString());
		clmhclm.setOtheradjst(sv.otheradjst.getbigdata());
		clmhclm.setIntdays(0);
		clmhclm.setInterest(new BigDecimal(0));
		clmhclm.setOfcharge(new BigDecimal(0));
		clmhclm.setCondte(sv.contactDate.toInt());//ILJ-766
		/* MOVE UPDAT                  TO CLMHCLM-FUNCTION.     <LA4544>*/
		if (wsaaToday.equals(0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		if(!(wsaaToday.equals(clmhclm.getTrdt()))) {
			//clmhclm.setTrdt(wsaaToday.toInt());
			clmhclm.setTrdt(varcom.vrcmDate.toInt()); //IBPLIFE-3359
		} 
		clmhclm.setTrtm(varcom.vrcmTime.toInt());
		clmhpfDAO.insert(clmhclm);
	}
	
protected void updateCrsvRecord(){
	List<Crsvpf> crsvpflist = crsvpfDAO.getCrsvpfRecord(chdrpf.getChdrnum(), "L", "1", "T668");
	if(null != crsvpflist  && !crsvpflist.isEmpty()){
		crsvpfDAO.updateValidFlgCrsvRecord(crsvpflist.get(0),"2");
	
	Crsvpf crsvpf = crsvpflist.get(0);
	
	crsvpf.setTranno(chdrpf.getTranno());
	crsvpf.setValidflag("1");
	crsvpf.setCondte(sv.contactDate.toInt());
	crsvpf.setTrcode(wsaaBatckey.batcBatctrcde.toString());
	crsvpf.setTrdt(wsaaToday.toInt());
	crsvpfDAO.insertCrsvRecord(crsvpf);
	}
}
	//ILJ-383 ends
protected void updateClient3600()
	{
		para3600();
	}

protected void para3600()
	{
		/*  Initialise the Date of Death.                                  */
		cltsIO.setCltdod(varcom.vrcmMaxDate);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		/*     MOVE CHDRCLM-COWNNUM      TO CLTS-CLNTNUM.                  */
		cltsIO.setClntnum(wsaaDeadLife);
		/*  Read and hold the record for updating.                         */
		cltsIO.setFunction(varcom.readh);
		getClientDetails3700();
		/* IF CLTS-STATUZ               = O-K                   <V76F10>*/
		/*    MOVE '2'                 TO CLTS-VALIDFLAG        <V76F10>*/
		/*    MOVE CLTSREC             TO CLTS-FORMAT           <V76F10>*/
		/*    MOVE REWRT               TO CLTS-FUNCTION         <V76F10>*/
		/*                                                      <V76F10>*/
		/*    CALL 'CLTSIO'         USING CLTS-PARAMS           <V76F10>*/
		/*    IF CLTS-STATUZ        NOT = O-K                   <V76F10>*/
		/*       MOVE CLTS-PARAMS      TO SYSR-PARAMS           <V76F10>*/
		/*       MOVE CLTS-STATUZ      TO SYSR-STATUZ           <V76F10>*/
		/*       PERFORM 600-FATAL-ERROR                        <V76F10>*/
		/*    END-IF                                            <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		/*                                                      <V76F10>*/
		/*  Update the Date of Death.                                      */
		cltsIO.setCltdod(sv.dtofdeath);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		/*     MOVE CHDRCLM-COWNNUM      TO CLTS-CLNTNUM.                  */
		cltsIO.setClntnum(wsaaDeadLife);
		/*  Rewrite the Client Details record with the updated DOD.        */
		/*  MOVE REWRT                TO CLTS-FUNCTION.         <V73F02>*/
		/*  MOVE '1'                  TO CLTS-VALIDFLAG.        <V76F10>*/
		/*  MOVE WRITR                TO CLTS-FUNCTION.         <V76F10>*/
		cltsIO.setFunction(varcom.rewrt);
		getClientDetails3700();
	}

protected void getClientDetails3700()
	{
		/*PARA*/
		/*  Call to I-O Module.                                            */
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsspcomn.nextprog.set(wsaaProg);
		/*  If first time into this section (stack action blank)*/
		/*  save next eight programs in stack*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
		if (isEQ(sv.fupflg, "?")) {
			checkFollowups1850();
		}
		if (isEQ(sv.fupflg, "X")) {
			gensswrec.function.set("A");
			sv.fupflg.set("?");
			callGenssw4300();
			return ;
		}
		/*  Check if beneficiary selected previously*/
		if (isEQ(sv.bnfying, "?")) {
			checkBeneficiaries3800();
		}
		if (isEQ(sv.bnfying, "X")) {
            wsaaTotalClaim.set(sv.totclaim);
            sd5jl.totalAmount.set(wsaaTotalClaim);
			gensswrec.function.set("B");
			sv.bnfying.set("?");
			callGenssw4300();
			return;
		}
		if(CMOTH003Permission){
		    if (isEQ(sv.investres, "?")) {
		    	checkInvspf();
			}
			if (isEQ(sv.investres, "X")) {
				gensswrec.function.set("C");
				sv.investres.set("?");
				callGenssw4300();
				return ;
			}
			if (isEQ(sv.claimnotes, "?")) {
				checkClnnpf();
			}
			if (isEQ(sv.claimnotes, "X")) {
				gensswrec.function.set("D");
				sv.claimnotes.set("?");
				setupNotipf();
				callGenssw4300();
				return ;
			}
			}
		/*   No more selected (or none)*/
		/*      - restore stack form wsaa to wssp*/
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			restoreProgram4200();
		}
		/*  If current stack action is * then re-display screen*/
		/*     (in this case, some other option(s) were requested)*/
		/*  Otherwise continue as normal.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void setupNotipf(){
	wsspcomn.chdrCownnum.set(sv.lifcnum.toString());	
	wsspcomn.wsaaclaimno.set(sv.claimnumber.toString().trim());
	if(isEQ(sv.aacct,SPACES)){
		wsspcomn.wsaarelationship.set(SPACES);
		wsspcomn.wsaaclaimant.set(sv.lifcnum.toString());
		wsspcomn.wsaanotificationNum.set(SPACES);
	}
	else{
		notipf = notipfDAO.getNotiReByNotifin(wsaaNotifinum.toString(),wsspcomn.company.toString());
		wsspcomn.wsaarelationship.set(notipf.getRelationcnum());
		wsspcomn.wsaaclaimant.set(notipf.getClaimant());
		wsspcomn.wsaanotificationNum.set(sv.aacct.toString());
	}
}
protected void checkClnnpf(){
	
	if(clnnpfList1!=null && !clnnpfList1.isEmpty()){
		sv.claimnotes.set("+");
	}
	else{
		sv.claimnotes.set(SPACES);
	}
}

protected void checkInvspf(){
	
	if(invspfList1!=null && !invspfList1.isEmpty()){
		sv.investres.set("+");
	}
	else{
		sv.investres.set(SPACES);
	}
}
	protected void checkBeneficiaries3800()
	{
        List<Cpbnfypf> cpbnfypfList = cpbnfypfDAO.searchCpbnfypfRecord(wsspcomn.company.toString(),chdrpf.getChdrnum());
        if (cpbnfypfList!=null && !cpbnfypfList.isEmpty()) {
            if ((isNE(cpbnfypfList.get(0).getChdrcoy(), chdrpf.getChdrcoy()))
                    || (isNE(cpbnfypfList.get(0).getChdrnum(), chdrpf.getChdrnum()))) {
                sv.bnfying.set(SPACES);
            } else {
                sv.bnfying.set("+");
            }
        }
	}
protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
		/*CALL-SUBROUTINE*/
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/*    load from gensw to wssp*/
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void loadProgram4400()
	{
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(sv.currcd);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-TRANSACTION-REC--INNER
 */
private static final class WsaaTransactionRecInner { 

		/*01  WSAA-TRANSACTION-REC.*/
	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(215);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaClamamtOld = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 1);
	private PackedDecimalData wsaaClamamtNew = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 10);
	private PackedDecimalData wsaaOtheradjst = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 19);
	private FixedLengthStringData wsaaCurrcd = new FixedLengthStringData(3).isAPartOf(wsaaTransactionRec, 37);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 40);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 44);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 48);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 52);
	private FixedLengthStringData filler = new FixedLengthStringData(159).isAPartOf(wsaaTransactionRec, 56, FILLER).init(SPACES);
}

//MIBT-181
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData f335 = new FixedLengthStringData(4).init("F335");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
}

//ILIFE-1137
protected void readTabT5688250()
{
	read251();
}

protected void read251()
{
	itdmIO.setItemcoy(chdrpf.getChdrcoy());
	itdmIO.setItemtabl(t5688);
	itdmIO.setItempfx("IT");
	itdmIO.setItemitem(chdrpf.getCnttype());
	itdmIO.setItmfrm(sv.effdate);
	itdmIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(itdmIO.getStatuz());
		fatalError600();
	}
	if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
	|| isNE(itdmIO.getItemtabl(), t5688)
	|| isNE(itdmIO.getItemitem(), chdrpf.getCnttype())
	|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
		itdmIO.setItemitem(chdrpf.getCnttype());
		syserrrec.params.set(itdmIO.getParams());
		syserrrec.statuz.set(e308);
		fatalError600();
	}
	t5688rec.t5688Rec.set(itdmIO.getGenarea());
}

	protected S5318ScreenVars getLScreenVars(){
		
		return ScreenProgram.getScreenVars( S5318ScreenVars.class);
		
	}

protected void CalcCustomerSpecifc() {
	
}

protected void retrieveSuspenseCustomerSpecifc() {
	
}

protected void customerSpecificPensionCalc() {
	
}

protected void setLifeAsClmdParam() {
	
}

protected void setLifeAsClmdSearchKey(){

}
protected void balErrCustomerSpecific2010(){

}

	
public StringUtil getStringUtil() {
	return stringUtil;
}
}
