package com.csc.life.terminationclaims.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:22
 * Description:
 * Copybook name: CHDRRGPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrrgpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrrgpFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrrgpKey = new FixedLengthStringData(64).isAPartOf(chdrrgpFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrrgpChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrrgpKey, 0);
  	public FixedLengthStringData chdrrgpChdrnum = new FixedLengthStringData(8).isAPartOf(chdrrgpKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrrgpKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrrgpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrrgpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}